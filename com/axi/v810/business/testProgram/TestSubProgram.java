package com.axi.v810.business.testProgram;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.communication.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * An TestSubProgram contains all the InspectionRegions that are associated with
 * a single CoordinateConverter.  For example, on a multi-up panel that uses board-based
 * alignment, each board's InspectionRegions will be contained in a separate
 * TestSubProgram.   Or, in a divided long panel that is inspected at two
 * different PIP stops (and therefore requires two separate alignments), there
 * will be two TestSubPrograms.
 *
 * @author Peter Esbensen
 */
public class TestSubProgram implements Serializable
{

  private int _id = -1;
  private static int _numSubPrograms = 0;
  private TestProgram _testProgram;
  private Collection<ReconstructionRegion> _inspectionRegions = Collections.emptyList();
  private Collection<ReconstructionRegion> _3DAlignmentRegions = Collections.emptyList();
  private Collection<ReconstructionRegion> _verificationRegions = Collections.emptyList();
  private Map<String, ReconstructionRegion> _nameToInspectionRegionMap = new HashMap<String, ReconstructionRegion>();
  private transient Collection<ReconstructionRegion> _adjustFocusInspectionRegions;
  private transient Map<String, List<ReconstructionRegion>> _nameToAdjustFocusRegionsMap = new HashMap<String, List<ReconstructionRegion>>();
  private List<AlignmentGroup> _alignmentGroups;
  private PanelLocationInSystemEnum _panelLocationInSystemEnum = PanelLocationInSystemEnum.RIGHT;
  private Map<Pad, JointInspectionData> _padToJointInspectionDataMap = new HashMap<Pad, JointInspectionData>();
  private Map<Pad, JointAlignmentData> _padToJointAlignmentDataMap = new HashMap<Pad, JointAlignmentData>();
  private Map<Component, ComponentAlignmentData> _componentToComponentAlignmentDataMap = new HashMap<Component, ComponentAlignmentData>();
  private Map<Subtype, Collection<SliceNameEnum>> _subtypeToInspectedSlicesMap = new HashMap<Subtype, Collection<SliceNameEnum>>();
  // Bounding region for the test sub-program
  private PanelRectangle _inspectionRegionsBoundsInNanoMeters;
  private PanelRectangle _verificationRegionsBoundsInNanoMeters;
  private PanelRectangle _imageableRegionInNanoMeters;
  private PanelRectangle _alignmentImageableRegionInNanoMeters;
  private int _inspectableAreaLengthInNanoMeters = -1;
  private List<ProcessorStrip> _processorStripsForVerificationRegions;
  private List<ProcessorStrip> _processorStripsForInspectionRegions;
  private transient List<ScanPath> _scanPathOverInspectionOrVerificationArea;
  private transient List<ScanPath> _alignmentScanPaths;
  private transient List<ScanPath> _rescanedReconstructionRegionPaths;
  private transient Set<ReconstructionRegion> _sortedInspectionRegions;
  private transient Set<ReconstructionRegion> _sortedAlignmentRegions;
  private transient Set<ReconstructionRegion> _sortedVerificationRegions;
  private int _numVerificationRegionColumns = -1;
  private int _numVerificationRegionRows = -1;
  private Board _board = null;
  //user gain
  private UserGainEnum _userGain = UserGainEnum.getUserGainToEnumMapValue((int)Config.getInstance().getDoubleValue(HardwareConfigEnum.AXI_TDI_CAMERA_USER_GAIN));
  //Kee, Chin Seong - To Prevent alignment been overwrited when using custom alignment,
  //                  new value have been introduced to overcome that. and default is GAIN ONE
  private UserGainEnum _verficationUserGain = UserGainEnum.ONE;
  private StageSpeedEnum _verficationStageSpeed = StageSpeedEnum.ONE;
  private StageSpeedEnum _stageSpeed = StageSpeedEnum.ONE;
  private UserGainEnum _virtualLiveAlignmentUserGain = UserGainEnum.ONE;
  private StageSpeedEnum _virtualLiveAlignmentStageSpeed = StageSpeedEnum.ONE;
  private int _subProgramRefferenceIdForAlignment;
  private boolean _isSubProgramPerformAlignment = false;
  private boolean _isSubProgramPerformSurfaceMap = false;
  //variable Magnification
  private MagnificationTypeEnum _magnificationType = null;
  private PanelRectangle _realVerificationRegionsBoundsInNanoMeters;
  // PSP
  private List<OpticalRegion> _opticalRegions = new ArrayList<OpticalRegion>();
  private transient List<OpticalPointToPointScan> _opticalPointToPointScanPath;
  //private List<String> _filteredComponentReferenceDesignators = new ArrayList<String>();
  // Comments added by Seng Yew on 23-May-2012 for clarity. Hope no one breaks this rule.
  // The order of the stored pairs in both container below are not important.
  // If need to sort whenever add pair, use TreeMap.
  // If need to make the order to follow which pair first appear, use LinkedHashMap.
  private Map<String, Integer> _opticalRegionNameToSubProgramReferenceIdMap = new HashMap<String, Integer>();
  private Map<String, List<Component>> _opticalRegionNameToComponentsMap = new HashMap<String, List<Component>>();
  
  private List<OpticalRegion> _alignmentOpticalRegions = new ArrayList<OpticalRegion>();
  private transient List<OpticalPointToPointScan> _alignmentOpticalPointToPointScanPath;
  private transient List<Double> _estimatedBoardThicknessInNanometersList;
  private transient double _estimateTestSubProgramExecutionTime; //Lim Lay Ngor add
  
  private boolean _isEstimatedBoardThicknessComputed = false;
  private double _estimatedBoardThicknessInNanometers = 0.0;
  
  //@author Kee chin Seong - XXL Speed Improvement Development
  private Map<String, List<ReconstructionRegion>> _componentToReconstructionRegionMap = new HashMap<String, List<ReconstructionRegion>>();
  private Map<Subtype, List<ReconstructionRegion>> _subtypeToReconstructionRegionMap = new HashMap<Subtype, List<ReconstructionRegion>>();
  
  // Added by Khang Wah, 2013-08-23, New Scan Route
  private transient Map<ScanPass, ImageGroup> _scanPassToImageGroupMap;
  
  // Added by Khang Wah, 2013-011-05, Real Time PSH
  private boolean _isSufficientMemoryForRealTimePsh = false;
  private Collection<ReconstructionRegion> _isolatedGlobalSurfaceRegions = new ArrayList<ReconstructionRegion>();
  
  private boolean _isUsingRuntimeManualAlignment = false;

  // Khaw Chek Hau - XCR2181 - Able to run alignment starting from failed board
  private boolean _isRunAlignmentLearned = false;
  private boolean _isInspectionDone = false;
  // Kok Chun, Tan - XCR3051 - use to check the test sub program is done inspect
  private boolean _isTestSubProgramDone = false;

  //Khaw Chek Hau - XCR2285: 2D Alignment on v810
  private Collection<ReconstructionRegion> _2DAlignmentRegions = Collections.emptyList();
  
  /**
   * @author Wei Chin, Chong
   */
  private static Comparator<ReconstructionRegion> _reconstructionRegionComparator = new Comparator<ReconstructionRegion>()
  {

    public int compare(ReconstructionRegion lhs, ReconstructionRegion rhs)
    {
      return lhs.getName().compareTo(rhs.getName());
    }
  };

  /**
   * @author Peter Esbensen
   */
  public TestSubProgram(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    ++_numSubPrograms;
    _id = _numSubPrograms;
    _testProgram = testProgram;
  }

  /**
   * @author Bill Darbie
   */
  public void setId(int id)
  {
    Assert.expect(id >= 0);
    _id = id;
  }

  /**
   * @author Bill Darbie
   */
  public int getId()
  {
    Assert.expect(_id >= 0);
    return _id;
  }

  /**
   * @author Matt Wharton
   */
  public TestProgram getTestProgram()
  {
    Assert.expect(_testProgram != null);

    return _testProgram;
  }

  /**
   * @author George A. David
   * @author Roy Williams
   */
  public PanelRectangle getInspectionRegionsBoundsInNanoMeters()
  {
    Assert.expect(_imageableRegionInNanoMeters != null);
    if (_inspectionRegionsBoundsInNanoMeters == null)
    {
      for (ReconstructionRegion inspectionRegion : _inspectionRegions)
      {
        if (_inspectionRegionsBoundsInNanoMeters == null)
        {
          _inspectionRegionsBoundsInNanoMeters = new PanelRectangle(inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters());
        }
        else
        {
          _inspectionRegionsBoundsInNanoMeters.add(inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters());
        }
      }

//      TestSubProgram rightProgram = null;
//      if(_testProgram.hasTestSubProgram(PanelLocationInSystemEnum.RIGHT))
//        rightProgram = _testProgram.getTestSubProgram(PanelLocationInSystemEnum.RIGHT);

      for (Fiducial fiducial : getFiducialsForTestProgram())
      {
        Rectangle2D fiducialShapeBounds = fiducial.getShapeRelativeToPanelInNanoMeters().getBounds2D();

        boolean addFiducialToRegion = false;
//        if(rightProgram == null || this == rightProgram)
//        {
        if (_imageableRegionInNanoMeters.contains(fiducialShapeBounds))
        {
          addFiducialToRegion = true;
        }
//        }
//        else // it's a long program and we are the left side of a long program
//        {
//          // only add it if it does not fit in the right region and it fits in the left region.
//          if(rightProgram.getImageableRegionInNanoMeters().contains(fiducialShapeBounds) == false &&
//            _imageableRegionInNanoMeters.contains(fiducialShapeBounds))
//          addFiducialToRegion = true;
//        }

        if (addFiducialToRegion)
        {
          if (_inspectionRegionsBoundsInNanoMeters == null)
          {
            _inspectionRegionsBoundsInNanoMeters = new PanelRectangle(fiducialShapeBounds);
          }
          else
          {
            _inspectionRegionsBoundsInNanoMeters.add(fiducialShapeBounds);
          }
        }
      }

//      // in some cases, the user is using an unloaded component for alignment purposes,
//      // so we need to make sure the bounds takes this into account.
//      for(ReconstructionRegion alignmentRegion : _alignmentRegions)
//      {
//        for(Pad pad : alignmentRegion.getAlignmentGroup().getPads())
//        {
//          if(pad.getComponent().isLoaded() == false)
//          {
//
//          }
//        }
//      }
//
      Assert.expect(_inspectionRegionsBoundsInNanoMeters != null);

      // ok, now add the alignment uncertainty to all sides
      addAlignmentUncertainty(_inspectionRegionsBoundsInNanoMeters);
    }

    return _inspectionRegionsBoundsInNanoMeters;
  }

  /**
   * @author George A. David
   */
  private static void addAlignmentUncertainty(PanelRectangle panelRectangle)
  {
    Assert.expect(panelRectangle != null);

    panelRectangle.setRect(panelRectangle.getMinX() - Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
            panelRectangle.getMinY() - Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
            panelRectangle.getWidth() + (2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters()),
            panelRectangle.getHeight() + (2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters()));
  }

  /**
   * @author Roy Williams
   */
  public List<Fiducial> getFiducialsForTestProgram()
  {
    Panel panel = _testProgram.getProject().getPanel();
    List<Fiducial> fiducials = panel.getFiducials();
    for (Board board : panel.getBoards())
    {
      fiducials.addAll(board.getFiducials());
    }
    return fiducials;
  }

  /**
   * This code originally found in getInspectionRegionsBoundsInNanoMeters() was moved
   * to this separate function so other objects could use the same algorithm.
   *
   * @author George A. David
   * @author Roy Williams
   */
  public static PanelRectangle calculateRegionBounds(Collection<ReconstructionRegion> regions,
          List<Fiducial> fiducials)
  {
    Assert.expect(regions != null);
    Assert.expect(fiducials != null);

    PanelRectangle panelRectangle = null;

    for (ReconstructionRegion inspectionRegion : regions)
    {
      if (panelRectangle == null)
      {
        panelRectangle = new PanelRectangle(inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters());
      }
      else
      {
        panelRectangle.add(inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters());
      }
    }

    for (Fiducial fiducial : fiducials)
    {
      Rectangle2D fiducialShapeBounds = fiducial.getShapeRelativeToPanelInNanoMeters().getBounds2D();

      if (panelRectangle.contains(fiducialShapeBounds) == false)
      {
        if (panelRectangle == null)
        {
          panelRectangle = new PanelRectangle(fiducialShapeBounds);
        }
        else
        {
          panelRectangle.add(fiducialShapeBounds);
        }
      }
    }
    Assert.expect(panelRectangle != null);

    // ok, now add the alignment uncertainty to all sides
    addAlignmentUncertainty(panelRectangle);

    return panelRectangle;
  }

  /**
   * @author George A. David
   */
  public PanelRectangle getVerificationRegionsBoundsInNanoMeters()
  {
    Assert.expect(_verificationRegionsBoundsInNanoMeters != null);

    PanelRectangle bounds = new PanelRectangle(_verificationRegionsBoundsInNanoMeters);
    
    // wei chin : will not happen!
    // ----------------------------------
    // sometimes the alignment regions are not included in the
    // verification region bounds. This should only happen for long panel.
    // at any rate, add the alignment bounds to the verification region bounds.
    // add the alignment group bounds
//    if (_alignmentRegions != null)
//    {
//      for (AlignmentGroup group : getAlignmentGroups())
//      {
//        if (group.isEmpty())
//        {
//          continue;
//        }
//        bounds.add(group.getBoundsRelativeToPanelInNanoMeters());
//      }
//    }

    // now add the alignment uncertainty
    bounds.setRect(bounds.getMinX() - Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
            bounds.getMinY() - Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
            bounds.getWidth() + (2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters()),
            bounds.getHeight() + (2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters()));

    return bounds;
  }

  /**
   * @author George A. David
   */
  public PanelRectangle getVerificationRegionsBoundsInNanoMetersIgnoringAlignmentRegions()
  {
    Assert.expect(_verificationRegionsBoundsInNanoMeters != null);

    PanelRectangle bounds = new PanelRectangle(_verificationRegionsBoundsInNanoMeters);

    // now add the alignment uncertainty
    bounds.setRect(bounds.getMinX() - Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
            bounds.getMinY() - Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
            bounds.getWidth() + (2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters()),
            bounds.getHeight() + (2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters()));

    return bounds;
  }

  /**
   * @author George A. David
   */
  public void setVerificationRegionsBoundsInNanoMeters(PanelRectangle verificationRegionsBoundsInNanoMeters)
  {
    Assert.expect(verificationRegionsBoundsInNanoMeters != null);

    _verificationRegionsBoundsInNanoMeters = verificationRegionsBoundsInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public synchronized boolean hasInspectionPad(Pad pad)
  {
    Assert.expect(pad != null);

    return _padToJointInspectionDataMap.containsKey(pad);
  }

  /**
   * @author Roy Williams
   */
  public synchronized boolean hasAlignmentPad(Pad pad)
  {
    Assert.expect(pad != null);

    return _padToJointAlignmentDataMap.containsKey(pad);
  }

  /**
   * @author Matt Wharton
   */
  boolean componentAlignmentDataExists(Component component)
  {
    Assert.expect(component != null);

    return (_componentToComponentAlignmentDataMap.containsKey(component));
  }

  /**
   * Get the JointInspectionData object associated with a certain Pad object.
   *
   * @author Peter Esbensen
   */
  public synchronized JointInspectionData getJointInspectionData(Pad pad)
  {
    Assert.expect(pad != null);

    JointInspectionData inspectionRegionPad = _padToJointInspectionDataMap.get(pad);
    Assert.expect(inspectionRegionPad != null);
    return inspectionRegionPad;
  }

  /**
   * Get the JointAlignmentData object associated with a certain Pad object.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public synchronized JointAlignmentData getJointAlignmentData(Pad pad)
  {
    Assert.expect(pad != null);

    JointAlignmentData jointAlignmentData = _padToJointAlignmentDataMap.get(pad);
    Assert.expect(jointAlignmentData != null);

    return jointAlignmentData;
  }

  /**
   * @author Matt Wharton
   */
  ComponentAlignmentData getComponentAlignmentData(Component component)
  {
    Assert.expect(component != null);

    ComponentAlignmentData componentAlignmentData = _componentToComponentAlignmentDataMap.get(component);
    Assert.expect(componentAlignmentData != null);

    return componentAlignmentData;
  }

  /**
   * Get the InspectionRegion object associated with a certain Pad object.
   *
   * @author Peter Esbensen
   */
  public synchronized ReconstructionRegion getInspectionRegion(Pad pad)
  {
    return getJointInspectionData(pad).getInspectionRegion();
  }

  /**
   * @author Matt Wharton
   */
  void addComponentToAlignmentMap(Component component, ComponentAlignmentData componentAlignmentData)
  {
    Assert.expect(component != null);
    Assert.expect(componentAlignmentData != null);

    _componentToComponentAlignmentDataMap.put(component, componentAlignmentData);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void syncInspectionRegions()
  {
    syncInternalDataStructuresWithInspectionRegions();
  }

  /**
   * @author Peter Esbensen
   */
  private void syncInternalDataStructuresWithInspectionRegions()
  {
    Assert.expect(_inspectionRegions != null);

    _padToJointInspectionDataMap.clear();
    _subtypeToInspectedSlicesMap.clear();
    _testProgram.clearComponentMap();
    _nameToInspectionRegionMap.clear();

    for (ReconstructionRegion region : _inspectionRegions)
    {
      region.setTestSubProgram(this);
      _nameToInspectionRegionMap.put(region.getName(), region);
      Collection<JointInspectionData> inspectionRegionPads = region.getJointInspectionDataList();
      for (JointInspectionData jointInspectionData : inspectionRegionPads)
      {
        _padToJointInspectionDataMap.put(jointInspectionData.getPad(), jointInspectionData);

//        ComponentInspectionData componentInspectionData = _componentToComponentInspectionDataMap.get(
//            jointInspectionData.getComponent());
//
//        if (componentInspectionData == null)
//        {
//          Component component = jointInspectionData.getComponent();
//          componentInspectionData = new ComponentInspectionData(_testProgram);
//          componentInspectionData.setComponent(jointInspectionData.getComponent());
//          _componentToComponentInspectionDataMap.put(component, componentInspectionData);
//        }
//        componentInspectionData.addJointInspectionData(jointInspectionData);
//        jointInspectionData.setComponentInspectionData(componentInspectionData);
      }
    }
  }

  /**
   * @author George A. David
   */
  public boolean hasSubtype(Subtype subtype)
  {
    Assert.expect(subtype != null);

    if (_subtypeToInspectedSlicesMap.isEmpty())
    {
      createInternalSubtypeToSlicesMap();
    }

    return _subtypeToInspectedSlicesMap.containsKey(subtype);
  }

  /**
   * @author George A. David
   */
  private void createInternalSubtypeToSlicesMap()
  {
    for (ReconstructionRegion region : _inspectionRegions)
    {
      Collection<JointInspectionData> inspectionRegionPads = region.getJointInspectionDataList();
      for (JointInspectionData jointInspectionData : inspectionRegionPads)
      {
        Subtype subtype = jointInspectionData.getSubtype();
        //Collection<SliceNameEnum> sliceNames = region.getInspectedSliceNames(subtype);
        // Jack Hwee - sometimes the removeAll function will return UnsupportedOperationException, change collection init to below to avoid. 
        Collection<SliceNameEnum> sliceNames= new ArrayList(region.getInspectedSliceNames(subtype)); 
        Assert.expect(sliceNames.isEmpty() == false);
        if (_subtypeToInspectedSlicesMap.containsKey(subtype))
        {
          sliceNames.removeAll(_subtypeToInspectedSlicesMap.get(subtype));
          Assert.expect(sliceNames.isEmpty());
        }
        else
        {
          _subtypeToInspectedSlicesMap.put(subtype, sliceNames);
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  public Collection<SliceNameEnum> getSliceNames(Subtype subtype)
  {
    Assert.expect(subtype != null);

    if (_subtypeToInspectedSlicesMap.isEmpty())
    {
      createInternalSubtypeToSlicesMap();
    }

    Collection<SliceNameEnum> sliceNames = _subtypeToInspectedSlicesMap.get(subtype);

    Assert.expect(sliceNames != null);
    return sliceNames;
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  private void syncAlignmentRegionDataStructures()
  {
    Assert.expect(_3DAlignmentRegions != null);

    if (_3DAlignmentRegions.isEmpty() == false)
    {
      _padToJointAlignmentDataMap.clear();

      for (ReconstructionRegion region : _3DAlignmentRegions)
      {
        region.setTestSubProgram(this);

        // Add the alignment pads to the map.
        for (JointInspectionData jointInspectionData : region.getJointInspectionDataList())
        {
          Assert.expect(jointInspectionData instanceof JointAlignmentData);

          _padToJointAlignmentDataMap.put(jointInspectionData.getPad(), (JointAlignmentData) jointInspectionData);
        }

        // Add the context alignment pads to the map.
        for (JointInspectionData contextJointInspectionData : region.getAlignmentContextFeatures())
        {
          Assert.expect(contextJointInspectionData instanceof JointAlignmentData);

          _padToJointAlignmentDataMap.put(contextJointInspectionData.getPad(), (JointAlignmentData) contextJointInspectionData);
        }
      }
    }
  }

  /**
   * @author Peter Esbensen
   */
  public void setInspectionRegions(Collection<ReconstructionRegion> inspectionReconstructionRegions)
  {
    Assert.expect(inspectionReconstructionRegions != null);
    _inspectionRegions = inspectionReconstructionRegions;
    Assert.expect(_testProgram != null);
    syncInternalDataStructuresWithInspectionRegions();
  }

  /**
   * @author George A. David
   */
  public Collection<ReconstructionRegion> getAllInspectionRegions()
  {
    Assert.expect(_inspectionRegions != null);

    List<ReconstructionRegion> regions = new LinkedList<ReconstructionRegion>(_inspectionRegions);
    if (_adjustFocusInspectionRegions != null)
    {
      regions.addAll(_adjustFocusInspectionRegions);
    }

    return regions;
  }

  /**
   * @author George A. David
   */
  public Collection<ReconstructionRegion> getFilteredInspectionRegions()
  {
    List<ReconstructionRegion> inspectionReconstructionRegions = new LinkedList<ReconstructionRegion>();

    for (ReconstructionRegion reconstructionRegion : getAllInspectionRegions())
    {
      if (_testProgram.doesRegionPassFilters(reconstructionRegion))
      {
        // ok, the region passed all the filters, add it to the list
        inspectionReconstructionRegions.add(reconstructionRegion);
      }
    }
    return inspectionReconstructionRegions;
  }

  /**
   * Returns the verification regions associated with this Test Program
   * @return Collection
   * @author Dave Ferguson
   */
  public Collection<ReconstructionRegion> getVerificationRegions()
  {
    Assert.expect(_verificationRegions != null);

    List<ReconstructionRegion> regions = new ArrayList<ReconstructionRegion>(_verificationRegions);

    return regions;
  }

  /**
   * @author George A. David
   */
  public void setVerificationRegions(Collection<ReconstructionRegion> verificationReconstructionRegions)
  {
    Assert.expect(verificationReconstructionRegions != null);

    _verificationRegions = new ArrayList<ReconstructionRegion>(verificationReconstructionRegions);
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   * @author Khaw Chek Hau
   * @XCR-3804: Software crashed when generate Virtual Live image with 2D alignment recipe
   */
  public void setAlignmentRegions(Collection<ReconstructionRegion> alignmentReconstructionRegions)
  {
    Assert.expect(alignmentReconstructionRegions != null);

    //Khaw Chek Hau - XCR-3804: Software crashed when generate Virtual Live image with 2D alignment recipe
    if (getTestProgram().getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod())
    {
      set2DAlignmentRegions(alignmentReconstructionRegions);
    }
    else
    {
      set3DAlignmentRegions(alignmentReconstructionRegions);
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3804: Software crashed when generate Virtual Live image with 2D alignment recipe
   */
  public void set3DAlignmentRegions(Collection<ReconstructionRegion> alignmentReconstructionRegions)
  {
    Assert.expect(alignmentReconstructionRegions != null);

    _3DAlignmentRegions = alignmentReconstructionRegions;
    _alignmentGroups = null;
    syncAlignmentRegionDataStructures();
//    syncAlignmentRegionDataWithinBoundStructures();
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void set2DAlignmentRegions(Collection<ReconstructionRegion> alignmentReconstructionRegions)
  {
    Assert.expect(alignmentReconstructionRegions != null);

    _2DAlignmentRegions = alignmentReconstructionRegions;
    _alignmentGroups = null;
    
    for (ReconstructionRegion region : _2DAlignmentRegions)
    {
      region.setTestSubProgram(this);
    }
  }
  
  /**
   * @return a Collection of AlignmentRegion objects
   * @author Peter Esbensen
   */
  public Collection<ReconstructionRegion> getAlignmentRegions()
  {
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    if (getTestProgram().getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod())
    {
      Assert.expect(_2DAlignmentRegions != null);
      return _2DAlignmentRegions;
    }
    else
    {
      Assert.expect(_3DAlignmentRegions != null);
      return _3DAlignmentRegions;
    }
  }
  
  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public Collection<AlignmentGroup> getAlignmentGroups()
  {
    if (_alignmentGroups == null)
    {
      _alignmentGroups = new LinkedList<AlignmentGroup>();
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      for (ReconstructionRegion alignmentRegion : getAlignmentRegions())
      {
        _alignmentGroups.add(alignmentRegion.getAlignmentGroup());
      }
    }

    return _alignmentGroups;
  }

  /**
   * @author George A. David
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public ReconstructionRegion getAlignmentRegion(AlignmentGroup alignmentGroup)
  {
    ReconstructionRegion alignmentRegion = null;
    Collection<ReconstructionRegion> alignmentRegions;
    
    if (getTestProgram().getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod())
      alignmentRegions = _2DAlignmentRegions;
    else
      alignmentRegions = _3DAlignmentRegions;
    
    for (ReconstructionRegion region : alignmentRegions)
    {
      if (region.getAlignmentGroup() == alignmentGroup)
      {
        alignmentRegion = region;
        break;
      }
    }

    Assert.expect(alignmentRegion != null);
    return alignmentRegion;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public ReconstructionRegion get3DAlignmentRegion(AlignmentGroup alignmentGroup)
  {    
    ReconstructionRegion alignmentRegion = null;
    
    for (ReconstructionRegion region : _3DAlignmentRegions)
    {
      if (region.getAlignmentGroup() == alignmentGroup)
      {
        alignmentRegion = region;
        break;
      }
    }

    Assert.expect(alignmentRegion != null);
    return alignmentRegion;
  }

  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public ReconstructionRegion get2DAlignmentRegion(AlignmentGroup alignmentGroup)
  {
    ReconstructionRegion alignment2DRegion = null;

    for (ReconstructionRegion region : _2DAlignmentRegions)
    {
      if (region.getAlignmentGroup() == alignmentGroup)
      {
        alignment2DRegion = region;
        break;
      }
    }

    Assert.expect(alignment2DRegion != null);
    return alignment2DRegion;
  }
  
  /**
   * @author George A. David
   */
  public boolean hasAlignmentGroup(AlignmentGroup alignmentGroup)
  {
    if (_3DAlignmentRegions != null)
    {
      for (AlignmentGroup group : getAlignmentGroups())
      {
        if (alignmentGroup == group)
        {
          return true;
        }
      }
    }

    return false;
  }

  /**
   * Gets the transform which maps from PanelCoordinate space to SystemFiducialCoordinate space.
   * This varies based on which PIP is involved as well as the panel dimensions.
   *
   * This transform is used to help assemble the aggregate alignment transform.
   *
   * @author Matt Wharton
   */
  public AffineTransform getPanelToSystemFiducialCoordinateTransform()
  {
    Assert.expect(_testProgram != null);
    Assert.expect(_panelLocationInSystemEnum != null);

    AffineTransform panelToSystemFiducialCoordinateTransform = new AffineTransform();

    Project project = _testProgram.getProject();
    Panel panel = project.getPanel();
//    if(panel.getPanelSettings().isPanelBasedAlignment())
//    {
    int panelLengthInNanoMeters = panel.getLengthAfterAllRotationsInNanoMeters();
    int panelWidthInNanoMeters = panel.getWidthAfterAllRotationsInNanoMeters();

    // Determine which PIP sensor we're dealing with.
    if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
    {
      double rotation = Math.toRadians(-90);
      double xTranslation = 0;
      double yTranslation = panelWidthInNanoMeters;
      // NOTE: In the logic below, it's important that we apply the translation BEFORE the rotation for
      // the math to work out. -mdw
      panelToSystemFiducialCoordinateTransform.translate(xTranslation, yTranslation);
      panelToSystemFiducialCoordinateTransform.rotate(rotation);
    }
    else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
    {
      double rotation = Math.toRadians(-90);
      double xTranslation = -panelLengthInNanoMeters;
      double yTranslation = panelWidthInNanoMeters;
      // NOTE: In the logic below, it's important that we apply the translation BEFORE the rotation for
      // the math to work out. -mdw
      panelToSystemFiducialCoordinateTransform.translate(xTranslation, yTranslation);
      panelToSystemFiducialCoordinateTransform.rotate(rotation);
    }
    else
    {
      // Shouldn't ever get here.
      Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
    }
//    }
//    else
//    {
//      return getBoardToSystemFiducialCoordinateTransform();
//    }

    return panelToSystemFiducialCoordinateTransform;
  }

  /**
   * Gets the transform which maps from BoardCoordinate space to SystemFiducialCoordinate space.
   * This varies based on which PIP is involved as well as the panel dimensions.
   *
   * This transform is used to help assemble the aggregate alignment transform.
   *
   * @author Chong, Wei Chin
   */
  public AffineTransform getBoardToSystemFiducialCoordinateTransform()
  {
    Assert.expect(_testProgram != null);
    Assert.expect(_panelLocationInSystemEnum != null);

    AffineTransform boardToSystemFiducialCoordinateTransform = new AffineTransform();

    int boardLengthInNanoMeters = _board.getLengthAfterAllRotationsInNanoMeters();
    int boardWidthInNanoMeters = _board.getWidthAfterAllRotationsInNanoMeters();

    // Determine which PIP sensor we're dealing with.
    if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
    {
      double rotation = Math.toRadians(-90);
      double xTranslation = 0;
      double yTranslation = boardWidthInNanoMeters;
      // NOTE: In the logic below, it's important that we apply the translation BEFORE the rotation for
      // the math to work out. -mdw
      boardToSystemFiducialCoordinateTransform.translate(xTranslation, yTranslation);
      boardToSystemFiducialCoordinateTransform.rotate(rotation);
    }
    else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
    {
      double rotation = Math.toRadians(-90);
      double xTranslation = -boardLengthInNanoMeters;
      double yTranslation = boardWidthInNanoMeters;
      // NOTE: In the logic below, it's important that we apply the translation BEFORE the rotation for
      // the math to work out. -mdw
      boardToSystemFiducialCoordinateTransform.translate(xTranslation, yTranslation);
      boardToSystemFiducialCoordinateTransform.rotate(rotation);
    }
    else
    {
      // Shouldn't ever get here.
      Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
    }

    return boardToSystemFiducialCoordinateTransform;
  }

  /**
   * Gets the transform which compensates for the offsets from the applicable PIP sensor to the
   * system fiducial.  This is all in SystemFiducialCoordinates.
   *
   * This transform is used to help assemble the aggregate alignment transform.
   *
   * @author Matt Wharton
   */
  public AffineTransform getMachineSpecificTransform()
  {
    Assert.expect(_panelLocationInSystemEnum != null);

    AffineTransform machineSpecificTransform = new AffineTransform();
    if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
    {
      LeftPanelInPlaceSensor leftPipSensor = LeftPanelInPlaceSensor.getInstance();
      SystemFiducialCoordinate pipSensorOffsetFromSystemFiducial = new SystemFiducialCoordinate(
              leftPipSensor.getXDistanceFromSystemFiducial(), leftPipSensor.getYDistanceFromSystemFiducial());
      int xTranslation = pipSensorOffsetFromSystemFiducial.getX();
      int yTranslation = pipSensorOffsetFromSystemFiducial.getY();

      machineSpecificTransform.translate(xTranslation, yTranslation);
    }
    else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
    {
      RightPanelInPlaceSensor rightPipSensor = RightPanelInPlaceSensor.getInstance();
      SystemFiducialCoordinate pipSensorOffsetFromSystemFiducial = new SystemFiducialCoordinate(
              rightPipSensor.getXDistanceFromSystemFiducial(), rightPipSensor.getYDistanceFromSystemFiducial());
      int xTranslation = pipSensorOffsetFromSystemFiducial.getX();
      int yTranslation = pipSensorOffsetFromSystemFiducial.getY();

      machineSpecificTransform.translate(xTranslation, yTranslation);
    }
    else
    {
      // Shouldn't ever get here.
      Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
    }

    return machineSpecificTransform;
  }

  /**
   * Gets the aggreagate manual alignment transform that applies to this TestSubProgram.
   * This is the product of three seperate transforms:
   *
   * 1.  The "core" manual alignment transform which simply maps ideal PanelCoordinate
   *     locations to their actual locations.
   * 2.  The board specific transform which maps PanelCoordinates to SystemFiducialCoordinates.
   * 3.  The machine specific transform which offsets SystemFiducialCoordanates based on the PIP
   *     sensor offset from the system fiducial.
   *
   * @author George A. David
   * @author Peter Esbensen
   * @author Matt Wharton
   * @author Wei Chin
   */
  public AffineTransform getAggregateManualAlignmentTransform()
  {
    Assert.expect(_testProgram != null);
    Assert.expect(_panelLocationInSystemEnum != null);

    // Get the "core" alignment transform.
    AffineTransform coreManualAlignmentTransform = getCoreManualAlignmentTransform();

    // Build up the aggregate transform.
    AffineTransform aggregateTransform = new AffineTransform();
    Assert.expect(aggregateTransform != null);

    aggregateTransform.preConcatenate(coreManualAlignmentTransform);
    AffineTransform panelOrBoardToSystemFiducialCoordinateTransform = null;
    panelOrBoardToSystemFiducialCoordinateTransform = getPanelToSystemFiducialCoordinateTransform();
    Assert.expect(panelOrBoardToSystemFiducialCoordinateTransform != null);
    aggregateTransform.preConcatenate(panelOrBoardToSystemFiducialCoordinateTransform);
    AffineTransform machineSpecificTransform = getMachineSpecificTransform();
    Assert.expect(machineSpecificTransform != null);

    aggregateTransform.preConcatenate(machineSpecificTransform);

    Assert.expect(aggregateTransform != null);
    return aggregateTransform;
  }

  /**
   * Gets the aggreagate runtime alignment transform that applies to this TestSubProgram.
   * This is the product of three seperate transforms:
   *
   * 1.  The "core" runtime alignment transform which simply maps ideal PanelCoordinate
   *     locations to their actual locations.
   * 2.  The board specific transform which maps PanelCoordinates to SystemFiducialCoordinates.
   * 3.  The machine specific transform which offsets SystemFiducialCoordanates based on the PIP
   *     sensor offset from the system fiducial.
   *
   * @author George A. David
   * @author Peter Esbensen
   * @author Matt Wharton
   * @author Wei Chin
   */
  public AffineTransform getAggregateRuntimeAlignmentTransform()
  {
    Assert.expect(_testProgram != null);
    Assert.expect(_panelLocationInSystemEnum != null);

    // Get the "core" alignment transform.
    AffineTransform coreRuntimeAlignmentTransform = getCoreRuntimeAlignmentTransform();

    // Build up the aggregate transform.
    AffineTransform aggregateTransform = new AffineTransform();
    aggregateTransform.preConcatenate(coreRuntimeAlignmentTransform);
    
    AffineTransform panelOrBoardToSystemFiducialCoordinateTransform = null;
    panelOrBoardToSystemFiducialCoordinateTransform = getPanelToSystemFiducialCoordinateTransform();
    aggregateTransform.preConcatenate(panelOrBoardToSystemFiducialCoordinateTransform);
    AffineTransform machineSpecificTransform = getMachineSpecificTransform();
    aggregateTransform.preConcatenate(machineSpecificTransform);

    return aggregateTransform;
  }
  
  /**
   * Gets the "default" aggreagate alignment transform that applies to this TestSubProgram.
   * This is the product of two seperate transforms:
   *
   * 1.  The board specific transform which maps PanelCoordinates to SystemFiducialCoordinates.
   * 2.  The machine specific transform which offsets SystemFiducialCoordanates based on the PIP
   *     sensor offset from the system fiducial.
   *
   * @author Matt Wharton
   */
  public AffineTransform getDefaultAggregateAlignmentTransform()
  {
    // Build up the default aggregate transform.
    AffineTransform defaultAggregateTransform = getDefaultCoreAlignmentTransform();
    AffineTransform panelOrBoardToSystemFiducialCoordinateTransform = null;
    
    panelOrBoardToSystemFiducialCoordinateTransform = getPanelToSystemFiducialCoordinateTransform();
    defaultAggregateTransform.preConcatenate(panelOrBoardToSystemFiducialCoordinateTransform);

    AffineTransform machineSpecificTransform = getMachineSpecificTransform();
    defaultAggregateTransform.preConcatenate(machineSpecificTransform);

    return defaultAggregateTransform;
  }

  /**
   * Gets the "core" manual alignment transform from panel settings.  If one does not exist, the identity
   * transform is returned in its stead.
   *
   * @author Matt Wharton
   */
  public AffineTransform getCoreManualAlignmentTransform()
  {
    Assert.expect(_testProgram != null);
    Assert.expect(_panelLocationInSystemEnum != null);

    Project project = _testProgram.getProject();
    Panel panel = project.getPanel();
    PanelSettings panelSettings = panel.getPanelSettings();

    AffineTransform coreManualAlignmentTransform = null;

    if(isLowMagnification())
    {
      if (panelSettings.isPanelBasedAlignment())
      {
        if (hasCoreManualAlignmentTransform())
        {
          if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
          {
              coreManualAlignmentTransform = panelSettings.getLeftManualAlignmentTransform();
              
              //Siew Yeng - XCR1757 - use runtime manual alignment transform
              if(_isUsingRuntimeManualAlignment && panelSettings.getLeftRuntimeManualAlignmentTransform() != null)
              {
                coreManualAlignmentTransform = panelSettings.getLeftRuntimeManualAlignmentTransform();
              }
          }
          else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
          {
              coreManualAlignmentTransform = panelSettings.getRightManualAlignmentTransform();
              
              //Siew Yeng - XCR1757 - use runtime manual alignment transform
              if(_isUsingRuntimeManualAlignment && panelSettings.getRightRuntimeManualAlignmentTransform() != null)
              {
                coreManualAlignmentTransform = panelSettings.getRightRuntimeManualAlignmentTransform();
              }
          }
          else
          {
            // Shouldn't get here.
            Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
          }
        }
        else
        {
          coreManualAlignmentTransform = new AffineTransform();
        }
      }
      else
      {
        if (hasCoreManualAlignmentTransformByBoardSettings())
        {
          if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
          {
            coreManualAlignmentTransform = _board.getBoardSettings().getLeftManualAlignmentTransform();
            
            //Siew Yeng - XCR1757 - use runtime manual alignment transform
            if(_isUsingRuntimeManualAlignment && _board.getBoardSettings().getLeftRuntimeManualAlignmentTransform() != null)
            {
              coreManualAlignmentTransform = _board.getBoardSettings().getLeftRuntimeManualAlignmentTransform();
            }
          }
          else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
          {
            coreManualAlignmentTransform = _board.getBoardSettings().getRightManualAlignmentTransform();
            
            //Siew Yeng - XCR1757 - use runtime manual alignment transform
            if(_isUsingRuntimeManualAlignment && _board.getBoardSettings().getRightRuntimeManualAlignmentTransform() != null)
            {
              coreManualAlignmentTransform = _board.getBoardSettings().getRightRuntimeManualAlignmentTransform();
            }
          }
          else
          {
            // Shouldn't get here.
            Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
          }
        }
        else
        {
          coreManualAlignmentTransform = new AffineTransform();
        }
      }
    }
    else
    {
      if (panelSettings.isPanelBasedAlignment())
      {
        if (hasCoreManualAlignmentTransformForHighMag())
        {
          if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
          {
              coreManualAlignmentTransform = panelSettings.getLeftManualAlignmentTransformForHighMag();
              
              //Siew Yeng - XCR1757 - use runtime manual alignment transform
              if(_isUsingRuntimeManualAlignment && panelSettings.getLeftRuntimeManualAlignmentTransformForHighMag() != null)
              {
                coreManualAlignmentTransform = panelSettings.getLeftRuntimeManualAlignmentTransformForHighMag();
              }
          }
          else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
          {
              coreManualAlignmentTransform = panelSettings.getRightManualAlignmentTransformForHighMag();
              
              //Siew Yeng - XCR1757 - use runtime manual alignment transform
              if(_isUsingRuntimeManualAlignment && panelSettings.getRightRuntimeManualAlignmentTransformForHighMag() != null)
              {
                coreManualAlignmentTransform = panelSettings.getRightRuntimeManualAlignmentTransformForHighMag();
              }
          }
          else
          {
            // Shouldn't get here.
            Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
          }
        }
        else
        {
          coreManualAlignmentTransform = new AffineTransform();
        }
      }
      else
      {
        if (hasCoreManualAlignmentTransformByBoardSettingsForHighMag())
        {
          if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
          {
            coreManualAlignmentTransform = _board.getBoardSettings().getLeftManualAlignmentTransformForHighMag();
            
            //Siew Yeng - XCR1757 - use runtime manual alignment transform
            if(_isUsingRuntimeManualAlignment && _board.getBoardSettings().getLeftRuntimeManualAlignmentTransformForHighMag() != null)
            {
              coreManualAlignmentTransform = _board.getBoardSettings().getLeftRuntimeManualAlignmentTransformForHighMag();
            }
          }
          else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
          {
            coreManualAlignmentTransform = _board.getBoardSettings().getRightManualAlignmentTransformForHighMag();
            
            //Siew Yeng - XCR1757 - use runtime manual alignment transform
            if(_isUsingRuntimeManualAlignment && _board.getBoardSettings().getRightRuntimeManualAlignmentTransformForHighMag() != null)
            {
              coreManualAlignmentTransform = _board.getBoardSettings().getRightRuntimeManualAlignmentTransformForHighMag();
            }
          }
          else
          {
            // Shouldn't get here.
            Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
          }
        }
        else
        {
          coreManualAlignmentTransform = new AffineTransform();
        }
      }    
    }
    return coreManualAlignmentTransform;
  }
  
  /*
  * @author Kee Chin Seong - get last aligntment verification transform
  */
  public AffineTransform getLastAlignedWithVerificationAlignmentTransform()
  {
    Assert.expect(_testProgram != null);
    Assert.expect(_panelLocationInSystemEnum != null);

    Project project = _testProgram.getProject();
    Panel panel = project.getPanel();
    PanelSettings panelSettings = panel.getPanelSettings();

    AffineTransform coreManualAlignmentTransform = null;

    if(isLowMagnification())
    {
      if (panelSettings.isPanelBasedAlignment())
      {
        if (hasCoreManualAlignmentTransform())
        {
          if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
          {
              coreManualAlignmentTransform = panelSettings.getLeftLastSavedViewCadRuntimeAlignmentTransform();
              
              //Siew Yeng - XCR1757 - use runtime manual alignment transform
              if(_isUsingRuntimeManualAlignment && panelSettings.getLeftRuntimeManualAlignmentTransform() != null)
              {
                coreManualAlignmentTransform = panelSettings.getLeftRuntimeManualAlignmentTransform();
              }
          }
          else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
          {
              coreManualAlignmentTransform = panelSettings.getRightLastSavedViewCadRuntimeAlignmentTransform();
              
              //Siew Yeng - XCR1757 - use runtime manual alignment transform
              if(_isUsingRuntimeManualAlignment && panelSettings.getRightRuntimeManualAlignmentTransform() != null)
              {
                coreManualAlignmentTransform = panelSettings.getRightRuntimeManualAlignmentTransform();
              }
          }
          else
          {
            // Shouldn't get here.
            Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
          }
        }
        else
        {
          coreManualAlignmentTransform = new AffineTransform();
        }
      }
      else
      {
        if (hasCoreManualAlignmentTransformByBoardSettings())
        {
          if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
          {
            coreManualAlignmentTransform = _board.getBoardSettings().getLeftLastSavedViewCadRuntimeAlignmentTransform();
            
            //Siew Yeng - XCR1757 - use runtime manual alignment transform
            if(_isUsingRuntimeManualAlignment && _board.getBoardSettings().getLeftRuntimeManualAlignmentTransform() != null)
            {
              coreManualAlignmentTransform = _board.getBoardSettings().getLeftRuntimeManualAlignmentTransform();
            }
          }
          else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
          {
            coreManualAlignmentTransform = _board.getBoardSettings().getRightLastSavedViewCadRuntimeAlignmentTransform();
            
            //Siew Yeng - XCR1757 - use runtime manual alignment transform
            if(_isUsingRuntimeManualAlignment && _board.getBoardSettings().getRightRuntimeManualAlignmentTransform() != null)
            {
              coreManualAlignmentTransform = _board.getBoardSettings().getRightRuntimeManualAlignmentTransform();
            }
          }
          else
          {
            // Shouldn't get here.
            Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
          }
        }
        else
        {
          coreManualAlignmentTransform = new AffineTransform();
        }
      }
    }
    else
    {
      if (panelSettings.isPanelBasedAlignment())
      {
        if (hasCoreManualAlignmentTransformForHighMag())
        {
          if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
          {
              coreManualAlignmentTransform = panelSettings.getLeftLastSavedRuntimeAlignmentTransformForHighMag();
              
              //Siew Yeng - XCR1757 - use runtime manual alignment transform
              if(_isUsingRuntimeManualAlignment && panelSettings.getLeftRuntimeManualAlignmentTransformForHighMag() != null)
              {
                coreManualAlignmentTransform = panelSettings.getLeftRuntimeManualAlignmentTransformForHighMag();
              }
          }
          else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
          {
              coreManualAlignmentTransform = panelSettings.getRightLastSavedViewCadRuntimeAlignmentTransformForHighMag();
              
              //Siew Yeng - XCR1757 - use runtime manual alignment transform
              if(_isUsingRuntimeManualAlignment && panelSettings.getRightRuntimeManualAlignmentTransformForHighMag() != null)
              {
                coreManualAlignmentTransform = panelSettings.getRightRuntimeManualAlignmentTransformForHighMag();
              }
          }
          else
          {
            // Shouldn't get here.
            Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
          }
        }
        else
        {
          coreManualAlignmentTransform = new AffineTransform();
        }
      }
      else
      {
        if (hasCoreManualAlignmentTransformByBoardSettingsForHighMag())
        {
          if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
          {
            coreManualAlignmentTransform = _board.getBoardSettings().getLeftLastSavedViewCadRuntimeAlignmentTransformForHighMag();
            
            //Siew Yeng - XCR1757 - use runtime manual alignment transform
            if(_isUsingRuntimeManualAlignment && _board.getBoardSettings().getLeftRuntimeManualAlignmentTransformForHighMag() != null)
            {
              coreManualAlignmentTransform = _board.getBoardSettings().getLeftRuntimeManualAlignmentTransformForHighMag();
            }
          }
          else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
          {
            coreManualAlignmentTransform = _board.getBoardSettings().getRightLastSavedViewCadRuntimeAlignmentTransformForHighMag();
            
            //Siew Yeng - XCR1757 - use runtime manual alignment transform
            if(_isUsingRuntimeManualAlignment && _board.getBoardSettings().getRightRuntimeManualAlignmentTransformForHighMag() != null)
            {
              coreManualAlignmentTransform = _board.getBoardSettings().getRightRuntimeManualAlignmentTransformForHighMag();
            }
          }
          else
          {
            // Shouldn't get here.
            Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
          }
        }
        else
        {
          coreManualAlignmentTransform = new AffineTransform();
        }
      }    
    }
    return coreManualAlignmentTransform;
  }

  /**
   * Looks up in the PanelSettings and checks to see if a relevant manual alignment transform exists for
   * this TestSubProgram.
   *
   * @author Matt Wharton
   */
  public boolean hasCoreManualAlignmentTransform()
  {
    Assert.expect(_testProgram != null);
    Assert.expect(_panelLocationInSystemEnum != null);

    Project project = _testProgram.getProject();
    Panel panel = project.getPanel();
    PanelSettings panelSettings = panel.getPanelSettings();

    boolean alignmentTransformExists = false;
    if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
    {
      alignmentTransformExists = panelSettings.leftManualAlignmentTransformExists();
    }
    else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
    {
      alignmentTransformExists = panelSettings.rightManualAlignmentTransformExists();
    }
    else
    {
      // Shouldn't get here.
      Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
    }

    return alignmentTransformExists;
  }

  /**
   * Looks up in the BoardSettings and checks to see if a relevant manual alignment transform exists for
   * this TestSubProgram.
   *
   * @author Wei Chin, Chong
   */
  public boolean hasCoreManualAlignmentTransformByBoardSettings()
  {
    Assert.expect(_board != null);
    Assert.expect(_panelLocationInSystemEnum != null);

    boolean alignmentTransformExists = false;
    if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
    {
      alignmentTransformExists = _board.getBoardSettings().leftManualAlignmentTransformExists();
    }
    else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
    {
      alignmentTransformExists = _board.getBoardSettings().rightManualAlignmentTransformExists();
    }
    else
    {
      // Shouldn't get here.
      Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
    }

    return alignmentTransformExists;
  }

  /**
   * Looks up in the PanelSettings and checks to see if a relevant manual alignment transform exists for
   * this TestSubProgram.
   *
   * @author Matt Wharton
   */
  public boolean hasCoreManualAlignmentTransformForHighMag()
  {
    Assert.expect(_testProgram != null);
    Assert.expect(_panelLocationInSystemEnum != null);

    Project project = _testProgram.getProject();
    Panel panel = project.getPanel();
    PanelSettings panelSettings = panel.getPanelSettings();

    boolean alignmentTransformExists = false;
    if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
    {
      alignmentTransformExists = panelSettings.leftManualAlignmentTransformForHighMagExists();      
    }
    else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
    {
      alignmentTransformExists = panelSettings.rightManualAlignmentTransformForHighMagExists();      
    }
    else
    {
      // Shouldn't get here.
      Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
    }

    return alignmentTransformExists;
  }

  /**
   * Looks up in the BoardSettings and checks to see if a relevant manual alignment transform exists for
   * this TestSubProgram.
   *
   * @author Wei Chin, Chong
   */
  public boolean hasCoreManualAlignmentTransformByBoardSettingsForHighMag()
  {
    Assert.expect(_board != null);
    Assert.expect(_panelLocationInSystemEnum != null);

    boolean alignmentTransformExists = false;
    if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
    {
      alignmentTransformExists = _board.getBoardSettings().leftManualAlignmentTransformForHighMagExists();     
    }
    else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
    {
      alignmentTransformExists = _board.getBoardSettings().rightManualAlignmentTransformForHighMagExists();
    }
    else
    {
      // Shouldn't get here.
      Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
    }

    return alignmentTransformExists;
  }

  /**
   * Gets the "core" runtime alignment transform from panel settings.  If one does not exist, the identity
   * transform is returned in its stead.
   *
   * @author Matt Wharton
   */
  public AffineTransform getCoreRuntimeAlignmentTransform()
  {
    Assert.expect(_testProgram != null);
    Assert.expect(_panelLocationInSystemEnum != null);

    Project project = _testProgram.getProject();
    Panel panel = project.getPanel();
    PanelSettings panelSettings = panel.getPanelSettings();

    AffineTransform coreRuntimeAlignmentTransform = null;
    if(isLowMagnification())
    {
      if (panelSettings.isPanelBasedAlignment())
      {
        if (hasCoreRuntimeAlignmentTransform())
        {
          if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
          {
            coreRuntimeAlignmentTransform = panelSettings.getLeftRuntimeAlignmentTransform();
            
            //Siew Yeng
            if(_isUsingRuntimeManualAlignment && panelSettings.getLeftRuntimeManualAlignmentTransform() != null)
            {
              coreRuntimeAlignmentTransform = panelSettings.getLeftRuntimeManualAlignmentTransform();
            }
          }
          else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
          {
            coreRuntimeAlignmentTransform = panelSettings.getRightRuntimeAlignmentTransform();
            
            //Siew Yeng
            if(_isUsingRuntimeManualAlignment && panelSettings.getRightRuntimeManualAlignmentTransform() != null)
            {
              coreRuntimeAlignmentTransform = panelSettings.getRightRuntimeManualAlignmentTransform();
            }
          }
          else
          {
            // Shouldn't get here.
            Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
          }
        }
        else
        {
          coreRuntimeAlignmentTransform = new AffineTransform();
        }
      }
      else
      {
        if (hasCoreRuntimeAlignmentTransformFromBoardSettings())
        {
          if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
          {
            coreRuntimeAlignmentTransform = _board.getBoardSettings().getLeftRuntimeAlignmentTransform();
            
            //Siew Yeng
            if(_isUsingRuntimeManualAlignment && _board.getBoardSettings().getLeftRuntimeManualAlignmentTransform() != null)
            {
              coreRuntimeAlignmentTransform = _board.getBoardSettings().getLeftRuntimeManualAlignmentTransform();
            }
          }
          else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
          {
            coreRuntimeAlignmentTransform = _board.getBoardSettings().getRightRuntimeAlignmentTransform();
            
            //Siew Yeng
            if(_isUsingRuntimeManualAlignment && _board.getBoardSettings().getRightRuntimeManualAlignmentTransform() != null)
            {
              coreRuntimeAlignmentTransform = _board.getBoardSettings().getRightRuntimeManualAlignmentTransform();
            }
          }
          else
          {
            // Shouldn't get here.
            Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
          }
        }
        else
        {
          coreRuntimeAlignmentTransform = new AffineTransform();
        }
      }
    }
    else
    {
      if (panelSettings.isPanelBasedAlignment())
      {
        if (hasCoreRuntimeAlignmentTransformForHighMag())
        {
          if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
          {
            coreRuntimeAlignmentTransform = panelSettings.getLeftRuntimeAlignmentTransformForHighMag();
            
            //Siew Yeng
            if(_isUsingRuntimeManualAlignment && panelSettings.getLeftRuntimeManualAlignmentTransformForHighMag() != null)
            {
              coreRuntimeAlignmentTransform = panelSettings.getLeftRuntimeManualAlignmentTransformForHighMag();
            }
          }
          else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
          {
            coreRuntimeAlignmentTransform = panelSettings.getRightRuntimeAlignmentTransformForHighMag();
            
            //Siew Yeng
            if(_isUsingRuntimeManualAlignment && panelSettings.getRightRuntimeManualAlignmentTransformForHighMag() != null)
            {
              coreRuntimeAlignmentTransform = panelSettings.getRightRuntimeManualAlignmentTransformForHighMag();
            }
          }
          else
          {
            // Shouldn't get here.
            Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
          }
        }
        else
        {
          coreRuntimeAlignmentTransform = new AffineTransform();
        }
      }
      else
      {
        if (hasCoreRuntimeAlignmentTransformFromBoardSettingsForHighMag())
        {
          if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
          {
            coreRuntimeAlignmentTransform = _board.getBoardSettings().getLeftRuntimeAlignmentTransformForHighMag();
            
            //Siew Yeng
            if(_isUsingRuntimeManualAlignment && _board.getBoardSettings().getLeftRuntimeManualAlignmentTransformForHighMag() != null)
            {
              coreRuntimeAlignmentTransform = _board.getBoardSettings().getLeftRuntimeManualAlignmentTransformForHighMag();
            }
          }
          else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
          {
            coreRuntimeAlignmentTransform = _board.getBoardSettings().getRightRuntimeAlignmentTransformForHighMag();
            
            //Siew Yeng
            if(_isUsingRuntimeManualAlignment && _board.getBoardSettings().getRightRuntimeManualAlignmentTransformForHighMag() != null)
            {
              coreRuntimeAlignmentTransform = _board.getBoardSettings().getRightRuntimeManualAlignmentTransformForHighMag();
            }
          }
          else
          {
            // Shouldn't get here.
            Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
          }
        }
        else
        {
          coreRuntimeAlignmentTransform = new AffineTransform();
        }
      }
    }

    return coreRuntimeAlignmentTransform;
  }

  /**
   * Looks up in the PanelSettings and checks to see if a relevant runtime alignment transform exists for
   * this TestSubProgram.
   *
   * @author Matt Wharton
   */
  public boolean hasCoreRuntimeAlignmentTransform()
  {
    Assert.expect(_testProgram != null);
    Assert.expect(_panelLocationInSystemEnum != null);

    Project project = _testProgram.getProject();
    Panel panel = project.getPanel();
    PanelSettings panelSettings = panel.getPanelSettings();

    boolean alignmentTransformExists = false;
    if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
    {
      alignmentTransformExists = panelSettings.leftRuntimeAlignmentTransformExists();
    }
    else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
    {
      alignmentTransformExists = panelSettings.rightRuntimeAlignmentTransformExists();
    }
    else
    {
      // Shouldn't get here.
      Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
    }

    return alignmentTransformExists;
  }

  /**
   * Looks up in the BoardSettings and checks to see if a relevant runtime alignment transform exists for
   * this TestSubProgram.
   *
   * @author Wei Chin
   */
  public boolean hasCoreRuntimeAlignmentTransformFromBoardSettings()
  {
    Assert.expect(_board != null);
    Assert.expect(_panelLocationInSystemEnum != null);

    boolean alignmentTransformExists = false;
    if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
    {
      alignmentTransformExists = _board.getBoardSettings().leftRuntimeAlignmentTransformExists();
    }
    else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
    {
      alignmentTransformExists = _board.getBoardSettings().rightRuntimeAlignmentTransformExists();
    }
    else
    {
      // Shouldn't get here.
      Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
    }

    return alignmentTransformExists;
  }

  /**
   * Looks up in the PanelSettings and checks to see if a relevant runtime alignment transform exists for
   * this TestSubProgram.
   *
   * @author Wei Chin
   */
  public boolean hasCoreRuntimeAlignmentTransformForHighMag()
  {
    Assert.expect(_testProgram != null);
    Assert.expect(_panelLocationInSystemEnum != null);

    Project project = _testProgram.getProject();
    Panel panel = project.getPanel();
    PanelSettings panelSettings = panel.getPanelSettings();

    boolean alignmentTransformExists = false;
    if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
    {
      alignmentTransformExists = panelSettings.leftRuntimeAlignmentTransformForHighMagExists();      
    }
    else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
    {
      alignmentTransformExists = panelSettings.rightRuntimeAlignmentTransformForHighMagExists();      
    }
    else
    {
      // Shouldn't get here.
      Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
    }

    return alignmentTransformExists;
  }

  /**
   * Looks up in the BoardSettings and checks to see if a relevant runtime alignment transform exists for
   * this TestSubProgram.
   *
   * @author Wei Chin
   */
  public boolean hasCoreRuntimeAlignmentTransformFromBoardSettingsForHighMag()
  {
    Assert.expect(_board != null);
    Assert.expect(_panelLocationInSystemEnum != null);

    boolean alignmentTransformExists = false;
    if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
    {
      alignmentTransformExists = _board.getBoardSettings().leftRuntimeAlignmentTransformForHighMagExists();
    }
    else if (_panelLocationInSystemEnum.equals(PanelLocationInSystemEnum.RIGHT))
    {
      alignmentTransformExists = _board.getBoardSettings().rightRuntimeAlignmentTransformForHighMagExists();
    }
    else
    {
      // Shouldn't get here.
      Assert.expect(false, "Unexpected PanelLocationInSystemEnum value");
    }

    return alignmentTransformExists;
  }
  
  /**
   * Gets the "default" "core" alignment transform.  This is literally just the identity transform.
   *
   * @author Matt Wharton
   */
  public AffineTransform getDefaultCoreAlignmentTransform()
  {
    return new AffineTransform();
  }

  /**
   * @author George A. David
   */
  public PanelLocationInSystemEnum getPanelLocationInSystem()
  {
    Assert.expect(_panelLocationInSystemEnum != null);

    return _panelLocationInSystemEnum;
  }

  /**
   * @author George A. David
   */
  public void setPanelLocationInSystem(PanelLocationInSystemEnum panelLocationInSystem)
  {
    Assert.expect(panelLocationInSystem != null);

    _panelLocationInSystemEnum = panelLocationInSystem;
  }

  /**
   * @author Roy Williams
   */
  public void addScanPathOverInspectionOrVerificationArea(ScanPath scanPath)
  {
    Assert.expect(scanPath != null);

    if (_scanPathOverInspectionOrVerificationArea == null)
    {
      _scanPathOverInspectionOrVerificationArea = new ArrayList<ScanPath>();
    }

    _scanPathOverInspectionOrVerificationArea.add(scanPath);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void addOpticalPointToPointScan(OpticalPointToPointScan opticalPointToPointScan)
  {
      Assert.expect(opticalPointToPointScan != null);
      
      if (_opticalPointToPointScanPath == null)
      {
          _opticalPointToPointScanPath = new ArrayList<OpticalPointToPointScan>();
      }
      
      _opticalPointToPointScanPath.add(opticalPointToPointScan);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void addAlignmentOpticalPointToPointScan(OpticalPointToPointScan opticalPointToPointScan)
  {
      Assert.expect(opticalPointToPointScan != null);
      
      if (_alignmentOpticalPointToPointScanPath == null)
      {
          _alignmentOpticalPointToPointScanPath = new ArrayList<OpticalPointToPointScan>();
      }
      
      _alignmentOpticalPointToPointScanPath.add(opticalPointToPointScan);
  }

  /**
   * @author Roy Williams
   */
  public void setAlignmentScanPaths(List<ScanPath> scanPaths)
  {
    Assert.expect(scanPaths != null);

    _alignmentScanPaths = scanPaths;
  }

  /**
   * @author Roy Williams
   */
  public void clearScanPaths()
  {
    // Added by Lee Herng 6 May 2012 - We want to explicitly
    // remove all the scan path information (including MechanicalConversions) data.
    if (_scanPathOverInspectionOrVerificationArea != null)
    {
      for(ScanPath scanPath : _scanPathOverInspectionOrVerificationArea)
        scanPath.clear();
      
      _scanPathOverInspectionOrVerificationArea.clear();
      _scanPathOverInspectionOrVerificationArea = null;
    }
     
    if (_alignmentScanPaths != null)
    {
      for(ScanPath scanPath : _alignmentScanPaths)
        scanPath.clear();
      
      _alignmentScanPaths.clear();
      _alignmentScanPaths = null;
    }

    // Since we are going to clear the scan paths we must also clear their
    // constituents (i.e. scanPass) off the ReconstructionRegions.
    for (ReconstructionRegion region : _3DAlignmentRegions)
    {
      region.clearScanPasses();
    }
    for (ReconstructionRegion region : _2DAlignmentRegions)
    {
      region.clearScanPasses();
    }
    for (ReconstructionRegion region : _verificationRegions)
    {
      region.clearScanPasses();
    }
    for (ReconstructionRegion region : _inspectionRegions)
    {
      region.clearScanPasses();
    }
  }

  /**
   * Composes a compilation of scanpasses required for both alignment and inspection.
   * There is a defined ordering for these scanpasses, alignment will be given first
   * and then passes required for inspection.
   *
   * @author Roy Williams
   */
  public List<ScanPass> getScanPasses()
  {
    // Create new empty list.
    List<ScanPass> scanPasses = new ArrayList<ScanPass>();

    // If there are any alignment scanpasses, add them first.
    if (_alignmentScanPaths != null)
    {
      for (ScanPath scanPath : _alignmentScanPaths)
      {
        scanPasses.addAll(scanPath.getScanPasses());
      }
    }
    if (_scanPathOverInspectionOrVerificationArea != null)
    {
      for (ScanPath scanPath : _scanPathOverInspectionOrVerificationArea)
      {
        scanPasses.addAll(scanPath.getScanPasses());
      }
    }
    return scanPasses;
  }

  /**
   * Composes a compilation of scanpasses required for both alignment and inspection.
   * There is a defined ordering for these scanpasses, alignment will be given first
   * and then passes required for inspection.
   *
   * @author Roy Williams
   */
  public List<ScanPath> getScanPaths()
  {
    // Create new empty list.
    List<ScanPath> scanPaths = new ArrayList<ScanPath>();

    // If there are any alignment scanpasses, add them first.
    if (_alignmentScanPaths != null)
    {
      scanPaths.addAll(_alignmentScanPaths);
    }
    // Now we can add on all the inspection scanPaths.
    if (_scanPathOverInspectionOrVerificationArea != null)
    {
      scanPaths.addAll(_scanPathOverInspectionOrVerificationArea);
    }

    return scanPaths;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public List<OpticalPointToPointScan> getOpticalPointToPointScans()
  {
      List<OpticalPointToPointScan> pointToPointScans = new ArrayList<OpticalPointToPointScan>();
      
      if (_opticalPointToPointScanPath != null)
      {
          for(OpticalPointToPointScan pointToPointScan : _opticalPointToPointScanPath)
          {
              pointToPointScans.add(pointToPointScan);
          }
      }
      return pointToPointScans;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public List<OpticalPointToPointScan> getAlignmentOpticalPointToPointScans()
  {
      List<OpticalPointToPointScan> pointToPointScans = new ArrayList<OpticalPointToPointScan>();
      
      if (_alignmentOpticalPointToPointScanPath != null)
      {
          for(OpticalPointToPointScan pointToPointScan : _alignmentOpticalPointToPointScanPath)
          {
              pointToPointScans.add(pointToPointScan);
          }
      }
      return pointToPointScans;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clearOpticalPointToPointScans()
  {
      if (_opticalPointToPointScanPath != null)
      {          
          _opticalPointToPointScanPath.clear();
      }
      
      _opticalPointToPointScanPath = null;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clearAlignmentOpticalPointToPointScans()
  {
      if (_alignmentOpticalPointToPointScanPath != null)
      {          
          _alignmentOpticalPointToPointScanPath.clear();
      }
      
      _alignmentOpticalPointToPointScanPath = null;
  }

  /**
   * @author George A. David
   */
  public void setInspectableAreaLengthInNanoMeters(int inspectableAreaLengthInNanoMeters)
  {
    Assert.expect(inspectableAreaLengthInNanoMeters > 0);

    _inspectableAreaLengthInNanoMeters = inspectableAreaLengthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public void setImageableRegionInNanoMeters(PanelRectangle imageableRegionInNanoMeters)
  {
    Assert.expect(imageableRegionInNanoMeters != null);

    _imageableRegionInNanoMeters = imageableRegionInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public PanelRectangle getImageableRegionInNanoMeters()
  {
    Assert.expect(_imageableRegionInNanoMeters != null);

    return _imageableRegionInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getInspectableAreaLengthInNanoMeters()
  {
    Assert.expect(_inspectableAreaLengthInNanoMeters > 0);

    return _inspectableAreaLengthInNanoMeters;
  }

  /**
   * @author Rex Shang
   */
  public void setProcessorStripsForInspectionRegions(List<ProcessorStrip> processorStrips)
  {
    Assert.expect(processorStrips != null);

    _processorStripsForInspectionRegions = processorStrips;
  }

  /**
   * @author Rex Shang
   */
  public List<ProcessorStrip> getProcessorStripsForInspectionRegions()
  {
    Assert.expect(_processorStripsForInspectionRegions != null);

    return _processorStripsForInspectionRegions;
  }

  /**
   * @author Rex Shang
   */
  public void setProcessorStripsForVerificationRegions(List<ProcessorStrip> processorStrips)
  {
    Assert.expect(processorStrips != null);

    _processorStripsForVerificationRegions = processorStrips;
  }

  /**
   * @author Rex Shang
   */
  public List<ProcessorStrip> getProcessorStripsForVerificationRegions()
  {
    Assert.expect(_processorStripsForVerificationRegions != null);

    return _processorStripsForVerificationRegions;
  }
  
  /**
   * @author Ngie Xing
   */
  public boolean hasProcessorStripsForVerificationRegions()
  {
    if (_processorStripsForVerificationRegions == null || _processorStripsForVerificationRegions.isEmpty())
    {
      return false;
    }

    return true;
  }

  /**
   * Creating a fake process strip map for a sub program.  We should look at ways
   * to elimiate this method down the road.
   * @author Rex Shang
   */
  public static List<ProcessorStrip> getFakeProcessorStrips()
  {
    List<ProcessorStrip> processorStrips = new ArrayList<ProcessorStrip>();
    ProcessorStrip processorStrip = new ProcessorStrip(0);
    // Assign the strip to the first IRE.
    processorStrip.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
    processorStrip.setRegion(new PanelRectangle(0, 0, 1, 1));
    processorStrips.add(processorStrip);

    return processorStrips;
  }

  /**
   * @author George A. David
   */
  public Collection<ReconstructionRegion> getSurfaceModelingReconstructionRegions()
  {
    List<ReconstructionRegion> regions = new LinkedList<ReconstructionRegion>();
    for (ReconstructionRegion region : _inspectionRegions)
    {
      if (region.hasSurfaceModelingSlice())
      {
        regions.add(region);
      }
    }

    return regions;
  }

  /**
   * @author George A. David
   */
  public boolean hasInspectionRegions()
  {
    return _inspectionRegions != null && _inspectionRegions.isEmpty() == false;
  }

  /**
   * @author George A. David
   */
  public int getNumberOfFilteredInspectionImages()
  {
    int numImages = 0;
    for (ReconstructionRegion region : getFilteredInspectionRegions())
    {
      for (FocusGroup group : region.getFocusGroups())
      {
        for (Slice slice : group.getSlices())
        {
          if ((slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION) || 
               slice.getSliceType().equals(SliceTypeEnum.PROJECTION)) && // Chnee Khang Wah, 2012-02-21, 2.5D
               slice.createSlice())
          {
            ++numImages;
          }
        }
      }
    }
    return numImages;
  }

  /**
   * @author George A. David
   */
  public int getNumberOfInspectedInspectionImages()
  {
    int numImages = 0;
    for (ReconstructionRegion region : getFilteredInspectionRegions())
    {
      numImages += region.getInspectedSlices().size();
    }

    return numImages;
  }

  /**
   * @author George A. David
   */
  public void addAdjustFocusInspectionRegion(ReconstructionRegion adjustFocusRegion)
  {
    Assert.expect(adjustFocusRegion != null);

    if (_adjustFocusInspectionRegions == null)
    {
      _adjustFocusInspectionRegions = new LinkedList<ReconstructionRegion>();
    }

    if (_nameToAdjustFocusRegionsMap == null)
    {
      _nameToAdjustFocusRegionsMap = new HashMap<String, List<ReconstructionRegion>>();
    }

    _adjustFocusInspectionRegions.add(adjustFocusRegion);
    if (_nameToAdjustFocusRegionsMap.containsKey(adjustFocusRegion.getName()))
    {
      _nameToAdjustFocusRegionsMap.get(adjustFocusRegion.getName()).add(adjustFocusRegion);
    }
    else
    {
      List<ReconstructionRegion> regions = new LinkedList<ReconstructionRegion>();
      regions.add(adjustFocusRegion);
      _nameToAdjustFocusRegionsMap.put(adjustFocusRegion.getName(), regions);
    }
  }

  /**
   * @author George A. David
   */
  public List<ReconstructionRegion> getAdjustFocusRegions(String regionName)
  {
    Assert.expect(_nameToAdjustFocusRegionsMap.containsKey(regionName));

    return _nameToAdjustFocusRegionsMap.get(regionName);
  }

  /**
   * @author George A. David
   */
  public void clearAdjustFocusInspectionRegions()
  {
    if (_adjustFocusInspectionRegions != null)
    {
      _adjustFocusInspectionRegions.clear();
    }
    if (_nameToAdjustFocusRegionsMap != null)
    {
      _nameToAdjustFocusRegionsMap.clear();
    }
  }

  /**
   * @author George A. David
   */
  public boolean hasInspectionRegion(String regionName)
  {
    Assert.expect(regionName != null);
    Assert.expect(_nameToInspectionRegionMap != null);

    return _nameToInspectionRegionMap.containsKey(regionName);
  }

  /**
   * @author George A. David
   */
  public ReconstructionRegion getInspectionRegion(String regionName)
  {
    Assert.expect(regionName != null);
    Assert.expect(_nameToInspectionRegionMap != null);

    ReconstructionRegion region = _nameToInspectionRegionMap.get(regionName);
    Assert.expect(region != null);

    return region;
  }
  
  /**
   * @author Cheah Lee Herng
   * Edited by Kee Chin Seong - To Speed up in XXL V810
   */
  public List<ReconstructionRegion> getInspectionRegions(Component component)
  {
      Assert.expect(component != null);
      
      return _componentToReconstructionRegionMap.get(component.getReferenceDesignator());  
  }
  
  /**
   * @author Kee Chin Seong - for Focus region speed up
   */
  public List<ReconstructionRegion> getInspectionRegions(String componentRefDes)
  {
      Assert.expect(componentRefDes != null);
      
      return _componentToReconstructionRegionMap.get(componentRefDes);  
  }
  
  /*
   * @author Kee Chin Seong - To Speed up in XXL V810
   */
  public List<ReconstructionRegion> getInspectionRegions(Subtype subtype)
  {
      Assert.expect(subtype != null);
    
      return _subtypeToReconstructionRegionMap.get(subtype);    
  }
  
  /*
   * @author Kee Chin Seong - To Speed up in XXL V810
   */
  public void removeInspectionRegionFromTestSubProgram(ReconstructionRegion region)
  {
    if(_inspectionRegions.contains(region))
      _inspectionRegions.remove(region);
  }
  
  /**
   * This function gets a list of reconstruction regions that contains the component and 
   * encapsulated within the list o boards specified. This is important particularly for 
   * panel-based alignment on multi-board, where it has same component ref des on different
   * board location.
   * 
   * @author Cheah Lee Herng
   */
  public List<ReconstructionRegion> getInspectionRegions(List<Board> boards, Component component)
  {
    Assert.expect(boards != null);
    Assert.expect(component != null);

    java.util.List<ReconstructionRegion> reconstructionRegions = new ArrayList<ReconstructionRegion>();
    for(ReconstructionRegion region : getFilteredInspectionRegions())
    {
      if (region.getComponent().getReferenceDesignator().equalsIgnoreCase(component.getReferenceDesignator()) &&
          boards.contains(region.getComponent().getBoard()))
      {
          if (reconstructionRegions.contains(region) == false)
          {
              reconstructionRegions.add(region);
              
              if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
              {
                String printOut = "";
                if (boards.size() > 0)
                {
                  printOut = "Total Board Size = " + boards.size() + " : ";
                  for(Board board : boards)
                  {
                    printOut += board.getName() + " , ";
                  }
                }

                System.out.println("TestSubProgram " + getId() + ": " + printOut + " : Component Board " + 
                        region.getComponent().getBoard().getName() + " : Component " + 
                        region.getComponent().getReferenceDesignator() + " : Region Id " + 
                        region.getRegionId());
              }
          }
      }
    }
    return reconstructionRegions;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public List<ReconstructionRegion> getAlignmentInspectionRegions(List<Board> boards, Component component)
  {
    Assert.expect(boards != null);
    Assert.expect(component != null);

    java.util.List<ReconstructionRegion> reconstructionRegions = new ArrayList<ReconstructionRegion>();
    for(ReconstructionRegion region : getAlignmentRegions())
    {
      if (region.getComponent().getReferenceDesignator().equalsIgnoreCase(component.getReferenceDesignator()) &&
          boards.contains(region.getComponent().getBoard()))
      {
        if (reconstructionRegions.contains(region) == false)
        {
          reconstructionRegions.add(region);

          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
          {
            String printOut = "";
            if (boards.size() > 0)
            {
              printOut = "Total Board Size = " + boards.size() + " : ";
              for(Board board : boards)
              {
                printOut += board.getName() + " , ";
              }
            }

            System.out.println("TestSubProgram: getAlignmentInspectionRegions: " + printOut + " : Component Board " + region.getComponent().getBoard().getName() + " : Component " + region.getComponent().getReferenceDesignator() + " : Region Id " + region.getRegionId());
          }
        }
      }
    }
    return reconstructionRegions;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public List<ReconstructionRegion> getInspectionRegions(OpticalRegion opticalRegion, Component component, Collection<ReconstructionRegion> filteredRegions)
  {
      Assert.expect(opticalRegion != null);
      Assert.expect(component != null);
      Assert.expect(filteredRegions != null);
      Assert.expect(_testProgram != null);
      
      java.util.List<ReconstructionRegion> reconstructionRegions = new ArrayList<ReconstructionRegion>();
      Panel panel = _testProgram.getProject().getPanel();
      
      if (panel.getPanelSettings().isPanelBasedAlignment())
      {
        List<Board> boards = panel.getBoards();
        for(Board board : boards)
        {
          for(ReconstructionRegion region : filteredRegions)
          {
              if (region.getComponent().equals(component) &&
                  opticalRegion.getRegion().intersects(getImageableRegionInNanoMeters()) &&
                  opticalRegion.getBoards().contains(board))
              {
                  if (reconstructionRegions.contains(region) == false)
                      reconstructionRegions.add(region);
              }
          }
        }
      }
      else
      {          
        for(ReconstructionRegion region : filteredRegions)
        {
            if (region.getComponent().equals(component) &&
                opticalRegion.getRegion().intersects(getImageableRegionInNanoMeters()))
            {
                if (reconstructionRegions.contains(region) == false)
                    reconstructionRegions.add(region);
            }
        }
      }
      return reconstructionRegions;
  }

  /**
   * @author George A. David
   */
  public Set<String> getInspectionRegionNames()
  {
    Assert.expect(_nameToInspectionRegionMap != null);

    return new HashSet<String>(_nameToInspectionRegionMap.keySet());
  }

  /**
   * @author George A. David
   */
  public void setNumVerificationRegionColumns(int numVerificationRegionColumns)
  {
    Assert.expect(numVerificationRegionColumns > 0);

    _numVerificationRegionColumns = numVerificationRegionColumns;
  }

  /**
   * @author George A. David
   */
  public void setNumVerificationRegionRows(int numVerificationRegionRows)
  {
    Assert.expect(numVerificationRegionRows > 0);

    _numVerificationRegionRows = numVerificationRegionRows;
  }

  /**
   * @author George A. David
   */
  public int getNumVerificationRegionColumns()
  {
    Assert.expect(_numVerificationRegionColumns > 0);

    return _numVerificationRegionColumns;
  }

  /**
   * @author George A. David
   */
  public int getNumVerificationRegionRows()
  {
    Assert.expect(_numVerificationRegionRows > 0);

    return _numVerificationRegionRows;
  }

  /**
   * @author George A. David
   */
  public boolean hasInspectedRegions()
  {
    for (ReconstructionRegion region : getFilteredInspectionRegions())
    {
      if (region.getComponent().isInspected())
      {
        return true;
      }
    }

    return false;
  }

  /**
   * This will return reconstruction region which is needed by scan path only.
   * Only Inspected region will be return
   *
   * @author Poh Kheng
   */
  public Collection<ReconstructionRegion> getAllScanPathReconstructionRegions()
  {
    List<ReconstructionRegion> inspectionReconstructionRegions = new LinkedList<ReconstructionRegion>();

    for (ReconstructionRegion reconstructionRegion : getAllInspectionRegions())
    {
      // CR1085 fix by LeeHerng - Make sure we add in reconstruction region which uses Auto Predictive Slice Height feature, on top of
      // existing filter excluding non-inspected reconstruction region.
      if (reconstructionRegion.isParentRegionUseAutoPredictiveSliceHeight() && (inspectionReconstructionRegions.contains(reconstructionRegion) == false))
      {
        if (Config.isDeveloperDebugModeOn())
        {
          System.out.println("Region " + reconstructionRegion.getRegionId() + " has Parent Region set to Auto Predictive Slice Height and is included into the inspection region list.");
        }
        inspectionReconstructionRegions.add(reconstructionRegion);
      }
      else if (_testProgram.filterScanPathReconstructedRegion(reconstructionRegion))
      {
        inspectionReconstructionRegions.add(reconstructionRegion);
      }
    }
    return inspectionReconstructionRegions;
  }

  /**
   * @author Wei Chin, Chong
   */
  public Set<ReconstructionRegion> getSortedVerificationRegions()
  {
    if (_sortedVerificationRegions == null)
    {
      _sortedVerificationRegions = new TreeSet<ReconstructionRegion>(_reconstructionRegionComparator);
      _sortedVerificationRegions.addAll(getVerificationRegions());
    }
    return _sortedVerificationRegions;
  }

  /**
   * @author Wei Chin, Chong
   */
  public Set<ReconstructionRegion> getSortedInspectionRegions()
  {
    if (_sortedInspectionRegions == null)
    {
      _sortedInspectionRegions = new TreeSet<ReconstructionRegion>(_reconstructionRegionComparator);
      _sortedInspectionRegions.addAll(getAllInspectionRegions());
    }
    return _sortedInspectionRegions;
  }

  /**
   * @author Wei Chin, Chong
   */
  public Set<ReconstructionRegion> getSortedAlignmentRegions()
  {
    if (_sortedAlignmentRegions == null)
    {
      _sortedAlignmentRegions = new TreeSet<ReconstructionRegion>(_reconstructionRegionComparator);
      _sortedAlignmentRegions.addAll(getAlignmentRegions());
    }
    return _sortedAlignmentRegions;
  }

  /**
   * @param alignmentImageableRegionInNanoMeters
   */
  public void setAlignmentImageableRegionInNanoMeters(PanelRectangle alignmentImageableRegionInNanoMeters)
  {
    Assert.expect(alignmentImageableRegionInNanoMeters != null);
    _alignmentImageableRegionInNanoMeters = alignmentImageableRegionInNanoMeters;
  }

  /**
   * @return
   */
  public PanelRectangle getAlignmentImageableRegionInNanoMeters()
  {
    Assert.expect(_alignmentImageableRegionInNanoMeters != null);
    return _alignmentImageableRegionInNanoMeters;
  }

  /**
   * @param region
   * @author Chong, Wei Chin
   */
  public void addInspectionRegion(ReconstructionRegion region)
  {
    Assert.expect(region != null);
    Assert.expect(_componentToReconstructionRegionMap != null);
    Assert.expect(_subtypeToReconstructionRegionMap != null);

    if (_inspectionRegions == null || _inspectionRegions.isEmpty())
    {
      _inspectionRegions = new LinkedList<ReconstructionRegion>();
    }

    _inspectionRegions.add(region);
    
    // Add in component reference designator which pass the TestProgram
    // filter and it is an inspected component
    if (_testProgram.doesRegionPassFilters(region) && region.isInspected())
    {
        String componentReferenceDesignator = region.getComponent().getReferenceDesignator();
        
        if (_componentToReconstructionRegionMap.containsKey(componentReferenceDesignator) == false)
            _componentToReconstructionRegionMap.put(componentReferenceDesignator, new ArrayList<ReconstructionRegion>());
  
        _componentToReconstructionRegionMap.get(componentReferenceDesignator).add(region);    
    }
    
    for (Subtype subtype : region.getSubtypeToJointListMap().keySet())
    {
      if (_subtypeToReconstructionRegionMap.containsKey(subtype) == false)
      {
        _subtypeToReconstructionRegionMap.put(subtype, new ArrayList<ReconstructionRegion>());
      }

      if (_subtypeToReconstructionRegionMap.get(subtype).contains(region) == false)
        _subtypeToReconstructionRegionMap.get(subtype).add(region);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public List<String> getFilteredComponentReferenceDesignators()
  {
      Assert.expect(_componentToReconstructionRegionMap != null);
      //return _componentToReconstructionRegionMap;
      return new ArrayList<String>(_componentToReconstructionRegionMap.keySet());
  }

  /**
   * @return the _board
   * @author Chong, Wei Chin
   */
  public Board getBoard()
  {
    Assert.expect(_board != null);
    return _board;
  }

  /**
   * @param board the _board to set
   * @author Chong, Wei Chin
   */
  public void setBoard(Board board)
  {
    Assert.expect(board != null);
    _board = board;
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  public boolean hasBoard()
  {
    return (_board != null);
  }

  /**
   * @author Chong, Wei Chin
   */
  public static void resetNumberOfTestSubProgram()
  {
    _numSubPrograms = 0;
  }

  /**
   * @author sham
   */
  public void setUserGain(UserGainEnum userGain)
  {
    Assert.expect(userGain != null);

    _userGain = userGain;
  }

  /**
   * @author sham
   */
  public UserGainEnum getUserGain()
  {
    return _userGain;
  }
  
  /**
   * @author sheng chuan
   */
  public void setStageSpeed(StageSpeedEnum stageSpeed)
  {
    Assert.expect(stageSpeed != null);

    _stageSpeed = stageSpeed;
  }

  /**
   * @author sheng chuan
   */
  public StageSpeedEnum getStageSpeed()
  {
    return _stageSpeed;
  }

  /*
   * @author sham
   */
  public void setSubProgramRefferenceIdForAlignment(int id)
  {
    Assert.expect(id > 0);

    _subProgramRefferenceIdForAlignment = id;
  }

  /*
   * @author sham
   */
  public int getSubProgramRefferenceIdForAlignment()
  {
    return _subProgramRefferenceIdForAlignment;
  }

  /**
   * @author George A. David
   * @author sham
   */
  public Collection<ReconstructionRegion> getFilteredInspectableRegions()
  {
    List<ReconstructionRegion> inspectionReconstructionRegions = new LinkedList<ReconstructionRegion>();

    for (ReconstructionRegion reconstructionRegion : getAllInspectionRegions())
    {
      if (_testProgram.doesRegionPassFilters(reconstructionRegion) && reconstructionRegion.isInspected())
      {
        // ok, the region passed all the filters, add it to the list
        inspectionReconstructionRegions.add(reconstructionRegion);
      }
    }

    return inspectionReconstructionRegions;
  }

  /**
   * @return 
   * @author sham
   */
  public boolean isAllRegionNotIspectable()
  {
    for (ReconstructionRegion reconstructionRegion : _inspectionRegions)
    {
      if (_testProgram.doesRegionPassFilters(reconstructionRegion) && reconstructionRegion.isInspected())
      {
        return false;
      }
    }
    return true;
  }

  /**
   * @param isSubProgramPerformAlignment 
   * @author sham
   */
  public void setIsSubProgramPerformAlignment(boolean isSubProgramPerformAlignment)
  {
    Assert.expect(isSubProgramPerformAlignment == true || isSubProgramPerformAlignment == false);

    _isSubProgramPerformAlignment = isSubProgramPerformAlignment;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setIsSubProgramPerformSurfaceMap(boolean isSubProgramPerformSurfaceMap)
  {
      _isSubProgramPerformSurfaceMap = isSubProgramPerformSurfaceMap;
  }

  /**
   * @return 
   * @author Sham
   */
  public boolean isSubProgramPerformAlignment()
  {
    Assert.expect(_isSubProgramPerformAlignment == true || _isSubProgramPerformAlignment == false);

    return _isSubProgramPerformAlignment;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isSubProgramPerformSurfaceMap()
  {
    return _isSubProgramPerformSurfaceMap;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void addOpticalRegion(OpticalRegion opticalRegion)
  {
      Assert.expect(opticalRegion != null);
      
      if (_opticalRegions == null)
      {
          _opticalRegions = new ArrayList<OpticalRegion>();
      }
     
      if (_opticalRegions.contains(opticalRegion) == false)
          _opticalRegions.add(opticalRegion);   
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void addAlignmentOpticalRegion(OpticalRegion opticalRegion)
  {
      Assert.expect(opticalRegion != null);
      
      if (_alignmentOpticalRegions == null)
      {
          _alignmentOpticalRegions = new ArrayList<OpticalRegion>();
      }
      
      if (_alignmentOpticalRegions.contains(opticalRegion) == false)
          _alignmentOpticalRegions.add(opticalRegion);
  }
  
  /**
   * @author Jack Hwee
   */
  public List<OpticalRegion> getOpticalRegions()
  {
      Assert.expect(_opticalRegions != null);
      return _opticalRegions;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public List<OpticalRegion> getAlignmentOpticalRegions()
  {
      Assert.expect(_alignmentOpticalRegions != null);
      return _alignmentOpticalRegions;
  }
  
//  /**
//   * @author Cheah Lee Herng
//   */
//  public boolean hasOpticalRegions()
//  {
//      return getOpticalRegions().size() > 0 ? true : false;
//  }
  
  /**
   * @author Kok Chun, Tan
   * @return 
   */
  public boolean hasEnabledOpticalCameraRectangles()
  {
    Assert.expect(_opticalRegions != null);
    
    boolean hasEnabledOpticalCameraRectangle = false;
    if (_testProgram.getProject().getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      for (OpticalRegion opticalRegion : _opticalRegions)
      {
        if (opticalRegion.getEnabledOpticalCameraRectangles().isEmpty() == false)
        {
          hasEnabledOpticalCameraRectangle = true;
          break;
        }
      }
    }
    else
    {
      for (OpticalRegion opticalRegion : _opticalRegions)
      {
        if (opticalRegion.getEnabledBoardOpticalCameraRectangles(_board).isEmpty() == false)
        {
          hasEnabledOpticalCameraRectangle = true;
          break;
        }
      }
    }
    
    return hasEnabledOpticalCameraRectangle;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean hasAlignmentOpticalRegions()
  {
      return getAlignmentOpticalRegions().size() > 0 ? true : false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isOpticalRegionAvailable(OpticalRegion opticalRegion)
  {
      Assert.expect(opticalRegion != null);
      Assert.expect(_opticalRegions != null);
      
      if (_opticalRegions.contains(opticalRegion))
          return true;
      else
          return false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isAlignmentOpticalRegionAvailable(OpticalRegion opticalRegion)
  {
      Assert.expect(opticalRegion != null);
      Assert.expect(_alignmentOpticalRegions != null);
      
      if (_alignmentOpticalRegions.contains(opticalRegion))
          return true;
      else
          return false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public OpticalRegion getOpticalRegion(String opticalRegionName)
  {
      Assert.expect(opticalRegionName != null);
      Assert.expect(_opticalRegions != null);
      
      OpticalRegion foundOpticalRegion = null;
      for(OpticalRegion opticalRegion : _opticalRegions)
      {
          if (opticalRegion.getName().equalsIgnoreCase(opticalRegionName))
          {
              foundOpticalRegion = opticalRegion;
              break;
          }
      }
      return foundOpticalRegion;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void removeOpticalRegion(OpticalRegion opticalRegion)
  {
      Assert.expect(opticalRegion != null);
      Assert.expect(_opticalRegions != null);
      
      _opticalRegions.remove(opticalRegion);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clearOpticalRegions()
  {
      if (_opticalRegions != null)   
          _opticalRegions.clear();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clearAlignmentOpticalRegions()
  {
      if (_alignmentOpticalRegions != null)   
          _alignmentOpticalRegions.clear();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clearOpticalReferenceTestSubProgram()
  {
      if (_opticalRegionNameToSubProgramReferenceIdMap != null)
          _opticalRegionNameToSubProgramReferenceIdMap.clear();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clearOpticalRegionToComponents()
  {
    if (_opticalRegionNameToComponentsMap != null)
      _opticalRegionNameToComponentsMap.clear();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void updateModel()
  {
    // Update the z-height for components
    updateZHeightInfo(getOpticalPointToPointScans(), getOpticalRegions());
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void updateAlignmentModel()
  {
    List<OpticalPointToPointScan> alignmentOpticalPointToPointScans = getAlignmentOpticalPointToPointScans();
    List<OpticalRegion> alignmentOpticalRegions = getAlignmentOpticalRegions();        
    
    // First, we need to reset TPS model
    for(OpticalRegion alignmentOpticalRegion : alignmentOpticalRegions)
      alignmentOpticalRegion.resetModel();
    
    // Update the z-height for components
    updateZHeightInfo(alignmentOpticalPointToPointScans, alignmentOpticalRegions);
    
    // Finally, we update TPS model
    for(OpticalRegion opticalRegion : alignmentOpticalRegions)
      opticalRegion.updateModel();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void updateZHeightInfo(List<OpticalPointToPointScan> opticalPointToPointScans, List<OpticalRegion> opticalRegions)
  {
    Assert.expect(opticalPointToPointScans != null);
    Assert.expect(opticalRegions != null);
    
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
    {
      System.out.println("TestSubProgram: " + getId() + ": Total point to point scan = " + opticalPointToPointScans.size());
      System.out.println("TestSubProgram: " + getId() + ": Total optical region = " + opticalRegions.size());
    }
    
    for(OpticalPointToPointScan opticalPointToPointScan : opticalPointToPointScans)
    {
      for(OpticalRegion opticalRegion : opticalRegions)
      {              
        if (opticalRegion.getName().equals(opticalPointToPointScan.getRegionPositionName()))
        {          
          // One OpticalPointToPointScan maps to One OpticalCameraRectangle
          if (opticalPointToPointScan.isValidAverageZHeight())
          {
            double zHeightInNanometer = opticalPointToPointScan.getAverageZHeightInNanometer();
            
            List<MeshTriangle> meshTriangles = opticalRegion.getMeshTriangles();
            if (meshTriangles.isEmpty() == false && 
                getTestProgram().getProject().getPanel().isUseMeshTriangle())
            {
              for (MeshTriangle meshTriangle : meshTriangles)
              {
                for (OpticalCameraRectangle opticalCameraRectangle : meshTriangle.getOpticalCameraRectangles())
                {
                  if (opticalCameraRectangle.getName().equals(opticalPointToPointScan.getOpticalCameraRectangleName()))
                  {
                    opticalCameraRectangle.setZHeightInNanometer(zHeightInNanometer);
                    opticalCameraRectangle.setZHeightValid(true);

                    break;
                  }
                }
              }
              meshTriangles.clear();
            }
            else
            {
              Project project = _testProgram.getProject();
              Panel panel = project.getPanel();
              PanelSettings panelSettings = panel.getPanelSettings();
              boolean isPanelBasedAlignment = panelSettings.isPanelBasedAlignment();
              List<OpticalCameraRectangle> opticalCameraRectangleList;
              if (isPanelBasedAlignment)
                opticalCameraRectangleList = opticalRegion.getAllOpticalCameraRectangles();
              else
                opticalCameraRectangleList = opticalRegion.getOpticalCameraRectangles(_board);
              for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangleList)
              {
                if (opticalCameraRectangle.getName().equals(opticalPointToPointScan.getOpticalCameraRectangleName()))
                {
                  opticalCameraRectangle.setZHeightInNanometer(zHeightInNanometer);
                  opticalCameraRectangle.setZHeightValid(true);

                  break;
                }
              }
              if (opticalCameraRectangleList != null)
                opticalCameraRectangleList.clear();
            }
          }
        }
      }
    }
    
    // Update MeshTriangle Z-Height info
    for(OpticalRegion opticalRegion : opticalRegions)
    {
      if (opticalRegion.getMeshTriangles().isEmpty() == false && 
          getTestProgram().getProject().getPanel().isUseMeshTriangle())
      {
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
        {
          System.out.println("TestSubProgram: " + getId() + ": OpticalRegion " + opticalRegion.getName()
                  + " has " + opticalRegion.getMeshTriangles().size() + " MeshTriangle");
        }

        for (MeshTriangle meshTriangle : opticalRegion.getMeshTriangles())
        {
          meshTriangle.calculateZHeightInNanometer();
        }
      }
    }    
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void updateReconstructionRegionsFocusInstructionWithModelData(FileLoggerAxi fileLogger) throws XrayTesterException
  {
      Assert.expect(_testProgram != null);
      Assert.expect(fileLogger != null);
      
      Project project = _testProgram.getProject();
      Panel panel = project.getPanel();
      PanelSettings panelSettings = panel.getPanelSettings();
      boolean isPanelBasedAlignment = panelSettings.isPanelBasedAlignment();
      
      List<Component> components = new ArrayList<Component>();
      Collection<ReconstructionRegion> filteredRegions = getFilteredInspectionRegions();
      try
      {
        for(OpticalRegion opticalRegion : getOpticalRegions())
        {
          if (isPanelBasedAlignment)
            components = opticalRegion.getComponents();
          else
            components = opticalRegion.getComponents(getBoard());

          for(Component component : components)
          {
            modifySurfaceMapFocusInstruction(this, component, opticalRegion, false, fileLogger);
          }
        }  
      }
      finally
      {
         if (filteredRegions != null)
         {
             filteredRegions.clear();
         }
         
         if (components != null)
           components.clear();
      }
  }
  
  /**
   * Edited on 01 Oct 2016 - XCR3780 Additional Surface Map Info log
   * 
   * @author Cheah Lee Herng 
   */
  public void updateAlignmentReconstructionRegionsFocusInstructionWithModelData() throws XrayTesterException
  {
//    Assert.expect(_testProgram != null);
//      
//    Project project = _testProgram.getProject();
//    Panel panel = project.getPanel();
//    PanelSettings panelSettings = panel.getPanelSettings();
//    boolean isPanelBasedAlignment = panelSettings.isPanelBasedAlignment();
//
//    List<Component> components = new ArrayList<Component>();    
//    try
//    {
//      for(OpticalRegion alignmentOpticalRegion : getAlignmentOpticalRegions())
//      {
//        if (alignmentOpticalRegion.isSufficientModelDataPoint())
//        {
//          if (isPanelBasedAlignment)
//            components = alignmentOpticalRegion.getComponents();
//          else
//            components = alignmentOpticalRegion.getComponents(getBoard());
//
//          for(Component component : components)
//            modifySurfaceMapFocusInstruction(this, component, alignmentOpticalRegion, true);
//        }
//      }  
//    }
//    finally
//    {
//      if (components != null)
//        components.clear();
//    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void updateReconstructionRegionsFocusInstructionWithReferenceSubProgramModelData(FileLoggerAxi fileLogger) throws XrayTesterException
  {
      Assert.expect(_testProgram != null);
      Assert.expect(_opticalRegionNameToSubProgramReferenceIdMap != null);
      Assert.expect(fileLogger != null);
      
      Collection<ReconstructionRegion> filteredRegions = getFilteredInspectionRegions();
      try
      {
        for(Map.Entry<String,Integer> entry : _opticalRegionNameToSubProgramReferenceIdMap.entrySet())
        {
          String opticalRegionName = entry.getKey();
          int subProgramReferenceId = entry.getValue();

          TestSubProgram referenceSubProgram = _testProgram.getTestSubProgramById(subProgramReferenceId);
          Assert.expect(referenceSubProgram != null);

          OpticalRegion opticalRegion = referenceSubProgram.getOpticalRegion(opticalRegionName);
          Assert.expect(opticalRegion != null);

          List<Component> componentReferenceToSubProgramOpticalRegion = getComponentsReferenceToSubProgramForOpticalRegion(opticalRegionName);
          for(Component component : componentReferenceToSubProgramOpticalRegion)
          {
            modifySurfaceMapFocusInstruction(referenceSubProgram, component, opticalRegion, false, fileLogger);
          }
        }  
      }
      finally
      {
         if (filteredRegions != null)
         {
             filteredRegions.clear();
         } 
      }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void modifySurfaceMapFocusInstruction(TestSubProgram testSubProgram, 
                                                Component component, 
                                                OpticalRegion opticalRegion,
                                                boolean isAlignmentMode,
                                                FileLoggerAxi fileLogger) throws XrayTesterException
  {
      Assert.expect(testSubProgram != null);
      Assert.expect(component != null);
      Assert.expect(opticalRegion != null);
      Assert.expect(fileLogger != null);
      
      // Initialize all variables needed for later use
      ReconstructedImagesProducer reconstructedImagesProducer = ImageAcquisitionEngine.getInstance().getReconstructedImagesProducer();
      byte focusProfileShape = FocusProfileShapeTypeEnum.map(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
      byte focusMethodId = ReconstructionTypeEnum.map(FocusMethodEnum.USE_Z_HEIGHT);
      boolean isUnitTest = UnitTest.unitTesting() ? true : false;
      int zHeight = 0;
      
      List<ReconstructionRegion> reconstructionRegions = null;
      if (isAlignmentMode == false)
        reconstructionRegions = getInspectionRegions(opticalRegion.getBoards(), component);
      else
        reconstructionRegions = getAlignmentInspectionRegions(opticalRegion.getBoards(), component);
      
      for(ReconstructionRegion inspectionRegion : reconstructionRegions)
      {
        // XCR-2643 Ability to Turn On/Off PSP Local Search Feature - Cheah Lee Herng        
        Subtype subtype = inspectionRegion.getSubtypes().iterator().next();
        boolean isEnabledPspLocalSearch = SharedPspAlgorithm.isEnabledPspLocalSearch(subtype);
        
          for(FocusGroup focusGroup : inspectionRegion.getFocusGroups())
          {
            // Reset height value
            zHeight = 0;             
            int interpolatedTopSurfaceZHeightInNanometers = 0;
            
            // XCR-2679
            MeshTriangle meshTriangle = null;
            OpticalCameraRectangle opticalCameraRectangle = null;

            if (inspectionRegion.hasMeshTriangle() && testSubProgram.getTestProgram().getProject().getPanel().isUseMeshTriangle())
            {
              meshTriangle = inspectionRegion.getMeshTriangle();
              interpolatedTopSurfaceZHeightInNanometers = (int)meshTriangle.getZHeightInNanometer();
            }
            else if (inspectionRegion.hasReferenceOpticalCameraRectangle())
            {
              opticalCameraRectangle = inspectionRegion.getReferenceOpticalCameraRectangle();
              interpolatedTopSurfaceZHeightInNanometers = (int)opticalCameraRectangle.getZHeightInNanometer();
            }
            else
              Assert.expect(false, "Region ID " + inspectionRegion.getRegionId() + " : Fail to get Z-Height");

              boolean focusProfileShapeModified = false;
              for(Slice slice : focusGroup.getSlices())
              {
                  // Skip uninspected slice and surface modeling slices
                  if (slice.createSlice() && slice.getSliceName() != SliceNameEnum.SURFACE_MODELING)
                  {
                      // khang-wah.chnee, 2012-12-28, 2.5D
                      SliceNameEnum sliceName = slice.getSliceName();
                      if (slice.getSliceType().equals(SliceTypeEnum.PROJECTION)==true)
                      {
                        if (inspectionRegion.getInspectionFamilyEnum().equals(InspectionFamilyEnum.THROUGHHOLE) ||
                            inspectionRegion.getInspectionFamilyEnum().equals(InspectionFamilyEnum.OVAL_THROUGHHOLE)) //Siew Yeng - XCR-3318 - Oval PTH
                        {
                          sliceName = SliceNameEnum.THROUGHHOLE_BARREL;
                        }
                        else if (inspectionRegion.getInspectionFamilyEnum().equals(InspectionFamilyEnum.GRID_ARRAY))
                        {
                          sliceName = SliceNameEnum.MIDBALL;
                        }
                        else
                        {
                          continue;
                        }
                      }
                      // end
                  
                      if (inspectionRegion.hasMeshTriangle() &&
                          testSubProgram.getTestProgram().getProject().getPanel().isUseMeshTriangle())
                      {
                        // Get interpolated Z height
                        zHeight = getSurfaceMapHeightInNanometers(testSubProgram,
                                                                  component,
                                                                  inspectionRegion.getSubtypes().iterator().next(),
                                                                  inspectionRegion.getMeshTriangle().getName(),
                                                                  inspectionRegion.getJointTypeEnum(), 
                                                                  sliceName, // khang-wah.chnee, 2012-12-28, 2.5D
                                                                  interpolatedTopSurfaceZHeightInNanometers);
                      }
                      else
                      {
                          // Get interpolated Z height
                         zHeight = getSurfaceMapHeightInNanometers(testSubProgram,
                                                                  component,
                                                                  inspectionRegion.getSubtypes().iterator().next(),
                                                                  "",
                                                                  inspectionRegion.getJointTypeEnum(), 
                                                                  sliceName, // khang-wah.chnee, 2012-12-28, 2.5D
                                                                  interpolatedTopSurfaceZHeightInNanometers);
                      }
                      
                      if (isUnitTest == false && focusProfileShapeModified == false)
                      {
                        if (inspectionRegion.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE) == false &&
                            inspectionRegion.getJointTypeEnum().equals(JointTypeEnum.OVAL_THROUGH_HOLE) == false && //Siew Yeng - XCR-3318 - Oval PTH
                            inspectionRegion.getJointTypeEnum().equals(JointTypeEnum.PRESSFIT) == false && 
                            isEnabledPspLocalSearch)
                        {
                          // Use the same FocusProfileShape and FocusMethod defined in each slice
                          focusProfileShape = FocusProfileShapeTypeEnum.map(focusGroup.getFocusSearchParameters().getFocusProfileShape());
                          focusMethodId = ReconstructionTypeEnum.map(slice.getFocusInstruction().getFocusMethod());
                          
                          reconstructedImagesProducer.modifyFocusProfileShape(getId(), 
                                                                          inspectionRegion, 
                                                                          focusGroup.getId(), 
                                                                          focusProfileShape, 
                                                                          zHeight,
                                                                          true,
                                                                          focusGroup.getSearchRangeLowLimitInNanometers(),
                                                                          focusGroup.getSearchRangeHighLimitInNanometers(),
                                                                          focusGroup.getPspZOffsetInNanometers());
                        }
                        else
                        {
                          focusProfileShape = FocusProfileShapeTypeEnum.map(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
                          focusMethodId = ReconstructionTypeEnum.map(FocusMethodEnum.USE_Z_HEIGHT);
                          
                          reconstructedImagesProducer.modifyFocusProfileShape(getId(), 
                                                                          inspectionRegion, 
                                                                          focusGroup.getId(), 
                                                                          focusProfileShape, 
                                                                          -1,
                                                                          true,
                                                                          focusGroup.getSearchRangeLowLimitInNanometers(),
                                                                          focusGroup.getSearchRangeHighLimitInNanometers(),
                                                                          focusGroup.getPspZOffsetInNanometers());
                        }                                                  

                        focusProfileShapeModified = true;
                        
                        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
                        {
                          System.out.println("TestSubProgram Id " + getId() + " : FocusGroup Id " + focusGroup.getId() + " : Slice Id " + slice.getSliceName().getId() + 
                                             " : Component " + component.getReferenceDesignator() +
                                             " : JointType " + inspectionRegion.getJointTypeEnum().getName() +
                                             " : FocusProfileShape " + (int)focusProfileShape + " : FocusMethod " + (int)focusMethodId +
                                             " : isEnabledPspLocalSearch = " + isEnabledPspLocalSearch);
                        }
                      }

                      FocusInstruction focusInstruction = slice.getFocusInstruction();
                      if (isUnitTest == false)
                      {
                        reconstructedImagesProducer.modifyFocusInstruction(getId(),
                                                                           inspectionRegion,
                                                                           slice.getSliceName().getId(),
                                                                           zHeight,
                                                                           focusInstruction.getPercentValue(),
                                                                           focusInstruction.getZOffsetInNanoMeters(),
                                                                           focusMethodId);

                        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SURFACE_MAP_INFO_LOG) == true)
                        {
                          fileLogger.appendWithoutDateTime(generateSurfaceMapInfo(component,
                                                                                  inspectionRegion,
                                                                                  focusGroup,
                                                                                  sliceName,
                                                                                  zHeight,
                                                                                  isEnabledPspLocalSearch));
                        }
                      }
                      
                      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
                      {
                        double finalZHeightInNanometers = zHeight + focusInstruction.getZOffsetInNanoMeters();
                        System.out.println("TestSubProgram Id " + getId() + " : FocusGroup Id " + focusGroup.getId() + " : Slice Id " + slice.getSliceName().getId() + 
                                           " : Component " + component.getReferenceDesignator() + 
                                           " : Interpolated Z Height for region " + inspectionRegion.getRegionId() + " is " + finalZHeightInNanometers + " nanometers.");
                      }
                  }
              }
          }
      }
      
      // Clear list
      if (reconstructionRegions != null)
        reconstructionRegions.clear();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean hasComponent(Component component)
  {
      Assert.expect(component != null);
      
      boolean hasComponent = false;
      List<String> filteredComponentReferenceDesignators = getFilteredComponentReferenceDesignators();
      if (filteredComponentReferenceDesignators.contains(component.getReferenceDesignator()))
          hasComponent = true;
      return hasComponent;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void addOpticalRegionNameToSubProgramReference(String opticalRegionName, int id, Component component)
  {
      Assert.expect(opticalRegionName != null);
      Assert.expect(id > 0);
      Assert.expect(component != null);
      Assert.expect(_opticalRegionNameToSubProgramReferenceIdMap != null);
      Assert.expect(_opticalRegionNameToComponentsMap != null);
      
      if (_opticalRegionNameToSubProgramReferenceIdMap.containsKey(opticalRegionName) == false)
          _opticalRegionNameToSubProgramReferenceIdMap.put(opticalRegionName, id);
      
      if (_opticalRegionNameToComponentsMap.containsKey(opticalRegionName) == false)
      {
          List<Component> components = new ArrayList<Component>();
          components.add(component);
          _opticalRegionNameToComponentsMap.put(opticalRegionName, components);
      }
      else
      {
          List<Component> components = _opticalRegionNameToComponentsMap.get(opticalRegionName);
          if (components.contains(component) == false)
              components.add(component);
      }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public Map<String, Integer> getOpticalRegionNameToSubProgramReference()
  {
      Assert.expect(_opticalRegionNameToSubProgramReferenceIdMap != null);
      return _opticalRegionNameToSubProgramReferenceIdMap;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isOpticalRegionNameToComponentExists(String opticalRegionName, Component component)
  {
    Assert.expect(opticalRegionName != null);
    Assert.expect(component != null);
    Assert.expect(_opticalRegionNameToComponentsMap != null);
    
    boolean isOpticalRegionNameToComponentExists = false;
    if (_opticalRegionNameToComponentsMap.containsKey(opticalRegionName))
    {
      List<Component> components = _opticalRegionNameToComponentsMap.get(opticalRegionName);
      if (components.contains(component))
        isOpticalRegionNameToComponentExists = true;
    }
    
    return isOpticalRegionNameToComponentExists;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public Map<String, List<Component>> getOpticalRegionNameToComponents()
  {
      Assert.expect(_opticalRegionNameToComponentsMap != null);
      return _opticalRegionNameToComponentsMap;
  }
  
    /**
   * @author Cheah Lee Herng
   */
  public boolean isComponentUsePsp(String componentRefDes)
  {
    Assert.expect(componentRefDes != null);
    Assert.expect(_opticalRegionNameToComponentsMap != null);
    
    boolean isComponentUsePsp = false;
    for(Map.Entry<String, List<Component>> entry : _opticalRegionNameToComponentsMap.entrySet())
    {
       List<Component> components = entry.getValue();
       for(Component component : components)
       {
          if (component.getReferenceDesignator().equalsIgnoreCase(componentRefDes))
          {
             isComponentUsePsp = true;
             break;
          }
       }
    }
    return isComponentUsePsp;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public List<Component> getComponentsReferenceToSubProgramForOpticalRegion(String opticalRegionName)
  {
      Assert.expect(opticalRegionName != null);
      Assert.expect(_opticalRegionNameToComponentsMap != null);
      
      return _opticalRegionNameToComponentsMap.get(opticalRegionName);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void removeOpticalRegionNameToSubProgramReference(String opticalRegionName)
  {
      Assert.expect(opticalRegionName != null);
      Assert.expect(_opticalRegionNameToSubProgramReferenceIdMap != null);
      Assert.expect(_opticalRegionNameToComponentsMap != null);
      
      if (_opticalRegionNameToSubProgramReferenceIdMap.containsKey(opticalRegionName))
          _opticalRegionNameToSubProgramReferenceIdMap.remove(opticalRegionName);
      
      if (_opticalRegionNameToComponentsMap.containsKey(opticalRegionName))
      {
          List<Component> components = _opticalRegionNameToComponentsMap.get(opticalRegionName);
          components.clear();
          _opticalRegionNameToComponentsMap.remove(opticalRegionName);
      }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean hasOpticalRegionNameToSubProgramReference(String opticalRegionName)
  {
      Assert.expect(opticalRegionName != null);
      Assert.expect(_opticalRegionNameToSubProgramReferenceIdMap != null);
      
      if (_opticalRegionNameToSubProgramReferenceIdMap.containsKey(opticalRegionName))
          return true;
      else
          return false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean hasOpticalRegionNameToSubProgramReference()
  {
      Assert.expect(_opticalRegionNameToSubProgramReferenceIdMap != null);
      return (_opticalRegionNameToSubProgramReferenceIdMap.size() > 0);
  }

    /**
   * @author sham
   */
  public void setMagnificationType(MagnificationTypeEnum magnificationType)
  {
    Assert.expect(magnificationType != null);

    _magnificationType = magnificationType;
  }

  /**
   * @author sham
   */
  public MagnificationTypeEnum getMagnificationType()
  {
    Assert.expect(_magnificationType != null);
    
    return _magnificationType;
  }

  /**
   * @author Chong Wei Chin
   */
  public boolean isLowMagnification()
  {
    return _magnificationType.equals(MagnificationTypeEnum.LOW);
  }
  
  /**
   * @author Chong Wei Chin
   */
  public boolean isHighMagnification()
  {
    return _magnificationType.equals(MagnificationTypeEnum.HIGH);
  }

 /**
   * @author sham
   */
  public void setRealVerificationRegionsBoundsInNanoMeters(PanelRectangle realVerificationRegionsBoundsInNanoMeters)
  {
    Assert.expect(realVerificationRegionsBoundsInNanoMeters != null);

    _realVerificationRegionsBoundsInNanoMeters = realVerificationRegionsBoundsInNanoMeters;
  }

  /**
   * @author sham
   */
  public PanelRectangle getRealVerificationRegionsBoundsInNanoMeters()
  {
    Assert.expect(_realVerificationRegionsBoundsInNanoMeters != null);

    return _realVerificationRegionsBoundsInNanoMeters;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void enableSurfaceMapParameters(Component component)
  {
      Assert.expect(component != null);
      
      // Set each pad type to use PSP
//      ComponentType componentType = component.getComponentType();
//      if (componentType.getGlobalSurfaceModel().equals(GlobalSurfaceModelEnum.PSP) == false)
//      {
//          for(PadType padType : componentType.getPadTypes())
//              padType.getPadTypeSettings().setGlobalSurfaceModel(GlobalSurfaceModelEnum.PSP);
//      }
//      
//      // Mark ComponentType to use PSP result
//      componentType.setToUsePspResult(true);
//      
//      List<com.axi.v810.business.testProgram.ReconstructionRegion> reconstructionRegions = getInspectionRegions(component);
//      for(ReconstructionRegion reconstructionRegion : reconstructionRegions)
//      {
//          // Setup each region to use PSP
//          reconstructionRegion.setGlobalSurfaceModelEnum(GlobalSurfaceModelEnum.PSP);
//          
//          // Mark each region not to retest when this region is set to use PSP
//          reconstructionRegion.setRetestForFailingJoint(false);
//          reconstructionRegion.setRetestForPassingJoint(false);
//      }
  }
  
  /**
   * This function sets the pad slice reference as the input to Image Reconstruction Processor (IRP).
   * Note that this function is somewhat different from ProgramGeneration.getZOffet function as this function
   * really serves the reference to other slice. ProgramGeneration should have actually define the actual Z offset
   * from pad slice instead of this function.
   * 
   * @author Cheah Lee Herng
   */
  private int getSurfaceMapHeightInNanometers(TestSubProgram testSubProgram, 
                                              Component component,
                                              Subtype subtype,
                                              String meshTriangleName,
                                              JointTypeEnum jointTypeEnum, 
                                              SliceNameEnum sliceNameEnum, 
                                              int topSidePadHeightInNanometers)
  {
      Assert.expect(_testProgram != null);
      Assert.expect(testSubProgram != null);
      Assert.expect(component != null);
      Assert.expect(subtype != null);
      Assert.expect(meshTriangleName != null);
      Assert.expect(jointTypeEnum != null);
      Assert.expect(sliceNameEnum != null);
      
      int referencePadHeightInNanometers = 0;
      
      boolean isTopSide = component.isTopSide();
      int panelThicknessInNanometers = _testProgram.getProject().getPanel().getThicknessInNanometers();
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION))
          panelThicknessInNanometers -= XrayTester.getSystemPanelThicknessOffset();
      double estimatedBoardThicknessInNanometers = 0.0;
      
      // Before start anything, we need to get the board thickness.
      // This depends on whether we have estimated the board thickness from Alignment OpticalRegion
      if (testSubProgram.hasEstimatedBoardThicknessInNanometers())
      {
          estimatedBoardThicknessInNanometers = testSubProgram.getEstimatedBoardThicknessInNanometers();
          if (estimatedBoardThicknessInNanometers == 0.0)
          {
              estimatedBoardThicknessInNanometers = panelThicknessInNanometers;
          }
      }
      else
      {
          estimatedBoardThicknessInNanometers = panelThicknessInNanometers;
      }
      
      // Before we do anything, we need to offset the top side pad height to cater the distance between system coupon to shim-down coupon area
      //topSidePadHeightInNanometers += MathUtil.convertMilsToNanoMeters(Config.getInstance().getIntValue(HardwareConfigEnum.DISTANCE_BETWEEN_FIXED_RAIL_DOWN_SHIM_TOP_FIDUCIAL_AREA_TO_SYSTEM_COUPON_IN_NANOMETERS)); 
      
      // First, get the initial reference pad slice height
      if (isTopSide)
          referencePadHeightInNanometers = topSidePadHeightInNanometers;
      else
          referencePadHeightInNanometers = topSidePadHeightInNanometers - (int)estimatedBoardThicknessInNanometers;
      
      // Get slice offset
      if (sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_PIN_SIDE) ||
          sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL) ||
          sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_2) ||
          sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_3) ||
          sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_4) ||
          sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_5) ||
          sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_6) ||
          sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_7) ||
          sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_8) ||
          sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_9) ||
          sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_10) ||
          sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE) ||
          sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_INSERTION) ||
          sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_PROTRUSION))
      {          
          // Note that Pin Through Hole (PTH) pin side slice is on the opposite side of the component.
          // While Pressfit joint type has the pin side slice on the same side as the component.
          if (jointTypeEnum.equals(JointTypeEnum.THROUGH_HOLE) ||
              jointTypeEnum.equals(JointTypeEnum.OVAL_THROUGH_HOLE)) //Siew Yeng - XCR-3318 - Oval PTH
          {
              if (isTopSide)
                  referencePadHeightInNanometers = topSidePadHeightInNanometers - (int)estimatedBoardThicknessInNanometers;
              else
                  referencePadHeightInNanometers = topSidePadHeightInNanometers;
          }
      }
      
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
      {
        System.out.println("TestSubProgram " + getId() + " : Component " + component.getReferenceDesignator() + " : referencePadHeightInNanometers = " + referencePadHeightInNanometers);
        System.out.println("TestSubProgram " + getId() + " : Component " + component.getReferenceDesignator() + " : panelThicknessInNanometers = " + panelThicknessInNanometers);
        System.out.println("TestSubProgram " + getId() + " : Component " + component.getReferenceDesignator() + " : sliceOffsetFromPadInNanometers = " + ProgramGeneration.getInstance().getZOffset(jointTypeEnum, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        System.out.println("TestSubProgram " + getId() + " : Component " + component.getReferenceDesignator() + " : MeshTriangle used = " + meshTriangleName);
      }
      
      return referencePadHeightInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void addEstimatedBoardThicknessInNanometers(ReconstructedImages reconstructedImages)
  {
      Assert.expect(reconstructedImages != null);
      
      if (_estimatedBoardThicknessInNanometersList == null)
      {
          _estimatedBoardThicknessInNanometersList = new ArrayList<Double>();
      }
      
      double estimatedBoardThicknessInNanometers = getOpticalEstimatedBoardThicknessInNanometers(reconstructedImages);
      
      // We only want to get the valid and reasonable board thickness
      if (estimatedBoardThicknessInNanometers > 0.0)
        _estimatedBoardThicknessInNanometersList.add(estimatedBoardThicknessInNanometers);
  }
  
  /**
   * In order to get board thickness, we need to have Alignment Region as the input.
   * So that we can use the Z Height for bottom components as the reference point for
   * us to calculate the offset from top to bottom. That will serve as our estimated
   * board thickness.
   * 
   * @author Cheah Lee Herng
   */
  private double getOpticalEstimatedBoardThicknessInNanometers(ReconstructedImages reconstructedImages)
  {
      Assert.expect(reconstructedImages != null);
      
      List<OpticalPointToPointScan> alignmentOpticalPointToPointScans = getAlignmentOpticalPointToPointScans();
      List<OpticalRegion> alignmentOpticalRegions = getAlignmentOpticalRegions();
      
      ReconstructionRegion alignmentRegion = reconstructedImages.getReconstructionRegion();
      AlignmentGroup alignmentGroup = alignmentRegion.getAlignmentGroup();
      Assert.expect(alignmentRegion.isAlignmentRegion());
      
      String alignmentGroupName = alignmentGroup.getName();
      int counter = 0;
      double totalZHeightInNanometers = 0.0;
      double meanTopZHeightInNanometers = 0.0;
      double estimatedBoardThickness = 0.0;

      try
      {
          if (alignmentRegion.isTopSide() == false)
          {
              for(OpticalPointToPointScan alignmentOpticalPointToPointScan : alignmentOpticalPointToPointScans)
              {
                  if (alignmentOpticalPointToPointScan.getAlignmentGroupName().equalsIgnoreCase(alignmentGroupName))
                  {
                      for(OpticalRegion alignmentOpticalRegion : alignmentOpticalRegions)
                      {
                          if (alignmentOpticalRegion.getName().equals(alignmentOpticalPointToPointScan.getRegionPositionName()))
                          {
                              for(Map.Entry<String,DoubleRef> entry : alignmentOpticalPointToPointScan.getPointPanelPositionNameToZHeightInNanometers().entrySet())
                              {
                                  DoubleRef zHeightInNanometers = entry.getValue();
                                  totalZHeightInNanometers += zHeightInNanometers.getValue();

                                  ++counter;
                              }
                          }
                      }
                  }
              }

              if (counter > 0)
              {
                  meanTopZHeightInNanometers = totalZHeightInNanometers / counter;

                  ReconstructedSlice reconstructedAlignmentSlice = reconstructedImages.getFirstSlice();
                  int alignmentZHeightInNanometers = reconstructedAlignmentSlice.getHeightInNanometers();

                  estimatedBoardThickness = meanTopZHeightInNanometers - alignmentZHeightInNanometers;

                  //if (Config.isDeveloperDebugModeOn())
                  //{
                      System.out.println("TestSubProgram Id " + getId() + ": Project " + _testProgram.getProject().getName() + 
                              ": Alignment GroupName " + alignmentGroupName + ": Estimated Board Thickness (in nm) = " + estimatedBoardThickness);
                  //}
              }
          }
          else
              estimatedBoardThickness = 0.0;
      }
      finally
      {
          if (alignmentOpticalPointToPointScans != null)
          {
              alignmentOpticalPointToPointScans.clear();
              alignmentOpticalPointToPointScans = null;
          }
      }
      return estimatedBoardThickness;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private boolean hasEstimatedBoardThicknessInNanometers()
  {     
      if (_estimatedBoardThicknessInNanometersList == null || _estimatedBoardThicknessInNanometersList.isEmpty())
          return false;
      else
          return true;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private double getEstimatedBoardThicknessInNanometers()
  {
      List<Double> boardThicknessInNanometers = new ArrayList<Double>();
      double estimatedBoardThicknessInNanometers = 0.0;
      try
      {
          if (_isEstimatedBoardThicknessComputed)
          {
              estimatedBoardThicknessInNanometers = _estimatedBoardThicknessInNanometers;
          }
          else
          {
              // First, get the transient board thickness value into new array list
              if (_estimatedBoardThicknessInNanometersList != null)
              {
                  for(Double boardThicknessValue : _estimatedBoardThicknessInNanometersList)
                  {
                      boardThicknessInNanometers.add(boardThicknessValue);
                  }
              }

              // From the new array list, we populate the value into float array and
              // calculate the median of the values
              if (boardThicknessInNanometers.size() > 0)
              {
                  int index = 0;
                  float[] estimatedBoardThicknessInNanometersFloatArray = new float[boardThicknessInNanometers.size()];

                  for(Double estimatedBoardThicknessValue : boardThicknessInNanometers)
                  {
                      estimatedBoardThicknessInNanometersFloatArray[index++] = estimatedBoardThicknessValue.floatValue();
                  }

                  Assert.expect(estimatedBoardThicknessInNanometersFloatArray.length == boardThicknessInNanometers.size());
                  estimatedBoardThicknessInNanometers = StatisticsUtil.median(estimatedBoardThicknessInNanometersFloatArray);

                  _isEstimatedBoardThicknessComputed = true;
                  _estimatedBoardThicknessInNanometers = estimatedBoardThicknessInNanometers;
                  
                  //if (Config.isDeveloperDebugModeOn())
                  //{
                      System.out.println("TestSubProgram Id " + getId() + ": Project " + _testProgram.getProject().getName() + 
                              ": Computed Median Estimated Board Thickness (in nm) = " + _estimatedBoardThicknessInNanometers);
                  //}
              }
          }
      }
      finally
      {
          if (boardThicknessInNanometers != null)
          {
              boardThicknessInNanometers.clear();
              boardThicknessInNanometers = null;
          }
      }
      return estimatedBoardThicknessInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clearEstimatedBoardThicknessInNanometers()
  {
      if (_estimatedBoardThicknessInNanometersList != null)
      {
          _estimatedBoardThicknessInNanometersList.clear();
      }
      
      _isEstimatedBoardThicknessComputed = false;
      _estimatedBoardThicknessInNanometers = 0.0;
  }
  
  /**
   * @author Kee, Chin Seong
   * Info :: To prevent custom alignment overwrite the ORIGINAL gain value, this function has been introduced.
   */
  public void setVerificationUserGain(UserGainEnum userGain)
  {
    Assert.expect(userGain != null);

    _verficationUserGain = userGain;
  }

  /**
   * @author Kee, Chin Seong
   * Info :: To prevent custom alignment overwrite the ORIGINAL gain value, this function has been introduced.
   */
  public UserGainEnum getVerificationUserGain()
  {
    return _verficationUserGain;
  }
  
  /**
   * @author sheng chuan
   * Info :: To prevent custom alignment overwrite the ORIGINAL gain value, this function has been introduced.
   */
  public void setVerificationStageSpeed(StageSpeedEnum stageSpeedEnum)
  {
    Assert.expect(stageSpeedEnum != null);

    _verficationStageSpeed = stageSpeedEnum;
  }

  /**
   * @author sheng chuan
   * Info :: To prevent custom alignment overwrite the ORIGINAL gain value, this function has been introduced.
   */
  public StageSpeedEnum getVerificationStageSpeed()
  {
    return _verficationStageSpeed;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setVirtualLiveAlignmentStageSpeed(StageSpeedEnum stageSpeedEnum)
  {
    Assert.expect(stageSpeedEnum != null);

    _virtualLiveAlignmentStageSpeed = stageSpeedEnum;
  }

  /**
   * @author Yong Sheng Chuan
   */
  public StageSpeedEnum getVirtualLiveAlignmentStageSpeed()
  {
    return _virtualLiveAlignmentStageSpeed;
  }

  /**
   * @author sheng chuan
   */
  public void setVirtualLiveAlignmentUserGain(UserGainEnum userGainEnum)
  {
    Assert.expect(userGainEnum != null);

    _virtualLiveAlignmentUserGain = userGainEnum;
  }

  /**
   * @author sheng chuan
   */
  public UserGainEnum getVirtualLiveAlignmentUserGain()
  {
    return _virtualLiveAlignmentUserGain;
  }
  
  /**
   * @author Lim Lay Ngor
   */
  public void setEstimateTestSubProgramExecutionTime(double estimateTestSubProgramExecutionTime)
  {
    _estimateTestSubProgramExecutionTime = estimateTestSubProgramExecutionTime;
  }

  /**
   * @author Lim Lay Ngor
   */
  public double getEstimateTestSubProgramExecutionTime()
  {
    return _estimateTestSubProgramExecutionTime;
  }
  
  /*
   * @author Khang Wah, 2013-08-23, New Scan Route
   */
  public void removeScanPassToImageGroup(ScanPass scanpass)
  {        
    Assert.expect(_scanPassToImageGroupMap.containsKey(scanpass));
    _scanPassToImageGroupMap.remove(scanpass);
  }
  
  /*
   * @author Khang Wah, 2013-08-23, New Scan Route
   */
  public void addScanPassToImageGroup(ScanPass scanPass, ImageGroup imageGroup)
  {
    if (_scanPassToImageGroupMap == null)
    {
      _scanPassToImageGroupMap = new HashMap<ScanPass, ImageGroup>(); 
    }
      
    _scanPassToImageGroupMap.put(scanPass, imageGroup);
  }
  
  /*
   * @author Khang Wah, 2013-08-23, New Scan Route
   */
  public ImageGroup getScanPassImageGroup(ScanPass scanPass)
  {
      Assert.expect(_scanPassToImageGroupMap != null);
    
      return _scanPassToImageGroupMap.get(scanPass);    
  }
  
  /**
   * @author Khang Wah, 2013-11-05, Real Time Psh
   */
  public void setMemorySufficientForRealTimePsh(boolean sufficient)
  {
    _isSufficientMemoryForRealTimePsh = sufficient;
  }
  
  /**
   * @author Khang Wah, 2013-11-05, Real Time Psh
   */
  public boolean isSufficientMemoryForRealTimePsh()
  {
    return _isSufficientMemoryForRealTimePsh;
  }
  
  /**
   * @author Khang Wah, 2014-06-19, PSH
   */
  public void clearIsolatedGlobalSurfaceRegion()
  {
    _isolatedGlobalSurfaceRegions.clear();
  }
  
  /**
   * @author Khang Wah, 2014-06-19, PSH
   */
  public Collection<ReconstructionRegion> getIsolatedGlobalSurfaceRegion()
  {
    return _isolatedGlobalSurfaceRegions;
  }
  
  /**
   * @author Khang Wah, 2014-06-19, PSH
   */
  public void addIsolatedGlobalSurfaceRegions(Collection<ReconstructionRegion> regions)
  {
    _isolatedGlobalSurfaceRegions.addAll(regions);
  }
  
  /**
   * @author Khang Wah, 2014-06-19, PSH
   */
  public boolean isContainIsolatedGlobalSurfaceRegion()
  {
    return !_isolatedGlobalSurfaceRegions.isEmpty();
  }
  
  /**
   * @author Khang Wah, 2013-08-23, New Scan Route
   */
  public void clearScanPassToImageGroupMap()
  {
    if (_scanPassToImageGroupMap != null)
      _scanPassToImageGroupMap.clear();    
  }

  /**
   * @author Cheah Lee Herng
   */
  public void clear(boolean isVirtualLiveWithAlignment)
  {
    if (isVirtualLiveWithAlignment)
    {
      if (_inspectionRegions != null)
      {
        for(ReconstructionRegion region : _inspectionRegions)
          region.clear();

        _inspectionRegions.clear();
      }
      
      if (_alignmentScanPaths != null)
      {
        for(ScanPath scanPath : _alignmentScanPaths)
          scanPath.clear();

        _alignmentScanPaths.clear();
      }
      
      if (_processorStripsForInspectionRegions != null)
        _processorStripsForInspectionRegions.clear();
    }
    else
    {
      if (_verificationRegions != null)
      {
        for(ReconstructionRegion region : _verificationRegions)
          region.clear();

        _verificationRegions.clear();
      }
      
      if (_processorStripsForVerificationRegions != null)
        _processorStripsForVerificationRegions.clear();
    }
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public boolean isUsingRuntimeManualAlignmentTransform()
  {
    return _isUsingRuntimeManualAlignment;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void setIsUsingRuntimeManualAlignmentTransform(boolean isUsingRuntimeManualAlignmentTransform)
  {
    _isUsingRuntimeManualAlignment = isUsingRuntimeManualAlignmentTransform;
  }

  /*
   * XCR2181: Able to run alignment starting from failed board
   * @author Khaw Chek Hau
   */
  public boolean isRunAlignmentLearned()
  {
    return _isRunAlignmentLearned;
  }
 
  public void setRunAlignmentLearned(boolean isRunAlignmentLearned)
  {
    _isRunAlignmentLearned = isRunAlignmentLearned;
  }
  
  public boolean isInspectionDone()
  {
    return _isInspectionDone;
  }
 
  public void setInspectionDone(boolean isInspectionDone)
  {
    _isInspectionDone = isInspectionDone;
  }  
  
   /**
    * This function will search the nearest OpticalCameraRectangle from inspection region.
    * Then, it will use the z-height from the nearest OpticalCameraRectangle.
    * 
   * @author Cheah Lee Herng
   */
  private int getZHeightInNanometer(ReconstructionRegion inspectionRegion, OpticalRegion opticalRegion)
  {
    Assert.expect(inspectionRegion != null);
    Assert.expect(opticalRegion != null);    
    
    double zHeightInNanometer = 0;
    Project project = _testProgram.getProject();
    Panel panel = project.getPanel();
    PanelSettings panelSettings = panel.getPanelSettings();
    boolean isPanelBasedAlignment = panelSettings.isPanelBasedAlignment();
    List<OpticalCameraRectangle> opticalCameraRectangleList;
    if (isPanelBasedAlignment)
      opticalCameraRectangleList = opticalRegion.getAllOpticalCameraRectangles();
    else
      opticalCameraRectangleList = opticalRegion.getOpticalCameraRectangles(_board);
    
    // Initialize variables
    boolean isFirst = true;
    double best = 0.0;
    int bestIndex = -1;
        
    for(int i=0; i < opticalCameraRectangleList.size(); i++)
    {
      OpticalCameraRectangle opticalCameraRectangle = opticalCameraRectangleList.get(i);
      
      if (opticalCameraRectangle.isZHeightValid() || isFirst)
      {
        double distance = Math.sqrt(getDistanceSquare(inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters(), 
                                                      opticalCameraRectangle.getRegion()));
        
        if ( (isFirst) || (distance < best) )
        {
          isFirst = false;
          bestIndex = i;
          best = distance;
        }
      }
      
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
      {
        System.out.println("Process OpticalCameraRectangle " + opticalCameraRectangle.getName() + 
                ": Current Z-Height (nm) = " + opticalCameraRectangle.getZHeightInNanometer() +
                ": Z-Height Valid = " + opticalCameraRectangle.isZHeightValid());
      }
    }
    
    // Get the nearest OpticalCameraRectangle z-height
    OpticalCameraRectangle opticalCameraRectangleFound = opticalCameraRectangleList.get(bestIndex);
    if (opticalCameraRectangleFound.isZHeightValid())
    {
      zHeightInNanometer = opticalCameraRectangleFound.getZHeightInNanometer();
      
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
      {
        System.out.println("Process opticalRegion " + opticalRegion.getName() + 
                ": OpticalCameraRectangle " + opticalCameraRectangleFound.getName() + 
                ": Final Z-Height (nm) = " + zHeightInNanometer);
      }
    }
    else
    {
      zHeightInNanometer = 0.0;
      
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
      {
        System.out.println("Process opticalRegion " + opticalRegion.getName() + 
                ": OpticalCameraRectangle " + opticalCameraRectangleFound.getName() + 
                ": Invalid Z-Height");
      }
    }
    return (int)zHeightInNanometer;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public List<Fiducial> getFiducialsForTestSubProgram(TestSubProgram testSubProgram)
  {
    Panel panel = _testProgram.getProject().getPanel();
    // XCR-2870 IRP encounter IRP_RECONSTRUCTION_REGION_NOT_IMAGE_KEY error due to fiducial points
    List<Fiducial> fiducials = new ArrayList<Fiducial>();
    
    if (testSubProgram.hasBoard())
      for (Board board : panel.getBoards())
      {
        if(testSubProgram.getBoard().equals(board))
          fiducials.addAll(board.getFiducials());
      }
    return fiducials;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private long getDistanceSquare(PanelRectangle point1PanelRectangle,
                                 PanelRectangle point2PanelRectangle)
  {
    Assert.expect(point1PanelRectangle != null);
    Assert.expect(point2PanelRectangle != null);
    
    long deltaX = point1PanelRectangle.getCenterX() - point2PanelRectangle.getCenterX();
    long deltaY = point1PanelRectangle.getCenterY() - point2PanelRectangle.getCenterY();

    return (deltaX * deltaX) + (deltaY * deltaY);
  }
  
  /**
   * Added in new checking condition to prevent alignmentGroup being assigned by alignment region
   * @author sheng chuan
   * @return 
   */
  public boolean isSubProgramRequiredCustomAlignmentPathGeneration()
  {
    boolean useCustomizedSetting = false;
    
    for (AlignmentGroup ag : getAlignmentGroups())
    {
      if (ag.useCustomizeAlignmentSetting() || 
          VirtualLiveManager.getInstance().isVirtualLiveMode())
      {
        useCustomizedSetting = true;
        break;
      }
    }
    return useCustomizedSetting;
  }
  
  
  /**
   * get alignment user gain setting
   * XCR-3433, alignment fail in virtual live if recipe alignment setting changed from customized to non-customized
   * @author Yong Sheng Chuan
   */
  public UserGainEnum getAlignmentUserGain()
  {
    UserGainEnum alignmentUserGain = null;
    for (AlignmentGroup ag : getAlignmentGroups())
    {
      if (ag.useCustomizeAlignmentSetting())
      {
        alignmentUserGain = ag.getUserGainEnum();
        break;
      }
    }
    
    //if the alignment group is customized, use the customized, else use the user gain in subprogram.
    if(alignmentUserGain == null)
      alignmentUserGain = _userGain;
    
    return alignmentUserGain;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public StageSpeedEnum getAlignmentStageSpeed()
  {
    StageSpeedEnum alignmentStageSpeed = null;
    for (AlignmentGroup ag : getAlignmentGroups())
    {
      if (ag.useCustomizeAlignmentSetting())
      {
        alignmentStageSpeed = ag.getStageSpeedEnum();
        break;
      }
    }
    
    //if the alignment group is customized, use the customized, else use the user gain in subprogram.
    if(alignmentStageSpeed == null)
      alignmentStageSpeed = _stageSpeed;
    
    return alignmentStageSpeed;
  }
  
  /**
   * @author Kok Chun, Tan
   * this flag is true when this test sub program is done inspected or x-out
   * only apply in production mode
   */
  public boolean isTestSubProgramDone()
  {
    return _isTestSubProgramDone;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setIsTestSubProgramDone(boolean isDone)
  {
    _isTestSubProgramDone = isDone;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public String generateSurfaceMapInfo(Component component,
                                       ReconstructionRegion region,
                                       FocusGroup focusGroup,
                                       SliceNameEnum sliceNameEnum,
                                       int zHeightInNanometers,
                                       boolean isEnabledPspLocalSearch) throws DatastoreException
  {
    Assert.expect(component != null);
    Assert.expect(region != null);
    Assert.expect(focusGroup != null);
    Assert.expect(sliceNameEnum != null);
    
    String logMessage = component.getBoardNameAndReferenceDesignator() + "," + (component.isTopSide() ? "TOP" : "BOT") + "," + 
                        region.getRegionId() + "," + focusGroup.getId() + "," + focusGroup.getSearchRangeLowLimitInNanometers() + "," +
                        focusGroup.getSearchRangeHighLimitInNanometers() + "," + focusGroup.getPspZOffsetInNanometers() + "," + 
                        sliceNameEnum.getId() + "," + sliceNameEnum.getName() + "," + zHeightInNanometers + "," + isEnabledPspLocalSearch;
    
    return logMessage;
  }

  /**
   * @author Kok Chun, Tan
   */
  public List<List<StageSpeedEnum>> getAllUsedStageSpeedSettings()
  {
    List<List<StageSpeedEnum>> listOfAllSpeedCombination = new ArrayList<List<StageSpeedEnum>>();
    for (ReconstructionRegion region : getAllInspectionRegions())
    {
      if (listOfAllSpeedCombination.contains(region.getStageSpeedList()) == false)
        listOfAllSpeedCombination.add(region.getStageSpeedList());
    }

    return listOfAllSpeedCombination;
  }
}
