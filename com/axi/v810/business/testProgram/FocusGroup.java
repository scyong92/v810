package com.axi.v810.business.testProgram;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.config.*;

/**
 * <p>Title: </p> FocusProfileGroup
 *
 * <p>Description: </p> This class maintains information needed to
 *    build a focus profile group to be used with a given set of focus
 *    regions on a given set of slices.
 *
 * @author Michael Tutkowski
 */
public class FocusGroup implements Serializable
{
  private static int _numFocusGroups = 0;

  private int _id;
  private List<Slice> _slices = new LinkedList<Slice>();
  private FocusSearchParameters _focusSearchParameters;
  private transient List<ReconstructionRegion> _neighbors;
  private int _relativeZoffsetFromSurfaceInNanoMeters = 0;
  private boolean _singleSidedRegionOfBoard = false;
  private int _autoFocusMidBoardOffsetInNanometers = 0;
  private boolean _isUsePhaseShiftProfiliometry = false;
  private int _searchRangeLowLimitInNanometers = 0;
  private int _searchRangeHighLimitInNanometers = 0;
  private int _pspZOffsetInNanometers = 0;
  private boolean _saveSharpnessProfile = false;
  //Khaw Chek Hau - XCR3554: Package on package (PoP) development
  private int _popTotalLayerNumber = 1;
  private POPLayerIdEnum _popLayerId = POPLayerIdEnum.BASE;
  private int _popZHeightInNanometers = 0;

  /**
   * @author Michael Tutkowski
   */
  public FocusGroup()
  {
    _id = _numFocusGroups++;
  }

  /**
   * @author Roy Williams
   */
  public int getId()
  {
    return _id;
  }

  /**
   * @author George A. David
   */
  public void setId(int id)
  {
    Assert.expect(id >= 0);

    _id = id;
  }

  /**
   * @author George A. David
   */
  public int getNumberOfSlices()
  {
    Assert.expect(_slices != null);

    return _slices.size();
  }

  /**
   * @author Dave Ferguson
   */
  public int getNumberOfReconstructableSlices()
  {
    Assert.expect(_slices != null);

    int count = 0;

    for (Slice slice : _slices)
    {
      if (slice.getSliceName().equals(SliceNameEnum.SURFACE_MODELING) == false &&
          slice.getSliceType().equals(SliceTypeEnum.SURFACE) == false)
      {
        count++;
      }
    }

    return count;
  }

  /**
   * Set whether the focus region geometry intersects parts on the other
   * side of the board.
   * True = no intersection
   * Indicates if the focus region
   * @author Dave Ferguson
   */
  public void setSingleSidedRegionOfBoard(boolean singleSidedRegionOfBoard)
  {
    _singleSidedRegionOfBoard = singleSidedRegionOfBoard;
  }

  /**
   * @author Dave Ferguson
   */
  public boolean isSingleSidedRegionOfBoard()
  {
    return _singleSidedRegionOfBoard;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public void setSaveSharpnessProfile(boolean saveSharpnessProfile)
  {
    _saveSharpnessProfile = saveSharpnessProfile;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public boolean isSaveSharpnessProfile()
  {
    //return _saveSharpnessProfile;
    return Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_IMAGE_WITH_SHARPNESS_INFO);
  }

  /**
   * @author George A. David
   */
  public void setFocusSearchParameters(FocusSearchParameters focusSearchParameters)
  {
    Assert.expect(focusSearchParameters != null);

    _focusSearchParameters = focusSearchParameters;
  }

  /**
   * @author George A. David
   */
  public FocusSearchParameters getFocusSearchParameters()
  {
    Assert.expect(_focusSearchParameters != null);

    return _focusSearchParameters;
  }

  /**
   * @author George A. David
   */
  public void addSlice(Slice slice)
  {
    Assert.expect(slice != null);

    _slices.add(slice);
  }

  /**
   * @author Yong Sheng Chuan
   */
  public void removeSlice(Slice slice)
  {
    Assert.expect(slice != null);

    if (_slices.contains(slice))
      _slices.remove(slice);
  }
  
  /**
   * @author George A. David
   */
  public List<Slice> getSlices()
  {
    Assert.expect(_slices != null);

    return _slices;
  }

  /**
   * @author George A. David
   */
  public void setRelativeZoffsetFromSurfaceInNanoMeters(int relativeZoffsetFromSurfaceInNanoMeters)
  {
    Assert.expect(relativeZoffsetFromSurfaceInNanoMeters >= 0);

    _relativeZoffsetFromSurfaceInNanoMeters = relativeZoffsetFromSurfaceInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getRelativeZoffsetFromSurfaceInNanoMeters()
  {
    return _relativeZoffsetFromSurfaceInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public boolean hasNeighbors()
  {
    if(_neighbors != null && _neighbors.isEmpty() == false)
    {
      return true;
    }

    return false;
  }

  /**
   * @author George A. David
   */
  public List<ReconstructionRegion> getNeighbors()
  {
    Assert.expect(_neighbors != null);

    return _neighbors;
  }

  /**
   * @author George A. David
   */
  public void addNeighbor(ReconstructionRegion neighbor)
  {
    Assert.expect(neighbor != null);

    if(_neighbors == null)
      _neighbors = new LinkedList<ReconstructionRegion>();

    _neighbors.add(neighbor);
  }

  /**
   * @author George A. David
   */
  public void addNeighbors(Collection<ReconstructionRegion> neighbors)
  {
    Assert.expect(neighbors != null);

    if(_neighbors == null)
      _neighbors = new LinkedList<ReconstructionRegion>();

    _neighbors.addAll(neighbors);
  }

  /**
   * @author Roy Williams
   */
  public boolean hasSlicesToCreate()
  {
    Assert.expect(_slices != null);

    for (Slice slice : _slices)
    {
      if (slice.createSlice())
        return true;
    }
    return false;
  }

  /**
   * @author George A. David
   */
  public boolean hasSurfaceModelingSlice()
  {
    return hasSlice(SliceNameEnum.SURFACE_MODELING);
  }

  /**
   * @author George A. David
   */
  public boolean hasSlice(SliceNameEnum sliceName)
  {
    Assert.expect(sliceName != null);

    for(Slice slice : _slices)
    {
      if(slice.getSliceName().equals(sliceName))
        return true;
    }

    return false;
  }

  /**
   * @author George A. David
   */
  public Slice getSurfaceModelingSlice()
  {
    return getSlice(SliceNameEnum.SURFACE_MODELING);
  }

  /**
   * @author George A. David
   */
  public Slice getSlice(SliceNameEnum sliceName)
  {
    Assert.expect(sliceName != null);
    Slice aSlice = null;

    for(Slice slice : _slices)
    {
      if(slice.getSliceName().equals(sliceName))
      {
        aSlice = slice;
        break;
      }
    }

    Assert.expect(aSlice != null);
    return aSlice;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setAutoFocusMidBoardOffsetInNanometers(int autoFocusMidBoardOffsetInNanometers)
  {
      _autoFocusMidBoardOffsetInNanometers = autoFocusMidBoardOffsetInNanometers;
  }

  /**
   * @author Cheah Lee Herng
   */
  public int getAutoFocusMidBoardOffsetInNanometers()
  {
      return _autoFocusMidBoardOffsetInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {
    if (_slices != null)
      _slices.clear();

    if (_neighbors != null) 
      _neighbors.clear(); 
  }
  
  // XCR-2851 - Ee Jun Jiang - VirtualLiveMemoryLeak
  public void clearNeighbors()
  {
    if(_neighbors!=null)
    {
      _neighbors.clear();
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setSearchRangeLowLimitInNanometers(int searchRangeLowLimitInNanometers)
  {
    _searchRangeLowLimitInNanometers = searchRangeLowLimitInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getSearchRangeLowLimitInNanometers()
  {
    return _searchRangeLowLimitInNanometers;
  }    
  
  /**
   * @author Cheah Lee Herng
   */
  public void setSearchRangeHighLimitInNanometers(int searchRangeHighLimitInNanometers)
  {
    _searchRangeHighLimitInNanometers = searchRangeHighLimitInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getSearchRangeHighLimitInNanometers()
  {
    return _searchRangeHighLimitInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setUsePhaseShiftProfiliometry(boolean isUsePhaseShiftProfiliometry)
  {
    _isUsePhaseShiftProfiliometry = isUsePhaseShiftProfiliometry;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isUsePhaseShiftProfiliometry()
  {
    return _isUsePhaseShiftProfiliometry;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void setPspZOffsetInNanometers(int pspZOffsetInNanometers)
  {
    _pspZOffsetInNanometers = pspZOffsetInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getPspZOffsetInNanometers()
  {
    return _pspZOffsetInNanometers;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void setPOPLayerNumber(POPLayerIdEnum popLayerId)
  {
    _popLayerId = popLayerId;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public POPLayerIdEnum getPOPLayerId()
  {
    return _popLayerId;
  }    
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void setPOPTotalLayerNumber(int popTotalLayerNumber)
  {
    _popTotalLayerNumber = popTotalLayerNumber;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public int getPOPTotalLayerNumber()
  {
    return _popTotalLayerNumber;
  }    
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void setPOPZHeightInNanometers(int popZHeightInNanometers)
  {
    _popZHeightInNanometers = popZHeightInNanometers;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public int getPOPZHeightInNanometers()
  {
    return _popZHeightInNanometers;
  }

  /**
   * @author Yong Sheng Chuan
   */
  public FocusGroup clone()
  {
    FocusGroup fg = new FocusGroup();
    fg.setAutoFocusMidBoardOffsetInNanometers(getAutoFocusMidBoardOffsetInNanometers());
    fg.setFocusSearchParameters(getFocusSearchParameters());
    fg.setRelativeZoffsetFromSurfaceInNanoMeters(getRelativeZoffsetFromSurfaceInNanoMeters());
    fg.setSaveSharpnessProfile(isSaveSharpnessProfile());
    fg.setSearchRangeHighLimitInNanometers(getSearchRangeHighLimitInNanometers());
    fg.setSearchRangeLowLimitInNanometers(getSearchRangeLowLimitInNanometers());
    fg.setSingleSidedRegionOfBoard(isSingleSidedRegionOfBoard());
    fg.setUsePhaseShiftProfiliometry(isUsePhaseShiftProfiliometry());
    for (Slice slice : getSlices())
      fg.addSlice(slice);
    
    fg.addNeighbors(getNeighbors());
    return fg;
  }
}
