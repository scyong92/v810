package com.axi.v810.business.testProgram;

import java.io.*;

import com.axi.v810.hardware.*;
import com.axi.util.*;

/**
 * This class contains all the variables used to
 * generate a program. This is necessary so we can later
 * check to see if the program is compatible with the
 * current hardware. If any of these have changed,
 * a program will need to be regenerated.
 *
 * @author George A. David
 */
public class TestProgramGenerationData implements Serializable
{
  private int _numberOfAvailableIrps = -1;
  private int _numberOfAvailablePhysicalIrps = -1;

  private int _maxInspectionRegionWidthInNanoMeters = -1;
  private int _maxInspectionRegionLengthInNanoMeters = -1;
  private int _maxInspectionRegionSizeDueToWarpInNanoMeters = -1;

  private int _maxVerificationRegionWidthInNanoMeters = -1;
  private int _maxVerificationRegionLengthInNanoMeters = -1;

  private int _maxAlignmentRegionWidthInNanoMeters = -1;
  private int _maxAlignmentRegionLengthInNanoMeters = -1;
  private int _alignmentFeatureLengthBufferInNanoMeters = -1;
  private int _alignmentUncertaintyInNanoMeters = -1;
  private int _alignmentUncertaintyAfterAutomaticAlignmentInNanoMeters = -1;

  private int _maxFocusRegionSizeInNanoMeters = -1;
  private int _minFocusRegionSizeInNanoMeters = -1;

  private int _maxInspectablePanelWidthInNanoMeters = -1;
  private int _maxImageableAreaLengthInNanoMeters = -1;

  private boolean _scanPathOptimization = true;
  private boolean _focusOptimization = false;
  
  private int _systemInspectionType = 1;
  
  private boolean _pspEnable = false;

  private SystemTypeEnum _systemType;
  
  //Siew Yeng - XCR1745 - Semi-automated mode
  private boolean _isSemiAutomatedMode = false; 
  private boolean _isVariableMagnification = false; 
  
  private String _lowMagnification = "";
  private String _highMagnification = "";

  /**
   * @author George A. David
   */
  public void setNumberOfAvailableIrps(int numIrps)
  {
    Assert.expect(numIrps > 0);

    _numberOfAvailableIrps = numIrps;
  }

  /**
   * @author George A. David
   */
  public int getNumberOfAvailabeIrps()
  {
    Assert.expect(_numberOfAvailableIrps > 0);

    return _numberOfAvailableIrps;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setNumberOfAvailablePhysicalIrps(int numPhysicalIrps)
  {
    Assert.expect(numPhysicalIrps > 0);

    _numberOfAvailablePhysicalIrps = numPhysicalIrps;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getNumberOfAvailablePhysicalIrps()
  {
    Assert.expect(_numberOfAvailablePhysicalIrps > 0);

    return _numberOfAvailablePhysicalIrps;
  }

  /**
   * @author George A. David
   */
  public void setMaxInspectionRegionLengthInNanoMeters(int maxInspectionRegionLengthInNanoMeters)
  {
    Assert.expect(maxInspectionRegionLengthInNanoMeters > 0);

    _maxInspectionRegionLengthInNanoMeters = maxInspectionRegionLengthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getMaxInspectionRegionLengthInNanoMeters()
  {
    Assert.expect(_maxInspectionRegionLengthInNanoMeters > 0);

    return _maxInspectionRegionLengthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public void setMaxInspectionRegionWidthInNanoMeters(int maxInspectionRegionWidthInNanoMeters)
  {
    Assert.expect(maxInspectionRegionWidthInNanoMeters > 0);

    _maxInspectionRegionWidthInNanoMeters = maxInspectionRegionWidthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getMaxInspectionRegionWidthInNanoMeters()
  {
    Assert.expect(_maxInspectionRegionWidthInNanoMeters > 0);

    return _maxInspectionRegionWidthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public void setMaxInspectionRegionSizeDueToWarpInNanoMeters(int maxInspectionRegionSizeDueToWarpInNanoMeters)
  {
    Assert.expect(maxInspectionRegionSizeDueToWarpInNanoMeters > 0);

    _maxInspectionRegionSizeDueToWarpInNanoMeters = maxInspectionRegionSizeDueToWarpInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getMaxInspectionRegionSizeDueToWarpInNanoMeters()
  {
    Assert.expect(_maxInspectionRegionSizeDueToWarpInNanoMeters > 0);

    return _maxInspectionRegionSizeDueToWarpInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public void setMaxVerificationRegionWidthInNanoMeters(int maxVerificationRegionWidthInNanoMeters)
  {
    Assert.expect(maxVerificationRegionWidthInNanoMeters > 0);

    _maxVerificationRegionWidthInNanoMeters = maxVerificationRegionWidthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getMaxVerificationRegionWidthInNanoMeters()
  {
    Assert.expect(_maxVerificationRegionWidthInNanoMeters > 0);

    return _maxVerificationRegionWidthInNanoMeters;
  }


  /**
   * @author George A. David
   */
  public void setMaxVerificationRegionLengthInNanoMeters(int maxVerificationRegionLengthInNanoMeters)
  {
    Assert.expect(maxVerificationRegionLengthInNanoMeters > 0);

    _maxVerificationRegionLengthInNanoMeters = maxVerificationRegionLengthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getMaxVerificationRegionLengthInNanoMeters()
  {
    Assert.expect(_maxVerificationRegionLengthInNanoMeters > 0);

    return _maxVerificationRegionLengthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public void setMaxAlignmentRegionWidthInNanoMeters(int maxAlignmentRegionWidthInNanoMeters)
  {
    Assert.expect(maxAlignmentRegionWidthInNanoMeters > 0);

    _maxAlignmentRegionWidthInNanoMeters = maxAlignmentRegionWidthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getMaxAlignmentRegionWidthInNanoMeters()
  {
    Assert.expect(_maxAlignmentRegionWidthInNanoMeters > 0);

    return _maxAlignmentRegionWidthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public void setMaxAlignmentRegionLengthInNanoMeters(int maxAlignmentRegionLengthInNanoMeters)
  {
    Assert.expect(maxAlignmentRegionLengthInNanoMeters > 0);

    _maxAlignmentRegionLengthInNanoMeters = maxAlignmentRegionLengthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getMaxAlignmentRegionLengthInNanoMeters()
  {
    Assert.expect(_maxAlignmentRegionLengthInNanoMeters > 0);

    return _maxAlignmentRegionLengthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public void setAlignmentFeatureLengthBufferInNanoMeters(int alignmentFeatureLengthBufferInNanoMeters)
  {
    Assert.expect(alignmentFeatureLengthBufferInNanoMeters > 0);

    _alignmentFeatureLengthBufferInNanoMeters = alignmentFeatureLengthBufferInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getAlignmentFeatureLengthBufferInNanoMeters()
  {
    Assert.expect(_alignmentFeatureLengthBufferInNanoMeters > 0);

    return _alignmentFeatureLengthBufferInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public void setAlignmentUncertaintyInNanoMeters(int alignmentUncertaintyInNanoMeters)
  {
    Assert.expect(alignmentUncertaintyInNanoMeters > 0);

    _alignmentUncertaintyInNanoMeters = alignmentUncertaintyInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getAlignmentUncertaintyInNanoMeters()
  {
    Assert.expect(_alignmentUncertaintyInNanoMeters > 0);

    return _alignmentUncertaintyInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public void setAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters(int alignmentUncertaintyAfterAutomaticAlignmentInNanoMeters)
  {
    Assert.expect(alignmentUncertaintyAfterAutomaticAlignmentInNanoMeters > 0);

    _alignmentUncertaintyAfterAutomaticAlignmentInNanoMeters = alignmentUncertaintyAfterAutomaticAlignmentInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters()
  {
    Assert.expect(_alignmentUncertaintyAfterAutomaticAlignmentInNanoMeters > 0);

    return _alignmentUncertaintyAfterAutomaticAlignmentInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public void setMaxFocusRegionSizeInNanoMeters(int maxFocusRegionSizeInNanoMeters)
  {
    Assert.expect(maxFocusRegionSizeInNanoMeters > 0);

    _maxFocusRegionSizeInNanoMeters = maxFocusRegionSizeInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getMaxFocusRegionSizeInNanoMeters()
  {
    Assert.expect(_maxFocusRegionSizeInNanoMeters > 0);

    return _maxFocusRegionSizeInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public void setMinFocusRegionSizeInNanoMeters(int minFocusRegionSizeInNanoMeters)
  {
    Assert.expect(minFocusRegionSizeInNanoMeters > 0);

    _minFocusRegionSizeInNanoMeters = minFocusRegionSizeInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getMinFocusRegionSizeInNanoMeters()
  {
    Assert.expect(_minFocusRegionSizeInNanoMeters > 0);

    return _minFocusRegionSizeInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public void setMaxInspectablePanelWidthInNanoMeters(int maxInspectablePanelWidthInNanoMeters)
  {
    Assert.expect(maxInspectablePanelWidthInNanoMeters > 0);

    _maxInspectablePanelWidthInNanoMeters = maxInspectablePanelWidthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getMaxInspectablePanelWidthInNanoMeters()
  {
    Assert.expect(_maxInspectablePanelWidthInNanoMeters > 0);

    return _maxInspectablePanelWidthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public void setMaxImageableAreaLengthInNanoMeters(int maxImageableAreaLengthInNanoMeters)
  {
    Assert.expect(maxImageableAreaLengthInNanoMeters > 0);

    _maxImageableAreaLengthInNanoMeters = maxImageableAreaLengthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getMaxImageableAreaLengthInNanoMeters()
  {
    Assert.expect(_maxImageableAreaLengthInNanoMeters > 0);

    return _maxImageableAreaLengthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public void setSystemType(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);

    _systemType = systemType;
  }

  /**
   * @author George A. David
   */
  public SystemTypeEnum getSystemType()
  {
    Assert.expect(_systemType != null);

    return _systemType;
  }

  /**
   * @author George A. David
   */
  public int hashCode()
  {
    int hashcode = 1;
    hashcode = hashcode * 31 + _alignmentFeatureLengthBufferInNanoMeters;
    hashcode = hashcode * 31 + _alignmentUncertaintyAfterAutomaticAlignmentInNanoMeters;
    hashcode = hashcode * 31 + _alignmentUncertaintyInNanoMeters;
    hashcode = hashcode * 31 + _maxAlignmentRegionLengthInNanoMeters;
    hashcode = hashcode * 31 + _maxAlignmentRegionWidthInNanoMeters;
    hashcode = hashcode * 31 + _maxFocusRegionSizeInNanoMeters;
    hashcode = hashcode * 31 + _maxImageableAreaLengthInNanoMeters;
    hashcode = hashcode * 31 + _maxInspectablePanelWidthInNanoMeters;
    hashcode = hashcode * 31 + _maxInspectionRegionLengthInNanoMeters;
    hashcode = hashcode * 31 + _maxInspectionRegionSizeDueToWarpInNanoMeters;
    hashcode = hashcode * 31 + _maxInspectionRegionWidthInNanoMeters;
    hashcode = hashcode * 31 + _maxVerificationRegionLengthInNanoMeters;
    hashcode = hashcode * 31 + _maxVerificationRegionWidthInNanoMeters;
    hashcode = hashcode * 31 + _minFocusRegionSizeInNanoMeters;
    hashcode = hashcode * 31 + _numberOfAvailableIrps;
    hashcode = hashcode * 31 + _systemType.hashCode();
    hashcode = hashcode * 31 + _systemInspectionType;

    return hashcode;
  }

  /**
   * @author George A. David
   */
  public boolean equals(Object object)
  {
    if(this == object)
      return true;
    if((object instanceof TestProgramGenerationData) == false)
      return false;

    TestProgramGenerationData currentData = this;
    TestProgramGenerationData programData = (TestProgramGenerationData)object;
    if(currentData.getAlignmentFeatureLengthBufferInNanoMeters() == programData.getAlignmentFeatureLengthBufferInNanoMeters() &&
       currentData.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters() == programData.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters() &&
       currentData.getAlignmentUncertaintyInNanoMeters() == programData.getAlignmentUncertaintyInNanoMeters() &&
       currentData.getMaxAlignmentRegionLengthInNanoMeters() == programData.getMaxAlignmentRegionLengthInNanoMeters() &&
       currentData.getMaxAlignmentRegionWidthInNanoMeters() == programData.getMaxAlignmentRegionWidthInNanoMeters() &&
       currentData.getMaxFocusRegionSizeInNanoMeters() == programData.getMaxFocusRegionSizeInNanoMeters() &&
       currentData.getMaxImageableAreaLengthInNanoMeters() == programData.getMaxImageableAreaLengthInNanoMeters() &&
       currentData.getMaxInspectablePanelWidthInNanoMeters() == programData.getMaxInspectablePanelWidthInNanoMeters() &&
       currentData.getMaxInspectionRegionLengthInNanoMeters() == programData.getMaxInspectionRegionLengthInNanoMeters() &&
       currentData.getMaxInspectionRegionSizeDueToWarpInNanoMeters() == programData.getMaxInspectionRegionSizeDueToWarpInNanoMeters() &&
       currentData.getMaxInspectionRegionWidthInNanoMeters() == programData.getMaxInspectionRegionWidthInNanoMeters() &&
       currentData.getMaxVerificationRegionLengthInNanoMeters() == programData.getMaxVerificationRegionLengthInNanoMeters() &&
       currentData.getMaxVerificationRegionWidthInNanoMeters() == programData.getMaxVerificationRegionWidthInNanoMeters() &&
       currentData.getMinFocusRegionSizeInNanoMeters() == programData.getMinFocusRegionSizeInNanoMeters() &&
       currentData.getNumberOfAvailabeIrps() == programData.getNumberOfAvailabeIrps() &&
       currentData.getSystemType().equals(programData.getSystemType()) && 
       currentData.getSystemInspectionType()  == programData.getSystemInspectionType() &&
       currentData.isPspEnable() == programData.isPspEnable() &&
       currentData.isSemiAutomatedModeEnabled() == programData.isSemiAutomatedModeEnabled() &&
       currentData.isVariableMagnificationEnabled() == programData.isVariableMagnificationEnabled() &&
       currentData.getLowMagnification().equalsIgnoreCase(programData.getLowMagnification()) &&
       currentData.getHighMagnification().equalsIgnoreCase(programData.getHighMagnification()))//punit.2889
    {
      return true;
    }

    return false;
  }

  /**
   * @return the _scanPathOptimization
   * @author Wei Chin
   */
  public boolean isScanPathOptimization()
  {
    return _scanPathOptimization;
  }

  /**
   * @param scanPathOptimization the _scanPathOptimization to set
   * @author Wei Chin
   */
  public void setScanPathOptimization(boolean scanPathOptimization)
  {
    this._scanPathOptimization = scanPathOptimization;
  }

  //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on"
  public boolean isFocusOptimization()
  {
      return _focusOptimization;
  }

  //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on"  
  public void setFocusOptimization(boolean focusOptimization)
  {
    this._focusOptimization = focusOptimization;
  }

  /**
   * @return the _systemInspectionType
   * @author Wei Chin
   */
  public int getSystemInspectionType()
  {
    return _systemInspectionType;
  }

  /**
   * @param systemInspectionType the _systemInspectionType to set
   * @author Wei Chin
   */
  public void setSystemInspectionType(int systemInspectionType)
  {
    _systemInspectionType = systemInspectionType;
  }
  
   /**
   * @author Jack Hwee
   */
  public boolean isPspEnable()
  {
      return _pspEnable;
  }

  /**
   * @author Jack Hwee
   */
  public void setPspEnable(boolean pspEnable)
  {
    this._pspEnable = pspEnable;
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean isSemiAutomatedModeEnabled()
  {
    return _isSemiAutomatedMode;
  }

  /**
   * @author Siew Yeng
   */
  public void setSemiAutomatedMode(boolean semiAuto)
  {
    _isSemiAutomatedMode = semiAuto;
  }
  
  /**
   * XCR-3513, Only low magnification alignment is running although VM is enable
   * @author Yong Sheng Chuan
   */
  public boolean isVariableMagnificationEnabled()
  {
    return _isVariableMagnification;
  }

  /**
   * XCR-3513, Only low magnification alignment is running although VM is enable
   * @author Yong Sheng Chuan
   */
  public void setVariableMagnificationMode(boolean isVariableMag)
  {
    _isVariableMagnification = isVariableMag;
  }
  
  /**
   * @author Ngie Xing
   */
  public String getLowMagnification()
  {
    return _lowMagnification;
  }

  /**
   * @author Ngie Xing
   */
  public void setLowMagnification(String lowMag)
  {
    _lowMagnification = lowMag;
  }
  
  /**
   * @author Ngie Xing
   */
  public String getHighMagnification()
  {
    return _highMagnification;
  }

  /**
   * @author Ngie Xing
   */
  public void setHighMagnification(String highMag)
  {
    _highMagnification = highMag;
  }
}
