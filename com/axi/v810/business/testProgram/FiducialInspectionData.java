package com.axi.v810.business.testProgram;

import java.awt.geom.*;
import java.io.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Represents a region-of-interest for a single joint.  See the Program Generation
 * subsystem for the details on how these regions are selected.
 *
 * @author Peter Esbensen
 */
public class FiducialInspectionData implements Feature, Serializable
{
  private PanelRectangle _regionRectangleRelativeToPanelOriginInNanoMeters;
  private transient java.awt.Shape _shapeRelativeToPanelInNanoMeters;
  private transient java.awt.Shape _shapeRelativeToAlignmentRegion;
  private ReconstructionRegion _alignmentRegion;
  private Fiducial _fiducial;

  /**
   * @author Peter Esbensen
   */
  public FiducialInspectionData(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);
    _fiducial = fiducial;
    _shapeRelativeToPanelInNanoMeters = fiducial.getShapeRelativeToPanelInNanoMeters();
  }

  /**
   * @author Peter Esbensen
   */
  public void setAlignmentRegion(ReconstructionRegion alignmentRegion)
  {
    Assert.expect(alignmentRegion != null);
    _alignmentRegion = alignmentRegion;
  }

  /**
   * Return a Shape in image coordinates for this object within its AlignmentRegion.
   *
   * @author Peter Esbensen
   */
  public java.awt.Shape getShapeRelativeToAlignmentRegion()
  {
    if (_shapeRelativeToAlignmentRegion != null)
      return _shapeRelativeToAlignmentRegion;

    Assert.expect(_alignmentRegion != null); // setInspectionRegion must be called before this point

    double regionOffsetInX = _alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinX();
    double regionOffsetInY = _alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMaxY();

    java.awt.geom.AffineTransform transform = java.awt.geom.AffineTransform.getTranslateInstance(regionOffsetInX, regionOffsetInY);
    //transform.scale(nanosPerPixel, nanosPerPixel);

    _shapeRelativeToAlignmentRegion = transform.createTransformedShape(getShapeRelativeToPanelInNanoMeters());
    return _shapeRelativeToAlignmentRegion;
  }

  /**
   * @author Peter Esbensen
   */
  public java.awt.Shape getShapeRelativeToPanelInNanoMeters()
  {
    if (_shapeRelativeToPanelInNanoMeters == null)
    {
      _shapeRelativeToPanelInNanoMeters = _fiducial.getShapeRelativeToPanelInNanoMeters();
    }

    return _shapeRelativeToPanelInNanoMeters;
  }

  /**
   * Get the rectangle enclosing this InspectionRegionPad.  This will return a
   * rectangle with coordinates relative to the panel origin.
   *
   * @author Peter Esbensen
   */
  public PanelRectangle getRegionRectangleRelativeToPanelOriginInNanoMeters()
  {
    if (_regionRectangleRelativeToPanelOriginInNanoMeters == null)
    {

      java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
      java.awt.geom.Rectangle2D topLeftBasedRect2D = shape.getBounds2D();

      // topLeftBasedRect2D is not what we want because this is based on the upper-left corner, but we
      // want a rectangle based on the lower-left corner
      // So return a PanelRectangle after modifying Y coordinate to match this convention:
      _regionRectangleRelativeToPanelOriginInNanoMeters = new PanelRectangle((int)Math.round(topLeftBasedRect2D.getMinX()),
          (int)Math.round(topLeftBasedRect2D.getMinY()),
          (int)Math.round(topLeftBasedRect2D.getWidth()),
          (int)Math.round(topLeftBasedRect2D.getHeight()));
    }

    return _regionRectangleRelativeToPanelOriginInNanoMeters;
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  public RegionOfInterest getRegionOfInterestRelativeToInspectionRegionInPixels()
  {
    // Removed the caching of this return value because it was being initalized when the inspection region was still not
    // completely defined - which meant that the cached value was invalid after the inspection region was finalized.

    // setInspectionRegion must be called before this point
    Assert.expect(_alignmentRegion != null);

    PanelRectangle panelRect = _alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    double regionOffsetInX = panelRect.getMinX();
    double regionOffsetInY = panelRect.getMaxY();

    // Map the ROI from panel coordinates to inspection region relative image coordinates.
    final double NANOMETERS_PER_PIXEL = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
    final double PIXELS_PER_NANOMETER = 1.0 / NANOMETERS_PER_PIXEL;
    AffineTransform panelToImageCoordinateTransform = AffineTransform.getScaleInstance(PIXELS_PER_NANOMETER, -PIXELS_PER_NANOMETER);
    panelToImageCoordinateTransform.translate(-regionOffsetInX, -regionOffsetInY);
    java.awt.Shape transformedShape = panelToImageCoordinateTransform.createTransformedShape(
      getFiducial().getShapeRelativeToPanelInNanoMeters());
    ImageRectangle transformedRect = new ImageRectangle(transformedShape);

    double fiducialOrientation = 0;

    // round down to the nearest multiple of 90 (ROI only allows multiples of 90)
    int cardinalPadOrientation = 90*(int)Math.floor((fiducialOrientation + 0.00001) / 90.0);
    RegionShapeEnum regionShapeEnum = RegionShapeEnum.RECTANGULAR;
    if (_fiducial.getShapeEnum().equals(ShapeEnum.CIRCLE))
      regionShapeEnum = RegionShapeEnum.OBROUND;

    return new RegionOfInterest(transformedRect, cardinalPadOrientation, regionShapeEnum);
  }

  /**
   * @author Peter Esbensen
   */
  public Fiducial getFiducial()
  {
    Assert.expect(_fiducial != null);
    return _fiducial;
  }
}
