package com.axi.v810.business.testProgram;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
* This class describes a focus profile which is created by the auto focus
* algorithm. Rather that storing the actual measured values the class contains
* a fitted parametrization.
* @author Horst Mueller
*/
public class FocusProfile implements Serializable
{

  //The location of the peak of the profile
  private int _locationOfPeakInNanometers;

  //The lower edge of the profile
  private int _locationOfLowerEdgeInNanometers;

  //The upper edge of the profile
  private int _locationOfUpperEdgeInNanometers;

  //Where or not this profile is a good approximation
  private boolean _isGoodProfile = false;

  /**
  * @author Dave Ferguson
  */
  public FocusProfile(int locationOfPeakInNanometers,
                      int locationOfLowerEdgeInNanometers,
                      int locationOfUpperEdgeInNanometers,
                      boolean isGoodProfile)
  {
    _locationOfPeakInNanometers = locationOfPeakInNanometers;
    _locationOfLowerEdgeInNanometers = locationOfLowerEdgeInNanometers;
    _locationOfUpperEdgeInNanometers = locationOfUpperEdgeInNanometers;
    _isGoodProfile = isGoodProfile;
  }

  /**
   * @author Dave Ferguson
   */
  public int getLocationOfPeakInNanometers()
  {
    return _locationOfPeakInNanometers;
  }

  /**
   * @author Dave Ferguson
   */
  public int getLocationOfLowerEdgeInNanometers()
  {
    return _locationOfLowerEdgeInNanometers;
  }

  /**
   * @author Dave Ferguson
   */
  public int getLocationOfUpperEdgeInNanometers()
  {
    return _locationOfUpperEdgeInNanometers;
  }

  /**
   * @author Dave Ferguson
   */
  public boolean isGoodProfile()
  {
    return _isGoodProfile;
  }
}
