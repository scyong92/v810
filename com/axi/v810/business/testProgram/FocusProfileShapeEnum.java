package com.axi.v810.business.testProgram;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * This class contains the focus profile shapes that can be identified by
 * the current auto focus algorithm
 * @author Horst Mueller
 * @author Rick Gaudette
 */
public class FocusProfileShapeEnum extends com.axi.util.Enum implements Serializable
{
  private String _description = null;
  private static int _index = -1;
  private static Map<String, FocusProfileShapeEnum> _descriptionToFocusProfileShapeEnumMap = new HashMap<String, FocusProfileShapeEnum>();
  private static List<FocusProfileShapeEnum> _focusProfileShapeEnums = new ArrayList<FocusProfileShapeEnum>();

  public static final FocusProfileShapeEnum GAUSSIAN = new FocusProfileShapeEnum("Gaussian", ++_index);
  public static final FocusProfileShapeEnum SQUARE_WAVE = new FocusProfileShapeEnum("Square wave", ++_index);
  public static final FocusProfileShapeEnum PEAK = new FocusProfileShapeEnum("Peak", ++_index);
  public static final FocusProfileShapeEnum EDGE = new FocusProfileShapeEnum("Edge", ++_index);
  public static final FocusProfileShapeEnum NO_SEARCH_REFERENCE_PLANE = new FocusProfileShapeEnum("No Search Reference Plane", ++_index);
  public static final FocusProfileShapeEnum NO_SEARCH_PREDICTED_SURFACE = new FocusProfileShapeEnum("No Search Predicted Surface", ++_index);
  public static final FocusProfileShapeEnum GAUSSIAN_EDGE = new FocusProfileShapeEnum("Gaussian/Edge", ++_index);
  /**
  * @author Horst Mueller
  */
  private FocusProfileShapeEnum(String description, int id)
  {
    super(id);

    Assert.expect(description != null);
    Assert.expect(_descriptionToFocusProfileShapeEnumMap != null);

    _description = description;

    _descriptionToFocusProfileShapeEnumMap.put(_description.toLowerCase(), this);
    _focusProfileShapeEnums.add(this);
  }

  /**
   * @author Horst Mueller
   */
  public String getDescription()
  {
    Assert.expect(_description != null);

    return _description;
  }

  /**
   * @author Horst Mueller
   */
  public String toString()
  {
    return getDescription();
  }

  /**
   * @author Horst Mueller
   */
  public static FocusProfileShapeEnum getFocusProfileShapeEnum(String description)
  {
    Assert.expect(description != null);
    Assert.expect(_descriptionToFocusProfileShapeEnumMap != null);

    FocusProfileShapeEnum FocusProfileShapeEnum = (FocusProfileShapeEnum)_descriptionToFocusProfileShapeEnumMap.get(description.toLowerCase());

    Assert.expect(FocusProfileShapeEnum != null);

    return FocusProfileShapeEnum;
  }

  /**
   * @author Horst Mueller
   */
  public static FocusProfileShapeEnum getFocusProfileShapeEnum(int id)
  {
    Assert.expect(id > -1);
    Assert.expect(id < _focusProfileShapeEnums.size());

    return (FocusProfileShapeEnum)_focusProfileShapeEnums.get(id);

  }

}
