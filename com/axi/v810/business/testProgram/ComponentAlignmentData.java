package com.axi.v810.business.testProgram;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Matt Wharton
 */
public class ComponentAlignmentData extends ComponentInspectionData
{
  private TestSubProgram _testSubProgram;

  /**
   * @author Matt Wharton
   */
  public ComponentAlignmentData(Project project, TestSubProgram testSubProgram)
  {
    super(project);

    Assert.expect(testSubProgram != null);
    _testSubProgram = testSubProgram;
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public Set<JointInspectionData> getInspectableJointInspectionDataSet()
  {
    Set<JointInspectionData> jointInspectionDataSet = new HashSet<JointInspectionData>();

    for (Pad pad : _component.getPads())
    {
      if (pad.isInspected())
      {
        if (pad.isInspected())
          jointInspectionDataSet.add(_testSubProgram.getJointAlignmentData(pad));
      }
    }
    return jointInspectionDataSet;
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public Set<JointInspectionData> getJointInspectionDataSet()
  {
    Set<JointInspectionData> jointInspectionDataSet = new HashSet<JointInspectionData>();

    for (Pad pad : _component.getPads())
    {
      jointInspectionDataSet.add(_testSubProgram.getJointAlignmentData(pad));
    }
    return jointInspectionDataSet;
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public JointInspectionData getPadOneJointInspectionData()
  {
    Pad padOne = _component.getPadOne();
    return _testSubProgram.getJointAlignmentData(padOne);
  }

  /**
   * Do not call this on anything that is not a two pin device.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public JointInspectionData getPadTwoJointInspectionDataOnTwoPinDevices()
  {
    Assert.expect(_component.getPads().size() == 2);
    JointInspectionData padTwoJointInspectionData = null;
    Pad padOne = _component.getPadOne();
    for (Pad pad : _component.getPads())
    {
      if (pad != padOne)
        padTwoJointInspectionData = _testSubProgram.getJointAlignmentData(pad);
    }
    return padTwoJointInspectionData;
  }

  /**
   * Get the region of interest for the component relative to the rotated
   * inspection region.  This will ONLY work for components that are contained
   * all in one region.
   *
   * Modify by Wei Chin 21-11-2013
   * - Handle alignment region won't affect by resize feature 
   * 
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public RegionOfInterest getOrthogonalComponentRegionOfInterest()
  {
    return getOrthogonalComponentRegionOfInterest(true);
  }
  
  /**
   * Get the region of interest for the component relative to the rotated
   * inspection region.  This will ONLY work for components that are contained
   * all in one region.
   * 
   * Modify by Wei Chin 21-11-2013
   * - Handle alignment region won't affect by resize feature 
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public RegionOfInterest getOrthogonalComponentRegionOfInterest(boolean resizeIfNeeded)
  {
    RegionOfInterest componentRegionOfInterest = null;
    ReconstructionRegion reconstructionRegion = null;

    // initialize region to be the same as pad one, that way we have the right orientation
    componentRegionOfInterest = new RegionOfInterest(getPadOneJointInspectionData().getOrthogonalRegionOfInterestInPixels(resizeIfNeeded));
    reconstructionRegion = getPadOneJointInspectionData().getInspectionRegion();

    for (Pad pad : _component.getPads())
    {
      JointInspectionData jointInspectionData = _testSubProgram.getJointAlignmentData(pad);
      componentRegionOfInterest.add(jointInspectionData.getOrthogonalRegionOfInterestInPixels(resizeIfNeeded));
      Assert.expect(reconstructionRegion == jointInspectionData.getInspectionRegion(),
                    "do not call getOrthogonalComponentRegionOfInterest on components that span multiple reconstructionRegions!");
    }

    return componentRegionOfInterest;
  }
}
