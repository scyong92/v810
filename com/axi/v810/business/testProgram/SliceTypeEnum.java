package com.axi.v810.business.testProgram;

import java.io.*;

import com.axi.util.*;

/**
* @author Dave Ferguson
*/
public class SliceTypeEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = 0;
  public static final SliceTypeEnum RECONSTRUCTION = new SliceTypeEnum(_index++);
  public static final SliceTypeEnum SURFACE = new SliceTypeEnum(_index++);
  public static final SliceTypeEnum PROJECTION = new SliceTypeEnum(_index++); // Chnee Khang Wah, 2012-02-21, 2.5D

  /**
   * @author Dave Ferguson
   */
  private SliceTypeEnum(int id)
  {
    super(id);
  }
}
