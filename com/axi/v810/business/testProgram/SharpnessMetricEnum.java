package com.axi.v810.business.testProgram;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * This class contains the metrics that are used by the auto-focus search algorithm to
 * calculate sharpness values.
 *
 * @author Horst Mueller
 * @author Michael Tutkowski
 */
public class SharpnessMetricEnum extends com.axi.util.Enum implements Serializable
{
  private String _description;
  private static Map<String, SharpnessMetricEnum> _descriptionToSharpnessMetricEnumMap = new HashMap<String, SharpnessMetricEnum>();

  private static int _index = -1;
  public static final SharpnessMetricEnum DEFAULT = new SharpnessMetricEnum("Default", ++_index);

  /**
   * @author Horst Mueller
   * @author Michael Tutkowski
   */
  private SharpnessMetricEnum(String description, int id)
  {
    super(id);

    Assert.expect(description != null);
    Assert.expect(_descriptionToSharpnessMetricEnumMap != null);

    _description = description;

    _descriptionToSharpnessMetricEnumMap.put(_description.toLowerCase(), this);
  }

  /**
   * @author Michael Tutkowski
   */
  public String getDescription()
  {
    Assert.expect(_description != null);

    return _description;
  }

  /**
   * @author Michael Tutkowski
   */
  public String toString()
  {
    return getDescription();
  }

  /**
   * @author Michael Tutkowski
   */
  public static SharpnessMetricEnum getSharpnessMetricEnum(String description)
  {
    Assert.expect(description != null);
    Assert.expect(_descriptionToSharpnessMetricEnumMap != null);

    SharpnessMetricEnum sharpnessMetricEnum = (SharpnessMetricEnum)_descriptionToSharpnessMetricEnumMap.get(description.
        toLowerCase());

    Assert.expect(sharpnessMetricEnum != null);

    return sharpnessMetricEnum;
  }

  /**
   * @author Horst Mueller
   */
  public static SharpnessMetricEnum getEnum(int id)
  {
    Assert.expect(id > -1);
    Assert.expect(id < 1);

    return DEFAULT;
  }
}
