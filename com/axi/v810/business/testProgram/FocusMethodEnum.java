package com.axi.v810.business.testProgram;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
* This class contains the locations used to focus an image at the appropriate
* appropriate slice height.
*
* @author Horst Mueller
* @author Michael Tutkowski
*/
public class FocusMethodEnum extends com.axi.util.Enum implements Serializable
{
  private String _description;
  private static Map<String, FocusMethodEnum> _descriptionToFocusMethodEnumMap = new HashMap<String, FocusMethodEnum>();
  private static List<FocusMethodEnum> _focusMethodEnums = new ArrayList<FocusMethodEnum>();

  private static int _index = -1;
  public static final FocusMethodEnum PERCENT_FROM_SHARPEST_TO_EDGE = new FocusMethodEnum("Percent From Sharpest To Edge", ++_index);
  public static final FocusMethodEnum PERCENT_BETWEEN_EDGES = new FocusMethodEnum("Percent Between Edges", ++_index);
  public static final FocusMethodEnum USE_Z_HEIGHT = new FocusMethodEnum("Use Z-Height", ++_index);
  public static final FocusMethodEnum SHARPEST = new FocusMethodEnum("Sharpest", ++_index);


  /**
  * @author Horst Mueller
  * @author Michael Tutkowski
  */
  private FocusMethodEnum(String description, int id)
  {
    super(id);

    Assert.expect(description != null);
    Assert.expect(_descriptionToFocusMethodEnumMap != null);

    _description = description;

    _descriptionToFocusMethodEnumMap.put(_description.toLowerCase(), this);
    _focusMethodEnums.add(this);
  }

  /**
   * @author Michael Tutkowski
   */
  public String getDescription()
  {
    Assert.expect(_description != null);

    return _description;
  }

  /**
   * @author Michael Tutkowski
   */
  public String toString()
  {
    return getDescription();
  }

  /**
   * @author Michael Tutkowski
   */
  public static FocusMethodEnum getFocusMethodEnum(String description)
  {
    Assert.expect(description != null);
    Assert.expect(_descriptionToFocusMethodEnumMap != null);

    FocusMethodEnum FocusMethodEnum = (FocusMethodEnum)_descriptionToFocusMethodEnumMap.get(description.toLowerCase());

    Assert.expect(FocusMethodEnum != null);

    return FocusMethodEnum;
  }

  /**
   * @author Horst Mueller
   */
  public static FocusMethodEnum getFocusMethodEnum(int id)
  {
    Assert.expect(id > -1);
    Assert.expect(id < _focusMethodEnums.size());

    return (FocusMethodEnum)_focusMethodEnums.get(id);

  }
}

