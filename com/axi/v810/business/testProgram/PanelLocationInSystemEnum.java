package com.axi.v810.business.testProgram;

import java.io.*;

import com.axi.util.*;

/**
* @author George A. David
*/
public class PanelLocationInSystemEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = 0;
  public static final PanelLocationInSystemEnum RIGHT = new PanelLocationInSystemEnum(_index++, "right");
  public static final PanelLocationInSystemEnum LEFT = new PanelLocationInSystemEnum(_index++, "left");

  private String _name;
  /**
   * @author George A. David
   */
  private PanelLocationInSystemEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);
    _name = name;
  }

  /**
   * @author George A. David
   */
  public String getName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @return
   * @author Chong, Wei Chin
   */
  public String toString()
  {
    return getName();
  }
}

