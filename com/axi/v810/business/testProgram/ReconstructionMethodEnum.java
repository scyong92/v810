package com.axi.v810.business.testProgram;

import java.io.*;

import com.axi.util.*;

/**
* @author George A. David
*/
public class ReconstructionMethodEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = 0;
  public static final ReconstructionMethodEnum AVERAGE = new ReconstructionMethodEnum(_index++);
  public static final ReconstructionMethodEnum LIGHTEST = new ReconstructionMethodEnum(_index++);
  public static final ReconstructionMethodEnum STRETCH = new ReconstructionMethodEnum(_index++);
  public static final ReconstructionMethodEnum BLEND = new ReconstructionMethodEnum(_index++);

  /**
   * @author George A. David
   */
  private ReconstructionMethodEnum(int id)
  {
    super(id);
  }
}

