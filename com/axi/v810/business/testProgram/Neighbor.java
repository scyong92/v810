package com.axi.v810.business.testProgram;

import java.io.*;

import com.axi.util.*;


/**
 * @author George A. David
 */
public class Neighbor implements Serializable
{
  private int _reconstructionRegionId = -1;

  /**
   * @author George A. David
   */
  public Neighbor(int reconstructionRegionId)
  {
    Assert.expect(reconstructionRegionId > 0);

    _reconstructionRegionId = reconstructionRegionId;
  }

  /**
   * @author Roy Williams
   */
  public int getReconstructionRegionId()
  {
    return _reconstructionRegionId;
  }
  }
