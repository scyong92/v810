package com.axi.v810.business.testProgram;

import java.io.*;

import com.axi.v810.util.*;

/**
 * Represents a generic region-of-interest within a ReconstructionRegion.  The
 * set of Features within a ReconstructionRegion is what defines the overall size
 * of the ReconstructionRegion.
 *
 * This is intended to be used by the hardware layer.  The business layer should
 * use the JointInspectionData class instead.  This reason for this interface is
 * to hide non-hardware-related stuff from the hardware layer.
 *
 * @author Peter Esbensen
 */
public interface Feature extends Serializable
{
  /**
   * Get the rectangle enclosing this InspectionRegionPad.  This will return a rectangle with coordinates
   * relative to the panel origin.
   * @author Peter Esbensen
   */
  public PanelRectangle getRegionRectangleRelativeToPanelOriginInNanoMeters();

  /**
   * @author George A. David
   */
//  public PanelRectangle getFeatureBoundsRelativeToPanelInNanoMeters();
}
