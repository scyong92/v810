package com.axi.v810.business.testProgram;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
* This class contains the methods that are used by the autofocus algorithm to find
* an image at an appropriate slice height.
*
* @author Horst Mueller
* @author Michael Tutkowski
*/
public class FocusSearchMethodEnum extends com.axi.util.Enum implements Serializable
{
  private String _description;
  private static Map<String, FocusSearchMethodEnum> _descriptionToFocusSearchMethodEnumMap = new HashMap<String, FocusSearchMethodEnum>();

  private static int _index = -1;
  //Determine the slice with the highest sharpness value in the focus profile
  public static final FocusSearchMethodEnum SHARPEST = new FocusSearchMethodEnum("Sharpest", ++_index);
  //Determine the slice that corresponds to a certain percentage of the maximum in the focus profile
  public static final FocusSearchMethodEnum PERCENT_OF_MAXIMUM = new FocusSearchMethodEnum("Percent of Maximum", ++_index);
  //Determine the slice that corresponds to a certain percentage between the maximum
  //and the background in the focus profile
  public static final FocusSearchMethodEnum PERCENT_BETWEEN = new FocusSearchMethodEnum("Percent Between", ++_index);
  //Determine the slice that corresponds to an inflection point in the focus profile
  public static final FocusSearchMethodEnum INFLECTION_POINT = new FocusSearchMethodEnum("Inflection Point", ++_index);
  //Do no search
  public static final FocusSearchMethodEnum NO_SEARCH = new FocusSearchMethodEnum("No Search", ++_index);

  /**
  * @author Horst Mueller
  * @author Michael Tutkowski
  */
  private FocusSearchMethodEnum(String description, int id)
  {
    super(id);

    Assert.expect(description != null);
    Assert.expect(_descriptionToFocusSearchMethodEnumMap != null);

    _description = description;

    _descriptionToFocusSearchMethodEnumMap.put(_description.toLowerCase(), this);
  }

  /**
   * @author Michael Tutkowski
   */
  public String getDescription()
  {
    Assert.expect(_description != null);

    return _description;
  }

  /**
   * @author Michael Tutkowski
   */
  public String toString()
  {
    return getDescription();
  }

  /**
   * @author Michael Tutkowski
   */
  public static FocusSearchMethodEnum getFocusSearchMethodEnum(String description)
  {
    Assert.expect(description != null);
    Assert.expect(_descriptionToFocusSearchMethodEnumMap != null);

    FocusSearchMethodEnum focusSearchMethodEnum = (FocusSearchMethodEnum)_descriptionToFocusSearchMethodEnumMap.get(description.toLowerCase());

    Assert.expect(focusSearchMethodEnum != null);

    return focusSearchMethodEnum;
  }

  /**
   * @author Horst Mueller
   */
  public static FocusSearchMethodEnum getEnum(int id)
  {
    Assert.expect(id > -1);
    Assert.expect(id < 5);

    if (id == 0)
    {
      return SHARPEST;
    }
    else if(id == 1)
    {
      return PERCENT_OF_MAXIMUM;
    }
    else if(id == 2)
    {
      return PERCENT_BETWEEN;
    }
    else if(id == 3)
    {
      return INFLECTION_POINT;
    }
    else
    {
      return NO_SEARCH;
    }
  }
}
