package com.axi.v810.business.testProgram;

import com.axi.util.*;
import java.util.*;

/**
 * Provides a meaningful description of the slice.
 *
 * @author Patrick Lacz
 */
public class SliceNameEnum extends com.axi.util.Enum
{
  /**
   * A map from an index to a slice name enum. An array could work here, however I choose a map because
   * the assumption that the indices range from 0..N may not remain valid.
   */
  private static Map<Integer, SliceNameEnum> _indexToEnumMap = new TreeMap<Integer, SliceNameEnum>();
  private static Map<String, SliceNameEnum> _nameToEnumMap = new TreeMap<String, SliceNameEnum>();

  private static int _index = 0;
  private static int _startingIndexForUnfocusedRegionSlicesForRetest = (_index + 1) * 10000;

  public static SliceNameEnum PAD = new SliceNameEnum(_index++, "Pad");
  public static SliceNameEnum CLEAR_CHIP_PAD = new SliceNameEnum(_index++, "Clear Chip Pad");
  public static SliceNameEnum OPAQUE_CHIP_PAD = new SliceNameEnum(_index++, "Opaque Chip Pad");
  public static SliceNameEnum PCAP_SLUG = new SliceNameEnum(_index++, "PCAP Slug");
  public static SliceNameEnum MIDBALL = new SliceNameEnum(_index++, "Midball");
  public static SliceNameEnum PACKAGE = new SliceNameEnum(_index++, "Package");

  /** @todo PE figure out if I should add Barrel 1, Barrel 2, Barrel 3 now */
  public static SliceNameEnum THROUGHHOLE_BARREL = new SliceNameEnum(_index++, "Barrel");
  public static SliceNameEnum THROUGHHOLE_INSERTION = new SliceNameEnum(_index++, "Insertion");
  public static SliceNameEnum THROUGHHOLE_PROTRUSION = new SliceNameEnum(_index++, "Protrusion");
  public static SliceNameEnum THROUGHHOLE_PIN_SIDE = new SliceNameEnum(_index++, "Pin Side");
  public static SliceNameEnum THROUGHHOLE_COMPONENT_SIDE = new SliceNameEnum(_index++, "Component Side Short");

  public static SliceNameEnum HIGH_SHORT = new SliceNameEnum(_index++, "High Short");
  public static SliceNameEnum BGA_LOWERPAD_ADDITIONAL = new SliceNameEnum(_index++, "BGA Additional Lower Pad");

  public static SliceNameEnum SURFACE_MODELING = new SliceNameEnum(_index++, "Surface Modeling");

  // a slice known by the TestProgram; in order to instruct the IRP to re-reconstruct more than
  // a single slice at a time, they must each have a unique sliceName (i.e. VIRTUAL_LIVE_SLICE_2, ...)
  public static SliceNameEnum VIRTUAL_LIVE_SLICE_1 = new SliceNameEnum(_index++, "Virtual Live reference slice");
  // a slice that exists only for the purpose of maintaining the 1:1 correspondance between reconstructedImages and slices
  public static SliceNameEnum VIRTUAL_LIVE_VIRTUAL_SLICE = new SliceNameEnum(_index++, "Virtual Live slice of interest");

  public static SliceNameEnum THROUGHHOLE_BARREL_2 = new SliceNameEnum(_index++, "Barrel2");
  public static SliceNameEnum THROUGHHOLE_BARREL_3 = new SliceNameEnum(_index++, "Barrel3");
  public static SliceNameEnum THROUGHHOLE_BARREL_4 = new SliceNameEnum(_index++, "Barrel4");
//  public static SliceNameEnum THROUGHHOLE_BARREL_5 = new SliceNameEnum(_index++, "Barrel5");
//  public static SliceNameEnum THROUGHHOLE_BARREL_6 = new SliceNameEnum(_index++, "Barrel6");
//  public static SliceNameEnum THROUGHHOLE_BARREL_7 = new SliceNameEnum(_index++, "Barrel7");
//  public static SliceNameEnum THROUGHHOLE_BARREL_8 = new SliceNameEnum(_index++, "Barrel8");
//  public static SliceNameEnum THROUGHHOLE_BARREL_9 = new SliceNameEnum(_index++, "Barrel9");
//  public static SliceNameEnum THROUGHHOLE_BARREL_10 = new SliceNameEnum(_index++, "Barrel10");
  
  // All obsolete SliceNameEnum here
  // Added back to fix CR31476 - Assert when reading pre 1.12 Subtype learning file
  // by Seng-Yew Lim
  // START
  public static SliceNameEnum THROUGHHOLE_10_OBSOLETE = new SliceNameEnum(_index++, "Throughhole 10% Obsolete");
  public static SliceNameEnum THROUGHHOLE_20_OBSOLETE = new SliceNameEnum(_index++, "Throughhole 20% Obsolete");
  public static SliceNameEnum THROUGHHOLE_30_OBSOLETE = new SliceNameEnum(_index++, "Throughhole 30% Obsolete");
  public static SliceNameEnum THROUGHHOLE_40_OBSOLETE = new SliceNameEnum(_index++, "Throughhole 40% Obsolete");
  public static SliceNameEnum THROUGHHOLE_50_OBSOLETE = new SliceNameEnum(_index++, "Throughhole 50% Obsolete");
  public static SliceNameEnum THROUGHHOLE_60_OBSOLETE = new SliceNameEnum(_index++, "Throughhole 60% Obsolete");
  public static SliceNameEnum THROUGHHOLE_70_OBSOLETE = new SliceNameEnum(_index++, "Throughhole 70% Obsolete");
  public static SliceNameEnum THROUGHHOLE_80_OBSOLETE = new SliceNameEnum(_index++, "Throughhole 80% Obsolete");
  public static SliceNameEnum THROUGHHOLE_90_OBSOLETE = new SliceNameEnum(_index++, "Throughhole 90% Obsolete");
  public static SliceNameEnum THROUGHHOLE_MINUS_10_OBSOLETE = new SliceNameEnum(_index++, "Throughhole Minus 10% Obsolete");
  public static SliceNameEnum THROUGHHOLE_MINUS_20_OBSOLETE = new SliceNameEnum(_index++, "Throughhole Minus 20% Obsolete");
  public static SliceNameEnum THROUGHHOLE_MINUS_30_OBSOLETE = new SliceNameEnum(_index++, "Throughhole Minus 30% Obsolete");
  public static SliceNameEnum THROUGHHOLE_75_OBSOLETE = new SliceNameEnum(_index++, "Throughhole Minus 75% Obsolete");
  public static SliceNameEnum THROUGHHOLE_25_OBSOLETE = new SliceNameEnum(_index++, "Throughhole Minus 25% Obsolete");

  public static SliceNameEnum MID_BOARD_SLICE = new SliceNameEnum(_index++, "Midboard Slice");
  public static SliceNameEnum RFP_SLICE = new SliceNameEnum(_index++, "RFP slice");
  // END

  // Chnee Khang Wah, 2012-02-21, 2.5D
  // START
  public static SliceNameEnum CAMERA_0 = new SliceNameEnum(_index++, "pro_00"); //41
  public static SliceNameEnum CAMERA_1 = new SliceNameEnum(_index++, "pro_10");
  public static SliceNameEnum CAMERA_2 = new SliceNameEnum(_index++, "pro_20");
  public static SliceNameEnum CAMERA_3 = new SliceNameEnum(_index++, "pro_30");
  public static SliceNameEnum CAMERA_4 = new SliceNameEnum(_index++, "pro_40");
  public static SliceNameEnum CAMERA_5 = new SliceNameEnum(_index++, "pro_50");
  public static SliceNameEnum CAMERA_6 = new SliceNameEnum(_index++, "pro_60");
  public static SliceNameEnum CAMERA_7 = new SliceNameEnum(_index++, "pro_70");
  public static SliceNameEnum CAMERA_8 = new SliceNameEnum(_index++, "pro_80");
  public static SliceNameEnum CAMERA_9 = new SliceNameEnum(_index++, "pro_90");
  public static SliceNameEnum CAMERA_10 = new SliceNameEnum(_index++, "pro_01");
  public static SliceNameEnum CAMERA_11 = new SliceNameEnum(_index++, "pro_11");
  public static SliceNameEnum CAMERA_12 = new SliceNameEnum(_index++, "pro_21");
  public static SliceNameEnum CAMERA_13 = new SliceNameEnum(_index++, "pro_31");
  // END
  
  public static SliceNameEnum THROUGHHOLE_BARREL_5 = new SliceNameEnum(_index++, "Barrel5");
  public static SliceNameEnum THROUGHHOLE_BARREL_6 = new SliceNameEnum(_index++, "Barrel6");
  public static SliceNameEnum THROUGHHOLE_BARREL_7 = new SliceNameEnum(_index++, "Barrel7");
  public static SliceNameEnum THROUGHHOLE_BARREL_8 = new SliceNameEnum(_index++, "Barrel8");
  public static SliceNameEnum THROUGHHOLE_BARREL_9 = new SliceNameEnum(_index++, "Barrel9");
  public static SliceNameEnum THROUGHHOLE_BARREL_10 = new SliceNameEnum(_index++, "Barrel10");
  
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_1 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice1");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_2 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice2");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_3 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice3");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_4 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice4");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_5 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice5");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_6 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice6");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_7 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice7");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_8 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice8");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_9 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice9");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_10 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice10");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_11 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice11");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_12 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice12");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_13 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice13");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_14 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice14");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_15 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice15");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_16 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice16");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_17 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice17");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_18 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice18");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_19 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice19");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_20 = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice20");
  
  public static SliceNameEnum CONNECTOR_BARREL_2 = new SliceNameEnum(_index++, "ConnectorBarrel2");
  public static SliceNameEnum CONNECTOR_BARREL_3 = new SliceNameEnum(_index++, "ConnectorBarrel3");
  public static SliceNameEnum CONNECTOR_BARREL_4 = new SliceNameEnum(_index++, "ConnectorBarrel4");
  public static SliceNameEnum CONNECTOR_BARREL_5 = new SliceNameEnum(_index++, "ConnectorBarrel5");
  public static SliceNameEnum CONNECTOR_BARREL_6 = new SliceNameEnum(_index++, "ConnectorBarrel6");
  public static SliceNameEnum CONNECTOR_BARREL_7 = new SliceNameEnum(_index++, "ConnectorBarrel7");
  public static SliceNameEnum CONNECTOR_BARREL_8 = new SliceNameEnum(_index++, "ConnectorBarrel8");
  public static SliceNameEnum CONNECTOR_BARREL_9 = new SliceNameEnum(_index++, "ConnectorBarrel9");
  public static SliceNameEnum CONNECTOR_BARREL_10 = new SliceNameEnum(_index++, "ConnectorBarrel10");
  
  public static SliceNameEnum LARGE_PAD_USER_DEFINED_SLICE_1 = new SliceNameEnum(_index++, "LargePadUserDefinedSlice1");
  public static SliceNameEnum LARGE_PAD_USER_DEFINED_SLICE_2 = new SliceNameEnum(_index++, "LargePadUserDefinedSlice2");
  
  public static SliceNameEnum USER_DEFINED_WAVELET_LEVEL = new SliceNameEnum(_index++, "SliceWithUserDefinedWaveletLevel");
  
  public static SliceNameEnum PSP_SEARCH_RANGE_LOW_LIMIT  = new SliceNameEnum(_index++, "PspSearchRangeLowLimit");
  public static SliceNameEnum PSP_SEARCH_RANGE_HIGH_LIMIT = new SliceNameEnum(_index++, "PspSearchRangeHighLimit");
  
  // XCR-3603 PSP Z-Offset
  public static SliceNameEnum PSP_Z_OFFSET = new SliceNameEnum(_index++, "PspZOffset");
  
  // Added by Lee Herng 16 Dec 2013 - To handle all slice in Measurement tab in AlgoTuner
  public static SliceNameEnum ALL_SLICE = new SliceNameEnum(_index++, "All");

  // bee-hoon.goh, 2014-01-02, Component Image Slices
  // START
  public static SliceNameEnum PAD_COMPONENT = new SliceNameEnum(_index++, "Pad Component");  //
  public static SliceNameEnum MIDBALL_COMPONENT = new SliceNameEnum(_index++, "Midball Component");
  public static SliceNameEnum PACKAGE_COMPONENT = new SliceNameEnum(_index++, "Package Component");

  public static SliceNameEnum THROUGHHOLE_BARREL_COMPONENT = new SliceNameEnum(_index++, "Barrel Component");
  public static SliceNameEnum THROUGHHOLE_INSERTION_COMPONENT = new SliceNameEnum(_index++, "Insertion Component");
  public static SliceNameEnum THROUGHHOLE_PROTRUSION_COMPONENT = new SliceNameEnum(_index++, "Protrusion Component");
  public static SliceNameEnum THROUGHHOLE_PIN_SIDE_COMPONENT = new SliceNameEnum(_index++, "Pin Side Component");
  public static SliceNameEnum THROUGHHOLE_COMPONENT_SIDE_COMPONENT = new SliceNameEnum(_index++, "Component Side Short Component");

  public static SliceNameEnum HIGH_SHORT_COMPONENT = new SliceNameEnum(_index++, "High Short Component");
  public static SliceNameEnum BGA_LOWERPAD_ADDITIONAL_COMPONENT = new SliceNameEnum(_index++, "BGA Additional Lower Pad Component");

  public static SliceNameEnum THROUGHHOLE_BARREL_2_COMPONENT = new SliceNameEnum(_index++, "Barrel2 Component");
  public static SliceNameEnum THROUGHHOLE_BARREL_3_COMPONENT = new SliceNameEnum(_index++, "Barrel3 Component");
  public static SliceNameEnum THROUGHHOLE_BARREL_4_COMPONENT = new SliceNameEnum(_index++, "Barrel4 Component");

  public static SliceNameEnum CAMERA_0_COMPONENT = new SliceNameEnum(_index++, "pro_00 Component"); //41
  public static SliceNameEnum CAMERA_1_COMPONENT = new SliceNameEnum(_index++, "pro_10 Component");
  public static SliceNameEnum CAMERA_2_COMPONENT = new SliceNameEnum(_index++, "pro_20 Component");
  public static SliceNameEnum CAMERA_3_COMPONENT = new SliceNameEnum(_index++, "pro_30 Component");
  public static SliceNameEnum CAMERA_4_COMPONENT = new SliceNameEnum(_index++, "pro_40 Component");
  public static SliceNameEnum CAMERA_5_COMPONENT = new SliceNameEnum(_index++, "pro_50 Component");
  public static SliceNameEnum CAMERA_6_COMPONENT = new SliceNameEnum(_index++, "pro_60 Component");
  public static SliceNameEnum CAMERA_7_COMPONENT = new SliceNameEnum(_index++, "pro_70 Component");
  public static SliceNameEnum CAMERA_8_COMPONENT = new SliceNameEnum(_index++, "pro_80 Component");
  public static SliceNameEnum CAMERA_9_COMPONENT = new SliceNameEnum(_index++, "pro_90 Component");
  public static SliceNameEnum CAMERA_10_COMPONENT = new SliceNameEnum(_index++, "pro_01 Component");
  public static SliceNameEnum CAMERA_11_COMPONENT = new SliceNameEnum(_index++, "pro_11 Component");
  public static SliceNameEnum CAMERA_12_COMPONENT = new SliceNameEnum(_index++, "pro_21 Component");
  public static SliceNameEnum CAMERA_13_COMPONENT = new SliceNameEnum(_index++, "pro_31 Component");

  public static SliceNameEnum THROUGHHOLE_BARREL_5_COMPONENT = new SliceNameEnum(_index++, "Barrel5 Component");
  public static SliceNameEnum THROUGHHOLE_BARREL_6_COMPONENT = new SliceNameEnum(_index++, "Barrel6 Component");
  public static SliceNameEnum THROUGHHOLE_BARREL_7_COMPONENT = new SliceNameEnum(_index++, "Barrel7 Component");
  public static SliceNameEnum THROUGHHOLE_BARREL_8_COMPONENT = new SliceNameEnum(_index++, "Barrel8 Component");
  public static SliceNameEnum THROUGHHOLE_BARREL_9_COMPONENT = new SliceNameEnum(_index++, "Barrel9 Component");
  public static SliceNameEnum THROUGHHOLE_BARREL_10_COMPONENT = new SliceNameEnum(_index++, "Barrel10 Component");
  
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_1_COMPONENT = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice1 Component");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_2_COMPONENT = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice2 Component");
  public static SliceNameEnum GRID_ARRAY_USER_DEFINED_SLICE_3_COMPONENT = new SliceNameEnum(_index++, "GridArrayUserDefinedSlice3 Component");
  
  public static SliceNameEnum CONNECTOR_BARREL_2_COMPONENT = new SliceNameEnum(_index++, "ConnectorBarrel2 Component");
  public static SliceNameEnum CONNECTOR_BARREL_3_COMPONENT = new SliceNameEnum(_index++, "ConnectorBarrel3 Component");
  public static SliceNameEnum CONNECTOR_BARREL_4_COMPONENT = new SliceNameEnum(_index++, "ConnectorBarrel4 Component");
  public static SliceNameEnum CONNECTOR_BARREL_5_COMPONENT = new SliceNameEnum(_index++, "ConnectorBarrel5 Component");
  public static SliceNameEnum CONNECTOR_BARREL_6_COMPONENT = new SliceNameEnum(_index++, "ConnectorBarrel6 Component");
  public static SliceNameEnum CONNECTOR_BARREL_7_COMPONENT = new SliceNameEnum(_index++, "ConnectorBarrel7 Component");
  public static SliceNameEnum CONNECTOR_BARREL_8_COMPONENT = new SliceNameEnum(_index++, "ConnectorBarrel8 Component");
  public static SliceNameEnum CONNECTOR_BARREL_9_COMPONENT = new SliceNameEnum(_index++, "ConnectorBarrel9 Component");
  public static SliceNameEnum CONNECTOR_BARREL_10_COMPONENT = new SliceNameEnum(_index++, "ConnectorBarrel10 Component");
  
  public static SliceNameEnum LARGE_PAD_USER_DEFINED_SLICE_1_COMPONENT = new SliceNameEnum(_index++, "LargePadUserDefinedSlice1 Component");
  public static SliceNameEnum LARGE_PAD_USER_DEFINED_SLICE_2_COMPONENT = new SliceNameEnum(_index++, "LargePadUserDefinedSlice2 Component");

  public static SliceNameEnum DIAGNOSTIC_PAD_SLICE = new SliceNameEnum(_index++, "Diagnostic Pad Slice");
  public static SliceNameEnum DIAGNOSTIC_USER_DEFINED_SLICE_1 = new SliceNameEnum(_index++, "Diagnostic User Defined Slice 1");
  public static SliceNameEnum DIAGNOSTIC_USER_DEFINED_SLICE_2 = new SliceNameEnum(_index++, "Diagnostic User Defined Slice 2");
  public static SliceNameEnum DIAGNOSTIC_MIDBALL_SLICE = new SliceNameEnum(_index++, "Diagnostic Midball Slice");
  public static SliceNameEnum DIAGNOSTIC_PACKAGE_SLICE = new SliceNameEnum(_index++, "Diagnostic Package Slice");
  public static SliceNameEnum DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_1 = new SliceNameEnum(_index++, "Diagnostic GridArrayUserDefinedSlice1");
  public static SliceNameEnum DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_2 = new SliceNameEnum(_index++, "Diagnostic GridArrayUserDefinedSlice2");
  public static SliceNameEnum DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_3 = new SliceNameEnum(_index++, "Diagnostic GridArrayUserDefinedSlice3");
  
  //Siew Yeng - XCR-2683 - Enhanced Slice Image
  public static SliceNameEnum ENHANCED_PAD_SLICE = new SliceNameEnum(_index++, "Enhanced Pad"); //XCR-3391 - Siew Yeng
  public static SliceNameEnum ENHANCED_CLEAR_CHIP_PAD = new SliceNameEnum(_index++, "Enhanced Clear Chip Pad");
  public static SliceNameEnum ENHANCED_OPAQUE_CHIP_PAD = new SliceNameEnum(_index++, "Enhanced Opaque Chip Pad");
  public static SliceNameEnum ENHANCED_PCAP_SLUG = new SliceNameEnum(_index++, "Enhanced PCAP Slug");
  
  public static SliceNameEnum ENHANCED_THROUGHHOLE_BARREL = new SliceNameEnum(_index++, "Enhanced Barrel");
  public static SliceNameEnum ENHANCED_THROUGHHOLE_INSERTION = new SliceNameEnum(_index++, "Enhanced Insertion");
  public static SliceNameEnum ENHANCED_THROUGHHOLE_PROTRUSION = new SliceNameEnum(_index++, "Enhanced Protrusion");
  public static SliceNameEnum ENHANCED_THROUGHHOLE_PIN_SIDE = new SliceNameEnum(_index++, "Enhanced Pin Side");
  public static SliceNameEnum ENHANCED_THROUGHHOLE_COMPONENT_SIDE = new SliceNameEnum(_index++, "Enhanced Component Side Short");
  
  public static SliceNameEnum ENHANCED_BGA_LOWERPAD_ADDITIONAL = new SliceNameEnum(_index++, "Enhanced BGA Additional Lower Pad Component");
  public static SliceNameEnum ENHANCED_HIGH_SHORT = new SliceNameEnum(_index++, "Enhanced High Short");
  
  public static SliceNameEnum ENHANCED_THROUGHHOLE_BARREL_2 = new SliceNameEnum(_index++, "Enhanced Barrel2");
  public static SliceNameEnum ENHANCED_THROUGHHOLE_BARREL_3 = new SliceNameEnum(_index++, "Enhanced Barrel3");
  public static SliceNameEnum ENHANCED_THROUGHHOLE_BARREL_4 = new SliceNameEnum(_index++, "Enhanced Barrel4");
  public static SliceNameEnum ENHANCED_THROUGHHOLE_BARREL_5 = new SliceNameEnum(_index++, "Enhanced Barrel5");
  public static SliceNameEnum ENHANCED_THROUGHHOLE_BARREL_6 = new SliceNameEnum(_index++, "Enhanced Barrel6");
  public static SliceNameEnum ENHANCED_THROUGHHOLE_BARREL_7 = new SliceNameEnum(_index++, "Enhanced Barrel7");
  public static SliceNameEnum ENHANCED_THROUGHHOLE_BARREL_8 = new SliceNameEnum(_index++, "Enhanced Barrel8");
  public static SliceNameEnum ENHANCED_THROUGHHOLE_BARREL_9 = new SliceNameEnum(_index++, "Enhanced Barrel9");
  public static SliceNameEnum ENHANCED_THROUGHHOLE_BARREL_10 = new SliceNameEnum(_index++, "Enhanced Barrel10");
  
  public static SliceNameEnum ENHANCED_CONNECTOR_BARREL_2 = new SliceNameEnum(_index++, "Enhanced ConnectorBarrel2");
  public static SliceNameEnum ENHANCED_CONNECTOR_BARREL_3 = new SliceNameEnum(_index++, "Enhanced ConnectorBarrel3");
  public static SliceNameEnum ENHANCED_CONNECTOR_BARREL_4 = new SliceNameEnum(_index++, "Enhanced ConnectorBarrel4");
  public static SliceNameEnum ENHANCED_CONNECTOR_BARREL_5 = new SliceNameEnum(_index++, "Enhanced ConnectorBarrel5");
  public static SliceNameEnum ENHANCED_CONNECTOR_BARREL_6 = new SliceNameEnum(_index++, "Enhanced ConnectorBarrel6");
  public static SliceNameEnum ENHANCED_CONNECTOR_BARREL_7 = new SliceNameEnum(_index++, "Enhanced ConnectorBarrel7");
  public static SliceNameEnum ENHANCED_CONNECTOR_BARREL_8 = new SliceNameEnum(_index++, "Enhanced ConnectorBarrel8");
  public static SliceNameEnum ENHANCED_CONNECTOR_BARREL_9 = new SliceNameEnum(_index++, "Enhanced ConnectorBarrel9");
  public static SliceNameEnum ENHANCED_CONNECTOR_BARREL_10 = new SliceNameEnum(_index++, "Enhanced ConnectorBarrel10");
  
  public static SliceNameEnum ENHANCED_USER_DEFINED_SLICE_1 = new SliceNameEnum(_index++, "Enhanced User Defined Slice 1");
  public static SliceNameEnum ENHANCED_USER_DEFINED_SLICE_2 = new SliceNameEnum(_index++, "Enhanced User Defined Slice 2");
  public static SliceNameEnum ENHANCED_MIDBALL_SLICE = new SliceNameEnum(_index++, "Enhanced Midball"); //XCR-3391 - Siew Yeng
  public static SliceNameEnum ENHANCED_PACKAGE_SLICE = new SliceNameEnum(_index++, "Enhanced Package"); //XCR-3391 - Siew Yeng
  public static SliceNameEnum ENHANCED_GRID_ARRAY_USER_DEFINED_SLICE_1 = new SliceNameEnum(_index++, "Enhanced GridArrayUserDefinedSlice1");
  public static SliceNameEnum ENHANCED_GRID_ARRAY_USER_DEFINED_SLICE_2 = new SliceNameEnum(_index++, "Enhanced GridArrayUserDefinedSlice2");
  public static SliceNameEnum ENHANCED_GRID_ARRAY_USER_DEFINED_SLICE_3 = new SliceNameEnum(_index++, "Enhanced GridArrayUserDefinedSlice3");
  //END

  public static SliceNameEnum DIAGNOSTIC_FOREIGN_INCLUSION = new SliceNameEnum(_index++, "Diganostic ForeignInclusion");
  public static SliceNameEnum FOREIGN_INCLUSION = new SliceNameEnum(_index++, "ForeignInclusion");

  //Lim, Lay Ngor - Clear Tombstone
  public static SliceNameEnum DIAGNOSTIC_CLEAR_CHIP_SLICE = new SliceNameEnum(_index++, "Diagnostic Clear Chip Slice");
  
  private static int _additionalSlicesStartIndex = _index;

  private String _name;


  // these slices are for unfocused regions, if the users selects a region
  // as unfocused, we will generate 4 extra slices for each slice 2 above
  // and 2 below the original slice at 2 mil intervals
  private List<SliceNameEnum> _unfocusedRegionSliceNamesForRetest = new ArrayList<SliceNameEnum>(4);
  private static boolean _isCreatingUnfocusedRegionSliceNamesForRetest = false;

  /**
   * @author Patrick Lacz
   */
  public static SliceNameEnum getEnumFromIndex(int index)
  {
    Assert.expect(_indexToEnumMap != null);
    return _indexToEnumMap.get(index);
  }

  /**
   * @author Patrick Lacz
   */
  public static SliceNameEnum getEnumFromName(String name)
  {
    Assert.expect(name != null);
    Assert.expect(_indexToEnumMap != null);
    return _nameToEnumMap.get(name);
  }

  /**
   * @author Patrick Lacz
   */
  private SliceNameEnum(int id, String name)
  {
    // performs a check to verify that the id is not already in use
    super(id);
    Assert.expect(name != null);
    _name = name.intern();

    Assert.expect(_indexToEnumMap != null);
    _indexToEnumMap.put(id, this);
    _nameToEnumMap.put(name, this);
    createUnfocusedRegionSliceNamesForRetest();
  }

  /**
   * @author George A. David
   */
  private void createUnfocusedRegionSliceNamesForRetest()
  {
    if(_isCreatingUnfocusedRegionSliceNamesForRetest)
      return;
    _isCreatingUnfocusedRegionSliceNamesForRetest = true;
    int index = (getId() + 1) * _startingIndexForUnfocusedRegionSlicesForRetest;


    ///////////////////////////////////////////////////////////////////////////////////////
    // this order is important! if you must modify this, make sure this still works with
    // ProgramGeneration.createSlicesForUnfocusedRegionsIfNecessary()
    //////////////////////////////////////////////////////////////////////////////////////
    _unfocusedRegionSliceNamesForRetest.add(0, new SliceNameEnum(index++, _name + " UnfocusedRegionSliceNameForRetest " + "2 Mils"));
    _unfocusedRegionSliceNamesForRetest.add(1, new SliceNameEnum(index++, _name + " UnfocusedRegionSliceNameForRetest " + "4 Mils"));
    _unfocusedRegionSliceNamesForRetest.add(2, new SliceNameEnum(index++, _name + " UnfocusedRegionSliceNameForRetest " + "-2 Mils"));
    _unfocusedRegionSliceNamesForRetest.add(3, new SliceNameEnum(index++, _name + " UnfocusedRegionSliceNameForRetest " + "-4 Mils"));
    _isCreatingUnfocusedRegionSliceNamesForRetest = false;
  }

  /**
   * @author Patrick Lacz
   */
  public String getName()
  {
    Assert.expect(_name != null);
    return _name;
  }

  /**
   * @author Rex Shang
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author George A. David
   */
  public boolean isAdditionalSlice()
  {
    return getId() >= _additionalSlicesStartIndex;
  }

  /**
   * Creates a new slice. this is necessary to allow defect analyzer and
   * user focus top be implemented
   * @author George A. David
   */
  public static SliceNameEnum getAdditionalSliceName(int additionalSliceNumber)
  {
    SliceNameEnum additionalSlice = null;
    int newSliceIndex = _additionalSlicesStartIndex + additionalSliceNumber;
    if(_indexToEnumMap.containsKey(newSliceIndex) == false)
      additionalSlice = new SliceNameEnum(newSliceIndex, "Additional Slice: " + newSliceIndex);
    else
      additionalSlice = _indexToEnumMap.get(newSliceIndex);

    return additionalSlice;
  }

  /**
   * @author George A. David
   */
  public List<SliceNameEnum> getUnfocusedRegionSliceNamesForRetest()
  {
    Assert.expect(_unfocusedRegionSliceNamesForRetest != null);

    return _unfocusedRegionSliceNamesForRetest;
  }

  /**
   * @author George A. David
   */
  public boolean isUnfocusedRegionSliceNameForRetest()
  {
    return getId() >= _startingIndexForUnfocusedRegionSlicesForRetest;
  }
  
  /** 
   * @author Cheah Lee Herng 
   * @return TRUE if any slice is Multi-Angle slice
   *         FALSE if non of the slice is Multi-Angle slice
   */
  public boolean isMultiAngleSlice()
  {
    return ((this == SliceNameEnum.CAMERA_0)  ||
            (this == SliceNameEnum.CAMERA_0_COMPONENT)  ||
            (this == SliceNameEnum.CAMERA_1)  ||
            (this == SliceNameEnum.CAMERA_1_COMPONENT)  ||
            (this == SliceNameEnum.CAMERA_2)  ||
            (this == SliceNameEnum.CAMERA_2_COMPONENT)  ||
            (this == SliceNameEnum.CAMERA_3)  ||
            (this == SliceNameEnum.CAMERA_3_COMPONENT)  ||
            (this == SliceNameEnum.CAMERA_4)  ||
            (this == SliceNameEnum.CAMERA_4_COMPONENT)  ||
            (this == SliceNameEnum.CAMERA_5)  ||
            (this == SliceNameEnum.CAMERA_5_COMPONENT)  ||
            (this == SliceNameEnum.CAMERA_6)  ||
            (this == SliceNameEnum.CAMERA_6_COMPONENT)  ||
            (this == SliceNameEnum.CAMERA_7)  ||
            (this == SliceNameEnum.CAMERA_7_COMPONENT)  ||
            (this == SliceNameEnum.CAMERA_8)  ||
            (this == SliceNameEnum.CAMERA_8_COMPONENT)  ||
            (this == SliceNameEnum.CAMERA_9)  ||
            (this == SliceNameEnum.CAMERA_9_COMPONENT)  ||
            (this == SliceNameEnum.CAMERA_10) ||
            (this == SliceNameEnum.CAMERA_10_COMPONENT) ||
            (this == SliceNameEnum.CAMERA_11) ||
            (this == SliceNameEnum.CAMERA_11_COMPONENT) ||
            (this == SliceNameEnum.CAMERA_12) ||
            (this == SliceNameEnum.CAMERA_12_COMPONENT) ||
            (this == SliceNameEnum.CAMERA_13) ||
            (this == SliceNameEnum.CAMERA_13_COMPONENT));
  }
}
