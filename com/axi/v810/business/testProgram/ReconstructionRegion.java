package com.axi.v810.business.testProgram;

import java.awt.geom.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Implementation for the <code>ReconstructionRegion</code> and <code>InspectionRegion</code> classes.
 * @author Peter Esbensen
 */
public class ReconstructionRegion implements Serializable
{
  // This is the maximum size the x or y dimension can be for
  // and inspection region
  private static transient Config _config = Config.getInstance();
  private static final int _MAX_INSPECTION_REGION_WIDTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(1000);
  private static final int _MAX_INSPECTION_REGION_LENGTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(550);
  private static final int _MAX_INSPECTION_REGION_PAD_BOUNDS_WIDTH_IN_NANOMETERS = _MAX_INSPECTION_REGION_WIDTH_IN_NANOMETERS - (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters());
  private static final int _MAX_INSPECTION_REGION_PAD_BOUNDS_LENGTH_IN_NANOMETERS = _MAX_INSPECTION_REGION_LENGTH_IN_NANOMETERS - (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters());
  private static final int _MAX_ALIGNMENT_REGION_WIDTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(1000);
  private static final int _MAX_ALIGNMENT_REGION_LENGTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(550);

  // the feature width and length is the maximum width and length the bounds of the pads/fiducials
  // in an alignment region. It is not the maximum size of an alignment region. That is currently
  // 1000 x 550 mils. But in order to take into account many uncertanties of the system, we need
  // to make sure we have a large enough border.
  private static final int _MAX_ALIGNMENT_FEATURE_WIDTH_IN_NANOMETERS = _MAX_ALIGNMENT_REGION_WIDTH_IN_NANOMETERS - (2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters());
  private static final int _ALIGNMENT_FEATURE_LENGTH_BUFFER_IN_NANOMETERS = (int)MathUtil.convertMilsToNanoMeters(60);
  private static final int _MAX_ALIGNMENT_FEATURE_LENGTH_IN_NANOMETERS = (int)(_MAX_ALIGNMENT_REGION_LENGTH_IN_NANOMETERS - (2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters()) - _ALIGNMENT_FEATURE_LENGTH_BUFFER_IN_NANOMETERS);

  private static final int _MAX_VERIFICATION_REGION_WIDTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(322);
  private static final int _MAX_VERIFICATION_REGION_LENGTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(322);
  
  private static final int _MAX_HIGH_MAG_INSPECTION_REGION_WIDTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(675); //MathUtil.convertMilsToNanoMetersInteger(625); (10um)
  private static final int _MAX_HIGH_MAG_INSPECTION_REGION_LENGTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(365); // MathUtil.convertMilsToNanoMetersInteger(343); (10um) 
  private static final int _MAX_HIGH_MAG_INSPECTION_REGION_PAD_BOUNDS_WIDTH_IN_NANOMETERS = _MAX_HIGH_MAG_INSPECTION_REGION_WIDTH_IN_NANOMETERS - (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters());
  private static final int _MAX_HIGH_MAG_INSPECTION_REGION_PAD_BOUNDS_LENGTH_IN_NANOMETERS = _MAX_HIGH_MAG_INSPECTION_REGION_LENGTH_IN_NANOMETERS - (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters());
  private static final int _MAX_HIGH_MAG_ALIGNMENT_REGION_WIDTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(675); // MathUtil.convertMilsToNanoMetersInteger(625); (10um)
  private static final int _MAX_HIGH_MAG_ALIGNMENT_REGION_LENGTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(365); // MathUtil.convertMilsToNanoMetersInteger(343); (10um)
  private static final int _MAX_HIGH_MAG_VERIFICATION_REGION_WIDTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(210); // MathUtil.convertMilsToNanoMetersInteger(180); (10um)
  private static final int _MAX_HIGH_MAG_VERIFICATION_REGION_LENGTH_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(210); // MathUtil.convertMilsToNanoMetersInteger(180); (10um)
  
  private static final int _HIGH_MAG_ALIGNMENT_FEATURE_LENGTH_BUFFER_IN_NANOMETERS = (int)MathUtil.convertMilsToNanoMeters(37);  
  private static final int _MAX_HIGH_MAG_ALIGNMENT_FEATURE_WIDTH_IN_NANOMETERS = _MAX_HIGH_MAG_ALIGNMENT_REGION_WIDTH_IN_NANOMETERS - (2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters());
  private static final int _MAX_HIGH_MAG_ALIGNMENT_FEATURE_LENGTH_IN_NANOMETERS = (int)(_MAX_HIGH_MAG_ALIGNMENT_REGION_LENGTH_IN_NANOMETERS - (2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters()) - _HIGH_MAG_ALIGNMENT_FEATURE_LENGTH_BUFFER_IN_NANOMETERS);

  private static int _numRegions = 0;
  private int _regionId = -1;

  private TestSubProgram _testSubProgram;

  private ImageReconstructionEngineEnum _reconstructionEngineId;
  private FocusRegion _surfaceModelingFocusRegion;

//  //The slice numbers are used to map focus regions to focus instructions
//  //Each slice can have multiple focus regions and focus instructions. The number
//  //of focus instructions and regions will generally not be the same
//  //Slice numbers are zero based
//  private int _numberOfSlices = 0;

  private List<FocusGroup> _focusGroups = new ArrayList<FocusGroup>();
  private transient List<ScanPass> _scanPasses = new ArrayList<ScanPass>();
  private BooleanRef _isTopSide;
  private PanelRectangle _regionRectangleRelativeToPanelInNanoMeters;
  private PanelRectangle _boundingRegionOfPadsInNanoMeters;
  private List<JointInspectionData> _jointInspectionDataList = new ArrayList<JointInspectionData>();
  private List<JointInspectionData> _jointInspectionDataWithinBoundList = new ArrayList<JointInspectionData>();
  private List<FiducialInspectionData> _fiducialInspectionDataList = new ArrayList<FiducialInspectionData>();
  private Collection<JointInspectionData> _alignmentContextFeatures = new LinkedList<JointInspectionData>();
  private InspectionFamilyEnum _inspectionFamilyEnum;
  private Board _board;
  private Component _component;
  private ReconstructionRegionTypeEnum _reconstructionRegionTypeEnum;
  private AlignmentGroup _alignmentGroup;
  private FocusPriorityEnum _focusPriority;
  private Map<Subtype, List<JointInspectionData>> _subtypeToJointListMap;
  private double _rotationCorrectionInDegrees = 0.0;
  private AffineTransform _rotationCorrectionTransformation = null;
  private AffineTransform _rotationCorrectionTransformationWithResize = null;
  private JointTypeEnum _jointType = null;
  private boolean _causesInterferencePattern = false;
  private boolean _isAffectedByInterferencePattern = false;
  private Set<Component> _componentsThatCauseInterferencePattern = new HashSet<Component>();
  private RangeUtil _candidateStepRanges = new RangeUtil();
  private PanelRectangle _allowableAlignmentRegionInNanoMeters;
  private boolean _isParentRegionUseAutoPredictiveSliceHeight = false;  // To mark whether parent region is using Auto Predictive Slice Height
  private GlobalSurfaceModelEnum _globalSurfaceModelEnum;
  private transient VirtualLiveRegionSetting _virtualLiveSetting;
  private boolean _useToBuildGlobalSurfaceModel;
  private boolean _waitForKickStart = false;
  private boolean _isUsedForLearningExposure = false;
  private AtomicInteger _remainingProducerCount=new AtomicInteger();
  private int _totalProducerCount = 0;

  // Added by Seng Yew on 22-Apr-2011
//  private float _minimumGraylevel;
//  private float _maximumGraylevel;
  
  // Added by Lee Herng on 19-Oct-2011
  // When a component is selected to use PSP, the corresponding reconstruction region
  // will automatically set to not retest for failing/passing region.
  // By default, the values are true so that we will not affect the existing flow of
  // FocusConfirmation
  private boolean _retestForFailingJoint = true;
  private boolean _retestForPassingJoint = true;
  
  // Added by Chnee Khang Wah, 24-Mar-2013
  // Improved IC calculation
  private List<Integer> _effectivePitchInNanos = new ArrayList<Integer>();
  private List<Double> _zAffectingRegionInNanos = new ArrayList<Double>();
  double _zAffectedRegionInNano = 0;
  Map<Integer,Double> _stepSizeInMilsToEntropyMap = new HashMap<Integer,Double>();
  
  // Added by Chnee Khang Wah, 2013-08-08, Real-time PSH
  private List<ReconstructionRegion> _consumerRegionList = new ArrayList<ReconstructionRegion>();
  private transient SurfaceModelBreakPoint _surfaceModelBreakPoint = null;

  // Added by Cheah Lee Herng, 04 Dec 2013
  private MeshTriangle _referenceMeshTriangle;
  private OpticalCameraRectangle _referenceOpticalCameraRectangle;
  
  //Siew Yeng - XCR-2218 - Increase inspectable pad size
  private static int _currentMaxInspectionRegionWidthInNanometers = -1;
  private static int _currentMaxInspectionRegionLengthInNanometers = -1;
  private static int _currentMaxHighMagInspectionRegionWidthInNanometers = -1;
  private static int _currentMaxHighMagInspectionRegionLengthInNanometers = -1;
  
  private List<Integer> _cameraEnabledList = new ArrayList<Integer>(Arrays.asList(XrayCameraIdEnum.XRAY_CAMERA_0.getId(),
                                                                                  XrayCameraIdEnum.XRAY_CAMERA_1.getId(),
                                                                                  XrayCameraIdEnum.XRAY_CAMERA_2.getId(),
                                                                                  XrayCameraIdEnum.XRAY_CAMERA_3.getId(),
                                                                                  XrayCameraIdEnum.XRAY_CAMERA_4.getId(),
                                                                                  XrayCameraIdEnum.XRAY_CAMERA_5.getId(),
                                                                                  XrayCameraIdEnum.XRAY_CAMERA_6.getId(),
                                                                                  XrayCameraIdEnum.XRAY_CAMERA_7.getId(),
                                                                                  XrayCameraIdEnum.XRAY_CAMERA_8.getId(),
                                                                                  XrayCameraIdEnum.XRAY_CAMERA_9.getId(),
                                                                                  XrayCameraIdEnum.XRAY_CAMERA_10.getId(),
                                                                                  XrayCameraIdEnum.XRAY_CAMERA_11.getId(),
                                                                                  XrayCameraIdEnum.XRAY_CAMERA_12.getId(),
                                                                                  XrayCameraIdEnum.XRAY_CAMERA_13.getId()));

  /**
   * @author Matt Wharton
   */
  public ReconstructionRegion()
  {
    ++_numRegions;
    _regionId = _numRegions;
    _globalSurfaceModelEnum = GlobalSurfaceModelEnum.AUTOFOCUS;

    _useToBuildGlobalSurfaceModel = false;

    _effectivePitchInNanos.clear();
    _zAffectingRegionInNanos.clear();
    _zAffectedRegionInNano = 0;
    _stepSizeInMilsToEntropyMap.clear();
//    // Added by Seng Yew on 22-Apr-2011
//    _minimumGraylevel = 0.0f;
//    _maximumGraylevel = 255.0f;
  }

  /**
  * @author Roy Williams
  */
  public void clearScanPasses()
  {
    if (_scanPasses != null)
    {
      // Added by Lee Herng - Clear the reference of ScanPassBreakPoint
      for(ScanPass scanPass : _scanPasses)
          scanPass.clearBreakPoint();
          
      _scanPasses.clear();
    }
  }

  /**
   * @author Yong Sheng Chuan
   */
  public void clearScanPasses(ScanPassSetting scanPassSetting)
  {
    Assert.expect(scanPassSetting != null);
    Set<ScanPass> scanPassToRemoved = new TreeSet<ScanPass>(getScanPasses());
    
    if (_scanPasses != null)
    {
      for(ScanPass scanPass : scanPassToRemoved)
      {
        ScanPassSetting currentScanPassSetting = new ScanPassSetting(scanPass.getStageSpeedValue(), 
                                                                     scanPass.getUserGainValue(), 
                                                                     scanPass.isLowMagnification());
        if (currentScanPassSetting.equals(scanPassSetting))
        {
          scanPass.clearBreakPoint();
          removeScanPass(scanPass);
        }
      }
    }
  }
  
  /**
  * @author Chnee Khang Wah
  */
  public void removeScanPass(ScanPass scanPass)
  {
    Assert.expect(scanPass != null);
    Assert.expect(_scanPasses != null);
    
    if (_scanPasses.contains(scanPass))
      _scanPasses.remove(scanPass);
  }
  
  /**
  * @author Bob Balliew
  */
  public void addScanPass(ScanPass scanPass)
  {
    Assert.expect(scanPass != null);
    if (_scanPasses == null)
      _scanPasses = new ArrayList<ScanPass>();
    
    if (_scanPasses.contains(scanPass) == false)
      _scanPasses.add(scanPass);
  }

  /**
  * @author Bob Balliew
  */
  public List<ScanPass> getScanPasses()
  {
    if (_scanPasses == null)
      _scanPasses = new ArrayList<ScanPass>();
    else if (_scanPasses.size() > 1)
      Collections.sort(_scanPasses, ScanPass.getScanPassComparator());
    
    return _scanPasses;
  }

  /**
  * @author Matt Wharton
  */
  public boolean isTopSide()
  {
    Assert.expect( _isTopSide != null );

    return _isTopSide.getValue();
  }

  /**
  * @author Matt Wharton
  */
  public void setTopSide( boolean topSide )
  {
    if (_isTopSide == null)
      _isTopSide = new BooleanRef(topSide);
    else
      _isTopSide.setValue(topSide);
  }

  /**
  * @author Rex Shang
  */
  public ImageReconstructionEngineEnum getReconstructionEngineId()
  {
    Assert.expect(_reconstructionEngineId != null);

    return _reconstructionEngineId;
  }

  /**
  * @author Rex Shang
  */
  public void setReconstructionEngineId( ImageReconstructionEngineEnum reconstructionEngineId )
  {
    Assert.expect(reconstructionEngineId != null);

    _reconstructionEngineId = reconstructionEngineId;
  }

  /**
   * Gets the PanelRectangle that encompasses this ReconstructionRegion in coordinates relative to the Panel origin.
   *
   * @author Peter Esbensen
   */
  public PanelRectangle getRegionRectangleRelativeToPanelInNanoMeters()
  {
    Assert.expect( _regionRectangleRelativeToPanelInNanoMeters != null );
    return _regionRectangleRelativeToPanelInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public PanelCoordinate getCenterCoordinateRelativeToPanelInNanoMeters()
  {
    Assert.expect( _regionRectangleRelativeToPanelInNanoMeters != null );
    return new PanelCoordinate(_regionRectangleRelativeToPanelInNanoMeters.getCenterX(),
                               _regionRectangleRelativeToPanelInNanoMeters.getCenterY());
  }

  /**
   * Gets an ImageRectangle that encompasses this ReconstructionRegion.  The (x,y) location of the region is meaningless;
   * the valuable information is the dimensions.
   *
   * @author Matt Wharton
   */
  public ImageRectangle getRegionRectangleInPixels()
  {
    Assert.expect(_regionRectangleRelativeToPanelInNanoMeters != null);
    
    if(_testSubProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
    {
      int width = (int)Math.ceil(_regionRectangleRelativeToPanelInNanoMeters.getWidth() * 1f / MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
      int height = (int)Math.ceil(_regionRectangleRelativeToPanelInNanoMeters.getHeight() * 1f / MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
      ImageRectangle regionInPixels = new ImageRectangle(0, 0, width, height);

      return regionInPixels;
    }
    else
    {
      int width = (int)Math.ceil(_regionRectangleRelativeToPanelInNanoMeters.getWidth() * 1f / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel());
      int height = (int)Math.ceil(_regionRectangleRelativeToPanelInNanoMeters.getHeight() * 1f / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel());
      ImageRectangle regionInPixels = new ImageRectangle(0, 0, width, height);

      return regionInPixels;
    }
  }

  /**
   * Gets an ImageRectangle that encompasses this ReconstructionRegion after rotation correction has been applied.
   * The (x,y) location of the region is meaningless; the valuable information is the dimensions.
   *
   * @author Matt Wharton
   * @author Lim, Lay Ngor - Hana Solder Bump
   * Please beware that this function does not perform resize handling as the caller now is from alignment region
   * which does not require resize on the original images.
   */
  public ImageRectangle getOrthogonalRegionRectangleInPixels()
  {
    ImageRectangle regionInPixels = getRegionRectangleInPixels();
    ImageRectangle correctedRegionInPixels = regionInPixels;
    if (hasRotationCorrectionTransformation())
    {
      AffineTransform rotationCorrectionTransform = getRotationCorrectionTransformation();
      Rectangle2D originalRegionRectangle2D = new Rectangle2D.Double(0, 0, regionInPixels.getWidth(), regionInPixels.getHeight());
      java.awt.Shape rotationCorrectedShape = rotationCorrectionTransform.createTransformedShape(originalRegionRectangle2D);
      correctedRegionInPixels = new ImageRectangle(rotationCorrectedShape);
    }

    return correctedRegionInPixels;
  }

  /**
   * Sets the PanelRectangle which encompasses this ReconstructionRegion.
   *
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void setRegionRectangleRelativeToPanelInNanoMeters( PanelRectangle regionRectangleRelativeToPanelInNanoMeters )
  {
    Assert.expect( regionRectangleRelativeToPanelInNanoMeters != null );

    _regionRectangleRelativeToPanelInNanoMeters = regionRectangleRelativeToPanelInNanoMeters;
  }

  /**
   * @author Peter Esbensen
   */
  public void setAlignmentGroup(AlignmentGroup alignmentGroup)
  {
    Assert.expect(alignmentGroup != null);
    _alignmentGroup = alignmentGroup;
  }

  /**
   * @author Dave Ferguson
   */
  public void setFocusPriority(FocusPriorityEnum focusPriority)
  {
    Assert.expect(focusPriority != null);
    _focusPriority = focusPriority;
  }

  /**
   * @author Dave Ferguson
   */
  public FocusPriorityEnum getFocusPriority()
  {
    Assert.expect(_focusPriority != null);
    return _focusPriority;
  }

  /**
   * Provides a textual description of this InspectionRegion.  For use in debugging.
   *
   *
   * @author Peter Esbensen
   */
  public String toString()
  {
    Assert.expect(_jointInspectionDataList != null);

    String description = "Region ID: " + getRegionId();

    description = description + " Pads: ";

    for(JointInspectionData jointInspectionData : _jointInspectionDataList)
    {
      String componentName = jointInspectionData.getComponent().getReferenceDesignator();
      String padName = jointInspectionData.getPad().getName();

      description = description + componentName + "-" + padName + " ";
    }

    Assert.expect(description != null);

    return description;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getFullDescription()
  {
    LocalizedString desc = new LocalizedString("BUS_RECONSTRUCTION_REGION_FULL_DESCRIPTION_KEY",
                                               new Object[]
                                               {getComponent().getReferenceDesignator(),
                                               getPadDescription()});
    return StringLocalizer.keyToString(desc);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getPadDescription()
  {
    int padLimit = 6;
    String description = "";
    int totalPads = getPads().size();
    int currentPadNumber = 0;
    Iterator<Pad> it = getPads().iterator();

    while (it.hasNext() && (currentPadNumber < padLimit))
    {
      Pad pad = it.next();
      currentPadNumber++;
      description += pad.getName() + ", ";
    }
    // remove the last comma
    description = description.substring(0, description.lastIndexOf(","));
    if (description.length() > 40)
    {
      description = description.substring(0, 40);
      description = description + " ...";
    }
    else if (totalPads >= padLimit)
    {
      description = description + " ...";
    }
    return description;
  }

  /**
   * @return A list of features comprising this ReconstructionRegion
   * @author Peter Esbensen
   */
  public List<Feature> getFeatures()
  {
    return new LinkedList<Feature>(getJointInspectionDataList());
  }

  /**
   * @author Peter Esbensen
   */
  public List<FiducialInspectionData> getFiducialInspectionDataList()
  {
    Assert.expect(_fiducialInspectionDataList != null);
    return _fiducialInspectionDataList;
  }

  /**
   * @author George A. David
   */
  public List<Fiducial> getFiducials()
  {
    Assert.expect(isAlignmentRegion());

    List<Fiducial> fiducials = new LinkedList<Fiducial>();
    for(FiducialInspectionData data : _fiducialInspectionDataList)
      fiducials.add(data.getFiducial());

    return fiducials;
  }

  /**
   * @return A list of JointInspectionData objects comprising this InspectionRegion
   * @author Peter Esbensen
   */
  public List<JointInspectionData> getJointInspectionDataList()
  {
    Assert.expect(_jointInspectionDataList != null);

    return _jointInspectionDataList;
  }

  /**
   * @return A list of JointInspectionData objects comprising this InspectionRegion
   * @author Peter Esbensen
   * @author sham
   */
  public List<JointInspectionData> getJointInspectionDataWithinBoundList()
  {
    Assert.expect(_jointInspectionDataWithinBoundList != null);
    return _jointInspectionDataWithinBoundList;
  }

  /**
   * @author George A. David
   * @author Peter Esbensen
   */
  public List<JointInspectionData> getInspectableJointInspectionDataList(Subtype subtype)
  {
    Assert.expect(subtype != null);
    Assert.expect(_jointInspectionDataList != null);
    List<JointInspectionData> dataList = new LinkedList<JointInspectionData>();
    for(JointInspectionData jointData : _jointInspectionDataList)
    {
      if ( (jointData.getSubtype() == subtype) && (jointData.getPad().isInspected()) )
        dataList.add(jointData);
    }

    return dataList;
  }

  /**
   * @author George A. David
   * @author Peter Esbensen
   */
  public List<JointInspectionData> getJointInspectionDataList(Subtype subtype)
  {
    Assert.expect(subtype != null);
    Assert.expect(_jointInspectionDataList != null);
    List<JointInspectionData> dataList = new LinkedList<JointInspectionData>();
    for(JointInspectionData jointData : _jointInspectionDataList)
    {
      if (jointData.getSubtype() == subtype)
        dataList.add(jointData);
    }

    return dataList;
  }


  /**
   * Get a mapping of the subtypes in this region to the JointInspectionData objects
   * that are of those subtypes.
   *dev
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public Map<Subtype, List<JointInspectionData>> getSubtypeToJointListMap()
  {
    Assert.expect(_jointInspectionDataList != null);

    if (_subtypeToJointListMap == null)
    {
      _subtypeToJointListMap = new HashMap<Subtype, List<JointInspectionData>>();

      for (JointInspectionData jointInspectionData : _jointInspectionDataList)
      {
        Subtype subtype = jointInspectionData.getSubtype();
        if (_subtypeToJointListMap.containsKey(subtype))
        {
          _subtypeToJointListMap.get(subtype).add(jointInspectionData);
        }
        else
        {
          List<JointInspectionData> jointList = new LinkedList<JointInspectionData>();
          jointList.add(jointInspectionData);
          _subtypeToJointListMap.put(subtype, jointList);
        }
      }
    }

    return _subtypeToJointListMap;
  }

  /**
   * Get a mapping of the subtypes in this region to the inspectable JointInspectionData objects
   * that are of those subtypes.
   *
   * NOTE: We don't want to cache this map since 'test'/'no test' status can change between runs without
   * regenerating the program.  This makes sure we are always in sync.
   *
   * @author Matt Wharton
   */
  public Map<Subtype, List<JointInspectionData>> getSubtypeToInspectableJointListMap()
  {
    Map<Subtype, List<JointInspectionData>> subtypeToInspectableJointListMap = new HashMap<Subtype,List<JointInspectionData>>();

    for (Map.Entry<Subtype, List<JointInspectionData>> mapEntry : getSubtypeToJointListMap().entrySet())
    {
      // Create a sub-list of all the inspectable joints in the subtype.
      List<JointInspectionData> inspectableJoints = new LinkedList<JointInspectionData>();
      for (JointInspectionData jointInspectionData : mapEntry.getValue())
      {
        if (jointInspectionData.getPad().isInspected())
        {
          inspectableJoints.add(jointInspectionData);
        }
      }

      // If we have some inspectable joints, add an entry to our map.
      if (inspectableJoints.isEmpty() == false)
      {
        Subtype subtype = mapEntry.getKey();
        subtypeToInspectableJointListMap.put(subtype, inspectableJoints);
      }
    }

    return subtypeToInspectableJointListMap;
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   */
  public Collection<Subtype> getSubtypes()
  {
    return getSubtypeToJointListMap().keySet();
  }

  /**
   * Add the given JointInspectionData to this InspectionRegion.  We only expect Program Generation to
   * call this method.
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void addJointInspectionData(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(_jointInspectionDataList != null);

    jointInspectionData.setInspectionRegion(this);

    if (_jointInspectionDataList.isEmpty() && _fiducialInspectionDataList.isEmpty())
    {
      _board = jointInspectionData.getBoard();
      if (isAlignmentRegion() == false)
      {
        _component = jointInspectionData.getComponent();
        _inspectionFamilyEnum = jointInspectionData.getInspectionFamilyEnum();
        _jointType = jointInspectionData.getJointTypeEnum();
      }
      _boundingRegionOfPadsInNanoMeters = new PanelRectangle(jointInspectionData.getPad().getShapeRelativeToPanelInNanoMeters());
      _regionRectangleRelativeToPanelInNanoMeters = new PanelRectangle(jointInspectionData.getRegionRectangleRelativeToPanelOriginInNanoMeters());
      setRotationCorrectionFromJoint(jointInspectionData);
    }
    else
    {
      Assert.expect(_board == jointInspectionData.getBoard());
      if (isAlignmentRegion() == false)
      {
        Assert.expect(_component == jointInspectionData.getComponent());
        Assert.expect(_inspectionFamilyEnum.equals(jointInspectionData.getInspectionFamilyEnum()));
        Assert.expect(_jointType.equals(jointInspectionData.getJointTypeEnum()));
      }
      _regionRectangleRelativeToPanelInNanoMeters.add(jointInspectionData.getRegionRectangleRelativeToPanelOriginInNanoMeters());
      _boundingRegionOfPadsInNanoMeters.addShape(jointInspectionData.getPad().getShapeRelativeToPanelInNanoMeters());
    }

    _jointInspectionDataList.add(jointInspectionData);
  }
  
  /**
   * Add the given JointInspectionData to this InspectionRegion.  We only expect Program Generation to
   * call this method.
   * @author sham
   */
  public void addJointInspectionDataWithinBound(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(_jointInspectionDataWithinBoundList != null);

    jointInspectionData.setInspectionRegion(this);
    _jointInspectionDataWithinBoundList.add(jointInspectionData);
  }

  /**
   * Clear all joint inspection data in list
   * @author sham
   */
  public void clearJointInspectionDataWithinBoundList()
  {
    Assert.expect(_jointInspectionDataWithinBoundList!=null);
    
    _jointInspectionDataWithinBoundList.clear();
  }

  /**
   * @author Peter Esbensen
   */
  public void addAlignmentFeature(Pad pad)
  {
    Assert.expect(pad != null);
    JointInspectionData jointInspectionData = new JointAlignmentData(pad);
    addJointInspectionData(jointInspectionData);
  }

  /**
   * @author Peter Esbensen
   */
  public void addAlignmentFeatureWithinBound(Pad pad)
  {
    Assert.expect(pad != null);
    JointInspectionData jointInspectionData = new JointAlignmentData(pad);
    addJointInspectionDataWithinBound(jointInspectionData);
  }

  /**
   * @author Peter Esbensen
   */
  public void addAlignmentFeature(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);
    FiducialInspectionData fiducialInspectionData = new FiducialInspectionData(fiducial);

    fiducialInspectionData.setAlignmentRegion(this);

    if (_fiducialInspectionDataList.isEmpty() && _jointInspectionDataList.isEmpty())
    {
      _boundingRegionOfPadsInNanoMeters = new PanelRectangle(fiducial.getShapeRelativeToPanelInNanoMeters());
      _regionRectangleRelativeToPanelInNanoMeters = new PanelRectangle(fiducialInspectionData.getRegionRectangleRelativeToPanelOriginInNanoMeters());
    }
    else
    {
      _boundingRegionOfPadsInNanoMeters.addShape(fiducial.getShapeRelativeToPanelInNanoMeters());
      _regionRectangleRelativeToPanelInNanoMeters.add(fiducialInspectionData.getRegionRectangleRelativeToPanelOriginInNanoMeters());
    }
//    generateRegionRectangleForAlignmentRegion();

    _fiducialInspectionDataList.add( fiducialInspectionData );
  }

  /**
   * @author Matt Wharton
   */
  public Collection<JointInspectionData> getAlignmentContextFeatures()
  {
    Assert.expect(_alignmentContextFeatures != null);

    return _alignmentContextFeatures;
  }

  /**
   * @author Matt Wharton
   */
  public void setAlignmentContextFeatures(Collection<JointInspectionData> alignmentContextFeatures)
  {
    Assert.expect(alignmentContextFeatures != null);

    _alignmentContextFeatures = alignmentContextFeatures;
  }

  /**
   * Removes the specified JointInspectionData from this InspectionRegion.
   *
   * @author Matt Wharton
   */
  public void removeJointInspectionData(JointInspectionData jointInspectionData)
  {
    Assert.expect( jointInspectionData != null );
    Assert.expect( _jointInspectionDataList.contains( jointInspectionData ) );

    jointInspectionData.clearInspectionRegion();
    _jointInspectionDataList.remove( jointInspectionData );

    // Rebuild a new bounding rectangle for the region that no longer includes this JointInspectionData.
    _regionRectangleRelativeToPanelInNanoMeters.setRect( 0, 0, 0, 0 );
    if ( _jointInspectionDataList.size() > 0 )
    {
      JointInspectionData initialJoint = (JointInspectionData)_jointInspectionDataList.get( 0 );
      _regionRectangleRelativeToPanelInNanoMeters = new PanelRectangle( initialJoint.getRegionRectangleRelativeToPanelOriginInNanoMeters() );

      for (JointInspectionData currJointInspectionData : _jointInspectionDataList)
      {
        _regionRectangleRelativeToPanelInNanoMeters.add( currJointInspectionData.getRegionRectangleRelativeToPanelOriginInNanoMeters() );
      }
    }
  }

  /**
   * Removes the given features from this ReconstructionRegion and puts
   * them in a new one.  This new ReconstructionRegion is returned.  Should
   * probably assert if a given feature is not in the given ReconstructionRegion.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public ReconstructionRegion moveFeaturesToNewReconstructionRegion(List<Feature> features)
  {
    Assert.expect( features != null );
    Assert.expect(features.size() > 0);
    Assert.expect(isAlignmentRegion() == false);
    ReconstructionRegion newReconstructionRegion = new ReconstructionRegion();
    newReconstructionRegion.setReconstructionRegionTypeEnum(_reconstructionRegionTypeEnum);
    newReconstructionRegion.setTopSide(_isTopSide.getValue());
    newReconstructionRegion.setRegionRectangleRelativeToPanelInNanoMeters(_regionRectangleRelativeToPanelInNanoMeters);

    // Iterate thru the the list of features and add them to the new reconstruction region while removing them
    // from the existing reconstruction region.  Also calculate a bounding rectangle for these features.
    for(Feature currentFeature : features)
    {
      Assert.expect( _jointInspectionDataList.contains( currentFeature ) );
      Assert.expect( currentFeature instanceof JointInspectionData );  // a little ghetto, but reasonable.
      JointInspectionData currentJointInspectionData = (JointInspectionData)currentFeature;
      removeJointInspectionData( currentJointInspectionData );
      newReconstructionRegion.addJointInspectionData( currentJointInspectionData );
    }

    Assert.expect(newReconstructionRegion != null);

    return newReconstructionRegion;
  }

  /**
   * @author George A. David
   */
  public Component getComponent()
  {
    Assert.expect(_component != null);

    return _component;
  }

  /**
   * @author George A. David
   */
  public boolean hasComponent()
  {
    return _component != null;
  }


  /**
   * @author George A. David
   */
  public boolean isPadInRegion(Pad pad)
  {
    Assert.expect(pad != null);

    boolean isPadInRegion = false;

    if(pad.getComponent().getBoard() == _board &&
       pad.getComponent() == _component)
    {
      for (JointInspectionData jointInspectionData : _jointInspectionDataList)
      {
        if (jointInspectionData.getPad() == pad)
        {
          isPadInRegion = true;
          break;
        }
      }
    }

    return isPadInRegion;
  }

  /**
   * @author George A. David
   */
  public JointTypeEnum getJointTypeEnum()
  {
    Assert.expect(_jointType != null);

    return _jointType;
  }

  /**
   * @author George A. David
   */
  public Collection<Pad> getPads()
  {
    Collection<Pad> pads = new LinkedList<Pad>();

    for (JointInspectionData jointInspectionData : _jointInspectionDataList)
      pads.add(jointInspectionData.getPad());

    return pads;
  }
  
  /**
   * XCR-3403, Total joint processed is different by using TIFF image
   * @author Yong Sheng Chuan
   */
  public Collection<Pad> getInspectablePads()
  {
    Collection<Pad> pads = new LinkedList<Pad>();

    for (JointInspectionData jointInspectionData : _jointInspectionDataList)
    {
      if(jointInspectionData.getPad().isInspected())
        pads.add(jointInspectionData.getPad());
    }
    return pads;
  }

  /**
   * @author George A. David
   */
  public boolean hasSubtype(Subtype subtype)
  {
    for(JointInspectionData inspectionData : _jointInspectionDataList)
    {
      if(inspectionData.getSubtype() == subtype)
        return true;
    }

    return false;
  }

  /**
   * @author George A. David
   */
  public InspectionFamilyEnum getInspectionFamilyEnum()
  {
    Assert.expect(_inspectionFamilyEnum != null);

    return _inspectionFamilyEnum;
  }

  /**
   * @author George A. David
   */
  public Board getBoard()
  {
    Assert.expect(_board != null);

    return _board;
  }

  /**
   * @return the region id
   *
   * @author Peter Esbensen
   */
  public int getRegionId()
  {
    return _regionId;
  }

  /**
   * set the region id
   *
   * @author Peter Esbensen
   */
  public void setRegionId(int id)
  {
    _regionId = id;
  }

  /**
   * @author Michael Tutkowski
   */
  public int getNumberOfSlices()
  {
    Assert.expect(_focusGroups != null);

    int numberOfSlices = 0;
    for (FocusGroup focusGroup : _focusGroups)
    {
      numberOfSlices += focusGroup.getNumberOfSlices();
    }

    return numberOfSlices;
  }

  /**
   * @author Roy Williams
   */
  public int getNumberOfSlicesToBeReconstructed()
  {
    Assert.expect(_focusGroups != null);

    int numberOfSlices = 0;
    for (FocusGroup focusGroup : _focusGroups)
    {
      for (Slice slice : focusGroup.getSlices())
      {
        if (slice.createSlice() && 
            (slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION)||slice.getSliceType().equals(SliceTypeEnum.PROJECTION))) // Chnee Khang Wah, 2012-02-21, 2.5D
        {
          numberOfSlices++;
        }
      }
    }

    return numberOfSlices;
  }

  /**
   * @author Michael Tutkowski
   */
  public void addFocusGroup(FocusGroup focusGroup)
  {
    Assert.expect(focusGroup != null);
    Assert.expect(_focusGroups != null);

    if(_surfaceModelingFocusRegion != null)
      Assert.expect(focusGroup.hasSurfaceModelingSlice() == false, "component : " + _component.getReferenceDesignator());
    else if(focusGroup.hasSurfaceModelingSlice())
      _surfaceModelingFocusRegion = focusGroup.getFocusSearchParameters().getFocusRegion();

    _focusGroups.add(focusGroup);
  }

  /**
   * @author Michael Tutkowski
   */
  public List<FocusGroup> getFocusGroups()
  {
    Assert.expect(_focusGroups != null);

    return _focusGroups;
  }

  /**
   * @author Horst Mueller
   * @author Matt Wharton
   */
  public boolean isAlignmentRegion()
  {
    Assert.expect(_reconstructionRegionTypeEnum != null);

    return (_reconstructionRegionTypeEnum.equals(ReconstructionRegionTypeEnum.ALIGNMENT));
  }

  /**
   * @author Horst Mueller
   * @author Matt Wharton
   */
  public boolean isVerificationRegion()
  {
    Assert.expect(_reconstructionRegionTypeEnum != null);

    return (_reconstructionRegionTypeEnum.equals(ReconstructionRegionTypeEnum.VERIFICATION));
  }

  /**
   * @author George A. David
   */
  public boolean hasJoints()
  {
    return _jointInspectionDataList.isEmpty() == false;
  }

  /**
   * @author George A. David
   */
  public static void resetRegionCounter()
  {
    _numRegions = 0;
  }

  /**
   * @author George A. David
   */
  public void setFocusGroups(List<FocusGroup> focusGroups)
  {
    Assert.expect(focusGroups != null);

    _focusGroups = focusGroups;
  }

  /**
   * @author Matt Wharton
   */
  private static Config getConfig()
  {
    if (_config == null)
    {
      _config = Config.getInstance();
    }

    return _config;
  }

  /**
   * @author George A. David
   */
  public static int getMaxInspectionRegionSizeDueToWarpInNanoMeters()
  {
    return getConfig().getIntValue(HardwareConfigEnum.MAX_RECONSTRUCITON_REGION_SIZE_DUE_TO_WARP_LIMITS_IN_NANOMETERS);
  }

  /**
   * @author George A. David
   */
  private static int getMaxReconstructionRegionSizeDueToCameraInNanoMeters()
  {
    return getConfig().getIntValue(HardwareConfigEnum.MAX_RECONSTRUCTION_REGION_SIZE_DUE_TO_CAMERA_LIMITS_IN_NANOMETERS);
  }

  /**
   * @author George A. David
   */
  public static int getMaxInspectionRegionSizeInNanoMeters()
  {
    return Math.min(getMaxInspectionRegionSizeDueToWarpInNanoMeters(), getMinInspectionRegionDimensionInNanoMeters());
//    return _MAX_RECONSTRUCTION_REGION_LENGTH_IN_NANOMETERS;
  }

  /**
   * @author George A. David
   */
  private static int getMinInspectionRegionDimensionInNanoMeters()
  {
    return Math.min(getMaxInspectionRegionLengthInNanoMeters(), getMaxInspectionRegionWidthInNanoMeters());
  }

  /**
   * @author Siew Yeng
   */
  public static void setCurrentMaxInspectionRegionWidthInNanoMeters(int maxWidth)
  {
    _currentMaxInspectionRegionWidthInNanometers = maxWidth;
    _currentMaxHighMagInspectionRegionWidthInNanometers = maxWidth;
  }
  
  /**
   * @author George A. David
   */
  public static int getMaxInspectionRegionWidthInNanoMeters()
  {
    //Siew Yeng
    //return _MAX_INSPECTION_REGION_WIDTH_IN_NANOMETERS;
    
    if(_currentMaxInspectionRegionWidthInNanometers == -1)
      _currentMaxInspectionRegionWidthInNanometers = getDefaultMaxInspectionRegionWidthInNanoMeters();
      
    return _currentMaxInspectionRegionWidthInNanometers; 
  }

  /**
   * @author Siew Yeng
   */
  public static int getDefaultMaxInspectionRegionWidthInNanoMeters()
  {
    return _MAX_INSPECTION_REGION_WIDTH_IN_NANOMETERS;
  }
  
  /**
   * @author Siew Yeng
   */
  public static void setCurrentMaxInspectionRegionLengthInNanoMeters(int maxLength)
  {
    _currentMaxInspectionRegionLengthInNanometers = maxLength;
    _currentMaxHighMagInspectionRegionLengthInNanometers = maxLength;
  }
  
  /**
   * @author George A. David
   */
  public static int getMaxInspectionRegionLengthInNanoMeters()
  {
    //Siew Yeng
    //return _MAX_INSPECTION_REGION_LENGTH_IN_NANOMETERS;
    
    if(_currentMaxInspectionRegionLengthInNanometers == -1)
      _currentMaxInspectionRegionLengthInNanometers = getDefaultMaxInspectionRegionLengthInNanoMeters();
      
    return _currentMaxInspectionRegionLengthInNanometers;
  }
  
  /**
   * @author Siew Yeng
   */
  public static int getDefaultMaxInspectionRegionLengthInNanoMeters()
  {
    return _MAX_INSPECTION_REGION_LENGTH_IN_NANOMETERS;
  }

  /**
   * @author George A. David
   */
  public static int getMaxInspectionRegionPadBoundsWidthInNanoMeters()
  {
    //Siew Yeng
    //return _MAX_INSPECTION_REGION_PAD_BOUNDS_WIDTH_IN_NANOMETERS;
    return getMaxInspectionRegionWidthInNanoMeters() - (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters());
  }

  /**
   * @author George A. David
   */
  public static int getMaxInspectionRegionPadBoundsLengthInNanoMeters()
  {
    //Siew Yeng
    //return _MAX_INSPECTION_REGION_PAD_BOUNDS_LENGTH_IN_NANOMETERS;
    return getMaxInspectionRegionLengthInNanoMeters() - (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters());
  }

  /**
   * @author George A. David
   */
  public static int getMaxAlignmentRegionWidthInNanoMeters()
  {
    return _MAX_ALIGNMENT_REGION_WIDTH_IN_NANOMETERS;
  }

  /**
   * @author George A. David
   */
  public static int getMaxAlignmentRegionLengthInNanoMeters()
  {
    return _MAX_ALIGNMENT_REGION_LENGTH_IN_NANOMETERS;
  }

  /**
   * @author George A. David
   */
  public static int getMaxAlignmentFeatureWidthInNanoMeters()
  {
    return _MAX_ALIGNMENT_FEATURE_WIDTH_IN_NANOMETERS;
  }

  /**
   * @author George A. David
   */
  public static int getMaxAlignmentFeatureLengthInNanoMeters()
  {
    return _MAX_ALIGNMENT_FEATURE_LENGTH_IN_NANOMETERS;
  }


  /**
   * @author George A. David
   */
  public static int getAlignmentFeatureLengthBufferInNanoMeters()
  {
    return _ALIGNMENT_FEATURE_LENGTH_BUFFER_IN_NANOMETERS;
  }

  /**
   * @author George A. David
   */
  public static int getMaxVerificationRegionWidthInNanoMeters()
  {
    return _MAX_VERIFICATION_REGION_WIDTH_IN_NANOMETERS;
  }

  /**
   * @author George A. David
   */
  public static int getMaxVerificationRegionLengthInNanoMeters()
  {
    return _MAX_VERIFICATION_REGION_LENGTH_IN_NANOMETERS;
  }
  
  /**
   * @author Wei Chin
   */
  public static int getMaxHighMagVerificationRegionWidthInNanoMeters()
  {
    return _MAX_HIGH_MAG_VERIFICATION_REGION_WIDTH_IN_NANOMETERS;
  }

  /**
   * @author Wei Chin
   */
  public static int getMaxHighMagVerificationRegionLengthInNanoMeters()
  {
    return _MAX_HIGH_MAG_VERIFICATION_REGION_LENGTH_IN_NANOMETERS;
  }
  
  /**
   * @author Wei Chin
   */
  public static int getMaxHighMagInspectionRegionPadBoundsWidthInNanoMeters()
  {
    //Siew Yeng
    //return _MAX_HIGH_MAG_INSPECTION_REGION_PAD_BOUNDS_WIDTH_IN_NANOMETERS;
    return getMaxHighMagInspectionRegionWidthInNanoMeters() - (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters());
  }

  /**
   * @author Wei Chin
   */
  public static int getMaxHighMagInspectionRegionPadBoundsLengthInNanoMeters()
  {
    //Siew Yeng
    //return _MAX_HIGH_MAG_INSPECTION_REGION_PAD_BOUNDS_LENGTH_IN_NANOMETERS;
    return getMaxHighMagInspectionRegionLengthInNanoMeters()- (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters());
  }

/**
   * @author Wei Chin
   */
  public static int getMaxHighMagInspectionRegionSizeInNanoMeters()
  {
    return Math.min(getMaxHighMagInspectionRegionSizeDueToWarpInNanoMeters(), getMinHighMagInspectionRegionDimensionInNanoMeters());
//    return _MAX_RECONSTRUCTION_REGION_LENGTH_IN_NANOMETERS;
  }
  
  /**
   * @author Wei CHin
   */
  private static int getMinHighMagInspectionRegionDimensionInNanoMeters()
  {
    return Math.min(getMaxHighMagInspectionRegionLengthInNanoMeters(), getMaxHighMagInspectionRegionWidthInNanoMeters());
  }

  /**
   * @author Wei CHin
   */
  public static int getMaxHighMagInspectionRegionWidthInNanoMeters()
  {
    //Siew Yeng
    //return _MAX_HIGH_MAG_INSPECTION_REGION_WIDTH_IN_NANOMETERS;
    
    if(_currentMaxHighMagInspectionRegionWidthInNanometers == -1)
      _currentMaxHighMagInspectionRegionWidthInNanometers = getDefaultMaxHighMagInspectionRegionWidthInNanoMeters();
      
    return _currentMaxHighMagInspectionRegionWidthInNanometers;
  }
  
  /**
   * @author Siew Yeng
   */
  public static int getDefaultMaxHighMagInspectionRegionWidthInNanoMeters()
  {
    return _MAX_HIGH_MAG_INSPECTION_REGION_WIDTH_IN_NANOMETERS;
  }

  /**
   * @author Wei Chin
   */
  public static int getMaxHighMagInspectionRegionSizeDueToWarpInNanoMeters()
  {
    int lowMagInspectionRegionSizeDueToWarpInNanoMeters = getConfig().getIntValue(HardwareConfigEnum.MAX_RECONSTRUCITON_REGION_SIZE_DUE_TO_WARP_LIMITS_IN_NANOMETERS);
    return (int)Math.ceil(lowMagInspectionRegionSizeDueToWarpInNanoMeters / 1.6);
  }
  
  /**
   * @author Wei CHin 
   */
  public static int getMaxHighMagInspectionRegionLengthInNanoMeters()
  {
    //Siew Yeng
    //return _MAX_HIGH_MAG_INSPECTION_REGION_LENGTH_IN_NANOMETERS;
    
    if(_currentMaxHighMagInspectionRegionLengthInNanometers == -1)
      _currentMaxHighMagInspectionRegionLengthInNanometers = getDefaultMaxHighMagInspectionRegionLengthInNanoMeters();
    
    return _currentMaxHighMagInspectionRegionLengthInNanometers;
  }
  
  /**
   * @author Siew Yeng
   */
  public static int getDefaultMaxHighMagInspectionRegionLengthInNanoMeters()
  {
    return _MAX_HIGH_MAG_INSPECTION_REGION_LENGTH_IN_NANOMETERS;
  }
  
  /**
   * @author Wei Chin
   */
  public static int getMaxHighMagAlignmentRegionWidthInNanoMeters()
  {
    return _MAX_HIGH_MAG_ALIGNMENT_REGION_WIDTH_IN_NANOMETERS;
  }

  /**
   * @author Wei Chin
   */
  public static int getMaxHighMagAlignmentRegionLengthInNanoMeters()
  {
    return _MAX_HIGH_MAG_ALIGNMENT_REGION_LENGTH_IN_NANOMETERS;
  }

  /**
   * @author Wei Chin
   */
  public static int getMaxHighMagAlignmentFeatureWidthInNanoMeters()
  {
    return _MAX_HIGH_MAG_ALIGNMENT_FEATURE_WIDTH_IN_NANOMETERS;
  }

  /**
   * @author Wei Chin
   */
  public static int getMaxHighMagAlignmentFeatureLengthInNanoMeters()
  {
    return _MAX_HIGH_MAG_ALIGNMENT_FEATURE_LENGTH_IN_NANOMETERS;
  } 

  /**
   * @author George A. David
   */
  public static boolean fitsInInspectionRegion(Pad pad, boolean isEnlargeReconstructionRegion)
  {
    Assert.expect(pad != null);

    boolean doesPadFit = false;

    PanelRectangle shapeBounds = null;
    if( InspectionFamily.isSpecialTwoPinDevice( pad.getSubtype().getInspectionFamilyEnum() ) )
    {
      // for chips, both pads must fit
      List<Pad> pads = pad.getComponent().getPads();
      Assert.expect(pads.size() == 2);
      for(Pad compPad : pads)
      {
        if(shapeBounds == null)
          shapeBounds = ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(compPad);
        else
          shapeBounds.add(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(compPad));
      }
    }
    else
      shapeBounds = ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad);

    Assert.expect(shapeBounds != null);
    if (shapeBounds.getWidth() <= getMaxInspectionRegionPadBoundsWidthInNanoMeters() &&
        shapeBounds.getHeight() <= getMaxInspectionRegionPadBoundsLengthInNanoMeters())
      doesPadFit = true;
    else
    {
      //Siew Yeng - enlarge reconstruction region
      if(isEnlargeReconstructionRegion)
      {
        int newWidth = shapeBounds.getWidth() + (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters());
        int newLength = shapeBounds.getHeight()+ (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters());
        
        //Khaw Chek Hau - XCR3427: Assert happens after set the capacitor pads distance too far in adjust cad
        if (newWidth <= getMaxIRPInspectionRegionWidthInNanoMeters() && newLength <= getMaxIRPInspectionRegionLengthInNanoMeters())
        {
          if(newWidth > getMaxInspectionRegionPadBoundsWidthInNanoMeters())
          {
            setCurrentMaxInspectionRegionWidthInNanoMeters(newWidth);
          }
          
          if (newLength > getMaxInspectionRegionPadBoundsLengthInNanoMeters())
          {
            setCurrentMaxInspectionRegionLengthInNanoMeters(newLength);
          }
          doesPadFit = true;
        }
      }
    }
    
    if((shapeBounds.getWidth() >= getMaxHighMagInspectionRegionPadBoundsWidthInNanoMeters() ||
       shapeBounds.getHeight() >= getMaxHighMagInspectionRegionPadBoundsLengthInNanoMeters()) &&
       (shapeBounds.getWidth()) > getMaxHighMagIRPInspectionRegionWidthInNanoMeters() ||
        shapeBounds.getHeight() > getMaxHighMagIRPInspectionRegionLengthInNanoMeters())
    {
      pad.getSubtype().getSubtypeAdvanceSettings().setCanUseVariableMagnification(false);      
    }
    else
    {
      pad.getSubtype().getSubtypeAdvanceSettings().setCanUseVariableMagnification(true);
    }

    return doesPadFit;
  }

  /**
   * @author George A. David
   */
  public static boolean fitsInAlignmentRegion(List<Pad> pads, List<Fiducial> fiducials)
  {
    Assert.expect(pads != null);
    Assert.expect(fiducials != null);

    PanelRectangle bounds = null;

    for(Pad pad : pads)
    {
      if (bounds == null)
//        bounds = new PanelRectangle(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad));
        bounds = new PanelRectangle(pad.getShapeRelativeToPanelInNanoMeters());
      else
//        bounds.add(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad));
        bounds.addShape(pad.getShapeRelativeToPanelInNanoMeters());

      if(bounds.getWidth() > _MAX_ALIGNMENT_FEATURE_WIDTH_IN_NANOMETERS||
         bounds.getHeight() > _MAX_ALIGNMENT_FEATURE_LENGTH_IN_NANOMETERS)
        return false;
    }

    for(Fiducial fiducial : fiducials)
    {
      if(bounds == null)
        bounds = new PanelRectangle(fiducial.getShapeRelativeToPanelInNanoMeters());
      else
        bounds.addShape(fiducial.getShapeRelativeToPanelInNanoMeters());
    }

    if(bounds.getWidth() > _MAX_ALIGNMENT_FEATURE_WIDTH_IN_NANOMETERS||
       bounds.getHeight() > _MAX_ALIGNMENT_FEATURE_LENGTH_IN_NANOMETERS)
      return false;

    return true;
  }
  
  /**
   * @author Wei Chin
   */
  public static boolean fitsInAlignmentRegionForHighMag(List<Pad> pads, List<Fiducial> fiducials)
  {
    Assert.expect(pads != null);
    Assert.expect(fiducials != null);

    PanelRectangle bounds = null;

    for(Pad pad : pads)
    {
      if (bounds == null)
//        bounds = new PanelRectangle(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad));
        bounds = new PanelRectangle(pad.getShapeRelativeToPanelInNanoMeters());
      else
//        bounds.add(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad));
        bounds.addShape(pad.getShapeRelativeToPanelInNanoMeters());

      if(bounds.getWidth() > _MAX_HIGH_MAG_ALIGNMENT_FEATURE_WIDTH_IN_NANOMETERS ||
         bounds.getHeight() > _MAX_HIGH_MAG_ALIGNMENT_FEATURE_LENGTH_IN_NANOMETERS)
        return false;
    }

    for(Fiducial fiducial : fiducials)
    {
      if(bounds == null)
        bounds = new PanelRectangle(fiducial.getShapeRelativeToPanelInNanoMeters());
      else
        bounds.addShape(fiducial.getShapeRelativeToPanelInNanoMeters());
    }

    if(bounds.getWidth() > _MAX_HIGH_MAG_ALIGNMENT_FEATURE_WIDTH_IN_NANOMETERS ||
       bounds.getHeight() > _MAX_HIGH_MAG_ALIGNMENT_FEATURE_LENGTH_IN_NANOMETERS)
      return false;

    return true;
  }

  /**
   * @author George A. David
   */
  public static boolean fitsInInspectionRegion(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    boolean doesPadFit = false;

    PanelRectangle shapeBounds = ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundLandPatternPad(landPatternPad);
    if (shapeBounds.getWidth() <= getMaxInspectionRegionWidthInNanoMeters() &&
        shapeBounds.getHeight() <= getMaxInspectionRegionLengthInNanoMeters())
      doesPadFit = true;

    return doesPadFit;

  }

  /**
   * @author Peter Esbensen
   */
  public AlignmentGroup getAlignmentGroup()
  {
    Assert.expect(_alignmentGroup != null);
    return _alignmentGroup;
  }

  /**
   * @author Peter Esbensen
   */
  public List<Feature> getAlignmentFeatures()
  {
    Assert.expect(isAlignmentRegion());
    Assert.expect(_jointInspectionDataList != null);
    List<Feature> features = new LinkedList<Feature>(_jointInspectionDataList);
    features.addAll(_fiducialInspectionDataList);

    return features;
  }

  /**
   * @author George A. David
   */
  public PanelRectangle getAlignmentFeatureBoundsInNanoMeters()
  {
    Assert.expect(isAlignmentRegion());

    PanelRectangle boundsInNanoMeters = null;
    for(Feature feature : getAlignmentFeatures())
    {
      if (boundsInNanoMeters == null)
        boundsInNanoMeters = new PanelRectangle(feature.getRegionRectangleRelativeToPanelOriginInNanoMeters());
      else
        boundsInNanoMeters.add(feature.getRegionRectangleRelativeToPanelOriginInNanoMeters());
    }

    Assert.expect(boundsInNanoMeters != null);
    return boundsInNanoMeters;
  }

  /**
   * @author Peter Esbensen
   */
  public TestSubProgram getTestSubProgram()
  {
    Assert.expect(_testSubProgram != null);
    return _testSubProgram;
  }

  /**
   * @author Peter Esbensen
   */
  public void setTestSubProgram(TestSubProgram testSubProgram)
  {
    Assert.expect(testSubProgram != null);
    _testSubProgram = testSubProgram;
  }

  /**
   * Gets the alignment transform associated with this region.
   *
   * @author Horst Mueller
   */
  public AffineTransform getManualAlignmentMatrix()
  {
    Assert.expect(_testSubProgram != null);
    return _testSubProgram.getAggregateManualAlignmentTransform();
  }

  /**
   * @author Patrick Lacz
   */
  public double getRotationCorrectionInDegrees()
  {
    return _rotationCorrectionInDegrees;
  }

 /**
  * Set the rotation correction for the joint, rotate to the nearest multiple of 90 degrees.
  *
  * @author Patrick Lacz
  */
  private void setRotationCorrectionFromJoint(JointInspectionData joint)
  {
    Assert.expect(joint != null);

    double totalDegrees = joint.getPad().getDegreesRotationAfterAllRotations();
    double degreesMod90 = totalDegrees - 90.0 * Math.floor(totalDegrees / 90.0);

    if (degreesMod90 > 45.0)
      _rotationCorrectionInDegrees = 90.0 - degreesMod90;
    else
      _rotationCorrectionInDegrees = -degreesMod90;
  }

  /**
   * the bounding region of pads is region that only encompasses the pads
   * it does not account for total region needed the the image analysis subsystem.
   * @author George A. David
   */
  public PanelRectangle getBoundingRegionOfPadsInNanoMeters()
  {
    Assert.expect(_boundingRegionOfPadsInNanoMeters != null);

    return _boundingRegionOfPadsInNanoMeters;
  }

  /**
   * Gets the bounding region of the pads relative to the image (in pixels).
   *
   * @author Matt Wharton
   */
  public ImageRectangle getBoundingRegionOfPadsInPixels()
  {
    Assert.expect(_jointInspectionDataList != null);
    Assert.expect(_fiducialInspectionDataList != null);

    // Create an ImageRectangle that represents the bounding box (in pixels) of all pads and fiducials in the alignment
    // region.
    ImageRectangle boundingRegionOfPadsInPixels = null;

    // Add the alignment pads to the bounding box.
    for (JointInspectionData jointInspectionData : _jointInspectionDataList)
    {
      if (boundingRegionOfPadsInPixels == null)
      {
        boundingRegionOfPadsInPixels =
            new ImageRectangle(jointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels());
      }
      else
      {
        boundingRegionOfPadsInPixels.add(jointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels());
      }
    }

    // Add the alignment fiducials to the bounding box.
    for (FiducialInspectionData fiducialInspectionData : _fiducialInspectionDataList)
    {
      if (boundingRegionOfPadsInPixels == null)
      {
        boundingRegionOfPadsInPixels =
            new ImageRectangle(fiducialInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels());
      }
      else
      {
        boundingRegionOfPadsInPixels.add(fiducialInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels());
      }
    }

    Assert.expect(boundingRegionOfPadsInPixels != null);
    return boundingRegionOfPadsInPixels;
  }

  /**
   * @author George A. David
   */
  public List<Slice> getReconstructionSlices()
  {
    List<Slice> slices = new LinkedList<Slice>();

    for(FocusGroup focusGroup : _focusGroups)
    {
      for(Slice slice : focusGroup.getSlices())
      {
        if(slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION) || slice.getSliceType().equals(SliceTypeEnum.PROJECTION)) // Chnee Khang Wah, 2012-02-21, 2.5D
          slices.add(slice);
      }
    }

    return slices;
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   */
  public Collection<SliceNameEnum> getInspectedSliceNames(Subtype subtype)
  {
    Assert.expect(subtype != null);

    InspectionFamily inspectionFamily = subtype.getInspectionFamily();

    return inspectionFamily.getOrderedInspectionSlices(subtype);
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   */
  public Collection<SliceNameEnum> getInspectedSliceNames()
  {
    Collection<SliceNameEnum> inspectedSliceNames = new HashSet<SliceNameEnum>();
    for (Subtype subtype : getSubtypeToJointListMap().keySet())
    {
      inspectedSliceNames.addAll(getInspectedSliceNames(subtype));
    }

    return inspectedSliceNames;
  }

  /**
   * @author George A. David
   */
  public Collection<Slice> getInspectedSlices()
  {
    Collection<SliceNameEnum> sliceNames = getInspectedSliceNames();
    List<Slice> slices = new ArrayList<Slice>(sliceNames.size());

    for(FocusGroup group : _focusGroups)
    {
      for(Slice slice : group.getSlices())
      {
        if(sliceNames.contains(slice.getSliceName()))
          slices.add(slice);
      }
    }

    return slices;
  }
  
  /**
   * @author Chnee Khang Wah, 2012-02-29, 2.5D
   */
  public Collection<Slice> getInspectedAndMultiAngleSlices()
  {
    Collection<SliceNameEnum> sliceNames = getInspectedSliceNames();
    List<Slice> slices = new ArrayList<Slice>(sliceNames.size());

    for(FocusGroup group : _focusGroups)
    {
      for(Slice slice : group.getSlices())
      {
        if(sliceNames.contains(slice.getSliceName()) || slice.getSliceType().equals(SliceTypeEnum.PROJECTION))
          slices.add(slice);
      }
    }

    return slices;
  }

  /**
   * @author George A. David
   */
  public boolean hasSurfaceModelingSlice()
  {
    return _surfaceModelingFocusRegion != null;
  }

  /**
   * @author George A. David
   */
  public boolean hasSlice(SliceNameEnum sliceName)
  {
    for(FocusGroup group : _focusGroups)
    {
      if(group.hasSlice(sliceName))
        return true;
    }
    return false;
  }

  /**
   * @author George A. David
   */
  public Slice getSlice(SliceNameEnum sliceName)
  {
    Slice slice = null;
    for(FocusGroup group : _focusGroups)
    {
      if(group.hasSlice(sliceName))
        slice = group.getSlice(sliceName);
    }

    Assert.expect(slice != null);
    return slice;
  }

  /**
   * @author George A. David
   */
  public List<Slice> getSlices()
  {
    List<Slice> slices = new LinkedList<Slice>();
    for(FocusGroup group : _focusGroups)
      slices.addAll(group.getSlices());

    return slices;
  }

  /**
   * @author George A. David
   */
  public FocusRegion getSurfaceModelingFocusRegion()
  {
    Assert.expect(_surfaceModelingFocusRegion != null);

    return _surfaceModelingFocusRegion;
  }

  /**
   * @author Peter Esbensen
   */
  public int getSizeOfReconstructedImagesDataInBytes()
  {
    return (int)getMaximumExpectedMemorySize();
  }

  /**
   * @author Patrick Lacz
   */
  public long getMaximumExpectedMemorySize()
  {
    double degreeRotation = getRotationCorrectionInDegrees();
    boolean addOrthogonalImageSize = (MathUtil.fuzzyEquals(degreeRotation,0.0d) == false);

    final long _SIZE_OF_FLOAT_IN_BYTES = 4;

    ImageRectangle rawImageRectangle = getRegionRectangleInPixels();

    // change this when/if the raw image is stored in bytes.
    long sizeOfOneSlice = rawImageRectangle.getWidth() * rawImageRectangle.getHeight() * _SIZE_OF_FLOAT_IN_BYTES;
    long sizeOfOneInspectedSlice = sizeOfOneSlice;
    if (addOrthogonalImageSize)
    {
      RegionOfInterest rawImageRoi = new RegionOfInterest(0, 0,
          rawImageRectangle.getWidth(), rawImageRectangle.getHeight(),
          0, RegionShapeEnum.RECTANGULAR);

      AffineTransform rotationTransform = AffineTransform.getRotateInstance( -Math.toRadians(degreeRotation));
      AffineTransform correctionTransform = new AffineTransform();

      RegionOfInterest correctedImageRoi = new RegionOfInterest(rawImageRoi);

      Transform.boundTransformedRegionOfInterest(rawImageRoi, rotationTransform, correctedImageRoi, correctionTransform);

      // corrected image is always in floats
      long sizeOfOrthogonalImage = correctedImageRoi.getWidth() * correctedImageRoi.getHeight() * _SIZE_OF_FLOAT_IN_BYTES;
      sizeOfOneInspectedSlice += sizeOfOrthogonalImage;
    }

    int numberOfInspectedSlices = 0;
    if (isInspected())
      numberOfInspectedSlices = getInspectedSlices().size();
    int numberOfNonInspectedSlices = getNumberOfSlices() - numberOfInspectedSlices;

    return sizeOfOneInspectedSlice * numberOfInspectedSlices + sizeOfOneSlice * numberOfNonInspectedSlices;
  }

  /**
   * The name of this region depends on it' s attributes. It is
   * <top|bottom>_<xCoordinate>_<yCoordinate>_<width>_<length>
   * The reconstruction region must be completely created before
   * you can ask for its name.
   *
   * @author George A. David
   */
  public String getName()
  {
    Assert.expect(_regionRectangleRelativeToPanelInNanoMeters != null);
    Assert.expect(_isTopSide != null);

    String name = "";

    if (_reconstructionRegionTypeEnum.equals(ReconstructionRegionTypeEnum.VERIFICATION) && VirtualLiveManager.getInstance().isVirtualLiveMode() == false)
    {
      // add left or right indicating what side we are doing
      if (_testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
      {
        name += "right_";
      }
      else if (_testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
      {
        name += "left_";
      }
      else
      {
        Assert.expect(false, "unrecognized PanelLocationInSystemEnum: " + _testSubProgram.getPanelLocationInSystem());
      }
    }

    if (_isTopSide.getValue())
    {
      name += "top_";
    }
    else
    {
      name += "bottom_";
    }
    
    if(_testSubProgram == null )
      System.out.println("Is " + _component.getReferenceDesignator());
    
    if(_testSubProgram.isHighMagnification())
        name += "high_";

    name += (int) MathUtil.convertNanoMetersToMils(_regionRectangleRelativeToPanelInNanoMeters.getMinX()) + "_"
      + (int) MathUtil.convertNanoMetersToMils(_regionRectangleRelativeToPanelInNanoMeters.getMinY()) + "_"
      + (int) MathUtil.convertNanoMetersToMils(_regionRectangleRelativeToPanelInNanoMeters.getWidth()) + "_"
      + (int) MathUtil.convertNanoMetersToMils(_regionRectangleRelativeToPanelInNanoMeters.getHeight());


    return name;
  }

  /**
   * @author Patrick Lacz
   */
  public void setRotationCorrectionTransformation(AffineTransform correctionTransformation)
  {
    Assert.expect(correctionTransformation != null);
    _rotationCorrectionTransformation = correctionTransformation;
  }

  /**
   * @author Patrick Lacz
   */
  public boolean hasRotationCorrectionTransformation()
  {
    return _rotationCorrectionTransformation != null;
  }

  /**
   * @author Patrick Lacz
   */
  public AffineTransform getRotationCorrectionTransformation()
  {
    Assert.expect(_rotationCorrectionTransformation != null);
    return _rotationCorrectionTransformation;
  }
  
  /**
   * Store the "after resize" Affine Transform matrix.
   * This is specially use to handle the short algorithm which do not using the resize image although
   * the resize pre-process feature is turn on. The special handling is needed because we need to 
   * perform image enhancement before perform rotation.
   * @author Lim, Lay Ngor - Hana Solder Bump
   */
  public void setRotationCorrectionTransformationWithResize(AffineTransform correctionTransformationWithResize)
  {
    Assert.expect(correctionTransformationWithResize != null);
    _rotationCorrectionTransformationWithResize = correctionTransformationWithResize;
  }

  /**
   * Check the availability of "after resize" Affine Transform matrix.
   * @author Lim, Lay Ngor - Hana Solder Bump
   */
  public boolean hasRotationCorrectionTransformationWithResize()
  {
    return _rotationCorrectionTransformationWithResize != null;
  }

  /**
   * Get the "after resize" Affine Transform matrix.
   * This is use when we have the pre-process resize is turn on and the image is
   * need to rotate.
   * @author Lim, Lay Ngor - Hana Solder Bump
   */
  public AffineTransform getRotationCorrectionTransformationWithResize()
  {
    Assert.expect(_rotationCorrectionTransformationWithResize != null);
    return _rotationCorrectionTransformationWithResize;
  }

  /**
   * @author George A. David
   */
  public boolean isInspected()
  {
    boolean isInspected = false;

    for(JointInspectionData data : _jointInspectionDataList)
    {
      if(data.getPad().isInspected())
      {
        isInspected = true;
        break;
      }
      else
        isInspected = false;
    }

    return isInspected;
  }

  /**
   * @author George A. David
   */
  public void setUseRetest(boolean useRetest)
  {
    Assert.expect(_testSubProgram != null);

    if(useRetest)
      _testSubProgram.getTestProgram().addUnfocusedReconstructionRegionNeedingRetest(this);
    else
      _testSubProgram.getTestProgram().removeUnfocusedReconstructionRegionNeedingRetest(this);
  }

  /**
   * @author George A. David
   */
  public boolean isUsingRetest()
  {
    Assert.expect(_testSubProgram != null);

    return _testSubProgram.getTestProgram().isUnfocusedRegionNeedingRetest(this);
  }

  /**
   * @author George A. David
   */
  public void setTestestableForUnfocusedRegions(boolean isTestable)
  {
    Assert.expect(_testSubProgram != null);

    _testSubProgram.getTestProgram().setUnfocusedReconstructionRegionTestable(this, isTestable);
  }

  /**
   * @author George A. David
   */
  public boolean isTestableForUnfocusedRegions()
  {
    Assert.expect(_testSubProgram != null);

    return _testSubProgram.getTestProgram().isUnfocusedReconstructionRegionTestable(this);
  }
  /**
   * @author George A. David
   */
  public void setFixFocusLater(Slice slice, boolean fixFocusLater)
  {
    Assert.expect(_testSubProgram != null);

    if(fixFocusLater)
      _testSubProgram.getTestProgram().addUnfocusedSliceToBeFixed(this, slice);
    else
      _testSubProgram.getTestProgram().removeUnfocusedSliceToBeFixed(this, slice);
  }

  /**
   * @author George A. David
   */
  public void clearFixFocusLater()
  {
    Assert.expect(_testSubProgram != null);

    _testSubProgram.getTestProgram().removeUnfocusedSliceToBeFixed(this);
  }

  /**
   * @author George A. David
   */
  public boolean isMarkedForFixFocus(Slice slice)
  {
    Assert.expect(_testSubProgram != null);

    return _testSubProgram.getTestProgram().isUnfocusedSliceMarkedToBeFixed(this, slice);
  }

  /**
   * @author George A. David
   */
  public boolean isMarkedForFixFocus()
  {
    Assert.expect(_testSubProgram != null);

    return _testSubProgram.getTestProgram().isUnfocusedSliceMarkedToBeFixed(this);
  }

  /**
   * @author George A. David
   */
  public void setCustomFocus(Slice slice, int zOffsetInNanoMeters)
  {
    Assert.expect(_testSubProgram != null);

    _testSubProgram.getTestProgram().setUnfocusedSliceZheightOffsetInNanoMeters(this, slice, zOffsetInNanoMeters);

    if(slice.isUsingCustomFocus() == false)
    {
      slice.setDefaultFocusInstructionOffsetInNanoMeters(slice.getFocusInstruction().getZOffsetInNanoMeters());
      slice.setIsUsingCustomFocus(true);
    }
    slice.getFocusInstruction().setZOffsetInNanoMeters(zOffsetInNanoMeters);

  }

  /**
   * @author Andy Mechtenberg
   */
  public int getCustomFocusZheightOffsetInNanoMeters(Slice slice)
  {
    Assert.expect(_testSubProgram != null);
    Assert.expect(isUsingCustomFocus(slice));
    int height = _testSubProgram.getTestProgram().getUnfocusedSliceZheightOffsetInNanoMeters(this, slice);
    return height;
  }

  /**
   * @author George A. David
   */
  public boolean isUsingCustomFocus(Slice slice)
  {
    Assert.expect(_testSubProgram != null);

    return _testSubProgram.getTestProgram().isUnfocusedSliceUsingCustomFocus(this, slice);
  }

  /**
   * @author George A. David
   */
  public boolean isUsingCustomFocus()
  {
    Assert.expect(_testSubProgram != null);

    return _testSubProgram.getTestProgram().isUnfocusedSliceUsingCustomFocus(this);
  }

  /**
   * @author George A. David
   */
  public void clearCustomFocus(Slice slice)
  {
    Assert.expect(_testSubProgram != null);

    if(slice.isUsingCustomFocus())
    {
      slice.getFocusInstruction().setZOffsetInNanoMeters(slice.getDefaultFocusInstructionOffsetInNanoMeters());
      slice.setIsUsingCustomFocus(false);
    }
    _testSubProgram.getTestProgram().removeUnfocusedSliceUsingCustomFocus(this, slice);
  }

  /**
   * @author George A. David
   */
  public void clearCustomFocus()
  {
    Assert.expect(_testSubProgram != null);

    for(Slice slice : getSlices())
    {
      if (slice.isUsingCustomFocus())
      {
        slice.getFocusInstruction().setZOffsetInNanoMeters(slice.getDefaultFocusInstructionOffsetInNanoMeters());
        slice.setIsUsingCustomFocus(false);
      }
    }

    _testSubProgram.getTestProgram().removeUnfocusedSliceUsingCustomFocus(this);
  }

  /**
   * @author Matt Wharton
   */
  public ReconstructionRegionTypeEnum getReconstructionRegionTypeEnum()
  {
    Assert.expect(_reconstructionRegionTypeEnum != null);

    return _reconstructionRegionTypeEnum;
  }

  /**
   * @author Matt Wharton
   */
  public void setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum reconstructionRegionTypeEnum)
  {
    Assert.expect(reconstructionRegionTypeEnum != null);

    _reconstructionRegionTypeEnum = reconstructionRegionTypeEnum;
  }

  /**
   * @author George A. David
   */
  public void setIsAffectedByInterferencePattern(boolean isAffectedByInterferencePattern)
  {
    _isAffectedByInterferencePattern = isAffectedByInterferencePattern;
    
    determineZAffected();
  }

  /**
   * @author George A. David
   */
  public List<Component> getComponentsThatCauseInterferencePattern()
  {
    Assert.expect(_componentsThatCauseInterferencePattern != null);

    return new ArrayList<Component>(_componentsThatCauseInterferencePattern);
  }

  /**
   * @author George A. David
   * @author Yee Seong
   */
  public void addComponentThatCausesInterferencePattern(Component component)
  {
    Assert.expect(component != null);

    _componentsThatCauseInterferencePattern.add(component);
    for(Pad pad : getPads())
    {
      pad.getPadType().addComponentTypeThatCausesInteferencePattern(component.getComponentType());
      component.getComponentType().addInterferenceAffectedPadType(pad.getPadType());
    }
  }


  /**
   * @author George A. David
   */
  public boolean isAffectedByInterferencePattern()
  {
    return _isAffectedByInterferencePattern;
  }

  /**
   * @author George A. David
   * @author Wei Chin
   */
  public SignalCompensationEnum getSignalCompensation()
  {
    SignalCompensationEnum signalComp = SignalCompensationEnum.DEFAULT_LOW;
    for(Subtype subtype: getSubtypes())
    {
      signalComp = SignalCompensationEnum.getHigherSignalCompensation(signalComp,
                                                                      subtype.getSubtypeAdvanceSettings().getSignalCompensation());
    }

    return signalComp;
  }

  /**
   * @author George A. David
   */
  public Set<Integer> getPitchesInNanoMeters()
 {
   Set<Integer> pitches = new HashSet<Integer>();
    for(Pad pad : getPads())
      pitches.add(pad.getPitchInNanoMeters());

    return pitches;
  }

  /**
   * @author George A. David
   */
  public int getModeOfPitchesInNanoMeters()
 {
   Map<Integer, Integer> pitchesToNumPads = new HashMap<Integer, Integer>();
    for(Pad pad : getPads())
    {
      Integer numPads = pitchesToNumPads.get(pad.getPitchInNanoMeters());
      if(numPads == null)
        pitchesToNumPads.put(pad.getPitchInNanoMeters(), 1);
      else
        pitchesToNumPads.put(pad.getPitchInNanoMeters(), numPads + 1);
    }

    int modeOfPitches = -1;
    int maxCount = 0;
    for(Map.Entry<Integer, Integer> entry : pitchesToNumPads.entrySet())
    {
      if(modeOfPitches == -1 || maxCount < entry.getValue())
      {
        modeOfPitches = entry.getKey();
        maxCount = entry.getValue();
      }
      // in case of a tie, choose the larger pitch
      else if(maxCount == entry.getValue() && modeOfPitches < entry.getKey())
        modeOfPitches = entry.getKey();
    }

    return modeOfPitches;
  }

  /**
   * @author Roy Williams
   */
  public boolean requiresShadingCompensation()
  {
    if (hasNonDefaultSignalCompensation())
    {
      // if signal compensation is not default low then it must have some candidate
      // steps (i.e. step recommendations) to fulfill this contract.
      Assert.expect(_candidateStepRanges.hasRanges());
      return true;
    }
    else if(isAffectedByInterferencePattern())
    {
//      Assert.expect(_candidateStepRanges.isEmpty() == false);
//      return true;

      // check to see if the user has decided not to allow shading compensation
      // this is what we should be doing
      boolean allowShadingCompensation = false;
      for(Pad pad : getPads())
      {
        if(pad.getPadTypeSettings().getEffectiveArtifactCompensationState().equals(ArtifactCompensationStateEnum.COMPENSATED))
        {
          allowShadingCompensation = true;
          break;
        }
      }
      return allowShadingCompensation;
    }

    return false;
  }
  
  /**
   * @author Chnee Khang Wah, 2012-07-19
   */
  public boolean isCompensated()
  {
    // check to see if the user has decided not to allow shading compensation
    // this is what we should be doing
    boolean isCompensated = false;
    for(Pad pad : getPads())
    {
      if(pad.getPadTypeSettings().getEffectiveArtifactCompensationState().equals(ArtifactCompensationStateEnum.COMPENSATED))
      {
        isCompensated = true;
        break;
      }
    }
    return isCompensated;
  }

  /**
   * @author George A. David
   */
  public boolean hasNonDefaultSignalCompensation()
  {
    if (getSignalCompensation().equals(SignalCompensationEnum.DEFAULT_LOW) == false)
      return true;

    if (hasVirtualLiveSetting())
    {
      return true;
    }

    return false;
  }

  /**
   * @author George A. David
   */
  public void setCandidateStepRanges(RangeUtil stepRanges)
  {
    Assert.expect(stepRanges != null);

    _candidateStepRanges.clear();
    _candidateStepRanges.add(stepRanges);
//    SignalCompensationEnum maxCompensation = SignalCompensationEnum.getMaxSignalCompensation(RangeUtil.getMaxInRangeList(stepRanges));
    for(Pad pad : getPads())
      pad.getPadTypeSettings().setCandidateStepRanges(stepRanges);
  }

  /**
   * @author George A. David
   */
  public RangeUtil getCandidateStepRanges()
  {
    Assert.expect(_candidateStepRanges != null);

    return _candidateStepRanges;
  }

  /**
   * @author George A. David
   */
  public void setAllowableAlignmentRegionInNanoMetes(PanelRectangle allowableAlignmentRegionInNanoMeters)
  {
    Assert.expect(allowableAlignmentRegionInNanoMeters != null);

    _allowableAlignmentRegionInNanoMeters = allowableAlignmentRegionInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public PanelRectangle getAllowableAlignmentRegionInNanoMeters()
  {
    Assert.expect(_allowableAlignmentRegionInNanoMeters != null);

    return _allowableAlignmentRegionInNanoMeters;
  }

  /**
   * @author Cheah, Lee Herng
   */
  public boolean hasNeighbors()
  {
      Assert.expect(_focusGroups != null);

      boolean hasNeighbors = false;
      for(FocusGroup focusGroup : _focusGroups)
      {
          if (focusGroup.hasNeighbors())
          {
              hasNeighbors = true;
              break;
          }
      }
      return hasNeighbors;
  }

  /**
   * @author Cheah, Lee Herng
   */
  public boolean useAutoPredictiveSliceHeight()
  {
      JointInspectionData joint = getJointInspectionDataList().get(0);
      Subtype subtype = joint.getSubtype();

      if (subtype.useAutoPredictiveSliceHeight())
          return true;
      else
          return false;
  }

  /**
   * @author Cheah, Lee Herng
   */
  public boolean isParentRegionUseAutoPredictiveSliceHeight()
  {
      return _isParentRegionUseAutoPredictiveSliceHeight;
  }

  /**
   * @author Cheah, Lee Herng
   */
  public void setParentRegionUseAutoPredictiveSliceHeight(boolean parentUseAutoPredictiveSliceHeight)
  {
      _isParentRegionUseAutoPredictiveSliceHeight = parentUseAutoPredictiveSliceHeight;
  }
  
  /**
   * @author Cheah, Lee Herng
   */
  public void setGlobalSurfaceModelEnum(GlobalSurfaceModelEnum globalSurfaceModelEnum)
  {
      Assert.expect(globalSurfaceModelEnum != null);
      _globalSurfaceModelEnum = globalSurfaceModelEnum;
  }

  /**
   * @author Cheah, Lee Herng
   */
  public GlobalSurfaceModelEnum getGlobalSurfaceModelEnum()
  {
      Assert.expect(_globalSurfaceModelEnum != null);
      return _globalSurfaceModelEnum;
  }

  /**
   * @author Yong Sheng Chuan

   */
  public void initializeVirtualLiveSetting(List<StageSpeedEnum> speedList,
                                           UserGainEnum userGain,
                                           SignalCompensationEnum ilLevel,
                                           DynamicRangeOptimizationLevelEnum dro)
  {
    Assert.expect(speedList != null);
    Assert.expect(userGain != null);
    Assert.expect(ilLevel != null);
    Assert.expect(dro != null);
    
    _virtualLiveSetting = new VirtualLiveRegionSetting(dro, speedList, userGain, ilLevel);
    
    int maxStepSizeInNanos = ImagingChainProgramGenerator.calculateMaxStepSizeInNanometers();
    int minAverageStepSizeInNanos = ImagingChainProgramGenerator.getMinAverageStepSizeInNanometers();
    int maxAverageStepSizeInNanos = ImagingChainProgramGenerator.getMaxAverageStepSizeInNanometers();
    int maxStepSizeForRegion = ProgramGeneration.getInstance().getMaxStepSizeInNanos(ilLevel, maxStepSizeInNanos, maxAverageStepSizeInNanos);
    if (minAverageStepSizeInNanos > maxStepSizeForRegion)
      minAverageStepSizeInNanos = maxStepSizeForRegion;
    RangeUtil rangeUtil = new RangeUtil(minAverageStepSizeInNanos, maxStepSizeForRegion);
    _candidateStepRanges.clear();
    _candidateStepRanges.add(rangeUtil);
  }

  /**
   * @author Chong, Wei Chin
   */
  public void clearVirtualLiveSetting()
  {
    _virtualLiveSetting = null;
    
    int maxStepSizeInNanos = ImagingChainProgramGenerator.calculateMaxStepSizeInNanometers();
    int minAverageStepSizeInNanos = ImagingChainProgramGenerator.getMinAverageStepSizeInNanometers();
    int maxAverageStepSizeInNanos = ImagingChainProgramGenerator.getMaxAverageStepSizeInNanometers();
    int maxStepSizeForRegion = ProgramGeneration.getInstance().getMaxStepSizeInNanos(SignalCompensationEnum.DEFAULT_LOW, maxStepSizeInNanos, maxAverageStepSizeInNanos);
    if (minAverageStepSizeInNanos > maxStepSizeForRegion)
      minAverageStepSizeInNanos = maxStepSizeForRegion;
    RangeUtil rangeUtil = new RangeUtil(minAverageStepSizeInNanos, maxStepSizeForRegion);
    _candidateStepRanges.clear();
    _candidateStepRanges.add(rangeUtil);
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean hasVirtualLiveSetting()
  {
    return _virtualLiveSetting != null;
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean useToBuildGlobalSurfaceModel()
  {
    return _useToBuildGlobalSurfaceModel;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setUseToBuildGlobalSurfaceModel(boolean useToBuildGlobalSurfaceModel)
  {
    _useToBuildGlobalSurfaceModel = useToBuildGlobalSurfaceModel;
  }
  
  /**
   * @author Chnee Khang Wah, 2013-06-17, New PSH Handling
   */
  public boolean waitForKickStart()
  {
    return _waitForKickStart;
  }
  
  /**
   * @author Chnee Khang Wah, 2013-06-17, New PSH Handling
   */
  public void setWaitForKickStart(boolean waitForKickStart)
  {
    _waitForKickStart = waitForKickStart;
  }
  
  /**
   * @author Yong Sheng Chuan
   * @return 
   */
  public boolean isRegionUsedForExposureLearning()
  {
    return _isUsedForLearningExposure;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setIsUseForLearningExposure(boolean isUsedForSpeedOptimization)
  {
    _isUsedForLearningExposure = isUsedForSpeedOptimization;
  }

  /**
   * @author Wei Chin
   */
  public static PanelRectangle getMinimumInspectableRegionforPad(Pad pad)
  {
    Assert.expect(pad != null);

    PanelRectangle shapeBounds = null;
    if (InspectionFamily.isSpecialTwoPinDevice(pad.getSubtype().getInspectionFamilyEnum()))
    {
      // for chips, both pads must fit
      List<Pad> pads = pad.getComponent().getPads();
      Assert.expect(pads.size() == 2);
      for (Pad compPad : pads)
      {
        if (shapeBounds == null)
        {
          shapeBounds = ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(compPad);
        }
        else
        {
          shapeBounds.add(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(compPad));
        }
      }
    }
    else
    {
      shapeBounds = ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad);
    }

    return shapeBounds;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setRetestForFailingJoint(boolean retestForFailingJoint)
  {
      _retestForFailingJoint = retestForFailingJoint;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isRetestForFailingJoint()
  {
      return _retestForFailingJoint;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setRetestForPassingJoint(boolean retestForPassingJoint)
  {
      _retestForPassingJoint = retestForPassingJoint;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isRetestForPassingJoint()
  {
      return _retestForPassingJoint;
  }

  /**
   * @author sham
   */
  public void enlargeRegionRectangle(PanelRectangle rec)
  {
    Assert.expect(rec != null);
    
    _regionRectangleRelativeToPanelInNanoMeters.add(rec);
  }

  /**
   * author Chnee Khang Wah
   */
  private void determineZAffected()
  {
    double zRef = XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers() / MagnificationEnum.getNominalMagnificationAtReferencePlane();
    
    double zBottom = zRef + StageBelts.getNominalBeltTopDistanceBelowSystemCouponInNanoMeters();
    double zTop = zBottom - _component.getBoard().getPanel().getThicknessInNanometers();
    
    
    int dir  = -1;
    double zSide = zTop;
    
    if (_component.isTopSide())
    {
      dir = -1;
      zSide = zTop;
    }
    else
    {
      dir = 1;
      zSide = zBottom;
    }
    
    for (JointTypeEnum jointType : _component.getJointTypeEnums())
    {
      if(jointType.equals(JointTypeEnum.PRESSFIT) || 
         jointType.equals(JointTypeEnum.THROUGH_HOLE) || 
         jointType.equals(JointTypeEnum.OVAL_THROUGH_HOLE)) //Siew Yeng - XCR-3318 - Oval PTH
      {
        _zAffectedRegionInNano = (zTop + zBottom) / 2;
      }
      else
      {
        int offset = InterferenceCompensationEnum.getOffset(jointType.getName());
        _zAffectedRegionInNano = zSide + dir*offset;
      }
    }
  }
  
  /**
   * author Chnee Khang Wah
   */
  public void addAffectingPicth(int affectingPitch)
  {
    Assert.expect(affectingPitch != 0);
    
    _effectivePitchInNanos.add(affectingPitch);
  }
  
  /**
   * author Chnee Khang Wah
   */
  public List<Integer> getAffectingPitch()
  {
    return _effectivePitchInNanos;
  }
  
  /**
   * author Chnee Khang Wah
   */
  public void addzAffectingRegion(double zAffectingRegionInNanos)
  {
    _zAffectingRegionInNanos.add(zAffectingRegionInNanos);
  }
  
  /**
   * author Chnee Khang Wah
   */
  public List<Double> getzAffectingRegion()
  {
    return _zAffectingRegionInNanos;
  }
  
  /**
   * author Chnee Khang Wah
   */
  public void setzAffectedRegion(double zAffectedRegionInNanos)
  {
    _zAffectedRegionInNano = zAffectedRegionInNanos;
  }
  
  /**
   * author Chnee Khang Wah
   */
  public double getzAffectedRegion()
  {
    return _zAffectedRegionInNano;
  }
  
  /**
   * author Chnee Khang Wah
   */
  public double getEntropy(int StepSizeInInNanos)
  {
    int StepSizeInMils = (int)MathUtil.convertNanoMetersToMils(StepSizeInInNanos);
            
    if(_stepSizeInMilsToEntropyMap.containsKey(StepSizeInMils))
    {
      return _stepSizeInMilsToEntropyMap.get(StepSizeInMils);
    }
    else
      return getLowestEntropyValueForStepSize(StepSizeInMils);
  }
  
  /**
   * author Chnee Khang Wah
   */
  public Map<Integer,Double> calculateEntropyForEachStepSize(RangeUtil stepRanges)
  {
    Assert.expect(stepRanges.hasRanges());
    _stepSizeInMilsToEntropyMap.clear();
    
    for (Pair<Integer, Integer> pair : stepRanges.getRanges())
    {
      // calculate entropy for every mils of step size
      for (int i = pair.getFirst(); i<pair.getSecond()+1; i=i+(int)MathUtil.NANOMETERS_PER_MIL)
      {
        int stepSizeInMils = (int)MathUtil.convertNanoMetersToMils(i);
        stepSizeInMils = Math.max(1, stepSizeInMils);
        double entropy = getLowestEntropyValueForStepSize(stepSizeInMils);
        _stepSizeInMilsToEntropyMap.put(stepSizeInMils, entropy);
      }
    }
    
    return _stepSizeInMilsToEntropyMap;
  }
  
  private double getLowestEntropyValueForStepSize(int stepSizeInMils)
  {
    Assert.expect(_effectivePitchInNanos != null);
    Assert.expect(_zAffectingRegionInNanos != null);
    
    if(_zAffectedRegionInNano==0)
    {
      determineZAffected();
    }
    
    // assumes every list have the same size
    int t = _effectivePitchInNanos.size();
    double entropy = 9999999.0;
    double zAffectedRegionInMils  = MathUtil.convertNanoMetersToMils(_zAffectedRegionInNano);
    for  (int i=0; i<t; i++)
    {
      double pitchInMils = MathUtil.convertNanoMetersToMils(_effectivePitchInNanos.get(i));
      double zAffectingRegionInMils = MathUtil.convertNanoMetersToMils(_zAffectingRegionInNanos.get(i));
      
      entropy = Math.min(entropy, calculateEntropy(pitchInMils, zAffectingRegionInMils, zAffectedRegionInMils, stepSizeInMils));
    }
            
    return entropy;
  }
  
  /**
   * author Chnee Khang Wah
   */
  private double calculateEntropy(double pitchInMils,
                                  double zAffectingInMils,
                                  double zAffectedInMils,
                                  int stepSizeInMils)
  {
    if(zAffectingInMils==zAffectedInMils)
    {
      zAffectingInMils += 1;
    }
    
    double entropy=0;
    double normalizedPhaseShift = stepSizeInMils * (zAffectingInMils - zAffectedInMils) / (pitchInMils * zAffectedInMils);
    double shiftInRadian = normalizedPhaseShift * 2 * Math.PI;
    int n = getPassesInvolved(stepSizeInMils);
    
    entropy = calculatePhaseShiftEntropy(shiftInRadian, n);
    return entropy;
  }
  
  /**
   * author Chnee Khang Wah
   */
  private int getPassesInvolved(int stepSizeInMils)
  {
    int passes = 0;
    MachineRectangle cameraArrayRectangle = MechanicalConversions.getCameraArrayRectangleAtMinSliceHeight();
    
    if(this.getTestSubProgram().isHighMagnification())
      cameraArrayRectangle = MechanicalConversions.getCameraArrayRectangleAtMinSliceHeightInHighMag();
    
    int wBot= (int)MathUtil.convertNanoMetersToMils(cameraArrayRectangle.getWidth()); // need to confirm for correct value
    
    passes = (int)Math.round(wBot/stepSizeInMils);
    return passes;
  }
  
  /**
   * author Chnee Khang Wah
   */
  private double calculatePhaseShiftEntropy(double shiftInRadian, int n)
  {
    Assert.expect(n > 0);
    double entropy = 0.0;
    
    List<Double> shiftInRadians = new ArrayList<Double>();
    for (int i=1; i<n+1; i++)
    {
      shiftInRadians.add((i*shiftInRadian)%(2*Math.PI));
    }
    
    Collections.sort(shiftInRadians);
    
    // phis are deltaAngles
    List<Double> phis = new ArrayList<Double>();
    if (n>1)
    {
      for (int i=1; i<n; i++)
      {
        phis.add(shiftInRadians.get(i)-shiftInRadians.get(i-1));
      }
      
      // add the last column
      phis.add(shiftInRadians.get(0)-shiftInRadians.get(n-1)+2*Math.PI);
      
      // turn this into a probability distribution
      List<Double> dist = new ArrayList<Double>();
      for (int i=0; i<n; i++)
      {
        double d = phis.get(i)/(2*Math.PI);
        if (d==0) 
        {
          d=1;
        }
        dist.add(d);
      }
      
      for (int i=0; i<n; i++)
      {
        // entropy calculation involves taking log2(dist) which is OK if all values are non-zero
        double e = Math.log(dist.get(i))/Math.log(2) * dist.get(i);
        entropy = entropy - e;
      } 
    }
    
    Assert.expect(entropy != 0.0);
    return entropy; 
  }
  
  /**
   * @author Chnee Khang Wah, 2013-08-29, RealTime PSH
   */
  public void incrementProducerCount()
  {
    Assert.expect(_waitForKickStart);
    
    ++_totalProducerCount;
  }
  
  /**
   * author Chnee Khang Wah, 2014-06-17, RealTime PSH
   */
  public int GetTotalProducerCount()
  {
    return _totalProducerCount;
  }
  
  /**
   * @author Chnee Khang Wah, 2013-08-29, RealTime PSH
   */
  public void oneProducerReady()
  {
    Assert.expect(_waitForKickStart);
    
    int count = _remainingProducerCount.decrementAndGet();
    Assert.expect(count >= 0);
    if(count == 0)
    {
      try
      {
        _surfaceModelBreakPoint.executeAction(this);
      }
      catch(XrayTesterException xe)
      {
        // do nothing
      }
    }
  }
  
  /**
   * @author Chnee Khang Wah, 2013-08-29, RealTime PSH
   */
  public void resetProducerState()
  {
    if(_waitForKickStart)
    {
      _remainingProducerCount.set(_totalProducerCount);
    }
    }
      
  /**
   * @author Chnee Khang Wah, 2013-08-29, RealTime PSH
   */
  public void addConsumerRegion(ReconstructionRegion region)
  {
    Assert.expect(_waitForKickStart==false);
    
    _consumerRegionList.add(region);
  }
  
  /**
   * @author Chnee Khang Wah, 2013-08-29, RealTime PSH
   */
  public void triggerConsumerRegion()
  { 
    for(ReconstructionRegion region:_consumerRegionList)
      region.oneProducerReady();
  }
  
  /**
   * @author Chnee Khang Wah, 2013-08-16, Realtime PSH
   */
  public boolean hasSurfaceModelBreakPoint()
  {
    return (_surfaceModelBreakPoint != null);
  }
  
  /**
   * @author Chnee Khang Wah, 2013-08-16, Realtime PSH
   */
  public void setSurfaceModelBreakPoint(SurfaceModelBreakPoint breakPoint)
  {
    _surfaceModelBreakPoint = breakPoint;
  }
  
  /**
   * @author Chnee Khang Wah, 2013-08-16, Realtime PSH
   */
  public SurfaceModelBreakPoint getSurfaceModelBreakPoint()
  {
    Assert.expect(_surfaceModelBreakPoint != null);
    
    return _surfaceModelBreakPoint;
  }
  
  /**
   * @author Chnee Khang Wah, 2013-08-29, RealTime PSH
   */
  public void clearConsumerAndProducer()
  {
    if (_consumerRegionList != null)
      _consumerRegionList.clear();
    
    _surfaceModelBreakPoint = null;
    
    _totalProducerCount = 0;
  }
  
  /**
   * @author Chong Wei Chin
   */
  public void clearFocusGroups()
  {
    if(_focusGroups.isEmpty() == false)
    {
      // XCR-2851 - Ee Jun Jiang - VirtualLiveMemoryLeak
      for(FocusGroup focusgroup : _focusGroups)
      {
        focusgroup.clear();
      }
      
      _focusGroups.clear();
      _surfaceModelingFocusRegion = null;
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {
    clearFocusGroups();
    clearVirtualLiveSetting();
    if (_jointInspectionDataList != null)
      _jointInspectionDataList.clear();
    
    if (_fiducialInspectionDataList != null)
      _fiducialInspectionDataList.clear();
    
    if (_alignmentContextFeatures != null)
      _alignmentContextFeatures.clear();
    
    // Chnee Khang Wah, 2013-04-18
    if (_effectivePitchInNanos != null)
      _effectivePitchInNanos.clear();
    if (_zAffectingRegionInNanos != null)
      _zAffectingRegionInNanos.clear();
    if (_stepSizeInMilsToEntropyMap != null)
      _stepSizeInMilsToEntropyMap.clear();
    
    _zAffectedRegionInNano = 0;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setMeshTriangle(MeshTriangle meshTriangle)
  {
    Assert.expect(meshTriangle != null);
    _referenceMeshTriangle = meshTriangle;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public MeshTriangle getMeshTriangle()
  {
    Assert.expect(_referenceMeshTriangle != null);
    return _referenceMeshTriangle;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasMeshTriangle()
  {
    if (_referenceMeshTriangle == null)
      return false;
    else
      return true;
  }
  
  /**
   * @author Siew Yeng
   */
  public DynamicRangeOptimizationLevelEnum getDynamicRangeOptimizationLevel()
  {
    DynamicRangeOptimizationLevelEnum droLevel = DynamicRangeOptimizationLevelEnum.ONE;
    if (hasVirtualLiveSetting())
      droLevel = _virtualLiveSetting.getDynamicRangeOptimizationLevel();
    else
    {
      for (Subtype subtype : getSubtypes())
      {
        droLevel = DynamicRangeOptimizationLevelEnum.getHigherDynamicRangeOptimizationLevel(droLevel,
          subtype.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel());
      }
    }
    return droLevel;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public List<StageSpeedEnum> getStageSpeedList()
  {
    List<StageSpeedEnum> stageSpeedEnum = new ArrayList<StageSpeedEnum>();
    if (hasVirtualLiveSetting())
      stageSpeedEnum = _virtualLiveSetting.getStageSpeedEnumList();
    else
    {
      Assert.expect(getSubtypes().isEmpty() == false);
      for (Subtype subtype : getSubtypes())
      {
        stageSpeedEnum = subtype.getSubtypeAdvanceSettings().getStageSpeedList();
      }
    }
    
    return stageSpeedEnum;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public UserGainEnum getUserGainEnum()
  {
    UserGainEnum userGainEnum = UserGainEnum.ONE;
    if (hasVirtualLiveSetting())
      userGainEnum = _virtualLiveSetting.getUserGainEnum();
    else
    {
      Assert.expect(getSubtypes().isEmpty() == false);
      for (Subtype subtype : getSubtypes())
      {
        userGainEnum = subtype.getSubtypeAdvanceSettings().getUserGain();
      }
    }
    
    return userGainEnum;
  }
  
  /**
   * @author Siew Yeng
   * @author Khang Wah, 2016-05-16, MVEDR
   */
  public boolean useDynamicRangeOptimization()
  {
    if(useMVEDR())
      return false;
    else
      return (getDynamicRangeOptimizationLevel() != DynamicRangeOptimizationLevelEnum.ONE);
  }
  
  /**
   * @author Khang Wah, 2016-05-16, MVEDR
   */
  public boolean useMVEDR()
  {
    if (getStageSpeedList().size() >=2)
      return true;
    else
      return false;
  }
  
  /**
   * @author Siew Yeng
   */
  public static void resetMaxInspectionRegionWidthLength()
  {
    _currentMaxInspectionRegionWidthInNanometers = -1;
    _currentMaxInspectionRegionLengthInNanometers = -1;
    _currentMaxHighMagInspectionRegionWidthInNanometers = -1;
    _currentMaxHighMagInspectionRegionLengthInNanometers = -1;
  }
  
  /**
   * @author Siew Yeng
   */
  public static int getMaxIRPInspectionRegionWidthInNanoMeters()
  {
    return getConfig().getIntValue(HardwareConfigEnum.MAX_INSPECTION_REGION_WIDTH_IN_NANOMETERS);
  }
  
  /**
   * @author Siew Yeng
   */
  public static int getMaxIRPInspectionRegionLengthInNanoMeters()
  {
    return getConfig().getIntValue(HardwareConfigEnum.MAX_INSPECTION_REGION_LENGTH_IN_NANOMETERS);
  }
  
  /**
   * @author Siew Yeng
   */
  public static int getMaxHighMagIRPInspectionRegionWidthInNanoMeters()
  {
    return getConfig().getIntValue(HardwareConfigEnum.MAX_HIGH_MAG_INSPECTION_REGION_WIDTH_IN_NANOMETERS);
  }
  
  /**
   * @author Siew Yeng
   */
  public static int getMaxHighMagIRPInspectionRegionLengthInNanoMeters()
  {
    return getConfig().getIntValue(HardwareConfigEnum.MAX_HIGH_MAG_INSPECTION_REGION_LENGTH_IN_NANOMETERS);
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean isIsolatedGlobalSurfaceRegion()
  {
    JointInspectionData joint = getJointInspectionDataList().get(0);
    Subtype subtype = joint.getSubtype();

    if (subtype.usePredictiveSliceHeight() && hasNeighbors() == false)
      return true;
    
    return false;
  }
  
  /**
   * @author Kok Chun, Tan 
   */
  public boolean hasReferenceOpticalCameraRectangle()
  {
    if (_referenceOpticalCameraRectangle == null)
      return false;
    else
      return true;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public OpticalCameraRectangle getReferenceOpticalCameraRectangle()
  {
    Assert.expect(_referenceOpticalCameraRectangle != null);
    return _referenceOpticalCameraRectangle;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setReferenceOpticalCameraRectangle(OpticalCameraRectangle ocr)
  {
    Assert.expect(ocr != null);
    _referenceOpticalCameraRectangle = ocr;
  }
  
  /**
   * @author Siew Yeng
   */
  public ReconstructionRegion clone()
  {
    ReconstructionRegion newRegion = new ReconstructionRegion();

    newRegion.setReconstructionEngineId(getReconstructionEngineId());
    newRegion.setReconstructionRegionTypeEnum(getReconstructionRegionTypeEnum());
    newRegion.setFocusPriority(getFocusPriority());
    newRegion.setTestSubProgram(getTestSubProgram());
    newRegion.setTopSide(isTopSide());
    newRegion.setRegionRectangleRelativeToPanelInNanoMeters(getRegionRectangleRelativeToPanelInNanoMeters());
    newRegion.setFocusGroups(getFocusGroups());
    newRegion.setCandidateStepRanges(getCandidateStepRanges()); 
    
    if(getReconstructionRegionTypeEnum().equals(ReconstructionRegionTypeEnum.ALIGNMENT))
    {
      newRegion.setAlignmentGroup(getAlignmentGroup());
      
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      if (getTestSubProgram().getTestProgram().getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod() == false)
      {
        for(Pad pad : getPads())
          newRegion.addAlignmentFeature(pad);

        for(Fiducial fiducial : getFiducials())
          newRegion.addAlignmentFeature(fiducial);

        newRegion.enlargeRegionRectangle(getRegionRectangleRelativeToPanelInNanoMeters());
      }

      newRegion.setAllowableAlignmentRegionInNanoMetes(getAllowableAlignmentRegionInNanoMeters());
    }
    else if(getReconstructionRegionTypeEnum().equals(ReconstructionRegionTypeEnum.INSPECTION))
    {
      if(_jointInspectionDataList.isEmpty() == false)
      {
        for(JointInspectionData jointInspectionData : _jointInspectionDataList)
          newRegion.addJointInspectionData(jointInspectionData);
      }
      newRegion.setIsAffectedByInterferencePattern(isAffectedByInterferencePattern());
    }
    
    newRegion.setGlobalSurfaceModelEnum(getGlobalSurfaceModelEnum());

    if(hasMeshTriangle())
      newRegion.setMeshTriangle(getMeshTriangle());

    if(hasReferenceOpticalCameraRectangle())
      newRegion.setReferenceOpticalCameraRectangle(getReferenceOpticalCameraRectangle());

    if(hasRotationCorrectionTransformation())
      newRegion.setRotationCorrectionTransformation(getRotationCorrectionTransformation());

    if(hasSurfaceModelBreakPoint())
      newRegion.setSurfaceModelBreakPoint(getSurfaceModelBreakPoint());

    newRegion.setParentRegionUseAutoPredictiveSliceHeight(isParentRegionUseAutoPredictiveSliceHeight());
    newRegion.setRetestForFailingJoint(isRetestForFailingJoint());
    newRegion.setRetestForPassingJoint(isRetestForPassingJoint());
    newRegion.setUseToBuildGlobalSurfaceModel(useToBuildGlobalSurfaceModel());
    newRegion.setWaitForKickStart(waitForKickStart());

    if(_zAffectingRegionInNanos.isEmpty() == false)
    {
      for(Double zAffectedRegion : _zAffectingRegionInNanos)
      {
        newRegion.addzAffectingRegion(zAffectedRegion);
      }
    }
    
    return newRegion;
  }
  
 /**
  * @Author Kee Chin Seong
  * @return 
  */
  public List<Integer> getCameraEnabledIDList()
  {
     return _cameraEnabledList;
  }
  
 /**
  * Author Kee Chin Seong
  * @param cameraIDEnum
  * @param isEnabled 
  */
  public void setEnabledCamera(List<Integer> enabledCameraIdList)
  {
     Assert.expect(_cameraEnabledList != null);
     
     _cameraEnabledList = enabledCameraIdList;
  }

  /**
   * @author Siew Yeng 
   */
  public static boolean isDefaultMaxInspectionRegionWidth()
  {
    return (_currentMaxInspectionRegionWidthInNanometers == getDefaultMaxInspectionRegionWidthInNanoMeters() &&  
            _currentMaxHighMagInspectionRegionWidthInNanometers == getDefaultMaxHighMagInspectionRegionWidthInNanoMeters());
  }
  
  /**
   * @author Siew Yeng 
   */
  public static boolean isDefaultMaxInspectionRegionLength()
  {
    return (_currentMaxInspectionRegionLengthInNanometers == getDefaultMaxInspectionRegionLengthInNanoMeters() &&  
            _currentMaxHighMagInspectionRegionLengthInNanometers == getDefaultMaxHighMagInspectionRegionLengthInNanoMeters());
  }
}
