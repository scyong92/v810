package com.axi.v810.business.testProgram;

import java.io.*;

import com.axi.util.*;

/**
* This class holds the focus instructions that are used to by the auto focus algorithm
* to identify the correct slice and slice height.
* @author Horst Mueller
*/
public class FocusInstruction implements Serializable
{
  private FocusMethodEnum _focusMethodEnum;
  private int _percentValue;
  private int _zHeightInNanometers;
  private int _zOffsetInNanoMeters;

  /**
  * @author Horst Mueller
  */
  public FocusInstruction()
  {
  }

  /**
   *  @author John Heumann
   */
  public FocusInstruction(FocusMethodEnum focusMethodEnum,
                          int percentValue,
                          int zHeightInNanometers,
                          int zOffsetInNanometers)
  {
    _focusMethodEnum = focusMethodEnum;
    _percentValue = percentValue;
    _zHeightInNanometers = zHeightInNanometers;
    _zOffsetInNanoMeters = zOffsetInNanometers;
  }

  /**
   * @author Horst Mueller
   */
  public FocusMethodEnum getFocusMethod()
  {
    Assert.expect(_focusMethodEnum != null);

    return _focusMethodEnum;
  }

  /**
   * This method returns the id of the FocusEnum which is used for
   * the transition to the C-code via JNI
   * @author Horst Mueller
   */
  public int getFocusMethodEnumNativeId()
  {
    Assert.expect(_focusMethodEnum != null);

    return _focusMethodEnum.getId();
  }

  /**
   * @author Horst Mueller
   */
  public void setFocusMethod(FocusMethodEnum focusMethodEnum)
  {
    Assert.expect(focusMethodEnum != null);

    _focusMethodEnum = focusMethodEnum;
  }

  /**
   * @author Roy Williams
   */
  public int getZOffsetInNanoMeters()
  {
    return _zOffsetInNanoMeters;
  }

  /**
   * @author Roy Williams
   */
  public void setZOffsetInNanoMeters(int zOffsetInNanoMeters)
  {
    _zOffsetInNanoMeters = zOffsetInNanoMeters;
  }

  /**
   * @author Horst Mueller
   */
  public int getZHeightInNanoMeters()
  {
    return _zHeightInNanometers;
  }

  /**
   * @author Horst Mueller
   */
  public void setZHeightInNanoMeters(int zHeightInNanometers)
  {
    _zHeightInNanometers = zHeightInNanometers;
  }

  /**
   * @author Horst Mueller
   */
  public int getPercentValue()
  {
    return _percentValue;
  }

  /**
   * @author Horst Mueller
   */
  public void setPercentValue(int percentValue)
  {
    _percentValue = percentValue;
  }

}
