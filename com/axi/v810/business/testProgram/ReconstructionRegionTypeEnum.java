package com.axi.v810.business.testProgram;

import com.axi.util.*;

/**
 * @author Matt Wharton
 */
public class ReconstructionRegionTypeEnum extends com.axi.util.Enum
{
  private static int _index = 0;

  public static final ReconstructionRegionTypeEnum VERIFICATION = new ReconstructionRegionTypeEnum(_index++);
  public static final ReconstructionRegionTypeEnum ALIGNMENT = new ReconstructionRegionTypeEnum(_index++);
  public static final ReconstructionRegionTypeEnum INSPECTION = new ReconstructionRegionTypeEnum(_index++);

  /**
   * @author Matt Wharton
   */
  private ReconstructionRegionTypeEnum(int id)
  {
    super(id);
  }
}
