package com.axi.v810.business.testProgram;

import java.io.*;

import com.axi.util.*;

/**
 * @author George A. David
 */
public class FocusSearchParameters implements Serializable
{
  private FocusRegion _focusRegion;
  private FocusProfileShapeEnum _focusProfileShape;
  private int _zCurveWidthInNanoMeters;
  private int _gradientWeightingMagnitudePercentage;
  private int _gradientWeightingOrientationInDegrees;
  private int _horizontalSharpnessComponentPercentage;
  private int _initialWaveletLevel=0;

  /**
   * @author George A. David
   */
  public FocusSearchParameters()
  {
    // By default, do not use gradient weighting, and have
    // horizontal and vertical sharpness components contribute equally
    _gradientWeightingMagnitudePercentage = 0;
    _horizontalSharpnessComponentPercentage = 50;
    
    // 0 or 1 = let system decide initial level to use.
    _initialWaveletLevel=0;
  }

  /**
   * @author George A. David
   */
  public void setFocusProfileShape(FocusProfileShapeEnum focusProfileShape)
  {
    Assert.expect(focusProfileShape != null);

    _focusProfileShape = focusProfileShape;
  }

  /**
   * @author George A. David
   */
  public FocusProfileShapeEnum getFocusProfileShape()
  {
    Assert.expect(_focusProfileShape != null);

    return _focusProfileShape;
  }

  /**
   * @author George A. David
   */
  public void setFocusRegion(FocusRegion focusRegion)
  {
    Assert.expect(focusRegion != null);

    _focusRegion = focusRegion;
  }

  /**
   * @author George A. David
   */
  public FocusRegion getFocusRegion()
  {
    Assert.expect(_focusRegion != null);

    return _focusRegion;
  }

  /**
   * @author Roy Williams
   */
  public int getZCurveWidthInNanoMeters()
  {
    return _zCurveWidthInNanoMeters;
  }

  /**
   * @author Roy Williams
   */
  public void setZCurveWidthInNanoMeters(int zCurveWidthInNanoMeters)
  {
    Assert.expect(zCurveWidthInNanoMeters >= 0);
    _zCurveWidthInNanoMeters = zCurveWidthInNanoMeters;
  }

  /**
   * @author John Heumann
   */
  public void setGradientWeightingMagnitudePercentage(int magnitude)
  {
    Assert.expect(magnitude >= 0);
    _gradientWeightingMagnitudePercentage = magnitude;
  }

  /**
   * @author John Heumann
   */
  public int getGradientWeightingMagnitudePercentage()
  {
    return _gradientWeightingMagnitudePercentage;
  }

  /**
   * @author John Heumann
   */
  public void setGradientWeightingOrientationInDegrees(int orientation)
  {
    Assert.expect(orientation >= 0 && orientation <= 359);
    _gradientWeightingOrientationInDegrees = orientation;
  }

  /**
   * @author John Heumann
   */
  public int getGradientWeightingOrientationInDegrees()
  {
    return _gradientWeightingOrientationInDegrees;
  }

  /**
   * @author John Heumann
   */
  public void setHorizontalSharpnessComponentPercentage(int percentage)
  {
    Assert.expect(percentage >= 0);
    _horizontalSharpnessComponentPercentage = percentage;
  }

  /**
   * @author John Heumann
   */
  public int getHorizontalSharpnessComponentPercentage()
  {
    return _horizontalSharpnessComponentPercentage;
  }
  
  /**
   * @author Chnee Khang Wah, 2013-09-11, controllable wavelet level
   */
  public void setInitialWaveletLevel(int initialWaveletLevel)
  {
    _initialWaveletLevel = initialWaveletLevel;
  }
  
  /**
   * @author Chnee Khang Wah, 2013-09-11, controllable wavelet level
   */
  public int getInitialWaveletLevel()
  {
    return _initialWaveletLevel;
  }

}
