package com.axi.v810.business;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class SoftwareAlreadyRunningBusinessException extends BusinessException
{
  /**
   * @author Bill Darbie
   */
  public SoftwareAlreadyRunningBusinessException()
  {
    super(new LocalizedString("BUS_SOFTWARE_ALREADY_RUNNING_KEY", null));
  }
}
