package com.axi.v810.business.cAndD;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.hardware.autoCal.*;

/**
 * Simple container class to hold folders and list of hardware task interfaces
 * to provide structure for any GUI that wants to execute hardware tasks like
 * confirmations, adjusements or diagnostics.
 *
 * @author Reid Hayhow
 */
public class HardwareTaskFolder
{

  private List<HardwareTaskFolder> _folders;
  private List<HardwareTaskExecutionInterface> _tasks;
  private String _name;

  /**
   * Constructor for the folder class
   *
   * @author Reid Hayhow
   */
  protected HardwareTaskFolder(String name)
  {
    Assert.expect(name != null);

    _name = name;
    _folders = new ArrayList<HardwareTaskFolder>();
    _tasks = new ArrayList<HardwareTaskExecutionInterface>();
  }



  /**
   * Access to the folder name String
   *
   * @author Reid Hayhow
   */
  public String getFolderName()
  {
    return _name;
  }

  /**
   * Get method to provide access to the list of folders in this folder
   *
   * @author Reid Hayhow
   */
  public List<HardwareTaskFolder> getListOfFolders()
  {
    return _folders;
  }

  /**
   * Get method to provide access to the list of tasks in this folder
   *
   * @author Reid Hayhow
   */
  public List<HardwareTaskExecutionInterface> getListOfTasks()
  {
    return _tasks;
  }

  /**
   * Internal set method to create a list of tasks
   *
   * @author Reid Hayhow
   */
  protected void setListOfTasks(List<HardwareTaskExecutionInterface> tasks)
  {
    Assert.expect(tasks != null);

    _tasks = tasks;
  }

  /**
   * Internal set method to create a list of folders
   *
   * @author Reid Hayhow
   */
  protected void setListOfFolders(List<HardwareTaskFolder> folders)
  {
    Assert.expect(folders != null);

    _folders = folders;
  }

  /**
   * @return the name of the folder
   * @author Erica Wheatcroft
   */
  public String toString()
  {
    return getFolderName();
  }

  /**
   * UI access to set/unset stop on fail for ALL Hardware Tasks.
   *
   * @author Reid Hayhow
   */
  public void setStopOnFail(boolean taskShouldStopOnFail)
  {
    HardwareTask.setStopOnFail(taskShouldStopOnFail);
  }
}
