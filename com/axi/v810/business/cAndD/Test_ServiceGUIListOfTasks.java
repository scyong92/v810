package com.axi.v810.business.cAndD;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.*;

/**
 * Test harness for the folder classes needed for GUI display.
 *
 * @author Reid Hayhow
 */
class Test_ServiceGUIListOfTasks extends UnitTest implements Observer
{
  //Simple internal counter to keep track of the number of observable 'updates'
  //the class received
  private int _countOfUpdates = 0;
  //Save off the hardware.calib because running tasks will change it by adding timestamps
  private TestUtils _utils = new TestUtils();

  /**
   * Test class guts. Become an observer, get a task interface and execute it.
   * Make sure the task passes. Check the folder structure and move on.
   *
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    ListOfManualHardwareTasks listOfTasks = ListOfManualHardwareTasks.getInstance();

    //Make us an observer for all tasks
    HardwareTask.addObserver(this);

    //Execute a task (in this case a fast one, the calibration file confirmation)
    HardwareTaskFolder rootFolder = listOfTasks.getRootFolder();

    //First, get a task interface from the list of tasks and show that
    //we can execute it

    //Get the root folder first
    List<HardwareTaskFolder> listOfFolders = rootFolder.getListOfFolders();

    //Get the Scheduled tasks folder magic number 0 for now
    HardwareTaskFolder scheduledTasksFolder = listOfFolders.get(0);

    //Get the Manual Tasks folder magic number 1 for now
    HardwareTaskFolder manualTasksFolder = listOfFolders.get(1);

    //Guarantee that we got the confirmations folder
    Expect.expect(manualTasksFolder.toString().equalsIgnoreCase(StringLocalizer.keyToString("ACD_MANUAL_TASKS_KEY")));

    try
    {
      XrayTester.getInstance().startup();

      //Get a specific task to execute, in this case the cal file confirmation
      HardwareTaskExecutionInterface taskInterface = scheduledTasksFolder.getListOfTasks().get(3);

      //Guarantee that we got the right task
      Expect.expect(taskInterface.toString().equalsIgnoreCase(StringLocalizer.keyToString("CD_CAMERA_CAL_FILE_CONFIRMATION_KEY")));

      HardwareTaskStatusEnum taskStatus = taskInterface.getTaskStatus();

      Expect.expect(taskStatus.equals(HardwareTaskStatusEnum.NOT_RUN));

      //Execute the task
      taskInterface.executeUnconditionally();

      //Check on the status of the task
      taskStatus = taskInterface.getTaskStatus();

      //The task should have passed
      Expect.expect(taskStatus.equals(HardwareTaskStatusEnum.PASSED));

      //Now check our ability to recurse through the list of tasks
      List<HardwareTaskFolder> folders = rootFolder.getListOfFolders();

      //Folders can be empty but not null
      Expect.expect(folders != null);

      //Adding check for Erica
      for (HardwareTaskFolder folder : folders)
      {
        //Sub folders can be empty but not null either
        Expect.expect(folder != null);
        List<HardwareTaskFolder> subFolders = folder.getListOfFolders();
        Expect.expect(subFolders != null);
      }

      //check the whole deeper structure for validity
      validateFolderStructure(rootFolder);

    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace(os);
      Expect.expect(false);
    }
    //Give the task observable thread time to update this thread
    try
    {
      Thread.currentThread().sleep(500);
    }
    catch (InterruptedException ex1)
    {
      ex1.printStackTrace(os);
      Expect.expect(false);
    }

    //We executed exactly one task
    Expect.expect(_countOfUpdates == 1);

    /***  Test set stopOnFail *****/
    try
    {
      //Get a specific task to execute, in this case the cal file confirmation
      HardwareTaskExecutionInterface confirmAllCommunicationsInterface =
      scheduledTasksFolder.getListOfTasks().get(0);
      scheduledTasksFolder.setStopOnFail(true);

      //Guarantee that we got the right task
      Expect.expect(confirmAllCommunicationsInterface.toString().equalsIgnoreCase(StringLocalizer.keyToString("CD_CONFIRM_ALL_COMMUNICATIONS_KEY")));

      HardwareTaskStatusEnum taskStatus = confirmAllCommunicationsInterface.getTaskStatus();

      Expect.expect(taskStatus.equals(HardwareTaskStatusEnum.NOT_RUN));

      //Execute the task
      confirmAllCommunicationsInterface.executeUnconditionally();

      //Check on the status of the task
      taskStatus = confirmAllCommunicationsInterface.getTaskStatus();

      //The task should have passed
      Expect.expect(taskStatus.equals(HardwareTaskStatusEnum.FAILED));
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace(os);
    }

    //Give the task observable thread time to update this thread
    try
    {
      Thread.currentThread().sleep(500);
    }
    catch (InterruptedException ex1)
    {
      ex1.printStackTrace(os);
    }

    //We executed exactly three tasks CameraCal, SwitchCom
    Expect.expect(_countOfUpdates == 2);


    //Test execution of dependent tasks
    boolean taskStatus = false;
    try
    {
      //This will execute three tasks, Grayscale cal, Cal Confirm and Hysteresis
      TestUtils.initConfigForRegressionTest();
      HardwareTaskExecutionInterface taskInterface = CalSystemFiducialEventDrivenHardwareTask.getInstance();

      //For now the returned status will be a failure but this may change, so I won't
      //Expect based on it
      taskStatus = taskInterface.executeDependentTasks(0);

      TestUtils.restoreConfigAfterRegressionTest();

    }
    //Shouldn't see any exceptions
    catch(XrayTesterException ex)
    {
      if(ex instanceof HardwareTaskExecutionException)
      {
        //xray spot call will fail, so clean up after it here
        TestUtils.restoreConfigAfterRegressionTest();
      }
      else
      {
        ex.printStackTrace(os);
      }
    }
    finally
    {
    }

    //executeDependentTasks updates, but we will only see two tasks execute
    //Grayscale cal and Cal Confirm
    Expect.expect(_countOfUpdates == 3);

    //Now check task types on a per-folder basis
    List<HardwareTaskExecutionInterface> confirmationTaskInterfaceList = manualTasksFolder.getListOfFolders().get(0).getListOfTasks();

    //Check Confirmations in the confirmations folder
    for(HardwareTaskExecutionInterface taskInterface : confirmationTaskInterfaceList)
    {
      //I need access to HardwareTask methods, so cast the interface to get them
      HardwareTask castedTask = (HardwareTask)taskInterface;

      //Get the task type from the casted task
      HardwareTaskTypeEnum taskType = castedTask.getHardwareTaskType();

      //It must be a confirmation
      Expect.expect(taskType == HardwareTaskTypeEnum.MANUAL_CONFIRMATION_TASK_TYPE, castedTask.getName() );
    }

    //Now check task types on a per-folder basis
    List<HardwareTaskExecutionInterface> diagnosticTaskInterfaceList = manualTasksFolder.getListOfFolders().get(1).getListOfTasks();

    //Check Confirmations in the confirmations folder
    for(HardwareTaskExecutionInterface taskInterface : diagnosticTaskInterfaceList)
    {
      //I need access to HardwareTask methods, so cast the interface to get them
      HardwareTask castedTask = (HardwareTask)taskInterface;

      //Get the task type from the casted task
      HardwareTaskTypeEnum taskType = castedTask.getHardwareTaskType();

      //It must be a confirmation
      Expect.expect(taskType == HardwareTaskTypeEnum.MANUAL_DIAGNOSTIC_TASK_TYPE, castedTask.getName() );
    }

    //Check Adjustments in the adjustments folder
    List<HardwareTaskExecutionInterface> adjustmentTaskInterfaceList = scheduledTasksFolder.getListOfTasks();

    //Check each task in the adjustment folder
    for(HardwareTaskExecutionInterface taskInterface : adjustmentTaskInterfaceList)
    {
      //I need access to HardwareTask methods, so cast the interface to get them
      HardwareTask castedTask = (HardwareTask)taskInterface;

      //Get the task type from the casted task
      HardwareTaskTypeEnum taskType = castedTask.getHardwareTaskType();

      //It must be an adjustment
      Expect.expect(taskType == HardwareTaskTypeEnum.ADJUSTMENT_TASK_TYPE ||
                    taskType == HardwareTaskTypeEnum.AUTOMATIC_CONFIRMATION_TASK_TYPE, castedTask.getName() );
    }
  }

  /**
   * Main test class
   *
   * @author Reid Hayhow
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ServiceGUIListOfTasks());
  }

  /**
   * Recursively descend through list structure and guarantee that it has lists
   * and elements where they are expected
   *
   * @author Reid Hayhow
   */
  private void validateFolderStructure(HardwareTaskFolder baseFolder)
  {
    //get the list of folders at this node and the iterator for that list
    ListIterator<HardwareTaskFolder> iter = baseFolder.getListOfFolders().listIterator();

    while (iter.hasNext())
    {
      //Folders can have sub folders, so check them
      HardwareTaskFolder subFolder = iter.next();

      //No null sub folders allowed
      Expect.expect(subFolder != null);

      //Folders must have a name
      Expect.expect(subFolder.getFolderName() != null);

      //Can't have null list of tasks either
      Expect.expect(subFolder.getListOfTasks() != null);
      validateTaskList(subFolder.getListOfTasks());

      //Descend the list of sub folders
      validateFolderStructure(subFolder);
    }
  }

  /**
   * Quick check for the task list
   *
   * @author Reid Hayhow
   */
  private void validateTaskList(List<HardwareTaskExecutionInterface> taskList)
  {
    //Get a list iterator for the list
    ListIterator<HardwareTaskExecutionInterface> taskIter = taskList.listIterator();

    //The main check right now is to make sure the task has a name
    while (taskIter.hasNext())
    {
      Expect.expect(taskIter.next().getName() != null);
    }
  }

  /**
   * Simple method to get updates from the Observable. All it does is keep count
   * of the number times it is called.
   *
   * @author Reid Hayhow
   */
  public void update(Observable o, Object arg)
  {
    _countOfUpdates++;
  }

  /**
   * Copy the golden limits into place before starting the hardware. Also copy
   * the calib file (it will get clobbered).
   *
   * @author Reid Hayhow
   */
  protected void setupBeforeTest()
  {
    _utils.backupHWCalib(getTestSourceDir());
    _utils.backupHWConfig(getTestSourceDir());
  }

  /**
   * Restore the original limits into place when done. Also copy
   * the original calib file back.
   *
   * @author Reid Hayhow
   */
  protected void restoreAfterTest()
  {
    //restore the saved hardware.calib without timestamp
    _utils.restoreHWCalib(getTestSourceDir());
    _utils.restoreHWConfig(getTestSourceDir());
  }
}
