package com.axi.v810.business.cAndD;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.license.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class provides the list of interfaces the Service GUI should use to run
 * Adjustment, Confirmation and Diagnostic tasks. It also contains the folder
 * structure to display in the GUI and populates the list of interfaces.
 *
 * @author Reid Hayhow
 */
public class ListOfManualHardwareTasks extends HardwareTaskFolder
{
  //The root folder for the Service GUI display of Adjustment, Confirmation
  //and Diagnostic tasks
  static private HardwareTaskFolder _rootFolder;

  private static Config _config = Config.getInstance();

  //Instance of this list
  static private ListOfManualHardwareTasks _instance;
  private static boolean _earlyProtoHardware = false;
  
  private static boolean _enablePsp = _config.getBooleanValue(SoftwareConfigEnum.ENABLE_PSP);
  private static PspSettingEnum _settingEnum = PspSettingEnum.getEnum(Config.getInstance().getIntValue(HardwareConfigEnum.PSP_SETTING));

  private static boolean _enableCDNABenchmark = false;
  /**
   * @author Reid Hayhow
   */
  private ListOfManualHardwareTasks()
  {
    //The list of tasks inherits from the folder but shouldn't have a name
    super("");
    
    _enableCDNABenchmark = Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_CDNA_BENCHMARK);
  }

  /**
   * Instance access for the list of tasks to display and run in the Service
   * GUI
   *
   * @author Reid Hayhow
   */
  static public ListOfManualHardwareTasks getInstance()
  {
    if(_instance == null)
    {
      _rootFolder = new HardwareTaskFolder(StringLocalizer.keyToString("ACD_ROOT_FOLDER_KEY"));

      _instance = new ListOfManualHardwareTasks();
      buildSubFolders();
    }
    return _instance;
  }

  /**
   * @author Reid Hayhow
   */
  public HardwareTaskFolder getRootFolder()
  {
    Assert.expect(_rootFolder != null);

    return _rootFolder;
  }

  /**
   * Method to build the folders under the root folder. Currently there should
   * only be three folders, Adjustments, Confirmations and Diagnostics.
   *
   * @author Reid Hayhow
   */
  private static void buildSubFolders()
  {
    HardwareTaskFolder scheduledTasks = new HardwareTaskFolder(StringLocalizer.keyToString("ACD_SCHEDULED_TASKS_KEY"));

    HardwareTaskFolder manualTasks = new HardwareTaskFolder(StringLocalizer.keyToString("ACD_MANUAL_TASKS_KEY"));
    HardwareTaskFolder manualCalibrations = new HardwareTaskFolder(StringLocalizer.keyToString("ACD_MANUAL_ADJUSTMENTS_KEY"));
    HardwareTaskFolder manualConfirmations = new HardwareTaskFolder(StringLocalizer.keyToString("ACD_MANUAL_CONFIRMATION_KEY"));
    HardwareTaskFolder manualBenchmarking = new HardwareTaskFolder(StringLocalizer.keyToString("ACD_MANUAL_BENCHMARK_KEY"));
    HardwareTaskFolder manualDiagnostics = new HardwareTaskFolder(StringLocalizer.keyToString("ACD_MANUAL_DIAGNOSTICS_KEY"));
    HardwareTaskFolder manualOpticalConfirmations = new HardwareTaskFolder(StringLocalizer.keyToString("ACD_MANUAL_OPTICAL_CONFIRMATION_KEY"));
    HardwareTaskFolder manualOpticalCalibrations = new HardwareTaskFolder(StringLocalizer.keyToString("ACD_MANUAL_OPTICAL_ADJUSTMENTS_KEY"));


    List<HardwareTaskFolder> manualFolders = new ArrayList<HardwareTaskFolder>();

    //Create the list of folders that will live under the root folder
    List<HardwareTaskFolder> rootFolderList = new ArrayList<HardwareTaskFolder>();

    //Four folders at the root level, one each for adjustment, automatic confirm
   //manual confirm and manual diag
    rootFolderList.add(scheduledTasks);

//    try
//    {
//      if (LicenseManager.getInstance().isDeveloperSystemEnabled())
//      {
//        rootFolderList.add(manualTasks);
//      }
//    }
//    catch (BusinessException ex)
//    {
//    //Do nothing
//    }
    rootFolderList.add(manualTasks);

    manualFolders.add(manualConfirmations);
    
    if(_enableCDNABenchmark) manualFolders.add(manualBenchmarking);
     
    //Ngie Xing
    if(XrayTester.isS2EXEnabled() == false)
    {
      manualFolders.add(manualDiagnostics);
      manualFolders.add(manualCalibrations);
    }
    
    if (_enablePsp)
    {      
      manualFolders.add(manualOpticalCalibrations);
      manualFolders.add(manualOpticalConfirmations);
    }

    manualTasks.setListOfFolders(manualFolders);

    //Set the root folder to have the list of folders that go under it
    _rootFolder.setListOfFolders(rootFolderList);

    //Now call the methods to create the folders and tasks for Adjustments
    //confirmations and diagnostics
    buildListOfScheduledTasks(scheduledTasks);
    buildListOfManualConfirmations(manualConfirmations);
    if(_enableCDNABenchmark) buildListOfManualBenchmarking(manualBenchmarking);
    
    //Ngie Xing
    if (XrayTester.isS2EXEnabled() == false)
    {
      buildListOfManualDiagnostics(manualDiagnostics);
      buildListOfManualAdjustments(manualCalibrations);
    }
    
    if (_enablePsp)
    {      
      buildListOfManualOpticalAdjustments(manualOpticalCalibrations);
      buildListOfManualOpticalConfirmations(manualOpticalConfirmations);
    }
  }

  /**
   * Method to create the folder structure for Calibrations and create the
   * specific list of calibrations.
   *
   * @author Reid Hayhow
   */
  private static void buildListOfScheduledTasks(HardwareTaskFolder scheduledTaskFolder)
  {
    //First get the interface to each of the calibration tasks
    HardwareTaskExecutionInterface confirmAreaModeImages =
      ConfirmAreaModeImages.getInstance();

    HardwareTaskExecutionInterface calGrayscaleInterface =
      CalGrayscale.getInstance();

     HardwareTaskExecutionInterface calSystemFiducialInterface =
      CalSystemFiducialEventDrivenHardwareTask.getInstance();

    HardwareTaskExecutionInterface calXraySpotInterface =
      CalXraySpotCoordinateEventDrivenHardwareTask.getInstance();

    HardwareTaskExecutionInterface calHysteresisInterface =
      CalHysteresisEventDrivenHardwareTask.getInstance();

    HardwareTaskExecutionInterface calMagnificationInterface =
      CalMagnification.getInstance();
    
    HardwareTaskExecutionInterface calHighMagXraySpotInterface =
      CalHighMagXraySpotCoordinateEventDrivenHardwareTask.getInstance();
    
    HardwareTaskExecutionInterface calHighMagHysteresisInterface =
      CalHighMagHysteresisEventDrivenHardwareTask.getInstance();
        
    HardwareTaskExecutionInterface calHighMagSystemFiducialInterface =
      CalHighMagSystemFiducialEventDrivenHardwareTask.getInstance();
    
    HardwareTaskExecutionInterface calHighMagnificationInterface =
      CalHighMagnification.getInstance();
    
    //Get the interface to each of the confirmation tasks
    HardwareTaskExecutionInterface cameraCalFileInterface =
      ConfirmCameraCalibration.getInstance();

    HardwareTaskExecutionInterface cameraHighMagCalFileInterface =
      ConfirmCameraHighMagCalibration.getInstance();
        
    HardwareTaskExecutionInterface cameraLightAll =
      ConfirmCameraLightAllGroups.getInstance();

    HardwareTaskExecutionInterface calHighMagGrayscaleInterface =
      CalHighMagGrayscale.getInstance();

    HardwareTaskExecutionInterface cameraHighMagLightAll =
      ConfirmCameraHighMagLightAllGroups.getInstance();

    HardwareTaskExecutionInterface cameraLightNoPanel =
      ConfirmCameraLightImageWithNoPanel.getInstance();
    
    HardwareTaskExecutionInterface cameraHighMagLightNoPanel =
      ConfirmCameraHighMagLightImageWithNoPanel.getInstance();
     

    HardwareTaskExecutionInterface cameraDarkInterface =
      ConfirmCameraDarkImageTask.getInstance();

    HardwareTaskExecutionInterface cameraHighMagDarkInterface =
      ConfirmCameraHighMagDarkImageTask.getInstance();


    HardwareTaskExecutionInterface xraySourceInterface;
    XraySourceTypeEnum sourceTypeEnum = AbstractXraySource.getSourceTypeEnum();
    if (sourceTypeEnum.equals(XraySourceTypeEnum.LEGACY))
    {
      xraySourceInterface = ConfirmLegacyXRaySource.getInstance();
    }
    else if (sourceTypeEnum.equals(XraySourceTypeEnum.HTUBE))
    {
      xraySourceInterface = ConfirmHTubeXRaySource.getInstance();
    }
    else
    {
      Assert.expect(false, "unknown xraySourceType from config file, HardwareConfigEnum bug?");
      xraySourceInterface = null;
    }

    HardwareTaskExecutionInterface systemMTFInterface =
      ConfirmLowMagSystemMTF.getInstance();

    HardwareTaskExecutionInterface magnificationCouponInterface =
      ConfirmMagnificationCoupon.getInstance();
    
    HardwareTaskExecutionInterface systemMTFInterfaceInHighMag =
      ConfirmHighMagSystemMTF.getInstance();

    HardwareTaskExecutionInterface magnificationCouponInterfaceInHighMag =
      ConfirmMagnificationCouponForHighMag.getInstance();

    HardwareTaskExecutionInterface communicationTestInterface =
      ConfirmAllCommunication.getInstance();

    //Then create the list to hold the interfaces in the folder
    List<HardwareTaskExecutionInterface> scheduledTasks =
      new ArrayList<HardwareTaskExecutionInterface>();

    //Add the interfaces IN ORDER OF APPEARANCE to the list
    scheduledTasks.add(communicationTestInterface);
    scheduledTasks.add(confirmAreaModeImages);
    scheduledTasks.add(calGrayscaleInterface);

    scheduledTasks.add(cameraCalFileInterface);

    scheduledTasks.add(cameraDarkInterface);

    //If we have early prototype hardware, we have to use camera groups
    /** @todo rfh remove this when LP2 goes away */
    if(_earlyProtoHardware)
    {
      scheduledTasks.add(cameraLightAll);
    }
    //Otherwise the panel will be out of the system, so use the new confirm
    //without a panel in the system
    else
    {
      scheduledTasks.add(cameraLightNoPanel);
    }

    scheduledTasks.add(calXraySpotInterface);
    scheduledTasks.add(calHysteresisInterface);
    scheduledTasks.add(calSystemFiducialInterface);
    scheduledTasks.add(calMagnificationInterface);
    scheduledTasks.add(systemMTFInterface);
    scheduledTasks.add(magnificationCouponInterface);
//    scheduledTasks.add(systemMTFInterfaceInHighMag);
//    scheduledTasks.add(magnificationCouponInterfaceInHighMag);
    
    try
    {
      if(LicenseManager.isVariableMagnificationEnabled())
      {
        scheduledTasks.add(calHighMagGrayscaleInterface);
        scheduledTasks.add(cameraHighMagCalFileInterface);
        scheduledTasks.add(cameraHighMagDarkInterface);
        
        //If we have early prototype hardware, we have to use camera groups
        /** @todo rfh remove this when LP2 goes away */
        if(_earlyProtoHardware)
        {
          scheduledTasks.add(cameraHighMagLightAll);
        }
        //Otherwise the panel will be out of the system, so use the new confirm
        //without a panel in the system
        else
        {
          scheduledTasks.add(cameraHighMagLightNoPanel);
        }
        scheduledTasks.add(calHighMagXraySpotInterface);
        scheduledTasks.add(calHighMagHysteresisInterface);
        scheduledTasks.add(calHighMagSystemFiducialInterface);
        scheduledTasks.add(calHighMagnificationInterface);
        scheduledTasks.add(systemMTFInterfaceInHighMag);
        scheduledTasks.add(magnificationCouponInterfaceInHighMag);
      }
    }
    catch(BusinessException be)
    {
      // do nothing
    }

//    scheduledTasks.add(systemMTFInterface);
//    scheduledTasks.add(magnificationCouponInterface);

    scheduledTasks.add(xraySourceInterface);

    //Finally add the list to the calibration folder
    scheduledTaskFolder.setListOfTasks(scheduledTasks);
  }

  /**
   * Method to create the folder structure and list of manual confirmations
   *
   * @author Reid Hayhow
   */
  private static void buildListOfManualConfirmations(HardwareTaskFolder manualConfirmationFolder)
  {
    /** XCR-3353 Failed to run manual confirmation task on XXL system
     *  Commented out by Cheah Lee Herng 15 Mar 2016, due to S2EX and XXL system do not have fiducial
     *  installed in adjustable-rail.
    //Get the interface to each of the confirmation tasks
    HardwareTaskExecutionInterface systemFiducialInterface =
      ConfirmSystemFiducial.getInstance();
    
        //Get the interface to each of the confirmation tasks
    HardwareTaskExecutionInterface highMagSystemFiducialInterface =
      ConfirmHighMagSystemFiducial.getInstance();

    HardwareTaskExecutionInterface systemFiducialBInterface =
      ConfirmSystemFiducialB.getInstance();
    
    HardwareTaskExecutionInterface highMagSystemFiducialBInterface =
      ConfirmHighMagSystemFiducialB.getInstance();
      * */

    HardwareTaskExecutionInterface panelHandlingInterface =
      ConfirmPanelHandlingTask.getInstance();

    HardwareTaskExecutionInterface panelPositioningTestInterface =
      ConfirmPanelPositioningTask.getInstance();

    HardwareTaskExecutionInterface xRayCylinderInterface =
      ConfirmXRayCylinderTask.getInstance();

    //Swee Yee Wong
    //PSFC Base image quality for 14 cameras
    HardwareTaskExecutionInterface confirmLowMagFullSystemMTF =
      ConfirmLowMagFullSystemMTF.getInstance();
    HardwareTaskExecutionInterface confirmHighMagFullSystemMTF =
      ConfirmHighMagFullSystemMTF.getInstance();
    
    //Swee Yee Wong - Camera debugging purpose
    HardwareTaskExecutionInterface confirmXraySpotReverseInterface =
      ConfirmXraySpotCoordinateForReverseDirectionTask.getInstance();
    
    HardwareTaskExecutionInterface confirmCameraCalibrationImages = 
      ConfirmCameraCalibrationImages.getInstance();
    
    HardwareTaskExecutionInterface confirmHighMagCameraCalibrationImages = 
      ConfirmHighMagCameraCalibrationImages.getInstance();
    
    HardwareTaskExecutionInterface xRayCPMotorInterface =
      ConfirmXRayCPMotorTask.getInstance();
    //Create the list to hold the interfaces
    List<HardwareTaskExecutionInterface> manualConfirmationTasks =
      new ArrayList<HardwareTaskExecutionInterface>();

    //Anthony Fong - CameraQuality
    //HardwareTaskExecutionInterface confirmCameraQualityInterface =
    //  ConfirmCameraQuality.getInstance();

    //Add the interfaces to the list in the order they will appear in the GUI
    //Ngie Xing
    // XCR-3353 Failed to run manual confirmation task on XXL system
    //if (XrayTester.isS2EXEnabled() == false)
    //{
    //  manualConfirmationTasks.add(systemFiducialInterface);
    //  manualConfirmationTasks.add(systemFiducialBInterface);
    //}
    
    try
    {
      if(LicenseManager.isVariableMagnificationEnabled())
      {
        if (XrayTester.isS2EXEnabled() && XrayCPMotorActuator.isInstalled())
        {
          manualConfirmationTasks.add(xRayCPMotorInterface);
        }
        else if ((XrayTester.isS2EXEnabled() == false) && XrayCylinderActuator.isInstalled())
        {
          // XCR-3321 Failed to run High Mag Adjustable Rail Image Quality Confirmation
          //manualConfirmationTasks.add(highMagSystemFiducialInterface);
          //manualConfirmationTasks.add(highMagSystemFiducialBInterface);
          manualConfirmationTasks.add(xRayCylinderInterface);
        }
      }
    }
    catch(BusinessException be)
    {
      // do nothing
    }
    manualConfirmationTasks.add(panelHandlingInterface);
    manualConfirmationTasks.add(panelPositioningTestInterface);
    //manualConfirmationTasks.add(confirmCameraQualityInterface);

    try
    {
      if (_config.isMultiAnglePostProcessing())
      {
        manualConfirmationTasks.add(confirmLowMagFullSystemMTF);
        if (LicenseManager.isVariableMagnificationEnabled())
        {
          manualConfirmationTasks.add(confirmHighMagFullSystemMTF);
        }
      }
    }
    catch (BusinessException be)
    {
      // do nothing
    }
    
    try
    {
      //Swee Yee Wong - Camera debugging purpose
      manualConfirmationTasks.add(confirmXraySpotReverseInterface);
      manualConfirmationTasks.add(confirmCameraCalibrationImages);
      if (LicenseManager.isVariableMagnificationEnabled())
      {
        manualConfirmationTasks.add(confirmHighMagCameraCalibrationImages);
      }
    }
    catch (BusinessException be)
    {
      // do nothing
    }
    
    //Add the list of interfaces to the Manual Confirmation folder
    manualConfirmationFolder.setListOfTasks(manualConfirmationTasks);
  }

  /**
   * Method to create the folder structure and list of manual Benchmarking
   *
   * @author Anthony Fong
   */
  private static void buildListOfManualBenchmarking(HardwareTaskFolder manualBenchmarkingFolder)
  {
    HardwareTaskExecutionInterface leftOuterBarrierInterface =
      ConfirmBenchmarkTask.getInstance(ConfirmBenchmarkEnum.LEFT_OUTER_BARRIER_OPEN_CLOSE);
    
    HardwareTaskExecutionInterface rightOuterBarrierInterface =
      ConfirmBenchmarkTask.getInstance(ConfirmBenchmarkEnum.RIGHT_OUTER_BARRIER_OPEN_CLOSE);
    
    HardwareTaskExecutionInterface innerBarrierInterface =
      ConfirmBenchmarkTask.getInstance(ConfirmBenchmarkEnum.INNER_BARRIER_OPEN_CLOSE);
    
     HardwareTaskExecutionInterface leftPIPInterface =
      ConfirmBenchmarkTask.getInstance(ConfirmBenchmarkEnum.LEFT_PIP_IN_OUT);   
                
     HardwareTaskExecutionInterface rightPIPInterface =
      ConfirmBenchmarkTask.getInstance(ConfirmBenchmarkEnum.RIGHT_PIP_IN_OUT);   
 
     HardwareTaskExecutionInterface panelClampsInterface =
      ConfirmBenchmarkTask.getInstance(ConfirmBenchmarkEnum.PANEL_CLAMPS_OPEN_CLOSE);   
 
     HardwareTaskExecutionInterface panelHanldeRailWidthAdjustInterface =
      ConfirmBenchmarkTask.getInstance(ConfirmBenchmarkEnum.PANEL_HANDLER_RAIL_WIDTH_ADJUST);   
 
     HardwareTaskExecutionInterface panelPositionerLoadUnloadInterface =
      ConfirmBenchmarkTask.getInstance(ConfirmBenchmarkEnum.PANEL_POSITIONER_LOAD_UNLOAD);   
 
     HardwareTaskExecutionInterface xRayTubeActuatorInterface =
      ConfirmBenchmarkTask.getInstance(ConfirmBenchmarkEnum.XRAY_TUBE_ACTUATOR_UP_DOWN);   
    
     HardwareTaskExecutionInterface longPathScanningInterface =
      ConfirmBenchmarkTask.getInstance(ConfirmBenchmarkEnum.LONG_PATH_SCANNING);   
    
     
    //Create the list to hold the interfaces
    List<HardwareTaskExecutionInterface> manualBenchmarkinTasks =
      new ArrayList<HardwareTaskExecutionInterface>();

    manualBenchmarkinTasks.add(leftOuterBarrierInterface);
    manualBenchmarkinTasks.add(rightOuterBarrierInterface);
    manualBenchmarkinTasks.add(innerBarrierInterface);
    manualBenchmarkinTasks.add(leftPIPInterface);
    manualBenchmarkinTasks.add(rightPIPInterface);
    manualBenchmarkinTasks.add(panelClampsInterface);
    manualBenchmarkinTasks.add(panelHanldeRailWidthAdjustInterface);
    manualBenchmarkinTasks.add(panelPositionerLoadUnloadInterface);
 
    try
    {
      if(LicenseManager.isVariableMagnificationEnabled())
      {
        if (XrayActuator.isInstalled())
        {
          manualBenchmarkinTasks.add(xRayTubeActuatorInterface);
        }
      }
    }
    catch(BusinessException be)
    {
      // do nothing
    } 
    manualBenchmarkinTasks.add(longPathScanningInterface);

    //Add the list of interfaces to the Manual Confirmation folder
    manualBenchmarkingFolder.setListOfTasks(manualBenchmarkinTasks);
  }
  
   
  /**
   * Method to create the folder structure and list of manual diagnostics
   *
   * @author Reid Hayhow
   */
  private static void buildListOfManualDiagnostics(HardwareTaskFolder manualDiagnosticFolder)
  {
    //Get the interface to each of the diagnostic tasks
    HardwareTaskExecutionInterface diagnoseSharpnessInterface =
      DiagnoseSharpness.getInstance();

    //Create the list to hold the interfaces
    List<HardwareTaskExecutionInterface> manualDiagnosticTasks =
      new ArrayList<HardwareTaskExecutionInterface>();

    //Add the interfaces to the list in the order they will appear in the GUI
    manualDiagnosticTasks.add(diagnoseSharpnessInterface);

    //Add the list of interfaces to the Manual Diagnostic folder
    manualDiagnosticFolder.setListOfTasks(manualDiagnosticTasks);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private static void buildListOfManualOpticalConfirmations(HardwareTaskFolder manualOpticalConfirmationFolder)
  {      
      //Create the list to hold the interfaces
      List<HardwareTaskExecutionInterface> manualOpticalConfirmationTasks =
        new ArrayList<HardwareTaskExecutionInterface>();
            
      if (_settingEnum.equals(PspSettingEnum.SETTING_BOTH))
      {
        manualOpticalConfirmationTasks.add(ConfirmFrontOpticalMeasurementTask.getInstance());
        manualOpticalConfirmationTasks.add(ConfirmRearOpticalMeasurementTask.getInstance());
      }
      else if (_settingEnum.equals(PspSettingEnum.SETTING_FRONT))
      {
        manualOpticalConfirmationTasks.add(ConfirmFrontOpticalMeasurementTask.getInstance());
      }
      else if (_settingEnum.equals(PspSettingEnum.SETTING_REAR))
      {
        manualOpticalConfirmationTasks.add(ConfirmRearOpticalMeasurementTask.getInstance());
      }
      
      //Add the list of interfaces to the Manual Optical Confirmation folder
      manualOpticalConfirmationFolder.setListOfTasks(manualOpticalConfirmationTasks);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private static void buildListOfManualOpticalAdjustments(HardwareTaskFolder manualOpticalAdjustmentFolder)
  {      
      HardwareTaskExecutionInterface opticalSystemFiducialInterface = 
              CalOpticalSystemFiducial.getInstance();

      HardwareTaskExecutionInterface opticalCalibrationInterface = CalOpticalCalibration.getInstance();      
      
      //Create the list to hold the interfaces
      List<HardwareTaskExecutionInterface> manualOpticalAdjustmentTasks =
        new ArrayList<HardwareTaskExecutionInterface>();
      
      manualOpticalAdjustmentTasks.add(opticalSystemFiducialInterface);
      manualOpticalAdjustmentTasks.add(opticalCalibrationInterface);
      
      // XCR-3314 Support PSP New Manual-Loaded Jig Plate
      if (PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_3) ||
          PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_4))
      {
        manualOpticalAdjustmentTasks.add(CalOpticalSystemOffsetAdjustment.getInstance());
      }
      
      //Add the list of interfaces to the Manual Optical Confirmation folder
      manualOpticalAdjustmentFolder.setListOfTasks(manualOpticalAdjustmentTasks);
  }

   /**
   * Method to create the folder structure and list of manual adjustments
   *
   * @author Reid Hayhow
   */
  private static void buildListOfManualAdjustments(HardwareTaskFolder manualAdjustmentsFolder)
  {
    HardwareTaskExecutionInterface interf = CalXraySpotDeflection.getInstance();
    //Create the list to hold the interfaces
    List<HardwareTaskExecutionInterface> manualAdjustmentTasks =
      new ArrayList<HardwareTaskExecutionInterface>();

    manualAdjustmentTasks.add(interf);

    manualAdjustmentsFolder.setListOfTasks(manualAdjustmentTasks);
  }
}
