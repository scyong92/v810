package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Cheah Lee Herng
 */
public class BoardMeshEventEnum extends ProjectChangeEventEnum 
{
  private static int _index = -1;
  public static BoardMeshEventEnum CREATE_OR_DESTROY = new BoardMeshEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static BoardMeshEventEnum ADD_MESH_POINTS = new BoardMeshEventEnum(++_index);
  public static BoardMeshEventEnum REMOVE_MESH_POINTS = new BoardMeshEventEnum(++_index);
  
  /**
   * @author Cheah Lee Herng 
   */
  private BoardMeshEventEnum(int id)
  {
    super(id);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private BoardMeshEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
