/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import java.io.*;

/**
 *
 * @author sheng-chuan.yong
 */
public class Test_ExpectedImageTemplateLearning extends UnitTest
{

  /**
   * @author sheng chuan
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ExpectedImageTemplateLearning());
  }
  
  /**
   * sheng chuan
   * @param is
   * @param os 
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    ExpectedImageTemplateLearning expectedImageTemplate = new ExpectedImageTemplateLearning();
    // populated
    try
    {
      expectedImageTemplate.getBoardName();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    
    Board board = new Board();
    expectedImageTemplate.setBoardName(board);
    Expect.expect(expectedImageTemplate.getBoardName() == board);
    
    try
    {
      expectedImageTemplate.getImageName();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    String imageName = "sample.png";
    expectedImageTemplate.setImageName(imageName);
    Expect.expect(expectedImageTemplate.getImageName() == imageName);
    
    try
    {
      expectedImageTemplate.getLearnedComponentName();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    String componentName = "C17_example";
    expectedImageTemplate.setLearnedComponentName(componentName);
    Expect.expect(expectedImageTemplate.getLearnedComponentName() == componentName);
    
    try
    {
      expectedImageTemplate.getLearnedComponentSlice();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    
    SliceNameEnum sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
    expectedImageTemplate.setLearnedComponentSlice(sliceNameEnum);
    Expect.expect(expectedImageTemplate.getLearnedComponentSlice() == sliceNameEnum);
    
    try
    {
      expectedImageTemplate.getLearnedComponentTemplate();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    Component component = new Component();
    expectedImageTemplate.setLearnedComponentTemplate(component);
    Expect.expect(expectedImageTemplate.getLearnedComponentTemplate() == component);
    
    try
    {
      expectedImageTemplate.getLearnedExpectedImage();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    Image image = new Image(10,10);
    expectedImageTemplate.setLearnedExpectedImage(image);
    Expect.expect(expectedImageTemplate.getLearnedExpectedImage() == image);
  }
  
}
