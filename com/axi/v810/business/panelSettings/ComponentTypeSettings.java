package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.ProjectHistoryLog;
import com.axi.v810.datastore.config.*;

/**
 * This class holds x-ray test information related to a ComponentType.
 * Each ComponentType object will have a reference to it's ComponentTypeSettings.
 * @author Bill Darbie
 */
public class ComponentTypeSettings implements Serializable
{
  // created by ComponentNdfReader, BoardTypeSettingsReader

  private BooleanRef _loaded; // ComponentNdfReader, BoardTypeSettingsReader, ComponentNoLoadSettings
  private ComponentType _componentType; // set by ComponentType
  private BooleanRef _usesOneSubtype;
  private PspEnum _psp = PspEnum.NO_PSP;

  private static transient ProjectObservable _projectObservable;
  //Project's history log CR - hsia-fen.tan
  private static transient ProjectHistoryLog _fileWriter;

  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
    _fileWriter = ProjectHistoryLog.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public ComponentTypeSettings()
  {
    _projectObservable.stateChanged(this, null, this, ComponentTypeSettingsEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.stateChanged(this, this, null, ComponentTypeSettingsEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void setComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);

    if (componentType == _componentType)
      return;

    ComponentType oldValue = _componentType;
    _projectObservable.setEnabled(false);
    try
    {
      _componentType = componentType;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, componentType, ComponentTypeSettingsEventEnum.COMPONENT_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public ComponentType getComponentType()
  {
    Assert.expect(_componentType != null);

    return _componentType;
  }

  /**
   * Set all PadTypes of this ComponentType to be inspected.
   * @author Bill Darbie
   */
  public void setInspected(boolean inspected)
  {
    _projectObservable.setEnabled(false);
    boolean oldValue = isInspected();
    try
    {
      for (PadType padType : _componentType.getPadTypes())
      {
        padType.getPadTypeSettings().setInspected(inspected);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, inspected, ComponentTypeSettingsEventEnum.INSPECTED);
  }

  /**
   * @return true if one or more PadTypes of this ComponentType are inspected.
   * @author Bill Darbie
   */
  public boolean isInspected()
  {
    boolean inspected = false;

    if (isLoaded())
    {
      // if the component is set to be tested, make sure at least one pad is going
      // to be tested before returning true.
      for (PadType padType : _componentType.getPadTypes())
      {
        if (padType.getPadTypeSettings().isInspected())
        {
          inspected = true;
          break;
        }
      }
    }

    return inspected;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isTestable()
  {
    for (PadType padType : getComponentType().getPadTypes())
    {
      for (Pad pad : padType.getPads())
      {
        if (pad.isTestable() == false)
          return false;
      }
    }

    return true;
  }

  /**
   * @return true if all PadTypes of this ComponentType are set to be inspected, EVEN if it this ComponentType is not loaded or not testable
   * @author Bill Darbie
   */
  public boolean areAllPadTypesInspectedFlagsSet()
  {
    boolean allInspected = true;
    for (PadType padType : _componentType.getPadTypes())
    {
      if (padType.getPadTypeSettings().isInspectedFlagSet() == false)
      {
        allInspected = false;
        break;
      }
    }

    return allInspected;
  }

  /**
   * @return true if all PadTypes of this ComponentType are NOT inspected, EVEN if it this ComponentType is not loaded or not testable
   * @author Bill Darbie
   */
  public boolean areAllPadTypesInspectedFlagNotSet()
  {
    boolean allNotInspected = true;
    for (PadType padType : _componentType.getPadTypes())
    {
      if (padType.getPadTypeSettings().isInspectedFlagSet())
      {
        allNotInspected = false;
        break;
      }
    }

    return allNotInspected;
  }

  /**
   * @return true if all PadTypes of this ComponentType are inspected.
   * @author Bill Darbie
   */
  public boolean areAllPadTypesTestable()
  {
    boolean allTestable = true;
    
    for (PadType padType : getComponentType().getPadTypes())
    {
      for (Pad pad : padType.getPads())
      {
        if (pad.getPadSettings().isTestable() == false)
        {
          allTestable = false;
          return allTestable;
        }
      }
    }

    return allTestable;
  }

  /**
   * @author George A. David
   */
  public boolean areAllPadTypesAtDefaultSignalCompensation()
  {
    boolean atDefaultLevel = true;

    for (PadType padType : _componentType.getPadTypes())
    {
      if (padType.getPadTypeSettings().getSignalCompensation().equals(SignalCompensationEnum.DEFAULT_LOW) == false)
      {
        atDefaultLevel = false;
        break;
      }
    }

    return atDefaultLevel;
  }

  /**
   * @author George A. David
   * @author Wei Chin, Chong
   */
  public boolean allPadTypesAtDefaultArtifactCompensationState()
  {
    boolean isStateAtDefaultLevel = true;

    for (PadType padType : _componentType.getPadTypes())
    {
      if ( padType.getPadTypeSettings().getEffectiveArtifactCompensationState().equals(ArtifactCompensationStateEnum.COMPENSATED))
      {
        isStateAtDefaultLevel = false;
        break;
      }
    }

    return isStateAtDefaultLevel;
  }

  /**
   * @author Cheah, Lee Herng
   */
  public boolean areAllPadTypesAtDefaultGlobalSurfaceModel()
  {
    boolean isStateAtDefaultLevel = true;

    for (PadType padType : _componentType.getPadTypes())
    {
      if ( padType.getPadTypeSettings().getGlobalSurfaceModel().equals(GlobalSurfaceModelEnum.AUTOFOCUS) == false)
      {
          isStateAtDefaultLevel = false;
          break;
      }
    }

    return isStateAtDefaultLevel;
  }

  /**
   * @return true if all PadTypes of this ComponentType are NOT inspected.
   * @author Bill Darbie
   */
  public boolean areAllPadTypesNotTestable()
  {
    boolean allNotTestable = true;
    for (PadType padType : _componentType.getPadTypes())
    {
      for (Pad pad : padType.getPads())
      {
        if (pad.getPadSettings().isTestable())
        {
          allNotTestable = false;
          break;
        }
      }
    }

    return allNotTestable;
  }

  /**
   * @author Bill Darbie
   */
  public void setLoaded(boolean loaded)
  {
    if ((_loaded != null) && (loaded == _loaded.getValue()))
      return;

    BooleanRef oldValue = null;
    if (_loaded != null)
      oldValue = new BooleanRef(_loaded);
    _projectObservable.setEnabled(false);
    try
    {
      if (_loaded == null)
        _loaded = new BooleanRef(loaded);
      else
        _loaded.setValue(loaded);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, new BooleanRef(loaded), ComponentTypeSettingsEventEnum.LOADED);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isLoaded()
  {
    Assert.expect(_loaded != null);

    return _loaded.getValue();
  }

  /**
   * @author Bill Darbie
   */
  public void invalidateUsesOneSubtype()
  {
    _usesOneSubtype = null;
  }

  /**
   * @return true if all the PadTypes for this ComponentType use only one CustomAlgorithmFamily
   * @author Bill Darbie
   */
  public boolean usesOneSubtype()
  {
    if (_usesOneSubtype != null)
      return _usesOneSubtype.getValue();

    Assert.expect(_componentType != null);
    boolean usesOneSubtype = true;
    Set<Subtype> subtypeSet = new HashSet<Subtype>();
    boolean first = true;
    for (PadType padType : _componentType.getPadTypes())
    {
      boolean added = subtypeSet.add(padType.getSubtype());
      if ((first == false) && (added))
      {
        usesOneSubtype = false;
        break;
      }

      first = false;
    }

    _usesOneSubtype = new BooleanRef(usesOneSubtype);
    return usesOneSubtype;
  }

  /**
   * @return the Subtype that all PadTypes share for this ComponentType.  Call
   * usesSubtype() first to be sure that only one Subtype is used.
   *
   * @author Bill Darbie
   */
  public Subtype getSubtype()
  {
    Assert.expect(_componentType != null);

    // make sure that there is really only one subtype
    Assert.expect(usesOneSubtype());

    List<PadType> padTypes = getComponentType().getPadTypes();
    Assert.expect(padTypes.size() > 0);
    PadType padType = (PadType)padTypes.get(0);

    return padType.getSubtype();
  }

  /**
   * @author Bill Darbie
   */
  public void setSubtype(Subtype subtype)
  {
    Assert.expect(subtype != null);

    Assert.expect(_componentType != null);

    _projectObservable.setEnabled(false);
    Subtype oldValue = null;
    if (usesOneSubtype())
      oldValue = getSubtype();
    try
    {
      for (PadType padType : _componentType.getPadTypes())
      {
        PadTypeSettings padTypeSettings = padType.getPadTypeSettings();
        padTypeSettings.setSubtype(subtype);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, subtype, ComponentTypeSettingsEventEnum.SUBTYPE);   
    _fileWriter.appendSubtype(_componentType,oldValue, subtype);

  }

   /**
   * @author Jack Hwee
   */
  public void setPsp(PspEnum psp)
  {
      Assert.expect(psp != null);

      if(_psp != null && _psp.equals(psp))
        return;

      PspEnum oldValue = _psp;
      _projectObservable.setEnabled(false);

       try
    {
      _psp = psp;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
      _projectObservable.stateChanged(this, oldValue, psp, ComponentTypeSettingsEventEnum.PSP_ENUM);
  }

  /**
   * @author Jack Hwee
   */
  public PspEnum getPsp()
  {
    Assert.expect(_psp != null);

    return _psp;
  }
  
  /**
   * @author sham
   */
  public boolean areAllPadTypesAtDefaultUserGain()
  {
    boolean isStateAtDefaultLevel = true;

    for (PadType padType : _componentType.getPadTypes())
    {
      if (padType.getPadTypeSettings().getUserGain().equals(UserGainEnum.getUserGainToEnumMapValue((int)Config.getInstance().getDoubleValue(HardwareConfigEnum.AXI_TDI_CAMERA_USER_GAIN))) == false)
      {
        isStateAtDefaultLevel = false;
        break;
      }
    }

    return isStateAtDefaultLevel;
  }
  
  /**
   * @author sheng chuan
   */
  public boolean areAllPadTypesAtDefaultStageSpeed()
  {
    boolean isStateAtDefaultLevel = true;

    for (PadType padType : _componentType.getPadTypes())
    {
      if (padType.getSubtype().getSubtypeAdvanceSettings().getStageSpeedList().size() == 1 && 
          padType.getSubtype().getSubtypeAdvanceSettings().getLowestStageSpeedSetting().equals(StageSpeedEnum.ONE))
      {
        isStateAtDefaultLevel = false;
        break;
      }
    }

    return isStateAtDefaultLevel;
  }

   /**
   * @author Jack Hwee
   */
  public boolean areAllPadTypesAtDefaultPsp()
  {
    boolean isStateAtDefaultLevel = true;

    for (PadType padType : _componentType.getPadTypes())
    {
    //   String pspModelStr = padType.getPadTypeSettings().getPsp().toString();
         String pspModelStr = getPsp().toString();
         
     // if ( padType.getPadTypeSettings().getPsp().equals(PspEnum.NO_PSP) == false)
        if ( getPsp().equals(PspEnum.NO_PSP) == false)
      {
          isStateAtDefaultLevel = false;
          break;
      }
    }

    return isStateAtDefaultLevel;
  }
}
