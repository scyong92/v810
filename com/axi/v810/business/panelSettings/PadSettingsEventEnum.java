package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Matt Wharton
 */
public class PadSettingsEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static final PadSettingsEventEnum CREATE_OR_DESTROY = new PadSettingsEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static final PadSettingsEventEnum PAD = new PadSettingsEventEnum(++_index);
  public static final PadSettingsEventEnum TESTABLE = new PadSettingsEventEnum(++_index);

  /**
   * @author Matt Wharton
   */
  private PadSettingsEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Matt Wharton
   */
  private PadSettingsEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
