package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;
/**
 *
 * @author wei-chin.chong
 */
public class MagnificationTypeEnum extends com.axi.util.Enum
{
  private static Map<String, MagnificationTypeEnum> _magnificationToEnumMap = new HashMap<String, MagnificationTypeEnum>();
  private static int _index = -1;
  private String _magnification;
  private static List<String> _magnificationNameList = new ArrayList<String>();
  public static MagnificationTypeEnum LOW = new MagnificationTypeEnum(++_index, "0_LOW");
  public static MagnificationTypeEnum HIGH = new MagnificationTypeEnum(++_index, "1_HIGH");

  
  /**
   * @author wei-chin.chong
   */
  private MagnificationTypeEnum(int id, String magnification)
  {
    super(id);
    _magnification = magnification;
    _magnificationToEnumMap.put(magnification, this);
    _magnificationNameList.add(_magnification.substring(2));
  }

  /**
   * @author wei-chin.chong
   */
  public String toString()
  {
    return String.valueOf(_magnification.substring(2));
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public String getName()
  {
    return _magnification;
  }

  /**
   * @author wei-chin.chong
   */
  public static MagnificationTypeEnum getMagnificationToEnumMapValue(String key)
  {
    Assert.expect(key != null);

    return _magnificationToEnumMap.get(key);
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public static List<String> getMagnificationTypeEnumList()
  {
    return _magnificationNameList;
  }

  /**
   * @author sham
   */
  public static Map<String, MagnificationTypeEnum> getMagnificationToEnumMap()
  {
    return _magnificationToEnumMap;
  }
  
  /**
   * @author Wei Chin
   */
  public static MagnificationTypeEnum getHigherMagnificationTypeEnum(MagnificationTypeEnum firstMagnificationType,
                                                                     MagnificationTypeEnum secondMagnificationType)
  {
    if(firstMagnificationType.getId() > secondMagnificationType.getId())
      return firstMagnificationType;
    else
      return secondMagnificationType;
  }
}
