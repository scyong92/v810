package com.axi.v810.business.panelSettings;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.algorithmLearning.*;
import java.util.*;

/**
 * This class provides access to settings that are specified to each pad instance (not type) on the panel.
 * A prime example of such a setting is short profile learning.
 *
 * @author Matt Wharton
 */
public class PadSettings implements Serializable
{
  private Pad _pad;
  private BooleanRef _testable; // BoardNdfReader, BoardTypeSettingsReader
  private String _reasonOfUntestable;
  private List<String> _reasonOfUntestableList = new ArrayList<String>();

  private transient AlgorithmShortsLearningReaderWriter _algorithmLearning;
  private transient AlgorithmExpectedImageLearningReaderWriter _expectedImageLearning;
  private transient AlgorithmBrokenPinLearningReaderWriter _algorithmBrokenPinLearning; //Broken Pin
  private static transient ProjectObservable _projectObservable;

  /**
   * @author Matt Wharton
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Matt Wharton
   */
  public PadSettings()
  {
    _projectObservable.stateChanged(this, null, this, PadSettingsEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.stateChanged(this, this, null, PadSettingsEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Matt Wharton
   */
  public void setPad(Pad pad)
  {
    Assert.expect(pad != null);

    if (pad == _pad)
      return;

    Pad oldValue = _pad;
    _projectObservable.setEnabled(false);
    try
    {
      _pad = pad;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, pad, PadSettingsEventEnum.PAD);
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasShortProfileLearning() throws DatastoreException
  {
    Assert.expect(_pad != null);

    getAlgorithmShortsLearningReference();
    return _algorithmLearning.doesShortLearningProfileExist(_pad);
  }


  /**
   * @return true if there is any short profile learning for the specified slice, false otherwise.
   *
   * @author Matt Wharton
   */
  public boolean hasShortProfileLearning(SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_pad != null);

    getAlgorithmShortsLearningReference();
    boolean hasShortProfileLearning = _algorithmLearning.doesShortLearningProfileExist(_pad, sliceNameEnum);

    return hasShortProfileLearning;
  }

  /**
   * @author Bill Darbie
   */
  private void getAlgorithmShortsLearningReference()
  {
    if (_algorithmLearning == null)
      _algorithmLearning = _pad.getComponent().getBoard().getPanel().getProject().getAlgorithmShortsLearningReaderWriter();
  }

  /**
   * Returns the learned short profile data for specified slice.  Assumes that learning exists; if it
   * doesn't, the underlying datastore code will assert!  Use the 'hasShortProfileLearning' method to
   * check.
   *
   * @author Matt Wharton
   */
  public ShortProfileLearning getShortProfileLearning(SliceNameEnum sliceNameEnum)
      throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_pad != null);

    getAlgorithmShortsLearningReference();
    ShortProfileLearning shortProfileLearning = _algorithmLearning.readShortProfileLearning(_pad, sliceNameEnum);
    Assert.expect(shortProfileLearning != null);

    return shortProfileLearning;
  }
  /*
   * @author Kee Chin Seong - In Order to speed up the learning process, pass in board will
   *                          faster 10x
  */
  public ShortProfileLearning getLearnedShortProfileLearning(Board board, SliceNameEnum sliceNameEnum)
      throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_pad != null);

    getAlgorithmShortsLearningReference();
    ShortProfileLearning shortProfileLearning = _algorithmLearning.getLearnedPadShortProfileLearning(board, _pad, sliceNameEnum);
    Assert.expect(shortProfileLearning != null);

    return shortProfileLearning;
  }

  /**
   * Sets the learned short profile data for the specified slice.
   *
   * @author Matt Wharton
   */
  public void setShortProfileLearning(ShortProfileLearning shortProfileLearning) throws DatastoreException
  {
    Assert.expect(shortProfileLearning != null);

    // NOTE: we do not need to do a _projectObservable.stateChanged() here because
    //       the event it getting tracked at a higher level

    getAlgorithmShortsLearningReference();
    _algorithmLearning.writeShortProfileLearning(shortProfileLearning);
  }

  /**
   * Deletes the learned short profile data for the associated Pad.
   *
   * @author Matt Wharton
   */
  public void deleteShortProfileLearning() throws DatastoreException
  {
    Assert.expect(_pad != null);

    // NOTE: we do not need to do a _projectObservable.stateChanged() here because
    //       the event it getting tracked at a higher level
    getAlgorithmShortsLearningReference();
    _algorithmLearning.deleteShortProfileLearning(_pad);
  }

  /**
   * @author George Booth
   */
  public boolean hasExpectedImageLearning() throws DatastoreException
  {
    Assert.expect(_pad != null);

    getAlgorithmExpectedImageLearningReference();
    return _expectedImageLearning.doesExpectedImageLearningExist(_pad);
  }

  /**
   * @return true if there is any expected image learning for the specified slice, false otherwise.
   *
   * @author George Booth
   */
  public boolean hasExpectedImageLearning(SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_pad != null);

    getAlgorithmExpectedImageLearningReference();
    boolean hasExpectedImageLearning = _expectedImageLearning.doesExpectedImageLearningExist(_pad, sliceNameEnum);

    return hasExpectedImageLearning;
  }

  /**
   * @author George Booth
   */
  private void getAlgorithmExpectedImageLearningReference()
  {
    if (_expectedImageLearning == null)
      _expectedImageLearning = _pad.getComponent().getBoard().getPanel().getProject().getAlgorithmExpectedImageLearningReaderWriter();
  }

  /**
   * @author George Booth
   */
  public void openExpectedImageLearning() throws DatastoreException
  {
    Assert.expect(_pad != null);

    getAlgorithmExpectedImageLearningReference();
    if (_expectedImageLearning.isOpen() == false)
      _expectedImageLearning.open();
  }

  /**
   * Returns the learned expected image data for specified slice.  Assumes that learning exists; if it
   * doesn't, the underlying datastore code will assert!  Use the 'hasExpectedImageLearning' method to
   * check.
   *
   * @author George Booth
   */
  public ExpectedImageLearning getExpectedImageLearning(SliceNameEnum sliceNameEnum)
      throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_pad != null);

    getAlgorithmExpectedImageLearningReference();
    ExpectedImageLearning ExpectedImageLearning = _expectedImageLearning.readExpectedImageLearning(_pad, sliceNameEnum);
    Assert.expect(ExpectedImageLearning != null);

    return ExpectedImageLearning;
  }

  /**
   * Sets the learned expected image data for the specified slice.
   *
   * @author George Booth
   */
  public void setExpectedImageLearning(ExpectedImageLearning ExpectedImageLearning) throws DatastoreException
  {
    Assert.expect(ExpectedImageLearning != null);

    // NOTE: we do not need to do a _projectObservable.stateChanged() here because
    //       the event it getting tracked at a higher level

    getAlgorithmExpectedImageLearningReference();
    _expectedImageLearning.writeExpectedImageLearning(ExpectedImageLearning);
  }

  /**
   * Deletes the learned expected image data for the associated Pad.
   *
   * @author George Booth
   */
  public void deleteExpectedImageLearning() throws DatastoreException
  {
    Assert.expect(_pad != null);

    // NOTE: we do not need to do a _projectObservable.stateChanged() here because
    //       the event it getting tracked at a higher level
    getAlgorithmExpectedImageLearningReference();
    _expectedImageLearning.deleteExpectedImageLearning(_pad);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void clearAlgorithmLearningReference()
  {
      if (_algorithmLearning != null)
          _algorithmLearning = null;

      if (_expectedImageLearning != null)
          _expectedImageLearning = null;

    //Broken Pin
    if (_algorithmBrokenPinLearning != null)
      _algorithmBrokenPinLearning = null;
  }
  
 /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor - Broken Pin
   */
  public boolean hasBrokenPinLearning() throws DatastoreException
  {
    Assert.expect(_pad != null);

    getAlgorithmBrokenPinLearningReference();
    return _algorithmBrokenPinLearning.doesBrokenPinLearningDataExist(_pad);
  }


  /**
   * @return true if there is any short profile learning for the specified slice, false otherwise.
   *
   * @author Matt Wharton
   * @author Lim, Lay Ngor - Broken Pin
   */
  public boolean hasBrokenPinLearning(SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_pad != null);

    getAlgorithmBrokenPinLearningReference();
    boolean hasBrokenPinLearning = _algorithmBrokenPinLearning.doesBrokenPinLearningDataExist(_pad, sliceNameEnum);

    return hasBrokenPinLearning;
  }

  /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor - Broken Pin
   */
  private void getAlgorithmBrokenPinLearningReference()
  {
    if (_algorithmBrokenPinLearning == null)
      _algorithmBrokenPinLearning = _pad.getComponent().getBoard().getPanel().getProject().getAlgorithmBrokenPinLearningReaderWriter();
  }

  /**
   * Returns the learned short profile data for specified slice.  Assumes that learning exists; if it
   * doesn't, the underlying datastore code will assert!  Use the 'hasShortProfileLearning' method to
   * check.
   *
   * @author Matt Wharton
   * @author Lim, Lay Ngor - Broken Pin
   */
  public BrokenPinLearning getBrokenPinLearning(SliceNameEnum sliceNameEnum)
      throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_pad != null);

    getAlgorithmBrokenPinLearningReference();
    BrokenPinLearning brokenPinLearning = _algorithmBrokenPinLearning.readBrokenPinLearning(_pad, sliceNameEnum);
    Assert.expect(brokenPinLearning != null);

    return brokenPinLearning;
  }

  /*
   * @author Kee Chin Seong - In Order to speed up the learning process, pass in board will
   *                          faster 10x
   * @author Lim, Lay Ngor - Broken Pin
  */
  public BrokenPinLearning getLearnedBrokenPinLearning(Board board, SliceNameEnum sliceNameEnum)
      throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_pad != null);

    getAlgorithmBrokenPinLearningReference();
    BrokenPinLearning brokenPinLearning = _algorithmBrokenPinLearning.getLearnedPadBrokenPinLearning(board, _pad, sliceNameEnum);
    Assert.expect(brokenPinLearning != null);

    return brokenPinLearning;
  }

  /**
   * Sets the learned short profile data for the specified slice.
   *
   * @author Matt Wharton
   * @author Lim, Lay Ngor - Broken Pin
   */
  public void setBrokenPinLearning(BrokenPinLearning brokenPinLearning) throws DatastoreException
  {
    Assert.expect(brokenPinLearning != null);

    // NOTE: we do not need to do a _projectObservable.stateChanged() here because
    //       the event it getting tracked at a higher level

    getAlgorithmBrokenPinLearningReference();
    _algorithmBrokenPinLearning.writeBrokenPinLearning(brokenPinLearning);
  }

  /**
   * Deletes the learned short profile data for the associated Pad.
   *
   * @author Matt Wharton
   * @author Lim, Lay Ngor - Broken Pin
   */
  public void deleteBrokenPinLearning() throws DatastoreException
  {
    Assert.expect(_pad != null);

    // NOTE: we do not need to do a _projectObservable.stateChanged() here because
    //       the event it getting tracked at a higher level
    getAlgorithmBrokenPinLearningReference();
    _algorithmBrokenPinLearning.deleteBrokenPinLearning(_pad);
  }
  
   /**
   * @author Jack Hwee
   */
  public void setTestable(boolean testable)
  {
    if ((_testable != null) && (testable == _testable.getValue()))
    {
      return;
    }

    BooleanRef oldValue = null;
    if (_testable != null)
    {
      oldValue = new BooleanRef(_testable);
    }
    _projectObservable.setEnabled(false);
    try
    {
      if (_testable == null)
      {
        _testable = new BooleanRef(testable);
      }
      else
      {
        _testable.setValue(testable);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, testable, PadSettingsEventEnum.TESTABLE);
  }
 
  /**
   * @author Jack Hwee
   */ 
    public void setUntestableReason(String reason)
  {
    _reasonOfUntestable = reason;
  }

  /**
   * @author Jack Hwee
   */
  public String getUntestableReason()
  {
    return _reasonOfUntestable;
  }

  /**
   * @author Jack Hwee
   */
  public void AddUntestableReasonToList(String reason)
  {
    if (_reasonOfUntestableList.contains(reason) == false)
    {
      _reasonOfUntestableList.add(reason);
    }
  }

  /**
   * @author Jack Hwee
   */
  public List<String> getUntestableReasonList()
  {
    return _reasonOfUntestableList;
  }

  /**
   * @author Jack Hwee
   */
  public boolean isTestable()
  {
    Assert.expect(_testable != null);
    return _testable.getValue();
  }
}
