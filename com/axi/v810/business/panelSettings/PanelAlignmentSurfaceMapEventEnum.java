package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Cheah Lee Herng
 */
public class PanelAlignmentSurfaceMapEventEnum extends ProjectChangeEventEnum
{
    private static int _index = -1;
    public static PanelAlignmentSurfaceMapEventEnum CREATE_OR_DESTROY = new PanelAlignmentSurfaceMapEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
    public static PanelAlignmentSurfaceMapEventEnum ADD_SURFACE_MAP_POINTS = new PanelAlignmentSurfaceMapEventEnum(++_index);
    public static PanelAlignmentSurfaceMapEventEnum REMOVE_SURFACE_MAP_POINTS = new PanelAlignmentSurfaceMapEventEnum(++_index);
    public static PanelAlignmentSurfaceMapEventEnum ADD_OPTICAL_REGION = new PanelAlignmentSurfaceMapEventEnum(++_index);
    public static PanelAlignmentSurfaceMapEventEnum REMOVE_OPTICAL_REGION = new PanelAlignmentSurfaceMapEventEnum(++_index);
    
    /**
     * @author CHeah Lee Herng
    */
    private PanelAlignmentSurfaceMapEventEnum(int id)
    {
        super(id);
    }

    /**
    * @author Bill Darbie
    */
    private PanelAlignmentSurfaceMapEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
    {
        super(id, projectChangeTypeEnum);
        Assert.expect(projectChangeTypeEnum != null);
    }
}
