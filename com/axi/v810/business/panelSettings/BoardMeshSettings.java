package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.psp.*;

/**
 * A BoardMeshSettings contains all the meshes contained
 * within a board.
 * 
 * @author Cheah Lee Herng
 */
public class BoardMeshSettings implements Serializable
{
  private Board _board;
  private List<MeshTriangle> _triangleMeshList;
  private static transient ProjectObservable _projectObservable;
  
  /**
   * @author Cheah Lee Herng
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }
   
  /**
   * @author Cheah Lee Herng
   */
  public BoardMeshSettings()
  {
    _projectObservable.stateChanged(this, null, this, BoardMeshEventEnum.CREATE_OR_DESTROY);
    _triangleMeshList = new ArrayList<MeshTriangle>();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public BoardMeshSettings(Board board)
  {
    Assert.expect(board != null);
    
    _board = board;    
    _projectObservable.stateChanged(this, null, this, BoardMeshEventEnum.CREATE_OR_DESTROY);
    _triangleMeshList = new ArrayList<MeshTriangle>();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void destroy()
  {
    _projectObservable.stateChanged(this, this, null, BoardMeshEventEnum.CREATE_OR_DESTROY);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void addMeshTriangle(MeshTriangle meshTriangle)
  {
    Assert.expect(meshTriangle != null);
    
    boolean isTriangleMeshAdd = false;
    _projectObservable.setEnabled(false);
    try
    {
      if (_triangleMeshList.contains(meshTriangle) == false)
      {
        _triangleMeshList.add(meshTriangle);
        isTriangleMeshAdd = true;
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    if (isTriangleMeshAdd)
      _projectObservable.stateChanged(this, null, meshTriangle, BoardMeshEventEnum.ADD_MESH_POINTS);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void removeMeshTriangle(MeshTriangle meshTriangle)
  {
    Assert.expect(meshTriangle != null);
    
    boolean isTriangleMeshRemove = false;
    _projectObservable.setEnabled(false);
    try
    {
      if (_triangleMeshList.contains(meshTriangle))
      {
        _triangleMeshList.remove(meshTriangle);
        isTriangleMeshRemove = true;
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    if (isTriangleMeshRemove)
      _projectObservable.stateChanged(this, null, meshTriangle, BoardMeshEventEnum.REMOVE_MESH_POINTS);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public List<MeshTriangle> getMeshTriangleList()
  {
    Assert.expect(_triangleMeshList != null);
    return _triangleMeshList;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {
    if (_triangleMeshList != null)
      _triangleMeshList.clear();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public Board getBoard()
  {
    Assert.expect(_board != null);
    return _board;
  }
}
