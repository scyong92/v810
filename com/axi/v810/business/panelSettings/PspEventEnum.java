package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Cheah Lee Herng
 */
public class PspEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;
  public static PspEventEnum CREATE_OR_DESTROY = new PspEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static PspEventEnum PANEL = new PspEventEnum(++_index);
  //Ying-Huan.Chu - temporary commented Larger FOV feature for 5.7 release.
  //public static PspEventEnum SET_USE_LARGER_FOV = new PspEventEnum(++_index);
  
  /**
    * @author CHeah Lee Herng
  */
  private PspEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Cheah Lee Herng
  */
  private PspEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
