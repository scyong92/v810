package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author George A. David
 */
public class Test_Project extends UnitTest
{
  private static final int _ZERO = 0;
  private static final int _NON_ZERO = 70;
  private static final int _NINETY = 90;
  private static final int _ONE_EIGHTY = 180;
  private static final int _TWO_SEVENTY = 270;
  private static final int _NEGATIVE_NUMBER = -1;
  private static final String _TEST = "Test";

  private static Project _project = null;

  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Project());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String file = FileName.getSoftwareConfigFullPath();
    String origFile = file + ".orig";
    try
    {
      if (FileUtil.exists(origFile))
        FileUtil.delete(origFile);
      FileUtil.copy(file, origFile);

      testBasicMethods();
      testImportLoadAndSave();
      testTransferringProjectOffline();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        if (FileUtil.exists(origFile))
          FileUtil.copy(origFile, file);
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   * @author George A. David
   */
  private void testTransferringProjectOffline()
  {
    System.loadLibrary("nativeAxiUtil");
    String offlinePath = Directory.getTempDir() + File.separator + "offline";
//    String offlinePath ="a:\\";
    try
    {
      //create the offline path
      if (FileUtil.exists(offlinePath) == false)
      {
        try
        {
          FileUtil.createDirectory(offlinePath);
        }
        catch (CouldNotCreateFileException ex)
        {
          ex.printStackTrace();
        }
      }

      // set the offline path
      try
      {
        Directory.setOfflineWorkstationPath(offlinePath);
      }
      catch (DatastoreException ex)
      {
        ex.printStackTrace();
      }


      // try null test
      try
      {
        Project.transferProjectOffline(null);
        Expect.expect(false);
      }
      catch(DatastoreException dex)
      {
        dex.printStackTrace();
      }
      catch(AssertException assertException)
      {
        //do nothing
      }

      // transfer a project that does not exist
      String projectName = "non-existant-project";
      try
      {
        Project.transferProjectOffline(projectName);
        Expect.expect(false);
      }
      catch(DatastoreException dex)
      {
        // do nothing
      }

      projectName = "nepcon_wide_ew";

      // transfer an existing project
      Expect.expect(Project.doesProjectExistOffline(projectName) == false);

      // import the project to create genesis project files.
      // we don't want to create project files just yet since they
      // are in flux still
      try
      {
        Project.importProjectFromNdfs(projectName, new LinkedList<LocalizedString>());
      }
      catch (DatastoreException ex1)
      {
        Assert.expect(false);
      }

      try
      {
        Project.transferProjectOffline(projectName);
      }
      catch(DatastoreException dex)
      {
        dex.printStackTrace();
      }
      Expect.expect(Project.doesProjectExistOffline(projectName) == true);

      // transfer it again
      try
      {
        Project.transferProjectOffline(projectName);
      }
      catch(DatastoreException dex)
      {
        dex.printStackTrace();
      }

      // transfer one last time
      try
      {
        Project.transferProjectOffline(projectName);
      }
      catch(DatastoreException dex)
      {
        dex.printStackTrace();
      }
    }
    finally
    {
      if (FileUtil.exists(offlinePath))
      {
        try
        {
          FileUtil.delete(offlinePath);
        }
        catch (CouldNotDeleteFileException ex)
        {
          ex.printStackTrace();
        }
      }
    }
  }

  /**
   * This tests setting and getting on the basic Panel methods.
   * It will also load a panel (twice) and do a quick check that it's populated correctly.
   * @author Andy Mechtenberg
   */
  private void testBasicMethods() throws Exception
  {
    _project = new Project(false);

    //test getPanel assert
    try
    {
      _project.getPanel();
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }

    // test setPanel assert
    try
    {
      _project.setPanel(null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    Panel panel = new Panel();
    _project.setPanel(panel);
    Expect.expect(panel == _project.getPanel());

    // test getTestProgramVersion
    int programVersion = 1;
    _project.setTestProgramVersion(programVersion);
    Expect.expect(_project.getVersion() == programVersion);

    // test getName
    String name = "programName";
    _project.setName(name);
    Expect.expect(name.equals(_project.getName()));

    String panelName = "nepcon_wide_ew";
    // remove the the .panel, .version, thrshlds.ndf & thrshlds.rtf files
    File file = null;
    file = new File(FileName.getProjectSerializedFullPath(panelName));//.panel
    if (file.exists())
      file.delete();
    file = new File(FileName.getProjectVersionFullName(panelName));//.version
    if (file.exists())
      file.delete();
    _project = null;

    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    _project = Project.importProjectFromNdfs(panelName, warnings);
    Expect.expect(_project != null);

    // quick check to see if the panel's data loaded correctly.
    // we'll check this again after loading the .panel file
    checkBasicPanelAttributesForNepconWideEW(panelName);

    _project = null;
    try
    {
      BooleanRef abortedDuringLoad = new BooleanRef();
      _project = Project.load(panelName, abortedDuringLoad);
      Assert.expect(abortedDuringLoad.getValue() == false);
    }
    catch(XrayTesterException e)
    {
      e.printStackTrace();
    }
    Expect.expect(_project != null);

    // add some checks here to make sure that two loads did not cause any
    // problems - the number of boards, components, etc should not be twice the expected
    // for instance
    checkBasicPanelAttributesForNepconWideEW(panelName);

  }

  /**
   * @author George A. David
   */
  private void deleteProjectFileIfExists(String projectName)
  {
    String[] strArray = null;
    Set<String> mySet = new TreeSet<String>();
    Collection<String> coll = Arrays.asList(strArray);
    mySet.addAll(coll);

    Arrays.sort(strArray);



    //delete the panel file if it exists, we first want to test loading ndf files.
    String panelFullPath = FileName.getProjectSerializedFullPath(projectName);

    if (FileUtil.exists(panelFullPath))
    {
      try
      {
        FileUtilAxi.delete(FileName.getProjectSerializedFullPath(projectName));
      }
      catch(DatastoreException dex)
      {
        dex.printStackTrace();
      }
    }
  }

  /**
   * @author George A. David
   */
  private void testImportLoadAndSave() throws Exception
  {
    String projectName = "nepcon_wide_ew";
    // import the project
    _project = importProject(projectName, new ArrayList<LocalizedString>());

    // force the generation of TestProgram
    _project.getTestProgram();

    double preSaveVersion = _project.getVersion();
    // save it
    _project.save();
    _project = null;
    // load it
    BooleanRef abortedDuringLoad = new BooleanRef();
    _project = Project.load(projectName, abortedDuringLoad);
    Assert.expect(abortedDuringLoad.getValue() == false);

    // do some checks
    String projectFullPath = FileName.getProjectSerializedFullPath(projectName);
    Expect.expect(FileUtil.exists(projectFullPath));
    Expect.expect(_project.getVersion() == preSaveVersion);

    // delete it
    _project.delete();
    Expect.expect(FileUtil.exists(projectFullPath) == false);
  }

  /**
   * @author George A. David
   */
  private Project importProject(String projectName, List<LocalizedString> warnings)
  {
    Project project = null;
    try
    {
      project = Project.importProjectFromNdfs(projectName, warnings);
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }

    Assert.expect(project != null);
    return project;
  }

  /**
   * @author George A. David
   */
  private void importTestProject1()
  {
    String panelName = "Test_project_1";
    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    _project = importProject(panelName, warnings);
    Expect.expect(_project != null);
    Expect.expect(warnings.size() == 0);

    // test getComponents
    List<Component> components = _project.getPanel().getComponents();
    Expect.expect(components.size() == 4);
    Expect.expect(doesComponentExist(_project, "D1") == true);
    Expect.expect(doesComponentExist(_project, "J1") == true);
    Expect.expect(doesComponentExist(_project, "U4") == true);
    Expect.expect(doesComponentExist(_project, "R5") == true);

    // test getNumJoints and getNumTestedJoints
    Expect.expect(_project.getPanel().getNumJoints() == 46);
    Expect.expect(_project.getPanel().getNumInspectedJoints() == 44);
  }

  /**
   * Test_project_2 is the exact same board as Test_project_1 ony the component
   * D1 is not falsed out.
   * @author George A. David
   */
  private void loadTestProject2()
  {
    Project project = null;
    String panelName = "Test_project_2";
    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    try
    {
      project = Project.importProjectFromNdfs(panelName, warnings);
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }
    Expect.expect(project != null);
    Expect.expect(warnings.size() == 0);

    // test getComponents
    List<Component> components = project.getPanel().getComponents();
    Expect.expect(components.size() == 4);
    Expect.expect(doesComponentExist(project, "D1") == true);
    Expect.expect(doesComponentExist(project, "J1") == true);
    Expect.expect(doesComponentExist(project, "U4") == true);
    Expect.expect(doesComponentExist(project, "R5") == true);

    // test getNumJoints and getNumTestedJoints
    Expect.expect(project.getPanel().getNumJoints() == 46);
    Expect.expect(project.getPanel().getNumInspectedJoints() == 46);
  }

  /**
   * @author George A. David
   */
  private boolean doesComponentExist(Project project, String componentName)
  {
    Assert.expect(project != null);
    Assert.expect(componentName != null);

    boolean componentExists = false;
    List<Component> components = project.getPanel().getComponents();
    for (Component component : components)
    {
      if (component.getReferenceDesignator().equalsIgnoreCase(componentName))
      {
        componentExists = true;
        break;
      }
    }
    return componentExists;
  }

  /**
   * For the panel nepcon_wide_ew, this will check that specific data is correctly loaded.
   *@author Andy Mechtenberg
   */
  private void checkBasicPanelAttributesForNepconWideEW(String panelName)
  {
    Expect.expect(_project.getName().equals(panelName));
    Panel panel = _project.getPanel();
    Expect.expect(panel.getBoards().size() == 1);
    Expect.expect(panel.getNumBoards() == 1);
    Expect.expect(panel.getComponents().size() == 61);
    Expect.expect(panel.getNumJoints() == 1269);
    Expect.expect(panel.getNumInspectedJoints() == 1269, "expected 1269, but got " + panel.getNumInspectedJoints());
    Expect.expect(panel.getWidthInNanoMeters() == 127406400);
    Expect.expect(panel.getLengthInNanoMeters() == 104140000);
    Expect.expect(panel.getDegreesRotationRelativeToCad() == 90);
  }

  /**
   * Compares a shape with the double data that it should contain
   * A rectangle, represented by a general path, takes the form of a SEG_MOVETO point, followed by
   * 4 SEG_LINETO points for a total of 2 + (4*2) = 10 points.
   * @author Andy Mechtenberg
   */
  private void compareRectangleShape(java.awt.Shape shape, double[] expectedCoords, boolean printOnly)
  {
    double[] coords = new double[6];
    java.awt.geom.AffineTransform identityTransform = new java.awt.geom.AffineTransform();
    java.awt.geom.PathIterator pathIterator = shape.getPathIterator(identityTransform);

    int type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_MOVETO)
    {
      if (printOnly)
        System.out.println(coords[0] + " " + coords[1]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[0]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[1]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_LINETO)
    {
      if (printOnly)
        System.out.println(coords[0] + " " + coords[1]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[2]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[3]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_LINETO)
    {
      if (printOnly)
        System.out.println(coords[0] + " " + coords[1]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[4]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[5]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_LINETO)
    {
      if (printOnly)
        System.out.println(coords[0] + " " + coords[1]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[6]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[7]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_LINETO)
    {
      if (printOnly)
        System.out.println(coords[0] + " " + coords[1]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[8]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[9]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_CLOSE) // we're done
    {
      pathIterator.next();
    }
    else
      Expect.expect(false);
  }

  /**
   * Compares a shape with the double data that it should contain.
   * A circular shape, represented as a general path takes the form of a MOVE_TO point, followed
   * by 4 SEG_CUBICTO points, for a total of 2 + (4*6) = 26 points.
   * @author Andy Mechtenberg
   */
  private void compareCircularShape(java.awt.Shape shape, double[] expectedCoords, boolean printOnly)
  {
    double[] coords = new double[6];
    java.awt.geom.AffineTransform identityTransform = new java.awt.geom.AffineTransform();
    java.awt.geom.PathIterator pathIterator = shape.getPathIterator(identityTransform);

    int type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_MOVETO)
    {
      if (printOnly)
        System.out.println(coords[0] + " " + coords[1]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[0]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[1]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_CUBICTO)  // CUBICTO holds 6 doubles
    {
      if (printOnly)
        System.out.println(coords[0] + " " + coords[1] + " " + coords[2] + " " + coords[3] + " " + coords[4] + " " + coords[5]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[2]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[3]));
        Expect.expect(MathUtil.fuzzyEquals(coords[2], expectedCoords[4]));
        Expect.expect(MathUtil.fuzzyEquals(coords[3], expectedCoords[5]));
        Expect.expect(MathUtil.fuzzyEquals(coords[4], expectedCoords[6]));
        Expect.expect(MathUtil.fuzzyEquals(coords[5], expectedCoords[7]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_CUBICTO)
    {
      if (printOnly)
        System.out.println(coords[0] + " " + coords[1] + " " + coords[2] + " " + coords[3] + " " + coords[4] + " " + coords[5]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[8]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[9]));
        Expect.expect(MathUtil.fuzzyEquals(coords[2], expectedCoords[10]));
        Expect.expect(MathUtil.fuzzyEquals(coords[3], expectedCoords[11]));
        Expect.expect(MathUtil.fuzzyEquals(coords[4], expectedCoords[12]));
        Expect.expect(MathUtil.fuzzyEquals(coords[5], expectedCoords[13]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_CUBICTO)
    {
      if (printOnly)
        System.out.println(coords[0] + " " + coords[1] + " " + coords[2] + " " + coords[3] + " " + coords[4] + " " + coords[5]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[14]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[15]));
        Expect.expect(MathUtil.fuzzyEquals(coords[2], expectedCoords[16]));
        Expect.expect(MathUtil.fuzzyEquals(coords[3], expectedCoords[17]));
        Expect.expect(MathUtil.fuzzyEquals(coords[4], expectedCoords[18]));
        Expect.expect(MathUtil.fuzzyEquals(coords[5], expectedCoords[19]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_CUBICTO)
    {
      if (printOnly)
        System.out.println(coords[0] + " " + coords[1] + " " + coords[2] + " " + coords[3] + " " + coords[4] + " " + coords[5]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[20]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[21]));
        Expect.expect(MathUtil.fuzzyEquals(coords[2], expectedCoords[22]));
        Expect.expect(MathUtil.fuzzyEquals(coords[3], expectedCoords[23]));
        Expect.expect(MathUtil.fuzzyEquals(coords[4], expectedCoords[24]));
        Expect.expect(MathUtil.fuzzyEquals(coords[5], expectedCoords[25]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_CLOSE) // we're done
    {
      pathIterator.next();
    }
    else
      Expect.expect(false);
  }
}
