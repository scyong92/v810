package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;

/**
 * @author George A. David
 * @author Erica Wheatcroft
 * @author Wei Chin, Chong
 */
public class ArtifactCompensationStateEnum extends com.axi.util.Enum
{
  private static Map<String, ArtifactCompensationStateEnum> _stringToEnumMap = new HashMap<String,ArtifactCompensationStateEnum>();
  private static List<ArtifactCompensationStateEnum> _orderedArtifactCompensationState = new ArrayList<>();
  private String _name;
  private static int _index = -1;

  public static ArtifactCompensationStateEnum COMPENSATED = new ArtifactCompensationStateEnum(++_index, "True");
  public static ArtifactCompensationStateEnum NOT_COMPENSATED = new ArtifactCompensationStateEnum(++_index, "False");
  public static ArtifactCompensationStateEnum NOT_APPLICABLE = new ArtifactCompensationStateEnum(++_index, "N/A");

  /**
   * @author George A. David
   */
  private ArtifactCompensationStateEnum(int id, String name)
  {
    super(id);
    _name = name;

    _stringToEnumMap.put(name, this);
    _orderedArtifactCompensationState.add(this);
  }

  /**
   * @author Erica Wheatcroft
   */
  public String toString()
  {
    return _name;
  }

  /**
   * @author Erica Wheatcroft
   */
  static Collection<ArtifactCompensationStateEnum> getAllTypes()
  {
    return _stringToEnumMap.values();
  }
  
   /**
   * @author Chong, Wei Chin
   */
  public static List<ArtifactCompensationStateEnum> getOrderedArtifactCompensationState()
  {
    return new ArrayList<ArtifactCompensationStateEnum>(_orderedArtifactCompensationState);
  }

  /**
   * @author Cheah Lee Herng
   */
  public static ArtifactCompensationStateEnum getArtifactCompensationStateEnum(String key)
  {
    Assert.expect(key != null);

    return _stringToEnumMap.get(key);
  }
}
