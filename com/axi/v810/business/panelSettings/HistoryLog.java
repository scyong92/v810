/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.business.panelSettings;

import com.axi.util.Assert;
import com.axi.util.FileUtil;
import com.axi.util.Pair;
import com.axi.util.PairComparator;
import com.axi.v810.datastore.DatastoreException;
import com.axi.v810.datastore.Directory;
import com.axi.v810.datastore.FileName;
import com.axi.v810.datastore.config.Config;
import com.axi.v810.datastore.config.SoftwareConfigEnum;
import com.axi.v810.util.FileUtilAxi;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Project's history log CR- Used to queue the history log file and delete
 * automatically
 *
 * @author hsia-fen.tan
 */
public class HistoryLog
{

  private static HistoryLog _instance = null;
  private static Config _config = Config.getInstance();
  private PriorityQueue<Pair<Long, String>> _localHistoryFilesDeletionQueue = null;
  private String _projectName;
  private boolean _deletionPolicyEnabled;
  private int _maximumNumberOfHistoryFilesToKeep;

  private HistoryLog()
  {
    _deletionPolicyEnabled = _config.getBooleanValue(SoftwareConfigEnum.AUTOMATICALLY_DELETE_HISTORY_FILES);
    _maximumNumberOfHistoryFilesToKeep = _config.getIntValue(SoftwareConfigEnum.KEEP_N_MOST_RECENT_HISTORY_FILE);

  }

  public synchronized static HistoryLog getInstance()
  {
    if (_instance == null)
    {
      _instance = new HistoryLog();
    }
    return _instance;
  }

  public void Delete() throws DatastoreException
  {
    setUsesAutomaticDeleteHistoryFileDeletion(true);

    if (getUsesAutomaticHistoryFileDeletion() == false)
    {
      return;
    }


    if (_localHistoryFilesDeletionQueue == null)
    {
      initializeDeletionQueues();
    }

    Assert.expect(_localHistoryFilesDeletionQueue != null);

    int numberOfHistoryToKeep = getMaximumNumberOfHistoryToKeep();

    while (_localHistoryFilesDeletionQueue.size() > numberOfHistoryToKeep)
    {
      synchronized (_localHistoryFilesDeletionQueue)
      {
        Pair<Long, String> resultTimeAndDirectory = _localHistoryFilesDeletionQueue.peek();
        String resultDirectoryToDelete = resultTimeAndDirectory.getSecond();

        FileUtilAxi.delete(resultDirectoryToDelete);
        _localHistoryFilesDeletionQueue.remove();
      }
    }
  }

  public boolean getUsesAutomaticHistoryFileDeletion()
  {
    return _deletionPolicyEnabled;
  }

  public int getMaximumNumberOfHistoryToKeep()
  {
    Assert.expect(_maximumNumberOfHistoryFilesToKeep >= 0);
    return _maximumNumberOfHistoryFilesToKeep;
  }

  public synchronized void setMaximumNumberOfHistoryFileToKeep(int newMax) throws DatastoreException
  {
    Assert.expect(newMax >= 0);
    _maximumNumberOfHistoryFilesToKeep = newMax;
    _config.setValue(SoftwareConfigEnum.KEEP_N_MOST_RECENT_HISTORY_FILE, newMax);
  }

  public synchronized void setUsesAutomaticDeleteHistoryFileDeletion(boolean flag) throws DatastoreException
  {
    _deletionPolicyEnabled = flag;
    _config.setValue(SoftwareConfigEnum.AUTOMATICALLY_DELETE_HISTORY_FILES, flag);
    if (_localHistoryFilesDeletionQueue != null)
    {
      _localHistoryFilesDeletionQueue.clear();
      _localHistoryFilesDeletionQueue = null;
    }
  }

  private void initializeDeletionQueues()
  {

    Assert.expect(_localHistoryFilesDeletionQueue == null);

    _localHistoryFilesDeletionQueue = new PriorityQueue<Pair<Long, String>>(20, new PairComparator<Long, String>());

    if (Project.isCurrentProjectLoaded() == true)
    {
      _projectName = Project.getCurrentlyLoadedProject().getName();
    }

    String resultsDirectory = Directory.getHistoryFileDir(_projectName);
    if (FileUtilAxi.exists(resultsDirectory) == false)
    {
      return;
    }

    List<String> HistoryLogDirectoryList = Directory.getProjectHistoryFileDirs(_projectName);
    for (String HistoryLogDirectory : HistoryLogDirectoryList)
    {
      long timeInMillis = Directory.getTimeStampforHistoryLog(_projectName, HistoryLogDirectory);
      _localHistoryFilesDeletionQueue.add(new Pair<Long, String>(timeInMillis, HistoryLogDirectory));
    }
  }
}
