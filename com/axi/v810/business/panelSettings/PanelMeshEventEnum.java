package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Cheah Lee Herng
 */
public class PanelMeshEventEnum extends ProjectChangeEventEnum 
{
  private static int _index = -1;
  public static PanelMeshEventEnum CREATE_OR_DESTROY = new PanelMeshEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static PanelMeshEventEnum ADD_MESH_POINTS = new PanelMeshEventEnum(++_index);
  public static PanelMeshEventEnum REMOVE_MESH_POINTS = new PanelMeshEventEnum(++_index);
  
  /**
   * @author CHeah Lee Herng
   */
  private PanelMeshEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private PanelMeshEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
