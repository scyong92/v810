package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;

/**
 * @author Lim, Seng Yew
 */
public class ScanPathMethodEnum extends com.axi.util.Enum
{
  private static Map<Integer, ScanPathMethodEnum> _scanPathMethodToEnumMap = new HashMap<Integer, ScanPathMethodEnum>();
  private static List<ScanPathMethodEnum> _orderedScanPathMethod = new ArrayList<ScanPathMethodEnum>();
  private static int _index = -1;
  // MUST NOT change the order of the enums below, as the value is very important.
  // The consequence will be all existing recipe will be affected.
  // Affected files:
  // 1) business/panelSettings/Project.java - getScanPathMethod() method - determine which version to use for newly created recipes.
  // 2) datastore/projReaders/ProjectSettingsReader.java - read(Project project) method - set the default enum for project.x.settings ver 5 or older.
  // 3) datastore/projWriters/ProjectSettingsWriter.java - write(Project project) method - get and write to file.
  // 4) business/imageAcquisition/HomogeneousImageGroup.java - this file determine all the scan path generation behaviour
  // METHOD_OPTIMIZED_LOADING_AND_STEP_SIZE : 
  // Optimized IL/IC grouping & fix max scan step size for each reconstruction region
  // METHOD_COMMON_STEP_FIX_AND_IC_NA_FIX : (this will include all previous fixes in ScanPathMethod 2 and below)
  // a) fixes for finding common step size in main scan path
  // b) fix a bug for ScanPathMethod 2 in 5.2, where components with IC N/A is not handled properly when set max step size value.
  public static ScanPathMethodEnum METHOD_ORIGINAL = new ScanPathMethodEnum(++_index);
  public static ScanPathMethodEnum METHOD_OPTIMIZED_LOADING = new ScanPathMethodEnum(++_index);
  public static ScanPathMethodEnum METHOD_OPTIMIZED_LOADING_AND_STEP_SIZE = new ScanPathMethodEnum(++_index);
  public static ScanPathMethodEnum METHOD_COMMON_STEP_FIX_AND_IC_NA_FIX = new ScanPathMethodEnum(++_index);
  public static ScanPathMethodEnum METHOD_COMMON_STEP_FIX_AND_IC_NA_MIN_MAX_FIX = new ScanPathMethodEnum(++_index);
  public static ScanPathMethodEnum METHOD_MERGE_WITHOUT_NOLOAD_COMPONENTS = new ScanPathMethodEnum(++_index);
  public static ScanPathMethodEnum METHOD_THROUGHOLE_PRESSFIT_SKIP_AREA_RATIO_CHECK = new ScanPathMethodEnum(++_index); //Siew Yeng - XCR-2760


  /**
   * @author Lim, Seng Yew
   */
  private ScanPathMethodEnum(int id)
  {
    super(id);

    _scanPathMethodToEnumMap.put(id, this);
    _orderedScanPathMethod.add(this);
  }

  /**
   * @author Lim, Seng Yew
   */
  public String toString()
  {
    return String.valueOf(this.getId());
  }

  /**
   * @author Lim, Seng Yew
   */
  public static ScanPathMethodEnum getScanPathMethodToEnumMapValue(int key)
  {
    if (key >= 0 && key <= _index)
      return _scanPathMethodToEnumMap.get(key);
    // Always return latest method if key not valid.
    return _scanPathMethodToEnumMap.get(_index);
  }

  /**
   * @author Lim, Seng Yew
   */
  public static List<ScanPathMethodEnum> getOrderedScanPathMethod()
  {
    return new ArrayList<ScanPathMethodEnum>(_orderedScanPathMethod);
  }
}
