package com.axi.v810.business.panelSettings;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * @author Andy Mechtenberg
 */
public class Test_PadTypeSettings extends UnitTest
{
  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_PadTypeSettings());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    PadTypeSettings padTypeSettings = new PadTypeSettings();
    PadType padType = new PadType();
    ComponentType componentType = new ComponentType();
    padType.setComponentType(componentType);
    ComponentTypeSettings componentTypeSettings = new ComponentTypeSettings();
    componentType.setComponentTypeSettings(componentTypeSettings);
    componentTypeSettings.setLoaded(true);
    padTypeSettings.setPadType(padType);

//wpd virgo
//    // setPad and getPad
//    try
//    {
//      padTypeSettings.getPad();
//      Expect.expect(false);
//    }
//    catch(AssertException e)
//    {
//      // do nothing
//    }
//    try
//    {
//      padTypeSettings.setPad(null);
//      Expect.expect(false);
//    }
//    catch(AssertException e)
//    {
//      // do nothing
//    }
//    Pad pad = new Pad();
//    padTypeSettings.setPad(pad);
//    Expect.expect(padTypeSettings.getPad() == pad);

    // tested boolean
    try
    {
      padTypeSettings.isInspected();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    boolean inspected = true;
    //padTypeSettings.setTestable(true);
    padTypeSettings.setInspected(inspected);
    Expect.expect(padTypeSettings.isInspected() == inspected);

//wpd virgo
//    // copy constructor
//    PadTypeSettings copy = new PadTypeSettings(padTypeSettings);
//    Expect.expect(copy.getPad() == padTypeSettings.getPad());
//    Expect.expect(copy.isInspected() == padTypeSettings.isInspected());
  }
}
