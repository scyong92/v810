package com.axi.v810.business.panelSettings;

import java.text.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.projReaders.*;

/**
 * @author Kok Chun, Tan
 */
public class VariationSettingManager
{
  private transient static ProjectObservable _projectObservable;
  public static final String ALL_LOADED = "All Loaded";
  public static final String NOT_AVAILABLE = "N/A";
  private static final String DEFAULT_VARIATION_NAME = "Variation_";
  private static VariationSettingManager _instance;
  
  /**
   * @author Kok Chun, Tan
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static synchronized VariationSettingManager getInstance()
  {
    if (_instance == null)
    {
      _instance = new VariationSettingManager();
    }

    return _instance;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static List <String> getProjectNameToVariationNamesMap(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);
    List <String> variationList = new ArrayList<String> ();
    
      // don't add projects with invalid names
      if (Project.isProjectNameValid(projectName))
      {
        variationList.clear();
        Map<String, Pair<String, String>> variationNameToEnabledMap = ProjectReader.getInstance().readProjectVariation(projectName);
        for (String name : variationNameToEnabledMap.keySet())
        {
          variationList.add(name);
        }
      }
    return variationList;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static Map<String, Pair<String, String>> getVariationNamesToEnabledMap(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);
    Map<String, Pair<String, String>> variationNameToEnabledMap = new LinkedHashMap<String, Pair<String, String>>();

    if (Project.isProjectNameValid(projectName))
    {
      variationNameToEnabledMap = ProjectReader.getInstance().readProjectVariation(projectName);
    }
    return variationNameToEnabledMap;
  }
  
  /**
   * @author Kok Chun, Tan
   * @editedBy Kee Chin Seong - Imported Project MUST BE FOLLOW BACK the settings previous
   *                          - XCR-3096 Even Variation is no load, no test always a Holly Cow
   */
  public void setupVariationSetting(boolean isEnable, Project project, Map<String, java.util.List<String>> currentBoardTypeToRefDesMap)
  {
    Assert.expect(project != null);
    Assert.expect(currentBoardTypeToRefDesMap != null);
    
    List<ComponentTypeSettings> componentTypeSettingsList = new ArrayList<ComponentTypeSettings> ();
    _projectObservable.setEnabled(false);
    try
    {
      if (isEnable)
      {
        for (BoardType boardType : project.getPanel().getBoardTypes())
        {
          List<String> refDesList = currentBoardTypeToRefDesMap.get(boardType.getName());
          // Kok Chun, Tan - XCR-3096 - when enableNoTestAsHighPriorityInVariation key is true, no test will have higher priority.
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.TEST_EXEC_NO_TEST_AS_HIGH_PRIORITY))
          {
            for (ComponentType componentType : boardType.getComponentTypes())
            {
              if (componentType.getComponentTypeSettings().isLoaded() && refDesList.contains(componentType.getReferenceDesignator())
                && componentType.getComponentTypeSettings().isInspected())
              {
                componentType.getComponentTypeSettings().setLoaded(false);
                if (componentTypeSettingsList.contains(componentType.getComponentTypeSettings()) == false)
                  componentTypeSettingsList.add(componentType.getComponentTypeSettings());
              }
              else
              {
                if (refDesList.contains(componentType.getReferenceDesignator()) == false)
                {
                  //if previous variant was not load now should make it inspected and loaded
                  if (componentType.getComponentTypeSettings().isLoaded() == false)
                  {
                    componentType.getComponentTypeSettings().setLoaded(true);
                    if (componentTypeSettingsList.contains(componentType.getComponentTypeSettings()) == false)
                      componentTypeSettingsList.add(componentType.getComponentTypeSettings());
                  }
                  else
                  {
                    for (Component comp : componentType.getComponents())
                    {
                      for (Pad pad : comp.getPads())
                      {
                        // handle x6000. This checking is correct. Only can set to test when the pad test. Never set no test to test.
                        if (pad.getPadTypeSettings().isInspected() == true)
                          pad.getPadTypeSettings().setInspected(true);
                      }
                    }
                  }
                }
              }
            }
          }
          else
          {
            for (ComponentType componentType : boardType.getComponentTypes())
            {
              if (componentType.getComponentTypeSettings().isLoaded() && refDesList.contains(componentType.getReferenceDesignator()))
              {
                componentType.getComponentTypeSettings().setLoaded(false);
                if (componentTypeSettingsList.contains(componentType.getComponentTypeSettings()) == false)
                  componentTypeSettingsList.add(componentType.getComponentTypeSettings());
              }
              else
              {
                if (refDesList.contains(componentType.getReferenceDesignator()) == false)
                {
                  //if previous variant was not load now should make it loaded
                  if (componentType.getComponentTypeSettings().isLoaded() == false)
                  {
                    componentType.getComponentTypeSettings().setLoaded(true);
                    if (componentTypeSettingsList.contains(componentType.getComponentTypeSettings()) == false)
                      componentTypeSettingsList.add(componentType.getComponentTypeSettings());
                  }
                }
              }
            }
          }
        }
      }
      else
      {
        for (BoardType boardType : project.getPanel().getBoardTypes())
        {
          for (ComponentType componentType : boardType.getComponentTypes())
          {
            if (componentType.getComponentTypeSettings().isLoaded() == false)
            {
              componentType.getComponentTypeSettings().setLoaded(true);
              if (componentTypeSettingsList.contains(componentType.getComponentTypeSettings()) == false)
              {
                componentTypeSettingsList.add(componentType.getComponentTypeSettings());
              }
            }
          }
        }
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(componentTypeSettingsList, null, null, ComponentTypeSettingsEventEnum.LOADED_LIST);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public List<String> getAllVariationNames()
  {
    List<String> variationNames = new ArrayList<String> ();
    Project project = Project.getCurrentlyLoadedProject();
    List<VariationSetting> variations = project.getPanel().getPanelSettings().getAllVariationSettings();
    
    for (VariationSetting variation : variations)
    {
      if (variationNames.contains(variation.getName()) == false)
      {
        variationNames.add(variation.getName());
      }
    }
    
    return variationNames;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public String getDefaultVariationName()
  {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
    Date date = new Date();
    String dateString = dateFormat.format(date);
    String name = DEFAULT_VARIATION_NAME + dateString;
    return name;
  }
}
