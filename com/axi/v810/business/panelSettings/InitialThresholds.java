package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class InitialThresholds
{
  private static InitialThresholds _instance = null;
  private InitialThresholdsReader _reader = InitialThresholdsReader.getInstance();
  private InitialThresholdsWriter _writer = InitialThresholdsWriter.getInstance();
  private List<String> _allSetNames = new ArrayList<String>();
  private MathUtilEnum _currentUnits = MathUtilEnum.MILS;
  private String _currentSetFullPath = null;
  private boolean _isDirty = false;

  private List<SingleInitialThreshold> _initialThresholds = new ArrayList<SingleInitialThreshold>();

  private ConfigObservable _configObservable = ConfigObservable.getInstance();

  /**
   * @author Andy Mechtenberg
   */
  private InitialThresholds()
  {
    _currentSetFullPath = getSystemDefaultSetName();
  }

  /**
   * @author Andy Mechtenberg
   */
  public static synchronized InitialThresholds getInstance()
  {
    if (_instance == null)
      _instance = new InitialThresholds();

    return _instance;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void save() throws DatastoreException
  {
    // cannot save to the SystemDefaults file name
    Assert.expect(_currentSetFullPath.equalsIgnoreCase(getSystemDefaultSetName()) == false);

    _writer.write(_currentSetFullPath, _currentUnits, _initialThresholds);
    _isDirty = false;
    // repopulate our list of available sets since we just saved one
    getAllSetNames();
  }

  /**
   * @author Andy Mechtenberg
   */
   public void load() throws DatastoreException
   {
     // cannot read the SystemDefaults file
     Assert.expect(_currentSetFullPath.equalsIgnoreCase(getSystemDefaultSetName()) == false);
     _initialThresholds.clear();

     // load from disk the _currentThresholdSetName and set _initialThresholds to it's contents
     _initialThresholds.addAll(_reader.read(_currentSetFullPath));
     _currentUnits = _reader.getUnits();

     _isDirty = false;
   }

  /**
   * @author Andy Mechtenberg
   */
   public void delete() throws DatastoreException
   {
     Assert.expect(_currentSetFullPath.equalsIgnoreCase(getSystemDefaultSetName()) == false);
     // add .config extension and find the correct path on disk before deleting
     FileUtilAxi.delete(_currentSetFullPath);

     // repopulate our list of available sets since we just deleted one
     getAllSetNames();
   }

  /**
   * @author Andy Mechtenberg
   */
  private String getSystemDefaultSetName()
  {
    return FileName.getInitialThresholdsDefaultFileWithoutExtension();
  }

  /**
   * @author Andy Mechtenberg
   */
   public boolean isDefaultSet(String setName)
   {
     if (setName.equalsIgnoreCase(getSystemDefaultSetName()))
       return true;
     else
       return false;
   }

  /**
   * @author Andy Mechtenberg
   */
   public List<String> getAllSetNames()
   {
     _allSetNames.clear();
     _allSetNames.addAll(FileName.getAllInitialThresholdSetNames());
     return _allSetNames;
   }

  /**
   * @author Andy Mechtenberg
   */
   public void loadNewThresholdSet(String newSetName) throws DatastoreException
   {
     Assert.expect(newSetName != null);
     getAllSetNames();
     Assert.expect(_allSetNames.contains(newSetName));
     _currentSetFullPath = FileName.getInitialThresholdsFullPath(newSetName);
     if (newSetName.equalsIgnoreCase(getSystemDefaultSetName()))
       _initialThresholds.clear();
     else
       load();
     _isDirty = false;
   }

   /**
    * @author Andy Mechtenberg
    */
    public boolean isThresholdSetNameExist(String newSetName) throws DatastoreException
    {
      Assert.expect(newSetName != null);
      getAllSetNames();
      return _allSetNames.contains(newSetName);
   }

   /**
   * @author Andy Mechtenberg
   */
   public void loadDefaultSet()
   {
     _currentSetFullPath = getSystemDefaultSetName();
     _initialThresholds.clear();
     _isDirty = false;
   }

  /**
   * @author Andy Mechtenberg
   */
   public void changeThresholdSetName(String newSetName)
   {
     Assert.expect(newSetName != null);
     _currentSetFullPath = FileName.getInitialThresholdsFullPath(newSetName);
     _isDirty = true;
   }

   /**
   * @author Andy Mechtenberg
   */
   public void createNewSet(String newSetName)
   {
     Assert.expect(newSetName != null);
     _initialThresholds.clear();
     _currentSetFullPath = FileName.getInitialThresholdsFullPath(newSetName);
     _isDirty = true;
   }

   /**
   * @author Andy Mechtenberg
   */
   public String getThresholdSetName()
   {
     return FileUtil.getNameWithoutExtension(FileUtil.getNameWithoutPath(_currentSetFullPath));
   }

  /**
   * @author Andy Mechtenberg
   */
   public void setCurrentUnits(MathUtilEnum units)
   {
     Assert.expect(units != null);
     _currentUnits = units;
     _isDirty = true;
     _configObservable.stateChanged(this);
   }

  /**
   * @author Andy Mechtenberg
   */
   public MathUtilEnum getCurrentUnits()
   {
     return _currentUnits;
   }

  /**
   * @author Andy Mechtenberg
   */
   public void setAlgorithmSettingValue(JointTypeEnum jointTypeEnum, Algorithm algorithm, AlgorithmSetting algorithmSetting, Serializable value)
   {
     SingleInitialThreshold single;
     if (doesAlgorithmSettingExist(jointTypeEnum, algorithm, algorithmSetting))
     {
       single = getSingleInitialThreshold(jointTypeEnum, algorithm, algorithmSetting);
       Serializable defaultValue = algorithmSetting.getDefaultValue(algorithm.getVersion());
       if (value.equals(defaultValue))
       {
         // if setting of of these to the default, we should REMOVE it from the set.
         _initialThresholds.remove(single);
       }
       else
         single.setValue(value);
     }
     else
     {
       single = new SingleInitialThreshold(jointTypeEnum, algorithm, algorithmSetting, value);
       _initialThresholds.add(single);
     }
     _isDirty = true;
     _configObservable.stateChanged(single);
   }

  /**
   * @author Andy Mechtenberg
   */
   public Serializable getAlgorithmSettingValue(JointTypeEnum jointTypeEnum, Algorithm algorithm, AlgorithmSetting algorithmSetting)
   {
     Serializable value = null;
     if (doesAlgorithmSettingExist(jointTypeEnum, algorithm, algorithmSetting))
     {
       SingleInitialThreshold single = getSingleInitialThreshold(jointTypeEnum, algorithm, algorithmSetting);
       value = single.getValue();
     }
     else
       value = algorithmSetting.getDefaultValue(algorithm.getVersion());
     return value;
   }

  /**
   * @author Andy Mechtenberg
   */
   private boolean doesAlgorithmSettingExist(JointTypeEnum jointTypeEnum, Algorithm algorithm, AlgorithmSetting algorithmSetting)
   {
     for(SingleInitialThreshold single : _initialThresholds)
     {
       if ((single.getJointTypeEnum().equals(jointTypeEnum)) &&
           (single.getAlgorithm().equals(algorithm)) &&
           (single.getAlgorithmSetting().equals(algorithmSetting)))
         return true;
     }
     return false;
   }

  /**
   * @author Andy Mechtenberg
   */
   private SingleInitialThreshold getSingleInitialThreshold(JointTypeEnum jointTypeEnum, Algorithm algorithm, AlgorithmSetting algorithmSetting)
   {
     for(SingleInitialThreshold single : _initialThresholds)
     {
       if ((single.getJointTypeEnum().equals(jointTypeEnum)) &&
           (single.getAlgorithm().equals(algorithm)) &&
           (single.getAlgorithmSetting().equals(algorithmSetting)))
         return single;
     }
     Assert.expect(false, "Unable to find: " + jointTypeEnum + " " + algorithm + " " + algorithmSetting);
     return null;
   }

   /**
   * @author Andy Mechtenberg
   */
   public List<SingleInitialThreshold> getAllSettingsInSortedOrder()
   {
     List<SingleInitialThreshold> sortedCopy = new ArrayList<SingleInitialThreshold>(_initialThresholds);
     Collections.sort(sortedCopy);  // uses the compareTo in SingleInitialThreshold itself
     return sortedCopy;
   }

  /**
   * @author Andy Mechtenberg
   */
   public boolean isDirty()
   {
     return _isDirty;
   }
}
