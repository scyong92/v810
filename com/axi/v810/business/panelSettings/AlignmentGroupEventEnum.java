package com.axi.v810.business.panelSettings;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class AlignmentGroupEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static final AlignmentGroupEventEnum CREATE_OR_DESTROY = new AlignmentGroupEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static final AlignmentGroupEventEnum NAME = new AlignmentGroupEventEnum(++_index);
  public static final AlignmentGroupEventEnum ADD_OR_REMOVE_PAD = new AlignmentGroupEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static final AlignmentGroupEventEnum ADD_OR_REMOVE_FIDUCIAL = new AlignmentGroupEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static final AlignmentGroupEventEnum MATCH_QUALITY = new AlignmentGroupEventEnum(++_index);
  public static final AlignmentGroupEventEnum FOREGROUND_GRAY_LEVEL = new AlignmentGroupEventEnum(++_index);
  public static final AlignmentGroupEventEnum BACKGROUND_GRAY_LEVEL = new AlignmentGroupEventEnum(++_index);
  public static final AlignmentGroupEventEnum USE_ZHEIGHT = new AlignmentGroupEventEnum(++_index);
  public static final AlignmentGroupEventEnum SIGNAL_COMPENSATION = new AlignmentGroupEventEnum(++_index);
  public static final AlignmentGroupEventEnum STAGE_SPEED = new AlignmentGroupEventEnum(++_index);
  public static final AlignmentGroupEventEnum DRO_LEVEL = new AlignmentGroupEventEnum(++_index);
  public static final AlignmentGroupEventEnum USER_GAIN = new AlignmentGroupEventEnum(++_index);
  public static final AlignmentGroupEventEnum USE_CUSTOMIZED_ALIGNMENT = new AlignmentGroupEventEnum(++_index);
  

  /**
   * @author Bill Darbie
   */
  private AlignmentGroupEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private AlignmentGroupEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
