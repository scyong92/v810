package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class PadTypeSettingsEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static PadTypeSettingsEventEnum CREATE_OR_DESTROY = new PadTypeSettingsEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static PadTypeSettingsEventEnum PAD_TYPE = new PadTypeSettingsEventEnum(++_index);
//  public static PadTypeSettingsEventEnum TESTABLE = new PadTypeSettingsEventEnum(++_index);
  public static PadTypeSettingsEventEnum INSPECTED = new PadTypeSettingsEventEnum(++_index);
  public static PadTypeSettingsEventEnum SUBTYPE = new PadTypeSettingsEventEnum(++_index);
//  public static PadTypeSettingsEventEnum SIGNAL_COMPENSATION_ENUM = new PadTypeSettingsEventEnum(++_index);
//  public static PadTypeSettingsEventEnum ARTIFACT_COMPENSATION_STATE_ENUM = new PadTypeSettingsEventEnum(++_index);
  public static PadTypeSettingsEventEnum GLOBAL_SURFACE_MODEL_STATE_ENUM = new PadTypeSettingsEventEnum(++_index);
//  public static PadTypeSettingsEventEnum USER_GAIN_ENUM = new PadTypeSettingsEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private PadTypeSettingsEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private PadTypeSettingsEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
