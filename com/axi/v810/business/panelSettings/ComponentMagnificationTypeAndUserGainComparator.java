package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class ComponentMagnificationTypeAndUserGainComparator implements Comparator<Component> 
{
  private boolean _magnificationTypeAscending;
  private boolean _userGainAscending;
  private static AlphaNumericComparator _alphaNumericComparator = new AlphaNumericComparator();
  
  /**
    * @author Cheah Lee Herng
    */
  public ComponentMagnificationTypeAndUserGainComparator(boolean magnificationTypeAscending, boolean userGainAscending)
  {        
    _magnificationTypeAscending = magnificationTypeAscending;
    _userGainAscending = userGainAscending;
  }
  
  /**
    * @author Cheah Lee Herng
    */
  public ComponentMagnificationTypeAndUserGainComparator()
  {
    _magnificationTypeAscending = true;
    _userGainAscending = true;
  }
  
  /**
    * @author Cheah Lee Herng
    */
  public int compare(Component lhs, Component rhs) 
  {
      Assert.expect( lhs != null );
      Assert.expect( rhs != null );

      if (lhs.equals(rhs))
        return 0;
      
      ComponentType lhsComponentType = lhs.getComponentType();
      ComponentType rhsComponentType = rhs.getComponentType();
      
      // Before do any comparison, we need to check if ComponentType is of mixed subtype
      List<Subtype> lhsSubtypes = lhsComponentType.getSubtypes();
      List<Subtype> rhsSubtypes = rhsComponentType.getSubtypes();

      if (_magnificationTypeAscending)
      {
        // Get the corresponding subtype, based on either ascending or descending
        Collections.sort(lhsSubtypes, new SubtypeAdvanceSettingsComparator(true, SubtypeCompratorEnum.MAGNIFICATION_TYPE));
        Collections.sort(rhsSubtypes, new SubtypeAdvanceSettingsComparator(true, SubtypeCompratorEnum.MAGNIFICATION_TYPE));
        
        Subtype lhsSubtype = lhsSubtypes.iterator().next();
        Subtype rhsSubtype = rhsSubtypes.iterator().next(); 
        
        int result = _alphaNumericComparator.compare(rhsSubtype.getSubtypeAdvanceSettings().getMagnificationType().toString(), lhsSubtype.getSubtypeAdvanceSettings().getMagnificationType().toString());
        if (result == 0)
        {
          if (_userGainAscending)
            result = _alphaNumericComparator.compare(lhsSubtype.getSubtypeAdvanceSettings().getUserGain().getId(), rhsSubtype.getSubtypeAdvanceSettings().getUserGain().getId());
          else
            result = _alphaNumericComparator.compare(rhsSubtype.getSubtypeAdvanceSettings().getUserGain().getId(), lhsSubtype.getSubtypeAdvanceSettings().getUserGain().getId());
        }
        return result;
      }
      else
      {
        // Get the corresponding subtype, based on either ascending or descending
        Collections.sort(lhsSubtypes, new SubtypeAdvanceSettingsComparator(false, SubtypeCompratorEnum.MAGNIFICATION_TYPE));
        Collections.sort(rhsSubtypes, new SubtypeAdvanceSettingsComparator(false, SubtypeCompratorEnum.MAGNIFICATION_TYPE));
        
        Subtype lhsSubtype = lhsSubtypes.iterator().next();
        Subtype rhsSubtype = rhsSubtypes.iterator().next();
        
        int result = _alphaNumericComparator.compare(lhsSubtype.getSubtypeAdvanceSettings().getMagnificationType().toString(), rhsSubtype.getSubtypeAdvanceSettings().getMagnificationType().toString());
        if (result == 0)
        {
          if (_userGainAscending)
            result = _alphaNumericComparator.compare(lhsSubtype.getSubtypeAdvanceSettings().getUserGain().getId(), rhsSubtype.getSubtypeAdvanceSettings().getUserGain().getId());
          else
            result = _alphaNumericComparator.compare(rhsSubtype.getSubtypeAdvanceSettings().getUserGain().getId(), lhsSubtype.getSubtypeAdvanceSettings().getUserGain().getId());
        }
        return result;
      }
  }
}
