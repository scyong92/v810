package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class BoardSettingsEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static BoardSettingsEventEnum CREATE_OR_DESTROY = new BoardSettingsEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static BoardSettingsEventEnum INSPECTED = new BoardSettingsEventEnum(++_index);
  public static BoardSettingsEventEnum POPULATED = new BoardSettingsEventEnum(++_index);
  public static BoardSettingsEventEnum ADD_ALIGNMENT_GROUP = new BoardSettingsEventEnum(++_index);
  public static BoardSettingsEventEnum MANUAL_ALIGNMENT_TRANSFORM_GENERATED = new BoardSettingsEventEnum(++_index);
  public static BoardSettingsEventEnum MANUAL_ALIGNMENT_TRANSFORM_DESTROYED = new BoardSettingsEventEnum(++_index);
  public static BoardSettingsEventEnum BOARD_ZOFFSET = new BoardSettingsEventEnum(++_index);
  public static BoardSettingsEventEnum SET_MANUAL_ALIGNMENT_MODE = new BoardSettingsEventEnum(++_index);
  
  /**
   * @author Bill Darbie
   */
  private BoardSettingsEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private BoardSettingsEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
