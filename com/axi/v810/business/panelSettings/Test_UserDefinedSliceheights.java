package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.testProgram.SliceNameEnum;
import com.axi.v810.business.panelDesc.JointTypeEnum;

class Test_UserDefinedSliceheights extends UnitTest
{

  /**
   * @author Peter Esbensen
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_UserDefinedSliceheights());
  }

  /**
   * @author Peter Esbensen
   */
  public void testGetUserDefinedSliceheightSettings()
  {
    // this test will need to be updated or rewritten if we ever change the sliceheights used by gullwing
    JointTypeEnum jointTypeEnum = JointTypeEnum.GULLWING;

    Set<AlgorithmSettingEnum> expectedUserDefinedSliceheightSettings = new HashSet<AlgorithmSettingEnum>();
    expectedUserDefinedSliceheightSettings.add(AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT);
    expectedUserDefinedSliceheightSettings.add(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_SLICEHEIGHT);
    expectedUserDefinedSliceheightSettings.add(AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT);

    // test the method, make sure it returns the right set of algo settings
    Collection<AlgorithmSettingEnum> gullwingUserDefinedSliceheightSettings = UserDefinedSliceheights.getUserDefinedSliceheightSettings(jointTypeEnum);

    Expect.expect( gullwingUserDefinedSliceheightSettings.equals(expectedUserDefinedSliceheightSettings) );

    // this part will need to be updated or rewritten if we ever change the sliceheights used by capacitor
    jointTypeEnum = JointTypeEnum.CAPACITOR;

    expectedUserDefinedSliceheightSettings = new HashSet<AlgorithmSettingEnum>();
    expectedUserDefinedSliceheightSettings.add(AlgorithmSettingEnum.USER_DEFINED_CLEAR_CHIP_SLICEHEIGHT);
    expectedUserDefinedSliceheightSettings.add(AlgorithmSettingEnum.USER_DEFINED_OPAQUE_CHIP_SLICEHEIGHT);
    expectedUserDefinedSliceheightSettings.add(AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT);

    // test the method, make sure it returns the right set of algo settings
    Collection<AlgorithmSettingEnum> capacitorUserDefinedSliceheightSettings = UserDefinedSliceheights.getUserDefinedSliceheightSettings(jointTypeEnum);

    Expect.expect( capacitorUserDefinedSliceheightSettings.equals(expectedUserDefinedSliceheightSettings) );

  }

  /**
   * @author Peter Esbensen
   */
  private void testAllInspectionFamilies()
  {
    // make sure every slice used by every inspection family works with the UserDefinedSliceheights class
    Collection<InspectionFamily> inspectionFamilies = InspectionFamily.getInspectionFamilies();
    for (InspectionFamily inspectionFamily : inspectionFamilies)
    {
      // create a dummy subtype
      Subtype subtype = new Subtype();
      // set up a couple things so that later calls to the subtype don't crash
      subtype.setInspectionFamilyEnum(inspectionFamily.getInspectionFamilyEnum());
      if (inspectionFamily.getInspectionFamilyEnum().equals(InspectionFamilyEnum.GRID_ARRAY))
        subtype.setJointTypeEnum(JointTypeEnum.COLLAPSABLE_BGA);
      if (inspectionFamily.getInspectionFamilyEnum().equals(InspectionFamilyEnum.GULLWING))
        subtype.setJointTypeEnum(JointTypeEnum.GULLWING);
      if (inspectionFamily.getInspectionFamilyEnum().equals(InspectionFamilyEnum.THROUGHHOLE))
        subtype.setJointTypeEnum(JointTypeEnum.THROUGH_HOLE);
      //Siew Yeng - XCR-3318 - Oval PTH
      if (inspectionFamily.getInspectionFamilyEnum().equals(InspectionFamilyEnum.OVAL_THROUGHHOLE))
        subtype.setJointTypeEnum(JointTypeEnum.OVAL_THROUGH_HOLE);

      // get a collection of slices used by this inspection family
      Collection<SliceNameEnum> sliceNameEnums = inspectionFamily.getOrderedInspectionSlices(subtype);

      for (SliceNameEnum sliceNameEnum : sliceNameEnums)
      {
        boolean inInternalMap = UserDefinedSliceheights.internalMapContainsForUnitTesting(sliceNameEnum);
        Expect.expect(inInternalMap, inspectionFamily.getName() + " slice " + sliceNameEnum.getName() + " not set up correctly with UserDefinedSliceheights.java");

        if (inInternalMap)
        {
          boolean foundAlgorithmSetting = false;
          Collection<Algorithm> algorithms = inspectionFamily.getAlgorithms();
          for (Algorithm algorithm : algorithms)
          {
            foundAlgorithmSetting |= algorithm.doesAlgorithmSettingExist(
                UserDefinedSliceheights.getAlgorithmSettingEnumForUnitTesting(sliceNameEnum));
          }
          Expect.expect(foundAlgorithmSetting, "could not find user defined sliceheight algorithm setting for " + inspectionFamily.getName() + " slice " + sliceNameEnum.getName());
        }
      }
    }
  }

  /**
   * @todo PE is it possible to adjust each setting and make sure a generated program changes appropriately?
   *
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testAllInspectionFamilies();

    testGetUserDefinedSliceheightSettings();
  }

}
