package com.axi.v810.business.panelSettings;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;

/**
 * BoardSettings are inspection settings that apply to both the top and and bottom side of
 * the board.  There is one BoardSetting object for each board object defined in panelDesc.
 * @author Andy Mechtenberg
 */
public class BoardSettings implements Serializable
{
  private Board _board;
  // created by PanelNdfReader, BoardSettingsReader

  private BooleanRef _inspected; // PanelNdfReader, BoardSettingsReader

  // during inspection time, has this board been detected as unpopulated, meaning we shouldn't test it
  private BooleanRef _populated; // not set

  private AffineTransform _rightManualAlignmentTransform; // right manual alignment transform
  private AffineTransform _leftManualAlignmentTransform; // left manual alignment transform
  private AffineTransform _rightRuntimeAlignmentTransform; // right runtime alignment transform
  private AffineTransform _leftRuntimeAlignmentTransform; // left runtime alignment transform

  private AffineTransform _rightManualAlignmentTransformForHighMag; // right manual alignment transform
  private AffineTransform _leftManualAlignmentTransformForHighMag; // left manual alignment transform
  private AffineTransform _rightRuntimeAlignmentTransformForHighMag; // right runtime alignment transform
  private AffineTransform _leftRuntimeAlignmentTransformForHighMag; // left runtime alignment transform  
  
  //Kee Chin Seong - Separating out the AFfine Transform from View CAD
  private AffineTransform _rightLastSavedViewCadRuntimeAlignmentTransform; // right runtime alignment transform
  private AffineTransform _leftLastSavedViewCadRuntimeAlignmentTransform; // left runtime alignment transform
  private AffineTransform _rightLastSavedViewCadRuntimeAlignmentTransformForHighMag; // right runtime alignment transform
  private AffineTransform _leftLastSavedViewCadRuntimeAlignmentTransformForHighMag; // left runtime alignment transform  
  
  private List<AlignmentGroup> _leftAlignmentGroups = new ArrayList<AlignmentGroup>();
  private List<AlignmentGroup> _rightAlignmentGroups = new ArrayList<AlignmentGroup>();

  private static transient ProjectObservable _projectObservable;
  
  //Siew Yeng - XCR1757 - use in production only
  private transient AffineTransform _rightRuntimeManualAlignmentTransform; // right runtime manual alignment transform
  private transient AffineTransform _leftRuntimeManualAlignmentTransform; // left runtime manual alignment transform
  private transient AffineTransform _rightRuntimeManualAlignmentTransformForHighMag; // right runtime manual alignment transform
  private transient AffineTransform _leftRuntimeManualAlignmentTransformForHighMag; // left runtime manual alignment transform
  
  private int _zOffset = 0;
  
  //Khaw Chek Hau - XCR2285: 2D Alignment on v810
  private boolean _isUsing2DAlignmentMethod = false;
  
  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public BoardSettings()
  {
    _projectObservable.stateChanged(this, null, this, BoardSettingsEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Wei Chin, Chong
   */
  public BoardSettings(Board board)
  {
    Assert.expect(board != null);
    _board = board;
    _projectObservable.stateChanged(this, null, this, BoardSettingsEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.stateChanged(this, this, null, BoardSettingsEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Keith Lee
   */
  public void setInspected(boolean inspected)
  {
    if ((_inspected != null) && (inspected == _inspected.getValue()))
      return;

    BooleanRef oldValue = null;
    if (_inspected != null)
      oldValue = new BooleanRef(_inspected);
    _projectObservable.setEnabled(false);
    try
    {
      if (_inspected == null)
        _inspected = new BooleanRef(inspected);
      else
        _inspected.setValue(inspected);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, inspected, BoardSettingsEventEnum.INSPECTED);
  }

  /**
   * @author Keith Lee
   */
  public boolean isInspected()
  {
    Assert.expect(_inspected != null);

    return _inspected.getValue();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setPopulated(boolean populated)
  {
    if ((_populated != null) && (populated == _populated.getValue()))
      return;

    BooleanRef oldValue = null;
    if (oldValue != null)
      oldValue = new BooleanRef(_populated);
    _projectObservable.setEnabled(false);
    try
    {
      if (_populated == null)
        _populated = new BooleanRef(populated);
      else
        _populated.setValue(populated);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, populated, BoardSettingsEventEnum.POPULATED);
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isPopulated()
  {
    Assert.expect(_populated != null);

    return _populated.getValue();
  }

  /**
   * @author Wei Chin, Chong
   */
  public void addAlignmentGroup(AlignmentGroup alignmentGroup)
  {
    Assert.expect(alignmentGroup != null);
    Assert.expect(_rightAlignmentGroups != null);

    _projectObservable.setEnabled(false);
    try
    {
      _rightAlignmentGroups.add(alignmentGroup);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, alignmentGroup, BoardSettingsEventEnum.ADD_ALIGNMENT_GROUP);
  }

  /**
   * @return a List of AlignmentGroup objects
   * @author Wei Chin, Chong
   */
  public List<AlignmentGroup> getAlignmentGroupsForShortPanel()
  {
    //Assert.expect(isLongBoard() == false);
    //Assert.expect(_rightAlignmentGroups != null);

    if(useLeftAlignmentGroup() == false)
      return _rightAlignmentGroups;
    else
      return _leftAlignmentGroups;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean hasAlignmentGroups()
  {
    if(useLeftAlignmentGroup() == false)
      return ((_rightAlignmentGroups != null) && (_rightAlignmentGroups.isEmpty() == false));
    else
      return ((_leftAlignmentGroups != null) && (_leftAlignmentGroups.isEmpty() == false));
  }

  /**
   * @author Wei Chin, Chong
   */
  public void assignEmptyRightAlignmentGroups()
  {
    AlignmentGroup alignmentGroup1 = new AlignmentGroup(this);
    AlignmentGroup alignmentGroup2 = new AlignmentGroup(this);
    AlignmentGroup alignmentGroup3 = new AlignmentGroup(this);
    
    if(_board != null)
    {
      alignmentGroup1.setName(_board.getName() + "_1");
      alignmentGroup2.setName(_board.getName() + "_2");
      alignmentGroup3.setName(_board.getName() + "_3");
    }
    else
    {
      alignmentGroup1.setName("1");
      alignmentGroup2.setName("2");
      alignmentGroup3.setName("3");
    }

    addAlignmentGroup(alignmentGroup1);
    addAlignmentGroup(alignmentGroup2);
    addAlignmentGroup(alignmentGroup3);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void assignEmptyLeftAlignmentGroups()
  {
    AlignmentGroup leftAlignmentGroup1 = new AlignmentGroup(this);
    AlignmentGroup leftAlignmentGroup2 = new AlignmentGroup(this);
    AlignmentGroup leftAlignmentGroup3 = new AlignmentGroup(this);

    if(_board != null)
    {
      leftAlignmentGroup1.setName(_board.getName() + "_4");
      leftAlignmentGroup2.setName(_board.getName() + "_5");
      leftAlignmentGroup3.setName(_board.getName() + "_6");
    }
    else
    {
      leftAlignmentGroup1.setName("4");
      leftAlignmentGroup2.setName("5");
      leftAlignmentGroup3.setName("6");
    }

    addLeftAlignmentGroup(leftAlignmentGroup1);
    addLeftAlignmentGroup(leftAlignmentGroup2);
    addLeftAlignmentGroup(leftAlignmentGroup3);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void assignAlignmentGroups()
  {
    // create empty right alignment groups
    assignEmptyRightAlignmentGroups();

    // create empty left alignment groups
    assignEmptyLeftAlignmentGroups();
  }

  /**
   * This is a wrapper for the addAlignmentGroup function.
   * It's added to make it easier to understand which side
   * of the panel you are adding an alignment group to. This
   * is only to be used when we have a long panel
   * @author Wei Chin, Chong
   */
  public void addRightAlignmentGroup(AlignmentGroup alignmentGroup)
  {
//    Assert.expect(isLongBoard());

    addAlignmentGroup(alignmentGroup);
  }

  /**
   * This is a wrapper for the getAlignmentGroups function.
   * It's added to make it easier to understand which side
   * of the panel you are getting the  alignment groups from. This
   * is only to be used when we have a long panel
   * @author Wei Chin, Chong
   */
  public List<AlignmentGroup> getRightAlignmentGroupsForLongPanel()
  {
//    Assert.expect(isLongBoard());
    Assert.expect(_rightAlignmentGroups != null);

    return _rightAlignmentGroups;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean hasRightAlignmentGroups()
  {
//    Assert.expect(isLongBoard());
    return ((_rightAlignmentGroups != null) && (_rightAlignmentGroups.isEmpty() == false));
//    return hasAlignmentGroups();
  }

  /**
   * Add an alignment group to the left side of the panel
   * in a panel.
   * @author Wei Chin, Chong
   */
  public void addLeftAlignmentGroup(AlignmentGroup alignmentGroup)
  {
    Assert.expect(alignmentGroup != null);
    Assert.expect(_leftAlignmentGroups != null);

    _projectObservable.setEnabled(false);
    try
    {
      _leftAlignmentGroups.add(alignmentGroup);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, alignmentGroup, BoardSettingsEventEnum.ADD_ALIGNMENT_GROUP);
  }

  /**
   * Gets the alignment groups of the left side of the panel
   * in a long panel. Use only with long panels.
   * @author Wei Chin, Chong
   */
  public List<AlignmentGroup> getLeftAlignmentGroupsForLongPanel()
  {
    Assert.expect(_leftAlignmentGroups != null);
//    Assert.expect(isLongBoard());

    return _leftAlignmentGroups;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean hasLeftAlignmentGroups()
  {
//    Assert.expect(isLongBoard());

    return ((_leftAlignmentGroups != null) && (_leftAlignmentGroups.isEmpty() == false));
  }

  /**
   * @author Wei Chin, Chong
   */
  public List<Pad> getAllAlignmentPads()
  {
    List<Pad> alignmentPads = new ArrayList<Pad>();
    for (AlignmentGroup alignmentGroup : _leftAlignmentGroups)
      alignmentPads.addAll(alignmentGroup.getPads());
    for (AlignmentGroup alignmentGroup : _rightAlignmentGroups)
      alignmentPads.addAll(alignmentGroup.getPads());

    return alignmentPads;
  }
  
  /**
   * @author Wei Chin, Chong
   */
  public boolean hasLeftAlignmentPads()
  {
    for (AlignmentGroup alignmentGroup : _leftAlignmentGroups)
    {
      if(alignmentGroup.isEmpty() == false)
        return true;
    }
    return false;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean hasRightAlignmentPads()
  {
    for (AlignmentGroup alignmentGroup : _rightAlignmentGroups)
    {
      if(alignmentGroup.isEmpty() == false)
        return true;
    }
    return false;
  }
  
  /**
   * @author Wei Chin, Chong
   */
  public boolean isLongBoard()
  {
    return getBoard().isLongBoard();
  }

  /**
   * @author Wei Chin, Chong
   * Not using this function anymore - Siew Yeng
   */
//  public boolean hasManualAlignmentBeenPerformed()
//  {
//    if(isLongBoard())
//    {
//      if ((_leftManualAlignmentTransform != null) &&
//          (_rightManualAlignmentTransform != null) &&
//          (getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid()))
//        return true;
//    }
//    else if( _board != null && useLeftAlignmentGroup() && _leftManualAlignmentTransform != null &&
//             getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid())
//      return true;
//    else if ( _rightManualAlignmentTransform != null &&
//              getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid())
//      return true;
//
//    return false;
//  }

  /**
   * Set the manual alignment transform for this long board when it is
   * positioned at the left panel-in-place sensor.
   *
   * @author Wei Chin, Chong
   */
  public void setLeftManualAlignmentTransform(AffineTransform leftManualAlignmentTransform)
  {
    Assert.expect(leftManualAlignmentTransform != null);
//    Assert.expect(isLongBoard());

    if (leftManualAlignmentTransform == _leftManualAlignmentTransform)
      return;

    Pair<AffineTransform, AffineTransform> oldValue =
      new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform);
    _projectObservable.setEnabled(false);
    try
    {
      _leftManualAlignmentTransform = leftManualAlignmentTransform;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform),
                                    BoardSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_GENERATED);
  }

  /**
   * Set the runtime alignment transform for this long panel when it is
   * positioned at the left panel-in-place sensor.
   * @author Wei Chin, Chong
   */
  public void setLeftRuntimeAlignmentTransform(AffineTransform leftRuntimeAlignmentTransform)
  {
    Assert.expect(leftRuntimeAlignmentTransform != null);
//    Assert.expect(isLongBoard());

    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _leftRuntimeAlignmentTransform = leftRuntimeAlignmentTransform;
  }
  
  /**
   * @author Kee Chin Seong - Separating out the AFfine Transform from View CAD
   * @param leftRuntimeAlignmentTransform 
   */
  public void setLeftLastSavedViewCadRuntimeAlignmentTransform(AffineTransform leftRuntimeAlignmentTransform)
  {
    Assert.expect(leftRuntimeAlignmentTransform != null);
    
    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _leftLastSavedViewCadRuntimeAlignmentTransform = leftRuntimeAlignmentTransform;
  }

  /**
   * Set the manual alignment transform for this long board when it is
   * positioned at the right panel-in-place sensor.
   *
   * @author Wei Chin, Chong
   */
  public void setRightManualAlignmentTransform(AffineTransform rightManualAlignmentTransform)
  {
    Assert.expect(rightManualAlignmentTransform != null);

    if (_rightManualAlignmentTransform == rightManualAlignmentTransform)
      return;

    Pair<AffineTransform, AffineTransform> oldValue =
        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform);
    _projectObservable.setEnabled(false);
    try
    {
      _rightManualAlignmentTransform = rightManualAlignmentTransform;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform),
                                    BoardSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_GENERATED);
  }

  /**
   * Set the runtime alignment transform for this long board when it is
   * positioned at the right panel-in-place sensor.
   *
   * @author Wei Chin, Chong
   */
  public void setRightRuntimeAlignmentTransform(AffineTransform rightRuntimeAlignmentTransform)
  {
    Assert.expect(rightRuntimeAlignmentTransform != null);

    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _rightRuntimeAlignmentTransform = rightRuntimeAlignmentTransform;
  }
  
  /**
   * @author Kee Chin Seong - Separating out the AFfine Transform from View CAD
   * @param rightRuntimeAlignmentTransform 
   */
  public void setRightLastSavedViewCadRuntimeAlignmentTransform(AffineTransform rightRuntimeAlignmentTransform)
  {
    Assert.expect(rightRuntimeAlignmentTransform != null);

    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _rightLastSavedViewCadRuntimeAlignmentTransform = rightRuntimeAlignmentTransform;
  }

  /**
   * Sets the manual and runtime left and right alignment transforms back to null and marks alignment as invalid.
   *
   * @author Wei Chin, Chong
   * @edited By Kee Chin Seong - Separating out the AFfine Transform from View CAD
   * @edited hee-jihn.chuah - XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
   */
  public void clearAllAlignmentTransforms()
  {
    getBoard().getPanel().getPanelSettings().setIsManualAlignmentAllCompleted(false);
    Pair<AffineTransform, AffineTransform> oldValue =
        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform);
    _projectObservable.setEnabled(false);
    try
    {
      _leftManualAlignmentTransform = null;
      _rightManualAlignmentTransform = null;
      _leftRuntimeAlignmentTransform = null;
      _rightRuntimeAlignmentTransform = null;
      
      //Clear all alignment transform need to include alignment transform for highmag
      _leftManualAlignmentTransformForHighMag = null;
      _rightManualAlignmentTransformForHighMag = null;
      _leftRuntimeAlignmentTransformForHighMag = null;
      _rightRuntimeAlignmentTransformForHighMag = null;
      
      //Kee Chin Seong - Separating out the AFfine Transform from View CAD
      _leftLastSavedViewCadRuntimeAlignmentTransform = null;
      _rightLastSavedViewCadRuntimeAlignmentTransform = null;
      _leftLastSavedViewCadRuntimeAlignmentTransformForHighMag = null;
      _rightLastSavedViewCadRuntimeAlignmentTransformForHighMag = null;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(null, null),
                                    BoardSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED);
  }
  
  /**
   * XCR2619: The initial alignment can't change state to "completed" when doing manual alignment on mixed mag recipe
   * @author Khaw Chek Hau 
   * @edited By Kee Chin Seong - Separating out the AFfine Transform from View CAD
   */
  public void clearRightAllAlignmentTransformsForHighMag()
  {
    Pair<AffineTransform, AffineTransform> oldValue =
        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform);
    _projectObservable.setEnabled(false);
    try
    {      
      //Clear all alignment transform need to include alignment transform for highmag
      _rightManualAlignmentTransformForHighMag = null;
      _rightRuntimeAlignmentTransformForHighMag = null;
      _rightLastSavedViewCadRuntimeAlignmentTransformForHighMag = null;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(null, null),
                                    BoardSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED);
  }
  
  /**
   * XCR2619: The initial alignment can't change state to "completed" when doing manual alignment on mixed mag recipe
   * @author Khaw Chek Hau 
   * @edited By Kee Chin Seong - Separating out the AFfine Transform from View CAD
   */
  public void clearLeftAllAlignmentTransformsForHighMag()
  {
    Pair<AffineTransform, AffineTransform> oldValue =
        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform);
    _projectObservable.setEnabled(false);
    try
    {      
      //Clear all alignment transform need to include alignment transform for highmag
      _leftManualAlignmentTransformForHighMag = null;
      _leftRuntimeAlignmentTransformForHighMag = null;
      _leftLastSavedViewCadRuntimeAlignmentTransformForHighMag = null;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(null, null),
                                    BoardSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED);
  }
  
  /**
   * XCR2619: The initial alignment can't change state to "completed" when doing manual alignment on mixed mag recipe
   * @author Khaw Chek Hau
   * @author Kee Chin Seong - Separating out the AFfine Transform from View CAD
   */
  public void clearRightAllAlignmentTransformsForLowMag()
  {
    Pair<AffineTransform, AffineTransform> oldValue =
        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform);
    _projectObservable.setEnabled(false);
    try
    {      
      //Clear all alignment transform need to include alignment transform for highmag
      _rightManualAlignmentTransform = null;
      _rightRuntimeAlignmentTransform = null;
      _rightLastSavedViewCadRuntimeAlignmentTransform = null;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(null, null),
                                    BoardSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED);
  }
  
  /**
   * XCR2619: The initial alignment can't change state to "completed" when doing manual alignment on mixed mag recipe
   * @author Khaw Chek Hau
   * @edited by Kee Chin Seong - Separating out the AFfine Transform from View CAD
   */
  public void clearLeftAllAlignmentTransformsForLowMag()
  {
    Pair<AffineTransform, AffineTransform> oldValue =
        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform);
    _projectObservable.setEnabled(false);
    try
    {      
      //Clear all alignment transform need to include alignment transform for highmag
      _leftManualAlignmentTransform = null;
      _leftRuntimeAlignmentTransform = null;
      _leftLastSavedViewCadRuntimeAlignmentTransform = null;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(null, null),
                                    BoardSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED);
  }

  /**
   * Get the manual alignment AffineTransform for this panel when
   * it is positioned against the left panel-in-place sensor.

   * @author Wei Chin, Chong
   */
  public AffineTransform getLeftManualAlignmentTransform()
  {
    Assert.expect(_leftManualAlignmentTransform != null);
//    Assert.expect(isLongBoard());
    Assert.expect(getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    return _leftManualAlignmentTransform;
  }

  /**
   * Get the runtime alignment AffineTransform for this panel when
   * it is positioned against the left panel-in-place sensor.

   * @author Wei Chin, Chong
   */
  public AffineTransform getLeftRuntimeAlignmentTransform()
  {
    Assert.expect(_leftManualAlignmentTransform != null);
//    Assert.expect(isLongBoard());
    Assert.expect(getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    if (_leftRuntimeAlignmentTransform == null)
    {
      _leftRuntimeAlignmentTransform = new AffineTransform(_leftManualAlignmentTransform);
    }

    return _leftRuntimeAlignmentTransform;
  }
  
  /**
   * @author Kee Chin Seong - Separating out the AFfine Transform from View CAD
   * @return 
   */
  public AffineTransform getLeftLastSavedViewCadRuntimeAlignmentTransform()
  {
    Assert.expect(_leftManualAlignmentTransform != null);
    Assert.expect(getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    if (_leftLastSavedViewCadRuntimeAlignmentTransform == null)
    {
      _leftLastSavedViewCadRuntimeAlignmentTransform = new AffineTransform(_leftManualAlignmentTransform);
    }

    return _leftLastSavedViewCadRuntimeAlignmentTransform;
  }

  /**
   * Returns true if the left manual alignment transform exists.
   *
   * @author Wei Chin, Chong
   */
  public boolean leftManualAlignmentTransformExists()
  {
    if ((_leftManualAlignmentTransform == null) ||
        getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid() == false)
      return false;

    return true;
  }

  /**
   * Returns true if the left runtime alignment transform exists.
   *
   * @author Wei Chin, Chong
   */
  public boolean leftRuntimeAlignmentTransformExists()
  {
    // If we have at least a manual transform, we are guaranteed to have something we can use as a runtime
    // transform.
    return leftManualAlignmentTransformExists();
  }

  /**
   * Get the manual alignment AffineTransform for this board when
   * it is positioned against the right panel-in-place sensor.
   *
   * @author Wei Chin, Chong
   */
  public AffineTransform getRightManualAlignmentTransform()
  {
    Assert.expect(_rightManualAlignmentTransform != null);
    Assert.expect(getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    return _rightManualAlignmentTransform;
  }

  /**
   * Get the runtime alignment AffineTransform for this board when
   * it is positioned against the right panel-in-place sensor.
   *
   * @author Wei Chin, Chong
   */
  public AffineTransform getRightRuntimeAlignmentTransform()
  {
    Assert.expect(_rightManualAlignmentTransform != null);
    Assert.expect(getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    if (_rightRuntimeAlignmentTransform == null)
    {
      _rightRuntimeAlignmentTransform = new AffineTransform(_rightManualAlignmentTransform);
    }

    return _rightRuntimeAlignmentTransform;
  }
  
  /**
   * @author Kee Chin Seong - Separating out the AFfine Transform from View CAD
   * @return 
   */
  public AffineTransform getRightLastSavedViewCadRuntimeAlignmentTransform()
  {
    Assert.expect(_rightManualAlignmentTransform != null);
    Assert.expect(getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    if (_rightLastSavedViewCadRuntimeAlignmentTransform == null)
    {
      _rightLastSavedViewCadRuntimeAlignmentTransform = new AffineTransform(_rightManualAlignmentTransform);
    }

    return _rightLastSavedViewCadRuntimeAlignmentTransform;
  }

  /**
   * @author Wei Chin, Chong
   * @author Phang Siew Yeng
   * This function is ONLY called by VerifyCadPanel to check if ALIGNMENT TRANSFORM is VALID.
   * For long panel/long board, alignment transform VALID - both left and right alignment transforms exist.
   * For short panel, alignment transform VALID - right/left alignment transform exists.
   * NOTE: Alignment VALID != Alignment COMPLETE
   */
  public boolean areManualAlignmentTransformsValid()
  {
    boolean valid = true;
    if (isLongBoard() || useLeftAlignmentGroup())
    {
      if (leftManualAlignmentTransformExists() == false ||
        _board.getPanel().getProject().getTestProgram().hasTestSubProgram(_board,PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.LOW) == false)
        valid = false;
    }

    if(isLongBoard() || useLeftAlignmentGroup() == false)
    {
      if (rightManualAlignmentTransformExists() == false ||
        _board.getPanel().getProject().getTestProgram().hasTestSubProgram(_board,PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.LOW) == false)
        valid = false;
    }

    return valid;
  }

  /**
   * @return true if the right manual alignment transform exists.
   *
   * @author Wei Chin, Chong
   */
  public boolean rightManualAlignmentTransformExists()
  {
    if ((_rightManualAlignmentTransform == null) ||
         getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid() == false)
      return false;

    return true;
  }

  /**
   * Returns true if the left runtime alignment transform exists.
   *
   * @author Wei Chin, Chong
   */
  public boolean rightRuntimeAlignmentTransformExists()
  {
    // If we have at least a manual transform, we are guaranteed to have something we can use as a runtime
    // transform.
    return rightManualAlignmentTransformExists();
  }

/**
   * Assert if two alignment groups have the same name.
   * @author Wei Chin, Chong
   */
  void checkForDuplicateAlignmentGroupNames()
  {
    Set<String> nameSet = new HashSet<String>();
    for (AlignmentGroup group : _leftAlignmentGroups)
    {
      if (group.hasName())
      {
        boolean added = nameSet.add(group.getName());
        Assert.expect(added);
      }
    }

    for (AlignmentGroup group : _rightAlignmentGroups)
    {
      if (group.hasName())
      {
        boolean added = nameSet.add(group.getName());
        Assert.expect(added);
      }
    }
  }

  /**
   * @author Wei Chin
   */
  public List<AlignmentGroup> getAllAlignmentGroups()
  {
    List<AlignmentGroup> alignmentGroups = new ArrayList<AlignmentGroup>();
    if (isLongBoard() == false)
    {
      alignmentGroups.addAll(getAlignmentGroupsForShortPanel());
    }
    else
    {
      alignmentGroups.addAll(getRightAlignmentGroupsForLongPanel());
      alignmentGroups.addAll(getLeftAlignmentGroupsForLongPanel());      
    }

    return alignmentGroups;
  }

  /**
   * @author Wei Chin
   */
  public List<Fiducial> getAllAlignmentFiducials()
  {
    List<Fiducial> fiducials = new ArrayList<Fiducial>();
    for (AlignmentGroup alignmentGroup : _leftAlignmentGroups)
      fiducials.addAll(alignmentGroup.getFiducials());
    for (AlignmentGroup alignmentGroup : _rightAlignmentGroups)
      fiducials.addAll(alignmentGroup.getFiducials());

    return fiducials;
  }

  /**
   * @return the _board
   * @author Wei Chin, Chong
   */
  public Board getBoard()
  {
    Assert.expect(_board != null);
    
    return _board;
  }

  /**
   * @author Wei Chin
   */
  public void removePadFromAlignmentGroupIfItIsUsed(Pad padToRemove)
  {
    Assert.expect(padToRemove != null);

    for (AlignmentGroup alignmentGroup : _rightAlignmentGroups)
    {
      for (Pad pad : alignmentGroup.getPads())
      {
        if (padToRemove == pad)
        {
          alignmentGroup.removePad(pad);
          break;
        }
      }
    }
    if(isLongBoard() || useLeftAlignmentGroup())
    {
      for (AlignmentGroup alignmentGroup : _leftAlignmentGroups)
      {
        for (Pad pad : alignmentGroup.getPads())
        {
          if (padToRemove == pad)
          {
            alignmentGroup.removePad(pad);
            break;
          }
        }
      }
    }
  }

  /**
   * @return
   * @author Wei Chin
   * Long panel with panel-based alignment will use left alignment group
   * Long panel with board-based alignment will use left alignment group if is long board.
   */
  public boolean useLeftAlignmentGroup()
  {
    Assert.expect(_board != null);

    if(_board.getPanel().getPanelSettings().isLongPanel() && 
      _board.getRightSectionOfLongBoard().getMaxY() <= XrayTester.getMaxImageableAreaLengthInNanoMeters())
      return true;
    else
      return false;
  }
  
  /**
   * @author Wei Chin, Chong
   * Not using this function anymore - Siew Yeng
   */
//  public boolean hasManualAlignmentBeenPerformedForHighMag()
//  {
//    if(isLongBoard())
//    {
//      if ((_leftManualAlignmentTransformForHighMag != null) &&
//          (_rightManualAlignmentTransformForHighMag != null) &&
//          (getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid()))
//        return true;
//    }
//    else if( _board != null && useLeftAlignmentGroup() && _leftManualAlignmentTransformForHighMag != null &&
//             getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid())
//      return true;
//    else if ( _rightManualAlignmentTransformForHighMag != null &&
//              getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid())
//      return true;
//
//    return false;
//  }

  /**
   * Set the manual alignment transform for this long board when it is
   * positioned at the left panel-in-place sensor.
   *
   * @author Wei Chin, Chong
   */
  public void setLeftManualAlignmentTransformForHighMag(AffineTransform leftManualAlignmentTransform)
  {
    Assert.expect(leftManualAlignmentTransform != null);
//    Assert.expect(isLongBoard());

    if (leftManualAlignmentTransform == _leftManualAlignmentTransformForHighMag)
      return;

    Pair<AffineTransform, AffineTransform> oldValue =
      new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransformForHighMag, _rightManualAlignmentTransformForHighMag);
    _projectObservable.setEnabled(false);
    try
    {
      _leftManualAlignmentTransformForHighMag = leftManualAlignmentTransform;
      setLeftLastSavedViewCadRuntimeAlignmentTransformForHighMag(leftManualAlignmentTransform);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransformForHighMag, _rightManualAlignmentTransformForHighMag),
                                    BoardSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_GENERATED);
  }

  /**
   * Set the runtime alignment transform for this long panel when it is
   * positioned at the left panel-in-place sensor.
   * @author Wei Chin, Chong
   */
  public void setLeftRuntimeAlignmentTransformForHighMag(AffineTransform leftRuntimeAlignmentTransform)
  {
    Assert.expect(leftRuntimeAlignmentTransform != null);
//    Assert.expect(isLongBoard());

    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _leftRuntimeAlignmentTransformForHighMag = leftRuntimeAlignmentTransform;
  }
  
  /**
   * @author Kee Chin Seong - Separating out the AFfine Transform from View CAD
   * @param leftRuntimeAlignmentTransform 
   */
  public void setLeftLastSavedViewCadRuntimeAlignmentTransformForHighMag(AffineTransform leftRuntimeAlignmentTransform)
  {
    Assert.expect(leftRuntimeAlignmentTransform != null);
    
    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _leftLastSavedViewCadRuntimeAlignmentTransformForHighMag = leftRuntimeAlignmentTransform;
  }

  /**
   * Set the manual alignment transform for this long board when it is
   * positioned at the right panel-in-place sensor.
   *
   * @author Wei Chin, Chong
   */
  public void setRightManualAlignmentTransformForHighMag(AffineTransform rightManualAlignmentTransform)
  {
    Assert.expect(rightManualAlignmentTransform != null);

    if (_rightManualAlignmentTransformForHighMag == rightManualAlignmentTransform)
      return;

    Pair<AffineTransform, AffineTransform> oldValue =
        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransformForHighMag, _rightManualAlignmentTransformForHighMag);
    _projectObservable.setEnabled(false);
    try
    {
      _rightManualAlignmentTransformForHighMag = rightManualAlignmentTransform;
      setRightLastSavedViewCadRuntimeAlignmentTransformForHighMag(rightManualAlignmentTransform);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransformForHighMag, _rightManualAlignmentTransformForHighMag),
                                    BoardSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_GENERATED);
  }

  /**
   * Set the runtime alignment transform for this long board when it is
   * positioned at the right panel-in-place sensor.
   *
   * @author Wei Chin, Chong
   */
  public void setRightRuntimeAlignmentTransformForHighMag(AffineTransform rightRuntimeAlignmentTransform)
  {
    Assert.expect(rightRuntimeAlignmentTransform != null);

    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _rightRuntimeAlignmentTransformForHighMag = rightRuntimeAlignmentTransform;
  }
  
  /**
   * @author Kee Chin Seong - Separating out the AFfine Transform from View CAD
   * @param rightRuntimeAlignmentTransform 
   */
  public void setRightLastSavedViewCadRuntimeAlignmentTransformForHighMag(AffineTransform rightRuntimeAlignmentTransform)
  {
    Assert.expect(rightRuntimeAlignmentTransform != null);

    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _rightLastSavedViewCadRuntimeAlignmentTransformForHighMag = rightRuntimeAlignmentTransform;
  }

  /**
   * Sets the manual and runtime left and right alignment transforms back to null and marks alignment as invalid.
   * To clear all alignment transform, use clearAllAlignmentTransforms() only - Commented by Siew Yeng.
   * @author Wei Chin, Chong
   */
//  public void clearAllAlignmentTransformsForHighMag()
//  {
//    Pair<AffineTransform, AffineTransform> oldValue =
//        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransformForHighMag, _rightManualAlignmentTransformForHighMag);
//    _projectObservable.setEnabled(false);
//    try
//    {
//      _leftManualAlignmentTransformForHighMag = null;
//      _rightManualAlignmentTransformForHighMag = null;
//      _leftRuntimeAlignmentTransformForHighMag = null;
//      _rightRuntimeAlignmentTransformForHighMag = null;
//    }
//    finally
//    {
//      _projectObservable.setEnabled(true);
//    }
//    _projectObservable.stateChanged(this,
//                                    oldValue,
//                                    new Pair<AffineTransform, AffineTransform>(null, null),
//                                    BoardSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED);
//  }

  /**
   * Get the manual alignment AffineTransform for this panel when
   * it is positioned against the left panel-in-place sensor.

   * @author Wei Chin, Chong
   */
  public AffineTransform getLeftManualAlignmentTransformForHighMag()
  {
    Assert.expect(_leftManualAlignmentTransformForHighMag != null);
//    Assert.expect(isLongBoard());
    Assert.expect(getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    return _leftManualAlignmentTransformForHighMag;
  }

  /**
   * Get the runtime alignment AffineTransform for this panel when
   * it is positioned against the left panel-in-place sensor.

   * @author Wei Chin, Chong
   */
  public AffineTransform getLeftRuntimeAlignmentTransformForHighMag()
  {
    Assert.expect(_leftManualAlignmentTransformForHighMag != null);
//    Assert.expect(isLongBoard());
    Assert.expect(getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    if (_leftRuntimeAlignmentTransformForHighMag == null)
    {
      _leftRuntimeAlignmentTransformForHighMag = new AffineTransform(_leftManualAlignmentTransformForHighMag);
    }

    return _leftRuntimeAlignmentTransformForHighMag;
  }
  
  /**
   * @author Kee Chin Seong - Separating out the AFfine Transform from View CAD
   */
  public AffineTransform getLeftLastSavedViewCadRuntimeAlignmentTransformForHighMag()
  {
    Assert.expect(_leftManualAlignmentTransformForHighMag != null);
    Assert.expect(getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    if (_leftLastSavedViewCadRuntimeAlignmentTransformForHighMag == null)
    {
      _leftLastSavedViewCadRuntimeAlignmentTransformForHighMag = new AffineTransform(_leftManualAlignmentTransformForHighMag);
    }

    return _leftLastSavedViewCadRuntimeAlignmentTransformForHighMag;
  }

  /**
   * Returns true if the left manual alignment transform exists.
   *
   * @author Wei Chin, Chong
   */
  public boolean leftManualAlignmentTransformForHighMagExists()
  {
    if ((_leftManualAlignmentTransformForHighMag == null) ||
        getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid() == false)
      return false;

    return true;
  }

  /**
   * Returns true if the left runtime alignment transform exists.
   *
   * @author Wei Chin, Chong
   */
  public boolean leftRuntimeAlignmentTransformForHighMagExists()
  {
    // If we have at least a manual transform, we are guaranteed to have something we can use as a runtime
    // transform.
    return leftManualAlignmentTransformForHighMagExists();
  }

  /**
   * Get the manual alignment AffineTransform for this board when
   * it is positioned against the right panel-in-place sensor.
   *
   * @author Wei Chin, Chong
   */
  public AffineTransform getRightManualAlignmentTransformForHighMag()
  {
    Assert.expect(_rightManualAlignmentTransformForHighMag != null);
    Assert.expect(getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    return _rightManualAlignmentTransformForHighMag;
  }

  /**
   * Get the runtime alignment AffineTransform for this board when
   * it is positioned against the right panel-in-place sensor.
   *
   * @author Wei Chin, Chong
   */
  public AffineTransform getRightRuntimeAlignmentTransformForHighMag()
  {
    Assert.expect(_rightManualAlignmentTransformForHighMag != null);
    Assert.expect(getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    if (_rightRuntimeAlignmentTransformForHighMag == null)
    {
      _rightRuntimeAlignmentTransformForHighMag = new AffineTransform(_rightManualAlignmentTransformForHighMag);
    }

    return _rightRuntimeAlignmentTransformForHighMag;
  }
  
  /**
   * @author Kee Chin Seong - Separating out the AFfine Transform from View CAD
   * @return 
   */
  public AffineTransform getRightLastSavedViewCadRuntimeAlignmentTransformForHighMag()
  {
    Assert.expect(_rightManualAlignmentTransformForHighMag != null);
    Assert.expect(getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    if (_rightLastSavedViewCadRuntimeAlignmentTransformForHighMag == null)
    {
      _rightLastSavedViewCadRuntimeAlignmentTransformForHighMag = new AffineTransform(_rightManualAlignmentTransformForHighMag);
    }

    return _rightLastSavedViewCadRuntimeAlignmentTransformForHighMag;
  }

  /**
   * @author Wei Chin, Chong
   * @author Phang Siew Yeng
   * This function is ONLY called by VerifyCadPanel to check if ALIGNMENT TRANSFORM is VALID.
   * For long panel/long board, alignment transform VALID - both left and right alignment transforms exist.
   * For short panel, alignment transform VALID - right/left alignment transform exists.
   * NOTE: Alignment VALID != Alignment COMPLETE
   */
  public boolean areManualAlignmentTransformsForHighMagValid()
  {
    boolean valid = true;
    if (isLongBoard() || useLeftAlignmentGroup())
    {
      if (leftManualAlignmentTransformForHighMagExists() == false ||
        _board.getPanel().getProject().getTestProgram().hasTestSubProgram(_board,PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.HIGH) == false)
        valid = false;
    }

    if(isLongBoard() || useLeftAlignmentGroup() == false)
    {
      if (rightManualAlignmentTransformForHighMagExists() == false ||
        _board.getPanel().getProject().getTestProgram().hasTestSubProgram(_board,PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.HIGH) == false)
        valid = false;
    }
    return valid;
  }

  /**
   * @return true if the right manual alignment transform exists.
   *
   * @author Wei Chin, Chong
   */
  public boolean rightManualAlignmentTransformForHighMagExists()
  {
    if ((_rightManualAlignmentTransformForHighMag == null) ||
         getBoard().getPanel().getProject().getProjectState().isManualAlignmentTransformValid() == false)
      return false;

    return true;
  }

  /**
   * Returns true if the left runtime alignment transform exists.
   *
   * @author Wei Chin, Chong
   */
  public boolean rightRuntimeAlignmentTransformForHighMagExists()
  {
    // If we have at least a manual transform, we are guaranteed to have something we can use as a runtime
    // transform.
    return rightManualAlignmentTransformForHighMagExists();
  }

  /**
   * @author sham
   */
  public void clearAllManualAlignmentTransform()
  {
    _leftManualAlignmentTransformForHighMag = null;
    _rightManualAlignmentTransformForHighMag = null;

    _leftManualAlignmentTransform = null;
    _rightManualAlignmentTransform = null;
  }
  
  /*
   * Goh Bee Hoon
   */
  public void setBoardZOffsetInNanometers(int zOffset)
  {
    int oldValue = _zOffset;
    _projectObservable.setEnabled(false);
    try
    {
      _zOffset = zOffset;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, zOffset, BoardSettingsEventEnum.BOARD_ZOFFSET);
  }
  
  /*
   * Goh Bee Hoon
   */
  public int getBoardZOffsetInNanometers()
  {
    return _zOffset;
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  public int getBoardZOffsetFromMeansOffsetInNanometers()
  {
    return _zOffset - getBoard().getPanel().getMeansBoardZOffsetInNanometers();
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void setLeftRuntimeManualAlignmentTransform(AffineTransform leftRuntimeManualAlignmentTransform)
  {
    _leftRuntimeManualAlignmentTransform = leftRuntimeManualAlignmentTransform;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void setRightRuntimeManualAlignmentTransform(AffineTransform rightRuntimeManualAlignmentTransform)
  {
    _rightRuntimeManualAlignmentTransform = rightRuntimeManualAlignmentTransform;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void setLeftRuntimeManualAlignmentTransformForHighMag(AffineTransform leftRuntimeManualAlignmentTransform)
  {
    _leftRuntimeManualAlignmentTransformForHighMag = leftRuntimeManualAlignmentTransform;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void setRightRuntimeManualAlignmentTransformForHighMag(AffineTransform rightRuntimeManualAlignmentTransform)
  {
    _rightRuntimeManualAlignmentTransformForHighMag = rightRuntimeManualAlignmentTransform;
  }

  /*
   * @author Phang Siew Yeng
   */
  public AffineTransform getLeftRuntimeManualAlignmentTransform()
  {
    return _leftRuntimeManualAlignmentTransform;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public AffineTransform getRightRuntimeManualAlignmentTransform()
  {
    return _rightRuntimeManualAlignmentTransform;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public AffineTransform getLeftRuntimeManualAlignmentTransformForHighMag()
  {
    return _leftRuntimeManualAlignmentTransformForHighMag;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public AffineTransform getRightRuntimeManualAlignmentTransformForHighMag()
  {
    return _rightRuntimeManualAlignmentTransformForHighMag;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void clearRuntimeManualAlignmentTransform()
  {
    _rightRuntimeManualAlignmentTransform = null;
    _leftRuntimeManualAlignmentTransform = null;
    
    _rightRuntimeManualAlignmentTransformForHighMag = null;
    _leftRuntimeManualAlignmentTransformForHighMag = null;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setUsing2DAlignmentMethod(boolean isUsing2DAlignmentMethod)
  {
    if (isUsing2DAlignmentMethod == _isUsing2DAlignmentMethod)
      return;

    boolean oldValue = _isUsing2DAlignmentMethod;
    _projectObservable.setEnabled(false);
    try
    {
      _isUsing2DAlignmentMethod = isUsing2DAlignmentMethod;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, isUsing2DAlignmentMethod, BoardSettingsEventEnum.SET_MANUAL_ALIGNMENT_MODE);
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public boolean isUsing2DAlignmentMethod()
  {
    return _isUsing2DAlignmentMethod;  
  }
}
