/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 *
 * @author weng-jian.eoh
 */
public class InitialRecipeSetting
{

  private static InitialRecipeSetting _instance = null;
  private InitialRecipeSettingReader _reader = InitialRecipeSettingReader.getInstance();
  private InitialRecipeSettingWriter _writer = InitialRecipeSettingWriter.getInstance();
  private List<String> _allSetNames = new ArrayList<String>();
  private String _currentSetFullPath = null;
  private boolean _isDirty = false;
  
  private Map<RecipeAdvanceSettingEnum,Object> _recipeSettingEnumToValueMap = new HashMap<>();
  private ConfigObservable _configObservable = ConfigObservable.getInstance();
  
  private final int _defaultMaxDroLevel = 1;
  private final int _defaultMinCameraForReconstruction = 14;
  private final int _maxDroLevel = Config.getInstance().getIntValue(SoftwareConfigEnum.MAXIMUM_DYNAMIC_RANGE_OPTIMIZATION_LEVEL);
  private final int _minCameraForReconstruction = Config.getInstance().getIntValue(SoftwareConfigEnum.MINIMUM_CAMERA_USED_FOR_RECONSTRUCTION);
  
  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private InitialRecipeSetting()
  {
    _currentSetFullPath = getSystemDefaultSetName();
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public static synchronized InitialRecipeSetting getInstance()
  {
    if (_instance == null)
    {
      _instance = new InitialRecipeSetting();
    }

    return _instance;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public void save() throws DatastoreException
  {
    // cannot save to the SystemDefaults file name
    Assert.expect(_currentSetFullPath.equalsIgnoreCase(getSystemDefaultSetName()) == false);

    _writer.write(_currentSetFullPath, _recipeSettingEnumToValueMap);
    _isDirty = false;
    // repopulate our list of available sets since we just saved one
    getAllSetNames();
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void load() throws DatastoreException
  {
    // cannot read the SystemDefaults file
    Assert.expect(_currentSetFullPath.equalsIgnoreCase(getSystemDefaultSetName()) == false);
    clear();

    // load from disk the _currentSetFullPAth and set _initiaRecipeSetting to it's contents
    _recipeSettingEnumToValueMap = _reader.read(_currentSetFullPath);
        
    _isDirty = false;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public void delete() throws DatastoreException
  {
    Assert.expect(_currentSetFullPath.equalsIgnoreCase(getSystemDefaultSetName()) == false);
    // add .config extension and find the correct path on disk before deleting
    FileUtilAxi.delete(_currentSetFullPath);

    // repopulate our list of available sets since we just deleted one
    getAllSetNames();
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private String getSystemDefaultSetName()
  {
    return FileName.getInitialRecipeDefaultSettingFileWithoutExtension();
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public boolean isDefaultSet(String setName)
  {
    if (setName.equalsIgnoreCase(getSystemDefaultSetName()))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public void loadNewRecipeSetting(String newSetName) throws DatastoreException
  {
    Assert.expect(newSetName != null);
    getAllSetNames();
    Assert.expect(_allSetNames.contains(newSetName));
    _currentSetFullPath = FileName.getInitialRecipeSettingFullPath(newSetName);
    if (newSetName.equalsIgnoreCase(getSystemDefaultSetName()))
    {
      clear();
    }
    else
    {
      load();
    }
    _isDirty = false;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public boolean isRecipeSettingNameExist(String newSetName) throws DatastoreException
  {
    Assert.expect(newSetName != null);
    getAllSetNames();
    return _allSetNames.contains(newSetName);
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public void loadDefaultSet()
  {
    _currentSetFullPath = getSystemDefaultSetName();
    clear();
    _isDirty = false;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public void changeRecipeSettingName(String newSetName)
  {
    Assert.expect(newSetName != null);
    _currentSetFullPath = FileName.getInitialRecipeSettingFullPath(newSetName);
    _isDirty = true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void createNewSet(String newSetName)
  {
    Assert.expect(newSetName != null);
    clear();
    _currentSetFullPath = FileName.getInitialRecipeSettingFullPath(newSetName);
    _isDirty = true;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public String getRecipeSettingName()
  {
    return FileUtil.getNameWithoutExtension(FileUtil.getNameWithoutPath(_currentSetFullPath));
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public boolean isDirty()
  {
    return _isDirty;
  }
  
  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private void clear()
  {
    _recipeSettingEnumToValueMap.clear();
  }
  
  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public List<String> getAllSetNames()
  {
    _allSetNames.clear();
    _allSetNames.addAll(FileName.getAllInitialRecipeSettingNames());
    return _allSetNames;
  }
  
  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public Object getRecipeSettingValue(RecipeAdvanceSettingEnum setting)
  {
    if (_recipeSettingEnumToValueMap.containsKey((RecipeAdvanceSettingEnum)setting))
    {
      return _recipeSettingEnumToValueMap.get(setting);
    }
      
    return setting.getDefaultValue();
  }
  
  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public void setRecipeSettingValue(RecipeAdvanceSettingEnum setting,Object value)
  {
    _recipeSettingEnumToValueMap.put(setting, value);
    _isDirty = true;
    Pair<RecipeAdvanceSettingEnum,Object> recipeSettingAndValue = new Pair(setting,value);
    _configObservable.stateChanged(recipeSettingAndValue);
  }
  
  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public Map<RecipeAdvanceSettingEnum,Object> getAllRecipeAdvanceSettingEnum()
  {
    return _recipeSettingEnumToValueMap;
  }
  
  public Map<RecipeAdvanceSettingEnum,Object> getRecipeAdvanceSettingEnumsInOrder()
  {
    Map<RecipeAdvanceSettingEnum, Object> treeMap = new TreeMap<RecipeAdvanceSettingEnum, Object>(_recipeSettingEnumToValueMap);
    
    return treeMap;
  }
}
