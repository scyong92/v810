package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;

/**
 * @author siew-yeng.phang
 */
public class SerialNumberMappingData
{
  private String _projectName;
  private String _boardName;
  private int _barcodeReaderId;
  private int _scanSequence;
  
  /*
   * @author Phang Siew Yeng
   */
  public SerialNumberMappingData(String projectName, String boardName, int barcodeReaderId, int serialNumberSequence)
  {
    Assert.expect(projectName != null);
    _projectName = projectName.intern();
    _boardName = boardName;
    _barcodeReaderId = barcodeReaderId;
    _scanSequence = serialNumberSequence;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public SerialNumberMappingData(SerialNumberMappingData data)
  {
    Assert.expect(data != null);
    _projectName = data.getProjectName();
    _boardName = data.getBoardName();
    _barcodeReaderId = data.getBarcodeReaderId();
    _scanSequence = data.getScanSequence();
  }
     
  /*
   * @author Phang Siew Yeng
   */
  public String getProjectName()
  {
    return _projectName;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void setBoardName(String newBoardName)
  {
    _boardName = newBoardName;
  }

  /*
   * @author Phang Siew Yeng
   */
  public String getBoardName()
  {
    return _boardName;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void setBarcodeReaderId(int newBarcodeReaderId)
  {
    _barcodeReaderId = newBarcodeReaderId;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public int getBarcodeReaderId()
  {
    return _barcodeReaderId;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void setSerialNumberSequence(int newScanSequence)
  {
    _scanSequence = newScanSequence;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public int getScanSequence()
  {
    return _scanSequence;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  @Override
  public boolean equals(Object obj)
  {
    if (obj == null || obj.getClass() != this.getClass())
      return false;
    
    final SerialNumberMappingData other = (SerialNumberMappingData) obj;
    if (Objects.equals(this._boardName, other._boardName) == false ||
      this._barcodeReaderId != other._barcodeReaderId ||
      this._scanSequence != other._scanSequence)
      return false;

    return true;
  }

  /*
   * @author Phang Siew Yeng
   */
  @Override
  public int hashCode()
  {
    int hash = 5;
    hash = 79 * hash + Objects.hashCode(this._boardName);
    hash = 79 * hash + this._barcodeReaderId;
    hash = 79 * hash + this._scanSequence;
    return hash;
  }
}
