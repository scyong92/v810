package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 * This class holds summary information.  This data can be loaded without loading
 * an entire project into memory.
 * NOTE that the data in this class will NOT be updated with the project currently
 * loaded into memory!!  It is just a snapshot of what was on disk.
 *
 * @author Bill Darbie
 */
public class ProjectSummary
{
  private double _version = 1.0;
  private String _projectNotes;
  private long _lastModificationTimeInMillis = -1;

  private String _targetCustomerName;
  private String _programmerName;

  private int _numBoardTypes = -1;
  private int _numBoards = -1;
  private List<String> _boardNames = new ArrayList<String>();
  private int _numComponents = -1;
  private int _numJoints = -1;
  private int _numInspectedJoints = -1;
  private int _numUnTestableJoints = -1; //XCR1374, KhangWah, 2011-09-12
  private long _cadXmlCheckSum = -1;
  private long _settingsXmlCheckSum = -1;
  private int _databaseVersion;
  private String _databaseComment;
  private String _projectName;
  private String _systemType;

  /**
   * @author Bill Darbie
   */
  public void setTargetCustomerName(String targetCustomerName)
  {
    Assert.expect(targetCustomerName != null);
    _targetCustomerName = targetCustomerName;
  }

  /**
   * @author Bill Darbie
   */
  public String getTargetCustomerName()
  {
    Assert.expect(_targetCustomerName != null);
    return _targetCustomerName;
  }

    /**
   * @author Bill Darbie
   */
  public void setProgrammerName(String programmerName)
  {
    Assert.expect(programmerName != null);
    _programmerName = programmerName;
  }

  /**
   * @author Bill Darbie
   */
  public String getProgrammerName()
  {
    Assert.expect(_programmerName != null);
    return _programmerName;
  }

  /**
   * @author Bill Darbie
   */
  public void setVersion(double version)
  {
    _version = version;
  }

  /**
   * @author Bill Darbie
   */
  public double getVersion()
  {
    Assert.expect(_version != -1.0);
    return _version;
  }

  /**
   * @author Bill Darbie
   */
  public void setLastModificationTimeInMillis(long lastModificationTimeInMillis)
  {
    _lastModificationTimeInMillis = lastModificationTimeInMillis;
  }

  /**
   * @author Bill Darbie
   */
  public long getLastModificationTimeInMillis()
  {
    Assert.expect(_lastModificationTimeInMillis != -1);
    return _lastModificationTimeInMillis;
  }

//  /**
//   * @author Bill Darbie
//   */
//  public void setProgrammerName(String programmerName)
//  {
//    Assert.expect(programmerName != null);
//    _programmerName = programmerName;
//  }
//
//  /**
//   * @author Bill Darbie
//   */
//  public String getProgrammerName()
//  {
//    Assert.expect(_programmerName != null);
//    return _programmerName;
//  }

  /**
   * @author Bill Darbie
   */
  public void setNumBoardTypes(int numBoardTypes)
  {
    Assert.expect(numBoardTypes >= 0);
    _numBoardTypes = numBoardTypes;
  }

  /**
   * @author Bill Darbie
   */
  public int getNumBoardTypes()
  {
    Assert.expect(_numBoardTypes >= 0);
    return _numBoardTypes;
  }

  /**
   * @author Bill Darbie
   */
  public void setNumBoards(int numBoards)
  {
    Assert.expect(numBoards >= 0);
    _numBoards = numBoards;
  }

  /**
   * @author Bill Darbie
   */
  public int getNumBoards()
  {
    Assert.expect(_numBoards >= 0);
    return _numBoards;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void addBoardName(String boardName)
  {
    Assert.expect(boardName != null);
    _boardNames.add(boardName);
  }

  /**
   * @author Andy Mechtenberg
   */
  public List<String> getBoardNames()
  {
    Assert.expect(_boardNames != null);
    Assert.expect(_boardNames.isEmpty() == false);
    return _boardNames;
  }

  /**
   * @author Bill Darbie
   */
  public void setNumComponents(int numComponents)
  {
    Assert.expect(numComponents >= 0);
    _numComponents = numComponents;
  }

  /**
   * @author Bill Darbie
   */
  public int getNumComponents()
  {
    Assert.expect(_numComponents >= 0);
    return _numComponents;
  }

  /**
   * @author Bill Darbie
   */
  public void setNumJoints(int numJoints)
  {
    Assert.expect(numJoints >= 0);
    _numJoints = numJoints;
  }

  /**
   * @author Bill Darbie
   */
  public int getNumJoints()
  {
    Assert.expect(_numJoints >= 0);
    return _numJoints;
  }

  /**
   * @author Bill Darbie
   */
  public void setNumInspectedJoints(int numInspectedJoints)
  {
    Assert.expect(numInspectedJoints >= 0);
    _numInspectedJoints = numInspectedJoints;
  }

  /**
   * @author Bill Darbie
   */
  public int getNumInspectedJoints()
  {
    Assert.expect(_numInspectedJoints >= 0);
    return _numInspectedJoints;
  }
  
   /**
   * @author Chnee Khang Wah, XCR1374
   */
  public void setNumUnTestableJoints(int numUnTestableJoints)
  {
    Assert.expect(numUnTestableJoints >= 0);
    _numUnTestableJoints = numUnTestableJoints;
  }
  
   /**
   * @author Chnee Khang Wah, XCR1374
   */
  public int getNumUnTestableJoints()
  {
    Assert.expect(_numUnTestableJoints >= 0);
    return _numUnTestableJoints;
  }

  /**
   * @author Bill Darbie
   */
  public double getJointTestCoveragePercent()
  {
    return 100.0 * (double)getNumInspectedJoints() / (double)getNumJoints();
  }

  /**
   * @author Bill Darbie
   */
  public void setProjectNotes(String projectNotes)
  {
    Assert.expect(projectNotes != null);
    _projectNotes = projectNotes;
  }

  /**
   * @author Bill Darbie
   */
  public String getProjectNotes()
  {
    Assert.expect(_projectNotes != null);
    return _projectNotes;
  }

  /**
   * @author George A. David
   */
  public long getCadXmlCheckSum()
  {
    Assert.expect(_cadXmlCheckSum > 0);

    return _cadXmlCheckSum;
  }

  /**
   * @author George A. David
   */
  public void setCadXmlCheckSum(long cadXmlCheckSum)
  {
    Assert.expect(cadXmlCheckSum > 0);

    _cadXmlCheckSum = cadXmlCheckSum;
  }

  /**
   * @author George A. David
   */
  public long getSettingsXmlCheckSum()
  {
    Assert.expect(_settingsXmlCheckSum > 0);

    return _settingsXmlCheckSum;
  }

  /**
   * @author George A. David
   */
  public void setSettingsXmlCheckSum(long settingsXmlCheckSum)
  {
    Assert.expect(settingsXmlCheckSum > 0);

    _settingsXmlCheckSum = settingsXmlCheckSum;
  }

  /**
   * @author George A. David
   */
  public void setDatabaseVersion(int databaseVersion)
  {
    Assert.expect(databaseVersion >= 0);

    _databaseVersion = databaseVersion;
  }

  /**
   * @author George A. David
   */
  public int getDatabaseVersion()
  {
    Assert.expect(_databaseVersion >= 0);

    return _databaseVersion;
  }

  /**
   * @author George A. David
   */
  public void setProjectName(String projectName)
  {
    Assert.expect(projectName != null);

    _projectName = projectName;
  }

  /**
   * @author George A. David
   */
  public String getProjectName()
  {
    Assert.expect(_projectName != null);

    return _projectName;
  }

  /**
   * @author George A. David
   */
  public void setDatabaseComment(String databaseComment)
  {
    Assert.expect(databaseComment != null);

    _databaseComment = databaseComment;
  }

  /**
   * @author George A. David
   */
  public String getDatabaseComment()
  {
    Assert.expect(_databaseComment != null);

    return _databaseComment;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setSystemType(String systemType)
  {
    Assert.expect(systemType != null);

    _systemType = systemType;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public String getSystemType()
  {
    Assert.expect(_systemType != null);

    return _systemType;
  }
}
