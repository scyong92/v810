package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;

/**
 * Manages the learned profile information for each joint.  The learned profiles contained herein
 * actually represent an incremental running average of
 *
 * @author Matt Wharton
 */
public class ShortProfileLearning
{
  private static float _uninitializedValue = -1f;

  private Pad _pad;
  private SliceNameEnum _sliceNameEnum;

  private int _numberOfThicknessProfilesLearned = 0;
  private float[] _averageLearnedThicknessProfile = null;
  private float _averageRegionGrayLevel = _uninitializedValue;

  /**
   * @author Matt Wharton
   */
  public ShortProfileLearning()
  {
    // Do nothing...
  }

  /**
   * @author Bill Darbie
   */
  public static float getUninitializedValue()
  {
    return _uninitializedValue;
  }

  /**
   * @author Bill Darbie
   */
  public void setPad(Pad pad)
  {
    Assert.expect(pad != null);
    _pad = pad;
  }

  /**
   * @author Bill Darbie
   */
  public Pad getPad()
  {
    Assert.expect(_pad != null);
    return _pad;
  }

  /**
   * @author Bill Darbie
   */
  public void setSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    _sliceNameEnum = sliceNameEnum;
  }

  /**
   * @author Bill Darbie
   */
  public SliceNameEnum getSliceNameEnum()
  {
    Assert.expect(_sliceNameEnum != null);
    return _sliceNameEnum;
  }

  /**
   * @author Matt Wharton
   */
  public float[] getAverageLearnedThicknessProfile()
  {
    Assert.expect(_averageLearnedThicknessProfile != null);

    return _averageLearnedThicknessProfile;
  }

  /**
   * @author Matt Wharton
   */
  public void setAverageLearnedThicknessProfile(float[] averageLearnedRingProfile)
  {
    // NOTE: _projectObservable.stateChanged() does not need to be called here - a higher level
    //       call we get the event out
    Assert.expect(averageLearnedRingProfile != null);

    _averageLearnedThicknessProfile = averageLearnedRingProfile;
  }

  /**
   * @author Matt Wharton
   */
  public int getNumberOfThicknessProfilesLearned()
  {
    return _numberOfThicknessProfilesLearned;
  }

  /**
   * @author Matt Wharton
   */
  public void setNumberOfThicknessProfilesLearned(int numberOfRingProfilesLearned)
  {
    // NOTE: _projectObservable.stateChanged() does not need to be called here - a higher level
    //       call we get the event out
    Assert.expect(numberOfRingProfilesLearned >= 0);

    _numberOfThicknessProfilesLearned = numberOfRingProfilesLearned;
  }

  /**
   * Adds the specified learned thickness profile to our incremental average thickness profile.
   *
   * @author Matt Wharton
   */
  public void addLearnedThicknessProfile(float[] thicknessProfile)
  {
    // NOTE: _projectObservable.stateChanged() does not need to be called here - a higher level
    //       call we get the event out

    Assert.expect(thicknessProfile != null);
    Assert.expect(_numberOfThicknessProfilesLearned >= 0);


    // If this is the first profile we've learned, just store it.
    if (_numberOfThicknessProfilesLearned == 0)
    {
      // Increment the total number of ring profiles we've learned.
      ++_numberOfThicknessProfilesLearned;

      _averageLearnedThicknessProfile = thicknessProfile;
    }
    // Otherwise, average in this latest profile with the incremental average profile.
    else
    {
      // Increment the total number of ring profiles we've learned.
      ++_numberOfThicknessProfilesLearned;

      _averageLearnedThicknessProfile = makeWeightedAverageProfile(_averageLearnedThicknessProfile,
                                                                   thicknessProfile,
                                                                   _numberOfThicknessProfilesLearned);
    }
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasAverageLearnedThicknessProfile()
  {
    return _averageLearnedThicknessProfile != null;
  }

  /**
   * Clears out the pre-existing average learned thickness profile and sets the learned profile count back to zero.
   *
   * @author Matt Wharton
   */
  public void clearAverageLearnedThicknessProfile()
  {
    Assert.expect(_averageLearnedThicknessProfile != null);

    _averageLearnedThicknessProfile = null;
    _numberOfThicknessProfilesLearned = 0;
  }

  /**
   * @author Matt Wharton
   */
  public float getAverageRegionGrayLevel()
  {
    Assert.expect(_averageRegionGrayLevel != _uninitializedValue);

    return _averageRegionGrayLevel;
  }

  /**
   * @author Matt Wharton
   */
  public void setAverageRegionGrayLevel(float averageRegionGrayLevel)
  {
    Assert.expect(MathUtil.fuzzyGreaterThanOrEquals(averageRegionGrayLevel, 0f) &&
                  MathUtil.fuzzyLessThan(averageRegionGrayLevel, 256f));

    _averageRegionGrayLevel = averageRegionGrayLevel;
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasAverageRegionGrayLevel()
  {
    return (_averageRegionGrayLevel != _uninitializedValue);
  }

  /**
   * Updates the specified weighted average profile with the specified candidate profile.
   *
   * @author Matt Wharton
   */
  private float[] makeWeightedAverageProfile(float[] oldWeightedAverageProfile,
                                             float[] candidateProfile,
                                             int totalNumberOfProfilesInAverage)
  {
    // NOTE: _projectObservable.stateChanged() does not need to be called here - a higher level
    //       call we get the event out

    Assert.expect(oldWeightedAverageProfile != null);
    Assert.expect(candidateProfile != null);
    Assert.expect(oldWeightedAverageProfile.length == candidateProfile.length);
    Assert.expect(totalNumberOfProfilesInAverage > 1);

    float[] newWeightedAverageProfile = oldWeightedAverageProfile;

    for (int i = 0; i < oldWeightedAverageProfile.length; ++i)
    {
      newWeightedAverageProfile[i] = (((totalNumberOfProfilesInAverage - 1f) / totalNumberOfProfilesInAverage) *
                                      oldWeightedAverageProfile[i])
                                     + ((1f / totalNumberOfProfilesInAverage) * candidateProfile[i]);
    }

    return newWeightedAverageProfile;
  }
}
