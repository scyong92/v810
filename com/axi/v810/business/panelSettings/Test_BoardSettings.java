package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Andy Mechtenberg
 */
class Test_BoardSettings extends UnitTest
{
  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_BoardSettings());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    BoardSettings boardSettings = new BoardSettings();

    try
    {
      boardSettings.isInspected();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    boolean inspected = true;
    boardSettings.setInspected(inspected);
    Expect.expect(boardSettings.isInspected() == inspected);

    // populated
    try
    {
      boardSettings.isPopulated();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    boolean populated = true;
    boardSettings.setPopulated(populated);
    Expect.expect(boardSettings.isPopulated() == populated);

// wpd virgo
//    // coordinate converter
//    // the default is null for panel and boards
//    try
//    {
//      boardSettings.getCoordinateConverter();
//      Expect.expect(false);
//    }
//    catch (Exception ex)
//    {
//      // do nothing
//    }
//
//    try
//    {
//      boardSettings.setCoordinateConverter(null);
//      Expect.expect(false);
//    }
//    catch (Exception ex)
//    {
//      // do nothing
//    }
//    CoordinateConverter coordinateConverter = new CoordinateConverter();
//    boardSettings.setCoordinateConverter(coordinateConverter);
//    Expect.expect(boardSettings.getCoordinateConverter() == coordinateConverter);

// wpd virgo
//    // sensor postion
//    try
//    {
//      boardSettings.getSensorPosition();
//      Expect.expect(false);
//    }
//    catch (Exception ex)
//    {
//      // do nothing
//    }
//    try
//    {
//      boardSettings.setSensorPosition(null);
//      Expect.expect(false);
//    }
//    catch (Exception ex)
//    {
//      // do nothing
//    }
//    SensorPositionEnum position = SensorPositionEnum.RIGHT;
//    boardSettings.setSensorPosition(position);
//    Expect.expect(boardSettings.getSensorPosition().equals(position));

    // readLaserSurfaceMapData is tested in Test_Panel as it needs a full panel load to test correctly.
  }
}
