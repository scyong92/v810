package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class PanelSettingsEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static PanelSettingsEventEnum CREATE_OR_DESTROY = new PanelSettingsEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static PanelSettingsEventEnum PANEL = new PanelSettingsEventEnum(++_index);
  public static PanelSettingsEventEnum ALGORITHM_FAMILY_LIBRARY = new PanelSettingsEventEnum(++_index);
  public static PanelSettingsEventEnum LEFT_TO_RIGHT_FLIP = new PanelSettingsEventEnum(++_index);
  public static PanelSettingsEventEnum TOP_TO_BOTTOM_FLIP = new PanelSettingsEventEnum(++_index);
  public static PanelSettingsEventEnum SHIFT_X = new PanelSettingsEventEnum(++_index);
  public static PanelSettingsEventEnum SHIFT_Y = new PanelSettingsEventEnum(++_index);
  public static PanelSettingsEventEnum MANUAL_ALIGNMENT_TRANSFORM_GENERATED = new PanelSettingsEventEnum(++_index);
  public static PanelSettingsEventEnum MANUAL_ALIGNMENT_TRANSFORM_DESTROYED = new PanelSettingsEventEnum(++_index);
  public static PanelSettingsEventEnum ADD_ALIGNMENT_GROUP = new PanelSettingsEventEnum(++_index);
  public static PanelSettingsEventEnum DEGREES_ROTATION = new PanelSettingsEventEnum(++_index);
  public static PanelSettingsEventEnum ADD_OR_REMOVE_UNFOCUSED_REGION_SLICE_TO_BE_FIXED = new PanelSettingsEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static PanelSettingsEventEnum ADD_OR_REMOVE_UNFOCUSED_REGION_SLICE_TO_Z_OFFSET = new PanelSettingsEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static PanelSettingsEventEnum ADD_OR_REMOVE_UNFOCUSED_REGION_NAME_TO_RETEST = new PanelSettingsEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static PanelSettingsEventEnum ADD_OR_REMOVE_NO_TEST_REGION_NAME = new PanelSettingsEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static PanelSettingsEventEnum POTENTIAL_LONG_PROGRAM_DETERMINED = new PanelSettingsEventEnum(++_index);
  public static PanelSettingsEventEnum ARTIFACT_COMPENSATION_STATE = new PanelSettingsEventEnum(++_index);

  public static PanelSettingsEventEnum ALIGNMENT_METHOD = new PanelSettingsEventEnum(++_index);
  public static PanelSettingsEventEnum GLOBAL_SURFACE_MODEL_STATE = new PanelSettingsEventEnum(++_index);
  public static PanelSettingsEventEnum USER_GAIN = new PanelSettingsEventEnum(++_index);
  
  public static PanelSettingsEventEnum SPLIT_SETTING = new PanelSettingsEventEnum(++_index);
  public static PanelSettingsEventEnum USE_NEW_THICKNESS_TABLE = new PanelSettingsEventEnum(++_index);
  public static PanelSettingsEventEnum BYPASS_ALIGNMENT_TRANSFORM_WITHIN_LIMIT_VERIFICATION = new PanelSettingsEventEnum(++_index);
  
  //Khaw Chek Hau - XCR2285: 2D Alignment on v810
  public static PanelSettingsEventEnum SET_ALIGNMENT_MODE = new PanelSettingsEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private PanelSettingsEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private PanelSettingsEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
