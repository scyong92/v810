package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 *
 * @author Administrator
 */
public class OpticalRegion implements Serializable
{
  // Map of surface maps that are parked under this surface map region

  private Map<String, PanelCoordinate> _pointPanelPositionNameToPointPanelCoordinateMap = new LinkedHashMap<String, PanelCoordinate>(); // PanelSurfaceMapSettingsReader
  private Map<Board, List<Component>> _boardNameToComponenMap = new LinkedHashMap<Board, List<Component>>();
  private Map<Board, List<OpticalCameraRectangle>> _boardNameToOpticalCameraRectangleMap = new LinkedHashMap<Board, List<OpticalCameraRectangle>>();
  private Map<Board, Map<String, PanelCoordinate>> _boardNameToPointPanelCoordinateMap = new LinkedHashMap<Board, Map<String, PanelCoordinate>>();
  
  private PanelRectangle _panelRectangle;
  private PanelCoordinate _coordinate;
  private boolean _isDataAvailable;
  private static transient ProjectObservable _projectObservable;
  private String _name;
  private String _alignmentGroupName; // For Alignment OpticalRegion usage
  private boolean _setToUsePspForAlignment = true;
  // This section is mainly used for PanelBasedAlignment
  private List<Component> _components = new ArrayList<Component>();
  private List<OpticalCameraRectangle> _opticalCameraRectangles = new ArrayList<OpticalCameraRectangle>();
  private List<Board> _boards = new ArrayList<Board>();
  private transient ThinPlateSpline _thinPlateSpline;
  private java.awt.geom.AffineTransform _transformFromRendererCoords = new java.awt.geom.AffineTransform();
  private List<MeshTriangle> _meshTriangles = new ArrayList<MeshTriangle>();
  
  private static transient ComponentMagnificationTypeAndUserGainComparator _componentMagnificationTypeAndUserGainComparator;

  /**
   * @author Jack Hwee
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
    _componentMagnificationTypeAndUserGainComparator = new ComponentMagnificationTypeAndUserGainComparator();
  }

  /**
   * @author Cheah Lee Herng
   */
  public OpticalRegion()
  {
    _projectObservable.stateChanged(this, null, this, PanelSurfaceMapEventEnum.CREATE_OR_DESTROY);
    _isDataAvailable = false;
    _name = "";
    _alignmentGroupName = "";
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.stateChanged(this, this, null, PanelSurfaceMapEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setRegion(int xCoordinate, int yCoordinate, int width, int height)
  {
    Assert.expect(xCoordinate >= 0);
    Assert.expect(yCoordinate >= 0);
    Assert.expect(width >= 0);
    Assert.expect(height >= 0);

    _panelRectangle = new PanelRectangle(xCoordinate, yCoordinate, width, height);
    setName(xCoordinate, yCoordinate, width, height);
  }

  /**
   * @author Jack Hwee
   */
  public void setAffineTransform(java.awt.geom.AffineTransform affineTransform)
  {
    double scaleX = affineTransform.getScaleX();
    double shearY = affineTransform.getShearY();
    double shearX = affineTransform.getShearX();
    double scaleY = affineTransform.getScaleY();
    double translateX = affineTransform.getTranslateX();
    double translateY = affineTransform.getTranslateY();

    _transformFromRendererCoords.setTransform(scaleX, shearY, shearX, scaleY, translateX, translateY);
  }

  /**
   * @author Jack Hwee
   */
  public java.awt.geom.AffineTransform getAffineTransform()
  {
    Assert.expect(_transformFromRendererCoords != null);
    return _transformFromRendererCoords;
  }

  /**
   * @author Cheah Lee Herng
   */
  public PanelRectangle getRegion()
  {
    Assert.expect(_panelRectangle != null);
    return _panelRectangle;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void addSurfaceMapPoints(int pointXCoordinateInNanometers, int pointYCoordinateInNanometers, boolean isVerified)
  {
    Assert.expect(_pointPanelPositionNameToPointPanelCoordinateMap != null);
    Assert.expect(pointXCoordinateInNanometers >= 0);
    Assert.expect(pointYCoordinateInNanometers >= 0);

    _isDataAvailable = true;

    PanelCoordinate panelCoordinate = new PanelCoordinate();
    panelCoordinate.setLocation(pointXCoordinateInNanometers, pointYCoordinateInNanometers);
    String panelPositionName = pointXCoordinateInNanometers + "_" + pointYCoordinateInNanometers;

    PanelCoordinate oldPanelCoordinateValue = null;
    _projectObservable.setEnabled(false);
    try
    {
      oldPanelCoordinateValue = _pointPanelPositionNameToPointPanelCoordinateMap.put(panelPositionName, panelCoordinate);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldPanelCoordinateValue, panelCoordinate, PanelSurfaceMapEventEnum.ADD_SURFACE_MAP_POINTS);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void addSurfaceMapPoints(Board board, int pointXCoordinateInNanometers, int pointYCoordinateInNanometers)
  {
    Assert.expect(_boardNameToPointPanelCoordinateMap != null);
    Assert.expect(pointXCoordinateInNanometers >= 0);
    Assert.expect(pointYCoordinateInNanometers >= 0);

    PanelCoordinate panelCoordinate = new PanelCoordinate();
    panelCoordinate.setLocation(pointXCoordinateInNanometers, pointYCoordinateInNanometers);
    String panelPositionName = pointXCoordinateInNanometers + "_" + pointYCoordinateInNanometers;

    PanelCoordinate oldPanelCoordinateValue = null;
    _projectObservable.setEnabled(false);
    try
    {
      if (_boardNameToPointPanelCoordinateMap.containsKey(board))
      {
        Map<String, PanelCoordinate> pointPanelCoordinateNameToPointPanelCoordinateMap = _boardNameToPointPanelCoordinateMap.get(board);
        if (pointPanelCoordinateNameToPointPanelCoordinateMap.containsKey(panelPositionName) == false)
        {
          pointPanelCoordinateNameToPointPanelCoordinateMap.put(panelPositionName, panelCoordinate);
        }
      }
      else
      {
        Map<String, PanelCoordinate> pointPanelCoordinateNameToPointPanelCoordinateMap = new HashMap<String, PanelCoordinate>();
        pointPanelCoordinateNameToPointPanelCoordinateMap.put(panelPositionName, panelCoordinate);
        _boardNameToPointPanelCoordinateMap.put(board, pointPanelCoordinateNameToPointPanelCoordinateMap);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldPanelCoordinateValue, panelCoordinate, PanelSurfaceMapEventEnum.ADD_SURFACE_MAP_POINTS);
  }

  /**
   * @author Jack Hwee
   */
  public void removeSurfaceMapPoint(int panelXCoordinateInNanometers, int panelYCoordinateInNanometers)
  {
    Assert.expect(_pointPanelPositionNameToPointPanelCoordinateMap != null);

    _isDataAvailable = true;

    PanelCoordinate panelCoordinate = new PanelCoordinate();
    panelCoordinate.setLocation(panelXCoordinateInNanometers, panelYCoordinateInNanometers);
    String panelPositionName = panelXCoordinateInNanometers + "_" + panelYCoordinateInNanometers;

    PanelCoordinate oldPanelCoordinateValue = null;
    _projectObservable.setEnabled(false);
    try
    {
      oldPanelCoordinateValue = _pointPanelPositionNameToPointPanelCoordinateMap.remove(panelPositionName);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldPanelCoordinateValue, panelCoordinate, PanelSurfaceMapEventEnum.REMOVE_SURFACE_MAP_POINTS);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void removeSurfaceMapPoint(Board board, int panelXCoordinateInNanometers, int panelYCoordinateInNanometers)
  {
    Assert.expect(_boardNameToPointPanelCoordinateMap != null);
    Assert.expect(panelXCoordinateInNanometers >= 0);
    Assert.expect(panelYCoordinateInNanometers >= 0);

    PanelCoordinate panelCoordinate = new PanelCoordinate();
    panelCoordinate.setLocation(panelXCoordinateInNanometers, panelYCoordinateInNanometers);
    String panelPositionName = panelXCoordinateInNanometers + "_" + panelYCoordinateInNanometers;

    PanelCoordinate oldPanelCoordinateValue = null;
    _projectObservable.setEnabled(false);
    try
    {
      if (_boardNameToPointPanelCoordinateMap.containsKey(board))
      {
        Map<String, PanelCoordinate> pointPanelCoordinateNameToPointPanelCoordinateMap = _boardNameToPointPanelCoordinateMap.get(board);
        if (pointPanelCoordinateNameToPointPanelCoordinateMap.containsKey(panelPositionName))
        {
          pointPanelCoordinateNameToPointPanelCoordinateMap.remove(panelPositionName);
        }
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldPanelCoordinateValue, panelCoordinate, PanelSurfaceMapEventEnum.REMOVE_SURFACE_MAP_POINTS);
  }

  /**
   * @author Cheah Lee Herng
   */
  public Map<String, PanelCoordinate> getPointPanelPositionNameToPointPanelCoordinateMap()
  {
    Assert.expect(_pointPanelPositionNameToPointPanelCoordinateMap != null);
    return _pointPanelPositionNameToPointPanelCoordinateMap;
  }

  /**
   * @return a List containing panelCoordinates for this LandPattern
   * @author Jack Hwee
   */
  public List<PanelCoordinate> getPointPanelCoordinates()
  {
    Assert.expect(_pointPanelPositionNameToPointPanelCoordinateMap != null);

    return new ArrayList<PanelCoordinate>(_pointPanelPositionNameToPointPanelCoordinateMap.values());
  }

  /**
   * @author Cheah Lee Herng
   */
  public PanelCoordinate getPointPanelCoordinate(String pointPositionName)
  {
    Assert.expect(pointPositionName != null);
    Assert.expect(_pointPanelPositionNameToPointPanelCoordinateMap != null);

    PanelCoordinate pointPanelCoordinate = _pointPanelPositionNameToPointPanelCoordinateMap.get(pointPositionName);
    Assert.expect(pointPanelCoordinate != null);

    return pointPanelCoordinate;
  }

  /**
   * @author Cheah Lee Herng
   */
  public List<PanelCoordinate> getPointPanelCoordinates(Board board)
  {
    Assert.expect(board != null);
    Assert.expect(_boardNameToPointPanelCoordinateMap != null);

    List<PanelCoordinate> panelCoordinates = new ArrayList<PanelCoordinate>();
    if (_boardNameToPointPanelCoordinateMap.containsKey(board))
    {
      Map<String, PanelCoordinate> pointPositionNameToPointPanelCoordinateMap = _boardNameToPointPanelCoordinateMap.get(board);
      for (Map.Entry<String, PanelCoordinate> entry : pointPositionNameToPointPanelCoordinateMap.entrySet())
      {
        panelCoordinates.add(entry.getValue());
      }
    }
    return panelCoordinates;
  }

  /**
   * @author Cheah Lee Herng
   */
  public PanelCoordinate getPointPanelCoordinate(Board board, String pointPositionName)
  {
    Assert.expect(board != null);
    Assert.expect(pointPositionName != null);
    Assert.expect(_boardNameToPointPanelCoordinateMap != null);

    PanelCoordinate pointPanelCoordinate = null;
    if (_boardNameToPointPanelCoordinateMap.containsKey(board))
    {
      Map<String, PanelCoordinate> pointPositionNameToPointCoordinateMap = _boardNameToPointPanelCoordinateMap.get(board);
      if (pointPositionNameToPointCoordinateMap.containsKey(pointPositionName))
      {
        pointPanelCoordinate = pointPositionNameToPointCoordinateMap.get(pointPositionName);
      }
    }
    return pointPanelCoordinate;
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean isDataAvailable()
  {
    return _isDataAvailable;
  }

  public String toString()
  {
    return "Region_";
  }

  /**
   * The name of this region depends on it' s attributes. It is
   * <top|bottom>_<xCoordinate>_<yCoordinate>_<width>_<length> The
   * reconstruction region must be completely created before you can ask for its
   * name.
   *
   * @author George A. David
   */
  public String getName()
  {
    return _name;
  }

  /**
   * @author Jack Hwee
   */
  public void setName(int xCoordinateInNanometers, int yCoordinateInNanometers, int widthInNanometers, int heightInNanometers)
  {
    Assert.expect(xCoordinateInNanometers >= 0);
    Assert.expect(yCoordinateInNanometers >= 0);
    Assert.expect(widthInNanometers > 0);
    Assert.expect(heightInNanometers > 0);

    _name = "";
    _name += (int)(xCoordinateInNanometers) + "_"
          + (int)(yCoordinateInNanometers) + "_"
          + (int) (widthInNanometers) + "_"
          + (int)(heightInNanometers);    
  }

  /**
   * @author Jack Hwee
   */
  public void setOpticalRegionName(OpticalRegion opticalRegion)
  {
    Assert.expect(opticalRegion != null);

    _name = "";

    _name += opticalRegion.getRegion().getMinX() + "_"
            + opticalRegion.getRegion().getMinY() + "_"
            + opticalRegion.getRegion().getWidth() + "_"
            + opticalRegion.getRegion().getHeight();
  }

  /**
   * @author Cheah Lee Herng
   */
  public String getAlignmentGroupName()
  {
    Assert.expect(_alignmentGroupName != null);
    return _alignmentGroupName;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setAlignmentGroupName(String alignmentGroupName)
  {
    Assert.expect(alignmentGroupName != null);
    _alignmentGroupName = alignmentGroupName;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void addModelPoint(int coordinateXInNanometers, int coordinateYInNanometers, int zheightInNanometers)
  {
    if (_thinPlateSpline == null)
      _thinPlateSpline = new ThinPlateSpline(_name);

    _thinPlateSpline.addPoint(coordinateXInNanometers, coordinateYInNanometers, zheightInNanometers);

    if (Config.isDeveloperDebugModeOn())
    {
      System.out.println("OpticalRegion: Add model point (" + coordinateXInNanometers + "," + coordinateYInNanometers + ") with height " + zheightInNanometers + " nanometers into TPS model.");
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public void resetModel()
  {
    if (_thinPlateSpline == null)
      _thinPlateSpline = new ThinPlateSpline(_name);

    _thinPlateSpline.reset();
  }

  /**
   * @author Cheah Lee Herng
   */
  public void addPointStagePositionZHeightInNanometers(int pointStagePositionXInNanometers,
          int pointStagePositionYInNanometers,
          int zHeightInNanometers)
  {
    // Add point into TPS model
    // We need to do explicit data type conversion here
    // from double to float as it is only accepted float data
    // type in TPS model.
    addModelPoint(pointStagePositionXInNanometers, pointStagePositionYInNanometers, zHeightInNanometers);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void updateModel()
  {
    if (_thinPlateSpline != null)
    {
      _thinPlateSpline.update();

      if (Config.isDeveloperDebugModeOn())
      {
        System.out.println("OpticalRegion " + _name + " : TPS model data point = " + _thinPlateSpline.getNumberOfControlPoints());
      }
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public double getZHeightInNanometers(int focusRegionCenterXInNanometers, int focusRegionCenterYInNanometers)
  {
    Assert.expect(_thinPlateSpline != null);
    return _thinPlateSpline.interpolateZHeightInNanometers(focusRegionCenterXInNanometers, focusRegionCenterYInNanometers);
  }

  /**
   * @author Cheah Lee Herng
   */
  public List<Component> getComponents()
  {
    Assert.expect(_components != null);

    List<Component> components = new ArrayList<Component>();
    for (Component comp : _components)
    {
      if (comp.getComponentType().isLoaded())
        components.add(comp);
    }
    //components.addAll(_components);
    Collections.sort(components, _componentMagnificationTypeAndUserGainComparator);
    return components;
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean hasComponent(Component component)
  {
    Assert.expect(component != null);
    Assert.expect(_components != null);

    if (_components.contains(component))
      return true;
    else
      return false;
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean hasComponent(Component component, Board board)
  {
    Assert.expect(component != null);
    Assert.expect(board != null);
    Assert.expect(_boardNameToComponenMap != null);

    if (_boardNameToComponenMap.containsKey(board))
    {
      List<Component> components = _boardNameToComponenMap.get(board);
      if (components.contains(component))
        return true;
    }
    return false;
  }

  /**
   * @author Cheah Lee Herng
   */
  public List<Component> getComponents(Board board)
  {
    Assert.expect(board != null);
    Assert.expect(_boardNameToComponenMap != null);

    List<Component> components = new ArrayList<Component>();
    if (_boardNameToComponenMap.containsKey(board))
    {
      for (Component comp : _boardNameToComponenMap.get(board))
      {
        if (comp.getComponentType().isLoaded())
          components.add(comp);
      }
    }
    Collections.sort(components, _componentMagnificationTypeAndUserGainComparator);
    return components;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void addComponent(Component component)
  {
    Assert.expect(component != null);
    Assert.expect(_components != null);

    boolean isComponentAddedSuccessfully = false;
    _projectObservable.setEnabled(false);
    try
    {
      if (_components.contains(component) == false)
         isComponentAddedSuccessfully = _components.add(component);
    }
    finally
    {
      _projectObservable.setEnabled(true);
      if (isComponentAddedSuccessfully)
      {
        _projectObservable.stateChanged(this, null, component, PanelSurfaceMapEventEnum.ADD_SURFACE_MAP_COMPONENT);
      }
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public void addComponent(Board board, Component component)
  {
    Assert.expect(board != null);
    Assert.expect(component != null);
    Assert.expect(_boardNameToComponenMap != null);

    boolean isComponentAddedSuccessfully = false;
    _projectObservable.setEnabled(false);
    try
    {
      if (_boardNameToComponenMap.containsKey(board))
      {
        List<Component> components = _boardNameToComponenMap.get(board);
        Component comp = board.getComponent(component.getReferenceDesignator());
        if (components.contains(comp) == false)
          isComponentAddedSuccessfully = components.add(comp);
      }
      else
      {
        List<Component> components = new ArrayList<Component>();
        components.add(component);
        _boardNameToComponenMap.put(board, components);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
      if (isComponentAddedSuccessfully)
      {
        _projectObservable.stateChanged(this, null, component, PanelSurfaceMapEventEnum.ADD_SURFACE_MAP_COMPONENT);
      }
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public void removeComponent(Component component)
  {
    Assert.expect(component != null);
    Assert.expect(_components != null);

    boolean isComponentRemovedSuccessfully = false;
    _projectObservable.setEnabled(false);
    try
    {
      if (_components.contains(component))
      {
        isComponentRemovedSuccessfully = _components.remove(component);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
      if (isComponentRemovedSuccessfully)
      {
        _projectObservable.stateChanged(this, null, component, PanelSurfaceMapEventEnum.REMOVE_SURFACE_MAP_COMPONENT);
      }
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removeAllComponents()
  {
    Assert.expect(_components != null);
    
    _components.clear();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void removeAllPanelCoordinates()
  {
    if (_pointPanelPositionNameToPointPanelCoordinateMap != null)
      _pointPanelPositionNameToPointPanelCoordinateMap.clear();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removeAllComponents(Board board)
  {
    Assert.expect(board != null);
    Assert.expect(_components != null);
    
    _boardNameToComponenMap.remove(board);
    _components.removeAll(getComponents(board));
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void removeAllPanelCoordinates(Board board)
  {
    Assert.expect(board != null);
    
    if (_boardNameToPointPanelCoordinateMap.containsKey(board))
    {
      Map<String, PanelCoordinate> panelCoordinateMap = _boardNameToPointPanelCoordinateMap.get(board);
      for(Map.Entry<String, PanelCoordinate> entry : panelCoordinateMap.entrySet())
      {
        String panelCoordinateName = entry.getKey();
        if (_pointPanelPositionNameToPointPanelCoordinateMap.containsKey(panelCoordinateName))
          _pointPanelPositionNameToPointPanelCoordinateMap.remove(panelCoordinateName);
      }
            
      _boardNameToPointPanelCoordinateMap.remove(board);
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public void removeComponent(Component component, Board board)
  {
    Assert.expect(component != null);
    Assert.expect(board != null);

    boolean isComponentRemovedSuccessfully = false;
    _projectObservable.setEnabled(false);
    try
    {
      if (_boardNameToComponenMap.containsKey(board))
      {
        List<Component> components = _boardNameToComponenMap.get(board);
        if (components.contains(component))
          isComponentRemovedSuccessfully = components.remove(component);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
      if (isComponentRemovedSuccessfully)
      {
        _projectObservable.stateChanged(this, null, component, PanelSurfaceMapEventEnum.REMOVE_SURFACE_MAP_COMPONENT);
      }
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean isSufficientModelDataPoint()
  {
    boolean isSufficientSurfaceModelDataPoint = false;
    if (_thinPlateSpline != null)
    {
      isSufficientSurfaceModelDataPoint = _thinPlateSpline.isSufficientSurfaceModelDataPoint();
    }

    return isSufficientSurfaceModelDataPoint;
  }

  /**
   * @author Jack Hwee
   */
  public void setCoordinate(PanelCoordinate coordinate)
  {
    Assert.expect(coordinate != null);

    if (coordinate.equals(_coordinate))
      return;

    PanelCoordinate oldValue = _coordinate;
    _projectObservable.setEnabled(false);

    _coordinate = coordinate;

    _projectObservable.setEnabled(true);

    _projectObservable.stateChanged(this, oldValue, coordinate, PanelSurfaceMapEventEnum.SET_SURFACE_MAP_COORDINATE);
  }

  /**
   *
   * @author Jack Hwee
   */
  public PanelCoordinate getCoordinate()
  {
    Assert.expect(_coordinate != null);

    return new PanelCoordinate(_coordinate);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {
    if (_pointPanelPositionNameToPointPanelCoordinateMap != null)
      _pointPanelPositionNameToPointPanelCoordinateMap.clear();
    if (_components != null)
      _components.clear();
    if (_opticalCameraRectangles != null)
      _opticalCameraRectangles.clear();
    if (_boards != null)
      _boards.clear();
    if (_boardNameToComponenMap != null)
      _boardNameToComponenMap.clear();
    if (_boardNameToOpticalCameraRectangleMap != null)
      _boardNameToOpticalCameraRectangleMap.clear();
    if (_boardNameToPointPanelCoordinateMap != null)
      _boardNameToPointPanelCoordinateMap.clear();
  }

  /**
   * @author Cheah Lee Herng
   */
  public List<Board> getBoards()
  {
    Assert.expect(_boards != null);
    return _boards;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setBoard(Board board)
  {
    Assert.expect(board != null);
    Assert.expect(_boards != null);

    if (_boards.contains(board) == false)
      _boards.add(board);

    if (_boardNameToComponenMap.containsKey(board) == false)
    {
      _boardNameToComponenMap.put(board, new ArrayList<Component>());
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public void addOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {
    Assert.expect(opticalCameraRectangle != null);

    boolean isNewOpticalCameraRectangleAddedSuccessful = false;
    _projectObservable.setEnabled(false);
    try
    {
      if (_opticalCameraRectangles.contains(opticalCameraRectangle))
      {
        _opticalCameraRectangles.remove(opticalCameraRectangle);
      }
      isNewOpticalCameraRectangleAddedSuccessful = _opticalCameraRectangles.add(opticalCameraRectangle);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    if (isNewOpticalCameraRectangleAddedSuccessful)
    {
      _projectObservable.stateChanged(this, null, opticalCameraRectangle, PanelSurfaceMapEventEnum.ADD_SURFACE_MAP_OPTICAL_RECTANGLE);
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public void addBoardOpticalCameraRectangle(Board board, OpticalCameraRectangle opticalCameraRectangle)
  {
    Assert.expect(board != null);
    Assert.expect(opticalCameraRectangle != null);

    _projectObservable.setEnabled(false);
    try
    {
      opticalCameraRectangle.setBoard(board);
      if (_boardNameToOpticalCameraRectangleMap.containsKey(board))
      {
        if (_boardNameToOpticalCameraRectangleMap.get(board).contains(opticalCameraRectangle))
        {
          _boardNameToOpticalCameraRectangleMap.get(board).remove(opticalCameraRectangle);
        }
        _boardNameToOpticalCameraRectangleMap.get(board).add(opticalCameraRectangle);
      }
      else
      {
        List<OpticalCameraRectangle> opticalCameraRectangles = new ArrayList<OpticalCameraRectangle>();
        opticalCameraRectangles.add(opticalCameraRectangle);
        _boardNameToOpticalCameraRectangleMap.put(board, opticalCameraRectangles);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, opticalCameraRectangle, PanelSurfaceMapEventEnum.ADD_SURFACE_MAP_OPTICAL_RECTANGLE);
  }

  /**
   * @author Cheah Lee Herng
   */
  public List<OpticalCameraRectangle> getAllOpticalCameraRectangles()
  {
    Assert.expect(_opticalCameraRectangles != null);

    List<OpticalCameraRectangle> newOpticalCameraRectangleList = new ArrayList<OpticalCameraRectangle>();
    for (OpticalCameraRectangle opticalCameraRectangle : _opticalCameraRectangles)
    {
      newOpticalCameraRectangleList.add(opticalCameraRectangle);
    }
    return newOpticalCameraRectangleList;
  }
  
  /**
   * @author Jack Hwee
   */
  public List<OpticalCameraRectangle> getEnabledOpticalCameraRectangles()
  {
    Assert.expect(_opticalCameraRectangles != null);

    List<OpticalCameraRectangle> newOpticalCameraRectangleList = new ArrayList<OpticalCameraRectangle>();
    for (OpticalCameraRectangle opticalCameraRectangle : _opticalCameraRectangles)
    {
      if (opticalCameraRectangle.getInspectableBool() == true)
        newOpticalCameraRectangleList.add(opticalCameraRectangle);
    }
    return newOpticalCameraRectangleList;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean hasOpticalCameraRectangles()
  {
    Assert.expect(_opticalCameraRectangles != null);
    return (_opticalCameraRectangles.isEmpty() == false);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean hasOpticalCameraRectangles(Board board)
  {
    Assert.expect(board != null);
    Assert.expect(_boardNameToOpticalCameraRectangleMap != null);
    if (_boardNameToOpticalCameraRectangleMap.containsKey(board))
    {
      if (_boardNameToOpticalCameraRectangleMap.get(board).isEmpty())
        return false;
      else
        return true;
    }
    return false;
  }

  /**
   * @author Cheah Lee Herng
   */
  public List<OpticalCameraRectangle> getOpticalCameraRectangles(Board board)
  {
    Assert.expect(board != null);
    Assert.expect(_boardNameToOpticalCameraRectangleMap != null);

    List<OpticalCameraRectangle> opticalCameraRectangles = new ArrayList<OpticalCameraRectangle>();
    if (_boardNameToOpticalCameraRectangleMap.containsKey(board))
    {
      opticalCameraRectangles.addAll(_boardNameToOpticalCameraRectangleMap.get(board));
    }
    return opticalCameraRectangles;
  }
  
   /**
   * @author Jack Hwee
   */
  public List<OpticalCameraRectangle> getEnabledBoardOpticalCameraRectangles(Board board)
  {
    Assert.expect(board != null);
    Assert.expect(_boardNameToOpticalCameraRectangleMap != null);

    List<OpticalCameraRectangle> newOpticalCameraRectangleList = new ArrayList<OpticalCameraRectangle>();
    if (_boardNameToOpticalCameraRectangleMap.isEmpty() == false)
    {
      if (_boardNameToOpticalCameraRectangleMap.containsKey(board))
      {
        for (OpticalCameraRectangle opticalCameraRectangle : _boardNameToOpticalCameraRectangleMap.get(board))
        {
          if (opticalCameraRectangle.getInspectableBool() == true)
            newOpticalCameraRectangleList.add(opticalCameraRectangle);
        }
      }
    }
    return newOpticalCameraRectangleList;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setToUsePspForAlignment(boolean setToUsePspForAlignment)
  {
    if (setToUsePspForAlignment == _setToUsePspForAlignment)
      return;

    boolean oldValue = _setToUsePspForAlignment;
    _projectObservable.setEnabled(false);

    _setToUsePspForAlignment = setToUsePspForAlignment;

    _projectObservable.setEnabled(true);

    _projectObservable.stateChanged(this, oldValue, setToUsePspForAlignment, PanelSurfaceMapEventEnum.SET_SURFACE_MAP_COORDINATE);
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean isSetToUsePspForAlignment()
  {
    return _setToUsePspForAlignment;
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void removeOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {
    Assert.expect(opticalCameraRectangle != null);
    _opticalCameraRectangles.remove(opticalCameraRectangle);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void removeOpticalCameraRectangles(List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    Assert.expect(opticalCameraRectangles != null);
    _opticalCameraRectangles.removeAll(opticalCameraRectangles);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void removeOpticalCameraRectangle(Board board, OpticalCameraRectangle opticalCameraRectangle)
  {
    _opticalCameraRectangles.remove(opticalCameraRectangle);
    java.util.List<OpticalCameraRectangle> opticalCameraRectangleList = _boardNameToOpticalCameraRectangleMap.get(board);
    opticalCameraRectangleList.remove(opticalCameraRectangle);
    _boardNameToOpticalCameraRectangleMap.put(board, opticalCameraRectangleList);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void removeOpticalCameraRectangles(Board board, List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    _opticalCameraRectangles.removeAll(opticalCameraRectangles);
    java.util.List<OpticalCameraRectangle> opticalCameraRectangleList = _boardNameToOpticalCameraRectangleMap.get(board);
    opticalCameraRectangleList.removeAll(opticalCameraRectangles);
    _boardNameToOpticalCameraRectangleMap.put(board, opticalCameraRectangleList);
  }

  /**
   * @author Ying-Huan.Chu
   */
  public void removeOpticalCameraRectangle(String opticalCameraRectangle)
  {
    OpticalCameraRectangle opticalCameraRectangleToBeDeleted = new OpticalCameraRectangle();
    for (int i = 0; i < _opticalCameraRectangles.size(); ++i)
    {
      if (opticalCameraRectangle.equals(_opticalCameraRectangles.get(i).getName()))
      {
        opticalCameraRectangleToBeDeleted = _opticalCameraRectangles.get(i);
      }
    }
    _opticalCameraRectangles.remove(opticalCameraRectangleToBeDeleted);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removeAllOpticalCameraRectangles()
  {
    Assert.expect(_opticalCameraRectangles != null);
    
    _opticalCameraRectangles.clear();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removeAllOpticalCameraRectangles(Board board)
  {
    Assert.expect(board != null);
    Assert.expect(_boardNameToOpticalCameraRectangleMap != null);
    Assert.expect(_opticalCameraRectangles != null);
    
    _boardNameToOpticalCameraRectangleMap.remove(board);
    _opticalCameraRectangles.removeAll(getOpticalCameraRectangles(board));
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void addMeshTriangle(MeshTriangle meshTriangle)
  {
    Assert.expect(meshTriangle != null);
    
    if (_meshTriangles.contains(meshTriangle) == false)
      _meshTriangles.add(meshTriangle);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public List<MeshTriangle> getMeshTriangles()
  {
    Assert.expect(_meshTriangles != null);
    return new ArrayList<MeshTriangle>(_meshTriangles);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public MeshTriangle getMeshTriangle(String meshTriangleName)
  {
    Assert.expect(meshTriangleName != null);
    
    for(MeshTriangle meshTriangle : _meshTriangles)
    {
      if (meshTriangle.getName().equals(meshTriangleName))
        return meshTriangle;
    }
    return null;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasMeshTriangle(String meshTriangleName)
  {
    Assert.expect(meshTriangleName != null);
    
    for(MeshTriangle meshTriangle : _meshTriangles)
    {
      if (meshTriangle.getName().equals(meshTriangleName))
        return true;
    }
    return false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public List<MeshTriangle> getMeshTriangles(String opticalCameraRectangleName)
  {
    Assert.expect(opticalCameraRectangleName != null);
    
    List<MeshTriangle> meshTriangles = new ArrayList<MeshTriangle>();
    for(MeshTriangle meshTriangle : _meshTriangles)
    {
      if (meshTriangle.hasOpticalCameraRectangle(opticalCameraRectangleName))
        meshTriangles.add(meshTriangle);
    }
    return meshTriangles;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public OpticalCameraRectangle getOpticalCameraRectangle(String opticalCameraRectangleName)
  {
    Assert.expect(opticalCameraRectangleName != null);
    
    OpticalCameraRectangle opticalCameraRectangle = null;
    for (OpticalCameraRectangle ocr : _opticalCameraRectangles)
    {
      if (ocr.getName().equals(opticalCameraRectangleName))
      {
        opticalCameraRectangle = ocr;
      }
    }
    return opticalCameraRectangle;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public OpticalCameraRectangle getOpticalCameraRectangle(Board board, String opticalCameraRectangleName)
  {
    Assert.expect(opticalCameraRectangleName != null);
    
    OpticalCameraRectangle opticalCameraRectangle = null;
    for (OpticalCameraRectangle ocr : _boardNameToOpticalCameraRectangleMap.get(board))
    {
      if (ocr.getName().equals(opticalCameraRectangleName))
      {
        opticalCameraRectangle = ocr;
      }
    }
    return opticalCameraRectangle;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public Map<Board, Map<String, PanelCoordinate>> getBoardNameToPointPanelCoordinateMap()
  {
    Assert.expect(_boardNameToPointPanelCoordinateMap != null);
    return _boardNameToPointPanelCoordinateMap;
  }
  
  /**
   * remove panel based optical region
   * @author Kok Chun, Tan
   */
  public void remove()
  {
    Project project = Project.getCurrentlyLoadedProject();
    
    project.getPanel().getPanelSurfaceMapSettings().removeSurfaceMapRegion(this);
    removeAllOpticalCameraRectangles();
    removeAllComponents();
    removeAllPanelCoordinates();
  }
  
  /**
   * remove board based optical region
   * @author Kok Chun, Tan
   */
  public void remove(Board board)
  {
    board.getBoardSurfaceMapSettings().removeSurfaceMapRegion(this);
    removeAllOpticalCameraRectangles(board);
    removeAllComponents(board);
    removeAllPanelCoordinates(board);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void clearAllMeshTriangles()
  {
    _meshTriangles.clear();
  }
}
