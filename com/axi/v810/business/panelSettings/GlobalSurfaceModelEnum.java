package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 *
 * @author Cheah, Lee Herng
 */
public class GlobalSurfaceModelEnum extends com.axi.util.Enum
{
    private static Map<String, GlobalSurfaceModelEnum> _stringToEnumMap = new HashMap<String,GlobalSurfaceModelEnum>();
    private String _name;
    private static int _index = -1;

    public static GlobalSurfaceModelEnum AUTOFOCUS = new GlobalSurfaceModelEnum(++_index, "AutoFocus");
    public static GlobalSurfaceModelEnum GLOBAL_SURFACE_MODEL = new GlobalSurfaceModelEnum(++_index, "GSM");
    public static GlobalSurfaceModelEnum AUTOSELECT = new GlobalSurfaceModelEnum(++_index, "AutoSelect");
    public static GlobalSurfaceModelEnum USE_RFB = new GlobalSurfaceModelEnum(++_index, "RFP");
    public static GlobalSurfaceModelEnum PSP = new GlobalSurfaceModelEnum(++_index, "SurfaceMap");

    //public static GlobalSurfaceModelEnum AUTOFOCUS = new GlobalSurfaceModelEnum(++_index, "AutoFocus");
    //public static GlobalSurfaceModelEnum GLOBAL_SURFACE_MODEL = new GlobalSurfaceModelEnum(++_index, "GSM");
    //public static GlobalSurfaceModelEnum AUTOSELECT = new GlobalSurfaceModelEnum(++_index, "AutoSelect");

    /**
     * @author Cheah, Lee Herng
     */
    private GlobalSurfaceModelEnum(int id, String name)
    {
        super(id);
        _name = name;

        _stringToEnumMap.put(name, this);
    }

    /**
     * @author Cheah, Lee Herng
     */
    public String toString()
    {
        return _name;
    }
    
  /**
   * @author Cheah Lee Herng
   */
  public static GlobalSurfaceModelEnum getGlobalSurfaceModelEnum(String key)
  {
    Assert.expect(key != null);

    return _stringToEnumMap.get(key);
  }
}
