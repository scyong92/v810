package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * @author Cheah Lee Herng
 */
public class ComponentUserGainComparator implements Comparator<Component>  
{
    private boolean _ascending;
    private static AlphaNumericComparator _alphaNumericComparator = new AlphaNumericComparator();
    
    /**
     * @author Cheah Lee Herng
     */
    public ComponentUserGainComparator(boolean ascending)
    {        
        _ascending = ascending;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public ComponentUserGainComparator()
    {
        _ascending = true;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public int compare(Component lhs, Component rhs) 
    {
        Assert.expect( lhs != null );
        Assert.expect( rhs != null );
        
        ComponentType lhsComponentType = lhs.getComponentType();
        ComponentType rhsComponentType = rhs.getComponentType();
        
        if (_ascending)
            return _alphaNumericComparator.compare(lhsComponentType.getUserGain().getId(), rhsComponentType.getUserGain().getId());
        else
            return _alphaNumericComparator.compare(rhsComponentType.getUserGain().getId(), lhsComponentType.getUserGain().getId());
    }
}
