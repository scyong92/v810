package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.business.panelDesc.*;

/**
 * <pre>
 * This class knows how to find out the user defined sliceheight for any given slice.
 *
 * As things are implemented now, any code that needs to find out the user defined sliceheight *could* just call
 * subtype.getAlgorithmSettingValue() with the appropriate AlgorithmSettingEnum.  However, since that is needed in
 * multiple places, I decided to centralize the mapping between slice and the associated setting here in this one class.
 *
 * The User Defined Sliceheights still does have some coupling between subsystems, so whenever you add or edit a slice,
 * you need to do the following:
 *
 * 1)  Make sure you have an AlgorithmSetting for the slice.  It should either be MeasurementUnitsEnum.Millimeters or
 *       MeasurementsUnitsEnum.PERCENT_THROUGHHOLE_FROM_BOTTOM.
 * 2)  Make sure that the slice appears in the internal map for this UserDefinedSliceheights class and has the proper
 *      algorithm setting associated with it.
 * 3)  Make sure ProgramGeneration asks this class for the user defined sliceheight offset when it is creating the
 *      focus instructions for the slice.
 *
 * </pre>
 *
 * @author Peter Esbensen
 */
public class UserDefinedSliceheights
{
  private static Map<SliceNameEnum, AlgorithmSettingEnum> _sliceToSettingMap = new HashMap<SliceNameEnum, AlgorithmSettingEnum>();

  static
  {
    // build the mapping of slice enum to algorithm setting enum
    assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT);
    assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE, AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT);
    assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_PIN_SIDE, AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT);
    assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_2, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_2);
    assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_3, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_3);
    assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_4, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_4);
    assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_5, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_5);
    assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_6, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_6);
    assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_7, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_7);
    assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_8, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_8);
    assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_9, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_9);
    assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_10, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_10);    
    assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_PROTRUSION, AlgorithmSettingEnum.THROUGHHOLE_PROTRUSION_SLICEHEIGHT);
    assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_INSERTION, AlgorithmSettingEnum.THROUGHHOLE_INSERTION_SLICEHEIGHT);
    assignSliceToSettingMap(SliceNameEnum.SURFACE_MODELING, AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT);
    assignSliceToSettingMap(SliceNameEnum.HIGH_SHORT, AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_SLICEHEIGHT);
    assignSliceToSettingMap(SliceNameEnum.PAD, AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT);
    assignSliceToSettingMap(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL, AlgorithmSettingEnum.USER_DEFINED_BGA_LOWERPAD_SLICEHEIGHT);
    assignSliceToSettingMap(SliceNameEnum.PACKAGE, AlgorithmSettingEnum.USER_DEFINED_PACKAGE_SLICEHEIGHT);
    assignSliceToSettingMap(SliceNameEnum.MIDBALL, AlgorithmSettingEnum.USER_DEFINED_MIDBALL_SLICEHEIGHT);
    assignSliceToSettingMap(SliceNameEnum.PCAP_SLUG, AlgorithmSettingEnum.USER_DEFINED_PCAP_SLUG_SLICEHEIGHT);
    assignSliceToSettingMap(SliceNameEnum.OPAQUE_CHIP_PAD, AlgorithmSettingEnum.USER_DEFINED_OPAQUE_CHIP_SLICEHEIGHT);
    assignSliceToSettingMap(SliceNameEnum.CLEAR_CHIP_PAD, AlgorithmSettingEnum.USER_DEFINED_CLEAR_CHIP_SLICEHEIGHT);
    //_sliceToSettingMap.put(SliceNameEnum.THROUGHHOLE_BARREL, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_MILS);
    assignSliceToSettingMap(SliceNameEnum.MID_BOARD_SLICE, AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET);
    assignSliceToSettingMap(SliceNameEnum.RFP_SLICE, AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    assignSliceToSettingMap(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20, AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    assignSliceToSettingMap(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1, AlgorithmSettingEnum.LARGE_PAD_USER_DEFINED_SLICE_1);
    assignSliceToSettingMap(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2, AlgorithmSettingEnum.LARGE_PAD_USER_DEFINED_SLICE_2);
    // Chnee Khang Wah, 2013-09-20, user defined initial wavelet level
    assignSliceToSettingMap(SliceNameEnum.USER_DEFINED_WAVELET_LEVEL, AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL);
    // Cheah Lee Herng, 2014-08-19, PSP Search Range Low / High Limit
    assignSliceToSettingMap(SliceNameEnum.PSP_SEARCH_RANGE_LOW_LIMIT, AlgorithmSettingEnum.PSP_SEARCH_RANGE_LOW_LIMIT);
    assignSliceToSettingMap(SliceNameEnum.PSP_SEARCH_RANGE_HIGH_LIMIT, AlgorithmSettingEnum.PSP_SEARCH_RANGE_HIGH_LIMIT);
    // XCR-3603 PSP Z-Offset
    assignSliceToSettingMap(SliceNameEnum.PSP_Z_OFFSET, AlgorithmSettingEnum.PSP_Z_OFFSET);
  }

  /**
   * must be unique key!
   * @param sliceNameEnum
   * @param agorithmSettingEnum
   * @author Wei Chin
   */
  private static void assignSliceToSettingMap(SliceNameEnum sliceNameEnum, AlgorithmSettingEnum agorithmSettingEnum)
  {
    Object prev = null;
    prev = _sliceToSettingMap.put(sliceNameEnum, agorithmSettingEnum);
    Assert.expect(prev == null);
  }

  /**
   * must be unique key!
   * @param sliceNameEnum
   * @param agorithmSettingEnum
   * @author Wei Chin
   */
  private static void removeSliceFromSliceToSettingMap(SliceNameEnum sliceNameEnum)
  {
    Object prev = null;
    prev = _sliceToSettingMap.remove(sliceNameEnum);
    Assert.expect(prev != null);
  }

  /**
   * @return the offset in nanometers.  This offset is relative to the board side, with numbers getting bigger as you
   * move further above the board surface.  Negative numbers are below the board's surface.
   *
   * @author Peter Esbensen
   */
  public synchronized static int getOffsetInNanometers(SliceNameEnum sliceNameEnum, Subtype subtype)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(_sliceToSettingMap != null);

    // get the algorithm setting associated with this slice
    AlgorithmSettingEnum algorithmSettingEnum = _sliceToSettingMap.get( sliceNameEnum );
    Assert.expect(algorithmSettingEnum != null);

    // do a sanity check on the units of the setting
    MeasurementUnitsEnum measurementUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(algorithmSettingEnum);
    Assert.expect(measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS));

    // get the setting value
    float userOffsetInMM = (Float)subtype.getAlgorithmSettingValue(algorithmSettingEnum);

    return Math.round(MathUtil.convertMillimetersToNanometers(userOffsetInMM));
  }

  public synchronized static AlgorithmSettingEnum getAlgoSettingsEnum(SliceNameEnum sliceNameEnum)
  {
     Assert.expect(sliceNameEnum != null);
     Assert.expect(_sliceToSettingMap != null);

     return  _sliceToSettingMap.get( sliceNameEnum );
  }

  /**
   * @author Cheah Lee Herng
   */
  public synchronized static int getAutoFocusMidBoardOffsetInNanometers(Subtype subtype)
  {
    Assert.expect(subtype != null);
    Assert.expect(_sliceToSettingMap != null);

    // do a sanity check on the units of the setting
    MeasurementUnitsEnum measurementUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET);
    Assert.expect(measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS));

    // get the setting value
    float autoFocusMidBoardOffsetInMM = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET);

    return Math.round(MathUtil.convertMillimetersToNanometers(autoFocusMidBoardOffsetInMM));
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized static int getSearchRangeLowLimitInNanometers(Subtype subtype)
  {
    Assert.expect(subtype != null);
    Assert.expect(_sliceToSettingMap != null);
    
    float searchRangeLowLimitInMM = MathUtil.convertMilsToMillimeters(-20); // Default to -20mils
    
    // XCR-2819 Cheah Lee Herng - Exclude PSP Search Range ? Low Limit and High Limit for PTH and pressfit
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.PSP_SEARCH_RANGE_LOW_LIMIT))
    {
      // do a sanity check on the units of the setting
      MeasurementUnitsEnum measurementUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(AlgorithmSettingEnum.PSP_SEARCH_RANGE_LOW_LIMIT);
      Assert.expect(measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS));

      // get the setting value
      searchRangeLowLimitInMM = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PSP_SEARCH_RANGE_LOW_LIMIT);
    }
    
    return Math.round(MathUtil.convertMillimetersToNanometers(searchRangeLowLimitInMM));
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized static int getSearchRangeHighLimitInNanometers(Subtype subtype)
  {
    Assert.expect(subtype != null);
    Assert.expect(_sliceToSettingMap != null);

    float searchRangeHighLimitInMM = MathUtil.convertMilsToMillimeters(-20); // Default to -20mils
    
    // XCR-2819 Cheah Lee Herng - Exclude PSP Search Range ? Low Limit and High Limit for PTH and pressfit
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.PSP_SEARCH_RANGE_HIGH_LIMIT))
    {
      // do a sanity check on the units of the setting
      MeasurementUnitsEnum measurementUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(AlgorithmSettingEnum.PSP_SEARCH_RANGE_HIGH_LIMIT);
      Assert.expect(measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS));

      // get the setting value
      searchRangeHighLimitInMM = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PSP_SEARCH_RANGE_HIGH_LIMIT);
    }

    return Math.round(MathUtil.convertMillimetersToNanometers(searchRangeHighLimitInMM));
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized static int getPspZOffsetInNanometers(Subtype subtype)
  {
    Assert.expect(subtype != null);
    Assert.expect(_sliceToSettingMap != null);

    float pspZOffsetInMM = MathUtil.convertMilsToMillimeters(0); // Default to 0mils
    
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.PSP_Z_OFFSET))
    {
      // do a sanity check on the units of the setting
      MeasurementUnitsEnum measurementUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(AlgorithmSettingEnum.PSP_Z_OFFSET);
      Assert.expect(measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS));

      // get the setting value
      pspZOffsetInMM = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PSP_Z_OFFSET);
    }

    return Math.round(MathUtil.convertMillimetersToNanometers(pspZOffsetInMM));
  }

  /**
   * @return the sliceheight as a percent of the barrel height.  Note that for ThroughHole, 0% should be considered
   * to be at the Pin Side, but for Pressfit, 0% would be at the Component Side.
   *
   * @todo PE is that comment about pressfit correct?
   *
   * @author Peter Esbensen
   */
  public synchronized static float getHeightAsPercentOfBarrel(SliceNameEnum sliceNameEnum, Subtype subtype)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(_sliceToSettingMap != null);
    
    // get the algorithm setting associated with this slice
    AlgorithmSettingEnum algorithmSettingEnum = _sliceToSettingMap.get(sliceNameEnum);
    //System.out.println(algorithmSettingEnum + "<<<>>>>" + sliceNameEnum);
    Assert.expect(algorithmSettingEnum != null);

    // do a sanity check on the units of the setting
    MeasurementUnitsEnum measurementUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(algorithmSettingEnum);
    Assert.expect(measurementUnitsEnum.equals(MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM));

    // get the setting value
    float userSliceheightAsPercentOfBarrel = (Float)subtype.getAlgorithmSettingValue(algorithmSettingEnum);

    return userSliceheightAsPercentOfBarrel;
  }
  
  /*
   * @author Kee Chin Seong
   * This feature is only for result process only
   */
  public synchronized static String getSliceNameForResult(SliceNameEnum sliceNameEnum, Subtype subtype)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(_sliceToSettingMap != null);

     // get the algorithm setting associated with this slice
    AlgorithmSettingEnum algorithmSettingEnum = _sliceToSettingMap.get(sliceNameEnum);
    Assert.expect(algorithmSettingEnum != null);
 
    String mesurementUnit = null;
   
    if(isSliceWorkingUnitInThickness(subtype) == false)
      mesurementUnit = "percent";
    else
      mesurementUnit = "mils";
   
    String resultSliceName = sliceNameEnum.getName() + "_" + Float.toString((Float)subtype.getAlgorithmSettingValue(algorithmSettingEnum)) +
                             mesurementUnit;
 
    return resultSliceName;
  }

  public static boolean isSliceWorkingUnitInThickness(Subtype subtype)
  {
    // get the setting value
    changeSliceHeightUnit(subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SLICEHEIGHT_WORKING_UNIT).toString().matches("Thickness"));
    return subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SLICEHEIGHT_WORKING_UNIT).toString().matches("Thickness");
  }

  /**
   * @param isSliceWorkingUnitIsInThickness
   */
  public synchronized static void changeSliceHeightUnit(boolean isSliceWorkingUnitIsInThickness)
  {
    removeSliceFromSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL);
    removeSliceFromSliceToSettingMap(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE);
    removeSliceFromSliceToSettingMap(SliceNameEnum.THROUGHHOLE_PIN_SIDE);
    removeSliceFromSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_2);
    removeSliceFromSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_3);
    removeSliceFromSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_4);
    removeSliceFromSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_5);
    removeSliceFromSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_6);
    removeSliceFromSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_7);
    removeSliceFromSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_8);
    removeSliceFromSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_9);
    removeSliceFromSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_10);
     //.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT
    if(isSliceWorkingUnitIsInThickness == false)
    {
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE, AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_PIN_SIDE, AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_2, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_2);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_3, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_3);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_4, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_4);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_5, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_5);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_6, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_6);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_7, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_7);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_8, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_8);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_9, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_9);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_10, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_10);
    }
    else
    {
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE, AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT_IN_THICKNESS);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_PIN_SIDE, AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT_IN_THICKNESS);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_2, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_2);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_3, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_3);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_4, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_4);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_5, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_5);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_6, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_6);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_7, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_7);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_8, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_8);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_9, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_9);
      assignSliceToSettingMap(SliceNameEnum.THROUGHHOLE_BARREL_10, AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_10);
    }
  }


  /**
   * ONLY FOR UNIT TESTING
   *
   * @author Peter Esbensen
   */
  static synchronized boolean internalMapContainsForUnitTesting(SliceNameEnum sliceNameEnum)
  {
    Assert.expect( UnitTest.unitTesting() );
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_sliceToSettingMap != null);

    return _sliceToSettingMap.containsKey(sliceNameEnum);
  }

  /**
   * @return all the AlgorithmSettingEnums that are for User Defined Sliceheights
   */
  static synchronized Collection<AlgorithmSettingEnum> getAlgorithmSettingEnums()
  {
    Assert.expect(_sliceToSettingMap != null);
    return _sliceToSettingMap.values();
  }

  /**
   * ONLY FOR UNIT TESTING
   *
   * @author Peter Esbensen
   */
  static synchronized AlgorithmSettingEnum getAlgorithmSettingEnumForUnitTesting(SliceNameEnum sliceNameEnum)
  {
    Assert.expect( UnitTest.unitTesting() );

    return _sliceToSettingMap.get(sliceNameEnum);
  }

  /**
   * @author Peter Esbensen
   * @author Ngie Xing
   * XCR1792 - Able to Copy PTH and PRESSFIT Barrel Slices Number
   */
  static synchronized public Collection<AlgorithmSettingEnum> getUserDefinedSliceheightSettings(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

    // get the set of algorithm settings in the inspection family
    //Ngie Xing, modified to include all settings from Slice Height 
    //Set< AlgorithmSettingEnum > userDefinedSliceheightSettingsInInspectionFamily = new HashSet<AlgorithmSettingEnum>();
    Set< AlgorithmSettingEnum > inspectionFamilyAlgoSettings = new HashSet<AlgorithmSettingEnum>();
    for (Algorithm algorithm : inspectionFamily.getAlgorithms())
    {
      // We could just ask the algorithm for ALL its algorithm setting enums, but some are not applicable to all joint types.
      // Instead, by calling getAlgorithmSettings and passing in the joint type, we get a list with the inapplicable settings filtered out.
      for (AlgorithmSetting algorithmSetting : algorithm.getAlgorithmSettings(jointTypeEnum))
      {
        //Ngie Xing add
        if(algorithmSetting.getAlgorithmSettingTypeEnum(algorithm.getVersion()) == AlgorithmSettingTypeEnum.SLICE_HEIGHT
              || algorithmSetting.getAlgorithmSettingTypeEnum(algorithm.getVersion()) == AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL)
          inspectionFamilyAlgoSettings.add(algorithmSetting.getAlgorithmSettingEnum());
      }
    }

//    // get ALL the user defined sliceheight settings that exist on the system (as defined in this class)
//    Set< AlgorithmSettingEnum > userDefinedSliceheightSettings = new HashSet<AlgorithmSettingEnum>();
//    userDefinedSliceheightSettings.addAll( _sliceToSettingMap.values() );
//
//    // now we can get the algorithm settings in the inspection family that are user defined sliceheight settings
//    Set< AlgorithmSettingEnum > userDefinedSliceheightSettingsInInspectionFamily = new HashSet<AlgorithmSettingEnum>(inspectionFamilyAlgoSettings);
//    userDefinedSliceheightSettingsInInspectionFamily.retainAll(userDefinedSliceheightSettings);
//
//    return userDefinedSliceheightSettingsInInspectionFamily;
   
    return inspectionFamilyAlgoSettings;
  }
}
