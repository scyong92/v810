package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;

/**
 * Manages the learned expected image information for each subtype.  The expected image is saved
 * as an array of pixels extracted from the image. The expected image is:
 * 1)  a composite of the darkest pixels across a number of images (MAXIMUM_THICKNESS).
 * 2)  a composite of the lightest pixels across a number of images (MINIMUM_THICKNESS).
 * 1)  a mask of the solder thresholds across a number of images (THRESHOLD_MASK).
 *
 * @author George Booth
 */
public class ExpectedImageLearning
{
  private static float _uninitializedValue = -1f;
  private static float CONTRIBUTOR_PIXEL = -1.0f;

  private Pad _pad;
  private SliceNameEnum _sliceNameEnum;

  private int _imageWidth = 0;
  private int _imageHeight = 0;
  private int _numberOfExpectedImagesLearned = 0;
  private float[] _compositeImagePixelArray = null;
  private float _numberOfTestedPixels = _uninitializedValue;
  private VoidingTechniqueEnum _voidingTechniqueEnum = VoidingTechniqueEnum.MAXIMUM_THICKNESS; //default
  private float _solderThicknessThreshold = 0.0f;

  /**
   * @author George Booth
   */
  public ExpectedImageLearning(int width, int height)
  {
    Assert.expect(width > 0);
    Assert.expect(height > 0);

    _imageWidth = width;
    _imageHeight = height;
  }

  /**
   * @author George Booth
   */
  public static float getUninitializedValue()
  {
    return _uninitializedValue;
  }

  /**
   * @author George Booth
   */
  public static float getContributorPixelValue()
  {
    return CONTRIBUTOR_PIXEL;
  }

  /**
   * @author George Booth
   */
  public void setPad(Pad pad)
  {
    Assert.expect(pad != null);
    _pad = pad;
  }

  /**
   * @author George Booth
   */
  public Pad getPad()
  {
    Assert.expect(_pad != null);
    return _pad;
  }

  /**
   * @author George Booth
   */
  public void setSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    _sliceNameEnum = sliceNameEnum;
  }

  /**
   * @author George Booth
   */
  public SliceNameEnum getSliceNameEnum()
  {
    Assert.expect(_sliceNameEnum != null);
    return _sliceNameEnum;
  }

  /**
   * @author George Booth
   */
  public int getImageWidth()
  {
    return _imageWidth;
  }

  /**
   * @author George Booth
   */
  public int getImageHeight()
  {
    return _imageHeight;
  }

  /**
   * @author George Booth
   */
  public void setVoidingTechniqueEnum(VoidingTechniqueEnum voidingTechniqueEnum)
  {
    Assert.expect(voidingTechniqueEnum != null);
    _voidingTechniqueEnum = voidingTechniqueEnum;
  }

  /**
   * @author George Booth
   */
  public VoidingTechniqueEnum getVoidingTechniqueEnum()
  {
    Assert.expect(_voidingTechniqueEnum != null);
    return _voidingTechniqueEnum;
  }

  /**
   * @author George Booth
   */
  public void setSolderThicknessThreshold(float solderThicknessThreshold)
  {
    _solderThicknessThreshold = solderThicknessThreshold;
  }

  /**
   * @author George Booth
   */
  public float getSolderThicknessThreshold()
  {
    return _solderThicknessThreshold;
  }

  /**
   * @author George Booth
   */
  public void setNumberOfExpectedImagesLearned(int numberOfExpectedImagesLearned)
  {
    Assert.expect(numberOfExpectedImagesLearned > 0);
    _numberOfExpectedImagesLearned = numberOfExpectedImagesLearned;
  }

  /**
   * @author George Booth
   */
  public int getNumberOfExpectedImagesLearned()
  {
    return _numberOfExpectedImagesLearned;
  }

  /**
   * @author George Booth
   */
  public void setCompositeImageArray(float[] compositeImagePixelArray)
  {
    Assert.expect(compositeImagePixelArray != null);

    _compositeImagePixelArray = compositeImagePixelArray;
  }

  /**
   * @author George Booth
   */
  public boolean hasCompositeExpectedImageArray()
  {
    return _compositeImagePixelArray != null;
  }

  /**
   * @author George Booth
   */
  public float[] getCompositeImageArray()
  {
    Assert.expect(_compositeImagePixelArray != null);

    return _compositeImagePixelArray;
  }

  /**
   * Clears out the pre-existing composite expected image and sets the learned count back to zero.
   * Updates width and length to new values.
   *
   * @author George Booth
   */
  public void clearCompositeExpectedImageArray(int newWidth, int newHeight)
  {
    Assert.expect(_compositeImagePixelArray != null);
    Assert.expect(newWidth > 0);
    Assert.expect(newHeight > 0);

    _compositeImagePixelArray = null;
    _numberOfExpectedImagesLearned = 0;
    _numberOfTestedPixels = _uninitializedValue;
    _imageWidth = newWidth;
    _imageHeight = newHeight;
  }

  /**
   * @author George Booth
   */
  public void setNumberOfTestedPixels(float numberOfTestedPixels)
  {
    Assert.expect(numberOfTestedPixels >= 0.0f);

    _numberOfTestedPixels = numberOfTestedPixels;
  }

  /**
   * @author George Booth
   */
  public boolean hasNumberOfTestedPixels()
  {
    return (_numberOfTestedPixels != _uninitializedValue);
  }

  /**
   * @author George Booth
   */
  public float getNumberOfTestedPixels()
  {
    Assert.expect(_numberOfTestedPixels != _uninitializedValue);

    return _numberOfTestedPixels;
  }

  /**
   * Adds the specified expected image to our composite expected image.
   *
   * @author George Booth
   */
  public void addLearnedExpectedImagePixelArray(int width, int height, float[] expectedImagePixelArray)
  {
    // NOTE: _projectObservable.stateChanged() does not need to be called here - a higher level
    //       call will get the event out

    Assert.expect(expectedImagePixelArray != null);
    Assert.expect(width == _imageWidth);
    Assert.expect(height == _imageHeight);
    Assert.expect(expectedImagePixelArray.length == _imageWidth * _imageHeight);

    makeCompositeExpectedImageArray(expectedImagePixelArray);
  }

  /**
   * Updates the composite image array with the specified candidate image array by
   * selecting the thickest pixel from either.  Also keeps track of the number of
   * tested (non-zero) pixels.
   *
   * @author George Booth
   */
  private void makeCompositeExpectedImageArray(float[] candidatePixelArray)
  {
    // is this the first merged image?
    boolean firstImage = false;
    if (_numberOfExpectedImagesLearned == 0)
    {
      firstImage = true;
      _compositeImagePixelArray = new float[_imageWidth * _imageHeight];
    }

    // recalculate the number of tested (non-zero) pixels
    _numberOfTestedPixels = 0;

    // do fast gets and sets via pointer math
    float pixelValue = 0.0f;
    int pixelIndex = 0;
    for (int y = 0; y < _imageHeight; ++y)
    {
      for (int x = 0; x < _imageWidth; ++x)
      {
//        pixelIndex = y * _imageWidth + x;
        // just use candidate pixel value for first image
        if (firstImage)
        {
          pixelValue = candidatePixelArray[pixelIndex];
        }
        else
        {
          float compositeThickness = _compositeImagePixelArray[pixelIndex];
          float candidateThickness = candidatePixelArray[pixelIndex];
          pixelValue = selectValueToMerge(compositeThickness, candidateThickness);
        }
        _compositeImagePixelArray[pixelIndex] = pixelValue;
        // update the number of tested (non-zero) pixels
        if (pixelValue > 0.0f)
        {
          _numberOfTestedPixels++;
        }
        pixelIndex++;
      }
    }

    _numberOfExpectedImagesLearned++;
  }

  /**
   * Select a value to merge based on voiding technique in effect
   *
   * @author George Booth
   */
  private float selectValueToMerge(float compositeThickness, float candidateThickness)
  {
    float pixelValue = 0.0f;
    if (_voidingTechniqueEnum.equals(VoidingTechniqueEnum.MAXIMUM_THICKNESS))
    {
      // merge by selecting the pixel with the most solder (highest value)
      pixelValue = Math.max(compositeThickness, candidateThickness);
    }
    else if (_voidingTechniqueEnum.equals(VoidingTechniqueEnum.MINIMUM_THICKNESS))
    {
      // if both composite and candidate are > 0, merge by selecting the pixel with the least solder (lowest value)
      // otherwise use the most solder (0 or either value)
      if (compositeThickness > 0.0f && candidateThickness > 0.0f)
      {
        pixelValue = Math.min(compositeThickness, candidateThickness);
      }
      else
      {
        pixelValue = Math.max(compositeThickness, candidateThickness);
      }
    }
    else if (_voidingTechniqueEnum.equals(VoidingTechniqueEnum.THRESHOLD_MASK))
    {
      // if either composite or candidate are > 0, merge by using the Solder Thickness threshhold
      // otherwise use 0
      if (compositeThickness > 0.0f || candidateThickness > 0.0f)
      {
        pixelValue = _solderThicknessThreshold;
      }
      else
        pixelValue = 0.0f;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected Voiding Technique setting: " + _voidingTechniqueEnum);
    }
    return pixelValue;
  }

  /**
   * Creates a trail version of the updated composite image array for use with Interactive Learning. The returned
   * image has pixels flagged to indicate the contributions of the candidate pixel to the composite image.
   *
   * @author George Booth
   */
  public float[] createExpectedImageArray(int width, int height, float[] candidatePixelArray)
  {
    Assert.expect(candidatePixelArray != null);
    Assert.expect(width == _imageWidth);
    Assert.expect(height == _imageHeight);
    Assert.expect(candidatePixelArray.length == _imageWidth * _imageHeight);

    float[] tempCompositeImagePixelArray = new float[_imageWidth * _imageHeight];

    // is this the first merged image?
    boolean firstImage = false;
    if (_numberOfExpectedImagesLearned == 0)
    {
      firstImage = true;
    }
    // make a copy of the current composite pixel array
    int pixelIndex = 0;
    for (int y = 0; y < _imageHeight; ++y)
    {
      for (int x = 0; x < _imageWidth; ++x)
      {
  //        pixelIndex = y * _imageWidth + x;
        if (firstImage)
        {
          tempCompositeImagePixelArray[pixelIndex] = 0.0f;
        }
        else
        {
          tempCompositeImagePixelArray[pixelIndex] = _compositeImagePixelArray[pixelIndex];
        }
        pixelIndex++;
      }
    }

    // do fast gets and sets via pointer math
    float pixelValue = 0.0f;
    pixelIndex = 0;
    for (int y = 0; y < _imageHeight; ++y)
    {
      for (int x = 0; x < _imageWidth; ++x)
      {
//        pixelIndex = y * _imageWidth + x;
        // just use candidate pixel value for first image
        if (firstImage)
        {
          pixelValue = candidatePixelArray[pixelIndex];
          if (pixelValue > 0.0f) // non-black
          {
            pixelValue = CONTRIBUTOR_PIXEL;
          }
        }
        else
        {
          float compositeThickness = tempCompositeImagePixelArray[pixelIndex];
          float candidateThickness = candidatePixelArray[pixelIndex];
          pixelValue = selectValueToMerge(compositeThickness, candidateThickness);
          // highlight pixels that have filled in background (black) regions
          if (compositeThickness == 0.0f && candidateThickness > compositeThickness)
          {
            pixelValue = CONTRIBUTOR_PIXEL;
          }
        }
        tempCompositeImagePixelArray[pixelIndex] = pixelValue;
        pixelIndex++;
      }
    }
    return tempCompositeImagePixelArray;
  }

}
