package com.axi.v810.business.panelSettings;

import java.awt.geom.*;

import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * Surface Model Logger for debugging
 * @author siew-yeng.phang
 */
public class SurfaceModelInfoLogger 
{
  private static SurfaceModelInfoLogger _surfaceModelInfoLog;
  private FileLoggerAxi _surfaceModelLogger;
  
  private SurfaceModelInfoLogger()
  {
    //do nothing
  }
  
  /**
   * @author Siew Yeng
   */
  public synchronized static SurfaceModelInfoLogger getInstance()
  {
    if (_surfaceModelInfoLog == null)
    {
      _surfaceModelInfoLog = new SurfaceModelInfoLogger();
    }

    return _surfaceModelInfoLog;
  }
  
  /**
   * @author Siew Yeng
   */
  public void open(String projectName)
  {
    _surfaceModelLogger = new FileLoggerAxi(FileName.getSurfaceModelInfoLogFullPath(projectName, Directory.getDirName(System.currentTimeMillis())));   
  }
  
  /**
   * @author Siew Yeng
   */
  public void logAddPointInfo(boolean topSide, ReconstructionRegion region, JointTypeEnum jointType, Point2D point, int zHeight, boolean isFocusConfirmation)
  {
    String side = topSide? "TOP" : "BOTTOM";
    String fcOrSm = isFocusConfirmation ? "FocusConfirmation" : "SurfaceModel";
    
    writeDebugLog("addPoint-"+ fcOrSm +"::" + region.getComponent().getBoardNameAndReferenceDesignator() + 
                  ",regionId: " + region.getRegionId() +
                  ",side: "+ side +
                  ",jointType: "+ jointType.getName() + 
                  ",point: (" + Math.round(point.getX()) + "," + Math.round(point.getY()) + ")" +
                  ",zHeight: " + zHeight);
  }
  
  /**
   * @author Siew Yeng
   */
  public void logNeighborInfo(boolean topSide, ReconstructionRegion region, int distanceThresholdInMils, Point2D point, int zHeight)
  {
    String side = topSide? "TOP" : "BOTTOM";
    
    writeDebugLog("  neighbor::" + region.getComponent().getBoardNameAndReferenceDesignator() + 
                  ",regionId: " + region.getRegionId() +
                  ",side: "+ side +
                  ",jointType: "+ region.getJointTypeEnum().getName() + 
                  ",distanceThreshold: " + distanceThresholdInMils + "mils " +
                  ",point: (" + Math.round(point.getX()) + "," + Math.round(point.getY()) + ")" +
                  ",zHeight: "+ zHeight);
  }
  
  /**
   * @author Siew Yeng
   */
  public void logSelectiveNeighborInfo(boolean topSide, ReconstructionRegion region, int zHeight)
  {
    String side = topSide? "TOP" : "BOTTOM";
    
    writeDebugLog("  selectiveNeighbor::" + region.getComponent().getBoardNameAndReferenceDesignator() +
                  ",regionId: " + region.getRegionId() +
                  ",side: "+ side +
                  ",jointType: "+ region.getJointTypeEnum().getName() +
                  ",zHeight: "+ zHeight);
  }
  
  /**
   * @author Siew Yeng
   */
  public void logEstimatedZHeightInfo(boolean topSide, ReconstructionRegion region, int distanceThresholdInMils, Point2D point, 
                                      int estimatedBoardThickness, int cadBoardThickness, int estimatedZHeight)
  {
    String side = topSide? "TOP" : "BOTTOM";
    
    writeDebugLog("    pshComponent::"+ region.getComponent().getBoardNameAndReferenceDesignator() +
                  ",regionId: " + region.getRegionId() + 
                  ",side: "+ side +
                  ",jointType: "+ region.getJointTypeEnum().getName() +
                  ",distanceThreshold: " + distanceThresholdInMils + "mils" +
                  ",point: (" + Math.round(point.getX()) + "," + Math.round(point.getY()) + ")" +
                  ",estimatedboardThickness: " + estimatedBoardThickness +
                  ",cadboardThickness: " + cadBoardThickness +
                  ",estimatedZHeight: " + estimatedZHeight);
  }
  
  /**
   * @author Siew Yeng
   */
  public void logSelectiveBoardSideEstimatedZHeightInfo(boolean topSide, ReconstructionRegion region, int distanceThresholdInMils, Point2D point, 
                                      int estimatedBoardThickness, int cadBoardThickness, int estimatedZHeight)
  {
    String side = topSide? "TOP" : "BOTTOM";
    
    writeDebugLog("    pshComponentSelectiveBoardSide::"+ region.getComponent().getBoardNameAndReferenceDesignator() +
                  ",regionId: " + region.getRegionId() + 
                  ",side: "+ side +
                  ",jointType: "+ region.getJointTypeEnum().getName() +
                  ",distanceThreshold: " + distanceThresholdInMils + "mils" +
                  ",point: (" + Math.round(point.getX()) + "," + Math.round(point.getY()) + ")" +
                  ",estimatedboardThickness: " + estimatedBoardThickness +
                  ",cadboardThickness: " + cadBoardThickness +
                  ",estimatedZHeight: " + estimatedZHeight);
  }
  
  /**
   * @author Siew Yeng
   */
  public void logSelectiveNeigborEstimatedZHeightInfo(boolean topSide, ReconstructionRegion region,int estimatedBoardThickness, 
                                                      int cadBoardThickness, int estimatedZHeight)
  {
    String side = topSide? "TOP" : "BOTTOM";
    
    writeDebugLog("    pshComponentSelectiveNeighbour::"+ region.getComponent().getBoardNameAndReferenceDesignator() + 
                  ",regionId: " + region.getRegionId() + 
                  ",side: "+ side +
                  ",jointType: "+ region.getJointTypeEnum().getName() +
                  ",estimatedboardThickness: " + estimatedBoardThickness +
                  ",cadboardThickness: " + cadBoardThickness +
                  ",estimatedZHeight: " + estimatedZHeight);
  }
  
  /**
   * @author Siew Yeng
   */
  private void writeDebugLog(String log)
  {
    try 
    {
      _surfaceModelLogger.appendWithoutDateTime(log);
    } 
    catch (DatastoreException ex) 
    {
       System.out.println("SurfaceModelLoggerException: "+ ex.getMessage());
    }
  }
}