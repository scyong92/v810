package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Cheah Lee Herng
 */
public class PanelSurfaceMapEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;
  public static PanelSurfaceMapEventEnum CREATE_OR_DESTROY = new PanelSurfaceMapEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static PanelSurfaceMapEventEnum PANEL = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum USER_COMMENT = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum ADD_SURFACE_MAP_POINTS = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum ADD_SURFACE_MAP_VERIFIED = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum REMOVE_SURFACE_MAP_POINTS = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum REMOVE_SURFACE_MAP_VERIFIED = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum SET_SURFACE_MAP_COORDINATE = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum ADD_SURFACE_MAP_OPTICAL_REGION = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum ADD_OPTICAL_REGION = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum REMOVE_SURFACE_MAP_REGION = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum REMOVE_SURFACE_MAP_COMPONENT = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum ADD_SURFACE_MAP_COMPONENT = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum ADD_SURFACE_MAP_OPTICAL_RECTANGLE = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum ADD_OPTICAL_CAMERA_RECTANGLE = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum REMOVE_OPTICAL_CAMERA_RECTANGLE = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum TOGGLE_OPTICAL_CAMERA_RECTANGLE = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum SET_Z_HEIGHT_OFFSET = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum ADD_ALIGNMENT_SURFACE_MAP_REGION = new PanelSurfaceMapEventEnum(++_index);
  public static PanelSurfaceMapEventEnum REMOVE_ALIGNMENT_SURFACE_MAP_REGION = new PanelSurfaceMapEventEnum(++_index);
    
  /**
   * @author Cheah Lee Herng
   */
  private PanelSurfaceMapEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private PanelSurfaceMapEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
