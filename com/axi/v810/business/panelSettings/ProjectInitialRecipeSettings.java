package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * XCR-2895 : Initial recipes setting to speed up the programming time
 *
 * @author weng-jian.eoh
 * @return
 */
public class ProjectInitialRecipeSettings
{
  private static ProjectInitialRecipeSettings _instance = null;
  private Config _config;
  private InitialRecipeSettingReader _reader = InitialRecipeSettingReader.getInstance();
//  private String _currentSetName = null;
  private String _currentSetFullPath = getSystemDefaultSetName();
  private Map<RecipeAdvanceSettingEnum,Object> _recipeSettingEnumToValueMap = new HashMap<RecipeAdvanceSettingEnum,Object>();

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  private ProjectInitialRecipeSettings()
  {
    _config = Config.getInstance();
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public static synchronized ProjectInitialRecipeSettings getInstance()
  {
    if (_instance == null)
      _instance = new ProjectInitialRecipeSettings();

    return _instance;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
   public void load() throws DatastoreException
   {
     _recipeSettingEnumToValueMap.clear();
     // load from disk the _currentThresholdSetName and set _initialThresholds to it's contents
     _recipeSettingEnumToValueMap = _reader.read(_currentSetFullPath);
   }

  /**
   * @author Andy Mechtenberg
   */
  public String getSystemDefaultSetName()
  {
    return FileName.getInitialRecipeDefaultSettingFileWithoutExtension();
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public void loadProjectRecipeSettingSet(String newSetFullPath) throws DatastoreException
  {
    Assert.expect(newSetFullPath != null);
    _currentSetFullPath = newSetFullPath;
    if (_currentSetFullPath.equalsIgnoreCase(getSystemDefaultSetName()))
      _recipeSettingEnumToValueMap.clear();
    else
      load();
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public String getInitialRecipeSettingWithoutExtension()
  {
    Assert.expect(_config != null);

    String fileName = _config.getStringValue(SoftwareConfigEnum.INITIAL_RECIPE_SETTING_FILENAME);
    if (fileName.endsWith(FileName.getConfigFileExtension()))
      fileName = fileName.substring(0, fileName.length() - FileName.getConfigFileExtension().length());

    return fileName;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
   public void setImportInitialRecipeSettingFileNameWithoutExtension(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);
    Assert.expect(_config != null);

    Assert.expect(fileName.endsWith(FileName.getConfigFileExtension()) == false);
    _config.setValue(SoftwareConfigEnum.INITIAL_RECIPE_SETTING_FILENAME, fileName);
    if(fileName.equalsIgnoreCase(FileName.getInitialRecipeDefaultSettingFileWithoutExtension()))
      setInitialRecipeSettingFileFullPath( fileName );
    else
      setInitialRecipeSettingFileFullPath(FileName.getInitialRecipeSettingFullPath(fileName) );
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public void setInitialRecipeSettingFileFullPath(String filePath) throws DatastoreException
  {
    Assert.expect(filePath != null);
    Assert.expect(_config != null);
    String fileName = FileUtil.getNameWithoutExtension(FileUtil.getNameWithoutPath(filePath));
    if(filePath.equalsIgnoreCase(FileName.getInitialRecipeDefaultSettingFileWithoutExtension()) == false)
      Assert.expect(filePath.endsWith(FileName.getConfigFileExtension()));
    
    
    loadProjectRecipeSettingSet(filePath);

    // this is optional, but it pre-loads the config ui screen with the set used for the loaded project, if any.
//    InitialRecipeSetting.getInstance().loadNewRecipeSetting(fileName);
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
   public String getRecipeSettingName()
   {
     Assert.expect(_currentSetFullPath != null);
     return FileUtil.getNameWithoutExtension(FileUtil.getNameWithoutPath(_currentSetFullPath));
   }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
   public Object getRecipeSettingValue(RecipeAdvanceSettingEnum setting)
  {
    if (_recipeSettingEnumToValueMap.containsKey((RecipeAdvanceSettingEnum)setting))
    {
      return _recipeSettingEnumToValueMap.get(setting);
    }
      
    return setting.getDefaultValue();
  }
}
