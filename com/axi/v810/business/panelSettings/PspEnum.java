/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.v810.util.*;

/**
 *
 * @author zee-foo.hwee
 */
public class PspEnum extends com.axi.util.Enum
{

   private static Map<String, PspEnum> _stringToEnumMap = new HashMap<String,PspEnum>();
    private String _name;
    private static int _index = -1;

    public static PspEnum NO_PSP = new PspEnum(++_index, StringLocalizer.keyToString("PLWK_NO_STRING_KEY"));
    public static PspEnum YES_PSP = new PspEnum(++_index, StringLocalizer.keyToString("PLWK_YES_STRING_KEY"));
   

    /**
     * @author Jack Hwee
     */
    private PspEnum(int id, String name)
    {
        super(id);
        _name = name;

        _stringToEnumMap.put(name, this);
    }

    /**
     * @author Jack Hwee
     */
    public String toString()
    {
        return _name;
    }
}
