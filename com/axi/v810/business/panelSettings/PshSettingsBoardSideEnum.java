package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;

/**
 * @author siew-yeng.phang
 */
public class PshSettingsBoardSideEnum extends com.axi.util.Enum 
{
  private static HashMap<Integer, PshSettingsBoardSideEnum> _integerToEnumMap;
  private static Map<String, PshSettingsBoardSideEnum> _nameToBoardSideEnumMap = new TreeMap<String, PshSettingsBoardSideEnum>(new AlphaNumericComparator());
  
  private static int _index = -1;
  private String _name;

  /**
   * @author Siew Yeng
   */
  static
  {
    _integerToEnumMap = new HashMap<Integer, PshSettingsBoardSideEnum>();
  }
  
  public static PshSettingsBoardSideEnum BOTH = new PshSettingsBoardSideEnum(++_index, "Both");
  public static PshSettingsBoardSideEnum SAME_SIDE_WITH_COMPONENT = new PshSettingsBoardSideEnum(++_index, "Same Side With Component");
  public static PshSettingsBoardSideEnum TOP = new PshSettingsBoardSideEnum(++_index, "Top");
  public static PshSettingsBoardSideEnum BOTTOM  = new PshSettingsBoardSideEnum(++_index, "Bottom");
  
  /**
   * @author Siew Yeng
   */
  private PshSettingsBoardSideEnum(int id, String name)
  {
    super(id);
    _name = name;
    _nameToBoardSideEnumMap.put(_name, this);
    
    Assert.expect(_integerToEnumMap.put(new Integer(id), this) == null);
  }
  
  /**
   * @author Siew Yeng
   */
  public String toString()
  {
    return _name;
  }
  
  /**
  * @author Siew Yeng
  */
  static public ArrayList<PshSettingsBoardSideEnum> getAllEnums()
  {
    return new ArrayList<PshSettingsBoardSideEnum>(_integerToEnumMap.values());
  }
  
  /**
  * @author Siew Yeng
  */
  static public PshSettingsBoardSideEnum getPshSettingsBoardSideEnum(String boardSideStr)
  {
    PshSettingsBoardSideEnum boardSideEnum = _nameToBoardSideEnumMap.get(boardSideStr);
    Assert.expect(boardSideEnum != null);
    return boardSideEnum;
  }
  
}
