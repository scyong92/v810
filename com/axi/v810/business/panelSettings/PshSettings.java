package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;

/**
 * @author Siew Yeng
 */
public class PshSettings implements Serializable 
{
  private Panel _panel;
  
  private boolean _isEnableSelectiveBoardSide = false;
  private boolean _isEnableSelectiveJointType = false;
  private boolean _isEnabledSelectiveComponent = false;
  private PshSettingsBoardSideEnum _pshSettingsBoardSideEnum = PshSettingsBoardSideEnum.BOTH;
  private List<JointTypeEnum> _disabledJointTypes = new ArrayList();
  
  private Map<ComponentType, List<ComponentType>> _pshComponentTypeToNeighbourComponentTypesMap = new HashMap<ComponentType, List<ComponentType>>();
  
  private static transient ProjectObservable _projectObservable;
  
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }
  
  /**
   * @author Siew Yeng
   */
  public PshSettings(Panel panel)
  {
    //Siew Yeng - XCR-3855
    _panel = panel;
    _projectObservable.stateChanged(this, null, this, PshSettingsEventEnum.CREATE_OR_DESTROY);
  }
  
  /**
   * @author Siew Yeng
   */
  public void destroy()
  {
    _projectObservable.stateChanged(this, this, null, PshSettingsEventEnum.CREATE_OR_DESTROY);
  }
  
  /**
   * @author Siew Yeng
   */  
  public boolean isEnabledSelectiveComponent()
  {
    return _isEnabledSelectiveComponent;
  }

  /**
   * @author Siew Yeng
   */  
  public void setEnableSelectiveComponent(boolean useSelectiveComponent)
  {
    boolean oldValue = _isEnabledSelectiveComponent;
    _projectObservable.setEnabled(false);
    try
    {
      _isEnabledSelectiveComponent = useSelectiveComponent;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    _isEnabledSelectiveComponent,
                                    PshSettingsEventEnum.SELECTIVE_COMPONENT_ENABLED);    
  }
  
  /**
   * @author Siew Yeng
   */  
  public boolean isEnableSelectiveBoardSide()
  {
    return _isEnableSelectiveBoardSide;
  }

  /**
   * @author Siew Yeng
   */  
  public void setEnableSelectiveBoardSide(boolean enableSelectiveBoardSide)
  {
    boolean oldValue = _isEnableSelectiveBoardSide;
    _projectObservable.setEnabled(false);
    try
    {
      _isEnableSelectiveBoardSide = enableSelectiveBoardSide;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    _isEnableSelectiveBoardSide,
                                    PshSettingsEventEnum.SELECTIVE_BOARD_SIDE_ENABLED);    
  }
  
  /**
   * @author Siew Yeng
   */  
  public boolean isEnableSelectiveJointType()
  {
    return _isEnableSelectiveJointType;
  }

  /**
   * @author Siew Yeng
   */  
  public void setEnableSelectiveJointType(boolean enableSelectiveJointType)
  {
    boolean oldValue = _isEnableSelectiveJointType;
    _projectObservable.setEnabled(false);
    try
    {
      _isEnableSelectiveJointType = enableSelectiveJointType;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    _isEnableSelectiveJointType,
                                    PshSettingsEventEnum.SELECTIVE_JOINT_TYPE_ENABLED);
  }
  
  /**
   * @author Siew Yeng
   */  
  public PshSettingsBoardSideEnum getPshSettingsBoardSideEnum()
  {
    return _pshSettingsBoardSideEnum;
  }

  /**
   * @author Siew Yeng
   */  
  public void setPshBoardSideReference(PshSettingsBoardSideEnum boardSideReference)
  {
    Assert.expect(boardSideReference != null);
    
    PshSettingsBoardSideEnum oldValue = _pshSettingsBoardSideEnum;
    _projectObservable.setEnabled(false);
    try
    {
      _pshSettingsBoardSideEnum = boardSideReference;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    _pshSettingsBoardSideEnum,
                                    PshSettingsEventEnum.BOARD_SIDE_REFERENCE_ENUM);
  }
  
  /**
   * @author Siew Yeng
   */
  public Panel getPanel()
  {
    Assert.expect(_panel != null);

    return _panel;
  }
  
  /**
   * @author Siew Yeng
   */
  public void setEnabledJointType(JointTypeEnum jointTypeEnum, boolean enabled)
  {
    Assert.expect(jointTypeEnum != null);
    
    Pair<JointTypeEnum, Boolean> oldValue = null;
    if (_disabledJointTypes.contains(jointTypeEnum))
      oldValue = new Pair<JointTypeEnum, Boolean>(jointTypeEnum, false);
    else
      oldValue = new Pair<JointTypeEnum, Boolean>(jointTypeEnum, true);
    _projectObservable.setEnabled(false);
    try
    {
      if (enabled == false && _disabledJointTypes.contains(jointTypeEnum) == false)
      {
        _disabledJointTypes.add(jointTypeEnum);
      }
      else if(enabled && _disabledJointTypes.contains(jointTypeEnum))
      {
        _disabledJointTypes.remove(jointTypeEnum);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    
    Pair<JointTypeEnum, Boolean> newValue = new Pair<JointTypeEnum, Boolean>(jointTypeEnum, enabled);
    _projectObservable.stateChanged(this, oldValue, newValue, PshSettingsEventEnum.ADD_OR_REMOVE_ENABLED_JOINT_TYPE);
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean isJointTypeEnabled(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);
    return _disabledJointTypes.contains(jointTypeEnum) == false;
  }
  
  /**
   * @author Siew Yeng
   */
  public List<JointTypeEnum> getDisabledJointType()
  {
    Assert.expect(_disabledJointTypes != null);
    
    return _disabledJointTypes;
  }
  
  /*
   * @author Siew Yeng
   */
  public Map<ComponentType, List<ComponentType>> getPshComponentTypeToNeighbourComponentTypesMap()
  {
    Assert.expect(_pshComponentTypeToNeighbourComponentTypesMap != null);
    
    return _pshComponentTypeToNeighbourComponentTypesMap;
  }
  
  /*
   * @author Siew Yeng
   */
  public void addSelectiveNeighbor(ComponentType pshComponentType, ComponentType neighbour)
  {
    Assert.expect(pshComponentType != null);
    Assert.expect(neighbour != null);
    Assert.expect(_pshComponentTypeToNeighbourComponentTypesMap != null);
    
    boolean neighbourAdded = false;
    _projectObservable.setEnabled(false);
    
    try
    {
      if(_pshComponentTypeToNeighbourComponentTypesMap.containsKey(pshComponentType) == false)
      {
         _pshComponentTypeToNeighbourComponentTypesMap.put(pshComponentType, new ArrayList<ComponentType>());
      }

      if(_pshComponentTypeToNeighbourComponentTypesMap.get(pshComponentType).contains(neighbour) == false)
        neighbourAdded = _pshComponentTypeToNeighbourComponentTypesMap.get(pshComponentType).add(neighbour);
    }
    finally
    {
      _projectObservable.setEnabled(true);
      
      if(neighbourAdded)
        _projectObservable.stateChanged(this, null, neighbour, PshSettingsEventEnum.ADD_OR_REMOVE_PSH_NEIGHBOUR_COMPONENT);
    }
  }
  
  /*
   * @author Siew Yeng
   */
  public void removeSelectiveNeighbour(ComponentType pshComponentType, ComponentType neighbour)
  {
    Assert.expect(pshComponentType != null);
    Assert.expect(neighbour != null);
    Assert.expect(_pshComponentTypeToNeighbourComponentTypesMap != null);
    
    boolean componentRemoved = false;
    _projectObservable.setEnabled(false);
    
    try
    {
      if(_pshComponentTypeToNeighbourComponentTypesMap.containsKey(pshComponentType))
      {
         if(_pshComponentTypeToNeighbourComponentTypesMap.get(pshComponentType).contains(neighbour))
         {
           componentRemoved = _pshComponentTypeToNeighbourComponentTypesMap.get(pshComponentType).remove(neighbour);
         }
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
      
      if(componentRemoved)
        _projectObservable.stateChanged(this, neighbour, null, PshSettingsEventEnum.ADD_OR_REMOVE_PSH_NEIGHBOUR_COMPONENT);
    }
  }
  
  /*
   * @author Siew Yeng
   */
  public boolean isNeighborExist(ComponentType neighbor)
  { 
    Assert.expect(neighbor != null);
    Assert.expect(_pshComponentTypeToNeighbourComponentTypesMap != null);
    
    for(List<ComponentType> neighbours : _pshComponentTypeToNeighbourComponentTypesMap.values())
    {
      if(neighbours.contains(neighbor))
        return true;
    }
      
    return false;
  }
  
  /*
   * @author Siew Yeng
   */
  public boolean isNeighborOfPshComponent(ComponentType neighbor, ComponentType pshComponent)
  {
    Assert.expect(neighbor != null);
    Assert.expect(pshComponent != null);
    Assert.expect(_pshComponentTypeToNeighbourComponentTypesMap != null);
    
    if(_pshComponentTypeToNeighbourComponentTypesMap.containsKey(pshComponent))
    {
      if(_pshComponentTypeToNeighbourComponentTypesMap.get(pshComponent).contains(neighbor))
        return true;
    }
    
    return false;
  }
  
  /*
   * @author Siew Yeng
   */
  public boolean isUsingSelectiveComponent(Component componet)
  {
    Assert.expect(componet != null);
    Assert.expect(_pshComponentTypeToNeighbourComponentTypesMap != null);
    
    if(isEnabledSelectiveComponent() == false)
      return false;
    
    return _pshComponentTypeToNeighbourComponentTypesMap.containsKey(componet.getComponentType());
  }
  
  /*
   * @author Siew Yeng
   */
  public List<Component> getSelectiveNeighbors(Component pshComponent)
  {
    Assert.expect(pshComponent != null);
    Assert.expect(_pshComponentTypeToNeighbourComponentTypesMap != null);
    
    List<Component> neighbors = new ArrayList<Component>();
    if(_pshComponentTypeToNeighbourComponentTypesMap.containsKey(pshComponent.getComponentType()))
    {
      Board board = pshComponent.getBoard();

      for(ComponentType componentType : _pshComponentTypeToNeighbourComponentTypesMap.get(pshComponent.getComponentType()))
      {
        neighbors.add(componentType.getComponent(board));
      }
    }
    return neighbors;
  }
  
  /*
   * @author Siew Yeng
   */
  public List<ComponentType> getPshComponentTypes()
  {
    Assert.expect(_panel != null);
    
    List<ComponentType> pshComponents = new ArrayList<ComponentType>();
    
    //Siew Yeng - XCR-3865
    for(Subtype subtype : _panel.getInspectedPredictiveSliceHeightSubtypes())
    {
      for(ComponentType compType : subtype.getComponentTypes())
      {
        if(compType.isInspected() == false)
          continue;

        if(pshComponents.contains(compType) == false)
        pshComponents.add(compType);
      }
    }
    
    return pshComponents;
  }
  
  /*
   * @author Siew Yeng
   */
  public boolean doesRegionPassPshSettingsFilter(ReconstructionRegion region)
  {
    if(isEnabledSelectiveComponent() && 
       isNeighborExist(region.getComponent().getComponentType()))
      return true;
    
    if(isEnableSelectiveJointType() &&  isJointTypeEnabled(region.getJointTypeEnum()) == false)
      return false;
    
    if(isEnableSelectiveBoardSide())
    {
      if((getPshSettingsBoardSideEnum().equals(PshSettingsBoardSideEnum.TOP) && region.isTopSide() == false) ||
          getPshSettingsBoardSideEnum().equals(PshSettingsBoardSideEnum.BOTTOM) && region.isTopSide())
        return false;
    }
    
    return true;
  }
}
