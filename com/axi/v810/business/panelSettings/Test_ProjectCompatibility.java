package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.AlgorithmSettingEnum;
import com.axi.v810.business.panelDesc.JointTypeEnum;
import com.axi.v810.datastore.projReaders.ProjectReader;

/**
 * @author Andy Mechtenberg
 */
public class Test_ProjectCompatibility extends UnitTest
{  
  private static Project _project = null;

  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ProjectCompatibility());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader is, PrintWriter os)
  {    
    try
    {      
      Expect.expect(ProjectCompatibility.doesProjectRequireCompatibilityModification("1.0") == true);
      Expect.expect(ProjectCompatibility.doesProjectRequireCompatibilityModification("1.1") == true);
      Expect.expect(ProjectCompatibility.doesProjectRequireCompatibilityModification("1.11") == true);
      Expect.expect(ProjectCompatibility.doesProjectRequireCompatibilityModification("1.12") == true);
      Expect.expect(ProjectCompatibility.doesProjectRequireCompatibilityModification("1.13") == false);

      ProjectReader projectReader = ProjectReader.getInstance();
      
      // load a project with a last save version of 1.13
      Project project = projectReader.load("0027Parts");      
      Expect.expect(ProjectCompatibility.doesProjectRequireCompatibilityModification(project.getSoftwareVersionOfProjectLastSave()) == false);
      List<String> messages = ProjectCompatibility.applyCompatibilityChanges(project);
      Expect.expect(messages.isEmpty());

      /**
       * This portion is commented by LeeHerng 23 April 2010 - The test procedure below is invalid in v810 system
       * because by default, all the projects in v810 system is 2.0 version.
       *
      // load a project with a last save version of 1.1
      project = projectReader.load("ALL_JOINT_TYPES");      
      Expect.expect(ProjectCompatibility.doesProjectRequireCompatibilityModification(project.getSoftwareVersionOfProjectLastSave()) == true);
      
      List<Subtype> pthSubtypes = project.getPanel().getSubtypes(JointTypeEnum.THROUGH_HOLE);
      for (Subtype subtype : pthSubtypes)
      {
        Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT);
        Expect.expect(value.equals("On"));
      }      
      messages = ProjectCompatibility.applyCompatibilityChanges(project);                  
      Expect.expect(messages.isEmpty() == false);
      
      for (Subtype subtype : pthSubtypes)
      {
        Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT);
        Expect.expect(value.equals("Off"));
      }
       * */
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }    
  }
}
