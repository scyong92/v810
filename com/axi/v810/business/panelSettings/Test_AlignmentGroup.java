package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Andy Mechtenberg
 */
public class Test_AlignmentGroup extends UnitTest
{
  /**
   * @author Andy Mechtenberg
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_AlignmentGroup());
  }

  /**
   * @author Keith Lee
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    PanelSettings panelSettings = new PanelSettings();
    AlignmentGroup alignmentGroup = new AlignmentGroup(panelSettings);

    // test getName, setName
    try
    {
      String name = alignmentGroup.getName();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      alignmentGroup.setName(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    String name = "Name";
    alignmentGroup.setName(name);
    Expect.expect(alignmentGroup.getName().equals(name));
  }
}
