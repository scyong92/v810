package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;

/**
 * Manages the learned profile information for each joint.  The learned profiles contained herein
 * actually represent an incremental running average of
 *
 * @author Lim, Lay Ngor
 */
public class BrokenPinLearning
{
  private static float _uninitializedValue = -1f;

  private Pad _pad;
  private SliceNameEnum _sliceNameEnum;
  
  private int _numberOfBrokenPinDataLearned = 0;
  private int[] _learnBorderObjectRect = null;
  
  public BrokenPinLearning()
  {
    // Do nothing...
  }
  
  /**
   * @author Bill Darbie
   */
  public static float getUninitializedValue()
  {
    return _uninitializedValue;
  }

  /**
   * @author Bill Darbie
   */
  public void setPad(Pad pad)
  {
    Assert.expect(pad != null);
    _pad = pad;
  }

  /**
   * @author Bill Darbie
   */
  public Pad getPad()
  {
    Assert.expect(_pad != null);
    return _pad;
  }

  /**
   * @author Bill Darbie
   */
  public void setSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    _sliceNameEnum = sliceNameEnum;
  }
  /**
   * @author Bill Darbie
   */
  public SliceNameEnum getSliceNameEnum()
  {
    Assert.expect(_sliceNameEnum != null);
    return _sliceNameEnum;
  }

  /**
   * @author Lim, Lay Ngor
   */
  public int getNumberOfBrokenPinDataLearned()
  {
    return _numberOfBrokenPinDataLearned;
  }

  /**
   * @author Lim, Lay Ngor
   */
  public void setNumberOfBrokenPinDataLearned(int numberOfBrokenPinDataLearned)
  {
    // NOTE: _projectObservable.stateChanged() does not need to be called here - a higher level
    //       call we get the event out
    Assert.expect(numberOfBrokenPinDataLearned >= 0);

    _numberOfBrokenPinDataLearned = numberOfBrokenPinDataLearned;
  }
  
  /**
   * @author Lim, Lay Ngor
   */
  public int[] getLearnBorderObjectRect()
  {
    Assert.expect(_learnBorderObjectRect != null);

    return _learnBorderObjectRect;
  }

  /**
   * @author Lim, Lay Ngor
   */
  public void setLearnBorderObjectRect(int[] learnBorderObjectRect)
  {
    // NOTE: _projectObservable.stateChanged() does not need to be called here - a higher level
    //       call we get the event out
    Assert.expect(learnBorderObjectRect != null);

    _learnBorderObjectRect = learnBorderObjectRect;
  }
}