package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.hardware.*;

/**
 * A BoardSurfaceMapSettings contains all the surface map points list contained
 * within a board.
 * 
 * 
 * @author Cheah Lee Herng
 */
public class BoardSurfaceMapSettings implements Serializable 
{   
  private Board _board;

  // created by SurfaceMapSettingsReader
  private String _userComment; // SurfaceMapSettingsReader

  // Map of surface maps that are parked under this surface map region
  private Map<String, OpticalRegion> _regionPositionNameToOpticalRegionMap = new LinkedHashMap<String, OpticalRegion>(); // SurfaceMapSettingsReader
  private Map<String, Boolean> _panelPositionNameToPanelPositionVerifiedMap = new LinkedHashMap<String, Boolean>(); // SurfaceMapSettingsReader

  private Map<String, OpticalCameraIdEnum> _panelPositionNameToCameraMap = new LinkedHashMap<String, OpticalCameraIdEnum>();

  private boolean _isDataAvailable;

  private static transient ProjectObservable _projectObservable;

  private OpticalRegion _boardOpticalRegion; // PanelSurfaceMapSettings

  private Map<Integer, OpticalRegion> _tableIndexToOpticalRegionMap = new LinkedHashMap<Integer, OpticalRegion>();    

  /**
   * @author Cheah Lee Herng
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }
    
  /**
   * @author Cheah Lee Herng
   */
  public BoardSurfaceMapSettings()
  {
    _projectObservable.stateChanged(this, null, this, BoardSurfaceMapEventEnum.CREATE_OR_DESTROY);
    _isDataAvailable = false;
  }
    
  /**
   * @author Cheah Lee Herng
  */
  public BoardSurfaceMapSettings(Board board)
  {
    Assert.expect(board != null);
    _board = board;
    _projectObservable.stateChanged(this, null, this, BoardSettingsEventEnum.CREATE_OR_DESTROY);
  }
    
  /**
  * @author Jack Hwee
  */
  public void setOpticalRegion(OpticalRegion opticalRegion)
  {
    Assert.expect(opticalRegion != null);       
    _boardOpticalRegion = opticalRegion;
  } 
     
  /**
   * @author Jack Hwee
  */
  public OpticalRegion getOpticalRegion()
  {
    //Assert.expect(_boardOpticalRegion != null);  
    return _boardOpticalRegion;  
  }

  /**
   * @author Ying-Huan.Chu
   */
  public boolean hasOpticalRegion()
  {
    if (_boardOpticalRegion != null)
      return true;
    else
      return false;
  }

  /**
   * @author Bill Darbie
  */
  public void destroy()
  {        
    _projectObservable.stateChanged(this, this, null, BoardSurfaceMapEventEnum.CREATE_OR_DESTROY);
  }    

  /**
   * A " character is not allowed in the comment field
   * @author Cheah Lee Herng
  */
  public boolean isUserCommentValid(String comment)
  {
    Assert.expect(comment != null);
    if (comment.contains("\""))
      return false;

    return true;
  }

  /**
   * @author Cheah Lee Herng
  */
  public void setUserComment(String comment)
  {
    Assert.expect(comment != null);
    Assert.expect(isUserCommentValid(comment));

    String oldValue = _userComment;
    _projectObservable.setEnabled(false);
    try
    {
      _userComment = comment.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, comment, BoardSurfaceMapEventEnum.USER_COMMENT);
  }

  /**
   * @author Cheah Lee Herng
   */
  public String getUserComment()
  {
    Assert.expect(_userComment != null);
    return _userComment;
  }

    /**
   * @author Cheah Lee Herng
   */
  public void addOpticalRegion(OpticalRegion opticalRegion)
  {
    Assert.expect(_regionPositionNameToOpticalRegionMap != null);
    Assert.expect(opticalRegion != null);

    _isDataAvailable = true;     

    _boardOpticalRegion = opticalRegion;
    String regionPositionName = "Board_" + opticalRegion.getBoards().iterator().next() + "_" + opticalRegion.getRegion().getMinX() + "_" + opticalRegion.getRegion().getMinY() + "_" + opticalRegion.getRegion().getWidth() + "_" + opticalRegion.getRegion().getHeight();
    _board = opticalRegion.getBoards().iterator().next();
    OpticalRegion oldBoardOpticalRegionSettings = null;
    _projectObservable.setEnabled(false);
    try
    {
      oldBoardOpticalRegionSettings = _regionPositionNameToOpticalRegionMap.put(regionPositionName, opticalRegion);      
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }         
    _projectObservable.stateChanged(this, oldBoardOpticalRegionSettings, opticalRegion, BoardSurfaceMapEventEnum.REMOVE_SURFACE_MAP_OPTICAL_REGION);

  }

  /**
   * @author Jack Hwee
   */
  public void addOpticalRegionWithIndex(OpticalRegion opticalRegion, int tableIndex)
  {
    Assert.expect( _tableIndexToOpticalRegionMap != null);
    Assert.expect(opticalRegion != null);

    _tableIndexToOpticalRegionMap.put(tableIndex, opticalRegion);  

  }

   /**
   * @author Jack Hwee
   */
  public Map<Integer, OpticalRegion> getOpticalRegionTableIndexToOpticalRegionMap()
  {
    Assert.expect(_tableIndexToOpticalRegionMap != null);
    return _tableIndexToOpticalRegionMap;
  }

  /**
   * @author Jack Hwee
   */
  public void clearOpticalRegionWithIndex()
  {
    _tableIndexToOpticalRegionMap.clear();
  }


  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  public void removeSurfaceMapRegion(OpticalRegion opticalRegion)
  {
    Assert.expect(_regionPositionNameToOpticalRegionMap != null);

    String regionPositionName = "Board_" + getBoard().getName() + "_" + opticalRegion.getRegion().getMinX() + "_" + opticalRegion.getRegion().getMinY() + "_" + opticalRegion.getRegion().getWidth() + "_" + opticalRegion.getRegion().getHeight();
       
    OpticalRegion oldOpticalRegion = null;
    _projectObservable.setEnabled(false);
    try
    {
      oldOpticalRegion = _regionPositionNameToOpticalRegionMap.remove(regionPositionName);
    }
    finally
    {
      _projectObservable.setEnabled(true);
      if (oldOpticalRegion != null) // the key existed in the map.
      {
        _boardOpticalRegion = null;
      }
    }         
    _projectObservable.stateChanged(this, oldOpticalRegion, opticalRegion, BoardSurfaceMapEventEnum.REMOVE_SURFACE_MAP_OPTICAL_REGION);

  } 
  
  /**
   * @author Cheah Lee Herng 
   */
  public void updateSurfaceMapRegion(OpticalRegion opticalRegion, String oldBoardName, String newBoardName)
  {
    Assert.expect(opticalRegion != null);
    
    String oldRegionPositionName = "Board_" + oldBoardName + "_" + opticalRegion.getRegion().getMinX() + "_" + opticalRegion.getRegion().getMinY() + "_" + opticalRegion.getRegion().getWidth() + "_" + opticalRegion.getRegion().getHeight();
    if (_regionPositionNameToOpticalRegionMap.containsKey(oldRegionPositionName))
    {
      _regionPositionNameToOpticalRegionMap.remove(oldRegionPositionName);      
    }
    
    String newRegionPositionName = "Board_" + newBoardName + "_" + opticalRegion.getRegion().getMinX() + "_" + opticalRegion.getRegion().getMinY() + "_" + opticalRegion.getRegion().getWidth() + "_" + opticalRegion.getRegion().getHeight();
    if (_regionPositionNameToOpticalRegionMap.containsKey(newRegionPositionName) == false)
    {
      _regionPositionNameToOpticalRegionMap.put(newRegionPositionName, opticalRegion);
    }
  }

  /**
   * @author Cheah Lee Herng 
   */
  public Map<String, OpticalRegion> getOpticalRegionPositionNameToOpticalRegionMap()
  {
    Assert.expect(_regionPositionNameToOpticalRegionMap != null);
    return _regionPositionNameToOpticalRegionMap;
  }

/**
 * @return a List containing OpticalRegion for this region
 * @author Jack Hwee
 */
  public List<OpticalRegion> getOpticalRegions()
  {
    Assert.expect(_regionPositionNameToOpticalRegionMap != null);

    return new ArrayList<OpticalRegion>(_regionPositionNameToOpticalRegionMap.values());
  }

/**
 * @return a List containing Name for this region
 * @author Jack Hwee
 */
  public List<String> getOpticalRegionsName()
  {
    Assert.expect(_regionPositionNameToOpticalRegionMap != null);

    return new ArrayList<String>(_regionPositionNameToOpticalRegionMap.keySet());
  }

  /**
   * @author Cheah Lee Herng
   */
  public Map<String, Boolean> getPanelPositionNameToPanelPositionVerifiedMap()
  {
    Assert.expect(_panelPositionNameToPanelPositionVerifiedMap != null);
    return _panelPositionNameToPanelPositionVerifiedMap;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public boolean getPanelPositionVerifiedFlag(String panelPositionName)
  {
    Assert.expect(_panelPositionNameToPanelPositionVerifiedMap != null);
    Assert.expect(panelPositionName != null);

    boolean verifiedFlag = false;
    if (_panelPositionNameToPanelPositionVerifiedMap.containsKey(panelPositionName))
    {
      Boolean verifiedFlagBoolean = _panelPositionNameToPanelPositionVerifiedMap.get(panelPositionName);
      if (verifiedFlagBoolean.booleanValue())
        verifiedFlag = true;
      else
        verifiedFlag = false;
    }
    else
      Assert.expect(false);
    return verifiedFlag;
  }

  /**
   * @author Cheah Lee Herng
  */
  public Board getBoard()
  {
    Assert.expect(_board != null);

    return _board;
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean isDataAvailable()
  {
    return _isDataAvailable;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void assignCamera(String panelPositionName, OpticalCameraIdEnum cameraId)
  {
    Assert.expect(panelPositionName != null);
    Assert.expect(cameraId != null);
    Assert.expect(_panelPositionNameToCameraMap != null);

    Object prev = _panelPositionNameToCameraMap.put(panelPositionName, cameraId);
    Assert.expect(prev == null);                
  }

  /**
   * @author Phang Siew Yeng
   */
  public void clearAllSurfaceMapRegion()
  {
    _regionPositionNameToOpticalRegionMap.clear();
  }
}
