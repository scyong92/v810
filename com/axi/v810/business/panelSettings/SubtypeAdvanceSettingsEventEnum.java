package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 *
 * @author wei-chin.chong
 */
public class SubtypeAdvanceSettingsEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static SubtypeAdvanceSettingsEventEnum CREATE_OR_DESTROY = new SubtypeAdvanceSettingsEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static SubtypeAdvanceSettingsEventEnum SIGNAL_COMPENSATION_ENUM = new SubtypeAdvanceSettingsEventEnum(++_index);
  public static SubtypeAdvanceSettingsEventEnum ARTIFACT_COMPENSATION_STATE_ENUM = new SubtypeAdvanceSettingsEventEnum(++_index);
  public static SubtypeAdvanceSettingsEventEnum USER_GAIN_ENUM = new SubtypeAdvanceSettingsEventEnum(++_index);  
  public static SubtypeAdvanceSettingsEventEnum STAGE_SPEED_ENUM = new SubtypeAdvanceSettingsEventEnum(++_index);  
  public static SubtypeAdvanceSettingsEventEnum MAGNIFICATION_TYPE_ENUM = new SubtypeAdvanceSettingsEventEnum(++_index);
  public static SubtypeAdvanceSettingsEventEnum DYNAMIC_RANGE_OPTIMIZATION_LEVEL_ENUM = new SubtypeAdvanceSettingsEventEnum(++_index);  //Siew Yeng - XCR-2140
  public static SubtypeAdvanceSettingsEventEnum FOCUS_METHOD = new SubtypeAdvanceSettingsEventEnum(++_index); 
  /**
   * @author wei-chin.chong
   */
  private SubtypeAdvanceSettingsEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author wei-chin.chong
   */
  private SubtypeAdvanceSettingsEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
