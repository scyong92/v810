/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import java.util.*;

/**
 *
 * @author sheng-chuan.yong
 */
public class StageSpeedEnum extends com.axi.util.Enum
{
  private static Map<Double, StageSpeedEnum> _stageSpeedToEnumMap = new LinkedHashMap<Double, StageSpeedEnum>();
  private static List<StageSpeedEnum> _orderedStageSpeed = new ArrayList<StageSpeedEnum>();
  private static List<String> _orderedStageSpeedNameList = new ArrayList<String>();
  private double _stageSpeed;
  private static int _index = -1;
  public static StageSpeedEnum ONE        = new StageSpeedEnum(++_index, 1.0);
  public static StageSpeedEnum TWO        = new StageSpeedEnum(++_index, 1.5);
  public static StageSpeedEnum THREE      = new StageSpeedEnum(++_index, 2.0);
  public static StageSpeedEnum FOUR       = new StageSpeedEnum(++_index, 2.5);
  public static StageSpeedEnum FIVE       = new StageSpeedEnum(++_index, 3.0);
  public static StageSpeedEnum SIX        = new StageSpeedEnum(++_index, 3.5);
  public static StageSpeedEnum SEVEN      = new StageSpeedEnum(++_index, 4.0);
  public static StageSpeedEnum EIGHT      = new StageSpeedEnum(++_index, 4.5);
  public static StageSpeedEnum NINE       = new StageSpeedEnum(++_index, 5.0);
  public static StageSpeedEnum TEN        = new StageSpeedEnum(++_index, 5.5);
  public static StageSpeedEnum ELEVEN     = new StageSpeedEnum(++_index, 6.0);
  public static StageSpeedEnum TWELVE     = new StageSpeedEnum(++_index, 6.5);
  public static StageSpeedEnum THIRTEEN   = new StageSpeedEnum(++_index, 7.0);
  public static StageSpeedEnum FOURTEEN   = new StageSpeedEnum(++_index, 7.5);
  public static StageSpeedEnum FIFTEEN    = new StageSpeedEnum(++_index, 8.0);
  public static StageSpeedEnum SIXTEEN    = new StageSpeedEnum(++_index, 8.5);
  public static StageSpeedEnum SEVENTEEN  = new StageSpeedEnum(++_index, 9.0);
  
  
  /**
   * @author sham
   */
  private StageSpeedEnum(int id, double stageSpeed)
  {
    super(id);
    _stageSpeed = stageSpeed;

    _stageSpeedToEnumMap.put(stageSpeed, this);
    _orderedStageSpeed.add(this);
    _orderedStageSpeedNameList.add(Double.toString(stageSpeed));
  }
  
  /*
   * @author sham
   */
  public double toDouble()
  {
    return Double.valueOf(_stageSpeed);
  }

  /**
   * @author sham
   */
  public String toString()
  {
    return String.valueOf(_stageSpeed);
  }

  /**
   * @author sham
   */
  public static StageSpeedEnum getStageSpeedToEnumMapValue(double key)
  {
    Assert.expect(key > 0);

    return _stageSpeedToEnumMap.get(key);
  }

  /**
   * @author Chong, Wei Chin
   */
  public static List<StageSpeedEnum> getOrderedStageSpeed()
  {
    return new ArrayList<StageSpeedEnum>(_orderedStageSpeed);
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public static StageSpeedEnum getHighestStageSpeed()
  {
    return _orderedStageSpeed.get(_orderedStageSpeed.size() - 1);
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public static StageSpeedEnum getLowestStageSpeed()
  {
    return _orderedStageSpeed.get(0);
  }
  
  /**
   * @author Yong Sheng Chuan
   * @return 
   */
  public static List<StageSpeedEnum> getWholeNumberStageSpeedList()
  {
    List<StageSpeedEnum> speedEnumList = new ArrayList<StageSpeedEnum>();
    for (StageSpeedEnum speed : getOrderedStageSpeed())
    {
      double speedValue = speed.toDouble();
      if (speedValue == Math.floor(speedValue) && !Double.isInfinite(speedValue) && speedEnumList.contains(speed) == false) 
      {
        speedEnumList.add(speed);
      }
    }
    return speedEnumList;
  }
  
  public static List<String> getOrderStageSpeedName()
  {
    return _orderedStageSpeedNameList;
  }
}
