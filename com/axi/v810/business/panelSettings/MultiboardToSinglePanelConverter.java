package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;


/**
 *
 * @author siew-yeng.phang
 */
public class MultiboardToSinglePanelConverter
{
  private static MultiboardToSinglePanelConverter _instance;
  
   private static final String _REFDES_POSTFIX = "Bd";
  
   /**
   * @author siew-yeng.phang
   */
  public static synchronized MultiboardToSinglePanelConverter getInstance()
  {
    if (_instance == null)
      _instance = new MultiboardToSinglePanelConverter();

    return _instance;
  }

  /**
   * @author siew-yeng.phang
   */  
  public void mergeMultiboard(Project project)
  {
    ProjectObservable.getInstance().setEnabled(false);
    
    try
    {   
      //delete all learned data
      // Broken Pin & ShengChuan - Clear Tombstone
      StatisticalTuningEngine.getInstance().deletePanelLearnedData(project, true, true, true, true, true);
      
      Panel projectPanel = project.getPanel();

      //clear alignment and surface map regions
      for(AlignmentGroup alignmentGroup : projectPanel.getPanelSettings().getAllAlignmentGroups())
      {            
        alignmentGroup.clearPads();

        alignmentGroup.resetBackgroundGrayLevel();
        alignmentGroup.resetForegroundGrayLevel();
        alignmentGroup.resetMatchQuality();
      }
      projectPanel.getPanelSettings().clearAllManualAlignmentTransform();
      
      for(Board board:projectPanel.getBoards())
      {
        for(AlignmentGroup alignmentGroup : board.getBoardSettings().getAllAlignmentGroups())
        {
          alignmentGroup.clearPads();

          alignmentGroup.resetBackgroundGrayLevel();
          alignmentGroup.resetForegroundGrayLevel();
          alignmentGroup.resetMatchQuality();
        }
        board.getBoardSettings().clearAllManualAlignmentTransform();
        
        if(board.hasBoardSurfaceMapSettings())
          board.getBoardSurfaceMapSettings().clearAllSurfaceMapRegion();
      }
      
      if(projectPanel.hasPanelSurfaceMapSettings())
        projectPanel.getPanelSurfaceMapSettings().clearAllSurfaceMapRegion();

      if(!projectPanel.getPanelSettings().isPanelBasedAlignment())
        projectPanel.getPanelSettings().setIsPanelBasedAlignment(true);
      
      // Component will be add into this board
      Board boardOne = projectPanel.getBoards().get(0);
      
      //original board component list
      List<Component> componentList = boardOne.getComponents();
      
      //create new component from other boards and add into board one
      for(Board board : projectPanel.getBoards())
      {              
        if(board.getName().equals(boardOne.getName()))
          continue;
        
        for (Component c : componentList)
        {
          Component component = board.getComponent(c.getReferenceDesignator());
          
          SideBoard sideBoard = boardOne.getTopSideBoard();
          if((component.isTopSide() && boardOne.isFlipped()) || 
            (component.isBottomSide() && !boardOne.isFlipped()))
          {
              sideBoard = boardOne.getBottomSideBoard();
          }
          
          double newRotationRelativeToBoard = calculateNewDegreeRotationRelativeToBoard(board, component);

          PanelCoordinate coordinateRelativeToCadOrigin = component.getCoordinateRelativeToCadOriginInNanoMeters(); 
          BoardCoordinate newBoardCoordinate = new BoardCoordinate(coordinateRelativeToCadOrigin.getX(), coordinateRelativeToCadOrigin.getY());
          String newReferenceDesignator = component.getReferenceDesignator()+ "_" + _REFDES_POSTFIX + board.getName();

          ComponentType newComponentType = sideBoard.getSideBoardType().createComponentType(newReferenceDesignator, 
                                                                                            component.getLandPattern(), 
                                                                                            newBoardCoordinate, 
                                                                                            newRotationRelativeToBoard);
          
          newComponentType.setComponentTypeSettings(component.getComponentTypeSettings());
          newComponentType.setCompPackage(component.getCompPackage());
        
          for (PadType newPadType : newComponentType.getPadTypes())
          {
            PadType padType = component.getComponentType().getPadType(newPadType.getName());
            
            newPadType.setPackagePin(padType.getPackagePin());
            newPadType.setPadTypeSettings(padType.getPadTypeSettings());
            
            for(Pad newPad : newPadType.getPads())
            {
              newPad.setPadSettings(component.getPad(newPad.getName()).getPadSettings());
            }
          }
        }
      }
      
      //resize boardOne, rename components on boardOne
      for (Component component : componentList)
      {
        String refDes = component.getReferenceDesignator();
        ComponentType compType = boardOne.getComponentType(component.getReferenceDesignator());

        double newRotationRelativeToBoard = calculateNewDegreeRotationRelativeToBoard(boardOne, component);
                  
        if(boardOne.isFlipped())
          compType.changeSide();
        
        //deleteComponentTypeThatCausesInterferencePatterns for componentType with original refDes
        List<PadType> interferenceAffectedPadTypes = compType.getInterferenceAffectedPadTypes();
        if(!interferenceAffectedPadTypes.isEmpty())
        {
          for(PadType padType : interferenceAffectedPadTypes)
          {
            padType.deleteComponentTypeThatCausesInterferencePatterns(compType);
          }
        }
        
        PanelCoordinate coordinateRelativeToCadOrigin = component.getCoordinateRelativeToCadOriginInNanoMeters();
        BoardCoordinate newBoardCoordinate = new BoardCoordinate(coordinateRelativeToCadOrigin.getX(), coordinateRelativeToCadOrigin.getY());
        String newReferenceDesignator = refDes + "_" + _REFDES_POSTFIX + boardOne.getName();
        
        compType.setCoordinateInNanoMeters(newBoardCoordinate);
        compType.setDegreesRotationRelativeToBoard(newRotationRelativeToBoard);
        compType.setReferenceDesignator(newReferenceDesignator);
        
        // addComponentTypeThatCausesInteferencePattern for componentType with new refDes
        if(!interferenceAffectedPadTypes.isEmpty())
        {
          for(PadType padType : interferenceAffectedPadTypes)
          {
            padType.addComponentTypeThatCausesInteferencePattern(compType);
          }
        }
      }
      //New board one will be same size as panel with board rotation 0 and no left-to-right/top-to-bottom flip.
      boardOne.getBoardType().setWidthInNanoMeters(projectPanel.getWidthInNanoMeters());
      boardOne.getBoardType().setLengthInNanoMeters(projectPanel.getLengthInNanoMeters());
      
      boardOne.setDegreesRotationRelativeToPanel(0);
      boardOne.setLeftToRightFlip(false);
      boardOne.setTopToBottomFlip(false);
      boardOne.setLowerLeftCoordinateRelativeToPanelInNanoMeters(new PanelCoordinate(0,0));
      
      //delete all boards except board one
      for(Board board : projectPanel.getBoards())
      {
        if(board.getName().equals(boardOne.getName()))
          continue;

        board.remove();
      }
    }
    catch(XrayTesterException xte)
    {
      xte.printStackTrace();
    }
    finally
    {
      ProjectObservable.getInstance().setEnabled(true); 
      MemoryUtil.garbageCollect();
    }
  }
  
  /**
   * Calculate new degree rotation relative to board
   * @author siew-yeng.phang
   */  
  private double calculateNewDegreeRotationRelativeToBoard(Board board, Component component)
  {
    int boardRotationRelativeToPanel = board.getDegreesRotationRelativeToPanel();
    double newRotationRelativeToBoard = component.getDegreesRotationRelativeToBoard();
    
    if(boardRotationRelativeToPanel == 0 || boardRotationRelativeToPanel == 180)
    {
      if(board.isTopToBottomFlip())
        newRotationRelativeToBoard = MathUtil.getDegreesWithin0To359(newRotationRelativeToBoard - boardRotationRelativeToPanel - 180);
      else if(board.isLeftToRightFlip())
        newRotationRelativeToBoard = MathUtil.getDegreesWithin0To359(newRotationRelativeToBoard - boardRotationRelativeToPanel);
      else
        newRotationRelativeToBoard = MathUtil.getDegreesWithin0To359(newRotationRelativeToBoard - boardRotationRelativeToPanel);
    }
    else if(boardRotationRelativeToPanel == 90 || boardRotationRelativeToPanel == 270)
    {
      if(board.isTopToBottomFlip())
        newRotationRelativeToBoard = MathUtil.getDegreesWithin0To359(newRotationRelativeToBoard - boardRotationRelativeToPanel);
      else if(board.isLeftToRightFlip())
        newRotationRelativeToBoard = MathUtil.getDegreesWithin0To359(newRotationRelativeToBoard - boardRotationRelativeToPanel - 180);
      else
        newRotationRelativeToBoard = MathUtil.getDegreesWithin0To359(newRotationRelativeToBoard - boardRotationRelativeToPanel - 180);

      if((component.isBottomSide() && !board.getPanel().isFlipped()) ||
        (component.isTopSide() && board.getPanel().isFlipped()))
        newRotationRelativeToBoard = MathUtil.getDegreesWithin0To359(newRotationRelativeToBoard - 180);
    }
    
    return newRotationRelativeToBoard;
  }
}
