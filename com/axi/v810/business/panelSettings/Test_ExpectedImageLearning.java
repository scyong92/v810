package com.axi.v810.business.panelSettings;

import java.io.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.panelDesc.Pad;


/**
 * <p>Title: Test_ExpectedImageLearning</p>
 * <p>Description: Regression test for Image class</p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company:  Agilent Technogies</p>
 * @author George Booth
 * @version 1.0
 */

public class Test_ExpectedImageLearning extends UnitTest
{

  /**
   * @author George Booth
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ExpectedImageLearning());
  }

  /**
   * @author George Booth
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    methodTest();
    functionalTest();
  }

  /**
   * Test method actions
   * @author George Booth
   */
  private void methodTest()
  {
    int width = 40;
    int height = 40;

    // constructor requires width and height > 0
    try
    {
      ExpectedImageLearning expectedImageLearning = new ExpectedImageLearning(0, height);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    try
    {
      ExpectedImageLearning expectedImageLearning = new ExpectedImageLearning(width, 0);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    // good constructor
    ExpectedImageLearning expectedImageLearning = new ExpectedImageLearning(width, height);

    // Pad must be set before get
    try
    {
      expectedImageLearning.getPad();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    // Slice name enum must be set before get
    try
    {
      expectedImageLearning.getSliceNameEnum();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    // number of learned images should be zero
    int numImages = expectedImageLearning.getNumberOfExpectedImagesLearned();
    Assert.expect(numImages == 0);

    // the composite expected image array doesn't exist until an image is added
    Assert.expect(expectedImageLearning.hasCompositeExpectedImageArray() == false);

    // can't get composite expected image array until it has been created
    try
    {
      float [] badArray = expectedImageLearning.getCompositeImageArray();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    // number of tested pixels should be uninitialized
    Assert.expect(expectedImageLearning.hasNumberOfTestedPixels() == false);

    // can't get number of pixels if not initialized
    try
    {
      float numPixels = expectedImageLearning.getNumberOfTestedPixels();
      Expect.expect(false);
    }
    catch (AssertException ae)
    {
      // do nothing
    }

    // can't add a null pixel array
    try
    {
      expectedImageLearning.addLearnedExpectedImagePixelArray(width, height, null);
      Expect.expect(false);
    }
    catch (AssertException ae)
    {
      // do nothing
    }

    float[] dummyArray = new float[20];
    // width and height must match constructor values
    try
    {
      expectedImageLearning.addLearnedExpectedImagePixelArray(0, height, dummyArray);
      Expect.expect(false);
    }
    catch (AssertException ae)
    {
      // do nothing
    }

    try
    {
      expectedImageLearning.addLearnedExpectedImagePixelArray(width, 0, dummyArray);
      Expect.expect(false);
    }
    catch (AssertException ae)
    {
      // do nothing
    }

    // length of pixel array must match width and height
    try
    {
      expectedImageLearning.addLearnedExpectedImagePixelArray(width, height, dummyArray);
      Expect.expect(false);
    }
    catch (AssertException ae)
    {
      // do nothing
    }

    // create a blank image (no tested pixels)
    Image blankImage = new Image(width, height);
    for (int y = 0; y < height; y++)
      for (int x = 0; x < width; x++)
      {
        blankImage.setPixelValue(x, y, 0.0f);
      }

    // add it to composite
    RegionOfInterest imageRoi = new RegionOfInterest(0, 0, width, height, 0, RegionShapeEnum.RECTANGULAR);
    float[] blankArray = Image.createArrayFromImage(blankImage, imageRoi);
    expectedImageLearning.addLearnedExpectedImagePixelArray(width, height, blankArray);

    // number of learned images should be 1
    numImages = expectedImageLearning.getNumberOfExpectedImagesLearned();
    Assert.expect(numImages == 1);

    // the composite expected image array exists
    Assert.expect(expectedImageLearning.hasCompositeExpectedImageArray() == true);

    // number of tested pixels should be initialized and still be 0
    Assert.expect(expectedImageLearning.hasNumberOfTestedPixels() == true);
    Assert.expect(expectedImageLearning.getNumberOfTestedPixels() == 0);

    // add one tested and one untested pixel
    blankImage.setPixelValue(width / 2, height / 2, 1.0f);
    blankImage.setPixelValue(width / 3, height / 3, 0.0f);
    float[] onePixelArray = Image.createArrayFromImage(blankImage, imageRoi);
    expectedImageLearning.addLearnedExpectedImagePixelArray(width, height, onePixelArray);

    // number of learned images should be 2
    numImages = expectedImageLearning.getNumberOfExpectedImagesLearned();
    Assert.expect(numImages == 2);

    // number of tested pixels should be 1
    Assert.expect(expectedImageLearning.getNumberOfTestedPixels() == 1);

    // add more at boundaries
    blankImage.setPixelValue(width / 2, height / 2, 0.0f);
    blankImage.setPixelValue(0, 0, 10.0f);
    blankImage.setPixelValue(width-1, 0, 50.0f);
    blankImage.setPixelValue(0, height-1, 100.0f);
    blankImage.setPixelValue(width-1, height-1, 150.0f);
    float[] fourPixelArray = Image.createArrayFromImage(blankImage, imageRoi);
    expectedImageLearning.addLearnedExpectedImagePixelArray(width, height, fourPixelArray);

    // number of learned images should be 3
    numImages = expectedImageLearning.getNumberOfExpectedImagesLearned();
    Assert.expect(numImages == 3);

    // number of tested pixels should be 5
    Assert.expect(expectedImageLearning.getNumberOfTestedPixels() == 5);

    // clear learned images
    expectedImageLearning.clearCompositeExpectedImageArray(width, height);

    // the composite expected image array doesn't exist
    Assert.expect(expectedImageLearning.hasCompositeExpectedImageArray() == false);

    // number of tested pixels should be uninitialized
    Assert.expect(expectedImageLearning.hasNumberOfTestedPixels() == false);

    // see if clearing did a good job
    expectedImageLearning.addLearnedExpectedImagePixelArray(width, height, fourPixelArray);

    // number of learned images should be 1
    numImages = expectedImageLearning.getNumberOfExpectedImagesLearned();
    Assert.expect(numImages == 1);

    // number of tested pixels should be 4
    Assert.expect(expectedImageLearning.getNumberOfTestedPixels() == 4);
  }

  /**
   * Create and check a composite image
   * @author George Booth
   */
  private void functionalTest()
  {
    int width = 40;
    int height = 40;

    // create a blank (dark) image and an test result image for comparison
    Image blankImage = new Image(40, 40);
    Image testResultImage = new Image(40, 40);
    for (int y = 0; y < height; y++)
      for (int x = 0; x < width; x++)
      {
        blankImage.setPixelValue(x, y, 0.0f);
        testResultImage.setPixelValue(x, y, 0.0f);
      }

    // create a image with a dark gray area at 10,10 to 25,20
    Image image1 = new Image(40, 40);
    for (int y = 0; y < height; y++)
      for (int x = 0; x < width; x++)
        if (x >= 10 && y >= 10 && x <= 25 && y <= 20)
        {
          image1.setPixelValue(x, y, 50.0f);
          testResultImage.setPixelValue(x, y, 50.0f);
        }
        else
          image1.setPixelValue(x, y, 0.0f);


    // create a image with a medium gray area at 20,10 to 35,20
    Image image2 = new Image(40, 40);
    for (int y = 0; y < height; y++)
      for (int x = 0; x < width; x++)
        if (x >= 20 && y >= 10 && x <= 35 && y <= 20)
        {
          image2.setPixelValue(x, y, 100.0f);
          testResultImage.setPixelValue(x, y, 100.0f);
        }
        else
          image2.setPixelValue(x, y, 0.0f);

    // create a image with a light gray area at 15,15 to 25,30
    Image image3 = new Image(40, 40);
    for (int y = 0; y < height; y++)
      for (int x = 0; x < width; x++)
        if (x >= 15 && y >= 15 && x <= 25 && y <= 30)
        {
          image3.setPixelValue(x, y, 150.0f);
          testResultImage.setPixelValue(x, y, 150.0f);
        }
        else
          image3.setPixelValue(x, y, 0.0f);

//    testResultImage.setPixelValue(37, 37, 151.0f);

    RegionOfInterest imageRoi = new RegionOfInterest(0, 0, width, height, 0, RegionShapeEnum.RECTANGULAR);

    // build a composite darkets pixel image of the above images
    ExpectedImageLearning expectedImageLearning = new ExpectedImageLearning(width, height);

    float[] blankArray = Image.createArrayFromImage(blankImage, imageRoi);
    expectedImageLearning.addLearnedExpectedImagePixelArray(width, height, blankArray);

    float[] array1 = Image.createArrayFromImage(image1, imageRoi);
    expectedImageLearning.addLearnedExpectedImagePixelArray(width, height, array1);

    float[] array2 = Image.createArrayFromImage(image2, imageRoi);
    expectedImageLearning.addLearnedExpectedImagePixelArray(width, height, array2);

    float[] array3 = Image.createArrayFromImage(image3, imageRoi);
    expectedImageLearning.addLearnedExpectedImagePixelArray(width, height, array3);

    // create the final image
    float[] finalArray = expectedImageLearning.getCompositeImageArray();
    Image finalImage = Image.createFloatImageFromArray(width, height, finalArray);
//    ImageDebugWindow.displayAndBlock(finalImage, "Expected Image");

    // check final image against expected result
    boolean matched = true;
    for (int y = 0; y < height; y++)
      for (int x = 0; x < width; x++)
        if (finalImage.getPixelValue(x, y) != testResultImage.getPixelValue(x, y))
        {
          System.out.println("FinalImage pixel value " + finalImage.getPixelValue(x, y) + " at x,y(" + x + "," + y +
                             ") does not equal expected testResultImage value of " + testResultImage.getPixelValue(x, y));
          matched = false;
        }

    Expect.expect(matched == true);

    // create a image with numeric values at each pixel
    width = 19;
    height = 11;
    Image image4 = new Image(width, height);
    for (int y = 0; y < height; y++)
      for (int x = 0; x < width; x++)
      {
        image4.setPixelValue(x, y, y * width + x);
//        System.out.println(x + ", " + y + " image = " + image4.getPixelValue(x, y));
      }

    matched = true;
    RegionOfInterest image4Roi = new RegionOfInterest(0, 0, width, height, 0, RegionShapeEnum.RECTANGULAR);
    float[] array4 = Image.createArrayFromImage(image4, image4Roi);
    int pixelIndex = 0;
    for (int y = 0; y < height; y++)
    {
      for (int x = 0; x < width; x++)
      {
        float imagePixel = image4.getPixelValue(x, y);
        float arrayPixel = array4[pixelIndex];
//        System.out.println(x + ", " + y + " image = " + image4.getPixelValue(x, y) + ", array = " + array4[pixelIndex]);
        if (imagePixel != arrayPixel)
        {
          System.out.println("pixel mismatch at " + x + ", " + y);
          matched = false;
        }
        pixelIndex++;
      }
    }

    if (matched)
    {
//      System.out.println("All pixels matched");
    }
  }

}
