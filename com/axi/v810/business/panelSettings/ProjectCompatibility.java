package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.testExec.InspectionEventObservable;
import com.axi.v810.business.testExec.MessageInspectionEvent;
import com.axi.v810.business.testGen.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ProjectCompatibility 
{
  
  private static InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();  
  /**
   * @author Andy Mechtenberg
   */
  static boolean doesProjectRequireCompatibilityModification(String projectVersion)
  {
    return doesProjectRequireCompatibilityModification(projectVersion, true);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  static boolean doesProjectRequireCompatibilityModification(String projectVersion, boolean isCompatibleWithAvailablePhysicalIrps)
  {
    Assert.expect(projectVersion != null);
    
    // at 1.13 we needed to modify all PTH subtypes to change the Use Predictive Slice setting to OFF    
    if (VersionUtil.isOlderVersion(projectVersion, "1.12") || VersionUtil.isRequiredVersion(projectVersion, "1.12"))
      return true;
    
    //Lim, Lay Ngor - added at the 5.6 release to fix the compatibility for 
    //FFT "Small Scale Filter" tuned value which larger than current maximum value.
    if (VersionUtil.isOlderVersion(projectVersion, "5.6"))
      return true;
    
    // XCR-2702 Software Always Save Recipe During Recipe Loading - Cheah Lee Herng    
    if (VersionUtil.isOlderVersion(projectVersion, "5.7") || isCompatibleWithAvailablePhysicalIrps == false)
      return true;
    
    //Siew Yeng - XCR-2843 - masking rotation
    if(VersionUtil.isOlderVersion(projectVersion, "5.8"))
      return true;
    
    //Siew Yeng - XCR-3603 - fix void detection does not show difference when change sensitivity for floodfill with masking
    if(VersionUtil.isOlderVersion(projectVersion, "5.10"))
      return true;
    
    return false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  static boolean doesProjectRequireCompatibilityModification(Project project)
  {
    Assert.expect(project != null);
    
    return doesProjectRequireCompatibilityModification(project.getSoftwareVersionOfProjectLastSave(),
                                                       ProgramGeneration.getInstance().isCompatibleWithAvailablePhysicalIrps(project.getTestProgram()));
  }
  
  /**
   * @author Andy Mechtenberg
   */
  static List<String> applyCompatibilityChanges(Project project) throws DatastoreException
  {
    List<String> compatibilityMessages = new LinkedList<String>();
    Assert.expect(project != null);
        
    String projectVersion = project.getSoftwareVersionOfProjectLastSave();
    
    // added at the 1.13 release to fix pre-1.13 projects
    if (VersionUtil.isOlderVersion(projectVersion, "1.12") || VersionUtil.isRequiredVersion(projectVersion, "1.12"))
      compatibilityMessages.add(setThroughHoleSubtypesToNotUsePredictiveSlice(project));
    
    // add for 64-bit IRP (a.k.a Series II)
    // If this piece of code is run on all Series II machine, this checking can be removed eventually.
    // XCR-2702 Software Always Save Recipe During Recipe Loading - Cheah Lee Herng
    if ((VersionUtil.isOlderVersion(projectVersion, "5.7") && Config.is64bitIrp()) ||
        (ProgramGeneration.getInstance().isCompatibleWithAvailablePhysicalIrps(project.getTestProgram()) == false && Config.is64bitIrp()))
    {
      compatibilityMessages.add(setSubtypesToUseSlowSearchSpeed(project));
      //compatibilityMessages.add(setDifferSolderThicknessVersion(project));
    }
    
    //Lim, Lay Ngor - added at the 5.6 release to fix the compatibility for 
    //FFT "Small Scale Filter" tuned value which larger than current maximum value.
    if (VersionUtil.isOlderVersion(projectVersion, "5.6"))
    {
      compatibilityMessages.add(setSubtypesToUseNewFFTSmallScaleFilterMaxValue(project));
      //Siew Yeng - XCR2168 - component level voiding for mixed subtype for Large Pad
      compatibilityMessages.add(setComponentVoidingThresholdForMixedSubtype(project));
    }
    
    if(VersionUtil.isOlderVersion(projectVersion, "5.8"))
    {
      //Siew Yeng - XCR-2843 - masking rotation
      compatibilityMessages.add(copyMaskImageIfNecessary(project));
      //Siew Yeng - XCR-3603 - remove this as it will be call at if condition below(projectVersion < 5.10)
      //Siew Yeng - XCR-3109
      //compatibilityMessages.add(setLargePadVoidingVersionIfNecessary(project, projectVersion));
    }
    
    if(VersionUtil.isOlderVersion(projectVersion, "5.10"))
    {
      //Siew Yeng - XCR-3603 - fix void detection does not show difference when change sensitivity for floodfill with masking
      compatibilityMessages.add(setLargePadVoidingVersionIfNecessary(project, projectVersion));
    }
    return compatibilityMessages;
  }
  
  /**
   * Starting at the 1.13 release, we added the Predictive Slice feature, where joints types
   * where we don't autofocus well can use neighbors to predict where a focused slice can be
   * generated.
   * There is a threshold "Predictive Slice Height" that is defaulted to ON for PTH and Pressfit.
   * However, for existing projects, keeping this ON has the downside of invalidating shorts learned data
   * and potentially slower thoughput (as a re-scan is necessary to do predictive slice). So, for projects
   * that have been created prior to 1.13, we want to find all the PTH subtypes and change their "Predictive
   * Slice Height" to OFF (away from the default) to ensure these projects behave the same as the did prior
   * to 1.13.
   * @author Andy Mechtenberg
   */
  private static String setThroughHoleSubtypesToNotUsePredictiveSlice(Project project)
  {
    Assert.expect(project != null);
    // these are applied during load so we don't want project state to invalide shorts learned data
    project.getProjectState().disable();
    List<Subtype> pthSubtypes = project.getPanel().getSubtypes(JointTypeEnum.THROUGH_HOLE);
    pthSubtypes.addAll(project.getPanel().getSubtypes(JointTypeEnum.OVAL_THROUGH_HOLE)); //Siew Yeng - XCR-3318 - Oval PTH
    
    for(Subtype subtype : pthSubtypes)
    {
      subtype.setTunedValue(AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT, new String("Off"));
    }
    project.getProjectState().enableAfterProjectLoad();
    if (pthSubtypes.isEmpty() == false)
    {
      // bump up the minor version number ONLY if any changes were made
      int minorVersion = project.getMinorVersion();
      project.setMinorVersion(++minorVersion);
      
      // notify with inspectionEventObservable the Test Execution flow
      MessageInspectionEvent event = new MessageInspectionEvent(new LocalizedString("BS_COMPATIBILITY_THROUGH_HOLE_SUBTYPE_THRESHOLD_CHANGE_TEST_EXECUTION_KEY",
                                                                                    new Object[]{project.getName()}));
      _inspectionEventObservable.sendEventInfo(event);

      // add in the test development message for display if loaded through Test Dev.
      return StringLocalizer.keyToString("BS_COMPATIBILITY_THROUGH_HOLE_SUBTYPE_THRESHOLD_CHANGE_KEY");
    }
    else
      return "";
  }
  
  /** 
   * @author Cheah Lee Herng 
   */
  private static String setSubtypesToUseSlowSearchSpeed(Project project)
  {
    Assert.expect(project != null);
    
    boolean isChanged = false;
    // these are applied during load so we don't want project state to invalide shorts learned data
    project.getProjectState().disable();
    List<Subtype> subtypes = project.getPanel().getSubtypes();
    for(Subtype subtype : subtypes)
    {
      if (subtype.isTuned() == false)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL);
        isChanged = true;
      }
    }
    project.getProjectState().enableAfterProjectLoad();
    if (isChanged)//isChanged will set to true only when subtype is available.
    {
      // bump up the minor version number ONLY if any changes were made
      int minorVersion = project.getMinorVersion();
      project.setMinorVersion(++minorVersion);
    }
    return "";
  }
  
  /**
   * @author Swee Yee Wong - new thickness table implementation
   */
  private static String setDifferSolderThicknessVersion(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    
    boolean isChanged = false;
    // these are applied during load so we don't want project state to invalide shorts learned data
    project.getProjectState().disable();
    
    isChanged = project.notifyUserOfDifferSolderThicknessVersion(project);
    
    project.getProjectState().enableAfterProjectLoad();
    if (isChanged)//isChanged will set to true only when solder thickness version is changed
    {
      // bump up the minor version number ONLY if any changes were made
      int minorVersion = project.getMinorVersion();
      project.setMinorVersion(++minorVersion);

      return StringLocalizer.keyToString(new LocalizedString("BS_COMPATIBILITY_SOLDER_THICKNESS_VERSION_CHANGED_TO_LATEST_KEY",
                                                                                    new Object[]{project.getName()}));
    }
    return "";
  }
  
  /**
   * Starting at the 5.6 release, we modified the maximum value of the 
   * IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SMALLSCALE algorithm setting from 1000
   * to 100. This is to avoid the system crash due to VxImageEnhancer failed to 
   * process the image correctly when user pass in FFT Small Scale value that
   * too large. 
   * So for project that have been created prior to 5.6, we want to find all 
   * subtypes and change their "Small Scale Filter" to current maximum value which 
   * is 100 if its original tuned value is greater than the current maximum value(100).
   * 
   * @author Lim, Lay Ngor
   */
  private static String setSubtypesToUseNewFFTSmallScaleFilterMaxValue(Project project)
  {
    Assert.expect(project != null);
    
    boolean isChanged = false;
    // these are applied during load so we don't want project state to invalide shorts learned data
    project.getProjectState().disable();
    List<Subtype> subtypes = project.getPanel().getSubtypes();
    for (Subtype subtype : subtypes)
    {
      final int tunedValue = (int) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SMALLSCALE);
      final int maxValue = (int) subtype.getAlgorithmSettingMaximumValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SMALLSCALE);
      if (tunedValue > maxValue)
      {
        subtype.setSettingToMaximumValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SMALLSCALE);
        isChanged = true;
      }
    }
    project.getProjectState().enableAfterProjectLoad();
    if (isChanged)
    {
      // bump up the minor version number ONLY if any changes were made
      int minorVersion = project.getMinorVersion();
      project.setMinorVersion(++minorVersion);
      return StringLocalizer.keyToString("BS_COMPATIBILITY_IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SUBTYPE_THRESHOLD_CHANGE_KEY");
    }
    
    return "";
  }  
  
  /** 
   * Currently only handle for LargePad Family
   * @author Siew Yeng
   */
  private static String setComponentVoidingThresholdForMixedSubtype(Project project)
  {
    Assert.expect(project != null);

    project.getProjectState().disable();
    List<ComponentType> componentTypes = project.getPanel().getComponentTypes();
    Set<Subtype> modifiedSubtypes = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    for(ComponentType compType : componentTypes)
    {
      if(compType.getSubtypes().size() == 1 || 
        modifiedSubtypes.containsAll(compType.getSubtypes()))
        continue;
      
      //check for mixed subtype only
      float componentVoidingThreshold = -1; 
      for(Subtype subtype : compType.getSubtypes())
      {
        if (subtype.getInspectionFamilyEnum() == InspectionFamilyEnum.LARGE_PAD && 
          subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_COMPONENT_PERCENT_PAD) == false)
        {
          componentVoidingThreshold = (float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_COMPONENT_PERCENT_PAD);
          break;
        }
      }
      
      if(componentVoidingThreshold != -1)
      {
        for(Subtype subtype : compType.getSubtypes())
        {
          if (subtype.getInspectionFamilyEnum() == InspectionFamilyEnum.LARGE_PAD &&
            (float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_COMPONENT_PERCENT_PAD) != componentVoidingThreshold)
          {
            subtype.setTunedValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_COMPONENT_PERCENT_PAD,componentVoidingThreshold);
            modifiedSubtypes.add(subtype);
          }
        }
      }
    }
    
    project.getProjectState().enableAfterProjectLoad();
    if (modifiedSubtypes.isEmpty() == false)
    {
      // bump up the minor version number ONLY if any changes were made
      int minorVersion = project.getMinorVersion();
      project.setMinorVersion(++minorVersion);
      
      // notify with inspectionEventObservable the Test Execution flow
      MessageInspectionEvent event = new MessageInspectionEvent(new LocalizedString("BS_COMPATIBILITY_LARGE_PAD_SUBTYPE_THRESHOLD_CHANGE_TEST_EXECUTION_KEY",
                                                                                    new Object[]{project.getName()}));
      _inspectionEventObservable.sendEventInfo(event);

      // add in the test development message for display if loaded through Test Dev.
      return StringLocalizer.keyToString(new LocalizedString("BS_COMPATIBILITY_LARGE_PAD_SUBTYPE_COMPONENT_VOIDING_THRESHOLD_CHANGE_KEY",
                                                                                    new Object[]{modifiedSubtypes}));
    }
    return "";
  }
  
  /** 
   * Rename old mask image name to new mask image name(with rotation)
   * @author Siew Yeng
   */
  private static String copyMaskImageIfNecessary(Project project)
  {
    Assert.expect(project != null);
    
    boolean isRenamed = false;
    
    //Siew Yeng - XCR-2937 - software crash when loaded subtype is not an inspected subtype(not in use subtype)
    for(Subtype subtype : project.getPanel().getInspectedSubtypes())
    {
      if(subtype.getInspectionFamilyEnum().equals(InspectionFamilyEnum.LARGE_PAD) == false)
        continue;
      
      double rotation = subtype.getPads().get(0).getComponent().getDegreesRotationAfterAllRotations();
      
      if(copyMaskImage(project.getName(), subtype, rotation))
        isRenamed = true;
      
      if(subtype.hasSubSubtypes())
      {
        for(Subtype subSubtype : subtype.getSubSubtypes())
        {
          if(copyMaskImage(project.getName(), subSubtype, rotation))
            isRenamed = true;
        }
      }
    }
    
    if(isRenamed)
    {
      String message = StringLocalizer.keyToString("BS_COMPATIBILITY_MASK_IMAGE_NAME_CHANGE_KEY");
      
      if(project.getPanel().isMultiBoardPanel())
      {
        double firstBoardRotation = -1;
        for(Board board : project.getPanel().getBoards())
        {
          if(firstBoardRotation == -1)
          {
            firstBoardRotation = board.getDegreesRotationAfterAllRotations();
            continue;
          }

          if(firstBoardRotation != board.getDegreesRotationAfterAllRotations())
          {
            message += StringLocalizer.keyToString("BS_COMPATIBILITY_MASK_IMAGE_NAME_UPDATE_INCORRECT_FOR_MULTIBOARD_WARNING_KEY");
            break;
          }
        }
      }
      return message;
    }
    
    return "";
  }
  
  /** 
   * Rename old mask image name to new mask image name(with rotation)
   * @author Siew Yeng
   */
  private static boolean copyMaskImage(String projName, Subtype subtype, double rotation)
  {
    String oldMaskImagePath = FileName.getMaskImageFullPath(projName, subtype.getShortName());
    if(FileUtil.exists(oldMaskImagePath))
    {
      //Siew Yeng - XCR-3033 - Always prompt update mask  images message box when load recipe (only happened on 5.6ex)
      List<String> maskImagePathList = FileUtil.listFiles(Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()), 
                                                          FileName.getMaskImagePatternString(subtype.getShortName()));
      if(maskImagePathList.isEmpty())
      {
        String newMaskImagePath = FileName.getMaskImageWithRotationFullPath(projName, subtype.getShortName(), rotation);
        try 
        {
          FileUtilAxi.copy(oldMaskImagePath, newMaskImagePath);
          return true;
        } 
        catch (DatastoreException ex) 
        {
          System.out.println(ex.getMessage());
        }
      }
    }
    return false;
  }
  
  /** 
   * @param projectVersion Voiding Version is 1 if projectVersion older than 5.8
   *                       Voiding Version is 2 if projectVersion newer or equals to 5.8 AND 
   *                                               older than 5.10 with condition Voiding Version is default version  
   * @author Siew Yeng
   */
  private static String setLargePadVoidingVersionIfNecessary(Project project, String projectVersion)
  {
    Assert.expect(project != null);
    
    project.getProjectState().disable();
    List<Subtype> modifiedSubtypes = new ArrayList();
    
    boolean isVoidVersionOne = VersionUtil.isOlderVersion(projectVersion, "5.8");
    
    for(Subtype subtype : project.getPanel().getInspectedSubtypes())
    {
      if(subtype.getInspectionFamilyEnum().equals(InspectionFamilyEnum.LARGE_PAD) == false)
        continue;
      
      if(isVoidVersionOne)
      {
        //Siew Yeng - XCR-3109
        if(subtype.hasSubSubtypes() == false)
        {
          subtype.setTunedValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VERSION, "1");
          modifiedSubtypes.add(subtype);
        }
      }
      else
      {
        //Siew Yeng - XCR-3603
        if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VERSION))
        {
          subtype.setTunedValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VERSION, "2");
          modifiedSubtypes.add(subtype);
        }
      }
    }
    project.getProjectState().enableAfterProjectLoad();
    if (modifiedSubtypes.isEmpty() == false)
    {
      // bump up the minor version number ONLY if any changes were made
      int minorVersion = project.getMinorVersion();
      project.setMinorVersion(++minorVersion);
      
      return StringLocalizer.keyToString(new LocalizedString("BS_COMPATIBILITY_LARGE_PAD_VOIDING_ALGORITHM_VERSION_NUMBER_CHANGE_KEY",
                                                                                    new Object[]{modifiedSubtypes}));
    }
    
    return "";
  }
}
