package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author sham
 */
public class UserGainEnum extends com.axi.util.Enum
{
  private static Map<Integer, UserGainEnum> _userGainToEnumMap = new HashMap<Integer, UserGainEnum>();
  private static List<UserGainEnum> _orderedUserGain = new ArrayList<UserGainEnum>();
  private static List<String> _orderedUserGainNameList = new ArrayList<String>(); 
  private int _userGain;
  private String _solderThicknessSubFolderName = null;
  private static int _index = -1;
  public static UserGainEnum ONE = new UserGainEnum(++_index, 1, "Gain1");
  public static UserGainEnum TWO = new UserGainEnum(++_index, 2, "Gain2");
  public static UserGainEnum THREE = new UserGainEnum(++_index, 3, "Gain3");
  public static UserGainEnum FOUR = new UserGainEnum(++_index, 4, "Gain4");
  public static UserGainEnum FIVE = new UserGainEnum(++_index, 5, "Gain5");
  public static UserGainEnum SIX = new UserGainEnum(++_index, 6, "Gain6");
  public static UserGainEnum SEVEN = new UserGainEnum(++_index, 7, "Gain7");
  public static UserGainEnum EIGHT = new UserGainEnum(++_index, 8, "Gain8");
  public static UserGainEnum NINE = new UserGainEnum(++_index, 9, "Gain9");

    /**
   * @author sham
   */
  private UserGainEnum(int id, int userGain, String solderThicknessSubFolderName)
  {
    super(id);
    _userGain = userGain;
    _solderThicknessSubFolderName = solderThicknessSubFolderName;
    Directory.addSolderThicknessLayer2DirList(solderThicknessSubFolderName);
    _userGainToEnumMap.put(userGain, this);
    _orderedUserGainNameList.add(Integer.toString(userGain));
    _orderedUserGain.add(this);
  }

  /*
   * @author sham
   */
  public double toDouble()
  {
    return Double.valueOf(_userGain);
  }
  
  /*
   * @author swee yee wong
   */
  public int toInt()
  {
    return _userGain;
  }

  /**
   * @author sham
   */
  public String toString()
  {
    return String.valueOf(_userGain);
  }
  
  /**
   * @author swee yee wong
   */
  public String getSolderThicknessSubFolderName()
  {
    Assert.expect(_solderThicknessSubFolderName != null);
    return _solderThicknessSubFolderName;
  }

  /**
   * @author sham
   */
  public static UserGainEnum getUserGainToEnumMapValue(int key)
  {
    Assert.expect(key > 0);

    return _userGainToEnumMap.get(key);
  }

  /**
   * @author Chong, Wei Chin
   */
  public static List<UserGainEnum> getOrderedUserGain()
  {
    return new ArrayList<UserGainEnum>(_orderedUserGain);
  }
  
  public static List<String> getOrderedUserGainName()
  {
    return _orderedUserGainNameList;
  }
}
