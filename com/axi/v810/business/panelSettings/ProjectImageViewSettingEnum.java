package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 *
 * @author swee-yee.wong
 */
public class ProjectImageViewSettingEnum extends com.axi.util.Enum
{
  private static int _index = 0;
  private String _name;
  private static java.util.List<ProjectImageViewSettingEnum> _projectImageViewSettingEnumList = new ArrayList<ProjectImageViewSettingEnum>();
  private java.util.List<String> _selectionList = new ArrayList<String>();
 
  public static ProjectImageViewSettingEnum LARGE_VIEW_IMAGE = new ProjectImageViewSettingEnum(_index++, "Generate large view images", null);
  public static ProjectImageViewSettingEnum MULTI_ANGLE_IMAGE = new ProjectImageViewSettingEnum(_index++, "Generate 2.5D images", null);
  public static ProjectImageViewSettingEnum COMPONENT_IMAGE = new ProjectImageViewSettingEnum(_index++, "Generate component images during production", null);
  public static ProjectImageViewSettingEnum DYNAMIC_RANGE_OPTIMIZATION_VERSION = new ProjectImageViewSettingEnum(_index++, "DRO version", DynamicRangeOptimizationVersionEnum.getAllDynamicRangeOptimizationVersionEnumListInString());
  /**
   *
   * @author swee-yee.wong
   */
  private ProjectImageViewSettingEnum(int id, String name, java.util.List<String> selectionList)
  {
    super(id);
    Assert.expect(name != null);

    _name = name.intern();
    _selectionList = selectionList;
  }
  
  /**
   *
   * @author swee-yee.wong
   */
  public String getName()
  {
    Assert.expect(_name != null);
    return _name;
  }
  
  /**
   *
   * @author swee-yee.wong
   */
  public java.util.List<String> getSelectionList()
  {
    return _selectionList;
  }
  
  /**
   *
   * @author swee-yee.wong
   */
  public static java.util.List<ProjectImageViewSettingEnum> getAllProjectImageViewSettingList()
  {
    if (_projectImageViewSettingEnumList != null)
      _projectImageViewSettingEnumList.clear();
    
    _projectImageViewSettingEnumList.add(LARGE_VIEW_IMAGE);
    _projectImageViewSettingEnumList.add(MULTI_ANGLE_IMAGE);
    //_projectImageViewSettingEnumList.add(COMPONENT_IMAGE);
    _projectImageViewSettingEnumList.add(DYNAMIC_RANGE_OPTIMIZATION_VERSION);
    
    return _projectImageViewSettingEnumList;
  }
}


