package com.axi.v810.business.panelSettings;

import com.axi.guiUtil.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import static com.axi.v810.business.ImageManager.getInstance;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.algorithmLearning.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.ndfReaders.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.business.imageAcquisition.ImagingChainProgramGenerator;
import com.axi.v810.business.imageAcquisition.ImageAcquisitionModeEnum;
import com.axi.v810.business.license.*;
import com.axi.v810.hardware.autoCal.HardwareTaskEngine;
/**
 * This class contains all project level information.  A project consists of
 * all the information used to inspect a panel.
 *
 * @author Bill Darbie
 */
public class Project implements Serializable, Observer
{
  // created by PanelNdfReader, ProjectSettingsReader

  private transient static Project _currentProject;
  private transient static ProjectWriter _projectWriter;
  private transient static ProjectReader _projectReader;
  private transient static MainReader _mainReader;
  private transient static ProgramGeneration _programGeneration;
  private transient static ProjectObservable _projectObservable;
  private transient static ImageManager _imageManager;
  // Bee Hoon, XCR1650 - Open recipe and immediately save recipe, it will crash
  private transient static ProjectErrorLogUtil _projectErrorLogUtil;

  private transient AlgorithmShortsLearningReaderWriter _algorithmShortsLearning;
  private transient AlgorithmSubtypesLearningReaderWriter _algorithmSubtypeLearning;
  private transient AlgorithmExpectedImageLearningReaderWriter _algorithmExpectedImageLearning;
  private transient AlgorithmBrokenPinLearningReaderWriter _algorithmBrokenPinLearning;//Broken Pin
  private transient AlgorithmExpectedImageTemplateLearningReaderWriter _algorithmExpectedImageTemplateLearning;//ShengChuan - Clear Tombstone
  
  private ProjectState _projectState;

  // _name is transient because it is based on the directory name
  private transient String _name; // MainReader, ProjectReader
  private Panel _panel; // PanelNdfReader, PanelReader
  private String _notes; // PanelNdfReader and ProjectNdfReader, ProjectSettingsReader

  // initial threshold settings
  private String _initialThresholdsSetName;

  private String _initialRecipeSettingName;
  // version of this project
  private int _programGenerationVersion = 0;
  private int _testProgramVersion = 1; // ProjectSettingsReader
  private int _minorVersion = 0; // ProjectSettingsReader
  private int _verificationImagesVersion = 1;

  // the software revision that the program data was created with
  private String _softwareVersionOfLastSave = null; // PanelNdfReader, ProjectSettingsReader

  private long _cadXmlChecksum;
  private long _settingsXmlChecksum;

  private String _targetCustomerName = "";
  private String _programmerName = "";

  private long _lastModificationTimeInMillis;

  private MathUtilEnum _displayUnits = MathUtilEnum.MILLIMETERS; // PanelNdfReader, ProjectSettingsReader
  private BooleanRef _readyForProduction;

  private TestProgram _testProgram;
  private boolean _saveInProgress = false;

  private int _databaseVersion = 0;
  private String _databaseComment = "";

  private static List<String> _compatibilityMessages = new LinkedList<String>();

  private SystemTypeEnum _systemType;
  // Will be used when creating HomogenousImageGroup object (in constructor) later
  private ScanPathMethodEnum _scanPathMethod;
  // Determine the project are to generate by new scan route method or not, XCR1529, New Scan Route, khang-wah.chnee
  private boolean _isGeneratedByNewScanRoute = false;
  private boolean _isVarDivNEnable = false;
  private boolean _isUseLargeTomoAngle = false;
  // Specified a specific scanroute.csvconfig to use, XCR1529, New Scan Route, khang-wah.chnee
  private String _scanRouteConfigFileName = FileName.getScanRouteConfigFile();
  
  //Ngie Xing, added for Systems that can have more than one Low or High Magnification
  private String _projectLowMagnification = "";
  private String _projectHighMagnification = "";

  private boolean _representsInternalSystemFeatures;
  private transient boolean _cancelSaveAs = false;
  private static boolean _displayTimes = false;

  private static BooleanLock _waitForUserResponseLock = new BooleanLock();
  private static boolean _proceedWithLoad = false;
  private static boolean _loadSaveAsNewProject = false;
  private static boolean _loadWithNewSolderThicknessVersion = false;
  private static String _newProjectName;
  private boolean _useScanStepOptimization = true;
  private boolean _checkLatestTestProgram = true;
  private static boolean _printOpticalRegionInfo = Config.isDeveloperDebugModeOn();
  private static boolean _isDummyProject = false;
  private static transient ProjectHistoryLog _fileWriter;
  
  private boolean _isEnableLargeViewImage = false;
  private boolean _isGenerateComponentImage = false;
  private boolean _isGenerateMultiAngleImage = false;
  private int _panelExtraClearDelayInMiliSeconds = 0;
  
  private int _componentMagType = -1;//-1 unkown
                                     //0 Low Mag Only
                                     //1 High Mag Only
                                     //2 Mix High and Low Mag 
  
  //Kee Chin Seong - Component to Focus Region Map
  private Map<Component, Map<String, List<FocusRegion>>> _componentToFocusRegionMap = new HashMap<Component, Map<String, List<FocusRegion>>>();

  // Single IRP - Bee Hoon
  private boolean _isReuseProjectionAvailableOption = false;
  
  //Siew Yeng - Enlarge Reconstruction
  private boolean _isEnlargeReconstructionRegion = false;
  //Siew Yeng - XCR-3599 - Store ProcessorStrip Overlap size and Max InspectionRegion at Project level
  //                       as these variables will be different for each project when inspection region size is enlarged
  //Enlarge Reconstruction - ProcessorStrip
  private int _overlapNeededForLargestPossibleJointInNanometers = -1; 
  //Enlarge Reconstruction - ReconstructionRegion
  private int _currentMaxInspectionRegionWidthInNanometers = -1;
  private int _currentMaxInspectionRegionLengthInNanometers = -1;
  private int _currentMaxHighMagInspectionRegionWidthInNanometers = -1;
  private int _currentMaxHighMagInspectionRegionLengthInNanometers = -1;
  
  //Swee Yee
  private boolean _isBGAJointBasedThresholdInspectionEnabled = false;
  
  //swee-yee.wong
  private boolean _isBGAInspectionRegionSetTo1X1 = false;
  private boolean _isProjectBasedBypassMode = false;
  
  //Khaw Chek Hau - XCR2603: Corrupted recipe unable to load
  private static boolean _isSummaryUpdateNeeded = false;
  private String _currentSelectedVariationName = VariationSettingManager.ALL_LOADED;
  private String _currentEnabledVariationName = VariationSettingManager.ALL_LOADED;
  private boolean _enablePreSelectVariation = true; //Kok Chun, Tan - variation management
  private boolean _isUpdateComponentNoLoadSetting = true;
  private static Map<String, String> _projectNameToSystemTypeMap = null;
  //Swee Yee
  private int _thicknessTableVersion = -1;
  private DynamicRangeOptimizationVersionEnum _dynamicRangeOptimizationVersion = DynamicRangeOptimizationVersionEnum.VERSION_0;
  //Khaw Chek Hau - XCR2943: Alignment Fail Due to Too Close To Edge
  private boolean _isEnlargeAlignmentRegion = false;
  
  //Kee Chin Seong - Project Surface Map Speed up loading
  private boolean _isSurfaceMapPointPopulated = false;
  
  private SystemTypeEnum _saveAsSystemType = null;
  private String _saveAsLowMagnification = null;
  private SystemTypeEnum _previousSystemType = null;
  private static boolean _isProjectConvertedFromDifferentSystemType = false;
  private static final String _M19 = "M19";
  private static final String _M23 = "M23";
  private static final String _M11 = "M11";
  /**
   * @author Bill Darbie
   */
  static
  {
    setupProjectReaderWriter();
  }

  /**
   * @author Bill Darbie
   */
  public Project(boolean representsInternalSystemFeatures)
  {
    _representsInternalSystemFeatures = representsInternalSystemFeatures;
    _projectState = new ProjectState();

    if (_representsInternalSystemFeatures == false)
    {
      _projectObservable.setProjectState(_projectState);
      _projectObservable.stateChanged(this, null, this, ProjectEventEnum.CREATE_OR_DESTROY);
    }
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.stateChanged(this, this, null, ProjectEventEnum.CREATE_OR_DESTROY);
  }
  
  /**
   * @author Anthony Fong
   */
  public ProjectReader  getProjectReader()
  {
    return _projectReader;
  }
  
  /**
   * Do not load the same project if it has already been loaded.  Note that this method assumes
   * that the project has not been changed on disk.  It will not load a different project with
   * the same name.  To force a load no matter what use the load() method.
   *
   * @author Bill Darbie
   */
  public static Project loadIfNotAlreadyLoaded(String projectName, BooleanRef abortedDuringLoad) throws XrayTesterException
  {
    Assert.expect(projectName != null);

    if (ProjectDatabase.isAutoUpdateLocalProjectEnabled())
    {
      if (ProjectDatabase.doesNewerProjectExist(projectName))
      {
        // CR1058 - DS_ERROR_FILE_CANNOT_BE_RENAMED_KEY error
        // In ProjectDatabase.java, inside backupLocalProject(...)function.
        // It will give cannot rename error when try to move "algorithmLearning" folder to backup folder.
        // Steps to reproduce :
        // 1) This will happen only when after run once on the project(for example 1.0) in Test Execution.
        // 2) Enable auto-pull latest project feature in "Software Options" --> "General" tab.
        // 3) Run Test Execution again with the same project (for example 1.0), and it will try to pull the latest project (for example 1.1) from database.
        // Explanation :
        // The first time it runs test execution, it opens the project, and created temporary learning files in "algorithmLearning" folder.
        // The learning files are still open as long as the project is not closed.
        // After enable auto-pull feature, run test execution & choose the same project, it found the latest project.
        // It tries to backup the project first before loading the latest one, but the learning files are still open.
        // So this gives the error. The older project will only close when try to load new project later.
        // Solution : Unload current project before get project from project database.
        //XCR-3364
        if (ProjectDatabase.isContinueWithoutDatabase() == false)
        {
          unloadCurrentProject();
          ProjectDatabase.getProject(projectName);
          load(projectName, abortedDuringLoad);
        }
      }
    }
    Project project = null;
    if (isCurrentProjectLoaded() && (getCurrentlyLoadedProject().getName().equals(projectName)))
      project = getCurrentlyLoadedProject();
    else
    {
      project = load(projectName, abortedDuringLoad);
      if(abortedDuringLoad.getValue())
        return null;
    }
    Assert.expect(project != null);
    return project;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public static Project loadCadCreatorProgram(String projectName, BooleanRef abortDuringLoad) throws DatastoreException
  {
    Project project = null;
    
    _projectObservable.setEnabled(false);
    try
    {
       project = CadCreatorProjectReader.getInstance().load(projectName);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    
    return project;
  }

  /**
   * Load the project from disk.
   * @author Bill Darbie
   */
  public static Project load(String projectName, BooleanRef abortDuringLoad) throws DatastoreException
  {
    Assert.expect(projectName != null);

    MemoryUtil.freeMemory();
    TimerUtil timer = new TimerUtil();
    timer.start();

    boolean fastLoad = true;

    Project project = null;
    //Siew Yeng - XCR-3131 - clear message when unload project
    //_compatibilityMessages.clear();
    Project oldValue = _currentProject;
    
    //Khaw Chek Hau - XCR2603: Corrupted recipe unable to load
    _isSummaryUpdateNeeded = false;

    _projectObservable.setEnabled(false);
    try
    {
      // making sure all calibration and configuration information loaded.
      loadCalAndConfigInformationForUnitTest();

      unloadCurrentProject();

      // Kok Chun, Tan - XCR-2519
      try
      {
        _projectObservable.setEnabled(true);
        // XCR-3293 - Kok Chun, Tan - if the map do not have this key, add it.
        if (_projectNameToSystemTypeMap.containsKey(projectName) == false)
          setProjectNameToSystemType(projectName, ProjectReader.getInstance().readProjectSystemType(projectName).getName());
        
        if (XrayTester.getSystemType().getName().equals(_projectNameToSystemTypeMap.get(projectName)) == false)
        {
          Pair<String, String> projectNameToSystemType = new Pair<String, String>();
          projectNameToSystemType.setPair(projectName, _projectNameToSystemTypeMap.get(projectName));
          _proceedWithLoad = false;
          if (UnitTest.unitTesting() == false)
          {
            // we are trying to load a project targeted for a different system
            // the  user may continue but they may need to re-learn shorts and re-tune
            // let's ask the user if they want to continue.
            _waitForUserResponseLock.setValue(false);
            _projectObservable.stateChanged(projectNameToSystemType, null, null, ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_SYSTEM_TYPE);
            waitForUserResponse();
          }

          if (_proceedWithLoad == false && _loadSaveAsNewProject == false)
          {
            abortDuringLoad.setValue(true);
            _currentProject = null;
            return null;
          }
          else if (projectName.equalsIgnoreCase(_newProjectName) && projectName.equals(_newProjectName) == false)
          {
            //Khaw Chek Hau - XCR3235: System failed to open converted recipe if the recipe name is same but different letter case
            String fromDirectory = Directory.getProjectDir(projectName);
            String toDirectory = Directory.getProjectDir(_newProjectName);
            String tempDir = null;

            if (FileUtilAxi.exists(fromDirectory))
            {
              tempDir = FileUtilAxi.getUniqueTempFile(toDirectory);
              FileUtilAxi.rename(fromDirectory, tempDir);
            }

            if (FileUtilAxi.exists(tempDir))
            {
              FileUtilAxi.rename(tempDir, toDirectory);
            }
            projectName = _newProjectName;       
          }
        }
        
        // first check if the project is converted from other system type
        // if not converted project, check current magnification is same with project magnification 
        // if different, load new config setting into software based on project magnification setting
        if (XrayTester.isOfflineProgramming())
        {
          if (_isProjectConvertedFromDifferentSystemType == false &&
              Config.getSystemCurrentLowMagnification().equals(ProjectReader.getInstance().readProjectLowMagnification(projectName)) == false &&
              (ProjectReader.getInstance().readProjectLowMagnification(projectName).equals(_M19)||ProjectReader.getInstance().readProjectLowMagnification(projectName).equals(_M23)))
          {
            String ProjectLowMagnification = ProjectReader.getInstance().readProjectLowMagnification(projectName);
            _waitForUserResponseLock.setValue(false);
            _projectObservable.stateChanged(ProjectLowMagnification, null, null,
              ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_MAGNIFICATION_FOR_OFFLINE_PROGRAMMING);
            waitForUserResponse();

            if (_proceedWithLoad == false)
            {
              abortDuringLoad.setValue(true);
              _currentProject = null;
              return null;
            }
          }
        }        
      }
      finally
      {
        //reset the value
        _isProjectConvertedFromDifferentSystemType = false;
        _projectObservable.setEnabled(false);
      }

      project = _projectReader.load(projectName);
      fastLoad = _projectReader.wasFastLoad();

      if (fastLoad)
      {
        project.setName(projectName);

        if (_printOpticalRegionInfo)
        {
          Panel panel = project.getPanel();
          boolean isPanelBasedAlignment = panel.getPanelSettings().isPanelBasedAlignment();

            for(TestSubProgram testSubProgram : project.getTestProgram().getAllInspectableTestSubPrograms())
            {
                System.out.println("Project Load: TestSubProgram Id " + testSubProgram.getId() + " has " + testSubProgram.getOpticalRegions().size() + " OpticalRegions and " +
                                                                testSubProgram.getFilteredComponentReferenceDesignators().size() + " filtered components.");
                for(OpticalRegion opticalRegion : testSubProgram.getOpticalRegions())
                {
                  List<Component> components = new ArrayList<Component>();
                  List<OpticalCameraRectangle> opticalCameraRectangles = new ArrayList<OpticalCameraRectangle>();
                  if (isPanelBasedAlignment)
                  {
                    components = opticalRegion.getComponents();
                    opticalCameraRectangles = opticalRegion.getAllOpticalCameraRectangles();
                  }
                  else
                  {
                    components = opticalRegion.getComponents(testSubProgram.getBoard());
                    opticalCameraRectangles = opticalRegion.getOpticalCameraRectangles(testSubProgram.getBoard());
                  }

                  System.out.println("Project Load:      Optical Region " + opticalRegion.getName() + " has " + opticalCameraRectangles.size() + 
                          " OpticalCameraRectangles and " + components.size() + " components.");

                  for(OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangles)
                  {
                      System.out.println("Project Load:          OpticalCameraRectangle " + opticalCameraRectangle.getName() + " has " + 
                              opticalCameraRectangle.getPanelCoordinateList().size() + " points.");
                  }

                  components.clear();
                  opticalCameraRectangles.clear();
                }

                for(Map.Entry<String,Integer> entry : testSubProgram.getOpticalRegionNameToSubProgramReference().entrySet())
                {
                    String referenceOpticalRegionName = entry.getKey();
                    int subProgramReferenceIdForOpticalRegion = entry.getValue();
                    List<Component> components = testSubProgram.getComponentsReferenceToSubProgramForOpticalRegion(referenceOpticalRegionName);

                    if (components == null || components.isEmpty())
                    {
                        System.out.println("Project Load: TestSubProgram Id " + testSubProgram.getId() + " has reference to TestSubProgram " + 
                            subProgramReferenceIdForOpticalRegion + " and optical region " + referenceOpticalRegionName + ".");
                    }
                    else
                    {
                        System.out.println("Project Load: TestSubProgram Id " + testSubProgram.getId() + " has reference to TestSubProgram " + 
                            subProgramReferenceIdForOpticalRegion + " and optical region " + referenceOpticalRegionName + " with " + components.size() + " reference components.");
                    }
                }
            }
            System.out.println();
        }
        
        //Siew Yeng - XCR-3599
        setupForEnlargeReconstructionRegionIfNecessary(project);
      }

      // set ProjectObservable to the current Project
      _projectObservable.setProjectState(project.getProjectState());

      Assert.expect(project != null);
      _currentProject = project;

      _projectObservable.addObserver(_currentProject);

      // now make a copy of learned files
      project.copyLearnedFiles();

      // Do any conversion necessary for converting legacy optical region
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP))
      {
        project.convertLegacyOpticalRegion();
      }

      project._projectState.enableAfterProjectLoad();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }

    // Must force recalculate each time after loading project if step size optimization is enabled.
    if ( ImagingChainProgramGenerator.getEnableStepSizeOptimization() )
      ImagingChainProgramGenerator.forceCalculateMaxStepSizeInNanometers(project.getPanel().getThicknessInNanometers(), project.getPanel().getMeansBoardZOffsetInNanometers());

    //swee yee wong - XCR-3004 Load recipe assert java.lang.NullPointerException
    //swee yee wong - XCR-3046 Alignment is cleared if attempt to unload and reload recipe after run production
    //move the verification step before fast load/ slow load to prevent any failing process to cause project become null
    boolean saveFullProject = false;
    boolean notifyUserAlternativeVersion = true;
    saveFullProject = project.isProjectSolderThicknessFilesAvailable(notifyUserAlternativeVersion) == false;
    
    // Swee-yee.wong
    // =====================================WARNING=================================================
    // PLEASE DO NOT PUT ANY PROCESS THAT WILL CAUSE EXCEPTION AFTER FAST LOAD/SLOW LOAD AS IT WILL CAUSE ASSERT WHEN PROJECT BECOME NULL
    // =====================================WARNING=================================================
    
    if (fastLoad)
      _projectObservable.stateChanged(project, oldValue, project, ProjectEventEnum.FAST_LOAD);
    else
      _projectObservable.stateChanged(project, oldValue, project, ProjectEventEnum.SLOW_LOAD);

    // Siew Yeng - XCR-2992 - check compatibility before save project
    // apply any automatic software version project changes here
    if (ProjectCompatibility.doesProjectRequireCompatibilityModification(project))
    {
      _compatibilityMessages = ProjectCompatibility.applyCompatibilityChanges(project);
      saveFullProject = true;
    }

    if (project.getSystemType().equals(XrayTester.getSystemType()) == false)
    {
      if (_proceedWithLoad == false)
      {
        if (project.getPanel().getPanelSettings().isLongPanel() && XrayTester.isXXLBased())
        {
          _projectObservable.stateChanged(project, null, null, ProjectEventEnum.LOADING_LONG_PANEL_WITH_DIFFERENT_SYSTEM_TYPE);
        }

        // this would save the project as a new project with the machine system type
        _currentProject.setSystemType(XrayTester.getSystemType());

        _currentProject.setProjectLowMagnification(Config.getSystemCurrentLowMagnification());
        _currentProject.setProjectHighMagnification(Config.getSystemCurrentHighMagnification());

        //Swee Yee Wong - XCR-3300 Thickness profile is capped at 100 in PTH subtype
        //New thickness table is not ready, will get flat response when reaching high deltagray value
        //Will change the default value back to 0 until the new thickness table is ready and stable.
        //_currentProject.setThicknessTableVersion(FileName.getLatestThicknessTableVersionFound());
        _currentProject.setThicknessTableVersion(0);
        
        //swee-yee.wong - XCR-3237 Intermittent software crash when generate precision tuning image
        try
        {
          project.isProjectSolderThicknessFilesAvailable(false);
        }
        catch(DatastoreException de)
        {
          //do nothing, try to copy all thickness table files and verify them when change system type to speed up
          //do not throw exception here because it might cause project become null
          //will do this verification process again during inspection
          System.out.println("Solder thickness file corrupted, it will try again during inspection");
        }

        // Clear PSP optical camera rectangle
        SurfaceMapping.getInstance().removeAllOpticalCameraRectangle(_currentProject);

        //Khaw Chek Hau - XCR3235: System failed to open converted recipe if the recipe name is same but different letter case
        if (_currentProject.getName().equalsIgnoreCase(_newProjectName) == false)
        {
            //Kee Chin Seong - need to destroy the test program for safety purpose
          //                 XXL might get as short if the std is long panel (long program as well).
          //                 Regenerate is the best.
          _currentProject.destroyTestProgram();
          _currentProject.getTestProgram();

          _currentProject.saveAs(_newProjectName, false);
        }
        else
          _currentProject.save(); //Siew Yeng - save recipe if projectName is the same, move this here to avoid recipe save two times

        _currentProject.loadProjectSystemTypeSettingToConfig();
        return _currentProject;
      }
      else
      {
        if (project.getPanel().getPanelSettings().isLongPanel() && XrayTester.isXXLBased())
        {
          _projectObservable.stateChanged(project, null, null, ProjectEventEnum.LOADING_LONG_PANEL_WITH_DIFFERENT_SYSTEM_TYPE);
        }
        // the user has decided to load the project although it is different from the machine.
        // we would need to load the project system type config into the memory
        _currentProject.loadProjectSystemTypeSettingToConfig();
        saveFullProject = true;
      }
    }
    else
    {
      // even the project and the machine system type is the same, but there might be posibility
      // that the system type setting in the memory is different
      _currentProject.loadProjectSystemTypeSettingToConfig();
    }
    
    //Yi Fong
    // XCR 3322 subtask: XCR 3702 -  Software crash when run test with different joint types after recipe conversion	  
    // to get the current project magnification  
    _imageManager.setPrevProjectLowMagnification(project.getProjectLowMagnification());
    _imageManager.setPrevProjectHighMagnification(project.getProjectHighMagnification());
    
    //Ngie Xing
    //Check whether the low and high magnification on last project save is the 
    //same as the system's current low and high magnification
    //force the user to redo manual alignment    
    if (isProjectAndSystemLowMagSame(project) == false)
    {
      String oldProjectLowMagnification = project.getProjectLowMagnification();
      String systemCurrentLowMagnification = Config.getSystemCurrentLowMagnification();
      _waitForUserResponseLock.setValue(false);
      _projectObservable.stateChanged(project, oldProjectLowMagnification, systemCurrentLowMagnification,
        ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_LOW_MAGNIFICATION);
      waitForUserResponse();
    
      if (_proceedWithLoad == false)
      {
        abortDuringLoad.setValue(true);
        _currentProject = null;
        project = null;
        return null;
      }
      project.setProjectLowMagnification(systemCurrentLowMagnification);
      _projectObservable.stateChanged(project, null, null, ProjectEventEnum.LOW_MAGNIFICATION_CHANGED);
      saveFullProject = true;   
      
      // XCR 3322 subtask: XCR 3702 -  Software crash when run test with different joint types after recipe conversion	  
      // to write in the project magnification info into those image set info without the magnification info
      List<ImageSetData> imageSetDatas = _imageManager.getImageSetData(project,true);
      for (ImageSetData imageSetData : imageSetDatas)
      {
        if (imageSetData.hasHighMagnification() == false || imageSetData.hasLowMagnification() == false)
        {
          ImageSetInfoWriter writer = new ImageSetInfoWriter();
          imageSetData.setHighMagnification(_imageManager.getPrevProjectHighMagnification());
          writer.write(imageSetData);
          imageSetData.setLowMagnification(_imageManager.getPrevProjectLowMagnification());
          writer.write(imageSetData);
        }
      }
    }

    if (isProjectAndSystemHighMagSame(project) == false)
    {
      String oldProjectHighMagnification = project.getProjectHighMagnification();
      String systemCurrentHighMagnification = Config.getSystemCurrentHighMagnification();
      _waitForUserResponseLock.setValue(false);
      _projectObservable.stateChanged(project, oldProjectHighMagnification, systemCurrentHighMagnification,
        ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_HIGH_MAGNIFICATION);
      waitForUserResponse();

      if (_proceedWithLoad == false)
      {
        abortDuringLoad.setValue(true);
        _currentProject = null;
        project = null;
        return null;
      }
      project.setProjectHighMagnification(systemCurrentHighMagnification);
      _projectObservable.stateChanged(project, null, null, ProjectEventEnum.HIGH_MAGNIFICATION_CHANGED);
      saveFullProject = true;
    }

    //Siew Yeng - move this block above
    // apply any automatic software version project changes here
//    if (ProjectCompatibility.doesProjectRequireCompatibilityModification(project))
//    {
//      _compatibilityMessages = ProjectCompatibility.applyCompatibilityChanges(project);
//      saveFullProject = true;
//    }

    //Khaw Chek Hau - XCR2603: Corrupted recipe unable to load
    if(saveFullProject || _isSummaryUpdateNeeded)
    {
      fastLoad = true; // to force skipping the program generation and save later
      if (project.isTestProgramValid() == false || _programGeneration.isCompatibleWithHardware(project.getTestProgram()) == false)
        project.destroyTestProgram();
      project.save();
    }

    boolean isCompatibleWithHardware = _programGeneration.isCompatibleWithHardware(project.getTestProgram());
    if (fastLoad == false || isCompatibleWithHardware == false)
    {
      // Jack Hwee - Discovered that when slow load, it will generate test program two times, 
      //             add a checking here so that it won't generate unnecessary test program.

      if (project.isTestProgramValid() && isCompatibleWithHardware)
      {
        // Bee Hoon, XCR1650 - Open recipe and immediately save recipe, it will crash
        if(_projectErrorLogUtil.getIsLandPatternNameNotFoundError() == false)
          project.saveAsSerializedFile();
      }
      else
      {
        // a slow, text file load will not contain the test program, so generate it here
        // and save the project to disk with the test program included
        // do this after the _projectObservable.stateChanged() call above so the state machine
        // will be tracking this
        project.destroyTestProgram();
        project.getTestProgram();
      }
    }
    // Added by Seng Yew on 22-Apr-2011
    // Update any graylevel minimum & maximum for image normalization by subtype
    // Have to make sure this is called if fastLoad is true
//    else if (fastLoad == true)
//    {
//      project.getTestProgram().setAllInspectionRegionsWithImageNormalizationBySubtype();
//    }

    timer.stop();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
    {
      long totalTime = timer.getElapsedTimeInMillis();
      Date date = new Date();
      System.out.println("Date: " + date);
      System.out.println("\nwpd - Project.load() total time to load in millis: " + totalTime + "\n");
      int numPins = project.getPanel().getNumJoints();
      System.out.println("wpd - number of pins: " + numPins);
      System.out.println("wpd - pins / sec: " + (double)numPins / totalTime * 1000.0);
      System.out.println("wpd - TOTAL LOAD TIME in milliseconds: " + totalTime);
    }

    abortDuringLoad.setValue(false);

    // Force to dump out scan path info if in hardware simulation mode
    Config softwareConfig = Config.getInstance();
    boolean isHardwareSimulation = softwareConfig.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION);
    if (isHardwareSimulation == false)
      return project;
    try
    {
      if (Config.isDeveloperSystemEnabled() == false)
        return project;
    }
    catch (Exception ex)
    {
      return project;
    }

    if(project.getTestProgram().getAllInspectableTestSubPrograms().isEmpty() == true)
      return project;

    generateScanPathForProject(project.getTestProgram());

    return project;
  }

  /**
   * Return the list of compatibilty messages (if any) of the last project loaded
   * @author Andy Mechtenberg
   */
  public static List<String> getCompatibiltyMessages()
  {
    Assert.expect(_compatibilityMessages != null);
    return _compatibilityMessages;
  }

  /**
   * @author Bill Darbie
   */
  public static boolean isCurrentProjectLoaded()
  {
    if (_currentProject == null)
      return false;

    return true;
  }

  /**
   * @return a reference to the most recently loaded panel.  A null will be returned if no panel is
   * currently loaded.
   *
   * @author Bill Darbie
   */
  public static Project getCurrentlyLoadedProject()
  {
    Assert.expect(_currentProject != null);
    return _currentProject;
  }

  /**
   * @author Bill Darbie
   */
  public static void unloadCurrentProject() throws DatastoreException
  {
    if (_currentProject == null)
      return;

    Config.getInstance().resetToMachineSystemTypeSetting();

    //Siew Yeng - XCR1725 - Customizable serial number mapping
    SerialNumberMappingManager.getInstance().clear();
    
    //Siew Yeng - XCR-2218 - Increase Inspectable pad size
    ReconstructionRegion.resetMaxInspectionRegionWidthLength();
    ProcessorStrip.resetOverlapNeededForLargestPossibleJoint();
    
    _projectObservable.setEnabled(false);
    Project oldValue = _currentProject;
    try
    {
      if (_currentProject._algorithmShortsLearning != null)
      {
        _currentProject._algorithmShortsLearning.close();
        _currentProject._algorithmShortsLearning.clearProjectReference();
      }
      if (_currentProject._algorithmSubtypeLearning != null)
      {
        _currentProject._algorithmSubtypeLearning.close();
        _currentProject._algorithmSubtypeLearning.clearProjectReference();
      }
      if (_currentProject._algorithmExpectedImageLearning != null)
      {
        _currentProject._algorithmExpectedImageLearning.close();
        _currentProject._algorithmExpectedImageLearning.clearProjectReference();
      }
      //XCR2782
      if (_currentProject._algorithmBrokenPinLearning != null)
      {
        _currentProject._algorithmBrokenPinLearning.close();
        _currentProject._algorithmBrokenPinLearning.clearProjectReference();
      }   
      //XCR2787, Close temp file reference before unload
      if (_currentProject._algorithmExpectedImageTemplateLearning != null)
      {
        _currentProject._algorithmExpectedImageTemplateLearning.close();
        _currentProject._algorithmExpectedImageTemplateLearning.clearProjectReference();
      }  
     
      //Kee chin Seong - Clear the map
      if(_currentProject._componentToFocusRegionMap.isEmpty() == false)
      {
        _currentProject._componentToFocusRegionMap.clear();
      }
      if (_currentProject._testProgram != null)
      {
        _currentProject._testProgram.clearProject();
        _currentProject._testProgram = null;
      }
      if (_currentProject._panel != null)
      {
        _currentProject._panel.clearProject();
        _currentProject._panel = null;
      }
      
      //Siew Yeng - XCR-3131 - import ndf does not clear previous recipe compatibility message
      // clear this message every time when unload project.
      _compatibilityMessages.clear();
      
      // Bee Hoon, XCR1650, initialize the flag to false when the project is closed
      _projectErrorLogUtil.setIsLandPatternNameNotFoundError(false);
      
      _currentProject._projectObservable.deleteObserver(_currentProject);
      _currentProject = null;
      
      if (VirtualLiveImageGenerationSettings.getInstance().hasVirtualLiveROI())
      {
        VirtualLiveImageGenerationSettings.getInstance().clearVirtualLiveROI();
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
      _isDummyProject = false;
    }

    _projectObservable.stateChanged(oldValue, oldValue, null, ProjectEventEnum.UNLOAD);
  }

  /**
   * @author Bill Darbie
   * @author George Booth
   */
  private void copyLearnedFiles() throws DatastoreException
  {
    String projectName = getName();

    String shortsLearningFile = FileName.getAlgorithmShortsLearningFullPath(projectName);
    String shortsLearningProjOpenFile = FileName.getAlgorithmShortsLearningTempProjOpenFullPath(projectName);
    if (FileUtilAxi.exists(shortsLearningProjOpenFile))
      FileUtilAxi.delete(shortsLearningProjOpenFile);
    if (FileUtilAxi.exists(shortsLearningFile))
      FileUtilAxi.copy(shortsLearningFile, shortsLearningProjOpenFile);

    String expectedImageLearningFile = FileName.getAlgorithmExpectedImageLearningFullPath(projectName);
    String expectedImageLearningProjOpenFile = FileName.getAlgorithmExpectedImageLearningTempProjOpenFullPath(projectName);
    if (FileUtilAxi.exists(expectedImageLearningProjOpenFile))
      FileUtilAxi.delete(expectedImageLearningProjOpenFile);
    if (FileUtilAxi.exists(expectedImageLearningFile))
      FileUtilAxi.copy(expectedImageLearningFile, expectedImageLearningProjOpenFile);
    
    //ShengChuan - Clear Tombstone - START
    String expectedImageTemplateLearningFile = FileName.getAlgorithmExpectedImageTemplateLearningFullPath(projectName);
    String expectedImageTemplateLearningProjOpenFile = FileName.getAlgorithmExpectedImageTemplateLearningTempProjOpenFullPath(projectName);
    if (FileUtilAxi.exists(expectedImageTemplateLearningProjOpenFile))
      FileUtilAxi.delete(expectedImageTemplateLearningProjOpenFile);
    if (FileUtilAxi.exists(expectedImageTemplateLearningFile))
      FileUtilAxi.copy(expectedImageTemplateLearningFile, expectedImageTemplateLearningProjOpenFile);
    //ShengChuan - Clear Tombstone - END

    String subtypesLearningFile = FileName.getAlgorithmSubtypesLearningFullPath(projectName);
    String subtypesLearningProjOpenFile = FileName.getAlgorithmSubtypesLearningTempProjOpenFullPath(projectName);
    if (FileUtilAxi.exists(subtypesLearningProjOpenFile))
      FileUtilAxi.delete(subtypesLearningProjOpenFile);
    if (FileUtilAxi.exists(subtypesLearningFile))
      FileUtilAxi.copy(subtypesLearningFile, subtypesLearningProjOpenFile);
    
    //Lim, Lay Ngor - Broken Pin - START
    String brokenPinLearningFile = FileName.getAlgorithmBrokenPinLearningFullPath(projectName);
    String brokenPinLearningProjOpenFile = FileName.getAlgorithmBrokenPinLearningTempProjOpenFullPath(projectName);
    if (FileUtilAxi.exists(brokenPinLearningProjOpenFile))
      FileUtilAxi.delete(brokenPinLearningProjOpenFile);
    if (FileUtilAxi.exists(brokenPinLearningFile))
      FileUtilAxi.copy(brokenPinLearningFile, brokenPinLearningProjOpenFile);
    //Lim, Lay Ngor - Broken Pin - END
  }

  /**
   * @author Bill Darbie
   * @author George Booth
   */
  private void saveLearnedFiles() throws DatastoreException
  {
    String projectName = getName();

    String shortsLearningFile = FileName.getAlgorithmShortsLearningFullPath(projectName);
    String shortsLearningProjOpenFile = FileName.getAlgorithmShortsLearningTempProjOpenFullPath(projectName);
    if (FileUtilAxi.exists(shortsLearningProjOpenFile))
    {
      if (_projectState.isShortLearningFileValid() == false)
        FileUtilAxi.copy(shortsLearningProjOpenFile, shortsLearningFile);
    }
    else
    {
      if (FileUtilAxi.exists(shortsLearningFile))
        FileUtilAxi.delete(shortsLearningFile);
    }

    String expectedImageLearningFile = FileName.getAlgorithmExpectedImageLearningFullPath(projectName);
    String expectedImageLearningProjOpenFile = FileName.getAlgorithmExpectedImageLearningTempProjOpenFullPath(projectName);
    if (FileUtilAxi.exists(expectedImageLearningProjOpenFile))
    {
      if (_projectState.isExpectedImageLearningFileValid() == false)
        FileUtilAxi.copy(expectedImageLearningProjOpenFile, expectedImageLearningFile);
    }
    else
    {
      if (FileUtilAxi.exists(expectedImageLearningFile))
        FileUtilAxi.delete(expectedImageLearningFile);
    }

    //ShengChuan - Clear Tombstone - START    
    String expectedImageTemplateLearningFile = FileName.getAlgorithmExpectedImageTemplateLearningFullPath(projectName);
    String expectedImageTemplateLearningProjOpenFile = FileName.getAlgorithmExpectedImageTemplateLearningTempProjOpenFullPath(projectName);
    if (FileUtilAxi.exists(expectedImageTemplateLearningProjOpenFile))
    {
      if (_projectState.isExpectedImageTemplateLearningFileValid() == false)
        FileUtilAxi.copy(expectedImageTemplateLearningProjOpenFile, expectedImageTemplateLearningFile);
    }
    else
    {
      if (FileUtilAxi.exists(expectedImageTemplateLearningFile))
        FileUtilAxi.delete(expectedImageTemplateLearningFile);
    }
    //ShengChuan - Clear Tombstone - END

    String subtypesLearningFile = FileName.getAlgorithmSubtypesLearningFullPath(projectName);
    String subtypesLearningProjOpenFile = FileName.getAlgorithmSubtypesLearningTempProjOpenFullPath(projectName);
    if (FileUtilAxi.exists(subtypesLearningProjOpenFile))
    {
      if (_projectState.isSubtypeLearningFileValid() == false)
        FileUtilAxi.copy(subtypesLearningProjOpenFile, subtypesLearningFile);
    }
    else
    {
      if (FileUtilAxi.exists(subtypesLearningFile))
        FileUtilAxi.delete(subtypesLearningFile);
    }
    
    //Lim, Lay Ngor - Broken Pin - START
    String brokenPinLearningFile = FileName.getAlgorithmBrokenPinLearningFullPath(projectName);
    String brokenPinLearningProjOpenFile = FileName.getAlgorithmBrokenPinLearningTempProjOpenFullPath(projectName);
    if (FileUtilAxi.exists(brokenPinLearningProjOpenFile))
    {
      if (_projectState.isBrokenPinLearningFileValid() == false)
        FileUtilAxi.copy(brokenPinLearningProjOpenFile, brokenPinLearningFile);
    }
    else
    {
      if (FileUtilAxi.exists(brokenPinLearningFile))
        FileUtilAxi.delete(brokenPinLearningFile);
    }    
    //Lim, Lay Ngor - Broken Pin - START
  }

  /**
   * @author Bill Darbie
   */
  public static boolean doesShortLearningFileExist(String projectName)
  {
    Assert.expect(projectName != null);

    String shortsLearningFile = FileName.getAlgorithmShortsLearningFullPath(projectName);
    if (FileUtilAxi.exists(shortsLearningFile))
      return true;

    return false;
  }

  /**
   * @author Ngie Xing
   */
  public static boolean doesShortLearningTempProjOpenFileExist(String projectName)
  {
    Assert.expect(projectName != null);

    String shortsLearningFile = FileName.getAlgorithmShortsLearningTempProjOpenFullPath(projectName);
    if (FileUtilAxi.exists(shortsLearningFile))
      return true;

    return false;
  }

  /**
   * @author George Booth
   */
  private static boolean doesExpectedImageLearningFileExist(String projectName)
  {
    Assert.expect(projectName != null);

    String expectedImageLearningFile = FileName.getAlgorithmExpectedImageLearningFullPath(projectName);
    if (FileUtilAxi.exists(expectedImageLearningFile))
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  private static boolean doesSubtypeLearningFileExist(String projectName)
  {
    Assert.expect(projectName != null);

    String subtypesLearningFile = FileName.getAlgorithmSubtypesLearningFullPath(projectName);
    if (FileUtilAxi.exists(subtypesLearningFile))
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor - Broken Pin
   */
  private static boolean doesBrokenPinLearningFileExist(String projectName)
  {
    Assert.expect(projectName != null);

    String brokenPinLearningFile = FileName.getAlgorithmBrokenPinLearningFullPath(projectName);
    if (FileUtilAxi.exists(brokenPinLearningFile))
      return true;

    return false;
  }
  
  /**
   * @author Bill Darbie
   */
  public AlgorithmShortsLearningReaderWriter getAlgorithmShortsLearningReaderWriter()
  {
    if (_algorithmShortsLearning == null)
      _algorithmShortsLearning = new AlgorithmShortsLearningReaderWriter(this);

    return _algorithmShortsLearning;
  }

  /**
   * @author Bill Darbie
   */
  public AlgorithmSubtypesLearningReaderWriter getAlgorithmSubtypeLearningReaderWriter()
  {
    if (_algorithmSubtypeLearning == null)
      _algorithmSubtypeLearning = new AlgorithmSubtypesLearningReaderWriter(this);

    return _algorithmSubtypeLearning;
  }

  /**
   * @author George Booth
   */
  public AlgorithmExpectedImageLearningReaderWriter getAlgorithmExpectedImageLearningReaderWriter()
  {
    if (_algorithmExpectedImageLearning == null)
      _algorithmExpectedImageLearning = new AlgorithmExpectedImageLearningReaderWriter(this);

    return _algorithmExpectedImageLearning;
  }

  /**
   * @author Lim, Lay Ngor - Broken Pin
   */
  public AlgorithmBrokenPinLearningReaderWriter getAlgorithmBrokenPinLearningReaderWriter()
  {
    if (_algorithmBrokenPinLearning == null)
      _algorithmBrokenPinLearning = new AlgorithmBrokenPinLearningReaderWriter(this);

    return _algorithmBrokenPinLearning;
  }

  /**
   * Clear Tombstone
   * @author sheng chuan
   */
  public AlgorithmExpectedImageTemplateLearningReaderWriter getAlgorithmExpectedImageTemplateLearningReaderWriter()
  {
    if (_algorithmExpectedImageTemplateLearning == null)
      _algorithmExpectedImageTemplateLearning = new AlgorithmExpectedImageTemplateLearningReaderWriter(this);

    return _algorithmExpectedImageTemplateLearning;
  }

  /**
   * @author Bill Darbie
   */
  public ProjectState getProjectState()
  {
    Assert.expect(_projectState != null);
    return _projectState;
  }

  /**
   * Load in a project from ndf files on disk and save it to the native file format.
   *
   * @author Bill Darbie
   */
  public static Project importProjectFromNdfs(String projectName, List<LocalizedString> warnings) throws DatastoreException
  {
    Assert.expect(projectName != null);
    Assert.expect(warnings != null);

    return importProjectFromNdfs(projectName, projectName, warnings);
  }

  /**
   * Load in a project from ndf files on disk and save it to the native file format.
   *
   * @author Bill Darbie
   */
  public static Project importProjectFromNdfs(String ndfProjectName, String newProjectName, List<LocalizedString> warnings) throws DatastoreException
  {
    Assert.expect(ndfProjectName != null);
    Assert.expect(newProjectName != null);
    Assert.expect(warnings != null);

    TimerUtil timer = new TimerUtil();
    timer.start();

    Project project = null;
    Project oldValue = _currentProject;
    _projectObservable.setEnabled(false);
    try
    {
      // making sure all calibration and configuration information loaded.
      loadCalAndConfigInformationForUnitTest();

      unloadCurrentProject();
      // read it
      project = _mainReader.importProjectFromNdfs(ndfProjectName, warnings);
      if (ndfProjectName.equals(newProjectName) == false)
        project.setName(newProjectName);
      
      Object isEnableLargeImageView = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_GENERATE_LARGE_VIEW_IMAGE_INITIAL_SETTING);
      Object isGenerateMultiAngleView = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_GENERATE_MULTI_ANGLE_IMAGE_INITIAL_SETTING);
      Object isEnlargeRecontrusionRegion = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_ENLARGE_RECONSTRUSION_REGION_INITIAL_SETTING);
      Object isEnlargeAlignmentRegion = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_ENLARGE_ALIGNMENT_REGION_INTIAL_SETTING);
      Object isProjectByPass = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_PROJECT_BASED_BYPASS_MODE_INTIAL_SETTING);
      Object isJointBasedBGAInspection = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_SET_BGA_INSPECTION_REGION_TO_1X1_INTIAL_SETTING);
      Object droLevelValue = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_DRO_INTIAL_SETTING);
      Object isGenerateNewScanRoute = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_GENERATE_NEW_SCAN_ROUTE_INTIAL_SETTING);
      Object projectDroVersion = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_DRO_VERSION_INITIAL_SETTING);
      Object magnificationValue = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_MAGNIFICATION_INTIAL_SETITNG);
      String magnificationString = String.valueOf(magnificationValue);
      if (magnificationString.equals("HIGH"))
      {
        magnificationString = "1_"+magnificationString;
      }
      else
      {
        magnificationString = "0_"+magnificationString;
      }
      Object userGainValue = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_USER_GAIN_INTIAL_SETITNG);
      Object signalCompensationValue = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_SIGNAL_COMPENSATION_INITIAL_SETTING);
      Object stageSpeedValue = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_STAGE_SPEED_INTIAL_SETTING);
      Object interconferenceCompensation = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_INTERFERENCE_COMPENSATION_INTIAL_SETITNG);
//      Object camereAngle = ProjectInitialRecipeSettings.getInstance().getRecipeSettingValue(RecipeAdvanceSettingEnum.RECIPE_XRAY_ENABLE_CAMERA_INDEX_INTIAL_SETTING);
      final java.util.List<StageSpeedEnum> stageSpeedEnumList = new ArrayList<StageSpeedEnum>();
        for (String stageSpeedName : (java.util.List<String>) stageSpeedValue)
      {
        try
        {
          StageSpeedEnum stageSpeedEnum = StageSpeedEnum.getStageSpeedToEnumMapValue(StringUtil.convertStringToDouble(stageSpeedName));
          stageSpeedEnumList.add(stageSpeedEnum);
        }
        catch (BadFormatException ex)
        {

        }
      }

//      final java.util.List<Integer> cameraIdList = new ArrayList<Integer>();
//      for (String cameraIdName : (java.util.List<String>) camereAngle)
//      {
//        try
//        {
//          int cameraId = StringUtil.convertStringToInt(cameraIdName);
//          cameraIdList.add(cameraId);
//        }
//        catch (BadFormatException ex)
//        {
//
//        }
//      }
      //Ngie Xing, ndf that is loaded will have largeImageView enabled based on software config
      project.setEnableLargeImageView(new Boolean((String) isEnableLargeImageView));
      project.setBGAInspectionRegionTo1X1(new Boolean((String) isJointBasedBGAInspection));
      project.setProjectBasedBypassMode(new Boolean((String) isProjectByPass));
      project.setGenerateMultiAngleImage(new Boolean((String) isGenerateMultiAngleView));
      project.setEnlargeAlignmentRegion(new Boolean((String) isEnlargeAlignmentRegion));
      project.setEnlargeReconstructionRegion(new Boolean((String) isEnlargeRecontrusionRegion));
      project.setGenerateByNewScanRoute(new Boolean((String) isGenerateNewScanRoute));
      try
      {
        project.setDynamicRangeOptimizationVersion(StringUtil.convertStringToInt((String) projectDroVersion));
      }
      catch (BadFormatException ex)
      {
        project.setDynamicRangeOptimizationVersion(DynamicRangeOptimizationVersionEnum.VERSION_0.getVersion());
      }

      for (Subtype subtype : project.getPanel().getSubtypes())
      {
        try
        {
          SubtypeAdvanceSettings subtypeSetting = subtype.getSubtypeAdvanceSettings();
          if (Config.getInstance().getIntValue(SoftwareConfigEnum.MAXIMUM_DYNAMIC_RANGE_OPTIMIZATION_LEVEL) < StringUtil.convertStringToInt((String) droLevelValue))
          {
            subtypeSetting.setDynamicRangeOptimizationLevel(DynamicRangeOptimizationLevelEnum.getDynamicRangeOptimizationLevelToEnumMapValue(Config.getInstance().getIntValue(SoftwareConfigEnum.MAXIMUM_DYNAMIC_RANGE_OPTIMIZATION_LEVEL)));
          }
          else
          {
            subtypeSetting.setDynamicRangeOptimizationLevel(DynamicRangeOptimizationLevelEnum.getDynamicRangeOptimizationLevelToEnumMapValue(StringUtil.convertStringToInt((String) droLevelValue)));
          }
          subtypeSetting.setUserGain(UserGainEnum.getUserGainToEnumMapValue(StringUtil.convertStringToInt((String) userGainValue)));
          subtypeSetting.setSignalCompensation(SignalCompensationEnum.getSignalCompensationEnum((String) signalCompensationValue));
          subtypeSetting.setIsInterferenceCompensatable(new Boolean((String) interconferenceCompensation));
          subtypeSetting.setStageSpeedEnum(stageSpeedEnumList);
//          if (Config.getInstance().getIntValue(SoftwareConfigEnum.MINIMUM_CAMERA_USED_FOR_RECONSTRUCTION) > cameraIdList.size())
//          {
//            Object defaultValue = RecipeAdvanceSettingEnum.RECIPE_XRAY_ENABLE_CAMERA_INDEX_INTIAL_SETTING.getDefaultValue();
//            java.util.List<Integer> defaultCameraList = new ArrayList<Integer>();
//            for (String cameraIdName : (java.util.List<String>) defaultValue)
//            {
//              try
//              {
//                int cameraId = StringUtil.convertStringToInt(cameraIdName);
//                defaultCameraList.add(cameraId);
//              }
//              catch (BadFormatException ex)
//              {
//
//              }
//            }
//            subtypeSetting.setEnabledCamera(defaultCameraList);
//          }
//          else
//          {
//            subtypeSetting.setEnabledCamera(cameraIdList);
//          }
          
          if (Config.getInstance().getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED) == false &&
              Config.getInstance().getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED) == false)
          {
            subtypeSetting.setMagnificationType(MagnificationTypeEnum.LOW);
          }
          else
          {
            subtypeSetting.setMagnificationType(MagnificationTypeEnum.getMagnificationToEnumMapValue(magnificationString));
          }
        }
        catch (BadFormatException e)
        {
          SubtypeAdvanceSettings subtypeSetting = subtype.getSubtypeAdvanceSettings();
          subtypeSetting.setDynamicRangeOptimizationLevel(DynamicRangeOptimizationLevelEnum.ONE);
          subtypeSetting.setUserGain(UserGainEnum.ONE);
          subtypeSetting.setSignalCompensation(SignalCompensationEnum.getSignalCompensationEnum((String) signalCompensationValue));
          subtypeSetting.setIsInterferenceCompensatable(new Boolean((String) interconferenceCompensation));
          //subtypeSetting.setStageSpeed(StageSpeedEnum.getStageSpeedToEnumMapValue(StringUtil.convertStringToDouble((String) stageSpeedValue)));
          subtypeSetting.setMagnificationType(MagnificationTypeEnum.getMagnificationToEnumMapValue(magnificationString));
        }
      }
      //set default value for project's low and high magnification
      project.setProjectLowMagnification(Config.getSystemCurrentLowMagnification());
      project.setProjectHighMagnification(Config.getSystemCurrentHighMagnification());

      int latestThicknessTableVersion = Config.getInstance().getIntValue(SoftwareConfigEnum.THICKNESS_TABLE_LATEST_VERSION);

      //Swee Yee Wong - XCR-3300 Thickness profile is capped at 100 in PTH subtype
      //New thickness table is not ready, will get flat response when reaching high deltagray value
      //Will change the default value back to 0 until the new thickness table is ready and stable.
      //project.setThicknessTableVersion(latestThicknessTableVersion);
      project.setThicknessTableVersion(0);
        
      //swee yee wong - XCR-3004 Load recipe assert java.lang.NullPointerException
      //swee yee wong - XCR-3046 Alignment is cleared if attempt to unload and reload recipe after run production
      boolean notifyUserAlternativeVersion = true;
      project.isProjectSolderThicknessFilesAvailable(notifyUserAlternativeVersion);

      // this is set during import at the GUI layer from the import dialog
      project.saveInitialThresholdsSetName(ProjectInitialThresholds.getInstance().getThresholdSetName());
      
      project.saveInitialRecipeSettingSetName(ProjectInitialRecipeSettings.getInstance().getRecipeSettingName());

      // delete any learning files since they no longer apply
      project.deleteLearnedFiles();

      // delete the cad and settings XML files since they no longer apply either
      project.deleteXMLFiles();

      // this must be called before getTestProgram()
      Assert.expect(project != null);
      _currentProject = project;

      // force the test program to be generated before we save the project
      TimerUtil timer2 = new TimerUtil();
      timer2.start();
      project.getTestProgram();
      timer2.stop();
      if ((_displayTimes) && (UnitTest.unitTesting() == false))
      {
        long totalTime = timer2.getElapsedTimeInMillis();
        System.out.println("\nwpd - Project. program generation during Project.importProjectFromNdfs() total time in millis: " + totalTime + "\n");
      }

      timer2.reset();
      timer2.start();
      //Save project file here
      project.save();
      timer2.stop();
      if ((_displayTimes) &&(UnitTest.unitTesting() == false))
      {
        long totalTime = timer2.getElapsedTimeInMillis();
        System.out.println("\nwpd - Project. time to save program during Project.importProjectFromNdfs() total time in millis: " + totalTime + "\n");
      }

      _projectObservable.addObserver(_currentProject);

      project._projectState.enableAfterProjectLoad();
    }
    finally
    {
      _projectObservable.setEnabled(true);
      // Bee Hoon, XCR1632, reset the 5dx ndf subtype assignment with id flag back to normal subtype assignment without id
      NdfReader.set5dxNdfWithDiffSubtype('0');
    }

    _projectObservable.stateChanged(project, oldValue, project, ProjectEventEnum.IMPORT_PROJECT_FROM_NDFS);

    timer.stop();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
    {
      long totalTime = timer.getElapsedTimeInMillis();
      System.out.println("\nwpd - Project.importProjectFromNdfs() total time to load in millis: " + totalTime + "\n");
//      int numPins = project.getPanel().getNumJoints();
//      System.out.println("wpd - number of pins: " + numPins);
//      System.out.println("wpd - pins / sec: " + (double)numPins / totalTime * 1000.0);
//      System.out.println("wpd - TOTAL LOAD TIME in milliseconds: " + totalTime);
    }
    return project;
  }

  /**
   * @author Bill Darbie
   */
  public void deleteShortProfileLearning() throws DatastoreException
  {
    String fileName = FileName.getAlgorithmShortsLearningTempProjOpenFullPath(getName());
    boolean fileExists = FileUtilAxi.exists(fileName);

    if (fileExists == false)
      return;

    _projectObservable.setEnabled(false);
    try
    {
      if (fileExists)
        FileUtilAxi.delete(fileName);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }

    _projectObservable.stateChanged(this, fileExists, false, ProjectEventEnum.DELETE_SHORTS_LEARNING);
  }

  /**
   * @author George Booth
   */
  public void deleteExpectedImageLearning() throws DatastoreException
  {
    String fileName = FileName.getAlgorithmExpectedImageLearningTempProjOpenFullPath(getName());
    boolean fileExists = FileUtilAxi.exists(fileName);

    if (fileExists == false)
      return;

    _projectObservable.setEnabled(false);
    try
    {
      if (fileExists)
        FileUtilAxi.delete(fileName);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }

    _projectObservable.stateChanged(this, fileExists, false, ProjectEventEnum.DELETE_EXPECTED_IMAGE_LEARNING);
  }

  /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor - Broken Pin
   */
  public void deleteBrokenPinDataLearning() throws DatastoreException
  {
    String fileName = FileName.getAlgorithmBrokenPinLearningTempProjOpenFullPath(getName());
    boolean fileExists = FileUtilAxi.exists(fileName);

    if (fileExists == false)
      return;

    _projectObservable.setEnabled(false);
    try
    {
      if (fileExists)
        FileUtilAxi.delete(fileName);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }

    //Use the short event??
    _projectObservable.stateChanged(this, fileExists, false, ProjectEventEnum.DELETE_SHORTS_LEARNING);
  }
  
  /**
   * Clear Tombstone
   * @author ShengChuan
   */
  public void deleteExpectedImageTemplateLearning() throws DatastoreException
  {
    String fileName = FileName.getAlgorithmExpectedImageTemplateLearningTempProjOpenFullPath(getName());
    boolean fileExists = FileUtilAxi.exists(fileName);

    if (fileExists == false)
      return;

    _projectObservable.setEnabled(false);
    try
    {
      if (fileExists)
        FileUtilAxi.delete(fileName);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }

    _projectObservable.stateChanged(this, fileExists, false, ProjectEventEnum.DELETE_EXPECTED_IMAGE_TEMPLATE_LEARNING);
  }
  
  /**
   * @author Bill Darbie
   */
  public void deleteLearnedJointMeasurements() throws DatastoreException
  {
    String fileName = FileName.getAlgorithmSubtypesLearningTempProjOpenFullPath(getName());
    boolean fileExists = FileUtilAxi.exists(fileName);
    if (fileExists == false)
      return;

    _projectObservable.setEnabled(false);
    try
    {
      FileUtilAxi.delete(fileName);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }

    _projectObservable.stateChanged(this, fileExists, false, ProjectEventEnum.DELETE_SUBTYPE_LEARNING);
  }

  /**
   * @author Bill Darbie
   */
  private void deleteLearnedFiles() throws DatastoreException
  {
    String projName = getName();
    String shortsLearningFile = FileName.getAlgorithmShortsLearningFullPath(projName);
    if (FileUtilAxi.exists(shortsLearningFile))
      FileUtilAxi.delete(shortsLearningFile);
    String expectedImageLearningFile = FileName.getAlgorithmExpectedImageLearningFullPath(projName);
    if (FileUtilAxi.exists(expectedImageLearningFile))
      FileUtilAxi.delete(expectedImageLearningFile);
    //ShengChuan - Clear Tombstone - START
    String expectedImageTemplateLearningFile = FileName.getAlgorithmExpectedImageTemplateLearningFullPath(projName);
    if (FileUtilAxi.exists(expectedImageTemplateLearningFile))
      FileUtilAxi.delete(expectedImageTemplateLearningFile);
    //ShengChuan - Clear Tombstone - END
    String subytpesLearningFile = FileName.getAlgorithmSubtypesLearningFullPath(projName);
    if (FileUtilAxi.exists(subytpesLearningFile))
      FileUtilAxi.delete(subytpesLearningFile);
    //Broken Pin
    String brokenPinLearningFile = FileName.getAlgorithmBrokenPinLearningFullPath(projName);
    if (FileUtilAxi.exists(brokenPinLearningFile))
      FileUtilAxi.delete(brokenPinLearningFile);    

    String shortsLearningProjOpenFile = FileName.getAlgorithmShortsLearningTempProjOpenFullPath(projName);
    if (FileUtilAxi.exists(shortsLearningProjOpenFile))
      FileUtilAxi.delete(shortsLearningProjOpenFile);
    String expectedImageLearningProjOpenFile = FileName.getAlgorithmExpectedImageLearningTempProjOpenFullPath(projName);
    if (FileUtilAxi.exists(expectedImageLearningProjOpenFile))
      FileUtilAxi.delete(expectedImageLearningProjOpenFile);
    //ShengChuan - Clear Tombstone - START
    String expectedImageTemplateLearningProjOpenFile = FileName.getAlgorithmExpectedImageTemplateLearningTempProjOpenFullPath(projName);
    if (FileUtilAxi.exists(expectedImageTemplateLearningProjOpenFile))
      FileUtilAxi.delete(expectedImageTemplateLearningProjOpenFile);
    //ShengChuan - Clear Tombstone - END
    String subytpesLearningProjOpenFile = FileName.getAlgorithmSubtypesLearningTempProjOpenFullPath(projName);
    if (FileUtilAxi.exists(subytpesLearningProjOpenFile))
        FileUtilAxi.delete(subytpesLearningProjOpenFile);
    //Broken Pin
    String brokenPinLearningProjOpenFile = FileName.getAlgorithmBrokenPinLearningTempProjOpenFullPath(projName);
    if (FileUtilAxi.exists(brokenPinLearningProjOpenFile))
      FileUtilAxi.delete(brokenPinLearningProjOpenFile);    

    String shortsLearningOpenFile = FileName.getAlgorithmShortsLearningTempOpenFullPath(projName);
    if (FileUtilAxi.exists(shortsLearningOpenFile))
      FileUtilAxi.delete(shortsLearningOpenFile);
    String expectedImageLearningOpenFile = FileName.getAlgorithmExpectedImageLearningTempOpenFullPath(projName);
    if (FileUtilAxi.exists(expectedImageLearningOpenFile))
      FileUtilAxi.delete(expectedImageLearningOpenFile);
    //ShengChuan - Clear Tombstone - START
    String expectedImageTemplateLearningOpenFile = FileName.getAlgorithmExpectedImageTemplateLearningTempOpenFullPath(projName);
    if (FileUtilAxi.exists(expectedImageTemplateLearningOpenFile))
      FileUtilAxi.delete(expectedImageTemplateLearningOpenFile);
    //ShengChuan - Clear Tombstone - END
    String subytpesLearningOpenFile = FileName.getAlgorithmSubtypesLearningTempOpenFullPath(projName);
    if (FileUtilAxi.exists(subytpesLearningOpenFile))
      FileUtilAxi.delete(subytpesLearningOpenFile);
    //Broken Pin
    String brokenPinLearningOpenFile = FileName.getAlgorithmBrokenPinLearningTempOpenFullPath(projName);
    if (FileUtilAxi.exists(brokenPinLearningOpenFile))
      FileUtilAxi.delete(brokenPinLearningOpenFile);    
  }

  /**
   * @author Laura Cormos
   */
  private void deleteXMLFiles() throws DatastoreException
  {
    String projName = getName();
    String fileNameRegularExpression = "^(cad|settings).*\\.(xml)$";
    Pattern fileNamePattern = Pattern.compile(fileNameRegularExpression, Pattern.CASE_INSENSITIVE);

    String sourcePath = Directory.getProjectDir(projName);
    // gather all the file names.
    Collection<String> filesOnDisk = FileUtilAxi.listAllFilesFullPathInDirectory(sourcePath);
    for (String xmlFileNameWithFullPath : filesOnDisk)
    {
      String fileName = FileUtil.getNameWithoutPath(xmlFileNameWithFullPath);
      Matcher matcher = fileNamePattern.matcher(fileName);
      if (matcher.matches())
      {
        FileUtilAxi.delete(xmlFileNameWithFullPath);
      }
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void saveCreatorProject() throws DatastoreException
  {
    _saveInProgress = true;
    _projectState.disable();
    try
    {
      CadCreatorProjectWriter.getInstance().save(this, false);  
    }
    catch (DatastoreException dex)
    {
      throw dex;
    }
    finally
    {
      _projectState.enable();
      _saveInProgress = false;
    }
  }
  
  /*
   * @author Kee Chin Seong 
   */
  public void convertCadCreatorProjectToV810Project() throws DatastoreException
  {
    try
    {
      save();
      
      FileUtilAxi.delete(FileName.getCadCreatorProjectSettingsFullPath(getName(), FileName.getCadCreatorProjectSettingsLatestFileVersion()));
    }
    catch(DatastoreException ex)
    {
      //Do Nothing
    }
  }
  
  public final boolean isProjectLoadedSystemTypeSameWithLoadedSystemType()
  {
     return Config.getInstance().getSystemTypeSettingLoaded().equals(_systemType);
  }

  /**
   * Save the project to disk.
   *
   * @author Bill Darbie
   */
  public void save() throws DatastoreException
  {
    _saveInProgress = true;
    _projectState.disable();
    try
    {
      // since the save can now generate important events during program generation, we don't want to supress those
      // during the save, otherwise we won't get those events and we will have memory leaks.
//      _projectObservable.setEnabled(false);

      int origTestProgramVersion = _testProgramVersion;
      int origMinorVersion = _minorVersion;

      TimerUtil totalTimer = new TimerUtil();
      totalTimer.start();
      boolean prevCheckLatestTestProgram = _checkLatestTestProgram;
      setCheckLatestTestProgram(true);
      try
      {
        updateLastModificationTimeInMillis();

        Boolean resetTestProgram=false;
        // now write out the new files
        if (_projectState.isProjectMajorStateValid() == false)
        {
          ++_testProgramVersion;
          _minorVersion = 0;
//          _testProgram = null;
        }
        else if (_projectState.isProjectMinorStateValid() == false)
        {
          ++_minorVersion;
          resetTestProgram =true;
        }

        TimerUtil timer = new TimerUtil();
        timer.reset();
        timer.start();

        // generate the program if it's not currently valid
        // We want a valid program on disk at all times if the user saves the project
        // so that project data is in sync with program data        
//        if (_testProgram == null)
//        {
//          //reset _isAllComponentSameUserGain
//          _currentProject.getPanel().getPanelSettings().resetIsAllComponentSameUserGainSetting();
//          _currentProject.getPanel().getPanelSettings().resetLowAndHighMagSettings();
//          _projectState.resetDueToTestProgramGenerated();
//          // since projectState hold references to the testProgram, we want to clear out the project state first so we don't have
//          // two simulateous instances of a test program -- better for memory.
//         getTestProgram();
//         resetTestProgram=false;
//        }
        //KEe Chin Seong :: Clear Filter before Regenerate Test Sub Program.
        if(_testProgram != null)
          _testProgram.clearFilters();
        
        getTestProgram();
        if(resetTestProgram)
        {
           if(_testProgram.getProject().getPanel().getPanelSettings().isAllComponentSameUserGain()==false || 
             _testProgram.getProject().getPanel().getPanelSettings().isAllComponentSameStageSpeed()==false)
           {
             ProgramGeneration.getInstance().resetTestProgram(_testProgram);
           }
        }
        
        timer.stop();
        long time = timer.getElapsedTimeInMillis();
        if ((_displayTimes) && (UnitTest.unitTesting() == false))
        {
          System.out.println("wpd time to generate program for save: " + time);
        }

        timer.reset();
        timer.start();

        PanelSettings panelSettings = _panel.getPanelSettings();
        panelSettings.syncShortLearningStatusForAllSubtypes();

        timer.stop();
        time = timer.getElapsedTimeInMillis();
        if ((_displayTimes) && (UnitTest.unitTesting() == false))
        {
          System.out.println("wpd time to sync short learning for save: " + time);
        }

        timer.reset();
        timer.start();

        //Lim, Lay Ngor - Broken Pin - START
        panelSettings.syncBrokenPinLearningStatusForAllSubtypes();

        timer.stop();
        time = timer.getElapsedTimeInMillis();
        if ((_displayTimes) && (UnitTest.unitTesting() == false))
        {
          System.out.println("wpd time to sync broken pin learning for save: " + time);
        }
        
        timer.reset();
        timer.start();
        //Lim, Lay Ngor - Broken Pin - END

        //ShengChuan - Clear Tombstone - Start
        panelSettings.syncExpectedImageTemplateLearningStatusForAllSubtypes();
        
        timer.stop();
        time = timer.getElapsedTimeInMillis();
        if ((_displayTimes) && (UnitTest.unitTesting() == false))
        {
          System.out.println("wpd time to sync Template learning for save: " + time);
        }

        timer.reset();
        timer.start();
        //ShengChuan - Clear Tombstone - End

        saveLearnedFiles();

        timer.stop();
        time = timer.getElapsedTimeInMillis();
        if ((_displayTimes) && (UnitTest.unitTesting() == false))
        {
          System.out.println("wpd time to save learned files for save: " + time);
        }

        // reset the project state machine before saving it since it's state gets saved too
        _projectState.resetDueToProjectSave();
        _projectWriter.save(this, false);

        totalTimer.stop();
        time = totalTimer.getElapsedTimeInMillis();
        if ((_displayTimes) && (UnitTest.unitTesting() == false))
        {
          System.out.println("wpd TOTAL time to SAVE project: " + time);
        }
        
        if (_printOpticalRegionInfo)
        {
            for(TestSubProgram testSubProgram : getTestProgram().getAllInspectableTestSubPrograms())
            {
                System.out.println("Project Save: TestSubProgram Id " + testSubProgram.getId() + " has " + testSubProgram.getOpticalRegions().size() + " OpticalRegions and " +
                                                                testSubProgram.getFilteredComponentReferenceDesignators().size() + " filtered components.");
                for(OpticalRegion opticalRegion : testSubProgram.getOpticalRegions())
                {
                    System.out.println("Project Save:      Optical Region " + opticalRegion.getName() + " has " + opticalRegion.getAllOpticalCameraRectangles().size() + 
                            " OpticalCameraRectangles and " + opticalRegion.getComponents().size() + " components.");
                    
                    for(OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getAllOpticalCameraRectangles())
                    {
                        System.out.println("Project Save:          OpticalCameraRectangle " + opticalCameraRectangle.getName() + " has " + 
                                opticalCameraRectangle.getPanelCoordinateList().size() + " points.");
                    }
                }
                
                for(Map.Entry<String,Integer> entry : testSubProgram.getOpticalRegionNameToSubProgramReference().entrySet())
                {
                    String referenceOpticalRegionName = entry.getKey();
                    int subProgramReferenceIdForOpticalRegion = entry.getValue();
                    List<Component> components = testSubProgram.getComponentsReferenceToSubProgramForOpticalRegion(referenceOpticalRegionName);
                    
                    if (components == null || components.size() == 0)
                    {
                        System.out.println("Project Save: TestSubProgram Id " + testSubProgram.getId() + " has reference to TestSubProgram " + 
                            subProgramReferenceIdForOpticalRegion + " and optical region " + referenceOpticalRegionName + ".");
                    }
                    else
                    {
                        System.out.println("Project Save: TestSubProgram Id " + testSubProgram.getId() + " has reference to TestSubProgram " + 
                            subProgramReferenceIdForOpticalRegion + " and optical region " + referenceOpticalRegionName + " with " + components.size() + " reference components.");
                    }
                }
            }
            System.out.println();
        }
      }
      catch (DatastoreException dex)
      {
        _testProgramVersion = origTestProgramVersion;
        _minorVersion = origMinorVersion;

        throw dex;
      }
      finally
      {
        // since the save can now generate important events during program generation, we don't want to supress those
        // during the save, otherwise we won't get those events and we will have memory leaks.
//        _projectObservable.setEnabled(true);
      }
      setCheckLatestTestProgram(prevCheckLatestTestProgram);
    }
    finally
    {
      _projectState.enable();
      _saveInProgress = false;
    }
    _projectObservable.stateChanged(this, this, this, ProjectEventEnum.SAVE);


//    try
//    {
//      FileWriterUtilAxi writer = new FileWriterUtilAxi(Directory.getProjectDir(getName()) + File.separator +
//                                                       "subtypeInfo.csv", false);
//      writer.open();
//      writer.writeln("SUBTYPE NAME, PAD WIDTH, PAD LENGTH");
//
//
//      for (Subtype subtype : this.getPanel().getSubtypes())
//      {
//        if(subtype.getInspectionFamilyEnum().equals(com.axi.v810.business.imageAnalysis.InspectionFamilyEnum.GULLWING))
//        {
//          Pad firstPad = subtype.getPads().iterator().next();
//          writer.writeln(subtype.getLongName() + "," + firstPad.getWidthAfterAllRotationsInNanoMeters() + "," + firstPad.getLengthAfterAllRotationsInNanoMeters());
//        }
//      }
//      writer.close();
//    }
//    catch (DatastoreException dex)
//    {
//      dex.printStackTrace();
//    }
  }

  /**
   * @author Bill Darbie
   */
  private void saveAsSerializedFile() throws DatastoreException
  {
    Assert.expect(_projectWriter != null);
    _projectWriter.saveProjectAsSerializedFile(this);
  }

  /**
   * Save the project to disk as a new name.
   *
   * @author Bill Darbie
   */
  public void saveAs(String projName, boolean copyImageSets) throws DatastoreException
  {
    Assert.expect(projName != null);
    Assert.expect(projName.equalsIgnoreCase(getName()) == false);

    _projectState.disable();
    try
    {
      _projectObservable.setEnabled(false);

      _cancelSaveAs = false;
      // first copy the current project directory (if it exists) to the new directory name
      // this is required so we get all subdirectories that contain things like short learning, etc. moved over
      String fromDirectory = Directory.getProjectDir(getName());
      String toDirectory = Directory.getProjectDir(projName);
      String tempDir = null;
      String origProjName = getName();
      try
      {
        if (FileUtilAxi.exists(toDirectory))
        {
          tempDir = FileUtilAxi.getUniqueTempFile(toDirectory);
          FileUtilAxi.rename(toDirectory, tempDir);
        }

        FileUtilAxi.mkdir(toDirectory);
        if (copyImageSets)
        {
          FileUtilAxi.copyDirectoryRecursively(fromDirectory, toDirectory);
          // ok, now load all the image set info file and change their project names
          List<ImageSetData> imageSetDatas = _imageManager.getImageSetData(this, true);
          ImageSetInfoWriter writer = new ImageSetInfoWriter();
          for(ImageSetData data : imageSetDatas)
          {
            data.setProjectName(projName);
            writer.write(data);
          }
        }
        else
        {
          String imagesDir = Directory.getXrayImagesDir(origProjName);
          String opticalImagesDir = Directory.getOpticalImagesDir(origProjName);
          List<String> dontCopyDirs = new ArrayList<String>();
          dontCopyDirs.add(imagesDir);
          dontCopyDirs.add(opticalImagesDir);
          FileUtilAxi.copyDirectoryRecursively(fromDirectory, toDirectory, dontCopyDirs);
        }
        
        //Ngie Xing, delete previous versions of the Project Settings 
        int version = FileName.getProjectProjectSettingsLatestFileVersion();
        --version;
        String projectSettingsFullPath;
        while (version > 0)
        {
          projectSettingsFullPath = FileName.getProjectProjectSettingsFullPath(projName, version);
          if (FileUtilAxi.exists(projectSettingsFullPath))
          {
            FileUtilAxi.delete(projectSettingsFullPath);
          }
          --version;
        }

        // set version variables so the new project will start at 1.0
        if (_cancelSaveAs == false)
        {
          // now save the current state of the project to the new directory
          setName(projName);
          
          if (RecipeConverter.getInstance().isConvertRecipe())
          {
            if (getPanel().getPanelSettings().isPanelBasedAlignment())
            {
              if (getPanel().getPanelSettings().isUsing2DAlignmentMethod())
              {
                if ((RecipeConverter.getInstance().getConvertRevision().equals(SoftwareRevisionEnum._5_10.toString())||
                     RecipeConverter.getInstance().getConvertRevision().equals(SoftwareRevisionEnum._5_9.toString())) == false )
                {
                  getPanel().getPanelSettings().clearAllAlignmentTransforms();
                }
              }
            }
            else
            {
              for (Board board : getPanel().getBoards())
              {
                if (board.getBoardSettings().isUsing2DAlignmentMethod())
                {
                  if ((RecipeConverter.getInstance().getConvertRevision().equals(SoftwareRevisionEnum._5_10.toString())||
                       RecipeConverter.getInstance().getConvertRevision().equals(SoftwareRevisionEnum._5_9.toString())) == false )
                  {
                    board.getBoardSettings().clearAllAlignmentTransforms();
                  }
                }
              }
            }
          }
          //_newSystemType == null mean license is not OLP ; ignore 
          // 
          if (_saveAsSystemType != null)
          {
            if (_saveAsSystemType.equals(_previousSystemType) == false)
            {
              clearAlignmentTransformIfnecessary();
            }
            if (_saveAsSystemType.equals(SystemTypeEnum.S2EX))
            {
              if (_saveAsLowMagnification.equals(getProjectLowMagnification()) == false)
              {
                clearAlignmentTransformIfnecessary();
              }
              setProjectLowMagnification(_saveAsLowMagnification);
            }
            else
            {
              setProjectLowMagnification(_M19);
            }
            XrayTester.changeSystemType(_saveAsSystemType);
            setSystemType(_saveAsSystemType);
            _testProgramVersion = 1;
            _minorVersion = 0;
            save();
            if (RecipeConverter.getInstance().isConvertRecipe())
            {
              setSystemType(_previousSystemType);
              XrayTester.changeSystemType(_previousSystemType);
            }
          }
          else
          {
            _testProgramVersion = 1;
            _minorVersion = 0;
            save();
          }
          
          if(RecipeConverter.getInstance().isConvertRecipe())
          {
            setName(origProjName);
          }
        }
      }
      catch (DatastoreException dex)
      {
        try
        {
          // clean up before throwing the exception
          FileUtilAxi.delete(toDirectory);
          if (tempDir != null)
            FileUtilAxi.rename(tempDir, toDirectory);
        }
        catch (DatastoreException ex)
        {
          ex.printStackTrace();
        }

        throw dex;
      }

      // the saveAs worked, so remove the temp directory
      if (tempDir != null)
        FileUtilAxi.delete(tempDir);

      if (_cancelSaveAs)
      {
        RecipeConverter.getInstance().setConvertRecipe(false);
        if (FileUtilAxi.exists(toDirectory))
          FileUtilAxi.delete(toDirectory);
        if (origProjName != null)
          setName(origProjName);
      }
    }
    finally
    {
      _saveAsSystemType = null;
      _saveAsLowMagnification = null;
      _previousSystemType = null;
      _projectObservable.setEnabled(true);
      _projectState.enable();
    }
    _projectObservable.stateChanged(this, this, this, ProjectEventEnum.SAVE);
  }
  
  /**
   * @author Bill Darbie
   */
  public void cancelSaveAs()
  {
    FileUtilAxi.cancelDirectoryCopy();
    _cancelSaveAs = true;
  }

  /**
   * @author Bill Darbie
   */
  public static void delete(String projName) throws DatastoreException
  {
    Assert.expect(projName != null);

    String dirName = Directory.getProjectDir(projName);
    if (FileUtilAxi.exists(dirName))
      FileUtilAxi.delete(dirName);
  }

  /**
   * Delete the project from disk.
   * @author Bill Darbie
   */
  public void delete() throws DatastoreException
  {
    String projName = getName();
    _projectState.disable();
    try
    {
      _projectObservable.setEnabled(false);
      try
      {
        delete(projName);
      }
      finally
      {
        _projectObservable.setEnabled(true);
      }
      _projectObservable.stateChanged(Project.class.getClass(), projName, null, ProjectEventEnum.DELETE);
    }
    finally
    {
      _projectState.enable();
    }
  }

  /**
   * Normally, the Remote Server will load Calibration and Configuration Information. However
   * during Unit Testing no remote server is instaniated, so this function will load all calibration
   * configuration information.
   *
   * Because this is only run during UnitTesting, we don't return a list of warnings because
   * we are not testing this functionality.
   *
   * @author Erica Wheatcroft
   */
  private static void loadCalAndConfigInformationForUnitTest() throws DatastoreException
  {
    if (UnitTest.unitTesting())
    {
      Config config = Config.getInstance();

      if (config.isLoaded() == false)
        config.loadIfNecessary();
    }
  }

  /**
   * @author Bill Darbie
   */
  public void checkProjectName(String name) throws BusinessException
  {
    if (isProjectNameValid(name) == false)
      throw new InvalidNameBusinessException("panel", name, getProjectNameIllegalChars());
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectNameIllegalChars()
  {
    return " " + FileName.getIllegalChars();
  }

  /**
   * @author Bill Darbie
   */
  public static boolean isProjectNameValid(String projectName)
  {
    Assert.expect(projectName != null);

    // glb - added check for "ends with a period" - windows doesn't like that
    if (projectName.contains(" ") || FileName.hasIllegalChars(projectName) || projectName.equals("") ||
        projectName.endsWith("."))
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  public void setName(String name)
  {
    Assert.expect(name != null);
    Assert.expect(isProjectNameValid(name));


    if (name.equals(_name))
      return;

    String oldValue = _name;
    _projectObservable.setEnabled(false);
    try
    {
      _name = name.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, name, ProjectEventEnum.NAME);
  }

  /**
   * @author Bill Darbie
   */
  public String getName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author Bill Darbie
   */
  public void setPanel(Panel panel)
  {
    Assert.expect(panel != null);

    if (panel == _panel)
      return;

    Panel oldValue = _panel;
    _projectObservable.setEnabled(false);
    try
    {
      _panel = panel;
      _panel.setProject(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, panel, ProjectEventEnum.PANEL);
  }

  /**
   * @author Bill Darbie
   */
  public Panel getPanel()
  {
    Assert.expect(_panel != null);

    return _panel;
  }

  /**
   *
   * @return boolean
   *
   * @author Tan Hock Zoon
   */
  public boolean doesPanelExist()
  {
    return _panel != null;
  }

  /**
   * There are two version numbers.  The major version gets
   * incremented whenever the project is changed in such a way that
   * would cause the TestProgram to be invalid.  The minor version
   * gets incremented whenever the projectis changed in a way that
   * does not invalidate the TestProgram.
   * @author Bill Darbie
   */
  public void setTestProgramVersion(int testProgramVersion)
  {
    Assert.expect(testProgramVersion >= 0);

    if (testProgramVersion == _testProgramVersion)
      return;

    int oldValue = _testProgramVersion;
    _projectObservable.setEnabled(false);
    try
    {
      _testProgramVersion = testProgramVersion;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, testProgramVersion, ProjectEventEnum.PROJECT_TEST_PROGRAM_VERSION);
  }

  /**
   * There are two version numbers.  The major version gets
   * incremented whenever the project is changed in such a way that
   * would cause the TestProgram to be invalid.  The minor version
   * gets incremented whenever the projectis changed in a way that
   * does not invalidate the TestProgram.
   * @author Bill Darbie
   */
  public void setMinorVersion(int minorVersion)
  {
    Assert.expect(minorVersion >= 0);

    if (minorVersion == _minorVersion)
      return;

    int oldValue = _minorVersion;
    _projectObservable.setEnabled(false);
    try
    {
      _minorVersion = minorVersion;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, minorVersion, ProjectEventEnum.PROJECT_MINOR_VERSION);
  }

  /**
   * The version number is in the format MM.mm where MM is the
   *  major version number, and mm is the minor version number.
   *   - majorVersion gets incremented whenever the program is changed
   *     in such a way that would cause any reconstructed images that
   *     may have been collected to be invalid for any reason.
   *   - minorVersion gets incremented whenever the program is changed,
   *     but that change does not cause any reconstructed images to
   *     be invalid
   * @author Bill Darbie
   */
  public double getVersion()
  {
    int minorVersion = getMinorVersion();
    int i = 10;
    while (minorVersion > i)
    {
      i *= 10;
    }

    double version = getTestProgramVersion() + ((double)minorVersion / (double)i);
    version = MathUtil.roundToPlaces(version, 10);
    return version;
  }

  /**
   * @author Bill Darbie
   */
  public int getTestProgramVersion()
  {
    int version = _testProgramVersion;
    if ((_projectState.isProjectMajorStateValid() == false) && (_saveInProgress == false))
      ++version;

    Assert.expect(version > 0);
    return version;
  }

  /**
   * @author Bill Darbie
   */
  public int getMinorVersion()
  {
    int version = _minorVersion;
    if (_saveInProgress == false)
    {
      if (_projectState.isProjectMajorStateValid() == false)
        version = 0;
      else if (_projectState.isProjectMinorStateValid() == false)
        ++version;
    }

    Assert.expect(version >= 0);
    return version;
  }

  /**
   * @author Bill Darbie
   */
  public int getProgramGenerationVersion()
  {
    if (_programGenerationVersion == 0)
      _programGenerationVersion = 1; /** @todo wpd - G$ - call getProgramGenerationVersion() here */

    return _programGenerationVersion;
  }

  /**
   * @author Bill Darbie
   */
  public void setProgramGenerationVersion(int programGenerationVersion)
  {
    Assert.expect(programGenerationVersion > 0);

    int oldValue = _programGenerationVersion;
    _projectObservable.setEnabled(false);
    try
    {
      _programGenerationVersion = programGenerationVersion;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, programGenerationVersion, ProjectEventEnum.PROGRAM_GENERATION_VERSION);
  }

  /**
   * @author Bill Darbie
   * @author George A. David
   */
  public void acquireVerificationImages(TestSubProgram subProgram, TestExecutionTimer testExecutionTimer, boolean alignBeforeAcquiringImages, boolean useRFP) throws XrayTesterException
  {
    Assert.expect(testExecutionTimer != null);

    _projectObservable.setEnabled(false);
    try
    {
      ImageGeneration.getInstance().acquireAllVerificationImages(subProgram, testExecutionTimer, alignBeforeAcquiringImages, useRFP);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, ProjectEventEnum.VERIFICATION_IMAGES_GENERATED);
  }

  /**
   * @author Bill Darbie
   * @author George A. David
   */
  public void alignAndAcquireVerificationImages(TestExecutionTimer testExecutionTimer, boolean alignBeforeAcquiringImages) throws XrayTesterException
  {
    Assert.expect(testExecutionTimer != null);

    _projectObservable.setEnabled(false);
    try
    {
      ImageGeneration.getInstance().alignAndAcquireVerificationImages(getTestProgram(), testExecutionTimer, alignBeforeAcquiringImages);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, ProjectEventEnum.VERIFICATION_IMAGES_GENERATED);
  }

  /**
   * @author Scott Richardson
   */
  public void acquireVirtualLiveImages(TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(testExecutionTimer != null);

    _projectObservable.setEnabled(false);
    try
    {
      VirtualLiveImageGeneration.getInstance().acquireVirtualLiveImages(getVirtualLiveTestProgram(), testExecutionTimer);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, ProjectEventEnum.VERIFICATION_IMAGES_GENERATED);
  }


      /**
   * @author Scott Richardson
   * @author khang-shian.sham
   */
  public void acquireVirtualLiveImages(TestExecutionTimer testExecutionTimer, TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testExecutionTimer != null);

    _projectObservable.setEnabled(false);
    try
    {
      VirtualLiveImageGeneration.getInstance().acquireVirtualLiveImages(testProgram, testExecutionTimer);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, ProjectEventEnum.VERIFICATION_IMAGES_GENERATED);
  }

  
  /**
   * @author Scott Richardson
   */
  public void markReconstructionRegionsComplete() throws XrayTesterException
  {
    VirtualLiveImageGeneration.getInstance().markReconstructionRegionsComplete(getVirtualLiveTestProgram());
  }

  /**
   * @author Bill Darbie
   */
  public int getVerificationImagesVersion()
  {
    int version = _verificationImagesVersion;
    if (_projectState.areVerificationImagesValid() == false)
      ++version;

    return version;
  }

  /**
   * @author Bill Darbie
   */
  public boolean areVerificationImagesValid(boolean isNeedCheckLatestProgram)
  {
    if (_projectState.areVerificationImagesValid() && _imageManager.hasValidVerificationImages(this, isNeedCheckLatestProgram))
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public boolean areVerificationImagesValid(TestSubProgram subProgram)
  {
    if (_projectState.areVerificationImagesValid() && _imageManager.hasValidVerificationImages(subProgram))
      return true;

    return false;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean doVerificationImagesExist(TestSubProgram subProgram)
  {
    if (_imageManager.hasValidVerificationImages(subProgram))
      return true;
    
    return false;
  }

  /**
   * @author Bill Darbie
   */
  public boolean areShortAndSubtypeLearningFilesValid()
  {
    String projectName = getName();

    if (doesShortLearningFileExist(projectName) && doesSubtypeLearningFileExist(projectName) &&
        _projectState.isShortLearningFileValid() && _projectState.isSubtypeLearningFileValid())
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasBeenModified()
  {
    if (_projectState.isProjectStateValid() == false)
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public void setCadXmlChecksum(long checksum)
  {
    // NOTE: this gets set when we save xml files, so no _projectObservable.stateChanged() call is needed
    _cadXmlChecksum = checksum;
  }

  /**
   * @author Bill Darbie
   */
  public long getCadXmlChecksum()
  {
    return _cadXmlChecksum;
  }

  /**
   * @author Bill Darbie
   */
  public void setSettingsXmlChecksum(long checksum)
  {
    // NOTE: this gets set when we save xml files, so no _projectObservable.stateChanged() call is needed
    _settingsXmlChecksum = checksum;
  }

  /**
   * @author Bill Darbie
   */
  public long getSettingsXmlChecksum()
  {
    return _settingsXmlChecksum;
  }

  /**
   * @author Bill Darbie
   */
  public void setLastModificationTimeInMillis(long lastModificationTimeInMillis)
  {
    long oldValue = _lastModificationTimeInMillis;
    _projectObservable.setEnabled(false);
    try
    {
      _lastModificationTimeInMillis = lastModificationTimeInMillis;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, lastModificationTimeInMillis, ProjectEventEnum.LAST_MODIFICATION_TIME);
  }

  /**
   * @author Bill Darbie
   */
  private void updateLastModificationTimeInMillis()
  {
    setLastModificationTimeInMillis(System.currentTimeMillis());
  }

  /**
   * @author Bill Darbie
   */
  public long getLastModificationTimeInMillis()
  {
    return _lastModificationTimeInMillis;
  }

  /**
   * @author Bill Darbie
   */
  /**
   * @author Bill Darbie
   */
  public void setTargetCustomerName(String targetCustomerName)
  {
    Assert.expect(targetCustomerName != null);

    String oldValue = _targetCustomerName;
    _projectObservable.setEnabled(false);
    try
    {
      _targetCustomerName = targetCustomerName;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, targetCustomerName, ProjectEventEnum.TARGET_CUSTOMER_NAME);
  }

  /**
   * @author Bill Darbie
   */
  public String getTargetCustomerName()
  {
    Assert.expect(_targetCustomerName != null);
    return _targetCustomerName;
  }

    /**
   * @author Bill Darbie
   */
  public void setProgrammerName(String programmerName)
  {
    Assert.expect(programmerName != null);

    String oldValue = _programmerName;
    _projectObservable.setEnabled(false);
    try
    {
      _programmerName = programmerName;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, programmerName, ProjectEventEnum.PROGRAMMER_NAME);
  }

  /**
   * @author Bill Darbie
   */
  public String getProgrammerName()
  {
    Assert.expect(_programmerName != null);
    return _programmerName;
  }

  /**
   * @author Bill Darbie
   */
  public void setSoftwareVersionOfProjectLastSave(String softwareVersionOfLastSave)
  {
    Assert.expect(softwareVersionOfLastSave != null);

    if (softwareVersionOfLastSave == _softwareVersionOfLastSave)
      return;

    String oldValue = _softwareVersionOfLastSave;
    _projectObservable.setEnabled(false);
    try
    {
      _softwareVersionOfLastSave = softwareVersionOfLastSave;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, softwareVersionOfLastSave, ProjectEventEnum.SOFTWARE_VERSION_CREATION);
  }
  
  /**
   * @author Kee Chin Seong
   * @return 
   */
  public boolean hasSoftwareVersionOfProjectLastSave()
  {
     return (Double.parseDouble(_softwareVersionOfLastSave) >= 0.0) ? true : false;
  }
  
  /**
   * @author Bill Darbie
   */
  public String getSoftwareVersionOfProjectLastSave()
  {
    Assert.expect(_softwareVersionOfLastSave != null);

    return _softwareVersionOfLastSave;
  }

  /**
   * @author Bill Darbie
   */
  public void setNotes(String notes)
  {
    Assert.expect(notes != null);

    if (notes.equals(_notes))
      return;

    String oldValue = _notes;
    _projectObservable.setEnabled(false);
    try
    {
      _notes = notes.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, notes, ProjectEventEnum.NOTES);
  }

  /**
   * @author Bill Darbie
   */
  public void appendNotes(String appendStr)
  {
    Assert.expect(appendStr != null);

    if (_notes == null)
      _notes = "";

    String oldValue = _notes;
    _projectObservable.setEnabled(false);
    try
    {
      _notes = _notes + appendStr;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _notes, ProjectEventEnum.NOTES);
  }

  /**
   * @author Bill Darbie
   */
  public String getNotes()
  {
    Assert.expect(_notes != null);

    return _notes;
  }

  /**
   * @author Bill Darbie
   */
  public void setInitialThresholdsSetName(String initialThresholdsSetName) throws DatastoreException
  {
    Assert.expect(initialThresholdsSetName != null);

    if (initialThresholdsSetName.equals(_initialThresholdsSetName))
      return;

    String oldValue = _initialThresholdsSetName;
    _projectObservable.setEnabled(false);
    try
    {
      _initialThresholdsSetName = initialThresholdsSetName.intern();
      ProjectInitialThresholds.getInstance().setInitialThresholdsFileFullPath(getInitialThresholdsSetFullPath());
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, initialThresholdsSetName, ProjectEventEnum.INITIAL_THESHOLDS);
  }

  /**
   * Save the ThresholdSet Name without notify any Observer and without open the initialThresholdSet
   * It used during the read project setting to avoid throw exception if the initialThresholdFile Not Found
   * @author Wei Chin
   */
  public void saveInitialThresholdsSetName(String initialThresholdsSetName)
  {
    Assert.expect(initialThresholdsSetName != null);

    if (initialThresholdsSetName.equals(_initialThresholdsSetName))
      return;

    _initialThresholdsSetName = initialThresholdsSetName.intern();
  }

  /**
   * @author Andy Mechtenberg
   */
   public String getInitialThresholdsSetName()
   {
     Assert.expect(_initialThresholdsSetName != null);

     return _initialThresholdsSetName;
   }
   
   /**
    * @author Wei Chin, Chong
    */
    public String getInitialThresholdsSetFullPath()
    {
      Assert.expect(_initialThresholdsSetName != null);
      if(_initialThresholdsSetName.equalsIgnoreCase(FileName.getInitialThresholdsDefaultFileWithoutExtension()))
        return _initialThresholdsSetName;
      else
        return FileName.getInitialThresholdsFullPath(_initialThresholdsSetName);
   }
    
  /**
   * @author Bill Darbie
   */
  public void setDisplayUnits(MathUtilEnum displayUnits)
  {
    Assert.expect(displayUnits != null);

    if (displayUnits.equals(_displayUnits))
      return;

    MathUtilEnum oldValue = _displayUnits;
    _projectObservable.setEnabled(false);
    try
    {
      _displayUnits = displayUnits;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, displayUnits, ProjectEventEnum.DISPLAY_UNITS);
    _fileWriter.appendWorkingUnit(oldValue, displayUnits);
  }

  /**
   * @author Bill Darbie
   */
  public MathUtilEnum getDisplayUnits()
  {
    Assert.expect(_displayUnits != null);

    return _displayUnits;
  }

  /**
   * @author Bill Darbie
   */
  public void setReadyForProduction(boolean readyForProduction)
  {
    if ((_readyForProduction != null) && (readyForProduction == _readyForProduction.getValue()))
      return;

    BooleanRef oldValue = _readyForProduction;
    _projectObservable.setEnabled(false);
    try
    {
      if (_readyForProduction == null)
        _readyForProduction = new BooleanRef(readyForProduction);
      else
        _readyForProduction.setValue(readyForProduction);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, readyForProduction, ProjectEventEnum.READY_FOR_PRODUCTION);
  }

  /**
   * @return true if this project has been flagged as being ready for production use.
   * @author Bill Darbie
   */
  public boolean isReadyForProduction()
  {
    Assert.expect(_readyForProduction != null);

    return _readyForProduction.getValue();
  }

  /**
   * determine if the project exists locally
   * @param projectName the name of the project to check
   * @author George A. David
   */
  public static boolean doesProjectExistLocally(String projectName)
  {
    Assert.expect(projectName != null);

    String projectPath = Directory.getProjectDir(projectName);

    return FileUtil.exists(projectPath);
  }

  /**
   * determine if the project exists in the offline workstation
   * @param projectName the name of the project to check
   * @author George A. David
   */
  public static boolean doesProjectExistOffline(String projectName)
  {
    Assert.expect(projectName != null);

    String offlineProjectPath = Directory.getOfflineProjectDir(projectName);

    return FileUtil.exists(offlineProjectPath);
  }

  /**
   * transfer the project to the offline workstations
   * @param projectName the name of the project to be transferred
   * @author George A. David
   */
  public static void transferProjectOffline(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    boolean offlineProjectExists = doesProjectExistOffline(projectName);

    // first determine if there is enough disk space to transfer over the files
    checkOfflineDiskSpace(projectName);

    // copy over the project to a temporary offline path
    copyProjectToTemporaryOfflinePath(projectName);

    if (offlineProjectExists)
    {
      // backup the offline project
      backupOfflineProject(projectName);
    }

    // move over the temporary project dir (previously created) to the offline project dir
    moveTemporaryOfflineProjectDirToOfflineProjectDir(projectName);
  }

  /**
   * check if there is enough space to transfer the project offline
   * @param projectName the name of the project to transfer
   * @author George A. David
   */
  private static void checkOfflineDiskSpace(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    String projectDir = Directory.getProjectDir(projectName);
    String offlinePath = Directory.getOfflineWorkstationPath();
    long projectSize = FileUtil.getSizeOfDirectoryContentsInBytes(projectDir);
    long freeDiskSpace = FileUtil.getAvailableDiskSpaceInBytes(offlinePath);
    if (freeDiskSpace < projectSize)
    {
      throw new InsufficientSpaceToTransferProjectOfflineDatastoreException(projectName, offlinePath);
    }
  }

  /**
   * copy over the project to a temporary offline location.
   * @param projectName the name of the project
   * @author George A. David
   */
  private static void copyProjectToTemporaryOfflinePath(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    String localProjectDir = Directory.getProjectDir(projectName);
    String offlineProjectDir = Directory.getOfflineProjectDir(projectName);
    String temporaryOfflineProjectDir = Directory.getOfflineTempProjectDir(projectName);

    // we never copy images to the offline workstation.
    List<String> dontCopyFiles = new ArrayList<String>();
    // don't copy over the backup
    dontCopyFiles.add(Directory.getBackupProjectDir(projectName));

    // delete the current temp files if they exists
    if (FileUtil.exists(temporaryOfflineProjectDir))
    {
      try
      {
        FileUtil.deleteDirectoryContents(temporaryOfflineProjectDir);
      }
      catch (CouldNotDeleteFileException ex)
      {
        DatastoreException dex =  new FailedTransferringProjectOfflineDatastoreException(projectName,
                                                                                         Directory.getOfflineWorkstationPath());
        dex.initCause(ex);
        throw dex;
      }
    }
    else
    {
      //create the temporary directory
      try
      {
        FileUtil.createDirectory(temporaryOfflineProjectDir);
      }
      catch (CouldNotCreateFileException ex)
      {
        DatastoreException dex = new FailedTransferringProjectOfflineDatastoreException(projectName,
            Directory.getOfflineWorkstationPath());
        dex.initCause(ex);
        throw dex;
      }
    }

    try
    {
      FileUtil.copyDirectoryRecursively(localProjectDir, temporaryOfflineProjectDir, dontCopyFiles);
    }
    catch(FileDoesNotExistException exception)
    {
      // try deleting the partially written data
      try
      {
        FileUtil.delete(temporaryOfflineProjectDir);
      }
      catch (CouldNotDeleteFileException ex)
      {
        // do nothing
      }

      DatastoreException dex = new FailedTransferringProjectOfflineDatastoreException(projectName,
                                                                             Directory.getOfflineWorkstationPath());
      dex.initCause(exception);
      throw dex;
    }
    catch(CouldNotCopyFileException exception)
    {
      // try deleting the partially written data
      try
      {
        FileUtil.delete(temporaryOfflineProjectDir);
      }
      catch (CouldNotDeleteFileException ex)
      {
        // do nothing
      }

      DatastoreException dex = new FailedTransferringProjectOfflineDatastoreException(projectName,
                                                                             Directory.getOfflineWorkstationPath());
      dex.initCause(exception);
      throw dex;
    }
    catch(CouldNotCreateFileException exception)
    {
      // try deleting the partially written data
      try
      {
        FileUtil.delete(temporaryOfflineProjectDir);
      }
      catch (CouldNotDeleteFileException ex)
      {
        // do nothing
      }

      DatastoreException dex = new FailedTransferringProjectOfflineDatastoreException(projectName,
                                                                             Directory.getOfflineWorkstationPath());
      dex.initCause(exception);
      throw dex;
    }
    catch(FileFoundWhereDirectoryWasExpectedException exception)
    {
      // try deleting the partially written data
      try
      {
        FileUtil.delete(temporaryOfflineProjectDir);
      }
      catch (CouldNotDeleteFileException ex)
      {
        // do nothing
      }

      DatastoreException dex = new FailedTransferringProjectOfflineDatastoreException(projectName,
                                                                             Directory.getOfflineWorkstationPath());
      dex.initCause(exception);
      throw dex;
    }
  }

  /**
   * backup the offline project
   * @param projectName the name of the project to backup
   * @author George A. David
   */
  private static void backupOfflineProject(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    String offlineProjectDir = Directory.getOfflineProjectDir(projectName);
    Assert.expect(FileUtil.exists(offlineProjectDir));

    String offlineBackupProjectDir = Directory.getOfflineBackupProjectDir(projectName);

    // delete the current backup if it exists
    if (FileUtil.exists(offlineBackupProjectDir))
    {
      try
      {
        FileUtil.deleteDirectoryContents(offlineBackupProjectDir);
      }
      catch (CouldNotDeleteFileException ex)
      {
        DatastoreException dex =  new FailedTransferringProjectOfflineDatastoreException(projectName,
                                                                                         Directory.getOfflineWorkstationPath());
        dex.initCause(ex);
        throw dex;
      }
    }
    else
    {
      try
      {
        FileUtil.createDirectory(offlineBackupProjectDir);
      }
      catch (CouldNotCreateFileException ex)
      {
        DatastoreException dex =  new FailedTransferringProjectOfflineDatastoreException(projectName,
                                                                                         Directory.getOfflineWorkstationPath());
        dex.initCause(ex);
        throw dex;
      }
    }

    List<String> dontMoveFiles = new ArrayList<String>();
    dontMoveFiles.add(offlineBackupProjectDir);
    dontMoveFiles.add(Directory.getOfflineTempProjectDir(projectName));
    try
    {
      FileUtil.moveDirectoryContents(offlineProjectDir, offlineBackupProjectDir, dontMoveFiles);
    }
    catch (CouldNotRenameFileException ex)
    {
      DatastoreException dex =  new FailedTransferringProjectOfflineDatastoreException(projectName,
                                                                                       Directory.getOfflineWorkstationPath());
      dex.initCause(ex);
      throw dex;
    }
    catch (FileFoundWhereDirectoryWasExpectedException ex)
    {
      DatastoreException dex =  new FailedTransferringProjectOfflineDatastoreException(projectName,
                                                                                       Directory.getOfflineWorkstationPath());
      dex.initCause(ex);
      throw dex;
    }
    catch (FileDoesNotExistException ex)
    {
      DatastoreException dex =  new FailedTransferringProjectOfflineDatastoreException(projectName,
                                                                                       Directory.getOfflineWorkstationPath());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * move the temporary offline project dir to the actual offline project dir
   * @author George A. David
   */
  private static void moveTemporaryOfflineProjectDirToOfflineProjectDir(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    String offlineProjectDir = Directory.getOfflineProjectDir(projectName);
    String temporaryOfflineProjectDir = Directory.getOfflineTempProjectDir(projectName);
    try
    {
      FileUtil.moveDirectoryContents(temporaryOfflineProjectDir, offlineProjectDir);
    }
    catch (CouldNotRenameFileException ex)
    {
      DatastoreException dex =  new FailedTransferringProjectOfflineDatastoreException(projectName,
                                                                                       Directory.getOfflineWorkstationPath());
      dex.initCause(ex);
      throw dex;
    }
    catch (FileFoundWhereDirectoryWasExpectedException ex)
    {
      DatastoreException dex =  new FailedTransferringProjectOfflineDatastoreException(projectName,
                                                                                       Directory.getOfflineWorkstationPath());
      dex.initCause(ex);
      throw dex;
    }
    catch (FileDoesNotExistException ex)
    {
      DatastoreException dex =  new FailedTransferringProjectOfflineDatastoreException(projectName,
                                                                                       Directory.getOfflineWorkstationPath());
      dex.initCause(ex);
      throw dex;
    }

    // the temp directory is now empty, delete it
    try
    {
      FileUtil.delete(temporaryOfflineProjectDir);
    }
    catch (CouldNotDeleteFileException ex)
    {
      // do nothing
    }
  }

  /**
   * @author Laura Cormos
   */
  public static double getMaxAllowedJointHeightInDisplayUnits(MathUtilEnum displayUnits)
  {
    Config softwareConfig = Config.getInstance();
    Assert.expect(softwareConfig != null);
    Assert.expect(softwareConfig.getValueType(SoftwareConfigEnum.MAXIMUM_JOINT_HEIGHT_ALLOWED_FROM_USER_IN_NANOMETERS).equals(TypeEnum.INTEGER));
    int value = softwareConfig.getIntValue(SoftwareConfigEnum.MAXIMUM_JOINT_HEIGHT_ALLOWED_FROM_USER_IN_NANOMETERS);
    return MathUtil.convertUnits(value, MathUtilEnum.NANOMETERS, displayUnits);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isTestProgramValid()
  {
    /** @todo wpd - once G$ adds get() methods to testProgram I need to check that
     *              those settings match the current machine - if they do not that
     *              should cause the program to be regenerated.
     *
     * Also put code here to check if the hardware has changed in an incompatible
     * way - if it has regenerate!
     */
    if ((_testProgram == null) ||
        (_projectState.isTestProgramValid() == false))
      return false;

    return true;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isTestProgramHasUserGainChanged()
  {
    if((_testProgram == null) ||
        (_projectState.isTestProgramValidIgnoringUserGainChanges() == false))
      return true;
    
    return false;
  }
  
  /*
   * @author sheng chuan
   */
  public boolean isTestProgramHasStageSpeedChanged()
  {
    if((_testProgram == null) ||
        (_projectState.isTestProgramValidIgnoringStageSpeedChanges() == false))
      return true;
    
    return false;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isTestProgramSignalOrArtifactCompensationChanged()
  {
    if((_testProgram == null) ||
        (_projectState.isTestProgramValidIgnoringSignalOrArtifactCompensationChanges() == false))
      return true;
    
    return false;
  }
  
    /*
   * @author Goh Bee Hoon
   */
  public boolean isTestProgramHasBoardZOffsetChanged()
  {
    if((_testProgram == null) ||
        (_projectState.isTestProgramValidIgnoringBoardZOffsetChanges() == false))
      return true;
    
    return false;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isTestProgramValidIgnoringAlignmentRegions()
  {
    if ((_testProgram == null) ||
        (_projectState.isTestProgramValidIgnoringAlignmentRegions() == false))
      return false;

    return true;
  }
  
  /**
   * XCR1710 - Change subtype feature on Result tab.
   * @author Jack Hwee
   */
  public boolean isTestProgramValidIgnoringTunedSliceHeightChanges()
  {
    if ((_testProgram == null) ||
        (_projectState.isTestProgramValidIgnoringTunedSliceHeightChanges() == false))
      return false;

    return true;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isTestProgramCheckSumValid()
  {
    if( _testProgram == null ||
        _projectState.isTestProgramCheckSumValid() == false)
    {
      return false;
    }
    return true;
  }
  
  /*
   * @author Kee Chin Seong
   * - This is just a minor reset of test program.
   * - Make sure checkSum is invalidate.
   */
  public void resetTestProgram()
  {
    _programGeneration.resetTestProgram(_testProgram);
    _testProgram.invalidateCheckSum();
  }
  
  /*
   * @Kee Chin Seong
   */
  public void regenerateTestProgram()
  {
    _testProgram = _programGeneration.generateTestProgram(this);
    _testProgram.invalidateCheckSum();
  }

  /**
   * Get the Test Program, generating one if any part of the program is invalid.
   * @author Bill Darbie
   */
  public TestProgram getTestProgram()
  {
    if (isTestProgramValidIgnoringAlignmentRegions() && 
        (isTestProgramValid() == false) &&
        //(isTestProgramHasUserGainChanged() == false) &&
        (isTestProgramSignalOrArtifactCompensationChanged() == false) &&
        (isTestProgramHasBoardZOffsetChanged() == false))
    {
      // we have a test program that is valid except that it's alignment regions need to be updated.
      _programGeneration.updateAlignmentRegions(_testProgram);
      _testProgram.invalidateCheckSum();
      _projectObservable.stateChanged(this, null, _testProgram, ProjectEventEnum.TEST_PROGRAM_UPDATED_WITH_ALIGNMENT_REGIONS);
//      _projectObservable.stateChanged(this, this, _testProgram, ProjectEventEnum.TEST_PROGRAM_CHECK_SUM_INVALIDATED);
    }
    else if (isTestProgramValidIgnoringAlignmentRegions() == false &&  _checkLatestTestProgram)
    {
      // we have to regenerate the entire test program
      _projectObservable.stateChanged(this, null, _testProgram, ProjectEventEnum.TEST_PROGRAM_CLEARING);
      _testProgram = _programGeneration.generateTestProgram(this);
      
      if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) == true)
          generateScanPathForProject(_testProgram);
      _testProgram.invalidateCheckSum();
      _projectObservable.stateChanged(this, null, _testProgram, ProjectEventEnum.TEST_PROGRAM_GENERATED);
//      _projectObservable.stateChanged(this, this, _testProgram, ProjectEventEnum.TEST_PROGRAM_CHECK_SUM_INVALIDATED);
    }
    else
    {
       if(isTestProgramHasBoardZOffsetChanged())
       {
        _programGeneration.rearrangeTestProgramBoardZOffset(_testProgram);
        _projectObservable.stateChanged(this, null, _testProgram, ProjectEventEnum.TEST_PROGRAM_BOARD_Z_OFFSET_CHANGED);
        if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.DEBUG_BOARD_Z_OFFSET))
          System.out.println("DebugBoardZOffset - rearrangeTestProgramBoardZOffset");
       }
       //seperate from the if above. this 2 portion of if must be go through
      //because if the user change IL and Gain at same time it will cause problem
      /*if(isTestProgramHasUserGainChanged())
      {
        _programGeneration.rearrageTestProgramUserGain(_testProgram);
        _testProgram.invalidateCheckSum();
        _projectObservable.stateChanged(this, null, _testProgram, ProjectEventEnum.TEST_PROGRAM_USER_GAIN_CHANGED);
      }*/
      if(isTestProgramSignalOrArtifactCompensationChanged())
      {
        _programGeneration.resetupArtifacts(_testProgram);
        // no need invalidate checksum because IL and IC has been checked the checksum
        _projectObservable.stateChanged(this, null, _testProgram, ProjectEventEnum.TEST_PROGRAM_SIGNAL_OR_ARTIFACT_COMPENSATION_CHANGED);
      }
    }
      
    Assert.expect(_testProgram != null);

    if( _testProgram.isNeighboringAssignmentDone() == false)
    {
      _testProgram = _programGeneration.determineNeighborsForAll(_testProgram);
    }
    
    if (isTestProgramCheckSumValid() == false)
    {
      _testProgram.invalidateCheckSum();
      _projectObservable.stateChanged(this, this, _testProgram, ProjectEventEnum.TEST_PROGRAM_CHECK_SUM_INVALIDATED);
    }

    return _testProgram;
  }
  
  /*
   * @author Kee Chin Seong
   * - In order to make sure the scan path is having filter program, only let it pass thru
   */
  private static void generateScanPathForProject(TestProgram testProgram)
  {
    if(testProgram.getAllInspectableTestSubPrograms().isEmpty() == true)
      return;
    
    try
    {  
      ImagingChainProgramGenerator imagingChainProgramGenerator = new ImagingChainProgramGenerator(
          ImageAcquisitionModeEnum.PRODUCTION,
          ImagingChainProgramGenerator.getDefaultScanStrategy(),
          0);
      TestExecutionTimer testExecutionTimer = new TestExecutionTimer();
      double appropriateMaxSliceHeight = XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(testProgram.getProject().getPanel().getThicknessInNanometers(), testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers());
      imagingChainProgramGenerator.generateProgramsForLowerImagingChainHardware(
         testProgram,
         testExecutionTimer,
         appropriateMaxSliceHeight,
         Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers(testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers())));
    }
    catch(XrayTesterException xex)
    {
      // Do nothing as this is purposely dump out scan path in offline mode.
    }
  }

  /**
   * Get the Virtual Live TestProgam
   * @todo sgr this is more or less a hack, and not guaranteed to work in the future. The problem is any time
   * getTestProgram is called, I have to trust that it is the VirtualLiveTestProgram that will be returned
   * (i.e. that a standard TestProgram will not be created in the process of getTestProgram()). Proposed solution:
   * when getTestProgram() is called and we are in SoftwareConfigEnum.VIRTUAL_LIVE_MODE, then return the
   * VirtualLiveTestProgram.
   * Also, although we don't really need to inform anyone of our state change via the _projectObservable.stateChanged(...)
   * method, it still might be good to create a VIRTUAL_LIVE_TEST_PROGRAM_GENERATED, which will be observed by
   * PanelGraphicsEngineSetup.updateProjectData(...), to preform the necessary graphics.
   * @author Scott Richardson
   */
  public TestProgram getVirtualLiveTestProgram()
  {
    if (_testProgram == null)
    {
      _testProgram = VirtualLiveProgramGeneration.getInstance().generateVirtualLiveTestProgram(this);

//      _projectObservable.stateChanged(this, null, _testProgram, ProjectEventEnum.VIRTUAL_LIVE_TEST_PROGRAM_GENERATED);
    }

    Assert.expect(_testProgram != null);
    return _testProgram;
  }

  /**
   * @return the TestProgram if it is valid in every way except that it may have invalid Alignment Regions.
   * @author Bill Darbie
   */
  public TestProgram getTestProgramForAlignmentRegionsGeneration()
  {
    if (isTestProgramValidIgnoringAlignmentRegions() == false)
    {
      _testProgram = _programGeneration.generateTestProgram(this);
      _projectObservable.stateChanged(this, null, _testProgram, ProjectEventEnum.TEST_PROGRAM_GENERATED);
    }
    return _testProgram;
  }

  /**
   * @author George A. David
   */
  public boolean isCompatibleWithImageSet(ImageSetData imageSetData) throws DatastoreException
  {
    return _imageManager.isCompatible(this, imageSetData);
  }

  /**
   * @author George A. David
   */
  public void setDatabaseVersion(int databaseVersion)
  {
    Assert.expect(databaseVersion >= 0);

    _databaseVersion = databaseVersion;
  }

  /**
   * @author George A. David
   */
  public int getDatabaseVersion()
  {
    Assert.expect(_databaseVersion >= 0);

    return _databaseVersion;
  }

  /**
   * @author George A. David
   */
  public void setDatabaseComment(String databaseComment)
  {
    Assert.expect(databaseComment != null);

    _databaseComment = databaseComment;
  }
  
  
  /**
   * XCR-3158 Assert when set all component to no load and save the recipe as new recipe
   * @author Kee Chin Seong
  */
  public void setCurrentLoadedProject(Project project)
  {
    Assert.expect(project != null);
    _currentProject = project;
  }

  /**
   * @author George A. David
   */
  public String getDatabaseComment()
  {
    Assert.expect(_databaseComment != null);

    return _databaseComment;
  }

  /**
   * @author Bill Darbie
   */
  public void setRepresentsInternalSystemFeatures(boolean representsInternalSystemFeatures)
  {
    _representsInternalSystemFeatures = representsInternalSystemFeatures;
  }

  /**
   * @author Bill Darbie
   */
  public boolean doesRepresentsInternalSystemFeatures()
  {
    return _representsInternalSystemFeatures;
  }

  /**
   * @author Scott Richardson
   */
  public void destroyTestProgram()
  {
    _testProgram = null;
  }


  /**
   * The nearest neighbors need to be regenerated when a Grid Array or QuadFlatNoLead joint is changed to or from "No Test"
   *
   * @author Sunit Bhalla
   * @author George Booth
   */
  public void update(Observable observable, Object object)
  {
    Assert.expect(object != null);

    if (observable instanceof ProjectObservable)
    {
      if (object instanceof ProjectChangeEvent)
      {
        ProjectChangeEvent projectChangeEvent = (ProjectChangeEvent)object;
        ProjectChangeEventEnum projectChangeEventEnum = projectChangeEvent.getProjectChangeEventEnum();

        if ((projectChangeEventEnum instanceof PadTypeSettingsEventEnum) == false)
          return;

        if (projectChangeEventEnum.equals(PadTypeSettingsEventEnum.INSPECTED) == false)
          return;

        Object source = projectChangeEvent.getSource();
        if ((source instanceof PadTypeSettings) == false)
          return;

        // If test program is invalid, it'll get regenerated.
        if (isTestProgramValid() == false)
          return;

        PadTypeSettings padTypeSettings = (PadTypeSettings)source;

        // Regenerate nearest neighbor list if this is a Grid Array
        if (padTypeSettings.getSubtype().getInspectionFamilyEnum().equals(InspectionFamilyEnum.GRID_ARRAY))
        {
          for (Pad pad : padTypeSettings.getPadType().getPads())
          {
            JointInspectionData jointInspectionData = _testProgram.getJointInspectionData(pad);
            _programGeneration.determineOpenOutlierNeighbors(jointInspectionData);
          }
        }
        // Regenerate nearest neighbor list if this is a QuadFlatNoLead pad
        else if (padTypeSettings.getSubtype().getInspectionFamilyEnum().equals(InspectionFamilyEnum.QUAD_FLAT_NO_LEAD))
        {
          for (Pad pad : padTypeSettings.getPadType().getPads())
          {
            JointInspectionData jointInspectionData = _testProgram.getJointInspectionData(pad);
            _programGeneration.determineQuadFlatNoLeadNeighbors(jointInspectionData);
          }
        }
      }
    }
  }

  /**
   * @author George A. David
   * @author Poh Kheng
   */
  public void setSystemType(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);
    if (systemType.equals(_systemType))
      return;

    SystemTypeEnum oldValue = _systemType;
    _projectObservable.setEnabled(false);
    try
    {
      _systemType = systemType;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _systemType, ProjectEventEnum.SYSTEM_TYPE);
  }

  /**
   * @author George A. David
   */
  public SystemTypeEnum getSystemType()
  {
    Assert.expect(_systemType != null);

    return _systemType;
  }

  /**
   * @author Lim, Seng Yew
   */
  public void setScanPathMethod(ScanPathMethodEnum scanPathMethod)
  {
    Assert.expect(scanPathMethod != null);
    if (scanPathMethod.equals(_scanPathMethod))
      return;

    ScanPathMethodEnum oldValue = _scanPathMethod;
    _projectObservable.setEnabled(false);
    try
    {
      _scanPathMethod = scanPathMethod;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, scanPathMethod, ProjectEventEnum.SCAN_PATH_METHOD);
  }

  /**
   * @author Lim, Seng Yew
   */
  public ScanPathMethodEnum getScanPathMethod()
  {
    // Set to use new method for new recipes(ProjectSettingsReader version 6 and above)
    // Khang Wah, 2013-09-25, bug found in scan path method 3, upgrade to 4 in order to solve it.
    if (_scanPathMethod == null)
      // _scanPathMethod = ScanPathMethodEnum.METHOD_COMMON_STEP_FIX_AND_IC_NA_FIX;
//      _scanPathMethod = ScanPathMethodEnum.METHOD_COMMON_STEP_FIX_AND_IC_NA_MIN_MAX_FIX;
      //_scanPathMethod = ScanPathMethodEnum.METHOD_MERGE_WITHOUT_NOLOAD_COMPONENTS;
      _scanPathMethod = ScanPathMethodEnum.METHOD_THROUGHOLE_PRESSFIT_SKIP_AREA_RATIO_CHECK; //Siew Yeng - XCR-2760-scanpath method 6
    
    return _scanPathMethod;
  }
  
  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public void setGenerateByNewScanRoute(boolean isGeneratedByNewScanRoute)
  {
    // Set to use new scan route generation for recipes(ProjectSettingsReader version 7 and above)
    if (isGeneratedByNewScanRoute == _isGeneratedByNewScanRoute)
    {
      return;
    }

    boolean oldValue = _isGeneratedByNewScanRoute;
    _projectObservable.setEnabled(false);
    _isGeneratedByNewScanRoute = isGeneratedByNewScanRoute;

    _projectObservable.setEnabled(true);
      
    _projectObservable.stateChanged(this, oldValue, isGeneratedByNewScanRoute, ProjectEventEnum.SET_USE_NEW_SCAN_ROUTE_GENERATION);
    _fileWriter.appendSetUseNewScanRoute(oldValue, isGeneratedByNewScanRoute);
  }
  
  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public boolean isGenerateByNewScanRoute()
  {
    return _isGeneratedByNewScanRoute;
  }
  
  /**
   * @author Further speed scan path in old scan path
   */
  public void setVarDivN(boolean isVarDivNEnable)
  {
    if (isVarDivNEnable == _isVarDivNEnable)
    {
      return;
    }

    boolean oldValue = _isVarDivNEnable;
    _projectObservable.setEnabled(false);
    _isVarDivNEnable = isVarDivNEnable;

    _projectObservable.setEnabled(true);
      
    _projectObservable.stateChanged(this, oldValue, isVarDivNEnable, ProjectEventEnum.SET_USE_VARIABLE_DIVN);
    _fileWriter.appendSetUseVariableDivN(oldValue, isVarDivNEnable);
  }
  
   /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public boolean isVarDivNEnable()
  {
    return _isVarDivNEnable || _isGeneratedByNewScanRoute;
  }
  
  /**
   * @author Further speed scan path in old scan path
   */
  public void setUseLargeTomoAngle(boolean isUseLargeTomoAngle)
  {
    _isUseLargeTomoAngle = isUseLargeTomoAngle;
  }
  
   /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public boolean isUseLargeTomoAngle()
  {
    return _isUseLargeTomoAngle;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public void setPanelExtraClearDelayInMiliSeconds(int panelExtraClearDelayInMiliSeconds)
  {
    _panelExtraClearDelayInMiliSeconds = panelExtraClearDelayInMiliSeconds;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public int getPanelExtraClearDelayInMiliSeconds()
  {
    return _panelExtraClearDelayInMiliSeconds;
  }
  
  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public void setScanRouteConfigFileName(String scanRouteConfigFileName)
  {
    // Set to refer specified scanroute.csvconfig for recipes(ProjectSettingsReader version 7 and above)
    _scanRouteConfigFileName = scanRouteConfigFileName;
  }
  
  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public String getScanRouteConfigFileName()
  {
    return _scanRouteConfigFileName;
  }

  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public String getScanRouteConfigFullPath()
  {
    return Directory.getProjectDir(_name) +  File.separator + _scanRouteConfigFileName;
  }

  /**
   * @author Ngie Xing
   */
  public String getProjectLowMagnification()
  {
    return _projectLowMagnification;
  }

  /**
   * @author Ngie Xing
   */
  public String getProjectHighMagnification()
  {
    return _projectHighMagnification;
  }
  
  /**
   * @author Ngie Xing
   */
  public void setProjectLowMagnification(String lowMagnification)
  {
    _projectLowMagnification = lowMagnification;
  }
  
  /**
   * @author Ngie Xing
   */
  public void setProjectHighMagnification(String highMagnification)
  {
    _projectHighMagnification = highMagnification;
  }
  
  /**
   * @author Ngie Xing
   */
  private static boolean isProjectAndSystemLowMagSame(Project project)
  {
    return project.getProjectLowMagnification().equals(Config.getSystemCurrentLowMagnification());
  }

  /**
   * @author Ngie Xing
   */
  private static boolean isProjectAndSystemHighMagSame(Project project)
  {
    return project.getProjectHighMagnification().equals(Config.getSystemCurrentHighMagnification());
  }
  
  /**
   * @author George A. David
   */
  private static void waitForUserResponse()
  {
    try
    {
      _waitForUserResponseLock.waitUntilTrue();
    }
    catch (Exception exception)
    {
      // should not happen
      Assert.logException(exception);
    }
  }

  /**
   * @author Ngie Xing
   */
  public static void userResponseToProceedWithProjectLoad(boolean proceedWithLoad)
  {
    _proceedWithLoad = proceedWithLoad;
    _waitForUserResponseLock.setValue(true);
  }
  
   /**
   * @author weng-jian.eoh
   * 
   * check project is converted from other system type. 
   * To prevent system check project original magnification setting is different with system and reload config again 
   */
  public static void userResponseToDifferentSystemTypeOLP(boolean proceedWithLoad, boolean loadSaveAsNewProject, String newProjectName, boolean isConvertProject)
  {
    _isProjectConvertedFromDifferentSystemType = isConvertProject;
    userResponseToSystemTypesDiffer(proceedWithLoad,loadSaveAsNewProject,newProjectName);      
  }
  
  /**
   * @author Swee yee
   */
  public static void userResponseToProceedWithDifferSolderThicknessVersion(boolean loadWithNewSolderThicknessVersion)
  {
    _loadWithNewSolderThicknessVersion = loadWithNewSolderThicknessVersion;
    _waitForUserResponseLock.setValue(true);
  }

  /**
   * @author George A. David
   */
  public static void userResponseToSystemTypesDiffer(boolean proceedWithLoad, boolean loadSaveAsNewProject)
  {
    _proceedWithLoad = proceedWithLoad;
    _loadSaveAsNewProject = loadSaveAsNewProject;
    _waitForUserResponseLock.setValue(true);
  }

  /**
   * @author George A. David
   */
  public static void userResponseToSystemTypesDiffer(boolean proceedWithLoad, boolean loadSaveAsNewProject, String newProjectName)
  {
    Assert.expect(loadSaveAsNewProject == false || (loadSaveAsNewProject && newProjectName != null));

    if (loadSaveAsNewProject == true)
      _newProjectName = newProjectName;

    userResponseToSystemTypesDiffer(proceedWithLoad, loadSaveAsNewProject);
  }

  /**
   * @author Poh Kheng
   */
  public void loadProjectSystemTypeSettingToConfig() throws DatastoreException
  {
    if(getSystemType().equals(Config.getInstance().getSystemTypeSettingLoaded()) == false)
    {
      Config.getInstance().loadSystemSpecificConfigFiles(getSystemType());
    }
  }

  /**
   * @author Poh Kheng
   */
  public boolean isSystemTypeSettingConfigCompatibleWithSystem()
  {
    return getSystemType().equals(XrayTester.getSystemType());
  }

  /**
   * @author Cheah Lee Herng
   */
  public void clearAlgorithmLearningReference()
  {
      if (_algorithmShortsLearning != null)
          _algorithmShortsLearning = null;

      if (_algorithmExpectedImageLearning != null)
          _algorithmExpectedImageLearning = null;
      
      //XCR2782
      if (_algorithmBrokenPinLearning != null)
          _algorithmBrokenPinLearning = null;      
      
      //XCR2787 by sheng chuan
      if (_algorithmExpectedImageTemplateLearning != null)
          _algorithmExpectedImageTemplateLearning = null;      
  }
  
  /**
   * @author Ngie Xing
   */
  public static void deleteAndRemoveAllShortsLearningData()
  {
    try
    {
      //Ngie Xing, delete learned data for Short Algorithm if it exists
      String projName = Project.getCurrentlyLoadedProject().getName().intern();
      if (doesShortLearningFileExist(projName))
      {
        FileUtilAxi.delete(FileName.getAlgorithmShortsLearningFullPath(projName));
      }
      if (doesShortLearningTempProjOpenFileExist(projName))
      {
        FileUtilAxi.delete(FileName.getAlgorithmShortsLearningTempProjOpenFullPath(projName));
      }
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace();
    }
  }

    /**
     * @author sham
     */
    public void setCheckLatestTestProgram(boolean checkLatestTestProgram)
    {
      if(checkLatestTestProgram)
      {
        if (_panel != null)
        {
            _panel.getPanelSettings().resetIsAllComponentSameUserGainSetting();
            _panel.getPanelSettings().resetIsAllComponentSameUserStageSpeed();
            _panel.getPanelSettings().resetLowAndHighMagSettings();
        }
      }
      _checkLatestTestProgram=checkLatestTestProgram;
    }

  /**
   * @author sham
   */
  public boolean isCheckLatestTestProgram()
  {
    return _checkLatestTestProgram;
  }
  
    /**
   * Save the project to disk.
   *
   * @author Chong Wei Chin
   */
  public void fastSave() throws DatastoreException
  {
    _saveInProgress = true;
    _projectState.disable();
    try
    {
      // since the save can now generate important events during program generation, we don't want to supress those
      // during the save, otherwise we won't get those events and we will have memory leaks.
//      _projectObservable.setEnabled(false);

      int origTestProgramVersion = _testProgramVersion;
      int origMinorVersion = _minorVersion;

      TimerUtil totalTimer = new TimerUtil();
      totalTimer.start();
      try
      {
        updateLastModificationTimeInMillis();

        // now write out the new files
        if (_projectState.isProjectMajorStateValid() == false)
        {
          ++_testProgramVersion;
          _minorVersion = 0;
        }
        else if (_projectState.isProjectMinorStateValid() == false)
        {
          ++_minorVersion;
        }

        TimerUtil timer = new TimerUtil();
        timer.reset();
        timer.start();

        // no genereate testProgram --> save time

        timer.stop();
        long time = timer.getElapsedTimeInMillis();
        if ((_displayTimes) && (UnitTest.unitTesting() == false))
        {
          System.out.println("wpd time to generate program for save: " + time);
        }

        timer.reset();
        timer.start();

        PanelSettings panelSettings = _panel.getPanelSettings();
        panelSettings.syncShortLearningStatusForAllSubtypes();

        timer.stop();
        time = timer.getElapsedTimeInMillis();
        if ((_displayTimes) && (UnitTest.unitTesting() == false))
        {
          System.out.println("wpd time to sync short learning for save: " + time);
        }

        timer.reset();
        timer.start();

        //Lim, Lay Ngor - Broken Pin - START
        panelSettings.syncBrokenPinLearningStatusForAllSubtypes();

        timer.stop();
        time = timer.getElapsedTimeInMillis();
        if ((_displayTimes) && (UnitTest.unitTesting() == false))
        {
          System.out.println("wpd time to sync broken pin learning for save: " + time);
        }
        
        timer.reset();
        timer.start();
        //Lim, Lay Ngor - Broken Pin - END

        //ShengChuan - Clear Tombstone - Start
        panelSettings.syncExpectedImageTemplateLearningStatusForAllSubtypes();
        
        timer.stop();
        time = timer.getElapsedTimeInMillis();
        if ((_displayTimes) && (UnitTest.unitTesting() == false))
        {
          System.out.println("wpd time to sync Template learning for save: " + time);
        }

        timer.reset();
        timer.start();
        //ShengChuan - Clear Tombstone - End

        saveLearnedFiles();

        timer.stop();
        time = timer.getElapsedTimeInMillis();
        if ((_displayTimes) && (UnitTest.unitTesting() == false))
        {
          System.out.println("wpd time to save learned files for save: " + time);
        }

        // reset the project state machine before saving it since it's state gets saved too
        _projectState.resetDueToProjectSave();
        _projectWriter.save(this, true);

        totalTimer.stop();
        time = totalTimer.getElapsedTimeInMillis();
        if ((_displayTimes) && (UnitTest.unitTesting() == false))
        {
          System.out.println("wpd TOTAL time to SAVE project: " + time);
        }
      }
      catch (DatastoreException dex)
      {
        _testProgramVersion = origTestProgramVersion;
        _minorVersion = origMinorVersion;

        throw dex;
      }
      finally
      {
        // since the save can now generate important events during program generation, we don't want to supress those
        // during the save, otherwise we won't get those events and we will have memory leaks.
//        _projectObservable.setEnabled(true);
      }
    }
    finally
    {
      _projectState.enable();
      _saveInProgress = false;
    }
    _projectObservable.stateChanged(this, this, this, ProjectEventEnum.SAVE);
  }
  
  /*
   * @author Kee Chin Seong
   * -
   */
  public void loadNewEmptyProject(Project project) throws DatastoreException
  {
    _projectObservable.setEnabled(false);
    
    Project oldValue = _currentProject;
    try
    {
      unloadCurrentProject();
      // set ProjectObservable to the current Project
      _projectObservable.setProjectState(project.getProjectState());

      Assert.expect(project != null);
      _currentProject = project;
      
      _isDummyProject = true;
      
      _projectObservable.addObserver(_currentProject);
      
      project._projectState.enableAfterProjectLoad();
      
      if(XrayTester.isHardwareAvailable() == false)
      {
        // we must change the system type on the offline system now before
        // any program generations happens to ensure that we use the correct
        // config values
        XrayTester.changeSystemTypeIfNecessary(project.getSystemType());
       
      }
      else
      {
          // this would save the project as a new project with the machine system type
          _currentProject.setSystemType(XrayTester.getSystemType());
          _currentProject.loadProjectSystemTypeSettingToConfig();
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(project, oldValue, project, ProjectEventEnum.SLOW_LOAD);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void loadVirtualLiveDummyProject(Project project) throws DatastoreException
  {
    _projectObservable.setEnabled(false);
    try
    {
      //unloadCurrentProject();
      // set ProjectObservable to the current Project
      _projectObservable.setProjectState(project.getProjectState());

      Assert.expect(project != null);
      _currentProject = project;
      
      _isDummyProject = true;
      
      _projectObservable.addObserver(_currentProject);
      
      project._projectState.enableAfterProjectLoad();
      
      if(XrayTester.isHardwareAvailable() == false)
      {
        // we must change the system type on the offline system now before
        // any program generations happens to ensure that we use the correct
        // config values
        XrayTester.changeSystemTypeIfNecessary(project.getSystemType());
       
      }
      else
      {
          // this would save the project as a new project with the machine system type
          _currentProject.setSystemType(XrayTester.getSystemType());
          _currentProject.loadProjectSystemTypeSettingToConfig();
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
  }
  
  /*
   * @author Kee Chin Seong
   * - Dummy panel virtual Live
   */
  public static boolean isUsingVirtualDummyProject()
  {
    return _isDummyProject;
  }
  
  /*
   * @author Kee Chin Seong
   * - Save the focus region into map by using component and region key.
   *   - this map will keep the Focus Region for Datastore and the Program Generation
   */
  public void saveFocusRegionIntoMap(Component component, List<ReconstructionRegion> rrList)
  {
    Assert.expect(_componentToFocusRegionMap != null);
    
    Map<String, List<FocusRegion>> regionToFcRegionMap = new HashMap<String, List<FocusRegion>>();
      
    for(ReconstructionRegion region : rrList)
    {
      List<FocusRegion> fcList = new ArrayList<FocusRegion>();
      for(FocusGroup fg : region.getFocusGroups())
      {
         fcList.add(fg.getFocusSearchParameters().getFocusRegion());
      }
      String regionKey = region.getBoundingRegionOfPadsInNanoMeters().getCenterX() + "_" +
                         region.getBoundingRegionOfPadsInNanoMeters().getCenterY() + "_" +
                         region.getBoundingRegionOfPadsInNanoMeters().getWidth() + "_" +
                         region.getBoundingRegionOfPadsInNanoMeters().getHeight();
          
      regionToFcRegionMap.put(regionKey, fcList);
      
      if(_componentToFocusRegionMap.containsKey(component) == false)
      {     
        _componentToFocusRegionMap.put(component, regionToFcRegionMap);
      }
      else
      {
        _componentToFocusRegionMap.get(component).put(regionKey, fcList);
      }
    }
    
  }
  
  /*
   * @author Kee Chin Seong
   * - Edit Focus region, this map must be "clear" off while certain condition met. 
   *   - Reason : To Avoid memory exploded, memory increase for junk.
   */
  public void removeComponentFocusRegionInMap(Component component)
  {
    Assert.expect(_componentToFocusRegionMap != null);
    
    if(_componentToFocusRegionMap.containsKey(component))
       _componentToFocusRegionMap.remove(component);
  }
  
  /*
   * @author Kee Chin Seong
   * - Save the focus region into map by using component and region key.
   *   - this map will keep the Focus Region for Datastore and the Program Generation
   */
  public void saveFocusRegionIntoMap(Component component, String regionKey, FocusRegion fcRegion)
  {
    Assert.expect(_componentToFocusRegionMap != null);
    
    List<FocusRegion> fcList = new ArrayList<FocusRegion>();
    Map<String, List<FocusRegion>> regionToFcRegionMap = new HashMap<String, List<FocusRegion>>();
    
    fcList.add(fcRegion);
    regionToFcRegionMap.put(regionKey, fcList);
    
    if(_componentToFocusRegionMap.containsKey(component) == false)
    {
       _componentToFocusRegionMap.put(component, regionToFcRegionMap);
    }
    else
    {
      _componentToFocusRegionMap.get(component).put(regionKey, fcList);
    }   
  }
  
  
  /*
   * @author Kee Chin Seong
   * - Get the map of focus region.
   */
  public Map<Component, Map<String, List<FocusRegion>>> getComponentToFocusRegionMap()
  {
    Assert.expect(_componentToFocusRegionMap != null);
    
    return _componentToFocusRegionMap;
  }

   /**
   * @author bee-hoon.goh
   */
  public void setEnableLargeImageView(boolean isEnableLargeViewImage)
  {
    // Set to use large image view generation for recipes(ProjectSettingsReader version 8 and above)
    if (isEnableLargeViewImage == _isEnableLargeViewImage)
      return;

    boolean oldValue = _isEnableLargeViewImage;
    _projectObservable.setEnabled(false);
    try
    {
      _isEnableLargeViewImage = isEnableLargeViewImage;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, isEnableLargeViewImage, ProjectEventEnum.ENABLE_LARGE_VIEW_IMAGE);
    _fileWriter.appendIsEnableLargeViewImage(oldValue, isEnableLargeViewImage);
  }
  
  /**
   * @author bee-hoon.goh
   */
  public boolean isEnableLargeImageView()
  {
    return _isEnableLargeViewImage;
  }
    
  /**
   * @author bee-hoon.goh
   */
  public void setGenerateMultiAngleImage(boolean isGenerateMultiAngleImage)
  {
    // Set to use multi angle image generation for recipes(ProjectSettingsReader version 8 and above)
    if (isGenerateMultiAngleImage == _isGenerateMultiAngleImage)
      return;

    boolean oldValue = _isGenerateMultiAngleImage;
    _projectObservable.setEnabled(false);
    try
    {
      _isGenerateMultiAngleImage = isGenerateMultiAngleImage;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, isGenerateMultiAngleImage, ProjectEventEnum.GENERATE_MULTI_ANGLE_IMAGE);
    _fileWriter.appendIsGenerateMultiAngleImage(oldValue, isGenerateMultiAngleImage);
  }
  
  /**
   * @author bee-hoon.goh
   */
  public boolean isGenerateMultiAngleImage()
  {
    return _isGenerateMultiAngleImage;
  }
  
  /**
   * @author bee-hoon.goh
   */
  public void setGenerateComponentImage(boolean isGenerateComponentImage)
  {
    // Set to use component image generation for recipes(ProjectSettingsReader version 8 and above)
    if (isGenerateComponentImage == _isGenerateComponentImage)
      return;

    boolean oldValue = _isGenerateComponentImage;
    _projectObservable.setEnabled(false);
    try
    {
      _isGenerateComponentImage = isGenerateComponentImage;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, isGenerateComponentImage, ProjectEventEnum.GENERATE_COMPONENT_IMAGE);
    _fileWriter.appendIsGenerateComponentImage(oldValue, isGenerateComponentImage);
  }
  
  /**
   * @author bee-hoon.goh
   */
  public boolean isGenerateComponentImage()
  {
    return _isGenerateComponentImage;
  }
  
  /**
   * @author swee-yee.wong
   */
  public boolean getProjectAlgorithmSetting(ProjectAlgorithmSettingEnum projectAlgorithmSettingEnum)
  {
    boolean projectSetting = false;
    
    Assert.expect(projectAlgorithmSettingEnum.getSelectionList() == null);
    
    //Swee Yee - moved to subtype level
//    if (projectAlgorithmSettingEnum.equals(ProjectAlgorithmSettingEnum.ENABLE_BGA_JOINT_BASED_THRESHOLD_INSPECTION))
//      projectSetting = _isBGAJointBasedThresholdInspectionEnabled;
    
    return projectSetting;
  }
  
  /**
   * @author swee-yee.wong
   */
  public String getProjectAlgorithmSelectionListSetting(ProjectAlgorithmSettingEnum projectAlgorithmSettingEnum)
  {
    String projectSetting = null;
    
    Assert.expect(projectAlgorithmSettingEnum.getSelectionList() != null);
    
    if(projectAlgorithmSettingEnum.equals(ProjectAlgorithmSettingEnum.SOLDER_THICKNESS_VERSION_SETTING))
      projectSetting = StringLocalizer.keyToString("MMGUI_SOLDER_THICKNESS_VERSION_REFERENCE_KEY") + String.valueOf(_thicknessTableVersion);
    
    Assert.expect(projectSetting != null);
    return projectSetting;
  }
  
  /**
   * @author swee-yee.wong
   */
  public boolean getProjectReconstructionSetting(ProjectReconstructionSettingEnum projectReconstructionSettingEnum)
  {
    boolean projectSetting = false;
    
    Assert.expect(projectReconstructionSettingEnum.getSelectionList() == null);
    
    if (projectReconstructionSettingEnum.equals(ProjectReconstructionSettingEnum.ENLARGE_RECONSTRUCTION_REGION))
      projectSetting = _isEnlargeReconstructionRegion;
    
    if (projectReconstructionSettingEnum.equals(ProjectReconstructionSettingEnum.SET_BGA_INSPECTION_REGION_TO_1X1))
      projectSetting = _isBGAInspectionRegionSetTo1X1;
    
    if (projectReconstructionSettingEnum.equals(ProjectReconstructionSettingEnum.NEW_SCAN_ROUTE_GENERATION))
      projectSetting = _isGeneratedByNewScanRoute;
    
    return projectSetting;
  }
  
  /**
   * @author swee-yee.wong
   */
  public String getProjectReconstructionSelectionListSetting(ProjectReconstructionSettingEnum projectReconstructionSettingEnum)
  {
    String projectSetting = null;
    
    Assert.expect(projectReconstructionSettingEnum.getSelectionList() != null);
    //temporarily there is no selection list setting for reconstruction setting
    
    Assert.expect(projectSetting != null);
    return projectSetting;
  }
  
  /**
   * @author swee-yee.wong
   */
  public boolean getProjectImageViewSetting(ProjectImageViewSettingEnum projectImageViewSettingEnum)
  {
    boolean projectSetting = false;
    
    Assert.expect(projectImageViewSettingEnum.getSelectionList() == null);
    
    if(projectImageViewSettingEnum.equals(ProjectImageViewSettingEnum.LARGE_VIEW_IMAGE))
      projectSetting = _isEnableLargeViewImage;
    else if (projectImageViewSettingEnum.equals(ProjectImageViewSettingEnum.MULTI_ANGLE_IMAGE))
      projectSetting = _isGenerateMultiAngleImage;
    //else if (projectImageViewSettingEnum.equals(ProjectImageViewSettingEnum.COMPONENT_IMAGE))
    //  projectSetting = _isGenerateComponentImage;
    
    return projectSetting;
  }
  
  /**
   * @author swee-yee.wong
   */
  public String getProjectImageViewSelectionListSetting(ProjectImageViewSettingEnum projectImageViewSettingEnum)
  {
    String projectSetting = null;
    
    Assert.expect(projectImageViewSettingEnum.getSelectionList() != null);
    //temporarily there is no selection list setting for Image View setting
    
    if(projectImageViewSettingEnum.equals(ProjectImageViewSettingEnum.DYNAMIC_RANGE_OPTIMIZATION_VERSION))
      projectSetting = String.valueOf(_dynamicRangeOptimizationVersion.getVersion());
    
    Assert.expect(projectSetting != null);
    return projectSetting;
  }
  
  /**
   * @author swee-yee.wong
   */
  public boolean getProjectProductionSetting(ProjectProductionSettingEnum projectProductionSettingEnum)
  {
    boolean projectSetting = false;
    
    Assert.expect(projectProductionSettingEnum.getSelectionList() == null);
    
    if(projectProductionSettingEnum.equals(ProjectProductionSettingEnum.PROJECT_BASED_BYPASS_MODE))
      projectSetting = _isProjectBasedBypassMode;
    
    return projectSetting;
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR2943: Alignment Fail Due to Too Close To Edge
   */
  public boolean getProjectAlignmentSetting(ProjectAlignmentSettingEnum projectAlignmentSettingEnum)
  {
    boolean projectSetting = false;
    
    Assert.expect(projectAlignmentSettingEnum.getSelectionList() == null);
    
    if(projectAlignmentSettingEnum.equals(ProjectAlignmentSettingEnum.ENLARGE_ALIGNMENT_REGION))
      projectSetting = _isEnlargeAlignmentRegion;
    
    return projectSetting;
  }
  
  /**
   * @author swee-yee.wong
   */
  public String getProjectProductionSelectionListSetting(ProjectProductionSettingEnum projectProductionSettingEnum)
  {
    String projectSetting = null;
    
    Assert.expect(projectProductionSettingEnum.getSelectionList() != null);
    //temporarily there is no selection list setting for Image View setting
    
    Assert.expect(projectSetting != null);
    return projectSetting;
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR2943: Alignment Fail Due to Too Close To Edge
   */
  public String getProjectAlignmentSelectionListSetting(ProjectAlignmentSettingEnum projectAlignmentSettingEnum)
  {
    String projectSetting = null;
    
    Assert.expect(projectAlignmentSettingEnum.getSelectionList() != null);
    
    Assert.expect(projectSetting != null);
    return projectSetting;
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean isSemiAutomatedRecipe()
  {
    return getName().contains("SEMIAUTO");
  }
  
  /**
   * XCR-2068 - Force to run CD&A ASAP if user switch between Low Mag <-> High Mag   
   * @author Anthony Fong
   * @edit Cheah Lee Herng
   */
  public void isNeedToForceRunAllCDAImmediatelyIfLowHighMagSwitching(MagnificationTypeEnum newMagnificationTypeEnum) throws DatastoreException
  {
    Assert.expect(newMagnificationTypeEnum != null);
    
    String passiveMode = Config.getInstance().getStringValue(SoftwareConfigEnum.CONFIRMATION_AND_ADJUSTMENT_MODE);
    if(passiveMode.equals(ConfirmationAndAdjustmentModeEnum.PASSIVE_MODE.toString()))
    {
      boolean hasLowMagComponents = _panel.getPanelSettings().hasLowMagnificationComponent();
      boolean hasHighMagComponents = _panel.getPanelSettings().hasHighMagnificationComponent();
      boolean hasMixMagComponents = (hasLowMagComponents == true && hasHighMagComponents == true) ? true : false;
      
      if (hasMixMagComponents == false)
      {
        if (hasLowMagComponents && newMagnificationTypeEnum.equals(MagnificationTypeEnum.HIGH))
        {
          forceToRunAllCDAImmediatelyIfLowHighMagSwitching();
        }
        else if (hasHighMagComponents && newMagnificationTypeEnum.equals(MagnificationTypeEnum.LOW))
        {
          forceToRunAllCDAImmediatelyIfLowHighMagSwitching();
        }
      }
    }
  }
  
  /**
   * Force to run CDA ASAP if user switch between Low Mag <-> High Mag
   * @author Anthony Fong
   * @edit Cheah Lee Herng
   */
  private static void forceToRunAllCDAImmediatelyIfLowHighMagSwitching() throws DatastoreException
  {
    //Test camera calibrations (set to run ASAP)
    HardwareTaskEngine.getInstance().setCameraCalibrationsToRunASAP();

    //Test all calibrations (set to run ASAP)
    HardwareTaskEngine.getInstance().setAllCalibrationsToRunASAP();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void convertLegacyOpticalRegion()
  {
    // Check if we have previous version of SurfaceMap
    int version = FileName.getProjectBoardSurfaceMapSettingsLatestFileVersion();
    while (version > 0)
    {
      String fileName;
      if (getPanel().getPanelSettings().isPanelBasedAlignment())
        fileName = FileName.getProjectPanelSurfaceMapSettingsFullPath(this.getName(), version);
      else
        fileName = FileName.getProjectBoardSurfaceMapSettingsFullPath(this.getName(), 1, version);

      if (FileUtilAxi.exists(fileName))
        break;
      --version;
    }
    
    // Fix by Tan, Kok Chun
    int opticalRegionSize = 0;
    if (getPanel().hasPanelSurfaceMapSettings())
    {
      if (getPanel().getPanelSurfaceMapSettings().hasOpticalRegion())
      {
        opticalRegionSize = getPanel().getPanelSurfaceMapSettings().getOpticalRegions().size();
      }
    }
    
    if (opticalRegionSize > 1)
    {
      if (getPanel().getPanelSettings().isPanelBasedAlignment())
      {
        if (getPanel().hasPanelSurfaceMapSettings())
        {
          OpticalRegion firstOpticalRegion = null;
          List<OpticalCameraRectangle> firstOpticalCameraRectangles = new ArrayList<OpticalCameraRectangle>();
                  
          for(OpticalRegion opticalRegion : getPanel().getPanelSurfaceMapSettings().getOpticalRegions())
          {
            if (firstOpticalRegion == null)
            {
              firstOpticalRegion = opticalRegion;
              firstOpticalCameraRectangles = firstOpticalRegion.getAllOpticalCameraRectangles();
              continue;
            }                       
            
            // Add OpticalCameraRectangle into first OpticalRegion
            for(OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getAllOpticalCameraRectangles())
            {
              if (firstOpticalCameraRectangles.contains(opticalCameraRectangle) == false)
                firstOpticalRegion.addOpticalCameraRectangle(opticalCameraRectangle);

              for (PanelCoordinate panelCoordinate : opticalCameraRectangle.getPanelCoordinateList())
              {
                firstOpticalRegion.addSurfaceMapPoints(panelCoordinate.getX(), panelCoordinate.getY(), true);
              }
              
              for(Component component: opticalRegion.getComponents())
              {
                firstOpticalRegion.addComponent(component);
              }
            }
            
            // Remove from SurfaceMapSettings object
            getPanel().getPanelSurfaceMapSettings().removeSurfaceMapRegion(opticalRegion);
          }
          
          // Finally, make the firstOpticalRegion as the target object
          if (firstOpticalRegion != null)
            getPanel().getPanelSurfaceMapSettings().setOpticalRegion(firstOpticalRegion);
        }
      }
      else
      {
        for(Board board : getPanel().getBoards())
        {
          if (board.hasBoardSurfaceMapSettings())
          {
            OpticalRegion firstOpticalRegion = null;
            List<OpticalCameraRectangle> firstOpticalCameraRectangles = new ArrayList<OpticalCameraRectangle>();
            
            for(OpticalRegion opticalRegion : board.getBoardSurfaceMapSettings().getOpticalRegions())
            {
              if (firstOpticalRegion == null)
              {
                firstOpticalRegion = opticalRegion;
                firstOpticalCameraRectangles = firstOpticalRegion.getOpticalCameraRectangles(board);
                continue;
              }
              
              // Add OpticalCameraRectangle into first OpticalRegion
              for(OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getOpticalCameraRectangles(board))
              {
                if (firstOpticalCameraRectangles.contains(opticalCameraRectangle) == false)
                  firstOpticalRegion.addBoardOpticalCameraRectangle(board, opticalCameraRectangle);

                for (PanelCoordinate panelCoordinate : opticalCameraRectangle.getPanelCoordinateList())
                {
                  firstOpticalRegion.addSurfaceMapPoints(board, panelCoordinate.getX(), panelCoordinate.getY());
                }

                for(Component component: opticalRegion.getComponents(board))
                {
                  firstOpticalRegion.addComponent(board, component);
                }
              }
              
              // Remove from SurfaceMapSettings object
              board.getBoardSurfaceMapSettings().removeSurfaceMapRegion(opticalRegion);
            }
            
            // Finally, make the firstOpticalRegion as the target object
            if (firstOpticalRegion != null)
              board.getBoardSurfaceMapSettings().setOpticalRegion(firstOpticalRegion);
          }
        }
      }
    }
  }
  
  /**
   * @author Siew Yeng
   */
  public void setEnlargeReconstructionRegion(boolean isEnlargeReconstructionRegion)
  {
    // Set to use multi angle image generation for recipes(ProjectSettingsReader version 8 and above)
    if (isEnlargeReconstructionRegion == _isEnlargeReconstructionRegion)
    {
      return;
    }

    boolean oldValue = _isEnlargeReconstructionRegion;
    _projectObservable.setEnabled(false);
    try
    {
      _isEnlargeReconstructionRegion = isEnlargeReconstructionRegion;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, isEnlargeReconstructionRegion, ProjectEventEnum.ENLARGE_RECONSTRUCTION_REGION);
    _fileWriter.appendIsEnlargeReconstructionRegion(oldValue, isEnlargeReconstructionRegion);
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean isEnlargeReconstructionRegion()
  {
    return _isEnlargeReconstructionRegion;
  }
  
  /**
   * Set Processor Strip Overlap size for this project (XCR-3599)
   * @author Siew Yeng
   */
  public void setOverlapNeededForLargestPossibleJointInNanometers(int overlapInNanometers)
  {
    _overlapNeededForLargestPossibleJointInNanometers = overlapInNanometers;
  }
  
  /**
   * @author Siew Yeng
   */
  public int getOverlapNeededForLargestPossibleJointInNanometers()
  {
    return _overlapNeededForLargestPossibleJointInNanometers;
  }
  
  /**
   * Set Max Inspection Region Width for this project (XCR-3599)
   * @author Siew Yeng
   */
  public void setMaxInspectionRegionWidthInNanometers(int maxInspectionRegionWidthInNanometers)
  {
    _currentMaxInspectionRegionWidthInNanometers = maxInspectionRegionWidthInNanometers;
  }
  
  /**
   * Set Max Inspection Region Length for this project (XCR-3599)
   * @author Siew Yeng
   */
  public void setMaxInspectionRegionLengthInNanometers(int maxInspectionRegionLengthInNanometers)
  { 
    _currentMaxInspectionRegionLengthInNanometers = maxInspectionRegionLengthInNanometers;
  }
  
  /**
   * Set Max High Mag Inspection Region Width for this project (XCR-3599)
   * @author Siew Yeng
   */
  public void setMaxHighMagInspectionRegionWidthInNanometers(int maxHighMagInspectionRegionWidthInNanometers)
  {
    _currentMaxHighMagInspectionRegionWidthInNanometers = maxHighMagInspectionRegionWidthInNanometers;
  }
  
  /**
   * Set Max High Mag Inspection Region Length for this project (XCR-3599)
   * @author Siew Yeng
   */
  public void setMaxHighMagInspectionRegionLengthInNanometers(int maxHighMagInspectionRegionLengthInNanometers)
  {
    _currentMaxHighMagInspectionRegionLengthInNanometers = maxHighMagInspectionRegionLengthInNanometers;
  }
  
  /**
   * @author Siew Yeng
   */
  public int getMaxInspectionRegionWidthInNanometers()
  {
    return _currentMaxInspectionRegionWidthInNanometers;
  }
  
  /**
   * @author Siew Yeng
   */
  public int getMaxInspectionRegionLengthInNanometers()
  {
    return _currentMaxInspectionRegionLengthInNanometers;
  }
  
  /**
   * @author Siew Yeng
   */
  public int getMaxHighMagInspectionRegionWidthInNanometers()
  {
    return _currentMaxHighMagInspectionRegionWidthInNanometers;
  }
  
  /**
   * @author Siew Yeng
   */
  public int getMaxHighMagInspectionRegionLengthInNanometers()
  {
    return _currentMaxHighMagInspectionRegionLengthInNanometers;
  }
  
  /**
   * Reset Max Inspection Region size that store in project(XCR-3599)
   * Must call ReconstructionRegion.resetMaxInspectionRegionWidthLength() to reset actual ReconstructionRegion max size
   * @author Siew Yeng
   */
  public void resetMaxInspectionRegionWidthLength()
  {
    _currentMaxInspectionRegionWidthInNanometers = -1;
    _currentMaxInspectionRegionLengthInNanometers = -1;
    _currentMaxHighMagInspectionRegionWidthInNanometers = -1;
    _currentMaxHighMagInspectionRegionLengthInNanometers = -1;
  }
  
  /**
   * Reset Processor Strip overlap size that store in project(XCR-3599)
   * Must call ProcessorStrip.resetOverlapNeededForLargestPossibleJoint() to reset actual ProcessorStrip overlap size
   * @author Siew Yeng
   */
  public void resetOverlapNeededForLargestPossibleJoint()
  {
    _overlapNeededForLargestPossibleJointInNanometers = -1;
  }
  
  /**
   * @author Swee Yee
   */
  public void setEnableBGAJointBasedThresholdInspection(boolean isBGAJointBasedThresholdInspectionEnabled)
  {
    if (isBGAJointBasedThresholdInspectionEnabled == _isBGAJointBasedThresholdInspectionEnabled)
      return;

    boolean oldValue = _isBGAJointBasedThresholdInspectionEnabled;
    _projectObservable.setEnabled(false);
    try
    {
      _isBGAJointBasedThresholdInspectionEnabled = isBGAJointBasedThresholdInspectionEnabled;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, isBGAJointBasedThresholdInspectionEnabled, ProjectEventEnum.ENABLE_BGA_JOINT_BASED_THRESHOLD_INSPECTION);
    _fileWriter.appendIsBGAJointBasedThresholdInspectionEnabled(oldValue, isBGAJointBasedThresholdInspectionEnabled);
  }
  
  public boolean isBGAJointBasedThresholdInspectionEnabled()
  {
    return _isBGAJointBasedThresholdInspectionEnabled;
  }
  
  /*
  * swee-yee.wong
  */
  public void setBGAInspectionRegionTo1X1(boolean isBGAInspectionRegionSetTo1X1)
  {
    if (isBGAInspectionRegionSetTo1X1 == _isBGAInspectionRegionSetTo1X1)
    {
      return;
    }

    boolean oldValue = _isBGAInspectionRegionSetTo1X1;
    _projectObservable.setEnabled(false);
    try
    {
      _isBGAInspectionRegionSetTo1X1 = isBGAInspectionRegionSetTo1X1;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, isBGAInspectionRegionSetTo1X1, ProjectEventEnum.SET_BGA_INSPECTION_REGION_TO_1X1);
    _fileWriter.appendIsBGAInspectionRegionSetTo1X1(oldValue, isBGAInspectionRegionSetTo1X1);
  }
  
  /*
  * swee-yee.wong
  */
  public boolean isBGAInspectionRegionSetTo1X1()
  {
    return _isBGAInspectionRegionSetTo1X1;
  }
  
  /*
  * @author Kok Chun, Tan
  */
  public void setProjectBasedBypassMode(boolean isProjectBasedBypassMode)
  {
    if (isProjectBasedBypassMode == _isProjectBasedBypassMode)
    {
      return;
    }

    boolean oldValue = _isProjectBasedBypassMode;
    _projectObservable.setEnabled(false);
    try
    {
      _isProjectBasedBypassMode = isProjectBasedBypassMode;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, isProjectBasedBypassMode, ProjectEventEnum.PROJECT_BASED_BYPASS_MODE);
    _fileWriter.appendIsBypassMode(oldValue, isProjectBasedBypassMode);
  }
  
  /*
  * @author Kok Chun, Tan
  */
  public boolean isProjectBasedBypassMode()
  {
    return _isProjectBasedBypassMode;
  }

  /**
   * @author Khaw Chek Hau
   * XCR2603: Corrupted recipe unable to load
   */
  public void setSummaryUpdateNeeded(boolean isSummaryUpdateNeeded)
  {
    _isSummaryUpdateNeeded = isSummaryUpdateNeeded; 
  }
  
  /**
   * @author Kok Chun, Tan
   * set current variation name
   */
  public void setSelectedVariationName(String name)
  {
    Assert.expect(name != null);
    _currentSelectedVariationName = name;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public String getSelectedVariationName()
  {
    Assert.expect(_currentSelectedVariationName != null);
    return _currentSelectedVariationName;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void resetSelectedVariationName()
  {
    _currentSelectedVariationName = VariationSettingManager.ALL_LOADED;
  }
  
  /**
   * @author Kok Chun, Tan
   * set current enabled variation name in panel setup
   */
  public void setEnabledVariationName(String name)
  {
    Assert.expect(name != null);
    _currentEnabledVariationName = name;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public String getEnabledVariationName()
  {
    Assert.expect(_currentEnabledVariationName != null);
    return _currentEnabledVariationName;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void resetEnabledVariationName()
  {
    _currentEnabledVariationName = VariationSettingManager.ALL_LOADED;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setEnablePreSelectVariation(boolean enable) throws DatastoreException
  {
    if (enable == _enablePreSelectVariation)
      return;
    
    boolean oldValue = _enablePreSelectVariation;
    _projectObservable.setEnabled(false);
    try
    {
      _enablePreSelectVariation = enable;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, enable, VariationSettingEnum.PRESELECT);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean enablePreSelectVariation()
  {
    return _enablePreSelectVariation;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setIsUpdateComponentNoLoadSetting(boolean enable)
  {
    _isUpdateComponentNoLoadSetting = enable;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean isUpdateComponentNoLoadSetting()
  {
    return _isUpdateComponentNoLoadSetting;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static void setProjectNameToSystemType(String projectName, String systemType)
  {
    Assert.expect(projectName != null);
    Assert.expect(systemType != null);
    
    if (_projectNameToSystemTypeMap == null)
    {
      _projectNameToSystemTypeMap = new HashMap<String, String>();
    }
    
    // if already have this project name, remove it from the map first.
    if (_projectNameToSystemTypeMap.containsKey(projectName))
    {
      _projectNameToSystemTypeMap.remove(projectName);
    }
    _projectNameToSystemTypeMap.put(projectName, systemType);
  }
  
  /**
   * @author Swee Yee Wong - new thickness table implementation
   */
  public void setThicknessTableVersion(int thicknessTableVersion) throws DatastoreException
  {
    if (thicknessTableVersion == _thicknessTableVersion)
    {
      return;
    }

    int oldValue = _thicknessTableVersion;
    _projectObservable.setEnabled(false);
    _thicknessTableVersion = thicknessTableVersion;

    _projectObservable.setEnabled(true);
      
    _projectObservable.stateChanged(this, oldValue, thicknessTableVersion, ProjectEventEnum.SET_THICKNESS_TABLE_VERSION);
    _fileWriter.appendSetThicknessTableVersion(oldValue, thicknessTableVersion);
  }
  
  /**
   * @author Swee Yee Wong - new thickness table implementation
   */
  public int getThicknessTableVersion()
  {
    return _thicknessTableVersion;
  }
  
  /**
   * @author Swee Yee Wong - new thickness table implementation
   */
  public boolean notifyUserOfDifferSolderThicknessVersion(Project project) throws DatastoreException
  {
    //Swee Yee
    //Check whether the current solder thickness version is the latest version
    //if not, notify user there is a new solder thickness version which has better accuracy
    int projectSolderThicknessVersion = project.getThicknessTableVersion();
    int latestSolderThicknessVersion = Config.getInstance().getIntValue(SoftwareConfigEnum.THICKNESS_TABLE_LATEST_VERSION);
    if (projectSolderThicknessVersion < latestSolderThicknessVersion)
    {
      _waitForUserResponseLock.setValue(false);
      String projectSolderThicknessVersionName = StringLocalizer.keyToString("MMGUI_SOLDER_THICKNESS_VERSION_REFERENCE_KEY") + String.valueOf(projectSolderThicknessVersion);
      String latestSolderThicknessVersionName = StringLocalizer.keyToString("MMGUI_SOLDER_THICKNESS_VERSION_REFERENCE_KEY") + String.valueOf(latestSolderThicknessVersion);
      _projectObservable.stateChanged(project, projectSolderThicknessVersionName, latestSolderThicknessVersionName,
        ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_THICKNESS_TABLE_VERSION);
      waitForUserResponse();

      if (_loadWithNewSolderThicknessVersion == true)
      {
        project.setThicknessTableVersion(latestSolderThicknessVersion);
        //swee yee wong - XCR-3004 Load recipe assert java.lang.NullPointerException
        //swee yee wong - XCR-3046 Alignment is cleared if attempt to unload and reload recipe after run production
        boolean notifyUserAlternativeVersion = false;
        project.isProjectSolderThicknessFilesAvailable(notifyUserAlternativeVersion);
      }
    }
    return _loadWithNewSolderThicknessVersion;
  }
  
  /**
   * @author Swee Yee Wong - new thickness table implementation
   */
  public boolean notifyUserOfAlternativeSolderThicknessVersion(Project project) throws DatastoreException
  {
    //Swee Yee
    //Check whether the current solder thickness version is the latest version
    //if not, notify user there is a new solder thickness version which has better accuracy
    int projectSolderThicknessVersion = project.getThicknessTableVersion();
    
    //Swee Yee Wong - XCR-3300 Thickness profile is capped at 100 in PTH subtype
    //New thickness table is not ready, will get flat response when reaching high deltagray value
    //Will change the default value back to 0 until the new thickness table is ready and stable.
    //int latestSolderThicknessVersionFound = FileName.getLatestThicknessTableVersionFound();
    int latestSolderThicknessVersionFound = 0;
    
    // return false if no thickness table can be found
    if(latestSolderThicknessVersionFound < 0)
      return false;

    getProjectState().disable();
    _waitForUserResponseLock.setValue(false);
    String projectSolderThicknessVersionName = StringLocalizer.keyToString("MMGUI_SOLDER_THICKNESS_VERSION_REFERENCE_KEY") + String.valueOf(projectSolderThicknessVersion);
    String latestSolderThicknessVersionName = StringLocalizer.keyToString("MMGUI_SOLDER_THICKNESS_VERSION_REFERENCE_KEY") + String.valueOf(latestSolderThicknessVersionFound);
    _projectObservable.stateChanged(project, projectSolderThicknessVersionName, latestSolderThicknessVersionName,
      ProjectEventEnum.LOADING_PROJECT_WITH_ALTERNATIVE_THICKNESS_TABLE_VERSION);
    waitForUserResponse();

    if (_loadWithNewSolderThicknessVersion == true)
    {
      project.setThicknessTableVersion(latestSolderThicknessVersionFound);
      //swee yee wong - XCR-3004 Load recipe assert java.lang.NullPointerException
      //swee yee wong - XCR-3046 Alignment is cleared if attempt to unload and reload recipe after run production
      boolean notifyUserAlternativeVersion = false;
      project.isProjectSolderThicknessFilesAvailable(notifyUserAlternativeVersion);
    }
    project.getProjectState().enableAfterProjectLoad();

    return _loadWithNewSolderThicknessVersion;
  }
  
  /**
   * @author Swee Yee Wong - new thickness table implementation
   * XCR-3237 Intermittent software crash when generate precision tuning image
   */
  public synchronized boolean isProjectSolderThicknessFilesAvailable(boolean notifyUserAlternativeVersion) throws DatastoreException
  {
    int maximumNumberOfRetry = 5;
    try
    {
      List<String> solderThicknessLayer1DirFullList = Directory.getSolderThicknessLayer1DirFullList();
      List<String> solderThicknessLayer2DirFullList = Directory.getSolderThicknessLayer2DirFullList();
      
      boolean fileHierarchyBreak = false;
      
      for (SolderThicknessEnum solderThicknessEnum : SolderThicknessEnum.getFullEnumList())
      {
        for (String layer1Dir : solderThicknessLayer1DirFullList)
        {
          for (String layer2Dir : solderThicknessLayer2DirFullList)
          {
            String solderThicknessFilePathInProject = FileName.getProjectThicknessTableFullPath(getName(), layer1Dir, layer2Dir, solderThicknessEnum.getFileName(), _thicknessTableVersion);
            if (FileUtil.exists(solderThicknessFilePathInProject) == false)
            {
              fileHierarchyBreak = true;
              break;
            }
          }
          if(fileHierarchyBreak)
            break;
        }
        if(fileHierarchyBreak)
            break;
      }
      
      if (fileHierarchyBreak)
      {
        //swee-yee.wong - XCR-3237 Intermittent software crash when generate precision tuning image
        try
        {
          if (FileUtil.exists(Directory.getProjectSolderThicknessDir(getName())))
          {
            FileUtil.delete(Directory.getProjectSolderThicknessDir(getName()));
          }
        }
        catch (CouldNotDeleteFileException fe)
        {
          //do nothing, something is holding the files/folders, just keep it there if cannot delete
        }
        for (SolderThicknessEnum solderThicknessEnum : SolderThicknessEnum.getFullEnumList())
        {
          for (String layer1Dir : solderThicknessLayer1DirFullList)
          {
            for (String layer2Dir : solderThicknessLayer2DirFullList)
            {
              String solderThicknessFilePathInProject = FileName.getProjectThicknessTableFullPath(getName(), layer1Dir, layer2Dir, solderThicknessEnum.getFileName(), _thicknessTableVersion);
              String solderThicknessFilePathInConfig = FileName.getCalibThicknessTableFullPath(layer1Dir, layer2Dir, solderThicknessEnum.getFileName(), _thicknessTableVersion);

              //swee-yee.wong - XCR-3237 Intermittent software crash when generate precision tuning image
              if (FileUtil.exists(solderThicknessFilePathInProject) == false || SolderThickness.isSolderThicknessFileWorkable(solderThicknessFilePathInProject)==false)
              {
                //Assert.expect(FileUtil.exists(solderThicknessFilePathInConfig));
                if (FileUtil.exists(Directory.getProjectSolderThicknessDir(getName()) + File.separator + layer1Dir + File.separator + layer2Dir) == false)
                {
                  FileUtil.createDirectory(Directory.getProjectSolderThicknessDir(getName()) + File.separator + layer1Dir + File.separator + layer2Dir);
                }
                if(FileUtil.exists(solderThicknessFilePathInConfig))
                {
                  //swee-yee.wong - XCR-3237 Intermittent software crash when generate precision tuning image
                  if(SolderThickness.isSolderThicknessFileWorkable(solderThicknessFilePathInConfig)==false)
                  {
                    FileCorruptDatastoreException dex = new FileCorruptDatastoreException(solderThicknessFilePathInConfig);
                    throw dex;
                  }
                  copySolderThicknessFileWithRetry(solderThicknessFilePathInConfig, solderThicknessFilePathInProject, maximumNumberOfRetry);
                }
                else if(_thicknessTableVersion == 0)
                {
                  //swee-yee.wong - XCR-3237 Intermittent software crash when generate precision tuning image
                  if(SolderThickness.isSolderThicknessFileWorkable(FileName.getSolderThicknessTable1FullPath())==false)
                  {
                    FileCorruptDatastoreException dex = new FileCorruptDatastoreException(FileName.getSolderThicknessTable1FullPath());
                    throw dex;
                  }
                  if (FileUtil.exists(Directory.getConfigSolderThicknessDir() + File.separator + layer1Dir + File.separator + layer2Dir) == false)
                  {
                    FileUtil.createDirectory(Directory.getConfigSolderThicknessDir() + File.separator + layer1Dir + File.separator + layer2Dir);
                  }
                  //swee-yee.wong - XCR-3237 Intermittent software crash when generate precision tuning image
                  copySolderThicknessFileWithRetry(FileName.getSolderThicknessTable1FullPath(), solderThicknessFilePathInConfig, maximumNumberOfRetry);
                  copySolderThicknessFileWithRetry(FileName.getSolderThicknessTable1FullPath(), solderThicknessFilePathInProject, maximumNumberOfRetry);
                }
                else
                {
                  //file not found
                  if(notifyUserAlternativeVersion)
                  {
                    if (this.notifyUserOfAlternativeSolderThicknessVersion(this) == false)
                    {
                      FileDoesNotExistException fe = new FileDoesNotExistException(solderThicknessFilePathInConfig);
                      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fe.getFileName());
                      dex.initCause(fe);
                      throw dex;
                    }
                  }
                  else
                  {
                    FileDoesNotExistException fe = new FileDoesNotExistException(solderThicknessFilePathInConfig);
                    FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fe.getFileName());
                    dex.initCause(fe);
                    throw dex;
                  }
                  return false;
                }
              }
            }
          }
        }
      }
      return true;
    }
    catch (FileDoesNotExistException fe)
    {
      System.out.println("File does not exist during copying thickness table from calib to recipe.");
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fe.getFileName());
      dex.initCause(fe);
      throw dex;
    }
    catch (CouldNotCreateFileException ex)
    {
      System.out.println("Could not create solder thickness sub directory in recipe directory");
      CannotCreateFileDatastoreException ex2 = new CannotCreateFileDatastoreException(ex.getFileName());
      ex2.initCause(ex);
      throw ex2;
    }
    catch (CouldNotCopyFileException ce)
    {
      System.out.println("Could not copy file during copying thickness table from calib to recipe.");
      CannotCopyFileDatastoreException dex = new CannotCopyFileDatastoreException(ce.getFromFile(), ce.getToFile());
      dex.initCause(ce);
      throw dex;
    }
  }
  
  /**
   * XCR-3237 Intermittent software crash when generate precision tuning image
   * copy solder thickness file with few more attempts, if the file still not workable, prompt exceptions
   * @author Swee Yee Wong
   */
  private void copySolderThicknessFileWithRetry(String sourcePath, String destPath, int maximumNumberOfRetry) throws DatastoreException, FileDoesNotExistException, CouldNotCopyFileException
  {
    int numberOfRetry = 0;
    do
    {
      FileUtil.copy(sourcePath, destPath);
      sleep(20);
      numberOfRetry++;
      if (numberOfRetry >= maximumNumberOfRetry && SolderThickness.isSolderThicknessFileWorkable(destPath) == false)
      {
        FileCorruptDatastoreException dex = new FileCorruptDatastoreException(destPath);
        throw dex;
      }
    }
    while (SolderThickness.isSolderThicknessFileWorkable(destPath)==false && numberOfRetry < maximumNumberOfRetry);
  }
  /**
   * at various point(s), the code needs to sleep for a bit.  wrap up exception handling here
   *
   * @author Swee Yee Wong
   */
  static private void sleep(long millis)
  {
    try
    {
      Thread.sleep(millis);
    }
    catch (InterruptedException ix)
    {
      // don't expect this, but don't really care how sleep returns either
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR2943: Alignment Fail Due to Too Close To Edge
   */
  public void setEnlargeAlignmentRegion(boolean isEnlargeAlignmentRegion)
  {
    if (isEnlargeAlignmentRegion == _isEnlargeAlignmentRegion)
    {
      return;
    }

    boolean oldValue = _isEnlargeAlignmentRegion;
    _projectObservable.setEnabled(false);
    try
    {
      _isEnlargeAlignmentRegion = isEnlargeAlignmentRegion;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, isEnlargeAlignmentRegion, ProjectEventEnum.ENLARGE_ALIGNMENT_REGION);
    _fileWriter.appendIsEnlargeAlignmentRegion(oldValue, isEnlargeAlignmentRegion);
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR2943: Alignment Fail Due to Too Close To Edge
   */
  public boolean isEnlargeAlignmentRegion()
  {
    return _isEnlargeAlignmentRegion;
  }
  
    
  /**
   * @author Kee Chin Seong
   * @param isSurfaceMapPointPopulated 
   */
  public void setIsSurfaceMapPointPopulated(boolean isSurfaceMapPointPopulated)
  {
     _isSurfaceMapPointPopulated = isSurfaceMapPointPopulated;
  }
  
  /**
   * @author Kee Chin Seong
   * @return 
   */
  public boolean isSurfaceMapPointPopulated()
  {
    return _isSurfaceMapPointPopulated;
  }
  
  /**
   * @author Kee Chin Seong
   * @Setup the surface map if neccesary
   */
  public void setupSurfaceMapIfNeeded()
  {
    if(isSurfaceMapPointPopulated() == false)
    {
        if(getPanel().getPanelSettings().isPanelBasedAlignment())
        {
            OpticalProgramGeneration.getInstance().setupPanelSurfaceMap(this, getTestProgram());
        }
        else
        {
            OpticalProgramGeneration.getInstance().setupBoardSurfaceMap(this, getTestProgram()); 
        }
    }
  }
  
  /**
   * Set ReconstructionRegion Size and Processor Strip Overlap size if the project is with max region size enlarged.(XCR-3599)
   * @author Siew Yeng 
   */
  public static void setupForEnlargeReconstructionRegionIfNecessary(Project project)
  {
    if(project.isEnlargeReconstructionRegion())
    {
      //Processor Strip
      if(project.getOverlapNeededForLargestPossibleJointInNanometers() != -1)
      {
        ProcessorStrip.setOverlapNeededForLargestPossibleJointInNanometers(project.getOverlapNeededForLargestPossibleJointInNanometers());
      }
      
      //InspectionRegion Width
      if(project.getMaxInspectionRegionWidthInNanometers() != -1)
      {
        ReconstructionRegion.setCurrentMaxInspectionRegionWidthInNanoMeters(project.getMaxInspectionRegionWidthInNanometers());
      }
      
      //InspectionRegion Length
      if(project.getMaxInspectionRegionLengthInNanometers() != -1)
      {
        ReconstructionRegion.setCurrentMaxInspectionRegionLengthInNanoMeters(project.getMaxInspectionRegionLengthInNanometers());
      }
    }
  }
  
   /**
    * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   */
  public void setSaveAsSystemType(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);
    _saveAsSystemType = systemType;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   */
  public void setSaveAsLowMagnification(String lowMag)
  {
     Assert.expect(lowMag != null);
     _saveAsLowMagnification = lowMag;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   */
  public void setPreviousSystemType(SystemTypeEnum previousSystemType)
  {
    _previousSystemType = previousSystemType;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   */
  public void clearAlignmentTransformIfnecessary()
  {
    Assert.expect(_saveAsSystemType != null);
    
    if (getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      getPanel().getPanelSettings().clearAllAlignmentTransforms();
    }
    else
    {
      for (Board board : getPanel().getBoards())
      {
        board.getBoardSettings().clearAllAlignmentTransforms();
      }
    }
  }
  
  public static void setupProjectReaderWriter()
  {
    _projectObservable = ProjectObservable.getInstance();
    _projectReader = ProjectReader.getInstance();
    _projectWriter = ProjectWriter.getInstance();
    _mainReader = new MainReader();
    _programGeneration = ProgramGeneration.getInstance();
    _imageManager = ImageManager.getInstance();
    _fileWriter = ProjectHistoryLog.getInstance();
    _projectErrorLogUtil = ProjectErrorLogUtil.getInstance();
  }
  
   /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   *
   * @author weng-jian.eoh
   * @return
   */
  public String getInitialRecipeSettingName()
  {
    Assert.expect(_initialRecipeSettingName != null);

    return _initialRecipeSettingName;
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public String getInitialRecipeSettingSetFullPath()
  {
    Assert.expect(_initialRecipeSettingName != null);
    if (_initialRecipeSettingName.equalsIgnoreCase(FileName.getInitialRecipeDefaultSettingFileWithoutExtension()))
    {
      return _initialRecipeSettingName;
    }
    else
    {
      return FileName.getInitialRecipeSettingFullPath(_initialRecipeSettingName);
    }
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public void setInitialRecipeSettingSetName(String initialRecipeSettingSetName) throws DatastoreException
  {
    Assert.expect(initialRecipeSettingSetName != null);

    if (initialRecipeSettingSetName.equals(_initialRecipeSettingName))
    {
      return;
    }

    String oldValue = _initialRecipeSettingName;
    _projectObservable.setEnabled(false);
    try
    {
      _initialRecipeSettingName = initialRecipeSettingSetName.intern();
      ProjectInitialRecipeSettings.getInstance().setInitialRecipeSettingFileFullPath(getInitialRecipeSettingSetFullPath());
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _initialRecipeSettingName, ProjectEventEnum.INITIAL_RECIPE_SETTING);
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public void saveInitialRecipeSettingSetName(String initialRecipeSettingSetname)
  {
    Assert.expect(initialRecipeSettingSetname != null);

    if (initialRecipeSettingSetname.equals(_initialRecipeSettingName))
    {
      return;
    }

    _initialRecipeSettingName = initialRecipeSettingSetname.intern();
  }

  /**
   * @author Kok Chun, Tan - DRO version
   * Version 0 - Does not include Camera 0
   * Version 1 - Include Camera 0
   */
  public void setDynamicRangeOptimizationVersion(int dynamicRangeOptimizationVersion)
  {
    if (dynamicRangeOptimizationVersion == _dynamicRangeOptimizationVersion.getVersion())
      return;

    int oldValue = _dynamicRangeOptimizationVersion.getVersion();
    _projectObservable.setEnabled(false);
    try
    {
      _dynamicRangeOptimizationVersion = DynamicRangeOptimizationVersionEnum.getDynamicRangeOptimizationVersionEnumFromVersionNumber(dynamicRangeOptimizationVersion);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    
    _projectObservable.stateChanged(this, oldValue, dynamicRangeOptimizationVersion, ProjectEventEnum.SET_DYNAMIC_RANGE_OPTIMIZATION_VERSION);
    _fileWriter.appendSetDynamicRangeOptimizationVersion(oldValue, dynamicRangeOptimizationVersion);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public DynamicRangeOptimizationVersionEnum getDynamicRangeOptimizationVersion()
  {
    return _dynamicRangeOptimizationVersion;
  }
}