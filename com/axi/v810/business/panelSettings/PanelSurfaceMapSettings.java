package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.hardware.*;

/**
 * A PanelSurfaceMapSettings contains all the surface map points list contained
 * within a panel.
 * 
 * @author Cheah Lee Herng
 */
public class PanelSurfaceMapSettings implements Serializable  
{
  private Panel _panel;

  private String _userComment;

  // Map of surface maps that are parked under this surface map region
  private Map<String, OpticalRegion> _regionPositionNameToOpticalRegionMap = new LinkedHashMap<>();
  private Map<String, Boolean> _panelPositionNameToPanelPositionVerifiedMap = new LinkedHashMap<>();
  private Map<String, OpticalCameraIdEnum> _panelPositionNameToCameraMap = new LinkedHashMap<>();

  private static transient ProjectObservable _projectObservable;

  private OpticalRegion _panelOpticalRegion; 
  private Map<Integer, OpticalRegion> _tableIndexToOpticalRegionMap = new LinkedHashMap<>();

  /**
   * @author Cheah Lee Herng
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Cheah Lee Herng
   */
  public PanelSurfaceMapSettings()
  {
    _projectObservable.stateChanged(this, null, this, PanelSurfaceMapEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Cheah Lee Herng
   */
  public PanelSurfaceMapSettings(Panel panel)
  {
    Assert.expect(panel != null);
    _panel = panel;
    _projectObservable.stateChanged(this, null, this, PanelSurfaceMapEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Jack Hwee
   */
  public void setOpticalRegion(OpticalRegion opticalRegion)
  {
    Assert.expect(opticalRegion != null);       
    _panelOpticalRegion = opticalRegion;
  } 

  /**
   * @author Jack Hwee
   */
  public OpticalRegion getOpticalRegion()
  {
    Assert.expect(_panelOpticalRegion != null);  
    return _panelOpticalRegion;  
  }

  /**
   * @author Ying-Huan.Chu
   */
  public boolean hasOpticalRegion()
  {
    if (_panelOpticalRegion != null)
      return true;
    else
      return false;
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {        
    _projectObservable.stateChanged(this, this, null, PanelSurfaceMapEventEnum.CREATE_OR_DESTROY);
  }    

  /**
   * A " character is not allowed in the comment field
   * @author Cheah Lee Herng
   */
  public boolean isUserCommentValid(String comment)
  {
    Assert.expect(comment != null);
    if (comment.contains("\""))
      return false;

    return true;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setUserComment(String comment)
  {
    Assert.expect(comment != null);
    Assert.expect(isUserCommentValid(comment));

    String oldValue = _userComment;
    _projectObservable.setEnabled(false);
    try
    {
      _userComment = comment.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, comment, PanelSurfaceMapEventEnum.USER_COMMENT);
  }

  /**
   * @author Cheah Lee Herng
   */
  public String getUserComment()
  {
    Assert.expect(_userComment != null);
    return _userComment;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void addOpticalRegion(OpticalRegion opticalRegion)
  {
    Assert.expect(_regionPositionNameToOpticalRegionMap != null);
    Assert.expect(opticalRegion != null);     

    _panelOpticalRegion = opticalRegion;
     String regionPositionName = opticalRegion.getRegion().getMinX() + "_" + opticalRegion.getRegion().getMinY() + "_" + opticalRegion.getRegion().getWidth() + "_" + opticalRegion.getRegion().getHeight();

    OpticalRegion oldPanelOpticalRegionSettings = null;
    _projectObservable.setEnabled(false);
    try
    {
       oldPanelOpticalRegionSettings = _regionPositionNameToOpticalRegionMap.put(regionPositionName, opticalRegion);
     // _opticalRegion.add(opticalRegion);        
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }         
    _projectObservable.stateChanged(this, oldPanelOpticalRegionSettings, opticalRegion, PanelSurfaceMapEventEnum.ADD_OPTICAL_REGION);
  }

  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  public void removeSurfaceMapRegion(OpticalRegion opticalRegion)
  {
    Assert.expect(_regionPositionNameToOpticalRegionMap != null);

    String regionPositionName = opticalRegion.getRegion().getMinX() + "_" + opticalRegion.getRegion().getMinY() + "_" + opticalRegion.getRegion().getWidth() + "_" + opticalRegion.getRegion().getHeight();

    OpticalRegion oldOpticalRegion = null;
    _projectObservable.setEnabled(false);
    try
    {
      oldOpticalRegion = _regionPositionNameToOpticalRegionMap.remove(regionPositionName);
      if (oldOpticalRegion != null) // the key existed in the map.
      {
        _panelOpticalRegion = null;
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }         
    _projectObservable.stateChanged(this, oldOpticalRegion, opticalRegion, PanelSurfaceMapEventEnum.REMOVE_SURFACE_MAP_REGION);    
  }

  /**
   * @author Cheah Lee Herng 
   */
  public Map<String, OpticalRegion> getOpticalRegionPositionNameToOpticalRegionMap()
  {
    Assert.expect(_regionPositionNameToOpticalRegionMap != null);
    return _regionPositionNameToOpticalRegionMap;
  }

  /**
   * @return a List containing OpticalRegion for this region
   * @author Jack Hwee
   */
  public List<OpticalRegion> getOpticalRegions()
  {
    Assert.expect(_regionPositionNameToOpticalRegionMap != null);

    return new ArrayList<OpticalRegion>(_regionPositionNameToOpticalRegionMap.values());
  }

/**
 * @return a List containing Name for this region
 * @author Jack Hwee
 */
  public List<String> getOpticalRegionsName()
  {
    Assert.expect(_regionPositionNameToOpticalRegionMap != null);

    return new ArrayList<String>(_regionPositionNameToOpticalRegionMap.keySet());
  }

  /**
   * @author Cheah Lee Herng
   */
  public Map<String, Boolean> getPanelPositionNameToPanelPositionVerifiedMap()
  {
    Assert.expect(_panelPositionNameToPanelPositionVerifiedMap != null);
    return _panelPositionNameToPanelPositionVerifiedMap;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void assignCamera(String panelPositionName, OpticalCameraIdEnum cameraId)
  {
    Assert.expect(panelPositionName != null);
    Assert.expect(cameraId != null);
    Assert.expect(_panelPositionNameToCameraMap != null);

    Object prev = _panelPositionNameToCameraMap.put(panelPositionName, cameraId);
    Assert.expect(prev == null);
  }

  /**
   * @author Cheah Lee Herng
   */
  public Panel getPanel()
  {
    Assert.expect(_panel != null);

    return _panel;
  }

  /**
   * @author Jack Hwee
   */
  public void addOpticalRegionWithIndex(OpticalRegion opticalRegion, int tableIndex)
  {
    Assert.expect( _tableIndexToOpticalRegionMap != null);
    Assert.expect(opticalRegion != null);

    _tableIndexToOpticalRegionMap.put(tableIndex, opticalRegion);  
  }

  /**
   * @author Jack Hwee
   */
  public Map<Integer, OpticalRegion> getOpticalRegionTableIndexToOpticalRegionMap()
  {
    Assert.expect(_tableIndexToOpticalRegionMap != null);
    return _tableIndexToOpticalRegionMap;
  }

  /**
   * @author Jack Hwee
   */
  public void clearOpticalRegionWithIndex()
  {
    _tableIndexToOpticalRegionMap.clear();
  }

  /**
   * @author Phang Siew Yeng
   */
  public void clearAllSurfaceMapRegion()
  {
    _regionPositionNameToOpticalRegionMap.clear();
  }
}
