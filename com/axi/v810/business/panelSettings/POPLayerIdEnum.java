package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;

/**
 * @author Khaw Chek Hau
 * POP Development
 */
public class POPLayerIdEnum extends com.axi.util.Enum
{
  private static Map<Integer, POPLayerIdEnum> _layerIdToEnumMap = new HashMap<Integer, POPLayerIdEnum>();
  private static List<POPLayerIdEnum> _orderedLayerId = new ArrayList<POPLayerIdEnum>();
  private int _layerId;
  private static int _index = -1;
  public static POPLayerIdEnum BASE = new POPLayerIdEnum(++_index, 0);
  public static POPLayerIdEnum ONE = new POPLayerIdEnum(++_index, 1);
  public static POPLayerIdEnum TWO = new POPLayerIdEnum(++_index, 2);
  public static POPLayerIdEnum THREE = new POPLayerIdEnum(++_index, 3);
  public static POPLayerIdEnum FOUR = new POPLayerIdEnum(++_index, 4);
  public static POPLayerIdEnum FIVE = new POPLayerIdEnum(++_index, 5);

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private POPLayerIdEnum(int id, int layerId)
  {
    super(id);
    _layerId = layerId;

    _layerIdToEnumMap.put(layerId, this);
    _orderedLayerId.add(this);
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public double toDouble()
  {
    return Double.valueOf(_layerId);
  }
  
  public int toInteger()
  {
    return Integer.valueOf(_layerId);
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public String toString()
  {
    return String.valueOf(_layerId);
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public static POPLayerIdEnum getLayerIdToEnumMapValue(int key)
  {
    Assert.expect(key >= 0);

    return _layerIdToEnumMap.get(key);
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public static List<POPLayerIdEnum> getOrderedLayerIdList()
  {
    return _orderedLayerId;
  }
}
