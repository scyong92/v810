package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.JointTypeEnum;
import com.axi.v810.datastore.config.*;

/**
 *
 * @author khang-wah.chnee
 */
public class InterferenceCompensationEnum extends com.axi.util.Enum
{
  private static Map<Integer, InterferenceCompensationEnum> _integerToEnumMap = new HashMap<Integer,InterferenceCompensationEnum>();
  private static Map<String, InterferenceCompensationEnum> _stringToEnumMap = new HashMap<String,InterferenceCompensationEnum>();
  
  private String _name;
  private int _offset=0;
  private static int _index = -1;
  public static Config _config = Config.getInstance();
  
  // Z Affect (either affecting or affected are required same offset
  // Make sure the jointType name is same with JointTypeEnum
  public static InterferenceCompensationEnum CAPACITOR_AFFECT 
          = new InterferenceCompensationEnum(++_index, "Capacitor", MathUtil.convertMilsToNanoMetersInteger(3.0));
  public static InterferenceCompensationEnum RESISTOR_AFFECT 
          = new InterferenceCompensationEnum(++_index, "Resistor", MathUtil.convertMilsToNanoMetersInteger(3.0));
  public static InterferenceCompensationEnum POLARIZED_CAP_AFFECT 
          = new InterferenceCompensationEnum(++_index, "Polarized Capacitor", MathUtil.convertMilsToNanoMetersInteger(5.0));
  public static InterferenceCompensationEnum GULLWING_AFFECT 
          = new InterferenceCompensationEnum(++_index, "Gullwing", MathUtil.convertMilsToNanoMetersInteger(1.0));
  public static InterferenceCompensationEnum JLEAD_AFFECT 
          = new InterferenceCompensationEnum(++_index, "JLead", MathUtil.convertMilsToNanoMetersInteger(2.0));
  public static InterferenceCompensationEnum SURFACE_MOUNT_CONNECTOR_AFFECT 
          = new InterferenceCompensationEnum(++_index, "SMT Connector", MathUtil.convertMilsToNanoMetersInteger(1.0));
  public static InterferenceCompensationEnum NON_COLLAPSABLE_BGA_AFFECT 
          = new InterferenceCompensationEnum(++_index, "Non Collapsible BGA", MathUtil.convertMilsToNanoMetersInteger(10.0));
  public static InterferenceCompensationEnum COLLAPSABLE_BGA_AFFECT 
          = new InterferenceCompensationEnum(++_index, "Collapsible BGA", MathUtil.convertMilsToNanoMetersInteger(10.0));
  public static InterferenceCompensationEnum CGA_AFFECT 
          = new InterferenceCompensationEnum(++_index, "CGA", MathUtil.convertMilsToNanoMetersInteger(10.0));
  // PRESSFIT - no offset as it's at the middle of the board = (top+bottom)/2
  // THROUGH_HOLE - no offset as it's at the middle of the board = (top+bottom)/2
  public static InterferenceCompensationEnum SINGLE_PAD_AFFECT 
          = new InterferenceCompensationEnum(++_index, "Single Pad", MathUtil.convertMilsToNanoMetersInteger(0.0));
  public static InterferenceCompensationEnum RF_SHIELD_AFFECT 
          = new InterferenceCompensationEnum(++_index, "RF Shield", MathUtil.convertMilsToNanoMetersInteger(0.0)); //Not final
  public static InterferenceCompensationEnum LEADLESS_AFFECT 
          = new InterferenceCompensationEnum(++_index, "LeadLess", MathUtil.convertMilsToNanoMetersInteger(10.0));
  public static InterferenceCompensationEnum SMALL_OUTLINE_LEADLESS_AFFECT 
          = new InterferenceCompensationEnum(++_index, "SOT", MathUtil.convertMilsToNanoMetersInteger(0.0));
  public static InterferenceCompensationEnum QUAD_FLAT_PACK_NOLEAD_AFFECT 
          = new InterferenceCompensationEnum(++_index, "QFN", MathUtil.convertMilsToNanoMetersInteger(10.0));
  public static InterferenceCompensationEnum CHIP_SCALE_PACKAGE_AFFECT 
          = new InterferenceCompensationEnum(++_index, "CSP", MathUtil.convertMilsToNanoMetersInteger(4.0));
  public static InterferenceCompensationEnum VARIABLE_HEIGHT_BGA_CONNECTOR_AFFECT 
          = new InterferenceCompensationEnum(++_index, "Variable Height BGA Connector", MathUtil.convertMilsToNanoMetersInteger(10.0)); //Not final
  public static InterferenceCompensationEnum EXPOSED_PAD_AFFECT 
          = new InterferenceCompensationEnum(++_index, "Exposed Pad", MathUtil.convertMilsToNanoMetersInteger(0.0));
  public static InterferenceCompensationEnum LGA_AFFECT 
          = new InterferenceCompensationEnum(++_index, "LGA", MathUtil.convertMilsToNanoMetersInteger(10.0)); //Not final
  
  /**
   * @author Chnee Khang Wah
   */
  private InterferenceCompensationEnum(int id, String name, int offset)
  {
    super(id);
    _offset = offset;
    _name = name;
    _stringToEnumMap.put(name, this);
    _integerToEnumMap.put(offset, this);
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public static int getOffset(String jointTypeName)
  {
    InterferenceCompensationEnum interferenceCompensationEnum = _stringToEnumMap.get(jointTypeName);
    Assert.expect(interferenceCompensationEnum != null);
    int offset = interferenceCompensationEnum.offset();
    return offset;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public String toString()
  {
    return _name;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  private int offset()
  {
    return _offset;
  }
}
