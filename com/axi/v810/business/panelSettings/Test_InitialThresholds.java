package com.axi.v810.business.panelSettings;

import com.axi.v810.datastore.DatastoreException;
import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.JointTypeEnum;
import com.axi.v810.datastore.Directory;
import com.axi.v810.datastore.config.Config;

/**
 * @author Andy Mechtenberg
 */
class Test_InitialThresholds extends UnitTest
{
  private Config _config;

  Test_InitialThresholds()
  {
    super();

    _config = Config.getInstance();
    try
    {
      _config.loadIfNecessary();
    }
    catch (DatastoreException ds)
    {
      Expect.expect(false);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_InitialThresholds());
  }

  /**
   * @author Andy Mechtenberg
   */
   private void testInitialThresholds()
   {
     InitialThresholds initialThresholds = InitialThresholds.getInstance();
     try
     {
       // delete all files in the initial Thresholds directory
       FileUtil.deleteDirectoryContents(Directory.getInitialThresholdsDir());
     }
     catch (CouldNotDeleteFileException ex)
     {
       Expect.expect(false);
     }

     try
     {
       // default set name is "SytemDefaults" which should not be save-able
       initialThresholds.save();
       Expect.expect(false);
     }
     catch (DatastoreException ex)
     {
       Expect.expect(false);
     }
     catch (AssertException ae)
     {
       // should assert
       Expect.expect(true);
     }

     try
     {
       // default set name is "SytemDefaults" which should not be load-able
       initialThresholds.load();
       Expect.expect(false);
     }
     catch (DatastoreException ex)
     {
       Expect.expect(false);
     }
     catch (AssertException ae)
     {
       // should assert
       Expect.expect(true);
     }

     try
     {
       // default set name is "SytemDefaults" which should not be delete-able
       initialThresholds.delete();
       Expect.expect(false);
     }
     catch (DatastoreException ex)
     {
       Expect.expect(false);
     }
     catch (AssertException ae)
     {
       // should assert
       Expect.expect(true);
     }

     Expect.expect(initialThresholds.getThresholdSetName().equals("SystemDefaults"));
     Expect.expect(initialThresholds.isDefaultSet("SystemDefaults"));
     Expect.expect(initialThresholds.getCurrentUnits().equals(MathUtilEnum.MILS));
     initialThresholds.setCurrentUnits(MathUtilEnum.MILLIMETERS);
     Expect.expect(initialThresholds.getCurrentUnits().equals(MathUtilEnum.MILLIMETERS));
     Expect.expect(initialThresholds.isDirty());
     Expect.expect(initialThresholds.getAllSetNames().size() == 1);  // just the SystemDefaults "file"

     try
     {
       // "SytemDefaults" is load-able
       initialThresholds.loadNewThresholdSet("SystemDefaults");
       Expect.expect(initialThresholds.getAllSettingsInSortedOrder().isEmpty());
       initialThresholds.loadDefaultSet();
       Expect.expect(initialThresholds.getAllSettingsInSortedOrder().isEmpty());
       Expect.expect(true);
     }
     catch (DatastoreException ex)
     {
       Expect.expect(false);
     }

     try
     {
       // default set name is "SytemDefaults" which should not be delete-able
       initialThresholds.loadNewThresholdSet("setName");
       Expect.expect(true);
     }
     catch (DatastoreException ex)
     {
       Expect.expect(false);
     }
     catch (AssertException ae)
     {
       // should assert
       Expect.expect(true);
     }


     // methods using a real set name
     String newSetName = "setName";
     initialThresholds.createNewSet(newSetName);
     Expect.expect(initialThresholds.getThresholdSetName().equals(newSetName));
     try
     {
       initialThresholds.save();
     }
     catch (DatastoreException ex)
     {
       Expect.expect(false);
     }
     try
     {
       initialThresholds.load();
     }
     catch (DatastoreException ex)
     {
       Expect.expect(false);
     }

     Expect.expect(initialThresholds.getThresholdSetName().equals(newSetName));
     Expect.expect(initialThresholds.isDefaultSet(newSetName) == false);
     Expect.expect(initialThresholds.getCurrentUnits().equals(MathUtilEnum.MILLIMETERS));
     initialThresholds.setCurrentUnits(MathUtilEnum.MILS);
     Expect.expect(initialThresholds.getCurrentUnits().equals(MathUtilEnum.MILS));
     Expect.expect(initialThresholds.isDirty());
     Expect.expect(initialThresholds.getAllSetNames().size() == 2);  // just the SystemDefaults "file" and the new one

     // populate with two settings
     InspectionFamily inspectionFamily = InspectionFamily.getInstance(JointTypeEnum.CAPACITOR);

     Algorithm measAlgo = inspectionFamily.getAlgorithm(inspectionFamily.getInspectionFamilyEnum(), AlgorithmEnum.MEASUREMENT);
     AlgorithmSetting setting = measAlgo.getAlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH);

     initialThresholds.setAlgorithmSettingValue(JointTypeEnum.CAPACITOR,
                                                             measAlgo,
                                                             setting,
                                                             "5.6");

     // create a second one
     AlgorithmSetting setting2 = measAlgo.getAlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS);
     initialThresholds.setAlgorithmSettingValue(JointTypeEnum.CAPACITOR,
                                                              measAlgo,
                                                              setting2,
                                                              "5.7");

     Expect.expect(initialThresholds.getAllSettingsInSortedOrder().size() == 2);
     try
     {
       Expect.expect(initialThresholds.isDirty());
       initialThresholds.save();
       Expect.expect(initialThresholds.isDirty() == false);
     }
     catch (DatastoreException ex)
     {
       Expect.expect(false);
     }
     initialThresholds.loadDefaultSet();
     Expect.expect(initialThresholds.getAllSettingsInSortedOrder().size() == 0);
     try
     {
       initialThresholds.loadNewThresholdSet(newSetName);
     }
     catch (DatastoreException ex)
     {
       Expect.expect(false);
     }
     Expect.expect(initialThresholds.getAllSettingsInSortedOrder().size() == 2);
     Serializable value = initialThresholds.getAlgorithmSettingValue(JointTypeEnum.CAPACITOR,
                                                             measAlgo,
                                                             setting);
     Expect.expect(value.equals("5.6"));

     value = initialThresholds.getAlgorithmSettingValue(JointTypeEnum.CAPACITOR,
                                                             measAlgo,
                                                             setting2);
     Expect.expect(value.equals("5.7"));

     // @author wei chin
     String originalDir = "$(AXI_XRAY_HOME)/config/initialThresholds";
     String newDir = Directory.getConfigDir();

     // change the initial threshold path
     try
     {
       Directory.setInitialThresholdsDir(newDir);
     }
     catch(DatastoreException ds)
     {
       Expect.expect(false);
     }

     try
     {
       Expect.expect( initialThresholds.isThresholdSetNameExist(newSetName) == false);
     }
     catch (DatastoreException ex)
     {
       // do nothing
     }

     try
     {
       Directory.setInitialThresholdsDir(originalDir);
       _config.forceLoad();
     }
     catch(DatastoreException ds)
     {
       Expect.expect(false);
     }

     try
     {
       Expect.expect( initialThresholds.isThresholdSetNameExist(newSetName));
     }
     catch (DatastoreException ex)
     {
       Expect.expect(false);
     }

     try
     {
       initialThresholds.delete();
     }
     catch (DatastoreException ex)
     {
       Expect.expect(false);
     }
     Expect.expect(initialThresholds.getAllSetNames().size() == 1);  // just the SystemDefaults "file"

   }

  /**
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testInitialThresholds();
  }
}
