package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;

/**
 * Stores relevant information about an individual joint measurement.
 *
 * @author Matt Wharton
 * @author Sunit Bhalla
 */
public class LearnedJointMeasurement
{
  private int _id = -1;
  private Pad _pad;
  private SliceNameEnum _sliceNameEnum;
  private MeasurementEnum _measurementEnum;
  private float _measurementValue;
  private boolean _populatedBoard;
  private boolean _goodJoint;

  /**
   * @author Bill Darbie
   */
  public LearnedJointMeasurement()
  {
    // do nothing
  }

  /**
   * @author Matt Wharton
   * @author Sunit Bhalla
   */
  /** @todo wpd - remove this constructor */
  public LearnedJointMeasurement(Pad pad,
                                 SliceNameEnum sliceNameEnum,
                                 MeasurementEnum measurementEnum,
                                 float measurementValue,
                                 boolean populatedBoard,
                                 boolean goodJoint)
  {
    Assert.expect(pad != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(measurementEnum != null);

    _pad = pad;
    _sliceNameEnum = sliceNameEnum;
    _measurementEnum = measurementEnum;
    _measurementValue = measurementValue;
    _populatedBoard = populatedBoard;
    _goodJoint = goodJoint;
  }

  /**
   * @author Matt Wharton
   */
  public int getId()
  {
    Assert.expect(_id >= 0);

    return _id;
  }

  /**
   * @author Matt Wharton
   */
  public void setId(int id)
  {
    Assert.expect(id >= 0);

    _id = id;
  }

  /**
   * @author Matt Wharton
   */
  public Pad getPad()
  {
    Assert.expect(_pad != null);

    return _pad;
  }

  /**
   * @author Matt Wharton
   */
  public void setPad(Pad pad)
  {
    Assert.expect(pad != null);

    _pad = pad;
  }

  /**
   * @author Matt Wharton
   */
  public SliceNameEnum getSliceNameEnum()
  {
    Assert.expect(_sliceNameEnum != null);

    return _sliceNameEnum;
  }

  /**
   * @author Matt Wharton
   */
  public void setSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);

    _sliceNameEnum = sliceNameEnum;
  }

  /**
   * @author Matt Wharton
   * @author Sunit Bhalla
   */
  public MeasurementEnum getMeasurementEnum()
  {
    Assert.expect(_measurementEnum != null);
    return _measurementEnum;
  }

  /**
   * @author Matt Wharton
   * @author Sunit Bhalla
   */
  public void setMeasurementEnum(MeasurementEnum measurementEnum)
  {
    Assert.expect(measurementEnum != null);
    _measurementEnum = measurementEnum;
  }

  /**
   * @author Matt Wharton
   * @author Sunit Bhalla
   */
  /** @todo wpd - matt - change to getValue() */
  public float getMeasurementValue()
  {
    return _measurementValue;
  }

  /**
   * @author Matt Wharton
   * @author Sunit Bhalla
   */
  /** @todo wpd - matt - change to setValue() */
  public void setMeasurementValue(float measurementValue)
  {
    _measurementValue = measurementValue;
  }

  /**
   * @author Matt Wharton
   * @author Sunit Bhalla
   */
  public boolean isPopulatedBoard()
  {
    return _populatedBoard;
  }

  /**
   * @author Matt Wharton
   * @author Sunit Bhalla
   */
  public void setPopulatedBoard(boolean populatedBoard)
  {
    _populatedBoard = populatedBoard;
  }

  /**
   * @author Matt Wharton
   * @author Sunit Bhalla
   */
  public boolean isGoodJoint()
  {
    return _goodJoint;
  }

  /**
   * @author Matt Wharton
   * @author Sunit Bhalla
   */
  public void setGoodJoint(boolean goodJoint)
  {
    _goodJoint = goodJoint;
  }

  /**
   * @author Matt Wharton
   * @author Sunit Bhalla
   */
  public static float[] convertLearnedJointMeasurementListToFloatArray(List<LearnedJointMeasurement> learnedJointMeasurements)
  {
    Assert.expect(learnedJointMeasurements != null);

    float[] learnedJointMeasurementValues = new float[learnedJointMeasurements.size()];
    int i = 0;
    for (LearnedJointMeasurement learnedJointMeasurement : learnedJointMeasurements)
    {
      learnedJointMeasurementValues[i++] = learnedJointMeasurement._measurementValue;
    }

    return learnedJointMeasurementValues;
  }

}
