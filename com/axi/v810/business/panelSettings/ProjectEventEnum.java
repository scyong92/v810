package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class ProjectEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static ProjectEventEnum CREATE_OR_DESTROY = new ProjectEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static ProjectEventEnum SLOW_LOAD = new ProjectEventEnum(++_index);
  public static ProjectEventEnum FAST_LOAD = new ProjectEventEnum(++_index);
  public static ProjectEventEnum UNLOAD = new ProjectEventEnum(++_index);
  public static ProjectEventEnum SAVE = new ProjectEventEnum(++_index);
  public static ProjectEventEnum DELETE = new ProjectEventEnum(++_index);
  public static ProjectEventEnum IMPORT_PROJECT_FROM_NDFS = new ProjectEventEnum(++_index);
  public static ProjectEventEnum NAME = new ProjectEventEnum(++_index);
  public static ProjectEventEnum PANEL = new ProjectEventEnum(++_index);
  public static ProjectEventEnum PROJECT_TEST_PROGRAM_VERSION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum PROJECT_MINOR_VERSION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum PROGRAM_GENERATION_VERSION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum SOFTWARE_VERSION_CREATION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum NOTES = new ProjectEventEnum(++_index);
  public static ProjectEventEnum INITIAL_THESHOLDS = new ProjectEventEnum(++_index);
  public static ProjectEventEnum INITIAL_RECIPE_SETTING = new ProjectEventEnum(++ _index);
  public static ProjectEventEnum DISPLAY_UNITS = new ProjectEventEnum(++_index);
  public static ProjectEventEnum READY_FOR_PRODUCTION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum TEST_PROGRAM_UPDATED_WITH_ALIGNMENT_REGIONS = new ProjectEventEnum(++_index);
  public static ProjectEventEnum TEST_PROGRAM_CLEARING = new ProjectEventEnum(++_index);
  public static ProjectEventEnum TEST_PROGRAM_GENERATED = new ProjectEventEnum(++_index);
  public static ProjectEventEnum TEST_PROGRAM_CHECK_SUM_INVALIDATED = new ProjectEventEnum(++_index);
  public static ProjectEventEnum VERIFICATION_IMAGES_GENERATED = new ProjectEventEnum(++_index);
  public static ProjectEventEnum LAST_MODIFICATION_TIME = new ProjectEventEnum(++_index);
  public static ProjectEventEnum TARGET_CUSTOMER_NAME = new ProjectEventEnum(++_index);
  public static ProjectEventEnum PROGRAMMER_NAME = new ProjectEventEnum(++_index);
  public static ProjectEventEnum PANEL_IMAGE_CREATION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum DELETE_SHORTS_LEARNING = new ProjectEventEnum(++_index);
  public static ProjectEventEnum DELETE_EXPECTED_IMAGE_LEARNING = new ProjectEventEnum(++_index);
  public static ProjectEventEnum DELETE_EXPECTED_IMAGE_TEMPLATE_LEARNING = new ProjectEventEnum(++_index);//ShengChuan - Clear Tombstone
  public static ProjectEventEnum DELETE_SUBTYPE_LEARNING = new ProjectEventEnum(++_index);
  public static ProjectEventEnum LIBRARY_IMPORT_COMPLETE = new ProjectEventEnum(++_index);
  public static ProjectEventEnum LOADING_PROJECT_WITH_DIFFERENT_SYSTEM_TYPE = new ProjectEventEnum(++_index);
  public static ProjectEventEnum LOADING_LONG_PANEL_WITH_DIFFERENT_SYSTEM_TYPE = new ProjectEventEnum(++_index);
  public static ProjectEventEnum SYSTEM_TYPE = new ProjectEventEnum(++_index);
  public static ProjectEventEnum SCAN_PATH_METHOD = new ProjectEventEnum(++_index);
  public static ProjectEventEnum TEST_PROGRAM_USER_GAIN_CHANGED = new ProjectEventEnum(++_index);
  public static ProjectEventEnum TEST_PROGRAM_STAGE_SPEED_CHANGED = new ProjectEventEnum(++_index);
  public static ProjectEventEnum TEST_PROGRAM_SIGNAL_OR_ARTIFACT_COMPENSATION_CHANGED = new ProjectEventEnum(++_index);
  public static ProjectEventEnum TEST_PROGRAM_BOARD_Z_OFFSET_CHANGED = new ProjectEventEnum(++_index);
  public static ProjectEventEnum ENABLE_LARGE_VIEW_IMAGE = new ProjectEventEnum(++_index);
  public static ProjectEventEnum GENERATE_MULTI_ANGLE_IMAGE = new ProjectEventEnum(++_index);
  public static ProjectEventEnum GENERATE_COMPONENT_IMAGE = new ProjectEventEnum(++_index);
  public static ProjectEventEnum ENLARGE_RECONSTRUCTION_REGION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum LOADING_PROJECT_WITH_DIFFERENT_LOW_MAGNIFICATION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum LOADING_PROJECT_WITH_DIFFERENT_MAGNIFICATION_FOR_OFFLINE_PROGRAMMING = new ProjectEventEnum(++_index);
  public static ProjectEventEnum LOADING_PROJECT_WITH_DIFFERENT_HIGH_MAGNIFICATION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum LOW_MAGNIFICATION_CHANGED = new ProjectEventEnum(++_index);
  public static ProjectEventEnum HIGH_MAGNIFICATION_CHANGED = new ProjectEventEnum(++_index);
  public static ProjectEventEnum ENABLE_BGA_JOINT_BASED_THRESHOLD_INSPECTION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum SET_BGA_INSPECTION_REGION_TO_1X1 = new ProjectEventEnum(++_index);
  public static ProjectEventEnum PROJECT_BASED_BYPASS_MODE = new ProjectEventEnum(++_index);
  public static ProjectEventEnum SET_THICKNESS_TABLE_VERSION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum SET_USE_NEW_SCAN_ROUTE_GENERATION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum SET_USE_VARIABLE_DIVN = new ProjectEventEnum(++_index);
  public static ProjectEventEnum SET_DYNAMIC_RANGE_OPTIMIZATION_VERSION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum LOADING_PROJECT_WITH_DIFFERENT_THICKNESS_TABLE_VERSION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum LOADING_PROJECT_WITH_ALTERNATIVE_THICKNESS_TABLE_VERSION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum ENLARGE_ALIGNMENT_REGION = new ProjectEventEnum(++_index);
  public static ProjectEventEnum IMPORT_LAND_PATTERN_COMPLETE = new ProjectEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private ProjectEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private ProjectEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
