package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import java.util.*;

/**
 * @author Khaw Chek Hau
 * XCR2943: Alignment Fail Due to Too Close To Edge
 */
public class ProjectAlignmentSettingEnum extends com.axi.util.Enum
{
  private static int _index = 0;
  private String _name;
  private static java.util.List<ProjectAlignmentSettingEnum> _projectAlignmentSettingEnumList = new ArrayList<ProjectAlignmentSettingEnum>();
  private java.util.List<String> _selectionList = new ArrayList<String>();
 
  public static ProjectAlignmentSettingEnum ENLARGE_ALIGNMENT_REGION = new ProjectAlignmentSettingEnum(_index++, "Enlarge alignment region", null);

  /**
   * @author Khaw Chek Hau
   * XCR2943: Alignment Fail Due to Too Close To Edge
   */
  private ProjectAlignmentSettingEnum(int id, String name, java.util.List<String> selectionList)
  {
    super(id);
    Assert.expect(name != null);

    _name = name.intern();
    _selectionList = selectionList;
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR2943: Alignment Fail Due to Too Close To Edge
   */
  public String getName()
  {
    Assert.expect(_name != null);
    return _name;
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR2943: Alignment Fail Due to Too Close To Edge
   */
  public java.util.List<String> getSelectionList()
  {
    return _selectionList;
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR2943: Alignment Fail Due to Too Close To Edge
   */
  public static java.util.List<ProjectAlignmentSettingEnum> getAllProjectAlignmentSettingList()
  {
    if (_projectAlignmentSettingEnumList != null)
      _projectAlignmentSettingEnumList.clear();
    
    _projectAlignmentSettingEnumList.add(ENLARGE_ALIGNMENT_REGION);
    
    return _projectAlignmentSettingEnumList;
  }
}


