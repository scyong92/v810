package com.axi.v810.business.panelSettings;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;

/**
 * @author Cheah Lee Herng
 */
public class PspSettings implements Serializable 
{
  private Panel _panel;
  
  private int _autoPopulateSpacingInNanometers = 38100000; // 1.5 inches
  private boolean _isRemoveRectanglesOnTopOfComponent = false;
  private boolean _isExtendRectanglesToEdgeOfBoard = false;
  private boolean _isUseMeshTriangle = false;
  private boolean _isShowMeshTriangle = false;
  private boolean _isApplyToAllBoard = false;
  private boolean _isAutomaticallyIncludeAllComponents = false;
  private boolean _isLargerFOV = false;
  private int _currentSetting = 350;
 
  private static transient ProjectObservable _projectObservable;
  
  /**
   * @author Cheah Lee Herng
   */
  static
  {
      _projectObservable = ProjectObservable.getInstance();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public PspSettings()
  {
    _projectObservable.stateChanged(this, null, this, PspEventEnum.CREATE_OR_DESTROY);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void destroy()
  {
    _projectObservable.stateChanged(this, this, null, PspEventEnum.CREATE_OR_DESTROY);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setAutoPopulateSpacingInNanometers(int autoPopulateSpacingInNanometers)
  {
    _autoPopulateSpacingInNanometers = autoPopulateSpacingInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getAutoPopulateSpacingInNanometers()
  {
    return _autoPopulateSpacingInNanometers;
  }
  
  /**
   * @author Jack Hwee
   */
  public void setIsRemoveRectanglesOnTopOfComponent(boolean isRemoveRectanglesOnTopOfComponent)
  {
    _isRemoveRectanglesOnTopOfComponent = isRemoveRectanglesOnTopOfComponent;
  }
  
  /**
   * @author Jack Hwee
   */
  public boolean isRemoveRectanglesOnTopOfComponent()
  {
    return _isRemoveRectanglesOnTopOfComponent;
  }
  
  /**
   * @author Jack Hwee
   */
  public void setIsExtendRectanglesToEdgeOfBoard(boolean isExtendRectanglesToEdgeOfBoard)
  {
    _isExtendRectanglesToEdgeOfBoard = isExtendRectanglesToEdgeOfBoard;
  }
  
  /**
   * @author Jack Hwee
   */
  public boolean isExtendRectanglesToEdgeOfBoard()
  {
    return _isExtendRectanglesToEdgeOfBoard;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setIsUseMeshTriangle(boolean isUseMeshTriangle)
  {
    _isUseMeshTriangle = isUseMeshTriangle;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean isUseMeshTriangle()
  {
    return _isUseMeshTriangle;
  }
  
  /**
   * @author Jack Hwee
   */
  public void setIsShowMeshTriangle(boolean isShowMeshTriangle)
  {
    _isShowMeshTriangle = isShowMeshTriangle;
  }
  
  /**
   * @author Jack Hwee
   */
  public boolean isShowMeshTriangle()
  {
    return _isShowMeshTriangle;
  }
  
  /**
   * @author Jack Hwee
   */
  public void setIsApplyToAllBoard(boolean isApplyToAllBoard)
  {
    _isApplyToAllBoard = isApplyToAllBoard;
  }
  
  /**
   * @author Jack Hwee
   */
  public boolean isApplyToAllBoard()
  {
    return _isApplyToAllBoard;
  }
  
  /**
   * @author Jack Hwee
   */
  public void setIsAutomaticallyIncludeAllComponents(boolean isAutomaticallyIncludeAllComponents)
  {
    _isAutomaticallyIncludeAllComponents = isAutomaticallyIncludeAllComponents;
  }
  
  /**
   * @author Jack Hwee
   */
  public boolean isAutomaticallyIncludeAllComponents()
  {
    return _isAutomaticallyIncludeAllComponents;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setUseLargerFOV(boolean isLargerFOV)
  {
    _isLargerFOV = isLargerFOV;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean isLargerFOV()
  {
    return _isLargerFOV;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setPanel(Panel panel)
  {
    Assert.expect(panel != null);

    if (panel == _panel)
      return;

    Panel oldValue = _panel;
    _projectObservable.setEnabled(false);
    try
    {
      _panel = panel;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, panel, PspEventEnum.PANEL);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public Panel getPanel()
  {
    Assert.expect(_panel != null);

    return _panel;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setCurrentSetting(int currentSetting)
  {
    _currentSetting = currentSetting;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getCurrentSetting()
  {
    return _currentSetting;
  }
}
