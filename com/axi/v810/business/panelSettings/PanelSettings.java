package com.axi.v810.business.panelSettings;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class PanelSettings implements Serializable
{
  // created by PanelNdfReader, PanelSettingsReader
  private Panel _panel; // PanelSettingsReader

  private boolean _leftToRightFlip = false;
  private boolean _topToBottomFlip = false;
  private int _degreesRotationRelativeToCad = -1; // PanelNdfReader, PanelSettingsReader
  private int _shiftXinNanoMeters; // calculated
  private int _shiftYinNanoMeters; // calculated

  private AffineTransform _rightManualAlignmentTransform; // right manual alignment transform
  private AffineTransform _leftManualAlignmentTransform; // left manual alignment transform
  private AffineTransform _rightRuntimeAlignmentTransform; // right runtime alignment transform
  private AffineTransform _leftRuntimeAlignmentTransform; // left runtime alignment transform

  private AffineTransform _rightManualAlignmentTransformForHighMag; // right manual alignment transform
  private AffineTransform _leftManualAlignmentTransformForHighMag; // left manual alignment transform
  private AffineTransform _rightRuntimeAlignmentTransformForHighMag; // right runtime alignment transform
  private AffineTransform _leftRuntimeAlignmentTransformForHighMag; // left runtime alignment transform

  // we have two alignment groups to handle long panels
  // if the panel is not long, then only the rightAlignmentGroup is used
  // right name is used assuming a left to right system.
  // if we have a long panel, then we also use the left alignment groups
  private List<AlignmentGroup> _rightAlignmentGroups = new ArrayList<AlignmentGroup>(); // PanelSettingsReader
  private List<AlignmentGroup> _leftAlignmentGroups = new ArrayList<AlignmentGroup>();

  private Map<String, Set<SliceNameEnum>> _unfocusedRegionSlicesToBeFixedMap = new HashMap<String, Set<SliceNameEnum>>();
  private Map<String, Set<Pair<SliceNameEnum, Integer>>> _unfocusedRegionSlicesToZoffsetInNanosMap = new HashMap<String, Set<Pair<SliceNameEnum, Integer>>>();
  private Set<String> _unfocusedRegionNamesToRetestSet = new HashSet<String>();
  private Map<String, Set<PadType>> _noTestRegionsNameToPadTypeMap = new HashMap<String, Set<PadType>>();
  private Boolean _isPotentialLongProgram;
  private boolean _userWarnedOfThicknessImportance = false;
  private boolean _isPanelBasedAlignment = true;
  private int _isAllComponentSameUserGain = -1;
  private int _isAllComponentSameStageSpeed = -1;
  private int _haslComponentUsingHighMag = -1;
  private int _hasComponentUsingLowMag = -1;
  private static transient ProjectObservable _projectObservable;
  private static transient ProgramGeneration _programGeneration;
  private UserGainEnum _panelBaseUserGain = UserGainEnum.getUserGainToEnumMapValue((int) Config.getInstance().getDoubleValue(HardwareConfigEnum.AXI_TDI_CAMERA_USER_GAIN));
  private List<StageSpeedEnum> _panelBasStageSpeed = new ArrayList<StageSpeedEnum>();
  private int _splitLocationInNanometer = -1;
  
  private boolean _useNewThicknessTable = false;
  private boolean _bypassAlignmentTranformWithinAcceptableLimit = false;
  
  //Siew Yeng - XCR1757 - use in production only
  private transient AffineTransform _rightRuntimeManualAlignmentTransform; // right runtime manual alignment transform
  private transient AffineTransform _leftRuntimeManualAlignmentTransform; // left runtime manual alignment transform
  private transient AffineTransform _rightRuntimeManualAlignmentTransformForHighMag; // right runtime manual alignment transform
  private transient AffineTransform _leftRuntimeManualAlignmentTransformForHighMag; // left runtime manual alignment transform
  
  //Kee Chin Seong - Separating out the AFfine Transform from View CAD
  private AffineTransform _rightLastSavedViewCadRuntimeAlignmentTransform; // right view Cad runtime alignment transform
  private AffineTransform _leftLastSavedViewCadRuntimeAlignmentTransform; // left view Cad runtime alignment transform
  private transient AffineTransform _rightLastSavedViewCadRuntimeAlignmentTransformForHighMag; // right view Cad runtime manual alignment transform
  private transient AffineTransform _leftLastSavedViewCadRuntimeAlignmentTransformForHighMag; // left view Cad runtime manual alignment transform
  
  
  //Kok Chun, Tan - XCR-2621 - Variant Management
  private List<VariationSetting> _noLoadSettingVariationList = new ArrayList<VariationSetting> ();
  
  //Khaw Chek Hau - XCR2285: 2D Alignment on v810
  private boolean _isUsing2DAlignmentMethod = false;
  
  //XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
  private boolean _isManualAlignmentAllCompleted = false;
  
  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
    _programGeneration = ProgramGeneration.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public PanelSettings()
  {
    _projectObservable.stateChanged(this, null, this, PanelSettingsEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.stateChanged(this, this, null, PanelSettingsEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void setPanel(Panel panel)
  {
    Assert.expect(panel != null);

    if (panel == _panel)
      return;

    Panel oldValue = _panel;
    _projectObservable.setEnabled(false);
    try
    {
      _panel = panel;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, panel, PanelSettingsEventEnum.PANEL);
  }

  /**
   * @author Bill Darbie
   */
  public Panel getPanel()
  {
    Assert.expect(_panel != null);

    return _panel;
  }

  /**
   * Flip the panel over.
   * @author Bill Darbie
   */
  public void flip()
  {
    int degrees = getPanel().getDegreesRotationRelativeToCad();
    if ((degrees == 0) || (degrees == 180))
    {
      setLeftToRightFlip(!_leftToRightFlip);
    }
    else if ((degrees == 90) || (degrees == 270))
    {
      setTopToBottomFlip(!_topToBottomFlip);
    }
    else
      Assert.expect(false);
  }

  /**
   * @return true if the Panel has been flipped so it is upside down in the machine.
   * @author Bill Darbie
   */
  public boolean isFlipped()
  {
    if (((_leftToRightFlip) && (_topToBottomFlip == false)) ||
         ((_leftToRightFlip == false) && (_topToBottomFlip)))
      return true;

     return false;
  }

  /**
   * @author Bill Darbie
   */
  public void setLeftToRightFlip(boolean leftToRightFlip)
  {
    _projectObservable.setEnabled(false);
    try
    {
      _leftToRightFlip = leftToRightFlip;
      if (_panel != null)
        _panel.invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, !leftToRightFlip, leftToRightFlip, PanelSettingsEventEnum.LEFT_TO_RIGHT_FLIP);
  }

  /**
   * @author Bill Darbie
   */
  public void setTopToBottomFlip(boolean topToBottomFlip)
  {
    _projectObservable.setEnabled(false);
    try
    {
      _topToBottomFlip = topToBottomFlip;
      if (_panel != null)
        _panel.invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, !topToBottomFlip, topToBottomFlip, PanelSettingsEventEnum.TOP_TO_BOTTOM_FLIP);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isLeftToRightFlip()
  {
    return _leftToRightFlip;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isTopToBottomFlip()
  {
    return _topToBottomFlip;
  }

  /**
   * @author Bill Darbie
   */
  public void setDegreesRotationRelativeToCad(int degreesRotationRelativeToCad)
  {
    degreesRotationRelativeToCad = MathUtil.getDegreesWithin0To359(degreesRotationRelativeToCad);

    Assert.expect(DegreesEnum.isValid(degreesRotationRelativeToCad));

    if (degreesRotationRelativeToCad == _degreesRotationRelativeToCad)
      return;

    int oldValue = _degreesRotationRelativeToCad;
    _projectObservable.setEnabled(false);
    try
    {
      calculateShiftsToRetainLowerLeftCorner(degreesRotationRelativeToCad);

      _degreesRotationRelativeToCad = degreesRotationRelativeToCad;
      if (_panel != null)
        _panel.invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, degreesRotationRelativeToCad, PanelSettingsEventEnum.DEGREES_ROTATION);
  }

  /**
   * This method is used to determine how much to shift in x and y after a Panel rotation
   * is changed in order to keep the Panels lower left corner in the same
   * place.
   * @author Bill Darbie
   */
  public void calculateShiftsToRetainLowerLeftCorner(int degreesRotation)
  {
    Assert.expect(_panel != null);
    int width = _panel.getWidthInNanoMeters();
    int length = _panel.getLengthInNanoMeters();

    int shiftXinNanoMeters = 0;
    int shiftYinNanoMeters = 0;
    if (degreesRotation == 0)
    {
      shiftXinNanoMeters = 0;
      shiftYinNanoMeters = 0;
    }
    else if (degreesRotation == 90)
    {
      shiftXinNanoMeters = length;
      shiftYinNanoMeters = 0;
    }
    else if (degreesRotation == 180)
    {
      shiftXinNanoMeters = width;
      shiftYinNanoMeters = length;
    }
    else if (degreesRotation == 270)
    {
      shiftXinNanoMeters = 0;
      shiftYinNanoMeters = width;
    }
    setShiftXinNanoMeters(shiftXinNanoMeters);
    setShiftYinNanoMeters(shiftYinNanoMeters);
  }

  /**
   * @author Bill Darbie
   */
  public int getDegreesRotationRelativeToCad()
  {
    Assert.expect(DegreesEnum.isValid(_degreesRotationRelativeToCad));

    return _degreesRotationRelativeToCad;
  }

  /**
   * @return the degrees rotation after all rotations from this Panel have been
   * applied.
   *
   * @author Bill Darbie
   */
  public int getDegreesRotationAfterAllRotations()
  {
    int degrees = _degreesRotationRelativeToCad;

    if (_leftToRightFlip)
      degrees = 180 - degrees;

    if (_topToBottomFlip)
      degrees = -degrees;

    degrees = MathUtil.getDegreesWithin0To359(degrees);

    return degrees;
  }

  /**
   * @author Bill Darbie
   */
  private void setShiftXinNanoMeters(int shiftXinNanoMeters)
  {
    int oldValue = _shiftXinNanoMeters;
    _projectObservable.setEnabled(true);
    try
    {
      _shiftXinNanoMeters = shiftXinNanoMeters;
    }
    finally
    {
      _projectObservable.setEnabled(false);
    }
    _projectObservable.stateChanged(this, oldValue, shiftXinNanoMeters, PanelSettingsEventEnum.SHIFT_X);
  }

  /**
   * @author Bill Darbie
   */
  public int getShiftXinNanoMeters()
  {
    return _shiftXinNanoMeters;
  }

  /**
   * @author Bill Darbie
   */
  private void setShiftYinNanoMeters(int shiftYinNanoMeters)
  {
    int oldValue = _shiftYinNanoMeters;
    _projectObservable.setEnabled(true);
    try
    {
      _shiftYinNanoMeters = shiftYinNanoMeters;
    }
    finally
    {
      _projectObservable.setEnabled(false);
    }
    _projectObservable.stateChanged(this, oldValue, shiftYinNanoMeters, PanelSettingsEventEnum.SHIFT_Y);
  }

  /**
   * @author Bill Darbie
   */
  public int getShiftYinNanoMeters()
  {
    return _shiftYinNanoMeters;
  }

  /**
   * @author Bill Darbie
   * @Edited By Kee Chin Seong - Manual Alignment now should checked is LONG or SHORT 
   *                             Panel with LONG or SHORT Program
   * @Edited By Phang Siew Yeng - XCR1638 - Add checking for long panel, if does not have testSubProgram, 
   *                                        alignment is not needed.  
   *                            - call isManualAlignmentCompleted() to check alignment COMPLETE
   */
  public boolean hasManualAlignmentBeenPerformed()
  {
    Assert.expect(_panel != null);

    if(isPanelBasedAlignment())
    {
      if(isLongPanel())
      {
        if(_panel.getProject().getTestProgram().isLongProgram())
        {
//          if (leftManualAlignmentTransformExists() == false || 
//              rightManualAlignmentTransformExists() == false)
          if((leftManualAlignmentTransformExists() == false && 
              _panel.getProject().getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.LOW))|| 
              (rightManualAlignmentTransformExists() == false &&
              _panel.getProject().getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.LOW)))
          {
            return false;
          }
        }
        else
        {
          if(rightManualAlignmentTransformExists() == false)
          {
            return false;
          }
        }
      }
      else
      {
        if (rightManualAlignmentTransformExists() == false)
        {
          return false;
        }   
      }
      
//      for(TestSubProgram subProgram: _panel.getProject().getTestProgram().getAllLowMagTestSubPrograms())
//      {
//        if(subProgram.isSubProgramPerformAlignment())
//        {
//          if(subProgram.isAllRegionNotIspectable()==false)
//          {
//            if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
//            {
//              if (leftManualAlignmentTransformExists() == false)
//              {
//                return false;
//              }
//            }
//            else
//            {
//              if (rightManualAlignmentTransformExists() == false)
//              {
//                return false;
//              }
//            }
//          }
//        }
//      }
    }
    else
    {
      for(Board board : _panel.getBoards())
      {
        if(board.isLongBoard())
        {
//          if(board.getBoardSettings().leftManualAlignmentTransformExists() == false ||
//             board.getBoardSettings().rightManualAlignmentTransformExists() == false)
          if((board.getBoardSettings().leftManualAlignmentTransformExists() == false &&
             _panel.getProject().getTestProgram().hasTestSubProgram(board, PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.LOW))|| 
             (board.getBoardSettings().rightManualAlignmentTransformExists() == false &&
            _panel.getProject().getTestProgram().hasTestSubProgram(board, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.LOW)))
          {
            return false;
          }
        }
        else
        {
          if(board.getBoardSettings().leftManualAlignmentTransformExists() == false && 
             board.getBoardSettings().rightManualAlignmentTransformExists() == false)
            return false;
        }
      }
      
//      for(TestSubProgram subProgram: _panel.getProject().getTestProgram().getAllLowMagTestSubPrograms())
//      {
//        if(subProgram.isSubProgramPerformAlignment())
//        {
//          if(subProgram.isAllRegionNotIspectable()==false)
//          {
//            if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
//            {
//              if ((subProgram.getBoard().getBoardSettings().leftManualAlignmentTransformExists() == false))
//              {
//                return false;
//              }
//            }
//            else
//            {
//              if ((subProgram.getBoard().getBoardSettings().rightManualAlignmentTransformExists() == false))
//              {
//                return false;
//              }
//            }
//          }
//        }
//      }
    }

    return true;
  }

  /**
   * Set the manual alignment transform for this long panel when it is
   * positioned at the left panel-in-place sensor.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void setLeftManualAlignmentTransform(AffineTransform leftManualAlignmentTransform)
  {
    Assert.expect(leftManualAlignmentTransform != null);
    Assert.expect(isLongPanel());

    if (leftManualAlignmentTransform == _leftManualAlignmentTransform)
      return;

    Pair<AffineTransform, AffineTransform> oldValue =
      new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform);
    _projectObservable.setEnabled(false);
    try
    {
      _leftManualAlignmentTransform = leftManualAlignmentTransform;
      setLeftLastSavedViewCadRuntimeAlignmentTransform(leftManualAlignmentTransform);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }

    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform),
                                    PanelSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_GENERATED);
  }

  /**
   * Set the runtime alignment transform for this long panel when it is
   * positioned at the left panel-in-place sensor.

   * @author Matt Wharton
   */
  public void setLeftRuntimeAlignmentTransform(AffineTransform leftRuntimeAlignmentTransform)
  {
    Assert.expect(leftRuntimeAlignmentTransform != null);
    Assert.expect(isLongPanel());

    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _leftRuntimeAlignmentTransform = leftRuntimeAlignmentTransform;
  }

  /**
   * Set the manual alignment transform for this long panel when it is
   * positioned at the right panel-in-place sensor.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void setRightManualAlignmentTransform(AffineTransform rightManualAlignmentTransform)
  {
    Assert.expect(rightManualAlignmentTransform != null);

    if (_rightManualAlignmentTransform == rightManualAlignmentTransform)
      return;

    Pair<AffineTransform, AffineTransform> oldValue =
        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform);
    _projectObservable.setEnabled(false);
    try
    {
      _rightManualAlignmentTransform = rightManualAlignmentTransform;
      //setRightLastSavedRuntimeAlignmentTransform(rightManualAlignmentTransform);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform),
                                    PanelSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_GENERATED);
  }

  /**
   * Set the runtime alignment transform for this long panel when it is
   * positioned at the right panel-in-place sensor.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void setRightRuntimeAlignmentTransform(AffineTransform rightRuntimeAlignmentTransform)
  {
    Assert.expect(rightRuntimeAlignmentTransform != null);

    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _rightRuntimeAlignmentTransform = rightRuntimeAlignmentTransform;
  }
  
  /**
   * @author Kee Chin Seong - Separating out the AFfine Transform from View CAD
   * @param rightRuntimeAlignmentTransform 
   */
  public void setRightLastSavedViewCadRuntimeAlignmentTransform(AffineTransform rightRuntimeAlignmentTransform)
  {
    Assert.expect(rightRuntimeAlignmentTransform != null);

    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _rightLastSavedViewCadRuntimeAlignmentTransform = rightRuntimeAlignmentTransform;
  }
  
  /**
   * @author Kee Chin Seong - Separating out the AFfine Transform from View CAD
   * @param leftRuntimeAlignmentTransform 
   */
  public void setLeftLastSavedViewCadRuntimeAlignmentTransform(AffineTransform leftRuntimeAlignmentTransform)
  {
    Assert.expect(leftRuntimeAlignmentTransform != null);

    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _leftLastSavedViewCadRuntimeAlignmentTransform = leftRuntimeAlignmentTransform;
  }

  /**
   * Sets the manual and runtime left and right alignment transforms back to null and marks alignment as invalid.
   *
   * @author Matt Wharton
   * @edited By Kee Chin Seong - Separating out the AFfine Transform from View CAD
   * @edited hee-jihn.chuah - XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
   */
  public void clearAllAlignmentTransforms()
  {
    _isManualAlignmentAllCompleted = false;
    Pair<AffineTransform, AffineTransform> oldValue =
        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform);
    _projectObservable.setEnabled(false);
    try
    {
      _leftManualAlignmentTransform = null;
      _rightManualAlignmentTransform = null;
      _leftRuntimeAlignmentTransform = null;
      _rightRuntimeAlignmentTransform = null;
      
      //Kee Chin Seong - Separating out the AFfine Transform from View CAD
      _leftLastSavedViewCadRuntimeAlignmentTransform = null;
      _rightLastSavedViewCadRuntimeAlignmentTransform = null;
      _leftLastSavedViewCadRuntimeAlignmentTransformForHighMag = null;
      _rightLastSavedViewCadRuntimeAlignmentTransformForHighMag = null;
      
      _leftManualAlignmentTransformForHighMag = null;
      _rightManualAlignmentTransformForHighMag = null;
      _leftRuntimeAlignmentTransformForHighMag = null;
      _rightRuntimeAlignmentTransformForHighMag = null;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(null, null),
                                    PanelSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED);
  }

  /**
   * Get the manual alignment AffineTransform for this panel when
   * it is positioned against the left panel-in-place sensor.

   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public AffineTransform getLeftManualAlignmentTransform()
  {
    Assert.expect(_leftManualAlignmentTransform != null);
    Assert.expect(isLongPanel());
    Assert.expect(getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    return _leftManualAlignmentTransform;
  }

  /**
   * Get the runtime alignment AffineTransform for this panel when
   * it is positioned against the left panel-in-place sensor.

   * @author Matt Wharton
   */
  public AffineTransform getLeftRuntimeAlignmentTransform()
  {
    Assert.expect(_leftManualAlignmentTransform != null);
    Assert.expect(isLongPanel());
    Assert.expect(getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    if (_leftRuntimeAlignmentTransform == null)
    {
      _leftRuntimeAlignmentTransform = new AffineTransform(_leftManualAlignmentTransform);
    }

    return _leftRuntimeAlignmentTransform;
  }
  
  /**
   * @author Kee Chin Seong - Separating out the AFfine Transform from View CAD
   * @return 
   */
  public AffineTransform getLeftLastSavedViewCadRuntimeAlignmentTransform()
  {
    Assert.expect(_leftManualAlignmentTransform != null);
    Assert.expect(isLongPanel());
    Assert.expect(getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    if (_leftLastSavedViewCadRuntimeAlignmentTransform == null)
    {
      _leftLastSavedViewCadRuntimeAlignmentTransform = new AffineTransform(_leftManualAlignmentTransform);
    }

    return _leftLastSavedViewCadRuntimeAlignmentTransform;
  }
  
  

  /**
   * Returns true if the left manual alignment transform exists.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public boolean leftManualAlignmentTransformExists()
  {
    if ((_leftManualAlignmentTransform == null) ||
        getPanel().getProject().getProjectState().isManualAlignmentTransformValid() == false)
      return false;

    return true;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasModifiedFromLongPanelToShortPanel()
  {
    if (isLongPanel() == false && 
        (_leftManualAlignmentTransform != null || _leftManualAlignmentTransformForHighMag != null))
    {
      return true;
    }
    else
      return false;
  }

  /**
   * Returns true if the left runtime alignment transform exists.
   *
   * @author Matt Wharton
   */
  public boolean leftRuntimeAlignmentTransformExists()
  {
    // If we have at least a manual transform, we are guaranteed to have something we can use as a runtime
    // transform.
    return leftManualAlignmentTransformExists();
  }

  /**
   * Get the manual alignment AffineTransform for this panel when
   * it is positioned against the right panel-in-place sensor.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public AffineTransform getRightManualAlignmentTransform()
  {
    Assert.expect(_rightManualAlignmentTransform != null);
    Assert.expect(getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    return _rightManualAlignmentTransform;
  }

  /**
   * Get the runtime alignment AffineTransform for this panel when
   * it is positioned against the right panel-in-place sensor.
   *
   * @author Matt Wharton
   */
  public AffineTransform getRightRuntimeAlignmentTransform()
  {
    Assert.expect(_rightManualAlignmentTransform != null);
    Assert.expect(getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    if (_rightRuntimeAlignmentTransform == null)
    {
      _rightRuntimeAlignmentTransform = new AffineTransform(_rightManualAlignmentTransform);
    }

    return _rightRuntimeAlignmentTransform;
  }
  
  /**
   * @author Kee Chin Seong - Separating out the AFfine Transform from View CAD
   * @return 
   */
  public AffineTransform getRightLastSavedViewCadRuntimeAlignmentTransform()
  {
    Assert.expect(_rightManualAlignmentTransform != null);
    Assert.expect(getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    //it could be empty for the first time, so get the Manual Alignment Transform first.
    if (_rightLastSavedViewCadRuntimeAlignmentTransform == null)
    {
      _rightLastSavedViewCadRuntimeAlignmentTransform = new AffineTransform(_rightManualAlignmentTransform);
    }

    return _rightLastSavedViewCadRuntimeAlignmentTransform;
  }

  /**
   * @author Bill Darbie
   * @author Wei Chin, Chong
   * @author Phang Siew Yeng
   * This function is ONLY called by VerifyCadPanel to check if ALIGNMENT TRANSFORM is VALID.
   * For long panel/long board, alignment transform VALID - both left and right alignment transforms exist.
   * For short panel, alignment transform VALID - right alignment transform exists.
   * NOTE: Alignment VALID != Alignment COMPLETE
   */
  public boolean areManualAlignmentTransformsValid()
  {
    boolean valid = true;
    if(isPanelBasedAlignment())
    {
      if (isLongPanel())
      {
        //if(isPotentialLongProgram())
        if(_panel.getProject().getTestProgram().isLongProgram())
        {
          if (leftManualAlignmentTransformExists() == false || 
            //this is to avoid getting previously saved alignment transform. Double check if the testsubprogram exist.
            _panel.getProject().getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.LOW) == false)
            valid = false;
        }
      }

      if (rightManualAlignmentTransformExists() == false ||
        _panel.getProject().getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.LOW) == false)
        valid = false;
    }
    else
    {
      // Wei Chin added for individual alignment
      for(Board board : _panel.getBoards())
      {
        valid = board.getBoardSettings().areManualAlignmentTransformsValid();
        
        if(valid == false)
          break;
      }
    }

    return valid;
  }

  /**
   * @return true if the right manual alignment transform exists.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public boolean rightManualAlignmentTransformExists()
  {
    if ((_rightManualAlignmentTransform == null) ||
        getPanel().getProject().getProjectState().isManualAlignmentTransformValid() == false)
      return false;

    return true;
  }

  /**
   * Returns true if the left runtime alignment transform exists.
   *
   * @author Matt Wharton
   */
  public boolean rightRuntimeAlignmentTransformExists()
  {
    // If we have at least a manual transform, we are guaranteed to have something we can use as a runtime
    // transform.
    return rightManualAlignmentTransformExists();
  }

  /**
   * @author Bill Darbie
   */
  public void addAlignmentGroup(AlignmentGroup alignmentGroup)
  {
    Assert.expect(alignmentGroup != null);
    Assert.expect(_rightAlignmentGroups != null);

    _projectObservable.setEnabled(false);
    try
    {
      _rightAlignmentGroups.add(alignmentGroup);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, alignmentGroup, PanelSettingsEventEnum.ADD_ALIGNMENT_GROUP);
  }

  /**
   * @return a List of AlignmentGroup objects
   * @author Bill Darbie
   */
  public List<AlignmentGroup> getAlignmentGroupsForShortPanel()
  {
//    Assert.expect(isLongPanel() == false);
    Assert.expect(_rightAlignmentGroups != null);

    return _rightAlignmentGroups;
  }

  /**
   * @author George A. David
   */
  public boolean hasAlignmentGroups()
  {
    return ((_rightAlignmentGroups != null) && (_rightAlignmentGroups.isEmpty() == false));
  }

  /**
   * @author Bill Darbie
   */
  public void assignEmptyRightAlignmentGroups()
  {
    AlignmentGroup alignmentGroup1 = new AlignmentGroup(this);
    alignmentGroup1.setName("1");

    AlignmentGroup alignmentGroup2 = new AlignmentGroup(this);
    alignmentGroup2.setName("2");

    AlignmentGroup alignmentGroup3 = new AlignmentGroup(this);
    alignmentGroup3.setName("3");

    addAlignmentGroup(alignmentGroup1);
    addAlignmentGroup(alignmentGroup2);
    addAlignmentGroup(alignmentGroup3);
  }

  /**
   * @author Bill Darbie
   */
  public void assignEmptyLeftAlignmentGroups()
  {
    AlignmentGroup leftAlignmentGroup1 = new AlignmentGroup(this);
    leftAlignmentGroup1.setName("4");

    AlignmentGroup leftAlignmentGroup2 = new AlignmentGroup(this);
    leftAlignmentGroup2.setName("5");

    AlignmentGroup leftAlignmentGroup3 = new AlignmentGroup(this);
    leftAlignmentGroup3.setName("6");

    addLeftAlignmentGroup(leftAlignmentGroup1);
    addLeftAlignmentGroup(leftAlignmentGroup2);
    addLeftAlignmentGroup(leftAlignmentGroup3);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isLeftAlignmentGroupsCreated()
  {
     return (_leftAlignmentGroups.isEmpty() == true) ? false : true;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isRightAlignmentGroupsCreated()
  {
     return (_rightAlignmentGroups.isEmpty() == true) ? false : true;
  }


  /**
   * @author Bill Darbie
   */
  public void assignAlignmentGroups()
  {
    // create empty right alignment groups
    assignEmptyRightAlignmentGroups();

    // create empty left alignment groups
    assignEmptyLeftAlignmentGroups();
  }

  /**
   * @author Bill Darbie
   */
  private void addPadToAlignmentGroup(AlignmentGroup alignmentGroup, Iterator<Pad> padIt, boolean choosingRightAlignmentGroups)
  {
    Assert.expect(alignmentGroup != null);
    Assert.expect(padIt != null);

    boolean done = false;

    List<Fiducial> fiducials = new LinkedList<Fiducial>();
    Project project = getPanel().getProject();

    while (done == false && padIt.hasNext())
    {
      Pad pad = padIt.next();
      JointTypeEnum jointTypeEnum = pad.getPackagePin().getJointTypeEnum();

      List<Pad> allPads = new LinkedList<Pad>();
      allPads.add(pad);

      // special case for Cap, Res and PCap
      // If one pad is selected, we automatically select the other JointTypeEnum padJointType = pad.getPadType().getPackagePin().getJointTypeEnum();
      if (jointTypeEnum.equals(JointTypeEnum.CAPACITOR) ||
          jointTypeEnum.equals(JointTypeEnum.POLARIZED_CAP) ||
          jointTypeEnum.equals(JointTypeEnum.RESISTOR)
          // Wei Chin (Tall Cap)
//          || jointTypeEnum.equals(JointTypeEnum.TALL_CAPACITOR)
          )
      {

        allPads = pad.getComponent().getPads();
        Assert.expect(allPads.size() == 2);
        padIt.next();
      }

      if (_programGeneration == null)
        _programGeneration = ProgramGeneration.getInstance();

      if (choosingRightAlignmentGroups && _programGeneration.isRightAlignmentRegionValid(project, allPads, fiducials) == false)
        continue;
      else if (choosingRightAlignmentGroups == false && _programGeneration.isLeftAlignmentRegionValid(project, allPads, fiducials) == false)
        continue;

      for (Pad alignmentPad : allPads)
        alignmentGroup.addPad(alignmentPad);

      done = true;
    }
  }

  /**
   * This is a wrapper for the addAlignmentGroup function.
   * It's added to make it easier to understand which side
   * of the panel you are adding an alignment group to. This
   * is only to be used when we have a long panel
   * @author George A. David
   */
  public void addRightAlignmentGroup(AlignmentGroup alignmentGroup)
  {
    Assert.expect(isLongPanel());

    addAlignmentGroup(alignmentGroup);
  }

  /**
   * This is a wrapper for the getAlignmentGroups function.
   * It's added to make it easier to understand which side
   * of the panel you are getting the  alignment groups from. This
   * is only to be used when we have a long panel
   * @author George A. David
   */
  public List<AlignmentGroup> getRightAlignmentGroupsForLongPanel()
  {
    Assert.expect(isLongPanel());
    Assert.expect(_rightAlignmentGroups != null);

    return _rightAlignmentGroups;
  }

  /**
   * @author George A. David
   */
  public boolean hasRightAlignmentGroups()
  {
    Assert.expect(isLongPanel());

    return hasAlignmentGroups();
  }

  /**
   * Add an alignment groupd to the left side of the panel
   * in a panel. Use only with long panels.
   * @author George A. David
   */
  public void addLeftAlignmentGroup(AlignmentGroup alignmentGroup)
  {
    Assert.expect(alignmentGroup != null);
    Assert.expect(_leftAlignmentGroups != null);

    _projectObservable.setEnabled(false);
    try
    {
      _leftAlignmentGroups.add(alignmentGroup);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, alignmentGroup, PanelSettingsEventEnum.ADD_ALIGNMENT_GROUP);
  }

  /**
   * Gets the alignment groups of the left side of the panel
   * in a long panel. Use only with long panels.
   * @author George A. David
   */
  public List<AlignmentGroup> getLeftAlignmentGroupsForLongPanel()
  {
    Assert.expect(_leftAlignmentGroups != null);
    Assert.expect(isLongPanel());

    return _leftAlignmentGroups;
  }

  /**
   * @author George A. David
   */
  public boolean hasLeftAlignmentGroups()
  {
    Assert.expect(isLongPanel());

    return ((_leftAlignmentGroups != null) && (_leftAlignmentGroups.isEmpty() == false));
  }

  /**
   * @author Bill Darbie
   */
  public List<Pad> getAllAlignmentPads()
  {
    List<Pad> alignmentPads = new ArrayList<Pad>();

    if(isPanelBasedAlignment())
    {
      for (AlignmentGroup alignmentGroup : _leftAlignmentGroups)
        alignmentPads.addAll(alignmentGroup.getPads());
      for (AlignmentGroup alignmentGroup : _rightAlignmentGroups)
        alignmentPads.addAll(alignmentGroup.getPads());
    }
    else
    {
      // Wei Chin added for individual alignment
      for(Board board : _panel.getBoards())
      {
        alignmentPads.addAll(board.getBoardSettings().getAllAlignmentPads());
      }
    }

    return alignmentPads;
  }

  /**
   * @author Bill Darbie
   * @author Wei Chin
   */
  public List<Fiducial> getAllAlignmentFiducials()
  {
    List<Fiducial> fiducials = new ArrayList<Fiducial>();
    if(isPanelBasedAlignment())
    {
      for (AlignmentGroup alignmentGroup : _leftAlignmentGroups)
        fiducials.addAll(alignmentGroup.getFiducials());
      for (AlignmentGroup alignmentGroup : _rightAlignmentGroups)
        fiducials.addAll(alignmentGroup.getFiducials());
    }
    else
    {
      // Wei Chin added for individual alignment
      for(Board board : _panel.getBoards())
      {
        fiducials.addAll(board.getBoardSettings().getAllAlignmentFiducials());
      }
    }

    return fiducials;
  }
  
  /*
   * @author Siew Yeng
   */
  public AlignmentGroup getAlignmentGroupFromPad(Pad padToLookFor)
  {
    AlignmentGroup alignmentGroup = null; 
    if(isPanelBasedAlignment())
    {
      for (AlignmentGroup ag : getAllAlignmentGroups())
      {
        for (Pad pad : ag.getPads())
        {
          if (pad == padToLookFor)
          {
            alignmentGroup = ag;
            return alignmentGroup;
          }
        }
      }
    }
    else
    {
      for(Board board : _panel.getBoards())
      {
        for (AlignmentGroup ag : board.getBoardSettings().getAllAlignmentGroups())
        {
          for (Pad pad : ag.getPads())
          {
            if (pad == padToLookFor)
            {
              alignmentGroup = ag;
              return alignmentGroup;
            }
          }
        }
      }
    }
    return alignmentGroup;
  }
  
  /**
   * @author George A. David
   */
  public boolean isLongPanel()
  {
    if (_panel.getLengthAfterAllRotationsInNanoMeters() > XrayTester.getMaxImageableAreaLengthInNanoMeters())
      return true;

    return false;
  }
  
  /**
   * XCR-3589
   * @author weng-jian.eoh
   * @param systemType
   * @return 
   */
  public boolean isLongPanel(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);
    SystemTypeEnum currentSystemType = XrayTester.getSystemType();
    boolean isLongPanel;
    try
    {
      XrayTester.changeSystemType(systemType);
      isLongPanel = _panel.getLengthAfterAllRotationsInNanoMeters() > XrayTester.getMaxImageableAreaLengthInNanoMeters();
    }
    finally
    {
      XrayTester.changeSystemType(currentSystemType);
    }
    
    return isLongPanel;
    
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isOpticalLongPanel()
  {
    if (_panel.getWidthAfterAllRotationsInNanoMeters() > XrayTester.getMaxOpticalImageableAreaWidthInNanoMeters())
      return true;
    
    return false;
  }

  /**
   * @author George A. David
   */
  boolean isPotentialLongProgramDeterminationValid()
  {
    return _isPotentialLongProgram != null && _panel.getProject().getProjectState().isPotentialLongProgramDeterminationValid();
  }

  /**
   * @author George A. David
   */
  private boolean isPotentialLongProgram()
  {
    if (isPotentialLongProgramDeterminationValid() == false)
    {
//      _projectObservable.setEnabled(false);
//      Boolean oldValue = _isPotentialLongProgram;
//      try
//      {
      SurroundingPadUtil padUtil = new SurroundingPadUtil();
      padUtil.setUseBoundsForDeterminingVicinity(true);
      padUtil.setPads(_panel.getPadsUnsorted());
      PanelRectangle leftPanel = getLeftSectionOfLongPanel();
      if (padUtil.getIntersectingPadsInRectangle(leftPanel).isEmpty())
      {
        _isPotentialLongProgram = new Boolean(false);
      }
      else
      {
        _isPotentialLongProgram = new Boolean(true);
      }
//      }
//      finally
//      {
//        _projectObservable.setEnabled(true);
//      }
//      _projectObservable.stateChanged(this,
//                                      oldValue,
//                                      _isPotentialLongProgram,
//                                      PanelSettingsEventEnum.POTENTIAL_LONG_PROGRAM_DETERMINED);
    }

    Assert.expect(_isPotentialLongProgram != null);
    return _isPotentialLongProgram.booleanValue();
  }

  /**
   * @author Cheah Lee Herng
   * @author Chong Wei Chin
   */
  public PanelRectangle getRightSectionOfLongPanel()
  {
    PanelRectangle rightSection = new PanelRectangle(_panel.getShapeInNanoMeters());
    if (isLongPanel())
    {
      if(hasCustomSplit() == false)
      {
        double yCoordinate = rightSection.getMinY();

        // Since it add 2 Verification Region Overlap from getAlignmentMaxImageableAreaLengthInNanoMeters() ,
  //       Y-offset need to minus 2 VR Region
        double yOffset = rightSection.getHeight() - getAlignmentMaxImageableAreaLengthInNanoMeters();

        if (yOffset > 0)
        {
          yCoordinate += yOffset;
        }

        rightSection.setRect(rightSection.getMinX(),
                            yCoordinate,
                            rightSection.getWidth(),
                            getAlignmentMaxImageableAreaLengthInNanoMeters());
      }
      else
      {
        double yCoordinate = rightSection.getMinY();

        // Since it add 2 Verification Region Overlap from getAlignmentMaxImageableAreaLengthInNanoMeters() ,
  //       Y-offset need to minus 2 VR Region
        double yOffset = rightSection.getHeight() - getSplitLocationInNanometer();

        if (yOffset > 0)
        {
          yCoordinate += yOffset;
        }

        rightSection.setRect(rightSection.getMinX(),
                            yCoordinate,
                            rightSection.getWidth(),
                            getSplitLocationInNanometer() + ReconstructionRegion.getMaxVerificationRegionLengthInNanoMeters());
      }
    }
    return rightSection;
  }
  
  /**
   * The maximum viewable area for Optical Camera 1 is defined in
   * XrayTester.getMaxOpticalImageableAreaWidthInNanoMeters(). So if the panel width
   * exceeds the limit, we need to divide the panel into 2 sections.
   * 
   * @author Cheah Lee Herng
   */
  public PanelRectangle getBottomSectionOfOpticalLongPanel()
  {
    PanelRectangle opticalBottomSection = new PanelRectangle(_panel.getShapeInNanoMeters());
    if (isOpticalLongPanel())
    {
        double xCoordinate = opticalBottomSection.getMaxX();
        double xOffset = XrayTester.getMaxOpticalImageableAreaWidthInNanoMeters();
        if (xOffset > 0)
            xCoordinate -= xOffset;
        
        opticalBottomSection.setRect(xCoordinate,
                                     opticalBottomSection.getMinY(),
                                     XrayTester.getMaxOpticalImageableAreaWidthInNanoMeters(),
                                     opticalBottomSection.getHeight());
    }
    return opticalBottomSection;
  }
  
  /**
   * The maximum viewable area for Optical Camera 1 is defined in
   * XrayTester.getMaxOpticalImageableAreaWidthInNanoMeters(). So if the panel width
   * exceeds the limit, we need to divide the panel into 2 sections and the rest of 
   * the area that Optical Camera 1 cannot cover will be the viewable area of Optical Camera 2.
   * 
   * @author Ying-Huan.Chu
   */
  public PanelRectangle getTopSectionOfOpticalLongPanel()
  {
    PanelRectangle opticalTopSection = new PanelRectangle(_panel.getShapeInNanoMeters());
    if (isOpticalLongPanel())
    {
      double xCoordinate = opticalTopSection.getMinX();
      double width = opticalTopSection.getWidth() - XrayTester.getMaxOpticalImageableAreaWidthInNanoMeters();
      opticalTopSection.setRect(xCoordinate,
                                   opticalTopSection.getMinY(),
                                   width,
                                   opticalTopSection.getHeight());
    }
    return opticalTopSection;
  }

  /**
   * @author George A. David
   */
  public PanelRectangle getRightAlignmentSectionOfLongPanel()
  {
    PanelRectangle rightSection = new PanelRectangle(_panel.getShapeInNanoMeters());
    if (isLongPanel())
    {
      double yCoordinate = rightSection.getMinY();

      double yOffset = rightSection.getHeight() - getAlignmentMaxImageableAreaLengthInNanoMeters();

      if (yOffset > 0)
        yCoordinate += yOffset;

      rightSection.setRect(rightSection.getMinX(),
                           yCoordinate,
                           rightSection.getWidth(),
                           getAlignmentMaxImageableAreaLengthInNanoMeters());
    }

    return rightSection;
  }

  /**
   *
   * @author Cheah Lee Herng
   * @author Chong Wei Chin
   */
  public PanelRectangle getLeftAlignmentSectionOfLongPanel()
  {
    Assert.expect(isLongPanel());
    PanelRectangle leftSection = new PanelRectangle(_panel.getShapeInNanoMeters());
    leftSection.setRect(leftSection.getMinX(),
                        leftSection.getMinY(),
                        leftSection.getWidth(),
                        getAlignmentMaxImageableAreaLengthInNanoMeters());
    
    return leftSection;
  }

  /**
   * @author George A. David
   */
  public PanelRectangle getLeftSectionOfLongPanel()
  {
    Assert.expect(isLongPanel(),"No second half panelSettings information available to retrieve.");
    PanelRectangle leftSection = new PanelRectangle(_panel.getShapeInNanoMeters());

    if( hasCustomSplit() )
    {
      int splitLocationInNanometer = getSplitLocationInNanometer();
    
      //Siew Yeng - XCR-2218 subtask XCR-2480
      int totalLeftAreaInspection;
      if(_panel.getProject().isEnlargeReconstructionRegion() && 
         (ReconstructionRegion.isDefaultMaxInspectionRegionWidth() == false || ReconstructionRegion.isDefaultMaxInspectionRegionLength() == false)) //Siew Yeng - XCR-3599
      {
        //Siew Yeng - XCR-2218
        totalLeftAreaInspection = leftSection.getHeight() - splitLocationInNanometer + ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters();
      }
      else
      {
        totalLeftAreaInspection = leftSection.getHeight() - splitLocationInNanometer + ReconstructionRegion.getMaxVerificationRegionLengthInNanoMeters();
      }
      
      leftSection.setRect( leftSection.getMinX(),
                           leftSection.getMinY(),
                           leftSection.getWidth(),
                           totalLeftAreaInspection);
    }
    else
    {
      leftSection.setRect( leftSection.getMinX(),
                         leftSection.getMinY(),
                         leftSection.getWidth(),
                         getAlignmentMaxImageableAreaLengthInNanoMeters());
    }

    return leftSection;
  }

  /**
   * @author Bill Darbie
   */
  public List<AlignmentGroup> getAllAlignmentGroups()
  {
    List<AlignmentGroup> alignmentGroups = new ArrayList<AlignmentGroup>();
    if (isLongPanel() == false)
    {
      alignmentGroups.addAll(getAlignmentGroupsForShortPanel());
    }
    else
    {
      alignmentGroups.addAll(getLeftAlignmentGroupsForLongPanel());
      alignmentGroups.addAll(getRightAlignmentGroupsForLongPanel());
    }

    return alignmentGroups;
  }

  /**
   * @author Bill Darbie
   * @author Wei Chin
   */
  public void removePadFromAlignmentGroupIfItIsUsed(Pad padToRemove)
  {
    Assert.expect(padToRemove != null);

    for (AlignmentGroup alignmentGroup : getAllAlignmentGroups())
    {
      for (Pad pad : alignmentGroup.getPads())
      {
        if (padToRemove == pad)
        {
          alignmentGroup.removePad(pad);
          break;
        }
      }
    }
  }

  /**
   * Iterates through every Subtype and makes sure that the Short learning status gets properly invalidated and deleted
   * based on project state changes (primarily region size threshold and joint type changes).
   *
   * @author Matt Wharton
   */
  public void syncShortLearningStatusForAllSubtypes() throws DatastoreException
  {
    ProjectState projectState = getPanel().getProject().getProjectState();

    Collection<Subtype> subtypesWithInvalidatedShortLearning = projectState.getShortLearningRecordsInFileInvalidSubtypes();
    projectState.clearShortLearningRecordsInFileInvalidSubtypes();

    for (Subtype subtype : subtypesWithInvalidatedShortLearning)
    {
      subtype.deleteLearnedJointSpecificData();
    }
  }

  /**
   * Iterates through every Subtype and makes sure that the Expected Image learning status gets properly invalidated and deleted
   * based on project state changes (primarily region size threshold and joint type changes).
   *
   * @author George Booth
   */
  public void syncExpectedImageLearningStatusForAllSubtypes() throws DatastoreException
  {
    ProjectState projectState = getPanel().getProject().getProjectState();

    Collection<Subtype> subtypesWithInvalidatedExpectedImageLearning = projectState.getExpectedImageLearningRecordsInFileInvalidSubtypes();
    projectState.clearExpectedImageLearningRecordsInFileInvalidSubtypes();

    for (Subtype subtype : subtypesWithInvalidatedExpectedImageLearning)
    {
      subtype.deleteLearnedExpectedImageData();
    }
  }
  
  /**
   * Iterates through every Subtype and makes sure that the Short learning status gets properly invalidated and deleted
   * based on project state changes (primarily region size threshold and joint type changes).
   *
   * @author Matt Wharton
   * @author Lim, Lay Ngor - Broken Pin
   */
  public void syncBrokenPinLearningStatusForAllSubtypes() throws DatastoreException
  {
    ProjectState projectState = getPanel().getProject().getProjectState();

    Collection<Subtype> subtypesWithInvalidatedBrokenPinLearning = projectState.getBrokenPinLearningRecordsInFileInvalidSubtypes();
    projectState.clearBrokenPinLearningRecordsInFileInvalidSubtypes();

    for (Subtype subtype : subtypesWithInvalidatedBrokenPinLearning)
    {
      subtype.deleteLearnedJointSpecificData();
    }
  }  

  /**
   * Clear Tombstone use.
   * @author sheng chuan
   */
  public void syncExpectedImageTemplateLearningStatusForAllSubtypes() throws DatastoreException
  {
    ProjectState projectState = getPanel().getProject().getProjectState();

    Collection<Subtype> subtypesWithInvalidatedExpectedImageTemplateLearning = projectState.getExpectedImageTemplateLearningRecordsInFileInvalidSubtypes();
    projectState.clearExpectedImageTemplateLearningRecoardsInFileInvalidSubtypes();

    for (Subtype subtype : subtypesWithInvalidatedExpectedImageTemplateLearning)
    {
      subtype.deleteLearnedExpectedImageTemplateData();
    }
  }

  /**
   * @author Bill Darbie
   */
  public void addUnfocusedRegionSliceToBeFixed(String regionName, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(regionName != null);
    Assert.expect(sliceNameEnum != null);

    Map<String, Set<SliceNameEnum>> oldValue = new HashMap<String, Set<SliceNameEnum>>(_unfocusedRegionSlicesToBeFixedMap);
    _projectObservable.setEnabled(false);
    try
    {
      Set<SliceNameEnum> sliceSet = _unfocusedRegionSlicesToBeFixedMap.get(regionName);
      if (sliceSet == null)
      {
        sliceSet = new HashSet<SliceNameEnum>();
        _unfocusedRegionSlicesToBeFixedMap.put(regionName, sliceSet);
      }
      sliceSet.add(sliceNameEnum);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _unfocusedRegionSlicesToBeFixedMap, PanelSettingsEventEnum.ADD_OR_REMOVE_UNFOCUSED_REGION_SLICE_TO_BE_FIXED);
  }

  /**
   * @author Bill Darbie
   */
  public void removeUnfocusedRegionSliceToBeFixed(String regionName, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(regionName != null);
    Assert.expect(sliceNameEnum != null);

    Map<String, Set<SliceNameEnum>> oldValue = new HashMap<String, Set<SliceNameEnum>>(_unfocusedRegionSlicesToBeFixedMap);
    _projectObservable.setEnabled(false);
    try
    {
      Set<SliceNameEnum> sliceSet = _unfocusedRegionSlicesToBeFixedMap.get(regionName);
      Assert.expect(sliceSet != null);

      sliceSet.remove(sliceNameEnum);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _unfocusedRegionSlicesToBeFixedMap, PanelSettingsEventEnum.ADD_OR_REMOVE_UNFOCUSED_REGION_SLICE_TO_BE_FIXED);
  }

  /**
   * @author Bill Darbie
   */
  public void removeUnfocusedRegionSliceToBeFixed(String regionName)
  {
    Assert.expect(regionName != null);

    Map<String, Set<SliceNameEnum>> oldValue = new HashMap<String, Set<SliceNameEnum>>(_unfocusedRegionSlicesToBeFixedMap);
    _projectObservable.setEnabled(false);
    try
    {
      Set<SliceNameEnum> sliceSet = _unfocusedRegionSlicesToBeFixedMap.remove(regionName);
      Assert.expect(sliceSet != null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _unfocusedRegionSlicesToBeFixedMap, PanelSettingsEventEnum.ADD_OR_REMOVE_UNFOCUSED_REGION_SLICE_TO_BE_FIXED);
  }

  /**
   * @author Bill Darbie
   */
  public void clearUnfocusedRegionSlicesToBeFixed()
  {
    Map<String, Set<SliceNameEnum>> oldValue = new HashMap<String, Set<SliceNameEnum>>(_unfocusedRegionSlicesToBeFixedMap);
    _projectObservable.setEnabled(false);
    try
    {
      _unfocusedRegionSlicesToBeFixedMap.clear();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _unfocusedRegionSlicesToBeFixedMap, PanelSettingsEventEnum.ADD_OR_REMOVE_UNFOCUSED_REGION_SLICE_TO_BE_FIXED);
  }

  /**
   * @author George A. David
   */
  public boolean hasUnfocusedRegionSlicesToBeFixed(String regionName)
  {
    Assert.expect(regionName != null);
    return _unfocusedRegionSlicesToBeFixedMap.containsKey(regionName);
  }

  /**
   * @author George A. David
   */
  public boolean hasUnfocusedRegionSlicesToBeFixed(String regionName, SliceNameEnum sliceName)
  {
    Assert.expect(regionName != null);
    Assert.expect(sliceName != null);

    if (_unfocusedRegionSlicesToBeFixedMap.containsKey(regionName) && _unfocusedRegionSlicesToBeFixedMap.get(regionName).contains(sliceName))
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public Map<String, Set<SliceNameEnum>> getUnfocusedRegionSlicesToBeFixed()
  {
    return new TreeMap<String, Set<SliceNameEnum>>(_unfocusedRegionSlicesToBeFixedMap);
  }

  /**
   * @author Bill Darbie
   */
  public void addUnfocusedRegionSlicesToZoffsetInNanos(String regionName, SliceNameEnum sliceNameEnum, int zOffsetInNanos)
  {
    Assert.expect(regionName != null);
    Assert.expect(sliceNameEnum != null);

    Map<String, Set<Pair<SliceNameEnum, Integer>>> oldValue = new HashMap<String, Set<Pair<SliceNameEnum, Integer>>>(_unfocusedRegionSlicesToZoffsetInNanosMap);
    _projectObservable.setEnabled(false);
    try
    {
      Set<Pair<SliceNameEnum, Integer>> sliceNameIntegerPairSet = _unfocusedRegionSlicesToZoffsetInNanosMap.get(regionName);
      if (sliceNameIntegerPairSet == null)
        sliceNameIntegerPairSet = new HashSet<Pair<SliceNameEnum, Integer>>();

      // check to see if the current sliceNameEnum is already in the sliceNameIntegerPairSet
      Pair<SliceNameEnum, Integer> foundPair = null;
      for (Pair<SliceNameEnum, Integer> pair : sliceNameIntegerPairSet)
      {
        if (pair.getFirst().equals(sliceNameEnum))
        {
          foundPair = pair;
          break;
        }
      }

      if (foundPair != null)
      {
        // a pair already exists for this sliceNameEnum, so just change it's z offset
        foundPair.setSecond(zOffsetInNanos);
      }
      else
      {
        // a pair does not already exists, so add a new sliceNameEnum, zOffset pair
        sliceNameIntegerPairSet.add(new Pair<SliceNameEnum, Integer>(sliceNameEnum, zOffsetInNanos));
      }
      _unfocusedRegionSlicesToZoffsetInNanosMap.put(regionName, sliceNameIntegerPairSet);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _unfocusedRegionSlicesToZoffsetInNanosMap, PanelSettingsEventEnum.ADD_OR_REMOVE_UNFOCUSED_REGION_SLICE_TO_Z_OFFSET);
  }

  /**
   * @author Bill Darbie
   */
  public void removeUnfocusedRegionSlicesToZoffsetInNanos(String regionName, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(regionName != null);
    Assert.expect(sliceNameEnum != null);

    Map<String, Set<Pair<SliceNameEnum, Integer>>> oldValue = new HashMap<String, Set<Pair<SliceNameEnum, Integer>>>(_unfocusedRegionSlicesToZoffsetInNanosMap);
    _projectObservable.setEnabled(false);
    try
    {
      Set<Pair<SliceNameEnum, Integer>> sliceNameIntegerPairSet = _unfocusedRegionSlicesToZoffsetInNanosMap.get(regionName);
      Assert.expect(sliceNameIntegerPairSet != null);

      boolean removed = false;
      for (Pair<SliceNameEnum, Integer> pair : sliceNameIntegerPairSet)
      {
        if (pair.getFirst().equals(sliceNameEnum))
        {
          sliceNameIntegerPairSet.remove(pair);
          removed = true;
          break;
        }
      }
      Assert.expect(removed);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _unfocusedRegionSlicesToZoffsetInNanosMap, PanelSettingsEventEnum.ADD_OR_REMOVE_UNFOCUSED_REGION_SLICE_TO_Z_OFFSET);
  }

  /**
   * @author Bill Darbie
   */
  public void removeUnfocusedRegionSlicesToZoffsetInNanos(String regionName)
  {
    Assert.expect(regionName != null);

    Map<String, Set<Pair<SliceNameEnum, Integer>>> oldValue = new HashMap<String, Set<Pair<SliceNameEnum, Integer>>>(_unfocusedRegionSlicesToZoffsetInNanosMap);
    _projectObservable.setEnabled(false);
    try
    {
      Set<Pair<SliceNameEnum, Integer>> sliceNameIntegerPairSet = _unfocusedRegionSlicesToZoffsetInNanosMap.remove(regionName);
      Assert.expect(sliceNameIntegerPairSet != null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _unfocusedRegionSlicesToZoffsetInNanosMap, PanelSettingsEventEnum.ADD_OR_REMOVE_UNFOCUSED_REGION_SLICE_TO_Z_OFFSET);
  }

  /**
   * @author Bill Darbie
   */
  public void clearUnfocusedRegionSlicesToZoffsetInNanos()
  {
    Map<String, Set<Pair<SliceNameEnum, Integer>>> oldValue = new HashMap<String, Set<Pair<SliceNameEnum, Integer>>>(_unfocusedRegionSlicesToZoffsetInNanosMap);
    _projectObservable.setEnabled(false);
    try
    {
      _unfocusedRegionSlicesToZoffsetInNanosMap.clear();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _unfocusedRegionSlicesToZoffsetInNanosMap, PanelSettingsEventEnum.ADD_OR_REMOVE_UNFOCUSED_REGION_SLICE_TO_Z_OFFSET);
  }

  /**
   * @author George A. David
   */
  public boolean hasUnfocusedRegionSlicesToZoffsetInNanos(String regionName)
  {
    Assert.expect(regionName != null);
    return _unfocusedRegionSlicesToZoffsetInNanosMap.containsKey(regionName);
  }

  /**
   * @author George A. David
   */
  public boolean hasUnfocusedRegionSlicesToZoffsetInNanos(String regionName, SliceNameEnum sliceName)
  {
    Assert.expect(regionName != null);
    Assert.expect(sliceName != null);

    if (_unfocusedRegionSlicesToZoffsetInNanosMap.containsKey(regionName))
    {
      Set<Pair<SliceNameEnum, Integer>> pairs = _unfocusedRegionSlicesToZoffsetInNanosMap.get(regionName);
      for(Pair<SliceNameEnum, Integer> pair : pairs)
      {
        if(sliceName.equals(pair.getFirst()))
          return true;
      }
    }

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public Map<String, Set<Pair<SliceNameEnum, Integer>>> getUnfocusedRegionSlicesToZoffsetInNanos()
  {
    return new TreeMap<String, Set<Pair<SliceNameEnum, Integer>>>(_unfocusedRegionSlicesToZoffsetInNanosMap);
  }

  /**
   * @author Bill Darbie
   */
  public void addUnfocusedRegionNameToRetest(String regionName)
  {
    Assert.expect(regionName != null);

    Set<String> oldValue = new HashSet<String>(_unfocusedRegionNamesToRetestSet);
    _projectObservable.setEnabled(false);
    try
    {
      boolean added = _unfocusedRegionNamesToRetestSet.add(regionName);
      // it is okay if the same region is added more than once
      // so do not assert on this
      // it is okay if the same region is added more than once
      // so do not assert on this
      //Assert.expect(added);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _unfocusedRegionNamesToRetestSet, PanelSettingsEventEnum.ADD_OR_REMOVE_UNFOCUSED_REGION_NAME_TO_RETEST);
  }

  /**
   * @author Bill Darbie
   */
  public void removeUnfocusedRegionNameToRetest(String regionName)
  {
    Assert.expect(regionName != null);

    Set<String> oldValue = new HashSet<String>(_unfocusedRegionNamesToRetestSet);
    _projectObservable.setEnabled(false);
    try
    {
      boolean removed = _unfocusedRegionNamesToRetestSet.remove(regionName);
      Assert.expect(removed);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _unfocusedRegionNamesToRetestSet, PanelSettingsEventEnum.ADD_OR_REMOVE_UNFOCUSED_REGION_NAME_TO_RETEST);
  }

  /**
   * @author Bill Darbie
   */
  public void clearUnfocusedRegionNamesToRetest()
  {
    Set<String> oldValue = new HashSet<String>(_unfocusedRegionNamesToRetestSet);
    _projectObservable.setEnabled(false);
    try
    {
      _unfocusedRegionNamesToRetestSet.clear();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _unfocusedRegionNamesToRetestSet, PanelSettingsEventEnum.ADD_OR_REMOVE_UNFOCUSED_REGION_NAME_TO_RETEST);
  }

  /**
   * @author George A. David
   */
  public boolean hasUnfocusedRegionNameToRetest(String regionName)
  {
    Assert.expect(regionName != null);
    return _unfocusedRegionNamesToRetestSet.contains(regionName);
  }

  /**
   * @author Bill Darbie
   */
  public Set<String> getUnfocusedRegionsNamesToRetest()
  {
    return new TreeSet<String>(_unfocusedRegionNamesToRetestSet);
  }

  /**
   * @author Bill Darbie
   */
  public void addNoTestRegionName(String regionName, List<PadType> padTypes)
  {
    Assert.expect(regionName != null);

    Map<String, Set<PadType>> oldValue = new HashMap<String, Set<PadType>>(_noTestRegionsNameToPadTypeMap);
    _projectObservable.setEnabled(false);
    try
    {
      Set<PadType> padTypeSet = new HashSet<PadType>();
      for (PadType padType : padTypes)
      {
        if (padType.isInspected())
        {
          padTypeSet.add(padType);
          padType.getPadTypeSettings().setInspected(false);
        }
      }

      _noTestRegionsNameToPadTypeMap.put(regionName, padTypeSet);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _noTestRegionsNameToPadTypeMap, PanelSettingsEventEnum.ADD_OR_REMOVE_NO_TEST_REGION_NAME);
  }

  /**
   * @author Bill Darbie
   */
  public void addNoTestRegionNameForProjectReader(String regionName, List<PadType> padTypes)
  {
    Assert.expect(regionName != null);

    Map<String, Set<PadType>> oldValue = new HashMap<String, Set<PadType>>(_noTestRegionsNameToPadTypeMap);
    _projectObservable.setEnabled(false);
    try
    {
      Set<PadType> padTypeSet = new HashSet<PadType>();
      for (PadType padType : padTypes)
      {
        padTypeSet.add(padType);
      }

      _noTestRegionsNameToPadTypeMap.put(regionName, padTypeSet);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _noTestRegionsNameToPadTypeMap, PanelSettingsEventEnum.ADD_OR_REMOVE_NO_TEST_REGION_NAME);
  }

  /**
   * @author Bill Darbie
   */
  public void removeNoTestRegionName(String regionName)
  {
    Assert.expect(regionName != null);

    Map<String, Set<PadType>> oldValue = new HashMap<String, Set<PadType>>(_noTestRegionsNameToPadTypeMap);
    _projectObservable.setEnabled(false);
    try
    {
      Set<PadType> padTypeSet = _noTestRegionsNameToPadTypeMap.get(regionName);
      for (PadType padType : padTypeSet)
      {
        padType.getPadTypeSettings().setInspected(true);
      }
      Set<PadType> prev = _noTestRegionsNameToPadTypeMap.remove(regionName);
      Assert.expect(prev != null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _noTestRegionsNameToPadTypeMap, PanelSettingsEventEnum.ADD_OR_REMOVE_NO_TEST_REGION_NAME);
  }

  /**
   * @author Bill Darbie
   */
  public void clearNoTestRegionNames()
  {
    Map<String, Set<PadType>> oldValue = new HashMap<String, Set<PadType>>(_noTestRegionsNameToPadTypeMap);
    _projectObservable.setEnabled(false);
    try
    {
      for (String regionName : _noTestRegionsNameToPadTypeMap.keySet())
      {
        removeNoTestRegionName(regionName);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _noTestRegionsNameToPadTypeMap, PanelSettingsEventEnum.ADD_OR_REMOVE_NO_TEST_REGION_NAME);
  }

  /**
   * @author George A. David
   */
  public boolean hasNoTestRegionName(String regionName)
  {
    Assert.expect(regionName != null);

    return _noTestRegionsNameToPadTypeMap.containsKey(regionName);
  }

  /**
   * @author Bill Darbie
   */
  public Set<String> getNoTestRegionsNames()
  {
    return new TreeSet<String>(_noTestRegionsNameToPadTypeMap.keySet());
  }

  /**
   * @author Bill Darbie
   */
  public List<PadType> getNoTestPadTypes(String regionName)
  {
    Assert.expect(regionName != null);

    Set<PadType> padTypeSet = new TreeSet<PadType>(new PadTypeComparator());
    padTypeSet.addAll(_noTestRegionsNameToPadTypeMap.get(regionName));
    List<PadType> padTypes = new ArrayList<PadType>(padTypeSet);

    return padTypes;
  }

  /**
   * Assert if two alignment groups have the same name.
   * @author Bill Darbie
   */
  void checkForDuplicateAlignmentGroupNames()
  {
    Set<String> nameSet = new HashSet<String>();
    for (AlignmentGroup group : _leftAlignmentGroups)
    {
      if (group.hasName())
      {
        boolean added = nameSet.add(group.getName());
        Assert.expect(added);
      }
    }

    for (AlignmentGroup group : _rightAlignmentGroups)
    {
      if (group.hasName())
      {
        boolean added = nameSet.add(group.getName());
        Assert.expect(added);
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  public void setUserWasWarnedOfThicknessImportance()
  {
    _userWarnedOfThicknessImportance = true;
  }

  /**
   * @author Bill Darbie
   */
  public boolean wasUserWarnedOfThicknessImportance()
  {
    return _userWarnedOfThicknessImportance;
  }

  /**
   * For a given panel take all joints and attempt to set the artifact compesation level to COMPENSATE
   * @author Erica Wheatcroft
   */
  public void enableArtifactCompensationForPanel()
  {
    _projectObservable.setEnabled(false);

    for (PadType padType : _panel.getPadTypes())
    {
      padType.getPadTypeSettings().setArtifactCompensationState(ArtifactCompensationStateEnum.COMPENSATED);
    }

    _projectObservable.setEnabled(true);
    _projectObservable.stateChanged(this, null, null, PanelSettingsEventEnum.ARTIFACT_COMPENSATION_STATE);

  }

  /**
   * For a given panel take all joints and attempt to set the artifact compesation level to the default state
   * @author Erica Wheatcroft
   */
  public void disableArtifactCompensationForPanel()
  {
    _projectObservable.setEnabled(false);

    for (PadType padType : _panel.getPadTypes())
    {
      padType.getPadTypeSettings().setArtifactCompensationState(ArtifactCompensationStateEnum.NOT_COMPENSATED);
    }

    _projectObservable.setEnabled(true);
    _projectObservable.stateChanged(this, null, null, PanelSettingsEventEnum.ARTIFACT_COMPENSATION_STATE);

  }

  /**
   * Previously for long panel, we divide the panel based on value from XrayTester.getMaxImageableAreaLengthInNanoMeters().
   * Now, for long panel, we divide it half the length of actual after rotated panel length. For new NPI system, AI (Alignment + Inspection)
   * scan pass is combined and will only be scanned once.
   *
   * @author Cheah Lee Herng
   * @author Chong Wei Chin
   */
  public int getAlignmentMaxImageableAreaLengthInNanoMeters()
  {
    Assert.expect(_panel != null);

    if(isLongPanel())
    {
      if(hasCustomSplit() == false)
      {
        return minimumLongPanelAlignmentRegion();
      }
      else
      {
        //Siew Yeng - XCR-2218 subtask XCR-2480
        if(_panel.getProject().isEnlargeReconstructionRegion() && 
           (ReconstructionRegion.isDefaultMaxInspectionRegionWidth() == false || ReconstructionRegion.isDefaultMaxInspectionRegionLength() == false))//XCR-3599
        {
          //Siew Yeng - XCR-2218
          return getSplitLocationInNanometer() + ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters();
        }
        else
        {
          return getSplitLocationInNanometer() + ReconstructionRegion.getMaxVerificationRegionLengthInNanoMeters();
        }
      }
    }
    else
    {
      return _panel.getLengthAfterAllRotationsInNanoMeters();
    }
  }

  /**
   * @return the _isPanelBasedAlignment
   * @author Wei Chin
   */
  public boolean isPanelBasedAlignment()
  {
    return _isPanelBasedAlignment;
  }

  /**
   * @param isPanelBasedAlignment the _isPanelBasedAlignment to set
   * @author Wei Chin
   */
  public void setIsPanelBasedAlignment(boolean isPanelBasedAlignment)
  {
    _projectObservable.setEnabled(false);
    try
    {
      _isPanelBasedAlignment = isPanelBasedAlignment;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, !isPanelBasedAlignment, isPanelBasedAlignment, PanelSettingsEventEnum.ALIGNMENT_METHOD);
  }

  /**
   * @author Cheah, Lee Herng
   */
  public void setUseAutoFocusForPanel()
  {
    _projectObservable.setEnabled(false);

    for (PadType padType : _panel.getPadTypes())
    {
      padType.getPadTypeSettings().setGlobalSurfaceModel(GlobalSurfaceModelEnum.AUTOFOCUS);
    }

    _projectObservable.setEnabled(true);
    _projectObservable.stateChanged(this, null, null, PanelSettingsEventEnum.GLOBAL_SURFACE_MODEL_STATE);

  }

  /**
   * @author Cheah, Lee Herng
   */
  public void setUseGSMForPanel()
  {
    _projectObservable.setEnabled(false);

    for (PadType padType : _panel.getPadTypes())
    {
      padType.getPadTypeSettings().setGlobalSurfaceModel(GlobalSurfaceModelEnum.GLOBAL_SURFACE_MODEL);
    }

    _projectObservable.setEnabled(true);
    _projectObservable.stateChanged(this, null, null, PanelSettingsEventEnum.GLOBAL_SURFACE_MODEL_STATE);

  }

  /**
   * @author Cheah, Lee Herng
   */
  public void setUseAutoSelectForPanel()
  {
    _projectObservable.setEnabled(false);

    for (PadType padType : _panel.getPadTypes())
    {
      padType.getPadTypeSettings().setGlobalSurfaceModel(GlobalSurfaceModelEnum.AUTOSELECT);
    }

    _projectObservable.setEnabled(true);
    _projectObservable.stateChanged(this, null, null, PanelSettingsEventEnum.GLOBAL_SURFACE_MODEL_STATE);

  }

  /*
   * @author sham
   */
  public boolean isAllComponentSameUserGain()
  {
    if (_isAllComponentSameUserGain == -1)
    {
      UserGainEnum c1 = null;
      for(Subtype subtype : _panel.getSubtypes())
      {
        if (c1 == null)
        {
          c1 = subtype.getSubtypeAdvanceSettings().getUserGain();
        }
        if(subtype.getSubtypeAdvanceSettings().getUserGain().equals(c1) == false)
        {
          _isAllComponentSameUserGain = 0;
            return (_isAllComponentSameUserGain == 1);
        }
      }
      if (c1 != null)
      {
        setPanelBaseUserGain(c1);
      }
      _isAllComponentSameUserGain = 1;      
    }     
    return (_isAllComponentSameUserGain == 1);
  }

  /**
   * @author sham
   */
  public UserGainEnum getPanelBaseUserGain()
  {
    Assert.expect(_panelBaseUserGain != null);

    return _panelBaseUserGain;
  }

  /**
   * @author sham
   */
  public void setPanelBaseUserGain(UserGainEnum panelBaseUserGain)
  {
    Assert.expect(panelBaseUserGain != null);

    _panelBaseUserGain = panelBaseUserGain;
  }
  
  /*
   * @author sheng chuan
   */
  public boolean isAllComponentSameStageSpeed()
  {
    if (_isAllComponentSameStageSpeed == -1)
    {
      List<StageSpeedEnum> c1 = null;
      for(Subtype subtype : _panel.getSubtypes())
      {
        if (c1 == null)
        {
          c1 = subtype.getSubtypeAdvanceSettings().getStageSpeedList();
        }
        if(subtype.getSubtypeAdvanceSettings().getStageSpeedList().size() != c1.size() || 
           subtype.getSubtypeAdvanceSettings().getStageSpeedList().containsAll(c1) == false)
        {
          _isAllComponentSameStageSpeed = 0;
            return (_isAllComponentSameStageSpeed == 1);
        }
      }
      if (c1 != null)
      {
        setPanelBaseStageSpeed(c1);
      }
      _isAllComponentSameStageSpeed = 1;      
    }     
    return (_isAllComponentSameStageSpeed == 1);
  }

  /**
   * @author sheng chuan
   */
  public StageSpeedEnum getPanelBaseStageSpeed()
  {
    Assert.expect(_panelBasStageSpeed != null);

    if(_panelBasStageSpeed.isEmpty())
      return StageSpeedEnum.ONE;
    else
      return _panelBasStageSpeed.iterator().next();
  }

  /**
   * @author sheng chuan
   */
  public void setPanelBaseStageSpeed(List<StageSpeedEnum> stageSpeedEnum)
  {
    Assert.expect(stageSpeedEnum != null);

    _panelBasStageSpeed = stageSpeedEnum;
  }

  /**
   * @author sham
   */
  public void resetIsAllComponentSameUserGainSetting()
  {
    _isAllComponentSameUserGain = -1;
  }
  
  /**
   * @author sheng chuan
   */
  public void resetIsAllComponentSameUserStageSpeed()
  {
    _isAllComponentSameStageSpeed = -1;
  }
  
  /*
   * void
   * @author Kee Chin Seong
   */
  public void resetLowAndHighMagSettings()
  {
    _hasComponentUsingLowMag = -1;
    _haslComponentUsingHighMag = -1;
  }
  
  /**
   * @return 
   * @author Chong, Wei Chin
   */
  public boolean hasHighMagnificationComponent()
  {
    for (Subtype subtype : _panel.getUsedSubtypes())
    {
      if (subtype.getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
      {
        for (Pad pad : subtype.getPads())
        {
          // Bee Hoon 19-Apr-2013
          // pad.getPadTypeSettings().isTestable() & 
          // pad.getComponent().getComponentTypeSettings().isTestable() are not used
          // and are always true. DO NOT USE this 2 function for checking.
          if (pad.getPadTypeSettings().isInspected())
          {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  /**
   * @return 
   * @author Chong, Wei Chin
   * @author Kee, Chin Seong
   */
  public boolean hasLowMagnificationComponent()
  {
    for (Subtype subtype : _panel.getUsedSubtypes())
    {
      if (subtype.getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.LOW))
      {
        for (Pad pad : subtype.getPads())
        {
            // Bee Hoon 19-Apr-2013
          // pad.getPadTypeSettings().isTestable() & 
          // pad.getComponent().getComponentTypeSettings().isTestable() are not used
          // and are always true. DO NOT USE this 2 function for checking.
          if (pad.getPadTypeSettings().isInspected())
          {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  /**
   * @author Chong, Wei Chin
   * @Edited By Kee Chin Seong - Manual Alignment now should checked is LONG or SHORT 
   *                             Panel with LONG or SHORT Program
   * @Edited By Phang Siew Yeng - XCR1638 - Add checking for long panel, if does not have testSubProgram, 
   *                                        alignment is not needed.  
   *                            - call isManualAlignmentCompleted() to check alignment COMPLETE
   */
  public boolean hasManualAlignmentBeenPerformedForHighMag()
  {
    Assert.expect(_panel != null);

    if(isPanelBasedAlignment())
    {
      if(isLongPanel())
      {
        if(_panel.getProject().getTestProgram().isLongProgram())
        {
//          if (leftManualAlignmentTransformForHighMagExists() == false ||
//              rightManualAlignmentTransformForHighMagExists() == false)
          if((leftManualAlignmentTransformForHighMagExists() == false &&
              _panel.getProject().getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.HIGH)) ||
              (rightManualAlignmentTransformForHighMagExists() == false &&
              _panel.getProject().getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.HIGH)))
          {
            return false;
          }
        } 
        else
        {
          if(rightManualAlignmentTransformForHighMagExists() == false)
          {
            return false;
          }
        }
      }
      else
      {
        if (rightManualAlignmentTransformForHighMagExists() == false)
        {
          return false;
        }   
      }
//      for(TestSubProgram subProgram: _panel.getProject().getTestProgram().getAllHighMagTestSubPrograms())
//      {
//        if(subProgram.isSubProgramPerformAlignment())
//        {
//          if(subProgram.isAllRegionNotIspectable()==false)
//          {
//            if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
//            {
//              if (leftManualAlignmentTransformForHighMagExists() == false)
//              {
//                return false;
//              }
//            }
//            else
//            {
//              if (rightManualAlignmentTransformForHighMagExists() == false)
//              {
//                return false;
//              }
//            }
//          }
//        }
//      }
    }
    else
    {
      for(Board board : _panel.getBoards())
      {
        if(board.isLongBoard())
        {
//          if(board.getBoardSettings().leftManualAlignmentTransformForHighMagExists() == false ||
//             board.getBoardSettings().rightManualAlignmentTransformForHighMagExists() == false)
          if((board.getBoardSettings().leftManualAlignmentTransformForHighMagExists() == false &&
              _panel.getProject().getTestProgram().hasTestSubProgram(board, PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.HIGH))|| 
             (board.getBoardSettings().rightManualAlignmentTransformForHighMagExists() == false &&
              _panel.getProject().getTestProgram().hasTestSubProgram(board, PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.HIGH)))
          {
            return false;
          }
        }
        else
        {
          if(board.getBoardSettings().leftManualAlignmentTransformForHighMagExists() == false && 
             board.getBoardSettings().rightManualAlignmentTransformForHighMagExists() == false)
          {
            return false;
          }
        }
      }
//      for(TestSubProgram subProgram: _panel.getProject().getTestProgram().getAllHighMagTestSubPrograms())
//      {
//        if(subProgram.isSubProgramPerformAlignment())
//        {
//          if(subProgram.isAllRegionNotIspectable()==false)
//          {
//            if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
//            {
//              if (subProgram.getBoard().getBoardSettings().leftManualAlignmentTransformForHighMagExists() == false)
//              {
//                return false;
//              }
//            }
//            else
//            {
//              if (subProgram.getBoard().getBoardSettings().rightManualAlignmentTransformForHighMagExists() == false)
//              {
//                return false;
//              }
//            }
//          }
//        }
    }
    
    
    return true;
  }

  /**
   * Set the manual alignment transform for this long panel when it is
   * positioned at the left panel-in-place sensor.
   *
   * @author Chong, Wei Chin
   */
  public void setLeftManualAlignmentTransformForHighMag(AffineTransform leftManualAlignmentTransform)
  {
    Assert.expect(leftManualAlignmentTransform != null);
    Assert.expect(isLongPanel());

    if (leftManualAlignmentTransform == _leftManualAlignmentTransformForHighMag)
      return;

    Pair<AffineTransform, AffineTransform> oldValue =
      new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransformForHighMag, _rightManualAlignmentTransformForHighMag);
    _projectObservable.setEnabled(false);
    try
    {
      _leftManualAlignmentTransformForHighMag = leftManualAlignmentTransform;
      //setLeftLastSavedRuntimeAlignmentTransformForHighMag(leftManualAlignmentTransform);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }

    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransformForHighMag, _rightManualAlignmentTransformForHighMag),
                                    PanelSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_GENERATED);
  }

  /**
   * Set the runtime alignment transform for this long panel when it is
   * positioned at the left panel-in-place sensor.
   * @author Chong, Wei Chin
   */
  public void setLeftRuntimeAlignmentTransformForHighMag(AffineTransform leftRuntimeAlignmentTransform)
  {
    Assert.expect(leftRuntimeAlignmentTransform != null);
    Assert.expect(isLongPanel());

    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _leftRuntimeAlignmentTransformForHighMag = leftRuntimeAlignmentTransform;
  }
  
  /**
  * Author Kee Chin Seong
  *
  */
  public void setLeftLastSavedRuntimeAlignmentTransformForHighMag(AffineTransform leftRuntimeAlignmentTransform)
  {
    Assert.expect(leftRuntimeAlignmentTransform != null);
    Assert.expect(isLongPanel());

    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _leftLastSavedViewCadRuntimeAlignmentTransformForHighMag = leftRuntimeAlignmentTransform;
  }
  
  /**
   * Set the manual alignment transform for this long panel when it is
   * positioned at the right panel-in-place sensor.
   *
   * @author Chong, Wei Chin
   */
  public void setRightManualAlignmentTransformForHighMag(AffineTransform rightManualAlignmentTransform)
  {
    Assert.expect(rightManualAlignmentTransform != null);

    if (_rightManualAlignmentTransformForHighMag == rightManualAlignmentTransform)
      return;

    Pair<AffineTransform, AffineTransform> oldValue =
        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransformForHighMag, _rightManualAlignmentTransformForHighMag);
    _projectObservable.setEnabled(false);
    try
    {
      _rightManualAlignmentTransformForHighMag = rightManualAlignmentTransform;
      setRightLastSavedViewCadRuntimeAlignmentTransformForHighMag(rightManualAlignmentTransform);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransformForHighMag, _rightManualAlignmentTransformForHighMag),
                                    PanelSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_GENERATED);
  }

  /**
   * Set the runtime alignment transform for this long panel when it is
   * positioned at the right panel-in-place sensor.
   *
   * @author Chong, Wei Chin
   */
  public void setRightRuntimeAlignmentTransformForHighMag(AffineTransform rightRuntimeAlignmentTransform)
  {
    Assert.expect(rightRuntimeAlignmentTransform != null);

    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _rightRuntimeAlignmentTransformForHighMag = rightRuntimeAlignmentTransform;
  }
  
  /**
   * @author Kee Chin Seong - Separating out the AFfine Transform from View CAD
   * @param rightRuntimeAlignmentTransform 
   */
  public void setRightLastSavedViewCadRuntimeAlignmentTransformForHighMag(AffineTransform rightRuntimeAlignmentTransform)
  {
    Assert.expect(rightRuntimeAlignmentTransform != null);

    // Intentionally NOT firing a project change event.  Setting the runtime transform is not something that
    // needs to be tracked.
    _rightLastSavedViewCadRuntimeAlignmentTransformForHighMag = rightRuntimeAlignmentTransform;
  }

  /**
   * Sets the manual and runtime left and right alignment transforms back to null and marks alignment as invalid.
   *
   * @author Chong, Wei Chin
   */
  public void clearAllAlignmentTransformsForHighMag()
  {
    Pair<AffineTransform, AffineTransform> oldValue =
        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransformForHighMag, _rightManualAlignmentTransformForHighMag);
    _projectObservable.setEnabled(false);
    try
    {
      _leftManualAlignmentTransformForHighMag = null;
      _rightManualAlignmentTransformForHighMag = null;
      _leftRuntimeAlignmentTransformForHighMag = null;
      _rightRuntimeAlignmentTransformForHighMag = null;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(null, null),
                                    PanelSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED);
  }

  /**
   * Get the manual alignment AffineTransform for this panel when
   * it is positioned against the left panel-in-place sensor.
   * @author Chong, Wei Chin
   */
  public AffineTransform getLeftManualAlignmentTransformForHighMag()
  {
    Assert.expect(_leftManualAlignmentTransformForHighMag != null);
    Assert.expect(isLongPanel());
    Assert.expect(getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    return _leftManualAlignmentTransformForHighMag;
  }

  /**
   * Get the runtime alignment AffineTransform for this panel when
   * it is positioned against the left panel-in-place sensor.

   * @author Chong Wei Chin
   */
  public AffineTransform getLeftRuntimeAlignmentTransformForHighMag()
  {
    Assert.expect(_leftManualAlignmentTransformForHighMag != null);
    Assert.expect(isLongPanel());
    Assert.expect(getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    if (_leftRuntimeAlignmentTransformForHighMag == null)
    {
      _leftRuntimeAlignmentTransformForHighMag = new AffineTransform(_leftManualAlignmentTransformForHighMag);
    }

    return _leftRuntimeAlignmentTransformForHighMag;
  }

  /**
  *
  *@author Kee Chin Seong
  **/
  public AffineTransform getLeftLastSavedRuntimeAlignmentTransformForHighMag()
  {
    Assert.expect(_leftManualAlignmentTransformForHighMag != null);
    Assert.expect(isLongPanel());
    Assert.expect(getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    if (_leftLastSavedViewCadRuntimeAlignmentTransformForHighMag == null)
    {
      _leftLastSavedViewCadRuntimeAlignmentTransformForHighMag = new AffineTransform(_leftManualAlignmentTransformForHighMag);
    }

    return _leftLastSavedViewCadRuntimeAlignmentTransformForHighMag;
  }
  
  /**
   * @author Kee Chin Seong - Separating out the AFfine Transform from View CAD
   * @return 
   */
  public AffineTransform getRightLastSavedViewCadRuntimeAlignmentTransformForHighMag()
  {
    Assert.expect(_rightManualAlignmentTransformForHighMag != null);
    Assert.expect(getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    if (_rightLastSavedViewCadRuntimeAlignmentTransformForHighMag == null)
    {
      _rightLastSavedViewCadRuntimeAlignmentTransformForHighMag = new AffineTransform(_rightManualAlignmentTransformForHighMag);
    }

    return _rightLastSavedViewCadRuntimeAlignmentTransformForHighMag;
  }
  
  /**
   * Returns true if the left manual alignment transform exists.
   *
   * @author Chong, Wei Chin
   */
  public boolean leftManualAlignmentTransformForHighMagExists()
  {
    if ((_leftManualAlignmentTransformForHighMag == null) ||
        getPanel().getProject().getProjectState().isManualAlignmentTransformValid() == false)
      return false;

    return true;
  }

  /**
   * Returns true if the left runtime alignment transform exists.
   *
   * @author Chong Wei Chin
   */
  public boolean leftRuntimeAlignmentTransformForHighMagExists()
  {
    // If we have at least a manual transform, we are guaranteed to have something we can use as a runtime
    // transform.
    return leftManualAlignmentTransformForHighMagExists();
  }

  /**
   * Get the manual alignment AffineTransform for this panel when
   * it is positioned against the right panel-in-place sensor.
   *
   * @author Chong Wei Chin
   */
  public AffineTransform getRightManualAlignmentTransformForHighMag()
  {
    Assert.expect(_rightManualAlignmentTransformForHighMag != null);
    Assert.expect(getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    return _rightManualAlignmentTransformForHighMag;
  }

  /**
   * Get the runtime alignment AffineTransform for this panel when
   * it is positioned against the right panel-in-place sensor.
   *
   * @author Chong Wei Chin
   */
  public AffineTransform getRightRuntimeAlignmentTransformForHighMag()
  {
    Assert.expect(_rightManualAlignmentTransformForHighMag != null);
    Assert.expect(getPanel().getProject().getProjectState().isManualAlignmentTransformValid());

    if (_rightRuntimeAlignmentTransformForHighMag == null)
    {
      _rightRuntimeAlignmentTransformForHighMag = new AffineTransform(_rightManualAlignmentTransformForHighMag);
    }

    return _rightRuntimeAlignmentTransformForHighMag;
  }

  /**
   * @author Wei Chin, Chong
   * @author Phang Siew Yeng
   * This function is ONLY called by VerifyCadPanel to check if ALIGNMENT TRANSFORM for high mag is VALID.
   * For long panel/long board, alignment transform VALID - both left and right alignment transforms exist.
   * For short panel, alignment transform VALID - right alignment transform exists.
   * NOTE: Alignment VALID != Alignment COMPLETE
   */
  public boolean areManualAlignmentTransformsForHighMagValid()
  {
    boolean valid = true;
    if(isPanelBasedAlignment())
    {
      if (isLongPanel())
      {
        //if(isPotentialLongProgram())
        if(_panel.getProject().getTestProgram().isLongProgram())
        {
          if (leftManualAlignmentTransformForHighMagExists() == false || 
            _panel.getProject().getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.LEFT, MagnificationTypeEnum.HIGH) == false)
            valid = false;
        }
      }

      if (rightManualAlignmentTransformForHighMagExists() == false ||
        _panel.getProject().getTestProgram().hasTestSubProgram(PanelLocationInSystemEnum.RIGHT, MagnificationTypeEnum.HIGH) == false)
        valid = false;
    }
    else
    {
      // Wei Chin added for individual alignment
      for(Board board : _panel.getBoards())
      {
        valid = board.getBoardSettings().areManualAlignmentTransformsForHighMagValid();
        
        if(valid == false)
          break;
      }
    }

    return valid;
  }

  /**
   * @return true if the right manual alignment transform exists.
   *
   * @author Chong Wei Chin
   */
  public boolean rightManualAlignmentTransformForHighMagExists()
  {
    if ((_rightManualAlignmentTransformForHighMag == null) ||
        getPanel().getProject().getProjectState().isManualAlignmentTransformValid() == false)
      return false;

    return true;
  }

  /**
   * Returns true if the left runtime alignment transform exists.
   *
   * @author Chong, Wei Chin
   */
  public boolean rightRuntimeAlignmentTransformForHighMagExists()
  {
    // If we have at least a manual transform, we are guaranteed to have something we can use as a runtime
    // transform.
    return rightManualAlignmentTransformForHighMagExists();
  }
  
  /**
   * @author sham
   * Siew Yeng - Rename function name from isValidPerformedManualAlignment() to isManualAlignmentCompleted().
   *           - Call this to check if alignment is COMPLETED 
   * @edited hee-jihn.chuah - XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
   */
  public boolean isManualAlignmentCompleted()
  {
    boolean valid = true;
    if (_isManualAlignmentAllCompleted == false)
    {
      valid = false; 
    }
    else
    {
      if(hasHighMagnificationComponent() == false)
      {
        if(hasManualAlignmentBeenPerformed())
          valid = true;
        else
          valid = false;
      }
      else if(hasLowMagnificationComponent() == false)
      {
        if(hasManualAlignmentBeenPerformedForHighMag())
          valid = true;
        else
          valid = false;
      }
      else
      {
        //both alignment performed
        if(hasManualAlignmentBeenPerformedForHighMag()&& hasManualAlignmentBeenPerformed())
          valid = true;
        else
          valid = false;
      }
      //Khaw Chek Hau - XCR2317: Recipe compatibility issue - alignment group invalid but still able to generate image
      if (_panel.getProject().getTestProgram().hasInvalidatedAlignmentGroups())
      {
        valid = false;  
      }
    }

    return valid;
  }

  /**
   * @author sham
   */
  public void clearAllManualAlignmentTransform()
  {
    if(isPanelBasedAlignment())
    {
    _leftManualAlignmentTransformForHighMag = null;
    _rightManualAlignmentTransformForHighMag = null;

    _leftManualAlignmentTransform = null;
    _rightManualAlignmentTransform = null;
    }
    else
    {
      for(Board board : _panel.getBoards())
      {
        board.getBoardSettings().clearAllManualAlignmentTransform();
      }
    }
  }

  /**
   * @return the _splitLocationInNanometer
   * @author Wei Chin
   */
  public int getSplitLocationInNanometer()
  {
    return _splitLocationInNanometer;
  }

  /**
   * @param splitLocationInNanometer the _splitLocationInNanometer to set
   * @author Wei Chin
   */
  public void setSplitLocationInNanometer(int splitLocationInNanometer)
  {
    Assert.expect(splitLocationInNanometer > 0);
   
    int oldValue = _splitLocationInNanometer;
    
    if(oldValue == splitLocationInNanometer)
      return;
      
     _projectObservable.setEnabled(false);
    try
    {
      _splitLocationInNanometer = splitLocationInNanometer;    
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    _splitLocationInNanometer,
                                    PanelSettingsEventEnum.SPLIT_SETTING);
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  public boolean hasCustomSplit()
  {
    return (_splitLocationInNanometer > 0);
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  public void disableCustomSplit()
  {
    int oldValue = _splitLocationInNanometer;
    
    if(hasCustomSplit() == false)
      return;
      
     _projectObservable.setEnabled(false);
    try
    {
      _splitLocationInNanometer = -1;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    _splitLocationInNanometer,
                                    PanelSettingsEventEnum.SPLIT_SETTING);    
  }
  
  /**
   * @author Wei Chin
   */
  public int minimumLongPanelAlignmentRegion()
  {
    Assert.expect(_panel != null);
    
    int panelLengthAfterAllRotationInNanoMeters = _panel.getLengthAfterAllRotationsInNanoMeters() + 2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters();

    int maxLength = ReconstructionRegion.getMaxVerificationRegionLengthInNanoMeters();

    int totalRows = (int) Math.ceil(panelLengthAfterAllRotationInNanoMeters / maxLength);

    int rowForHalfPanel = (int) Math.ceil(totalRows / 2);

    int width;
    
    //Siew Yeng - XCR-2218 subtask XCR-2480
    if(_panel.getProject().isEnlargeReconstructionRegion() && 
        (ReconstructionRegion.isDefaultMaxInspectionRegionWidth() == false || ReconstructionRegion.isDefaultMaxInspectionRegionLength() == false))//Siew Yeng - XCR-3599
    {
      //Siew Yeng -  XCR-2218
      // width = half board length + overlap region size
      width = (rowForHalfPanel * maxLength) + ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters();
    }
    else
    {
      // add 2 Verification Region Overlap
      width = (rowForHalfPanel + 2) * maxLength;
    }

    return width - 2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters();
  }

  /**
   * @return the _useNewThicknessTable
   * @author Wei Chin
   */
  public boolean isUseNewThicknessTable()
  {
    return _useNewThicknessTable;
  }

  /**
   * @param useNewThicknessTable the _useNewThicknessTable to set
   * @author Wei Chin
   */
  public void setUseNewThicknessTable(boolean useNewThicknessTable)
  {
    boolean oldValue = _useNewThicknessTable;
    _projectObservable.setEnabled(false);
    try
    {
      _useNewThicknessTable = useNewThicknessTable;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    _useNewThicknessTable,
                                    PanelSettingsEventEnum.USE_NEW_THICKNESS_TABLE);    
  }

  /**
   * @return the _bypassAlignmentTranformWithinAcceptableLimit
   * @author Wei Chin
   */
  public boolean isBypassAlignmentTranformWithinAcceptableLimit()
  {
    return _bypassAlignmentTranformWithinAcceptableLimit;
  }

  /**
   * @param bypassAlignmentTranformWithinAcceptableLimit the _bypassAlignmentTranformWithinAcceptableLimit to set
   * @author Wei Chin
   */
  public void setBypassAlignmentTranformWithinAcceptableLimit(boolean bypassAlignmentTranformWithinAcceptableLimit)
  {
    boolean oldValue = _bypassAlignmentTranformWithinAcceptableLimit;
    _projectObservable.setEnabled(false);
    try
    {
      _bypassAlignmentTranformWithinAcceptableLimit = bypassAlignmentTranformWithinAcceptableLimit;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    _bypassAlignmentTranformWithinAcceptableLimit,
                                    PanelSettingsEventEnum.BYPASS_ALIGNMENT_TRANSFORM_WITHIN_LIMIT_VERIFICATION);    
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void setLeftRuntimeManualAlignmentTransform(AffineTransform leftRuntimeManualAlignmentTransform)
  {
    _leftRuntimeManualAlignmentTransform = leftRuntimeManualAlignmentTransform;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void setRightRuntimeManualAlignmentTransform(AffineTransform rightRuntimeManualAlignmentTransform)
  {
    _rightRuntimeManualAlignmentTransform = rightRuntimeManualAlignmentTransform;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void setLeftRuntimeManualAlignmentTransformForHighMag(AffineTransform leftRuntimeManualAlignmentTransform)
  {
    _leftRuntimeManualAlignmentTransformForHighMag = leftRuntimeManualAlignmentTransform;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void setRightRuntimeManualAlignmentTransformForHighMag(AffineTransform rightRuntimeManualAlignmentTransform)
  {
    _rightRuntimeManualAlignmentTransformForHighMag = rightRuntimeManualAlignmentTransform;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public AffineTransform getLeftRuntimeManualAlignmentTransform()
  {
    return _leftRuntimeManualAlignmentTransform;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public AffineTransform getRightRuntimeManualAlignmentTransform()
  {
    return _rightRuntimeManualAlignmentTransform;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public AffineTransform getLeftRuntimeManualAlignmentTransformForHighMag()
  {
    return _leftRuntimeManualAlignmentTransformForHighMag;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public AffineTransform getRightRuntimeManualAlignmentTransformForHighMag()
  {
    return _rightRuntimeManualAlignmentTransformForHighMag;
  }

  /*
   * @author Phang Siew Yeng
   */
  public void clearRuntimeManualAlignmentTransform()
  {
    _rightRuntimeManualAlignmentTransform = null;
    _leftRuntimeManualAlignmentTransform = null;
    
    _rightRuntimeManualAlignmentTransformForHighMag = null;
    _leftRuntimeManualAlignmentTransformForHighMag = null;
  }
  
  /**
   * XCR2619: The initial alignment can't change state to "completed" when doing manual alignment on mixed mag recipe
   * @author Khaw Chek Hau
   * @edited Kee Chin Seong - Separating out the AFfine Transform from View CAD
   */
  public void clearRightAllAlignmentTransformForLowMag()
  {
    Pair<AffineTransform, AffineTransform> oldValue =
        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform);
    _projectObservable.setEnabled(false);
    try
    {
      _rightManualAlignmentTransform = null;
      _rightRuntimeAlignmentTransform = null;
      _rightLastSavedViewCadRuntimeAlignmentTransform = null;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(null, null),
                                    PanelSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED);
  }
  
  /**
   * XCR2619: The initial alignment can't change state to "completed" when doing manual alignment on mixed mag recipe
   * @author Khaw Chek Hau
   * @edited By Kee Chin Seong - Separating out the AFfine Transform from View CAD
   */
  public void clearLeftAllAlignmentTransformForLowMag()
  {
    Pair<AffineTransform, AffineTransform> oldValue =
        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform);
    _projectObservable.setEnabled(false);
    try
    {
      _leftManualAlignmentTransform = null;
      _leftRuntimeAlignmentTransform = null;
      _leftLastSavedViewCadRuntimeAlignmentTransform = null;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(null, null),
                                    PanelSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED);
  }
  
  /**
   * XCR2619: The initial alignment can't change state to "completed" when doing manual alignment on mixed mag recipe
   * @author Khaw Chek Hau
   * @edited By Kee Chin Seong - Separating out the AFfine Transform from View CAD
   */
  public void clearRightAllAlignmentTransformForHighMag()
  {
    Pair<AffineTransform, AffineTransform> oldValue =
        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform);
    _projectObservable.setEnabled(false);
    try
    {
      _rightManualAlignmentTransformForHighMag = null;
      _rightRuntimeAlignmentTransformForHighMag = null;
      _rightLastSavedViewCadRuntimeAlignmentTransformForHighMag = null;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(null, null),
                                    PanelSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED);
  }
  
  /**
   * XCR2619: The initial alignment can't change state to "completed" when doing manual alignment on mixed mag recipe
   * @author Khaw Chek Hau
   * @Edited By Kee Chin Seong - Separating out the AFfine Transform from View CAD
   */
  public void clearLeftAllAlignmentTransformForHighMag()
  {
    Pair<AffineTransform, AffineTransform> oldValue =
        new Pair<AffineTransform, AffineTransform>(_leftManualAlignmentTransform, _rightManualAlignmentTransform);
    _projectObservable.setEnabled(false);
    try
    {
      _leftManualAlignmentTransformForHighMag = null;
      _leftRuntimeAlignmentTransformForHighMag = null;
      _leftLastSavedViewCadRuntimeAlignmentTransformForHighMag = null;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this,
                                    oldValue,
                                    new Pair<AffineTransform, AffineTransform>(null, null),
                                    PanelSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED);
  }
  
  /**
   * @author Kok Chun, Tan
   * add a new variation
   */
  public void addVariationSetting(VariationSetting variation)
  {
    Assert.expect(variation != null);
    
    if (_noLoadSettingVariationList == null)
    {
      _noLoadSettingVariationList = new ArrayList<VariationSetting> ();
    }
    
    if (_noLoadSettingVariationList.contains(variation) == false)
    {
      _noLoadSettingVariationList.add(variation);
    }
  }
  
  /**
   * @author Kok Chun, Tan
   * get all variation
   */
  public List<VariationSetting> getAllVariationSettings()
  {
    return _noLoadSettingVariationList;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setUsing2DAlignmentMethod(boolean isUsing2DAlignmentMethod)
  {
    if (isUsing2DAlignmentMethod == _isUsing2DAlignmentMethod)
      return;

    boolean oldValue = _isUsing2DAlignmentMethod;
    _projectObservable.setEnabled(false);
    try
    {
      _isUsing2DAlignmentMethod = isUsing2DAlignmentMethod;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, isUsing2DAlignmentMethod, PanelSettingsEventEnum.SET_ALIGNMENT_MODE);
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public boolean isUsing2DAlignmentMethod()
  {
    return _isUsing2DAlignmentMethod;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean hasVariationSettingBasedOnVariationName(String name)
  {
    Assert.expect(name != null);

    for (VariationSetting setting : _noLoadSettingVariationList)
    {
      if (name.equalsIgnoreCase(setting.getName()))
        return true;
    }
    return false;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public VariationSetting getVariationSettingBasedOnVariationName(String name)
  {
    Assert.expect(name != null);
    
    VariationSetting variation = null;
    for (VariationSetting setting : _noLoadSettingVariationList)
    {
      if (name.equalsIgnoreCase(setting.getName()))
      {
        variation = setting;
        break;
      }
    }
    Assert.expect(variation != null);
    return variation;
  }
  
  /**
   * @author hee-jihn.chuah XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
   */
  public void setIsManualAlignmentAllCompleted(boolean manualAlignmentComplete)
  {
    _isManualAlignmentAllCompleted = manualAlignmentComplete;
  }
}
