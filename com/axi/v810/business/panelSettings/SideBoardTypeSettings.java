package com.axi.v810.business.panelSettings;

import java.io.*;

import com.axi.v810.business.*;

/**
 * There is one SideBoardTypeSettings for each SideBoardType in the panel.
 * @author Keith Lee
 */
public class SideBoardTypeSettings implements Serializable
{
  private static transient ProjectObservable _projectObservable;

  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public SideBoardTypeSettings()
  {
    _projectObservable.stateChanged(this, null, this, SideBoardTypeSettingsEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.stateChanged(this, this, null, SideBoardTypeSettingsEventEnum.CREATE_OR_DESTROY);
  }
}
