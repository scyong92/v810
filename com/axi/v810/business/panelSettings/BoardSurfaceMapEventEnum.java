package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Cheah Lee Herng
 */
public class BoardSurfaceMapEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;
  public static BoardSurfaceMapEventEnum CREATE_OR_DESTROY = new BoardSurfaceMapEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static BoardSurfaceMapEventEnum PANEL = new BoardSurfaceMapEventEnum(++_index);
  public static BoardSurfaceMapEventEnum USER_COMMENT = new BoardSurfaceMapEventEnum(++_index);
  public static BoardSurfaceMapEventEnum ADD_SURFACE_MAP_POINTS = new BoardSurfaceMapEventEnum(++_index);
  public static BoardSurfaceMapEventEnum ADD_SURFACE_MAP_VERIFIED = new BoardSurfaceMapEventEnum(++_index);
  public static BoardSurfaceMapEventEnum ADD_OPTICAL_CAMERA_RECTANGLE = new BoardSurfaceMapEventEnum(++_index);
  public static BoardSurfaceMapEventEnum REMOVE_OPTICAL_CAMERA_RECTANGLE = new BoardSurfaceMapEventEnum(++_index);
  public static BoardSurfaceMapEventEnum TOGGLE_OPTICAL_CAMERA_RECTANGLE = new BoardSurfaceMapEventEnum(++_index);
  public static BoardSurfaceMapEventEnum ADD_SURFACE_MAP_OPTICAL_REGION = new BoardSurfaceMapEventEnum(++_index);
  public static BoardSurfaceMapEventEnum REMOVE_SURFACE_MAP_OPTICAL_REGION = new BoardSurfaceMapEventEnum(++_index);
  public static BoardSurfaceMapEventEnum SET_Z_HEIGHT_OFFSET = new BoardSurfaceMapEventEnum(++_index);
  public static BoardSurfaceMapEventEnum ADD_ALIGNMENT_SURFACE_MAP_REGION = new BoardSurfaceMapEventEnum(++_index);
  public static BoardSurfaceMapEventEnum REMOVE_ALIGNMENT_SURFACE_MAP_REGION = new BoardSurfaceMapEventEnum(++_index);
  
  /**
   * @author CHeah Lee Herng
   */
  private BoardSurfaceMapEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private BoardSurfaceMapEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
