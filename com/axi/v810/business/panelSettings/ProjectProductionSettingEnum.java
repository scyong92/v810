package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import java.util.*;

/**
 *
 * @author swee-yee.wong
 */
public class ProjectProductionSettingEnum extends com.axi.util.Enum
{
  private static int _index = 0;
  private String _name;
  private static java.util.List<ProjectProductionSettingEnum> _projectProductionSettingEnumList = new ArrayList<ProjectProductionSettingEnum>();
  private java.util.List<String> _selectionList = new ArrayList<String>();
 
  public static ProjectProductionSettingEnum PROJECT_BASED_BYPASS_MODE = new ProjectProductionSettingEnum(_index++, "Set project based bypass mode", null);
  /**
   *
   * @author swee-yee.wong
   */
  private ProjectProductionSettingEnum(int id, String name, java.util.List<String> selectionList)
  {
    super(id);
    Assert.expect(name != null);

    _name = name.intern();
    _selectionList = selectionList;
  }
  
  /**
   *
   * @author swee-yee.wong
   */
  public String getName()
  {
    Assert.expect(_name != null);
    return _name;
  }
  
  /**
   *
   * @author swee-yee.wong
   */
  public java.util.List<String> getSelectionList()
  {
    return _selectionList;
  }
  
  /**
   *
   * @author swee-yee.wong
   */
  public static java.util.List<ProjectProductionSettingEnum> getAllProjectProductionSettingList()
  {
    if (_projectProductionSettingEnumList != null)
      _projectProductionSettingEnumList.clear();
    
    _projectProductionSettingEnumList.add(PROJECT_BASED_BYPASS_MODE);
    
    return _projectProductionSettingEnumList;
  }
}


