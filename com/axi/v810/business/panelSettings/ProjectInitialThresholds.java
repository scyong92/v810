package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Andy Mechtenberg
 */
public class ProjectInitialThresholds
{
  private static ProjectInitialThresholds _instance = null;
  private Config _config;
  private InitialThresholdsReader _reader = InitialThresholdsReader.getInstance();
//  private String _currentSetName = null;
  private String _currentSetFullPath = getSystemDefaultSetName();

  private List<SingleInitialThreshold> _initialThresholds = new ArrayList<SingleInitialThreshold>();

  /**
   * @author Andy Mechtenberg
   */
  private ProjectInitialThresholds()
  {
    _config = Config.getInstance();
  }

  /**
   * @author Andy Mechtenberg
   */
  public static synchronized ProjectInitialThresholds getInstance()
  {
    if (_instance == null)
      _instance = new ProjectInitialThresholds();

    return _instance;
  }

  /**
   * @author Andy Mechtenberg
   * @author Wei Chin,Chong
   */
   public void load() throws DatastoreException
   {
     _initialThresholds.clear();
     // load from disk the _currentThresholdSetName and set _initialThresholds to it's contents
     _initialThresholds.addAll(_reader.read(_currentSetFullPath));
   }

  /**
   * @author Andy Mechtenberg
   */
  public String getSystemDefaultSetName()
  {
    return FileName.getInitialThresholdsDefaultFileWithoutExtension();
  }

  /**
   * @author Andy Mechtenberg
   * @author Wei Chin,Chong
   */
  public void loadProjectThresholdSet(String newSetFullPath) throws DatastoreException
  {
    Assert.expect(newSetFullPath != null);
    _currentSetFullPath = newSetFullPath;
    if (_currentSetFullPath.equalsIgnoreCase(getSystemDefaultSetName()))
      _initialThresholds.clear();
    else
      load();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getInitialThresholdsFileNameWithoutExtension()
  {
    Assert.expect(_config != null);

    String fileName = _config.getStringValue(SoftwareConfigEnum.INITIAL_THRESHOLDS_FILENAME);
    if (fileName.endsWith(FileName.getConfigFileExtension()))
      fileName = fileName.substring(0, fileName.length() - FileName.getConfigFileExtension().length());

    return fileName;
  }

  /**
   * @author Andy Mechtenberg
   */
   public void setImportInitialThresholdsFileNameWithoutExtension(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);
    Assert.expect(_config != null);

    Assert.expect(fileName.endsWith(FileName.getConfigFileExtension()) == false);
    _config.setValue(SoftwareConfigEnum.INITIAL_THRESHOLDS_FILENAME, fileName);
    if(fileName.equalsIgnoreCase(FileName.getInitialThresholdsDefaultFileWithoutExtension()))
      setInitialThresholdsFileFullPath( fileName );
    else
      setInitialThresholdsFileFullPath( FileName.getInitialThresholdsFullPath(fileName) );
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setInitialThresholdsFileFullPath(String filePath) throws DatastoreException
  {
    Assert.expect(filePath != null);
    Assert.expect(_config != null);
    String fileName = FileUtil.getNameWithoutExtension(FileUtil.getNameWithoutPath(filePath));
    if(filePath.equalsIgnoreCase(FileName.getInitialThresholdsDefaultFileWithoutExtension()) == false)
      Assert.expect(filePath.endsWith(FileName.getConfigFileExtension()));
    loadProjectThresholdSet(filePath);

    // this is optional, but it pre-loads the config ui screen with the set used for the loaded project, if any.
    InitialThresholds.getInstance().loadNewThresholdSet(fileName);
  }

  /**
   * @author Andy Mechtenberg
   */
   public String getThresholdSetName()
   {
     Assert.expect(_currentSetFullPath != null);
     return FileUtil.getNameWithoutExtension(FileUtil.getNameWithoutPath(_currentSetFullPath));
   }

  /**
   * @author Andy Mechtenberg
   */
   public Serializable getAlgorithmSettingValue(JointTypeEnum jointTypeEnum, Algorithm algorithm, AlgorithmSetting algorithmSetting)
   {
     Serializable value = null;
     if (doesAlgorithmSettingExist(jointTypeEnum, algorithm, algorithmSetting))
     {
       SingleInitialThreshold single = getSingleInitialThreshold(jointTypeEnum, algorithm, algorithmSetting);
       value = single.getValue();
     }
     else
       value = algorithmSetting.getDefaultValue(algorithm.getVersion());
     return value;
   }

  /**
   * @author Andy Mechtenberg
   */
   public boolean doesAlgorithmSettingExist(JointTypeEnum jointTypeEnum, Algorithm algorithm, AlgorithmSetting algorithmSetting)
   {
     for(SingleInitialThreshold single : _initialThresholds)
     {
       if ((single.getJointTypeEnum().equals(jointTypeEnum)) &&
           (single.getAlgorithm().equals(algorithm)) &&
           (single.getAlgorithmSetting().equals(algorithmSetting)))
         return true;
     }
     return false;
   }

  /**
   * @author Andy Mechtenberg
   */
   private SingleInitialThreshold getSingleInitialThreshold(JointTypeEnum jointTypeEnum, Algorithm algorithm, AlgorithmSetting algorithmSetting)
   {
     for(SingleInitialThreshold single : _initialThresholds)
     {
       if ((single.getJointTypeEnum().equals(jointTypeEnum)) &&
           (single.getAlgorithm().equals(algorithm)) &&
           (single.getAlgorithmSetting().equals(algorithmSetting)))
         return single;
     }
     Assert.expect(false, "Unable to find: " + jointTypeEnum + " " + algorithm + " " + algorithmSetting);
     return null;
   }
}
