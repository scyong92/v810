package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;
import com.axi.v810.business.*;
import java.awt.geom.*;

/**
 * @author Bill Darbie
 */
public class Test_ProjectState extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ProjectState());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      new ProjectEventObserver();

      // load a small panel
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();
      Project project = Project.importProjectFromNdfs("smallPanel", warnings);
      ProjectState projectState = project.getProjectState();

      Expect.expect(projectState.isTestProgramValid());
      Expect.expect(projectState.isTestProgramValidIgnoringAlignmentRegions());
      Expect.expect(projectState.areVerificationImagesValid());
      Expect.expect(projectState.isManualAlignmentTransformValid() == false);

      Expect.expect(project.getVersion() == 1.0);
      //  do something that changes state
      project.setNotes("hello");
//      projectState.displayStateEvents();

      Expect.expect(project.getVersion() == 1.1);

      // undo something that changes state
      ComponentType compType = project.getPanel().getComponentTypes().get(0);
      String refDes = compType.getReferenceDesignator();
      compType.setReferenceDesignator("newName");
//      projectState.displayStateEvents();
      Expect.expect(project.getVersion() == 2.0);
//      projectState.displayStateEvents();
      compType.setReferenceDesignator(refDes);
      Expect.expect(project.getVersion() == 1.1);

      project.setNotes("");
//      projectState.displayStateEvents();
      Expect.expect(project.getVersion() == 1.0);

      // create a new ComponentType and do several state changes to it,
      // then destroy it
      Panel panel = project.getPanel();
      SideBoardType sideBoardType = panel.getBoardTypes().get(0).getSideBoardType1();
      LandPattern landPattern = compType.getLandPattern();

      Expect.expect(panel.getUnusedSubtypes().size() == 0);
      ComponentType newCompType = sideBoardType.createComponentType("newRefDes", landPattern, new BoardCoordinate(5, 5), 0);

      newCompType.setReferenceDesignator("newRefDes2");
      newCompType.setCoordinateInNanoMeters(new BoardCoordinate(10, 20));
//      projectState.displayStateEvents();
      Expect.expect(project.getVersion() == 2.0);
      newCompType.destroy();
//      projectState.displayStateEvents();
      Expect.expect(project.getVersion() == 2.0);
      Expect.expect(panel.getUnusedSubtypes().size() == 1);
      //      projectState.displayStateEvents();
      for (Subtype subtype : panel.getUnusedSubtypes())
        subtype.destroy();
//      projectState.displayStateEvents();
      Expect.expect(project.getVersion() == 1.0);

      newCompType = sideBoardType.createComponentType("newRefDes", landPattern, new BoardCoordinate(5, 5), 0);
      newCompType.setReferenceDesignator("newRefDes2");
      newCompType.setCoordinateInNanoMeters(new BoardCoordinate(10, 20));

//      projectState.displayStateEvents();
      Expect.expect(project.getVersion() == 2.0);
      newCompType.remove();
//      projectState.displayStateEvents();
      Expect.expect(project.getVersion() == 2.0);
      Expect.expect(panel.getUnusedSubtypes().size() == 1);
//      projectState.displayStateEvents();
      for (Subtype subtype : panel.getUnusedSubtypes())
        subtype.destroy();
//      projectState.displayStateEvents();
      Expect.expect(project.getVersion() == 2.0);
      newCompType.add();
//      projectState.displayStateEvents();
      Expect.expect(project.getVersion() == 2.0);
      Expect.expect(panel.getUnusedSubtypes().size() == 0);
      newCompType.destroy();

      // do one state change on one board instance, undo it on anther board instance
      Board board1 = panel.getBoards().get(0);
      board1.setName("newBoard1");
      Expect.expect(project.getVersion() == 1.1);
      Board board2 = panel.getBoards().get(1);
      String board2Name = board2.getName();
      board2.setName("newBoard2");
//      projectState.displayStateEvents();
      Expect.expect(project.getVersion() == 1.1);
      board2.setName(board2Name);
//      projectState.displayStateEvents();
      Expect.expect(project.getVersion() == 1.1);

      // test verification images
      Expect.expect(projectState.areVerificationImagesValid());
      int origDegrees = panel.getDegreesRotationRelativeToCad();
      panel.getPanelSettings().setDegreesRotationRelativeToCad(180);
      Expect.expect(projectState.areVerificationImagesValid() == false);
      panel.getPanelSettings().setDegreesRotationRelativeToCad(origDegrees);
      Expect.expect(projectState.areVerificationImagesValid());

      // test manual alignment
      Board board = panel.getBoards().get(0);
      PanelCoordinate origCoord = board.getLowerLeftCoordinateRelativeToPanelInNanoMeters();
      Expect.expect(projectState.isManualAlignmentTransformValid() == false);
      AffineTransform trans = AffineTransform.getScaleInstance(1.0, 1.0);
      panel.getPanelSettings().setRightManualAlignmentTransform(trans);
      Expect.expect(projectState.isManualAlignmentTransformValid());
      board.setCoordinateInNanoMeters(new PanelCoordinate(23, 23));
      Expect.expect(projectState.isManualAlignmentTransformValid());
      board.setCoordinateInNanoMeters(origCoord);
      Expect.expect(projectState.isManualAlignmentTransformValid());

      // destroy a pre-existing ComponentType
      Expect.expect(project.getVersion() == 1.1);
      board = panel.getBoards().get(0);
      ComponentType componentType = board.getBoardType().getComponentTypes().get(0);
      componentType.destroy();
      Expect.expect(project.getVersion() == 2.0);

      // remove and add a pre-existing ComponentType
      componentType = board.getBoardType().getComponentTypes().get(0);
      componentType.remove();
      Expect.expect(project.getVersion() == 2.0);
      componentType.add();
      Expect.expect(project.getVersion() == 2.0);

      // delete all ComponentTypes that use the same CompPackage
      // and make sure the CompPackage gets automatically deleted
      CompPackage compPackage = panel.getCompPackages().get(0);
      for (ComponentType aComponentType : compPackage.getComponentTypes())
        aComponentType.destroy();
      Expect.expect(project.getVersion() == 2.0);
      List<CompPackage> compPackages = panel.getCompPackages();
      Expect.expect(compPackages.contains(compPackage) == false);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}

/**
 * @author Bill Darbie
 */
class ProjectEventObserver implements Observer
{
  /**
   * @author Bill Darbie
   */
  ProjectEventObserver()
  {
    ProjectObservable.getInstance().addObserver(this);
  }

  /**
   * @author Bill Darbie
   */
  public void update(Observable observable, Object object)
  {
//    ProjectChangeEvent projChangeEvent = (ProjectChangeEvent)object;
//    System.out.println(projChangeEvent.getProjectChangeEventEnum() + " " + projChangeEvent.getProjectChangeEventEnum().getId());
  }
}
