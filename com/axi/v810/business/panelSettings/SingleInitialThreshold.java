package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;

/**
 * @author Andy Mechtenberg
 */
public class SingleInitialThreshold implements Comparable
{
  private JointTypeEnum _jointTypeEnum;
  private Algorithm _algorithm;
  private AlgorithmSetting _algorithmSetting;
  private Serializable _value;
  
  // for use by compareTo only.
  private static final List<JointTypeEnum> _allJointTypeEnums = JointTypeEnum.getAllJointTypeEnums();
  private static final int _additionalDisplayFactor = 10000;
  
  public SingleInitialThreshold(JointTypeEnum jointTypeEnum,
                                Algorithm algorithm,
                                AlgorithmSetting algorithmSetting,
                                Serializable value)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithm != null);
    Assert.expect(algorithmSetting != null);
    Assert.expect(value != null);
    
    _jointTypeEnum = jointTypeEnum;
    _algorithm = algorithm;
    _algorithmSetting = algorithmSetting;
    _value = value;
  }
  
  /**
   * @author Andy Mechtenberg
   */
   void setValue(Serializable value)
   {
     Assert.expect(value != null);
     _value = value;
   }
  
  /**
   * @author Andy Mechtenberg
   */
   public JointTypeEnum getJointTypeEnum()
   {
     return _jointTypeEnum;
   }
   
   /**
   * @author Andy Mechtenberg
   */
   public Algorithm getAlgorithm()
   {
     return _algorithm;
   }
   
   /**
   * @author Andy Mechtenberg
   */
   public AlgorithmSetting getAlgorithmSetting()
   {
     return _algorithmSetting;
   }
   
   /**
   * @author Andy Mechtenberg
   */
   public Serializable getValue()
   {
     return _value;
   }
   
  /**
   * @author Andy Mechtenberg
   */
   public int compareTo(Object object)
   {
     SingleInitialThreshold rhSingleInitialThreshold = (SingleInitialThreshold) object;
     SingleInitialThreshold lhSingleInitialThreshold = this;
     Assert.expect(lhSingleInitialThreshold != null);
     Assert.expect(rhSingleInitialThreshold != null);

     // sort by Joint Type, then Algorithm, then Threshold display order
     int lhJointTypeEnumOrder = _allJointTypeEnums.indexOf(lhSingleInitialThreshold.getJointTypeEnum());
     int rhJointTypeEnumOrder = _allJointTypeEnums.indexOf(rhSingleInitialThreshold.getJointTypeEnum());

     // algorithms are listed in this order to the user (by ID)
     // the exceptions are the thresholds that are of type SLICE_HEIGHT and SLICE_HEIGHT_ADDITIONAL
     // those should be last (within their joint type) because that's how we display them in the GUI
     int lhAlgorithmOrder = lhSingleInitialThreshold.getAlgorithm().getAlgorithmEnum().getId();
     int rhAlgorithmOrder = rhSingleInitialThreshold.getAlgorithm().getAlgorithmEnum().getId();

     int lhAlgorithmVersion = lhSingleInitialThreshold.getAlgorithm().getVersion();
     int rhAlgorithmVersion = rhSingleInitialThreshold.getAlgorithm().getVersion();
    
     // change the algorithm order if the setting is slice setup stuff -- we want it at the end, so use a BIG number.
     if ((lhSingleInitialThreshold.getAlgorithmSetting().getAlgorithmSettingTypeEnum(lhAlgorithmVersion).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT)) ||
             (lhSingleInitialThreshold.getAlgorithmSetting().getAlgorithmSettingTypeEnum(lhAlgorithmVersion).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL)))
       lhAlgorithmOrder = Integer.MAX_VALUE;
     if ((rhSingleInitialThreshold.getAlgorithmSetting().getAlgorithmSettingTypeEnum(rhAlgorithmVersion).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT)) ||
             (rhSingleInitialThreshold.getAlgorithmSetting().getAlgorithmSettingTypeEnum(rhAlgorithmVersion).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL)))
       rhAlgorithmOrder = Integer.MAX_VALUE;
     int lhDisplayOrder = lhSingleInitialThreshold.getAlgorithmSetting().getDisplayOrder();
     int rhDisplayOrder = rhSingleInitialThreshold.getAlgorithmSetting().getDisplayOrder();
     
     // we want to keep the Additional Thresholds AFTER the Standard ones, but the display order doesn't guarentee that, so 
     // we'll just add a big number to the additional display order to keep them after the standards.
     if (lhSingleInitialThreshold.getAlgorithmSetting().getAlgorithmSettingTypeEnum(lhAlgorithmVersion).equals(AlgorithmSettingTypeEnum.ADDITIONAL))
       lhDisplayOrder += _additionalDisplayFactor;
     if (lhSingleInitialThreshold.getAlgorithmSetting().getAlgorithmSettingTypeEnum(lhAlgorithmVersion).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL))
       lhDisplayOrder += _additionalDisplayFactor;
     if (rhSingleInitialThreshold.getAlgorithmSetting().getAlgorithmSettingTypeEnum(rhAlgorithmVersion).equals(AlgorithmSettingTypeEnum.ADDITIONAL))
       rhDisplayOrder += _additionalDisplayFactor;
     if (rhSingleInitialThreshold.getAlgorithmSetting().getAlgorithmSettingTypeEnum(rhAlgorithmVersion).equals(AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL))
       rhDisplayOrder += _additionalDisplayFactor;

     if (lhJointTypeEnumOrder > rhJointTypeEnumOrder)
       return 1;
     else if (lhJointTypeEnumOrder < rhJointTypeEnumOrder)
       return -1;
     else
     {
       if (lhAlgorithmOrder > rhAlgorithmOrder)
         return 1;
       else if (lhAlgorithmOrder < rhAlgorithmOrder)
         return -1;
       else
       {
         if (lhDisplayOrder == rhDisplayOrder)
           return 0;
         else if (lhDisplayOrder > rhDisplayOrder)
           return 1;
         else
           return -1;
       }
     }
  }
}
