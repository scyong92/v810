package com.axi.v810.business.panelSettings;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.JointTypeEnum;

/**
 * @author Andy Mechtenberg
 */
class Test_SingleInitialThreshold extends UnitTest
{

  /**
   * @author Andy Mechtenberg
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_SingleInitialThreshold());
  }

  /**
   * @author Andy Mechtenberg
   */
   private void testSingleInitialThreshold()
   {
     InspectionFamily inspectionFamily = InspectionFamily.getInstance(JointTypeEnum.CAPACITOR);
     
     Algorithm measAlgo = inspectionFamily.getAlgorithm(inspectionFamily.getInspectionFamilyEnum(), AlgorithmEnum.MEASUREMENT);
     AlgorithmSetting setting = measAlgo.getAlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH);
     
     SingleInitialThreshold sit = new SingleInitialThreshold(JointTypeEnum.CAPACITOR,
                                                             measAlgo,
                                                             setting,
                                                             "5.6");
     
     Expect.expect(sit.getJointTypeEnum().equals(JointTypeEnum.CAPACITOR));
     Expect.expect(sit.getAlgorithm() == measAlgo);
     Expect.expect(sit.getAlgorithmSetting() == setting);
     Expect.expect(sit.getValue() == "5.6");
     
     sit.setValue("5.7");
     Expect.expect(sit.getValue() == "5.7");
     
     // create a new one to test the compare method
     AlgorithmSetting setting2 = measAlgo.getAlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS);
     SingleInitialThreshold sit2 = new SingleInitialThreshold(JointTypeEnum.CAPACITOR,
                                                              measAlgo,
                                                              setting2,
                                                              "5.6");
     
     int compareResult = sit.compareTo(sit2);     
     Expect.expect(compareResult == -1);
     
     compareResult = sit2.compareTo(sit);
     Expect.expect(compareResult == 1);
     
     compareResult = sit2.compareTo(sit2);
     Expect.expect(compareResult == 0);
   }
    
  /**
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testSingleInitialThreshold();    
  }
}