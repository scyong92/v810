package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;
import java.awt.geom.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalCameraRectangle implements Serializable, Comparable 
{
  private Board _board;
  private PanelRectangle _panelRectangle;
  private String _name;
  private boolean _isInspectable;
  
  private double _zHeightInNanometer;
  private boolean _zHeightValid;
  private List<com.axi.v810.business.panelDesc.Component> _componentList = new ArrayList<>();
  private List<PanelCoordinate> _pointPanelCoordinateList = new ArrayList<>();

  /**
   * @author Cheah Lee Herng
   */
  public OpticalCameraRectangle()
  {
    _board = null;
    _panelRectangle = null;
    _name = "";
    _isInspectable = false;
    
    _zHeightInNanometer = -1;
    _zHeightValid = false;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public OpticalCameraRectangle(int xCoordinateInNanometer, int yCoordinateInNanometer, int widthInNanometer, int heightInNanometer)
  {
    _board = null;
    _panelRectangle = new PanelRectangle(xCoordinateInNanometer, yCoordinateInNanometer, widthInNanometer, heightInNanometer);
    setName(xCoordinateInNanometer, yCoordinateInNanometer, widthInNanometer, heightInNanometer);
    _isInspectable = true;
    
    _zHeightInNanometer = -1;
    _zHeightValid = false;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setRegion(int xCoordinate, int yCoordinate, int width, int height)
  {
    Assert.expect(width > 0);
    Assert.expect(height > 0);
    Assert.expect(xCoordinate >= 0);
    Assert.expect(yCoordinate >= 0);

    _panelRectangle = new PanelRectangle(xCoordinate, yCoordinate,  width, height);
    setName(xCoordinate, yCoordinate, width, height);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public PanelRectangle getRegion()
  {
    Assert.expect(_panelRectangle != null);
    return _panelRectangle;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void setName(int xCoordinateInNanometers, int yCoordinateInNanometers, int widthInNanometers, int heightInNanometers)
  {
    Assert.expect(widthInNanometers > 0);
    Assert.expect(heightInNanometers > 0);

    _name = "";
    _name += (int)(xCoordinateInNanometers) + "_"
            + (int)(yCoordinateInNanometers) + "_"
            + (int) (widthInNanometers) + "_"
            + (int)(heightInNanometers);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public String getName()
  {
    Assert.expect(_name != null);
    return _name;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void addPointPanelCoordinate(int xCoordinateInNanometer, int yCoordinateInNanometer)
  {
    Assert.expect(xCoordinateInNanometer >= 0);
    Assert.expect(yCoordinateInNanometer >= 0);
    
    PanelCoordinate panelCoordinate = new PanelCoordinate(xCoordinateInNanometer, yCoordinateInNanometer);
    if (_pointPanelCoordinateList.contains(panelCoordinate) == false)
    {
      _pointPanelCoordinateList.add(panelCoordinate);
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removePointPanelCoordinate(int xCoordinateInNanometer, int yCoordinateInNanometer)
  {
    Assert.expect(xCoordinateInNanometer >= 0);
    Assert.expect(yCoordinateInNanometer >= 0);
    
    PanelCoordinate panelCoordinate = new PanelCoordinate(xCoordinateInNanometer, yCoordinateInNanometer);
    if (_pointPanelCoordinateList.contains(panelCoordinate))
    {
      _pointPanelCoordinateList.remove(panelCoordinate);
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removePointPanelCoordinate(PanelCoordinate pointPanelCoordinate)
  {
    Assert.expect(pointPanelCoordinate != null);
    
    if (_pointPanelCoordinateList.contains(pointPanelCoordinate))
    {
      _pointPanelCoordinateList.remove(pointPanelCoordinate);
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void removePointPanelCoordinates(java.util.List<PanelCoordinate> pointPanelCoordinates)
  {
    Assert.expect(pointPanelCoordinates != null);
    
    _pointPanelCoordinateList.removeAll(pointPanelCoordinates);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean hasPanelCoordinateList()
  {
    Assert.expect(_pointPanelCoordinateList != null);
    
    return _pointPanelCoordinateList.size() > 0;
  }
  
  /**
   * @author Ying-Huan.Chu
   * @return 
   */
  public java.util.List<PanelCoordinate> getPanelCoordinateList()
  {
    Assert.expect(_pointPanelCoordinateList != null);
    
    return _pointPanelCoordinateList;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void clearPointPanelCoordinateList()
  {
    Assert.expect(_pointPanelCoordinateList != null);
    
    _pointPanelCoordinateList.clear();
  }

  /**
   * @author Cheah Lee Herng
   */
  public String toString()
  {
    return getName();
  }
    
  /**
   * @author Ying-Huan.Chu
   */
  public boolean getInspectableBool()
  {
    return _isInspectable;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setInspectableBool(boolean isInspectable)
  {
    _isInspectable = isInspectable;
  }
  
  /**
   * @author Cheah Lee Herng
   * @author Ying-Huan.Chu
   */
  public Board getBoard()
  {
    Assert.expect(_board != null);
    return _board;
  }
  
  /**
   * @author Cheah Lee Herng
   * @author Ying-Huan.Chu
   */
  public void setBoard(Board board)
  {
    Assert.expect(board != null);
    _board = board;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setZHeightInNanometer(double zHeightInNanometer)
  {
    _zHeightInNanometer = zHeightInNanometer;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public double getZHeightInNanometer()
  {
    return _zHeightInNanometer;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setZHeightValid(boolean zHeightValid)
  {
    _zHeightValid = zHeightValid;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isZHeightValid()
  {
    return _zHeightValid;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public int compareTo(Object rhs) 
  {
    Assert.expect(rhs instanceof OpticalCameraRectangle, "OpticalCameraRectangle should only be compared to other OpticalCameraRectangle!");
    
    OpticalCameraRectangle rhsOpticalCameraRectangle = (OpticalCameraRectangle)rhs;
    
    PanelCoordinate lhsPanelCoordinate = getRegion().getCenterCoordinate();    
    PanelCoordinate rhsPanelCoordinate = rhsOpticalCameraRectangle.getRegion().getCenterCoordinate();
    
    if (lhsPanelCoordinate.getX() > rhsPanelCoordinate.getX())
      return -1;
    else if (lhsPanelCoordinate.getX() < rhsPanelCoordinate.getX())
      return 1;
    else
    {
      if (lhsPanelCoordinate.getY() > rhsPanelCoordinate.getY())
        return -1;
      else if (lhsPanelCoordinate.getY() < rhsPanelCoordinate.getY())
        return 1;
      else
        return 0;
    }
  }
  
   /**
   * @author Jack Hwee
   */
  public void addToComponentList(com.axi.v810.business.panelDesc.Component component)
  {
    if (_componentList.contains(component) == false)
      _componentList.add(component);
  }
  
  /**
   * @author Jack Hwee
   */
  public List<com.axi.v810.business.panelDesc.Component> getComponentList()
  {
    return _componentList;
  }
  
  /**
   * @author Jack Hwee
   */
  public void modifyRegion(int xCoordinate, int yCoordinate, int width, int height)
  {
    Assert.expect(xCoordinate >= 0);
    Assert.expect(yCoordinate >= 0);
    Assert.expect(width > 0);
    Assert.expect(height > 0);

    _panelRectangle.setRect(xCoordinate, yCoordinate, width, height);
    _name = "";
    setName(xCoordinate, yCoordinate, width, height);
  }
}
