package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PanelAlignmentSurfaceMapSettings implements Serializable
{
    private Panel _panel;
    
    // Map of surface maps that are parked under this surface map region
    private Map<String, OpticalRegion> _regionPositionNameToOpticalRegionMap = new LinkedHashMap<String, OpticalRegion>(); // PanelAlignmentSurfaceMapSettingsReader
    private Map<String, OpticalCameraIdEnum> _panelPositionNameToCameraMap = new LinkedHashMap<String, OpticalCameraIdEnum>();
    private Map<String, String> _alignmentGroupNameToRegionPositionNameMap = new LinkedHashMap<String, String>();
    
    private static transient ProjectObservable _projectObservable;
    
    /**
     * @author Cheah Lee Herng
     */
    static
    {
        _projectObservable = ProjectObservable.getInstance();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public PanelAlignmentSurfaceMapSettings()
    {
        _projectObservable.stateChanged(this, null, this, PanelAlignmentSurfaceMapEventEnum.CREATE_OR_DESTROY);
    }
    
    /**
     * @author Cheah Lee Herng
    */
    public PanelAlignmentSurfaceMapSettings(Panel panel)
    {
        Assert.expect(panel != null);
        _panel = panel;
        _projectObservable.stateChanged(this, null, this, PanelAlignmentSurfaceMapEventEnum.CREATE_OR_DESTROY);
    }
    
    /**
     * @author Bill Darbie
    */
    public void destroy()
    {        
        _projectObservable.stateChanged(this, this, null, PanelAlignmentSurfaceMapEventEnum.CREATE_OR_DESTROY);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void addOpticalRegion(String alignmentGroupName, OpticalRegion opticalRegion)
    {
        Assert.expect(_regionPositionNameToOpticalRegionMap != null);
        Assert.expect(alignmentGroupName != null);
        Assert.expect(opticalRegion != null);     
      
        String regionPositionName = opticalRegion.getRegion().getMinX() + "_" + opticalRegion.getRegion().getMinY() + "_" + opticalRegion.getRegion().getWidth() + "_" + opticalRegion.getRegion().getHeight();
         
        OpticalRegion oldPanelOpticalRegionSettings = null;
        _projectObservable.setEnabled(false);
        try
        {
           oldPanelOpticalRegionSettings = _regionPositionNameToOpticalRegionMap.put(regionPositionName, opticalRegion);
           
           if (_alignmentGroupNameToRegionPositionNameMap.containsKey(alignmentGroupName))
           {
               _alignmentGroupNameToRegionPositionNameMap.remove(alignmentGroupName);
               _alignmentGroupNameToRegionPositionNameMap.put(alignmentGroupName, regionPositionName); 
           }
           else
           {
               _alignmentGroupNameToRegionPositionNameMap.put(alignmentGroupName, regionPositionName);
           }
        }
        finally
        {
          _projectObservable.setEnabled(true);
        }         
        _projectObservable.stateChanged(this, oldPanelOpticalRegionSettings, opticalRegion, PanelAlignmentSurfaceMapEventEnum.ADD_OPTICAL_REGION);
    }
    
     /**
     * @author Jack Hwee
     */
    public void removeOpticalRegion(String alignmentGroupName, OpticalRegion opticalRegion)
    {
        Assert.expect(_regionPositionNameToOpticalRegionMap != null);
        Assert.expect(alignmentGroupName != null);
        Assert.expect(opticalRegion != null);
    
        String regionPositionName = opticalRegion.getRegion().getMinX() + "_" + opticalRegion.getRegion().getMinY() + "_" + opticalRegion.getRegion().getWidth() + "_" + opticalRegion.getRegion().getHeight();
       
        OpticalRegion oldOpticalRegion = null;
        _projectObservable.setEnabled(false);
        try
        {
          oldOpticalRegion =   _regionPositionNameToOpticalRegionMap.remove(regionPositionName);
          
          if (_alignmentGroupNameToRegionPositionNameMap.containsKey(alignmentGroupName))
              _alignmentGroupNameToRegionPositionNameMap.remove(alignmentGroupName);
        }
        finally
        {
          _projectObservable.setEnabled(true);
        }         
        _projectObservable.stateChanged(this, oldOpticalRegion, opticalRegion, PanelAlignmentSurfaceMapEventEnum.REMOVE_OPTICAL_REGION);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public OpticalRegion getAlignmentOpticalRegion(String alignmentGroupName)
    {
        Assert.expect(alignmentGroupName != null);
        Assert.expect(_alignmentGroupNameToRegionPositionNameMap != null);
        Assert.expect(_regionPositionNameToOpticalRegionMap != null);
        
        String regionPositionName = _alignmentGroupNameToRegionPositionNameMap.get(alignmentGroupName);
        Assert.expect(regionPositionName != null);
        
        OpticalRegion opticalRegion = _regionPositionNameToOpticalRegionMap.get(regionPositionName);
        Assert.expect(opticalRegion != null);
        
        return opticalRegion;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public boolean isAlignmentOpticalRegionExists(String alignmentGroupName)
    {
        Assert.expect(alignmentGroupName != null);
        
        if (_alignmentGroupNameToRegionPositionNameMap.containsKey(alignmentGroupName))
            return true;
        else
            return false;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public Map<String, OpticalRegion> getOpticalRegionPositionNameToOpticalRegionMap()
    {
        Assert.expect(_regionPositionNameToOpticalRegionMap != null);
        return _regionPositionNameToOpticalRegionMap;
    }
    
    /**
     * @return a List containing OpticalRegion for this region
     * @author Jack Hwee
    */
    public List<OpticalRegion> getOpticalRegions()
    {
     Assert.expect(_regionPositionNameToOpticalRegionMap != null);

     return new ArrayList<OpticalRegion>(_regionPositionNameToOpticalRegionMap.values());
    }
    
    /**
     * @return a List containing Name for this region
     * @author Jack Hwee
    */
    public List<String> getOpticalRegionsName()
    {
     Assert.expect(_regionPositionNameToOpticalRegionMap != null);

     return new ArrayList<String>(_regionPositionNameToOpticalRegionMap.keySet());
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public OpticalRegion getOpticalRegion(String regionPositionName)
    {
        Assert.expect(regionPositionName != null);
        
        OpticalRegion opticalRegion = _regionPositionNameToOpticalRegionMap.get(regionPositionName);
        return opticalRegion;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public void assignCamera(String panelPositionName, OpticalCameraIdEnum cameraId)
    {
        Assert.expect(panelPositionName != null);
        Assert.expect(cameraId != null);
        Assert.expect(_panelPositionNameToCameraMap != null);
       
        Object prev = _panelPositionNameToCameraMap.put(panelPositionName, cameraId);
        Assert.expect(prev == null);                
    }
    
    /**
     * @author Cheah Lee Herng
    */
    public Panel getPanel()
    {
        Assert.expect(_panel != null);

        return _panel;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public Map<String, String> getAlignmentGroupNameToRegionPositionNameMap()
    {
        Assert.expect(_alignmentGroupNameToRegionPositionNameMap != null);
        return _alignmentGroupNameToRegionPositionNameMap;
    }
}
