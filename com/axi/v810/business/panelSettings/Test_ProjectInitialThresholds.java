package com.axi.v810.business.panelSettings;

import com.axi.v810.datastore.DatastoreException;
import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.JointTypeEnum;
import com.axi.v810.datastore.Directory;
import com.axi.v810.datastore.FileName;
import com.axi.v810.datastore.config.Config;

/**
 * @author Andy Mechtenberg
 */
class Test_ProjectInitialThresholds extends UnitTest
{
  private Config _config;

  Test_ProjectInitialThresholds()
  {
    super();

    _config = Config.getInstance();
    try
    {
      _config.loadIfNecessary();
    }
    catch(DatastoreException ds)
    {
      Expect.expect(false);
    }
  }
  /**
   * @author Andy Mechtenberg
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_ProjectInitialThresholds());
  }

  /**
   * @author Andy Mechtenberg
   */
   private void testProjectInitialThresholds()
   {
     ProjectInitialThresholds projectInitialThresholds = ProjectInitialThresholds.getInstance();
     try
     {
       // delete all files in the initial Thresholds directory
       FileUtil.deleteDirectoryContents(Directory.getInitialThresholdsDir());
     }
     catch (CouldNotDeleteFileException ex)
     {
       Expect.expect(false);
     }

     try
     {
       // default set name is always set but the default cannot open directly
       projectInitialThresholds.load();
       Expect.expect(true);
     }
     catch (DatastoreException ex)
     {
       Expect.expect(true);
     }
     catch (AssertException ae)
     {
       // should assert
       Expect.expect(false);
     }

     try
     {
       // name is not set, will be null and will assert
       projectInitialThresholds.getThresholdSetName();
     }
     catch (AssertException ae)
     {
       Expect.expect(true);
     }

     try
     {
       // "SytemDefaults" is load-able
       projectInitialThresholds.loadProjectThresholdSet("SystemDefaults");
       Expect.expect(true);
     }
     catch (DatastoreException ex)
     {
       Expect.expect(false);
     }

     try
     {
       // load one that doesn't exist
       projectInitialThresholds.loadProjectThresholdSet("setName");
       Expect.expect(false);
     }
     catch (DatastoreException ex)
     {
       Expect.expect(true);
     }

     // methods using a real set name
     String newSetName = "setName";
     createInitialThresholdSet(newSetName);
    try
    {
      projectInitialThresholds.setInitialThresholdsFileFullPath(FileName.getInitialThresholdsFullPath(newSetName));
    }
    catch (DatastoreException ex)
    {
      Expect.expect(false);
    }
     Expect.expect(projectInitialThresholds.getThresholdSetName().equals(newSetName));

     try
     {
       projectInitialThresholds.load();
     }
     catch (DatastoreException ex)
     {
       Expect.expect(false);
     }

     Expect.expect(projectInitialThresholds.getThresholdSetName().equals(newSetName));
     InspectionFamily inspectionFamily = InspectionFamily.getInstance(JointTypeEnum.CAPACITOR);

     Algorithm measAlgo = inspectionFamily.getAlgorithm(inspectionFamily.getInspectionFamilyEnum(), AlgorithmEnum.MEASUREMENT);
     AlgorithmSetting setting = measAlgo.getAlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH);
     AlgorithmSetting setting2 = measAlgo.getAlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS);
     Serializable value = projectInitialThresholds.getAlgorithmSettingValue(JointTypeEnum.CAPACITOR,
                                                             measAlgo,
                                                             setting);
     Expect.expect(value.equals("5.6"));

     value = projectInitialThresholds.getAlgorithmSettingValue(JointTypeEnum.CAPACITOR,
                                                             measAlgo,
                                                             setting2);
     Expect.expect(value.equals("5.7"));

     // test the import name being saved
     try
     {
       Expect.expect(projectInitialThresholds.getInitialThresholdsFileNameWithoutExtension().equals("SystemDefaults"));
       projectInitialThresholds.setImportInitialThresholdsFileNameWithoutExtension(newSetName);
       Expect.expect(projectInitialThresholds.getInitialThresholdsFileNameWithoutExtension().equals(newSetName));
       projectInitialThresholds.setImportInitialThresholdsFileNameWithoutExtension("SystemDefaults");
       Expect.expect(projectInitialThresholds.getInitialThresholdsFileNameWithoutExtension().equals("SystemDefaults"));
     }
     catch (DatastoreException ex)
     {
       Expect.expect(false);
     }
   }

  /**
   * @author Andy Mechtenberg
   */
   private void createInitialThresholdSet(String setName)
   {
     InitialThresholds initialThresholds = InitialThresholds.getInstance();
     initialThresholds.createNewSet(setName);

     initialThresholds.setCurrentUnits(MathUtilEnum.MILS);
     // populate with two settings
     InspectionFamily inspectionFamily = InspectionFamily.getInstance(JointTypeEnum.CAPACITOR);

     Algorithm measAlgo = inspectionFamily.getAlgorithm(inspectionFamily.getInspectionFamilyEnum(), AlgorithmEnum.MEASUREMENT);
     AlgorithmSetting setting = measAlgo.getAlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH);

     initialThresholds.setAlgorithmSettingValue(JointTypeEnum.CAPACITOR,
                                                             measAlgo,
                                                             setting,
                                                             "5.6");

     // create a second one
     AlgorithmSetting setting2 = measAlgo.getAlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS);
     initialThresholds.setAlgorithmSettingValue(JointTypeEnum.CAPACITOR,
                                                              measAlgo,
                                                              setting2,
                                                              "5.7");

     Expect.expect(initialThresholds.getAllSettingsInSortedOrder().size() == 2);
     try
     {
       Expect.expect(initialThresholds.isDirty());
       initialThresholds.save();
       Expect.expect(initialThresholds.isDirty() == false);
     }
     catch (DatastoreException ex)
     {
       Expect.expect(false);
     }
   }

  /**
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testProjectInitialThresholds();
  }

}
