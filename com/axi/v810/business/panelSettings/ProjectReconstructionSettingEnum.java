package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import java.util.*;

/**
 *
 * @author swee-yee.wong
 */
public class ProjectReconstructionSettingEnum extends com.axi.util.Enum
{
  private static int _index = 0;
  private String _name;
  private static java.util.List<ProjectReconstructionSettingEnum> _projectReconstructionSettingEnumList = new ArrayList<ProjectReconstructionSettingEnum>();
  private java.util.List<String> _selectionList = new ArrayList<String>();
 
  public static ProjectReconstructionSettingEnum ENLARGE_RECONSTRUCTION_REGION = new ProjectReconstructionSettingEnum(_index++, "Enlarge reconstruction region", null);
  //Swee Yee - add enable BGA inspection region size reduced to 1x1 checkbox
  public static ProjectReconstructionSettingEnum SET_BGA_INSPECTION_REGION_TO_1X1 = new ProjectReconstructionSettingEnum(_index++, "Joint-Based BGA inspection", null);
  public static ProjectReconstructionSettingEnum NEW_SCAN_ROUTE_GENERATION = new ProjectReconstructionSettingEnum(_index++, "New Scan Route generation", null);
  /**
   *
   * @author swee-yee.wong
   */
  private ProjectReconstructionSettingEnum(int id, String name, java.util.List<String> selectionList)
  {
    super(id);
    Assert.expect(name != null);

    _name = name.intern();
    _selectionList = selectionList;
  }
  
  /**
   *
   * @author swee-yee.wong
   */
  public String getName()
  {
    Assert.expect(_name != null);
    return _name;
  }
  
  /**
   *
   * @author swee-yee.wong
   */
  public java.util.List<String> getSelectionList()
  {
    return _selectionList;
  }
  
  /**
   *
   * @author swee-yee.wong
   */
  public static java.util.List<ProjectReconstructionSettingEnum> getAllProjectReconstructionSettingList()
  {
    if (_projectReconstructionSettingEnumList != null)
      _projectReconstructionSettingEnumList.clear();
    
    _projectReconstructionSettingEnumList.add(ENLARGE_RECONSTRUCTION_REGION);
    _projectReconstructionSettingEnumList.add(SET_BGA_INSPECTION_REGION_TO_1X1);
    _projectReconstructionSettingEnumList.add(NEW_SCAN_ROUTE_GENERATION);
    
    return _projectReconstructionSettingEnumList;
  }
}

