package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.ComponentType;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.XrayCameraIdEnum;

/**
 * @author wei-chin.chong
 */
public class SubtypeAdvanceSettings implements Serializable
{
  private static transient ProjectObservable _projectObservable;

  private Subtype _subtype;
  
  private SignalCompensationEnum _signalCompensation = SignalCompensationEnum.DEFAULT_LOW;  // Intergration Level, IL
  private ArtifactCompensationStateEnum _artifactCompensationState ; // Interferences Compensation, IC
  private boolean _isInterferenceCompensatable = false;
  private UserGainEnum _userGain = UserGainEnum.ONE;
 
  private MagnificationTypeEnum _magnificationType = MagnificationTypeEnum.LOW;
  private boolean _canUseVariableMagnification = true;  
  private static transient ProjectHistoryLog _fileWriter;
  
  private List<StageSpeedEnum> _stageSpeed = new ArrayList<StageSpeedEnum>();
  //Siew Yeng - XCR-2140 - DRO
  private DynamicRangeOptimizationLevelEnum _droLvel = DynamicRangeOptimizationLevelEnum.ONE;
  private boolean _isAutoExposureSettingLearnt = false;
 
  private List<Integer> _cameraEnabledList = new ArrayList<Integer>();

  /**
   * @author Wei Chin
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
    _fileWriter = ProjectHistoryLog.getInstance(); 
  }

  /**
   * @author Wei Chin
   */
  public SubtypeAdvanceSettings()
  {    
    _cameraEnabledList.add(XrayCameraIdEnum.XRAY_CAMERA_0.getId());
    _cameraEnabledList.add(XrayCameraIdEnum.XRAY_CAMERA_1.getId());
    _cameraEnabledList.add(XrayCameraIdEnum.XRAY_CAMERA_2.getId());
    _cameraEnabledList.add(XrayCameraIdEnum.XRAY_CAMERA_3.getId());
    _cameraEnabledList.add(XrayCameraIdEnum.XRAY_CAMERA_4.getId());
    _cameraEnabledList.add(XrayCameraIdEnum.XRAY_CAMERA_5.getId());
    _cameraEnabledList.add(XrayCameraIdEnum.XRAY_CAMERA_6.getId());
    _cameraEnabledList.add(XrayCameraIdEnum.XRAY_CAMERA_7.getId());
    _cameraEnabledList.add(XrayCameraIdEnum.XRAY_CAMERA_8.getId());
    _cameraEnabledList.add(XrayCameraIdEnum.XRAY_CAMERA_9.getId());
    _cameraEnabledList.add(XrayCameraIdEnum.XRAY_CAMERA_10.getId());
    _cameraEnabledList.add(XrayCameraIdEnum.XRAY_CAMERA_11.getId());
    _cameraEnabledList.add(XrayCameraIdEnum.XRAY_CAMERA_12.getId());
    _cameraEnabledList.add(XrayCameraIdEnum.XRAY_CAMERA_13.getId());

    _projectObservable.stateChanged(this, null, this, SubtypeAdvanceSettingsEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Wei Chin
   */
  public void destroy()
  {
    _projectObservable.stateChanged(this, this, null, SubtypeAdvanceSettingsEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @return the _subtype
   * @author Wei Chin
   */
  public Subtype getSubtype()
  {
    Assert.expect(_subtype != null);
    return _subtype;
  }

  /**
   * @param subtype the _subtype to set
   * @author Wei Chin
   */
  public void setSubtype(Subtype subtype)
  {
    Assert.expect(subtype != null);
    _subtype = subtype;
  }

  /**
   * @return the _signalCompensation
   * @author Wei Chin
   */
  public SignalCompensationEnum getSignalCompensation()
  {
    Assert.expect(_signalCompensation != null);
    
    // Edited by Lee Herng 25-Nov-2014: Somehow it caused intermittent assert when calling getProject().
    //                                  Temporary disable it.
    //
    // Added by Khang Wah, 2013-08-21. New scan route, IL6 & IL7 is only available for new scan route
    // it will automatic change to IL5 when detected in old scan path
    //if(_subtype.getPanel().getProject().isGenerateByNewScanRoute()==false)
    //{
    //  if(_signalCompensation.equals(SignalCompensationEnum.IL_6) ||
    //     _signalCompensation.equals(SignalCompensationEnum.IL_7))
    //    _signalCompensation = SignalCompensationEnum.IL_5;
    //}
      
    return _signalCompensation;
  }

  /**
   * @param signalCompensation the _signalCompensation to set
   * @author Wei Chin
   */
  public void setSignalCompensation(SignalCompensationEnum signalCompensation)
  {
    Assert.expect(signalCompensation != null);

    if (_signalCompensation != null
      && _signalCompensation.equals(signalCompensation))
    {
      return;
    }

    SignalCompensationEnum oldValue = _signalCompensation;
    _projectObservable.setEnabled(false);

    _signalCompensation = signalCompensation;

    _projectObservable.setEnabled(true);
    _projectObservable.stateChanged(this, oldValue, signalCompensation, SubtypeAdvanceSettingsEventEnum.SIGNAL_COMPENSATION_ENUM);
    _fileWriter.appendSubtype(_subtype, oldValue, signalCompensation);
  }

  /**
   * @return the _artifactCompensationState
   * @author Wei Chin
   */
  public ArtifactCompensationStateEnum getArtifactCompensationState()
  {
    Assert.expect(_artifactCompensationState != null);
    if(isIsInterferenceCompensatable())
      return _artifactCompensationState;
    else
      return ArtifactCompensationStateEnum.NOT_APPLICABLE;
  }

  /**
   * @param artifactCompensationState the _artifactCompensationState to set
   * @author Wei Chin
   */
  public void setArtifactCompensationState(ArtifactCompensationStateEnum artifactCompensationState)
  {
    Assert.expect(artifactCompensationState != null);
    Assert.expect(artifactCompensationState.equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false);

    if (_artifactCompensationState != null
      && _artifactCompensationState.equals(artifactCompensationState))
    {
      return;
    }

    ArtifactCompensationStateEnum oldValue = _artifactCompensationState;
    _projectObservable.setEnabled(false);

    _artifactCompensationState = artifactCompensationState;
    _projectObservable.setEnabled(true);

    _projectObservable.stateChanged(this, oldValue, artifactCompensationState, SubtypeAdvanceSettingsEventEnum.ARTIFACT_COMPENSATION_STATE_ENUM);
    _fileWriter.appendSubtype(_subtype, oldValue, artifactCompensationState);
  }

  /**
   * @return the _isInterferenceCompensatable
   * @author Wei Chin
   */
  public boolean isIsInterferenceCompensatable()
  {
    return _isInterferenceCompensatable;
  }

  /**
   * @param isInterferenceCompensatable the _isInterferenceCompensatable to set
   * @author Wei Chin
   */
  public void setIsInterferenceCompensatable(boolean isInterferenceCompensatable)
  {
    _isInterferenceCompensatable = isInterferenceCompensatable;
  }

  /**
   * @return the _userGain
   * @author Wei Chin
   */
  public UserGainEnum getUserGain()
  {
    Assert.expect(_userGain != null);
    return _userGain;
  }

  /**
   * @param userGain the _userGain to set
   * @author Wei Chin
   */
  public void setUserGain(UserGainEnum userGain)
  {
    Assert.expect(userGain != null);

    if (_userGain != null && _userGain.equals(userGain))
    {
      return;
    }
    UserGainEnum oldValue = _userGain;
    _projectObservable.setEnabled(false);

    _userGain = userGain;

    _projectObservable.setEnabled(true);
    _projectObservable.stateChanged(this, oldValue, userGain, SubtypeAdvanceSettingsEventEnum.USER_GAIN_ENUM);
    _fileWriter.appendSubtype(_subtype, oldValue, userGain);
  }
  
  /**
   * @author Yong Sheng Chuan 
   */
  public List<StageSpeedEnum> getStageSpeedList()
  {
    Assert.expect(_stageSpeed != null);
    return _stageSpeed;
  }
  
  /**
   * @Author Kee Chin Seong
   * @return 
   */
  public List<Integer> getCameraEnabledIDList()
  {
     return _cameraEnabledList;
  }
  
  /**
   * Author Kee Chin Seong
   * @param cameraIDEnum
   * @param isEnabled 
   */
  public void setEnabledCamera(List<Integer> enabledCameraIdList)
  {
     Assert.expect(_cameraEnabledList != null);
     
     _cameraEnabledList = enabledCameraIdList;
  }
  
  /**
   * XCR-3843, Software crashed when run production with  different stage speed
   * @author Yong Sheng Chuan
   */
  public StageSpeedEnum getHighestStageSpeedSetting()
  {
    StageSpeedEnum stageSpeedEnum = StageSpeedEnum.getLowestStageSpeed();
    for (StageSpeedEnum speed : getStageSpeedList())
    {
      if (speed.toDouble() > stageSpeedEnum.toDouble())
        stageSpeedEnum = speed;
    }
    return stageSpeedEnum;
  }
  
  /**
   * XCR-3843, Software crashed when run production with  different stage speed
   * @author Yong Sheng Chuan
   */
  public StageSpeedEnum getLowestStageSpeedSetting()
  {
    StageSpeedEnum stageSpeedEnum = StageSpeedEnum.getHighestStageSpeed();
    for (StageSpeedEnum speed : getStageSpeedList())
    {
      if (speed.toDouble() < stageSpeedEnum.toDouble())
        stageSpeedEnum = speed;
    }
    return stageSpeedEnum;
  }
  
  /**
   * @author Yong Sheng Chuan 
   */
  public void setStageSpeedEnum(List<StageSpeedEnum> stageSpeedList)
  {
    Assert.expect(stageSpeedList != null);

    if (_stageSpeed.size() == stageSpeedList.size() && _stageSpeed.containsAll(stageSpeedList))
      return;
    
    List<StageSpeedEnum> oldValue = new ArrayList<StageSpeedEnum>(_stageSpeed);
    _projectObservable.setEnabled(false);

    _stageSpeed = stageSpeedList;

    _projectObservable.setEnabled(true);
    _projectObservable.stateChanged(this, oldValue, _stageSpeed, SubtypeAdvanceSettingsEventEnum.STAGE_SPEED_ENUM);
  }
  
  /**
   * @author Sheng Chuan
   */
  public void addStageSpeedEnum(StageSpeedEnum stageSpeed)
  {
    Assert.expect(stageSpeed != null);

    if(_stageSpeed.contains(stageSpeed))
      return;
    
    List<StageSpeedEnum> oldValue = new ArrayList<StageSpeedEnum>(_stageSpeed);
    _projectObservable.setEnabled(false);

    _stageSpeed.add(stageSpeed);

    _projectObservable.setEnabled(true);
    _projectObservable.stateChanged(this, oldValue, _stageSpeed, SubtypeAdvanceSettingsEventEnum.STAGE_SPEED_ENUM);
  }
  
  /**
   * @author Sheng Chuan
   */
  public void removeStageSpeedEnum(StageSpeedEnum stageSpeed)
  {
    Assert.expect(stageSpeed != null);

    if(_stageSpeed.contains(stageSpeed) == false)
      return;
    
    List<StageSpeedEnum> oldValue = new ArrayList<StageSpeedEnum>(_stageSpeed);
    
    _projectObservable.setEnabled(false);

    _stageSpeed.remove(stageSpeed);

    _projectObservable.setEnabled(true);
    _projectObservable.stateChanged(this, oldValue, _stageSpeed, SubtypeAdvanceSettingsEventEnum.STAGE_SPEED_ENUM);
  }

  /**
   * @return the _magnification
   * @author Wei Chin
   */
  public MagnificationTypeEnum getMagnificationType()
  {
    Assert.expect(_magnificationType != null);
    return _magnificationType;
  }

  /**
   * @param magnification the _magnification to set
   * @author Wei Chin
   */
  public void setMagnificationType(MagnificationTypeEnum magnification)
  {
    Assert.expect(magnification != null);

    if (_magnificationType != null && _magnificationType.equals(magnification))
    {
      return;
    }
    MagnificationTypeEnum oldValue = _magnificationType;
    _projectObservable.setEnabled(false);

    _magnificationType = magnification;

    _projectObservable.setEnabled(true);
    _projectObservable.stateChanged(this, oldValue, magnification, SubtypeAdvanceSettingsEventEnum.MAGNIFICATION_TYPE_ENUM);
    _fileWriter.appendSubtype(_subtype, oldValue, magnification);
  }

  /**
   * Create a new SubtypeAdvanceSettings for the Subtype passed in. 
   * @author Wei Chin
   */
  public static SubtypeAdvanceSettings createNewSubtypeAdvanceSettings(Subtype subtype)
  {
    Assert.expect(subtype!= null);
    
    SubtypeAdvanceSettings subtypeAdvanceSettings = new SubtypeAdvanceSettings();
    subtypeAdvanceSettings.setSubtype(subtype);
    subtypeAdvanceSettings.setSignalCompensation(SignalCompensationEnum.DEFAULT_LOW);
    subtypeAdvanceSettings.setIsInterferenceCompensatable(false);
    subtypeAdvanceSettings.setArtifactCompensationState(ArtifactCompensationStateEnum.NOT_COMPENSATED);
    subtypeAdvanceSettings.setUserGain(UserGainEnum.ONE);
    subtypeAdvanceSettings.addStageSpeedEnum(StageSpeedEnum.ONE);
    subtypeAdvanceSettings.setMagnificationType(MagnificationTypeEnum.LOW);
    subtypeAdvanceSettings.setCanUseVariableMagnification(true);
    subtypeAdvanceSettings.setDynamicRangeOptimizationLevel(DynamicRangeOptimizationLevelEnum.ONE);//Siew Yeng - XCR-2140 - DRO
    
    return subtypeAdvanceSettings;
  }

  /**
   * @return the _canUseVariableMagnification
   */
  public boolean canUseVariableMagnification()
  {
    return _canUseVariableMagnification;
  }

  /**
   * @param canUseVariableMagnification the _canUseVariableMagnification to set
   * @author Wei Chin
   */
  public void setCanUseVariableMagnification(boolean canUseVariableMagnification)
  {
    // XCR-2725 Unable to switch to High Mag on Single Pad - Cheah Lee Herng
    _canUseVariableMagnification = canUseVariableMagnification;
    if(_canUseVariableMagnification == false)
      setMagnificationType(MagnificationTypeEnum.LOW);
  }
  
  /**
   * @param droLevel the _drolevel to set
   * @author Siew Yeng
   */
  public void setDynamicRangeOptimizationLevel(DynamicRangeOptimizationLevelEnum droLevel)
  {
    Assert.expect(droLevel != null);

    if (_droLvel != null && _droLvel.equals(droLevel))
    {
      return;
    }
    DynamicRangeOptimizationLevelEnum oldValue = _droLvel;
    _projectObservable.setEnabled(false);

    _droLvel = droLevel;

    _projectObservable.setEnabled(true);
    _projectObservable.stateChanged(this, oldValue, droLevel, SubtypeAdvanceSettingsEventEnum.DYNAMIC_RANGE_OPTIMIZATION_LEVEL_ENUM);
  }
  
  /**
   * @return the _droLvel
   * @author Siew Yeng
   */
  public DynamicRangeOptimizationLevelEnum getDynamicRangeOptimizationLevel()
  {
    Assert.expect(_droLvel != null);
    return _droLvel;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setIsAutoExposureSettingLearnt(boolean isOptimized)
  {
    _isAutoExposureSettingLearnt = isOptimized;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public boolean isAutoExposureSettingLearnt()
  {
    return _isAutoExposureSettingLearnt;
  }
}
