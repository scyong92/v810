package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import java.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 *
 * @author swee-yee.wong
 */
public class ProjectAlgorithmSettingEnum extends com.axi.util.Enum
{
  private static final java.util.List<String> SOLDER_THICKNESS_VERSION_SELECTION_LIST = new ArrayList<String>();
  private static int _index = 0;
  
  static
  {
    for(int version = 0; version <= Config.getInstance().getIntValue(SoftwareConfigEnum.THICKNESS_TABLE_LATEST_VERSION); version++)
    {
      SOLDER_THICKNESS_VERSION_SELECTION_LIST.add(StringLocalizer.keyToString("MMGUI_SOLDER_THICKNESS_VERSION_REFERENCE_KEY") + String.valueOf(version));
    }
  }
 
  public static ProjectAlgorithmSettingEnum SOLDER_THICKNESS_VERSION_SETTING = new ProjectAlgorithmSettingEnum(_index++, "Select thickness table version", SOLDER_THICKNESS_VERSION_SELECTION_LIST);
  
  //Swee Yee - moved to subtype level
  //Swee Yee - add enable BGA joint based threshold inspection checkbox
  //public static ProjectAlgorithmSettingEnum ENABLE_BGA_JOINT_BASED_THRESHOLD_INSPECTION = new ProjectAlgorithmSettingEnum(_index++, "Enable BGA joint based threshold inspection", null);
  
  private String _name;
  private static java.util.List<ProjectAlgorithmSettingEnum> _projectAlgorithmSettingEnumList = new ArrayList<ProjectAlgorithmSettingEnum>();
  private java.util.List<String> _selectionList = new ArrayList<String>();
  
  /**
   *
   * @author swee-yee.wong
   */
  private ProjectAlgorithmSettingEnum(int id, String name, java.util.List<String> selectionList)
  {
    super(id);
    Assert.expect(name != null);

    _name = name.intern();
    _selectionList = selectionList;
  }
  
  /**
   *
   * @author swee-yee.wong
   */
  public String getName()
  {
    Assert.expect(_name != null);
    return _name;
  }
  
  /**
   *
   * @author swee-yee.wong
   */
  public java.util.List<String> getSelectionList()
  {
    return _selectionList;
  }
  
  /**
   *
   * @author swee-yee.wong
   */
  public static java.util.List<ProjectAlgorithmSettingEnum> getAllProjectAlgorithmSettingList()
  {
    if (_projectAlgorithmSettingEnumList != null)
      _projectAlgorithmSettingEnumList.clear();
    
    _projectAlgorithmSettingEnumList.add(SOLDER_THICKNESS_VERSION_SETTING);
    //_projectAlgorithmSettingEnumList.add(ENABLE_BGA_JOINT_BASED_THRESHOLD_INSPECTION);
    
    return _projectAlgorithmSettingEnumList;
  }
}



