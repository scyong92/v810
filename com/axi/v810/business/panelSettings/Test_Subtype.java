package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Peter Esbensen
 */
class Test_Subtype extends UnitTest
{
  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute (new Test_Subtype());
  }

  /**
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    final String SUBTYPE_ID = "SubtypeID123";

    Panel panel = new Panel();
    Subtype subtype = new Subtype();
    subtype.setPanel(panel);
    subtype.setInspectionFamilyEnum(InspectionFamilyEnum.CHIP);
    subtype.setShortName(SUBTYPE_ID);

    Project project = loadProjectFromNdfs();

    Pad pad = project.getPanel().getBoards().get(0).getComponents().get(0).getPadOne();

    subtype.setInspectionFamilyEnum(InspectionFamilyEnum.GRID_ARRAY);

    // check id
    Expect.expect(subtype.getShortName().equals(SUBTYPE_ID));

    // check InspectionFamily
    InspectionFamily inspectionFamily = subtype.getInspectionFamily();
    Expect.expect(inspectionFamily == InspectionFamily.getInstance(InspectionFamilyEnum.GRID_ARRAY));
    Expect.expect(inspectionFamily.getInspectionFamilyEnum().equals(InspectionFamilyEnum.GRID_ARRAY));

    checkLearningFlags();
  }

  private Project loadProjectFromNdfs()
  {
    String panelName = "Test_Panel_1";
    try
    {
      String serializedProjectPath = FileName.getProjectSerializedFullPath(panelName);
      if(FileUtil.exists(serializedProjectPath))
        FileUtil.delete(serializedProjectPath);
    }
    catch (CouldNotDeleteFileException ex)
    {
      ex.printStackTrace();
      Expect.expect(false);
    }

    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    Project project = null;
    try
    {
      project = Project.importProjectFromNdfs(panelName, warnings);
    }
    catch (XrayTesterException xte)
    {
      Expect.expect(false);
      xte.printStackTrace();
    }
    Expect.expect(project != null);
    Expect.expect(warnings.size() == 0);
    return project;
  }

  /**
   * @author Sunit Bhalla
   */
  private void checkLearningFlags()
  {
    Subtype subtype = new Subtype();

    subtype.setAlgorithmSettingsLearned(true);
    Expect.expect(subtype.areAlgorithmSettingsLearned()==true);

    subtype.setJointSpecificDataLearned(true);
    Expect.expect(subtype.isJointSpecificDataLearned()==true);
  }

}
