package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Cheah Lee Herng
 */
public class BoardAlignmentSurfaceMapEventEnum extends ProjectChangeEventEnum 
{
    private static int _index = -1;
    public static BoardAlignmentSurfaceMapEventEnum CREATE_OR_DESTROY = new BoardAlignmentSurfaceMapEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
    public static BoardAlignmentSurfaceMapEventEnum ADD_SURFACE_MAP_POINTS = new BoardAlignmentSurfaceMapEventEnum(++_index);
    public static BoardAlignmentSurfaceMapEventEnum ADD_OPTICAL_REGION = new BoardAlignmentSurfaceMapEventEnum(++_index);
    public static BoardAlignmentSurfaceMapEventEnum REMOVE_OPTICAL_REGION = new BoardAlignmentSurfaceMapEventEnum(++_index);
    
    /**
     * @author CHeah Lee Herng
    */
    private BoardAlignmentSurfaceMapEventEnum(int id)
    {
        super(id);
    }

    /**
    * @author Bill Darbie
    */
    private BoardAlignmentSurfaceMapEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
    {
        super(id, projectChangeTypeEnum);
        Assert.expect(projectChangeTypeEnum != null);
    }
}
