package com.axi.v810.business.panelSettings;

import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class ProjectStateEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static ProjectStateEventEnum TEST_PROGRAM_VALID = new ProjectStateEventEnum(++_index);
  public static ProjectStateEventEnum TEST_PROGRAM_INVALID = new ProjectStateEventEnum(++_index);

  public static ProjectStateEventEnum PROJECT_MAJOR_STATE_VALID = new ProjectStateEventEnum(++_index);
  public static ProjectStateEventEnum PROJECT_MAJOR_STATE_INVALID = new ProjectStateEventEnum(++_index);

  public static ProjectStateEventEnum PROJECT_MINOR_STATE_VALID = new ProjectStateEventEnum(++_index);
  public static ProjectStateEventEnum PROJECT_MINOR_STATE_INVALID = new ProjectStateEventEnum(++_index);

  public static ProjectStateEventEnum VERIFICATION_IMAGES_VALID = new ProjectStateEventEnum(++_index);
  public static ProjectStateEventEnum VERIFICATION_IMAGES_INVALID = new ProjectStateEventEnum(++_index);

  public static ProjectStateEventEnum ALIGNMENT_TRANSFORM_VALID = new ProjectStateEventEnum(++_index);
  public static ProjectStateEventEnum ALIGNMENT_TRANSFORM_INVALID = new ProjectStateEventEnum(++_index);

  public static ProjectStateEventEnum SHORT_LEARNING_FILE_RECORDS_VALID = new ProjectStateEventEnum(++_index);
  public static ProjectStateEventEnum SHORT_LEARNING_FILE_RECORDS_INVALID = new ProjectStateEventEnum(++_index);

  public static ProjectStateEventEnum SHORT_LEARNING_FILE_VALID = new ProjectStateEventEnum(++_index);
  public static ProjectStateEventEnum SHORT_LEARNING_FILE_INVALID = new ProjectStateEventEnum(++_index);

  public static ProjectStateEventEnum EXPECTED_IMAGE_LEARNING_FILE_RECORDS_VALID = new ProjectStateEventEnum(++_index);
  public static ProjectStateEventEnum EXPECTED_IMAGE_LEARNING_FILE_RECORDS_INVALID = new ProjectStateEventEnum(++_index);

  public static ProjectStateEventEnum EXPECTED_IMAGE_LEARNING_FILE_VALID = new ProjectStateEventEnum(++_index);
  public static ProjectStateEventEnum EXPECTED_IMAGE_LEARNING_FILE_INVALID = new ProjectStateEventEnum(++_index);
  
  //Lim, Lay Ngor - Broken Pin - START
  public static ProjectStateEventEnum BROKEN_PIN_LEARNING_FILE_RECORDS_VALID = new ProjectStateEventEnum(++_index);
  public static ProjectStateEventEnum BROKEN_PIN_LEARNING_FILE_RECORDS_INVALID = new ProjectStateEventEnum(++_index);

  public static ProjectStateEventEnum BROKEN_PIN_LEARNING_FILE_VALID = new ProjectStateEventEnum(++_index);
  public static ProjectStateEventEnum BROKEN_PIN_LEARNING_FILE_INVALID = new ProjectStateEventEnum(++_index);
  //Lim, Lay Ngor - Broken Pin - END

  //ShengChuan - Clear Tombstone - Start
  public static ProjectStateEventEnum EXPECTED_IMAGE_TEMPLATE_LEARNING_FILE_VALID = new ProjectStateEventEnum(++_index);
  public static ProjectStateEventEnum EXPECTED_IMAGE_TEMPLATE_LEARNING_FILE_INVALID = new ProjectStateEventEnum(++_index);
  
  public static ProjectStateEventEnum EXPECTED_IMAGE_LEARNING_TEMPLATE_FILE_RECORDS_VALID = new ProjectStateEventEnum(++_index);
  public static ProjectStateEventEnum EXPECTED_IMAGE_LEARNING_TEMPALTE_FILE_RECORDS_INVALID = new ProjectStateEventEnum(++_index);
  //ShengChuan - Clear Tombstone - End

  public static ProjectStateEventEnum SUBTYPE_LEARNING_FILE_VALID = new ProjectStateEventEnum(++_index);
  public static ProjectStateEventEnum SUBTYPE_LEARNING_FILE_INVALID = new ProjectStateEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private ProjectStateEventEnum(int id)
  {
    super(id);
  }
}
