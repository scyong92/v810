package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class ComponentTypeSettingsEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static ComponentTypeSettingsEventEnum CREATE_OR_DESTROY = new ComponentTypeSettingsEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static ComponentTypeSettingsEventEnum COMPONENT_TYPE = new ComponentTypeSettingsEventEnum(++_index);
  public static ComponentTypeSettingsEventEnum LOADED = new ComponentTypeSettingsEventEnum(++_index);
  public static ComponentTypeSettingsEventEnum LOADED_LIST = new ComponentTypeSettingsEventEnum(++_index);
  public static ComponentTypeSettingsEventEnum INSPECTED = new ComponentTypeSettingsEventEnum(++_index);
  public static ComponentTypeSettingsEventEnum SUBTYPE = new ComponentTypeSettingsEventEnum(++_index);
  public static ComponentTypeSettingsEventEnum PSP_ENUM = new ComponentTypeSettingsEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private ComponentTypeSettingsEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private ComponentTypeSettingsEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
