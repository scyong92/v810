package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author siew-yeng.phang
 */
public class SerialNumberMappingEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;
  public static SerialNumberMappingEventEnum SERIAL_NUMBER_MAPPING = new SerialNumberMappingEventEnum(++_index);
  public static SerialNumberMappingEventEnum SERIAL_NUMBER_MAPPING_BARCODE_CONFIG_NAME = new SerialNumberMappingEventEnum(++_index);
  
  /**
   * @author Phang Siew Yeng
   */
  private SerialNumberMappingEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Phang Siew Yeng
   */
  private SerialNumberMappingEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
