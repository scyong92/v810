package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;

/**
 *
 * @author kok-chun.tan
 * @editedBy Kee Chin Seong 
 */
public class VariationSetting implements Serializable
{
  private Project _project = null;
  private String _name = null;
  private Map<String, java.util.List<String>> _boardTypeToRefDesMap = new LinkedHashMap<String, java.util.List<String>>();
  private boolean _isEnable = false;
  private boolean _isViewableInProduction = false;
  private transient static ProjectObservable _projectObservable;
  
  /**
   * @author Kok Chun, Tan
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }
  
  /**
   * @author Kok Chun, Tan
   * @author Kee Chin Seong
   * @param name
   * @param boardTypeToRefDesMap 
   */
  public VariationSetting(Project project, String name, boolean enable, boolean isViewableInProduction, Map<String, List<String>> boardTypeToRefDesMap) throws DatastoreException
  {
    Assert.expect(name != null);
    Assert.expect(boardTypeToRefDesMap != null);
    
    _project = project;
    _name = name;
    _boardTypeToRefDesMap = boardTypeToRefDesMap;
    _isViewableInProduction = isViewableInProduction;
    _isEnable = enable;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public String getName()
  {
    Assert.expect(_name != null);
    return _name;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setName(String name)
  {
    Assert.expect(name != null);
    
    if (name.equals(_name))
      return;
    
    String oldValue = _name;
    _projectObservable.setEnabled(false);
    try
    {
      _name = name;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, name, VariationSettingEnum.NAME);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean isEnable()
  {
    return _isEnable;
  }
  
  /**
   * @author Kee Chin Seong
  */
  public boolean isViewableInProduction()
  {
    return _isViewableInProduction;
  }
  
  /**
   * @author Kee Chin Seong
   * @param enable
   * @throws DatastoreException 
   */
  public void setIsViewableInProduction(boolean enable)
  {
    if (enable == _isViewableInProduction)
      return;
    
    boolean oldValue = _isEnable;
    _projectObservable.setEnabled(false);
    try
    {
      _isViewableInProduction = enable;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, enable, VariationSettingEnum.ENABLE_CHOOSABLE_IN_PRODUCTION);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setEnable(boolean enable) throws DatastoreException
  {
    if (enable == _isEnable)
      return;
    
    boolean oldValue = _isEnable;
    _projectObservable.setEnabled(false);
    try
    {
      _isEnable = enable;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, enable, VariationSettingEnum.ENABLE);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public Map<String, List<String>> getBoardTypeToRefDesMap()
  {
    Assert.expect(_boardTypeToRefDesMap != null);
    return _boardTypeToRefDesMap;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void addBoardTypeToRefDesMap(Map<String, List<String>> boardTypeToRefDesMap)
  {
    Assert.expect(boardTypeToRefDesMap != null);
    
    for (Map.Entry<String, List<String>> bordTypeToRefDesSet : boardTypeToRefDesMap.entrySet())
    {
      if (_boardTypeToRefDesMap.containsKey(bordTypeToRefDesSet.getKey()) == false)
      {
        _boardTypeToRefDesMap.put(bordTypeToRefDesSet.getKey(), bordTypeToRefDesSet.getValue());
      }
    }
  }
}
