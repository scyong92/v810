package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;

/**
 * @author Siew Yeng
 */
public class DynamicRangeOptimizationLevelEnum extends com.axi.util.Enum
{
  private static Map<Integer, DynamicRangeOptimizationLevelEnum> _droLevelToEnumMap = new HashMap<Integer, DynamicRangeOptimizationLevelEnum>();
  private static List<DynamicRangeOptimizationLevelEnum> _orderedDroLevel = new ArrayList<DynamicRangeOptimizationLevelEnum>();
  private static List<String> _orderdDroLevelNameList = new ArrayList<String>();
  private int _droLevel;
  private static int _index = -1;
  public static DynamicRangeOptimizationLevelEnum ONE = new DynamicRangeOptimizationLevelEnum(++_index, 1);
  public static DynamicRangeOptimizationLevelEnum TWO = new DynamicRangeOptimizationLevelEnum(++_index, 2);
  public static DynamicRangeOptimizationLevelEnum THREE = new DynamicRangeOptimizationLevelEnum(++_index, 3);
  public static DynamicRangeOptimizationLevelEnum FOUR = new DynamicRangeOptimizationLevelEnum(++_index, 4);
  public static DynamicRangeOptimizationLevelEnum FIVE = new DynamicRangeOptimizationLevelEnum(++_index, 5);
  public static DynamicRangeOptimizationLevelEnum SIX = new DynamicRangeOptimizationLevelEnum(++_index, 6);
  public static DynamicRangeOptimizationLevelEnum SEVEN = new DynamicRangeOptimizationLevelEnum(++_index, 7);
  public static DynamicRangeOptimizationLevelEnum EIGHT = new DynamicRangeOptimizationLevelEnum(++_index, 8);
  public static DynamicRangeOptimizationLevelEnum NINE = new DynamicRangeOptimizationLevelEnum(++_index, 9);
  public static DynamicRangeOptimizationLevelEnum TEN = new DynamicRangeOptimizationLevelEnum(++_index, 10);

    /**
   * @author Siew Yeng
   */
  private DynamicRangeOptimizationLevelEnum(int id, int droLevel)
  {
    super(id);
    _droLevel = droLevel;

    _droLevelToEnumMap.put(droLevel, this);
    _orderedDroLevel.add(this);
    _orderdDroLevelNameList.add(Integer.toString(droLevel));
  }

  /*
   * @author Siew Yeng
   */
  public double toDouble()
  {
    return Double.valueOf(_droLevel);
  }

  /**
   * @author Siew Yeng
   */
  public String toString()
  {
    return String.valueOf(_droLevel);
  }

  /**
   * @author Siew Yeng
   */
  public static DynamicRangeOptimizationLevelEnum getDynamicRangeOptimizationLevelToEnumMapValue(int key)
  {
    Assert.expect(key > 0);

    return _droLevelToEnumMap.get(key);
  }

  /**
   * @author Siew Yeng
   */
  public static List<DynamicRangeOptimizationLevelEnum> getOrderedDynamicRangeOptimizationLevel()
  {
    return new ArrayList<DynamicRangeOptimizationLevelEnum>(_orderedDroLevel);
  }
  
  /**
   * @author Siew Yeng
   */
  public static DynamicRangeOptimizationLevelEnum getHigherDynamicRangeOptimizationLevel(DynamicRangeOptimizationLevelEnum firstDroLevelEnum,
                                                                   DynamicRangeOptimizationLevelEnum secondDroLevelEnum)
  {
    if(firstDroLevelEnum.getId() > secondDroLevelEnum.getId())
      return firstDroLevelEnum;
    else
      return secondDroLevelEnum;
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public static List<String> getOrderDroLevelNameList()
  {
    return _orderdDroLevelNameList;
  }
}

