package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;

/**
 * This class monitors all Project events and keeps version numbers, etc up to date.
 *
 * @author Bill Darbie
 */
public class ProjectState implements Serializable
{
  private static ProjectState _instance;
  private static transient ProjectObservable _projectObservable;
  private static Project _project;

  // set of events that cause test program to be invalid
  private static Map<ProjectChangeEventEnum, Boolean> _projectEventToTestProgramValidMap = new HashMap<ProjectChangeEventEnum, Boolean>();
  // set of events that cause project minor state to be invalid
  private static Map<ProjectEventEnum, Boolean> _projectEventToProjectMinorChangeMap = new HashMap<ProjectEventEnum, Boolean>();
  // set of events that cause test program alignment regions to be invalid
  private static Set<ProjectChangeEventEnum> _projectEventToTestProgramAlignmentRegionsInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();
  // set of events that cause verification images to be invalid
  private static Set<ProjectChangeEventEnum> _verificationImageInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();

  // set of events that cause the manual alignment transform to be invalid
  private static Set<ProjectChangeEventEnum> _alignmentTransformInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();
  private static Set<ProjectChangeEventEnum> _boardSpecificAlignmentTransformInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();
  private static Set<ProjectChangeEventEnum> _componentSpecificAlignmentTransformInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();
  private static Set<ProjectChangeEventEnum> _componentTypeSpecificAlignmentTransformInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();
  private static Set<ProjectChangeEventEnum> _landPatternSpecificAlignmentTransformInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();
  private static Set<ProjectChangeEventEnum> _landPatternPadSpecificAlignmentTransformInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();

  // set of events that cause short learning file records to be invalid
  private static Set<ProjectChangeEventEnum> _shortLearningRecordsInFileInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();
  // set of events that indicate short learning has been deleted.
  private static Set<ProjectChangeEventEnum> _shortLearningRecordsInFileValidEventSet = new HashSet<ProjectChangeEventEnum>();
  // set of events that cause short learning file records to be either valid of invalid
  private static Set<ProjectChangeEventEnum> _shortLearningRecordsInFileValidatingOrInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();
  // set of events that invalidate the short learning file contents
  private static Set<ProjectChangeEventEnum> _shortLearningFileInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();

  // set of events that cause expected image short learning file records to be invalid
  private static Set<ProjectChangeEventEnum> _expectedImageLearningRecordsInFileInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();
  // set of events that indicate expected image learning has been deleted.
  private static Set<ProjectChangeEventEnum> _expectedImageLearningRecordsInFileValidEventSet = new HashSet<ProjectChangeEventEnum>();
  // set of events that cause expected image learning file records to be either valid of invalid
  private static Set<ProjectChangeEventEnum> _expectedImageLearningRecordsInFileValidatingOrInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();
  // set of events that invalidate the expected image learning file contents
  private static Set<ProjectChangeEventEnum> _expectedImageLearningFileInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();

  //added by sheng chuan - Clear Tombstone for template match - START
  // set of events that cause expected image template learning file records to be invalid
  private static Set<ProjectChangeEventEnum> _expectedImageTemplateLearningRecordsInFileInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();
  // set of events that indicate expected image template learning has been deleted.
  private static Set<ProjectChangeEventEnum> _expectedImageTemplateLearningRecordsInFileValidEventSet = new HashSet<ProjectChangeEventEnum>();
  // set of events that indicate expected image template learning has been deleted.
  private static Set<ProjectChangeEventEnum> _expectedImageTemplateLearningFileInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();
  // set of events that cause expected image template learning file records to be either valid of invalid
  private static Set<ProjectChangeEventEnum> _expectedImageTemplateLearningRecordsInFileValidatingOrInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();


  //added by sheng chuan - Clear Tombstone for template match - End

  //Lim, Lay Ngor - Broken Pin - START
  // set of events that cause short learning file records to be invalid
  private static Set<ProjectChangeEventEnum> _brokenPinLearningRecordsInFileInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();
  // set of events that indicate short learning has been deleted.
  private static Set<ProjectChangeEventEnum> _brokenPinLearningRecordsInFileValidEventSet = new HashSet<ProjectChangeEventEnum>();
  // set of events that cause short learning file records to be either valid of invalid
  private static Set<ProjectChangeEventEnum> _brokenPinLearningRecordsInFileValidatingOrInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();
  // set of events that invalidate the short learning file contents
  private static Set<ProjectChangeEventEnum> _brokenPinLearningFileInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();
  //Lim, Lay Ngor - Broken Pin - END
  
  // set of events that invalidate the subtype learning file contents
  private static Set<ProjectChangeEventEnum> _subtypeLearningFileInvalidatingEventSet = new HashSet<ProjectChangeEventEnum>();

  // set of events that cause the determination of a potential long program to be invalid
  private static Set<ProjectChangeEventEnum> _potentialLongProgramInvalidEventSet = new HashSet<ProjectChangeEventEnum>();

  // set of events that cause the test program check sum to be invalid
  private static Set<ProjectChangeEventEnum> _testProgramCheckSumInvalidEventSet = new HashSet<ProjectChangeEventEnum>();;

  // this tracks only test program state changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToTestProgramEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks only test program algignment regions state changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToTestProgramAlignmentRegionsEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks only project major state changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToMajorEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks only project minor state changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToMinorEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks the verification images state changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToVerificationImagesEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks the manual alignment transform state changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToManualAlignmentTransformEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks the short learning file record state changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToShortLearningInvalidRecordsInFileEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks the short learning file stat changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToShortLearningInvalidFileEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks the expected image learning file record state changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToExpectedImageLearningInvalidRecordsInFileEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks the expected image learning file stat changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToExpectedImageLearningInvalidFileEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  //Lim, Lay Ngor - Broken Pin - START
  // this tracks the Broken Pin learning file record state changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToBrokenPinLearningInvalidRecordsInFileEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks the Broken Pin learning file stat changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToBrokenPinLearningInvalidFileEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  //Lim, Lay Ngor - Broken Pin - END

  //Sheng chuan - Clear Tombstone for template match - Start
  // this tracks the subtype learning file stat changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToExpectedImageTemplateLearningInvalidRecordsInFileEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks the expected image learning file stat changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToExpectedImageTemplateLearningInvalidFileEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  //Sheng chuan - Clear Tombstone for template match - End

  // this tracks the subtype learning file stat changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToSubtypeLearningInvalidFileEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks the long panel state changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToPotentialLongProgramEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks the test program check sum state changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToTestProgramCheckSumEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks only test program user gain state changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToTestProgramUserGainEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks only test program user stage speed changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToTestProgramStageSpeedEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  // this tracks only test program board z-offset state changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToTestProgramBoardZOffsetEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  
  // this track only test program IL, IC changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToTestProgramSignalOrArtifactCompensationEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  
  // this tracks the Tuned slice height file record state changes
  private Map<Object, Map<ProjectChangeEventEnum, Object>> _classToTunedSliceHeightInvalidRecordsInFileEventToOrigValueMap = new HashMap<Object, Map<ProjectChangeEventEnum, Object>>();
  
  private int _enabled = -1;
  private boolean _enableProjectStateEvents = true;

  private boolean _testProgramValid;
  private boolean _projectMajorStateValid;
  private boolean _projectMinorStateValid;
  private boolean _verificationImagesValid;
  private boolean _alignmentTransformValid;
  private boolean _shortLearningRecordsInFileValid;
  private boolean _shortLearningFileValid;
  private boolean _expectedImageLearningRecordsInFileValid;
  private boolean _expectedImageLearningFileValid;
  private boolean _brokenPinLearningRecordsInFileValid; //Broken Pin
  private boolean _brokenPinLearningFileValid; //Broken Pin
  private boolean _expectedImageTemplateLearningRecordsInFileValid;//ShengChuan - Clear Tobmstone
  private boolean _expectedImageTemplateLearningFileValid;//ShengChuan - Clear Tobmstone
  private boolean _subtypeLearningFileValid;

  // test program and manual alignment state can be anything on project load
  // these variables get set to the proper state on project load or import
  private boolean _testProgramValidOnProjectLoad;
  private boolean _testProgramAlignmentRegionsValidOnProjectLoad;
  private boolean _manualAlignmentTransformValidOnProjectLoad;
  private boolean _potentialLongProgramDeterminedOnProjectLoad;

  /**
   * @author Bill Darbie
   */
  static
  {
    initProjectChangeEventToTestProgramValidMap();
    initProjectEventToProjectMinorChangeMap();
    initProjectEventToTestProgramAlignmentRegionsInvalidatingEvents();
    initVerificationImageInvalidatingEvents();
    initAlignmentTransformInvalidatingEvents();
    initShortsLearningRecordsInFileInvalidatingEvents();
    initShortsLearningRecordsInFileValidEvents();
    initShortLearningRecordsInFileValidatingOrInvalidatingEvents();
    initShortLearningFileInvalidatingEvents();
    initExpectedImageLearningRecordsInFileInvalidatingEvents();
    initExpectedImageLearningRecordsInFileValidEvents();
    initExpectedImageLearningRecordsInFileValidatingOrInvalidatingEvents();
    initExpectedImageLearningFileInvalidatingEvents();
    initBrokenPinLearningRecordsInFileInvalidatingEvents();//Broken Pin
    initBrokenPinLearningRecordsInFileValidEvents();//Broken Pin
    initBrokenPinLearningRecordsInFileValidatingOrInvalidatingEvents();//Broken Pin
    initBrokenPinLearningFileInvalidatingEvents();//Broken Pin

    //added by sheng-chuan - Clear Tombstone
    initExpectedImageTemplateLearningRecordsInFileInvalidatingEvents();
    initExpectedImageTemplateLearningRecordsInFileValidEvents();
    initExpectedImageTemplateLearningRecordsInFileValidatingOrInvalidatingEvents();
    initExpectedImageTemplateLearningFileInvalidatingEvents();
    
    initSubtypeLearningFileInvalidatingEvents();
    initPotentialLongProgramInvalidEvents();
    initTestProgramCheckSumInvalidEvents();
  }

  /**
   * @author Bill Darbie
   */
  public ProjectState()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  private static void initProjectChangeEventToTestProgramValidMap()
  {
    int numEnums = 0;
    int prevNumEnums = 0;

    addToProjectChangeEventToTestProgramValidMap(BoardEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(BoardEventEnum.ADD_OR_REMOVE, false);
    addToProjectChangeEventToTestProgramValidMap(BoardEventEnum.BOARD_TYPE, false);
    addToProjectChangeEventToTestProgramValidMap(BoardEventEnum.NAME, true);
    addToProjectChangeEventToTestProgramValidMap(BoardEventEnum.DEGREES_ROTATION, false);
    addToProjectChangeEventToTestProgramValidMap(BoardEventEnum.COORDINATE, false);
    addToProjectChangeEventToTestProgramValidMap(BoardEventEnum.LEFT_TO_RIGHT_FLIP, false);
    addToProjectChangeEventToTestProgramValidMap(BoardEventEnum.TOP_TO_BOTTOM_FLIP, false);
    addToProjectChangeEventToTestProgramValidMap(BoardEventEnum.SIDE_BOARD_1, false);
    addToProjectChangeEventToTestProgramValidMap(BoardEventEnum.SIDE_BOARD_2, false);
    addToProjectChangeEventToTestProgramValidMap(BoardEventEnum.BOARD_SETTINGS, false);
    addToProjectChangeEventToTestProgramValidMap(BoardEventEnum.BOARD_SURFACE_MAP_SETTINGS, false);
    addToProjectChangeEventToTestProgramValidMap(BoardEventEnum.BOARD_ALIGNMENT_SURFACE_MAP_SETTINGS, false);
    addToProjectChangeEventToTestProgramValidMap(BoardEventEnum.BOARD_MESH_SETTINGS, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(BoardEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(BoardTypeEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(BoardTypeEventEnum.PANEL, false);
    addToProjectChangeEventToTestProgramValidMap(BoardTypeEventEnum.NAME, true);
    addToProjectChangeEventToTestProgramValidMap(BoardTypeEventEnum.WIDTH, false);
    addToProjectChangeEventToTestProgramValidMap(BoardTypeEventEnum.LENGTH, false);
    addToProjectChangeEventToTestProgramValidMap(BoardTypeEventEnum.ADD_OR_REMOVE_BOARD, false);
    addToProjectChangeEventToTestProgramValidMap(BoardTypeEventEnum.SIDE_BOARD_TYPE_1, false);
    addToProjectChangeEventToTestProgramValidMap(BoardTypeEventEnum.SIDE_BOARD_TYPE_2, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(BoardTypeEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(CompPackageEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(CompPackageEventEnum.PANEL, true);
    addToProjectChangeEventToTestProgramValidMap(CompPackageEventEnum.LONG_NAME, true);
    addToProjectChangeEventToTestProgramValidMap(CompPackageEventEnum.SHORT_NAME, true);
    addToProjectChangeEventToTestProgramValidMap(CompPackageEventEnum.ADD_OR_REMOVE_COMPONENT_TYPE, false);
    addToProjectChangeEventToTestProgramValidMap(CompPackageEventEnum.ADD_OR_REMOVE_PACKAGE_PIN, false);
    addToProjectChangeEventToTestProgramValidMap(CompPackageEventEnum.LAND_PATTERN, false);
    addToProjectChangeEventToTestProgramValidMap(CompPackageEventEnum.JOINT_TYPE_ENUM, false);
    addToProjectChangeEventToTestProgramValidMap(CompPackageEventEnum.EXPORT_COMPLETE_ENUM, true);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(CompPackageEventEnum.class) == numEnums);
    
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    addToProjectChangeEventToTestProgramValidMap(CompPackageOnPackageEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(CompPackageOnPackageEventEnum.ADD_OR_REMOVE, false);
    addToProjectChangeEventToTestProgramValidMap(CompPackageOnPackageEventEnum.PANEL, true);
    addToProjectChangeEventToTestProgramValidMap(CompPackageOnPackageEventEnum.NAME, true);
    addToProjectChangeEventToTestProgramValidMap(CompPackageOnPackageEventEnum.ADD_OR_REMOVE_COMPONENT_TYPE, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(CompPackageOnPackageEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(ComponentEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentEventEnum.COMPONENT_TYPE, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentEventEnum.SIDE_BOARD, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentEventEnum.ADD_OR_REMOVE_PAD, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentEventEnum.CHANGE_SIDE, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(ComponentEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.ADD_OR_REMOVE, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.SIDE_BOARD_TYPE, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.REFERENCE_DESIGNATOR, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.PART_NUMBER, true);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.PART_LOCATION_IN_FACTORY, true);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.COORDINATE, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.CHANGE_SIDE, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.DEGREES_ROTATION, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.COMP_PACKAGE, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.LAND_PATTERN, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.COMPONENT_TYPE_SETTINGS, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.ADD_OR_REMOVE_PAD_TYPE, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.JOINT_TYPE_ENUM, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.SUBTYPE_NAME, false);
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.POP_LAYER_ID, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.POP_ZHEIGHT, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeEventEnum.COMP_PACKAGE_ON_PACKAGE, false);
    
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(ComponentTypeEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(FiducialEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(FiducialEventEnum.ADD_OR_REMOVE, false);
    addToProjectChangeEventToTestProgramValidMap(FiducialEventEnum.SIDE_BOARD, false);
    addToProjectChangeEventToTestProgramValidMap(FiducialEventEnum.PANEL, true);
    addToProjectChangeEventToTestProgramValidMap(FiducialEventEnum.PANEL_SIDE1, false);
    addToProjectChangeEventToTestProgramValidMap(FiducialEventEnum.FIDUCIAL_TYPE, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(FiducialEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(FiducialTypeEventEnum.CREATE_OR_DESTROY, true);
    addToProjectChangeEventToTestProgramValidMap(FiducialTypeEventEnum.ADD_OR_REMOVE, true);
    addToProjectChangeEventToTestProgramValidMap(FiducialTypeEventEnum.NAME, true);
    addToProjectChangeEventToTestProgramValidMap(FiducialTypeEventEnum.COORDINATE, false);
    addToProjectChangeEventToTestProgramValidMap(FiducialTypeEventEnum.WIDTH, false);
    addToProjectChangeEventToTestProgramValidMap(FiducialTypeEventEnum.LENGTH, false);
    addToProjectChangeEventToTestProgramValidMap(FiducialTypeEventEnum.SHAPE_ENUM, false);
    addToProjectChangeEventToTestProgramValidMap(FiducialTypeEventEnum.ADD_OR_REMOVE_FIDUCIAL, true);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(FiducialTypeEventEnum.class) == numEnums);

    // creating or destroying a land pattern by itself does not invalidate the test
    // program.  Adding or removing the LandPattern to or from a ComponentType is.
    addToProjectChangeEventToTestProgramValidMap(LandPatternEventEnum.CREATE_OR_DESTROY, true);
    addToProjectChangeEventToTestProgramValidMap(LandPatternEventEnum.ADD_OR_REMOVE, true);
    addToProjectChangeEventToTestProgramValidMap(LandPatternEventEnum.PANEL, true);
    addToProjectChangeEventToTestProgramValidMap(LandPatternEventEnum.NAME, true);
    addToProjectChangeEventToTestProgramValidMap(LandPatternEventEnum.WIDTH, true);
    addToProjectChangeEventToTestProgramValidMap(LandPatternEventEnum.LENGTH, true);
    addToProjectChangeEventToTestProgramValidMap(LandPatternEventEnum.LOWER_LEFT_COORDINATE, true);
    addToProjectChangeEventToTestProgramValidMap(LandPatternEventEnum.ADD_OR_REMOVE_LAND_PATTERN_PAD, false);
    addToProjectChangeEventToTestProgramValidMap(LandPatternEventEnum.ADD_OR_REMOVE_COMPONENT_TYPE, false);
    addToProjectChangeEventToTestProgramValidMap(LandPatternEventEnum.NUM_ROWS, false);
    addToProjectChangeEventToTestProgramValidMap(LandPatternEventEnum.NUM_COLS, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(LandPatternEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(LandPatternPadEventEnum.CREATE_OR_DESTROY, true);
    addToProjectChangeEventToTestProgramValidMap(LandPatternPadEventEnum.ADD_OR_REMOVE, true);
    addToProjectChangeEventToTestProgramValidMap(LandPatternPadEventEnum.LAND_PATTERN, false);
    addToProjectChangeEventToTestProgramValidMap(LandPatternPadEventEnum.NAME, true);
    addToProjectChangeEventToTestProgramValidMap(LandPatternPadEventEnum.COORDINATE, false);
    addToProjectChangeEventToTestProgramValidMap(LandPatternPadEventEnum.WIDTH, false);
    addToProjectChangeEventToTestProgramValidMap(LandPatternPadEventEnum.LENGTH, false);
    addToProjectChangeEventToTestProgramValidMap(LandPatternPadEventEnum.SHAPE_ENUM, false);
    addToProjectChangeEventToTestProgramValidMap(LandPatternPadEventEnum.DEGREES_ROTATION, false);
    addToProjectChangeEventToTestProgramValidMap(LandPatternPadEventEnum.PITCH, false);
    addToProjectChangeEventToTestProgramValidMap(LandPatternPadEventEnum.INTERPAD_DISTANCE, false);
    addToProjectChangeEventToTestProgramValidMap(LandPatternPadEventEnum.PAD_ORIENTATION, false);
    addToProjectChangeEventToTestProgramValidMap(LandPatternPadEventEnum.PAD_ONE, false);
    addToProjectChangeEventToTestProgramValidMap(LandPatternPadEventEnum.ADD_OR_REMOVE_LIST, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(LandPatternPadEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(PackagePinEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(PackagePinEventEnum.COMP_PACKAGE, false);
    addToProjectChangeEventToTestProgramValidMap(PackagePinEventEnum.LAND_PATTERN_PAD, false);
    addToProjectChangeEventToTestProgramValidMap(PackagePinEventEnum.PAD_ORIENTATION_ENUM, false);
    addToProjectChangeEventToTestProgramValidMap(PackagePinEventEnum.JOINT_TYPE_ENUM, false);
    addToProjectChangeEventToTestProgramValidMap(PackagePinEventEnum.CUSTOM_JOINT_HEIGHT, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(PackagePinEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(PadEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(PadEventEnum.PAD_TYPE, false);
    addToProjectChangeEventToTestProgramValidMap(PadEventEnum.PAD_SETTINGS, false);
    addToProjectChangeEventToTestProgramValidMap(PadEventEnum.NAME, true);
    addToProjectChangeEventToTestProgramValidMap(PadEventEnum.NODE_NAME, true);
    addToProjectChangeEventToTestProgramValidMap(PadEventEnum.COMPONENT, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(PadEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(PadTypeEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(PadTypeEventEnum.COMPONENT_TYPE, false);
    addToProjectChangeEventToTestProgramValidMap(PadTypeEventEnum.ADD_OR_REMOVE_PAD, false);
    addToProjectChangeEventToTestProgramValidMap(PadTypeEventEnum.LAND_PATTERN_PAD, false);
    addToProjectChangeEventToTestProgramValidMap(PadTypeEventEnum.PAD_TYPE_SETTINGS, false);
    addToProjectChangeEventToTestProgramValidMap(PadTypeEventEnum.PACKAGE_PIN, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(PadTypeEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.PROJECT, true);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.NAME, true);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.CAD_VERSION, true);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.WIDTH, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.WIDTH_RETAINING_BOARD_COORDS, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.LENGTH, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.LENGTH_RETAINING_BOARD_COORDS, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.ADD_OR_REMOVE_BOARD, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.ADD_OR_REMOVE_BOARD_TYPE, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.PANEL_SETTINGS, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.THICKNESS, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.ADD_OR_REMOVE_COMP_PACKAGE, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.ADD_OR_REMOVE_FIDUCIAL, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.ADD_OR_REMOVE_LAND_PATTERN, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.ADD_OR_REMOVE_SUBTYPE, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.PANEL_SURFACE_MAP_SETTINGS, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.PANEL_ALIGNMENT_SURFACE_MAP_SETTINGS, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.PSP_SETTINGS, false);
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.PANEL_MESH_SETTINGS, false);
	addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.PANEL_SURFACE_MODEL_SETTINGS, false);
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    addToProjectChangeEventToTestProgramValidMap(PanelEventEnum.ADD_OR_REMOVE_COMP_PACKAGE_ON_PACKAGE, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(PanelEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(SideBoardEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(SideBoardEventEnum.SIDE_BOARD_TYPE, false);
    addToProjectChangeEventToTestProgramValidMap(SideBoardEventEnum.TOP_SIDE, false);
    addToProjectChangeEventToTestProgramValidMap(SideBoardEventEnum.BOARD, false);
    addToProjectChangeEventToTestProgramValidMap(SideBoardEventEnum.ADD_OR_REMOVE_COMPONENT, false);
    addToProjectChangeEventToTestProgramValidMap(SideBoardEventEnum.ADD_OR_REMOVE_FIDUCIAL, false);
    addToProjectChangeEventToTestProgramValidMap(SideBoardEventEnum.MOVE_BY_OFFSET, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(SideBoardEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(SideBoardTypeEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(SideBoardTypeEventEnum.BOARD_TYPE, false);
    addToProjectChangeEventToTestProgramValidMap(SideBoardTypeEventEnum.ADD_OR_REMOVE_COMPONENT_TYPE, false);
    addToProjectChangeEventToTestProgramValidMap(SideBoardTypeEventEnum.SIDE_BOARD_TYPE_SETTINGS, false);
    addToProjectChangeEventToTestProgramValidMap(SideBoardTypeEventEnum.ADD_OR_REMOVE_SIDE_BOARD, false);
    addToProjectChangeEventToTestProgramValidMap(SideBoardTypeEventEnum.ADD_OR_REMOVE_FIDUCIAL_TYPE, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(SideBoardTypeEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(ThroughHoleLandPatternPadEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(ThroughHoleLandPatternPadEventEnum.HOLE_COORDINATE, false);
    addToProjectChangeEventToTestProgramValidMap(ThroughHoleLandPatternPadEventEnum.HOLE_DIAMETER, false);
    //Siew Yeng - XCR-3318 - Oval PTH
    addToProjectChangeEventToTestProgramValidMap(ThroughHoleLandPatternPadEventEnum.HOLE_WIDTH, false);
    addToProjectChangeEventToTestProgramValidMap(ThroughHoleLandPatternPadEventEnum.HOLE_LENGTH, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(ThroughHoleLandPatternPadEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(AlignmentGroupEventEnum.CREATE_OR_DESTROY, true); // alignment
    addToProjectChangeEventToTestProgramValidMap(AlignmentGroupEventEnum.NAME, true);
    addToProjectChangeEventToTestProgramValidMap(AlignmentGroupEventEnum.ADD_OR_REMOVE_PAD, true); // alignment
    addToProjectChangeEventToTestProgramValidMap(AlignmentGroupEventEnum.ADD_OR_REMOVE_FIDUCIAL, true); // alignment
    addToProjectChangeEventToTestProgramValidMap(AlignmentGroupEventEnum.MATCH_QUALITY, true); // alignment
    addToProjectChangeEventToTestProgramValidMap(AlignmentGroupEventEnum.FOREGROUND_GRAY_LEVEL, true); // alignment
    addToProjectChangeEventToTestProgramValidMap(AlignmentGroupEventEnum.BACKGROUND_GRAY_LEVEL, true); // alignment
    addToProjectChangeEventToTestProgramValidMap(AlignmentGroupEventEnum.USE_ZHEIGHT, true); // alignment
    addToProjectChangeEventToTestProgramValidMap(AlignmentGroupEventEnum.SIGNAL_COMPENSATION, false); // alignment
    addToProjectChangeEventToTestProgramValidMap(AlignmentGroupEventEnum.STAGE_SPEED, false); // alignment
    addToProjectChangeEventToTestProgramValidMap(AlignmentGroupEventEnum.DRO_LEVEL, false); // alignment
    addToProjectChangeEventToTestProgramValidMap(AlignmentGroupEventEnum.USER_GAIN, false); // alignment
    addToProjectChangeEventToTestProgramValidMap(AlignmentGroupEventEnum.USE_CUSTOMIZED_ALIGNMENT, false); // alignment
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(AlignmentGroupEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(BoardSettingsEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(BoardSettingsEventEnum.INSPECTED, false);
    addToProjectChangeEventToTestProgramValidMap(BoardSettingsEventEnum.POPULATED, false);
    addToProjectChangeEventToTestProgramValidMap(BoardSettingsEventEnum.ADD_ALIGNMENT_GROUP, false);
    addToProjectChangeEventToTestProgramValidMap(BoardSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED, true);
    addToProjectChangeEventToTestProgramValidMap(BoardSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_GENERATED, true);
    addToProjectChangeEventToTestProgramValidMap(BoardSettingsEventEnum.BOARD_ZOFFSET, true);
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    addToProjectChangeEventToTestProgramValidMap(BoardSettingsEventEnum.SET_MANUAL_ALIGNMENT_MODE, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(BoardSettingsEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(ComponentTypeSettingsEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeSettingsEventEnum.COMPONENT_TYPE, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeSettingsEventEnum.LOADED, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeSettingsEventEnum.LOADED_LIST, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeSettingsEventEnum.INSPECTED, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeSettingsEventEnum.SUBTYPE, false);
    addToProjectChangeEventToTestProgramValidMap(ComponentTypeSettingsEventEnum.PSP_ENUM, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(ComponentTypeSettingsEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(PadSettingsEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(PadSettingsEventEnum.PAD, false);
    addToProjectChangeEventToTestProgramValidMap(PadSettingsEventEnum.TESTABLE, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(PadSettingsEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(PadTypeSettingsEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(PadTypeSettingsEventEnum.PAD_TYPE, false);
    addToProjectChangeEventToTestProgramValidMap(PadTypeSettingsEventEnum.INSPECTED, false);
   // addToProjectChangeEventToTestProgramValidMap(PadTypeSettingsEventEnum.TESTABLE, false);
    addToProjectChangeEventToTestProgramValidMap(PadTypeSettingsEventEnum.SUBTYPE, false);
    addToProjectChangeEventToTestProgramValidMap(PadTypeSettingsEventEnum.GLOBAL_SURFACE_MODEL_STATE_ENUM, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(PadTypeSettingsEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.PANEL, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.ALGORITHM_FAMILY_LIBRARY, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.LEFT_TO_RIGHT_FLIP, false);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.TOP_TO_BOTTOM_FLIP, false);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.SHIFT_X, false);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.SHIFT_Y, false);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_GENERATED, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.ADD_ALIGNMENT_GROUP, false);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.DEGREES_ROTATION, false);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.ADD_OR_REMOVE_NO_TEST_REGION_NAME, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.ADD_OR_REMOVE_UNFOCUSED_REGION_NAME_TO_RETEST, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.ADD_OR_REMOVE_UNFOCUSED_REGION_SLICE_TO_BE_FIXED, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.ADD_OR_REMOVE_UNFOCUSED_REGION_SLICE_TO_Z_OFFSET, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.POTENTIAL_LONG_PROGRAM_DETERMINED, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.ARTIFACT_COMPENSATION_STATE, true);

    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.ALIGNMENT_METHOD, false);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.GLOBAL_SURFACE_MODEL_STATE, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.USER_GAIN, false);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.SPLIT_SETTING, false);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.USE_NEW_THICKNESS_TABLE, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.BYPASS_ALIGNMENT_TRANSFORM_WITHIN_LIMIT_VERIFICATION, true);
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    addToProjectChangeEventToTestProgramValidMap(PanelSettingsEventEnum.SET_ALIGNMENT_MODE, false);
    
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(PanelSettingsEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.CREATE_OR_DESTROY, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.FAST_LOAD, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.SLOW_LOAD, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.UNLOAD, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.SAVE, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.DELETE, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.IMPORT_PROJECT_FROM_NDFS, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.NAME, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.PANEL, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.PROJECT_TEST_PROGRAM_VERSION, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.PROJECT_MINOR_VERSION, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.PROGRAM_GENERATION_VERSION, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.SOFTWARE_VERSION_CREATION, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.NOTES, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.INITIAL_THESHOLDS, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.INITIAL_RECIPE_SETTING, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.DISPLAY_UNITS, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.READY_FOR_PRODUCTION, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.TEST_PROGRAM_UPDATED_WITH_ALIGNMENT_REGIONS, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.TEST_PROGRAM_CLEARING, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.TEST_PROGRAM_GENERATED, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.TEST_PROGRAM_CHECK_SUM_INVALIDATED, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.VERIFICATION_IMAGES_GENERATED, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.LAST_MODIFICATION_TIME, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.PROGRAMMER_NAME, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.TARGET_CUSTOMER_NAME, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.PANEL_IMAGE_CREATION, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.DELETE_SHORTS_LEARNING, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.DELETE_EXPECTED_IMAGE_LEARNING, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.DELETE_SUBTYPE_LEARNING, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.DELETE_EXPECTED_IMAGE_TEMPLATE_LEARNING, true);//ShengChuan - Clear Tombstone
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_SYSTEM_TYPE, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.LOADING_LONG_PANEL_WITH_DIFFERENT_SYSTEM_TYPE, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.SYSTEM_TYPE, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.LIBRARY_IMPORT_COMPLETE, false);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.SCAN_PATH_METHOD, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.TEST_PROGRAM_USER_GAIN_CHANGED, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.TEST_PROGRAM_BOARD_Z_OFFSET_CHANGED, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.TEST_PROGRAM_SIGNAL_OR_ARTIFACT_COMPENSATION_CHANGED, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.TEST_PROGRAM_STAGE_SPEED_CHANGED, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.ENABLE_LARGE_VIEW_IMAGE, false);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.GENERATE_MULTI_ANGLE_IMAGE, false);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.GENERATE_COMPONENT_IMAGE, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.ENLARGE_RECONSTRUCTION_REGION, false);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_LOW_MAGNIFICATION, false);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_MAGNIFICATION_FOR_OFFLINE_PROGRAMMING, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_HIGH_MAGNIFICATION, false);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.LOW_MAGNIFICATION_CHANGED, false);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.HIGH_MAGNIFICATION_CHANGED, false);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.ENABLE_BGA_JOINT_BASED_THRESHOLD_INSPECTION, false);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.SET_BGA_INSPECTION_REGION_TO_1X1, false);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.PROJECT_BASED_BYPASS_MODE, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.SET_THICKNESS_TABLE_VERSION, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.SET_USE_NEW_SCAN_ROUTE_GENERATION, false);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.SET_USE_VARIABLE_DIVN, false);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.SET_DYNAMIC_RANGE_OPTIMIZATION_VERSION, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_THICKNESS_TABLE_VERSION, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.LOADING_PROJECT_WITH_ALTERNATIVE_THICKNESS_TABLE_VERSION, true);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.ENLARGE_ALIGNMENT_REGION, false);
    addToProjectChangeEventToTestProgramValidMap(ProjectEventEnum.IMPORT_LAND_PATTERN_COMPLETE, false);
    
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(ProjectEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(SideBoardTypeSettingsEventEnum.CREATE_OR_DESTROY, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(SideBoardTypeSettingsEventEnum.class) == numEnums);

    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.CREATE_OR_DESTROY, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.PANEL, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.SHORT_NAME, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.LONG_NAME, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.INSPECTION_FAMILY_ENUM, false);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.ADD_OR_REMOVE_ALGORITHM_ENUM, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.ALGORITHM_ENABLED, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.USER_COMMENT, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.JOINT_TYPE_ENUM, false);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.TUNED_VALUE, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.TUNED_SLICE_HEIGHT_VALUE, false);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.ALGORITHM_SETTING_USER_COMMENT, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.ALGORITHM_SETTINGS_LEARNED, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.JOINT_SPECIFIC_DATA_LEARNED, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.EXPECTED_IMAGE_DATA_LEARNED, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.EXPECTED_IMAGE_TEMPLATE_LEARNED, true);//ShengChuan - Clear Tombstone
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.ALL_ALGORITHMS_EXCEPT_SHORT_HAVE_QUESTIONS, true );
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.SHORT_LEARNING_HAS_QUESTIONS, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_ALL_ALGORITHMS_EXCEPT_SHORT, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_SHORT, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_EXPECTED_IMAGE, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_EXPECTED_IMAGE_TEMPLATE, true);//ShengChuan - Clear Tombstone
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.LEARN, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.SET_LEARNED_SETTINGS_TO_DEFAULTS, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.IS_IMPORTED_FROM_LIBRARY, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.IS_MODIFIED_BY_USER, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.IMPORTED_PACKAGE_LIBRARY_PATH, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.VOID_METHOD, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.TUNED_GRAYLEVEL_MIN_MAX_CHANGE, false);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.MASK_IMAGE, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.ADD_OR_REMOVE_SUB_SUBTYPE, true);
    addToProjectChangeEventToTestProgramValidMap(SubtypeEventEnum.SET_ARTIFACT_BASED_REGION_SEPERATION_DIRECTION, false);

    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(SubtypeEventEnum.class) == numEnums);
    
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.CREATE_OR_DESTROY, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.PANEL, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.USER_COMMENT, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.ADD_SURFACE_MAP_POINTS, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.ADD_SURFACE_MAP_VERIFIED, true);  
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.REMOVE_SURFACE_MAP_POINTS, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.REMOVE_SURFACE_MAP_VERIFIED, true); 
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.SET_SURFACE_MAP_COORDINATE, true); 
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.ADD_SURFACE_MAP_OPTICAL_REGION, true); 
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.ADD_OPTICAL_REGION, true);   
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.REMOVE_SURFACE_MAP_REGION, true); 
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.REMOVE_SURFACE_MAP_COMPONENT, true); 
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.ADD_SURFACE_MAP_COMPONENT, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.ADD_SURFACE_MAP_OPTICAL_RECTANGLE, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.ADD_OPTICAL_CAMERA_RECTANGLE, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.REMOVE_OPTICAL_CAMERA_RECTANGLE, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.TOGGLE_OPTICAL_CAMERA_RECTANGLE, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.SET_Z_HEIGHT_OFFSET, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.ADD_ALIGNMENT_SURFACE_MAP_REGION, true);
    addToProjectChangeEventToTestProgramValidMap(PanelSurfaceMapEventEnum.REMOVE_ALIGNMENT_SURFACE_MAP_REGION, true);
    
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(PanelSurfaceMapEventEnum.class) == numEnums);
    
    addToProjectChangeEventToTestProgramValidMap(BoardSurfaceMapEventEnum.CREATE_OR_DESTROY, true);
    addToProjectChangeEventToTestProgramValidMap(BoardSurfaceMapEventEnum.PANEL, true);
    addToProjectChangeEventToTestProgramValidMap(BoardSurfaceMapEventEnum.USER_COMMENT, true);
    addToProjectChangeEventToTestProgramValidMap(BoardSurfaceMapEventEnum.ADD_SURFACE_MAP_POINTS, true);
    addToProjectChangeEventToTestProgramValidMap(BoardSurfaceMapEventEnum.ADD_SURFACE_MAP_VERIFIED, true);
    addToProjectChangeEventToTestProgramValidMap(BoardSurfaceMapEventEnum.ADD_OPTICAL_CAMERA_RECTANGLE, true);
    addToProjectChangeEventToTestProgramValidMap(BoardSurfaceMapEventEnum.REMOVE_OPTICAL_CAMERA_RECTANGLE, true);
    addToProjectChangeEventToTestProgramValidMap(BoardSurfaceMapEventEnum.TOGGLE_OPTICAL_CAMERA_RECTANGLE, true);
    addToProjectChangeEventToTestProgramValidMap(BoardSurfaceMapEventEnum.ADD_SURFACE_MAP_OPTICAL_REGION, true);
    addToProjectChangeEventToTestProgramValidMap(BoardSurfaceMapEventEnum.REMOVE_SURFACE_MAP_OPTICAL_REGION, true);
    addToProjectChangeEventToTestProgramValidMap(BoardSurfaceMapEventEnum.SET_Z_HEIGHT_OFFSET, true);
    addToProjectChangeEventToTestProgramValidMap(BoardSurfaceMapEventEnum.ADD_ALIGNMENT_SURFACE_MAP_REGION, true);
    addToProjectChangeEventToTestProgramValidMap(BoardSurfaceMapEventEnum.REMOVE_ALIGNMENT_SURFACE_MAP_REGION, true);
    
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(BoardSurfaceMapEventEnum.class) == numEnums);
  
    addToProjectChangeEventToTestProgramValidMap(PanelAlignmentSurfaceMapEventEnum.CREATE_OR_DESTROY, true);
    addToProjectChangeEventToTestProgramValidMap(PanelAlignmentSurfaceMapEventEnum.ADD_SURFACE_MAP_POINTS, true);
    addToProjectChangeEventToTestProgramValidMap(PanelAlignmentSurfaceMapEventEnum.REMOVE_SURFACE_MAP_POINTS, true);
    addToProjectChangeEventToTestProgramValidMap(PanelAlignmentSurfaceMapEventEnum.ADD_OPTICAL_REGION, false);
    addToProjectChangeEventToTestProgramValidMap(PanelAlignmentSurfaceMapEventEnum.REMOVE_OPTICAL_REGION, false);    
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(PanelAlignmentSurfaceMapEventEnum.class) == numEnums);
    
    addToProjectChangeEventToTestProgramValidMap(BoardAlignmentSurfaceMapEventEnum.CREATE_OR_DESTROY, true);
    addToProjectChangeEventToTestProgramValidMap(BoardAlignmentSurfaceMapEventEnum.ADD_SURFACE_MAP_POINTS, true);
    addToProjectChangeEventToTestProgramValidMap(BoardAlignmentSurfaceMapEventEnum.ADD_OPTICAL_REGION, false);
    addToProjectChangeEventToTestProgramValidMap(BoardAlignmentSurfaceMapEventEnum.REMOVE_OPTICAL_REGION, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(BoardAlignmentSurfaceMapEventEnum.class) == numEnums);

    // added by Wei Chin
    addToProjectChangeEventToTestProgramValidMap(SubtypeAdvanceSettingsEventEnum.SIGNAL_COMPENSATION_ENUM, false);
    addToProjectChangeEventToTestProgramValidMap(SubtypeAdvanceSettingsEventEnum.ARTIFACT_COMPENSATION_STATE_ENUM, false);
    addToProjectChangeEventToTestProgramValidMap(SubtypeAdvanceSettingsEventEnum.USER_GAIN_ENUM, false);
    addToProjectChangeEventToTestProgramValidMap(SubtypeAdvanceSettingsEventEnum.STAGE_SPEED_ENUM, false);
    addToProjectChangeEventToTestProgramValidMap(SubtypeAdvanceSettingsEventEnum.MAGNIFICATION_TYPE_ENUM, false);
    addToProjectChangeEventToTestProgramValidMap(SubtypeAdvanceSettingsEventEnum.DYNAMIC_RANGE_OPTIMIZATION_LEVEL_ENUM, false); //Siew Yeng - XCR-2140
    addToProjectChangeEventToTestProgramValidMap(SubtypeAdvanceSettingsEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(SubtypeAdvanceSettingsEventEnum.FOCUS_METHOD, false);
    
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(SubtypeAdvanceSettingsEventEnum.class) == numEnums);

    // added by Lee Herng - 20 Nov 2013
    addToProjectChangeEventToTestProgramValidMap(PanelMeshEventEnum.CREATE_OR_DESTROY, true);
    addToProjectChangeEventToTestProgramValidMap(PanelMeshEventEnum.ADD_MESH_POINTS, true);
    addToProjectChangeEventToTestProgramValidMap(PanelMeshEventEnum.REMOVE_MESH_POINTS, true); 
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(PanelMeshEventEnum.class) == numEnums);
    
    // added by Lee Herng - 05 Dec 2013
    addToProjectChangeEventToTestProgramValidMap(BoardMeshEventEnum.CREATE_OR_DESTROY, true);
    addToProjectChangeEventToTestProgramValidMap(BoardMeshEventEnum.ADD_MESH_POINTS, true);
    addToProjectChangeEventToTestProgramValidMap(BoardMeshEventEnum.REMOVE_MESH_POINTS, true); 
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(BoardMeshEventEnum.class) == numEnums);
    
    //Siew Yeng - XCR1725 - Customizable serial number mapping
    addToProjectChangeEventToTestProgramValidMap(SerialNumberMappingEventEnum.SERIAL_NUMBER_MAPPING, true);
    addToProjectChangeEventToTestProgramValidMap(SerialNumberMappingEventEnum.SERIAL_NUMBER_MAPPING_BARCODE_CONFIG_NAME, true);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(SerialNumberMappingEventEnum.class) == numEnums);
    
    //Kee Chin Seong - Project event added for PSP event Enum
    addToProjectChangeEventToTestProgramValidMap(PspEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(PspEventEnum.PANEL, false);
    //Ying-Huan.Chu - temporary commented Larger FOV feature for 5.7 release.
    //addToProjectChangeEventToTestProgramValidMap(PspEventEnum.SET_USE_LARGER_FOV, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(PspEventEnum.class) == numEnums);
    
    addToProjectChangeEventToTestProgramValidMap(VariationSettingEnum.ADD_OR_REMOVE, false);
    addToProjectChangeEventToTestProgramValidMap(VariationSettingEnum.ENABLE, false);
    addToProjectChangeEventToTestProgramValidMap(VariationSettingEnum.NAME, false);
    addToProjectChangeEventToTestProgramValidMap(VariationSettingEnum.PRESELECT, false);
    addToProjectChangeEventToTestProgramValidMap(VariationSettingEnum.ENABLE_CHOOSABLE_IN_PRODUCTION, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(VariationSettingEnum.class) == numEnums);
	
	//Siew Yeng - XCR-3781 - PSH settings
    addToProjectChangeEventToTestProgramValidMap(PshSettingsEventEnum.CREATE_OR_DESTROY, false);
    addToProjectChangeEventToTestProgramValidMap(PshSettingsEventEnum.SELECTIVE_BOARD_SIDE_ENABLED, false);
    addToProjectChangeEventToTestProgramValidMap(PshSettingsEventEnum.BOARD_SIDE_REFERENCE_ENUM, false);
    addToProjectChangeEventToTestProgramValidMap(PshSettingsEventEnum.SELECTIVE_JOINT_TYPE_ENABLED, false);
    addToProjectChangeEventToTestProgramValidMap(PshSettingsEventEnum.SELECTIVE_COMPONENT_ENABLED, false);
    addToProjectChangeEventToTestProgramValidMap(PshSettingsEventEnum.ADD_OR_REMOVE_PSH_NEIGHBOUR_COMPONENT, false);
    addToProjectChangeEventToTestProgramValidMap(PshSettingsEventEnum.ADD_OR_REMOVE_ENABLED_JOINT_TYPE, false);
    numEnums = _projectEventToTestProgramValidMap.size() - prevNumEnums;
    prevNumEnums += numEnums;
    Assert.expect(com.axi.util.Enum.getNumEnums(PshSettingsEventEnum.class) == numEnums);
  }

  /**
   * @author Bill Darbie
   */
  private static void initProjectEventToProjectMinorChangeMap()
  {
    // ProjectEventEnum does not always change project minor state like all the other
    // enums do, so populate a map just for it
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.CREATE_OR_DESTROY, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.FAST_LOAD, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.SLOW_LOAD, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.UNLOAD, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.SAVE, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.DELETE, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.IMPORT_PROJECT_FROM_NDFS, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.NAME, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.PANEL, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.PROJECT_TEST_PROGRAM_VERSION, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.PROJECT_MINOR_VERSION, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.PROGRAM_GENERATION_VERSION, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.SOFTWARE_VERSION_CREATION, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.NOTES, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.INITIAL_THESHOLDS, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.INITIAL_RECIPE_SETTING, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.DISPLAY_UNITS, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.READY_FOR_PRODUCTION, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.TEST_PROGRAM_UPDATED_WITH_ALIGNMENT_REGIONS, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.TEST_PROGRAM_CLEARING, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.TEST_PROGRAM_GENERATED, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.VERIFICATION_IMAGES_GENERATED, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.TEST_PROGRAM_CHECK_SUM_INVALIDATED, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.LAST_MODIFICATION_TIME, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.PROGRAMMER_NAME, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.TARGET_CUSTOMER_NAME, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.PANEL_IMAGE_CREATION, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.DELETE_SHORTS_LEARNING, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.DELETE_EXPECTED_IMAGE_LEARNING, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.DELETE_SUBTYPE_LEARNING, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.DELETE_EXPECTED_IMAGE_TEMPLATE_LEARNING, true);//ShengChuan - Clear Tombstone
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.LIBRARY_IMPORT_COMPLETE, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_SYSTEM_TYPE, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.LOADING_LONG_PANEL_WITH_DIFFERENT_SYSTEM_TYPE, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.SYSTEM_TYPE, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.SCAN_PATH_METHOD, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.TEST_PROGRAM_USER_GAIN_CHANGED, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.TEST_PROGRAM_BOARD_Z_OFFSET_CHANGED, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.TEST_PROGRAM_STAGE_SPEED_CHANGED, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.TEST_PROGRAM_SIGNAL_OR_ARTIFACT_COMPENSATION_CHANGED, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.ENABLE_LARGE_VIEW_IMAGE, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.GENERATE_MULTI_ANGLE_IMAGE, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.GENERATE_COMPONENT_IMAGE, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.ENLARGE_RECONSTRUCTION_REGION, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_LOW_MAGNIFICATION, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_MAGNIFICATION_FOR_OFFLINE_PROGRAMMING, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_HIGH_MAGNIFICATION, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.LOW_MAGNIFICATION_CHANGED, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.HIGH_MAGNIFICATION_CHANGED, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.ENABLE_BGA_JOINT_BASED_THRESHOLD_INSPECTION, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.SET_BGA_INSPECTION_REGION_TO_1X1, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.PROJECT_BASED_BYPASS_MODE, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.SET_THICKNESS_TABLE_VERSION, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.SET_USE_NEW_SCAN_ROUTE_GENERATION, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.SET_USE_VARIABLE_DIVN, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.SET_DYNAMIC_RANGE_OPTIMIZATION_VERSION, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.LOADING_PROJECT_WITH_DIFFERENT_THICKNESS_TABLE_VERSION, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.LOADING_PROJECT_WITH_ALTERNATIVE_THICKNESS_TABLE_VERSION, true);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.ENLARGE_ALIGNMENT_REGION, false);
    addToProjectEventToProjectMinorChangeMap(ProjectEventEnum.IMPORT_LAND_PATTERN_COMPLETE, false);
    
    int numEnums = _projectEventToProjectMinorChangeMap.size();
    Assert.expect(com.axi.util.Enum.getNumEnums(ProjectEventEnum.class) == numEnums);
  }

  /**
   * @author Bill Darbie
   */
  private static void addToProjectChangeEventToTestProgramValidMap(ProjectChangeEventEnum projectChangeEventEnum, boolean testProgramValid)
  {
    Assert.expect(projectChangeEventEnum != null);

    Boolean prev = _projectEventToTestProgramValidMap.put(projectChangeEventEnum, testProgramValid);
    Assert.expect(prev == null);
  }

  /**
   * @author Bill Darbie
   */
  private static void addToProjectEventToProjectMinorChangeMap(ProjectEventEnum projectEventEnum, boolean minorChange)
  {
    Assert.expect(projectEventEnum != null);

    Boolean prev = _projectEventToProjectMinorChangeMap.put(projectEventEnum, minorChange);
    Assert.expect(prev == null);
  }

  /**
   * @author Bill Darbie
   */
  private static void initProjectEventToTestProgramAlignmentRegionsInvalidatingEvents()
  {
    _projectEventToTestProgramAlignmentRegionsInvalidatingEventSet.add(AlignmentGroupEventEnum.CREATE_OR_DESTROY);
    _projectEventToTestProgramAlignmentRegionsInvalidatingEventSet.add(AlignmentGroupEventEnum.ADD_OR_REMOVE_PAD);
    _projectEventToTestProgramAlignmentRegionsInvalidatingEventSet.add(AlignmentGroupEventEnum.ADD_OR_REMOVE_FIDUCIAL);
    _projectEventToTestProgramAlignmentRegionsInvalidatingEventSet.add(AlignmentGroupEventEnum.USE_ZHEIGHT);
    _projectEventToTestProgramAlignmentRegionsInvalidatingEventSet.add(AlignmentGroupEventEnum.SIGNAL_COMPENSATION);
    _projectEventToTestProgramAlignmentRegionsInvalidatingEventSet.add(AlignmentGroupEventEnum.STAGE_SPEED);
    _projectEventToTestProgramAlignmentRegionsInvalidatingEventSet.add(AlignmentGroupEventEnum.DRO_LEVEL);
    _projectEventToTestProgramAlignmentRegionsInvalidatingEventSet.add(AlignmentGroupEventEnum.USER_GAIN);
    _projectEventToTestProgramAlignmentRegionsInvalidatingEventSet.add(AlignmentGroupEventEnum.USE_CUSTOMIZED_ALIGNMENT);
  }

  /**
   * @author Bill Darbie
   */
  private static void initVerificationImageInvalidatingEvents()
  {
    //Ngie Xing, XCR-2477,Bypass Verification Image Checking
    //removed unncessary events that invalidates verification checking
    // Panel events
    _verificationImageInvalidatingEventSet.add(PanelEventEnum.WIDTH);
    _verificationImageInvalidatingEventSet.add(PanelEventEnum.WIDTH_RETAINING_BOARD_COORDS);
    _verificationImageInvalidatingEventSet.add(PanelEventEnum.LENGTH);
    _verificationImageInvalidatingEventSet.add(PanelEventEnum.LENGTH_RETAINING_BOARD_COORDS);

    // PanelSetting events
    _verificationImageInvalidatingEventSet.add(PanelSettingsEventEnum.DEGREES_ROTATION);
    _verificationImageInvalidatingEventSet.add(PanelSettingsEventEnum.LEFT_TO_RIGHT_FLIP);
    _verificationImageInvalidatingEventSet.add(PanelSettingsEventEnum.TOP_TO_BOTTOM_FLIP);

    // BoardEvents
    _verificationImageInvalidatingEventSet.add(BoardEventEnum.CREATE_OR_DESTROY);
  }


  /**
   * @author Bill Darbie
   */
  private static void initAlignmentTransformInvalidatingEvents()
  {
    //Ngie Xing, removed some alignment transform invalidating event
    // events that will definately cause the alignment transform to be invalid
    _alignmentTransformInvalidatingEventSet.add(AlignmentGroupEventEnum.ADD_OR_REMOVE_PAD);
    _alignmentTransformInvalidatingEventSet.add(AlignmentGroupEventEnum.ADD_OR_REMOVE_FIDUCIAL);
    _alignmentTransformInvalidatingEventSet.add(AlignmentGroupEventEnum.USER_GAIN);
    _alignmentTransformInvalidatingEventSet.add(AlignmentGroupEventEnum.SIGNAL_COMPENSATION);
    _alignmentTransformInvalidatingEventSet.add(AlignmentGroupEventEnum.STAGE_SPEED);
    _alignmentTransformInvalidatingEventSet.add(AlignmentGroupEventEnum.DRO_LEVEL);
    _alignmentTransformInvalidatingEventSet.add(AlignmentGroupEventEnum.USE_CUSTOMIZED_ALIGNMENT);
    _alignmentTransformInvalidatingEventSet.add(PanelEventEnum.WIDTH);
    _alignmentTransformInvalidatingEventSet.add(PanelEventEnum.WIDTH_RETAINING_BOARD_COORDS);
    _alignmentTransformInvalidatingEventSet.add(PanelEventEnum.LENGTH);
    _alignmentTransformInvalidatingEventSet.add(PanelEventEnum.LENGTH_RETAINING_BOARD_COORDS);
    _alignmentTransformInvalidatingEventSet.add(PanelSettingsEventEnum.DEGREES_ROTATION);
    _alignmentTransformInvalidatingEventSet.add(PanelSettingsEventEnum.LEFT_TO_RIGHT_FLIP);
    _alignmentTransformInvalidatingEventSet.add(PanelSettingsEventEnum.TOP_TO_BOTTOM_FLIP);
    _alignmentTransformInvalidatingEventSet.add(PanelSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED);
    _alignmentTransformInvalidatingEventSet.add(BoardSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_DESTROYED);

    // events that might cause invalidation if the Board instance has a pad or fiducial on it that is used for alignment
    _boardSpecificAlignmentTransformInvalidatingEventSet.add(BoardEventEnum.COORDINATE);
    _boardSpecificAlignmentTransformInvalidatingEventSet.add(BoardEventEnum.DEGREES_ROTATION);
    _boardSpecificAlignmentTransformInvalidatingEventSet.add(BoardEventEnum.LEFT_TO_RIGHT_FLIP);
    _boardSpecificAlignmentTransformInvalidatingEventSet.add(BoardEventEnum.TOP_TO_BOTTOM_FLIP);

    // events that might cause invalidation if the Component instance has a pad that is used for alignment
    _componentSpecificAlignmentTransformInvalidatingEventSet.add(ComponentEventEnum.CHANGE_SIDE);
    _componentSpecificAlignmentTransformInvalidatingEventSet.add(ComponentEventEnum.CREATE_OR_DESTROY);

    // events that might cause invalidation if the ComponentType instance has a pad that is used for alignment
    _componentTypeSpecificAlignmentTransformInvalidatingEventSet.add(ComponentTypeEventEnum.CHANGE_SIDE);
    _componentTypeSpecificAlignmentTransformInvalidatingEventSet.add(ComponentTypeEventEnum.COORDINATE);
    _componentTypeSpecificAlignmentTransformInvalidatingEventSet.add(ComponentTypeEventEnum.DEGREES_ROTATION);

    // events that might cause invalidation if the LandPattern instance has a pad that is used for alignment
    _landPatternSpecificAlignmentTransformInvalidatingEventSet.add(LandPatternEventEnum.ADD_OR_REMOVE_LAND_PATTERN_PAD);

    // events that might cause invalidation if the LandPatternPad instance has a pad that is used for alignment
    _landPatternPadSpecificAlignmentTransformInvalidatingEventSet.add(LandPatternPadEventEnum.COORDINATE);
    _landPatternPadSpecificAlignmentTransformInvalidatingEventSet.add(LandPatternPadEventEnum.CREATE_OR_DESTROY);
    _landPatternPadSpecificAlignmentTransformInvalidatingEventSet.add(LandPatternPadEventEnum.ADD_OR_REMOVE);
    _landPatternPadSpecificAlignmentTransformInvalidatingEventSet.add(LandPatternPadEventEnum.DEGREES_ROTATION);
    _landPatternPadSpecificAlignmentTransformInvalidatingEventSet.add(LandPatternPadEventEnum.LENGTH);
    _landPatternPadSpecificAlignmentTransformInvalidatingEventSet.add(LandPatternPadEventEnum.WIDTH);
    _landPatternPadSpecificAlignmentTransformInvalidatingEventSet.add(LandPatternPadEventEnum.SHAPE_ENUM);
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  private static void initShortsLearningRecordsInFileInvalidatingEvents()
  {
    _shortLearningRecordsInFileInvalidatingEventSet.add(SubtypeEventEnum.TUNED_VALUE);
    _shortLearningRecordsInFileInvalidatingEventSet.add(SubtypeEventEnum.TUNED_SLICE_HEIGHT_VALUE);
    _shortLearningRecordsInFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.ARTIFACT_COMPENSATION_STATE_ENUM);
    _shortLearningRecordsInFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.SIGNAL_COMPENSATION_ENUM);
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  private static void initShortsLearningRecordsInFileValidEvents()
  {
    _shortLearningRecordsInFileValidEventSet.add(SubtypeEventEnum.JOINT_SPECIFIC_DATA_LEARNED);
    _shortLearningRecordsInFileValidEventSet.add(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_SHORT);
  }

  /**
   * @author Matt Wharton
   */
  private static void initShortLearningRecordsInFileValidatingOrInvalidatingEvents()
  {
    _shortLearningRecordsInFileValidatingOrInvalidatingEventSet.add(ComponentTypeEventEnum.JOINT_TYPE_ENUM);
    _shortLearningRecordsInFileValidatingOrInvalidatingEventSet.add(CompPackageEventEnum.JOINT_TYPE_ENUM);
  }

  /**
   * @author Bill Darbie
   */
  private static void initShortLearningFileInvalidatingEvents()
  {
    _shortLearningFileInvalidatingEventSet.add(SubtypeEventEnum.JOINT_SPECIFIC_DATA_LEARNED);
    _shortLearningFileInvalidatingEventSet.add(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_SHORT);
    _shortLearningFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.ARTIFACT_COMPENSATION_STATE_ENUM);
    _shortLearningFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.SIGNAL_COMPENSATION_ENUM);
  }

  /**
   * @author Bill Darbie
   * @author George Booth
   */
  private static void initExpectedImageLearningRecordsInFileInvalidatingEvents()
  {
    _expectedImageLearningRecordsInFileInvalidatingEventSet.add(SubtypeEventEnum.TUNED_VALUE);
    _expectedImageLearningRecordsInFileInvalidatingEventSet.add(SubtypeEventEnum.TUNED_SLICE_HEIGHT_VALUE);
    _expectedImageLearningRecordsInFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.ARTIFACT_COMPENSATION_STATE_ENUM);
    _expectedImageLearningRecordsInFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.SIGNAL_COMPENSATION_ENUM);
  }

  /**
   * Clear Tombstone use
   * @author sheng chuan
   */
  private static void initExpectedImageTemplateLearningRecordsInFileInvalidatingEvents()
  {
    _expectedImageTemplateLearningRecordsInFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.ARTIFACT_COMPENSATION_STATE_ENUM);
    _expectedImageTemplateLearningRecordsInFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.SIGNAL_COMPENSATION_ENUM);
    _expectedImageTemplateLearningRecordsInFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.MAGNIFICATION_TYPE_ENUM);
    _expectedImageTemplateLearningRecordsInFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.USER_GAIN_ENUM);
  }
  
  /**
   * @author Bill Darbie
   * @author George Booth
   */
  private static void initExpectedImageLearningRecordsInFileValidEvents()
  {
    _expectedImageLearningRecordsInFileValidEventSet.add(SubtypeEventEnum.EXPECTED_IMAGE_DATA_LEARNED);
    _expectedImageLearningRecordsInFileValidEventSet.add(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_EXPECTED_IMAGE);
  }
  
  /**
   * Clear Tombstone use.
   * @author sheng chuan
   */
  private static void initExpectedImageTemplateLearningRecordsInFileValidEvents()
  {
    _expectedImageTemplateLearningRecordsInFileValidEventSet.add(SubtypeEventEnum.EXPECTED_IMAGE_TEMPLATE_LEARNED);
    _expectedImageTemplateLearningRecordsInFileValidEventSet.add(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_EXPECTED_IMAGE_TEMPLATE);
  }

  /**
   * @author George Booth
   */
  private static void initExpectedImageLearningRecordsInFileValidatingOrInvalidatingEvents()
  {
    _expectedImageLearningRecordsInFileValidatingOrInvalidatingEventSet.add(ComponentTypeEventEnum.JOINT_TYPE_ENUM);
    _expectedImageLearningRecordsInFileValidatingOrInvalidatingEventSet.add(CompPackageEventEnum.JOINT_TYPE_ENUM);
  }
  
  /**
   * Clear Tombstone use.
   * @author sheng chuan
   */
  private static void initExpectedImageTemplateLearningRecordsInFileValidatingOrInvalidatingEvents()
  {
    _expectedImageTemplateLearningRecordsInFileValidatingOrInvalidatingEventSet.add(ComponentTypeEventEnum.JOINT_TYPE_ENUM);
    _expectedImageTemplateLearningRecordsInFileValidatingOrInvalidatingEventSet.add(CompPackageEventEnum.JOINT_TYPE_ENUM);
  }

  /**
   * @author George Booth
   */
  private static void initExpectedImageLearningFileInvalidatingEvents()
  {
    _expectedImageLearningFileInvalidatingEventSet.add(SubtypeEventEnum.EXPECTED_IMAGE_DATA_LEARNED);
    _expectedImageLearningFileInvalidatingEventSet.add(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_EXPECTED_IMAGE);
    _expectedImageLearningFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.ARTIFACT_COMPENSATION_STATE_ENUM);
    _expectedImageLearningFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.SIGNAL_COMPENSATION_ENUM);
  }

 /**
   * @author Bill Darbie
   * @author Matt Wharton ??????
   * @author Lim, Lay Ngor - Broken Pin
   */
  private static void initBrokenPinLearningRecordsInFileInvalidatingEvents()
  {
    _brokenPinLearningRecordsInFileInvalidatingEventSet.add(SubtypeEventEnum.TUNED_VALUE);
    _brokenPinLearningRecordsInFileInvalidatingEventSet.add(SubtypeEventEnum.TUNED_SLICE_HEIGHT_VALUE);
    _brokenPinLearningRecordsInFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.ARTIFACT_COMPENSATION_STATE_ENUM);
    _brokenPinLearningRecordsInFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.SIGNAL_COMPENSATION_ENUM);
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   * @author Lim, Lay Ngor - Broken Pin
   */
  private static void initBrokenPinLearningRecordsInFileValidEvents()
  {
    _brokenPinLearningRecordsInFileValidEventSet.add(SubtypeEventEnum.JOINT_SPECIFIC_DATA_LEARNED);
    _brokenPinLearningRecordsInFileValidEventSet.add(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_SHORT);
  }

  /**
   * @author Matt Wharton
   * @author Lim, Lay Ngor - Broken Pin
   */
  private static void initBrokenPinLearningRecordsInFileValidatingOrInvalidatingEvents()
  {
    _brokenPinLearningRecordsInFileValidatingOrInvalidatingEventSet.add(ComponentTypeEventEnum.JOINT_TYPE_ENUM);
    _brokenPinLearningRecordsInFileValidatingOrInvalidatingEventSet.add(CompPackageEventEnum.JOINT_TYPE_ENUM);
  }

  /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor - Broken Pin
   */
  private static void initBrokenPinLearningFileInvalidatingEvents()
  {
    _brokenPinLearningFileInvalidatingEventSet.add(SubtypeEventEnum.JOINT_SPECIFIC_DATA_LEARNED);
    _brokenPinLearningFileInvalidatingEventSet.add(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_SHORT);
    _brokenPinLearningFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.ARTIFACT_COMPENSATION_STATE_ENUM);
    _brokenPinLearningFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.SIGNAL_COMPENSATION_ENUM);
  }

  /**
   * Clear Tombstone use.
   * @author Sheng-chuan
   */
  private static void initExpectedImageTemplateLearningFileInvalidatingEvents()
  {
    _expectedImageTemplateLearningFileInvalidatingEventSet.add(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_EXPECTED_IMAGE_TEMPLATE);
    _expectedImageTemplateLearningFileInvalidatingEventSet.add(SubtypeEventEnum.EXPECTED_IMAGE_TEMPLATE_LEARNED);
    _expectedImageTemplateLearningFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.ARTIFACT_COMPENSATION_STATE_ENUM);
    _expectedImageTemplateLearningFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.SIGNAL_COMPENSATION_ENUM);
    _expectedImageTemplateLearningFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.MAGNIFICATION_TYPE_ENUM);
    _expectedImageTemplateLearningFileInvalidatingEventSet.add(SubtypeAdvanceSettingsEventEnum.USER_GAIN_ENUM);
  }
  
  /**
   * @author Bill Darbie
   */
  private static void initSubtypeLearningFileInvalidatingEvents()
  {
    _subtypeLearningFileInvalidatingEventSet.add(SubtypeEventEnum.ALGORITHM_SETTINGS_LEARNED);
    _subtypeLearningFileInvalidatingEventSet.add(SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_ALL_ALGORITHMS_EXCEPT_SHORT);
  }

  /**
   * @author Bill Darbie
   */
  private static void initPotentialLongProgramInvalidEvents()
  {
    _potentialLongProgramInvalidEventSet.add(PanelSettingsEventEnum.DEGREES_ROTATION);
    _potentialLongProgramInvalidEventSet.add(PanelSettingsEventEnum.LEFT_TO_RIGHT_FLIP);
    _potentialLongProgramInvalidEventSet.add(PanelSettingsEventEnum.SHIFT_X);
    _potentialLongProgramInvalidEventSet.add(PanelSettingsEventEnum.SHIFT_Y);
    _potentialLongProgramInvalidEventSet.add(PanelSettingsEventEnum.TOP_TO_BOTTOM_FLIP);

    _potentialLongProgramInvalidEventSet.add(BoardEventEnum.COORDINATE);
    _potentialLongProgramInvalidEventSet.add(BoardEventEnum.DEGREES_ROTATION);
    _potentialLongProgramInvalidEventSet.add(BoardEventEnum.LEFT_TO_RIGHT_FLIP);
    _potentialLongProgramInvalidEventSet.add(BoardEventEnum.TOP_TO_BOTTOM_FLIP);

    _potentialLongProgramInvalidEventSet.add(BoardTypeEventEnum.ADD_OR_REMOVE_BOARD);
    _potentialLongProgramInvalidEventSet.add(BoardTypeEventEnum.CREATE_OR_DESTROY);

    _potentialLongProgramInvalidEventSet.add(PadEventEnum.CREATE_OR_DESTROY);

    _potentialLongProgramInvalidEventSet.add(PadTypeEventEnum.ADD_OR_REMOVE_PAD);
    _potentialLongProgramInvalidEventSet.add(PadTypeEventEnum.CREATE_OR_DESTROY);

    _potentialLongProgramInvalidEventSet.add(LandPatternPadEventEnum.ADD_OR_REMOVE);
    _potentialLongProgramInvalidEventSet.add(LandPatternPadEventEnum.COORDINATE);
    _potentialLongProgramInvalidEventSet.add(LandPatternPadEventEnum.CREATE_OR_DESTROY);
    _potentialLongProgramInvalidEventSet.add(LandPatternPadEventEnum.DEGREES_ROTATION);
    _potentialLongProgramInvalidEventSet.add(LandPatternPadEventEnum.LENGTH);
    _potentialLongProgramInvalidEventSet.add(LandPatternPadEventEnum.WIDTH);

    _potentialLongProgramInvalidEventSet.add(ComponentEventEnum.ADD_OR_REMOVE_PAD);
    _potentialLongProgramInvalidEventSet.add(ComponentEventEnum.CREATE_OR_DESTROY);
    _potentialLongProgramInvalidEventSet.add(ComponentEventEnum.SIDE_BOARD);

    _potentialLongProgramInvalidEventSet.add(ComponentTypeEventEnum.ADD_OR_REMOVE);
    _potentialLongProgramInvalidEventSet.add(ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT);
    _potentialLongProgramInvalidEventSet.add(ComponentTypeEventEnum.ADD_OR_REMOVE_PAD_TYPE);
    _potentialLongProgramInvalidEventSet.add(ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT_LIST);
    _potentialLongProgramInvalidEventSet.add(ComponentTypeEventEnum.COORDINATE);
    _potentialLongProgramInvalidEventSet.add(ComponentTypeEventEnum.CREATE_OR_DESTROY);
    _potentialLongProgramInvalidEventSet.add(ComponentTypeEventEnum.DEGREES_ROTATION);
  }

  /**
   * @author Wei Chin, Chong
   */
  private static void initTestProgramCheckSumInvalidEvents()
  {
    _testProgramCheckSumInvalidEventSet.add(SubtypeAdvanceSettingsEventEnum.ARTIFACT_COMPENSATION_STATE_ENUM);
    _testProgramCheckSumInvalidEventSet.add(SubtypeAdvanceSettingsEventEnum.SIGNAL_COMPENSATION_ENUM);
    _testProgramCheckSumInvalidEventSet.add(BoardSettingsEventEnum.BOARD_ZOFFSET);
  }

  /**
   * @author Bill Darbie
   */
  public void resetEnabledStateForUnitTest()
  {
    _enabled = -1;
  }

  /**
   * @author Bill Darbie
   */
  public void enableAfterProjectLoad()
  {
    _enabled = 0;
  }

  /**
   * @author Bill Darbie
   */
  public void enable()
  {
    --_enabled;
    Assert.expect(_enabled >= 0);
  }

  /**
   * @author Bill Darbie
   */
  public void disable()
  {
    if (_enabled == -1)
      _enabled = 0;
    ++_enabled;
  }

  /**
   * @author Bill Darbie
   */
  private boolean isEnabled()
  {
    if (_enabled == 0)
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public void enableProjectStateEvents()
  {
    _enableProjectStateEvents = true;
  }

  /**
   * @author Bill Darbie
   */
  public void disableProjectStateEvents()
  {
    _enableProjectStateEvents = false;
  }

  /**
   * @author Bill Darbie
   */
  private void getCurrentStates()
  {
    _testProgramValid = isTestProgramValid();
    _projectMajorStateValid = isProjectMajorStateValid();
    _projectMinorStateValid = isProjectMinorStateValid();
    _verificationImagesValid = areVerificationImagesValid();
    _alignmentTransformValid = isManualAlignmentTransformValid();
    _shortLearningRecordsInFileValid = areShortLearningRecordsValid();
    _shortLearningFileValid = isShortLearningFileValid();
    _expectedImageLearningRecordsInFileValid = areExpectedImageLearningRecordsValid();
    _expectedImageLearningFileValid = isExpectedImageLearningFileValid();
    _brokenPinLearningRecordsInFileValid = areBrokenPinLearningRecordsValid(); //Broken Pin
    _brokenPinLearningFileValid = isBrokenPinLearningFileValid(); //Broken Pin
    _expectedImageTemplateLearningFileValid = isExpectedImageTemplateLearningFileValid();//ShengChuan - Clear Tombstone
    _expectedImageTemplateLearningRecordsInFileValid = areExpectedImageTemplateLearningRecordsValid();//ShengChuan - Clear Tombstone
    _subtypeLearningFileValid = isSubtypeLearningFileValid();
  }

  /**
   * @author Bill Darbie
   */
  private void sendChangedStateEvents()
  {
    if (_enableProjectStateEvents == false)
      return;

    if (_projectObservable == null)
      _projectObservable = ProjectObservable.getInstance();

    boolean testProgramValid = isTestProgramValid();
    if (_testProgramValid != testProgramValid)
    {
      ProjectChangeEvent projChangeEvent = null;
      if (testProgramValid)
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.TEST_PROGRAM_VALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
      else
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.TEST_PROGRAM_INVALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
    }

    boolean projectMajorStateValid = isProjectMajorStateValid();
    if (_projectMajorStateValid != projectMajorStateValid)
    {
      ProjectChangeEvent projChangeEvent = null;
      if (projectMajorStateValid)
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.PROJECT_MAJOR_STATE_VALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
      else
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.PROJECT_MAJOR_STATE_INVALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
    }

    boolean projectMinorStateValid = isProjectMinorStateValid();
    if (_projectMinorStateValid != projectMinorStateValid)
    {
      ProjectChangeEvent projChangeEvent = null;
      if (projectMinorStateValid)
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.PROJECT_MINOR_STATE_VALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
      else
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.PROJECT_MINOR_STATE_INVALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
    }

    boolean verificationImagesValid = areVerificationImagesValid();
    if (_verificationImagesValid != verificationImagesValid)
    {
      ProjectChangeEvent projChangeEvent = null;
      if (verificationImagesValid)
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.VERIFICATION_IMAGES_VALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
      else
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.VERIFICATION_IMAGES_INVALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
    }

    boolean alignmentTransformValid = isManualAlignmentTransformValid();
    if (_alignmentTransformValid != alignmentTransformValid)
    {
      ProjectChangeEvent projChangeEvent = null;
      if (alignmentTransformValid)
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.ALIGNMENT_TRANSFORM_VALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
      else
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.ALIGNMENT_TRANSFORM_INVALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
    }

    boolean shortLearningRecordsInFileValid = areShortLearningRecordsValid();
    if (_shortLearningRecordsInFileValid != shortLearningRecordsInFileValid)
    {
      ProjectChangeEvent projChangeEvent = null;
      if (shortLearningRecordsInFileValid)
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.SHORT_LEARNING_FILE_RECORDS_VALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
      else
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.SHORT_LEARNING_FILE_RECORDS_INVALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
    }

    boolean shortLearningFileValid = isShortLearningFileValid();
    if (_shortLearningFileValid != shortLearningFileValid)
    {
      ProjectChangeEvent projChangeEvent = null;
      if (shortLearningFileValid)
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.SHORT_LEARNING_FILE_VALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
      else
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.SHORT_LEARNING_FILE_INVALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
    }

    boolean expectedImageLearningRecordsInFileValid = areExpectedImageLearningRecordsValid();
    if (_expectedImageLearningRecordsInFileValid != expectedImageLearningRecordsInFileValid)
    {
      ProjectChangeEvent projChangeEvent = null;
      if (expectedImageLearningRecordsInFileValid)
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.EXPECTED_IMAGE_LEARNING_FILE_RECORDS_VALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
      else
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.EXPECTED_IMAGE_LEARNING_FILE_RECORDS_INVALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
    }

    boolean expectedImageLearningFileValid = isExpectedImageLearningFileValid();
    if (_expectedImageLearningFileValid != expectedImageLearningFileValid)
    {
      ProjectChangeEvent projChangeEvent = null;
      if (expectedImageLearningFileValid)
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.EXPECTED_IMAGE_LEARNING_FILE_VALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
      else
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.EXPECTED_IMAGE_LEARNING_FILE_INVALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
    }
    
    //Lim, Lay Ngor - Broken Pin - START
    boolean brokenPinLearningRecordsInFileValid = areBrokenPinLearningRecordsValid();
    if (_brokenPinLearningRecordsInFileValid != brokenPinLearningRecordsInFileValid)
    {
      ProjectChangeEvent projChangeEvent = null;
      if (brokenPinLearningRecordsInFileValid)
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.BROKEN_PIN_LEARNING_FILE_RECORDS_VALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
      else
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.BROKEN_PIN_LEARNING_FILE_RECORDS_INVALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
    }

    boolean brokenPinLearningFileValid = isShortLearningFileValid();
    if (_brokenPinLearningFileValid != shortLearningFileValid)
    {
      ProjectChangeEvent projChangeEvent = null;
      if (brokenPinLearningFileValid)
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.BROKEN_PIN_LEARNING_FILE_VALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
      else
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.BROKEN_PIN_LEARNING_FILE_INVALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
    }    
    //Lim, Lay Ngor - Broken Pin - END

    //added by sheng-chuan - Clear Tombstone for template learning - START
    boolean expectedImageTemplateLearningFileValie = isExpectedImageTemplateLearningFileValid();
    if(_expectedImageTemplateLearningFileValid != expectedImageTemplateLearningFileValie)
    {
      ProjectChangeEvent projChangeEvent = null;
      if (expectedImageTemplateLearningFileValie)
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.EXPECTED_IMAGE_TEMPLATE_LEARNING_FILE_VALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
      else
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.EXPECTED_IMAGE_TEMPLATE_LEARNING_FILE_INVALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
    }
    
    boolean expectedImageTemplateLearningRecordsInFileValid = areExpectedImageTemplateLearningRecordsValid();
    if (_expectedImageTemplateLearningRecordsInFileValid != expectedImageTemplateLearningRecordsInFileValid)
    {
      ProjectChangeEvent projChangeEvent = null;
      if (expectedImageLearningRecordsInFileValid)
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.EXPECTED_IMAGE_LEARNING_TEMPLATE_FILE_RECORDS_VALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
      else
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.EXPECTED_IMAGE_LEARNING_TEMPALTE_FILE_RECORDS_INVALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
    }
    //sheng-chuan - Clear Tombstone for template learning - End

    boolean subtypeLearningFileValid = isSubtypeLearningFileValid();
    if (_subtypeLearningFileValid != subtypeLearningFileValid)
    {
      ProjectChangeEvent projChangeEvent = null;
      if (subtypeLearningFileValid)
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.SUBTYPE_LEARNING_FILE_VALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
      else
      {
        projChangeEvent = new ProjectChangeEvent(this, null, null, ProjectStateEventEnum.SUBTYPE_LEARNING_FILE_INVALID);
        _projectObservable.sendProjectChangeEvent(projChangeEvent);
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  public void stateChange(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    if (isEnabled())
    {
      setProjectIfNecessary(projChangeEvent);

      getCurrentStates();
      resetStatesIfNecessary(projChangeEvent);

      trackMajorAndMinorAndTestProgramState(projChangeEvent);
      //trackVerificationImageState(projChangeEvent);
      trackAlignmentTransformState(projChangeEvent);
      trackShortLearningInvalidRecordsInFileState(projChangeEvent);
      trackShortLearningInvalidFileState(projChangeEvent);
      trackExpectedImageLearningInvalidRecordsInFileState(projChangeEvent);
      trackExpectedImageLearningInvalidFileState(projChangeEvent);
      trackBrokenPinLearningInvalidRecordsInFileState(projChangeEvent); //Broken Pin
      trackBrokenPinLearningInvalidFileState(projChangeEvent); //Broken Pin
      trackExpectedImageTemplateLearningInvalidRecordsInFileState(projChangeEvent);//ShengChuan - Clear Tombstone
      trackExpectedImageTemplateLearningInvalidFileState(projChangeEvent);//ShengChuan - Clear Tombstone
      trackSubtypeLearningInvalidFileState(projChangeEvent);
      trackPotentialLongProgramState(projChangeEvent);
      trackTestProgramCheckSumState(projChangeEvent);

      sendChangedStateEvents();

//      displayStateEvents();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void setProjectIfNecessary(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    ProjectChangeEventEnum eventEnum = projChangeEvent.getProjectChangeEventEnum();
    if (eventEnum.equals(ProjectEventEnum.SLOW_LOAD) ||
        eventEnum.equals(ProjectEventEnum.FAST_LOAD) ||
        eventEnum.equals(ProjectEventEnum.IMPORT_PROJECT_FROM_NDFS))
    {
      _project = (Project)projChangeEvent.getNewValue();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void resetStatesIfNecessary(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    ProjectChangeEventEnum eventEnum = projChangeEvent.getProjectChangeEventEnum();
    if (eventEnum.equals(ProjectEventEnum.SAVE))
    {
      resetDueToProjectSave();
    }
    else if (eventEnum.equals(ProjectEventEnum.SLOW_LOAD) ||
             eventEnum.equals(ProjectEventEnum.IMPORT_PROJECT_FROM_NDFS))
    {
      resetDueToProjectSlowLoadOrImport();
    }
    else if (eventEnum.equals(ProjectEventEnum.FAST_LOAD))
    {
      resetDueToProjectFastLoad();
    }
    else if (eventEnum.equals(ProjectEventEnum.TEST_PROGRAM_GENERATED))
    {
      resetDueToTestProgramGenerated();
    }
    else if (eventEnum.equals(ProjectEventEnum.TEST_PROGRAM_USER_GAIN_CHANGED))
    {
      resetDueToTestProgramUserGainChanged();
    }
    else if (eventEnum.equals(ProjectEventEnum.TEST_PROGRAM_STAGE_SPEED_CHANGED))
    {
      resetDueToTestProgramStageSpeedChanged();
    }
    else if(eventEnum.equals(ProjectEventEnum.TEST_PROGRAM_SIGNAL_OR_ARTIFACT_COMPENSATION_CHANGED))
    {
      resetDueToTestProgramSignalOrArtifactCompensationChanged();
    }
    else if (eventEnum.equals(ProjectEventEnum.VERIFICATION_IMAGES_GENERATED))
    {
      resetDueToVerificationImagesBeingGenerated();
    }
    else if (eventEnum.equals(ProjectEventEnum.TEST_PROGRAM_UPDATED_WITH_ALIGNMENT_REGIONS))
    {
      resetDueToTestProgramUpdated();
    }
    else if (eventEnum.equals(PanelSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_GENERATED))
    {
      resetDueTolAlignmentTransformGenerated();
    }
    else if (eventEnum.equals(BoardSettingsEventEnum.MANUAL_ALIGNMENT_TRANSFORM_GENERATED))
    {
      resetDueTolAlignmentTransformGenerated();
    }
    else if (eventEnum.equals(PanelSettingsEventEnum.POTENTIAL_LONG_PROGRAM_DETERMINED))
    {
      resetDueToPotentialLongProgramBeingDetermined();
    }
    else if (eventEnum.equals(ProjectEventEnum.TEST_PROGRAM_CHECK_SUM_INVALIDATED))
    {
      resetDueToTestProgramCheckSumHasReset();
    }
    else if (eventEnum.equals(ProjectEventEnum.TEST_PROGRAM_BOARD_Z_OFFSET_CHANGED))
    {
      resetDueToTestProgramBoardZOffsetChanged();
    }
    else if (eventEnum.equals(ProjectEventEnum.LOW_MAGNIFICATION_CHANGED)
      || eventEnum.equals(ProjectEventEnum.HIGH_MAGNIFICATION_CHANGED))
    {
      resetDueToMagnificationHasChanged();
    }
    //XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
    else if (eventEnum.equals(AlignmentGroupEventEnum.USER_GAIN) || 
             eventEnum.equals(AlignmentGroupEventEnum.SIGNAL_COMPENSATION) || 
             eventEnum.equals(AlignmentGroupEventEnum.STAGE_SPEED) || 
             eventEnum.equals(AlignmentGroupEventEnum.USE_CUSTOMIZED_ALIGNMENT))
    {
      Panel panel = Project.getCurrentlyLoadedProject().getPanel();
      PanelSettings panelSettings = panel.getPanelSettings();
      if (panelSettings.isPanelBasedAlignment())
      {
        panelSettings.clearAllAlignmentTransforms();
      }
      else
      {
        for (Board board : panel.getBoards())
        {
          board.getBoardSettings().clearAllAlignmentTransforms();
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private Object updateClassToEventMap(ProjectChangeEvent projChangeEvent,
                                       Map<Object, Map<ProjectChangeEventEnum, Object>> classToEventToOrigValueMap)
  {
    Assert.expect(projChangeEvent != null);
    Assert.expect(classToEventToOrigValueMap != null);

    boolean newStateChange = false;
    boolean destroy = false;
    ProjectChangeEventEnum projectChangeEventEnum = projChangeEvent.getProjectChangeEventEnum();
    Object classObject = projChangeEvent.getSource();
    Object oldValue = projChangeEvent.getOldValue();
    Object newValue = projChangeEvent.getNewValue();

    // if both the old and new value are null, then do not count this as a state change
    if ((oldValue == null) && (newValue == null))
      return null;

    // oldValue can be null
    // newValue can be null
    boolean addOrRemove = projectChangeEventEnum.isAddOrRemove();
    boolean add = false;
    if (addOrRemove)
    {
      if (newValue == null)
        add = false;
      else
        add = true;
    }

    // update classToEventToOrigAndNewValueMap based on the projectChangeEvent
    // set newStateChange to true if a new Event is added to _classObjectToEventToOrigAndNewValueMap.
    Map<ProjectChangeEventEnum, Object> eventToValueMap = classToEventToOrigValueMap.get(classObject);
    if (eventToValueMap == null)
    {
      // we have a new event, add it to the map to track what the original value is
      eventToValueMap = new HashMap<ProjectChangeEventEnum, Object>();
      if (addOrRemove == false)
        eventToValueMap.put(projectChangeEventEnum, oldValue);
      else
      {
        // ADD_OR_REMOVE
        // for an add or remove we need to keep a Pair of Set objects - an addedSet and
        // a removedSet
        Set<Object> addSet = new HashSet<Object>();
        Set<Object> removeSet = new HashSet<Object>();
        if (add)
          addSet.add(newValue);
        else
          removeSet.add(oldValue);
        Pair<Set<Object>, Set<Object>> addAndRemoveSetPair = new Pair<Set<Object>, Set<Object>>(addSet, removeSet);

        eventToValueMap.put(projectChangeEventEnum, addAndRemoveSetPair);
      }
      Map<ProjectChangeEventEnum, Object> prev = classToEventToOrigValueMap.put(classObject, eventToValueMap);
      Assert.expect(prev == null);
      newStateChange = true;
    }
    else
    {
      boolean containsKey = eventToValueMap.containsKey(projectChangeEventEnum);
      Object origValue = eventToValueMap.get(projectChangeEventEnum);

      if (containsKey == false)
      {
        // this event has not happened before - add it to the map to show the state change and
        // hold the old and new values
        if (addOrRemove == false)
          eventToValueMap.put(projectChangeEventEnum, oldValue);
        else
        {
          // ADD_OR_REMOVE
          // for an add or remove we need to keep a Pair of Set objects - an addedSet and
          // a removedSet
          Set<Object> addSet = new HashSet<Object>();
          Set<Object> removeSet = new HashSet<Object>();
          if (add)
            addSet.add(newValue);
          else
            removeSet.add(oldValue);
          Pair<Set<Object>, Set<Object>> addAndRemoveSetPair = new Pair<Set<Object>, Set<Object>>(addSet, removeSet);

          eventToValueMap.put(projectChangeEventEnum, addAndRemoveSetPair);
        }

        newStateChange = true;
      }
      else
      {
        if (addOrRemove == false)
        {
          // we have seen this event before, check to see if the new value matches the
          // original value stored
          if (((origValue == null) && (newValue == null)) ||
              ((origValue != null) && (origValue.equals(newValue))))
          {
            // the new value equals the original value, so remove the event from the list since
            // we are back to a no state change state
            eventToValueMap.remove(projectChangeEventEnum);
            newStateChange = false;

            if (eventToValueMap.isEmpty())
            {
              // if the eventToValueMap is empty then remove the classObject from the
              // map so that _classObjectToEventToOrigValueMapForTestProgram.isEmpty()
              // can be used to determine if the TestProgram state has changed
              classToEventToOrigValueMap.remove(classObject);
            }
            else
            {
              if (projectChangeEventEnum.isCreateOrDestroy())
              {
                if ((oldValue != null) && (newValue == null))
                  destroy = true;
              }
            }
          }
          else
          {
            // the new value is not the original value, so keep the original value in the map
            newStateChange = true;
          }
        }
        else
        {
          // ADD_OR_REMOVE
          // we have seen this event before, check to see if the new value matches the
          // original value stored

          Pair<Set<Object>, Set<Object>> origAddAndRemoveSetPair = (Pair<Set<Object>, Set<Object>>)origValue;
          Set<Object> addSet = origAddAndRemoveSetPair.getFirst();
          Set<Object> removeSet = origAddAndRemoveSetPair.getSecond();
          if (add)
          {
            boolean removed = removeSet.remove(newValue);
            if (removed == false)
              addSet.add(newValue);
          }
          else
          {
            boolean removed = addSet.remove(oldValue);
            if (removed == false)
              removeSet.add(oldValue);
          }
          if (addSet.isEmpty() && removeSet.isEmpty())
          {
            // the new value equals the original value, so remove the event from the list since
            // we are back to a no state change state
            eventToValueMap.remove(projectChangeEventEnum);
            newStateChange = false;

            if (eventToValueMap.isEmpty())
            {
              // if the eventToValueMap is empty then remove the classObject from the
              // map so that _classObjectToEventToOrigValueMapForTestProgram.isEmpty()
              // can be used to determine if the TestProgram state has changed
              classToEventToOrigValueMap.remove(classObject);
            }
            else
            {
              if (projectChangeEventEnum.isCreateOrDestroy())
              {
                if ((oldValue != null) && (newValue == null))
                  destroy = true;
              }
            }
          }
          else
          {
            // the new value is not the original value, so keep the original value in the map
            newStateChange = true;
          }
        }
      }
    }

    Object removeClassFromEvents = null;
    if ((newStateChange == false) && (destroy))
    {
      classToEventToOrigValueMap.remove(classObject);
      removeClassFromEvents = classObject;
    }

    return removeClassFromEvents;
  }

  /**
   * @author Bill Darbie
   */
  private void trackMajorAndMinorAndTestProgramState(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    ProjectChangeEventEnum projectChangeEventEnum = projChangeEvent.getProjectChangeEventEnum();
    // check to see if the test program is valid still or not
    Boolean programValid = _projectEventToTestProgramValidMap.get(projectChangeEventEnum);
    Assert.expect(programValid != null, "no map entry for event: " + projectChangeEventEnum);

    boolean programAlignmentRegionsValid = !_projectEventToTestProgramAlignmentRegionsInvalidatingEventSet.contains(projectChangeEventEnum);

    if (programValid == false)
    {
       Object classToRemove = updateClassToEventMap(projChangeEvent, _classToTestProgramEventToOrigValueMap);
       if (classToRemove != null)
       _classToMinorEventToOrigValueMap.remove(classToRemove);

       classToRemove = updateClassToEventMap(projChangeEvent, _classToMajorEventToOrigValueMap);
       if (classToRemove != null)
         _classToMinorEventToOrigValueMap.remove(classToRemove);
    }

    if (programAlignmentRegionsValid == false)
    {
      Object classToRemove = updateClassToEventMap(projChangeEvent, _classToTestProgramAlignmentRegionsEventToOrigValueMap);
      if (classToRemove != null)
        _classToMinorEventToOrigValueMap.remove(classToRemove);
    }

    if (programValid)
    {
      // some ProjectEventEnum events do not affect minor state change, all others that
      // did not invalidate the TestProgram do
      if ((projectChangeEventEnum instanceof ProjectEventEnum))
      {
        Boolean minorChange = _projectEventToProjectMinorChangeMap.get(projectChangeEventEnum);
        Assert.expect(minorChange != null);
        if (minorChange)
        {
          Object classToRemove = updateClassToEventMap(projChangeEvent, _classToMinorEventToOrigValueMap);
          if (classToRemove != null)
          {
            _classToTestProgramEventToOrigValueMap.remove(classToRemove);
            _classToTestProgramAlignmentRegionsEventToOrigValueMap.remove(classToRemove);
            _classToMajorEventToOrigValueMap.remove(classToRemove);
          }
        }
      }
      else if (projectChangeEventEnum.equals(AlignmentGroupEventEnum.MATCH_QUALITY) ||
               projectChangeEventEnum.equals(AlignmentGroupEventEnum.FOREGROUND_GRAY_LEVEL) ||
               projectChangeEventEnum.equals(AlignmentGroupEventEnum.BACKGROUND_GRAY_LEVEL))
      {
        // do nothing - since the match quality, and gray levels are always written to disk,
        // they shouldn't affect project state in any way.
      }
//      else if(projectChangeEventEnum.equals(SubtypeAdvanceSettingsEventEnum.USER_GAIN_ENUM))
//      {
//        //Object classToRemove = updateClassToEventMap(projChangeEvent, _classToTestProgramUserGainEventToOrigValueMap);
//        //if (classToRemove != null)
//        //  _classToTestProgramUserGainEventToOrigValueMap.remove(classToRemove);
//        
//      }
      else if(projectChangeEventEnum.equals(BoardSettingsEventEnum.BOARD_ZOFFSET))
      {
        Object classToRemove = updateClassToEventMap(projChangeEvent, _classToTestProgramBoardZOffsetEventToOrigValueMap);
        if (classToRemove != null)
          _classToTestProgramBoardZOffsetEventToOrigValueMap.remove(classToRemove);
        
      }
      else if(projectChangeEventEnum.equals(SubtypeAdvanceSettingsEventEnum.SIGNAL_COMPENSATION_ENUM) ||
              projectChangeEventEnum.equals(SubtypeAdvanceSettingsEventEnum.ARTIFACT_COMPENSATION_STATE_ENUM))
      {
        Object classToRemove = updateClassToEventMap(projChangeEvent, _classToTestProgramSignalOrArtifactCompensationEventToOrigValueMap);
        if (classToRemove != null)
          _classToTestProgramSignalOrArtifactCompensationEventToOrigValueMap.remove(classToRemove);
      }
      else
      {
        Object classToRemove = updateClassToEventMap(projChangeEvent, _classToMinorEventToOrigValueMap);
        if (classToRemove != null)
        {
          _classToTestProgramEventToOrigValueMap.remove(classToRemove);
          _classToTestProgramAlignmentRegionsEventToOrigValueMap.remove(classToRemove);
          _classToMajorEventToOrigValueMap.remove(classToRemove);
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private void trackVerificationImageState(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    ProjectChangeEventEnum projectChangeEventEnum = projChangeEvent.getProjectChangeEventEnum();
    // check to see if the verification images are invalid or not
    if (_verificationImageInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      updateClassToEventMap(projChangeEvent, _classToVerificationImagesEventToOrigValueMap);
    }
  }

  /**
   * @author Bill Darbie
   * @author Tan Hock Zoon
   */
  private void trackAlignmentTransformState(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    ProjectChangeEventEnum projectChangeEventEnum = projChangeEvent.getProjectChangeEventEnum();
    if (_alignmentTransformInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      //check if the Joint Type has change
      if (projectChangeEventEnum.equals(ComponentTypeEventEnum.JOINT_TYPE_ENUM))
      {
        //check if the Joint Type is Special Two Pin component
        if (ImageAnalysis.isSpecialTwoPinComponent((JointTypeEnum)projChangeEvent.getNewValue()))
        {
          boolean updateMap = false;

          //retrieve the list of alignment groups
          List<AlignmentGroup> alignmentGroups = null;
          if(_project.getPanel().getPanelSettings().isPanelBasedAlignment())
          {
            alignmentGroups = _project.getPanel().getPanelSettings().getAllAlignmentGroups();
          }
          else
          {
            alignmentGroups = new ArrayList<AlignmentGroup>();
            for(Board board : _project.getPanel().getBoards())
            {
              alignmentGroups.addAll(board.getBoardSettings().getAllAlignmentGroups());
            }
          }

          //loop thru the alignment groups and check if all the require pad available
          for (AlignmentGroup alignmentGroup : alignmentGroups)
          {
            updateMap = alignmentGroup.isAlignmentGroupJointTypeInconsistent();
          }

          //if pad is incomplete then update the map class
          if (updateMap)
          {
            updateClassToEventMap(projChangeEvent, _classToManualAlignmentTransformEventToOrigValueMap);
          }
          else
          {
            _classToManualAlignmentTransformEventToOrigValueMap.remove(projChangeEvent.getSource());
          }
        }
        else
        {
          _classToManualAlignmentTransformEventToOrigValueMap.remove(projChangeEvent.getSource());
        }
      }
      else
      {
        updateClassToEventMap(projChangeEvent, _classToManualAlignmentTransformEventToOrigValueMap);
      }
    }
    else if (_boardSpecificAlignmentTransformInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      // now check to see if the event happened on a board that contains an alignment pad
      // or fiducial, only if it did should it affect the map
      Board boardThatGeneratedEvent = (Board)projChangeEvent.getSource();
      boolean eventAdded = false;
      for (Pad pad : _project.getPanel().getPanelSettings().getAllAlignmentPads())
      {
        Board board = pad.getComponent().getBoard();
        if (board == boardThatGeneratedEvent)
        {
          updateClassToEventMap(projChangeEvent, _classToManualAlignmentTransformEventToOrigValueMap);
          eventAdded = true;
          break;
        }
      }
      if (eventAdded == false)
      {
        // an event was not added for pads, check to see if a fiducial exists on the board
        // the event was generated on
        for (Fiducial fiducial : _project.getPanel().getPanelSettings().getAllAlignmentFiducials())
        {
          if (fiducial.isOnBoard())
          {
            Board board = fiducial.getSideBoard().getBoard();
            if (board == boardThatGeneratedEvent)
            {
              updateClassToEventMap(projChangeEvent, _classToManualAlignmentTransformEventToOrigValueMap);
              break;
            }
          }
        }
      }
    }
    else if (_componentSpecificAlignmentTransformInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      // now check to see if the event happened on a board that contains an alignment pad
      // or fiducial, only if it did should it affect the map
      Component componentThatGeneratedEvent = (Component)projChangeEvent.getSource();
      for (Pad pad : _project.getPanel().getPanelSettings().getAllAlignmentPads())
      {
        Component component = pad.getComponent();
        if (component == componentThatGeneratedEvent)
        {
          updateClassToEventMap(projChangeEvent, _classToManualAlignmentTransformEventToOrigValueMap);
          break;
        }
      }
    }
    else if (_componentTypeSpecificAlignmentTransformInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      // now check to see if the event happened on a board that contains an alignment pad
      // or fiducial, only if it did should it affect the map
      ComponentType componentTypeThatGeneratedEvent = (ComponentType)projChangeEvent.getSource();
      for (Pad pad : _project.getPanel().getPanelSettings().getAllAlignmentPads())
      {
        ComponentType componentType = pad.getComponent().getComponentType();
        if (componentType == componentTypeThatGeneratedEvent)
        {
          updateClassToEventMap(projChangeEvent, _classToManualAlignmentTransformEventToOrigValueMap);
          break;
        }
      }
    }
    else if (_landPatternSpecificAlignmentTransformInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      // now check to see if the event happened on a board that contains an alignment pad
      // or fiducial, only if it did should it affect the map
      LandPattern landPatternThatGeneratedEvent = (LandPattern)projChangeEvent.getSource();
      LandPatternPad landPatternPadThatGeneratedEvent = (LandPatternPad)projChangeEvent.getOldValue();
      // if the LandPatternPad was just created, then do not try to get its landPattern since it will not be set yet
      for (Pad pad : _project.getPanel().getPanelSettings().getAllAlignmentPads())
      {
        LandPatternPad landPatternPad = pad.getLandPatternPad();
        if (landPatternPad == landPatternPadThatGeneratedEvent)
        {
          updateClassToEventMap(projChangeEvent, _classToManualAlignmentTransformEventToOrigValueMap);
          break;
        }
      }
    }
    else if (_landPatternPadSpecificAlignmentTransformInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      // now check to see if the event happened on a board that contains an alignment pad
      // or fiducial, only if it did should it affect the map
      LandPatternPad landPatternPadThatGeneratedEvent = (LandPatternPad)projChangeEvent.getSource();

      // if the LandPatternPad was just created, then do not try to get its landPattern since it will not be set yet
      for (Pad pad : _project.getPanel().getPanelSettings().getAllAlignmentPads())
      {
        LandPatternPad landPatternPad = pad.getLandPatternPad();
        if (landPatternPad == landPatternPadThatGeneratedEvent)
        {
          updateClassToEventMap(projChangeEvent, _classToManualAlignmentTransformEventToOrigValueMap);
          break;
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private void trackShortLearningInvalidRecordsInFileState(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    Object source = projChangeEvent.getSource();
    ProjectChangeEventEnum projectChangeEventEnum = projChangeEvent.getProjectChangeEventEnum();

    // Check to see if we have an event that should invalidate records in the short learning file
    if (_shortLearningRecordsInFileInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      // there are certain algorithm settings that, if tuned, will invalidate things
      if (projectChangeEventEnum.equals(SubtypeEventEnum.TUNED_VALUE))
      {
        // now check to see if the event happened with an AlgorithmSettingEnum of
        // SHARED_SHORT_REGION_INNER_EDGE_LOCATION or SHARED_SHORT_REGION_OUTER_EDGE_LOCATION
        // or SHARED_SHORT_MINIMUM_PIXELS_SHORT_ROI_REGION
        Pair<AlgorithmSettingEnum, Serializable> newValue = (Pair<AlgorithmSettingEnum, Serializable>)projChangeEvent.getNewValue();
        AlgorithmSettingEnum algSettingEnum = newValue.getFirst();

        if ((algSettingEnum.equals(AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION)) ||
            (algSettingEnum.equals(AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION)) ||
            (algSettingEnum.equals(AlgorithmSettingEnum.SHARED_SHORT_MINIMUM_PIXELS_SHORT_ROI_REGION)) ||
          //Lim, Lay Ngor - XCR1743:Benchmark
          (algSettingEnum.equals(AlgorithmSettingEnum.SHARED_SHORT_CENTER_EFFECTIVE_LENGTH)) || 
          (algSettingEnum.equals(AlgorithmSettingEnum.SHARED_SHORT_CENTER_EFFECTIVE_WIDTH)))
        {
          updateClassToEventMap(projChangeEvent, _classToShortLearningInvalidRecordsInFileEventToOrigValueMap);
        }
      }
      // there is a special class of algorithm settings that affect sliceheights.  these also should invalidate the short learning
      if (projectChangeEventEnum.equals(SubtypeEventEnum.TUNED_SLICE_HEIGHT_VALUE) ||
          projectChangeEventEnum.equals(SubtypeEventEnum.VOID_METHOD)||
          projectChangeEventEnum.equals(SubtypeEventEnum.MASK_IMAGE))
      {
        updateClassToEventMap(projChangeEvent, _classToShortLearningInvalidRecordsInFileEventToOrigValueMap);
      
        // XCR1677 - When fine tuning subype, need to regenerate test program everytime slice setup is changed.
        updateClassToEventMap(projChangeEvent, _classToTunedSliceHeightInvalidRecordsInFileEventToOrigValueMap);
      }
    }
    else if (_shortLearningRecordsInFileValidEventSet.contains(projectChangeEventEnum))
    {
      // This kind of event says that the short learning for the given subtype is no longer "invalid".
      // Remove that subtype from the map that shows short learning as being invalidated.
      // This is necessary to prevent infinite re-deleting of learned data.

      // PE:  This can happen when the user does things like running Short learning, or if they delete the learning
      // completely.  For example, consider this two step use case:  The user might tune some setting that invalidates
      // learning for a subtype.  This is handled by the code above and will result in the subtype's data being considered
      // invalid.  Then suppose the user deletes all their learned data.  When that happens, we no longer consider the
      // learning to be invalid (rather it is in a third state of being Non-Existant).  That last step is what this branch
      // of code is trying to handle.

      Subtype subtype = (Subtype)source;
      _classToShortLearningInvalidRecordsInFileEventToOrigValueMap.remove(subtype);
    }

    if (_shortLearningRecordsInFileValidatingOrInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      // Must be a joint type change on a ComponentType or CompPackage.  Figure out what to do based on one of three possibilities:
      //   1.  No previous joint type recorded.  Add it to the state map.
      //   2.  Joint type differs from the previously recorded joint type.  Do nothing (preserve the original value).
      //   3.  Joint type is the same as the previously recorded joint type.  Remove from the state map.

      // PE: Why does Bill say this must be a ComponentType or CompPackage event?  Does this mean we should rename the
      // event set to something like "_componentOrPackageEventsAffectingShortLearning" to make that more clear?
      // Or is there some kind of Assert we can put in to enforce this?  Bill's name seems like a long winded way of saying
      // "miscellaneous events".
      Map<ProjectChangeEventEnum, Object> eventEnumToOrigValueMap = _classToShortLearningInvalidRecordsInFileEventToOrigValueMap.get(source);
      if (eventEnumToOrigValueMap != null)
      {
        Object origValue = eventEnumToOrigValueMap.get(projectChangeEventEnum);
        if (origValue != null)
        {
          Object newValue = projChangeEvent.getNewValue();
          if (origValue.equals(newValue))
          {
            // Joint type is the same as the previously recorded joint type.  Remove from the state map.
            _classToShortLearningInvalidRecordsInFileEventToOrigValueMap.remove(source);
          }
          else
          {
            // Joint type differs from the previously recorded joint type.  Do nothing (preserve the original value).
          }
        }
        else
        {
          // No previous joint type recorded.  Add it to the state map.
          updateClassToEventMap(projChangeEvent, _classToShortLearningInvalidRecordsInFileEventToOrigValueMap);
        }
      }
      else
      {
        // No previous joint type recorded.  Add it to the state map.
        updateClassToEventMap(projChangeEvent, _classToShortLearningInvalidRecordsInFileEventToOrigValueMap);
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private void trackShortLearningInvalidFileState(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    ProjectChangeEventEnum projectChangeEventEnum = projChangeEvent.getProjectChangeEventEnum();
    if (_shortLearningFileInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      updateClassToEventMap(projChangeEvent, _classToShortLearningInvalidFileEventToOrigValueMap);
    }
  }

  /**
   * @author George Booth
   */
  private void trackExpectedImageLearningInvalidRecordsInFileState(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    Object source = projChangeEvent.getSource();
    ProjectChangeEventEnum projectChangeEventEnum = projChangeEvent.getProjectChangeEventEnum();
    if (_expectedImageLearningRecordsInFileInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      if (projectChangeEventEnum.equals(SubtypeEventEnum.TUNED_VALUE))
      {
        /** @todo GLB what algo settings invalidate Expected Image? */
        // now check to see if the event happened with an AlgorithmSettingEnum of
        // SHARED_SHORT_REGION_INNER_EDGE_LOCATION or SHARED_SHORT_REGION_OUTER_EDGE_LOCATION
        Pair<AlgorithmSettingEnum, Serializable> newValue = (Pair<AlgorithmSettingEnum, Serializable>)projChangeEvent.getNewValue();
        AlgorithmSettingEnum algSettingEnum = newValue.getFirst();

//        if ((algSettingEnum.equals(AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION)) ||
//            (algSettingEnum.equals(AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION)))
//        {
//          updateClassToEventMap(projChangeEvent, _classToExpectedImageLearningInvalidRecordsInFileEventToOrigValueMap);
//        }
      }
    }
    else if (_expectedImageLearningRecordsInFileValidEventSet.contains(projectChangeEventEnum))
    {
      // Expected Image learning for the given subtype is no longer "invalid".
      // Remove that subtype from the map that shows Expected Image learning as being invalidated.
      // This is necessary to prevent infinite re-deleting of learned data.
      Subtype subtype = (Subtype)source;
      _classToExpectedImageLearningInvalidRecordsInFileEventToOrigValueMap.remove(subtype);
    }

    if (_expectedImageLearningRecordsInFileValidatingOrInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      // Must be a joint type change on a ComponentType or CompPackage.  Figure out what to do based on one of three possibilities:
      //   1.  No previous joint type recorded.  Add it to the state map.
      //   2.  Joint type differs from the previously recorded joint type.  Do nothing (preserve the original value).
      //   3.  Joint type is the same as the previously recorded joint type.  Remove from the state map.
      Map<ProjectChangeEventEnum, Object> eventEnumToOrigValueMap = _classToExpectedImageLearningInvalidRecordsInFileEventToOrigValueMap.get(source);
      if (eventEnumToOrigValueMap != null)
      {
        Object origValue = eventEnumToOrigValueMap.get(projectChangeEventEnum);
        if (origValue != null)
        {
          Object newValue = projChangeEvent.getNewValue();
          if (origValue.equals(newValue))
          {
            // Joint type is the same as the previously recorded joint type.  Remove from the state map.
            _classToExpectedImageLearningInvalidRecordsInFileEventToOrigValueMap.remove(source);
          }
          else
          {
            // Joint type differs from the previously recorded joint type.  Do nothing (preserve the original value).
          }
        }
        else
        {
          // No previous joint type recorded.  Add it to the state map.
          updateClassToEventMap(projChangeEvent, _classToExpectedImageLearningInvalidRecordsInFileEventToOrigValueMap);
        }
      }
      else
      {
        // No previous joint type recorded.  Add it to the state map.
        updateClassToEventMap(projChangeEvent, _classToExpectedImageLearningInvalidRecordsInFileEventToOrigValueMap);
      }
    }
  }

  /**
   * @author George Booth
   */
  private void trackExpectedImageLearningInvalidFileState(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    ProjectChangeEventEnum projectChangeEventEnum = projChangeEvent.getProjectChangeEventEnum();
    if (_expectedImageLearningFileInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      updateClassToEventMap(projChangeEvent, _classToExpectedImageLearningInvalidFileEventToOrigValueMap);
    }
  }
  
  /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor - Broken Pin
   */
  private void trackBrokenPinLearningInvalidRecordsInFileState(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    Object source = projChangeEvent.getSource();
    ProjectChangeEventEnum projectChangeEventEnum = projChangeEvent.getProjectChangeEventEnum();

    // Check to see if we have an event that should invalidate records in the short learning file
    if (_brokenPinLearningRecordsInFileInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      // there are certain algorithm settings that, if tuned, will invalidate things
      if (projectChangeEventEnum.equals(SubtypeEventEnum.TUNED_VALUE))
      {
        // now check to see if the event happened with an AlgorithmSettingEnum of
        // SHARED_SHORT_REGION_INNER_EDGE_LOCATION or SHARED_SHORT_REGION_OUTER_EDGE_LOCATION
        // or SHARED_SHORT_MINIMUM_PIXELS_SHORT_ROI_REGION
        Pair<AlgorithmSettingEnum, Serializable> newValue = (Pair<AlgorithmSettingEnum, Serializable>)projChangeEvent.getNewValue();
        AlgorithmSettingEnum algSettingEnum = newValue.getFirst();

        if ((algSettingEnum.equals(AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION)) ||
            (algSettingEnum.equals(AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION)))
        {
          updateClassToEventMap(projChangeEvent, _classToBrokenPinLearningInvalidRecordsInFileEventToOrigValueMap);
        }
      }
      // there is a special class of algorithm settings that affect sliceheights.  these also should invalidate the short learning
      if (projectChangeEventEnum.equals(SubtypeEventEnum.TUNED_SLICE_HEIGHT_VALUE) ||
          projectChangeEventEnum.equals(SubtypeEventEnum.VOID_METHOD)||
          projectChangeEventEnum.equals(SubtypeEventEnum.MASK_IMAGE))
      {
        updateClassToEventMap(projChangeEvent, _classToBrokenPinLearningInvalidRecordsInFileEventToOrigValueMap);
      
        // XCR1677 - When fine tuning subype, need to regenerate test program everytime slice setup is changed.
        updateClassToEventMap(projChangeEvent, _classToTunedSliceHeightInvalidRecordsInFileEventToOrigValueMap);
      }
    }
    else if (_brokenPinLearningRecordsInFileValidEventSet.contains(projectChangeEventEnum))
    {
      // This kind of event says that the short learning for the given subtype is no longer "invalid".
      // Remove that subtype from the map that shows short learning as being invalidated.
      // This is necessary to prevent infinite re-deleting of learned data.

      // PE:  This can happen when the user does things like running Short learning, or if they delete the learning
      // completely.  For example, consider this two step use case:  The user might tune some setting that invalidates
      // learning for a subtype.  This is handled by the code above and will result in the subtype's data being considered
      // invalid.  Then suppose the user deletes all their learned data.  When that happens, we no longer consider the
      // learning to be invalid (rather it is in a third state of being Non-Existant).  That last step is what this branch
      // of code is trying to handle.

      Subtype subtype = (Subtype)source;
      _classToBrokenPinLearningInvalidRecordsInFileEventToOrigValueMap.remove(subtype);
    }

    if (_brokenPinLearningRecordsInFileValidatingOrInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      // Must be a joint type change on a ComponentType or CompPackage.  Figure out what to do based on one of three possibilities:
      //   1.  No previous joint type recorded.  Add it to the state map.
      //   2.  Joint type differs from the previously recorded joint type.  Do nothing (preserve the original value).
      //   3.  Joint type is the same as the previously recorded joint type.  Remove from the state map.

      // PE: Why does Bill say this must be a ComponentType or CompPackage event?  Does this mean we should rename the
      // event set to something like "_componentOrPackageEventsAffectingShortLearning" to make that more clear?
      // Or is there some kind of Assert we can put in to enforce this?  Bill's name seems like a long winded way of saying
      // "miscellaneous events".
      Map<ProjectChangeEventEnum, Object> eventEnumToOrigValueMap = _classToBrokenPinLearningInvalidRecordsInFileEventToOrigValueMap.get(source);
      if (eventEnumToOrigValueMap != null)
      {
        Object origValue = eventEnumToOrigValueMap.get(projectChangeEventEnum);
        if (origValue != null)
        {
          Object newValue = projChangeEvent.getNewValue();
          if (origValue.equals(newValue))
          {
            // Joint type is the same as the previously recorded joint type.  Remove from the state map.
            _classToBrokenPinLearningInvalidRecordsInFileEventToOrigValueMap.remove(source);
          }
          else
          {
            // Joint type differs from the previously recorded joint type.  Do nothing (preserve the original value).
          }
        }
        else
        {
          // No previous joint type recorded.  Add it to the state map.
          updateClassToEventMap(projChangeEvent, _classToBrokenPinLearningInvalidRecordsInFileEventToOrigValueMap);
        }
      }
      else
      {
        // No previous joint type recorded.  Add it to the state map.
        updateClassToEventMap(projChangeEvent, _classToBrokenPinLearningInvalidRecordsInFileEventToOrigValueMap);
      }
    }
  }  

  /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor - Broken Pin
   */
  private void trackBrokenPinLearningInvalidFileState(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    ProjectChangeEventEnum projectChangeEventEnum = projChangeEvent.getProjectChangeEventEnum();
    if (_brokenPinLearningFileInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      updateClassToEventMap(projChangeEvent, _classToBrokenPinLearningInvalidFileEventToOrigValueMap);
    }
  }
  
  /**
   * Clear Tombstone
   * @author sheng chuan
   * @param projChangeEvent 
   */
  private void trackExpectedImageTemplateLearningInvalidRecordsInFileState(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    Object source = projChangeEvent.getSource();
    ProjectChangeEventEnum projectChangeEventEnum = projChangeEvent.getProjectChangeEventEnum();
    if (_expectedImageTemplateLearningRecordsInFileValidEventSet.contains(projectChangeEventEnum))
    {
      // Expected Image learning for the given subtype is no longer "invalid".
      // Remove that subtype from the map that shows Expected Image learning as being invalidated.
      // This is necessary to prevent infinite re-deleting of learned data.
      Subtype subtype = (Subtype)source;
      _classToExpectedImageTemplateLearningInvalidRecordsInFileEventToOrigValueMap.remove(subtype);
    }

    if (_expectedImageTemplateLearningRecordsInFileValidatingOrInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      // Must be a joint type change on a ComponentType or CompPackage.  Figure out what to do based on one of three possibilities:
      //   1.  No previous joint type recorded.  Add it to the state map.
      //   2.  Joint type differs from the previously recorded joint type.  Do nothing (preserve the original value).
      //   3.  Joint type is the same as the previously recorded joint type.  Remove from the state map.
      Map<ProjectChangeEventEnum, Object> eventEnumToOrigValueMap = _classToExpectedImageTemplateLearningInvalidRecordsInFileEventToOrigValueMap.get(source);
      if (eventEnumToOrigValueMap != null)
      {
        Object origValue = eventEnumToOrigValueMap.get(projectChangeEventEnum);
        if (origValue != null)
        {
          Object newValue = projChangeEvent.getNewValue();
          if (origValue.equals(newValue))
          {
            // Joint type is the same as the previously recorded joint type.  Remove from the state map.
            _classToExpectedImageTemplateLearningInvalidRecordsInFileEventToOrigValueMap.remove(source);
          }
          else
          {
            // Joint type differs from the previously recorded joint type.  Do nothing (preserve the original value).
          }
        }
        else
        {
          // No previous joint type recorded.  Add it to the state map.
          updateClassToEventMap(projChangeEvent, _classToExpectedImageTemplateLearningInvalidRecordsInFileEventToOrigValueMap);
        }
      }
      else
      {
        // No previous joint type recorded.  Add it to the state map.
        updateClassToEventMap(projChangeEvent, _classToExpectedImageTemplateLearningInvalidRecordsInFileEventToOrigValueMap);
      }
    }
  }

  /**
   * Clear Tombstone
   * @author sheng chuan
   */
  private void trackExpectedImageTemplateLearningInvalidFileState(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    ProjectChangeEventEnum projectChangeEventEnum = projChangeEvent.getProjectChangeEventEnum();
    if (_expectedImageTemplateLearningFileInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      updateClassToEventMap(projChangeEvent, _classToExpectedImageTemplateLearningInvalidFileEventToOrigValueMap);
    }
  }

  /**
   * @author Bill Darbie
   */
  private void trackSubtypeLearningInvalidFileState(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    ProjectChangeEventEnum projectChangeEventEnum = projChangeEvent.getProjectChangeEventEnum();
    if (_subtypeLearningFileInvalidatingEventSet.contains(projectChangeEventEnum))
    {
      updateClassToEventMap(projChangeEvent, _classToSubtypeLearningInvalidFileEventToOrigValueMap);
    }
  }


  /**
   * @author Bill Darbie
   */
  private void trackPotentialLongProgramState(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    ProjectChangeEventEnum projectChangeEventEnum = projChangeEvent.getProjectChangeEventEnum();
    // check to see if the verification images are invalid or not
    if (_potentialLongProgramInvalidEventSet.contains(projectChangeEventEnum))
    {
      updateClassToEventMap(projChangeEvent, _classToPotentialLongProgramEventToOrigValueMap);
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  private void trackTestProgramCheckSumState(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    ProjectChangeEventEnum projectChangeEventEnum = projChangeEvent.getProjectChangeEventEnum();
    // check to see if the test program check sum are invalid or not
    if (_testProgramCheckSumInvalidEventSet.contains(projectChangeEventEnum))
    {
      updateClassToEventMap(projChangeEvent, _classToTestProgramCheckSumEventToOrigValueMap);
    }
  }

  /**
   * @author Bill Darbie
   */
  private void printClass(Object clazz)
  {
    Assert.expect(clazz != null);

    if (clazz instanceof Project)
      System.out.println("Project");
    else if (clazz instanceof Panel)
    {
      System.out.println("Panel");
    }
    else if (clazz instanceof Board)
    {
      Board board = (Board)clazz;
      System.out.println("Board: " + board.getName());
    }
    else if (clazz instanceof Component)
    {
      Component component = (Component)clazz;
      System.out.println("Component: " + component.getBoardNameAndReferenceDesignator());
    }
    else if (clazz instanceof ComponentType)
    {
      ComponentType componentType = (ComponentType)clazz;
      System.out.println("ComponentType: " + componentType.getReferenceDesignator());
    }
    else if (clazz instanceof Pad)
    {
      Pad pad = (Pad)clazz;
      System.out.println("Pad: " + pad.getBoardAndComponentAndPadName());
    }
    else if (clazz instanceof LandPattern)
    {
      LandPattern landPattern = (LandPattern)clazz;
      System.out.println("LandPattern: " + landPattern.getName());
    }
    else if (clazz instanceof LandPatternPad)
    {
      LandPatternPad landPatternPad = (LandPatternPad)clazz;
      System.out.println("LandPatternPad: " + landPatternPad.getName());
    }
    else if (clazz instanceof PadTypeSettings)
    {
      PadTypeSettings padTypeSettings = (PadTypeSettings)clazz;
      System.out.println("PadTypeSettings: " + padTypeSettings.getPadType().getBoardAndComponentAndPadName());
    }
    else if (clazz instanceof PadSettings)
    {
      PadSettings padSettings = (PadSettings)clazz;
      System.out.println("PadSettings: ");
    }
    else if (clazz instanceof PadType)
    {
      PadType padType = (PadType)clazz;
      System.out.println("PadType: " + padType.getBoardAndComponentAndPadName());
    }
    else if (clazz instanceof Subtype)
    {
      Subtype subtype = (Subtype)clazz;
      System.out.println("Subtype: " + subtype.getLongName());
    }
    else if (clazz instanceof SideBoardType)
    {
      SideBoardType sideBoardType = (SideBoardType)clazz;
      System.out.println("SideBoardType: " + sideBoardType);
    }
    else if (clazz instanceof CompPackage)
    {
      CompPackage compPackage = (CompPackage)clazz;
      System.out.println("CompPackage: " + compPackage);
    }
    else if (clazz instanceof PackagePin)
    {
      PackagePin packagePin = (PackagePin)clazz;
      System.out.println("PackagePin: " + packagePin);
    }
    else
      System.out.println("class: " + clazz);
  }

  /**
   * @author Bill Darbie
   */
  void getEventsThatChangedState()
  {
    System.out.println("Test Program events:");
    getEventsThatChangedState(_classToTestProgramEventToOrigValueMap);

    System.out.println("Test Program Alignment Region events:");
    getEventsThatChangedState(_classToTestProgramAlignmentRegionsEventToOrigValueMap);

    System.out.println("Major state change events:");
    getEventsThatChangedState(_classToMajorEventToOrigValueMap);

    System.out.println("Minor state change events:");
    getEventsThatChangedState(_classToMinorEventToOrigValueMap);

    System.out.println("Alignment transform events:");
    getEventsThatChangedState(_classToManualAlignmentTransformEventToOrigValueMap);

    System.out.println("Verification Images events:");
    getEventsThatChangedState(_classToVerificationImagesEventToOrigValueMap);

    System.out.println("Shorts Records in Learning File events:");
    getEventsThatChangedState(_classToShortLearningInvalidRecordsInFileEventToOrigValueMap);

    System.out.println("Exoected Image Records in Learning File events:");
    getEventsThatChangedState(_classToExpectedImageLearningInvalidRecordsInFileEventToOrigValueMap);
    
    System.out.println("Broken Pin Records in Learning File events:"); //Broken Pin
    getEventsThatChangedState(_classToBrokenPinLearningInvalidRecordsInFileEventToOrigValueMap); //Broken Pin
  }

  /**
   * @author Bill Darbie
   */
  private void getEventsThatChangedState(Map<Object, Map<ProjectChangeEventEnum, Object>> classToEventToOrigValueMap)
  {
    System.out.println("\nState change events");
    for (Map.Entry<Object, Map<ProjectChangeEventEnum, Object>> entry1 : classToEventToOrigValueMap.entrySet())
    {
      Object clazz = entry1.getKey();
      Map<ProjectChangeEventEnum, Object> eventToOrigValueMap = entry1.getValue();
      for (Map.Entry<ProjectChangeEventEnum, Object> entry2 : eventToOrigValueMap.entrySet())
      {
        ProjectChangeEventEnum projChangeEventEnum = entry2.getKey();
        Object origValue = entry2.getValue();

        printClass(clazz);

        System.out.println("  event: " + projChangeEventEnum + " id: " + projChangeEventEnum.getId());
        System.out.println("  orig value: " + origValue);
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  void displayEventsInMap(Map<Object, Map<ProjectChangeEventEnum, Object>> classToEventToOrigValueMap)
  {
    Assert.expect(classToEventToOrigValueMap != null);

    for (Map.Entry<Object, Map<ProjectChangeEventEnum, Object>> entry1 : classToEventToOrigValueMap.entrySet())
    {
      Object clazz = entry1.getKey();
      Map<ProjectChangeEventEnum, Object> projChangeEventEnumMap = entry1.getValue();

      for (ProjectChangeEventEnum projChangeEventEnum : projChangeEventEnumMap.keySet())
      {
        printClass(clazz);
        System.out.println("  event: " + projChangeEventEnum + " id: " + projChangeEventEnum.getId());
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private void resetDueToProjectFastLoad()
  {
    // do nothing - fast load means that we read in the serialized file, which would already
    // have the correct state of all variables of this class
  }

  /**
   * @author Bill Darbie
   */
  private void resetDueToProjectSlowLoadOrImport()
  {
    _classToTestProgramEventToOrigValueMap.clear();
    _classToTestProgramAlignmentRegionsEventToOrigValueMap.clear();
    _classToMajorEventToOrigValueMap.clear();
    _classToMinorEventToOrigValueMap.clear();
    _classToVerificationImagesEventToOrigValueMap.clear();
    _classToManualAlignmentTransformEventToOrigValueMap.clear();
    _classToShortLearningInvalidRecordsInFileEventToOrigValueMap.clear();
    _classToShortLearningInvalidFileEventToOrigValueMap.clear();
    _classToExpectedImageLearningInvalidRecordsInFileEventToOrigValueMap.clear();
    _classToExpectedImageLearningInvalidFileEventToOrigValueMap.clear();
    _classToBrokenPinLearningInvalidRecordsInFileEventToOrigValueMap.clear();//Broken Pin
    _classToBrokenPinLearningInvalidFileEventToOrigValueMap.clear();//Broken Pin
    _classToExpectedImageTemplateLearningInvalidRecordsInFileEventToOrigValueMap.clear();//ShengChuan - Clear Tombstone
    _classToExpectedImageTemplateLearningInvalidFileEventToOrigValueMap.clear();//ShengChuan - Clear Tombstone
    _classToSubtypeLearningInvalidFileEventToOrigValueMap.clear();
    _classToPotentialLongProgramEventToOrigValueMap.clear();
    _classToTestProgramCheckSumEventToOrigValueMap.clear();
    _classToTunedSliceHeightInvalidRecordsInFileEventToOrigValueMap.clear();
    
    //Kee Chin Seong
    _classToTestProgramUserGainEventToOrigValueMap.clear();
    
    //Sheng Chuan
    _classToTestProgramStageSpeedEventToOrigValueMap.clear();
    
    // Goh Bee Hoon
    _classToTestProgramBoardZOffsetEventToOrigValueMap.clear();
   
    //Kee Chin Seong
    _classToTestProgramSignalOrArtifactCompensationEventToOrigValueMap.clear();

    
    Project project = Project.getCurrentlyLoadedProject();
    Panel panel = Project.getCurrentlyLoadedProject().getPanel();

    // _alignmentTransformValidOnProjectLoad must be set to true to have the panelSettings.areTransformsValid() call work properly
    _manualAlignmentTransformValidOnProjectLoad = true;
    _manualAlignmentTransformValidOnProjectLoad = panel.getPanelSettings().isManualAlignmentCompleted();

    // _testProgramValidOnProjectLoad must be set to true to have the project.isTestProgramValid() call work properly
    _testProgramValidOnProjectLoad = true;
    _testProgramValidOnProjectLoad = project.isTestProgramValid();

    // _testProgramAlignmentRegionsValidOnProjectLoad must be set to true to have the project.
    _testProgramAlignmentRegionsValidOnProjectLoad = true;
    _testProgramAlignmentRegionsValidOnProjectLoad = project.isTestProgramValidIgnoringAlignmentRegions();

    _potentialLongProgramDeterminedOnProjectLoad = true;
    _potentialLongProgramDeterminedOnProjectLoad = project.getPanel().getPanelSettings().isPotentialLongProgramDeterminationValid();
  }

  /**
   * @author Bill Darbie
   */
  public void resetDueToProjectSave()
  {
    _classToMajorEventToOrigValueMap.clear();
    _classToMinorEventToOrigValueMap.clear();

    // do not clear these maps since a save() should not have any affect on them
    //_classToTestProgramEventToOrigValueMap.clear();
    //_classToVerificationImagesEventMap.clear();
    //_classToAlignmentTransformEventMap.clear();
    //_classToTestProgramAlignmentRegionsEventToOrigValueMap.clear();
    //_classToShortLearningInvalidRecordsInFileEventToOrigValueMap.clear();
    //_classToPotentialLongProgramEventToOrigValueMap.clear();

    _classToShortLearningInvalidFileEventToOrigValueMap.clear();
    _classToExpectedImageLearningInvalidFileEventToOrigValueMap.clear();
    _classToBrokenPinLearningInvalidFileEventToOrigValueMap.clear();//Broken Pin
    _classToExpectedImageTemplateLearningInvalidFileEventToOrigValueMap.clear();//ShengChuan - Clear Tombstone
    _classToSubtypeLearningInvalidFileEventToOrigValueMap.clear();
    _classToTunedSliceHeightInvalidRecordsInFileEventToOrigValueMap.clear();
  }

  /**
   * @author Bill Darbie
   */
  private void resetDueToVerificationImagesBeingGenerated()
  {
    _classToVerificationImagesEventToOrigValueMap.clear();
  }

  /**
   * @author Bill Darbie
   */
  void resetDueToTestProgramGenerated()
  {
    _classToTestProgramEventToOrigValueMap.clear();
    _testProgramValidOnProjectLoad = true;

    _classToTestProgramAlignmentRegionsEventToOrigValueMap.clear();
    _testProgramAlignmentRegionsValidOnProjectLoad = true;

    _classToTestProgramCheckSumEventToOrigValueMap.clear();
    
    _classToTestProgramUserGainEventToOrigValueMap.clear();
    _classToTestProgramStageSpeedEventToOrigValueMap.clear();
    _classToTestProgramSignalOrArtifactCompensationEventToOrigValueMap.clear();
    _classToTestProgramBoardZOffsetEventToOrigValueMap.clear();
    _classToTunedSliceHeightInvalidRecordsInFileEventToOrigValueMap.clear();
  }
  
  /*
   * @author Kee Chin Seong
   */
  void resetDueToTestProgramUserGainChanged()
  {
    //Kee Chin Seong
    _classToTestProgramUserGainEventToOrigValueMap.clear();
  }
  
  /*
   * @author sheng chuan
   */
  void resetDueToTestProgramStageSpeedChanged()
  {
    //Kee Chin Seong
    _classToTestProgramStageSpeedEventToOrigValueMap.clear();
  }
    
  /*
   * @author Kee Chin Seong
   */
  void resetDueToTestProgramSignalOrArtifactCompensationChanged()
  {
    //Kee chin Seong
    _classToTestProgramSignalOrArtifactCompensationEventToOrigValueMap.clear();
  }
  
  /*
   * @author Goh Bee Hoon
   */
  void resetDueToTestProgramBoardZOffsetChanged()
  {
    _classToTestProgramBoardZOffsetEventToOrigValueMap.clear();
  }

  /**
   * @author Bill Darbie
   */
  private void resetDueToTestProgramUpdated()
  {
    _classToTestProgramAlignmentRegionsEventToOrigValueMap.clear();
    _testProgramAlignmentRegionsValidOnProjectLoad = true;
  }

  /**
   * @author Bill Darbie
   */
  private void resetDueTolAlignmentTransformGenerated()
  {
    _classToManualAlignmentTransformEventToOrigValueMap.clear();
    _manualAlignmentTransformValidOnProjectLoad = true;
  }

  /**
   * @author Bill Darbie
   */
  private void resetDueToPotentialLongProgramBeingDetermined()
  {
    _classToPotentialLongProgramEventToOrigValueMap.clear();
    _potentialLongProgramDeterminedOnProjectLoad = true;
  }

  /**
   * @author Wei Chin, Chong
   */
  private void resetDueToTestProgramCheckSumHasReset()
  {
    _classToTestProgramCheckSumEventToOrigValueMap.clear();
  }

  /**
   * @author Ngie Xing, Wong
   */
  private void resetDueToMagnificationHasChanged()
  { 
    //use the same function to clear alignment transforms for both low and high mag
    //because Run Alignment will run both low and high mag 
    //will need to make a separate function to clear alignment transforms if
    //low and high mag alignments can be separated
    Panel panel = Project.getCurrentlyLoadedProject().getPanel();
    PanelSettings panelSettings = panel.getPanelSettings();
    if (panelSettings.isPanelBasedAlignment())
    {
      panelSettings.clearAllAlignmentTransforms();
      for (int i = 0; i < 2; ++i)
      {
        List<AlignmentGroup> alignmentGroups = null;
        if (i == 0)
        {
          if (panelSettings.isLongPanel())
            alignmentGroups = panelSettings.getLeftAlignmentGroupsForLongPanel();
          else
            alignmentGroups = panelSettings.getAlignmentGroupsForShortPanel();
        }
        else if (i == 1)
        {
          if (panelSettings.isLongPanel())
            alignmentGroups = panelSettings.getRightAlignmentGroupsForLongPanel();
          else
            continue;
        }

        if (alignmentGroups.isEmpty())
          continue;

        for (AlignmentGroup alignmentGroup : alignmentGroups)
        {
          alignmentGroup.resetMatchQuality();
          alignmentGroup.resetForegroundGrayLevel();
          alignmentGroup.resetBackgroundGrayLevel();
        }
      }
    }
    else
    {
      for (Board board : panel.getBoards())
      {
        board.getBoardSettings().clearAllAlignmentTransforms();
        List<AlignmentGroup> alignmentGroups = new ArrayList<AlignmentGroup>();

        if (board.getBoardSettings().isLongBoard())
        {
          alignmentGroups.addAll(board.getBoardSettings().getLeftAlignmentGroupsForLongPanel());
          alignmentGroups.addAll(board.getBoardSettings().getRightAlignmentGroupsForLongPanel());
        }
        else
        {
          alignmentGroups.addAll(board.getBoardSettings().getAlignmentGroupsForShortPanel());
        }

        for (AlignmentGroup alignmentGroup : alignmentGroups)
        {
          alignmentGroup.resetMatchQuality();
          alignmentGroup.resetForegroundGrayLevel();
          alignmentGroup.resetBackgroundGrayLevel();
        }
      }
    }
    _manualAlignmentTransformValidOnProjectLoad = panel.getPanelSettings().isManualAlignmentCompleted();
    _classToManualAlignmentTransformEventToOrigValueMap.clear();

    //delete verification images also
    try
    {
      String verificationImagesFullPath = Directory.getXrayVerificationImagesDir(Project.getCurrentlyLoadedProject().getName());
      if (FileUtilAxi.exists(verificationImagesFullPath))
      {
        FileUtilAxi.delete(verificationImagesFullPath);
      }
      _classToVerificationImagesEventToOrigValueMap.clear();
    }
    catch (DatastoreException dex)
    {
      //do nothing
    }
  }

  /**
   * @author Bill Darbie
   */
  boolean isTestProgramValid()
  {
    if (isTestProgramValidIgnoringAlignmentRegions() == false)
      return false;

    if (_testProgramAlignmentRegionsValidOnProjectLoad == false)
      return false;

    if (_classToTestProgramAlignmentRegionsEventToOrigValueMap.isEmpty() == false)
      return false;
    
    if (_classToTestProgramUserGainEventToOrigValueMap.isEmpty() == false)
      return false;
    
    if(_classToTestProgramStageSpeedEventToOrigValueMap.isEmpty() == false)
      return false;
    
    if (_classToTestProgramSignalOrArtifactCompensationEventToOrigValueMap.isEmpty() == false)
      return false;
    
    if (_classToTestProgramBoardZOffsetEventToOrigValueMap.isEmpty() == false)
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  boolean isTestProgramValidIgnoringAlignmentRegions()
  {
    if (_testProgramValidOnProjectLoad == false)
      return false;

    if (_classToTestProgramEventToOrigValueMap.isEmpty())
      return true;

    return false;
  }
  
  /*
   * @author Kee Chin Seong
   * 
   */
  boolean isTestProgramValidIgnoringUserGainChanges()
  {
    //if (_testProgramValidOnProjectLoad == false)
    //  return false;
    
    if(_classToTestProgramUserGainEventToOrigValueMap.isEmpty())
      return true;
    
    return false;
  }
  
  /*
   * @author sheng chuan
   * 
   */
  boolean isTestProgramValidIgnoringStageSpeedChanges()
  {
    if(_classToTestProgramStageSpeedEventToOrigValueMap.isEmpty())
      return true;
    
    return false;
  }
  
  /*
   * @author Kee Chin Seong
   */
  boolean isTestProgramValidIgnoringSignalOrArtifactCompensationChanges()
  {
    if(_classToTestProgramSignalOrArtifactCompensationEventToOrigValueMap.isEmpty())
      return true;
    
    return false;
  }
  
  /*
   * @author Goh Bee Hoon
   */
  boolean isTestProgramValidIgnoringBoardZOffsetChanges()
  {
    if(_classToTestProgramBoardZOffsetEventToOrigValueMap.isEmpty())
      return true;
    
    return false;
  }
  
  /*
   * XCR1710 - Change subtype feature on Result tab.
   * @author Jack Hwee
   */
  boolean isTestProgramValidIgnoringTunedSliceHeightChanges()
  {
    if(_classToTunedSliceHeightInvalidRecordsInFileEventToOrigValueMap.isEmpty())
      return true;
    
    return false;
  }

  /**
   * @author Kee Chin Seong
   * @author Cheah Lee Herng
   */
  public List<Subtype> getUserGainChangeSubtype()
  {
    List<Subtype> subtypeList = new ArrayList<Subtype>();
    
    Iterator mySetIterator = _classToTestProgramUserGainEventToOrigValueMap.keySet().iterator();
    while (mySetIterator.hasNext()) 
    {
       SubtypeAdvanceSettings subtypeAdvanceSettings = (SubtypeAdvanceSettings)mySetIterator.next();
       if (subtypeList.contains(subtypeAdvanceSettings.getSubtype()) == false)
        subtypeList.add(subtypeAdvanceSettings.getSubtype());
    }
    
    return subtypeList;
  }
  
  /**
   * @author sheng chuan
   */
  public List<Subtype> getStageSpeedChangeSubtype()
  {
    List<Subtype> subtypeList = new ArrayList<Subtype>();
    
    Iterator mySetIterator = _classToTestProgramStageSpeedEventToOrigValueMap.keySet().iterator();
    while (mySetIterator.hasNext()) 
    {
       SubtypeAdvanceSettings subtypeAdvanceSettings = (SubtypeAdvanceSettings)mySetIterator.next();
       if (subtypeList.contains(subtypeAdvanceSettings.getSubtype()) == false)
        subtypeList.add(subtypeAdvanceSettings.getSubtype());
    }
    
    return subtypeList;
  }

  /**
   * @author Bill Darbie
   */
  boolean isProjectStateValid()
  {
    if (isProjectMajorStateValid() && isProjectMinorStateValid())
      return true;

    return false;
  }


  /**
   * @author Bill Darbie
   */
  boolean isProjectMajorStateValid()
  {
    if (_classToMajorEventToOrigValueMap.isEmpty())
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  boolean isProjectMinorStateValid()
  {
    if (_classToMinorEventToOrigValueMap.isEmpty())
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  boolean isManualAlignmentTransformValid()
  {
    if (_manualAlignmentTransformValidOnProjectLoad == false)
      return false;

    if (_classToManualAlignmentTransformEventToOrigValueMap.isEmpty())
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  boolean areVerificationImagesValid()
  {
    if (_classToVerificationImagesEventToOrigValueMap.isEmpty())
      return true;

    return false;
  }

  /**
   * @author Wei Chin, Chong
   */
  boolean isTestProgramCheckSumValid()
  {
    if (_classToTestProgramCheckSumEventToOrigValueMap.isEmpty())
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  boolean isPotentialLongProgramDeterminationValid()
  {
    if (_potentialLongProgramDeterminedOnProjectLoad == false)
      return false;

    if (_classToPotentialLongProgramEventToOrigValueMap.isEmpty())
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  Collection<Subtype> getShortLearningRecordsInFileInvalidSubtypes()
  {
    Assert.expect(_classToShortLearningInvalidRecordsInFileEventToOrigValueMap != null);

    Collection<Subtype> subtypes = new HashSet<Subtype>();
    for (Object object : _classToShortLearningInvalidRecordsInFileEventToOrigValueMap.keySet())
    {
      if (object instanceof Subtype)
      {
        Subtype subtype = (Subtype)object;
        subtypes.add(subtype);
      }
      else if (object instanceof ComponentType)
      {
        ComponentType componentType = (ComponentType)object;
        subtypes.addAll(componentType.getSubtypes());
      }
      else if (object instanceof CompPackage)
      {
        CompPackage compPackage = (CompPackage)object;
        subtypes.addAll(compPackage.getSubtypes());
      }
    }

    return subtypes;
  }

  /**
   * @author Matt Wharton
   */
  void clearShortLearningRecordsInFileInvalidSubtypes()
  {
    Assert.expect(_classToShortLearningInvalidRecordsInFileEventToOrigValueMap != null);

    _classToShortLearningInvalidRecordsInFileEventToOrigValueMap.clear();
  }

  /**
   * @author Bill Darbie
   */
  boolean areShortLearningRecordsValid()
  {
    if (isProjectMajorStateValid() == false)
      return false;

    if (_classToShortLearningInvalidRecordsInFileEventToOrigValueMap.isEmpty() == false)
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  boolean isShortLearningFileValid()
  {
    if (_classToShortLearningInvalidFileEventToOrigValueMap.isEmpty() == false)
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor - Broken Pin
   */
  boolean isBrokenPinLearningFileValid()
  {
    if (_classToBrokenPinLearningInvalidFileEventToOrigValueMap.isEmpty() == false)
      return false;

    return true;
  }  

  /**
   * @author Bill Darbie
   * @author George Booth
   */
  Collection<Subtype> getExpectedImageLearningRecordsInFileInvalidSubtypes()
  {
    Assert.expect(_classToExpectedImageLearningInvalidRecordsInFileEventToOrigValueMap != null);

    Collection<Subtype> subtypes = new HashSet<Subtype>();
    for (Object object : _classToExpectedImageLearningInvalidRecordsInFileEventToOrigValueMap.keySet())
    {
      if (object instanceof Subtype)
      {
        Subtype subtype = (Subtype)object;
        subtypes.add(subtype);
      }
      else if (object instanceof ComponentType)
      {
        ComponentType componentType = (ComponentType)object;
        subtypes.addAll(componentType.getSubtypes());
      }
      else if (object instanceof CompPackage)
      {
        CompPackage compPackage = (CompPackage)object;
        subtypes.addAll(compPackage.getSubtypes());
      }
    }

    return subtypes;
  }

  /**
   * @author George Booth
   */
  void clearExpectedImageLearningRecordsInFileInvalidSubtypes()
  {
    Assert.expect(_classToExpectedImageLearningInvalidRecordsInFileEventToOrigValueMap != null);

    _classToExpectedImageLearningInvalidRecordsInFileEventToOrigValueMap.clear();
  }

  /**
   * @author George Booth
   */
  boolean areExpectedImageLearningRecordsValid()
  {
    if (isProjectMajorStateValid() == false)
      return false;

    if (_classToExpectedImageLearningInvalidRecordsInFileEventToOrigValueMap.isEmpty() == false)
      return false;

    return true;
  }

  /**
   * @author George Booth
   */
  boolean isExpectedImageLearningFileValid()
  {
    if (_classToExpectedImageLearningInvalidFileEventToOrigValueMap.isEmpty() == false)
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   * @author Lim, Lay Ngor - Broken Pin
   */
  Collection<Subtype> getBrokenPinLearningRecordsInFileInvalidSubtypes()
  {
    Assert.expect(_classToBrokenPinLearningInvalidRecordsInFileEventToOrigValueMap != null);

    Collection<Subtype> subtypes = new HashSet<Subtype>();
    for (Object object : _classToBrokenPinLearningInvalidRecordsInFileEventToOrigValueMap.keySet())
    {
      if (object instanceof Subtype)
      {
        Subtype subtype = (Subtype)object;
        subtypes.add(subtype);
      }
      else if (object instanceof ComponentType)
      {
        ComponentType componentType = (ComponentType)object;
        subtypes.addAll(componentType.getSubtypes());
      }
      else if (object instanceof CompPackage)
      {
        CompPackage compPackage = (CompPackage)object;
        subtypes.addAll(compPackage.getSubtypes());
      }
    }

    return subtypes;
  }
  
  /**
   * @author Matt Wharton
   * @author Lim, Lay Ngor - Broken Pin
   */
  void clearBrokenPinLearningRecordsInFileInvalidSubtypes()
  {
    Assert.expect(_classToBrokenPinLearningInvalidRecordsInFileEventToOrigValueMap != null);

    _classToBrokenPinLearningInvalidRecordsInFileEventToOrigValueMap.clear();
  }
  
  /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor - Broken Pin
   */
  boolean areBrokenPinLearningRecordsValid()
  {
    if (isProjectMajorStateValid() == false)
      return false;

    if (_classToBrokenPinLearningInvalidRecordsInFileEventToOrigValueMap.isEmpty() == false)
      return false;

    return true;
  }  

  /**
   * Clear Tombstone
   * @author sheng chuan
   * @return 
   */
  Collection<Subtype> getExpectedImageTemplateLearningRecordsInFileInvalidSubtypes()
  {
    Assert.expect(_classToExpectedImageTemplateLearningInvalidRecordsInFileEventToOrigValueMap != null);

    Collection<Subtype> subtypes = new HashSet<Subtype>();
    for (Object object : _classToExpectedImageTemplateLearningInvalidRecordsInFileEventToOrigValueMap.keySet())
    {
      if (object instanceof Subtype)
      {
        Subtype subtype = (Subtype)object;
        subtypes.add(subtype);
      }
      else if (object instanceof ComponentType)
      {
        ComponentType componentType = (ComponentType)object;
        subtypes.addAll(componentType.getSubtypes());
      }
      else if (object instanceof CompPackage)
      {
        CompPackage compPackage = (CompPackage)object;
        subtypes.addAll(compPackage.getSubtypes());
      }
    }

    return subtypes;
  }

  /**
   * Clear Tombstone
   * @author sheng chuan
   */
  void clearExpectedImageTemplateLearningRecoardsInFileInvalidSubtypes()
  {
    Assert.expect(_classToExpectedImageTemplateLearningInvalidRecordsInFileEventToOrigValueMap != null);
    
    _classToExpectedImageTemplateLearningInvalidRecordsInFileEventToOrigValueMap.clear();
  }
  
  /**
   * Clear Tombstone
   * @author sheng chuan
   */
  boolean areExpectedImageTemplateLearningRecordsValid()
  {
    if (isProjectMajorStateValid() == false)
      return false;
    
    if (_classToExpectedImageTemplateLearningInvalidRecordsInFileEventToOrigValueMap.isEmpty() == false)
      return false;

    return true;
  }
  
  /**
   * Clear Tombstone
   * @author Sheng-chuan.yong 
   */
  boolean isExpectedImageTemplateLearningFileValid()
  {
    if (_classToExpectedImageTemplateLearningInvalidFileEventToOrigValueMap.isEmpty() == false)
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  boolean isSubtypeLearningFileValid()
  {
    if (_classToSubtypeLearningInvalidFileEventToOrigValueMap.isEmpty() == false)
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  void displayStateEvents()
  {
//    System.out.println("\n####################### all state events #################");
//    System.out.println("\nTest Program State change events");
//    displayEventsInMap(_classToTestProgramEventToOrigValueMap);
//    System.out.println("\nTest Program State change events");
//    displayEventsInMap(_classToTestProgramAlignmentRegionsEventToOrigValueMap);
//    System.out.println("\nProject major State change events");
//    displayEventsInMap(_classToMajorEventToOrigValueMap);
//    System.out.println("\nProject minor State change events");
//    displayEventsInMap(_classToMinorEventToOrigValueMap);
//    System.out.println("\nVerification Images State change events");
//    displayEventsInMap(_classToVerificationImagesEventToOrigValueMap);
//    System.out.println("\nManual Alignment Transform State change events");
//    displayEventsInMap(_classToManualAlignmentTransformEventToOrigValueMap);
//    System.out.println("\nShorts Learning State change events");
//    displayEventsInMap(_classToShortLearningEventToOrigValueMap);
  }
  
   /**
   * @author Jack Hwee
   * XCR1677 - When fine tuning subype, need to regenerate test program everytime slice setup is changed.
   */
  public Collection<Subtype> getInvalidSubtypes()
  {
    Assert.expect(_classToTunedSliceHeightInvalidRecordsInFileEventToOrigValueMap != null);

    Collection<Subtype> subtypes = new HashSet<Subtype>();
    for (Object object : _classToTunedSliceHeightInvalidRecordsInFileEventToOrigValueMap.keySet())
    {
      if (object instanceof Subtype)
      {
        Subtype subtype = (Subtype)object;
        subtypes.add(subtype);
      }
      else if (object instanceof ComponentType)
      {
        ComponentType componentType = (ComponentType)object;
        subtypes.addAll(componentType.getSubtypes());
      }
      else if (object instanceof CompPackage)
      {
        CompPackage compPackage = (CompPackage)object;
        subtypes.addAll(compPackage.getSubtypes());
      }
    }

    return subtypes;
  }
}
