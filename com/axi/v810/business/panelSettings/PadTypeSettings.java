package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.ProjectHistoryLog;

/**
 * The class contains all the 5dx settings that are used for a particular joint.
 * There is one JointSettings object for each joint on the panel.
 *
 * @author Bill Darbie
 */
public class PadTypeSettings implements Serializable
{
  // created by BoardNdfReader, BoardTypeSettingsReader

  private PadType _padType; // BoardNdfReader, BoardTypeSettingsReader
 // private BooleanRef _testable; // BoardNdfReader, BoardTypeSettingsReader
  private BooleanRef _inspected; // BoardNdfReader, BoardTypeSettingsReader
  // These control how the image of the joint is inspected
  private Subtype _subtype; // calculated, BoardTypeSettingsReader
  private RangeUtil _candidateStepRanges;
  private RangeUtil _previousCandidateStepRanges;
  private static transient ProjectObservable _projectObservable;
//  private String _reasonOfUntestable;
//  private List<String> _reasonOfUntestableList = new ArrayList<String>();
  private boolean _isInterferenceCompensatable = false;
  private GlobalSurfaceModelEnum _globalSurfaceModel = GlobalSurfaceModelEnum.AUTOFOCUS;
  private static transient ProjectHistoryLog _fileWriter;
  
  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
    _fileWriter = ProjectHistoryLog.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public PadTypeSettings()
  {
    _projectObservable.stateChanged(this, null, this, PadTypeSettingsEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.stateChanged(this, this, null, PadTypeSettingsEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void setPadType(PadType padType)
  {
    Assert.expect(padType != null);

    if (padType == _padType)
    {
      return;
    }

    PadType oldValue = _padType;
    _projectObservable.setEnabled(false);
    try
    {
      _padType = padType;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, padType, PadTypeSettingsEventEnum.PAD_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public PadType getPadType()
  {
    Assert.expect(_padType != null);

    return _padType;
  }

//  /**
//   * @author Bill Darbie
//   */
//  public void setTestable(boolean testable)
//  {
//    if ((_testable != null) && (testable == _testable.getValue()))
//    {
//      return;
//    }
//
//    BooleanRef oldValue = null;
//    if (_testable != null)
//    {
//      oldValue = new BooleanRef(_testable);
//    }
//    _projectObservable.setEnabled(false);
//    try
//    {
//      if (_testable == null)
//      {
//        _testable = new BooleanRef(testable);
//      }
//      else
//      {
//        _testable.setValue(testable);
//      }
//    }
//    finally
//    {
//      _projectObservable.setEnabled(true);
//    }
//    _projectObservable.stateChanged(this, oldValue, testable, PadTypeSettingsEventEnum.TESTABLE);
//  }
//
//  public void setUntestableReason(String reason)
//  {
//    _reasonOfUntestable = reason;
//  }
//
//  public String getUntestableReason()
//  {
//    return _reasonOfUntestable;
//  }
//
//  public void AddUntestableReasonToList(String reason)
//  {
//    if (_reasonOfUntestableList.contains(reason) == false)
//    {
//      _reasonOfUntestableList.add(reason);
//    }
//  }
//
//  public List<String> getUntestableReasonList()
//  {
//    return _reasonOfUntestableList;
//  }
//
//  /**
//   * @author Bill Darbie
//   */
//  public boolean isTestable()
//  {
//    Assert.expect(_testable != null);
//    return _testable.getValue();
//  }

  /**
   * @author Bill Darbie
   */
  public void setInspected(boolean inspected)
  {
    if ((_inspected != null) && (inspected == _inspected.getValue()))
    {
      return;
    }

    BooleanRef oldValue = null;
    if (_inspected != null)
    {
      oldValue = new BooleanRef(_inspected);
    }
    _projectObservable.setEnabled(false);
    try
    {
      if (_inspected == null)
      {
        _inspected = new BooleanRef(inspected);
      }
      else
      {
        _inspected.setValue(inspected);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, inspected, PadTypeSettingsEventEnum.INSPECTED);
  }

  /**
   * @return true if this PadType is set to be inspected AND is is loaded and testable.
   * @author Bill Darbie
   */
  public boolean isInspected()
  {
    Assert.expect(_inspected != null);
    Assert.expect(_padType != null);
    //Assert.expect(_testable != null);
    
    if ((_padType.getComponentType().isLoaded() && _inspected.getValue() == true && (_padType.getPadTypeSettings().areAllPadsNotTestable() == false)))
    //if ((_padType.getComponentType().isLoaded() && (_testable.getValue() == true) && _inspected.getValue() == true))
    {
      return true;
    }

    return false;
  }

  /**
   * @return true if this PadType is set to be inspected, EVEN if it is not loaded or not testable
   * @author Bill Darbie
   */
  public boolean isInspectedFlagSet()
  {
    Assert.expect(_inspected != null);

    return _inspected.getValue();
  }

  /**
   * @author Bill Darbie
   */
  public void setSubtype(Subtype subtype)
  {
    Assert.expect(subtype != null);

    if (subtype == _subtype)
    {
      return;
    }

    Subtype oldValue = _subtype;
    _projectObservable.setEnabled(false);
    try
    {
      Assert.expect(_padType != null);
      // tell the old _subtype that it is no longer assigned to this PadTypeSettings padType
      if (_subtype != null)
      {
        _subtype.removeFromPadType(_padType);
      }
      _subtype = subtype;
      // tell the new _subtype that it is assigned to this PadTypeSettings padType
      _subtype.assignedToPadType(_padType);
      _padType.getComponentType().getComponentTypeSettings().invalidateUsesOneSubtype();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, subtype, PadTypeSettingsEventEnum.SUBTYPE);
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasSubtype()
  {
    if (_subtype == null)
    {
      return false;
    }

    return true;
  }

  /**
   * @author Bill Darbie
   */
  public Subtype getSubtype()
  {
    Assert.expect(_subtype != null);

    // check that the subtype matches the JointTypeEnum Setting - they
    // both should be referring to the same InspectionFamily or there was a bug
    // in the code somewhere

    JointTypeEnum jointTypeEnum = getPadType().getPackagePin().getJointTypeEnum();
    InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

    // (PWL) Since this is a method that may end up getting called frequently, we don't want to do the work to construct
    // this assert message unless it's going to fail.
    boolean assertCondition = inspectionFamily == _subtype.getInspectionFamily();
    if (assertCondition == false)
    {
      if (UnitTest.unitTesting())
      {
        Assert.expect(assertCondition);
      }
      else
      {
        Assert.expect(assertCondition,
          "The JointTypeEnum inspection family does not match the Subtype inspection family for Padtype \n"
          + getPadType().getBoardAndComponentAndPadName()
          + "\nPadType inspection family name: " + inspectionFamily.getName()
          + "\nsubtype inspection family name: " + _subtype.getInspectionFamily().getName());
      }
    }

    return _subtype;
  }
  
  /**
   * @author Kee Chin Seong - Too many access from other class for getSubtype
   *                        - This is special handle for the Subtype where DIFFERENT JOINT/INSPECTION FAMILY when
   *                          importing the Library.
  */
  public boolean doesSubtypeHasIncompatibleInspectionFamily()
  {
    Assert.expect(_subtype != null);

    // check that the subtype matches the JointTypeEnum Setting - they
    // both should be referring to the same InspectionFamily or there was a bug
    // in the code somewhere

    JointTypeEnum jointTypeEnum = getPadType().getPackagePin().getJointTypeEnum();
    InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

    // XCR-3112 Assert when edit land pattern and edit subtype package during import
    return (inspectionFamily.equals(_subtype.getInspectionFamily()) == false);
  }


  /**
   * @author George A. David
   */
  public void setSignalCompensation(SignalCompensationEnum signalCompensation)
  {
    Assert.expect(signalCompensation != null);
    
    _subtype.getSubtypeAdvanceSettings().setSignalCompensation(signalCompensation);
  }

  /**
   * @author George A. David
   */
  public SignalCompensationEnum getSignalCompensation()
  {
    return _subtype.getSubtypeAdvanceSettings().getSignalCompensation();
  }

  /**
   * @author George A. David
   */
  public void setCandidateStepRanges(RangeUtil candidateStepRanges)
  {
    Assert.expect(candidateStepRanges != null);

    _previousCandidateStepRanges = _candidateStepRanges;
    _candidateStepRanges = new RangeUtil(candidateStepRanges);
  }

  /**
   * @author George A. David
   */
  public RangeUtil getCandidateStepRanges()
  {
    Assert.expect(_candidateStepRanges != null);

    return new RangeUtil(_candidateStepRanges);
  }

  /**
   * @author Cheah, Lee Herng
   */
  public RangeUtil getPreviousCandidateStepRanges()
  {
    Assert.expect(_previousCandidateStepRanges != null);

    return new RangeUtil(_previousCandidateStepRanges);
  }

  /**
   * @author George A. David
   */
  public boolean hasCandidateStepRanges()
  {
    return _candidateStepRanges != null;
  }

  /**
   * @author Cheah, Lee Herng
   */
  public boolean hasPreviousCandidateStepRanges()
  {
    return (_previousCandidateStepRanges != null);
  }

  /**
   *
   * @author George A. David
   */
  public void setArtifactCompensationState(ArtifactCompensationStateEnum artifactCompensationState)
  {
    Assert.expect(artifactCompensationState != null);
    Assert.expect(artifactCompensationState.equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false);

    _subtype.getSubtypeAdvanceSettings().setArtifactCompensationState(artifactCompensationState);

  }

  /**
   * It will check whether it is Interference Compensatable or not and
   * return the Effective Artifact Compensation State
   *
   * @author George A. David
   * @author Wei Chin, Chong
   */
  public ArtifactCompensationStateEnum getEffectiveArtifactCompensationState()
  {
    return _subtype.getSubtypeAdvanceSettings().getArtifactCompensationState();
  }

  /**
   * @author George A. David
   */
  public boolean isInterferenceCompensated()
  {
    return getEffectiveArtifactCompensationState().equals(ArtifactCompensationStateEnum.COMPENSATED);
  }

  /**
   * Special Case for get the ArtifactCompensationState without checking the Interference Compensatable State
   * normally use in writer
   *
   * @author Wei Chin, Chong
   */
  public ArtifactCompensationStateEnum getArtifactCompensationStateUnconditionally()
  {
    Assert.expect(_subtype != null);
    
    return _subtype.getSubtypeAdvanceSettings().getArtifactCompensationState();
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isInterferenceCompensatable()
  {
    return _isInterferenceCompensatable;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void setIsInterferenceCompensatable(boolean isInterferenceCompensatable)
  {
    _subtype.getSubtypeAdvanceSettings().setIsInterferenceCompensatable(isInterferenceCompensatable);
  }

  /**
   * @author Cheah, Lee Herng
   */
  public void setGlobalSurfaceModel(GlobalSurfaceModelEnum globalSurfaceModel)
  {
      Assert.expect(globalSurfaceModel != null);

      if(_globalSurfaceModel != null && _globalSurfaceModel.equals(globalSurfaceModel))
        return;

      GlobalSurfaceModelEnum oldValue = _globalSurfaceModel;
      _projectObservable.setEnabled(false);

      _globalSurfaceModel = globalSurfaceModel;

      _projectObservable.setEnabled(true);
      _projectObservable.stateChanged(this, oldValue, globalSurfaceModel, PadTypeSettingsEventEnum.GLOBAL_SURFACE_MODEL_STATE_ENUM);
      _fileWriter.appendSubtype(_padType.getComponentType().getReferenceDesignator(),_padType,oldValue, globalSurfaceModel);
  }

  /**
   * @author Cheah, Lee Herng
   */
  public GlobalSurfaceModelEnum getGlobalSurfaceModel()
  {
    Assert.expect(_globalSurfaceModel != null);

    return _globalSurfaceModel;
  }

  /**
   * @author sham
   */
  public void setUserGain(UserGainEnum userGain)
  {
    Assert.expect(userGain != null);

    _subtype.getSubtypeAdvanceSettings().setUserGain(userGain);
  }

  /**
   * @author sham
   */
  public UserGainEnum getUserGain()
  {
    return _subtype.getSubtypeAdvanceSettings().getUserGain();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public MagnificationTypeEnum getMagnificationType()
  {
    return _subtype.getSubtypeAdvanceSettings().getMagnificationType();
  }
  
    /**
   * @return true if all Pads of this PadType are NOT inspected.
   * @author Jack Hwee
   */
  public boolean areAllPadsNotTestable()
  {
    Assert.expect(_padType != null);
    boolean allNotTestable = true;

    for (Pad pad : _padType.getPads())
    {
      if (pad.getPadSettings().isTestable())
      {
        allNotTestable = false;
        break;
      }
    }
    return allNotTestable;
  }
  
  /**
   * @author Jack Hwee
   */
  public boolean isTestable()
  {
    for (Pad pad : _padType.getPads())
    {
      if (pad.isTestable() == false)
      {
        return false;
      }
    }
    return true;
  }
  
   /**
   * @author Jack Hwee
   */
  public String getFirstUntestableReason()
  {
    String firstUntestableReason = "";
    
    for (Pad pad : _padType.getPads())
    {
      if (pad.isTestable() == false)
      {
        firstUntestableReason = pad.getPadSettings().getUntestableReason();
        return firstUntestableReason;
      }
    }
    return firstUntestableReason;
  }
}
