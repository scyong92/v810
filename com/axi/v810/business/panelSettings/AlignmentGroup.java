package com.axi.v810.business.panelSettings;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * An AlignmentGroup is a set of pads and/or fiducials that provide a pattern that is easy to find for the
 * purposes of alignment.  There are three alignment groups per panel.  For a long panel, there
 * will be two sets of three alignment groups.
 *
 * @author Bill Darbie
 */
public class AlignmentGroup implements Serializable
{
  // created by BoardNdfReader, PanelSettingsReader
  private static final int _MAX_PADS_AND_FIDUCIALS = 6;  // the upper limit for the total of both pads/fiducials
  private String _name; // BoardNdfReader, PanelSettingsReader
  private List<Pad> _pads = new ArrayList<Pad>(); // BoardNdfReader, PanelSettingsReader
  private List<Fiducial> _fiducials = new ArrayList<Fiducial>(); // PanelSettingsReader
  private double _matchQuality = -2d;
  private double _foregroundGrayLevel = -1d;
  private double _backgroundGrayLevel = -1d;
  private double _foregroundGrayLevelInHighMag = -1d;
  private double _backgroundGrayLevelInHighMag = -1d;
  
  private PanelSettings _panelSettings;
  private BoardSettings _boardSettings;
  
  private transient PanelCoordinate _locatedCoordinateOfCentroidInNanometers;  // centroid of this alignment group, as found by manual or automatic alignment
  private transient boolean _hasDarkPadsWithLightBackground = true; // for now this is a dynamic setting that should NOT be persisted with the Project.
  private static transient ProjectObservable _projectObservable;
  
  private transient PanelCoordinate _locatedCoordinateOfCentroidInNanometersForHighMag;  // centroid of this alignment group, as found by manual or automatic alignment  
  
  private boolean _useZHeight = false;
  private int _zHeightInNanometer = 0;
  
  private boolean _useCustomizeAlignmentSetting = false;
  private SignalCompensationEnum _signalCompensationEnum = SignalCompensationEnum.DEFAULT_LOW;
  private StageSpeedEnum _stageSpeedEnum = StageSpeedEnum.ONE;
  private UserGainEnum _userGainEnum = UserGainEnum.ONE;
  private DynamicRangeOptimizationLevelEnum _dynamicRangeOptimizationLevelEnum = DynamicRangeOptimizationLevelEnum.ONE;

  //Khaw Chek Hau - XCR2285: 2D Alignment on v810
  private transient int _draggedActualMinX = 0;
  private transient int _draggedActualMinY = 0;
  private transient int _draggedExpectedMinX = 0;
  private transient int _draggedExpectedMinY = 0;
  private transient int _draggedWidth = 0;
  private transient int _draggedHeight = 0;
  private transient int _draggedRegionRecWidth = 0;
  private transient int _draggedRegionRecHeight = 0;
  private transient int _currentManualAlignmentXOffsetRelativeToPanelInNanoMeters = 0;
  private transient int _currentManualAlignmentYOffsetRelativeToPanelInNanoMeters = 0;
  private transient String _draggedImageFileName; 
  private transient String _draggedImageFileNameForHighMag; 
  private transient PanelCoordinate _expectedDraggedImageCoordinateInNanometers;
  private transient boolean _hasDraggedRegion = false;
  
  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Andy Mechtenberg
   */
  public static int getMaximumPadsAndFiducials()
  {
    return _MAX_PADS_AND_FIDUCIALS;
  }

  /**
   * @author Bill Darbie
   */
  public AlignmentGroup(PanelSettings panelSettings)
  {
    Assert.expect(panelSettings != null);
    _panelSettings = panelSettings;
    _projectObservable.stateChanged(this, null, this, AlignmentGroupEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Wei Chin, Chong
   */
  public AlignmentGroup(BoardSettings boardSettings)
  {
    Assert.expect(boardSettings != null);
    _boardSettings = boardSettings;
    _projectObservable.stateChanged(this, null, this, AlignmentGroupEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.stateChanged(this, this, null, AlignmentGroupEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Keith Lee
   */
  public void setName(String name)
  {
    Assert.expect(name != null);

    if (name.equals(_name))
      return;

    String oldValue = _name;
    _projectObservable.setEnabled(false);
    try
    {
      _name = name.intern();
      if(_panelSettings != null)
        _panelSettings.checkForDuplicateAlignmentGroupNames();
      if(_boardSettings != null)
        _boardSettings.checkForDuplicateAlignmentGroupNames();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, name, AlignmentGroupEventEnum.NAME);
  }

  /**
   * @author Keith Lee
   */
  public String getName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author Bill Darbie
   */
  boolean hasName()
  {
    if (_name == null)
      return false;
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public void addPad(Pad pad)
  {
    Assert.expect(pad != null);

    _projectObservable.setEnabled(false);
    try
    {
      if(_pads.isEmpty() == false ||  _fiducials.isEmpty() == false)
        Assert.expect(pad.isTopSide() == isTopSide());

        _pads.add(pad);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, pad, AlignmentGroupEventEnum.ADD_OR_REMOVE_PAD);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void removePad(Pad pad)
  {
    Assert.expect(pad != null);
    Assert.expect(_pads.contains(pad));
    _projectObservable.setEnabled(false);
    try
    {
      _pads.remove(pad);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, pad, null, AlignmentGroupEventEnum.ADD_OR_REMOVE_PAD);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void clearPads()
  {
    Assert.expect(_pads != null);
    _projectObservable.setEnabled(false);
    try
    {
      _pads.clear();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, AlignmentGroupEventEnum.ADD_OR_REMOVE_PAD);
  }

  /**
   * @author Bill Darbie
   */
  public void addFiducial(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);

    _projectObservable.setEnabled(false);
    try
    {
      if (_pads.isEmpty() == false || _fiducials.isEmpty() == false)
        Assert.expect(fiducial.isTopSide() == isTopSide());
      _fiducials.add(fiducial);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, fiducial, AlignmentGroupEventEnum.ADD_OR_REMOVE_FIDUCIAL);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void removeFiducial(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);
    Assert.expect(_fiducials.contains(fiducial));
    _projectObservable.setEnabled(false);
    try
    {
      _fiducials.remove(fiducial);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, fiducial, null, AlignmentGroupEventEnum.ADD_OR_REMOVE_FIDUCIAL);
  }

  /**
   * @author Andy Mechtenberg
   */
  public Project getProject()
  {
    Assert.expect(_panelSettings != null || _boardSettings != null);

    if(_panelSettings != null)
      return _panelSettings.getPanel().getProject();

    return _boardSettings.getBoard().getPanel().getProject();
  }

  /**
   * @author Keith Lee
   */
  public List<Pad> getPads()
  {
    Assert.expect(_pads != null);

    return _pads;
  }

  /**
   * @author Keith Lee
   */
  public List<Fiducial> getFiducials()
  {
    Assert.expect(_fiducials != null);

    return _fiducials;
  }

  /**
   * @author George A. David
   */
  public PanelRectangle getBoundsRelativeToPanelInNanoMeters()
  {
    PanelRectangle bounds = null;

    for(Pad pad : _pads)
    {
      if(bounds == null)
        bounds = new PanelRectangle(pad.getShapeRelativeToPanelInNanoMeters());
      else
        bounds.addShape(pad.getShapeRelativeToPanelInNanoMeters());
    }

    for(Fiducial fiducial : _fiducials)
    {
      if(bounds == null)
        bounds = new PanelRectangle(fiducial.getShapeRelativeToPanelInNanoMeters());
      else
        bounds.addShape(fiducial.getShapeRelativeToPanelInNanoMeters());
    }

    Assert.expect(bounds != null);
    return bounds;
  }

  /**
   * Returns the centroid of all the pads and fiducials in this AlignmentGroup
   * in terms of a PanelCoordinate.
   *
   * @author Peter Esbensen
   */
  public PanelCoordinate getExpectedCentroidInNanometers()
  {
    PanelCoordinate coordinateSum = new PanelCoordinate(0,0);
    int numberOfPadsAndFiducials = 0;

    List<Pad> pads = getPads();
    numberOfPadsAndFiducials += pads.size();

    for (Pad pad : pads)
    {
      coordinateSum = coordinateSum.plus(pad.getCenterCoordinateRelativeToPanelInNanoMeters());
    }

    List<Fiducial> fiducials = getFiducials();
    numberOfPadsAndFiducials += fiducials.size();

    for (Fiducial fiducial : fiducials)
    {
      coordinateSum = coordinateSum.plus(fiducial.getPanelCoordinateInNanoMeters());
    }

    Assert.expect(numberOfPadsAndFiducials > 0);
    return new PanelCoordinate(coordinateSum.scale( 1.0 / numberOfPadsAndFiducials ));
  }

  /**
   * Set the located PanelCoordinate of this AlignmentGroup, as determined by
   * manual or automatic alignment.
   *
   * @author Peter Esbensen
   */
  public void setLocatedCentroidInNanometers(PanelCoordinate coordinate)
  {
    Assert.expect(coordinate != null);

    // NOTE: This does NOT fire a state changed event since this data is not persisted with the Project
    // or TestProgram.
    _locatedCoordinateOfCentroidInNanometers = coordinate;
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public PanelCoordinate getLocatedCentroidInNanometers()
  {
    Assert.expect(_locatedCoordinateOfCentroidInNanometers != null);

    return _locatedCoordinateOfCentroidInNanometers;
  }
  
  /**
   * Set the located PanelCoordinate of this AlignmentGroup, as determined by
   * manual or automatic alignment.
   * 
   * @author Wei Chin
   */
  public void setLocatedCentroidInNanometersForHighMag(PanelCoordinate coordinate)
  {
    Assert.expect(coordinate != null);

    // NOTE: This does NOT fire a state changed event since this data is not persisted with the Project
    // or TestProgram.
    _locatedCoordinateOfCentroidInNanometersForHighMag = coordinate;
  }

  /**
   * @author Wei Chin
   */
  public PanelCoordinate getLocatedCentroidInNanometersForHighMag()
  {
    Assert.expect(_locatedCoordinateOfCentroidInNanometersForHighMag != null);

    return _locatedCoordinateOfCentroidInNanometersForHighMag;
  }

  /**
   * @author Peter Esbensen
   * @author Wei Chin
   */
  public void clearLocatedCentroid()
  {
    // NOTE: This does NOT fire a state changed event since this data is not persisted with the Project
    // or TestProgram.
    _locatedCoordinateOfCentroidInNanometers = null;
    _locatedCoordinateOfCentroidInNanometersForHighMag = null;
  }

  /**
   * @author Peter Esbensen
   */
  public boolean locatedCentroidExists()
  {
    return (_locatedCoordinateOfCentroidInNanometers != null);
  }
  
  /**
   * @author Wei Chin
   */
  public boolean locatedCentroidForHighMagExists()
  {
    return (_locatedCoordinateOfCentroidInNanometersForHighMag != null);
  }

  /**
   * Gets the match quality value for this AlignmentGroup.
   *
   * @author Matt Wharton
   */
  public double getMatchQuality()
  {
    Assert.expect(_matchQuality != -2d);

    return _matchQuality;
  }

  /**
   * Sets the match quality value for this AlignmentGroup.
   *
   * @author Matt Wharton
   */
  public void setMatchQuality(double matchQuality)
  {
    final double MATCH_QUALITY_EQUALITY_TOLERANCE = 0.001;
    Assert.expect(MathUtil.fuzzyGreaterThanOrEquals(matchQuality, -1d, MATCH_QUALITY_EQUALITY_TOLERANCE) &&
                  MathUtil.fuzzyLessThanOrEquals(matchQuality, 1d, MATCH_QUALITY_EQUALITY_TOLERANCE),
                  "Match quality: " + matchQuality);

    double oldValue = _matchQuality;
    _projectObservable.setEnabled(false);
    try
    {
      _matchQuality = matchQuality;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, matchQuality, AlignmentGroupEventEnum.MATCH_QUALITY);
  }

  /**
   * added by sheng chuan to check the alignment image quality before set.
   * @param matchQuality
   * @return 
   * @author sheng chuan
   */
  public boolean isMatchQualityAcceptable(double matchQuality)
  {
    final double MATCH_QUALITY_EQUALITY_TOLERANCE = 0.001;
    return MathUtil.fuzzyGreaterThanOrEquals(matchQuality, -1d, MATCH_QUALITY_EQUALITY_TOLERANCE) &&
                  MathUtil.fuzzyLessThanOrEquals(matchQuality, 1d, MATCH_QUALITY_EQUALITY_TOLERANCE);
  }
  
  /**
   * Returns true if match quality has been set for this AlignmentGroup, false otherwise.
   *
   * @author Matt Wharton
   */
  public boolean hasMatchQuality()
  {
    return (_matchQuality != -2d);
  }

  /**
   * @author Matt Wharton
   */
  public double getForegroundGrayLevel()
  {
    Assert.expect(_foregroundGrayLevel != -1d);

    return _foregroundGrayLevel;
  }

  /**
   * @author Matt Wharton
   */
  public void setForegroundGrayLevel(double foregroundGrayLevel)
  {
    Assert.expect(foregroundGrayLevel >= 0d);
    Assert.expect(foregroundGrayLevel < 256d);

    double oldValue = _foregroundGrayLevel;
    _projectObservable.setEnabled(false);
    try
    {
      _foregroundGrayLevel = foregroundGrayLevel;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, foregroundGrayLevel, AlignmentGroupEventEnum.FOREGROUND_GRAY_LEVEL);
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasForegroundGrayLevel()
  {
    return (_foregroundGrayLevel != -1d);
  }

  /**
   * @author Matt Wharton
   */
  public double getBackgroundGrayLevel()
  {
    Assert.expect(_backgroundGrayLevel != -1d);

    return _backgroundGrayLevel;
  }

  /**
   * @author Matt Wharton
   */
  public void setBackgroundGrayLevel(double backgroundGrayLevel)
  {
    Assert.expect(backgroundGrayLevel >= 0d);
    Assert.expect(backgroundGrayLevel < 256d);

    double oldValue = _backgroundGrayLevel;
    _projectObservable.setEnabled(false);
    try
    {
      _backgroundGrayLevel = backgroundGrayLevel;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, backgroundGrayLevel, AlignmentGroupEventEnum.BACKGROUND_GRAY_LEVEL);
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasBackgroundGrayLevel()
  {
    return (_backgroundGrayLevel != -1d);
  }

  /**
   * @author Wei Chin
   */
  public double getForegroundGrayLevelInHighMag()
  {
    if (_foregroundGrayLevelInHighMag == -1d)
      _foregroundGrayLevelInHighMag = _foregroundGrayLevel;

    Assert.expect(_foregroundGrayLevelInHighMag != -1d);

    return _foregroundGrayLevelInHighMag;
  }

  /**
   * @author Wei Chin
   */
  public void setForegroundGrayLevelInHighMag(double foregroundGrayLevel)
  {
    Assert.expect(foregroundGrayLevel >= 0d);
    Assert.expect(foregroundGrayLevel < 256d);

    double oldValue = _foregroundGrayLevelInHighMag;
    _projectObservable.setEnabled(false);
    try
    {
      _foregroundGrayLevelInHighMag = foregroundGrayLevel;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, foregroundGrayLevel, AlignmentGroupEventEnum.FOREGROUND_GRAY_LEVEL);
  }

  /**
   * @author Wei Chin
   */
  public boolean hasForegroundGrayLevelInHighMag()
  {
    return (_foregroundGrayLevelInHighMag != -1d);
  }

  /**
   * @author Wei Chin
   */
  public double getBackgroundGrayLevelInHighMag()
  {
    if (_backgroundGrayLevelInHighMag == -1d)
      _backgroundGrayLevelInHighMag = _backgroundGrayLevel;

    Assert.expect(_backgroundGrayLevelInHighMag != -1d);

    return _backgroundGrayLevelInHighMag;
  }

  /**
   * @author Wei Chin
   */
  public void setBackgroundGrayLevelInHighMag(double backgroundGrayLevel)
  {
    Assert.expect(backgroundGrayLevel >= 0d);
    Assert.expect(backgroundGrayLevel < 256d);

    double oldValue = _backgroundGrayLevelInHighMag;
    _projectObservable.setEnabled(false);
    try
    {
      _backgroundGrayLevelInHighMag = backgroundGrayLevel;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, backgroundGrayLevel, AlignmentGroupEventEnum.BACKGROUND_GRAY_LEVEL);
  }

  /**
   * @author Wei Chin
   */
  public boolean hasBackgroundGrayLevelInHighMag()
  {
    return (_backgroundGrayLevelInHighMag != -1d);
  }  
  
  /**
   * @return true if the alignment group consists of dark pads on a lighter background; false if the alignment
   * group consists of light pads on a darker background.
   *
   * @author Matt Wharton
   */
  public boolean hasDarkPadsWithLightBackground()
  {
    return _hasDarkPadsWithLightBackground;
  }

  /**
   * @author Matt Wharton
   */
  public void setDarkPadsWithLightBackground(boolean hasDarkPadsWithLightBackground)
  {
    // NOTE: This does NOT fire a state changed event since this data is not persisted with the Project
    // or TestProgram.
    _hasDarkPadsWithLightBackground = hasDarkPadsWithLightBackground;
  }

  /**
   * @author George A. David
   */
  public boolean contains(Pad pad)
  {
    Assert.expect(pad != null);
    return _pads.contains(pad);
  }

  /**
   * @author George A. David
   */
  public boolean contains(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);

    return _fiducials.contains(fiducial);
  }

  /**
   * @author George A. David
   */
  public boolean isTopSide()
  {
    boolean isTopSide = false;

    if(_pads.isEmpty() == false)
      isTopSide = _pads.get(0).isTopSide();
    else if(_fiducials.isEmpty()== false)
      isTopSide = _fiducials.get(0).isTopSide();
    else
      Assert.expect(false);

    return isTopSide;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isEmpty()
  {
    if (_pads.isEmpty() && _fiducials.isEmpty())
      return true;
    else
      return false;
  }

  /**
   * @author Andy Mechtenberg
   * @author Tan Hock Zoon
   */
  public boolean isAlignmentGroupJointTypeInconsistent()
  {
    for(Pad pad : getPads())
    {
      boolean specialTwoPinDevice = ImageAnalysis.isSpecialTwoPinComponent(pad.getJointTypeEnum());
      if (specialTwoPinDevice)
      {
        // look for it's pair -- return true if you can't find it.
        java.util.List<Pad> allPads = pad.getComponent().getPads();
        for (Pad requiredPad : allPads)
        {
          if (getPads().contains(requiredPad) == false)
            return true;
        }
      }
    }
    return false;
  }

  /**
  *
  * @return
  */
  public boolean isUseShapeMatching()
  {
    for (Pad pad : _pads)
    {
      if(pad.getJointTypeEnum().equals(JointTypeEnum.PRESSFIT)
//              || pad.getJointTypeEnum().equals(JointTypeEnum.CHIP_SCALE_PACKAGE)
//              || pad.getJointTypeEnum().equals(JointTypeEnum.COLLAPSABLE_BGA)
//              || pad.getJointTypeEnum().equals(JointTypeEnum.NON_COLLAPSABLE_BGA)
//              || pad.getJointTypeEnum().equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR)
//              || pad.getJointTypeEnum().equals(JointTypeEnum.CGA)
//              || pad.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE)
        )
      {
        return true;
      }
    }

    return false;
  }

  /**
   * @return the _useZHeight
   * @author Wei Chin
   */
  public boolean useZHeight()
  {
    return _useZHeight;
  }

  /**
   * @param useZHeight the _useZHeight to set
   * @author Wei Chin
   */
  public void setUseZHeight(boolean useZHeight)
  {
    boolean oldValue = _useZHeight;
    if(oldValue == useZHeight)
      return;
    
    _projectObservable.setEnabled(false);
    try
    {
      _useZHeight = useZHeight;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, useZHeight, AlignmentGroupEventEnum.USE_ZHEIGHT);
  }

  /**
   * @return the _zHeightInNanometer
   * @author Wei Chin
   */
  public int getZHeightInNanometer()
  {
    return _zHeightInNanometer;
  }

  /**
   * @param zHeightInNanometer the _zHeightInNanometer to set
   * @author Wei Chin
   */
  public void setZHeightInNanometer(int zHeightInNanometer)
  {
    int oldValue = _zHeightInNanometer;
    if(oldValue == zHeightInNanometer)
      return;
    
    _projectObservable.setEnabled(false);
    try
    {
      _zHeightInNanometer = zHeightInNanometer;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, zHeightInNanometer, AlignmentGroupEventEnum.USE_ZHEIGHT);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public SignalCompensationEnum getSignalCompensationEnum()
  {
    return _signalCompensationEnum;
  }
  
  /**
   * @author Cheah Lee Herng   
   */
  public void setSignalCompensationEnum(SignalCompensationEnum signalCompensationEnum)
  {
    Assert.expect(signalCompensationEnum != null);
    
    SignalCompensationEnum oldSignalCompensationEnum = _signalCompensationEnum;
    if (oldSignalCompensationEnum.equals(signalCompensationEnum))
      return;
    
    _projectObservable.setEnabled(false);
    try
    {
      _signalCompensationEnum = signalCompensationEnum;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldSignalCompensationEnum, signalCompensationEnum, AlignmentGroupEventEnum.SIGNAL_COMPENSATION);
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public StageSpeedEnum getStageSpeedEnum()
  {
    return _stageSpeedEnum;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setStageSpeedEnum(StageSpeedEnum stageSpeedEnum)
  {
    Assert.expect(stageSpeedEnum != null);
    
    StageSpeedEnum oldStageSpeed = _stageSpeedEnum;
    if (oldStageSpeed.equals(stageSpeedEnum))
      return;
    
    _projectObservable.setEnabled(false);
    try
    {
      _stageSpeedEnum = stageSpeedEnum;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldStageSpeed, stageSpeedEnum, AlignmentGroupEventEnum.STAGE_SPEED);
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public DynamicRangeOptimizationLevelEnum getDynamicRangeOptimizationLevel()
  {
    return _dynamicRangeOptimizationLevelEnum;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setDynamicRangeOptimizationLevel(DynamicRangeOptimizationLevelEnum droLevel)
  {
    Assert.expect(droLevel != null);
    
    DynamicRangeOptimizationLevelEnum oldDroLevel = _dynamicRangeOptimizationLevelEnum;
    if (oldDroLevel.equals(droLevel))
      return;
    
    _projectObservable.setEnabled(false);
    try
    {
      _dynamicRangeOptimizationLevelEnum = droLevel;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldDroLevel, droLevel, AlignmentGroupEventEnum.DRO_LEVEL);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public UserGainEnum getUserGainEnum()
  {
    return _userGainEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setUserGainEnum(UserGainEnum userGainEnum)
  {
    Assert.expect(userGainEnum != null);
    
    UserGainEnum oldUserGainEnum = _userGainEnum;
    if (oldUserGainEnum.equals(userGainEnum))
      return;
    
    _projectObservable.setEnabled(false);
    try
    {
      _userGainEnum = userGainEnum;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldUserGainEnum, userGainEnum, AlignmentGroupEventEnum.USER_GAIN);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean hasUserDefinedUserGainEnum()
  {
    if (_userGainEnum == null)
      return false;
    else
      return true;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean useCustomizeAlignmentSetting()
  {
    return _useCustomizeAlignmentSetting;
  }
  
  /**
   * @author Cheah Lee Herng 
   * @author sheng chuan
   */
  public void setUseCustomizeAlignmentSetting(boolean useCustomizeAlignmentSetting)
  {
    boolean oldValue = _useCustomizeAlignmentSetting;
    if(oldValue == useCustomizeAlignmentSetting)
      return;
    
    _projectObservable.setEnabled(false);
    try
    {
      _useCustomizeAlignmentSetting = useCustomizeAlignmentSetting;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, useCustomizeAlignmentSetting, AlignmentGroupEventEnum.USE_CUSTOMIZED_ALIGNMENT);
  }
  
  /**
   * Reset background gray level value to initial value
   * @author Phang Siew Yeng
   */
  public void resetBackgroundGrayLevel()
  {
    _backgroundGrayLevel = -1d;
    _backgroundGrayLevelInHighMag = -1d;
  }
  
  /**
   * Reset foreground gray level value to initial value
   * @author Phang Siew Yeng
   */
  public void resetForegroundGrayLevel()
  {
    _foregroundGrayLevel = -1d;
    _foregroundGrayLevelInHighMag = -1d;
  }
  
  /**
   * Reset match quality to initial value
   * @author Phang Siew Yeng
   */
  public void resetMatchQuality()
  {
    _matchQuality = -2d;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setDraggedActualMinX(int minX)
  {
    _draggedActualMinX = minX;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setDraggedActualMinY(int minY)
  {
    _draggedActualMinY = minY;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setDraggedExpectedMinX(int minX)
  {
    _draggedExpectedMinX = minX;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setDraggedExpectedMinY(int minY)
  {
    _draggedExpectedMinY = minY;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setDraggedWidth(int width)
  {
    _draggedWidth = width;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setDraggedHeight(int height)
  {
    _draggedHeight = height;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public int getDraggedActualMinX()
  {
    return _draggedActualMinX;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public int getDraggedActualMinY()
  {
    return _draggedActualMinY;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public int getDraggedExpectedMinX()
  {
    return _draggedExpectedMinX;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public int getDraggedExpectedMinY()
  {
    return _draggedExpectedMinY;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public int getDraggedWidth()
  {
    return _draggedWidth;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public int getDraggedHeight()
  {
    return _draggedHeight;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setCurrentManualAlignmentXOffsetRelativeToPanelInNanoMeters(int currentManualAlignmentXOffsetRelativeToPanelInNanoMeters)
  {
    _currentManualAlignmentXOffsetRelativeToPanelInNanoMeters = currentManualAlignmentXOffsetRelativeToPanelInNanoMeters;  
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setCurrentManualAlignmentYOffsetRelativeToPanelInNanoMeters(int currentManualAlignmentYOffsetRelativeToPanelInNanoMeters)
  {
    _currentManualAlignmentYOffsetRelativeToPanelInNanoMeters = currentManualAlignmentYOffsetRelativeToPanelInNanoMeters;  
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public int getCurrentManualAlignmentXOffsetRelativeToPanelInNanoMeters()
  {
    return _currentManualAlignmentXOffsetRelativeToPanelInNanoMeters;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public int getCurrentManualAlignmentYOffsetRelativeToPanelInNanoMeters()
  {
    return _currentManualAlignmentYOffsetRelativeToPanelInNanoMeters;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setDraggedImageFileName(String draggedImageFileName)
  {
    Assert.expect(draggedImageFileName != null);
    
    _draggedImageFileName = draggedImageFileName;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public String getDraggedImageFileName()
  {
     return _draggedImageFileName;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setDraggedImageFileNameForHighMag(String draggedImageFileNameForHighMag)
  {
    Assert.expect(draggedImageFileNameForHighMag != null);
    
    _draggedImageFileNameForHighMag = draggedImageFileNameForHighMag;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public String getDraggedImageFileNameForHighMag()
  {
     return _draggedImageFileNameForHighMag;
  }
    
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setExpectedDraggedImageCoordinateInNanometers(PanelCoordinate draggedCoordinate)
  {
    _expectedDraggedImageCoordinateInNanometers = draggedCoordinate;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public PanelCoordinate getExpectedDraggedImageCoordinateInNanometers()
  {    
    return _expectedDraggedImageCoordinateInNanometers;
  }

  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setHasDraggedRegion(boolean hasDraggedRegion)
  {    
    _hasDraggedRegion = hasDraggedRegion;
  }

  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public boolean hasDraggedRegion()
  {    
    return _hasDraggedRegion;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setDraggedRegionRecWidth(int width)
  {
    _draggedRegionRecWidth = width;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setDraggedRegionRecHeight(int height)
  {
    _draggedRegionRecHeight = height;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public int getDraggedRegionRecWidth()
  {
    return _draggedRegionRecWidth;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public int getDraggedRegionRecHeight()
  {
    return _draggedRegionRecHeight;
  }
}
