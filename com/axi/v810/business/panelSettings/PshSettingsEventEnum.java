package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Siew Yeng
 */
public class PshSettingsEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;
  public static PshSettingsEventEnum CREATE_OR_DESTROY = new PshSettingsEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static PshSettingsEventEnum SELECTIVE_BOARD_SIDE_ENABLED = new PshSettingsEventEnum(++_index);
  public static PshSettingsEventEnum SELECTIVE_JOINT_TYPE_ENABLED = new PshSettingsEventEnum(++_index);
  public static PshSettingsEventEnum SELECTIVE_COMPONENT_ENABLED = new PshSettingsEventEnum(++_index);
  public static PshSettingsEventEnum BOARD_SIDE_REFERENCE_ENUM = new PshSettingsEventEnum(++_index);
  public static PshSettingsEventEnum ADD_OR_REMOVE_ENABLED_JOINT_TYPE = new PshSettingsEventEnum(++_index);
  public static PshSettingsEventEnum ADD_OR_REMOVE_PSH_NEIGHBOUR_COMPONENT = new PshSettingsEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  
  /**
   * @author Siew Yeng
   */
  private PshSettingsEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Siew Yeng
   */
  private PshSettingsEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
