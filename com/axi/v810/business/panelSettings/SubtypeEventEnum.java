package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class SubtypeEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static SubtypeEventEnum CREATE_OR_DESTROY = new SubtypeEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static SubtypeEventEnum PANEL = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum SHORT_NAME = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum LONG_NAME = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum INSPECTION_FAMILY_ENUM = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum ADD_OR_REMOVE_ALGORITHM_ENUM = new SubtypeEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static SubtypeEventEnum ALGORITHM_ENABLED = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum USER_COMMENT = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum JOINT_TYPE_ENUM = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum TUNED_VALUE = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum TUNED_SLICE_HEIGHT_VALUE = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum ALGORITHM_SETTING_USER_COMMENT = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum ALGORITHM_SETTINGS_LEARNED = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum JOINT_SPECIFIC_DATA_LEARNED = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum EXPECTED_IMAGE_DATA_LEARNED = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum EXPECTED_IMAGE_TEMPLATE_LEARNED = new SubtypeEventEnum(++_index);//ShengChuan - Clear Tombstone
  public static SubtypeEventEnum ALL_ALGORITHMS_EXCEPT_SHORT_HAVE_QUESTIONS = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum SHORT_LEARNING_HAS_QUESTIONS = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum DELETE_LEARNED_DATA_FOR_ALL_ALGORITHMS_EXCEPT_SHORT = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum DELETE_LEARNED_DATA_FOR_SHORT = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum DELETE_LEARNED_DATA_FOR_EXPECTED_IMAGE = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum DELETE_LEARNED_DATA_FOR_EXPECTED_IMAGE_TEMPLATE = new SubtypeEventEnum(++_index);//ShengChuan - Clear Tombstone
  public static SubtypeEventEnum LEARN = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum SET_LEARNED_SETTINGS_TO_DEFAULTS = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum IS_IMPORTED_FROM_LIBRARY = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum IS_MODIFIED_BY_USER = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum IMPORTED_PACKAGE_LIBRARY_PATH = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum VOID_METHOD = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum TUNED_GRAYLEVEL_MIN_MAX_CHANGE = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum MASK_IMAGE = new SubtypeEventEnum(++_index);
  public static SubtypeEventEnum ADD_OR_REMOVE_SUB_SUBTYPE = new SubtypeEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static SubtypeEventEnum SET_ARTIFACT_BASED_REGION_SEPERATION_DIRECTION = new SubtypeEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private SubtypeEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private SubtypeEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
