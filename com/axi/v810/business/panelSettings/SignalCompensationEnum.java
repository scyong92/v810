package com.axi.v810.business.panelSettings;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * enumarates the different levels of signal compensation.
 * The lower the signal comp, the smalle the scan step sizes
 * This determines the maximum size of scan steps
 * @author George A. David
 * @author Erica Wheatcroft
 */
public class SignalCompensationEnum extends com.axi.util.Enum
{
  private static Map<String, SignalCompensationEnum> _stringToEnumMap = new HashMap<String,SignalCompensationEnum>();
  public static List<SignalCompensationEnum> _orderedSignalCompensations = new ArrayList<SignalCompensationEnum>();
  
  private static List<String> _orderedSignalCompensationNameList = new ArrayList<String>();

  private String _name;
  private static int _index = -1;
  public static Config _config = Config.getInstance();


  // the order of these matters, see getHigherSignalCompensation()
//  public static SignalCompensationEnum DEFAULT_LOW = new SignalCompensationEnum(++_index, "1");
//  public static SignalCompensationEnum MEDIUM = new SignalCompensationEnum(++_index, "2");
//  public static SignalCompensationEnum MEDIUM_HIGH = new SignalCompensationEnum(++_index, "3");
//  public static SignalCompensationEnum HIGH = new SignalCompensationEnum(++_index, "4");
//  public static SignalCompensationEnum SUPER_HIGH = new SignalCompensationEnum(++_index, "8");
  
  // XCR1529, New Scan Route, khang-wah.chnee
  public static SignalCompensationEnum DEFAULT_LOW = new SignalCompensationEnum(++_index, "1");
  public static SignalCompensationEnum IL_1_5 = new SignalCompensationEnum(++_index, "1.5");
  public static SignalCompensationEnum MEDIUM = new SignalCompensationEnum(++_index, "2");
  public static SignalCompensationEnum MEDIUM_HIGH = new SignalCompensationEnum(++_index, "3");
  public static SignalCompensationEnum HIGH = new SignalCompensationEnum(++_index, "4");
  public static SignalCompensationEnum IL_5 = new SignalCompensationEnum(++_index, "5"); 
  public static SignalCompensationEnum IL_6 = new SignalCompensationEnum(++_index, "6"); 
  public static SignalCompensationEnum IL_7 = new SignalCompensationEnum(++_index, "7");
  public static SignalCompensationEnum SUPER_HIGH = new SignalCompensationEnum(++_index, "8");
  
  /**
   * @author George A. David
   * @author Erica Wheatcroft
   */
  private SignalCompensationEnum(int id, String name)
  {
    super(id);
    _name = name;
    _orderedSignalCompensations.add(this);
    _orderedSignalCompensationNameList.add(name);
    _stringToEnumMap.put(name, this);
  }

  /**
   * @author Erica Wheatcroft
   */
  public String toString()
  {
    return _name;
  }

  /**
   * @author Erica Wheatcroft
   */
  static Collection<SignalCompensationEnum> getAllTypes()
  {
    return _stringToEnumMap.values();
  }

  /**
   * @author George A. David
   * @author Erica Wheatcroft
   */
  public static SignalCompensationEnum getHigherSignalCompensation(SignalCompensationEnum firstCompensationEnum,
                                                                   SignalCompensationEnum secondCompensationEnum)
  {
    if(firstCompensationEnum.getId() > secondCompensationEnum.getId())
      return firstCompensationEnum;
    else
      return secondCompensationEnum;
  }

  /**
   * @author George A. David
   */
  public static SignalCompensationEnum getLowerSignalCompensation(SignalCompensationEnum firstCompensationEnum,
                                                                  SignalCompensationEnum secondCompensationEnum)
  {
    if(firstCompensationEnum.getId() < secondCompensationEnum.getId())
      return firstCompensationEnum;
    else
      return secondCompensationEnum;
  }

  // Modified by Seng Yew on 28-Sep-2011
  // This signalCompensation.getMaxStepSizeInNanoMeters() is not used in anywhere.
  // No other places as of the time of this modification, except in SignalCompensation.getStepSizeRange().
  // getStepSizeRange() in SignalCompensationEnum is using hardcoded max scan step size values in systemConfigEnum, which is not supposed to.
  /**
   * @author George A. David
   */
//  public static int getMaxStepSizeInNanoMeters(SignalCompensationEnum signalCompensation)
//  {
//    Assert.expect(signalCompensation != null);
//    int maxStep = -1;
//    if (signalCompensation.equals(DEFAULT_LOW))
//      maxStep = _config.getIntValue(SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_DEFAULT_LOW_SIGNAL_COMPENSATION);
//    else if (signalCompensation.equals(MEDIUM))
//      maxStep = _config.getIntValue(SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_MEDIUM_SIGNAL_COMPENSATION);
//    else if (signalCompensation.equals(MEDIUM_HIGH))
//      maxStep = _config.getIntValue(SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_MEDIUM_HIGH_SIGNAL_COMPENSATION);
//    else if (signalCompensation.equals(HIGH))
//      maxStep = _config.getIntValue(SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_HIGH_SIGNAL_COMPENSATION);
////    else if (signalCompensation.equals(SUPER_HIGH))
////      maxStep = _config.getIntValue(SystemConfigEnum.MAX_STEP_SIZE_IN_NANOMETERS_HIGH_SIGNAL_COMPENSATION) / 2;
//    else
//      Assert.expect(false);
//
//    Assert.expect(maxStep > 0);
//    return maxStep;
//  }

  /**
   * @author George A. David
   */
  public static SignalCompensationEnum getMaxSignalCompensation()
  {
    return _orderedSignalCompensations.get(_orderedSignalCompensations.size() - 1);
  }

  /**
   * @author George A. David
   */
  public static List<SignalCompensationEnum> getOrderedSignalCompensations()
  {
    return new ArrayList<SignalCompensationEnum>(_orderedSignalCompensations);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static SignalCompensationEnum getSignalCompensationEnum(String key)
  {
    Assert.expect(key != null);

    return _stringToEnumMap.get(key);
  }

  // Modified by Seng Yew on 28-Sep-2011
  // This signalCompensation.getStepSizeRange() is only used in ProgramGeneration.java, getValidSignalCompensations(...).
  // No other places as of the time of this modification.
  // getStepSizeRange() in SignalCompensationEnum is using hardcoded max scan step size values in systemConfigEnum, which is not supposed to.
  /**
   * @author George A. David
   */
//  public Pair<Integer, Integer> getStepSizeRange()
//  {
//    SignalCompensationEnum signalCompensation = this;
//    Pair<Integer, Integer> pair = new Pair<Integer, Integer>();
//    if (signalCompensation.equals(SignalCompensationEnum.DEFAULT_LOW))
//    {
//      pair.setFirst(getMaxStepSizeInNanoMeters(SignalCompensationEnum.MEDIUM) + 1);
//      pair.setSecond(getMaxStepSizeInNanoMeters(SignalCompensationEnum.DEFAULT_LOW));
//    }
//    else if (signalCompensation.equals(SignalCompensationEnum.MEDIUM))
//    {
//      pair.setFirst(getMaxStepSizeInNanoMeters(SignalCompensationEnum.MEDIUM_HIGH) + 1);
//      pair.setSecond(getMaxStepSizeInNanoMeters(SignalCompensationEnum.MEDIUM));
//    }
//    else if (signalCompensation.equals(SignalCompensationEnum.MEDIUM_HIGH))
//    {
//      pair.setFirst(getMaxStepSizeInNanoMeters(SignalCompensationEnum.HIGH) + 1);
//      pair.setSecond(getMaxStepSizeInNanoMeters(SignalCompensationEnum.MEDIUM_HIGH));
//    }
//    else if (signalCompensation.equals(SignalCompensationEnum.HIGH))
//    {
//      pair.setFirst(1);
//      pair.setSecond(getMaxStepSizeInNanoMeters(SignalCompensationEnum.HIGH));
//    }
////    else if (signalCompensation.equals(SignalCompensationEnum.SUPER_HIGH))
////    {
////      pair.setFirst(1);
////      pair.setSecond(getMaxStepSizeInNanoMeters(SignalCompensationEnum.HIGH));
////    }
//    else
//      Assert.expect( false);
//
//    return pair;
//  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public static List<String> getOrderSignalCompensationNameList()
  {
    return _orderedSignalCompensationNameList;
  }
}
