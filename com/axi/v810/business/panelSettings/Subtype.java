package com.axi.v810.business.panelSettings;

import java.io.*;
import java.net.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.algorithmLearning.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;

/**
 * A Subtype may be associated with one or more joints that are all tested
 * using the same inspection settings.
 *
 * @author Peter Esbensen
 * @author Bill Darbie
 * @author George Booth
 */
public class Subtype implements Serializable
{
  // created by SubtypeSettingsReader
  private Panel _panel; // SubtypeSettingsReader
  // name starts the same as _originalName but can be changed by the user
  private String _shortName; // SubtypeSettingsReader
  // original name is the name that was automatically assigned to this subtype
  private String _longName; // SubtypeSettingsReader
  private InspectionFamilyEnum _inspectionFamilyEnum; // SubtypeSettingsReader
  private JointTypeEnum _jointTypeEnum; // SubtypeSettingsReader
  private String _userComment; // SubtypeSettingsReader
  private BooleanRef _algorithmSettingsLearned;
  private BooleanRef _jointSpecificDataLearned;
  private BooleanRef _expectedImageDataLearned;
  private BooleanRef _expectedImageTemplateLearned;//ShengChuan - Clear Tombstone
  private Set<PadType> _padTypeSet;
  private boolean _interactiveLearning = false;
  private transient List<Pad> _pads;
  private transient List<Pad> _backupPads;
  private String _importedPackageLibraryPath = "";

  // list of algorithms that will be run (a subset of the total available in InspectionFamily
  private List<AlgorithmEnum> _enabledAlgorithmEnums = new ArrayList<AlgorithmEnum>(); // SubtypeSettingsReader
  // map of algorithmSettingEnum to tuned values
  private Map<AlgorithmSettingEnum, Serializable> _algorithmSettingEnumToTunedValueMap = new HashMap<AlgorithmSettingEnum, Serializable>(); // SubtypeSettingsReader
  // map of algorithmSettingEnum to imported values
  private Map<AlgorithmSettingEnum, Serializable> _algorithmSettingEnumToImportedValueMap = new HashMap<AlgorithmSettingEnum, Serializable>(); // SubtypeSettingsReader
  // map of algorithmSettingEnum to threshold comment
  private Map<AlgorithmSettingEnum, String> _algorithmSettingEnumToUserCommentMap = new HashMap<AlgorithmSettingEnum, String>(); // SubtypeSettingsReader

  private Map<AlgorithmEnum, Integer> _algorithmEnumToAlgorithmVersionMap = new HashMap<AlgorithmEnum, Integer>();

  private transient List<Algorithm> _enabledAlgorithms;
  private transient List<Algorithm> _algorithms;

  private transient AlgorithmSubtypesLearningReaderWriter _algorithmSubtypeLearning;

  private static transient ProjectInitialThresholds _projectInitialThresholds = ProjectInitialThresholds.getInstance();

  private static transient ProjectObservable _projectObservable;

  private SubtypeAdvanceSettings _subtypeAdvanceSettings = null;
  
  private LandPattern _landPattern = null;
  
  //Siew Yeng - XCR-2139 - Learn Slope Settings
  private transient boolean _learnSlopeSettings = false;
  
  //Siew Yeng - XCR-2764 - Sub-subtype feature
  public static transient int _MAX_SUB_REGION = 10;
  private boolean _isSubSubtype = false;
  private List<Subtype> _subSubtypes = new ArrayList<Subtype>();

  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public Subtype()
  {
    _padTypeSet = new HashSet<PadType>();
    _projectObservable.stateChanged(this, null, this, SubtypeEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      getPanel().removeSubtype(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, SubtypeEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Matt Wharton
   */
  public Panel getPanel()
  {
    Assert.expect(_panel != null);

    return _panel;
  }

  /**
   * @author Bill Darbie
   */
  public void setPanel(Panel panel)
  {
    Assert.expect(panel != null);

    Panel oldValue = _panel;
    _projectObservable.setEnabled(false);
    try
    {
      _panel = panel;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, panel, SubtypeEventEnum.PANEL);
  }

  /**
   * Create a new Subtype for the jointTypeEnum passed in.  This constructor will set the
   * subtype to have all Algorithms enabled unless an algorithm has overridden the algorithmIsEnabledByDefault() method.
   * @author Bill Darbie
   * @author George Booth
   * @author Siew Yeng - added parameter isSubSubtype
   */
  public static Subtype createNewSubtype(Panel panel, String longName, String shortName, JointTypeEnum jointTypeEnum, Algorithm algo, boolean isSubSubtype)
  {
    Assert.expect(panel != null);
    Assert.expect(longName != null);
    Assert.expect(shortName != null);
    Assert.expect(jointTypeEnum != null);

    Subtype subtype = new Subtype();
    subtype.setPanel(panel);
    subtype.setJointTypeEnum(jointTypeEnum);
    subtype.setUserComment("");

    // look up the InspectionFamily for the passed in JointTypeEnum
    InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);
    subtype.setInspectionFamilyEnum(inspectionFamily.getInspectionFamilyEnum());
    // set up the _algorithmEnumToAlgorithmVersionMap
    // set up the _enabledAlgorithmEnums
    for (Algorithm algorithm : inspectionFamily.getAlgorithms())
    {
      //create sub-subtype, algo Voiding will be passed in as parameter, add Voiding algo only to subtype
      if(isSubSubtype == false || (isSubSubtype && algorithm.equals(algo)))
      {
        AlgorithmEnum algorithmEnum = algorithm.getAlgorithmEnum();
        subtype.addAlgorithmEnum(algorithmEnum, algorithm.getVersion());
        if (algorithm.algorithmIsEnabledByDefault(subtype))
          subtype.setAlgorithmEnabled(algorithmEnum, true);
        else
          subtype.setAlgorithmEnabled(algorithmEnum, false);

        // apply the initial threshold value
        // it's probably more speedy to iterate over all the settings, and just set them, but there aren't really a lot of
        // thresholds or subtypes created at import anyway, so this should be fast enough
        for(AlgorithmSetting algorithmSetting : algorithm.getAlgorithmSettings(jointTypeEnum))
        {
          Serializable value = _projectInitialThresholds.getAlgorithmSettingValue(jointTypeEnum, algorithm, algorithmSetting);
          Serializable defaultValue = subtype.getAlgorithmSettingDefaultValue(algorithmSetting.getAlgorithmSettingEnum());
          if (defaultValue.equals(value) == false)
            subtype.setTunedValue(algorithmSetting.getAlgorithmSettingEnum(), value);
        }
      }
    }
    subtype.setLongName(longName, isSubSubtype);
    subtype.setShortName(shortName);
    subtype.setAlgorithmSettingsLearned(false);
    subtype.setJointSpecificDataLearned(false);
    subtype.setExpectedImageDataLearned(false);
    subtype.setExpectedImageTemplateLearned(false);//ShengChuan - Clear Tombstone
    SubtypeAdvanceSettings subtypeAdvanceSettings = SubtypeAdvanceSettings.createNewSubtypeAdvanceSettings(subtype);
    subtype.setSubtypeAdvanceSettings(subtypeAdvanceSettings);

    subtype.setIsSubSubtype(isSubSubtype);
    
    Assert.expect(subtype != null);
    return subtype;
  }
  
  /**
   * Create a new Subtype for the jointTypeEnum passed in.  This constructor will set the
   * subtype to have all Algorithms enabled unless an algorithm has overridden the algorithmIsEnabledByDefault() method.
   * @author Bill Darbie
   * @author George Booth
   */
  public static Subtype createNewSubtype(Panel panel, String longName, String shortName, JointTypeEnum jointTypeEnum)
  {
    return createNewSubtype(panel, longName, shortName, jointTypeEnum, null, false);
  }

  /**
   * @author Bill Darbie
   */
  public void setShortName(String shortName)
  {
    Assert.expect(shortName != null);
    Assert.expect(_panel.isSubtypeNameValid(shortName));
    // short name can be duplicate
    //Assert.expect(_panel.isSubtypeNameDuplicate(name) == false);

    if (shortName.equals(_shortName))
      return;

    String oldValue = _shortName;
    _projectObservable.setEnabled(false);
    try
    {
      _shortName = shortName.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, shortName, SubtypeEventEnum.SHORT_NAME);
  }

  /**
   * @author Bill Darbie
   */
  public String getShortName()
  {
    Assert.expect(_shortName != null);

    return _shortName;
  }

  /**
   * @author Bill Darbie
   */
  public void setLongName(String origName, boolean isSubSubtype)
  {
    Assert.expect(origName != null);
    Assert.expect(_panel.isSubtypeNameValid(origName));
    Assert.expect(_panel.isSubtypeNameDuplicate(origName) == false);

    if (origName.equals(_longName))
      return;

    String oldValue = _longName;
    _projectObservable.setEnabled(false);
    try
    {
      Assert.expect(_panel != null);
      //Siew Yeng -  XCR-2764 - subSubtype does not need to add into the map
      if(isSubSubtype == false)
        _panel.setNewSubtypeName(this, oldValue, origName);

      _longName = origName.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, origName, SubtypeEventEnum.LONG_NAME);
  }

  /**
   * @author Bill Darbie
   */
  public String getLongName()
  {
    Assert.expect(_longName != null);
    return _longName;
  }

  /**
   * @author Bill Darbie
   */
  public void setInspectionFamilyEnum(InspectionFamilyEnum inspectionFamilyEnum)
  {
    Assert.expect(inspectionFamilyEnum != null);

    InspectionFamilyEnum oldValue = _inspectionFamilyEnum;
    _projectObservable.setEnabled(false);
    try
    {
      _inspectionFamilyEnum = inspectionFamilyEnum;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, inspectionFamilyEnum, SubtypeEventEnum.INSPECTION_FAMILY_ENUM);
  }

  /**
   * @author Peter Esbensen
   */
  public InspectionFamily getInspectionFamily()
  {
    Assert.expect(_inspectionFamilyEnum != null);
    return InspectionFamily.getInstance(_inspectionFamilyEnum);
  }


  /**
   * @author Peter Esbensen
   */
  public InspectionFamilyEnum getInspectionFamilyEnum()
  {
    Assert.expect(_inspectionFamilyEnum != null);
    return _inspectionFamilyEnum;
  }

  /**
   * @author Peter Esbensen
   */
  public void addAlgorithmEnum(AlgorithmEnum algorithmEnum, int algVersion)
  {
    Assert.expect(algorithmEnum != null);

    _projectObservable.setEnabled(false);
    try
    {
      _algorithmEnumToAlgorithmVersionMap.put(algorithmEnum, algVersion);

      _enabledAlgorithms = null;
      _algorithms = null;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, algorithmEnum, SubtypeEventEnum.ADD_OR_REMOVE_ALGORITHM_ENUM);
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public void setAlgorithmEnabled(AlgorithmEnum algorithmEnum, boolean enabled)
  {
    Assert.expect(algorithmEnum != null);
    Assert.expect(_algorithmEnumToAlgorithmVersionMap.keySet().contains(algorithmEnum));

    Pair<AlgorithmEnum, Boolean> oldValue = null;
    if (_enabledAlgorithmEnums.contains(algorithmEnum))
      oldValue = new Pair<AlgorithmEnum, Boolean>(algorithmEnum, true);
    else
      oldValue = new Pair<AlgorithmEnum, Boolean>(algorithmEnum, false);
    _projectObservable.setEnabled(false);
    try
    {
      //Ngie Xing, add checking to remove duplication of Enabled AlgorithmEnums
      if (enabled && _enabledAlgorithmEnums.contains(algorithmEnum) == false)
      {
        _enabledAlgorithmEnums.add(algorithmEnum);
      }
      else if(enabled == false && _enabledAlgorithmEnums.contains(algorithmEnum) == true)
      {
        _enabledAlgorithmEnums.remove(algorithmEnum);
      }
      // Set the enabled algorithms list to null to force it to be regenerated.
      _enabledAlgorithms = null;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    Pair<AlgorithmEnum, Boolean> newValue = new Pair<AlgorithmEnum, Boolean>(algorithmEnum, enabled);
    _projectObservable.stateChanged(this, oldValue, newValue, SubtypeEventEnum.ALGORITHM_ENABLED);
  }

  /**
   * A " character is not allowed in the comment field
   * @author Bill Darbie
   */
  public boolean isUserCommentValid(String comment)
  {
    Assert.expect(comment != null);
    if (comment.contains("\""))
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  public void setUserComment(String comment)
  {
    Assert.expect(comment != null);
    Assert.expect(isUserCommentValid(comment));

    String oldValue = _userComment;
    _projectObservable.setEnabled(false);
    try
    {
      _userComment = comment.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, comment, SubtypeEventEnum.USER_COMMENT);
  }

  /**
   * @author Peter Esbensen
   */
  public String getUserComment()
  {
    Assert.expect(_userComment != null);
    return _userComment;
  }

  /**
   * @author Bill Darbie
   */
  public void setJointTypeEnum(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    JointTypeEnum oldValue = _jointTypeEnum;
    _projectObservable.setEnabled(false);
    try
    {
      _jointTypeEnum = jointTypeEnum;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, jointTypeEnum, SubtypeEventEnum.JOINT_TYPE_ENUM);
  }

  /**
   * @author Bill Darbie
   */
  public JointTypeEnum getJointTypeEnum()
  {
    Assert.expect(_jointTypeEnum != null);
    return _jointTypeEnum;
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  private void generateAlgorithms()
  {
    syncAlgorithmEnumToAlgorithmVersionMap();

    _algorithms = new ArrayList<Algorithm>();
    for (AlgorithmEnum algorithmEnum : _algorithmEnumToAlgorithmVersionMap.keySet())
    {
      if (InspectionFamily.hasAlgorithm(_inspectionFamilyEnum, algorithmEnum))
      {
        Algorithm algorithm = InspectionFamily.getAlgorithm(_inspectionFamilyEnum, algorithmEnum);
        _algorithms.add(algorithm);
      }
    }
    // make sure the list is in inspection order
    Collections.sort(_algorithms, new AlgorithmInspectionOrderComparator());
  }

  /**
   * @author Matt Wharton
   */
  private void syncAlgorithmEnumToAlgorithmVersionMap()
  {
    // We need to make sure our alg enum->version map is properly updated when we add a new algorithm
    // to a family.  This ensures that the algorithm is properly "registered" to this Subtype when an
    // existing project is loaded.  The new algorithm is disabled by default.
    InspectionFamily inspectionFamily = InspectionFamily.getInstance(_inspectionFamilyEnum);
    for (AlgorithmEnum algEnum : inspectionFamily.getAlgorithmEnums())
    {
      if (_algorithmEnumToAlgorithmVersionMap.containsKey(algEnum) == false)
      {
        Algorithm algorithm = InspectionFamily.getAlgorithm(_inspectionFamilyEnum, algEnum);
        _algorithmEnumToAlgorithmVersionMap.put(algEnum, algorithm.getVersion());
      }
    }
  }

  /**
   * @return a List of all Algorithms for this subtype, both enabled and disabled
   *
   * PE:  This needs to be sychronized so that the generateAlgorithms() call doesn't get out of sync when
   * multiple threads call this method.
   *
   * @author Bill Darbie
   */
  public synchronized List<Algorithm> getAlgorithms()
  {
    if (_algorithms == null)
      generateAlgorithms();

    return _algorithms;
  }

  /**
   * @return a List of all AlgorithmEnums for this subtype, both enabled and disabled
   * @author Bill Darbie
   */
  public List<AlgorithmEnum> getAlgorithmEnums()
  {
    syncAlgorithmEnumToAlgorithmVersionMap();

    ArrayList<AlgorithmEnum> algorithmEnums = new ArrayList<AlgorithmEnum>(_algorithmEnumToAlgorithmVersionMap.keySet());
    Collections.sort(algorithmEnums, new AlgorithmEnumInspectionOrderComparator());

    return algorithmEnums;
  }

  /**
   * @author Bill Darbie
   */
  private void generateEnabledAlgorithms()
  {
    _enabledAlgorithms = new ArrayList<Algorithm>();
    for (AlgorithmEnum algorithmEnum : _enabledAlgorithmEnums)
    {
      Algorithm algorithm = InspectionFamily.getAlgorithm(_inspectionFamilyEnum, algorithmEnum);
      _enabledAlgorithms.add(algorithm);
      Assert.expect(algorithm.getInspectionFamily() == getInspectionFamily());
    }
    // make sure the list is in inspection order
    Collections.sort(_enabledAlgorithms, new AlgorithmInspectionOrderComparator());
  }

  /**
   * PE:  I synchronized this because I was getting unexpected behavior when multiple image analysis threads are
   * processing the same subtype.
   *
   * @return all enabled Algorithms.
   * @author Bill Darbie
   */
  public synchronized List<Algorithm> getEnabledAlgorithms()
  {
    if (_enabledAlgorithms == null)
      generateEnabledAlgorithms();

    return _enabledAlgorithms;
  }

  /**
   * @author Peter Esbensen
   */
  public List<AlgorithmEnum> getEnabledAlgorithmEnums()
  {
    Assert.expect(_enabledAlgorithmEnums != null);
    return _enabledAlgorithmEnums;
  }

  /**
   * Added a general function that check the setting before set tuned value
   * any threshold with condition should be added here to perform initial checking
   * XCR-3456, Able to copy threshold for "masking void method" to true even target subtype do not have mask image
   * @author Yong Sheng Chuan
   */
  boolean isThresholdValid(AlgorithmSettingEnum algorithmSettingEnum)
  {
    boolean isValid = true;
    if(algorithmSettingEnum.equals(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES) && checkIfMaskImageExist() == false)
      isValid = false;
    
    return isValid;
  }
  
  /**
   * @author Bill Darbie
   */
  public void setTunedValue(AlgorithmSettingEnum algorithmSettingEnum, Serializable tunedValue)
  {
    Assert.expect(algorithmSettingEnum != null);
    Assert.expect(tunedValue != null);

    //XCR-3456, Able to copy threshold for "masking void method" to true even target subtype do not have mask image
    if (isThresholdValid(algorithmSettingEnum) == false)
      return;
    
    Pair<AlgorithmSettingEnum, Serializable> oldValue = new Pair<AlgorithmSettingEnum, Serializable>(algorithmSettingEnum, getAlgorithmSettingValue(algorithmSettingEnum));
    _projectObservable.setEnabled(false);
    try
    {
      // make sure that the algorithmSetting passed in exists for one of the Algorithms of
      // this subtype
      boolean validAlgorithmSetting = false;
      for (Algorithm algorithm : getAlgorithms())
      {
        Collection<AlgorithmSettingEnum> algorithmSettingEnums = algorithm.getAlgorithmSettingEnums();
        if (algorithmSettingEnums.contains(algorithmSettingEnum))
        {
          validAlgorithmSetting = true;
          break;
        }
      }
      Assert.expect(validAlgorithmSetting);
      _algorithmSettingEnumToTunedValueMap.put(algorithmSettingEnum, tunedValue);

      if(algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_10) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_9) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_8) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_7) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_6) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_5) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_4) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_3) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_2) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT_IN_THICKNESS) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT_IN_THICKNESS) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_10) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_9) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_8) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_7) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_6) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_5) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_4) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_3) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_2) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT) ||
         algorithmSettingEnum.equals(AlgorithmSettingEnum.SLICEHEIGHT_WORKING_UNIT))
        UserDefinedSliceheights.changeSliceHeightUnit(getAlgorithmSettingValue(AlgorithmSettingEnum.SLICEHEIGHT_WORKING_UNIT).toString().matches("Thickness"));
      
      if (algorithmSettingEnum.equals(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_DEFINE_OFFSET_FOR_PAD_AND_PACKAGE))
      {
        if (tunedValue instanceof String)
        {
          if (tunedValue.toString().matches("On"))
          {
            //Swee Yee Wong - user defined midball to edge offset
            Float midballToEdgeOffset = 0.0f;

            if (getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_DEFINE_OFFSET_FOR_PAD_AND_PACKAGE).toString().matches("On"))
            {
              Serializable value = getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_MIDBALL_TO_EDGE_SLICEHEIGHT);
              if (value instanceof Float)
              {
                midballToEdgeOffset = (Float) value;
              }

              // if this offset value is not learnt (equal to offset value, 0.0f), learn and get midball to edge offset value for this subtype
              if (midballToEdgeOffset == 0.0f)
              {
                Algorithm gridArrayMeasurementAlgorithm = InspectionFamily.getAlgorithm(InspectionFamilyEnum.GRID_ARRAY, AlgorithmEnum.MEASUREMENT);
                gridArrayMeasurementAlgorithm.learnAndUpdateAlgorithmSettingIfNecessaryDuringProgramGeneration(this, AlgorithmSettingEnum.USER_DEFINED_MIDBALL_TO_EDGE_SLICEHEIGHT);
              }
            }
          }
        }
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    Pair<AlgorithmSettingEnum, Serializable> newValue = new Pair<AlgorithmSettingEnum, Serializable>(algorithmSettingEnum, tunedValue);

    boolean settingIsForSliceheights = UserDefinedSliceheights.getAlgorithmSettingEnums().contains(algorithmSettingEnum);
    if (settingIsForSliceheights || 
        algorithmSettingEnum.equals(AlgorithmSettingEnum.GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE) ||
        algorithmSettingEnum.equals(AlgorithmSettingEnum.USER_DEFINED_NUMBER_OF_SLICE)||
        algorithmSettingEnum.equals(AlgorithmSettingEnum.ADVANCED_GULLWING_USER_DEFINED_NUMBER_OF_SLICE)||
        algorithmSettingEnum.equals(AlgorithmSettingEnum.LARGE_PAD_NUMBER_OF_USER_DEFINED_SLICE)||
        algorithmSettingEnum.equals(AlgorithmSettingEnum.USER_DEFINED_MIDBALL_TO_EDGE_SLICEHEIGHT)||
        algorithmSettingEnum.equals(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_DEFINE_OFFSET_FOR_PAD_AND_PACKAGE))
      _projectObservable.stateChanged(this, oldValue, newValue, SubtypeEventEnum.TUNED_SLICE_HEIGHT_VALUE);
    else if(algorithmSettingEnum.equals(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_METHOD))
      _projectObservable.stateChanged(this, oldValue, newValue, SubtypeEventEnum.VOID_METHOD);
    else if(algorithmSettingEnum.equals(AlgorithmSettingEnum.LARGE_PAD_VOIDING_METHOD))
      _projectObservable.stateChanged(this, oldValue, newValue, SubtypeEventEnum.VOID_METHOD);
    else if (algorithmSettingEnum.equals(AlgorithmSettingEnum.GULLWING_MEASUREMENT_VOID_COMPENSATION_METHOD) ||
             algorithmSettingEnum.equals(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_VOID_COMPENSATION_METHOD))
      _projectObservable.stateChanged(this, oldValue, newValue, SubtypeEventEnum.TUNED_VALUE);
    else if(algorithmSettingEnum.equals(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES))
      _projectObservable.stateChanged(this, oldValue, newValue, SubtypeEventEnum.MASK_IMAGE);  
    else if(algorithmSettingEnum.equals(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM) || algorithmSettingEnum.equals(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM))
      _projectObservable.stateChanged(this, oldValue, newValue, SubtypeEventEnum.TUNED_GRAYLEVEL_MIN_MAX_CHANGE);
    else if((algorithmSettingEnum.equals(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_LOCATOR_METHOD)) 
            || (algorithmSettingEnum.equals(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_METHOD)))
      _projectObservable.stateChanged(this, oldValue, newValue, SubtypeEventEnum.TUNED_VALUE);
    //Khaw Chek Hau - XCR3601: Auto distribute the pressfit region by direction of artifact
    else if((algorithmSettingEnum.equals(AlgorithmSettingEnum.ARTIFACT_BASED_REGION_SEPERATION_DIRECTION))) 
      _projectObservable.stateChanged(this, oldValue, newValue, SubtypeEventEnum.SET_ARTIFACT_BASED_REGION_SEPERATION_DIRECTION);
    else
      _projectObservable.stateChanged(this, oldValue, newValue, SubtypeEventEnum.TUNED_VALUE);
  }

  /**
   * @author Poh Kheng
   */
  public void setTunedValueFromLibrary(AlgorithmSettingEnum algorithmSettingEnum, Serializable tunedValue)
  {
    Assert.expect(algorithmSettingEnum != null);
    Assert.expect(tunedValue != null);

    // make sure that the algorithmSetting passed in exists for one of the Algorithms of
    // this subtype
    boolean validAlgorithmSetting = false;
    for (Algorithm algorithm : getAlgorithms())
    {
      Collection<AlgorithmSettingEnum> algorithmSettingEnums = algorithm.getAlgorithmSettingEnums();
      if (algorithmSettingEnums.contains(algorithmSettingEnum))
      {
        validAlgorithmSetting = true;
        break;
      }
    }
      Assert.expect(validAlgorithmSetting);

    _algorithmSettingEnumToTunedValueMap.put(algorithmSettingEnum, tunedValue);
  }


  /**
   * @author Bill Darbie
   */
  public void setAlgorithmSettingUserComment(AlgorithmSettingEnum algorithmSettingEnum,
                                             String algorithmSettingUserComment)
  {
    Assert.expect(algorithmSettingEnum != null);
    Assert.expect(algorithmSettingUserComment != null);

    _projectObservable.setEnabled(false);
    Pair<AlgorithmSettingEnum, String> oldValue = new Pair<AlgorithmSettingEnum, String>(algorithmSettingEnum, getAlgorithmSettingUserComment(algorithmSettingEnum));
    try
    {
      // make sure that the algorithmSetting passed in exists for one of the Algorithms of
      // this subtype
      boolean validAlgorithmSetting = false;
      for (Algorithm algorithm : getAlgorithms())
      {
        Collection<AlgorithmSettingEnum> algorithmSettingEnums = algorithm.getAlgorithmSettingEnums();
        if (algorithmSettingEnums.contains(algorithmSettingEnum))
        {
          validAlgorithmSetting = true;
          break;
        }
      }
      Assert.expect(validAlgorithmSetting);

      if (algorithmSettingUserComment.equals(""))
      {
        // remove the AlgorithmSettingEnum from the map if the comment is set back to ""
        _algorithmSettingEnumToUserCommentMap.remove(algorithmSettingEnum);
      }
      else
      {
        _algorithmSettingEnumToUserCommentMap.put(algorithmSettingEnum, algorithmSettingUserComment);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    Pair<AlgorithmSettingEnum, String> newValue = new Pair<AlgorithmSettingEnum, String>(algorithmSettingEnum, algorithmSettingUserComment);

    _projectObservable.stateChanged(this, oldValue, newValue, SubtypeEventEnum.ALGORITHM_SETTING_USER_COMMENT);
  }

  /**
   * @author Bill Darbie
   */
  public String getAlgorithmSettingUserComment(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    String userComment = _algorithmSettingEnumToUserCommentMap.get(algorithmSettingEnum);
    if (userComment == null)
      userComment = "";

    return userComment;
  }

  /**
   * @author Bill Darbie
   */
  public List<AlgorithmSettingEnum> getCommentedAlgorithmSettingEnums()
  {
    return new ArrayList<AlgorithmSettingEnum>(_algorithmSettingEnumToUserCommentMap.keySet());
  }

  /**
   * @author Bill Darbie
   */
  public List<AlgorithmSettingEnum> getTunedAlgorithmSettingEnums()
  {
    return new ArrayList<AlgorithmSettingEnum>(_algorithmSettingEnumToTunedValueMap.keySet());
  }

  /**
   * @author Wei Chin, Chong
   */
  public List<AlgorithmSettingEnum> getImportedAlgorithmSettingEnums()
  {
    return new ArrayList<AlgorithmSettingEnum>(_algorithmSettingEnumToImportedValueMap.keySet());
  }

  /**
   * @author Sunit Bhalla
   */
  public Serializable getAlgorithmSettingName(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);
    Serializable value = null;
    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
        value = algorithm.getSettingName(algorithmSettingEnum,
                                         _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
    }

    Assert.expect(value != null);
    return value;
  }


  /**
   * @author Peter Esbensen
   */
  public Serializable getAlgorithmSettingValue(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    Serializable value = null;
    if (_algorithmSettingEnumToTunedValueMap.containsKey(algorithmSettingEnum))
      value = _algorithmSettingEnumToTunedValueMap.get(algorithmSettingEnum);
    else
    {
      for (Algorithm algorithm : getAlgorithms())
      {
        if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
        {
          value = algorithm.getSettingDefaultValue(algorithmSettingEnum,
                                                   _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
        }
      }
    }

    Assert.expect(value != null, "Tried to get value for non-existent setting: " + algorithmSettingEnum.getName() );
    return value;
  }

  /**
   * @author Peter Esbensen
   */
  public Serializable getAlgorithmSettingDefaultValue(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);
    Serializable value = null;
    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
        value = algorithm.getSettingDefaultValue(algorithmSettingEnum,
                                                 _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * @author Patrick Lacz
   */
  public boolean isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);
    Serializable defaultValue = getAlgorithmSettingDefaultValue(algorithmSettingEnum);
    Serializable value = getAlgorithmSettingValue(algorithmSettingEnum);
    if (value instanceof Number)
    {
      return MathUtil.fuzzyEquals(((Number)defaultValue).doubleValue(), ((Number)value).doubleValue());
    }
    return defaultValue.equals(value);
  }

  /**
   * @author Andy Mechtenberg
   * @author Patrick Lacz
   */
  public List<String> getAlgorithmSettingValuesList(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);
    Serializable value = null;
    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
        value = algorithm.getSettingValuesList(algorithmSettingEnum,
                                               _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
    }

    Assert.expect(value != null);
    Assert.expect(value instanceof List);
    List<String> valueAsListOfStrings = (List<String>)value;
    return valueAsListOfStrings;
  }

  /**
   * @author Peter Esbensen
   */
  public Serializable getAlgorithmSettingMinimumValue(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    Serializable value = null;
    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
      {
        value = algorithm.getSettingMinimumValue(algorithmSettingEnum,
                                                 _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * @author Peter Esbensen
   */
  public Serializable getAlgorithmSettingMaximumValue(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    Serializable value = null;
    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
      {
        value = algorithm.getSettingMaximumValue(algorithmSettingEnum,
                                                 _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
      }
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getAlgorithmSettingUnits(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    String units = null;

    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
      {
        units = algorithm.getSettingUnits(algorithmSettingEnum,
                                          _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
      }
    }

    return units;
  }

  /**
   * @author Patrick Lacz
   */
  public MeasurementUnitsEnum getAlgorithmSettingUnitsEnum(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    MeasurementUnitsEnum units = null;

    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
      {
        units = algorithm.getSettingUnitsEnum(algorithmSettingEnum,
                                          _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
      }
    }

    Assert.expect(units != null);
    return units;
  }


  /**
   * @author Andy Mechtenberg
   */
  public boolean doesAlgorithmSettingDescriptionURLExist(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    URL descriptionURL = null; //com.axi.v810.gui.algoTuner.AlgoTunerGUI.class.getResource("nothresh.html");

    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
      {
        descriptionURL = algorithm.getSettingDescriptionURL(algorithmSettingEnum,
            _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
      }
    }
    if (descriptionURL == null)
      return false;
    else
      return true;
  }


  /**
   * @author Andy Mechtenberg
   */
  public URL getAlgorithmSettingDescriptionURL(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    URL descriptionURL = null; //com.axi.v810.gui.algoTuner.AlgoTunerGUI.class.getResource("nothresh.html");

    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
      {
        descriptionURL = algorithm.getSettingDescriptionURL(algorithmSettingEnum,
            _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
      }
    }
    return descriptionURL;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean doesAlgorithmSettingDetailedDescriptionURLExist(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    URL detailedDescriptionURL = null; //com.axi.v810.gui.algoTuner.AlgoTunerGUI.class.getResource("nothresh.html");

    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
      {
        detailedDescriptionURL = algorithm.getSettingDetailedDescriptionURL(algorithmSettingEnum,
            _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
      }
    }
    if (detailedDescriptionURL == null)
      return false;
    else
      return true;
  }
  /**
   * @author Andy Mechtenberg
   */
  public URL getAlgorithmSettingDetailedDescriptionURL(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    URL detailedDescriptionURL = null; //com.axi.v810.gui.algoTuner.AlgoTunerGUI.class.getResource("nothresh.html");

    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
      {
        detailedDescriptionURL = algorithm.getSettingDetailedDescriptionURL(algorithmSettingEnum,
            _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
      }
    }
    return detailedDescriptionURL;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean doesAlgorithmSettingImageURLExist(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    URL imageURL = null; //com.axi.v810.gui.algoTuner.AlgoTunerGUI.class.getResource("nothresh.html");

    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
      {
        imageURL = algorithm.getSettingImageURL(algorithmSettingEnum,
                                                _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
      }
    }
    if (imageURL == null)
      return false;
    else
      return true;
  }

  /**
   * @author Andy Mechtenberg
   */
  public URL getAlgorithmSettingImageURL(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    URL imageURL = null; //com.axi.v810.gui.algoTuner.AlgoTunerGUI.class.getResource("nothresh.html");

    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
      {
        imageURL = algorithm.getSettingImageURL(algorithmSettingEnum,
                                                _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
      }
    }
    return imageURL;
  }

  /**
   * Gets the AlgorithmSettingTypeEnum for the specified AlgorithmSettingEnum.
   * Algorithm version is intrinsically compensated for.
   *
   * @author Matt Wharton
   */
  public AlgorithmSettingTypeEnum getAlgorithmSettingTypeEnum(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    AlgorithmSettingTypeEnum algorithmSettingTypeEnum = null;

    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
      {
        Integer algorithmVersion = _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum());
        Assert.expect(algorithmVersion != null);
        algorithmSettingTypeEnum = algorithm.getAlgorithmSettingTypeEnum(
            algorithmSettingEnum,
            algorithmVersion);

        break;
      }
    }

    Assert.expect(algorithmSettingTypeEnum != null);
    return algorithmSettingTypeEnum;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isTuned()
  {
    if (_algorithmSettingEnumToTunedValueMap.isEmpty())
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  public int getAlgorithmVersion(AlgorithmEnum algorithmEnum)
  {
    Assert.expect(algorithmEnum != null);

    Integer version = _algorithmEnumToAlgorithmVersionMap.get(algorithmEnum);
    Assert.expect(version != null);
    return version;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isAlgorithmEnabled(AlgorithmEnum algorithmEnum)
  {
    Assert.expect(algorithmEnum != null);
    Assert.expect(_algorithmEnumToAlgorithmVersionMap.keySet().contains(algorithmEnum));

    return _enabledAlgorithmEnums.contains(algorithmEnum);
  }

  /**
   * @author Peter Esbensen
   */
  public String toString()
  {
    return getShortName();
  }

  /**
   * @author Sunit Bhalla
   */
  public void setAlgorithmSettingsLearned(boolean learned)
  {
    BooleanRef oldValue = _algorithmSettingsLearned;
    _projectObservable.setEnabled(false);
    try
    {
      if (_algorithmSettingsLearned == null)
        _algorithmSettingsLearned = new BooleanRef(learned);
      else
        _algorithmSettingsLearned.setValue(learned);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, learned, SubtypeEventEnum.ALGORITHM_SETTINGS_LEARNED);
  }

  /**
   * @author Sunit Bhalla
   */
  public boolean areAlgorithmSettingsLearned()
  {
    Assert.expect(_algorithmSettingsLearned != null);
    return _algorithmSettingsLearned.getValue();
  }

  /**
   * @author Sunit Bhalla
   */
  public void setJointSpecificDataLearned(boolean learned)
  {
    BooleanRef oldValue = _jointSpecificDataLearned;

    _projectObservable.setEnabled(false);
    try
    {
      if (_jointSpecificDataLearned == null)
        _jointSpecificDataLearned = new BooleanRef(learned);
      else
      _jointSpecificDataLearned.setValue(learned);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, learned, SubtypeEventEnum.JOINT_SPECIFIC_DATA_LEARNED);
  }

  /**
   * @author Sunit Bhalla
   */
  public boolean isJointSpecificDataLearned()
  {
    Assert.expect(_jointSpecificDataLearned != null);
    return _jointSpecificDataLearned.getValue();
  }

  /**
   * @author George Booth
   */
  public boolean hasExpectedImageDataToLearn()
  {
    Assert.expect(_inspectionFamilyEnum != null);
    // only exposed pad family has expected images
    return _inspectionFamilyEnum.equals(InspectionFamilyEnum.EXPOSED_PAD);
  }
  
  /**
   * @author sheng chuan - Clear Tombstone
   */
  public boolean hasExpectedImageTemplateToLearn()
  {
    Assert.expect(_inspectionFamilyEnum != null);
    // only exposed pad family has expected images
    return _inspectionFamilyEnum.equals(InspectionFamilyEnum.CHIP);
  }

  /**
   * @author George Booth
   */
  public boolean isInteractiveLearningAllowed()
  {
    Assert.expect(_inspectionFamilyEnum != null);
    // Exposed Pad family allows Interactive Learning
    return _inspectionFamilyEnum.equals(InspectionFamilyEnum.EXPOSED_PAD);
  }

  /**
   * @author George Booth
   */
  public void setExpectedImageDataLearned(boolean subtypeLearned)
  {
    BooleanRef oldValue = _expectedImageDataLearned;

    // only set the learned flag true is this subtype deals with expected images
    boolean learned = false;
    if (hasExpectedImageDataToLearn())
    {
      learned = subtypeLearned;
    }

    _projectObservable.setEnabled(false);
    try
    {
      if (_expectedImageDataLearned == null)
        _expectedImageDataLearned = new BooleanRef(learned);
      else
      _expectedImageDataLearned.setValue(learned);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, learned, SubtypeEventEnum.EXPECTED_IMAGE_DATA_LEARNED);
  }

  /**
   * @author George Booth
   */
  public boolean isExpectedImageDataLearned()
  {
    Assert.expect(_expectedImageDataLearned != null);
    return _expectedImageDataLearned.getValue();
  }
  
  /**
   * Clear Tombstone
   * @author Sheng-chuan.yong
   */
  public void setExpectedImageTemplateLearned(boolean subtypeLearned)
  {
    BooleanRef oldValue = _expectedImageTemplateLearned;

    // only set the learned flag true is this subtype deals with expected images
    boolean learned = false;
    if (hasExpectedImageTemplateToLearn())
    {
      learned = subtypeLearned;
    }

    _projectObservable.setEnabled(false);
    try
    {
      if (_expectedImageTemplateLearned == null)
        _expectedImageTemplateLearned = new BooleanRef(learned);
      else
      _expectedImageTemplateLearned.setValue(learned);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, learned, SubtypeEventEnum.EXPECTED_IMAGE_TEMPLATE_LEARNED);
  }
  
  /**
   * Clear Tombstone
   * @author Sheng-chuan.yong
   */
  public boolean isExpectedImageTemplateLearned()
  {
    Assert.expect(_expectedImageTemplateLearned != null);
    return _expectedImageTemplateLearned.getValue();
  }

  /**
   * @author Bill Darbie
   */
  private void getAlgorithmSubtypeLearningReference()
  {
    if (_algorithmSubtypeLearning == null)
      _algorithmSubtypeLearning = getPanel().getProject().getAlgorithmSubtypeLearningReaderWriter();
  }

  /**
   *  @author Sunit Bhalla
   *  This method resets all learned setting back to their defaults
   */
  public void setLearnedSettingsToDefaults()
  {
    _projectObservable.setEnabled(false);
    try
    {
      for (Algorithm algorithm : getAlgorithms())
      {
        algorithm.setLearnedSettingsToDefaults(this);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, SubtypeEventEnum.SET_LEARNED_SETTINGS_TO_DEFAULTS);
  }


  /**
   *  @author Sunit Bhalla
   */
  public void deleteLearnedAlgorithmSettingsData() throws DatastoreException
  {
    _projectObservable.setEnabled(false);
    try
    {
      // Delete the learned measurement data from the database.
      getAlgorithmSubtypeLearningReference();
      _algorithmSubtypeLearning.deleteLearnedJointMeasurements(this);

      setAlgorithmSettingsLearned(false);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_ALL_ALGORITHMS_EXCEPT_SHORT);
  }

  /**
   * Delete the learned data for every joint of this subtype in targetImages.  If a joint does not appear in
   * targetImages then it won't have its learning deleted.
   *
   * @author Peter Esbensen
   * @author Aimee Ong
   */
  public void deleteLearnedJointSpecificData(Collection<ReconstructionRegion> reconstructionRegions,
      Collection<ImageSetData> imageSetDataCollection) throws DatastoreException
  {
    Assert.expect(reconstructionRegions != null);
    Assert.expect(imageSetDataCollection != null);

    _projectObservable.setEnabled(false);

    Set<Pad> padSet = new HashSet<Pad>();

    // first, we'll compute the list of joints that are affected

    // get an instance of ImageManager . . . we'll need it to see which regions have images
    ImageManager imageManager = ImageManager.getInstance();

    // go through each image set
    for (ImageSetData imageSetData : imageSetDataCollection)
    {
      Set<ReconstructionRegion> recoRegionsWithCompatibleImages = imageManager.whichReconstructionRegionsHaveCompatibleImages(imageSetData, reconstructionRegions);
      for (ReconstructionRegion reconstructionRegion : recoRegionsWithCompatibleImages)
      {
        // this image set has images for this region, so it will be relearned.  add its pads to the list
        List<JointInspectionData> jointInspectionDataObjects = reconstructionRegion.getInspectableJointInspectionDataList(this);
        for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
          padSet.add(jointInspectionData.getPad());
      }
    }

    // now we go through all the affected joints and delete their learned data
    try
    {
      for (Pad pad : padSet)
      {

        for (Algorithm algorithm : getAlgorithms())
        {
          // Delete short background profiles
          algorithm.deleteLearnedJointSpecificData(pad);
        }
        setJointSpecificDataLearned(false);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_SHORT);
  }

  /**
   * @author Sunit Bhalla
   */
  public void deleteLearnedJointSpecificData() throws DatastoreException
  {
    _projectObservable.setEnabled(false);
    try
    {
      for (Algorithm algorithm : getAlgorithms())
      {
        // Delete short background profiles
        algorithm.deleteLearnedJointSpecificData(this);
      }
      setJointSpecificDataLearned(false);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_SHORT);
  }

  /**
   * Delete the learned data for every joint of this subtype in targetImages.  If a joint does not appear in
   * targetImages then it won't have its learning deleted.
   *
   * @author George Booth
   * @author Aimee Ong
   */
  public void deleteLearnedExpectedImageData(Collection<ReconstructionRegion> reconstructionRegions,
      Collection<ImageSetData> imageSetDataCollection) throws DatastoreException
  {
    Assert.expect(reconstructionRegions != null);
    Assert.expect(imageSetDataCollection != null);

    _projectObservable.setEnabled(false);

    Set<Pad> padSet = new HashSet<Pad>();

    // first, we'll compute the list of joints that are affected

    // get an instance of ImageManager . . . we'll need it to see which regions have images
    ImageManager imageManager = ImageManager.getInstance();

    // go through each image set
    for (ImageSetData imageSetData : imageSetDataCollection)
    {
      Set<ReconstructionRegion> recoRegionsWithCompatibleImages = imageManager.whichReconstructionRegionsHaveCompatibleImages(imageSetData, reconstructionRegions);
      for (ReconstructionRegion reconstructionRegion : recoRegionsWithCompatibleImages)
      {
        // this image set has images for this region, so it will be relearned.  add its pads to the list
        List<JointInspectionData> jointInspectionDataObjects = reconstructionRegion.getInspectableJointInspectionDataList(this);
        for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
          padSet.add(jointInspectionData.getPad());
      }
    }

    // now we go through all the affected joints and delete their learned data
    try
    {
      for (Pad pad : padSet)
      {

        for (Algorithm algorithm : getAlgorithms())
        {
          // Delete short background profiles
          algorithm.deleteLearnedExpectedImageData(pad);
        }
        setExpectedImageDataLearned(false);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_EXPECTED_IMAGE);
  }

  /**
   * @author George Booth
   */
  public void deleteLearnedExpectedImageData() throws DatastoreException
  {
    _projectObservable.setEnabled(false);
    try
    {
      for (Algorithm algorithm : getAlgorithms())
      {
        // Delete short background profiles
        algorithm.deleteLearnedExpectedImageData(this);
      }
      setExpectedImageDataLearned(false);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_EXPECTED_IMAGE);
  }
  
  /**
   * Clear Tombstone
   * @author sheng chuan
   */
  public void deleteLearnedExpectedImageTemplateData() throws DatastoreException
  {
    _projectObservable.setEnabled(false);
    try
    {
      for (Algorithm algorithm : getAlgorithms())
      {
        // Delete short background profiles
        algorithm.deleteLearnedImageTemplateData(this);
      }
      setExpectedImageTemplateLearned(false);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, SubtypeEventEnum.DELETE_LEARNED_DATA_FOR_EXPECTED_IMAGE_TEMPLATE);
  }
  

  /**
   * Deletes the learned joint measurement data for the specified MeasurementEnum.
   *
   * @author Matt Wharton
   */
  public void deleteLearnedMeasurementData(MeasurementEnum measurementEnum) throws DatastoreException
  {
    Assert.expect(measurementEnum != null);

    getAlgorithmSubtypeLearningReference();
    _algorithmSubtypeLearning.deleteLearnedJointMeasurements(this, measurementEnum);
  }

  /**
   * Deletes the learned joint measurement data for ALL measurements for the subtype.

   * @author Matt Wharton
   */
  public void deleteLearnedMeasurementData() throws DatastoreException
  {
    getAlgorithmSubtypeLearningReference();
    _algorithmSubtypeLearning.deleteLearnedJointMeasurements(this);
  }

  /**
   * @author George Booth
   */
  public void enableInteractiveLearning(boolean interactiveLearning)
  {
    _interactiveLearning = interactiveLearning;
  }
  
  /*
   * @author Kee Chin Seong
   *  - Applying all boards' short profile by using first board only.
   */
  public void applyLearningToSubtype(Board board,
                                       ManagedOfflineImageSet typicalBoardImages,
                                       ManagedOfflineImageSet unloadedBoardImages,
                                       boolean learnAlgorithmSettings,
                                       boolean learnJointSpecificData,
                                       boolean learnTemplateData,
                                       boolean learnExpectedImageData) throws DatastoreException
  {
    _projectObservable.setEnabled(false);
    try
    {
      // Added by Lee Herng - 1st Oct 2012
      // Handle the switching of magnification
      ImageAcquisitionEngine.changeMagnification(getSubtypeAdvanceSettings().getMagnificationType());
      
      boolean expectedImageLearned = false;
      boolean expectedImageTemplateLearned = false;
      for (Algorithm algorithm : getAlgorithms())
      {
        algorithm.enableInteractiveLearning(_interactiveLearning);

        if (learnAlgorithmSettings)
        {
          //algorithm.learnAlgorithmSettings(this, typicalBoardImages, unloadedBoardImages);

          // If learning was done with at least one typical board, mark the subtype as "learned."
          //setAlgorithmSettingsLearned(true);
        }

        if (learnJointSpecificData)
        {
          algorithm.learnJointSpecificData(this, board, typicalBoardImages, unloadedBoardImages);

          // If learning was done with at least one typical board, mark short as "learned."
          setJointSpecificDataLearned(true);
        }

        if (learnExpectedImageData)
        {
          //if (algorithm.learnExpectedImageData(this, typicalBoardImages, unloadedBoardImages))
          //{
          //  expectedImageLearned = true;
          //}

          // If any image was learned, mark expected image as "learned."
         // setExpectedImageDataLearned(expectedImageLearned);
        }

        //ShengChuan - Clear Tombstone
        if (learnTemplateData && useTemplateMatch())
        {
          if (algorithm.learnImageTemplateData(this, board, typicalBoardImages, unloadedBoardImages))
          {
            expectedImageTemplateLearned = true;
          }
          setExpectedImageTemplateLearned(expectedImageTemplateLearned);
        }
        //ShengChuan - Clear Tombstone - End

        algorithm.enableInteractiveLearning(false);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, SubtypeEventEnum.LEARN);
  }

  /**
   * @param typicalBoardImages a collection of images that will be used in training.
   * @param unloadedBoardImages a collection of images that will be used in training.
   * @param learnAlgorithmSettings determines whether algorithm settings should be learned
   * @param learnJointSpecificData determines whether joint specific data (e.g. Short profiles) should be learned
   * @author Sunit Bhalla
   * @author Matt Wharton
   * @author George Booth
   */
  public void learn(ManagedOfflineImageSet typicalBoardImages,
                    ManagedOfflineImageSet unloadedBoardImages,
                    boolean learnAlgorithmSettings,
                    boolean learnJointSpecificData,
                    boolean learnTemplateImage,
                    boolean learnExpectedImageData) throws DatastoreException
  {
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    _projectObservable.setEnabled(false);
    try
    {
      // Added by Lee Herng - 1st Oct 2012
      // Handle the switching of magnification
      ImageAcquisitionEngine.changeMagnification(getSubtypeAdvanceSettings().getMagnificationType());
      
      boolean expectedImageLearned = false;
      boolean expectedImageTemplateLearned = false;
      for (Algorithm algorithm : getAlgorithms())
      {
        algorithm.enableInteractiveLearning(_interactiveLearning);

        if (learnAlgorithmSettings)
        {
          algorithm.learnAlgorithmSettings(this, typicalBoardImages, unloadedBoardImages);

          // If learning was done with at least one typical board, mark the subtype as "learned."
          if (typicalBoardImages.size() > 0)
            setAlgorithmSettingsLearned(true);
        }

        if (learnJointSpecificData)
        {
          algorithm.learnJointSpecificData(this, typicalBoardImages, unloadedBoardImages);

          // If learning was done with at least one typical board, mark short as "learned."
          if (typicalBoardImages.size() > 0)
            setJointSpecificDataLearned(true);
        }

        if (learnExpectedImageData)
        {
          if (algorithm.learnExpectedImageData(this, typicalBoardImages, unloadedBoardImages))
          {
            expectedImageLearned = true;
          }

          // If any image was learned, mark expected image as "learned."
          setExpectedImageDataLearned(expectedImageLearned);
        }
        
        //ShengChuan - Clear Tombstone
        if (learnTemplateImage && useTemplateMatch())
        {
          if (algorithm.learnImageTemplateData(this, null, typicalBoardImages, unloadedBoardImages))
          {
            expectedImageTemplateLearned = true;
          }
          setExpectedImageTemplateLearned(expectedImageTemplateLearned);
        }
        //ShengChuan - Clear Tombstone - End

        algorithm.enableInteractiveLearning(false);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, SubtypeEventEnum.LEARN);
  }

  /**
   * @author Matt Wharton
   */
  public void updateNominals(ManagedOfflineImageSet typicalBoardImages,
                             ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    _projectObservable.setEnabled(false);
    try
    {
      for (Algorithm algorithm : getAlgorithms())
      {
        algorithm.updateNominals(this, typicalBoardImages, unloadedBoardImages);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, SubtypeEventEnum.LEARN);
  }

  /**
   * For the specified subtype, the setting is set to the specified value.  If the value is above the maxiumum allowed
   * value, the setting is set to this maximum.  Likewise, if the value is below the minimum alloweded value,
   * the setting is set to this minimum.
   *
   * This method only works on settings that have values of type float.

   * @author Sunit Bhalla
   */
  public void setLearnedValue(JointTypeEnum jointTypeEnum, Algorithm algorithm, AlgorithmSettingEnum algSettingEnum, Float value)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithm != null);
    Assert.expect(algSettingEnum != null);

    // Don't allow setting a NaN value.
    if (value.equals(Float.NaN))
    {
      return;
    }

    // don't set the learned value if the user specified a value in the initial threshold set.
    boolean exist = _projectInitialThresholds.doesAlgorithmSettingExist(jointTypeEnum, algorithm, getAlgorithmSetting(algSettingEnum));
    if (exist)
    {
      Serializable initialValue = _projectInitialThresholds.getAlgorithmSettingValue(jointTypeEnum, algorithm, getAlgorithmSetting(algSettingEnum));
      Serializable currentValue = this.getAlgorithmSettingValue(algSettingEnum);
      if (currentValue.equals(initialValue))
        return;
    }

    MeasurementUnitsEnum unitsEnum = getAlgorithmSettingUnitsEnum(algSettingEnum);

    int decimalPlacesForUnits = unitsEnum.getNumberOfDecimalPlaces();

    float maxAllowedValue = 0.0f;
    float minAllowedValue = 0.0f;

    Serializable maxValueSerializable = getAlgorithmSettingMaximumValue(algSettingEnum);
    Serializable minValueSerializable = getAlgorithmSettingMinimumValue(algSettingEnum);

    if ((maxValueSerializable instanceof Float) == false)
      Assert.expect(false);
    else
      maxAllowedValue = (Float)maxValueSerializable;

    if ((minValueSerializable instanceof Float) == false)
      Assert.expect(false);
    else
      minAllowedValue = (Float)minValueSerializable;

    if (value > maxAllowedValue)
      value = maxAllowedValue;
    else if (value < minAllowedValue)
      value = minAllowedValue;

    setTunedValue(algSettingEnum, MathUtil.roundToPlaces(value, decimalPlacesForUnits));
  }

  /**
   * For the specified subtype, the setting is set to the specified value.  If the value is above the maxiumum allowed
   * value, the setting is set to this maximum.  Likewise, if the value is below the minimum alloweded value,
   * the setting is set to this minimum.
   *
   * This methoed only works on settings that have values of type int.
   * @author Sunit Bhalla
   */
  public void setLearnedValue(JointTypeEnum jointTypeEnum, Algorithm algorithm, AlgorithmSettingEnum algSettingEnum, int value)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithm != null);
    Assert.expect(algSettingEnum != null);

    // don't set the learned value if the user specified a value in the initial threshold set.
    boolean exist = _projectInitialThresholds.doesAlgorithmSettingExist(jointTypeEnum, algorithm, getAlgorithmSetting(algSettingEnum));
    if (exist)
    {
      Serializable initialValue = _projectInitialThresholds.getAlgorithmSettingValue(jointTypeEnum, algorithm, getAlgorithmSetting(algSettingEnum));
      Serializable currentValue = this.getAlgorithmSettingValue(algSettingEnum);
      if (currentValue.equals(initialValue))
        return;
    }

    int maxAllowedValue = 0;
    int minAllowedValue = 0;

    Serializable maxValueSerializable = getAlgorithmSettingMaximumValue(algSettingEnum);
    Serializable minValueSerializable = getAlgorithmSettingMinimumValue(algSettingEnum);

    if ((minValueSerializable instanceof Integer) == false)
      Assert.expect(false);
    if ((maxValueSerializable instanceof Integer) == false)
      Assert.expect(false);

    maxAllowedValue = (Integer)maxValueSerializable;
    minAllowedValue = (Integer)minValueSerializable;

    if (value > maxAllowedValue)
      value = maxAllowedValue;
    if (value < minAllowedValue)
      value = minAllowedValue;

    setTunedValue(algSettingEnum, value);
  }

  /**
   * For the specified subtype, the setting is set to the specified value.  If the value is above the maxiumum allowed
   * value, the setting is set to this maximum.  Likewise, if the value is below the minimum alloweded value,
   * the setting is set to this minimum.
   *
   * This methoed only works on settings that have values of type string.
   * @author Patrick Lacz
   */
  public void setLearnedValue(JointTypeEnum jointTypeEnum, Algorithm algorithm, AlgorithmSettingEnum algSettingEnum, String value)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithm != null);
    Assert.expect(algSettingEnum != null);
    Assert.expect(value != null);

    // don't set the learned value if the user specified a value in the initial threshold set.
    boolean exist = _projectInitialThresholds.doesAlgorithmSettingExist(jointTypeEnum, algorithm, getAlgorithmSetting(algSettingEnum));
    if (exist)
    {
      Serializable initialValue = _projectInitialThresholds.getAlgorithmSettingValue(jointTypeEnum, algorithm, getAlgorithmSetting(algSettingEnum));
      Serializable currentValue = this.getAlgorithmSettingValue(algSettingEnum);
      if (currentValue.equals(initialValue))
        return;
    }

    List<String> availableValues = getAlgorithmSettingValuesList(algSettingEnum);

    Assert.expect(availableValues.contains(value));

    setTunedValue(algSettingEnum, value);
  }

  /**
   * The algSettingEnum setting is set to its default value.
   * @author Sunit Bhalla
   */
  public void setSettingToDefaultValue(AlgorithmSettingEnum algSettingEnum)
  {
    Assert.expect(algSettingEnum != null);

    Serializable newValue = getAlgorithmSettingDefaultValue(algSettingEnum);

    setTunedValue(algSettingEnum, newValue);
  }
  
  /**
   * The algSettingEnum setting is set to its maximum value.
   * @author Lim, Lay Ngor
   */
  public void setSettingToMaximumValue(AlgorithmSettingEnum algSettingEnum)
  {
    Assert.expect(algSettingEnum != null);

    Serializable newValue = getAlgorithmSettingMaximumValue(algSettingEnum);

    setTunedValue(algSettingEnum, newValue);    
  }

  /**
   * @return a LearnedJointMeasurments representing the learned measurement data for the specified MeasurementEnum
   * at the specified SliceNameEnum.
   *
   * @author Matt Wharton
   * @author Sunit Bhalla
   */
  public List<LearnedJointMeasurement> getLearnedJointMeasurements(SliceNameEnum sliceNameEnum,
                                                                   MeasurementEnum measurementEnum,
                                                                   boolean populatedBoard,
                                                                   boolean goodJoint) throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(measurementEnum != null);

    getAlgorithmSubtypeLearningReference();
    return _algorithmSubtypeLearning.readLearnedJointMeasurements(this, sliceNameEnum, measurementEnum, populatedBoard, goodJoint);
  }

  /**
   * @return the raw measurement values representing the learned measurement data for the specified MeasurementEnum
   * at the specified SliceNameEnum.
   *
   * @author Matt Wharton
   * @author Sunit Bhalla
   */
  public float[] getLearnedJointMeasurementValues(SliceNameEnum sliceNameEnum,
                                                  MeasurementEnum measurementEnum,
                                                  boolean populatedBoard,
                                                  boolean goodJoint) throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(measurementEnum != null);

    // Get the LearnedJointMeasurment list and create a simple float[] with the raw values.
    List<LearnedJointMeasurement> learnedJointMeasurements = getLearnedJointMeasurements(sliceNameEnum,
                                                                                         measurementEnum,
                                                                                         populatedBoard,
                                                                                         goodJoint);
    float[] learnedJointMeasurementValues = new float[learnedJointMeasurements.size()];
    int i = 0;
    for (LearnedJointMeasurement learnedJointMeasurement : learnedJointMeasurements)
    {
      learnedJointMeasurementValues[i++] = learnedJointMeasurement.getMeasurementValue();
    }

    return learnedJointMeasurementValues;
  }

  /**
   * @return a LearnedJointMeasurment List representing the learned measurement data for the specified Pad and
   * MeasurementEnum at the specified SliceNameEnum.
   *
   * @author Matt Wharton
   * @author Sunit Bhalla
   */
  public List<LearnedJointMeasurement> getLearnedJointMeasurements(Pad pad,
                                                                   SliceNameEnum sliceNameEnum,
                                                                   MeasurementEnum measurementEnum,
                                                                   boolean populatedBoard,
                                                                   boolean goodJoint) throws DatastoreException
  {
    Assert.expect(pad != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(measurementEnum != null);

    getAlgorithmSubtypeLearningReference();
    return _algorithmSubtypeLearning.readLearnedJointMeasurements(this, pad, sliceNameEnum, measurementEnum, populatedBoard, goodJoint);
  }

  /**
   * @return the raw measurement values representing the learned measurement data for the specified MeasurementEnum
   * at the specified SliceNameEnum.
   *
   * @author Matt Wharton
   * @author Sunit Bhalla
   */
  public float[] getLearnedJointMeasurementValues(Pad pad,
                                                  SliceNameEnum sliceNameEnum,
                                                  MeasurementEnum measurementEnum,
                                                  boolean populatedBoard,
                                                  boolean goodJoint) throws DatastoreException
  {
    Assert.expect(pad != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(measurementEnum != null);

    // Get the LearnedJointMeasurment list and create a simple float[] with the raw values.
    List<LearnedJointMeasurement> learnedJointMeasurements = getLearnedJointMeasurements(pad,
                                                                                         sliceNameEnum,
                                                                                         measurementEnum,
                                                                                         populatedBoard,
                                                                                         goodJoint);
    float[] learnedJointMeasurementValues = new float[learnedJointMeasurements.size()];
    int i = 0;
    for (LearnedJointMeasurement learnedJointMeasurement : learnedJointMeasurements)
    {
      learnedJointMeasurementValues[i++] = learnedJointMeasurement.getMeasurementValue();
    }

    return learnedJointMeasurementValues;
  }

  /**
   * Adds the specified joint measurement datum to the learning database.
   *
   * @author Matt Wharton
   */
  public void addLearnedJointMeasurementData(LearnedJointMeasurement learnedJointMeasurement) throws DatastoreException
  {
    Assert.expect(learnedJointMeasurement != null);

    getAlgorithmSubtypeLearningReference();
    _algorithmSubtypeLearning.writeLearnedJointMeasurement(learnedJointMeasurement);
  }

  /**
   * Adds the specified list of LearnedJointMeasurements to the learning database.
   *
   * @author Matt Wharton
   * @author Sunit Bhalla
   */
  public void addLearnedJointMeasurementData(List<LearnedJointMeasurement> learnedJointMeasurements) throws DatastoreException
  {
    Assert.expect(learnedJointMeasurements != null);

    for (LearnedJointMeasurement learnedJointMeasurement : learnedJointMeasurements)
    {
      addLearnedJointMeasurementData(learnedJointMeasurement);
    }
  }

  /**
   * @author Bill Darbie
   */
  public void assignedToPadType(PadType padType)
  {
    Assert.expect(padType != null);
    boolean added = _padTypeSet.add(padType);
    Assert.expect(added);
    invalidatePads();
  }

  /**
   * @author Bill Darbie
   */
  public void removeFromPadType(PadType padType)
  {
    Assert.expect(padType != null);
    boolean removed = _padTypeSet.remove(padType);
    // okay if the padtype did not already exist
    invalidatePads();
  }

  /**
   * @author Bill Darbie
   */
  public void invalidatePads()
  {
    _pads = null;
  }

  /**
   * @author Bill Darbie
   */
  public List<Pad> getPads()
  {
    Assert.expect(_panel != null);

    if (_pads == null)
    {
      _pads = new ArrayList<Pad>();
      for (PadType padType : _padTypeSet)
      {
        _pads.addAll(padType.getPads());
      }
    }

    Assert.expect(_pads != null);
    return _pads;
  }

  /**
   * @author George A. David
   */
  public int getJointHeightInNanoMeters()
  {
    int jointHeight = -1;
    for(Pad pad : getPads())
    {
      jointHeight = Math.max(jointHeight, pad.getPackagePin().getJointHeightInNanoMeters());
    }

    Assert.expect(jointHeight >= 0);
    return jointHeight;
  }

  /**
   * @author Laura Cormos
   */
  public AlgorithmSetting getAlgorithmSetting(AlgorithmSettingEnum algoSettingEnum)
  {
    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algoSettingEnum))
          return algorithm.getAlgorithmSetting(algoSettingEnum);
    }
    Assert.expect(false);
    return null;
  }

  /**
   * @author Lim, Seng Yew
   */
  public boolean doesAlgorithmSettingExist(AlgorithmSettingEnum algoSettingEnum)
  {
    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algoSettingEnum))
          return true;
    }
    return false;
  }

  /**
   * @author Laura Cormos
   */
  public boolean usePredictiveSliceHeight()
  {
    Serializable value = getAlgorithmSettingValue(AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT);
    Assert.expect(value instanceof String);
    if (value.equals("Off"))
      return false;
    else if (value.equals("On"))
      return true;
    else if (value.equals("Auto"))
      return true;
    else
      Assert.expect(false, "Illegal value for threshold");
    return false;
  }
  
  /**
   * Clear Tombstone
   * @author sheng chuan
   */
  public boolean useTemplateMatch()
  {
    if(_jointTypeEnum.equals(JointTypeEnum.CAPACITOR) == false && _jointTypeEnum.equals(JointTypeEnum.RESISTOR) == false)
      return false;
    
    Serializable value = getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_DETECTION_METHOD);
    Assert.expect(value instanceof String);
    if (value.toString().equalsIgnoreCase("template match"))
      return true;
    else
      return false;
  }

  /**
   * @author Cheah, Lee Herng
   */
  public boolean useAutoPredictiveSliceHeight()
  {
      Serializable value = getAlgorithmSettingValue(AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT);
      Assert.expect(value instanceof String);
      if (value.equals("Auto"))
          return true;
      else
          return false;
  }

  /**
   * This would set where this subtype library path imported
   *
   * @author Poh Kheng
   */
  public void setImportedPackageLibraryPath(String importedPackageLibraryPath)
  {
    Assert.expect(importedPackageLibraryPath != null);

    if (importedPackageLibraryPath.equals(_importedPackageLibraryPath))
      return;

    String oldValue = _importedPackageLibraryPath;
    _projectObservable.setEnabled(false);
    try
    {
      _importedPackageLibraryPath = importedPackageLibraryPath.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, importedPackageLibraryPath, SubtypeEventEnum.SHORT_NAME);
  }

  /**
   * @author Poh Kheng
   */
  public String getImportedPackageLibraryPath()
  {
    Assert.expect(_importedPackageLibraryPath != null);

    return _importedPackageLibraryPath;
  }

  /**
   * @return boolean
   *
   * @author Wei Chin, Chong
   */
  public boolean isImportedPackageLibrary()
  {
    return (_importedPackageLibraryPath != null) && (_importedPackageLibraryPath.length() > 0);
  }

  /**
   * @param algorithmSettingEnum AlgorithmSettingEnum
   * @param value Serializable
   * @return boolean, true = imported, false = modified
   *
   * @author Wei Chin, Chong
   */
  public boolean isImportedValue(AlgorithmSettingEnum algorithmSettingEnum, Serializable value)
  {
    Assert.expect(algorithmSettingEnum != null);
    Assert.expect(value != null);
    Assert.expect(isImportedPackageLibrary());

    Serializable importedValue = null;

    if(_algorithmSettingEnumToImportedValueMap.containsKey(algorithmSettingEnum))
      importedValue = _algorithmSettingEnumToImportedValueMap.get(algorithmSettingEnum);
    else
      return false;

    if(importedValue instanceof Float &&
       value instanceof Float)
    {
      Float importValueFloat = (Float)importedValue;
      Float valueFloat = (Float)value;
      importValueFloat = MathUtil.roundToPlaces(importValueFloat, 4);
      valueFloat = MathUtil.roundToPlaces(valueFloat, 4);
      return importValueFloat.equals(valueFloat);
    }
    else
      return importedValue.equals(value);
  }

  /**
   * @param algorithmSettingEnum AlgorithmSettingEnum
   * @return Serializable
   *
   * @author Wei Chin, Chong
   */
  public Serializable getImportedAlgorithmSettingValue(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    Serializable value = null;

    if (_algorithmSettingEnumToImportedValueMap.containsKey(algorithmSettingEnum))
      value = _algorithmSettingEnumToImportedValueMap.get(algorithmSettingEnum);

    Assert.expect(value != null, "Tried to get value for non-existent setting: " + algorithmSettingEnum.getName());
    return value;
  }

  /**
   * This function will only call if this subtype is imported from library,
   * it tell user the subtype had changed after imported
   *
   * @return boolean, true = imported, false = modified.
   *
   * @author Wei Chin, Chong
   */
  public boolean isModifiedAfterImported()
  {
    Assert.expect(isImportedPackageLibrary());

    boolean isModified = true;

    List <AlgorithmSettingEnum> importedAlgoSetting = getImportedAlgorithmSettingEnums();
    List <AlgorithmSettingEnum> tunedAlgoSetting = getTunedAlgorithmSettingEnums();

    if(tunedAlgoSetting.size() == 0 && importedAlgoSetting.size() == 0)
    {
      isModified = false;
    }
    else if(tunedAlgoSetting.size() != importedAlgoSetting.size() )
    {
      return isModified;
    }
    else if(tunedAlgoSetting.containsAll(importedAlgoSetting) &&
       importedAlgoSetting.containsAll(tunedAlgoSetting))
    {
      for (AlgorithmSettingEnum algorithmSetting : importedAlgoSetting)
      {
        isModified = (isImportedValue(algorithmSetting, getAlgorithmSettingValue(algorithmSetting)) == false);

        if (isModified == true)
        {
          return isModified;
        }
      }
    }
    else
    {
      return isModified;
    }
    return isModified;
  }

  /**
   * @param algorithmSettingEnum AlgorithmSettingEnum
   * @param value Serializable
   *
   * @author Wei Chin, Chong
   */
  public void addAlgorithmsSettingEnumToImportedValueMap(AlgorithmSettingEnum algorithmSettingEnum, Serializable value)
  {
    Assert.expect(algorithmSettingEnum != null);
    Assert.expect(value != null);

    _algorithmSettingEnumToImportedValueMap.put(algorithmSettingEnum, value);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void setTunedValueToImportedValue()
  {
    Assert.expect(_algorithmSettingEnumToImportedValueMap != null);
    Assert.expect(_algorithmSettingEnumToTunedValueMap != null);

    _algorithmSettingEnumToImportedValueMap.putAll(_algorithmSettingEnumToTunedValueMap);
  }

  /**
   * @param pads List
   * @author Cheah Lee Herng
   */
  public void setBackupPads(List<Pad> pads)
  {
    Assert.expect(pads != null);

    if (_backupPads == null)
      _backupPads = new ArrayList<Pad>();
    else
      _backupPads.clear();

    for (Pad pad : pads)
    {
      _backupPads.add(pad);
    }
    Assert.expect(_backupPads != null);
  }

  /**
   * @return List
   * @author Cheah Lee Herng
   */
  public List<Pad> getBackupPads()
  {
    Assert.expect(_backupPads != null);
    return _backupPads;
  }

  /**
   * @return List
   * @author Cheah Lee Herng
   */
  public List<PadType> getPadTypes()
  {
    Assert.expect(_padTypeSet != null);
    return new ArrayList<PadType>(_padTypeSet);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void assignPadsFromBackupPads()
  {
    Assert.expect(_backupPads != null);

    if (_pads == null)
      _pads = new ArrayList<Pad>();

    for(Pad pad : _backupPads)
    {
      _pads.add(pad);
    }
  }

  /**
   * @param padType PadType
   * @return boolean
   *
   * @author Cheah Lee Herng
   */
  public boolean isPadTypeExist(PadType padType)
  {
    Assert.expect(padType != null);
    Assert.expect(_padTypeSet != null);

    if (_padTypeSet.contains(padType))
      return true;
    else
      return false;
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean isDependentAlgorithmSettingEnumConditionMatch(Object currentValue,
                                                               AlgorithmSettingEnum dependentAlgorithmSettingEnum,
                                                               AlgorithmSettingComparatorEnum algorithmSettingComparatorEnum)
  {
    Assert.expect(currentValue != null);
    Assert.expect(dependentAlgorithmSettingEnum != null);
    Assert.expect(algorithmSettingComparatorEnum != null);

    Serializable dependentValue = getAlgorithmSettingValue(dependentAlgorithmSettingEnum);
    if (currentValue instanceof String || dependentValue instanceof String)
    {
        // Do nothing
    }
    else if ((currentValue instanceof Integer && dependentValue instanceof Integer) ||
             (currentValue instanceof Integer && dependentValue instanceof Float) ||
             (currentValue instanceof Float && dependentValue instanceof Integer) ||
             (currentValue instanceof Float && dependentValue instanceof Float))
    {
        Float algorithmSettingValue = new Float(currentValue.toString());
        Float dependentAlgorithmSettingValue = (Float)dependentValue;

        if (algorithmSettingComparatorEnum == AlgorithmSettingComparatorEnum.GREATER)
            return (algorithmSettingValue.floatValue() > dependentAlgorithmSettingValue.floatValue());
        else if (algorithmSettingComparatorEnum == AlgorithmSettingComparatorEnum.GREATER_OR_EQUAL)
            return (algorithmSettingValue.floatValue() >= dependentAlgorithmSettingValue.floatValue());
        else if (algorithmSettingComparatorEnum == AlgorithmSettingComparatorEnum.LESS_THAN)
            return (algorithmSettingValue.floatValue() < dependentAlgorithmSettingValue.floatValue());
        else if (algorithmSettingComparatorEnum == AlgorithmSettingComparatorEnum.LESS_THAN_OR_EQUAL)
            return (algorithmSettingValue.floatValue() <= dependentAlgorithmSettingValue.floatValue());
        else if (algorithmSettingComparatorEnum == AlgorithmSettingComparatorEnum.EQUAL)
            return (algorithmSettingValue.floatValue() == dependentAlgorithmSettingValue.floatValue());
        else
            Assert.expect(false, "Unknown AlgorithmSettingComparator type.");
    }
    else
        Assert.expect(false);
    return true;
  }

  /**
   * @return the _subtypeAdvanceSettings
   * @author Wei Chin
   */
  public SubtypeAdvanceSettings getSubtypeAdvanceSettings()
  {
    if (_subtypeAdvanceSettings == null)
    {
      _subtypeAdvanceSettings = SubtypeAdvanceSettings.createNewSubtypeAdvanceSettings(this);
    }

    return _subtypeAdvanceSettings;
  }

  /**
   * @param subtypeAdvanceSettings the _subtypeAdvanceSettings to set
   * @author Wei Chin
   */
  public void setSubtypeAdvanceSettings(SubtypeAdvanceSettings subtypeAdvanceSettings)
  {
    Assert.expect(subtypeAdvanceSettings != null);
    _subtypeAdvanceSettings = subtypeAdvanceSettings;
  }
  
  /**
   * @author Wei Chin
   */
  static public void copyThreshold(Subtype sourceSubtype, Subtype targetSubtype, AlgorithmSetting algorithmSetting)
  {
    Assert.expect(sourceSubtype != null);
    Assert.expect(targetSubtype != null);

    Assert.expect(algorithmSetting != null);
    Serializable sourceValue = sourceSubtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum());
    Serializable targetValue = targetSubtype.getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum());
    // only change the value if it different
    if (sourceValue.equals(targetValue) == false)
      targetSubtype.setTunedValue(algorithmSetting.getAlgorithmSettingEnum(), sourceValue);
  }

  /**
   * @return the _landPattern
   * @author Wei Chin
   */
  public LandPattern getLandPattern()
  {
    if(_landPattern == null)
    {
      // handle previous missing landPattern case
      _landPattern = _padTypeSet.iterator().next().getLandPatternPad().getLandPattern();
    }
    return _landPattern;
  }

  /**
   * @param landPattern the _landPattern to set
   * @author Wei Chin
   */
  public void setLandPattern(LandPattern landPattern)
  {
    this._landPattern = landPattern;
  }
  
  /**
   * @author Chnee Khang Wah, 2013-09-11, user defined wavelet level
   */
  public int getUserDefinedInitialWaveletLevel()
  {
    Serializable value = getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL);
    Assert.expect(value instanceof String);
    if (value.equals("auto"))
      return 0;
    else if (value.equals("fast"))
      return 4;
    else if (value.equals("medium"))
      return 3;
    else if (value.equals("slow"))
      return 2;
    else
      Assert.expect(false, "Illegal value for search speed");
    return 0;
  }
  
  /**
   * @author Siew Yeng
   */
  public List<ComponentType> getComponentTypes()
  {
    Assert.expect(_padTypeSet != null);
    List<ComponentType> compTypes = new ArrayList();
    
    for(PadType padType : _padTypeSet)
    {
      if(compTypes.contains(padType.getComponentType()) == false)
        compTypes.add(padType.getComponentType());
    }
    return compTypes;
  }
  
  /**
   * @author Siew Yeng
   */
  public void setEnabledLearnSlopeSettings(boolean learnSlopeSettings)
  {
    _learnSlopeSettings = learnSlopeSettings;
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean isEnabledSlopeSettingsLearning()
  {
    return _learnSlopeSettings;
  }
  
  /**
   * @author Siew Yeng
   */
  public void addSubSubtype(Subtype subtype)
  {    
    Assert.expect(_isSubSubtype == false);
    _projectObservable.setEnabled(false);
    try
    {
      _subSubtypes.add(subtype);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, subtype, SubtypeEventEnum.ADD_OR_REMOVE_SUB_SUBTYPE);
  }
  
  /**
   * @author Siew Yeng
   */
  public void removeSubSubtype(Subtype subtype)
  {  
    Assert.expect(_isSubSubtype == false);
    
    _projectObservable.setEnabled(false);
    try
    {
      _subSubtypes.remove(subtype);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, subtype, null, SubtypeEventEnum.ADD_OR_REMOVE_SUB_SUBTYPE);
  }
  
  /**
   * @author Siew Yeng
   */
  public List<Subtype> getSubSubtypes()
  {    
    Assert.expect(_isSubSubtype == false);
    return _subSubtypes;
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean hasSubSubtypes()
  {    
    if(_isSubSubtype)
      return false;
    
    return _subSubtypes.size() > 0;
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean isSubSubtype()
  {
    return _isSubSubtype;
  }
  
  /**
   * author Siew Yeng
   */
  public void setIsSubSubtype(boolean isSubSubtype)
  {
    _isSubSubtype = isSubSubtype;
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean doesSubSubtypeExist(String subtypeLongName)
  {
    for(Subtype sub : _subSubtypes)
    {
      if(sub.getLongName().equals(subtypeLongName))
        return true;
    }
    return false;
  }
    
  /**
   * XCR-2894
   * Resets the subtype algorithm thresholds to selected initial threshold values.
   * 
   * @author Jen Ping Chong
   */
  public void resetToInitialThreshold()
  {
    InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(_jointTypeEnum);
    for (Algorithm algorithm : inspectionFamily.getAlgorithms())
    {
      for (AlgorithmSetting algorithmSetting : algorithm.getAlgorithmSettings(_jointTypeEnum))
      {
        // Do nothing if that algorithm settings does not exist
        if (doesAlgorithmSettingExist(algorithmSetting.getAlgorithmSettingEnum()) == false)
          return;
        
        Serializable initialValue = _projectInitialThresholds.getAlgorithmSettingValue(_jointTypeEnum, algorithm, algorithmSetting);
        Serializable currentValue = getAlgorithmSettingValue(algorithmSetting.getAlgorithmSettingEnum());
        // Optimization: I found that setTunedValue() is a rather costly operation
        //               Therefore, we update the threshold value only if it is
        //               different from the initial threshold.
        if (currentValue.equals(initialValue) == false)
          setTunedValue(algorithmSetting.getAlgorithmSettingEnum(), initialValue);
      }
    }
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean checkIfMaskImageExist()
  {
    //Siew Yeng - XCR-2843 - masking rotation
    List<String> maskImagePathList = FileUtil.listFiles(Directory.getAlgorithmLearningDir(getPanel().getProject().getName()), 
                                                        FileName.getMaskImagePatternString(getShortName()));
    
    boolean maskImageExist = false;
     
    if (maskImagePathList.isEmpty() == false)
      maskImageExist = true;
    return maskImageExist;
  }
}
