package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import java.util.*;

/**
 * @author Sheng-chuan
 * used for template learning
 */
public class ExpectedImageTemplateLearning
{
  private Pad _pad;
  private SliceNameEnum _sliceNameEnum;
  private int _numberOfExpectedImagesLearned = 0;
  private String _imageName;
  private Image _image;
  private Component _component;
  private String _componentName;
  private Board _board;

  /**
   * @author George Booth
   */
  public ExpectedImageTemplateLearning()
  {
    //do nothing
  }

  /**
   * @author George Booth
   */
  public void setPad(Pad pad)
  {
    Assert.expect(pad != null);
    _pad = pad;
  }

  /**
   * @author George Booth
   */
  public Pad getPad()
  {
    Assert.expect(_pad != null);
    return _pad;
  }

  /**
   * @author George Booth
   */
  public void setSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    _sliceNameEnum = sliceNameEnum;
  }

  /**
   * @author George Booth
   */
  public SliceNameEnum getSliceNameEnum()
  {
    Assert.expect(_sliceNameEnum != null);
    return _sliceNameEnum;
  }

  /**
   * @author George Booth
   */
  public void setNumberOfExpectedImagesLearned(int numberOfExpectedImagesLearned)
  {
    Assert.expect(numberOfExpectedImagesLearned > 0);
    _numberOfExpectedImagesLearned = numberOfExpectedImagesLearned;
  }

  /**
   * @author George Booth
   */
  public int getNumberOfExpectedImagesLearned()
  {
    return _numberOfExpectedImagesLearned;
  }

  /**
   * @author sheng chuan
   */
  public void setImageName(String imageName)
  {
    Assert.expect(imageName != null);
    _imageName = imageName;
  }
  
  /**
   * @author sheng chuan
   */
  public String getImageName()
  {
    Assert.expect(_imageName != null);
    return _imageName;
  }

  /**
   * Adds the specified expected image to our composite expected image.
   *
   * @author George Booth
   */
  public void setLearnedExpectedImage(Image image)
  {
    Assert.expect(image != null);

    _image = image;
  }
  
  /**
   * @author sheng chuan
   */
  public Image getLearnedExpectedImage()
  {
    Assert.expect(_image != null);
    return _image;
  }
  
  /**
   * @author sheng chuan
   */
  public void setLearnedComponentTemplate(Component component)
  {
    Assert.expect(component != null);
    _component = component;
  }
  
  /**
   * @author sheng chuan
   */
  public Component getLearnedComponentTemplate()
  {
    Assert.expect(_component != null);
    return _component;
  }
  
  /**
   * @author sheng chuan
   */
  public void setLearnedComponentSlice(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    _sliceNameEnum = sliceNameEnum;
  }
  
  /**
   * @author sheng chuan
   */
  public SliceNameEnum getLearnedComponentSlice()
  {
    Assert.expect(_sliceNameEnum != null);
    return _sliceNameEnum;
  }
  
  /**
   * @author sheng chuan
   */
  public String getLearnedComponentName()
  {
    Assert.expect(_componentName != null);
    return _componentName;
  }
  
  /**
   * @author sheng chuan
   */
  public void setLearnedComponentName(String componentName)
  {
    Assert.expect(componentName != null);
    _componentName = componentName;
  }
  
  /**
   * @author sheng chuan
   */
  public Board getBoardName()
  {
    Assert.expect(_board != null);
    return _board;
  }
  
  /**
   * @author sheng chuan
   */
  public void setBoardName(Board board)
  {
    Assert.expect(board != null);
    _board = board;
  }
}
