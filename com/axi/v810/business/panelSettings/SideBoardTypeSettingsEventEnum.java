package com.axi.v810.business.panelSettings;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class SideBoardTypeSettingsEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static SideBoardTypeSettingsEventEnum CREATE_OR_DESTROY = new SideBoardTypeSettingsEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);

  /**
   * @author Bill Darbie
   */
  private SideBoardTypeSettingsEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private SideBoardTypeSettingsEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
