package com.axi.v810.business;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import ndfconverter.*;


/**
 *
 * This Class is to used to call the external jar ndfConverter to do the whole
 * convert process;
 *
 * @author weng-jian.eoh
 */
public class CadConverter
{

  private static CadConverter _cadConverter = null;

  private String _destinationPath;
  private String _cadSystem;
  private String _cadFilePath;

  /**
   *
   * @author weng-jian.eoh
   */
  private CadConverter()
  {
    //do nothing
  }

  public void setCadSystem(String cadSystem)
  {
    _cadSystem = cadSystem;
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public static synchronized CadConverter getInstance()
  {
    if (_cadConverter == null)
    {
      _cadConverter = new CadConverter();
    }
    return _cadConverter;
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public void convert(String cadFilePath, String destinationPath) throws FileNotFoundDatastoreException, BusinessException
  {
    Assert.expect(cadFilePath != null);
    Assert.expect(destinationPath != null);

    _cadFilePath = cadFilePath;
    _destinationPath = destinationPath;

    convert();
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  private void convert() throws FileNotFoundDatastoreException, BusinessException
  {
    boolean ndfConverterJarFileExist = FileUtilAxi.exists(FileName.getNdfConverterFilePath());
    if (ndfConverterJarFileExist == false)
    {
      throw new FileNotFoundDatastoreException(FileName.getNdfConverterFilePath());
    }

    try
    {
      NDFConverter ndfConvert = NDFConverter.getAnInstance(CadFormatEnum.getCadFormatEnumByName(_cadSystem));
      ndfConvert.toNDF(_cadFilePath, _destinationPath);
    }
    catch (NullPointerException e)
    {
      throw new UnknownCadSystemFoundBusinessException(_cadSystem);    
    }
    catch (NDFException e)
    {
      throw new UnableConvertCadBusinessException(_cadFilePath, e.getMessage());
    }
  }

  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public void reset()
  {
    _cadSystem = null;
    _destinationPath = null;
    _cadFilePath = null;

  }
}
