package com.axi.v810.business;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author siew-yeng.phang
 */
public class SerialNumberMappingException extends XrayTesterException
{
  private SerialNumberMappingExceptionEnum _exceptionType;
  
  /**
   * @author Phang Siew Yeng
   */
  private SerialNumberMappingException(SerialNumberMappingExceptionEnum exceptionEnum, LocalizedString message)
  {
    super(message);
    Assert.expect(message != null);
    
    _exceptionType = exceptionEnum;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  private SerialNumberMappingException(SerialNumberMappingExceptionEnum exceptionEnum, String key, Object[] parameters)
  {
    this(exceptionEnum,new LocalizedString(key, parameters));

    Assert.expect(key != null, "key string is null.");

  }
  
  /**
   * @author Phang Siew Yeng
   */
  static SerialNumberMappingException getBarcodeConfigForRecipeAndConfigUsedInProductionNotSameException(Object[] parameters)
  {
    return new SerialNumberMappingException(SerialNumberMappingExceptionEnum.BARCODE_CONFIG_FOR_RECIPE_AND_CONFIG_USED_IN_PRODUCTION_NOT_SAME,
                                            "GUI_SERIAL_NUMBER_MAPPING_BARCODE_CONFIG_NOT_TALLY_WITH_BARCODE_CONFIG_USE_IN_PRODUCTION_KEY",
                                            parameters);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  static SerialNumberMappingException getSerialNumberMappingSettingNotTallyWithBarcodeReaderSetupException(Object[] parameters)
  {
    return new SerialNumberMappingException(SerialNumberMappingExceptionEnum.SERIAL_NUMBER_MAPPING_SETTING_NOT_TALLY_WITH_BARCODE_READER_SETUP,
                                            "GUI_SERIAL_NUMBER_MAPPING_SETTING_NOT_TALLY_WITH_BARCODE_READER_SETUP_KEY",
                                            parameters);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  static SerialNumberMappingException getSerialNumberMappingSettingInvalidException()
  {
    return new SerialNumberMappingException(SerialNumberMappingExceptionEnum.SERIAL_NUMBER_MAPPING_SETTING_INVALID,
                                            "GUI_SERIAL_NUMBER_MAPPING_IS_INVALID_KEY",
                                            null);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public SerialNumberMappingExceptionEnum getExceptionType()
  {
    return _exceptionType;
  }
}
