package com.axi.v810.business;

import com.axi.util.*;

/**
 * This exception is thrown when email sender throws an IOException
 *
 * @author Andy Mechtenberg
 */
public class CannotSendEmailBusinessException extends BusinessException
{
  /**
   * @author Andy Mechtenberg
   */
  public CannotSendEmailBusinessException(String errorMessage)
  {
    super(new LocalizedString("BS_UNABLE_TO_SEND_EMAIL_EXCEPTION_KEY",
                              new Object[]{errorMessage}));
    Assert.expect(errorMessage != null);
  }
}
