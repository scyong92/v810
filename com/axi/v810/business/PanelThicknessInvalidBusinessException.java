package com.axi.v810.business;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class PanelThicknessInvalidBusinessException extends BusinessException
{
  /**
   * @author Bill Darbie
   */
  public PanelThicknessInvalidBusinessException(int thicknessInNanoMeters)
  {
    super(new LocalizedString("BUS_PANEL_THICKNESS_INVALID_EXCEPTION_KEY", new Object[]{thicknessInNanoMeters}));
  }
}
