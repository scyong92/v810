package com.axi.v810.business.server;

import java.rmi.*;

import com.axi.v810.hardware.*;

/**
* This is the interface that allows all clients to get access to
* any remote interfaces they need from the 5dx server.
*
* @author Bill Darbie
*/
public interface RemoteServerInt extends Remote
{
  /**
  * Shut down the all the software
  * @author Bill Darbie
  */
  public void softwareShutDown() throws RemoteException, HardwareException;

  /**
   * Determine if the server is ready for use
   * @author George A. David
   */
  public boolean isServerInitialized() throws RemoteException;
}
