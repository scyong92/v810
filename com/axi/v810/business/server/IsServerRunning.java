package com.axi.v810.business.server;

import java.rmi.*;
import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;

/**
* When the main method of this class is run it determines if the Java server
* com.axi.v810.business.Server is running.  A return value of 0 indicates
* that is is running, a -1 indicates that it is not running
*
* @author Bill Darbie
*/
public class IsServerRunning
{
  public static void main(String[] args)
  {
    try
    {
      Assert.setLogFile("internalErrors", Version.getVersion());
      Config config = Config.getInstance();
      config.loadIfNecessary();

      // use rmi - do a lookup in rmiregistry for the server object
      //System.out.println("Server: attempting to connect to server object " + sbi.getBindString());
      String serverName = "localhost";
      RemoteServerInt server = (RemoteServerInt)Naming.lookup(ServerBindInfo.getBindString(serverName));
      //System.out.println("Server: successful connection to server.");
      // now make sure the server is initialized
      if (server.isServerInitialized())
      {
        // connection established so return a 0 to indicate this
        System.out.println("running");
        //System.exit(0);
      }
      else
      {
        // connection established, but server is not initialized
        System.out.println("notRunning");
        //System.exit(-1);
      }
    }
    catch(ConnectException ex)
    {
      System.out.println("notRunning");
      //System.exit(-1);
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
      //System.exit(-1);
    }
  }
}
