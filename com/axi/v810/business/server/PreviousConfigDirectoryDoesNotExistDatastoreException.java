package com.axi.v810.business.server;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Bill Darbie
 */
public class PreviousConfigDirectoryDoesNotExistDatastoreException extends DatastoreException
{

  /**
   * @author Bill Darbie
   */
  public PreviousConfigDirectoryDoesNotExistDatastoreException(String prevConfigDir)
  {
    super(new LocalizedString("DS_PREVIOUS_CONFIG_DIRECTORY_DOES_NOT_EXIST_KEY", new Object[]{prevConfigDir}));
    Assert.expect(prevConfigDir != null);
  }
}
