package com.axi.v810.business.server;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.license.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;

/**
 * This class allows clients to connect through a socket and send commands to
 * the Java server that controls the 5dx.
 *
 * @author Bill Darbie
 */
class MessageSocketServer implements SocketServerMessageHandlerInt, Runnable
{
  private static MessageSocketServer _instance;
  private static final String _ERROR = "ERROR: ";
  private static final String _BAD_FORMAT_ERROR = _ERROR + "The command supplied is not in the correct format. You must seperate each token with a space. ";
  private SocketServer _socketServer;
  private Server _server;
  private String _message = "OK";
  private Config _config;

  /**
   * @author Bill Darbie
   */
  static synchronized MessageSocketServer getInstance() throws DatastoreException, HardwareException
  {
    if (_instance == null)
      _instance = new MessageSocketServer();

    return _instance;
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  private MessageSocketServer()
  {
    _config = Config.getInstance();
  }

  /**
   * Connect to the socket
   * @author Bill Darbie
   */
  void connect() throws XrayTesterException
  {
    // only connect once
    if (_socketServer == null)
    {
      int port = _config.getIntValue(SoftwareConfigEnum.MESSAGE_SOCKET_PORT_NUMBER);
      try
      {
        if (UnitTest.unitTesting() == false)
        {
          _socketServer = new SocketServer(port, this);
        }
        else
        {
          port = 0;
          // during unit testing use port # 0, which means use any free port
          _socketServer = new SocketServer(port, this);
        }
      }
      catch(IOException ioe)
      {
        SocketConnectionFailedHardwareException sce = new SocketConnectionFailedHardwareException("localhost", port);
        sce.initCause(ioe);

        throw sce;
      }

      _server = Server.getInstance();
      Assert.expect(_server != null);
    }
  }

  /**
   * @author Bill Darbie
   */
  public void run()
  {
    try
    {
      connect();
      _socketServer.acceptConnectionAndProcessMessage();
    }
    catch(Throwable throwable)
    {
      Assert.logException(throwable);
    }
  }

  /**
   * @author Bill Darbie
   */
  public synchronized List<String> processMessage(String clientMessage)
  {
    _message = "OK";
    List<String> reply = new ArrayList<String>();

    if (clientMessage.equals("disconnect"))
    {
      _socketServer.disconnectFromClient();
    }
    else if (clientMessage.equals("shutdown"))
    {
      _server.softwareShutDown();
    }
    else if (clientMessage.startsWith("validateSoftwareLicense"))
    {
      try
      {
        LicenseManager.checkThatValidLicenseExists();
        _message = "true";
      }
      catch(XrayTesterException xte)
      {
        _message = "false";
      }
    }
    else
    {
      _message = _ERROR + " The 5dx Java message server did not recognize the message: " + clientMessage;
    }
    reply.add(_message);

    return reply;
  }
}
