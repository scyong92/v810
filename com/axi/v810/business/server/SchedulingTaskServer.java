package com.axi.v810.business.server;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author chin-seong.kee
 */
public class SchedulingTaskServer {

    class SchedulerTimerTask  extends TimerTask {
      
        private SchedulerTask schedulerTask;
        private ScheduleIterator iterator;
        
        public SchedulerTimerTask(SchedulerTask schedulerTask,
                ScheduleIterator iterator) 
        {
            this.schedulerTask = schedulerTask;
            this.iterator = iterator;
        }
        public void run() 
        {
            schedulerTask.run();
            reschedule(schedulerTask, iterator);
        }
    }

    private final Timer timer = new Timer();
    
    //XCR-2783 Allow automatic startup feature to work on any v810 software environment page XCR-3183
    private SchedulerTask _schedulerTask = null;

    public SchedulingTaskServer() 
    {
    }

    public void cancel() {
        timer.cancel();
    }

    /**
     * @author Kee Chin Seong 
     * XCR-2783 Allow automatic startup feature to work on any v810 software environment page XCR-3183
     * @param schedulerTask
     * @param iterator 
     */
    public void schedule(SchedulerTask schedulerTask,
            ScheduleIterator iterator) {

        if(_schedulerTask == null)
            _schedulerTask = schedulerTask;
        else
        {
            _schedulerTask.cancel();
            _schedulerTask = schedulerTask;
        }
        Date time = iterator.next();
        if (time == null) {
            schedulerTask.cancel();
        } 
        else 
        {
            synchronized(schedulerTask.lock) 
            {
                if (schedulerTask.state != SchedulerTask.VIRGIN) 
                {
                  throw new IllegalStateException("Task already scheduled" + "or cancelled");
                }
                schedulerTask.state = SchedulerTask.SCHEDULED;
                schedulerTask.timerTask =
                    new SchedulerTimerTask(schedulerTask, iterator);
                timer.schedule(schedulerTask.timerTask, time);
            }
        }
    }

    private void reschedule(SchedulerTask schedulerTask,
            ScheduleIterator iterator) {

        Date time = iterator.next();
        if (time == null) {
            schedulerTask.cancel();
        } else {
            synchronized(schedulerTask.lock) {
                if (schedulerTask.state != SchedulerTask.CANCELLED) {
                    schedulerTask.timerTask =
                        new SchedulerTimerTask(schedulerTask, iterator);
                    timer.schedule(schedulerTask.timerTask, time);
                }
            }
        }
    }
  
}
