package com.axi.v810.business.server;

import java.io.*;
import java.net.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import com.axi.v810.business.testExec.*;
/**
* All GUI's must connect to this server to get a reference to any interfaces that they use
* to access server functionality.
*
* @author Bill Darbie
*/
public class Server
{
  private static Server _instance;

  public static final String SERVER_SHUTDOWN_MESSAGE = "shutdown";

  private static boolean _configurationLoaded = false;
  private static Config _config;
  private static OverrideConfigValues _overrideConfigValues;

  private Runtime _runtime;
  private MessageSocketServer _messageSocketServer;
  private RemoteServer _remoteServer;
  private ConvertLocalizationToAscii _convertLocalizationToAscii;
  private AutoShortTermShutdownXrayProgressMonitor _autoShortTermShutdownXrayProgressMonitor;

  // this variable is set by MainMenuGui since it starts and initializes the server
  // it will be used by CxObjMgr to determine if the server is ready for use.
  private boolean _isServerInitialized = false;

  /**
   * @author Bill Darbie
   */
  static
  {
    _overrideConfigValues = OverrideConfigValues.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public static synchronized Server getInstance() throws XrayTesterException
  {
    if (_instance == null)
      _instance = new Server();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private Server() throws XrayTesterException
  {
    _config = Config.getInstance();

    // convert localization files over before reading them in
    _convertLocalizationToAscii = ConvertLocalizationToAscii.getInstance();
    _convertLocalizationToAscii.convert();

    // Check the config to see what kind of system it thinks is running.
    getConfiguration();

    // Make sure the license file is in agreement with the config file.
    syncConfigWithLicenseFile();

    // Initialize test programs that Adjustments and Confirmations use.  Do this now, before user's project
    // is loaded, to avoid state changes to user's project.
    ImageRetriever.getInstance().initTestPrograms();

    _runtime = Runtime.getRuntime();
    _messageSocketServer = MessageSocketServer.getInstance();
    _autoShortTermShutdownXrayProgressMonitor = AutoShortTermShutdownXrayProgressMonitor.getInstance();

  }

  /**
   * @author Matt Wharton
   * @author Eric McDermid
   */
  private void syncConfigWithLicenseFile() throws XrayTesterException
  {
    // Check the license to see what kind of a system we are running.
    boolean isLicenseX6000 = LicenseManager.isV810();
    boolean isLicenseTDW = LicenseManager.isTdw();

    // If the license is both X6000 AND TDW, we assume it's a developer license and we don't mess with the config
    // file.
    if (isLicenseX6000 && isLicenseTDW)
    {
      return;
    }

    boolean isConfigTDW = (_config.getBooleanValue(SoftwareConfigEnum.ONLINE_WORKSTATION) == false);

    // If the license and the config file disagree, the license setting
    // wins, and we need to update the config to match.
    if (isLicenseTDW != isConfigTDW)
    {
      if (isLicenseTDW)
      {
        // For TDW, onlineWorkstation = false
        _config.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, false);
        // do not set hardwareSimulation to true for TDW mode - wpd
        // _config.setValue(SoftwareConfigEnum.HARDWARE_SIMULATION, true);

        // additionally, the hardware serial number is "TDW"
        _config.setValue(HardwareConfigEnum.XRAYTESTER_SERIAL_NUMBER, "TDW");
      }
      else
      {
        // For online, onlineWorkstation = true
        _config.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, true);
      }

      // save
      _config.save(SoftwareConfigEnum.ONLINE_WORKSTATION.getFileName());
    }
  }

  /**
   * @author Bill Darbie
   * @author Tan Hock Zoon
   */
  public static void finalizeInstallSettingsIfNecessary() throws XrayTesterException
  {
    // NOTE that you cannot use config files in this method since this method will likely need
    // to modify them first before they are loaded!!

    // If this is the first startup, we have some housekeeping to do
    // before creating the server.
    String firstRunAfterInstallFile = FileName.getFirstRunAfterInstallFullPath();
    if (FileUtilAxi.exists(firstRunAfterInstallFile) == false)
    {
      //set dont copy list to exclude files copy from config.prev to config
      FileName.setDontCopyList();

      // copy any previous license directory over
      copyPrevConfigDirContentsToCurrentConfigDir(Directory.getPrevLicenseDir(), Directory.getLicenseDir());

      // copy any previous user accounts directory over
      copyPrevConfigDirContentsToCurrentConfigDir(Directory.getPrevUserAccountsDir(), Directory.getUserAccountsDir());

      // copy any barcode reader config files
      copyPrevConfigDirContentsToCurrentConfigDir(Directory.getPrevBarcodeReaderDir(), Directory.getBarcodeReaderDir());

      // copy any gui config files
      copyPrevConfigDirContentsToCurrentConfigDir(Directory.getPrevGuiConfigDir(), Directory.getGuiConfigDir());

      // copy any images
      copyPrevConfigDirContentsToCurrentConfigDir(Directory.getPrevGuiImagesDir(), Directory.getGuiImagesDir());

      // copy initialThresholds
      copyPrevConfigDirContentsToCurrentConfigDir(Directory.getPrevInitialThresholdsDir(), Directory.getDefaultInitialThresholdsDir());

      // copy jointTypeAssignment
      copyPrevConfigDirContentsToCurrentConfigDir(Directory.getPrevJointTypeAssignmentDir(), Directory.getJointTypeAssignmentDir());

      // copy serial numbers
      copyPrevConfigDirContentsToCurrentConfigDir(Directory.getPrevSerialNumberDir(), Directory.getSerialNumberDir());

      // copy any inspectionData
      copyPrevConfigDirContentsToCurrentConfigDir(Directory.getPrevInspectionDataDir(), Directory.getInspectionDataDir());

      // copy results processing config file
      String prevResultsProcessingConfigFile = FileName.getPreviousConfigDirectoryResultsProcessingConfigFullPath();
      if (FileUtilAxi.exists(prevResultsProcessingConfigFile))
      {
        String resultsProcessingConfigFile = FileName.getResultsProcessingConfigFullPath();
        FileUtilAxi.copy(prevResultsProcessingConfigFile, resultsProcessingConfigFile);
      }

      // copy production tune config file
      String prevProductionTuneConfigFile = FileName.getPrevProductionTuneConfigFullPath();
      if (FileUtilAxi.exists(prevProductionTuneConfigFile))
      {
        String productionTuneConfigFile = FileName.getProductionTuneConfigFullPath();
        FileUtilAxi.copy(prevProductionTuneConfigFile, productionTuneConfigFile);
      }

      // copy the hardware.override.config file
      String prevHardwareOverrideFile = FileName.getPrevHardwareConfigOverrideFullPath();
      String hardwareOverrideFile = FileName.getHardwareConfigOverrideFullPath();
      if (FileUtilAxi.exists(prevHardwareOverrideFile))
        FileUtilAxi.copy(prevHardwareOverrideFile, hardwareOverrideFile);

      // override hardware.config with settings in hardware.override.config
      // copy some config settings from previous install or uninstall to the new config files here
      if (FileUtilAxi.exists(hardwareOverrideFile))
        _overrideConfigValues.override(hardwareOverrideFile, FileName.getHardwareConfigFullPath());
      else
        throw new CannotOpenFileDatastoreException(hardwareOverrideFile);

      // carry forward any user settings from the previous software.config
      // this code will look at software.carryForward.config to see which software.config settings to carry
      // forward from the previous software.config file.
      String softwareConfigCarryForwardFile = FileName.getSoftwareConfigCarryForwardFullPath();
      String prevSoftwareConfigFile = FileName.getPrevSoftwareConfigFullPath();
      if (FileUtilAxi.exists(softwareConfigCarryForwardFile) && FileUtilAxi.exists(prevSoftwareConfigFile))
        _overrideConfigValues.carryForward(softwareConfigCarryForwardFile, prevSoftwareConfigFile, FileName.getSoftwareConfigFullPath());
      
      //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
      String falseCallTriggeringConfigCarryForwardFile = FileName.getFalseCallTriggeringConfigCarryForwardFullPath();
      String prevFalseCallTriggeringConfigFile = FileName.getPrevFalseCallTriggeringConfigFullPath();
      if (FileUtilAxi.exists(falseCallTriggeringConfigCarryForwardFile) && FileUtilAxi.exists(prevFalseCallTriggeringConfigFile))
        _overrideConfigValues.carryForward(falseCallTriggeringConfigCarryForwardFile, prevFalseCallTriggeringConfigFile, FileName.getFalseCallTriggeringConfigFullPath());

      // carry forward any user settings from the previous hardware.config
      // this code will look at hardware.carryForward.config to see which hardware.config settings to carry
      // forward from the previous hardware.config file.
      String hardwareConfigCarryForwardFile = FileName.getHardwareConfigCarryForwardFullPath();
      String prevHardwareConfigFile = FileName.getPrevHardwareConfigFullPath();
      if (FileUtilAxi.exists(hardwareConfigCarryForwardFile) && FileUtilAxi.exists(prevHardwareConfigFile))
        _overrideConfigValues.carryForward(hardwareConfigCarryForwardFile, prevHardwareConfigFile, FileName.getHardwareConfigFullPath());

      // Added by Jack Hwee - To copy distanceOffsetFromSystemNominalReferencePlaneToTopSurfaceOfBeltInNanometers value in current hardware.config
      // and override it into distanceOffsetFromSystemNominalReferencePlaneToTopSurfaceOfBeltInNanometersForFrontModule and 
      // copy its value into distanceOffsetFromSystemNominalReferencePlaneToTopSurfaceOfBeltInNanometersForRearModule.
      // Need to check version to make sure this process only occurs when upgrading 4.1 to 5.x version.
      if (FileUtilAxi.exists(prevHardwareConfigFile) && VersionUtil.isRequiredVersion("5.2"))
      {
        String oldHardwareConfigKeyString = HardwareConfigEnum.DISTANCE_OFFSET_FROM_SYSTEM_NOMINAL_REFERENCE_PLANE_TO_TOP_SURFACE_OF_BELT_IN_NANOMETERS.getKey();
        List<String> newHardwareConfigKeyStringList = new LinkedList<>();
        newHardwareConfigKeyStringList.add(HardwareConfigEnum.DISTANCE_OFFSET_FROM_SYSTEM_NOMINAL_REFERENCE_PLANE_TO_TOP_SURFACE_OF_BELT_IN_NANOMETERS_FOR_FRONT_MODULE.getKey());
        newHardwareConfigKeyStringList.add(HardwareConfigEnum.DISTANCE_OFFSET_FROM_SYSTEM_NOMINAL_REFERENCE_PLANE_TO_TOP_SURFACE_OF_BELT_IN_NANOMETERS_FOR_REAR_MODULE.getKey());
        
        _overrideConfigValues.overrideOldOffsetToNewOffset(prevHardwareConfigFile, FileName.getHardwareConfigFullPath(), oldHardwareConfigKeyString, newHardwareConfigKeyStringList);
      }
      
      // carry forward any user settings from the previous hardware.calib.binary 
      // this code will look at hardware.calib.carryForward to see which hardware.calib.binary settings to carry
      // forward from the previous hardware.calib.binary.
      String hardwareCalibCarryForwardFile = FileName.getHardwareCalibCarryForwardFullPath();
      String prevHardwareCalibFile = FileName.getPrevHardwareCalibFullPath();
      if (FileUtilAxi.exists(hardwareCalibCarryForwardFile) && FileUtilAxi.exists(prevHardwareCalibFile))
        _overrideConfigValues.carryForwardBinaryConfig(hardwareCalibCarryForwardFile, prevHardwareCalibFile, FileName.getHardwareCalibFullPath());
      
      // things I do NOT copy on purpose are:
      // digitalIO directory
      // motionControl directory
      // properties directory (the user can edit this, but a new release will have new or removed keys)
      // xRayCamera directory
      // xmlSchema directory



      // IMPORTANT - DO THIS LINE LAST - except for the config.prev check
      FileUtilAxi.createEmptyFile(firstRunAfterInstallFile);

      // check for config.prev setting
      String prevConfigDir = Directory.getPrevConfigDir();
      if (FileUtilAxi.exists(prevConfigDir) == false)
      {
        if (LicenseManager.isV810())
        {
          // we do not have a config.prev directory and we are on an v810 system
          throw new PreviousConfigDirectoryDoesNotExistDatastoreException(prevConfigDir);
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private static void copyPrevConfigDirContentsToCurrentConfigDir(String prevConfigDir, String currentConfigDir) throws DatastoreException
  {
    Assert.expect(prevConfigDir != null);
    Assert.expect(currentConfigDir != null);

    if (FileUtilAxi.exists(prevConfigDir))
    {
      if (FileUtilAxi.exists(currentConfigDir) == false)
        FileUtilAxi.mkdir(currentConfigDir);
      List<String> dontCopyList = FileName.getDontCopyList();
      
      // XCR-3345 Intermittent failed to open v810 application after patch installation
      if (dontCopyList.contains(FileName.getPrevGuiImagesThumbsDBFullPath()) == false)
        dontCopyList.add(FileName.getPrevGuiImagesThumbsDBFullPath());
      
      // XCR-3735 Failed to open v810 application after patch installation due to thumbs.db lock it
      if (dontCopyList.contains(FileName.getPrevJointThumbnailsThumbsDBFullPath()) == false)
        dontCopyList.add(FileName.getPrevJointThumbnailsThumbsDBFullPath());
      
      FileUtilAxi.copyDirectoryRecursively(prevConfigDir,currentConfigDir,dontCopyList);
    }
  }

  /**
   * @author Bill Darbie
   */
  public void startLegacyMessageSocketServer() throws XrayTesterException
  {
    _messageSocketServer.connect();
  }

  /**
   * Starts up the rmiregistry.
   * @author Bill Darbie
   */
  private void startRmiRegistry() throws BusinessException
  {
    // Set security manager to null, effectively turning it off.  This is done to avoid
    // a security conflict with the Comm API used by Board Tracking (CAM).
    // The security conflict will cause the following error:
    //
    // Caught java.lang.NullPointerException: name can't be null while loading driver com.sun.comm.Win32Driver
    //
    // This a problem with Java Communications API Version 2.0.  If Java ever releases
    // a fix for this problem, then we can change the line back to the original:
    //
    // System.setSecurityManager(new RMISecurityManager());  // set up an RMI security manager
    //
    System.setSecurityManager(null);

     try
     {
       int portNumber = _config.getIntValue(SoftwareConfigEnum.RMI_PORT_NUMBER);
       Registry registry = LocateRegistry.createRegistry(portNumber);
     }
     catch(RemoteException re)
     {
       throw new SoftwareAlreadyRunningBusinessException();
     }
  }

  /**
   * Tries to bind to the rmiregistry.  If the first attempt fails this method
   * will assume the failure is because the rmiregistry is not running.  It
   * will start up the rmiregistry and try to bind to it again.
   * @author Bill Darbie
   */
  private void bindJavaServerToRmiRegistry() throws BusinessException
  {
    startRmiRegistry();

    // get the server name
    String serverName = "localhost";
    String bindString = ServerBindInfo.getBindString(serverName);
    try
    {
      Naming.rebind(bindString, _remoteServer);
    }
    catch(MalformedURLException e)
    {
      Assert.logException(e);
    }
    catch(RemoteException e)
    {
      Assert.logException(e);
    }
  }

  /**
   * @author Bill Darbie
   */
  private boolean isAppRunning(String appName)
  {
    boolean running = false;
    try
    {
      Process process = _runtime.exec("isAppRunning \"" + appName + "\"");
      process.waitFor();
      if (process.exitValue() == 0)
        running = true;
    }
    catch(IOException e)
    {
      // do nothing
    }
    catch(InterruptedException ie)
    {
      // do nothing
    }

    return running;
  }

  /**
   * Start up this server so it is ready to go.
   * @author Bill Darbie
   */
  // this go() call and the setServersReady() call can come in on
  // different threads, so synchronize both calls
  public void go() throws RemoteException, XrayTesterException
  {
    _remoteServer = new RemoteServer(this);

    bindJavaServerToRmiRegistry();
    startJavaMessageServer();

    //Initialize the HardwareTaskEngine with the list of tasks from the
    //business layer
    initializeHardwareTaskEngine();
  }

  /**
   * @author Bill Darbie
   */
  private void startJavaMessageServer()
  {
    Thread messageSocketServerThread = new Thread(_messageSocketServer, "Java message socket server thread");
    messageSocketServerThread.start();
  }

  /**
   * @author Bill Darbie
   */
  private static void getConfiguration() throws DatastoreException
  {
    if (_configurationLoaded == false)
    {
      _configurationLoaded = true;

      // read in all configuration information
      _config.loadIfNecessary();

      NativeMemoryMonitor.initialize(_config.getLongValue(SoftwareConfigEnum.MAXIMUM_SPACE_FOR_IMAGES_IN_MEGABYTES)*1024*1024,
                                 _config.getLongValue(SoftwareConfigEnum.IMAGE_CACHE_TO_DISK_THRESHOLD_IN_MEGABYTES)*1024*1024);

      // load this config file so any DatastoreExceptions happen here instead of at the end of an inspection
      ResultsProcessor.getInstance().loadFromConfigurationFile();
    }
  }

  /**
   * @author Bill Darbie
   */
  void softwareShutDown()
  {
    // on another thread, wait a couple of seconds, then shut down this application
    // the wait allows this method to return from the original caller before things
    // are shut down
    WorkerThread workerThread = new WorkerThread("shutdown thread");
    workerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          Thread.sleep(2000);
        }
        catch (InterruptedException ex)
        {
          // do nothing
        }
        System.exit(0);
      }
    });
  }

  /**
   * Used by CxObjMgr to determine if the server is ready for use.
   * @author George A. David
   */
  public boolean isServerInitialized()
  {
    return _isServerInitialized;
  }

  /**
   * @author George A. David
   */
  public void setServerInitialized()
  {
    _isServerInitialized = true;
  }

    /**
   * In order to obey the architecture, the business layer list of HardwareTasks
   * needs to 'push' itself down to the hardware layer HardwareTaskEngine. Do this
   * at start-up by getting the list and calling the addListToTaskEngine method
   *
   * @author Reid Hayhow
   */
  private void initializeHardwareTaskEngine() throws DatastoreException
  {
    XrayHardwareAutomatedTaskList list = XrayHardwareAutomatedTaskList.getInstance();
    list.addListToTaskEngine();
  }

}
