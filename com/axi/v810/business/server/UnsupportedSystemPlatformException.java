package com.axi.v810.business.server;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * Display exception when V810 software runs on un-supported system platform.
 * @author Cheah Lee Herng
 */
public class UnsupportedSystemPlatformException extends BusinessException
{
  /**
   * @author Cheah Lee Herng 
   * @param softwareVersion Unsupported Software major version
   */
  public UnsupportedSystemPlatformException(String softwareVersion)
  {
    super(new LocalizedString("BUS_UNSUPPORTED_SYSTEM_PLATFORM_EXCEPTION_KEY", new Object[]{softwareVersion}));
  }
}
