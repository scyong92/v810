package com.axi.v810.business.server;

import java.io.*;

import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * This class provides a single instance of a DOS shell for use by the 5dx software.
 * Since most of the 5dx Pascal programs require that they are all run from the same
 * shell to work properly this class makes sure that only a single instance of the
 * Dos shell is used by all the software.
 *
 * @author Bill Darbie
 */
public class ServerDosShell implements DosOutputInt
{
  private static ServerDosShell _instance;
  private DosShell _dos;
  private DosShellOutputObservable _outObservable;
  private DosShellErrorObservable _errObservable;

  /**
   * @author Bill Darbie
   */
  private ServerDosShell()
  {
    _outObservable = DosShellOutputObservable.getInstance();
    _errObservable = DosShellErrorObservable.getInstance();

    try
    {
      _dos = new DosShell(this);
    }
    catch(IOException ioe)
    {
      Assert.logException(ioe);
    }
  }

  /**
   * @author Bill Darbie
   */
  public static synchronized ServerDosShell getInstance()
  {
    if (_instance == null)
      _instance = new ServerDosShell();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  public boolean sendCommand(String command)
  {
    return _dos.sendCommand(command);
  }

  /**
   * @author Bill Darbie
   */
  public boolean cd(String directory)
  {
    return _dos.cd(directory);
  }

  /**
   * @author Bill Darbie
   */
  public void hideWindow()
  {
    _dos.hideWindow();
  }

  /**
   * @author Bill Darbie
   */
  public void showWindow()
  {
    _dos.showWindow();
  }

  /**
   * This method will pass any standard output lines from here to any observers of this class.
   * @author Bill Darbie
   */
  public void standardOutputLine(String line)
  {
    Assert.expect(line != null);

    _outObservable.standardOutputLine(line);
  }

  /**
   * This method will pass any standard error lines from here to any observers of this class.
   * @author Bill Darbie
   */
  public void standardErrorLine(String line)
  {
    Assert.expect(line != null);

    _errObservable.standardErrorLine(line);
  }
}
