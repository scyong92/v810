package com.axi.v810.business.server;

import com.axi.util.*;
import java.util.*;

/**
 * This class allows Observers to get notified of any ServerDosShell standard error messages.
 *
 * @author Bill Darbie
 */
public class DosShellErrorObservable extends Observable
{
  private static DosShellErrorObservable _instance;

  /**
   * @author Bill Darbie
   */
  private DosShellErrorObservable()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  static synchronized DosShellErrorObservable getInstance()
  {
    if (_instance == null)
      _instance = new DosShellErrorObservable();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  void standardErrorLine(String line)
  {
    setChanged();
    notifyObservers(line);
  }
}
