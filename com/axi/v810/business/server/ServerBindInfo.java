package com.axi.v810.business.server;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Bill Darbie
 */
public class ServerBindInfo
{
  private static final String _OBJECT_NAME = "/com.axi.v810.business.server.Server";
  private static Config _config;

  /**
  * Return the bind string needed for an RMI Naming.lookup() call.  If you are running an
  * applet pass in the hostname of the applet as the parameter to this method.  If the parameter
  * is null the server found in the Config.properties will be used instead.
  *
  * @param hostName is the hostname of the server that the applet code was loaded from or null
  *                 if this in running as an application.
  * @author Bill Darbie
  */
  public static String getBindString(String hostName)
  {
    Assert.expect(hostName != null);
    if (_config == null)
      _config = Config.getInstance();

    int portNumber = _config.getIntValue(SoftwareConfigEnum.RMI_PORT_NUMBER);

    return "//" + hostName + ":" + portNumber + _OBJECT_NAME;
  }
}
