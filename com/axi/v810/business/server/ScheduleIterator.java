package com.axi.v810.business.server;

import java.util.Date;
/**
 *
 * @author chin-seong.kee
 */


public interface ScheduleIterator {
    public Date next();
}