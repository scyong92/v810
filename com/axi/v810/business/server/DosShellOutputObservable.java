package com.axi.v810.business.server;

import com.axi.util.*;
import java.util.*;

/**
 * This class allows Observers to get notified of any ServerDosShell standard output messages.
 *
 * @author Bill Darbie
 */
public class DosShellOutputObservable extends Observable
{
  private static DosShellOutputObservable _instance;

  /**
   * @author Bill Darbie
   */
  private DosShellOutputObservable()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  public static synchronized DosShellOutputObservable getInstance()
  {
    if (_instance == null)
      _instance = new DosShellOutputObservable();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  void standardOutputLine(String line)
  {
    Assert.expect(line != null);

    setChanged();
    notifyObservers(line);
  }
}
