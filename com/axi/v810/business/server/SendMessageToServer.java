package com.axi.v810.business.server;

import java.net.*;
import java.rmi.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;


/**
* This class provides a way to send messages to the java server.  The main of this class will
* return a 0 if the shutdown was successful and a 1 if an error occurred.
* Be careful when using this class.  It will connect remotely to whatever server is in
* you Config.properties file and send the message to it (including shutDown).
*
* @author Bill Darbie
*/
public class SendMessageToServer
{
  private static final String _SHUTDOWN = "shutdown";

  private static final String _PANEL_NAME_NEEDED = "You must specify the panel name when writing out ndfs.";
  private static final String _USAGE = "SendMessageToServer <message>\n" +
                                       "valid messages are: \n" +
                                       "  " + _SHUTDOWN + "\n";

  private static final String _COULD_NOT_CONNECT_ERROR = "ERROR: Could not connect to the 5DX server.\nPlease make sure that either the TDW or 5DX system software is running.";


  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    try
    {
      Assert.setLogFile("internalErrors", Version.getVersion());
      Config config = Config.getInstance();
      config.loadIfNecessary();

      if (args.length < 1)
      {
        System.out.println(_USAGE);
        System.exit(1);
      }
      String message = args[0];
      List<String> params = new ArrayList<String>();
      if (args.length >= 2)
      {
        int count = 0;
        while (count < args.length - 1)
        {
          params.add(count, args[count + 1]);
          count++;
        }
      }
      sendMessage(message, params);
    }
    catch (RuntimeException re)
    {
      Assert.logException(re);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      //System.exit(-1);
    }
  }

  /**
   * Do not allow constructor
   * @author Bill Darbie
   */
  private SendMessageToServer()
  {
    // do nothing
  }

  /**
  * Send the message passed in to this method to the Java server.  Use one of the public
  * final Strings as the passed in parameter.
  *
  * @param message - is one of the public static Strings provided by this class
  * @param params - is a list of the parameters that go along with the message.
  * @author Bill Darbie
  */
  public static void sendMessage(String message, List<String> params)
  {
    try
    {
      // use rmi - do a lookup in rmiregistry for the server object
      String serverName = "localhost";
      String bindString = ServerBindInfo.getBindString(serverName);
      RemoteServerInt server = (RemoteServerInt)Naming.lookup(bindString);

      //System.out.println("Server: successful connection to server.");
      // now send the message to the server
      if (message.equalsIgnoreCase(_SHUTDOWN))
        server.softwareShutDown();
      else
      {
        System.out.println(_USAGE);
        System.exit(1);
      }

      // connection established so return a 0 to indicate this
      System.out.println("Command was successful");
      System.exit(0);
    }
    catch(ServerError error)
    {
      // this string is hardcodeded because this method will get called from a batch
      // file that can exist on systems that do not have any other 5dx software
      // on them (including the localization file).
      System.err.println("ERROR: Unexpected server error.");
      error.printStackTrace();
      System.exit(1);
    }
    catch(RemoteException re)
    {
      // this string is hardcodeded because this method will get called from a batch
      // file that can exist on systems that do not have any other 5dx software
      // on them (including the localization file).
      System.err.println(_COULD_NOT_CONNECT_ERROR);
      System.exit(1);
    }
    catch(NotBoundException nbe)
    {
      // this string is hardcodeded because this method will get called from a batch
      // file that can exist on systems that do not have any other 5dx software
      // on them (including the localization file).
      System.err.println(_COULD_NOT_CONNECT_ERROR);
      System.exit(1);
    }
    catch(MalformedURLException mue)
    {
      // this string is hardcodeded because this method will get called from a batch
      // file that can exist on systems that do not have any other 5dx software
      // on them (including the localization file).
      System.err.println(_COULD_NOT_CONNECT_ERROR);
      System.exit(1);
    }
    catch(XrayTesterException xte)
    {
      System.err.println("ERROR: Unexpected exception.");
      xte.printStackTrace();
      System.exit(1);
    }
  }
}
