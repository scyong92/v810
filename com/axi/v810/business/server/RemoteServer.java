package com.axi.v810.business.server;

import java.rmi.*;
import java.rmi.server.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;

/**
* All GUI's must connect to this server to get a reference to any interfaces that they use
* to access server functionality.
*
* @author Bill Darbie
*/
public class RemoteServer extends UnicastRemoteObject implements RemoteServerInt
{
  private Server _server;

  /**
   * @author Bill Darbie
   */
  public RemoteServer(Server server) throws RemoteException, HardwareException, DatastoreException
  {
    Assert.expect(server != null);
    _server = server;
  }

  /**
  * Shut down the all the software - the Java server and the cxobj servers.
  * @author Bill Darbie
  */
  public void softwareShutDown() throws HardwareException
  {
    Assert.expect(_server != null);

    _server.softwareShutDown();
  }

  /**
   * Determine if the server is ready for use
   * @author George A. David
   */
  public boolean isServerInitialized()
  {
    Assert.expect(_server != null);
    return _server.isServerInitialized();
  }
}
