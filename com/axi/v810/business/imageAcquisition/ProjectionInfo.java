package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.util.image.*;

/**
 * Copyright: Copyright (c) 2005
 * Company: Agilent Technogies, Inc.
 * @author Roy Williams
 */
public class ProjectionInfo
{
  private Image          _projectionImage                                   = null;
  private int            _xStageWhereFirstRowImagedOnVerticalCenterOfCamera = -1;
  private int            _yStageWhereFirstRowImagedOnVerticalCenterOfCamera = -1;
  private ImageRectangle _imageRectangleForRequestedProjection              = null;
  private int            _heightOfScanPixels                                = -1;

  /**
   * @author Roy Williams
   */
  public Image getProjectionImage()
  {
    return _projectionImage;
  }

  /**
   * @author Eddie Williamson
   */
  public int getHeightOfScanPixels()
  {
    Assert.expect(_heightOfScanPixels > 0);
    return _heightOfScanPixels;
  }

  /**
   * @author Eddie Williamson
   */
  public void setHeightOfScanPixels(int newHeight)
  {
    Assert.expect(newHeight > 0);
    _heightOfScanPixels = newHeight;
  }

  /**
   * @author Roy Williams
   */
  public void setProjection(Image projectionImage)
  {
    Assert.expect(projectionImage != null);

    if (_projectionImage != null)
      _projectionImage.decrementReferenceCount();

    projectionImage.incrementReferenceCount();
    _projectionImage = projectionImage;
  }

  /**
   * Remove all the references to any images.
   * @author Patrick Lacz
   */
  public void clear()
  {
    if (_projectionImage != null)
      _projectionImage.decrementReferenceCount();
    _projectionImage = null;
  }

  /**
   * @author Roy Williams
   */
  public int getXStageWhereFirstRowImagedOnVerticalCenterOfCamera()
  {
    return _xStageWhereFirstRowImagedOnVerticalCenterOfCamera;
  }

  /**
   * @author Roy Williams
   */
  public void setXStageWhereFirstRowImagedOnVerticalCenterOfCamera(int position)
  {
    _xStageWhereFirstRowImagedOnVerticalCenterOfCamera = position;
  }


  /**
   * @author Roy Williams
   */
  public int getYStageWhereFirstRowImagedOnVerticalCenterOfCamera()
  {
    return _yStageWhereFirstRowImagedOnVerticalCenterOfCamera;
  }

  /**
   * @author Roy Williams
   */
  public void setYStageWhereFirstRowImagedOnVerticalCenterOfCamera(int position)
  {
    _yStageWhereFirstRowImagedOnVerticalCenterOfCamera = position;
  }

  /**
   * @author Roy Williams
   */
  public void setImageRectangleForRequestedProjection(ImageRectangle imageRectangle)
  {
    _imageRectangleForRequestedProjection = imageRectangle;
  }

  /**
   * @author Roy Williams
   */
  public ImageRectangle getImageRectangleForRequestedProjection()
  {
    if (_imageRectangleForRequestedProjection == null)
    {
      int i = 1;
    }
    return _imageRectangleForRequestedProjection;
  }

  static public String toStringOrder()
  {
    String commandSeparator = ", ";
    StringBuffer buf = new StringBuffer();
    buf.append("xStage"      + commandSeparator);
    buf.append("yStage"      + commandSeparator);
    buf.append("imageX"      + commandSeparator);
    buf.append("imageY"      + commandSeparator);
    buf.append("imageWidth"  + commandSeparator);
    buf.append("imageHeight" + commandSeparator);
    return buf.toString();
  }

  /**
   * @author Roy Williams
   */
  public String toString()
  {
    String commandSeparator = ", ";
    StringBuffer buf = new StringBuffer();
    buf.append(getXStageWhereFirstRowImagedOnVerticalCenterOfCamera() + commandSeparator);
    buf.append(getYStageWhereFirstRowImagedOnVerticalCenterOfCamera() + commandSeparator);
    buf.append(_imageRectangleForRequestedProjection.getMinX()        + commandSeparator);
    buf.append(_imageRectangleForRequestedProjection.getMinY()        + commandSeparator);
    buf.append(_imageRectangleForRequestedProjection.getWidth()       + commandSeparator);
    buf.append(_imageRectangleForRequestedProjection.getHeight()      + commandSeparator);
    return buf.toString();
  }
}
