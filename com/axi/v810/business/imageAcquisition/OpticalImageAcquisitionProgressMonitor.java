package com.axi.v810.business.imageAcquisition;

import java.io.*;
import java.net.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;

/**
 * Monitors the optical image acquisition engine. If it is taking
 * too long to progress, it notifies the OpticalImageAcquisitionEngine
 * 
 * @author Cheah Lee Herng
 */
public class OpticalImageAcquisitionProgressMonitor 
{
    private static OpticalImageAcquisitionProgressMonitor _instance;
    private OpticalImageAcquisitionEngine _opticalImageAcquisitionEngine;
    private boolean _isRunning = false;
    private TimerUtil _timer = new TimerUtil();
    private boolean _isMonitoring = false;
    private WorkerThread _workerThread = new WorkerThread("optical image acquisition progress monitor");
    private long _currentTimeOutMilliSeconds = -1;
    private LinkedList<String> _stateQueue = new LinkedList<String>();
    private int _imageReceived = 0;
    
    /**
     * @author Cheah Lee Herng
     */
    private OpticalImageAcquisitionProgressMonitor()
    {
        // do nothing
        Config _config = Config.getInstance();
        _currentTimeOutMilliSeconds = _config.getLongValue(SoftwareConfigEnum.OPTICAL_IMAGE_ACQUISITION_PROGRESS_MONITOR_TIMEOUT_SECONDS) * 1000;
    }
    
    /**
     * @author Cheah Lee Herng
    */
    public long getCurrentTimeOutInMilliSeconds()
    {
        return _currentTimeOutMilliSeconds;
    }

    /**
     * @author George A. David
    */
    public static synchronized OpticalImageAcquisitionProgressMonitor getInstance()
    {
        if(_instance == null)
          _instance = new OpticalImageAcquisitionProgressMonitor();

        return _instance;
    }
    
    /**
    * @author Cheah Lee Herng
    */
    private void checkTimer()
    {
        long elapsedTime = _timer.getElapsedTimeInMillis();

        // if we have a number here > 24 hours there is a bug in the code!
        Assert.expect(elapsedTime < 24 * 60 * 60 * 1000);
        // let's check how we are doing
        if (elapsedTime > _currentTimeOutMilliSeconds)
        {
          System.out.println("Checking optical image acquisition progress: " + elapsedTime + " > " + _currentTimeOutMilliSeconds);

          System.out.println("\n***** State Info *****");
          synchronized (_stateQueue)
          {
            for (String state : _stateQueue)
            {
              System.out.println(state);
            }
          }
          System.out.println("***** End State Info *****\n");
        
          _currentTimeOutMilliSeconds *= 2.0;            
        }
    }

    /**
     * @author Cheah Lee Herng
    */
    void resetTimeOut()
    {
        _timer.restart();
        saveState("resetTimeOut");
    }
    
    /**
    * @author Cheah Lee Herng
    */
    public void startMonitoring()
    {
        if(_opticalImageAcquisitionEngine == null)
          _opticalImageAcquisitionEngine = OpticalImageAcquisitionEngine.getInstance();

        if(_isMonitoring)
          return;

        _workerThread.invokeLater(new Runnable()
        {
          public synchronized void run()
          {
            _isRunning = true;
            _isMonitoring = true;
            _timer.reset();
            _timer.start();
            while(_isMonitoring && areAllOpticalImagesAcquired() == false)
            {
              checkTimer();

              // wait 1 second
              try
              {
                wait(1000);
              }
              catch (InterruptedException ex)
              {
                // do nothing
              }
            }
          }
        });
    }
    
    /**
     * @author Cheah Lee Herng
    */
    public synchronized void stopMonitoring()
    {
        _isMonitoring = false;
        notifyAll();
        saveState("StopMonitoring");
    }
    
    /**
     * @author Cheah Lee Herng
    */
    public void imageReceived()
    {
        _timer.restart();
        saveState("opticalImageReceived");
        _imageReceived++;
    }
    
    /**
     * @author Cheah Lee Herng
    */
    private boolean areAllOpticalImagesAcquired()
    {
        return _opticalImageAcquisitionEngine.getReconstructedOpticalImagesProducer().areAllOpticalImagesAcquired();
    }
    
    /**
     * @author Rex Shang
    */
    void pointToPointCompleted()
    {
        _timer.restart();
        saveState("allPointToPointCompleted");
    }
    
    /**
     * @author Cheah Lee Herng
    */
    private void saveState(String state)
    {
        synchronized (_stateQueue)
        {
          _stateQueue.add(state);
          if (_stateQueue.size() > 100)
            _stateQueue.remove();
        }
    }
}
