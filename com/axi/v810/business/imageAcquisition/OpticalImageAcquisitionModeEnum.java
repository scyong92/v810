package com.axi.v810.business.imageAcquisition;

/**
 * Enumeration for OpticalImageAcquisitionEngine modes
 * 
 * @author Cheah Lee Herng
 */
public class OpticalImageAcquisitionModeEnum extends com.axi.util.Enum 
{
    private static int _index = 0;
    public static final OpticalImageAcquisitionModeEnum NOT_IN_USE            = new OpticalImageAcquisitionModeEnum(_index++);
    public static final OpticalImageAcquisitionModeEnum PRODUCTION            = new OpticalImageAcquisitionModeEnum(_index++);
    public static final OpticalImageAcquisitionModeEnum SINGLE_REGION_IMAGE   = new OpticalImageAcquisitionModeEnum(_index++);
    public static final OpticalImageAcquisitionModeEnum CALIBRATION           = new OpticalImageAcquisitionModeEnum(_index++);
    
    /**
     * @author Cheah Lee Herng 
     */
    private OpticalImageAcquisitionModeEnum(int id)
    {
        super(id);
    }
}
