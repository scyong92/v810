package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.v810.hardware.*;

/**
 * @author Roy Williams
 */
public class ReorderProjectionRequestsAboveCamerasComparator implements Comparator<ProjectionRequestThreadTask>
{

  /**
   * @author Roy Williams
   */
  public int compare(ProjectionRequestThreadTask projectionRequest1,
                     ProjectionRequestThreadTask projectionRequest2)
  {
    int xDiff = projectionRequest1.xSortPosition() - projectionRequest2.xSortPosition();
    if (xDiff == 0)
    {
      // Same camera so must use StageDirection to discriminate.
      ScanPassDirectionEnum direction1 = projectionRequest1.getStageDirection();
      ScanPassDirectionEnum direction2 = projectionRequest2.getStageDirection();
      if (direction1.equals(direction2))
        return 0;
      return (direction1.equals(ScanPassDirectionEnum.FORWARD)) ? -1 : 1;
    }
    int rv = (xDiff < 0) ? -1 : 1;
    return rv;
  }
}
