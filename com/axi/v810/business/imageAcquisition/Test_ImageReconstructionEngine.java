package com.axi.v810.business.imageAcquisition;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.communication.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.business.autoCal.TestUtils;

/**
 * Test class for the ImageAcquisitionEngine communication to IRE/IRP.
 *
 * @author Roy Williams
 */
public class Test_ImageReconstructionEngine extends UnitTestWithSupportingProcesses implements Observer
{
  private HardwareWorkerThread   _hardwareWorkerThread   = null;
  private XrayTester             _xrayTester             = null;
  private int                    _irpsToLaunch           = 1;
  private Project                _project                = null;
  private ImageAcquisitionEngine _imageAcquisitionEngine = null;

  private String                 _baseDirName            = "c:/temp/";
  private String                 _baseFileName           = "IRPSkeleton.";
  private String                 _baseLogName            = "IrpExceptionSenderLog";
  private int                    _irpStartingPort        = 9119;

  private final int              _timeout                = 10000; // 10 seconds
  private boolean                _doneRunningProgram     = false;
  private boolean                _allImagesReturned      = false;
  private boolean                _unknownMessgeComplainedAbout = false;

  /**
   * @author Roy Williams
   */
  public Test_ImageReconstructionEngine() throws DatastoreException
  {
    super();
    Config config = Config.getInstance();
    try
    {
      config.loadIfNecessary();
    }
    catch (DatastoreException de)
    {
      de.printStackTrace();
      Assert.expect(false);
    }

    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    ImageAcquisitionEngine.setDebug(true);
    config.setValue(HardwareConfigEnum.NUMBER_OF_IMAGE_RECONSTRUCTION_ENGINES, new Integer(1));
    _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
    try
    {
      BooleanRef abortedDuringLoad = new BooleanRef();
      _project = Project.load("NEPCON_WIDE_EW", abortedDuringLoad);
      Assert.expect(abortedDuringLoad.getValue() == false);
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }
    Assert.expect(_project != null);
  }

  /**
   * @author Roy Williams
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    Exception exception = null;
    try
    {
      // startup the IRPs.
      starupIRPs(in, out);

      // Turn on the exception handler so we can note problems in IRP execution.
      if (exception == null)
        HardwareObservable.getInstance().addObserver(this);

      // Make sure the testProgram can be sent to the IRPs.
      if (exception == null)
        sendProgramToIRPs();

      // Make sure the testProgram can be sent to the IRPs.
      if (exception == null)
        getImagesBackFromIRP();

      // Test to see if the Exception handler works.
      if (exception == null)
        testExceptionHandler();

    }
    catch (Exception e)
    {
      exception = e;
      e.printStackTrace();
    }
    finally
    {
      cleanupIRPS();

      if (exception != null)
      {
        exception.printStackTrace();
        Assert.expect(false);
      }
    }
  }

  /**
   * @author Roy Williams
   */
  private void starupIRPs(BufferedReader in, PrintWriter out) throws IOException
  {
    ImageReconstructionEngine.setDeepSimulation(true);
    ImageReconstructionSocket.setLoopback(false);

    for (int i = 0; i < _irpsToLaunch; i++)
    {
      String portNumberString = Integer.toString(i + _irpStartingPort);
      File outputFile = new File(_baseDirName + _baseFileName + portNumberString);

      if (outputFile.exists())
        outputFile.delete();

      launch("IRPSkeleton.exe " + portNumberString, getUnitTestSystemEnvironment(), new File(getTestDataDir()), true, out);
    }
  }

  /**
   * @author Roy Williams
   */
  private void cleanupIRPS()
  {
    terminateAllLaunchedProcesses(20000);

    // Normally one must restore any HW config or calib stuff.   We have not
    // changed anything for this test.
    for (int i = 0; i < _irpsToLaunch; i++)
    {
      // Clean up any log files resulting from the launched processes.
      File exceptionLogFile = new File(_baseDirName + _baseLogName + (i + 1));
      if (exceptionLogFile.exists())
        exceptionLogFile.delete();

      // cat the log files into the expect file for this unit test.  We have
      // sent the output into seperate files... otherwise, the output from our
      // IRP's would be interleaved and non deterministic in order.
      File outputFile = new File(_baseDirName + _baseFileName + (i + _irpStartingPort));
      if (outputFile.exists())
      {
        BufferedReader fileReader = null;
        try
        {
          fileReader = new BufferedReader(new FileReader(outputFile));
          String line = null;
          System.out.println();
          System.out.println("Output from file: " + outputFile.getCanonicalFile());
          while ((line = fileReader.readLine()) != null)
            System.out.println(line);
        }
        catch (Exception e)
        {}
        finally
        {
          if (fileReader != null)
            try
            {
              fileReader.close();
            }
            catch (Exception e)
            {}

          // Finally, clean up the output files from the launched processes.
          outputFile.delete();
        }
      }
    }
  }

  /**
   * Sending a program to the IRP is done at the same time it is executed/run.
   *
   * @author Roy Williams
   */
  public void sendProgramToIRPs() throws XrayTesterException
  {
    // Start the hardware running if (and only if) we are not in simulation mode.
    _hardwareWorkerThread = HardwareWorkerThread.getInstance();
    _xrayTester = XrayTester.getInstance();
    final TestProgram testProgram = _project.getTestProgram();
    PanelSettings panelSettings = _project.getPanel().getPanelSettings();
//    for (TestSubProgram subProgram : testProgram.getTestSubPrograms())
//    {
//      if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
//        panelSettings.setRightManualAlignmentTransform(subProgram.getDefaultManualAlignmentTransform());
//      else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
//        panelSettings.setLeftManualAlignmentTransform(subProgram.getDefaultManualAlignmentTransform());
//      else
//        Assert.expect(false);
//    }
//
//    panelSettings.assignAlignmentGroups();

    _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
    {
      public void run() throws Exception
      {
        _xrayTester.startup();
        //_imageAcquisitionEngine.fitReconstructionRegionsToHardwareLimits(_project.getTestProgram());

        TestExecutionTimer testExecutionTimer = new TestExecutionTimer();
        _imageAcquisitionEngine.acquireProductionImages(testProgram, false, testExecutionTimer);
        _doneRunningProgram = true;
      }
    });

    int timeoutRemaining = _timeout;
    while (timeoutRemaining > 0 && _doneRunningProgram == false)
    {
      try
      {
        Thread.sleep(500);
        timeoutRemaining -= 500;
      }
      catch (InterruptedException e)
      {
        // do nothing till timeout kicks us out.
      }
    }
  }

  /**
   * @author Roy Williams
   */
  private void getImagesBackFromIRP() throws IOException, XrayTesterException
  {
    ReconstructionRegion reconstructionRegionOfInterest =
      _project.getTestProgram().getAllInspectionRegions().get(0);
    for (ImageReconstructionEngine imageReconstructionEngine :
         _imageAcquisitionEngine.getReconstructedImagesProducer().getImageReconstructionEngines())
    {
      ReconstructSpecificRegion reconstructSpecificRegion =
          new ReconstructSpecificRegion(imageReconstructionEngine.getCommandSender(),
                                        1,
                                        reconstructionRegionOfInterest.getRegionId(),
                                        ImageTypeEnum.AVERAGE,
                                        0);
      reconstructSpecificRegion.send();
    }

    int imagesReturned = 0;
    int timeoutRemaining = _timeout;
    while (timeoutRemaining > 0 && _allImagesReturned == false)
    {
      try
      {
        Thread.sleep(500);
        timeoutRemaining -= 500;
        for (ImageReconstructionEngine imageReconstructionEngine :
             _imageAcquisitionEngine.getReconstructedImagesProducer().getImageReconstructionEngines())
        {
          imagesReturned = imageReconstructionEngine.getImageReceiver().getNumberOfReconstructedImageSlices();
          if (imagesReturned == 5)
            _allImagesReturned = true;
        }
      }
      catch (InterruptedException e)
      {
        // do nothing till timeout kicks us out.
      }
    }
    Assert.expect((imagesReturned == 5), "Retrieve images from IRP: " + imagesReturned);
  }

  /**
   * @author Roy Williams
   */
  private void testExceptionHandler()
  {
    for (ImageReconstructionEngine imageReconstructionEngine :
         _imageAcquisitionEngine.getReconstructedImagesProducer().getImageReconstructionEngines())
    {
      try
      {
        _unknownMessgeComplainedAbout = false;
        UnknownMessageUsedForTesting unknownMessageUsedForTesting =
            new UnknownMessageUsedForTesting(imageReconstructionEngine.getCommandSender());
        unknownMessageUsedForTesting.send();
        int i = 0;
      }
      catch (IOException e)
      {
        e.printStackTrace();
        Expect.expect(false, "Error sending bogus command to IRE");
      }
      catch (XrayTesterException e)
      {
        e.printStackTrace();
        Expect.expect(false, "Error sending bogus command to IRE");
      }
    }
    int timeoutRemaining = _timeout;
    while (timeoutRemaining > 0 && _unknownMessgeComplainedAbout == false)
    {
      try
      {
        Thread.sleep(500);
        timeoutRemaining -= 500;
      }
      catch (InterruptedException e)
      {
        // do nothing till timeout kicks us out.
      }
    }
//    Assert.expect(_unknownMessgeComplainedAbout,
//                  "Never recieved Exception message from IRP");
  }

  /**
   * @author Roy Williams
   */
  class UnknownMessageUsedForTesting extends AbstractStateMessage
  {
    /**
     * @author Roy Williams
     */
    public UnknownMessageUsedForTesting(CommandSender writer) throws XrayTesterException, IOException
    {
      super(new ImageReconstructionMessageEnum(345), writer);
    }
  }

  /**
   * @author Roy Williams
   */
  public void update(Observable observable, Object object)
  {
    if (observable instanceof HardwareObservable)
    {
      try
      {
        ImageReconstructionProcessorException irpException =
            (ImageReconstructionProcessorException) object;
        System.out.println(irpException.key() + ": " +
                           irpException.message());
        _unknownMessgeComplainedAbout = true;
      }
      catch (ClassCastException e)
      {
        // Do nothing - we are only looking to validate specific information above.
      }
    }
  }

  /**
   * @author Matt Wharton
   */
  public static void main( String[] args )
  {
    //Utils class allows backup/restore of config files
    TestUtils utils = new TestUtils();
    UnitTest unitTest = null;
    try
    {
      unitTest = new Test_ImageReconstructionEngine();
      UnitTest.execute(unitTest);
    }
    catch (DatastoreException de)
    {
      de.printStackTrace();
    }
    finally
    {
      //Finally, restore the hardware.config
      utils.restoreHWConfig(unitTest.getTestSourceDir());
    }
  }
}
