package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.v810.hardware.*;
import com.axi.util.*;

/**
 * @author Roy Williams
 */
public class ProjectionScanPass extends ScanPass
{
  private List <ProjectionRequestThreadTask> _projectionRequestsToBeRun;
  private List <ProjectionRequestThreadTask> _projectionRequestsToClearCameras;

  /**
   * @author Roy Williams
   */
  public ProjectionScanPass( int id,
                             StagePosition startPointInNanoMeters,
                             StagePosition endPointInNanoMeters,
                             List<ProjectionRequestThreadTask> projectionRequestsToBeRunDuringScanPass )
  {
    super(id, startPointInNanoMeters, endPointInNanoMeters);
    Assert.expect(projectionRequestsToBeRunDuringScanPass != null);
    _projectionRequestsToBeRun = projectionRequestsToBeRunDuringScanPass;
    _projectionRequestsToClearCameras = new ArrayList<ProjectionRequestThreadTask>();
  }

  /**
   * @author Roy Williams
   */
  public List<ProjectionRequestThreadTask> getProjectionRequestsToBeRun()
  {
    return _projectionRequestsToBeRun;
  }

  /**
   * @author Roy Williams
   */
  public void addProjectionRequestToClearCamera(ProjectionRequestThreadTask projectionRequest)
  {
    Assert.expect(projectionRequest != null);

    _projectionRequestsToClearCameras.add(projectionRequest);
  }

  /**
   * @author Roy Williams
   */
  public List<ProjectionRequestThreadTask> projectionRequestsToClearCameras()
  {
    return _projectionRequestsToClearCameras;
  }
}
