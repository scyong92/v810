package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class ResumeReconstructionThreadTask extends ThreadTask<Object>
{
  private ImageReconstructionEngine _imageReconstructionEngine = null;
  private boolean                   _cancelled = false;

  /**
   * @author Roy Williams
   */
  public ResumeReconstructionThreadTask(ImageReconstructionEngine imageReconstructionEngine)
  {
    super("ResumeReconstructionThreadTask:" + imageReconstructionEngine.getId());

    Assert.expect(imageReconstructionEngine != null);

    _imageReconstructionEngine = imageReconstructionEngine;
  }

  /**
   * @author Roy Williams
   */
  public Object executeTask() throws Exception
  {
    // Between each step check to see if we should continue or cancel.
    if (_cancelled)
      return null;

    _imageReconstructionEngine.resumeReconstruction();
    return null;
  }

  /**
   * @author Roy Williams
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Roy Williams
   */
  protected void cancel() throws XrayTesterException
  {
    _cancelled = true;
  }
}
