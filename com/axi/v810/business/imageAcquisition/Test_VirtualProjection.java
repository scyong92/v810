package com.axi.v810.business.imageAcquisition;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 * @author Eddie Williamson
 */
public class Test_VirtualProjection extends UnitTest
{
  /**
   * @author Eddie Williamson
   */
  protected Test_VirtualProjection()
  {
    super();
  }


  /**
   * @author Eddie Williamson
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_VirtualProjection());
 }


 /**
  * @author Eddie Williamson
  */
 public void test(BufferedReader is, PrintWriter os)
 {
   XrayCameraIdEnum cameraId = XrayCameraIdEnum.XRAY_CAMERA_0;
   VirtualProjection virtualProjection = new VirtualProjection(XrayCameraArray.getCamera(cameraId), ScanPassDirectionEnum.FORWARD, -160000, -160000, 320000, 320000);

   AbstractXrayCamera camera = virtualProjection.getCamera();
   Expect.expect(camera.getId() == cameraId.getId());

   ProjectionRequestThreadTask projectionRequest = virtualProjection.getProjectionRequestThreadTask();
   Expect.expect(projectionRequest != null);

   int stageX = virtualProjection.getStageX();
   Expect.expect(stageX == -1);

   int stageY = virtualProjection.getStageY();

   Expect.expect(stageY == 25082999); // 25087999 is for Magnification=6.25, 25082999 is for Magnification=5.26
 }
}






