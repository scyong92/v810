package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * ProjectionConsumers are responsible to retrieve Images from the XrayCamera objects
 * that have been programmed by the ImageAcquisitionEngine to produce them.  In all
 * cases, they don't do the retrieval themselves but launch another object to do
 * the dirty work.  This is because there are often multiple threads involved in
 * this work.   This object is simply the coordinator for that responsibility.
 *
 * @author Roy Williams
 */
public abstract class ProjectionConsumer implements Simulatable
{
  private static Config _config  = null;
  private static List<AbstractXrayCamera> _xRayCameras;

  private ProjectionProducer _projectionProducer;
  private boolean _simulationModeOn = false;

  /**
   * @author Roy Williams
   */
  static
  {
    _config = Config.getInstance();
  }

  /**
   * @author Roy Williams
   */
  static protected Config getConfig()
  {
    return _config;
  }

  /**
   * @author Roy Williams
   */
  protected ProjectionConsumer()
  {
  }

  /**
   * @author Roy Williams
   */
  protected ProjectionConsumer(ProjectionProducer projectionProducer)
  {

    Assert.expect(projectionProducer != null);

    _projectionProducer = projectionProducer;

    // Initialize the XrayCamera instances.  We'll keep our own cached reference.
    _xRayCameras = XrayCameraArray.getCameras();
  }

  /**
   * @author Roy Williams
   */
  protected ProjectionProducer getProjectionProducer()
  {
    Assert.expect(_projectionProducer != null);
    return _projectionProducer;
  }

  /**
   * @author Roy Williams
   */
  public void setProjectionProducer(ProjectionProducer projectionProducer)
  {
    Assert.expect(projectionProducer != null);

    _projectionProducer = projectionProducer;
  }

  /**
   * @author Roy Williams
   */
  protected List<AbstractXrayCamera> getCameras()
  {
    Assert.expect(_xRayCameras != null);

    return _xRayCameras;
  }

  /**
   * @author Roy Williams
   */
  public abstract void initializeForNextScanPath() throws XrayTesterException;


  /**
   * Called by the ImageAcquisition.runAcquisitionSequence method to insure this
   * implementation of a ProjectionConsumer is ready (for it) to initiate the next
   * scanPass.   Once the next scanPass is initiated, this ProjectionConsumer must
   * be ready to acquuire the next set of images.
   *
   * @author Roy Williams
   */
  public abstract boolean canProceedWithScanPassWithWait(int scanPassNumber) throws XrayTesterException;

  /**
   * @author Roy Williams
   */
  public abstract void startScanPass(int scanPassNumber) throws XrayTesterException;

  /**
   * @author Roy Williams
   */
  public abstract int incrementRunnableScanPass();

  /**
   * @throws DatastoreException
   * @author Rex Shang
   */
  public void setSimulationMode() throws DatastoreException
  {
    _simulationModeOn = true;
  }

  /**
   * @throws DatastoreException
   * @author Rex Shang
   */
  public void clearSimulationMode() throws DatastoreException
  {
    _simulationModeOn = false;
  }

  /**
   * @return true if current object is or all hardware objects are in simulation mode.
   * @throws DatastoreException
   * @author Rex Shang
   */
  public boolean isSimulationModeOn()
  {
    if (_simulationModeOn || _config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION))
      return true;
    else
      return false;
  }
}
