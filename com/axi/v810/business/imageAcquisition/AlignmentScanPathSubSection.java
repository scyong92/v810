package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;


/**
 * @author Roy Williams
 */
class AlignmentScanPathSubSection extends ScanPathSubSection
{
  private ReconstructionRegion _alignmentRegion;

  /**
   * @author Roy Williams
   */
  AlignmentScanPathSubSection(ReconstructionRegion alignmentRegion)
  {
    super();

    Assert.expect(alignmentRegion != null);

    _alignmentRegion  = alignmentRegion;
  }

  /**
   * @author Roy Williams
   */
  ReconstructionRegion getAlignmentRegion()
  {
    return _alignmentRegion;
  }

}
