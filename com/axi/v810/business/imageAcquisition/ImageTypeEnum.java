package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.Enum;

/**
 * @author Roy Williams
 */
public class ImageTypeEnum extends com.axi.util.Enum
{
  static private int _index = -1;

  public static final ImageTypeEnum AVERAGE = new ImageTypeEnum(++_index);
  public static final ImageTypeEnum LIGHTEST = new ImageTypeEnum(++_index);
  public static List<ImageTypeEnum> _allImageTypeEnums;

  /**
   * @author Roy Williams
   */
  static {
    Collection<Enum> mapValues = getIdToEnumMap(ImageTypeEnum.class).values();
    _allImageTypeEnums = new ArrayList<ImageTypeEnum>(mapValues.size());
    for (com.axi.util.Enum type : mapValues)
    {
      _allImageTypeEnums.add((ImageTypeEnum)type);
    }
  }

  /**
   *
   * @author Roy Williams
   */
  private ImageTypeEnum(int ndx)
  {
    super(ndx);
  }

  /**
   * @author Roy Williams
   */
  public static List<ImageTypeEnum> getAllTypes()
  {
    return _allImageTypeEnums;
  }
}
