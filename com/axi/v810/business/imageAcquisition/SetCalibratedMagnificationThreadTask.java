package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class SetCalibratedMagnificationThreadTask extends ThreadTask<Object>
{
  private ImageReconstructionEngine _imageReconstructionEngine;
  private double                    _mag;
  private double                    _highMag;
  private boolean                   _cancelled;

  /**
   * @author Roy Williams
   */
  public SetCalibratedMagnificationThreadTask(ImageReconstructionEngine imageReconstructionEngine, double mag, double highMag)
  {
    super("SetCalibratedMagnificationThreadTask:" + imageReconstructionEngine.getId());

    Assert.expect(imageReconstructionEngine != null);
    Assert.expect(mag > 0);
    Assert.expect(highMag > 0);

    _imageReconstructionEngine = imageReconstructionEngine;
    _mag = mag;
    _highMag = highMag;
  }

  /**
   * @author Roy Williams
   */
  public Object executeTask() throws Exception
  {
    // Between each step check to see if we should continue or cancel.
    if (_cancelled)
      return null;

    _imageReconstructionEngine.setCalibratedMagnification(_mag, _highMag);
    return null;
  }

  /**
   * @author Roy Williams
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Roy Williams
   */
  protected void cancel() throws XrayTesterException
  {
    _cancelled = true;
  }
}
