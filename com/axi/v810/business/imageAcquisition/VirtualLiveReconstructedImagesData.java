package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.virtualLive.*;

/**
 * @author khang-shian.sham
 */
public class VirtualLiveReconstructedImagesData
{
  private static VirtualLiveReconstructedImagesData _reconstructedImagesData;
  private java.util.List<String> _imageNameList = new LinkedList<String>();
  private Map<Integer, List> _sliceToImageNameListMap = new HashMap<Integer, List>();
  private ArrayList _sliceList= new ArrayList();
  private int _version = 0;
  private String _imageSetDir;
  //0 = offline, 1 = online
  private int _mode = 0;

  /**
   * @author khang-shian.sham
   */
  public static VirtualLiveReconstructedImagesData getInstance()
  {
    if (_reconstructedImagesData == null)
    {
      _reconstructedImagesData = new VirtualLiveReconstructedImagesData();
    }
    return _reconstructedImagesData;
  }

  /**
   * @author khang-shian.sham
   */
  public VirtualLiveReconstructedImagesData()
  {
    //do nothing
  }

  /**
   * @author khang-shian.sham
   */
  public void setOfflineReconstructedImagesData(java.util.List<String> imageNameList, int version, String imageSetDir, int mode)
  {
    Assert.expect(imageNameList != null);
    Assert.expect(imageSetDir != null);
    clear();
    setImageNameList(imageNameList);
    setImageSetDir(imageSetDir);
    setSliceList(_imageNameList);
    setVersion(version);
    setMode(mode);
    //display zSlider
    VirtualLiveReconstructedImagesDataObservable.getInstance().setObservable();
  }

 /**
  * @author khang-shian.sham
  */
  public void setOnlineReconstructedImagesData(int version, int mode)
  {
    Assert.expect(_imageNameList != null);

    setSliceList(_imageNameList);
    setVersion(version);
    setMode(mode);
    //display zSlider
    VirtualLiveReconstructedImagesDataObservable.getInstance().setObservable();
  }
  
  /**
   * @author khang-shian.sham
   */
  public void setImageNameList(java.util.List<String> imageNameList)
  {
    Assert.expect(imageNameList != null);

    _imageNameList.clear();
    _imageNameList = imageNameList;
  }

  /**
   * @author sham
   */
  public void addImageName(String imageName)
  {
    Assert.expect(imageName != null);

    _imageNameList.add(imageName);
  }

  /**
   * @author khang-shian.sham
   */
  public void setSliceList(java.util.List<String> imageNameList)
  {
    Assert.expect(imageNameList != null);

    String[] str = null;
    List tempImageNameList;
    ArrayList listArray = new ArrayList();
    int value;
    String valueStr="";
    for (String imageName : imageNameList)
    {
      str = imageName.split("_");
      valueStr = str[str.length - 1].replaceAll(".png", "");      
      valueStr = valueStr.replaceAll(".jpg", "");
      valueStr = valueStr.replaceAll(".tiff", "");
      value = Integer.parseInt(valueStr);
      listArray.add(value);
      tempImageNameList = _sliceToImageNameListMap.get(value);
      if(tempImageNameList == null)
      {
        tempImageNameList = new LinkedList<String>();
      }
      tempImageNameList.add(imageName);
      _sliceToImageNameListMap.put(value,tempImageNameList);
    }
//    Set set = new HashSet(listArray);
    if (_sliceList != null)
    {
      _sliceList.clear();
    }
    _sliceList = new ArrayList(listArray);
    Collections.sort(_sliceList);
    listArray.clear();
  }

  /**
   * @author khang-shian.sham
   */
  public ArrayList getSliceList()
  {
    return _sliceList;
  }

  /**
   * @author khang-shian.sham
   */
  public java.util.List<String> getImageNameList()
  {
    return _imageNameList;
  }

  /**
   * @author sham
   */
  public java.util.List<String> getSliceImageNameList(int slice)
  {
    Assert.expect(_sliceToImageNameListMap != null);
    
    return _sliceToImageNameListMap.get(slice);
  }

  /**
   * @author sham
   */
  public void setVersion(int version)
  {
    _version = version;
  }

  /**
   * @author sham
   */
  public int getVersion()
  {
    return _version;
  }

  /**
   * @author sham
   */
  public void setImageSetDir(String imageSetDir)
  {
    Assert.expect(imageSetDir != null);

    _imageSetDir = imageSetDir;
  }

  /**
   * @author sham
   */
  public String getImageSetDir()
  {
    Assert.expect(_imageSetDir != null);

    return _imageSetDir;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean hasImageSet()
  {
    if (_imageSetDir == null)
    {
      return false;
    }
    return true;
  }

  /**
   * @author sham
   */
  public void setMode(int mode)
  {
    Assert.expect(mode >= 0);
    Assert.expect(mode <= 1);

    _mode = mode;
  }

  /**
   * @author sham
   */
  public int getMode()
  {
    return _mode;
  }

  /**
   * @author sham
   */
  public boolean isOnlineMode()
  {
    return (_mode == 1);
  }

  /**
   * @author sham
   */
  public void clear()
  {
    Assert.expect(_imageNameList != null);
    Assert.expect(_sliceToImageNameListMap != null);
    Assert.expect(_sliceList != null);
    _imageNameList.clear();
    _sliceToImageNameListMap.clear();
    _sliceList.clear();
  }
}
