package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 *
 * @author khang-wah.chnee
 */
public class NoNeighborsToPredictZHeightException extends XrayTesterException
{
  /**
   * @author khang-wah.chnee
   */
  public NoNeighborsToPredictZHeightException(String logFilePath)
  {
    super(new LocalizedString("HW_TEST_NO_NEIGHBORS_TO_PREDICT_ZHEIGHT_KEY",
                              new Object[]{logFilePath}));
  }
}
