package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PspImageAcquisitionEngineVersion1 extends PspImageAcquisitionEngine
{
  protected int MAXIMUM_WAIT_TIME_BEFORE_SEND_PROJECTOR_SEQUENCE_IN_MILISECONDS = 150;

  private BooleanLock _startProjectorSequenceLock = new BooleanLock(false);
  private BooleanLock _completeProjectorSequenceLock = new BooleanLock(false);
  private WorkerThread _startProjectorSequenceThread = new WorkerThread("start projector sequence worker thread");
  
  private PspHardwareModuleVersion1 _pspHardwareModuleVersion1;
  
  /**
   * @author Cheah Lee Herng
   */
  public PspImageAcquisitionEngineVersion1()
  {
    Assert.expect(_pspHardwareEngine instanceof PspHardwareModuleVersion1);
    
    _pspHardwareModuleVersion1 = (PspHardwareModuleVersion1)_pspHardwareEngine;
  }

  /**
   * @author Cheah Lee Herng
   */
  protected void initializeOpticalImageAcquistion(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalShiftEnum != null);
    
    _pspHardwareModuleVersion1.initializeProjector(moduleEnum);
    
    if (opticalShiftEnum.equals(OpticalShiftNameEnum.SHIFT_ONE))
      _pspHardwareModuleVersion1.displayFringePattern1(moduleEnum);
    else if (opticalShiftEnum.equals(OpticalShiftNameEnum.SHIFT_TWO))
      _pspHardwareModuleVersion1.displayFringePattern2(moduleEnum);
    else if (opticalShiftEnum.equals(OpticalShiftNameEnum.SHIFT_THREE))
      _pspHardwareModuleVersion1.displayFringePattern3(moduleEnum);
    else if (opticalShiftEnum.equals(OpticalShiftNameEnum.SHIFT_FOUR))
      _pspHardwareModuleVersion1.displayFringePattern4(moduleEnum);
    else if (opticalShiftEnum.equals(OpticalShiftNameEnum.WHITE_LIGHT))
      _pspHardwareModuleVersion1.displayWhiteLight(moduleEnum);
    else
      Assert.expect(false, "Invalid shift name enum"); 
  }

  /**
   * @author Cheah Lee Herng 
   */
  protected void performOpticalImageAcquisition(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalShiftEnum != null);
    
    _pspHardwareModuleVersion1.sendTriggerSequence(moduleEnum, opticalShiftEnum);
  }

  /**
   * @author Cheah Lee Herng 
   */
  protected void finishOpticalImageAcquisition(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    _pspHardwareModuleVersion1.shutdownProjector(moduleEnum);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void handleActualOpticalView(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalShiftEnum != null);
    
    // Initialize optical camera properties
    OpticalCameraSetting opticalCameraSetting = new OpticalCameraSetting(getFrontModuleCropImageOffsetXInPixel(),
                                                                         getRearModuleCropImageOffsetXInPixel());
    initializeOpticalCamera(moduleEnum, opticalCameraSetting);
    
    // Initialize projector
    _pspHardwareModuleVersion1.initializeProjector(moduleEnum);
    
    if (opticalShiftEnum.equals(OpticalShiftNameEnum.WHITE_LIGHT))
      _pspHardwareModuleVersion1.displayWhiteLight(moduleEnum);
    else
      Assert.expect(false, "Invalid shift name enum");
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void handlePostOpticalView(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalShiftEnum != null);
    
    _pspHardwareModuleVersion1.shutdownProjector(moduleEnum);
    _pspHardwareModuleVersion1.initializeProjector(moduleEnum);
    _pspHardwareModuleVersion1.displayWhiteLight(moduleEnum);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void handlePostImageDisplay(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    _pspHardwareModuleVersion1.finishDisplayFringePattern();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  protected String getCalibrationFullPath(PspModuleEnum moduleEnum, OpticalCalibrationProfileEnum opticalCalibrationProfileEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalCalibrationProfileEnum != null);
    
    OpticalCameraIdEnum cameraIdEnum = getOpticalCameraIdEnum(moduleEnum);
    return Directory.getOpticalCameraCalibProfileDir(cameraIdEnum.getId(), opticalCalibrationProfileEnum.getId());
  }

  /**
   * @author Cheah Lee Herng 
   */
  protected void runCalibrationImageAcquisition(PspModuleEnum moduleEnum, 
                                                OpticalCalibrationPlaneEnum planeEnum, 
                                                OpticalShiftNameEnum opticalShiftEnum, 
                                                String imageFullPath) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(planeEnum != null);
    Assert.expect(opticalShiftEnum != null);
    Assert.expect(imageFullPath != null);

    final String finalImageFullPath = imageFullPath;
    final OpticalCalibrationPlaneEnum finalPlaneEnum = planeEnum;
    final PspModuleEnum finalModuleEnum = moduleEnum;

    // We need to display fringe pattern ASAP before image acquistion starts
    initializeOpticalImageAcquistion(moduleEnum, opticalShiftEnum);

    _startProjectorSequenceLock.setValue(false);
    _completeProjectorSequenceLock.setValue(false);
    _startProjectorSequenceThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {                        
            _startProjectorSequenceLock.setValue(true);  
            _pspHardwareEngine.acquireCalibrationImage(finalModuleEnum, finalPlaneEnum, finalImageFullPath, false);
            _completeProjectorSequenceLock.setValue(true);
          }
          catch(XrayTesterException ex)
          {
              // Do nothing
          }
        }
      }
    );

    try
    {
      _startProjectorSequenceLock.waitUntilTrue();            
    }
    catch(InterruptedException ex)
    {
      // Do nothing
    }

    try
    {
      // We need to wait here so that we have enough time to send projector
      // sequence for optical camera triggering.
      Thread.sleep(MAXIMUM_WAIT_TIME_BEFORE_SEND_PROJECTOR_SEQUENCE_IN_MILISECONDS);
    }
    catch(InterruptedException ex)
    {
      // Do nothing
    }

    performOpticalImageAcquisition(moduleEnum, opticalShiftEnum);

    try
    {           
      _completeProjectorSequenceLock.waitUntilTrue();           
    }
    catch(InterruptedException ex)
    {
        // Do nothing
    }

    finishOpticalImageAcquisition(moduleEnum);
  }

  /**
   * @author Cheah Lee Herng 
   */
  protected void runHeightMapImageAcquisition(final OpticalInspectionData opticalInspectionData, 
                                              OpticalShiftNameEnum opticalShiftEnum, 
                                              String imageFullPath) throws XrayTesterException 
  {
    Assert.expect(opticalInspectionData != null);
    Assert.expect(opticalShiftEnum != null);
    Assert.expect(imageFullPath != null);

    final PspModuleEnum finalModuleEnum = opticalInspectionData.getPspModuleEnum();
    final String finalImageFullPath = imageFullPath;

    // We need to display fringe pattern ASAP before image acquistion starts
    initializeOpticalImageAcquistion(finalModuleEnum, opticalShiftEnum);

    _startProjectorSequenceLock.setValue(false);
    _completeProjectorSequenceLock.setValue(false);
    _startProjectorSequenceThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {                    
            _startProjectorSequenceLock.setValue(true);

            if (_config.getBooleanValue(SoftwareConfigEnum.SAVE_PSP_INSPECTION_IMAGES))
            {
              _pspHardwareEngine.acquireHeightMapImage(finalModuleEnum, 
                                                       finalImageFullPath, 
                                                       opticalInspectionData);
            }
            else
            {
              _pspHardwareEngine.acquireHeightMapImage(finalModuleEnum, 
                                                       null, 
                                                       opticalInspectionData);
            }

            _completeProjectorSequenceLock.setValue(true);
          }
          catch(XrayTesterException ex)
          {
              // Do nothing
          }
        }
      }
    );

    try
    {
      _startProjectorSequenceLock.waitUntilTrue();            
    }
    catch(InterruptedException ex)
    {
      // Do nothing
    }

    try
    {
      // We need to wait here so that we have enough time to send projector
      // sequence for optical camera triggering.
      Thread.sleep(MAXIMUM_WAIT_TIME_BEFORE_SEND_PROJECTOR_SEQUENCE_IN_MILISECONDS);
    }
    catch(InterruptedException ex)
    {
      // Do nothing
    }

    performOpticalImageAcquisition(finalModuleEnum, opticalShiftEnum);

    try
    {    
      _completeProjectorSequenceLock.waitUntilTrue();           
    }
    catch(InterruptedException ex)
    {
        // Do nothing
    }

    finishOpticalImageAcquisition(finalModuleEnum);
  }

  /**
   * @author Cheah Lee Herng
   */
  protected void runSingleImageAcquisition(final OpticalInspectionData opticalInspectionData, OpticalShiftNameEnum opticalShiftEnum, String imageFullPath) throws XrayTesterException 
  {
    Assert.expect(opticalInspectionData != null);
    Assert.expect(opticalShiftEnum != null);
    Assert.expect(imageFullPath != null);

    final PspModuleEnum finalModuleEnum = opticalInspectionData.getPspModuleEnum();
    final String finalImageFullPath = imageFullPath;

    // We need to display fringe pattern ASAP before image acquistion starts
    initializeOpticalImageAcquistion(finalModuleEnum, opticalShiftEnum);

    _startProjectorSequenceLock.setValue(false);
    _completeProjectorSequenceLock.setValue(false);
    _startProjectorSequenceThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {                        
            _startProjectorSequenceLock.setValue(true);  
            _pspHardwareEngine.acquireSingleImage(finalModuleEnum, finalImageFullPath, false);             
            _completeProjectorSequenceLock.setValue(true);
          }
          catch(XrayTesterException ex)
          {
              // Do nothing
          }
        }
      }
    );

    try
    {
      _startProjectorSequenceLock.waitUntilTrue();            
    }
    catch(InterruptedException ex)
    {
      // Do nothing
    }

    try
    {
      // We need to wait here so that we have enough time to send projector
      // sequence for optical camera triggering.
      Thread.sleep(MAXIMUM_WAIT_TIME_BEFORE_SEND_PROJECTOR_SEQUENCE_IN_MILISECONDS);
    }
    catch(InterruptedException ex)
    {
      // Do nothing
    }

    performOpticalImageAcquisition(finalModuleEnum, opticalShiftEnum);

    try
    {
      _completeProjectorSequenceLock.waitUntilTrue();           
    }
    catch(InterruptedException ex)
    {
        // Do nothing
    }

    finishOpticalImageAcquisition(finalModuleEnum);
  }
  
  /**
   * For PSP Module version 1, we set both front and rear the same offset.
   * 
   * @author Cheah Lee Herng 
   */
  public int getFrontModuleCropImageOffsetXInPixel()
  {
    int moduleOffsetInPixel = ((PspHardwareEngine.getInstance().getMaxImageWidth() - 
                                PspHardwareEngine.getInstance().getImageWidth()) / 2);
    return moduleOffsetInPixel;
  }
  
  /**
   * For PSP Module version 1, we set both front and rear the same offset.
   * 
   * @author Cheah Lee Herng 
   */
  public int getRearModuleCropImageOffsetXInPixel()
  {
    int moduleOffsetInPixel = ((PspHardwareEngine.getInstance().getMaxImageWidth() - 
                                PspHardwareEngine.getInstance().getImageWidth()) / 2);
    return moduleOffsetInPixel;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getLargerFovFrontModuleCropImageOffsetXInPixel()
  {
    return getFrontModuleCropImageOffsetXInPixel();
  }

  /**
   * @author Ying-Huan.Chu
   */
  public int getLargerFovRearModuleCropImageOffsetXInPixel()
  {
    return getRearModuleCropImageOffsetXInPixel();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void checkIfHeightMapIsAvailable(PspModuleEnum pspModuleEnum) throws XrayTesterException
  {
    // do nothing
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  protected void checkOpticalCalibrationDataExist() throws XrayTesterException
  {
    // Do nothing
  }
}
