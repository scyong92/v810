package com.axi.v810.business.imageAcquisition;

import com.axi.util.Assert;
import com.axi.v810.business.testProgram.*;
import java.util.Comparator;
import com.axi.v810.util.SystemFiducialRectangle;
import com.axi.util.Pair;

/**
 * @author Roy Williams
 */
public class PositionOfReconstructionRegionsWithSystemFiducialComparator implements Comparator<Pair<ReconstructionRegion, SystemFiducialRectangle>>
{
  /**
   * @author Roy Williams
   */
  private boolean _sortBasedOnLeadingEdge;
  private boolean _sortOnXaxis;

  /**
   * @author Roy Williams
   */
  public PositionOfReconstructionRegionsWithSystemFiducialComparator(boolean sortOnXaxis,
                                                                    boolean sortBasedOnLeadingEdge)
  {
    _sortBasedOnLeadingEdge = sortBasedOnLeadingEdge;
    _sortOnXaxis = sortOnXaxis;
  }

  /**
   * @author Roy Williams
   */
  public int compare(Pair<ReconstructionRegion, SystemFiducialRectangle> pair1,
                     Pair<ReconstructionRegion, SystemFiducialRectangle> pair2)
  {
    Assert.expect(pair1 != null);
    Assert.expect(pair2 != null);

    /** @todo Roy - use the y axis as a secondary sort key for the event where
     *  two regions intersect in x for > 80%.
     */

    if (_sortOnXaxis)
    {
      // Leading edge is the max x because panel is being moved left to right.
      int maxXofRegion1 = pair1.getSecond().getMaxX();
      int maxXofRegion2 = pair2.getSecond().getMaxX();
      if (maxXofRegion1 == maxXofRegion2)
        return 0;
      if (_sortBasedOnLeadingEdge)
        return (maxXofRegion1 > maxXofRegion2) ? -1 : 1;
      else // sort on trailing edge
        return (maxXofRegion1 < maxXofRegion2) ? -1 : 1;
    }

    // else sorting based on _sortOnYaxis.
    int maxYofRegion1 = pair1.getSecond().getMaxY();
    int maxYofRegion2 = pair2.getSecond().getMaxY();
    if (maxYofRegion1 == maxYofRegion2)
      return 0;
    if (_sortBasedOnLeadingEdge)
      return (maxYofRegion1 > maxYofRegion2) ? -1 : 1;
    else // sort on trailing edge
      return (maxYofRegion1 < maxYofRegion2) ? -1 : 1;
  }
}
