package com.axi.v810.business.imageAcquisition;


import com.axi.util.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 * Concrate class for PspEngine.
 * This is the version 1 Psp Engine which has MicroController, Projector and Optical Camera.
 * 
 * @author Cheah Lee Herng
 */
public class PspModuleVersion1 extends PspEngine
{  
  private static final int TOTAL_CALIBRATION_RUN                                                  = 1;
  private static final double PROFILE_1_REFERENCE_PLANE_KNOWN_HEIGHT                              = 2.0;
  private static final double PROFILE_1_OBJECT_PLANE_PLUS_THREE_KNOWN_HEIGHT                      = 5.0;
  private static final double PROFILE_2_REFERENCE_PLANE_KNOWN_HEIGHT                              = 1.0;
  private static final double PROFILE_2_OBJECT_PLANE_PLUS_FOUR_KNOWN_HEIGHT                       = 5.0;
  
  private PspHardwareModuleVersion1 _pspHardwareModuleVersion1;
  private PanelPositioner _panelPositioner;
  
  protected Map<PspModuleEnum, Map<OpticalCalibrationPlaneEnum, StagePositionMutable>> _pspModuleToStagePositionMap = new LinkedHashMap<PspModuleEnum, Map<OpticalCalibrationPlaneEnum, StagePositionMutable>>();
  
  /**
   * @author Cheah Lee Herng
   */
  public PspModuleVersion1()
  {
    Assert.expect(_pspHardwareEngine instanceof PspHardwareModuleVersion1);
    
    _pspHardwareModuleVersion1 = (PspHardwareModuleVersion1)_pspHardwareEngine;
    _panelPositioner = PanelPositioner.getInstance();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void handlePreOpticalImageAcquistion(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalShiftEnum != null);
    
    _pspHardwareModuleVersion1.initializeProjector(moduleEnum);
    
    if (opticalShiftEnum.equals(OpticalShiftNameEnum.SHIFT_ONE))
      _pspHardwareModuleVersion1.displayFringePattern1(moduleEnum);
    else if (opticalShiftEnum.equals(OpticalShiftNameEnum.SHIFT_TWO))
      _pspHardwareModuleVersion1.displayFringePattern2(moduleEnum);
    else if (opticalShiftEnum.equals(OpticalShiftNameEnum.SHIFT_THREE))
      _pspHardwareModuleVersion1.displayFringePattern3(moduleEnum);
    else if (opticalShiftEnum.equals(OpticalShiftNameEnum.SHIFT_FOUR))
      _pspHardwareModuleVersion1.displayFringePattern4(moduleEnum);
    else if (opticalShiftEnum.equals(OpticalShiftNameEnum.WHITE_LIGHT))
      _pspHardwareModuleVersion1.displayWhiteLight(moduleEnum);
    else
      Assert.expect(false, "Invalid shift name enum"); 
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void handleActualOpticalImageAcquisition(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalShiftEnum != null);
    
    _pspHardwareModuleVersion1.sendTriggerSequence(moduleEnum, opticalShiftEnum);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void handlePostOpticalImageAcquisition(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    _pspHardwareModuleVersion1.shutdownProjector(moduleEnum);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void performCalibration() throws XrayTesterException 
  {    
    // Setup stage position
    setupOpticalCalibrationStagePosition();
    
    // Start calibration process
    for(PspModuleEnum moduleEnum : PspModuleEnum.getAllEnums())
    {
      // Kok Chun, Tan - XCR2490 - 24/2/2014
      // Check the optical camera used is enable or not. If not enable, skip the rectangle.
      if (SurfaceMapping.getInstance().isOpticalCameraEnable(moduleEnum) == false)
      {
        continue;
      }
      
      Map<OpticalCalibrationPlaneEnum, StagePositionMutable> opticalCalibrationEnumToStagePositionMap = _pspModuleToStagePositionMap.get(moduleEnum) ;
      OpticalCameraIdEnum opticalCameraIdEnum = _pspImageAcquisitionEngine.getOpticalCameraIdEnum(moduleEnum);
      
      for(OpticalCalibrationProfileEnum profileEnum : OpticalCalibrationProfileEnum.getAllEnums())
      {
        int index = 1;
        double firstObjectPlaneKnownHeight  = 0.0;
        double secondObjectPlaneKnownHeight = 0.0;
        
        // Initialize optical system
        OpticalCameraSetting opticalCameraSetting = new OpticalCameraSetting(PspImageAcquisitionEngine.getInstance().getFrontModuleCropImageOffsetXInPixel(),
                                                                             PspImageAcquisitionEngine.getInstance().getRearModuleCropImageOffsetXInPixel());
        _pspImageAcquisitionEngine.initializeOpticalCamera(moduleEnum, opticalCameraSetting);
        
        OpticalCalibrationData opticalCalibrationData = new OpticalCalibrationData();
        opticalCalibrationData.setPspModuleEnum(moduleEnum);
        opticalCalibrationData.setProfileEnum(profileEnum);
        opticalCalibrationData.setTotalRun(TOTAL_CALIBRATION_RUN);
        
        // If there is no directory created for log/calib/OpticalCalibration, 
        // system will create manually
        if (FileUtilAxi.existsDirectory(Directory.getOpticalCalibrationCalLogDir()) == false)
        {
          FileUtilAxi.createDirectory(Directory.getOpticalCalibrationCalLogDir());
        }
        
        if (profileEnum.equals(OpticalCalibrationProfileEnum.PROFILE_1))
        {
          // Reference Plane
          index = 1;
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                                  OpticalShiftNameEnum.SHIFT_ONE,
                                                  FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                                  OpticalShiftNameEnum.SHIFT_TWO,
                                                  FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                                  OpticalShiftNameEnum.SHIFT_THREE,
                                                  FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                                  OpticalShiftNameEnum.SHIFT_FOUR,
                                                  FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          
          // Minus Two Plane
          index = 1;
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO),
                                                  OpticalShiftNameEnum.SHIFT_ONE,
                                                  FileName.getOpticalCalibrationObjectFringeMinusTwoImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO),
                                                  OpticalShiftNameEnum.SHIFT_TWO,
                                                  FileName.getOpticalCalibrationObjectFringeMinusTwoImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO),
                                                  OpticalShiftNameEnum.SHIFT_THREE,
                                                  FileName.getOpticalCalibrationObjectFringeMinusTwoImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO),
                                                  OpticalShiftNameEnum.SHIFT_FOUR,
                                                  FileName.getOpticalCalibrationObjectFringeMinusTwoImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          
          // Plus Three Plane
          index = 1;
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                                  OpticalShiftNameEnum.SHIFT_ONE,
                                                  FileName.getOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                                  OpticalShiftNameEnum.SHIFT_TWO,
                                                  FileName.getOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                                  OpticalShiftNameEnum.SHIFT_THREE,
                                                  FileName.getOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                                  OpticalShiftNameEnum.SHIFT_FOUR,
                                                  FileName.getOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          
          firstObjectPlaneKnownHeight = PROFILE_1_REFERENCE_PLANE_KNOWN_HEIGHT;
          secondObjectPlaneKnownHeight = PROFILE_1_OBJECT_PLANE_PLUS_THREE_KNOWN_HEIGHT;
        }
        else if (profileEnum.equals(OpticalCalibrationProfileEnum.PROFILE_2))
        {
          // Reference Plane
          index = 1;
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                                  OpticalShiftNameEnum.SHIFT_ONE,
                                                  FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                                  OpticalShiftNameEnum.SHIFT_TWO,
                                                  FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                                  OpticalShiftNameEnum.SHIFT_THREE,
                                                  FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                                  OpticalShiftNameEnum.SHIFT_FOUR,
                                                  FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          
          // Minus One Plane
          index = 1;
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE),
                                                  OpticalShiftNameEnum.SHIFT_ONE,
                                                  FileName.getOpticalCalibrationObjectFringeMinusOneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE),
                                                  OpticalShiftNameEnum.SHIFT_TWO,
                                                  FileName.getOpticalCalibrationObjectFringeMinusOneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE),
                                                  OpticalShiftNameEnum.SHIFT_THREE,
                                                  FileName.getOpticalCalibrationObjectFringeMinusOneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE),
                                                  OpticalShiftNameEnum.SHIFT_FOUR,
                                                  FileName.getOpticalCalibrationObjectFringeMinusOneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          
          // Plus Four Plane
          index = 1;
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR),
                                                  OpticalShiftNameEnum.SHIFT_ONE,
                                                  FileName.getOpticalCalibrationObjectFringePlusFourImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR),
                                                  OpticalShiftNameEnum.SHIFT_TWO,
                                                  FileName.getOpticalCalibrationObjectFringePlusFourImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR),
                                                  OpticalShiftNameEnum.SHIFT_THREE,
                                                  FileName.getOpticalCalibrationObjectFringePlusFourImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          opticalCalibrationData.addImageFullPath(profileEnum,
                                                  OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR,
                                                  opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR),
                                                  OpticalShiftNameEnum.SHIFT_FOUR,
                                                  FileName.getOpticalCalibrationObjectFringePlusFourImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), profileEnum.getId(), index++, TOTAL_CALIBRATION_RUN));
          
          firstObjectPlaneKnownHeight = PROFILE_2_REFERENCE_PLANE_KNOWN_HEIGHT;
          secondObjectPlaneKnownHeight = PROFILE_2_OBJECT_PLANE_PLUS_FOUR_KNOWN_HEIGHT;
        }
        else
          Assert.expect(false, "Invald profile enum");
        
        opticalCalibrationData.setKnownHeight1(firstObjectPlaneKnownHeight);
        opticalCalibrationData.setKnownHeight2(secondObjectPlaneKnownHeight);
        
        String opticalCalibrationProfileDirectory = Directory.getOpticalCameraCalibProfileDir(getOpticalCameraId(moduleEnum).getId(), profileEnum.getId());
        opticalCalibrationData.setCalibrationDirectoryFullPath(opticalCalibrationProfileDirectory);
        
        // If there is no directory created, there will be no calibration data
        // generated, which caused PSP not functioning.
        if (FileUtilAxi.existsDirectory(opticalCalibrationProfileDirectory) == false)
        {
          FileUtilAxi.createDirectory(opticalCalibrationProfileDirectory);
        }
        
        _pspImageAcquisitionEngine.acquireCalibrationImage(opticalCalibrationData);
        _pspHardwareEngine.performCalibration(moduleEnum, opticalCalibrationData);
      }
    }
  }
  
  /**
   * Setup calibration plate stage position for later use
   * 
   * @author Cheah Lee Herng
   */
  private void setupOpticalCalibrationStagePosition() throws XrayTesterException
  {
    Assert.expect(_pspModuleToStagePositionMap != null);
    
    // Clear before proceed
    _pspModuleToStagePositionMap.clear();

    // Depending on system type, we will get different system offset
    int offsetXFromOpticalFiducialToReferencePlaneInNanometers      = PspConfiguration.getInstance().getOpticalFiducialOffsetXOnReferencePlaneInNanometers();
    int offsetXFromOpticalFiducialToMinusTwoPlaneInNanometers       = PspConfiguration.getInstance().getOpticalFiducialOffsetXOnMinusTwoPlaneInNanometers();
    int offsetXFromOpticalFiducialToPlusThreePlaneInNanometers      = PspConfiguration.getInstance().getOpticalFiducialOffsetXOnPlusThreePlaneInNanometers();
    int offsetXFromOpticalFiducialToMinusOnePlaneInNanometers       = PspConfiguration.getInstance().getOpticalFiducialOffsetXOnMinusOnePlaneInNanometers();
    int offsetXFromOpticalFiducialToPlusFourPlaneInNanometers       = PspConfiguration.getInstance().getOpticalFiducialOffsetXOnPlusFourPlaneInNanometers();

    int opticalCamera1OffsetYFromOpticalFiducialToPlaneInNanometers = PspConfiguration.getInstance().getFrontModuleOpticalFiducialOffsetYInNanometers();
    int opticalCamera2OffsetYFromOpticalFiducialToPlaneInNanometers = PspConfiguration.getInstance().getRearModuleOpticalFiducialOffsetYInNanometers();
    
    for(PspModuleEnum moduleEnum : PspModuleEnum.getAllEnums())
    {
      Map<OpticalCalibrationPlaneEnum, StagePositionMutable> calMagnificationToStagePositionMap = new HashMap<OpticalCalibrationPlaneEnum, StagePositionMutable>();
      Object object = _pspModuleToStagePositionMap.put(moduleEnum, calMagnificationToStagePositionMap);
      Assert.expect(object == null);

      if (moduleEnum.equals(PspModuleEnum.FRONT))
      {
        StagePositionMutable opticalCalibrationReferencePlaneStagePositionForOpticalCamera1 = new StagePositionMutable();
        opticalCalibrationReferencePlaneStagePositionForOpticalCamera1.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers() - offsetXFromOpticalFiducialToReferencePlaneInNanometers);
        opticalCalibrationReferencePlaneStagePositionForOpticalCamera1.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                        (PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers() + opticalCamera1OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.REFERENCE_PLANE, opticalCalibrationReferencePlaneStagePositionForOpticalCamera1);
        Assert.expect(object == null);

        StagePositionMutable opticalCalibrationObjectPlane1StagePositionForOpticalCamera1 = new StagePositionMutable();
        opticalCalibrationObjectPlane1StagePositionForOpticalCamera1.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers() - offsetXFromOpticalFiducialToMinusTwoPlaneInNanometers);
        opticalCalibrationObjectPlane1StagePositionForOpticalCamera1.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                      (PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers() + opticalCamera1OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO, opticalCalibrationObjectPlane1StagePositionForOpticalCamera1);
        Assert.expect(object == null);

        StagePositionMutable opticalCalibrationObjectPlane2StagePositionForOpticalCamera1 = new StagePositionMutable();
        opticalCalibrationObjectPlane2StagePositionForOpticalCamera1.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers() - offsetXFromOpticalFiducialToPlusThreePlaneInNanometers);
        opticalCalibrationObjectPlane2StagePositionForOpticalCamera1.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                      (PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers() + opticalCamera1OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE, opticalCalibrationObjectPlane2StagePositionForOpticalCamera1);
        Assert.expect(object == null);

        StagePositionMutable opticalCalibrationObjectPlane3StagePositionForOpticalCamera1 = new StagePositionMutable();
        opticalCalibrationObjectPlane3StagePositionForOpticalCamera1.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers() + offsetXFromOpticalFiducialToMinusOnePlaneInNanometers);
        opticalCalibrationObjectPlane3StagePositionForOpticalCamera1.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                      (PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers() + opticalCamera1OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE, opticalCalibrationObjectPlane3StagePositionForOpticalCamera1);
        Assert.expect(object == null);

        StagePositionMutable opticalCalibrationObjectPlane4StagePositionForOpticalCamera1 = new StagePositionMutable();
        opticalCalibrationObjectPlane4StagePositionForOpticalCamera1.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers() + offsetXFromOpticalFiducialToPlusFourPlaneInNanometers);
        opticalCalibrationObjectPlane4StagePositionForOpticalCamera1.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                      (PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers() + opticalCamera1OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR, opticalCalibrationObjectPlane4StagePositionForOpticalCamera1);
        Assert.expect(object == null);

        if (Config.isDeveloperDebugModeOn())
        {
            System.out.println("CalOpticalCalibration: Camera 1: Ref Plane X = " + opticalCalibrationReferencePlaneStagePositionForOpticalCamera1.getXInNanometers());
            System.out.println("CalOpticalCalibration: Camera 1: Ref Plane Y = " + opticalCalibrationReferencePlaneStagePositionForOpticalCamera1.getYInNanometers());

            System.out.println("CalOpticalCalibration: Camera 1: Minus Two Plane X = " + opticalCalibrationObjectPlane1StagePositionForOpticalCamera1.getXInNanometers());
            System.out.println("CalOpticalCalibration: Camera 1: Minus Two Plane Y = " + opticalCalibrationObjectPlane1StagePositionForOpticalCamera1.getYInNanometers());

            System.out.println("CalOpticalCalibration: Camera 1: Plus Three Plane X = " + opticalCalibrationObjectPlane2StagePositionForOpticalCamera1.getXInNanometers());
            System.out.println("CalOpticalCalibration: Camera 1: Plus Three Plane Y = " + opticalCalibrationObjectPlane2StagePositionForOpticalCamera1.getYInNanometers());

            System.out.println("CalOpticalCalibration: Camera 1: Minus One Plane X = " + opticalCalibrationObjectPlane3StagePositionForOpticalCamera1.getXInNanometers());
            System.out.println("CalOpticalCalibration: Camera 1: Minus One Plane Y = " + opticalCalibrationObjectPlane3StagePositionForOpticalCamera1.getYInNanometers());

            System.out.println("CalOpticalCalibration: Camera 1: Plus Four Plane X = " + opticalCalibrationObjectPlane4StagePositionForOpticalCamera1.getXInNanometers());
            System.out.println("CalOpticalCalibration: Camera 1: Plus Four Plane Y = " + opticalCalibrationObjectPlane4StagePositionForOpticalCamera1.getYInNanometers());
        }
      }
      else if (moduleEnum.equals(PspModuleEnum.REAR))
      {
        StagePositionMutable opticalCalibrationReferencePlaneStagePositionForOpticalCamera2 = new StagePositionMutable();
        opticalCalibrationReferencePlaneStagePositionForOpticalCamera2.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers() - offsetXFromOpticalFiducialToReferencePlaneInNanometers);
        opticalCalibrationReferencePlaneStagePositionForOpticalCamera2.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                        (PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers() - opticalCamera2OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.REFERENCE_PLANE, opticalCalibrationReferencePlaneStagePositionForOpticalCamera2);
        Assert.expect(object == null);

        StagePositionMutable opticalCalibrationObjectPlane1StagePositionForOpticalCamera2 = new StagePositionMutable();
        opticalCalibrationObjectPlane1StagePositionForOpticalCamera2.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers() - offsetXFromOpticalFiducialToMinusTwoPlaneInNanometers);
        opticalCalibrationObjectPlane1StagePositionForOpticalCamera2.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                      (PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers() - opticalCamera2OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO, opticalCalibrationObjectPlane1StagePositionForOpticalCamera2);
        Assert.expect(object == null);

        StagePositionMutable opticalCalibrationObjectPlane2StagePositionForOpticalCamera2 = new StagePositionMutable();
        opticalCalibrationObjectPlane2StagePositionForOpticalCamera2.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers() - offsetXFromOpticalFiducialToPlusThreePlaneInNanometers);
        opticalCalibrationObjectPlane2StagePositionForOpticalCamera2.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                      (PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers() - opticalCamera2OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE, opticalCalibrationObjectPlane2StagePositionForOpticalCamera2);
        Assert.expect(object == null);

        StagePositionMutable opticalCalibrationObjectPlane3StagePositionForOpticalCamera2 = new StagePositionMutable();
        opticalCalibrationObjectPlane3StagePositionForOpticalCamera2.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers() + offsetXFromOpticalFiducialToMinusOnePlaneInNanometers);
        opticalCalibrationObjectPlane3StagePositionForOpticalCamera2.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                      (PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers() - opticalCamera2OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE, opticalCalibrationObjectPlane3StagePositionForOpticalCamera2);
        Assert.expect(object == null);

        StagePositionMutable opticalCalibrationObjectPlane4StagePositionForOpticalCamera2 = new StagePositionMutable();
        opticalCalibrationObjectPlane4StagePositionForOpticalCamera2.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers() + offsetXFromOpticalFiducialToPlusFourPlaneInNanometers);
        opticalCalibrationObjectPlane4StagePositionForOpticalCamera2.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                      (PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers() - opticalCamera2OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR, opticalCalibrationObjectPlane4StagePositionForOpticalCamera2);
        Assert.expect(object == null);

        if (Config.isDeveloperDebugModeOn())
        {
            System.out.println("CalOpticalCalibration: Camera 2: Ref Plane X = " + opticalCalibrationReferencePlaneStagePositionForOpticalCamera2.getXInNanometers());
            System.out.println("CalOpticalCalibration: Camera 2: Ref Plane Y = " + opticalCalibrationReferencePlaneStagePositionForOpticalCamera2.getYInNanometers());

            System.out.println("CalOpticalCalibration: Camera 2: Minus Two Plane X = " + opticalCalibrationObjectPlane1StagePositionForOpticalCamera2.getXInNanometers());
            System.out.println("CalOpticalCalibration: Camera 2: Minus Two Plane Y = " + opticalCalibrationObjectPlane1StagePositionForOpticalCamera2.getYInNanometers());

            System.out.println("CalOpticalCalibration: Camera 2: Plus Three Plane X = " + opticalCalibrationObjectPlane2StagePositionForOpticalCamera2.getXInNanometers());
            System.out.println("CalOpticalCalibration: Camera 2: Plus Three Plane Y = " + opticalCalibrationObjectPlane2StagePositionForOpticalCamera2.getYInNanometers());

            System.out.println("CalOpticalCalibration: Camera 2: Minus One Plane X = " + opticalCalibrationObjectPlane3StagePositionForOpticalCamera2.getXInNanometers());
            System.out.println("CalOpticalCalibration: Camera 2: Minus One Plane Y = " + opticalCalibrationObjectPlane3StagePositionForOpticalCamera2.getYInNanometers());

            System.out.println("CalOpticalCalibration: Camera 2: Plus Four Plane X = " + opticalCalibrationObjectPlane4StagePositionForOpticalCamera2.getXInNanometers());
            System.out.println("CalOpticalCalibration: Camera 2: Plus Four Plane Y = " + opticalCalibrationObjectPlane4StagePositionForOpticalCamera2.getYInNanometers());
        }
      }
    }         
  }

  /**
   * @author Cheah Lee Herng 
   */
  protected OpticalInspectionData setupHeightMapInspectionData(OpticalPointToPointScan opticalPointToPointScan, String projectName, String opticalImageSetName) 
  {
    Assert.expect(opticalPointToPointScan != null);
    Assert.expect(projectName != null);
    Assert.expect(opticalImageSetName != null);
    
    StagePosition stagePosition = opticalPointToPointScan.getPointToPointStagePositionInNanoMeters();
    PspModuleEnum pspModuleEnum = getPspModuleEnum(opticalPointToPointScan.getOpticalCameraIdEnum());
    
    OpticalInspectionData opticalInspectionData = new OpticalInspectionData();
    opticalInspectionData.setPspModuleEnum(pspModuleEnum);
    opticalInspectionData.setForceTrigger(false);
    opticalInspectionData.setOpticalCalibrationProfileEnum(opticalPointToPointScan.getOpticalCalibrationProfileEnum());
    opticalInspectionData.setReturnActualHeightMap(opticalPointToPointScan.getReturnActualHeightMap());
    
    String opticalImageName = opticalPointToPointScan.getPointPositionName();

    // Setup stage position
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_ONE, FileName.getOpticalImageFullPath(projectName, opticalImageSetName, opticalImageName, 1));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_TWO, FileName.getOpticalImageFullPath(projectName, opticalImageSetName, opticalImageName, 2));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_THREE, FileName.getOpticalImageFullPath(projectName, opticalImageSetName, opticalImageName, 3));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_FOUR, FileName.getOpticalImageFullPath(projectName, opticalImageSetName, opticalImageName, 4));
    
    return opticalInspectionData;
  }

  /**
   * This function calculates the final Z Height.
   * 
   * @author Cheah Lee Herng
   */
  protected void setZHeightInNanometers(OpticalPointToPointScan opticalPointToPointScan, PspModuleEnum moduleEnum) throws XrayTesterException
  {
    Assert.expect(opticalPointToPointScan != null);
    Assert.expect(moduleEnum != null);

    if (_pspHardwareEngine.isHeightMapAvailable(moduleEnum))
    {
      float[] newCoordinateXInPixelFromPsp = _pspHardwareEngine.getNewCoordinateXInPixel(moduleEnum);
      float[] newCoordinateYInPixelFromPsp = _pspHardwareEngine.getNewCoordinateYInPixel(moduleEnum);
      float[] zHeightArrayInMilimetersFromPsp = _pspHardwareEngine.getHeightMapValue(moduleEnum);

      // Sanity check to make sure return length is correct
      Assert.expect(newCoordinateXInPixelFromPsp.length > 0);
      Assert.expect(newCoordinateYInPixelFromPsp.length > 0);
      Assert.expect(zHeightArrayInMilimetersFromPsp.length > 0);
      Assert.expect(newCoordinateXInPixelFromPsp.length == newCoordinateYInPixelFromPsp.length);
      Assert.expect(newCoordinateXInPixelFromPsp.length == zHeightArrayInMilimetersFromPsp.length);
      Assert.expect(newCoordinateYInPixelFromPsp.length == zHeightArrayInMilimetersFromPsp.length);

      // PSP Height Outlier checking      
      Map<Integer,Pair<FloatRef,BooleanRef>> zHeightInMilimetersFromPspAfterOutlierRemovalMap = new LinkedHashMap<Integer,Pair<FloatRef,BooleanRef>>();
      float median = StatisticsUtil.median(zHeightArrayInMilimetersFromPsp);          
      float min = median - (0.10f * median);
      float max = median + (0.10f * median);
      boolean allValuesOutsideQuartileRange = true;

      for(int counter = 0; counter < zHeightArrayInMilimetersFromPsp.length; counter++)
      {
        if ((zHeightArrayInMilimetersFromPsp[counter] < max) && (zHeightArrayInMilimetersFromPsp[counter] > min))
        {
          Pair<FloatRef,BooleanRef> zHeightInfoPair = new Pair<FloatRef,BooleanRef>(new FloatRef(zHeightArrayInMilimetersFromPsp[counter]), 
                                                                                    new BooleanRef(true));
          zHeightInMilimetersFromPspAfterOutlierRemovalMap.put(counter, zHeightInfoPair);
          allValuesOutsideQuartileRange = false;
        }
        else
        {
          Pair<FloatRef,BooleanRef> zHeightInfoPair = new Pair<FloatRef,BooleanRef>(new FloatRef(zHeightArrayInMilimetersFromPsp[counter]), 
                                                                                    new BooleanRef(false));
          zHeightInMilimetersFromPspAfterOutlierRemovalMap.put(counter, zHeightInfoPair);
        }
      }
      
      // If all values lies outside of Median, promote Median value as the z-height from PSP engine.
      if (allValuesOutsideQuartileRange)
      {
        Pair<FloatRef,BooleanRef> zHeightInfoPair = zHeightInMilimetersFromPspAfterOutlierRemovalMap.get(zHeightArrayInMilimetersFromPsp.length - 1);
        zHeightInfoPair.getFirst().setValue(median);
        zHeightInfoPair.getSecond().setValue(true);
      }

      int counter = 0;
      for(Map.Entry<String,DoubleRef> entry : opticalPointToPointScan.getPointPanelPositionNameToZHeightInNanometers().entrySet())
      {
        boolean isZHeightValid = zHeightInMilimetersFromPspAfterOutlierRemovalMap.get(counter).getSecond().getValue();        
        float zHeightInMilimetersFromPsp = zHeightInMilimetersFromPspAfterOutlierRemovalMap.get(counter).getFirst().getValue();
        
        float zHeightInMilimetersFromPspBeforeOutlierRemoval = zHeightArrayInMilimetersFromPsp[counter];
        double zHeightInNanometersFromPsp = MathUtil.convertMillimetersToNanometers(zHeightInMilimetersFromPsp);
        double finalZHeightInNanometers = 0;

        if (isZHeightValid)
        {
          if (isReturnRawPspHeight() == false)
            finalZHeightInNanometers = zHeightInNanometersFromPsp + getOffset(moduleEnum);
          else
            finalZHeightInNanometers = zHeightInNanometersFromPsp;
        }
        
        String pointPanelPositionName = entry.getKey();
        
        // Set Z-Height info
        opticalPointToPointScan.setPointPanelPositionZHeight(pointPanelPositionName, finalZHeightInNanometers);
        
        // Set Z-Height Valid
        opticalPointToPointScan.setPointPanelPositionZHeightValid(pointPanelPositionName, isZHeightValid);

        if (newCoordinateXInPixelFromPsp[counter] < 0 || newCoordinateYInPixelFromPsp[counter] < 0)
        {
          Pair<Integer,Integer> pointPositionInPixel = opticalPointToPointScan.getPointPositionInPixel(pointPanelPositionName);
          newCoordinateXInPixelFromPsp[counter] = pointPositionInPixel.getFirst().intValue();
          newCoordinateYInPixelFromPsp[counter] = pointPositionInPixel.getSecond().intValue();
        }

        // Add new coordinate
        opticalPointToPointScan.addPointPanelPositionNameToNewPointPositionInPixel(pointPanelPositionName, 
                                                                                   (int)newCoordinateXInPixelFromPsp[counter], 
                                                                                   (int)newCoordinateYInPixelFromPsp[counter]);

        // PSP height information Logging purpose
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
        {
          if (isZHeightValid)
          {
            System.out.println("PspModuleVersion1: Camera " + opticalPointToPointScan.getOpticalCameraIdEnum().getId() + ": Calculated Z Height from PSP Algorithm for pixel coordinate (" + 
                                        (int)newCoordinateXInPixelFromPsp[counter] + ", " + (int)newCoordinateYInPixelFromPsp[counter] + 
                                        ") is " + zHeightInMilimetersFromPspBeforeOutlierRemoval + " mm / " + MathUtil.convertMillimetersToNanometers(zHeightInMilimetersFromPspBeforeOutlierRemoval) + " nm.");
            System.out.println("PspModuleVersion1: Camera " + opticalPointToPointScan.getOpticalCameraIdEnum().getId() + ": Final Z Height for pixel coordinate (" + 
                                        (int)newCoordinateXInPixelFromPsp[counter] + ", " + (int)newCoordinateYInPixelFromPsp[counter] + 
                                        ") is " + MathUtil.convertNanometersToMillimeters(finalZHeightInNanometers) + " mm / " + finalZHeightInNanometers + " nm.");
          }
          else
          {
            System.out.println("PspModuleVersion1: Camera " + opticalPointToPointScan.getOpticalCameraIdEnum().getId() + ": Calculated Z Height from PSP Algorithm for pixel coordinate (" + 
                                        (int)newCoordinateXInPixelFromPsp[counter] + ", " + (int)newCoordinateYInPixelFromPsp[counter] + 
                                        ") is " + zHeightInMilimetersFromPspBeforeOutlierRemoval + " mm / " + MathUtil.convertMillimetersToNanometers(zHeightInMilimetersFromPspBeforeOutlierRemoval) + " nm.");
            System.out.println("PspModuleVersion1: Camera " + opticalPointToPointScan.getOpticalCameraIdEnum().getId() + ": No final Z Height info for pixel coordinate (" + 
                                        (int)newCoordinateXInPixelFromPsp[counter] + ", " + (int)newCoordinateYInPixelFromPsp[counter] + ")");
          }
        }                
        
        ++counter;
      }
      
      // Calculate Average Z-Height
      opticalPointToPointScan.calculateAverageZHeightInNanometer();
      
      // Clean up
      zHeightInMilimetersFromPspAfterOutlierRemovalMap.clear();
    }
    else
        System.out.println("No Height Map available.");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  protected OpticalInspectionData setupOfflineHeightMapInspectionData(OpticalPointToPointScan opticalPointToPointScan, String projectName)
  {
    Assert.expect(opticalPointToPointScan != null);
    Assert.expect(projectName != null);    
    
    StagePosition stagePosition = opticalPointToPointScan.getPointToPointStagePositionInNanoMeters();
    PspModuleEnum pspModuleEnum = getPspModuleEnum(opticalPointToPointScan.getOpticalCameraIdEnum());
    
    OpticalInspectionData opticalInspectionData = new OpticalInspectionData();
    opticalInspectionData.setPspModuleEnum(pspModuleEnum);

    if (pspModuleEnum.equals(PspModuleEnum.FRONT))
      opticalInspectionData.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_1);
    else
      opticalInspectionData.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_2);
    
    String opticalImageName = opticalPointToPointScan.getPointPositionName();

    // Setup stage position
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_ONE, FileName.getOfflineOpticalImageFullPath(projectName, opticalImageName, 1));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_TWO, FileName.getOfflineOpticalImageFullPath(projectName, opticalImageName, 2));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_THREE, FileName.getOfflineOpticalImageFullPath(projectName, opticalImageName, 3));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_FOUR, FileName.getOfflineOpticalImageFullPath(projectName, opticalImageName, 4));
    
    return opticalInspectionData;
  }

  /**
   * @author Cheah Lee Herng 
   */
  protected OpticalInspectionData setupAlignmentHeightMapInspectionData(OpticalPointToPointScan opticalPointToPointScan)
  {
    Assert.expect(opticalPointToPointScan != null);    
    
    StagePosition stagePosition = opticalPointToPointScan.getPointToPointStagePositionInNanoMeters();
    PspModuleEnum pspModuleEnum = getPspModuleEnum(opticalPointToPointScan.getOpticalCameraIdEnum());
    
    OpticalInspectionData opticalInspectionData = new OpticalInspectionData();
    opticalInspectionData.setPspModuleEnum(pspModuleEnum);
    opticalInspectionData.setForceTrigger(false);
    opticalInspectionData.setOpticalCalibrationProfileEnum(opticalPointToPointScan.getOpticalCalibrationProfileEnum());
    opticalInspectionData.setReturnActualHeightMap(opticalPointToPointScan.getReturnActualHeightMap());
    
    String opticalImageName = opticalPointToPointScan.getPointPositionName();

    // Setup stage position
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_ONE,   FileName.getAlignmentOpticalImageFullPath(opticalImageName, 1));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_TWO,   FileName.getAlignmentOpticalImageFullPath(opticalImageName, 2));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_THREE, FileName.getAlignmentOpticalImageFullPath(opticalImageName, 3));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_FOUR,  FileName.getAlignmentOpticalImageFullPath(opticalImageName, 4));
    
    return opticalInspectionData;
  }
}
