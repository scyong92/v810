package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Allows asking for projections smaller than the minimum size. This handles
 * all the overhead of actualy asking for the minimum and then cropping the
 * image down to what the original request was. It also adjusts the stage
 * X & Y values and others as if you could ask for the too-small-region.
 *
 * @author Eddie Williamson
 */
public class VirtualProjection
{

  private static Image _SMALL_IMAGE = new Image(1,1);
  private int _minimumScanLengthInNanometers;

  private AbstractXrayCamera _camera;

  // Original requested rectangle
  private int _bottomLeftXNanometers;
  private int _bottomLeftYNanometers;
  private int _originalWidthNanometers;
  private int _originalHeightNanometers;

  private int _originalWidthInPixels;
  private int _originalHeightInPixels;

  private int _additionalRightSideWidthNanometers;
  private int _additionalScanHeightNanometers;

  private int _projectionRequestHeight;
  private int _projectionRequestWidth;
  private ProjectionRequestThreadTask _projectionRequestThreadTask;
  private Image _croppedImage;

  static private XrayTester _xrayTester = null;


  /**
   * @todo eddie Assumes imaging lower rail. Need to be smarter for adjustable rail images.
   * @author Eddie Williamson
   */
  public VirtualProjection(AbstractXrayCamera camera,
                           ScanPassDirectionEnum stageDirectionEnum,
                           int bottomLeftXNanometers,
                           int bottomLeftYNanometers,
                           int widthNanometers,
                           int heightNanometers)
  {
    this(camera,
         stageDirectionEnum,
         bottomLeftXNanometers,
         bottomLeftYNanometers,
         widthNanometers,
         heightNanometers,
         0);
  }


  /**
   * @todo eddie Assumes imaging lower rail. Need to be smarter for adjustable rail images.
   * @author Eddie Williamson
   */
  public VirtualProjection(AbstractXrayCamera camera,
                           ScanPassDirectionEnum stageDirectionEnum,
                           int bottomLeftXNanometers,
                           int bottomLeftYNanometers,
                           int widthNanometers,
                           int heightNanometers,
                           int additionalRightSideWidthNanometers)
  {
    Assert.expect(camera != null);
    if (_xrayTester == null)
    {
      _xrayTester = XrayTester.getInstance();
    }
    if (UnitTest.unitTesting())
    {
      // using hard coded value for unit testing because pre-recorded image files
      // are exactly this length.  And, no hardware is involved in these unit tests.
      _minimumScanLengthInNanometers = XrayCameraArray.getOneInchInNanometersAlignedUpToPixelBoundary();;
    }
    else
    {
      _minimumScanLengthInNanometers = MechanicalConversions.getMinimumStageTravelInNanometers();
    }
    int nominalPixelSizeInNanometers = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();

    _camera = camera;
    _bottomLeftXNanometers = bottomLeftXNanometers;
    _bottomLeftYNanometers = bottomLeftYNanometers;
    _originalWidthNanometers = widthNanometers;
    _originalHeightNanometers = heightNanometers;
    _additionalRightSideWidthNanometers = additionalRightSideWidthNanometers;

    /** @todo eddie Change this to use standard conversion method */
    _originalWidthInPixels = _originalWidthNanometers / nominalPixelSizeInNanometers;
    _originalHeightInPixels = _originalHeightNanometers / nominalPixelSizeInNanometers;

    _additionalScanHeightNanometers = _minimumScanLengthInNanometers - _originalHeightNanometers;
    _additionalScanHeightNanometers = Math.max(_additionalScanHeightNanometers, 0); // Only pad scan height if less than minimum
    _projectionRequestHeight = _originalHeightNanometers;
    _projectionRequestWidth = _originalWidthNanometers + _additionalRightSideWidthNanometers;

    Assert.expect(_projectionRequestWidth <= camera.getNumberOfSensorPixelsUsed() * nominalPixelSizeInNanometers);

    SystemFiducialRectangle systemFiducialRectangle = new SystemFiducialRectangle(_bottomLeftXNanometers,
                                                                                  _bottomLeftYNanometers,
                                                                                  _projectionRequestWidth,
                                                                                  _projectionRequestHeight);
    _projectionRequestThreadTask = new ProjectionRequestThreadTask(_camera,
                                                                   stageDirectionEnum,
                                                                   systemFiducialRectangle,
                                                                   _additionalScanHeightNanometers);
  }

  /**
   * @author Eddie Williamson
   */
  public ProjectionRequestThreadTask getProjectionRequestThreadTask()
  {
    return _projectionRequestThreadTask;
  }

  /**
   * @author Eddie Williamson
   */
  public AbstractXrayCamera getCamera()
  {
    return _camera;
  }

  /**
   * @return The image of the originally requested rectangle. This is the image returned
   * from IAE minus the _additionalRightSideWidthNanometers.
   * @author Eddie Williamson
   */
  public Image getImage()
  {
    Assert.expect(_projectionRequestThreadTask != null);

    if (_croppedImage == null)
    {
      int startingRow = 0;
      int startingCol = 0;
      int rotation = 0;
      RegionOfInterest roi = new RegionOfInterest(startingCol,
                                                  startingRow,
                                                  _originalWidthInPixels,
                                                  _originalHeightInPixels,
                                                  rotation,
                                                  RegionShapeEnum.RECTANGULAR);
      /** @todo PWL to Eddie: This code is confusing -- why is the projection image being overwritten with this small image? */
      _croppedImage = Image.createCopy(_projectionRequestThreadTask.getProjectionInfo().getProjectionImage(), roi);
      _projectionRequestThreadTask.getProjectionInfo().setProjection(_SMALL_IMAGE);
    }
    return _croppedImage;
  }

  /**
   * @author Patrick Lacz
   */
  public void clear()
  {
    if (_croppedImage != null)
    {
      _croppedImage.decrementReferenceCount();
      _croppedImage = null;
    }

    _projectionRequestThreadTask.clear();
  }


  /**
   * @author Eddie Williamson
   */
  public int getStageX()
  {
    Assert.expect(_projectionRequestThreadTask != null);

    return _projectionRequestThreadTask.getProjectionInfo().getXStageWhereFirstRowImagedOnVerticalCenterOfCamera();
  }


  /**
   * @return The stage Y position if we had just imaged the requested rectangle.
   * @author Eddie Williamson
   */
  public int getStageY()
  {
    Assert.expect(_projectionRequestThreadTask != null);

    return _projectionRequestThreadTask.getProjectionInfo().getYStageWhereFirstRowImagedOnVerticalCenterOfCamera()
        + _additionalScanHeightNanometers;
  }


  /**
   * @author Eddie Williamson
   */
  public int getCameraColumnWhereImageStarts()
  {
    Assert.expect(_projectionRequestThreadTask != null);

    return _projectionRequestThreadTask.getProjectionInfo().getImageRectangleForRequestedProjection().getMinX();
  }

}











