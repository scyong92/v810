package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class MarkReconstructionRegionCompleteThreadTask extends ThreadTask<Object>
{
  private ImageReconstructionEngine _imageReconstructionEngine = null;
  private int                       _testSubProgramId       = -1;
  private int                       _reconstructionRegionId = -1;
  private boolean                   _hasTestFailure         = false;
  private boolean                   _cancelled              = false;


  /**
   * @author Roy Williams
   */
  public MarkReconstructionRegionCompleteThreadTask(ImageReconstructionEngine imageReconstructionEngine,
                                                    int testSubProgramId,
                                                    int reconstructionRegionId,
                                                    boolean hasTestFailure)
  {
    super("MarkReconstructionRegionCompleteThreadTask:" + imageReconstructionEngine.getId());

    Assert.expect(imageReconstructionEngine != null);
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(reconstructionRegionId >= 0);

    _imageReconstructionEngine = imageReconstructionEngine;
    _testSubProgramId          = testSubProgramId;
    _reconstructionRegionId    = reconstructionRegionId;
    _hasTestFailure            = hasTestFailure;
  }

  /**
   * @author Roy Williams
   */
  public Object executeTask() throws Exception
  {
//    ImageReconstructionMessage.throwException = true;
    _imageReconstructionEngine.reconstructionRegionComplete(_testSubProgramId, _reconstructionRegionId, _hasTestFailure);
    return null;
  }

  /**
   * @author Roy Williams
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Roy Williams
   */
  protected void cancel() throws XrayTesterException
  {
    _cancelled = true;
  }
}
