package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class PauseReconstructionThreadTask extends ThreadTask<Object>
{
  private ImageReconstructionEngine _imageReconstructionEngine = null;
  private boolean                   _cancelled                 = false;

  /**
   * @author Roy Williams
   */
  public PauseReconstructionThreadTask(ImageReconstructionEngine imageReconstructionEngine)
  {
    super("PauseReconstructionThreadTask:" + imageReconstructionEngine.getId());

    Assert.expect(imageReconstructionEngine != null);

    _imageReconstructionEngine = imageReconstructionEngine;
  }

  /**
   * @author Roy Williams
   */
  public Object executeTask() throws Exception
  {
    _imageReconstructionEngine.pauseReconstruction();
    return null;
  }

  /**
   * @author Roy Williams
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Roy Williams
   */
  protected void cancel() throws XrayTesterException
  {
    _cancelled = true;
  }
}
