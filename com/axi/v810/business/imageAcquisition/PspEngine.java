package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This is PSP Engine class in Business layer which handles all the PSP-related logic.
 * It handles the calling to PSP Hardware Engine.
 * 
 * @author Cheah Lee Herng
 */
public abstract class PspEngine 
{  
  private static Map<PspModuleVersionEnum, PspEngine> _versionToInstanceMap;
  
  protected static Config _config;
  
  protected PspHardwareEngine _pspHardwareEngine;
  protected PspImageAcquisitionEngine _pspImageAcquisitionEngine;
  
  protected boolean _returnRawPspHeight = false;  // Raw Psp Height is the height value from OpticalCamera (without offset)
  protected boolean _filterOutlier = true; // Special flag to indicate if we need to filter outlier Z-Height. This is used in Optical Repeatability task.
  
  static
  {
    _config = Config.getInstance();
    _versionToInstanceMap = new HashMap<PspModuleVersionEnum, PspEngine>();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized static PspEngine getInstance()
  {    
    PspEngine instance = null;
    
    PspModuleVersionEnum versionEnum = getPspModuleVersionEnum();
    
    if (_versionToInstanceMap.containsKey(versionEnum))
    {
      instance = _versionToInstanceMap.get(versionEnum);
    }
    else
    {
      if (versionEnum.equals(PspModuleVersionEnum.VERSION_1))
      {
        instance = new PspModuleVersion1();
      }
      else if (versionEnum.equals(PspModuleVersionEnum.VERSION_2))
      {
        instance = new PspModuleVersion2();
      }        
      else
        Assert.expect(false);
    }

    Assert.expect(instance != null);
    _versionToInstanceMap.put(versionEnum, instance);

    return instance;
  }
  
  /**
   * Default Constructor
   * 
   * @author Cheah Lee Herng
   */
  protected PspEngine()
  {
    _pspHardwareEngine = PspHardwareEngine.getInstance();
    _pspImageAcquisitionEngine = PspImageAcquisitionEngine.getInstance();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public PspModuleEnum getPspModuleEnum(OpticalCameraIdEnum cameraId)
  {
    Assert.expect(cameraId != null);
    
    PspModuleEnum moduleEnum = null;
    if (cameraId.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1))
      moduleEnum = PspModuleEnum.FRONT;
    else
      moduleEnum = PspModuleEnum.REAR;
    return moduleEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public OpticalCameraIdEnum getOpticalCameraId(PspModuleEnum moduleEnum)
  {
    Assert.expect(moduleEnum != null);
    
    OpticalCameraIdEnum cameraId = null;
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      cameraId = OpticalCameraIdEnum.OPTICAL_CAMERA_1;
    else
      cameraId = OpticalCameraIdEnum.OPTICAL_CAMERA_2;
    return cameraId;
  }
  
  /**
    * @author Cheah Lee Herng
    */
  public static PspModuleVersionEnum getPspModuleVersionEnum()
  {
    int version = _config.getIntValue(HardwareConfigEnum.PSP_MODULE_VERSION);
    return PspModuleVersionEnum.getEnum(version);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static PspCalibrationVersionEnum getPspCalibrationVersionEnum()
  {
    int version = _config.getIntValue(HardwareConfigEnum.PSP_CALIBRATION_VERSION);
    return PspCalibrationVersionEnum.getEnum(version);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static boolean isVersion1()
  {
    PspModuleVersionEnum versionEnum = getPspModuleVersionEnum();
    if (versionEnum.equals(PspModuleVersionEnum.VERSION_1))
      return true;
    else
      return false;
  }
 
  /**
   * @author Cheah Lee Herng 
   */
  public void abort() throws XrayTesterException
  {
    _pspImageAcquisitionEngine.abort();
  }
  
  /**
   * This is a main function to calculate height map. It handles image acquisition
   * and the height map calculation.
   * 
   * @author Cheah Lee Herng 
   */
  public void performHeightMapOperation(OpticalPointToPointScan opticalPointToPointScan, String projectName, String opticalImageSetName) throws XrayTesterException 
  {
    performHeightMapOperation(opticalPointToPointScan, projectName, opticalImageSetName, true);
  }
  
  /**
   * This is a main function to calculate height map. It handles image acquisition
   * and the height map calculation.
   * 
   * @author Cheah Lee Herng 
   */
  public void performHeightMapOperation(OpticalPointToPointScan opticalPointToPointScan, String projectName, String opticalImageSetName, boolean moveStage) throws XrayTesterException 
  {
    Assert.expect(opticalPointToPointScan != null);
    Assert.expect(projectName != null);
    Assert.expect(opticalImageSetName != null);
    
    // Initialize system for first time run
    _pspImageAcquisitionEngine.initialize(getPspModuleEnum(opticalPointToPointScan.getOpticalCameraIdEnum()), opticalPointToPointScan);
    
    OpticalInspectionData opticalInspectionData = setupHeightMapInspectionData(opticalPointToPointScan, projectName, opticalImageSetName);
    _pspImageAcquisitionEngine.acquireHeightMapImage(opticalInspectionData, moveStage);
    
    if (moveStage)
    {
      // Set Z height from PSP into OpticalPointToPointScan
      setZHeightInNanometers(opticalPointToPointScan, opticalInspectionData.getPspModuleEnum());
    }
    
    // De-reference object
    if (opticalInspectionData != null)
    {
      opticalInspectionData.clear();
      opticalInspectionData = null;
    }
  }
  
  /**
   * This is a main function to calculate alignment height map. It handles image acquisition
   * and the height map calculation.
   * 
   * @author Cheah Lee Herng 
   */
  public void performAlignmentHeightMapOperation(OpticalPointToPointScan opticalPointToPointScan) throws XrayTesterException
  {
    Assert.expect(opticalPointToPointScan != null);    
    
    // Initialize system for first time run
    _pspImageAcquisitionEngine.initialize(getPspModuleEnum(opticalPointToPointScan.getOpticalCameraIdEnum()), opticalPointToPointScan);
    
    OpticalInspectionData opticalInspectionData = setupAlignmentHeightMapInspectionData(opticalPointToPointScan);
    _pspImageAcquisitionEngine.acquireAlignmentHeightMapImage(opticalInspectionData);
    
    // Set Z height from PSP into OpticalPointToPointScan
    setZHeightInNanometers(opticalPointToPointScan, opticalInspectionData.getPspModuleEnum());
    
    // De-reference object
    if (opticalInspectionData != null)
    {
      opticalInspectionData.clear();
      opticalInspectionData = null;
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void acquireSingleImageForLiveView(OpticalPointToPointScan opticalPointToPointScan, String projectName, String opticalImageSetName) throws XrayTesterException 
  {
    Assert.expect(opticalPointToPointScan != null);
    Assert.expect(projectName != null);
    Assert.expect(opticalImageSetName != null);
    
    // Initialize system for first time run
    _pspImageAcquisitionEngine.initialize(getPspModuleEnum(opticalPointToPointScan.getOpticalCameraIdEnum()), opticalPointToPointScan);
    
      // Get module information
    StagePosition stagePosition = opticalPointToPointScan.getPointToPointStagePositionInNanoMeters();
    PspModuleEnum pspModuleEnum = getPspModuleEnum(opticalPointToPointScan.getOpticalCameraIdEnum());
    
    // Setup necesssary information for image acquisition
    OpticalInspectionData opticalInspectionData = new OpticalInspectionData();
    opticalInspectionData.setPspModuleEnum(pspModuleEnum);
    opticalInspectionData.setForceTrigger(true);
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.WHITE_LIGHT, opticalPointToPointScan.getPointPositionName());
    
    // Acquire single image operation
    _pspImageAcquisitionEngine.acquireSingleImage(opticalInspectionData);
    
    // De-reference object
    if (opticalInspectionData != null)
    {
      opticalInspectionData.clear();
      opticalInspectionData = null;
    }
  }
  
   /**
   * This is a main function to capture single image and calculate height map for live view. It handles image acquisition
   * and the height map calculation.
   * 
   * @author Jack Hwee
   */
  public void performSingleImageAndHeightMapOperationForLiveView(OpticalPointToPointScan opticalPointToPointScan, String projectName, String opticalImageSetName) throws XrayTesterException 
  {
    Assert.expect(opticalPointToPointScan != null);
    Assert.expect(projectName != null);
    Assert.expect(opticalImageSetName != null);
    
    // Initialize system for first time run
    _pspImageAcquisitionEngine.initialize(getPspModuleEnum(opticalPointToPointScan.getOpticalCameraIdEnum()), opticalPointToPointScan);
    
      // Get module information
    StagePosition stagePosition = opticalPointToPointScan.getPointToPointStagePositionInNanoMeters();
    PspModuleEnum pspModuleEnum = getPspModuleEnum(opticalPointToPointScan.getOpticalCameraIdEnum());
    
    // Setup necesssary information for image acquisition
    OpticalInspectionData opticalInspectionData = new OpticalInspectionData();
    opticalInspectionData.setPspModuleEnum(pspModuleEnum);
    opticalInspectionData.setForceTrigger(true);
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.WHITE_LIGHT, opticalPointToPointScan.getPointPositionName());
    
    // Acquire single image operation
    PspHardwareEngine.getInstance().adjustCurrent(pspModuleEnum, Project.getCurrentlyLoadedProject().getPanel().getPspSettings().getCurrentSetting());
    _pspImageAcquisitionEngine.acquireSingleImage(opticalInspectionData);
    
    // De-reference object
    if (opticalInspectionData != null)
    {
      opticalInspectionData.clear();
      opticalInspectionData = null;
    }
    // reset it back to original current setting.
    PspHardwareEngine.getInstance().adjustCurrent(pspModuleEnum, _config.getIntValue(HardwareConfigEnum.ETHERNET_BASED_LIGHT_ENGINE_CURRENT_SETTING_MICRO_AMPS));
    OpticalInspectionData opticalInspectionData2 = setupHeightMapInspectionData(opticalPointToPointScan, projectName, opticalImageSetName);
    _pspImageAcquisitionEngine.acquireHeightMapImage(opticalInspectionData2, true);
    
    // De-reference object
    if (opticalInspectionData2 != null)
    {
      opticalInspectionData2.clear();
      opticalInspectionData2 = null;
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void performOfflineHeightMapOperation(OpticalPointToPointScan opticalPointToPointScan, String projectName, String opticalImageSetName) throws XrayTesterException
  {
    Assert.expect(opticalPointToPointScan != null);
    Assert.expect(projectName != null);
    Assert.expect(opticalImageSetName != null);
    
    // Initialize system for first time run
    _pspImageAcquisitionEngine.initialize(getPspModuleEnum(opticalPointToPointScan.getOpticalCameraIdEnum()), opticalPointToPointScan);
    
    OpticalInspectionData opticalInspectionData = setupOfflineHeightMapInspectionData(opticalPointToPointScan, projectName);
    for(Map.Entry<StagePosition, List<Pair<OpticalShiftNameEnum, String>>> entry: opticalInspectionData.getStagePositionMap().entrySet())
    {
      List<Pair<OpticalShiftNameEnum, String>> opticalShiftNameList = entry.getValue();
      for(Pair<OpticalShiftNameEnum, String> entry2 : opticalShiftNameList)
      {
        String offlineImageFullPath = entry2.getSecond();
        _pspImageAcquisitionEngine.acquireOfflineHeightMapImage(opticalInspectionData.getPspModuleEnum(),
                                                                opticalInspectionData.getOpticalCalibrationProfileEnum(), 
                                                                offlineImageFullPath);
      }
    }
    
    // Set Z height from PSP into OpticalPointToPointScan
    setZHeightInNanometers(opticalPointToPointScan, opticalInspectionData.getPspModuleEnum());
    
    // De-reference object
    if (opticalInspectionData != null)
    {
      opticalInspectionData.clear();
      opticalInspectionData = null;
    }
  }
  
  /**
   * This function captures single image, depending on projection type.
   * 
   * @author Cheah Lee Herng
   */
  public void performSingleImageOperation(OpticalPointToPointScan opticalPointToPointScan, OpticalShiftNameEnum opticalShiftNameEnum) throws XrayTesterException
  {
    Assert.expect(opticalPointToPointScan != null);
    Assert.expect(opticalShiftNameEnum != null);
    
    // Initialize system for first time run
    _pspImageAcquisitionEngine.initialize(getPspModuleEnum(opticalPointToPointScan.getOpticalCameraIdEnum()), opticalPointToPointScan);
    
    // Get module information
    StagePosition stagePosition = opticalPointToPointScan.getPointToPointStagePositionInNanoMeters();
    PspModuleEnum pspModuleEnum = getPspModuleEnum(opticalPointToPointScan.getOpticalCameraIdEnum());
    
    // Setup necesssary information for image acquisition
    OpticalInspectionData opticalInspectionData = new OpticalInspectionData();
    opticalInspectionData.setPspModuleEnum(pspModuleEnum);
    opticalInspectionData.setForceTrigger(true);
    opticalInspectionData.addImageFullPath(stagePosition, opticalShiftNameEnum, opticalPointToPointScan.getPointPositionName());
    
    // Acquire single image operation
    _pspImageAcquisitionEngine.acquireSingleImage(opticalInspectionData);
    
    // De-reference object
    if (opticalInspectionData != null)
    {
      opticalInspectionData.clear();
      opticalInspectionData = null;
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setReturnRawPspHeight(boolean returnRawPspHeight)
  {
    _returnRawPspHeight = returnRawPspHeight;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isReturnRawPspHeight()
  {
    return _returnRawPspHeight;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setFilterOutlier(boolean filterOutlier)
  {
    _filterOutlier = filterOutlier;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isFilterOutlier()
  {
    return _filterOutlier;
  }
  
  /**
   * This function returns the corresponding offset to remap to x-ray z-height.
   * 
   * @author Cheah Lee Herng
   */
  public double getOffset(PspModuleEnum moduleEnum)
  {
    Assert.expect(moduleEnum != null);
    
    double offset = 0.0;
    if (getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_3) ||
        getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_4))
    {
      // XCR-2932 Automate PSP Distance offset Calculation - Cheah Lee Herng 01 Oct 2015
      offset = MathUtil.convertMilsToNanoMeters(_config.getDoubleValue(PspCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_ZHEIGHT_IN_MILS)) + 
               StageBelts.getNominalBeltThicknessInNanometers();
    }
    else
    {
      if (moduleEnum.equals(PspModuleEnum.FRONT))
      {
        if (XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
        {
          offset = MathUtil.convertMillimetersToNanometers(PspConfiguration.getInstance().getDistanceFromBottomSurfaceOfTopStageToOpticalCalibrationJigReferencePlaneInMilimeters()) +
                   (_config.getIntValue(SoftwareConfigEnum.TOP_OF_BOARD_OFFSET_IN_NANOMETERS) + 
                    _config.getIntValue(HardwareConfigEnum.DISTANCE_OFFSET_FROM_SYSTEM_NOMINAL_REFERENCE_PLANE_TO_TOP_SURFACE_OF_BELT_IN_NANOMETERS_FOR_FRONT_MODULE));          
          offset = -offset;
        }
        else
        {
          offset = MathUtil.convertMillimetersToNanometers(PspConfiguration.getInstance().getDistanceFromBeltToOpticalCalibrationJigReferencePlaneInMilimeters()) - 
                   (MathUtil.convertMilsToNanoMeters(PspConfiguration.getInstance().getDistanceFromBeltToSystemReferencePlaneInMils()) + 
                    _config.getIntValue(HardwareConfigEnum.DISTANCE_OFFSET_FROM_SYSTEM_NOMINAL_REFERENCE_PLANE_TO_TOP_SURFACE_OF_BELT_IN_NANOMETERS_FOR_FRONT_MODULE));
        }
      }
      else
      {
        if (XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
        {
          offset = MathUtil.convertMillimetersToNanometers(PspConfiguration.getInstance().getDistanceFromBottomSurfaceOfTopStageToOpticalCalibrationJigReferencePlaneInMilimeters()) +
                   (_config.getIntValue(SoftwareConfigEnum.TOP_OF_BOARD_OFFSET_IN_NANOMETERS) + 
                    _config.getIntValue(HardwareConfigEnum.DISTANCE_OFFSET_FROM_SYSTEM_NOMINAL_REFERENCE_PLANE_TO_TOP_SURFACE_OF_BELT_IN_NANOMETERS_FOR_REAR_MODULE));
          offset = -offset;
        }
        else
        {
          offset = MathUtil.convertMillimetersToNanometers(PspConfiguration.getInstance().getDistanceFromBeltToOpticalCalibrationJigReferencePlaneInMilimeters()) - 
                   (MathUtil.convertMilsToNanoMeters(PspConfiguration.getInstance().getDistanceFromBeltToSystemReferencePlaneInMils()) + 
                    _config.getIntValue(HardwareConfigEnum.DISTANCE_OFFSET_FROM_SYSTEM_NOMINAL_REFERENCE_PLANE_TO_TOP_SURFACE_OF_BELT_IN_NANOMETERS_FOR_REAR_MODULE));
        }
      }
    }
    
    return offset;
  }
  
  // Protected abstract function
  protected abstract OpticalInspectionData setupHeightMapInspectionData(OpticalPointToPointScan opticalPointToPointScan, String projectName, String opticalImageSetName);
  protected abstract OpticalInspectionData setupOfflineHeightMapInspectionData(OpticalPointToPointScan opticalPointToPointScan, String projectName);
  protected abstract OpticalInspectionData setupAlignmentHeightMapInspectionData(OpticalPointToPointScan opticalPointToPointScan);
  protected abstract void setZHeightInNanometers(OpticalPointToPointScan opticalPointToPointScan, PspModuleEnum moduleEnum) throws XrayTesterException;
  
  // Public abstract function
  public abstract void performCalibration() throws XrayTesterException;
}
