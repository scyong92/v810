package com.axi.v810.business.imageAcquisition;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;

/**
 * The image and information about the actual magnification, the z height of the
 * slices and the rotation angle
 * @author Horst Mueller
 *
 * The ReconstructedImages may be updated with a set of rotated images.
 * RegionOfInterests for joints should be grabbed via getRegionOfInterestForJoint
 * rather than joint.getRegionOfInterestRelativeToInspectionRegion, which doesn't
 * know about rotations.
 * The rotation is assumed to be _reconstructionRegion.getRotationCorrectionInDegrees.
 * @author Patrick Lacz
 */
public class ReconstructedImages implements Serializable
{
  // PE:  We need this ID because the reconstructionRegion reference
  // below will be lost when this data gets passed over the socket
  // to and from the Image Reconstruction Processors.
  private int _reconstructionRegionId;
  private ReconstructionRegion _reconstructionRegion;
  private int _numberOfSlicesExpected = -1;

  private Map<Integer, ReconstructedSlice> _sliceIdToReconstructedSliceMap;

  // If the rotationCorrectionTransformation is non-null, it means that all
  // four of these member variables have been initialized.
  // @todo the other rotation-correction transformation values have been moved to ReconstructionRegion, these should
  // probably be moved there as well.
  private int _correctedImageWidth;
  private int _correctedImageHeight;

  private int _referenceCount = 1;

  // a debugging tool used to see where reference count error images were allocated.
  private static final boolean _KEEP_ALLOCATION_STACK_TRACES_FOR_DEBUGGING = false;
  private Throwable _debugAllocationTrace = null;

  /**
  * @author Horst Mueller
  * @author Patrick Lacz
  */
  public ReconstructedImages(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    _sliceIdToReconstructedSliceMap = new TreeMap<Integer, ReconstructedSlice>();
    _numberOfSlicesExpected = reconstructionRegion.getNumberOfSlicesToBeReconstructed();
    setReconstructionRegionId(reconstructionRegion.getRegionId());
    setReconstructionRegion(reconstructionRegion);

    if (_KEEP_ALLOCATION_STACK_TRACES_FOR_DEBUGGING)
    {
      Exception e = new Exception("Allocation Stack of ReconstructedImages");
      e.fillInStackTrace();
      _debugAllocationTrace = e;
    }
  }

  /**
   * @author Patrick Lacz
   */
  public boolean equals(Object obj)
  {
    if (obj instanceof ReconstructedImages)
    {
      ReconstructedImages otherAsReconstructedImages = (ReconstructedImages)obj;
      if (getReconstructionRegion().equals(otherAsReconstructedImages.getReconstructionRegion()) &&
          getNumberOfSlices() == otherAsReconstructedImages.getNumberOfSlices())
      {
        return true;
      }
    }
    return false;
  }

  /**
   * @author Roy Williams
   */
  public void reset()
  {
    _sliceIdToReconstructedSliceMap = new TreeMap<Integer, ReconstructedSlice>();
  }

  /**
   * @author Patrick Lacz
   */
  public int getNumberOfSlices()
  {
    Assert.expect(_sliceIdToReconstructedSliceMap != null);
    return _sliceIdToReconstructedSliceMap.size();
  }

  /**
   * @author Patrick Lacz
   */
  public ReconstructedSlice getFirstSlice()
  {
    Assert.expect(_sliceIdToReconstructedSliceMap != null);
    Assert.expect(_sliceIdToReconstructedSliceMap.size() > 0);
    return _sliceIdToReconstructedSliceMap.values().iterator().next();
  }

  /**
   * Be VERY careful with this method! You want to call it only when you are the sole owner of the
   * images, and you want to free up some of the space used by the images. For instance, when we're done
   * inspecting but keeping the images around for possible repair image saving. (we don't need the orthogonal ones)
   * @author Patrick Lacz
   */
  public void clearOrthogonalImages()
  {
    for (ReconstructedSlice slice : getReconstructedSlices())
    {
      slice.clearOrthogonalImage();
    }
  }
  
  /**
   * Be VERY careful with this method! You want to call it only when you are the sole owner of the
   * images, and you want to free up some of the space used by the images. For instance, when we're done
   * inspecting but keeping the images around for possible repair image saving. (we don't need the orthogonal ones)
   * @author Wei Chin
   */
  public void clearEnhancedImages()
  {
    for (ReconstructedSlice slice : getReconstructedSlices())
    {
      slice.clearEnhancedImage();
    }
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void incrementReferenceCount()
  {
    if (_referenceCount <= 0)
    {
      Assert.expect(_referenceCount > 0);
    }

    ++_referenceCount;

    if (_KEEP_ALLOCATION_STACK_TRACES_FOR_DEBUGGING)
    {
      Exception e = new Exception("Allocation Stack of ReconstructedImages");
      e.fillInStackTrace();
      _debugAllocationTrace = e;
    }
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void decrementReferenceCount()
  {
    if (_referenceCount <= 0)
    {
      Assert.expect(_referenceCount > 0);
    }

    --_referenceCount;

    if (_referenceCount == 0)
    {
      for (ReconstructedSlice slice : getReconstructedSlices())
      {
        slice.decrementReferenceCount();
      }
    }
  }

  /**
   * @author Patrick Lacz
   */
  public ReconstructedSlice getPadSlice(Subtype subtype)
  {
    Assert.expect(subtype != null);
    Assert.expect(_reconstructionRegion != null);

    SliceNameEnum padSliceName = subtype.getInspectionFamily().getDefaultPadSliceNameEnum(subtype);
    Assert.expect(_sliceIdToReconstructedSliceMap.containsKey(padSliceName.getId()));

    return getReconstructedSlice(padSliceName);
  }

  /**
   * @author Horst Mueller
   */
  public void setReconstructionRegionId(int reconstructionRegionId)
  {
    Assert.expect(reconstructionRegionId >= 0);
    _reconstructionRegionId = reconstructionRegionId;
  }

  /**
   * @author Horst Mueller
   */
  public int getReconstructionRegionId()
  {
    return _reconstructionRegionId;
  }

  /**
   * @author Horst Mueller
   */
  public void setReconstructionRegion(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    _reconstructionRegion = reconstructionRegion;
  }

  /**
   * @author Horst Mueller
   */
  public ReconstructionRegion getReconstructionRegion()
  {
    Assert.expect(_reconstructionRegion != null);
    return _reconstructionRegion;
  }

  /**
   * Add an image loaded from disc.
   * The zheight (and potentially other properties of the slice) are populated from the Image's ImageDescription.
   *
   * @author Patrick Lacz
   */
  public void addImage(Image image, SliceNameEnum sliceHeightName)
  {
    Assert.expect(_sliceIdToReconstructedSliceMap != null);
    Assert.expect(image != null);
    Assert.expect(sliceHeightName != null);

    ReconstructedSlice newSlice = new ReconstructedSlice(image, sliceHeightName);
    _sliceIdToReconstructedSliceMap.put(sliceHeightName.getId(), newSlice);
  }

  /**
   * Add an image transferred from the IRPs.
   * The zheight (and potentially other properties of the slice) are passed in explicitly.
   * Add arrayZHeight and arraySharpness by BH
   * @author Patrick Lacz
   * @author Lim Boon Hong
   */
  public void addImage(Image image, SliceNameEnum sliceHeightName, int sliceHeightInNanometers, List<Float> arrayZHeight, List<Float> arraySharpness)
  {
    Assert.expect(_sliceIdToReconstructedSliceMap != null);
    Assert.expect(image != null);
    Assert.expect(sliceHeightName != null);

    // Added by Seng Yew on 22-Apr-2011
    // Assuming those reconstruction region with component assign are for inspection,
    // this is to bypass other verification images and CD&A images.
    if (_reconstructionRegion.hasComponent() && !_reconstructionRegion.isAlignmentRegion() && !_reconstructionRegion.isVerificationRegion())
    {
      Subtype subtype = _reconstructionRegion.getSubtypes().iterator().next();
      if(ImageProcessingAlgorithm.useUserDefinedGrayLevel(subtype))
      {
        ImageProcessingAlgorithm.applyOriginalImageWithUserDefinedGrayLevel(subtype, image);
      }
    }
    ReconstructedSlice newSlice = new ReconstructedSlice(image, sliceHeightName, sliceHeightInNanometers, arrayZHeight, arraySharpness);//add arrayZHeight and arraySharpness by BH
    _sliceIdToReconstructedSliceMap.put(sliceHeightName.getId(), newSlice);
  }

  /**
   * @author Patrick Lacz
   */
  public ReconstructedSlice getReconstructedSlice(SliceNameEnum sliceName)
  {
    Assert.expect(_sliceIdToReconstructedSliceMap != null);
    Assert.expect(sliceName != null);
    ReconstructedSlice slice = _sliceIdToReconstructedSliceMap.get(sliceName.getId());
    Assert.expect(slice != null, "Failure to find slice: " + sliceName.getId() + " " + sliceName.getName());
    return slice;
  }
  
  /**
   * @author Wei Chin
   */
  public ReconstructedSlice getReconstructedSliceIgnoreNullImages(SliceNameEnum sliceName)
  {
    Assert.expect(_sliceIdToReconstructedSliceMap != null);
    Assert.expect(sliceName != null);
    ReconstructedSlice slice = _sliceIdToReconstructedSliceMap.get(sliceName.getId());
    return slice;
  }

  /**
   * @author Patrick Lacz
   */
  public Collection<ReconstructedSlice> getReconstructedSlices()
  {
    Assert.expect(_sliceIdToReconstructedSliceMap != null);
    return _sliceIdToReconstructedSliceMap.values();
  }

  /**
   * @author George A. David
   */
  public Collection<ReconstructedSlice> getAdjustFocusReconstructedSlices()
  {
    List<ReconstructedSlice> reconSlices = new LinkedList<ReconstructedSlice>();
    for(ReconstructedSlice reconSlice : getReconstructedSlices())
    {
      Slice slice = null;
      if(_reconstructionRegion.hasSlice(reconSlice.getSliceNameEnum()))
        slice = _reconstructionRegion.getSlice(reconSlice.getSliceNameEnum());
      else
      {
        for(ReconstructionRegion region : _reconstructionRegion.getTestSubProgram().getAdjustFocusRegions(_reconstructionRegion.getName()))
        {
          if(region.hasSlice(reconSlice.getSliceNameEnum()))
          {
            slice = region.getSlice(reconSlice.getSliceNameEnum());
            break;
          }
        }
      }
      Assert.expect(slice != null);
      // make sure it's an additional slice, all adjust focus slices will be additional
      // slices.
      if(slice.isAdditionalSlice())
      {
        reconSlice.incrementReferenceCount();
        reconSlices.add(reconSlice);
      }
    }

    Assert.expect(reconSlices.isEmpty() == false);
    return reconSlices;
  }

  /**
   * Get the set of ReconstructedSlices that are actually inspected by the
   * algorithms.  This can be useful, because in some cases, such as region inspected
   * by the ThroughHole InspectionFamily, we specify more slices than are actually
   * used during inspection.
   *
   * @author Peter Esbensen
   */
  public Collection<ReconstructedSlice> getInspectedReconstructedSlices()
  {
    List<ReconstructedSlice> reconstructedSlices = new ArrayList<ReconstructedSlice>();
    if (_reconstructionRegion.getReconstructionRegionTypeEnum().equals(ReconstructionRegionTypeEnum.INSPECTION))
    {
      Collection<Slice> slices = _reconstructionRegion.getInspectedSlices();
      for (Slice slice : slices)
      {
        reconstructedSlices.add(getReconstructedSlice(slice.getSliceName()));
      }
    }
    return reconstructedSlices;
  }

  /**
   * @author Patrick Lacz
   */
  public Collection<SliceNameEnum> getSliceNames()
  {
    Assert.expect(_sliceIdToReconstructedSliceMap != null);
    List<SliceNameEnum> nameList = new LinkedList<SliceNameEnum>();
    for (Integer index : _sliceIdToReconstructedSliceMap.keySet())
    {
      nameList.add(SliceNameEnum.getEnumFromIndex(index));
    }
    return nameList;
  }

  /**
   * Verifies that the images we have ready to work with are rotated properly, etc.
   * This method will create the orthogonalImage (probably a bad name) for each slice.
   * @author Patrick Lacz
   */
  public void ensureInspectedSlicesAreReadyForInspection() throws DatastoreException
  {
    double degreeRotation = getReconstructionRegion().getRotationCorrectionInDegrees();
    if (MathUtil.fuzzyEquals(degreeRotation,0.0d))
    {
      enhanceImageSlicesBeforeInspection();
      return;
    }

    if(getReconstructionRegion().getReconstructionRegionTypeEnum().equals(ReconstructionRegionTypeEnum.INSPECTION) &&
       ImageAnalysis.isSpecialTwoPinComponent(getReconstructionRegion().getJointTypeEnum()) == false)
    {
      // now check to make sure all pads are not circles, if they are, then we
      // do not need to rotate the image
      // this is done to lower the memory usage
      boolean isPerfectCircle = true;
      for (Pad pad : getReconstructionRegion().getPads())
      {
        if (pad.getLandPatternPad().isPerfectCircle() == false)
        {
          isPerfectCircle = false;
          break;
        }
      }
      if (isPerfectCircle)
      {
        enhanceImageSlicesBeforeInspection();
        return;
      }
    }

    for (ReconstructedSlice slice : this.getInspectedReconstructedSlices())
    {
      if (slice.hasModifiedImageForInspection() == false)
      {
        Image image = createOrthogonalImage(degreeRotation, slice.getImage(), false);
        slice.setOrthogonalImage(image);
        image.decrementReferenceCount();
      }
    }
    enhanceImageSlicesBeforeInspection();
  }
  
  /**
   * @author Wei Chin
   */
  void enhanceImageSlicesBeforeInspection() throws DatastoreException
  {
    // need to enhancement the image? do the image processing here.
    for (ReconstructedSlice slice : this.getInspectedReconstructedSlices())
    {
      if (slice.hasEnhancedImageForInspection() == false)
        enhanceImageBeforeInspection(slice);
    }
  }
  /**
   * Verifies that the images we have ready to work with are rotated properly, etc.
   * Use this version if you're doing something that potentially involves more slices than the inspected ones.
   * (eg. alignment)
   *
   * This method will create the orthogonal image for each slice.
   * @author Patrick Lacz
   */
  public void ensureAllSlicesAreReadyForInspection()
  {
    double degreeRotation = getReconstructionRegion().getRotationCorrectionInDegrees();
    if (MathUtil.fuzzyEquals(degreeRotation, 0.0d) == true)
      return;

    if(getReconstructionRegion().getReconstructionRegionTypeEnum().equals(ReconstructionRegionTypeEnum.INSPECTION) &&
       ImageAnalysis.isSpecialTwoPinComponent(getReconstructionRegion().getJointTypeEnum()) == false)
    {
      // now check to make sure all pads are not circles, if they are, then we
      // do not need to rotate the image
      // this is done to lower the memory usage
      boolean isPerfectCircle = true;
      for (Pad pad : getReconstructionRegion().getPads())
      {
        if (pad.getLandPatternPad().isPerfectCircle() == false)
        {
          isPerfectCircle = false;
          break;
        }
      }
      if (isPerfectCircle)
        return;
    }

    for (ReconstructedSlice slice : getReconstructedSlices())
    {
      if (slice.hasModifiedImageForInspection() == false)
      {
        Image image = createOrthogonalImage(degreeRotation, slice.getImage(), false);
        slice.setOrthogonalImage(image);
        image.decrementReferenceCount();
      }
    }
  }

  /**
   * Rotate the images in the collection by <pre>degreeRotation</pre> degrees.
   * Normally degreeRotation is set to <pre>getInspectionRegionInt().getRotationCorrectionInDegrees()</pre>, but it could
   * have other rotations.
   *
   * This function is expected to be called from the local ensure...() functions. BEEEYAAATCH
   * @author Patrick Lacz
   */
  private Image createOrthogonalImage(double degreeRotation, Image Image, boolean saveAsEnhancedTransform)
  {
    // build the transformation
    //double degreeRotation = reconstructedImages.getRotationCorrectionInDegrees();
    AffineTransform rotationTransform = AffineTransform.getRotateInstance( -Math.toRadians(degreeRotation));
    AffineTransform correctionTransform = new AffineTransform();

    RegionOfInterest baseRoi = RegionOfInterest.createRegionFromImage(Image);
    RegionOfInterest tempResultRoi = new RegionOfInterest(baseRoi);

    Transform.boundTransformedRegionOfInterest(baseRoi, rotationTransform, tempResultRoi, correctionTransform);

    // we need to keep the region's rotated dimensions so that we know how big of an image to create.
    _correctedImageWidth = tempResultRoi.getWidth();
    _correctedImageHeight = tempResultRoi.getHeight();
    if (saveAsEnhancedTransform)
      _reconstructionRegion.setRotationCorrectionTransformationWithResize(correctionTransform);
    else
      _reconstructionRegion.setRotationCorrectionTransformation(correctionTransform);

    Assert.expect(_correctedImageWidth > 0);
    Assert.expect(_correctedImageHeight > 0);

    RegionOfInterest resultRoi = new RegionOfInterest(0, 0, _correctedImageWidth, _correctedImageHeight, 0, RegionShapeEnum.RECTANGULAR);

    float rotatedImageBackgroundColor = 0f;
    if (_reconstructionRegion.isAlignmentRegion())
    {
      // Fill in rotated alignment images with a white background instead of black.
      rotatedImageBackgroundColor = 255f;
    }
    Image correctedImage = Transform.applyAffineTransform(Image,
                                                          correctionTransform,
                                                          resultRoi,
                                                          rotatedImageBackgroundColor);
    return correctedImage;
    
  }

  /**
   * @author Patrick Lacz
   */
  public long getMaximumExpectedMemorySize()
  {
    double degreeRotation = getReconstructionRegion().getRotationCorrectionInDegrees();
    boolean addOrthogonalImageSize = (MathUtil.fuzzyEquals(degreeRotation,0.0d) == false);

    final long _SIZE_OF_FLOAT_IN_BYTES = 4;

    ImageRectangle rawImageRectangle = getReconstructionRegion().getRegionRectangleInPixels();

    // change this when/if the raw image is stored in bytes.
    long sizeOfOneSlice = rawImageRectangle.getWidth() * rawImageRectangle.getHeight() * _SIZE_OF_FLOAT_IN_BYTES;
    long sizeOfOneInspectedSlice = sizeOfOneSlice;
    if (addOrthogonalImageSize)
    {
      RegionOfInterest rawImageRoi = new RegionOfInterest(0, 0,
          rawImageRectangle.getWidth(), rawImageRectangle.getHeight(),
          0, RegionShapeEnum.RECTANGULAR);

      AffineTransform rotationTransform = AffineTransform.getRotateInstance( -Math.toRadians(degreeRotation));
      AffineTransform correctionTransform = new AffineTransform();

      RegionOfInterest correctedImageRoi = new RegionOfInterest(rawImageRoi);

      Transform.boundTransformedRegionOfInterest(rawImageRoi, rotationTransform, correctedImageRoi, correctionTransform);

      // corrected image is always in floats
      long sizeOfOrthogonalImage = correctedImageRoi.getWidth() * correctedImageRoi.getHeight() * _SIZE_OF_FLOAT_IN_BYTES;
      sizeOfOneInspectedSlice += sizeOfOrthogonalImage;
    }

    int numberOfInspectedSlices = 0;
    if (getReconstructionRegion().isInspected())
      numberOfInspectedSlices = getInspectedReconstructedSlices().size();
    int numberOfNonInspectedSlices = getNumberOfSlices() - numberOfInspectedSlices;
    
    //Siew Yeng - XCR-2387 - Save Diagnostic Slice Image
    if (_reconstructionRegion.hasComponent() && !_reconstructionRegion.isAlignmentRegion() && !_reconstructionRegion.isVerificationRegion())
    {
      boolean diagnosticSliceExist = false;
      boolean enhancedSliceExist = false; //Siew Yeng
      for(ReconstructedSlice rs : getReconstructedSlices())
      {
        if(EnumToUniqueIDLookup.getInstance().isDiagnosticSlice(rs.getSliceNameEnum()))
        {
          diagnosticSliceExist = true;
        }
        
        if(EnumToUniqueIDLookup.getInstance().isEnhancedImageSlice(rs.getSliceNameEnum()))
        {
          enhancedSliceExist = true;
        }
      }

      if(diagnosticSliceExist == false && ImageProcessingAlgorithm.needToSaveVoidDiagnosticImage(_reconstructionRegion.getSubtypes().iterator().next()))
        numberOfNonInspectedSlices += numberOfInspectedSlices;
      
      //Siew Yeng
      if(enhancedSliceExist == false && ImageProcessingAlgorithm.needToSaveEnhancedImage(_reconstructionRegion.getSubtypes().iterator().next()))
        numberOfNonInspectedSlices += numberOfInspectedSlices;
    }

    return sizeOfOneInspectedSlice * numberOfInspectedSlices + sizeOfOneSlice * numberOfNonInspectedSlices;
  }

  /**
   * @author Roy Williams
   */
  public boolean hasImageForEachReconstructableSlice()
  {
    return (getNumberOfSlices() == _numberOfSlicesExpected);
  }

  /**
   * @author Roy Williams
   */
  public int getNumberOfSlicesExpected()
  {
    return _numberOfSlicesExpected;
  }

  /**
   * @author John Heumann
   * @edited By Kee Chin Seong - to make sure the sliceenum come in is approriate
   */
  public void replaceAllSlices(ReconstructedImages newReconstructedImages)
  {
    for (SliceNameEnum sliceNameEnum : getSliceNames())
    {
      if(hasReconstructedSliceName(getReconstructedSlice(sliceNameEnum)))
      {
        ReconstructedSlice oldSlice = getReconstructedSlice(sliceNameEnum);
        Assert.expect(oldSlice != null);
        //Siew Yeng - XCR-2888 - Failure to find slice: 137 Diagnostic Pad Slice
        if(newReconstructedImages.hasReconstructedSlice(sliceNameEnum))
        {
          ReconstructedSlice newSlice = newReconstructedImages.getReconstructedSlice(sliceNameEnum);
          Assert.expect(newSlice != null);
          // No need to remove the old entry from the map. According to the documentation this will replace it.
          _sliceIdToReconstructedSliceMap.put(sliceNameEnum.getId(), newSlice);
          newSlice.incrementReferenceCount();
        }
        else
        {
          if(EnumToUniqueIDLookup.getInstance().isDiagnosticSlice(sliceNameEnum))
            _sliceIdToReconstructedSliceMap.remove(sliceNameEnum.getId());
        }
        
        oldSlice.decrementReferenceCount();
      }
    }
  }

  /**
   * @author John Heumann
   */
  public void replaceASlice(ReconstructedImages newReconstructedImages, SliceNameEnum targetSliceName)
  {
    for (SliceNameEnum sliceNameEnum : getSliceNames())
    {
      if (sliceNameEnum == targetSliceName)
      {
        ReconstructedSlice oldSlice = getReconstructedSlice(sliceNameEnum);
        Assert.expect(oldSlice != null);
        ReconstructedSlice newSlice = newReconstructedImages.getReconstructedSlice(sliceNameEnum);
        Assert.expect(newSlice != null);
        // No need to remove the old entry from the map. According to the documentation this will replace it.
        _sliceIdToReconstructedSliceMap.put(sliceNameEnum.getId(), newSlice);
        newSlice.incrementReferenceCount();
        oldSlice.decrementReferenceCount();
      }
    }
  }

  /**
   * @return Map
   * @author Wei Chin, Chong
   */
  public Map<Integer, ReconstructedSlice> getSliceIdToReconstructedSliceMap()
  {
    Assert.expect(_sliceIdToReconstructedSliceMap != null);
    return _sliceIdToReconstructedSliceMap;
  }

  /**
   * @author George A. David
   */
  protected void finalize()
  {
    if(_referenceCount != 0)
    {
      System.out.println("ReconstructedImages reference count (" + _referenceCount + ") is not zero.");
      if(_reconstructionRegion.isAlignmentRegion())
      {
        System.out.println("Alignment Region id: " + _reconstructionRegion.getRegionId() + " AlignmentGroup name: " + _reconstructionRegion.getAlignmentGroup().getName());
      }
      else if(_reconstructionRegion.isVerificationRegion())
        System.out.println("Verification Region id: " + _reconstructionRegion.getRegionId());
      else
        System.out.println("Reconstruction Region id: " + _reconstructionRegion.getRegionId() + " RefDes: " + _reconstructionRegion.getComponent().getReferenceDesignator());

      if (_debugAllocationTrace != null)
          _debugAllocationTrace.printStackTrace();

//      for(int i = 0; i < _referenceCount; ++i)
//        decrementReferenceCount();

    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean isImageBeingReferenced()
  {
      return (_referenceCount > 0);
  }
  
  /*
   * @author Kee chin Seong - New function just to get the value from key.
   */
  public boolean hasReconstructedSliceName(ReconstructedSlice reconstructedSliceName)
  {
    Assert.expect(_sliceIdToReconstructedSliceMap != null);
    Assert.expect(reconstructedSliceName != null);
    return _sliceIdToReconstructedSliceMap.containsValue(reconstructedSliceName);  
  }

  /**
   * @author Wei Chin
   */
  public boolean hasReconstructedSlice(SliceNameEnum sliceName)
  {
    Assert.expect(_sliceIdToReconstructedSliceMap != null);
    Assert.expect(sliceName != null);
    return _sliceIdToReconstructedSliceMap.containsKey(sliceName.getId());    
  }  
  
  /**
   * @author Wei Chin
   */
  private void enhanceImageBeforeInspection(ReconstructedSlice reconstructedSlice) throws DatastoreException
  {
    if (_reconstructionRegion.hasComponent() && !_reconstructionRegion.isAlignmentRegion() && !_reconstructionRegion.isVerificationRegion())
    {
      // added by Wei Chin for Box Filtering Image Processing
      Subtype currentSubtype = _reconstructionRegion.getSubtypes().iterator().next();
      
      //Siew Yeng - XCR2170 - fix box filter and clahe always get different pad size
      Pad pad = _reconstructionRegion.getJointInspectionDataList(currentSubtype).get(0).getPad();
      double padWidth = pad.getShapeRelativeToPanelInNanoMeters().getBounds().getWidth();
      double padHeight = pad.getShapeRelativeToPanelInNanoMeters().getBounds().getHeight();
      Image newImage = null;
      Assert.expect(reconstructedSlice != null);
      if(ImageProcessingAlgorithm.useResize(currentSubtype))
      {
        newImage = ImageProcessingAlgorithm.resizeImage(currentSubtype, reconstructedSlice.getImage());
      }
      
      // Kok Chun, Tan - Motion Blur
      if(ImageProcessingAlgorithm.useMotionBlur(currentSubtype))
      {
        if(newImage == null)
          newImage = ImageProcessingAlgorithm.applyMotionBlur(currentSubtype, reconstructedSlice.getImage());
        else
          newImage = ImageProcessingAlgorithm.applyMotionBlur(currentSubtype, newImage);        
      }
      
      // Kok Chun, Tan - Shading Removal
      if(ImageProcessingAlgorithm.useShadingRemoval(currentSubtype))
      {
        if(newImage == null)
          newImage = ImageProcessingAlgorithm.applyShadingRemoval(currentSubtype, reconstructedSlice.getImage());
        else
          newImage = ImageProcessingAlgorithm.applyShadingRemoval(currentSubtype, newImage); 
      }
      
      //Siew Yeng - XCR-2389 - Image Enhancer - R Filter
      if(ImageProcessingAlgorithm.useRFilter(currentSubtype))
      {
        if(newImage == null)
          newImage = ImageProcessingAlgorithm.applyRFilter(currentSubtype, reconstructedSlice.getImage());
        else
          newImage = ImageProcessingAlgorithm.applyRFilter(currentSubtype, newImage);    
      }
      
      if(ImageProcessingAlgorithm.useCLAHE(currentSubtype))
      {
        if(newImage == null)
          newImage = ImageProcessingAlgorithm.applyCLAHE(currentSubtype, reconstructedSlice.getImage(), Math.min(padWidth, padHeight));
        else
          newImage = ImageProcessingAlgorithm.applyCLAHE(currentSubtype, newImage, Math.min(padWidth, padHeight));
      }
      
      if(ImageProcessingAlgorithm.useBoxFilter(currentSubtype))
      {
        if(newImage == null)
          newImage = ImageProcessingAlgorithm.removeBackgroundByBoxFilter(currentSubtype, reconstructedSlice.getImage(), padWidth, padHeight);
        else
          newImage = ImageProcessingAlgorithm.removeBackgroundByBoxFilter(currentSubtype, newImage, padWidth, padHeight);
      }
      
      //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - START
      if(ImageProcessingAlgorithm.useFFTBandPassFilter(currentSubtype))
      {
        if(newImage == null)
          newImage = ImageProcessingAlgorithm.applyFFTBandPassFilter(currentSubtype, reconstructedSlice.getImage());
        else
          newImage = ImageProcessingAlgorithm.applyFFTBandPassFilter(currentSubtype, newImage);        
      }
      //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - END
      
      if(newImage != null)
      {
        reconstructedSlice.setEnhancedImageWithoutRotation(newImage);
        
        double degreeRotation = getReconstructionRegion().getRotationCorrectionInDegrees();
        Image image = createOrthogonalImage(degreeRotation, newImage, true);
      
        reconstructedSlice.setEnhancedImage(image);
        newImage.decrementReferenceCount();
        image.decrementReferenceCount();
      }
    }
  }
}
