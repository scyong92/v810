package com.axi.v810.business.imageAcquisition;

import com.axi.util.Assert;
import com.axi.v810.business.testProgram.ReconstructionRegion;
import java.util.Comparator;

/**
 * @author Roy Williams
 */
public class PositionOfImageGroupsOnXaxisComparator implements Comparator<ImageGroup>
{
  /**
   * @author Roy Williams
   */
  private boolean _sortBasedOnLeadingEdge;

  /**
   * @author Roy Williams
   */
  public PositionOfImageGroupsOnXaxisComparator(boolean sortBasedOnLeadingEdge)
  {
    _sortBasedOnLeadingEdge = sortBasedOnLeadingEdge;
  }

  /**
   * @author Roy Williams
   */
  public int compare(ImageGroup imageGroup1,
                     ImageGroup imageGroup2)
  {
    Assert.expect(imageGroup1 != null);
    Assert.expect(imageGroup2 != null);

    if (_sortBasedOnLeadingEdge)
    {
      int maxYofRegion1 = getStartingLocation(imageGroup1);
      int maxYofRegion2 = getStartingLocation(imageGroup2);
      if (maxYofRegion1 == maxYofRegion2)
        return 0;
      return (maxYofRegion1 > maxYofRegion2) ? -1 : 1;
    }

    // else sorting based on trailing edge.
    int minYofRegion1 = getEndingLocation(imageGroup1);
    int minYofRegion2 = getEndingLocation(imageGroup2);
    if (minYofRegion1 == minYofRegion2)
      return 0;
    return (minYofRegion1 > minYofRegion2) ? -1 : 1;
  }

  /**
   * @author Roy Williams
   */
  public static int getStartingLocation(ImageGroup imageGroup)
  {
    return imageGroup.getNominalAreaToImage().getMinX();
  }

  /**
   * @author Roy Williams
   */
  public static int getEndingLocation(ImageGroup imageGroup)
  {
    return imageGroup.getNominalAreaToImage().getMaxX();
  }

}
