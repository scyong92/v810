package com.axi.v810.business.imageAcquisition;

import java.util.Comparator;
import com.axi.util.*;

/**
 * @author Roy Williams
 */
public class ProjectionRequestThreadTaskSystemFiducialImagingOrderComparator implements Comparator<ProjectionRequestThreadTask>
{
  /**
   * @author Roy Williams
   */
  public int compare(ProjectionRequestThreadTask projectionRequestThreadTask1,
                     ProjectionRequestThreadTask projectionRequestThreadTask2)
  {
    Assert.expect(projectionRequestThreadTask1 != null);
    Assert.expect(projectionRequestThreadTask2 != null);

    int result = 0;
    int xDiff = projectionRequestThreadTask1.getProjectionInfo().getXStageWhereFirstRowImagedOnVerticalCenterOfCamera()
                - projectionRequestThreadTask2.getProjectionInfo().getXStageWhereFirstRowImagedOnVerticalCenterOfCamera();
    if (xDiff == 0)
    {
      int yDiff = projectionRequestThreadTask1.getProjectionInfo().getYStageWhereFirstRowImagedOnVerticalCenterOfCamera() -
                  projectionRequestThreadTask2.getProjectionInfo().getYStageWhereFirstRowImagedOnVerticalCenterOfCamera();
      if (yDiff == 0)
        result = 0;
      else if (yDiff < 0)
        result = -1;
      else if (yDiff > 0)
        result = 1;
    }
    else if (xDiff < 0)
      result = -1;
    else if (xDiff > 0)
        result = 1;

    return result;
  }
}
