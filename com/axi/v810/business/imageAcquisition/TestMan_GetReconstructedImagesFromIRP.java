package com.axi.v810.business.imageAcquisition;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Dave Ferguson
 */
public class TestMan_GetReconstructedImagesFromIRP extends UnitTest
{
  private HardwareWorkerThread   _hardwareWorkerThread   = null;
  private XrayTester             _xrayTester             = null;
  private ImageManager _imageManager = ImageManager.getInstance();

  /**
   * @author Dave Ferguson
   */
  protected TestMan_GetReconstructedImagesFromIRP()
  {
    try
    {
      Config.getInstance().loadIfNecessary();
    }
    catch (DatastoreException de)
    {
      de.printStackTrace();
      Assert.expect(false);
    }

    _hardwareWorkerThread = HardwareWorkerThread.getInstance();
    _xrayTester = XrayTester.getInstance();
    try
    {
      _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
      {
        public void run() throws Exception
        {
          _xrayTester.startup();
        }
      });
    }
    catch (XrayTesterException e)
    {
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  private TestProgram createTestProgram()
  {
    Project dummyProject = new Project(true);
    dummyProject.getProjectState().disable();
    dummyProject.getProjectState().enable();
    Panel dummyPanel = new Panel();
    dummyPanel.setProject(dummyProject);
    dummyPanel.setWidthInNanoMeters(25400);
    dummyPanel.setLengthInNanoMeters(25400);
    dummyPanel.setThicknessInNanoMeters(25400*60);
    PanelSettings dummyPanelSettings = new PanelSettings();
    dummyPanelSettings.setPanel(dummyPanel);
    dummyPanelSettings.setDegreesRotationRelativeToCad(0);
    dummyPanel.setPanelSettings(dummyPanelSettings);
    AffineTransform transform = new AffineTransform();
    dummyPanelSettings.setRightManualAlignmentTransform(transform);
    dummyProject.setPanel(dummyPanel);

    Collection<ReconstructionRegion> reconstructionRegions = new ArrayList<ReconstructionRegion>();

/* BGA under JLead
    PanelRectangle bigRegionToReconstruct = new PanelRectangle(MathUtil.convertMilsToNanoMetersInteger( -200 - 2000),
        MathUtil.convertMilsToNanoMetersInteger( -200 + 2200),
        MathUtil.convertMilsToNanoMetersInteger(300),
        MathUtil.convertMilsToNanoMetersInteger(300));

    PanelRectangle bigRegionToReconstruct2 = new PanelRectangle(MathUtil.convertMilsToNanoMetersInteger( -200 - 2000),
        MathUtil.convertMilsToNanoMetersInteger( -200 + 2200),
        MathUtil.convertMilsToNanoMetersInteger(302),
        MathUtil.convertMilsToNanoMetersInteger(302));


    PanelRectangle smallRegionToReconstruct = new PanelRectangle(MathUtil.convertMilsToNanoMetersInteger( -200 - 2000),
        MathUtil.convertMilsToNanoMetersInteger( -200 + 2200),
        MathUtil.convertMilsToNanoMetersInteger(200),
        MathUtil.convertMilsToNanoMetersInteger(200));

    PanelRectangle smallRegionToReconstruct2 = new PanelRectangle(MathUtil.convertMilsToNanoMetersInteger( -200 - 2000),
        MathUtil.convertMilsToNanoMetersInteger( -200 + 2200),
        MathUtil.convertMilsToNanoMetersInteger(201),
        MathUtil.convertMilsToNanoMetersInteger(201));
*/

    int xPosOffset = -4500;
    int yPosOffset = 3500;
    int regionWidth = 300;
    int regionHeight = 300;

    PanelRectangle nominalBounds = null;

    for (int rowTile = 0; rowTile < 1; ++rowTile)
    {
      // Shift the region down
      yPosOffset -= regionHeight;
      for (int columnTile = 0; columnTile < 14; ++columnTile)
      {
        // Shift region to the right
        xPosOffset += regionWidth;

        PanelRectangle smallRegionToReconstruct = new PanelRectangle(MathUtil.convertMilsToNanoMetersInteger( -200 + xPosOffset),
                                                                     MathUtil.convertMilsToNanoMetersInteger( -200 + yPosOffset),
                                                                     MathUtil.convertMilsToNanoMetersInteger(regionWidth),
                                                                     MathUtil.convertMilsToNanoMetersInteger(regionHeight));

        if (nominalBounds == null)
        {
          nominalBounds = new PanelRectangle(smallRegionToReconstruct.getMinX(),
                                             smallRegionToReconstruct.getMinY(),
                                             smallRegionToReconstruct.getWidth(),
                                             smallRegionToReconstruct.getHeight());
        }
        else
        {
          nominalBounds.add(smallRegionToReconstruct);
        }


        // Bottom RR
        ReconstructionRegion rrBottom = new ReconstructionRegion();
        rrBottom.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
        rrBottom.setTopSide(false);
        rrBottom.setRegionRectangleRelativeToPanelInNanoMeters(smallRegionToReconstruct);
        FocusGroup fgBottom = new FocusGroup();
        fgBottom.setSingleSidedRegionOfBoard(false);
        fgBottom.setRelativeZoffsetFromSurfaceInNanoMeters(0);
        FocusSearchParameters fspBottom = new FocusSearchParameters();
        fspBottom.setFocusProfileShape(FocusProfileShapeEnum.PEAK);
        FocusRegion frBottom = new FocusRegion();
        frBottom.setRectangleRelativeToPanelInNanoMeters(smallRegionToReconstruct);
        fspBottom.setFocusRegion(frBottom);
        fgBottom.setFocusSearchParameters(fspBottom);
        FocusInstruction fiBottom = new FocusInstruction();
        fiBottom.setFocusMethod(FocusMethodEnum.SHARPEST);
        Slice sliceBottom = new Slice();
        sliceBottom.setCreateSlice(true);
        sliceBottom.setFocusInstruction(fiBottom);
        sliceBottom.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
        fgBottom.addSlice(sliceBottom);
        rrBottom.addFocusGroup(fgBottom);
        reconstructionRegions.add(rrBottom);

        // Top RR
        ReconstructionRegion rrTop = new ReconstructionRegion();
        rrTop.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
        rrTop.setTopSide(true);
        rrTop.setRegionRectangleRelativeToPanelInNanoMeters(smallRegionToReconstruct);
        FocusGroup fgTop = new FocusGroup();
        fgTop.setSingleSidedRegionOfBoard(false);
        fgTop.setRelativeZoffsetFromSurfaceInNanoMeters(0);
        FocusSearchParameters fspTop = new FocusSearchParameters();
        fspTop.setFocusProfileShape(FocusProfileShapeEnum.PEAK);
        FocusRegion frTop = new FocusRegion();
        frTop.setRectangleRelativeToPanelInNanoMeters(smallRegionToReconstruct);
        fspTop.setFocusRegion(frTop);
        fgTop.setFocusSearchParameters(fspTop);
        FocusInstruction fiTop = new FocusInstruction();
        fiTop.setFocusMethod(FocusMethodEnum.SHARPEST);
        Slice sliceTop = new Slice();
        sliceTop.setCreateSlice(true);
        sliceTop.setFocusInstruction(fiTop);
        sliceTop.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
        fgTop.addSlice(sliceTop);
        rrTop.addFocusGroup(fgTop);
        reconstructionRegions.add(rrTop);

        // Slice deck
        int numDeckSlices = 40;
        int milsPerSliceDeck = 5;
        for (int i = 0; i < numDeckSlices; ++i)
        {
          ReconstructionRegion rrDeck = new ReconstructionRegion();
          rrDeck.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
          rrDeck.setTopSide(false);
          rrDeck.setRegionRectangleRelativeToPanelInNanoMeters(smallRegionToReconstruct);

          FocusInstruction fi = new FocusInstruction();

          fi.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);
          int zOffsetFromReference = 0;
          fi.setZHeightInNanoMeters(MathUtil.convertMilsToNanoMetersInteger((i - numDeckSlices / 2) * milsPerSliceDeck +
              zOffsetFromReference));

          Slice slice = new Slice();
          slice.setCreateSlice(true);
          slice.setFocusInstruction(fi);
          slice.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);

          // Create a focus group with "No Search" to bypass autofocus
          FocusGroup fgDeck = new FocusGroup();
          fgDeck.setSingleSidedRegionOfBoard(false);
          fgDeck.setRelativeZoffsetFromSurfaceInNanoMeters(0);
          FocusSearchParameters fspDeck = new FocusSearchParameters();
          fspDeck.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
          FocusRegion frDeck = new FocusRegion();
          frDeck.setRectangleRelativeToPanelInNanoMeters(smallRegionToReconstruct);
          fspDeck.setFocusRegion(frDeck);
          fgDeck.setFocusSearchParameters(fspDeck);

          fgDeck.addSlice(slice);

          rrDeck.addFocusGroup(fgDeck);
          reconstructionRegions.add(rrDeck);
        }
      }
    }

    TestProgram program = new TestProgram();
    program.setProject(dummyProject);

    TestSubProgram tsp = new TestSubProgram(program);
    // Add verification regions to bypass auto-alignment
    tsp.setVerificationRegions(reconstructionRegions);
    tsp.setInspectionRegions(new ArrayList<ReconstructionRegion>());
    tsp.setAlignmentRegions(new ArrayList<ReconstructionRegion>());

    PanelRectangle enlargedBounds = new PanelRectangle(nominalBounds.getMinX() - (int) MathUtil.convertMilsToNanoMeters(500),
                                                       nominalBounds.getMinY() - (int) MathUtil.convertMilsToNanoMeters(500),
                                                       nominalBounds.getWidth()  + 2 * (int) MathUtil.convertMilsToNanoMeters(500),
                                                       nominalBounds.getHeight() + 2 * (int) MathUtil.convertMilsToNanoMeters(500));

    tsp.setVerificationRegionsBoundsInNanoMeters(enlargedBounds);
    tsp.setPanelLocationInSystem(PanelLocationInSystemEnum.RIGHT);

    tsp.setProcessorStripsForVerificationRegions(tsp.getFakeProcessorStrips());
    program.addTestSubProgram(tsp);

    return program;
  }

  /**
   * @author Dave Ferguson
   */
  public void test(BufferedReader in, PrintWriter out)
  {

    ImageAcquisitionEngine.setDebug(true);
    final TestProgram testProgram = createTestProgram();
    final ImageAcquisitionEngine iae = ImageAcquisitionEngine.getInstance();

    try
    {
      iae.initialize();

      _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
      {
        public void run() throws Exception
        {
          try
          {
            int numRuns = 1;
            for (int run = 0; run < numRuns; ++run)
            {
              TestExecutionTimer testExecutionTimer = new TestExecutionTimer();
              iae.acquireVerificationImages(testProgram, testExecutionTimer);

              BooleanRef abortedWhileGettingImages = new BooleanRef(false);
              int imagesRecv = 0;
              int imagesExpected = testProgram.getVerificationRegions().size();
              while (imagesRecv < imagesExpected)
              {
                ReconstructedImages reconstructedImages = iae.getReconstructedImages(abortedWhileGettingImages);

                if (reconstructedImages == null)
                {
                  System.out.println("Null image");
                }
                else if (abortedWhileGettingImages.getValue())
                {
                  System.out.println("Aborted image");
                }
                else
                {
                  System.out.println("Got image " + reconstructedImages.getReconstructionRegion().getRegionId());
                  imagesRecv++;
                  _imageManager.saveInspectionImagesForOnlineTestDevelopment(reconstructedImages);
                  reconstructedImages.decrementReferenceCount();
                }
              }

/*              for (int i = 0; i < testProgram.getVerificationRegions().size(); ++i)
              {
                iae.reacquireLightestImages(testProgram.getTestSubPrograms().get(0).getId(),
                                    testProgram.getVerificationRegions().get(i));

                ReconstructedImages reconstructedImages = iae.getReconstructedImages(abortedWhileGettingImages);
                int rrId = reconstructedImages.getReconstructionRegionId();
                reconstructedImages.setReconstructionRegionId(rrId);
                _imageManager.saveInspectionImagesForOnlineTestDevelopment(reconstructedImages);
              }
*/

              Thread.sleep(10000);

              for (int i = 0; i < testProgram.getVerificationRegions().size(); ++i)
              {
                ReconstructionRegion region = testProgram.getVerificationRegions().get(i);
                iae.reconstructionRegionComplete(region, true);
                iae.freeReconstructedImages(region);
              }
            }
          }
          catch (XrayTesterException hex)
          {
            hex.printStackTrace();

            // Shouldn't occur.
            Expect.expect(false);
          }
          try
          {
            Thread.sleep(10000);
          }
          catch (Exception e)
          {}

        }
      });
    }
    catch (XrayTesterException e)
    {
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new TestMan_GetReconstructedImagesFromIRP() );
  }
}
