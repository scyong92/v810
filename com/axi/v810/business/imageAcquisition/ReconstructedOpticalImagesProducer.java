package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class ReconstructedOpticalImagesProducer extends OpticalImagesProducer implements Observer
{
    private OpticalImageAcquisitionModeEnum _acquisitionMode = OpticalImageAcquisitionModeEnum.NOT_IN_USE;
    
    // Test program specific properties    
    private boolean                 _acquiringOpticalImagesCompleteInspectionEventSent = false;
    private int                     _panelCoordinateToReconstructComplete = -1;
    private Map<String, PanelCoordinate> _panelCoordinateNameToPanelCoordinateMap = Collections.synchronizedMap(new HashMap<String, PanelCoordinate>());
    
    private TestProgram _testProgram = null;
    
    private boolean _isAcquiringImages = false;
    private Object  _allOpticalImagesReturnedLock = new Object();
    private boolean _userAbortedOfflineOpticalImageAcquisition = false;
    
    private transient OpticalImagingChainProgramGenerator _opticalImagingChainProgramGenerator;
    
    protected static OpticalImageAcquisitionExceptionObservable _opticalImageAcquisitionExceptionObservable = 
                                                                OpticalImageAcquisitionExceptionObservable.getInstance();
        
    //private OpticalImageReconstructionEngine _opticalImageReconstructionEngine;
    
    private static Config _config = Config.getInstance();

    private static PerformanceLogUtil _performanceLog = PerformanceLogUtil.getInstance();
    
    /**
     * Inititalize all hardware subsystems used in reconstruction.
     *
     * @author Cheah Lee Herng
    */
    ReconstructedOpticalImagesProducer()
    {
        _hardwareObservable.addObserver(this);
        _opticalImageAcquisitionExceptionObservable.addObserver(this);
        //_opticalImageReconstructionEngine = OpticalImageReconstructionEngine.getInstance();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public OpticalImageAcquisitionModeEnum getOpticalImageAcquisitionMode()
    {
        Assert.expect(_acquisitionMode != null);

        return _acquisitionMode;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public synchronized boolean areAllOpticalImagesAcquired()
    {
        // Each acquire<XXX>Images and abort will set a flag (_isAcquiringImages)
        // when image acquisition is complete
        if(_isAcquiringImages == false)
          return true;
        
        if (_panelCoordinateToReconstructComplete == _panelCoordinateNameToPanelCoordinateMap.size())
            return true;
        return true;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public int getPanelCoordinateToReconstructComplete()
    {
        return _panelCoordinateToReconstructComplete;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public int getNumberOfPanelCoordinateReconstructComplete()
    {
        Assert.expect(_panelCoordinateNameToPanelCoordinateMap != null);
        return _panelCoordinateNameToPanelCoordinateMap.size();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected boolean checkAllOpticalImagesReturned() throws XrayTesterException
    {
        boolean areAllOpticalImagesAcquired = areAllOpticalImagesAcquired();
        synchronized (_allOpticalImagesReturnedLock)
        {

          if (areAllOpticalImagesAcquired == false)
          {
            try
            {
              _allOpticalImagesReturnedLock.wait(200);
            }
            catch (InterruptedException ex)
            {
              // Do nothing.  The timer expiring or notify all from setAllImagesAreAcquiredFlag
              // will pop us out.
            }
          }
          areAllOpticalImagesAcquired = areAllOpticalImagesAcquired();
        }
        return areAllOpticalImagesAcquired;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public synchronized void setAllImagesAreAcquiredFlag(boolean state)
    {        
        if (state == true)
        {
          notifyAll();
          synchronized (_allOpticalImagesReturnedLock)
          {
            _allOpticalImagesReturnedLock.notifyAll();
          }
        }
    }
    
//    /**
//     * This function moves to a point location (stage coordinate),
//     * make sure micro-controller are in ready state. After that,
//     * Fringe Pattern is displayed on Projector and V810 software
//     * will send command to micro-controller to trigger optical camera
//     * to capture optical image.
//     * 
//     * @author Cheah Lee Herng 
//     */
//    public void acquireProductionOpticalImages(TestProgram testProgram,
//                                               TestExecutionTimer testExecutionTimer,
//                                               OpticalImageAcquisitionModeEnum acquisitionMode) throws XrayTesterException
//    {
//        Assert.expect(testProgram != null);
//        Assert.expect(testExecutionTimer != null);
//        Assert.expect(acquisitionMode != null);
//        
//        if (checkForAbortInProgress())
//        {
//          // must be a user abort.
//          return;
//        }
//        
//         _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_ACQUIRE_OPTICAL_IMAGES);
//         
//        try
//        {
//            _isAcquiringImages = true;
//            _acquiringOpticalImagesCompleteInspectionEventSent = false;
//            
//            // Set the acquisition mode.
//            _acquisitionMode = acquisitionMode;
//            
//            if(checkForAbortInProgress())
//                return;
//            
//            _testProgram = testProgram;
//            
//            _opticalImagingChainProgramGenerator = new OpticalImagingChainProgramGenerator();
//            _opticalImagingChainProgramGenerator.generateOpticalCameraRectangleScanPathForTestProgram(testProgram);
//            
//            if(checkForAbortInProgress())
//                return;
//
//            // Modified by Lim, Seng Yew on 8-Aug-2012
//            // Should bypass at this point since height sensor safety already done while loading panel
//            // Will give height sensor assert if not bypass.
//           // PanelHandler.getInstance().setEnableSafetyLevelSensorForPanelLoading(false);
//            runAcquisitionSequenceForTestSubProgram(testExecutionTimer);
//        }
//        catch (XrayTesterException ex)
//        {
//          abort(ex);
//          if (_acquiringOpticalImagesCompleteInspectionEventSent == false)
//              sendAcquiringOpticalImagesCompleteInspectionEvent(false);
//          
//          throw ex;
//        }
//        finally
//        {
//            // Modified by Lim, Seng Yew on 8-Aug-2012
//            // MUST reset after done operation
//           // PanelHandler.getInstance().setEnableSafetyLevelSensorForPanelLoading(true);
//
//            _isAcquiringImages = false;
//
////          if (_opticalImageReconstructionEngine != null)
////              _opticalImageReconstructionEngine.decrementOpticalReconstructedImages();
//
//          checkForAbortInProgress();
//        }
//        
//        _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_ACQUIRE_OPTICAL_IMAGES);
//    }
    
    /**
     * This function gets the optical images for Alignment usage.
     * This is where we want to integrate PSP with current Alignment process
     * in order to improve the accuracy of board thickness.
     * 
     * Accurate board thickness is important so that we can estimate the bottom
     * surface of the board correctly.
     * 
     * @author Cheah Lee Herng 
     */
    public void acquireAlignmentOpticalImages(TestProgram testProgram,
                                              TestExecutionTimer testExecutionTimer,
                                              OpticalImageAcquisitionModeEnum acquisitionMode) throws XrayTesterException
    {
      Assert.expect(testProgram != null);
      Assert.expect(testExecutionTimer != null);
      Assert.expect(acquisitionMode != null);

      if (checkForAbortInProgress())
      {
        // must be a user abort.
        return;
      }

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_ACQUIRE_OPTICAL_IMAGES);

      try
      {
        _isAcquiringImages = true;
        _acquiringOpticalImagesCompleteInspectionEventSent = false;

        // Set the acquisition mode.
        _acquisitionMode = acquisitionMode;

        if(checkForAbortInProgress())
            return;

        _testProgram = testProgram;

        _opticalImagingChainProgramGenerator = new OpticalImagingChainProgramGenerator();
        _opticalImagingChainProgramGenerator.generateAlignmentOpticalCameraRectangleScanPathForTestProgram(testProgram);

        if(checkForAbortInProgress())
            return;

        // Modified by Lim, Seng Yew on 8-Aug-2012
        // Should bypass at this point since height sensor safety already done while loading panel
        // Will give height sensor assert if not bypass.
        // PanelHandler.getInstance().setEnableSafetyLevelSensorForPanelLoading(false);
        runAlignmentAcquisitionSequenceForTestSubProgram(testExecutionTimer);
      }
      catch (XrayTesterException ex)
      {
        abort(ex);        
        if (_acquiringOpticalImagesCompleteInspectionEventSent == false)
            sendAcquiringOpticalImagesCompleteInspectionEvent(false);
        
        throw ex;
      }
      finally
      {
        // Modified by Lim, Seng Yew on 8-Aug-2012
        // MUST reset after done operation
        //  PanelHandler.getInstance().setEnableSafetyLevelSensorForPanelLoading(true);

        _isAcquiringImages = false;

//          if (_opticalImageReconstructionEngine != null)
//              _opticalImageReconstructionEngine.decrementOpticalReconstructedImages();

        checkForAbortInProgress();
      }

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_ACQUIRE_OPTICAL_IMAGES);
    }
    
//    /**
//     * Gets the image acquisition done for each and every TestSubProgram.
//     * 
//     * @author Cheah Lee Herng
//     */
//    private void runAcquisitionSequenceForTestSubProgram(TestExecutionTimer testExecutionTimer) throws XrayTesterException
//    {
//        Assert.expect(_testProgram != null);
//        Assert.expect(testExecutionTimer != null);
//        Assert.expect(_panelPositioner != null);
//        
//        _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_OPTICAL_ACQUISITION_SEQUENCE_FOR_ALL_SUB_PROGRAMS);
//        
//        if(checkForAbortInProgress())
//            return;
//        
//        long timeInMils = System.currentTimeMillis();
//        String opticalImageSetName = Directory.getDirName(timeInMils);
//        
//        // Prepare optical image directory
//        if (isPspOfflineReconstructionEnabled() == false)
//        {
//          String opticalImageSetDirectory = Directory.getOpticalImagesDir(_testProgram.getProject().getName(), opticalImageSetName);
//          if (FileUtilAxi.existsDirectory(opticalImageSetDirectory) == false)
//          {
//             FileUtilAxi.createDirectory(opticalImageSetDirectory);
//          }
//        }
//        else
//        {
//          String opticalImageSetDirectory = Directory.getOfflineOpticalImagesDir(_testProgram.getProject().getName());
//          if (FileUtilAxi.existsDirectory(opticalImageSetDirectory) == false)
//          {
//             DirectoryNotFoundDatastoreException directoryNotFoundDatastoreException = new DirectoryNotFoundDatastoreException(opticalImageSetDirectory);
//             throw directoryNotFoundDatastoreException;
//          }
//        }
//        
//        // Send Surface Map Running event to TestExecutionObservable
//        List<OpticalPointToPointScan> totalOpticalPointToPointScans = _testProgram.getOpticalPointToPointScans();
//        fireRuntimeSurfaceMapStartedEvent(totalOpticalPointToPointScans);
//        
//        for(TestSubProgram testSubProgram : _testProgram.getAllInspectableSurfaceMapTestSubPrograms())
//        {
//            if (checkForAbortInProgress())
//                return;
//            
//            if (_acquisitionMode.equals(OpticalImageAcquisitionModeEnum.PRODUCTION)) 
//            {
//                Assert.expect(testSubProgram.hasOpticalRegions());      
//
//                List<OpticalPointToPointScan> testSubProgramOpticalPointToPointScans = testSubProgram.getOpticalPointToPointScans();
//                
//                // Before we start running optical image acquisition, we need to sort the points.
//                Collections.sort(testSubProgramOpticalPointToPointScans, 
//                        new OpticalPointToPointScanComparator(true, OpticalPointToPointScanComparatorEnum.OPTICAL_CAMERA_POSITION));
//                Collections.sort(testSubProgramOpticalPointToPointScans, 
//                        new OpticalPointToPointScanComparator(true, OpticalPointToPointScanComparatorEnum.PANEL_COORDINATE_X_IN_NANOMETERS));
//                Collections.sort(testSubProgramOpticalPointToPointScans, 
//                        new OpticalPointToPointScanComparator(true, OpticalPointToPointScanComparatorEnum.PANEL_COORDINATE_Y_IN_NANOMETERS));
//
//                // Next, position the panel according to TestSubProgram
//                if (testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
//                    PanelHandler.getInstance().movePanelToRightSide();
//                else if (testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
//                    PanelHandler.getInstance().movePanelToLeftSide();
//                else
//                    Assert.expect(false, "Invalid PanelLocationInSystemEnum");
//                
//                try
//                {
//                    //_progressMonitor.startMonitoring();
//                    runAcquisitionSequence(testExecutionTimer, 
//                                           testSubProgramOpticalPointToPointScans, 
//                                           testSubProgramOpticalPointToPointScans.size(),
//                                           testSubProgram.getTestProgram().getProject().getName(),
//                                           opticalImageSetName);
//                }
//                catch (XrayTesterException xte)
//                {
//                  throw xte;
//                }
//                finally
//                {
//                    // Clear TestSubProgram OpticalPointToPointScan list
//                    if (testSubProgramOpticalPointToPointScans != null)
//                    {
//                        testSubProgramOpticalPointToPointScans.clear();
//                        testSubProgramOpticalPointToPointScans = null;
//                    }
//                    
//                    // let's stop monitoring even if an exception is thrown.
//                    //_progressMonitor.stopMonitoring();
//                }
//            }
//        }
//        
//        // Send event to UI to inform that we have completed running Surface Map
//        postRunTimeSurfaceMapCompletedEvent();
//        
//        // Clear TestProgram OpticalPointToPointScan list
//        if (totalOpticalPointToPointScans != null)
//        {
//            totalOpticalPointToPointScans.clear();
//            totalOpticalPointToPointScans = null;
//        } 
//                
//        _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_OPTICAL_ACQUISITION_SEQUENCE_FOR_ALL_SUB_PROGRAMS);
//    }
    
    /**
     * Gets the image acquisition done for each and every TestSubProgram.
     * 
     * @author Cheah Lee Herng
     */
    private void runAlignmentAcquisitionSequenceForTestSubProgram(TestExecutionTimer testExecutionTimer) throws XrayTesterException
    {
      Assert.expect(_testProgram != null);
      Assert.expect(testExecutionTimer != null);
      Assert.expect(_panelPositioner != null);

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_OPTICAL_ACQUISITION_SEQUENCE_FOR_ALL_SUB_PROGRAMS);

      if(checkForAbortInProgress())
          return;     

      // Send Surface Map Running event to TestExecutionObservable
      List<OpticalPointToPointScan> totalAlignmentOpticalPointToPointScans = _testProgram.getAlignmentOpticalPointToPointScans();
      fireRuntimeAlignmentSurfaceMapStartedEvent(totalAlignmentOpticalPointToPointScans);

      for(TestSubProgram testSubProgram : _testProgram.getAllInspectableAlignmentSurfaceMapTestSubPrograms())
      {
        if (checkForAbortInProgress())
          return;

        if (_acquisitionMode.equals(OpticalImageAcquisitionModeEnum.PRODUCTION)) 
        {
          Assert.expect(testSubProgram.hasAlignmentOpticalRegions());      

          List<OpticalPointToPointScan> testSubProgramAlignmentOpticalPointToPointScans = testSubProgram.getAlignmentOpticalPointToPointScans();

          // Next, position the panel according to TestSubProgram
          if (testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
            PanelHandler.getInstance().movePanelToRightSide();
          else if (testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
            PanelHandler.getInstance().movePanelToLeftSide();
          else
            Assert.expect(false, "Invalid PanelLocationInSystemEnum");

          try
          {            
            runAlignmentAcquisitionSequence(testExecutionTimer, 
                                            testSubProgramAlignmentOpticalPointToPointScans, 
                                            testSubProgramAlignmentOpticalPointToPointScans.size());
          }
          finally
          {
            // Clear TestSubProgram OpticalPointToPointScan list
            if (testSubProgramAlignmentOpticalPointToPointScans != null)
            {
                testSubProgramAlignmentOpticalPointToPointScans.clear();
                testSubProgramAlignmentOpticalPointToPointScans = null;
            }

            // let's stop monitoring even if an exception is thrown.
            //_progressMonitor.stopMonitoring();
          }
        }
      }
   
      // Modify Focus Instruction with Z-Height from PSP
      for (TestSubProgram tsp : _testProgram.getFilteredTestSubPrograms())
      {
        if (tsp.hasAlignmentOpticalRegions())
        {
          // Update TPS model in each region
          tsp.updateAlignmentModel();

          // Modify reconstruction region focus profile
          tsp.updateAlignmentReconstructionRegionsFocusInstructionWithModelData();
        }
      }

      // Send event to UI to inform that we have completed running Alignment Surface Map
      postRunTimeAlignmentSurfaceMapCompletedEvent();

      // Clear TestProgram OpticalPointToPointScan list
      if (totalAlignmentOpticalPointToPointScans != null)
      {
        totalAlignmentOpticalPointToPointScans.clear();
        totalAlignmentOpticalPointToPointScans = null;
      } 

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_OPTICAL_ACQUISITION_SEQUENCE_FOR_ALL_SUB_PROGRAMS);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public boolean checkForAbortInProgress() throws XrayTesterException
    {
        if (super.checkForAbortInProgress())
        {
          return true;
        }
        else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.OFFLINE))
        {
          // for the offline case, we need to check one more variable
          return _userAbortedOfflineOpticalImageAcquisition;
        }

        return false;
    }
    
    /**
     * @author Cheah Lee Herng
    */
    private void sendAcquiringOpticalImagesCompleteInspectionEvent(boolean normalExit)
    {
      AcquiringOpticalImagesCompleteInspectionEvent acquiringOpticalImagesCompleteInspectionEvent = new AcquiringOpticalImagesCompleteInspectionEvent(normalExit);
      _inspectionEventObservable.sendEventInfo(acquiringOpticalImagesCompleteInspectionEvent);
      _acquiringOpticalImagesCompleteInspectionEventSent = true;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public synchronized void userAbort() throws XrayTesterException
    {
        // no need to abort if nothing is happening!
        if(_isAcquiringImages == false)
          return;

        if (_acquisitionMode.equals(ImageAcquisitionModeEnum.OFFLINE))
        {
          // just set this flag, no need to abort hardware since it is not in use.
          _userAbortedOfflineOpticalImageAcquisition = true;
          return;
        }

        checkForAbortInProgress();

        super.userAbort();

        checkForAbortInProgress();
    }
    
    /**
     * @author Cheah Lee Herng
    */
    private void fireRuntimeSurfaceMapStartedEvent(List<OpticalPointToPointScan> opticalPointToPointScans)
    {
        Assert.expect(opticalPointToPointScans != null);
        
        SurfaceMapInspectionEvent runtimeSurfaceMapStartedEvent =
            new SurfaceMapInspectionEvent(InspectionEventEnum.RUNTIME_SURFACE_MAP_STARTED);
        runtimeSurfaceMapStartedEvent.setOpticalPointToPointScans(opticalPointToPointScans);
        _inspectionEventObservable.sendEventInfo(runtimeSurfaceMapStartedEvent);
    }
    
    /**
     * @author Cheah Lee Herng
    */
    private void fireRuntimeAlignmentSurfaceMapStartedEvent(List<OpticalPointToPointScan> opticalPointToPointScans)
    {
      Assert.expect(opticalPointToPointScans != null);

      SurfaceMapInspectionEvent runtimeAlignmentSurfaceMapStartedEvent =
          new SurfaceMapInspectionEvent(InspectionEventEnum.RUNTIME_ALIGNMENT_SURFACE_MAP_STARTED);
      runtimeAlignmentSurfaceMapStartedEvent.setOpticalPointToPointScans(opticalPointToPointScans);
      _inspectionEventObservable.sendEventInfo(runtimeAlignmentSurfaceMapStartedEvent);
    }
}
