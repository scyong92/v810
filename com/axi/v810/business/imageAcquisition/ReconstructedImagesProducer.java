package com.axi.v810.business.imageAcquisition;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import static com.axi.v810.business.imageAcquisition.ProjectionProducer._alignmentLock;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testExec.VelocityMapping;
import com.axi.v810.business.testExec.VelocityMappingParameters;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.internalTools.*;
import com.axi.v810.util.*;

/**
 * This derived class of ProjectionProducer will control the hardware needed to
 * collect reconstructed images coming from a set of <tt>ImageReconstructionProcessors</tt>
 * and to be delivered to <tt>TestExectutionEngine</tt>.  The protocol/strategy to make this
 * happen has been defined in <tt>ProjectionProducer</tt>.
 *
 * @author Roy Williams
 */
public class ReconstructedImagesProducer extends ProjectionProducer implements Observer
{
  private ReconstructedImagesManager _reconstructedImagesManager;
  private ImageAcquisitionModeEnum _acquisitionMode = ImageAcquisitionModeEnum.NOT_IN_USE;
  private int _reconstructionRegionsToMarkComplete = -1;
  private Map<Integer, ReconstructionRegion> _reconstructionRegionsMarkedCompleteMap = Collections.synchronizedMap(new HashMap<Integer, ReconstructionRegion>());

  // Test program specific properties
  private TestProgram    _testProgram = null;
  private TestSubProgram _testSubProgram = null;
  private boolean        _executingFirstTestSubProgram = true;
  private boolean        _executingDifferentMagnificationTestSubProgram = true;
  private boolean        _acquiringImagesCompleteInspectionEventSent = false;
  private boolean _executingLastTestSubProgram = false;
  private List<TestSubProgram> _filteredTestSubProgram = new ArrayList<TestSubProgram>();

  // Lock to simulate waiting for the alignment result when in sim mode.
  private BooleanLock _simulatedAlignmentResultReceivedLock = new BooleanLock(false);
  private BooleanLock _simulatedIrpPausedLock = new BooleanLock(false);

  // Flag used to denote all cameras, stage, and IRP's must be reprogrammed.
  private ImageAcquisitionModeEnum _lastAcquisitionMode;
  private boolean _reprogramCamerasAndStage = true;
  private boolean _reprogramIrps            = true;
  private boolean _createOnlyInspectedSlices = false;
  private boolean _useOnlyInspectedRegionsInScanPath = false;
  private boolean _useRuntimeManualAlignmentTransform = false; // Siew Yeng - XCR1757

  // References to ProjectionConsumers
  private List<ImageReconstructionEngine> _imageReconstructionEngines = new ArrayList<ImageReconstructionEngine>();
  private InspectionEventObservable _inspectionEventObservable;
  private ImageManager _imageManager;
  private ImageSetGenerator _imageSetGenerator = ImageSetGenerator.getInstance();

  private boolean _isAcquiringImages = false;
  private Object  _allImagesReturnedLock = new Object();
  private boolean _userAbortedOfflineImageAcquisition = false;

  private transient ImagingChainProgramGenerator _imagingChainProgramGenerator;
  private transient OpticalImagingChainProgramGenerator _opticalImagingChainProgramGenerator;

  // Manage when AlignmentRegions are sent to IRP's.  This is a little cached
  // alignmentMatrix to send in lieu of the AlignmentRegions.  Sending the
  // AlignmentRegions will cause this to be set.
  private static ExecuteParallelThreadTasks<Object> _concurrentTasks = new ExecuteParallelThreadTasks<Object>();
  private static ExecuteParallelThreadTasks<Object> _concurrentAbortTasks = new ExecuteParallelThreadTasks<Object>();
  protected static ImageAcquisitionExceptionObservable _imageAcquisitionExceptionObservable = ImageAcquisitionExceptionObservable.getInstance();

  private static Config _config = Config.getInstance();
  private static HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();

  private static PerformanceLogUtil _performanceLog = PerformanceLogUtil.getInstance();

  private long _previousTestProgramCheckSum = -1;
  private long _previousTestProgramOpticalInspectionChecksum = -1;
  private long _previousTestProgramAlignmentOpticalInspectionChecksum = -1;
  
  //private MagnificationTypeEnum _previousTestProgramMagnification = MagnificationTypeEnum.LOW;
  
  private MagnificationTypeEnum _previousTestProgramMagnification = null;
  private DynamicRangeOptimizationVersionEnum _previousDynamicRangeOptimizationVersion = null;
  
  protected static boolean _debug = Config.isDeveloperDebugModeOn();
  private static String _me = "ReconstructedImagesProducer";
  private final static Object _abortObj = new Object();
  
//private double  _stepOverridePercentage;
//private double  _zHeightDeltaAboveNominalInNanometers;
//private double  _zHeightDeltaBelowNominalInNanometers;
  
  /**
   * Inititalize all hardware subsystems used in reconstruction.
   *
   * @author Matt Wharton
   * @author Roy Williams
   */
  ReconstructedImagesProducer(int numberOfIRPs)
  {
    Assert.expect(numberOfIRPs > 0);

    _reconstructedImagesManager = new ReconstructedImagesManager(this);
    _inspectionEventObservable = InspectionEventObservable.getInstance();
    _imageManager = ImageManager.getInstance();
    _hardwareObservable.addObserver(this);

    // Propogate the debug state down to the IRE components.
    if (_debug)
    {
      ImageReconstructionEngine.setDebug(_debug);
    }

    // Initialize the ImageReconstructionEngine instances.
    int numberOfIRPsAllocated = 0;
    for (ImageReconstructionEngineEnum id : ImageReconstructionEngineEnum.getEnumList())
    {
      ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(id);
      _imageReconstructionEngines.add(ire);
      ire.setProjectionProducer(this);

      numberOfIRPsAllocated++;
      if (numberOfIRPsAllocated >= numberOfIRPs)
        break;
    }
    ImagingChainProgramGenerator.setImageReconstructionEngines(_imageReconstructionEngines);
    _imageAcquisitionExceptionObservable.addObserver(this);
  }

  /**
   * @author Roy Williams
   */
  public List<ImageReconstructionEngine> getImageReconstructionEngines()
  {
    return _imageReconstructionEngines;
  }

  /**
   * @author Roy Williams
   */
  public ReconstructedImagesManager getReconstructedImagesManager()
  {
    Assert.expect(_reconstructedImagesManager != null);

    return _reconstructedImagesManager;
  }

  /**
   * @author Roy Williams
   */
  public ImageAcquisitionModeEnum getImageAcquisitionMode()
  {
    Assert.expect(_acquisitionMode != null);

    return _acquisitionMode;
  }

  /**
   * @author Roy Williams
   */
  public void calibrationExecutionHasChangedCameraAndStagePrograms()
  {
    _reprogramCamerasAndStage = true;
  }

  /**
   * @author Roy Williams
   */
  public synchronized boolean areAllImagesAcquired() throws XrayTesterException
  {
    if(checkForAbortInProgress())
      return false;
    // Each acquire<XXX>Images and abort will set a flag (_isAcquiringImages)
    // when image acquisition is complete
    if(_isAcquiringImages == false)
      return true;

    Assert.expect(_reconstructionRegionsToMarkComplete > 0);

    if (_reconstructionRegionsToMarkComplete == _reconstructionRegionsMarkedCompleteMap.size())
      return true;
    return false;
  }

  /**
   * @author Rex Shang
   */
  public int getReconstructionRegionsToMarkComplete()
  {
    return _reconstructionRegionsToMarkComplete;
  }

  /**
   * @author Rex Shang
   */
  public int getNumberOfReconstructionRegionsCompleted()
  {
    return _reconstructionRegionsMarkedCompleteMap.size();
  }

  /**
   * @author Roy Williams
   */
  protected boolean checkAllImagesReturned() throws XrayTesterException
  {
    boolean areAllImagesAcquired = areAllImagesAcquired();
    synchronized (_allImagesReturnedLock)
    {

      if (areAllImagesAcquired == false)
      {
        try
        {
          _allImagesReturnedLock.wait(200);
        }
        catch (InterruptedException ex)
        {
          // Do nothing.  The timer expiring or notify all from setAllImagesAreAcquiredFlag
          // will pop us out.
        }
      }
      areAllImagesAcquired = areAllImagesAcquired();
    }
    return areAllImagesAcquired;
  }

  /**
   * @author Roy Williams
   */
  public synchronized void setAllImagesAreAcquiredFlag(boolean state)
  {
    Assert.expect(_reconstructedImagesManager != null);

    _reconstructedImagesManager.setAllImagesAreAcquiredFlag(state);
    if (state == true)
    {
      notifyAll();
      synchronized (_allImagesReturnedLock)
      {
        _allImagesReturnedLock.notifyAll();
      }
    }
  }

  /**
   * @author Roy Williams
   */
  public ReconstructedImages getReconstructedImages(BooleanRef noImagesReturned) throws XrayTesterException
  {
    Assert.expect(_reconstructedImagesManager != null);
    Assert.expect(noImagesReturned != null);

    return _reconstructedImagesManager.getReconstructedImages(noImagesReturned);
  }

  /**
   * @author Matt Wharton
   */
  public void freeReconstructedImages(ReconstructionRegion reconstructionRegion) throws XrayTesterException
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(_reconstructedImagesManager != null);

    _reconstructedImagesManager.freeReconstructedImages(reconstructionRegion);
  }



  /**
   * @author Roy Williams
   */
  public void initializeReconstructedImagesList() throws DatastoreException
  {
    Assert.expect(_reconstructedImagesManager != null);

    _reconstructedImagesManager.initializeReconstructedImagesList();
  }


  /**
   * @author Roy Williams
   */
  public void setHardwareNeedsToBeReprogrammedOnNextRun()
  {
    _lastAcquisitionMode = null;
  }

  /**
   * Flags to reprogramCamerasAndStage and and reprogramIrps are already set from the
   * previous run.   Once the reprogramming action is performed (later), these
   * flags will be changed to false.
   *
   * @author Roy Williams
   */
  private boolean doesHardwareNeedToBeReProgrammed(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    long currentTestProgramCheckSum = -1;
    MagnificationTypeEnum currentTestProgramMagnification;
    DynamicRangeOptimizationVersionEnum currentDynamicRangeOptimizationVersion;
    // We are looking for any excuse to upgrade the reprogramming of
    // hardware from a partial to full.
    boolean reprogramAll =
      // If not in PROCUDTION mode, always reprogram everything.
      _acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION) == false      ||

      // Okay, we are in PRODUCTION mode but was the last mode the same?  If not,
      // reprogram everything.
      _lastAcquisitionMode == null                                               ||
      _lastAcquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION) == false  ||

      // Last mode was the same! Great!
      // It still could be that a different testProgram has been loaded.
      _testProgram == null                                                       ||
      testProgram.equals(_testProgram) == false                                  ||

      // Last time we ran this testProgram it specified how many slices were going
      // to be created.  If this is not the same from run to run, we must download
      // a new program to the IRPs.
      _createOnlyInspectedSlices != testProgram.isCreateOnlyInspectedSlicesEnabled() ||

      // This is to check the mode we set for UseOnlyInspectionRegion flag.
      // So if it is in different state, we would need to re-download the program
      _useOnlyInspectedRegionsInScanPath != testProgram.isUseOnlyInspectedRegionsInScanPath() ||
      
      // Siew Yeng - XCR1757
      // This is to check if runtime manual alignment is performed. Reprogram if 
      // _useRuntimeManualAlignmentTransform flag changed.
      _useRuntimeManualAlignmentTransform != testProgram.hasRuntimeManualAlignmentTransform();

    if(reprogramAll == false)
    {
      // validate the testProgram checkSum
      // It might be some minor changes on the testProgram and didn't cause it to re-generate
      currentTestProgramCheckSum = testProgram.getCheckSum();
      reprogramAll = currentTestProgramCheckSum != _previousTestProgramCheckSum;
    }
    
    currentTestProgramMagnification = testProgram.getFilteredTestSubPrograms().get(0).getMagnificationType();
    currentDynamicRangeOptimizationVersion = testProgram.getProject().getDynamicRangeOptimizationVersion();
    if(reprogramAll == false)
    {
      //currentTestProgramMagnification = testProgram.getFilteredTestSubPrograms().get(0).getMagnificationType();
      // Kok Chun, Tan - XCR3862 - failed to download test program to irp
      reprogramAll = currentTestProgramMagnification != _previousTestProgramMagnification
                     || currentDynamicRangeOptimizationVersion != _previousDynamicRangeOptimizationVersion;
      //_previousTestProgramMagnification = currentTestProgramMagnification;
    }

    if (reprogramAll)
    {
      _reprogramCamerasAndStage = true;
      _reprogramIrps = true;
    }

    if (_testProgram == null)
      _previousTestProgramCheckSum = testProgram.getCheckSum();
    else
      _previousTestProgramCheckSum = currentTestProgramCheckSum;
    
    _previousTestProgramMagnification = currentTestProgramMagnification;
    _previousDynamicRangeOptimizationVersion = currentDynamicRangeOptimizationVersion;
    _testProgram = testProgram;
    _lastAcquisitionMode = _acquisitionMode;
    _createOnlyInspectedSlices = testProgram.isCreateOnlyInspectedSlicesEnabled();
    _useOnlyInspectedRegionsInScanPath = testProgram.isUseOnlyInspectedRegionsInScanPath();
    _useRuntimeManualAlignmentTransform = testProgram.hasRuntimeManualAlignmentTransform(); // Siew Yeng - XCR1757
    return _reprogramCamerasAndStage || _reprogramIrps ||
           doesAlignmentOpticalHardwareNeedToBeReProgrammed(testProgram) ||
           doesOpticalHardwareNeedToBeReProgrammed(testProgram);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private boolean doesAlignmentOpticalHardwareNeedToBeReProgrammed(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);
    
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP) == false)
      return false;
    
    // Check Alignment Optical Inspection checksum
    long currentTestProgramAlignmentOpticalInspectionChecksum = testProgram.getAlignmentOpticalInspectionChecksum();
    boolean reprogramAlignmentOptical = currentTestProgramAlignmentOpticalInspectionChecksum != _previousTestProgramAlignmentOpticalInspectionChecksum;
    
    if (reprogramAlignmentOptical)
    {
      _previousTestProgramAlignmentOpticalInspectionChecksum = currentTestProgramAlignmentOpticalInspectionChecksum;
    }
    
    return reprogramAlignmentOptical;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private boolean doesOpticalHardwareNeedToBeReProgrammed(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);
    
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP) == false)
      return false;     

    // Check Optical Inspection checksum
    long currentTestProgramOpticalInspectionChecksum = testProgram.getOpticalInspectionChecksum();
    boolean reprogramOptical = currentTestProgramOpticalInspectionChecksum != _previousTestProgramOpticalInspectionChecksum;
    
    if (reprogramOptical)
    {      
      _previousTestProgramOpticalInspectionChecksum = currentTestProgramOpticalInspectionChecksum;
    }
    
    return reprogramOptical;
  }    

  /**
   * Scan the panel and return reconstructed images through an
   * Observer/Observable design pattern.
   *
   * This call will return when the scan path is complete.  However,
   * images are returned asynchronously on a separate thread.  Therefore,
   * images may still be generated even after this function returns.
   * <p>
   * Don't make this function synchronized!
   * <p>
   * This function was originally synchronized.  This was removed because one thread
   * (typically the HardwareWorkerThread) held the Java monitor for the one-and-only
   * instance - this.  Since the monitor was held by this synchronized function,
   * abort() became blocked.  This deadlock was resolved by giving abort the
   * exclusive rights to this monitor.
   *
   * @author Roy Williams
   * @edited by Kee Chin Seong  - when acquire images, invalidate check sum will need reset the mean step size!
   */
  public void acquireImages(TestProgram testProgram,
                            TestExecutionTimer testExecutionTimer,
                            double zHeightDeltaAboveNominalInNanometers,
                            double zHeightDeltaBelowNominalInNanometers,
                            ImageAcquisitionModeEnum acquisitionMode,
                            double stepOverridePercentage) throws XrayTesterException

  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(acquisitionMode != null);

    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_ACQUIRE_IMAGES);

    try
    {
      _isAcquiringImages = true;

      //For Speed Up    
      if (InnerBarrier.isInstalled())
      {
        //Swee Yee Wong - XCR-3321 Sanmina board inspection time slower.     
        //Kok Chun, Tan - XCR-3415 - When not in production mode, use InnerBarrier.getInstance().open(). 
        //This function will wait until the inner barrier is fully opened.
        //This is to avoid to get black image intermittent.
        //But for production, we need to speed up, direct open the inner barrier and no need wait. (Sanmina slow issues) 
        //Without waiting the innerbarrier task can increase cycle time by around 1 second
        if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION))
          DigitalIo.getInstance().openInnerBarrier();
        else
          InnerBarrier.getInstance().open();
      }

    // Abort must be checked after setting _isAcquiringImages to true
      // Just before this function was called (by ImageAcquisitionEngine) a call
      // was made to clearForNextRun().   Then a call was made to declare that we
      // were starting on the worker thread.   At that time, HardwareTaskEngine
      // has an opportunity to do other actions on the thread destined for here.
      // During that window of time, an abort or hardware exception may have occured.
      if (checkForAbortInProgress())
      {
        // must be a user abort.
        return;
      }
    
      _acquiringImagesCompleteInspectionEventSent = false;
      _testExecutionTimer = testExecutionTimer;

      // Set the acquisition mode.
      _acquisitionMode = acquisitionMode;

      // Check to see if we need to reprogram the hardware.
      if (doesHardwareNeedToBeReProgrammed(testProgram))
      {
        if(checkForAbortInProgress())
          return;

       // As the program is created (below), it will be attached (associated) to
       // the TestProgram and TestSubProgram.
       _imagingChainProgramGenerator = new ImagingChainProgramGenerator(
           acquisitionMode,
           ImagingChainProgramGenerator.getDefaultScanStrategy(),
           stepOverridePercentage);
       
       // Generate what we need for cameras, stage and IRP's => hardware.
       _performanceLog.logMilestone(PerformanceLogMilestoneEnum.GENERATE_IMAGE_CHAIN_PROGRAM);
       _imagingChainProgramGenerator.generateProgramsForLowerImagingChainHardware(
          testProgram,
          testExecutionTimer,
          zHeightDeltaAboveNominalInNanometers,
          zHeightDeltaBelowNominalInNanometers);
       
       // Generate optical scan path information, if there is any available
       _opticalImagingChainProgramGenerator = new OpticalImagingChainProgramGenerator();
       if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP))
       {
         if ((acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION) || 
              acquisitionMode.equals(ImageAcquisitionModeEnum.IMAGE_SET)))
         {
           _opticalImagingChainProgramGenerator.generateAlignmentOpticalCameraRectangleScanPathForTestProgram(testProgram);
           _opticalImagingChainProgramGenerator.generateOpticalCameraRectangleScanPathForTestProgram(testProgram);
         }
         else if (acquisitionMode.equals(ImageAcquisitionModeEnum.ALIGNMENT))
         {
           _opticalImagingChainProgramGenerator.generateAlignmentOpticalCameraRectangleScanPathForTestProgram(testProgram);
         }
       }
       // get velocity mapping parameters from the table
       if (_config.getBooleanValue(SoftwareConfigEnum.USE_VELOCITY_MAPPING))
         testProgram.setVelocityMappingParameters(getVelocityMappingParameters(testProgram));
      }
      // Added by Khang Wah, 2013-09-12, RT PSH
      for (ReconstructionRegion region : testProgram.getFilteredInspectionRegions())
      {
        region.resetProducerState();
      }

      if(checkForAbortInProgress())
        return;

      // Check to see if xrays are on
      //turnXraysOn(testExecutionTimer);
      
      // turnXraysOnAndMoveStageNearToTheFirstScanPositionForProductionInspection(testExecutionTimer); 

      if(checkForAbortInProgress())
        return;

      // Run scan path
      runAcquisitionSequenceForEachTestSubProgram(testExecutionTimer, getImageReconstructionEngines(), acquisitionMode);
      
      //For Speed Up - this is called inside runAcquisitionSequenceForEachTestSubProgram
      //turnXraysOffAndMoveStageToPanelUnloadPositionForProductionInspection();
    }
    catch (XrayTesterException ex)
    {
      abort(ex);
      try
      {
        // Swee yee - Make sure xray tube is at home position during abort
//        if(testProgram.getProject().getPanel().getPanelSettings().hasHighMagnificationComponent())
//        {
          if (XrayCylinderActuator.isInstalled() == true)
          {
            moveXrayCylinderUp();
          }
          else if (XrayCPMotorActuator.isInstalled() == true)
          {
            moveXrayZaxisHome();
          }
//        }
        // When an exception has occured, we need to eject the panel.  I realize
        // this same turnXraysOff() call is made in the finally block.  It's too
        // late.   If we don't do it (here in the catch block) the door will be
        // opened triping the interlocks.        
        turnXraysOff();
      }
      catch (XrayTesterException exx)
      {
        // We are already handling the original exception.  Do nothing.
        exx.printStackTrace();
      }
      if (_acquiringImagesCompleteInspectionEventSent == false)
        sendAcquiringImagesCompleteInspectionEvent(false);
    }
    finally
    {
      _isAcquiringImages = false;

      // We need to turn on synthetic triggers to keep the cameras warm.
      _cameraTriggerBoard.on();

      // CR1046 fix by LeeHerng - We need to dereference IRE here
      decrementIreReference();
      
      // XCR-3104 System crash when abort immediately during run alignment
      // Added by LeeHerng - Clear reference in ImagingChainProgramGenerator
      if (_imagingChainProgramGenerator != null)
        _imagingChainProgramGenerator.clearProjectInfo();

      if(testProgram.getProject().getPanel().getPanelSettings().hasHighMagnificationComponent())
      {
        if (XrayCylinderActuator.isInstalled() == true)
        {
          moveXrayCylinderUp();
        }
        else if (XrayCPMotorActuator.isInstalled() == true)
        {
          moveXrayZaxisHome();
        }
        if(ImagingChainProgramGenerator.getEnableStepSizeOptimization())
          ImagingChainProgramGenerator.forceCalculateMaxStepSizeInNanometers(testProgram.getProject().getPanel().getThicknessInNanometers(), testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers());
      }
      checkForAbortInProgress();
    }
    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_ACQUIRE_IMAGES);
  }
  
  /**
   * @author Anthony Fong
   * only used when _fastSpeedEnabled is set to true
   */
   public void fastSpeedEnabledAcquireImages(TestProgram testProgram,
                            TestExecutionTimer testExecutionTimer,
                            double zHeightDeltaAboveNominalInNanometers,
                            double zHeightDeltaBelowNominalInNanometers,
                            ImageAcquisitionModeEnum acquisitionMode,
                            double stepOverridePercentage) throws XrayTesterException

  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(acquisitionMode != null);
    
    
     

    //For Speed Up    
//    if (InnerBarrier.isInstalled())
//    {
//       DigitalIo.getInstance().openInnerBarrier();
//    }
    // Just before this function was called (by ImageAcquisitionEngine) a call
    // was made to clearForNextRun().   Then a call was made to declare that we
    // were starting on the worker thread.   At that time, HardwareTaskEngine
    // has an opportunity to do other actions on the thread destined for here.
    // During that window of time, an abort or hardware exception may have occured.
    if (checkForAbortInProgress())
    {
      // must be a user abort.
      return;
    }

    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_ACQUIRE_IMAGES);

    try
    {
      _isAcquiringImages = true;
      _acquiringImagesCompleteInspectionEventSent = false;
      
      _testExecutionTimer = testExecutionTimer;

      _fastTestExecutionTimer = testExecutionTimer;
      // Set the acquisition mode.
      _acquisitionMode = acquisitionMode;
      
//      _stepOverridePercentage=stepOverridePercentage;
//      _zHeightDeltaAboveNominalInNanometers=zHeightDeltaAboveNominalInNanometers;
//      _zHeightDeltaBelowNominalInNanometers=zHeightDeltaBelowNominalInNanometers;

      //_testProgram = testProgram;
      
      runPanelLoadingAndImageAcquisitionInitializationInParallel(testProgram,
                            testExecutionTimer,
                             zHeightDeltaAboveNominalInNanometers,
                             zHeightDeltaBelowNominalInNanometers,
                             acquisitionMode,
                             stepOverridePercentage) ;
      
      testExecutionTimer = _fastTestExecutionTimer;

      if(checkForAbortInProgress())
        return;

      // Check to see if xrays are on
      //turnXraysOn(testExecutionTimer);
      turnXraysOnAndMoveStageNearToTheFirstScanPositionForProductionInspection(testExecutionTimer); 

      if(checkForAbortInProgress())
        return;

      // Run scan path
      runAcquisitionSequenceForEachTestSubProgram(testExecutionTimer, getImageReconstructionEngines(), acquisitionMode);
      
      //For Speed Up
      //turnXraysOffAndMoveStageToPanelUnloadPositionForProductionInspection();
    }
    catch (XrayTesterException ex)
    {
      abort(ex);
      try
      {
        // Swee yee - Make sure xray tube is at home position during abort
//        if(testProgram.getProject().getPanel().getPanelSettings().hasHighMagnificationComponent())
//        {
          if (XrayCylinderActuator.isInstalled() == true)
          {
            moveXrayCylinderUp();
          }
          else if (XrayCPMotorActuator.isInstalled() == true)
          {
            moveXrayZaxisHome();
          }
//        }
        // When an exception has occured, we need to eject the panel.  I realize
        // this same turnXraysOff() call is made in the finally block.  It's too
        // late.   If we don't do it (here in the catch block) the door will be
        // opened triping the interlocks.        
        turnXraysOff();
      }
      catch (XrayTesterException exx)
      {
        // We are already handling the original exception.  Do nothing.
        exx.printStackTrace();
      }
      if (_acquiringImagesCompleteInspectionEventSent == false)
        sendAcquiringImagesCompleteInspectionEvent(false);
    }
    finally
    {
      _isAcquiringImages = false;

      // We need to turn on synthetic triggers to keep the cameras warm.
      _cameraTriggerBoard.on();

      // CR1046 fix by LeeHerng - We need to dereference IRE here
      decrementIreReference();
      
      // XCR-3104 System crash when abort immediately during run alignment
      // Added by LeeHerng - Clear reference in ImagingChainProgramGenerator
      if (_imagingChainProgramGenerator != null)
        _imagingChainProgramGenerator.clearProjectInfo();

      if(testProgram.getProject().getPanel().getPanelSettings().hasHighMagnificationComponent())
      {
        if (XrayCylinderActuator.isInstalled() == true)
        {
          moveXrayCylinderUp();
        }
        else if (XrayCPMotorActuator.isInstalled() == true)
        {
          moveXrayZaxisHome();
        }
        if(ImagingChainProgramGenerator.getEnableStepSizeOptimization())
          ImagingChainProgramGenerator.forceCalculateMaxStepSizeInNanometers(testProgram.getProject().getPanel().getThicknessInNanometers(), testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers());
      }
      checkForAbortInProgress();
    }
    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_ACQUIRE_IMAGES);
  }
  
   
   /**
   * @author Anthony Fong - only used when _fastSpeedEnabled is set to true
   * @edited by Kee Chin Seong  - when acquire images, invalidate check sum will need reset the mean step size!
   */
  public void runPanelLoadingAndImageAcquisitionInitializationInParallel(final TestProgram testProgram,
                            TestExecutionTimer testExecutionTimer2,
                            final double zHeightDeltaAboveNominalInNanometers,
                            final  double zHeightDeltaBelowNominalInNanometers,
                            final ImageAcquisitionModeEnum acquisitionMode,
                            final double stepOverridePercentage) throws XrayTesterException

  {
    {    
        ExecuteParallelThreadTasks<XrayTesterException> parallelThread = new ExecuteParallelThreadTasks<XrayTesterException>();
      
        ThreadTask<XrayTesterException> task1 = new ThreadTask<XrayTesterException>("fastSpeedEnabled_LoadPanelIntoMachineWithoutClearingSerialNumbers")
        {

          protected XrayTesterException executeTask() throws XrayTesterException
          {
            XrayTesterException exception = null;
            try
            {
              TestExecution.getInstance().fastSpeedEnabled_LoadPanelIntoMachineWithoutClearingSerialNumbers();
            }
            catch (XrayTesterException xte)
            {
              exception = xte;
              throw xte;
            }

            return exception;
          }
        };

        ThreadTask<XrayTesterException> task2 = new ThreadTask<XrayTesterException>("TurnXraysOnForProductionInspection")
        {

          protected XrayTesterException executeTask() throws XrayTesterException
          {
            XrayTesterException exception = null;
            try
            {
                  // Just before this function was called (by ImageAcquisitionEngine) a call
                  // was made to clearForNextRun().   Then a call was made to declare that we
                  // were starting on the worker thread.   At that time, HardwareTaskEngine
                  // has an opportunity to do other actions on the thread destined for here.
                  // During that window of time, an abort or hardware exception may have occured.
                  if (checkForAbortInProgress())
                  {
                    // must be a user abort.
                    return exception;
                  }

                  _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_ACQUIRE_IMAGES);

                  try
                  {
          
                    // Check to see if we need to reprogram the hardware.
                    if (doesHardwareNeedToBeReProgrammed(testProgram))
                    {
                      if(checkForAbortInProgress())
                        return exception;

                     // As the program is created (below), it will be attached (associated) to
                     // the TestProgram and TestSubProgram.
                     _imagingChainProgramGenerator = new ImagingChainProgramGenerator(
                         acquisitionMode,
                         ImagingChainProgramGenerator.getDefaultScanStrategy(),
                         stepOverridePercentage);
       
                     // Generate what we need for cameras, stage and IRP's => hardware.
                     _performanceLog.logMilestone(PerformanceLogMilestoneEnum.GENERATE_IMAGE_CHAIN_PROGRAM);
                     _imagingChainProgramGenerator.generateProgramsForLowerImagingChainHardware(
                        testProgram,
                        _fastTestExecutionTimer,
                        zHeightDeltaAboveNominalInNanometers,
                        zHeightDeltaBelowNominalInNanometers);

                     // Generate optical scan path information, if there is any available
                     if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP))
                     {
                       _opticalImagingChainProgramGenerator = new OpticalImagingChainProgramGenerator();
                       _opticalImagingChainProgramGenerator.generateAlignmentOpticalCameraRectangleScanPathForTestProgram(testProgram);
                       _opticalImagingChainProgramGenerator.generateOpticalCameraRectangleScanPathForTestProgram(testProgram);
                     }
                    }
                    // Added by Khang Wah, 2013-09-12, RT PSH
                    for (ReconstructionRegion region : testProgram.getFilteredInspectionRegions())
                    {
                      region.resetProducerState();
                    }

                    if(checkForAbortInProgress())
                      return exception;

                  
                  }
                  catch (XrayTesterException ex)
                  {
                    abort(ex);
                    try
                    {
                      // Swee yee - Make sure xray tube is at home position during abort
//                      if(testProgram.getProject().getPanel().getPanelSettings().hasHighMagnificationComponent())
//                      {
                        if (XrayCylinderActuator.isInstalled() == true)
                        {
                          moveXrayCylinderUp();
                        }
                        else if (XrayCPMotorActuator.isInstalled() == true)
                        {
                          moveXrayZaxisHome();
                        }
//                      }
                      // When an exception has occured, we need to eject the panel.  I realize
                      // this same turnXraysOff() call is made in the finally block.  It's too
                      // late.   If we don't do it (here in the catch block) the door will be
                      // opened triping the interlocks.        
                      turnXraysOff();
                    }
                    catch (XrayTesterException exx)
                    {
                      // We are already handling the original exception.  Do nothing.
                      exx.printStackTrace();
                    }
                    if (_acquiringImagesCompleteInspectionEventSent == false)
                      sendAcquiringImagesCompleteInspectionEvent(false);
                  }
                  finally
                  {
                    _isAcquiringImages = false;

                    // We need to turn on synthetic triggers to keep the cameras warm.
                    _cameraTriggerBoard.on();

                    // CR1046 fix by LeeHerng - We need to dereference IRE here
                    decrementIreReference();

                    // XCR-3104 System crash when abort immediately during run alignment
                    // Added by LeeHerng - Clear reference in ImagingChainProgramGenerator
                    if (_imagingChainProgramGenerator != null)
                      _imagingChainProgramGenerator.clearProjectInfo();

                    if(testProgram.getProject().getPanel().getPanelSettings().hasHighMagnificationComponent())
                    {
                      if (XrayCylinderActuator.isInstalled() == true)
                      {
                        moveXrayCylinderUp();
                      }
                      else if (XrayCPMotorActuator.isInstalled() == true)
                      {
                        moveXrayZaxisHome();
                      }
                      if(ImagingChainProgramGenerator.getEnableStepSizeOptimization())
                        ImagingChainProgramGenerator.forceCalculateMaxStepSizeInNanometers(testProgram.getProject().getPanel().getThicknessInNanometers(), testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers());
                    }
                    checkForAbortInProgress();
                  }
            }
            catch (XrayTesterException xte)
            {
              exception = xte;
              throw xte;
            }

            return exception;
          }
        };

        parallelThread.submitThreadTask(task1);
        parallelThread.submitThreadTask(task2);
        try
        {
          parallelThread.waitForTasksToComplete();
        }
        catch (XrayTesterException xex)
        {

          throw xex;

        }
        catch (Exception e)
        {
          Assert.logException(e);
        }
      }
}

  
  /**
   * @author Anthony Fong
   * only used when _fastSpeedEnabled is set to true
   */
  public void turnXraysOffAndMoveStageToPanelUnloadPositionForProductionInspection() throws XrayTesterException
  {
    //PanelHandler.getInstance().moveStageToUnloadPanelPosition(); 
    //turnXraysOff();
    if(_acquisitionMode == ImageAcquisitionModeEnum.PRODUCTION && _panelHandler.isPanelLoaded())
    {
      PanelHandler.getInstance().prepareToUnloadPanel(); 
    }
  }
 /**
   * @author Anthony Fong
   * only used when _fastSpeedEnabled is set to true
   */
  public void turnXraysOnAndMoveStageNearToTheFirstScanPositionForProductionInspection(TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
//    if(_debug)
//    {
//      List<ScanPass>  scanPath =  _testProgram.getScanPasses();
//      for (ScanPass scanPass : scanPath)
//      {
//        System.out.println("pass number: " + scanPass.getId() +
//                           "    direction: " + scanPass.getStageDirection());
//        StagePosition startPosition = scanPass.getStartPointInNanoMeters();
//        System.out.println("    start x: " + startPosition.getXInNanometers() +
//                           "    start y: " + startPosition.getYInNanometers());
//        StagePosition endPosition = scanPass.getEndPointInNanoMeters();
//        System.out.println("    start x: " + endPosition.getXInNanometers() +
//                           "    start y: " + endPosition.getYInNanometers());
//      }
//    }
        
       ExecuteParallelThreadTasks<XrayTesterException> parallelThread = new ExecuteParallelThreadTasks<XrayTesterException>();
//        ThreadTask<Object> task1 = new MoveStageToCalPositionThreadTask();

    ThreadTask<XrayTesterException> task1 = new ThreadTask<XrayTesterException>("MoveStageToFirstAlignmentPosition")
    {

      protected XrayTesterException executeTask() throws XrayTesterException
      {
        XrayTesterException exception = null;
        try
        {
          List<ScanPass> scanPath = _testProgram.getScanPasses();
          StagePosition firstAlignmentPosition = scanPath.get(0).getStartPointInNanoMeters();
          StagePositionMutable stagePosition;
          // Define stage position
          stagePosition = new StagePositionMutable();
          stagePosition.setXInNanometers(firstAlignmentPosition.getXInNanometers());
          //stagePosition.setYInNanometers(firstAlignmentPosition.getYInNanometers()-98400000 + 73935000);
          stagePosition.setYInNanometers(firstAlignmentPosition.getYInNanometers() - PanelPositioner._STAGE_READY_OFFSET_POSITION);

          // Save off the previously loaded motion profile
          MotionProfile originalMotionProfile = _panelPositioner.getActiveMotionProfile();
          _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
          _panelPositioner.pointToPointMoveAllAxes(stagePosition);
          // Now that we are done with the move, restore the motion profile.
          _panelPositioner.setMotionProfile(originalMotionProfile);
        }
        catch (XrayTesterException xte)
        {
          exception = xte;
          throw xte;
        }

        return exception;
      }
    };

    ThreadTask<XrayTesterException> task2 = new ThreadTask<XrayTesterException>("TurnXraysOnForProductionInspection")
    {

      protected XrayTesterException executeTask() throws XrayTesterException
      {
        XrayTesterException exception = null;
        try
        {
          turnXraysOn(_testExecutionTimer);
        }
        catch (XrayTesterException xte)
        {
          exception = xte;
          throw xte;
        }

        return exception;
      }
    };

    parallelThread.submitThreadTask(task1);
    parallelThread.submitThreadTask(task2);
    try
    {
      parallelThread.waitForTasksToComplete();
    }
    catch (XrayTesterException xex)
    {

      throw xex;

    }
    catch (Exception e)
    {
      Assert.logException(e);
    }
}

  /**
   * @author sheng chuan
   */
  private boolean checkIsLastTestSubProgram()
  {
    int inspectedTestSubProgramCount = 0;
    for(TestSubProgram testSubProgram : _testProgram.getFilteredBoardsScanPathTestSubPrograms())
    {
      if(testSubProgram.isTestSubProgramDone())
        inspectedTestSubProgramCount++;
    }
    if(inspectedTestSubProgramCount == _testProgram.getFilteredBoardsScanPathTestSubPrograms().size() - 1)
      return true;
    else
      return false;
  }
  
  /**
   * @author Anthony Fong
   */
  public void parallelTask1ForProductionInspection(TestExecutionTimer testExecutionTimer, final ImageAcquisitionModeEnum acquisitionMode) throws XrayTesterException
  {
    ExecuteParallelThreadTasks<XrayTesterException> parallelThread = new ExecuteParallelThreadTasks<XrayTesterException>();
//        ThreadTask<Object> task1 = new MoveStageToCalPositionThreadTask();

    ThreadTask<XrayTesterException> task1 = new ThreadTask<XrayTesterException>("MoveStageToFirstAlignmentPosition")
    {

      protected XrayTesterException executeTask() throws XrayTesterException
      {
        XrayTesterException exception = null;
        try
        {
          List<ScanPass> scanPath = _testProgram.getScanPasses();
          StagePosition firstAlignmentPosition = scanPath.get(0).getStartPointInNanoMeters();
          StagePositionMutable stagePosition;
          // Define stage position
          stagePosition = new StagePositionMutable();
          stagePosition.setXInNanometers(firstAlignmentPosition.getXInNanometers());
          //stagePosition.setYInNanometers(firstAlignmentPosition.getYInNanometers()-98400000 + 73935000);
          stagePosition.setYInNanometers(firstAlignmentPosition.getYInNanometers() - 24465000);

          // Save off the previously loaded motion profile
          MotionProfile originalMotionProfile = _panelPositioner.getActiveMotionProfile();
          _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
          _panelPositioner.pointToPointMoveAllAxes(stagePosition);
          // Now that we are done with the move, restore the motion profile.
          _panelPositioner.setMotionProfile(originalMotionProfile);
        }
        catch (XrayTesterException xte)
        {
          exception = xte;
          throw xte;
        }

        return exception;
      }
    };

    ThreadTask<XrayTesterException> task2 = new ThreadTask<XrayTesterException>("TurnXraysOnForProductionInspection")
    {

      protected XrayTesterException executeTask() throws XrayTesterException
      {
        XrayTesterException exception = null;
        try
        {
          turnXraysOn(_testExecutionTimer);
              //if(XrayActuator.isInstalled()) 
          //  XrayActuator.getInstance().home(false,false);

          if (acquisitionMode.equals(ImageAcquisitionModeEnum.COUPON))
          {
            try
            {
              Thread.sleep(_config.getIntValue(SoftwareConfigEnum.CALIB_DELAY_IN_MILLISSECONDS));
            }
            catch (InterruptedException ex)
            {
              // do nothing.
            }
          }

        }
        catch (XrayTesterException xte)
        {
          exception = xte;
          throw xte;
        }

        return exception;
      }
    };

    ThreadTask<XrayTesterException> task3 = new ThreadTask<XrayTesterException>("TurnXraysOnForProductionInspection")
    {

      protected XrayTesterException executeTask() throws XrayTesterException
      {
        XrayTesterException exception = null;
        try
        {
                  // Reset the ReconstructedImagesManager.  He manages the images coming up
          // from the IRP's.
          _performanceLog.logMilestone(PerformanceLogMilestoneEnum.INITIALIZE_RECONSTRUCTED_IMAGES_MANAGER);
          initializeReconstructedImagesManager(_testProgram);

        }
        catch (XrayTesterException xte)
        {
          exception = xte;
          throw xte;
        }

        return exception;
      }
    };

    ThreadTask<XrayTesterException> task4 = new ThreadTask<XrayTesterException>("TurnXraysOnForProductionInspection")
    {

      protected XrayTesterException executeTask() throws XrayTesterException
      {
        XrayTesterException exception = null;
        try
        {
                // Regardless of whether we have reprogrammed the cameras we must tell them
          // acquisition of images is about to commence.  This need only be done for
          // the whole TestProgram.
          _performanceLog.logMilestone(PerformanceLogMilestoneEnum.INITIALIZE_CAMERAS);
          initializeCameras();
        }
        catch (XrayTesterException xte)
        {
          exception = xte;
          throw xte;
        }

        return exception;
      }
    };

    parallelThread.submitThreadTask(task1);
    parallelThread.submitThreadTask(task2);
    parallelThread.submitThreadTask(task3);
    parallelThread.submitThreadTask(task4);
    try
    {
      parallelThread.waitForTasksToComplete();
    }
    catch (XrayTesterException xex)
    {

      throw xex;

    }
    catch (Exception e)
    {
      Assert.logException(e);
    }
  }

  /**
   * @author Roy Williams
   */
  public ImagingChainProgramGenerator getImagingChainProgramGenerator()
  {
    return _imagingChainProgramGenerator;
  }

  /**
   * Fires an event that we're starting the scans for runtime alignment for a particular TestSubProgram.
   *
   * @author Matt Wharton
   */
  private void fireRuntimeAlignmentStartedEvent()
  {
    AlignmentInspectionEvent runtimeAlignmentStartedEvent =
        new AlignmentInspectionEvent(InspectionEventEnum.RUNTIME_ALIGNMENT_STARTED);
    _inspectionEventObservable.sendEventInfo(runtimeAlignmentStartedEvent);
  }

  /**
   * @author Matt Wharton
   */
  void acquireSimulationVerificationImages(TestProgram testProgram, TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);

    try
    {
      _testExecutionTimer = testExecutionTimer;

      _isAcquiringImages = true;
      _simulatedIrpPausedLock.setValue(false);

      // Set the acquisition mode.
      _acquisitionMode = ImageAcquisitionModeEnum.VERIFICATION;

      // Clear any previous abort flags.
      clearForNextRun();

      // Tell the images manager what images to expect.
      Collection<ReconstructionRegion> reconstructionRegions = new ArrayList<ReconstructionRegion>();
      for (TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
      {
        reconstructionRegions.addAll(getReconstructionRegions(testSubProgram, true));
      }
      _reconstructedImagesManager.setRequestedRegions(reconstructionRegions);

      // Initialize the verification image generator.
      _imageSetGenerator.initializeForVerificationImages(testProgram);

      for (TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
      {
        // Get the verification images.
        for (ReconstructionRegion verificationRegion : testSubProgram.getVerificationRegions())
        {
          ReconstructedImages verificationImages = _imageSetGenerator.generateVerificationImages(verificationRegion);
          _reconstructedImagesManager.putReconstructedImages(verificationImages);

          verificationImages.decrementReferenceCount();
          if(checkForAbortInProgress())
            return;

          try
          {
            _simulatedIrpPausedLock.waitUntilFalse();
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }

          if(checkForAbortInProgress())
            return;
        }
      }

      _lastAcquisitionMode = _acquisitionMode;
    }
    finally
    {
      _imageSetGenerator.doneGeneratingVerificationImages();
      _isAcquiringImages = false;
    }
  }

  /**
   * Simulates alignment image acquisition.  This involves generating fake alignment images.
   * These images are returned via the normal channel of the ReconstructedImagesManager.
   *
   * @author Matt Wharton
   */
  void acquireSimulationAlignmentImages(TestProgram testProgram, TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);

    try
    {
      _testExecutionTimer = testExecutionTimer;

      Project project = testProgram.getProject();
      String projectName = project.getName();
      boolean useAlignmentImagesOnDisk = _config.getBooleanValue(SoftwareConfigEnum.USED_SAVED_IMAGES_FOR_AUTOMATIC_ALIGNMENT);

      _isAcquiringImages = true;
      _simulatedIrpPausedLock.setValue(false);

      // Set the acquisition mode.
      _acquisitionMode = ImageAcquisitionModeEnum.ALIGNMENT;

      // Clear any previous abort flags.
      clearForNextRun();

      if(checkForAbortInProgress())
        return;

      // Tell the images manager what images to expect.
      Collection<ReconstructionRegion> reconstructionRegions = new ArrayList<ReconstructionRegion>();
      for (TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
      {
        reconstructionRegions.addAll(getReconstructionRegions(testSubProgram, true));
      }

      if(checkForAbortInProgress())
        return;

      _reconstructedImagesManager.setRequestedRegions(reconstructionRegions);

      for (TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
      {
        // Fire an event that we're starting alignment for a new TestSubProgram.
        if(testSubProgram.isSubProgramPerformAlignment() ||
          testSubProgram.getTestProgram().getProject().getPanel().getPanelSettings().isAllComponentSameUserGain() == true || 
          testSubProgram.getTestProgram().getProject().getPanel().getPanelSettings().isAllComponentSameStageSpeed() == true)
        {
          if (testSubProgram.getAlignmentRegions().isEmpty() == false)
          {
            fireRuntimeAlignmentStartedEvent();
          }
        }

        PanelLocationInSystemEnum panelLocationInSystem = testSubProgram.getPanelLocationInSystem();
        boolean isSecondPortionOfLongPanel = panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT);

        // Get the alignment images.
        for (ReconstructionRegion alignmentRegion : testSubProgram.getAlignmentRegions())
        {
          AlignmentGroup alignmentGroup = alignmentRegion.getAlignmentGroup();
          String savedAlignmentImagePath = FileName.getSavedAlignmentImageFullPath(projectName, alignmentGroup.getName(), isSecondPortionOfLongPanel);
          ReconstructedImages alignmentImages = null;
          if (useAlignmentImagesOnDisk && FileUtilAxi.exists(savedAlignmentImagePath))
          {
            Image image = XrayImageIoUtil.loadPngImage(savedAlignmentImagePath);
            alignmentImages = new ReconstructedImages(alignmentRegion);
            alignmentImages.addImage(image, SliceNameEnum.PAD);
            image.decrementReferenceCount();
          }
          else
          {
            alignmentImages = _imageSetGenerator.generateAlignmentImages(alignmentRegion);
          }
          _reconstructedImagesManager.putReconstructedImages(alignmentImages);

          alignmentImages.decrementReferenceCount();

          if(checkForAbortInProgress())
            return;

          try
          {
            _simulatedIrpPausedLock.waitUntilFalse();
          }
          catch (InterruptedException iex)
          {
            // do nothing ...
          }

          if(checkForAbortInProgress())
            return;
        }
      }

      _lastAcquisitionMode = _acquisitionMode;
    }
    finally
    {
      _isAcquiringImages = false;
    }
  }


  /**
   * Simulates production image acquisition.  This involves generating both fake alignment images
   * and fake inspection images.  These images are returned via the normal channel of the ReconstructedImagesManager.
   *
   * @author Matt Wharton
   */
  void acquireSimulationProductionImages(TestProgram testProgram, TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);

    try
    {
      _testExecutionTimer = testExecutionTimer;

      Project project = testProgram.getProject();
      String projectName = project.getName();
      boolean useAlignmentImagesOnDisk = _config.getBooleanValue(SoftwareConfigEnum.USED_SAVED_IMAGES_FOR_AUTOMATIC_ALIGNMENT);

      boolean isSimulateProductionImageWithoutBoard = false;
      _isAcquiringImages = true;
      _simulatedIrpPausedLock.setValue(false);

      // Set the acquisition mode.
      _acquisitionMode = ImageAcquisitionModeEnum.PRODUCTION;

      /*if(testProgram != null)
      {
          _imagingChainProgramGenerator = new ImagingChainProgramGenerator(
                   _acquisitionMode,
                   ImagingChainProgramGenerator.getDefaultScanStrategy(),
                   0);
          double appropriateMaxSliceHeight = ImagingChainProgramGenerator.getEnableStepSizeOptimization() ?
                                               XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(testProgram.getProject().getPanel().getThicknessInNanometers()) :
                                               XrayTester.getMaximumSliceHeightFromNominalSliceInNanometers();
          _imagingChainProgramGenerator.generateProgramsForLowerImagingChainHardware(
                  testProgram,
                  testExecutionTimer,
                  appropriateMaxSliceHeight,
                  Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers()));
           testProgram.startNewInspectionRun();
            // Clear any previous abort flags.
          clearForNextRun();
          return;
      }*/
      
      //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
      _filteredTestSubProgram.clear();
      
      if (testProgram.isRealignPerformed())
      {
        for(TestSubProgram filteredTestSubProgram : testProgram.getFilteredTestSubPrograms())          
        {
          if (filteredTestSubProgram.isInspectionDone() == false)
            _filteredTestSubProgram.add(filteredTestSubProgram);            
        }
      }
      else
      {
        for(TestSubProgram filteredTestSubProgram : testProgram.getFilteredTestSubPrograms())
          _filteredTestSubProgram.add(filteredTestSubProgram);
      }

      testProgram.setRealignPerformed(false);
      
      // Tell the images manager what images to expect.
      Collection<ReconstructionRegion> reconstructionRegions = new ArrayList<ReconstructionRegion>();
//      for (TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())//getAllTestSubPrograms())
      for(TestSubProgram testSubProgram : _filteredTestSubProgram)
      {
        reconstructionRegions.addAll(getReconstructionRegions(testSubProgram, true));
      }
      _reconstructedImagesManager.setRequestedRegions(reconstructionRegions);

//      for (TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())//.getAllTestSubPrograms())
      for(TestSubProgram testSubProgram : _filteredTestSubProgram)
      {
        // Fire an event that we're starting a new TestSubProgram.
        if(testSubProgram.isSubProgramPerformAlignment() || 
          testSubProgram.getTestProgram().getProject().getPanel().getPanelSettings().isAllComponentSameUserGain() == true ||
          testSubProgram.getTestProgram().getProject().getPanel().getPanelSettings().isAllComponentSameStageSpeed() == true )
        {
          if (testSubProgram.getAlignmentRegions().isEmpty() == false)
          {
            fireRuntimeAlignmentStartedEvent();
          }
        }

        PanelLocationInSystemEnum panelLocationInSystem = testSubProgram.getPanelLocationInSystem();
        boolean isSecondPortionOfLongPanel = panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT);

        _simulatedAlignmentResultReceivedLock.setValue(false);
        
        //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board  
        testSubProgram.setInspectionDone(true);
        
        // Kok Chun, Tan - XCR3051 - set to true when this test sub program done inspection
        testSubProgram.setIsTestSubProgramDone(true);

        // First, let's get the alignment images.
        for (ReconstructionRegion alignmentRegion : testSubProgram.getAlignmentRegions())
        {
          AlignmentGroup alignmentGroup = alignmentRegion.getAlignmentGroup();
          String savedAlignmentImagePath = FileName.getSavedAlignmentImageFullPath(projectName, alignmentGroup.getName(), isSecondPortionOfLongPanel);
          ReconstructedImages alignmentImages = null;
          if (useAlignmentImagesOnDisk && FileUtilAxi.exists(savedAlignmentImagePath))
          {
            Image image = XrayImageIoUtil.loadPngImage(savedAlignmentImagePath);
            alignmentImages = new ReconstructedImages(alignmentRegion);
            alignmentImages.addImage(image, SliceNameEnum.PAD);
            image.decrementReferenceCount();
          }
          else
          {
            alignmentImages = _imageSetGenerator.generateAlignmentImages(alignmentRegion);
          }
          _reconstructedImagesManager.putReconstructedImages(alignmentImages);

          alignmentImages.decrementReferenceCount();

          if(checkForAbortInProgress())
            return;

          try
          {
            _simulatedIrpPausedLock.waitUntilFalse();
          }
          catch (InterruptedException iex)
          {
            // do nothing ...
          }

          if(checkForAbortInProgress())
            return;
        }

        if (testSubProgram.isSubProgramPerformAlignment() && testSubProgram.isSubProgramPerformSurfaceMap() == false)
        {
          if (UnitTest.unitTesting() == false)
          {
            // Wait until we get the alignment transform.
            try
            {
              _simulatedAlignmentResultReceivedLock.waitUntilTrue();
            }
            catch (InterruptedException iex)
            {
              // Do nothing ...
            }
          }
        }

        // Now let's get the inspection images.
        for (ReconstructionRegion inspectionRegion : testSubProgram.getFilteredInspectionRegions())
        {
          try
          {
          ReconstructedImages inspectionImages = _imageSetGenerator.generateInspectionImages(inspectionRegion);
          _reconstructedImagesManager.putReconstructedImages(inspectionImages);

          inspectionImages.decrementReferenceCount();

          if(checkForAbortInProgress())
            return;

            _simulatedIrpPausedLock.waitUntilFalse();
          }
          catch(NoSuchElementException nsee)
          {
            isSimulateProductionImageWithoutBoard = true;
            break;
          }
          catch (InterruptedException iex)
          {
            // do nothing ...
          }

          if(checkForAbortInProgress())
            return;
        }
        if(isSimulateProductionImageWithoutBoard)
        {
          acquireSimulationVerificationImages(testProgram, testExecutionTimer);
        }
      }

      _lastAcquisitionMode = _acquisitionMode;
    }
    finally
    {
      _isAcquiringImages = false;
    }
  }


  /**
   * Load reconstructed images from disk and return them to the business layer via
   * the Observer/Observable design pattern.
   *
   * This call will return when all images have been loaded.  However,
   * images are returned asynchronously on a separate thread.  Therefore,
   * images may still be generated even after this function returns.
   *
   * @param testProgram TestProgram
   * @author Matt Wharton
   * @author Aimee Ong
   */
  public void acquireOfflineProductionImages(TestProgram testProgram, ImageSetData imageSetData) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(imageSetData != null);

    try
    {
      _isAcquiringImages = true;

      // Set the flag indicating that we expect to generate more images
      _reconstructedImagesManager.setAllImagesAreAcquiredFlag(false);

      _acquisitionMode = ImageAcquisitionModeEnum.OFFLINE;

      // tell the manager which regions the client has requested
      _reconstructedImagesManager.setRequestedRegions(testProgram.getFilteredInspectionRegions());

      // Clear the abort flags
      clearForNextRun();

      if(checkForAbortInProgress())
        return;

      // For each TestSubProgram, pull the list of reconstruction regions and load the associated images from disk.
      List<TestSubProgram> testSubPrograms = testProgram.getAllTestSubPrograms();
      MagnificationTypeEnum prevMag = testSubPrograms.get(0).getMagnificationType();
      for (TestSubProgram testSubProgram : testSubPrograms)
      {
        Collection<ReconstructionRegion> reconstructionRegions = testSubProgram.getFilteredInspectionRegions();
        if(reconstructionRegions.isEmpty())
          continue; //nothing to do
        
        //Siew Yeng - XCR-2303 - Different results on same image set when run all joint type and all subtype.
        if(prevMag != testSubProgram.getMagnificationType())
        {
          waitingForPreviousTestSubProgramOfflineInspectionToComplete();
          prevMag = testSubProgram.getMagnificationType();
        }
        
        Set<ReconstructionRegion> recoRegionsWithCompatibleImages = _imageManager.whichReconstructionRegionsHaveCompatibleImages(imageSetData, reconstructionRegions);
        for (ReconstructionRegion recoRegion : reconstructionRegions)
        {
          // If we're aborting, stop trying to load images.
          if(checkForAbortInProgress())
            return;

          // Verify that images exist for this region.
          if (recoRegionsWithCompatibleImages.contains(recoRegion))
          {
            // Load the inspection images
            ReconstructedImages images = null;
            try
            {
              //images = _imageManager.loadOnlyInspectedImages(imageSetData, recoRegion);
              //Lim, Lay Ngor - XCR1711: Assert found when run inspection on 2.5D image set.
              if(imageSetData.isGenerateMultiAngleImagesEnabled())
                images = _imageManager.loadInspectionImages(imageSetData, recoRegion);//LN testing
              else
                images = _imageManager.loadOnlyInspectedImages(imageSetData, recoRegion);
            }
            catch (DatastoreException ex)
            {
              ex.printStackTrace();
              // Fire an event indicating that we don't have an image available for the region.
              LocalizedString imageNotAvailableMessage = new LocalizedString("BUS_NO_COMPATIBLE_IMAGE_AVAILABLE_FOR_REGION_WARNING_KEY",
                  new Object[]
                  {recoRegion.getFullDescription()});
              MessageInspectionEvent messageInspectionEvent = new MessageInspectionEvent(imageNotAvailableMessage);
              _inspectionEventObservable.sendEventInfo(messageInspectionEvent);
            }
            //ReconstructedImages images = ImageManager.loadInspectionImages(imageSetData, recoRegion);
            if(checkForAbortInProgress())
            {
              if (images != null)
                  images.decrementReferenceCount();
              return;
            }
            if (images != null)
            {
              _reconstructedImagesManager.putReconstructedImages(images);
              images.decrementReferenceCount();
            }
          }
          else
          {
            // Fire an event indicating that we don't have an image available for the region.
            LocalizedString imageNotAvailableMessage = new LocalizedString("BUS_NO_COMPATIBLE_IMAGE_AVAILABLE_FOR_REGION_WARNING_KEY",
                new Object[]
                {recoRegion.getFullDescription()});
            MessageInspectionEvent messageInspectionEvent = new MessageInspectionEvent(imageNotAvailableMessage);
            _inspectionEventObservable.sendEventInfo(messageInspectionEvent);
          }
        }
      }

      // Set the flag indicating that we expect no more images.
      _reconstructedImagesManager.setAllImagesAreAcquiredFlag(true);
    }
    finally
    {
      _reconstructedImagesManager.setAllImagesAreAcquiredFlag(true);
      _isAcquiringImages = false;
    }
  }

  /**
   * Loads the specified 'lightest' images from disk (if available).
   *
   * @author Matt Wharton
   */
  ReconstructedImages acquireOfflineLightestImages(ImageSetData imageSetData,
                                                   ReconstructionRegion reconstructionRegion,
                                                   BooleanRef lightestImagesAvailable) throws XrayTesterException
  {
    Assert.expect(imageSetData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(lightestImagesAvailable != null);

    ReconstructedImages lightestImages = null;
    if (_imageManager.haveLightestInspectionImagesForReconstructionRegion(imageSetData, reconstructionRegion))
    {
      lightestImages = _imageManager.loadLightestInspectionImages(imageSetData, reconstructionRegion);
    }

    lightestImagesAvailable.setValue(lightestImages != null);

    return lightestImages;
  }

  /**
   * Loads the specified re-reconstructed images from disk (if available).
   *
   * @author Matt Wharton
   */
  ReconstructedImages acquireOfflineReReconstructedImages(ImageSetData imageSetData,
                                                          ReconstructionRegion reconstructionRegion,
                                                          int sliceOffsetInNanoMeters,
                                                          BooleanRef reReconstructedImagesAvailable) throws XrayTesterException
  {
    Assert.expect(imageSetData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(reReconstructedImagesAvailable != null);

    ReconstructedImages reReconstructedImages = null;
    if (_imageManager.haveFocusConfirmationInspectionImagesForRegion(imageSetData,
                                                                     reconstructionRegion,
                                                                     sliceOffsetInNanoMeters))
    {
      reReconstructedImages = _imageManager.loadFocusConfirmationInspectionImages(imageSetData,
                                                                                  reconstructionRegion,
                                                                                  sliceOffsetInNanoMeters);
    }

    reReconstructedImagesAvailable.setValue(reReconstructedImages != null);

    return reReconstructedImages;
  }

  /**
   * Abort all hardware subsystems involved in image acquisition (cameras, IREs,
   * stage, and tube).  The reason why is attached as a parameter.
   *
   * @author Roy Williams
   */
  public synchronized void abort(XrayTesterException xrayTesterException)
  {
    Assert.expect(xrayTesterException != null);

    if(_isAcquiringImages == false)
    {
      // Not acquiring images yet we are still getting exceptions from IRPs.  Bad.
      // Probably a bug in the irp.  We must stop the complaining (from the IRP)
      // because it will come in over-and-over from a read on the dozen (or so)
      // reads going to the IRP/IRE.   Remember the IRE is the proxy for the IRP
      // and it gets its data through lots of socket connections.
      if (xrayTesterException instanceof ImageReconstructionProcessorCommunicationException)
      {
        ImageReconstructionProcessorCommunicationException irpce = (ImageReconstructionProcessorCommunicationException) xrayTesterException;
        try
        {
          irpce.getImageReconstructionEngine().abort();
        }
        catch (XrayTesterException xte)
        {
          // ingore because we are already dealing with it.
        }
      }
      return;
    }

    _simulatedAlignmentResultReceivedLock.setValue(true);
    _simulatedIrpPausedLock.setValue(false);

    if (hasXrayTesterException() == false)
      super.abort(xrayTesterException);
    else
      evaluateNewXrayTesterException(xrayTesterException);

    // Set flags to force everyone to be reprogrammed after an abort.
    _reprogramCamerasAndStage = true;
    _reprogramIrps            = true;
  }

  /**
   *  @author Roy Williams
   */
  protected void abortImageReconstructionProcessors() throws XrayTesterException
  {
    try
    {
      incrementAbortControlParticipants();

      // ReconstructedImagesManager has some memory that it would like to give up.
      _reconstructedImagesManager.abort();

      // Cancel currently running communications with the IRP and cameras.
      cancelCurrentCommunications();

      // Tell each IRP's to abort in parallel on different ExecutionTasks.
      abortImageReconstructionEngines();
    }
    finally
    {
      decrementAbortParticipants();
    }
  }

  /**
   * @author Roy Williams
   */
  private void cancelCurrentCommunications()
  {
    try
    {
      _concurrentTasks.cancel();
    }
    catch (XrayTesterException ex)
    {
      evaluateNewXrayTesterException(ex);
    }
    catch (Throwable throwable)
    {
      Assert.logException(throwable);
    }
  }

  /**
   * @author Roy Williams
   */
  public boolean areHardwareMessagingCommunicationsCancelled()
  {
    return _concurrentTasks.isCancelled();
  }

  /**
   * @author Roy Williams
   */
  private synchronized void abortImageReconstructionEngines()
  {
    // _imageReconstructionEngineThatCausedAbort has already been dealt with.
    //  Its connections were closed.  Now we need to abort the other/remaining IRPs.
    for (ImageReconstructionEngine ire : _imageReconstructionEngines)
    {
      if (ire.areCommunicationPathsToImageReconstructionProcessorEstablished())
      {
        _concurrentAbortTasks.submitThreadTask(new AbortImageReconstructionEngineThreadTask(ire));
      }
    }

    // Wait for all IRP's to acknowledge they have begun the abort process.
    try
    {
      waitForAbortTasksToComplete();
    }
    catch (XrayTesterException ex)
    {
      evaluateNewXrayTesterException(ex);
    }
    catch (Throwable throwable)
    {
      Assert.logException(throwable);
    }

    // Wait for each IRE (proxy for communications to the IRP) to acknowledge the
    // communications networks has been cleansed.
    waitForAllIreFreed();
  }

  /**
   * @author Yong Sheng Chuan
   */
  private void waitForAllIreFreed()
  {
    // Wait for each IRE (proxy for communications to the IRP) to acknowledge the
    // communications networks has been cleansed.
    boolean networkTrafficIdle = false;
    while (networkTrafficIdle == false)
    {
      for (ImageReconstructionEngine ire : _imageReconstructionEngines)
      {
        networkTrafficIdle = ire.isImageReceiverBusy() ? false : true;
//        System.out.println("networkTrafficIdle == " + networkTrafficIdle);
        if (networkTrafficIdle == false)
        {
          try
          {
            wait(200);
          }
          catch (InterruptedException ie)
          {
            // do nothing.
          }
          break;
        }
      }
    }
  }

  /**
   * Ordering within this function is very important.  First part aborts the
   * panelPostioner and cameras.  Both are universal to ProjectionProducers.
   * The second part is exclusive to this object.
   *
   * @author Roy Williams
   */
  public void userAbort() throws XrayTesterException
  {
    // Kok Chun, Tan - XCR3088 - place the synchronized inside the method instead of at the method declaration to avoid synchronization problem.
     synchronized (_abortObj)
    {
      _reconstructedImagesManager.abort();

      // no need to abort if nothing is happening!
      if (_isAcquiringImages == false)
      {
        return;
      }

      if (_acquisitionMode.equals(ImageAcquisitionModeEnum.OFFLINE))
      {
        // just set this flag, no need to abort hardware since it is not in use.
        _userAbortedOfflineImageAcquisition = true;
        return;
      }

      _simulatedAlignmentResultReceivedLock.setValue(true);
      _simulatedIrpPausedLock.setValue(false);

      checkForAbortInProgress();

      super.userAbort();

      checkForAbortInProgress();
    }
  }

  /**
   * Gets the image acquisition done for each and every TestSubProgram.
   *
   * @author Roy Williams
   */
  private void runAcquisitionSequenceForEachTestSubProgram(TestExecutionTimer testExecutionTimer,
                                                           List<? extends ProjectionConsumer> projectionConsumers, ImageAcquisitionModeEnum acquisitionMode) throws XrayTesterException
  {
    Assert.expect(testExecutionTimer != null);

    Assert.expect(_testProgram != null);
    Assert.expect(_panelPositioner != null);
    Assert.expect(_xRaySource != null);
    Assert.expect(projectionConsumers != null);
    Assert.expect(projectionConsumers.size() > 0);
    
    TimerUtil timer = new TimerUtil(); 
    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_ACQUISITION_SEQUENCE_FOR_ALL_SUB_PROGRAMS);

    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.TURN_OFF_TRIGGERS);
    
	// Swee Yee Wong - remove temporarily fix for slow stage, it is fixed in motion control
    // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
    CameraTrigger.getInstance().setNumberOfRequiredSampleTriggers();
    
    timer.reset();
    timer.start();
    turnOffSyntheticTriggersThenWaitForCamerasToBecomeIdle();
    timer.stop();
    debug("turnOffSyntheticTriggersThenWaitForCamerasToBecomeIdle = " + timer.getElapsedTimeInMillis());

    if(checkForAbortInProgress())
      return;
    
    timer.reset();
    timer.start();    
    // Do these operations once for the entire testProgram.
    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.DOWNLOAD_TEST_PROGRAMS);
    downloadTestProgramToHardwareIfNeeded(testExecutionTimer);
    
    timer.stop();
    debug("downloadTestProgramToHardwareIfNeeded = " + timer.getElapsedTimeInMillis());
    timer.reset();
    timer.start();
    
    parallelTask1ForProductionInspection(testExecutionTimer, acquisitionMode);
      
    timer.stop();
    debug("parallelTask1ForProductionInspection = " + timer.getElapsedTimeInMillis());
    
    timer.reset();
    timer.start();
    // Do these operations for each testSubProgram.
    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.RUN_ACQUISITION_SEQUENCES);
    _executingFirstTestSubProgram = true;
    _executingLastTestSubProgram = false;
    int expectedNumberOfScanPasses = 0;
    _testProgram.setFilteredTestSubProgramInScanPath(_acquisitionMode);
    _filteredTestSubProgram.clear();

    //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
    for(TestSubProgram filteredTestSubProgram : _testProgram.getFilteredScanPathTestSubPrograms())
    {
      _filteredTestSubProgram.add(filteredTestSubProgram);
    }
    _testProgram.setRerunningProduction(false);
    _testProgram.setRealignPerformed(false);

    timer.stop();      
    debug("getFilteredScanPathTestSubPrograms = " + timer.getElapsedTimeInMillis());
    
    MagnificationTypeEnum currentMagnificationType = null;
    for(TestSubProgram testSubProgram : _filteredTestSubProgram)
    {
      timer.reset();
      timer.start();
      
      _executingLastTestSubProgram = checkIsLastTestSubProgram();

      _testSubProgram = testSubProgram;
//      _runtimeAlignmentMatrixSentFromStack = null;

      // Fire an event that we're starting an alignment for the next TestSubProgram.
      // Be sure we are not in a user abort or exception mode first.
      if (checkForAbortInProgress())
        return;
      
      if(currentMagnificationType == null ||
         currentMagnificationType.equals(testSubProgram.getMagnificationType()) == false)
        _executingDifferentMagnificationTestSubProgram = true;
      
      // Added by Lee Herng 11 Sept 2014 - Reset SurfaceModel flag
	  // so that ZHeightEstimator able to add points to next TestSubProgram
      enableSurfaceModel();

      currentMagnificationType = testSubProgram.getMagnificationType();
      
      if (_acquisitionMode.equals(ImageAcquisitionModeEnum.ALIGNMENT) ||
          _acquisitionMode.equals(ImageAcquisitionModeEnum.IMAGE_SET) ||
          _acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION))
      {
        if (testSubProgram.isSubProgramPerformAlignment() || 
          testSubProgram.getTestProgram().getProject().getPanel().getPanelSettings().isAllComponentSameUserGain() == true ||
          testSubProgram.getTestProgram().getProject().getPanel().getPanelSettings().isAllComponentSameStageSpeed() == true)
        {
          if (testSubProgram.getAlignmentRegions().isEmpty() == false)
          {
            fireRuntimeAlignmentStartedEvent();
            _alignmentLock.setValue(false);
          }
        }
      }
      
      timer.stop();      
      debug("testSubProgram Init to fireRuntimeAlignmentStartedEvent = " + timer.getElapsedTimeInMillis());
      timer.reset();
      timer.start();

 
      // initialize camera, stage, and IR hardware
      prepareHardwareForNextTestSubProgram(testSubProgram, testExecutionTimer);
      
      timer.stop();      
      debug("prepareHardwareForNextTestSubProgram = " + timer.getElapsedTimeInMillis());
      
      // Run the acquisition sequence for this specific testSubProgram.
      List<? extends ScanPass> scanPasses = testSubProgram.getScanPasses();
      
      //remove scan pass of existing testsubprogram so that it only left next testsubprogram scan pass
      if(_totalScanPath.isEmpty() == false)
        _totalScanPath.removeAll(scanPasses);
      
      expectedNumberOfScanPasses += scanPasses.size();

      // Fire an event that we're starting an alignment for the next TestSubProgram.
      // Be sure we are not in a user abort or exception mode first.
      if (checkForAbortInProgress())
        return;
      
      try
      {
        // all right, let' s start monitoring the progress
        _progressMonitor.startMonitoring();
        runAcquisitionSequence(testExecutionTimer,
                               projectionConsumers,
                               scanPasses,
                               expectedNumberOfScanPasses,
                               _executingFirstTestSubProgram,
                               _acquisitionMode,
                               _executingLastTestSubProgram);
        _executingFirstTestSubProgram = false;
        _executingDifferentMagnificationTestSubProgram = false;
        //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
        testSubProgram.setInspectionDone(true);
        if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION))
        {
          // Kok Chun, Tan - XCR3051 - set to true when this test sub program done inspection, reset the flag when is last test sub program
          if (_executingLastTestSubProgram)
            _testProgram.resetIsTestSubProgramDone();
          else
            testSubProgram.setIsTestSubProgramDone(true);
        }
      }
      finally
      {
        // Clear TestSubProgram scan pass list
        if (scanPasses != null)
        {
            scanPasses.clear();
            scanPasses = null;
        }
        // let's stop monitoring even if an exception is thrown.
        _progressMonitor.stopMonitoring();
      }
    }

    timer.reset();
    timer.start();
    //For Speed Up
    if (InnerBarrier.isInstalled())
    {
      //Swee Yee Wong - XCR-3321 Sanmina board inspection time slower.     
      //Kok Chun, Tan - XCR-3415 - When not in production mode, use InnerBarrier.getInstance().close(). 
      //This function will wait until the inner barrier is fully closed.
      //This is to avoid base image quality get black image intermittent.
      //But for production, we need to speed up, direct close the inner barrier and no need wait. (Sanmina slow issues) 
      //Without waiting the innerbarrier task can increase cycle time by around 1 second
      if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION))
        DigitalIo.getInstance().closeInnerBarrier();
      //else
      //  InnerBarrier.getInstance().close();
    }
    timer.stop();      
    debug("closeInnerBarrier = " + timer.getElapsedTimeInMillis());
    if (isAbortInProgress() == false)
    {
      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.TURN_ON_TRIGGERS);
       timer.reset();
      timer.start();
      waitForCamerasToBecomeIdleThenTurnOnSyntheticTriggers();
      timer.stop();      
      debug("waitForCamerasToBecomeIdleThenTurnOnSyntheticTriggers = " + timer.getElapsedTimeInMillis());
    }
    // Swee Yee Wong - remove temporarily fix for slow stage, it is fixed in motion control
    // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23

    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_ACQUISITION_SEQUENCE_FOR_ALL_SUB_PROGRAMS);
  }

  /**
   * ProjectionConsumer for this class is ImageReconstructionEngine.  An ImageReconstructionEngine
   * is the proxy for an ImageReconstructionProcessor (otherwise known as an "IRP" in hardware
   * terms).
   * <p>
   * There are (or were at time of this writing) 4 IRPs on a system.   And we needed
   * a way to keep them all in lockstep as the stage moves.
   * <p>
   * An IRE/IRP offers the ability to download TestPrograms (has its own model) and
   * we execute the TestProgram.   Each TestProgram has multiple TestSubPrograms.
   * Each TestSubProgram has one or more ScanPath that will be summed up to the
   * TestProgram when it is time to download (to IRP).
   * <p>
   * Send an event to begin unloading the panel.
   *
   * @author Roy Williams
   */
  public boolean waitForScanPassToBeAcknowledged(int expectedNumberOfScanPasses) throws XrayTesterException
  {
    boolean allScanPassesDelivered = false;  // the logic is eazier with this sense.
    while (allScanPassesDelivered == false && isAbortInProgress() == false)
    {
      allScanPassesDelivered = true;  // the logic is eazier with this sense.
      for (ImageReconstructionEngine ire : _imageReconstructionEngines)
      {
        if (allScanPassesDelivered)
        {
          if (ire.hasReconstructionRegions() &&
              _imagingChainProgramGenerator.getParticipatingImageReconstructionEngines().contains(ire))
          {
            boolean irpSatisfied = false;
            if (ire.hasReceivedScanPasses())
            {
              int lastReceivedScanPassNumber = ire.getLastReceivedScanPassNumber();
              irpSatisfied = (lastReceivedScanPassNumber >= expectedNumberOfScanPasses - 1);
            }
            else if (expectedNumberOfScanPasses > 0)
              irpSatisfied = false;
            else
              irpSatisfied = true;

            allScanPassesDelivered = irpSatisfied && allScanPassesDelivered;
          }
        }
        if(checkForAbortInProgress())
          return false;
      }

      if (allScanPassesDelivered == false)
      {
        try
        {
          Thread.sleep(200);

          // Now that we woke back up, lets just be sure we don't have an abort
          // to deal with.  One may have come in when we were sleeping.
          if(checkForAbortInProgress())
            return false;
        }
        catch (InterruptedException ex)
        {
          // do nothing.
        }
      }
    }
    return allScanPassesDelivered;
  }

  /**
   * @author Roy Williams
   */
  protected void waitForLastScanPassToBeAcknowledged(int expectedNumberOfScanPasses) throws XrayTesterException
  {
    boolean recievedExpectedNumberOfScanPasses = waitForScanPassToBeAcknowledged(expectedNumberOfScanPasses);

    if (recievedExpectedNumberOfScanPasses)
    {
      if(checkForAbortInProgress())
        return;

      // Only on the last testSubProgram... we will ask for the Xrays to be turned
      // off and send an event to allow unloading of the board while we finish.
      if (_executingLastTestSubProgram)
      {
        //turnXraysOff(); //will be called parallelly for speed up during prepareToUnloadPanel in TestExecution
        sendAcquiringImagesCompleteInspectionEvent(true);
      }
    }
  }

  /**
   * @author Roy Williams
   */
  protected boolean isCalibrationRun()
  {
    return _acquisitionMode.equals(ImageAcquisitionModeEnum.COUPON);
  }

  /**
   * @author Roy Williams
   */
  private void sendAcquiringImagesCompleteInspectionEvent(boolean normalExit)
  {
    boolean isCalibrationRun = _acquisitionMode.equals(ImageAcquisitionModeEnum.COUPON);
    if (isCalibrationRun == false)
    {
      AcquiringImagesCompleteInspectionEvent acquiringImagesCompleteInspectionEvent = new AcquiringImagesCompleteInspectionEvent(normalExit);
      _inspectionEventObservable.sendEventInfo(acquiringImagesCompleteInspectionEvent);
      _acquiringImagesCompleteInspectionEventSent = true;
    }
  }

  /**
   * Save a cached alignmentMatrix.  This is used during testProgram initialize
   * to EITHER send the AlignmentRegions OR this set of cached AffineTransforms.
   *
   * @author Roy Williams
   */
  public void applyAlignmentResult(final AffineTransform affineTransform,
                                   final int testSubProgramId) throws XrayTesterException
  {
    Assert.expect(affineTransform != null);
    Assert.expect(testSubProgramId >= 0);

    // if we have already seen an abort then do not proceed.
    if(checkForAbortInProgress())
      return;

    // If we're in sim-mode, don't send anything.
    if (XrayTester.getInstance().isSimulationModeOn() == false ||
        ImageAcquisitionEngine.getInstance().isOfflineReconstructionEnabled())
    {
      sendAlignmentResult(affineTransform, testSubProgramId);
    }
    else
    {
      // For sim-mode, release the "alignment result received" lock.
      _simulatedAlignmentResultReceivedLock.setValue(true);
    }

    _alignmentLock.setValue(true);
    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_RECONSTRUCT);

    _testExecutionTimer.stopAlignmentTimer();
  }

  /**
   * @author Roy Williams
   */
  private void positionPanelInCorrectLocationForTestSubProgram(TestSubProgram testSubProgram) throws  XrayTesterException
  {
    Assert.expect(testSubProgram != null);

    // This is to handel Virtual Live Mode
    if (ImageReconstructionEngine.isDeepSimulation())
      return;

    // During unit testing, we are not loading panel into the system.  Since
    // PanelHandler excpects a panel in system before we can invoke the move
    // to left or right command, it will assert.  So, for unit testing, disable
    // PanelHandler commands.  Or, add the panel load to be part of our test.
    if (UnitTest.unitTesting())
      return;

    // We can be getting reconstructed images from one of the two coupons.  If so,
    // there is not a panel in the system.
    if (_acquisitionMode.equals(ImageAcquisitionModeEnum.COUPON))
      return;

    if(checkForAbortInProgress())
      return;

    if (_executingFirstTestSubProgram == false)
    {
      _panelPositioner.pauseScanPath();
    }

    if (testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
    {
      _panelHandler.movePanelToRightSide();

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.PANEL_IN_RIGHT_POSITION);
    }
    else if (testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
    {
      _panelHandler.movePanelToLeftSide();

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.PANEL_IN_LEFT_POSITION);
    }
    else
    {
      Assert.expect(false, "Invalid PanelLocationInSystemEnum value");
    }
  }

  /**
   * @author Roy Williams
   * @author Reid Hayhow
   */
  private void downloadTestProgramToHardwareIfNeeded(TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(testExecutionTimer != null);

    XrayTesterException exception = null;

    if (checkForAbortInProgress())
      return;

    testExecutionTimer.startIrpAndCameraInitializeTimer();

    // Try block to allow for cleanup and event bracketing
    try
    {
      // prepare image reconstruction engines
      initializeImageReconstructionEngines();
      
      //add entire scan pass to a list before start
      _totalScanPath.clear();
      _totalScanPath.addAll(_testProgram.getScanPasses());

      if (checkForAbortInProgress())
        return;

      if (_reprogramCamerasAndStage)
      {
        prepareCamerasForAcquisition();

        if (checkForAbortInProgress())
          return;

        _reprogramCamerasAndStage = false;
      }

      _reprogramIrps = false;
    }
    // Catch and save any exception from initializing the hardware
    catch (XrayTesterException firstException)
    {
      exception = firstException;
    }
    // No matter what, we need to wait for tasks to complete
    finally
    {
      // Inner try block because waitForTasksToComplete can throw
      try
      {
        // Both prepareCamerasForAcquisition and initializeImageReconstructionEngines
        // have been doing _concurrentTasks.submitThreadTask(task).   It is time to
        // wait for all this work to complete.
        waitForTasksToComplete();

        // x-ray cameras are configured and the flexi-buffer value is calculated and
        // ready to use
        int useCameraBuffers = _xrayCameraArray.getProjectionCaptureBufferDepth();
        if (XrayTester.getInstance().isSimulationModeOn() == true)
            useCameraBuffers = Integer.MAX_VALUE;
        ImageReconstructionEngine.setCameraBuffersAvailableForTestProgram(useCameraBuffers);

      }
      // Catch any exception thrown from the parallel thread tasks
      catch (XrayTesterException waitForTasksException)
      {
        // Only preserve a parallel thread task exception if it is the first exception
        // we have encountered
        if (exception == null)
        {
          exception = waitForTasksException;
        }
      }
      // Throw any exception we have preserved
      if (exception != null)
      {
        throw exception;
      }
    }

    testExecutionTimer.stopIrpAndCameraInitializeTimer();
  }

  /**
   * @author Roy Williams
   */
  private void prepareHardwareForNextTestSubProgram(TestSubProgram testSubProgram, TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(testSubProgram.getUserGain() != null);

    if(checkForAbortInProgress())
      return;
    
    TimerUtil timer = new TimerUtil();
    timer.reset();
    timer.start();    
    // set the panel into the right position
    positionPanelInCorrectLocationForTestSubProgram(testSubProgram);    
    timer.stop();
    debug("positionPanelInCorrectLocationForTestSubProgram = " + timer.getElapsedTimeInMillis());
    
    timer.reset();
    timer.start();
    if(testSubProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
    {
      moveXrayCylinderUp();
    }
    else if(testSubProgram.getMagnificationType().equals(MagnificationTypeEnum.HIGH))
    {
      moveXrayCylinderDown();
    }
    timer.stop();
    debug("move xray cylinder = " + timer.getElapsedTimeInMillis());
    if(checkForAbortInProgress())
      return;
    
    
    if(_xRaySource.areXraysOn() == false)
    {
      System.out.println("WARNING: X-rays source is off before acquire images for alignment or inspection.");
      turnXraysOn(testExecutionTimer);
    }
    
    if (InnerBarrier.isInstalled() && DigitalIo.getInstance().isInnerBarrierClosed())
    {
      System.out.println("WARNING: Inner barrier is closed before acquire images for alignment or inspection.");
      InnerBarrier.getInstance().open();
    }
    
    if(checkForAbortInProgress())
      return;
    
    timer.reset();
    timer.start();
     // Get the ball rolling by starting up the ImageReconstructionEngines.
    startReconstruction(testSubProgram, testExecutionTimer);
    timer.stop();
    debug("startReconstruction = " + timer.getElapsedTimeInMillis());
  }

  /**
   * @author Roy Williams
   */
  private void initializeReconstructedImagesManager(TestProgram testProgram)
      throws XrayTesterException
  {
    Assert.expect(_acquisitionMode != null);

    // clear out the old stuff.
    _reconstructedImagesManager.initialize();

    // Load In the new stuff.
    Collection<ReconstructionRegion> reconstructionRegions = new ArrayList<ReconstructionRegion>();
    testProgram.setFilteredTestSubProgramInScanPath(_acquisitionMode);
    for (TestSubProgram testSubProgram : testProgram.getFilteredScanPathTestSubPrograms())
    {
      reconstructionRegions.addAll(getReconstructionRegions(testSubProgram, true));
    }
    _reconstructedImagesManager.setRequestedRegions(reconstructionRegions);
  }

  /**
   * @author Roy Williams
   */
  private void initializeReconstructionRegionsToMarkComplete(TestSubProgram testSubProgram)
      throws XrayTesterException
  {
    Assert.expect(_acquisitionMode != null);

    // clear out the old stuff.
    _reconstructionRegionsMarkedCompleteMap.clear();
    _progressMonitor.newWorkStarted();

    // Load in the new stuff.  Note: below we will get the unfiltered list of reconstruction
    // regions.
    Collection<ReconstructionRegion> reconstructionRegions = getReconstructionRegions(testSubProgram, false);
    _reconstructionRegionsToMarkComplete = reconstructionRegions.size();
    Assert.expect(_reconstructionRegionsToMarkComplete > 0);
  }

  /**
   *  @author Roy Williams
   */
  private Collection<ReconstructionRegion> getReconstructionRegions(TestSubProgram testSubProgram,
                                                                    boolean filtered)
  {
    Collection<ReconstructionRegion>  reconstructionRegions = null;
    if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION) ||
        _acquisitionMode.equals(ImageAcquisitionModeEnum.IMAGE_SET))
    {
      if (filtered)
        reconstructionRegions = testSubProgram.getFilteredInspectionRegions();
      else
        reconstructionRegions = testSubProgram.getAllScanPathReconstructionRegions();

      Collection<ReconstructionRegion> alignmentList = testSubProgram.getAlignmentRegions();
      reconstructionRegions.addAll(alignmentList);
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.ALIGNMENT))
    {
      reconstructionRegions = testSubProgram.getAlignmentRegions();
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.VERIFICATION))
    {
      reconstructionRegions = testSubProgram.getVerificationRegions();
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.COUPON))
    {
      reconstructionRegions = testSubProgram.getVerificationRegions();
    }
    else
      Assert.expect(false);
    return reconstructionRegions;
  }

  /**
   * Start reconstruction on each image reconstruction engine.
   *
   * @author Roy Williams
   */
  private void startReconstruction(TestSubProgram testSubProgram, TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(testExecutionTimer != null);

    if(checkForAbortInProgress())
      return;

    // Always reinitialize the set of ReconstructionRegions that have been marked complete.
    initializeReconstructionRegionsToMarkComplete(testSubProgram);

    if(checkForAbortInProgress())
      return;

    // Start reconstruction on seperate threads
     for (final ImageReconstructionEngine ire : _imagingChainProgramGenerator.getParticipatingImageReconstructionEngines())
    {
      _concurrentTasks.submitThreadTask(new StartReconstructionThreadTask(ire,
                                                                          testSubProgram,
                                                                          _executingFirstTestSubProgram,
                                                                          _executingDifferentMagnificationTestSubProgram,
                                                                          testExecutionTimer));
    }
    waitForTasksToComplete();
  }

  /**
   * @author Roy Williams
   */
  protected List<? extends ProjectionConsumer> getProjectionConsumers()
  {
    List<? extends ProjectionConsumer> superProjectionConsumers = super.getProjectionConsumers();

    List<ImageReconstructionEngine> projectionConsumers = new ArrayList<ImageReconstructionEngine>();
    for (ProjectionConsumer projectionConsumer : superProjectionConsumers)
    {
      ImageReconstructionEngine ire = (ImageReconstructionEngine) projectionConsumer;
      if (_imagingChainProgramGenerator.getParticipatingImageReconstructionEngines().contains(ire) &&
          ire.hasReconstructionRegions())
        projectionConsumers.add(ire);
    }
    return projectionConsumers;
  }

  /**
   * Send the alignment result to all image reconstruction engines.
   *
   * @param affineTransform alignment transform
   * @param testSubProgramId that identifies the regions this alignment applies too
   *
   * @author Roy Williams
   */
  private void sendAlignmentResult(final AffineTransform affineTransform,
                                   final int testSubProgramId) throws XrayTesterException
  {
    Assert.expect(affineTransform != null);
    Assert.expect(testSubProgramId >= 0);
//    if (_runtimeAlignmentMatrixSentFromStack != null)
//    {
//      System.out.println();
//      System.out.println("Previous runtime alignment matrix sent from stack:");
//      _runtimeAlignmentMatrixSentFromStack.printStackTrace();
//
//      System.out.println();
//      System.out.println("Second runtime alignment matrix sent from stack:");
//      try
//      {
//        throw new Exception();
//      }
//      catch (Exception ex)
//      {
//        ex.printStackTrace();
//        Assert.expect(false);
//      }
//    }
//
    // Send alignment data on seperate threads
    for (final ImageReconstructionEngine ire : _imagingChainProgramGenerator.getParticipatingImageReconstructionEngines())
    {
      _concurrentTasks.submitThreadTask(new SetAlignmentThreadTask(ire,
                                                                   affineTransform,
                                                                   testSubProgramId));
    }
    waitForTasksToComplete();
//    try
//    {
//      throw new Exception();
//    }
//    catch (Exception ex)
//    {
//      _runtimeAlignmentMatrixSentFromStack = ex;
//    }
  }

  /**
   * Send the calibrated magnifications to all image reconstruction engines.
   *
   * @author Roy Williams
   */
  public void sendCalibratedMagnifications(double mag, double highMag) throws XrayTesterException
  {
    Assert.expect(mag > 0);
    Assert.expect(highMag > 0);

    // Send alignment data on seperate threads
    for (final ImageReconstructionEngine ire : _imagingChainProgramGenerator.getParticipatingImageReconstructionEngines())
    {
      _concurrentTasks.submitThreadTask(new SetCalibratedMagnificationThreadTask(ire, mag, highMag));
    }
    waitForTasksToComplete();
  }

  /**
   * Re-acquire a specified image(s).  Return reconstructed images through an
   * Observer/Observable design pattern.  The ReconstructionRegion must
   * match (region id & geometry) an existing region in the test program.
   *
   * @param reconstructionRegion
   * @author Roy Williams
   */
  public void reacquireImages(int testSubProgramId,
                              ReconstructionRegion reconstructionRegion,
                              int reconstructionZOffsetInNanometers) throws XrayTesterException
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(testSubProgramId >= 0);

    // if we have already seen an abort then do not proceed.
    if(checkForAbortInProgress())
      return;

    // Tell the progressMonitor that things may take a bit longer than expected.
    _progressMonitor.reacquireImageRequest();

    // Add the reconstruction region to the re-reconstructed images request queue.
    _reconstructedImagesManager.addReReconstructedImagesRequest(reconstructionRegion);

    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(reconstructionRegion.getReconstructionEngineId());

    ire.reacquireImage(testSubProgramId, reconstructionRegion, reconstructionZOffsetInNanometers);
  }

  /**
   * Simulates the re-reconstruction of images for the specified region.
   *
   * @author Matt Wharton
   */
  public void reacquireSimulationImages(ReconstructionRegion reconstructionRegion,
                                        int reconstructionZOffsetInNanometers) throws XrayTesterException
  {
    Assert.expect(reconstructionRegion != null);

    // if we have already seen an abort then do not proceed.
    if(checkForAbortInProgress())
      return;

    // Add the reconstruction region to the re-reconstructed images request queue.
    _reconstructedImagesManager.addReReconstructedImagesRequest(reconstructionRegion);

    // Generate some fake images.
    ReconstructedImages reReconstructedImages = _imageSetGenerator.generateInspectionImages(reconstructionRegion);

    // Put the images in the queue.
    _reconstructedImagesManager.putReconstructedImages(reReconstructedImages);
    reReconstructedImages.decrementReferenceCount();
  }

  /**
   * Re-acquire a specified image(s).  Return reconstructed images through an
   * Observer/Observable design pattern.  The ReconstructionRegion must
   * match (region id & geometry) an existing region in the test program.
   *
   * @param reconstructionRegion
   * @author Roy Williams
   */
  public void reacquireLightestImages(int testSubProgramId,
                                      ReconstructionRegion reconstructionRegion,
                                      int reconstructionZOffsetInNanometers) throws XrayTesterException
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(_reconstructedImagesManager != null);

    // if we have already seen an abort then do not proceed.
    if (checkForAbortInProgress())
      return;

    // Tell the progressMonitor that things may take a bit longer than expected.
    _progressMonitor.reacquireImageRequest();

    // Add the reconstruction region to the "lightest images request queue".
    _reconstructedImagesManager.addLightestImagesRequest(reconstructionRegion);

    _reconstructedImagesManager.setHighPriorityForImagesFromReconstructionRegion(reconstructionRegion);

    ImageReconstructionEngine ire =
        ImageReconstructionEngine.getInstance(reconstructionRegion.getReconstructionEngineId());

    ire.reacquireLightestImage(testSubProgramId, reconstructionRegion, reconstructionZOffsetInNanometers);
  }

  /**
   * Simulates the re-reconstruction of 'lightest' images for the specified region.
   *
   * @author Matt Wharton
   */
  public void reacquireSimulationLightestImages(ReconstructionRegion reconstructionRegion) throws XrayTesterException
  {
    Assert.expect(reconstructionRegion != null);

    // if we have already seen an abort then do not proceed.
    if(checkForAbortInProgress())
      return;

    // Add the reconstruction region to the "lightest images request queue".
    _reconstructedImagesManager.addLightestImagesRequest(reconstructionRegion);

    // Generate some fake images.
    ReconstructedImages lightestImages = _imageSetGenerator.generateInspectionImages(reconstructionRegion);

    // Put the images in the queue.
    _reconstructedImagesManager.putReconstructedImages(lightestImages);
    lightestImages.decrementReferenceCount();
  }

  /**
   * Tell the IRP to change the zHeight of the specified focusInstruction of the slice of the reconstructionRegion
   * @author Scott Richardson
   */
  public void modifyFocusInstruction(int testSubProgramId,
                                     ReconstructionRegion reconstructionRegion,
                                     int sliceId,
                                     int zHeightFocusInNanoMeters,
                                     int percent,
                                     int zOffset,
                                     byte focusMethodId
      ) throws XrayTesterException
  {
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceId >= 0);

    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(reconstructionRegion.getReconstructionEngineId());

    ire.modifyFocusInstruction(testSubProgramId, reconstructionRegion.getRegionId(), sliceId, zHeightFocusInNanoMeters, percent, zOffset, focusMethodId);
  }

  /**
   * Tell the IRP to change the FocusProfileShape on the focusGroup of a reconstructionRegion
   * @author Cheah Lee Herng
   * @author Roy Williams
   */
  public void modifyFocusProfileShape(int testSubProgramId,
                                      ReconstructionRegion reconstructionRegion,
                                      int focusGroupId,
                                      byte focusProfileShape,
                                      int zGuessFromPredictiveSliceHeightInNanometers,
                                      boolean usePhaseShiftProfiliometry,
                                      int searchRangeLowLimitInNanometers,
                                      int searchRangeHighLimitInNanometers,
                                      int pspZOffsetInNanometers) throws XrayTesterException
  {
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(focusGroupId >= 0);

    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(reconstructionRegion.getReconstructionEngineId());

    ire.modifyFocusProfileShape(testSubProgramId, 
                                reconstructionRegion.getRegionId(), 
                                focusGroupId, 
                                focusProfileShape, 
                                zGuessFromPredictiveSliceHeightInNanometers, 
                                usePhaseShiftProfiliometry,
                                searchRangeLowLimitInNanometers,
                                searchRangeHighLimitInNanometers,
                                pspZOffsetInNanometers);
  }
  
  /**
   * Tell the IRP to kick start a specific region
   * @author Chnee Khang Wah, 2013-06-17, New PSH Handling
   */
  public void kickStartSpecificRegion(int testSubProgramId,
                                          ReconstructionRegion reconstructionRegion) throws XrayTesterException
  {
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(reconstructionRegion != null);

    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(reconstructionRegion.getReconstructionEngineId());
    
    ire.kickStartSpecificRegion(testSubProgramId, reconstructionRegion.getRegionId());
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void enableSurfaceModeling() throws XrayTesterException
  {
    for (final ImageReconstructionEngine ire : _imagingChainProgramGenerator.getParticipatingImageReconstructionEngines())
    {
      ire.enableSurfaceModeling();
    }
  }

  /**
   * Tell the IRP to change the FocusProfileShape on the focusGroup of a reconstructionRegion
   * @author Roy Williams
   */
  public void disableSurfaceModeling() throws XrayTesterException
  {
    for (final ImageReconstructionEngine ire : _imagingChainProgramGenerator.getParticipatingImageReconstructionEngines())
    {
      ire.disableSurfaceModeling();
    }
  }

  /**
   * Tell the IRP to change the FocusProfileShape on the focusGroup of a reconstructionRegion
   * @author Roy Williams
   */
  public void waitForPriorIrpCommandsToComplete() throws XrayTesterException
  {
    for (final ImageReconstructionEngine ire : _imagingChainProgramGenerator.getParticipatingImageReconstructionEngines())
    {
      ire.waitForPriorIrpCommandsToComplete();
    }
  }

  /**
   * Indicate that higher level modules are finished with a
   * specified region so resources can be freed.
   *
   * The ReconstructionRegion must match (region id) an existing region
   * in the test program.
   *
   * @param reconstructionRegion region that has been fully processed
   * @param hasTestFailure test status
   * @author Roy Williams
   */
  public void reconstructionRegionComplete(final TestSubProgram testSubProgram,
                                           final ReconstructionRegion reconstructionRegion,
                                           final boolean hasTestFailure) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(reconstructionRegion != null);

    // if we have already seen an abort then do not proceed.
    if(checkForAbortInProgress())
      return;

    // If we're in sim-mode, we can just return here.
    if (XrayTester.getInstance().isSimulationModeOn() &&
        ImageAcquisitionEngine.getInstance().isOfflineReconstructionEnabled() == false)
    {
      return;
    }

    // Tell the reconstruction processor that resources associated with
    // this region can be freed and focus results associated with this
    // region can be utilized.
    int testSubProgramId = testSubProgram.getId();
    int reconstructionRegionId = reconstructionRegion.getRegionId();

    if (_debug)
      Assert.expect(_reconstructedImagesManager.isReconstructionRegionActive(reconstructionRegionId) == false);

    ImageReconstructionEngineEnum ireId = reconstructionRegion.getReconstructionEngineId();
    final ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ireId);
    Assert.expect(_imagingChainProgramGenerator.getParticipatingImageReconstructionEngines().contains(ire));
    Assert.expect(ire.hasReconstructionRegion(reconstructionRegionId));

    _progressMonitor.toMarkRegionComplete();

    _concurrentTasks.submitThreadTask(new MarkReconstructionRegionCompleteThreadTask(
      ire,
      testSubProgramId,
      reconstructionRegionId,
      hasTestFailure));

    waitForTasksToComplete();

    // return if any abort is in progress.
    if (checkForAbortInProgress())
    {
      return;
    }

    // Put the name of the completed reconsctruction region into the map.
    _reconstructionRegionsMarkedCompleteMap.put(reconstructionRegion.getRegionId(), reconstructionRegion);
    _progressMonitor.finishedTestingImage(_reconstructionRegionsMarkedCompleteMap.size());

    debug("IRP: " + ire.getId() + "  ReconstructionRegion marked complete: " +
                         reconstructionRegion.getName());
  }

  /**
   * Pause image acquisition.
   *
   * @author Roy Williams
   */
  public void pauseImageAcquisition() throws XrayTesterException
  {
    Assert.expect(_acquisitionMode != null);

    if (isAbortInProgress())
    {
      return;
    }

    if (_acquisitionMode.equals(ImageAcquisitionModeEnum.OFFLINE) == true)
    {
      return;
    }
    _testExecutionTimer.startIrpPausedTimer();

    debug("ReconstructedImagesProducer.pauseImageAcquisition");

    if ((XrayTester.getInstance().isSimulationModeOn() == false)
        || ImageAcquisitionEngine.getInstance().isOfflineReconstructionEnabled())
    {
      // Tell the IRE's to temporarily stop sending images
      for (final ImageReconstructionEngine ire : _imagingChainProgramGenerator.getParticipatingImageReconstructionEngines())
      {
        _concurrentTasks.submitThreadTask(new PauseReconstructionThreadTask(ire));
      }

      waitForTasksToComplete();
    }
    else
    {
      _simulatedIrpPausedLock.setValue(true);
    }

    _progressMonitor.reconstructionIsPaused();
  }

  /**
   * Resume image acquisition
   *
   * @author Roy Williams
   */
  public void resumeImageAcquisition() throws XrayTesterException
  {
    Assert.expect(_acquisitionMode != null);

    if (isAbortInProgress())
      return;

    if (_acquisitionMode.equals(ImageAcquisitionModeEnum.OFFLINE) == true)
    {
      return;
    }

    debug("ReconstructedImagesProducer.resumeImageAcquisition");

    if ((XrayTester.getInstance().isSimulationModeOn() == false)
        || ImageAcquisitionEngine.getInstance().isOfflineReconstructionEnabled())
    {
      // Tell the IRE's to resume sending images to the master
      for (final ImageReconstructionEngine ire : _imagingChainProgramGenerator.getParticipatingImageReconstructionEngines())
      {
        if (isAbortInProgress())
          return;
        _concurrentTasks.submitThreadTask(new ResumeReconstructionThreadTask(ire));
      }

      waitForTasksToComplete();
    }
    else
    {
      _simulatedIrpPausedLock.setValue(false);
    }

    _testExecutionTimer.stopIrpPausedTimer();
    _progressMonitor.reconstructionIsRunning();
  }

  /**
   * Sends each camera the necessary acquisition parameters.  This instructs
   * the cameras on which projections to aquire and where to send
   * the projection data.
   * <p>
   * If _debug is true the program(s) will be sent to the cameras one at a time.
   * Otherwise, each program will be programmed on a separate thread.
   *
   * @author Dave Ferguson
   * @author Roy Williams
   */
  protected void prepareCamerasForAcquisition() throws XrayTesterException
  {
    List<ScanPass> scanPasses = _testProgram.getScanPasses();
    List<XrayCameraAcquisitionParameters> xrayCameraAcquisitionParameters = _testProgram.getCameraAcquisitionParameters();

    for (XrayCameraAcquisitionParameters cameraParameters : xrayCameraAcquisitionParameters)
    {
      DownloadProgramToCameraThreadTask downloadProgramToCameraTask = new DownloadProgramToCameraThreadTask(cameraParameters.getCamera(),
                                                                                                            cameraParameters.getProjectionSettings(),
                                                                                                            scanPasses);
      if (_printProgramDownloadedToCamera)
      {
        try
        {
          downloadProgramToCameraTask.executeTask();
        }
        catch (XrayTesterException ex)
        {
          throw ex;
        }
        catch (Exception ex)
        {
          Assert.expect(false);
        }
      }
      else
        _concurrentTasks.submitThreadTask(downloadProgramToCameraTask);
    }
  }
  
  /**
   * Sends each camera the necessary acquisition parameters.  This instructs
   * the cameras on which projections to aquire and where to send
   * the projection data.
   * <p>
   * If _debug is true the program(s) will be sent to the cameras one at a time.
   * Otherwise, each program will be programmed on a separate thread.
   *
   * @author Wei Chin
   */
  protected void prepareCamerasForAcquisition(List<ScanPass> scanPasses, List<XrayCameraAcquisitionParameters> xrayCameraAcquisitionParameters) throws XrayTesterException
  {
    Assert.expect(scanPasses != null);
    Assert.expect(xrayCameraAcquisitionParameters != null);
    
    for (XrayCameraAcquisitionParameters cameraParameters : xrayCameraAcquisitionParameters)
    {
      DownloadProgramToCameraThreadTask downloadProgramToCameraTask = new DownloadProgramToCameraThreadTask(cameraParameters.getCamera(),
                                                                                                            cameraParameters.getProjectionSettings(),
                                                                                                            scanPasses);
      if (_printProgramDownloadedToCamera)
      {
        try
        {
          downloadProgramToCameraTask.executeTask();
        }
        catch (XrayTesterException ex)
        {
          throw ex;
        }
        catch (Exception ex)
        {
          Assert.expect(false, "prepareCamerasForAcquisition : " + ex.getMessage());
        }
      }
      else
        _concurrentTasks.submitThreadTask(downloadProgramToCameraTask);
    }
  }

  /**
   * Initialize image reconstruction engines.
   *
   * @author Roy Williams
   */
  boolean hasConnectedToCamerasBeenSentToAllIrps()
  {
    for (ImageReconstructionEngine ire : _imageReconstructionEngines)
    {
      if (ire.hasConnectedToCamerasBeenSent() == false)
        return false;
    }
    return true;
  }

  /**
   * Initialize image reconstruction engines.
   *
   * @author Roy Williams
   */
  private void initializeImageReconstructionEngines() throws XrayTesterException
  {
    boolean isIrpCameraConnectionLost = false ;
    // At a minimum, each IRE's must be told to re-initialize their image maps for the next subprogram.
    for (ImageReconstructionEngine ire : _imageReconstructionEngines)
    {
      ire.clearImagesMaps();
      ire.initializeReconstructedImages(_testProgram, _acquisitionMode);
      // XCR-3325, Intermittent IRP-Camera Socket Connection Crash in Magnification Adjustment
      // XCR-3336, Assert when run CDNA after production error
      if(ire.areCommunicationPathsToImageReconstructionProcessorEstablished() &&
         ire.isIrpToCameraConnectionBroken())
      {
        isIrpCameraConnectionLost = true;
      }
    }

    // if this is not a full reprogramming of the IRP's lets get out now.
    _reprogramIrps = isIrpCameraConnectionLost || _reprogramIrps;

    if (_reprogramIrps == false)
      return;

    for (ImageReconstructionEngine ire : _imageReconstructionEngines)
    {
      boolean irpMustParticipateDuringNextCollection = _imagingChainProgramGenerator.getParticipatingImageReconstructionEngines().contains(ire);

      _concurrentTasks.submitThreadTask(new InitializeTestProgramThreadTask(ire,
                                                                            _acquisitionMode,
                                                                            irpMustParticipateDuringNextCollection,
                                                                            _testProgram,
                                                                            true));
    }
    if (_config.isDeveloperDebugModeOn() || hasConnectedToCamerasBeenSentToAllIrps() == false)
    {
      debug("Waiting for all IRPs to finish InitializeTestProgramThreadTask");
      waitForTasksToComplete();
    }
    
    if (isUpdateStartPort())
      _imagingChainProgramGenerator.updateReconstructionRegionStartPortNumber(_testProgram);
  }

  /**
   * @author Roy Williams
   */
  private void initializeCameras() throws XrayTesterException
  {
    if(checkForAbortInProgress())
      return;

    // OKAY!  Everybody is programmed and we can tell cameras to start capturing images.
    List<XrayCameraAcquisitionParameters> xrayCameraAcquisitionParameters = _testProgram.getCameraAcquisitionParameters();
    for (XrayCameraAcquisitionParameters cameraParameters : xrayCameraAcquisitionParameters)
    {
      AbstractXrayCamera camera = cameraParameters.getCamera();
      camera.enableTriggerDetection();
      camera.initializeAcquisition();
    }
  }

  /**
   * @author Roy Williams
   */
  private void waitForTasksToComplete() throws XrayTesterException
  {

    if(checkForAbortInProgress())
      return;

    try
    {
      _concurrentTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xex)
    {
      // Let the abort procedure do the shutdown in an orderly fashion.  We were
      // (originally) trying to _concurrentTasks.cancel() here.  This is handled
      // by abort(xex);
      abort(xex);
      throw xex;
    }
    catch (Throwable tex)
    {
      Assert.logException(tex);
    }
    finally
    {
      checkForAbortInProgress();
    }
  }

  /**
   * @author George A. David
   */
  private void waitForAbortTasksToComplete() throws XrayTesterException
  {
    try
    {
      _concurrentAbortTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException xex)
    {
      try
      {
        _concurrentAbortTasks.cancel();
      }
      catch(Exception ex2)
      {
        // do nothing, we already have an exception to deal with
      }

      throw xex;
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * @author Roy Williams
   */
  public void update(Observable observable, Object object)
  {

    // On shutdown (from XrayTester) we will be notified and we are going to
    // make the currently downloaded program invalid.
    if (observable instanceof HardwareObservable)
    {
      if (object instanceof HardwareEvent)
      {
        HardwareEvent hardwareEvent = (HardwareEvent)object;
        if (hardwareEvent.getEventEnum().equals(XrayCameraEventEnum.INITIALIZE) ||
            hardwareEvent.getEventEnum().equals(PanelPositionerEventEnum.INITIALIZE) ||
            hardwareEvent.getEventEnum().equals(ImageReconstructionProcessorEventEnum.INITIALIZING_IMAGE_RECONSTRUCTION_PROCESSORS) ||
            hardwareEvent.getEventEnum().equals(XrayTesterEventEnum.SHUTDOWN))
        {
          _reprogramCamerasAndStage = true;
          _reprogramIrps = true;
        }
      }
    }

    else if (observable instanceof ImageAcquisitionExceptionObservable)
    {
      if (object instanceof XrayTesterException)
      {
        // Always, always let abort function choose whether this exception is more
        // important than the one it is currently servicing.
        abort((XrayTesterException)object);
      }
    }
  }

  /**
   * @author Roy Williams
   */
  public void setupUnitTestForHardwareExceptionReceiver(Observer unitTestUpdateObserver)
  {
    Assert.expect(unitTestUpdateObserver != null);

    _imageAcquisitionExceptionObservable.deleteObserver(this);
    _imageAcquisitionExceptionObservable.addObserver(unitTestUpdateObserver);
  }

  /**
   * When calibration constants are reset, then mechanical conversions need to be
   * reset.   This leads to the need to recompute stage travel and reprogram the
   * cameras and reprogram the IRP's.
   *
   * @author Roy Williams
   */
  public void calibrationConstantsWereReset() throws XrayTesterException
  {
    // Easiest way to force all recomputation is to set _testProgram to null.
    // When acquireImages() method is called again, everything gets recomputed
    // and downloaded to the hardware.
    _testProgram = null;
  }


  /**
   * @author George A. David
   */
  boolean isAcquiringImages()
  {
    return _isAcquiringImages;
  }

  /**
   * @author George A. David
   */
  boolean isPaused()
  {
    boolean isPaused = false;
    for(ImageReconstructionEngine ire : _imageReconstructionEngines)
    {
      isPaused = ire.isPaused();
      if(isPaused)
        break;
    }

    return isPaused;
  }

  /**
   * @author George A. David
   */
  public void clearForNextRun()
  {
    super.clearForNextRun();
    _userAbortedOfflineImageAcquisition = false;
  }

  /**
   * @author George A. David
   */
  public boolean isAbortInProgress()
  {
    if(super.isAbortInProgress())
      return true;
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.OFFLINE))
    {
      // for the offline case, we need to check one more variable
      return _userAbortedOfflineImageAcquisition;
    }

    return false;
  }

  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  public boolean checkForAbortInProgress() throws XrayTesterException
  {
    if (super.checkForAbortInProgress())
    {
      return true;
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.OFFLINE))
    {
      // for the offline case, we need to check one more variable
      return _userAbortedOfflineImageAcquisition;
    }

    return false;
  }

  /**
   * @author Roy Williams
   */
  protected void performBreakPointActionsForScanPass(ScanPass scanPass) throws XrayTesterException
  {
    Assert.expect(scanPass != null);

    // Check to see if there is an action.   If so, take it.
    if (scanPass.hasBreakPoints())
    {
      Collection<ScanPassBreakPoint> breakPoints = scanPass.getBreakPoints();
      
      if (_testSubProgram.getScanPasses().get(0).equals(scanPass) == false)
      {
        // Wait for all scan passes to be delivered to the IRPs.
        waitForScanPassToBeAcknowledged(scanPass.getId());
        // Must wait until all scan pass is completed
        PanelPositioner.getInstance().waitTillScanPassDone(_scanMotionTimeOutMultiplyFactor);
        waitTillIreFreeAndReady(scanPass.isContainScanPassBreak(ScanPassBreakPointEnum.SURFACE_MAP) == false);
        turnOffSyntheticTriggersThenWaitForCamerasToBecomeIdle();
      }
      
      if(checkForAbortInProgress())
        return;
      
      for(ScanPassBreakPoint breakPoint : breakPoints)
      {
        performBreakPointAction(scanPass, breakPoint);
        _scanMotionTimeOutMultiplyFactor = breakPoint.getScanMotionTimeOutMultiplyFactor();
      }
      waitTillProjectionConsumersAreReadyForScanPass(scanPass.getId());
      waitForPriorIrpCommandsToComplete();
    }
  }

  /**
   * @author Yong Sheng Chuan
   */
  private void waitTillIreFreeAndReady(boolean waitForAlignment)
  {
    try
    {
      if (waitForAlignment)
        _alignmentLock.waitUntilTrue(500);
      waitForAllIreFreed();
    }
    catch (InterruptedException ex)
    {
      //do nothing
    }
  }
  
  /**
   * @author Khang Wah, 2016-05-12, MVEDR 
   */
  protected void performBreakPointAction(ScanPass scanPass,
                                         ScanPassBreakPoint breakPointAction) throws XrayTesterException
  {
    if (breakPointAction.isEnabled())
    {
      //PCR-477, IRP Hung When Generate Precision Image Set
      //do not wait if it is real time PSH
      if(breakPointAction instanceof SurfaceModelBreakPoint && Config.isRealTimePshEnabled() && _testSubProgram.isSufficientMemoryForRealTimePsh())
        return;
      // Wait for all images to come in before pausing IRPs.
      waitForRegionsThatMustBeMarkedCompletePriorToExecutingBreakPointAction(
          breakPointAction.getNumberOfRegionsThatMustBeMarkedCompletePriorToExecutingAction());

      // Execute the breakpoint.
      breakPointAction.executeAction();
    }
  }
  
  /**
   * @author Chnee Khang Wah, 2013-08-16, Realtime PSH
   */
  protected void performClearBreakPointActions() throws XrayTesterException
  {
    if (Config.isRealTimePshEnabled()==false || _testSubProgram.isSufficientMemoryForRealTimePsh()==false)
      return;
      
    final boolean isUnitTest = UnitTest.unitTesting() ? true : false;
    
    if (isUnitTest == false)
      disableSurfaceModeling();
    
    InspectionEngine.getInstance().setDoSurfaceModeling(false);
    
    if (isUnitTest == false)
      waitForPriorIrpCommandsToComplete();
  }

  /**
   * Wait till all reconstruction regions required for calculations in the GSM
   * are classified.  Each region will be marked complete once retests are complete.
   */
  public void waitForRegionsThatMustBeMarkedCompletePriorToExecutingBreakPointAction(
      int regionsThatMustBeMarkedCompletePriorToUsingGlobalSurfaceModel)
      throws XrayTesterException
  {
    boolean unitTesting = UnitTest.unitTesting();
    while (getNumberOfReconstructionRegionsCompleted() <
           regionsThatMustBeMarkedCompletePriorToUsingGlobalSurfaceModel)
    {
      try
      {
        // must ask the holder of the lock/monitor to release it temporarily so
        // user aborts can come through.
        wait(200);
        if (checkForAbortInProgress() == true)
          return;
      }
      catch (InterruptedException ie)
      {
        // do nothing
      }
    }
  }

  /**
   * @author Roy Williams
   */
  public static void defineProcessorStripsForVerificationRegions(TestProgram testProgram)
  {
    ImagingChainProgramGenerator.defineProcessorStripsForVerificationRegions(testProgram);
  }

  /**
   * @author Roy Williams
   */
  public static void defineProcessorStripsForInspectionRegions(TestProgram testProgram)
  {
    ImagingChainProgramGenerator.defineProcessorStripsForInspectionRegions(testProgram);
  }

  /**
   * @author Roy Williams
   */
  public void clearProjectInfo () throws XrayTesterException
  {
    Assert.expect(_isAcquiringImages == false);   // not allowed to reset this info while running

    _acquisitionMode = ImageAcquisitionModeEnum.NOT_IN_USE;

    // do local cleanup then pass it down.
    for (ImageReconstructionEngine ire : _imageReconstructionEngines)
    {
      ire.clearProjectInfo();
    }


    // Get rid of all references to TestProgram and TestSubProgram.
    if (_imagingChainProgramGenerator != null)
      _imagingChainProgramGenerator.clearProjectInfo();
    if (_filteredTestSubProgram != null)
      _filteredTestSubProgram.clear();  // no testSubProgram references allowed.

    // Iterate through the ScanPasses.  They may have a reference to a ScanPassBreakPoint
    // and these traditionally have a refrence to the TestSubProgram.
    clearBreakPoints();
    _testProgram    = null;                   // no testProgram
    _testSubProgram = null;                // because it has a reference to TestProgram

    // Get rid of all those pesky references to ReconstructionRegions because
    // they hold reference to TestSubProgram.
    _reconstructionRegionsMarkedCompleteMap.clear();

    // pass it down?
    _reconstructedImagesManager.initialize();
  }

  /**
   * @author Roy Williams
   */
  private void clearBreakPoints()
  {
    if (_testProgram != null)
    {
      for (TestSubProgram testSubProgram : _testProgram.getAllTestSubPrograms())
      {
        for (ScanPass scanPass : testSubProgram.getScanPasses())
        {
          scanPass.clearBreakPoint();
        }
      }
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public void decrementIreReference()
  {
    //swee-yee.wong - shouldn't expect it is not null here, it will be null when user abort, 
    //so we just need to do it when it is not null
    //Assert.expect(_imageReconstructionEngines != null);

    if (_imageReconstructionEngines != null)
    {
      for (ImageReconstructionEngine ire : _imageReconstructionEngines)
      {
        ire.decrementReconstructedImages();
      }
    }
  }
  
  /**
   * author sham
   */
  public void clear()
  {
    Assert.expect(_reconstructionRegionsMarkedCompleteMap != null);
	
	_reconstructionRegionsMarkedCompleteMap.clear();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void enableSurfaceModel() throws XrayTesterException
  {
    if (UnitTest.unitTesting() == false)
      enableSurfaceModeling();
        
    InspectionEngine.getInstance().setDoSurfaceModeling(true);
  }
  
  /**
   * @author Siew Yeng
   */
  private void waitingForPreviousTestSubProgramOfflineInspectionToComplete() throws XrayTesterException
  {
    boolean isAllReconstructionRegionsFinishedInspection = false;
    
    while(isAllReconstructionRegionsFinishedInspection == false && checkForAbortInProgress() == false)
    {
      isAllReconstructionRegionsFinishedInspection = ReconstructedImagesMemoryManager.getInstance().isAllReconstructionRegionsFinishedInspection();
    }
  }
  
  /**
   * Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
   * @author Swee-Yee.Wong
   */
  private void logIaeStageCommands(String message)
  {
    if (_config.isMotionControlLogEnabled())
    {
      try
      {
        IaeStageCommandLogUtil.getInstance().log(message);
      }
      catch (XrayTesterException e)
      {
        System.out.println("Failed to log iaeStageCommands. \n" + e.getMessage());
      }
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  private void debug(String message)
  {
    if (_debug)
      System.out.println(_me + ": " + message);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean isUpdateStartPort()
  {
    for (ImageReconstructionEngine ire : _imageReconstructionEngines)
    {
      if (ire.isUpdateStartPort())
        return true;
    }
    return false;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private Set<VelocityMappingParameters> getVelocityMappingParameters(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);
    
    Set<VelocityMappingParameters> velocityMappingParametersList = new HashSet<VelocityMappingParameters> ();
    List<Double> stageSpeedList = new LinkedList<Double> ();
    for (TestSubProgram subProgram : testProgram.getFilteredTestSubPrograms())
    {
      for (List<StageSpeedEnum> listOfSpeedCombination : subProgram.getAllUsedStageSpeedSettings())
      {
        if (listOfSpeedCombination.size() <= 1)
          continue;
        
        stageSpeedList.clear();
        for (StageSpeedEnum speed : listOfSpeedCombination)
        {
          stageSpeedList.add(speed.toDouble());
        }
        // sort ascending
        Collections.sort(stageSpeedList);
        velocityMappingParametersList.addAll(VelocityMapping.getInstance().getVelocityMappingParameters(subProgram.getMagnificationType(), stageSpeedList));
      }
    }
    return velocityMappingParametersList;
  }
}
