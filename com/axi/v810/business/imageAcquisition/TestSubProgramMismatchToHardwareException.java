package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class TestSubProgramMismatchToHardwareException extends XrayTesterException
{
  /**
   * @author Roy Williams
   */
  public TestSubProgramMismatchToHardwareException(int testSubProgramExpects,
                                                          int hardwareConfiguredToRun)
  {
    super(new LocalizedString("HW_TEST_SUBPROGRAM_MISMATCH_TO_HARDWARE_KEY",
                              new Object[]
                              {
                                Integer.toString(testSubProgramExpects),
                                Integer.toString(hardwareConfiguredToRun)}));
  }
}
