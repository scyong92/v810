package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.ScanPass;
import com.axi.v810.util.SystemFiducialRectangle;

/**
 * The members of this class would have been added to ScanPass if it had visibility
 * from the hardware layer to business.  It doesn't.
 *
 * @author Roy Williams
 */
public class ReconstructionRegionsInScanPass
{
  private ScanPass _scanPass;
  private List<Pair<ReconstructionRegion, SystemFiducialRectangle>> _regionPairs = new ArrayList<Pair<ReconstructionRegion,SystemFiducialRectangle>>();
  private Pair<ReconstructionRegion, SystemFiducialRectangle> _lastReconstructionRegionPairBeginScopeLocation;
  private int _meanStepSizeTaken = 0;

  // Lists of regions that are having their first or last sweep on this _scanPass.
  private List<ReconstructionRegion> _regionsEnteringScanPassScope = new ArrayList<ReconstructionRegion>();
  private List<ReconstructionRegion> _regionsExitingScanPassScope  = new ArrayList<ReconstructionRegion>();

  /**
   * @author Roy Williams
   */
  public ReconstructionRegionsInScanPass()
  {
  }

  /**
   * @author Roy Williams
   */
  public ReconstructionRegionsInScanPass(ScanPass scanPass)
  {
    Assert.expect(scanPass != null);

    _scanPass = scanPass;
  }

  /**
   * @author Roy Williams
   */
  public ScanPass getScanPass()
  {
    return _scanPass;
  }

  /**
   * @author Roy Williams
   */
  public List<ReconstructionRegion> getRegionsEnteringScanPassScope()
  {
    return _regionsEnteringScanPassScope;
  }

  /**
   * @author Roy Williams
   */
  public List<ReconstructionRegion> getRegionsExitingScanPassScope()
  {
    return _regionsExitingScanPassScope;
  }

  /**
   * @author Roy Williams
   */
  public void add(Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair)
  {
    Assert.expect(regionPair != null);

    _regionPairs.add(regionPair);
  }

  /**
   * @author Roy Williams
   */
  public List<Pair<ReconstructionRegion, SystemFiducialRectangle>> getImagedReconstructionRegionPairs()
  {
    Assert.expect(_regionPairs != null);

    return _regionPairs;
  }

  /**
   * @author Roy Williams
   */
  public boolean contains(ReconstructionRegion region)
  {
    Assert.expect(_regionPairs != null);

    for (Pair<ReconstructionRegion, SystemFiducialRectangle> pair : _regionPairs)
    {
      if (pair.getFirst().getRegionId() == region.getRegionId())
        return true;
    }
    return false;
  }

  /**
   * @author Roy Williams
   */
  public int getNumberOfReconstructionRegionsUsingProposedScanPass()
  {
    Assert.expect(_regionPairs != null);

    return _regionPairs.size() + _regionsExitingScanPassScope.size();
  }

  /**
   * @author Roy Williams
   */
  public void addRegionsEnteringReconstructionScope(ReconstructionRegion region)
  {
    Assert.expect(region != null);

    _regionsEnteringScanPassScope.add(region);
  }

  /**
   * @author Roy Williams
   */
  public List<ReconstructionRegion> getRegionsEnteringReconstructionScope()
  {
    return _regionsEnteringScanPassScope;
  }

  /**
   * @author Roy Williams
   */
  public void addRegionsExitingReconstructionScope(ReconstructionRegion region)
  {
    Assert.expect(region != null);

    _regionsExitingScanPassScope.add(region);
  }

  /**
   * @author Roy Williams
   */
  boolean hasLastReconstructionRegionPairBeginScopeLocation()
  {
    return _lastReconstructionRegionPairBeginScopeLocation != null;
  }

  /**
   * @author Roy Williams
   */
  Pair<ReconstructionRegion, SystemFiducialRectangle> getLastReconstructionRegionPairBeginScopeLocation()
  {
    return _lastReconstructionRegionPairBeginScopeLocation;
  }

  /**
   * @author Roy Williams
   */
  void setLastReconstructionRegionPairBeginScopeLocation(Pair<ReconstructionRegion, SystemFiducialRectangle> last)
  {
    Assert.expect(last != null);
    _lastReconstructionRegionPairBeginScopeLocation = last;
  }

  /**
   * @author Roy Williams
   */
  public void setMeanStepSizeTaken(int meanStepSize)
  {
    _meanStepSizeTaken = meanStepSize;
  }

  /**
   * @author Roy Williams
   */
  public int getMeanStepSizeTaken()
  {
    return _meanStepSizeTaken;
  }
}
