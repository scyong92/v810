package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * An ImageZone is a rectangular region which is imaged at a uniform
 * mean step size throughout the region.  A HomogeneousImageGroup contains a single
 * ImageZone, while a HeterogeneousImageGroup may contain one or multiple ImageZones.
 * Reconstruction regions must be completely imaged within an ImageGroup, but they
 * may be covered by multiple ImageZones (which would mean that part of the
 * reconstruction region is imaged at one mean step size and another part of the
 * reconstruction region is imaged at a different mean step size.)
 *
 * @author Kay Lannen
 */
public class ImageZone implements Comparable<ImageZone>
{
  protected int _meanStepSizeInNanoMeters = 0;
  protected int _ditherAmplitude = 0;
  protected ScanPath _scanPath = null;
  private List<Pair<Integer, Integer>> _confirmedCandidateStepRanges;

  /**
   * @author Kay Lannen
   */
  public int getMeanStepSizeInNanoMeters()
  {
    return (_meanStepSizeInNanoMeters);
  }

  /**
   * @author Kay Lannen
   */
  public void setMeanStepSizeInNanoMeters(int meanStepSizeInNanoMeters)
  {
    _meanStepSizeInNanoMeters = meanStepSizeInNanoMeters;
  }

  /**
   * @author Kay Lannen
   */
  public int getDitherAmplitude()
  {
    return (_ditherAmplitude);
  }

  /**
   * @author Kay Lannen
   */
  public void setDitherAmplitude(int ditherAmplitude)
  {
    _ditherAmplitude = ditherAmplitude;
  }

  /**
   * @author Kay Lannen
   */
  public ScanPath getScanPath()
  {
    return (_scanPath);
  }

  /**
   * @author Kay Lannen
   */
  public void setScanPath(ScanPath scanPath)
  {
    _scanPath = scanPath;
  }

  /**
   * @author Kay Lannen
   */
  public MachineRectangle getScanPathExtents()
  {
    Assert.expect(_scanPath != null);
    return (_scanPath.getScanPathExtents());
  }

  /**
   * @author Kay Lannen
   */
  public List<Pair<Integer, Integer>> getConfirmedCandidateStepRanges()
  {
    return (_confirmedCandidateStepRanges);
  }

  /**
   * @author Kay Lannen
   */
  public void setConfirmedCandidateStepRanges(List<Pair<Integer, Integer>> confirmedCandidateStepRanges)
  {
    _confirmedCandidateStepRanges = confirmedCandidateStepRanges;
  }

  /**
   * @author Kay Lannen
   */
  public int compareTo(ImageZone other)
  {
    // This is the default comparator for ImageZones.
    // Sort Image Zones by xmin, ymin, width, and height of their ScanPathExtents,
    // then meanStepSizeInNanoMeters, in that order.
    if(_scanPath == null)
      return 0;
    
    MachineRectangle scanPathExtents = _scanPath.getScanPathExtents();
    MachineRectangle otherScanPathExtents = other.getScanPath().getScanPathExtents();

    int xmin = scanPathExtents.getMinX();
    int otherXmin = otherScanPathExtents.getMinX();
    if (xmin < otherXmin)
    {
      return -1;
    }
    else if (xmin > otherXmin)
    {
      return 1;
    }

    int ymin = scanPathExtents.getMinY();
    int otherYmin = otherScanPathExtents.getMinY();
    if (ymin < otherYmin)
    {
      return -1;
    }
    else if (ymin > otherYmin)
    {
      return 1;
    }

    int width = scanPathExtents.getWidth();
    int otherWidth = otherScanPathExtents.getWidth();
    if (width < otherWidth)
    {
      return -1;
    }
    else if (width > otherWidth)
    {
      return 1;
    }

    int height = scanPathExtents.getHeight();
    int otherHeight = otherScanPathExtents.getHeight();
    if (height < otherHeight)
    {
      return -1;
    }
    else if (height > otherHeight)
    {
      return 1;
    }

    if (_meanStepSizeInNanoMeters < other._meanStepSizeInNanoMeters)
    {
      return -1;
    }
    else if (_meanStepSizeInNanoMeters > other._meanStepSizeInNanoMeters)
    {
      return 1;
    }
    return 0;
  }
}
