package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;

/**
 * @author Roy Williams
 */
public class RescanShadedRegionsBasedUponComponentsWithAreaCompression extends RescanShadedRegionsBasedUponDripSourceComponent
{

  /**
   * @author Roy Williams
   */
  RescanShadedRegionsBasedUponComponentsWithAreaCompression()
  {
    super(ScanStrategyEnum.GROUP_ON_COMPONENTS_WITH_AREA_COMPRESSION);
  }

  /**
   * @author Roy Williams
   */
  public List<HomogeneousImageGroup> mergeGroups(List<HomogeneousImageGroup> currentGroups)
  {
    Assert.expect(false);
    return null; // ImageGroup.mergeGroups(currentGroups, false, true);
  };

}
