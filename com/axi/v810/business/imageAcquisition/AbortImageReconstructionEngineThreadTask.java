package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author George A. David
 */
public class AbortImageReconstructionEngineThreadTask extends ThreadTask<Object>
{
  private ImageReconstructionEngine _imageReconstructionEngine;
  private boolean _cancelled;

  /**
   * @author George A. David
   */
  public AbortImageReconstructionEngineThreadTask(ImageReconstructionEngine imageReconstructionEngine)
  {
    super("AbortImageReconstructionEngineThreadTask:" + imageReconstructionEngine.getId());

    Assert.expect(imageReconstructionEngine != null);

    _imageReconstructionEngine = imageReconstructionEngine;
    _cancelled = false;
  }

  /**
   * @author George A. David
   */
  public Object executeTask() throws Exception
  {
    // Between each step check to see if we should continue or cancel.
    if (_cancelled)
      return null;

    _imageReconstructionEngine.abort();

    return null;
  }

  /*
   * @author George A. David
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author George A. David
   */
  protected void cancel() throws XrayTesterException
  {
    _cancelled = true;
  }
}
