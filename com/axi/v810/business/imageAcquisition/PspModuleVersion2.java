package com.axi.v810.business.imageAcquisition;


import com.axi.util.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PspModuleVersion2 extends PspEngine
{  
  private static final int TOTAL_CALIBRATION_RUN = 1;    
  private PanelPositioner _panelPositioner;
  
  protected Map<PspModuleEnum, Map<OpticalCalibrationPlaneEnum, StagePositionMutable>> _pspModuleToStagePositionMap = new LinkedHashMap<PspModuleEnum, Map<OpticalCalibrationPlaneEnum, StagePositionMutable>>();
  
  /**
   * @author Cheah Lee Herng
   */
  public PspModuleVersion2()
  {    
    _panelPositioner = PanelPositioner.getInstance();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void handlePreOpticalImageAcquistion(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException 
  {
    // Do nothing
  }

  /**
   * @author Cheah Lee Herng
   */
  public void handleActualOpticalImageAcquisition(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException  
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalShiftEnum != null);
    
    _pspHardwareEngine.sendTriggerSequence(moduleEnum, opticalShiftEnum);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void handlePostOpticalImageAcquisition(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    _pspHardwareEngine.turnOffLED(moduleEnum);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void performCalibration() throws XrayTesterException 
  {
    performCalibrationForSmallFov();
    //Ying-Huan.Chu - temporary commented Larger FOV feature for 5.7 release.
    //performCalibrationForLargerFov();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void performCalibrationForSmallFov() throws XrayTesterException 
  {
    // Setup stage position
    setupOpticalCalibrationStagePosition();
    
    // Start calibration process
    for(PspModuleEnum moduleEnum : PspModuleEnum.getAllEnums())
    {      
      // Kok Chun, Tan - XCR2490 - 24/2/2014
      // Check the optical camera used is enable or not. If not enable, skip the rectangle.
      if (SurfaceMapping.getInstance().isOpticalCameraEnable(moduleEnum) == false)
      {
        continue;
      }
      
      Map<OpticalCalibrationPlaneEnum, StagePositionMutable> opticalCalibrationEnumToStagePositionMap = _pspModuleToStagePositionMap.get(moduleEnum) ;
      OpticalCameraIdEnum opticalCameraIdEnum = _pspImageAcquisitionEngine.getOpticalCameraIdEnum(moduleEnum);
      
      int index = 1;

      // Initialize optical system
      OpticalCameraSetting opticalCameraSetting = new OpticalCameraSetting(PspImageAcquisitionEngine.getInstance().getFrontModuleCropImageOffsetXInPixel(),
                                                                           PspImageAcquisitionEngine.getInstance().getRearModuleCropImageOffsetXInPixel());
      _pspImageAcquisitionEngine.initializeOpticalCamera(moduleEnum, opticalCameraSetting);

      OpticalCalibrationData opticalCalibrationData = new OpticalCalibrationData();
      opticalCalibrationData.setPspModuleEnum(moduleEnum);
      opticalCalibrationData.setProfileEnum(OpticalCalibrationProfileEnum.PROFILE_1);
      opticalCalibrationData.setTotalRun(TOTAL_CALIBRATION_RUN);
      opticalCalibrationData.setKnownHeight1(PspConfiguration.getInstance().getOpticalCalibrationKnownHeightInMicron());

      String opticalCalibrationProfileDirectory = null;
      if (moduleEnum.equals(PspModuleEnum.FRONT))
        opticalCalibrationProfileDirectory = Directory.getOpticalCalibrationProfileFrontModuleDir();        
      else
        opticalCalibrationProfileDirectory = Directory.getOpticalCalibrationProfileRearModuleDir();      
      opticalCalibrationData.setCalibrationDirectoryFullPath(opticalCalibrationProfileDirectory);
      
      // If there is no directory created, there will be no calibration data
      // generated, which caused PSP not functioning.
      if (FileUtilAxi.existsDirectory(opticalCalibrationProfileDirectory) == false)
      {
        FileUtilAxi.createDirectory(opticalCalibrationProfileDirectory);
      }
      
      // If there is no directory created for log/calib/OpticalCalibration, 
      // system will create manually
      if (FileUtilAxi.existsDirectory(Directory.getOpticalCalibrationCalLogDir()) == false)
      {
        FileUtilAxi.createDirectory(Directory.getOpticalCalibrationCalLogDir());
      }

      // Reference Plane
      index = 1;
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_ONE,
                                              FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_TWO,
                                              FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_THREE,
                                              FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_FOUR,
                                              FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_FIVE,
                                              FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_SIX,
                                              FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_SEVEN,
                                              FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_EIGHT,
                                              FileName.getOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));

      // Plus Three Plane
      index = 1;
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_ONE,
                                              FileName.getOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_TWO,
                                              FileName.getOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_THREE,
                                              FileName.getOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_FOUR,
                                              FileName.getOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_FIVE,
                                              FileName.getOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_SIX,
                                              FileName.getOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_SEVEN,
                                              FileName.getOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_EIGHT,
                                              FileName.getOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));

      _pspImageAcquisitionEngine.acquireCalibrationImage(opticalCalibrationData);
      _pspHardwareEngine.performCalibration(moduleEnum, opticalCalibrationData);
    }
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void performCalibrationForLargerFov() throws XrayTesterException 
  {
    // Setup stage position
    setupLargerFovOpticalCalibrationStagePosition();
    
    // Start calibration process
    for(PspModuleEnum moduleEnum : PspModuleEnum.getAllEnums())
    {
      // Kok Chun, Tan - XCR2490 - 24/2/2014
      // Check the optical camera used is enable or not. If not enable, skip the rectangle.
      if (SurfaceMapping.getInstance().isOpticalCameraEnable(moduleEnum) == false)
      {
        continue;
      }
      
      AbstractOpticalCamera abstractOpticalCamera;
      if (moduleEnum.equals(PspModuleEnum.FRONT))
        abstractOpticalCamera = AbstractOpticalCamera.getInstance(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
      else
        abstractOpticalCamera = AbstractOpticalCamera.getInstance(OpticalCameraIdEnum.OPTICAL_CAMERA_2);
      
      abstractOpticalCamera.setUseLargerFov(true);
      //abstractOpticalCamera.initialize();
      
      Map<OpticalCalibrationPlaneEnum, StagePositionMutable> opticalCalibrationEnumToStagePositionMap = _pspModuleToStagePositionMap.get(moduleEnum) ;
      OpticalCameraIdEnum opticalCameraIdEnum = _pspImageAcquisitionEngine.getOpticalCameraIdEnum(moduleEnum);
      int index = 1;

      // Initialize optical system
      OpticalCameraSetting opticalCameraSetting = new OpticalCameraSetting(PspImageAcquisitionEngine.getInstance().getLargerFovFrontModuleCropImageOffsetXInPixel(),
                                                                           PspImageAcquisitionEngine.getInstance().getLargerFovRearModuleCropImageOffsetXInPixel());
      _pspImageAcquisitionEngine.initializeOpticalCamera(moduleEnum, opticalCameraSetting);

      OpticalCalibrationData opticalCalibrationData = new OpticalCalibrationData();
      opticalCalibrationData.setPspModuleEnum(moduleEnum);
      opticalCalibrationData.setProfileEnum(OpticalCalibrationProfileEnum.PROFILE_1);
      opticalCalibrationData.setTotalRun(TOTAL_CALIBRATION_RUN);
      opticalCalibrationData.setKnownHeight1(PspConfiguration.getInstance().getOpticalCalibrationKnownHeightInMicron());

      String opticalCalibrationProfileDirectory = null;
      if (moduleEnum.equals(PspModuleEnum.FRONT))
        opticalCalibrationProfileDirectory = Directory.getLargerFovOpticalCalibrationProfileFrontModuleDir();        
      else
        opticalCalibrationProfileDirectory = Directory.getLargerFovOpticalCalibrationProfileRearModuleDir();
      opticalCalibrationData.setCalibrationDirectoryFullPath(opticalCalibrationProfileDirectory);
      
      // If there is no directory created, there will be no calibration data
      // generated, which caused PSP not functioning.
      if (FileUtilAxi.existsDirectory(opticalCalibrationProfileDirectory) == false)
      {
        FileUtilAxi.createDirectory(opticalCalibrationProfileDirectory);
      }
      
      // If there is no directory created for log/calib/OpticalCalibration, 
      // system will create manually
      if (FileUtilAxi.existsDirectory(Directory.getOpticalCalibrationCalLogDir()) == false)
      {
        FileUtilAxi.createDirectory(Directory.getOpticalCalibrationCalLogDir());
      }

      // Reference Plane
      index = 1;
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_ONE,
                                              FileName.getLargerFovOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_TWO,
                                              FileName.getLargerFovOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_THREE,
                                              FileName.getLargerFovOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_FOUR,
                                              FileName.getLargerFovOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_FIVE,
                                              FileName.getLargerFovOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_SIX,
                                              FileName.getLargerFovOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_SEVEN,
                                              FileName.getLargerFovOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.REFERENCE_PLANE),
                                              OpticalShiftNameEnum.SHIFT_EIGHT,
                                              FileName.getLargerFovOpticalCalibrationReferencePlaneImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));

      // Plus Three Plane
      index = 1;
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_ONE,
                                              FileName.getLargerFovOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_TWO,
                                              FileName.getLargerFovOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_THREE,
                                              FileName.getLargerFovOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_FOUR,
                                              FileName.getLargerFovOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_FIVE,
                                              FileName.getLargerFovOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_SIX,
                                              FileName.getLargerFovOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_SEVEN,
                                              FileName.getLargerFovOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));
      opticalCalibrationData.addImageFullPath(OpticalCalibrationProfileEnum.PROFILE_1,
                                              OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                              opticalCalibrationEnumToStagePositionMap.get(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE),
                                              OpticalShiftNameEnum.SHIFT_EIGHT,
                                              FileName.getLargerFovOpticalCalibrationObjectFringePlusThreeImageFullPath(moduleEnum.getId(), opticalCameraIdEnum.getDeviceId(), OpticalCalibrationProfileEnum.PROFILE_1.getId(), index++, TOTAL_CALIBRATION_RUN));

      _pspImageAcquisitionEngine.acquireCalibrationImage(opticalCalibrationData);
      _pspHardwareEngine.performCalibration(moduleEnum, opticalCalibrationData);
      abstractOpticalCamera.setUseLargerFov(false);
    }
  }
  
  /**
   * Setup calibration plate stage position for later use
   * 
   * @author Cheah Lee Herng
   */
  private void setupOpticalCalibrationStagePosition() throws XrayTesterException
  {
    Assert.expect(_pspModuleToStagePositionMap != null);
    
    // Clear before proceed
    _pspModuleToStagePositionMap.clear();

    // Depending on system type, we will get different system offset
    int offsetXFromOpticalFiducialToReferencePlaneInNanometers      = PspConfiguration.getInstance().getOpticalFiducialOffsetXOnReferencePlaneInNanometers();
    int offsetXFromOpticalFiducialToPlusThreePlaneInNanometers      = PspConfiguration.getInstance().getOpticalFiducialOffsetXOnPlusThreePlaneInNanometers();

    int opticalCamera1OffsetYFromOpticalFiducialToPlaneInNanometers = PspConfiguration.getInstance().getFrontModuleOpticalFiducialOffsetYInNanometers();
    int opticalCamera2OffsetYFromOpticalFiducialToPlaneInNanometers = PspConfiguration.getInstance().getRearModuleOpticalFiducialOffsetYInNanometers();
    
    for(PspModuleEnum moduleEnum : PspModuleEnum.getAllEnums())
    {
      Map<OpticalCalibrationPlaneEnum, StagePositionMutable> calMagnificationToStagePositionMap = new HashMap<OpticalCalibrationPlaneEnum, StagePositionMutable>();
      Object object = _pspModuleToStagePositionMap.put(moduleEnum, calMagnificationToStagePositionMap);
      Assert.expect(object == null);

      if (moduleEnum.equals(PspModuleEnum.FRONT))
      {
        StagePositionMutable opticalCalibrationReferencePlaneStagePositionForOpticalCamera1 = new StagePositionMutable();
        opticalCalibrationReferencePlaneStagePositionForOpticalCamera1.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers() - offsetXFromOpticalFiducialToReferencePlaneInNanometers);
        opticalCalibrationReferencePlaneStagePositionForOpticalCamera1.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                        (PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers() + opticalCamera1OffsetYFromOpticalFiducialToPlaneInNanometers)));        
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.REFERENCE_PLANE, opticalCalibrationReferencePlaneStagePositionForOpticalCamera1);
        Assert.expect(object == null);

        StagePositionMutable opticalCalibrationObjectPlane2StagePositionForOpticalCamera1 = new StagePositionMutable();
        opticalCalibrationObjectPlane2StagePositionForOpticalCamera1.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers() - offsetXFromOpticalFiducialToPlusThreePlaneInNanometers);
        opticalCalibrationObjectPlane2StagePositionForOpticalCamera1.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                      (PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers() + opticalCamera1OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE, opticalCalibrationObjectPlane2StagePositionForOpticalCamera1);
        Assert.expect(object == null);
      }
      else if (moduleEnum.equals(PspModuleEnum.REAR))
      {
        StagePositionMutable opticalCalibrationReferencePlaneStagePositionForOpticalCamera2 = new StagePositionMutable();
        opticalCalibrationReferencePlaneStagePositionForOpticalCamera2.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers() - offsetXFromOpticalFiducialToReferencePlaneInNanometers);
        opticalCalibrationReferencePlaneStagePositionForOpticalCamera2.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                        (PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers() - opticalCamera2OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.REFERENCE_PLANE, opticalCalibrationReferencePlaneStagePositionForOpticalCamera2);
        Assert.expect(object == null);

        StagePositionMutable opticalCalibrationObjectPlane2StagePositionForOpticalCamera2 = new StagePositionMutable();
        opticalCalibrationObjectPlane2StagePositionForOpticalCamera2.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers() - offsetXFromOpticalFiducialToPlusThreePlaneInNanometers);
        opticalCalibrationObjectPlane2StagePositionForOpticalCamera2.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                      (PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers() - opticalCamera2OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE, opticalCalibrationObjectPlane2StagePositionForOpticalCamera2);
        Assert.expect(object == null);
      }
    }         
  }
  
  private void setupLargerFovOpticalCalibrationStagePosition() throws XrayTesterException
  {
    Assert.expect(_pspModuleToStagePositionMap != null);
    
    // Clear before proceed
    _pspModuleToStagePositionMap.clear();

    // Depending on system type, we will get different system offset
    int offsetXFromOpticalFiducialToReferencePlaneInNanometers      = PspConfiguration.getInstance().getLargerFovOpticalFiducialOffsetXOnReferencePlaneInNanometers();
    int offsetXFromOpticalFiducialToPlusThreePlaneInNanometers      = PspConfiguration.getInstance().getLargerFovOpticalFiducialOffsetXOnPlusThreePlaneInNanometers();

    int opticalCamera1OffsetYFromOpticalFiducialToPlaneInNanometers = PspConfiguration.getInstance().getLargerFovFrontModuleOpticalFiducialOffsetYInNanometers();
    int opticalCamera2OffsetYFromOpticalFiducialToPlaneInNanometers = PspConfiguration.getInstance().getLargerFovRearModuleOpticalFiducialOffsetYInNanometers();
    
    for(PspModuleEnum moduleEnum : PspModuleEnum.getAllEnums())
    {
      Map<OpticalCalibrationPlaneEnum, StagePositionMutable> calMagnificationToStagePositionMap = new HashMap<OpticalCalibrationPlaneEnum, StagePositionMutable>();
      Object object = _pspModuleToStagePositionMap.put(moduleEnum, calMagnificationToStagePositionMap);
      Assert.expect(object == null);

      if (moduleEnum.equals(PspModuleEnum.FRONT))
      {
        StagePositionMutable opticalCalibrationReferencePlaneStagePositionForOpticalCamera1 = new StagePositionMutable();
        opticalCalibrationReferencePlaneStagePositionForOpticalCamera1.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers() - offsetXFromOpticalFiducialToReferencePlaneInNanometers);
        opticalCalibrationReferencePlaneStagePositionForOpticalCamera1.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                        (PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers() + opticalCamera1OffsetYFromOpticalFiducialToPlaneInNanometers)));        
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.REFERENCE_PLANE, opticalCalibrationReferencePlaneStagePositionForOpticalCamera1);
        Assert.expect(object == null);

        StagePositionMutable opticalCalibrationObjectPlane2StagePositionForOpticalCamera1 = new StagePositionMutable();
        opticalCalibrationObjectPlane2StagePositionForOpticalCamera1.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers() - offsetXFromOpticalFiducialToPlusThreePlaneInNanometers);
        opticalCalibrationObjectPlane2StagePositionForOpticalCamera1.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                      (PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers() + opticalCamera1OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE, opticalCalibrationObjectPlane2StagePositionForOpticalCamera1);
        Assert.expect(object == null);
      }
      else if (moduleEnum.equals(PspModuleEnum.REAR))
      {
        StagePositionMutable opticalCalibrationReferencePlaneStagePositionForOpticalCamera2 = new StagePositionMutable();
        opticalCalibrationReferencePlaneStagePositionForOpticalCamera2.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers() - offsetXFromOpticalFiducialToReferencePlaneInNanometers);
        opticalCalibrationReferencePlaneStagePositionForOpticalCamera2.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                        (PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers() - opticalCamera2OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.REFERENCE_PLANE, opticalCalibrationReferencePlaneStagePositionForOpticalCamera2);
        Assert.expect(object == null);

        StagePositionMutable opticalCalibrationObjectPlane2StagePositionForOpticalCamera2 = new StagePositionMutable();
        opticalCalibrationObjectPlane2StagePositionForOpticalCamera2.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers() - offsetXFromOpticalFiducialToPlusThreePlaneInNanometers);
        opticalCalibrationObjectPlane2StagePositionForOpticalCamera2.setYInNanometers(Math.max(_panelPositioner.getYaxisMinimumPositionLimitInNanometers(), 
                                                                                      (PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers() - opticalCamera2OffsetYFromOpticalFiducialToPlaneInNanometers)));
        object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE, opticalCalibrationObjectPlane2StagePositionForOpticalCamera2);
        Assert.expect(object == null);
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  protected OpticalInspectionData setupHeightMapInspectionData(OpticalPointToPointScan opticalPointToPointScan, String projectName, String opticalImageSetName) 
  {
    Assert.expect(opticalPointToPointScan != null);
    Assert.expect(projectName != null);
    Assert.expect(opticalImageSetName != null);
    
    StagePosition stagePosition = opticalPointToPointScan.getPointToPointStagePositionInNanoMeters();
    PspModuleEnum pspModuleEnum = getPspModuleEnum(opticalPointToPointScan.getOpticalCameraIdEnum());
    
    OpticalInspectionData opticalInspectionData = new OpticalInspectionData();
    opticalInspectionData.setPspModuleEnum(pspModuleEnum);
    opticalInspectionData.setForceTrigger(true);
    opticalInspectionData.setReturnActualHeightMap(opticalPointToPointScan.getReturnActualHeightMap());
    
    String opticalImageName = opticalPointToPointScan.getPointPositionName();
    
    // Setup stage position
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_ONE, FileName.getOpticalImageFullPath(projectName, opticalImageSetName, opticalImageName, 1));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_TWO, FileName.getOpticalImageFullPath(projectName, opticalImageSetName, opticalImageName, 2));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_THREE, FileName.getOpticalImageFullPath(projectName, opticalImageSetName, opticalImageName, 3));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_FOUR, FileName.getOpticalImageFullPath(projectName, opticalImageSetName, opticalImageName, 4));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_FIVE, FileName.getOpticalImageFullPath(projectName, opticalImageSetName, opticalImageName, 5));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_SIX, FileName.getOpticalImageFullPath(projectName, opticalImageSetName, opticalImageName, 6));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_SEVEN, FileName.getOpticalImageFullPath(projectName, opticalImageSetName, opticalImageName, 7));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_EIGHT, FileName.getOpticalImageFullPath(projectName, opticalImageSetName, opticalImageName, 8));
    
    return opticalInspectionData;
  }

  /**
   * This function calculates the final Z Height.
   * 
   * @author Cheah Lee Herng
   */
  protected void setZHeightInNanometers(OpticalPointToPointScan opticalPointToPointScan, PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(opticalPointToPointScan != null);
    Assert.expect(moduleEnum != null);

    if (_pspHardwareEngine.isHeightMapAvailable(moduleEnum))
    {
      float[] newCoordinateXInPixelFromPsp = _pspHardwareEngine.getNewCoordinateXInPixel(moduleEnum);
      float[] newCoordinateYInPixelFromPsp = _pspHardwareEngine.getNewCoordinateYInPixel(moduleEnum);
      float[] zHeightArrayInMilimetersFromPsp = _pspHardwareEngine.getHeightMapValue(moduleEnum);

      // Sanity check to make sure return length is correct
      Assert.expect(newCoordinateXInPixelFromPsp.length > 0);
      Assert.expect(newCoordinateYInPixelFromPsp.length > 0);
      Assert.expect(zHeightArrayInMilimetersFromPsp.length > 0);
      Assert.expect(newCoordinateXInPixelFromPsp.length == newCoordinateYInPixelFromPsp.length);
      Assert.expect(newCoordinateXInPixelFromPsp.length == zHeightArrayInMilimetersFromPsp.length);
      Assert.expect(newCoordinateYInPixelFromPsp.length == zHeightArrayInMilimetersFromPsp.length);

      // PSP Height Outlier checking      
      Map<Integer,Pair<FloatRef,BooleanRef>> zHeightInMilimetersFromPspAfterOutlierRemovalMap = new LinkedHashMap<Integer,Pair<FloatRef,BooleanRef>>();
      float median = StatisticsUtil.median(zHeightArrayInMilimetersFromPsp);          
      float min = median - (0.10f * median);
      float max = median + (0.10f * median);
      boolean allValuesOutsideQuartileRange = true;

      for(int counter = 0; counter < zHeightArrayInMilimetersFromPsp.length; counter++)
      {
        if (isFilterOutlier())
        {
          if ((zHeightArrayInMilimetersFromPsp[counter] < max) && (zHeightArrayInMilimetersFromPsp[counter] > min))
          {
            Pair<FloatRef,BooleanRef> zHeightInfoPair = new Pair<FloatRef,BooleanRef>(new FloatRef(zHeightArrayInMilimetersFromPsp[counter]), 
                                                                                      new BooleanRef(true));
            zHeightInMilimetersFromPspAfterOutlierRemovalMap.put(counter, zHeightInfoPair);
            allValuesOutsideQuartileRange = false;
          }
          else
          {
            Pair<FloatRef,BooleanRef> zHeightInfoPair = new Pair<FloatRef,BooleanRef>(new FloatRef(zHeightArrayInMilimetersFromPsp[counter]), 
                                                                                      new BooleanRef(false));
            zHeightInMilimetersFromPspAfterOutlierRemovalMap.put(counter, zHeightInfoPair);
          }
        }
        else
        {
          Pair<FloatRef,BooleanRef> zHeightInfoPair = new Pair<FloatRef,BooleanRef>(new FloatRef(zHeightArrayInMilimetersFromPsp[counter]), 
                                                                                    new BooleanRef(true));
          zHeightInMilimetersFromPspAfterOutlierRemovalMap.put(counter, zHeightInfoPair);
          allValuesOutsideQuartileRange = false;
        }        
      }
      
      // If all values lies outside of Median, promote Median value as the z-height from PSP engine.
      if (allValuesOutsideQuartileRange)
      {
        Pair<FloatRef,BooleanRef> zHeightInfoPair = zHeightInMilimetersFromPspAfterOutlierRemovalMap.get(zHeightArrayInMilimetersFromPsp.length - 1);
        zHeightInfoPair.getFirst().setValue(median);
        zHeightInfoPair.getSecond().setValue(true);
      }

      int counter = 0;
      for(Map.Entry<String,DoubleRef> entry : opticalPointToPointScan.getPointPanelPositionNameToZHeightInNanometers().entrySet())
      {
        boolean isZHeightValid = zHeightInMilimetersFromPspAfterOutlierRemovalMap.get(counter).getSecond().getValue();        
        float zHeightInMilimetersFromPsp = zHeightInMilimetersFromPspAfterOutlierRemovalMap.get(counter).getFirst().getValue();
        
        float zHeightInMilimetersFromPspBeforeOutlierRemoval = zHeightArrayInMilimetersFromPsp[counter];
        double zHeightInNanometersFromPsp = MathUtil.convertMillimetersToNanometers(zHeightInMilimetersFromPsp);
        double finalZHeightInNanometers = 0;

        if (isZHeightValid)
        {
          if (isReturnRawPspHeight() == false)
            finalZHeightInNanometers = zHeightInNanometersFromPsp + getOffset(moduleEnum);
          else
            finalZHeightInNanometers = zHeightInNanometersFromPsp;
        }
        
        String pointPanelPositionName = entry.getKey();
        
        // Set Z-Height info
        opticalPointToPointScan.setPointPanelPositionZHeight(pointPanelPositionName, finalZHeightInNanometers);
        
        // Set Z-Height Valid
        opticalPointToPointScan.setPointPanelPositionZHeightValid(pointPanelPositionName, isZHeightValid);

        if (newCoordinateXInPixelFromPsp[counter] < 0 || newCoordinateYInPixelFromPsp[counter] < 0)
        {
          Pair<Integer,Integer> pointPositionInPixel = opticalPointToPointScan.getPointPositionInPixel(pointPanelPositionName);
          newCoordinateXInPixelFromPsp[counter] = pointPositionInPixel.getFirst().intValue();
          newCoordinateYInPixelFromPsp[counter] = pointPositionInPixel.getSecond().intValue();
        }

        // Add new coordinate
        opticalPointToPointScan.addPointPanelPositionNameToNewPointPositionInPixel(pointPanelPositionName, 
                                                                                   (int)newCoordinateXInPixelFromPsp[counter], 
                                                                                   (int)newCoordinateYInPixelFromPsp[counter]);

        // PSP height information Logging purpose
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
        {
          if (isZHeightValid)
          {
            System.out.println("PspModuleVersion2: Camera " + opticalPointToPointScan.getOpticalCameraIdEnum().getId() + ": Calculated Z Height from PSP Algorithm for pixel coordinate (" + 
                                        (int)newCoordinateXInPixelFromPsp[counter] + ", " + (int)newCoordinateYInPixelFromPsp[counter] + 
                                        ") is " + zHeightInMilimetersFromPspBeforeOutlierRemoval + " mm / " + MathUtil.convertMillimetersToNanometers(zHeightInMilimetersFromPspBeforeOutlierRemoval) + " nm.");
            System.out.println("PspModuleVersion2: Camera " + opticalPointToPointScan.getOpticalCameraIdEnum().getId() + ": Final Z Height for pixel coordinate (" + 
                                        (int)newCoordinateXInPixelFromPsp[counter] + ", " + (int)newCoordinateYInPixelFromPsp[counter] + 
                                        ") is " + MathUtil.convertNanometersToMillimeters(finalZHeightInNanometers) + " mm / " + finalZHeightInNanometers + " nm.");
          }
          else
          {
            System.out.println("PspModuleVersion2: Camera " + opticalPointToPointScan.getOpticalCameraIdEnum().getId() + ": Calculated Z Height from PSP Algorithm for pixel coordinate (" + 
                                        (int)newCoordinateXInPixelFromPsp[counter] + ", " + (int)newCoordinateYInPixelFromPsp[counter] + 
                                        ") is " + zHeightInMilimetersFromPspBeforeOutlierRemoval + " mm / " + MathUtil.convertMillimetersToNanometers(zHeightInMilimetersFromPspBeforeOutlierRemoval) + " nm.");
            System.out.println("PspModuleVersion2: Camera " + opticalPointToPointScan.getOpticalCameraIdEnum().getId() + ": No final Z Height info for pixel coordinate (" + 
                                        (int)newCoordinateXInPixelFromPsp[counter] + ", " + (int)newCoordinateYInPixelFromPsp[counter] + ")");
          }
        }                
        
        ++counter;
      }
      
      // Calculate Average Z-Height
      opticalPointToPointScan.calculateAverageZHeightInNanometer();
      
      // Clean up
      zHeightInMilimetersFromPspAfterOutlierRemovalMap.clear();
    }
    else
        System.out.println("No Height Map available.");
  }

  /**
   * @author Cheah Lee Herng 
   */
  protected OpticalInspectionData setupOfflineHeightMapInspectionData(OpticalPointToPointScan opticalPointToPointScan, String projectName) 
  {
    Assert.expect(opticalPointToPointScan != null);
    Assert.expect(projectName != null);    
    
    StagePosition stagePosition = opticalPointToPointScan.getPointToPointStagePositionInNanoMeters();
    PspModuleEnum pspModuleEnum = getPspModuleEnum(opticalPointToPointScan.getOpticalCameraIdEnum());
    
    OpticalInspectionData opticalInspectionData = new OpticalInspectionData();
    opticalInspectionData.setPspModuleEnum(pspModuleEnum);

    if (pspModuleEnum.equals(PspModuleEnum.FRONT))
      opticalInspectionData.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_1);
    else
      opticalInspectionData.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_2);
    
    String opticalImageName = opticalPointToPointScan.getPointPositionName();

    // Setup stage position
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_ONE, FileName.getOfflineOpticalImageFullPath(projectName, opticalImageName, 1));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_TWO, FileName.getOfflineOpticalImageFullPath(projectName, opticalImageName, 2));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_THREE, FileName.getOfflineOpticalImageFullPath(projectName, opticalImageName, 3));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_FOUR, FileName.getOfflineOpticalImageFullPath(projectName, opticalImageName, 4));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_FIVE, FileName.getOfflineOpticalImageFullPath(projectName, opticalImageName, 5));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_SIX, FileName.getOfflineOpticalImageFullPath(projectName, opticalImageName, 6));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_SEVEN, FileName.getOfflineOpticalImageFullPath(projectName, opticalImageName, 7));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_EIGHT, FileName.getOfflineOpticalImageFullPath(projectName, opticalImageName, 8));
    
    return opticalInspectionData;
  }

  /**
   * @author Cheah Lee Herng 
   */
  protected OpticalInspectionData setupAlignmentHeightMapInspectionData(OpticalPointToPointScan opticalPointToPointScan)
  {
    Assert.expect(opticalPointToPointScan != null);    
    
    StagePosition stagePosition = opticalPointToPointScan.getPointToPointStagePositionInNanoMeters();
    PspModuleEnum pspModuleEnum = getPspModuleEnum(opticalPointToPointScan.getOpticalCameraIdEnum());
    
    OpticalInspectionData opticalInspectionData = new OpticalInspectionData();
    opticalInspectionData.setPspModuleEnum(pspModuleEnum);
    opticalInspectionData.setForceTrigger(false);
    opticalInspectionData.setOpticalCalibrationProfileEnum(opticalPointToPointScan.getOpticalCalibrationProfileEnum());
    opticalInspectionData.setReturnActualHeightMap(opticalPointToPointScan.getReturnActualHeightMap());
    
    String opticalImageName = opticalPointToPointScan.getPointPositionName();

    // Setup stage position
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_ONE,   FileName.getAlignmentOpticalImageFullPath(opticalImageName, 1));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_TWO,   FileName.getAlignmentOpticalImageFullPath(opticalImageName, 2));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_THREE, FileName.getAlignmentOpticalImageFullPath(opticalImageName, 3));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_FOUR,  FileName.getAlignmentOpticalImageFullPath(opticalImageName, 4));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_FIVE,  FileName.getAlignmentOpticalImageFullPath(opticalImageName, 5));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_SIX,   FileName.getAlignmentOpticalImageFullPath(opticalImageName, 6));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_SEVEN, FileName.getAlignmentOpticalImageFullPath(opticalImageName, 7));
    opticalInspectionData.addImageFullPath(stagePosition, OpticalShiftNameEnum.SHIFT_EIGHT, FileName.getAlignmentOpticalImageFullPath(opticalImageName, 8));
    
    return opticalInspectionData;
  }
}
