package com.axi.v810.business.imageAcquisition;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import java.util.concurrent.locks.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 * @author Matt Wharton
 */
public class ReconstructedImagesManager
{
  private static Config _config = Config.getInstance();

  // Kok Chun, Tan - XCR-3114 - make sure it is long data type before do any calculation
  private static final long _MEGABYTE_TO_BYTE = 1024 * 1024;
  protected static final long _MAX_IMAGE_MEMORY_USAGE_BEFORE_IRP_PAUSE =
      _config.getIntValue(SoftwareConfigEnum.IMAGE_PAUSE_IRP_THRESHOLD_IN_MEGABYTES) * _MEGABYTE_TO_BYTE;
  protected static final long _MAX_IMAGE_MEMORY_USAGE_BEFORE_IRP_RESUME =
      _config.getIntValue(SoftwareConfigEnum.IMAGE_RESUME_IRP_THRESHOLD_IN_MEGABYTES) * _MEGABYTE_TO_BYTE;
  protected static final float _MAX_IMAGE_MEMORY_USAGE_BEFORE_STOP_LOADING =
      _config.getIntValue(SoftwareConfigEnum.IMAGE_OFFLINE_INSPECTION_LOAD_LIMIT_IN_MEGABYTES);

  // Active images are those which are currently being inspected or scheduled for execution.
  private static AtomicLong _memoryConsumedByActiveReconstructedImages = new AtomicLong(0L);

  private static final boolean _DROP_INSPECTION_IMAGES =
      _config.getBooleanValue(SoftwareConfigEnum.DROP_INSPECTION_IMAGES);

  private ReconstructedImagesProducer _reconstructedImagesProducer = null;

  // The image queue is monitored by this lock
  protected Lock _imageQueueLock = new ReentrantLock();
  protected Condition _imageQueueReadyForMoreImagesCondition = _imageQueueLock.newCondition();
  protected Condition _imageQueueNotEmptyCondition = _imageQueueLock.newCondition();
  protected Condition _lightestImageQueueNotEmptyCondition = _imageQueueLock.newCondition();
  protected Condition _reReconstructedImageQueueNotEmptyCondition = _imageQueueLock.newCondition();

  // These variables are controlled by _imageQueueLock. Do not attempt access them without having a lock to _imageQueueLock.
  protected Queue<ReconstructionRegion> _reconstructedRegionsQueue = new LinkedList<ReconstructionRegion>();
  protected ReconstructedImagesMemoryManager _reconstructedImagesMemoryManager = ReconstructedImagesMemoryManager.getInstance();

  // state flags
  private boolean _pauseSentToIrps = false;
  private boolean _allImagesAreAcquired = false;

  // This queue represents a list of the ReconstructionRegions that we're waiting for lightest images for.
  private BlockingQueue<ReconstructionRegion> _lightestImagesRequestQueue = new LinkedBlockingQueue<ReconstructionRegion>();
  // This map maps ReconstructionRegions to their completed "lightest images".
  private ConcurrentMap<ReconstructionRegion, ReconstructedImages> _reconstructionRegionToCompletedLightestImagesMap =
      new ConcurrentHashMap<ReconstructionRegion, ReconstructedImages>();

  // This queue represents a list of the ReconstructionRegions that we're waiting for re-reconstructed images for.
  private BlockingQueue<ReconstructionRegion> _reReconstructRequestQueue = new LinkedBlockingQueue<ReconstructionRegion>();
  // This map maps ReconstructionRegions to their completed re-reconstructed images.
  private ConcurrentMap<ReconstructionRegion, ReconstructedImages> _reconstructionRegionToCompletedReRecontructedImagesMap =
      new ConcurrentHashMap<ReconstructionRegion, ReconstructedImages>();

  // These regions are not counted in _memoryConsumedByActiveReconstructedImages.
  private Set<ReconstructionRegion> _uncountedReconstructionRegions = Collections.synchronizedSet(new HashSet<ReconstructionRegion>());

  // this contains the requested regions, other regions can be ignored
  private Collection<Integer> _requestedRegionIds;
  // This is to prevent _requestedRegionIds from being nullified, when it is in use in putReconstructedImages(...)
  // Added by Seng-Yew Lim for CR32183
  //   While _requestedRegionIds is used in putReconstructedImages(...), user might abort.
  //   When abort() is called and _requestedRegionIds is set to null, it will cause NullPointerException assert in putReconstructedImages(...).
  protected Lock _requestedRegionIdsLock = new ReentrantLock();

  private AtomicInteger _numberOfRegularReconstructedImagesImagesReceived = new AtomicInteger(0);
  private boolean _aborted = false;

  /**
   * @author Roy Williams
   */
  protected ReconstructedImagesManager(ReconstructedImagesProducer reconstructedImagesProducer)
  {
    _reconstructedImagesProducer = reconstructedImagesProducer;
  }

  /**
   * This method should be called before clients start trying to acquire images or extract acquired images.
   * This ensures that the queue is properly cleaned out and the abort flag is cleared.
   *
   * @author Matt Wharton
   */
  public void initializeReconstructedImagesList() throws DatastoreException
  {
    clearReconstructedImagesList();
  }

  /**
   * @author Peter Esbensen
   */
  private void initializeRequestedRegions()
  {
    _requestedRegionIds = null;
  }

  /**
   * @author Matt Wharton
   */
  public void initializeLightestImagesQueue()
  {
    Assert.expect(_lightestImagesRequestQueue != null);
    Assert.expect(_reconstructionRegionToCompletedLightestImagesMap != null);

    _lightestImagesRequestQueue.clear();
    _reconstructionRegionToCompletedLightestImagesMap.clear();
  }

  /**
   * @author Matt Wharton
   */
  public void initializeReReconstructedImagesQueue()
  {
    Assert.expect(_reReconstructRequestQueue != null);
    Assert.expect(_reconstructionRegionToCompletedReRecontructedImagesMap != null);

    for (ReconstructedImages reconstructedImages : _reconstructionRegionToCompletedReRecontructedImagesMap.values())
    {
      reconstructedImages.decrementReferenceCount();
    }

    _reReconstructRequestQueue.clear();
    _reconstructionRegionToCompletedReRecontructedImagesMap.clear();
  }

  /**
   * @author Matt Wharton
   */
  void initialize() throws DatastoreException
  {
    // RMS: Clear the image mem count since we are starting fresh.
    _memoryConsumedByActiveReconstructedImages.set(0l);

    _numberOfRegularReconstructedImagesImagesReceived.set(0);
    initializeReconstructedImagesList();
    initializeReReconstructedImagesQueue();
    initializeLightestImagesQueue();
    initializeRequestedRegions();
    _uncountedReconstructionRegions.clear();
    _pauseSentToIrps = false;
    _aborted = false;
  }

  /**
   * @author Scott Richardson
   */
  public ReconstructedImagesMemoryManager getReconstructedImagesMemoryManager()
  {
    Assert.expect(_reconstructedImagesMemoryManager != null);

    return _reconstructedImagesMemoryManager;
  }

  /**
   * @author Matt Wharton
   */
  protected void clearReconstructedImagesList() throws DatastoreException
  {
    Assert.expect(_reconstructedRegionsQueue != null);
    Assert.expect(_imageQueueLock != null);

    _imageQueueLock.lock();
    _reconstructedRegionsQueue.clear();
    _reconstructedImagesMemoryManager.initialize();
    setAllImagesAreAcquiredFlag(false);
    _imageQueueLock.unlock();
  }

  /**
   * Attempts to place a ReconstructedImages object on the image queue.
   *
   * If this image pushes us beyond the maximum memory threshold,
   *   (online) signal the IRPs to pause and return. [non-blocking]
   *   (offline) block until the memory drops below the resume threshold. [blocking]
   *
   * @author Roy Williams
   * @author Matt Wharton
   */
  public void putReconstructedImages(ReconstructedImages reconstructedImages) throws XrayTesterException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(reconstructedImages.getNumberOfSlices() > 0);
    Assert.expect(_imageQueueLock != null);
    Assert.expect(_imageQueueNotEmptyCondition != null);
    Assert.expect(_lightestImagesRequestQueue != null);

    // if aborted or TestExecution declares that it has seen enough (by setting
    // _requestedRegionIds to nul) then do not accept any more images.   Also,
    // TestExecution will allow cals to run at end of inspectPanelForTestDevelopment
    // and they will also set _requestedRegionIds = null.  Must allow a null
    // _requestedRegionIds to prevent further putReconstructedImages(see below).
    if (_aborted || _requestedRegionIds == null)
      return;

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    ReconstructionRegionTypeEnum reconstructionRegionTypeEnum = reconstructionRegion.getReconstructionRegionTypeEnum();

    // If we're set up to drop inspection images and this is an inspection image, just mark complete and bail.
    if (_DROP_INSPECTION_IMAGES && reconstructionRegionTypeEnum.equals(ReconstructionRegionTypeEnum.INSPECTION))
    {
      if (_aborted == false)
      {
        // Bee Hoon - Trigger consumer region even the dropInspectionImages is set to true
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION)== false ||
            Config.getInstance().getBooleanValue(SoftwareConfigEnum.OFFLINE_RECONSTRUCTION))
        {
          reconstructionRegion.triggerConsumerRegion();
        }
        
        _reconstructedImagesProducer.reconstructionRegionComplete(reconstructionRegion.getTestSubProgram(),
                                                                  reconstructionRegion,
                                                                  false);
      }
      return;
    }

    //Assert.expect(_memoryConsumedByActiveReconstructedImages.get() >= 0L);
    if (_memoryConsumedByActiveReconstructedImages.get() < 0L)
      System.out.println("ReconstructionImagesManager: putReconstructedImages: Active Region size is " + _memoryConsumedByActiveReconstructedImages.get());
    
    NativeMemoryMonitor nativeMemoryMonitor = NativeMemoryMonitor.getInstance();

    _requestedRegionIdsLock.lock();
    if (_aborted || _requestedRegionIds == null)
    {
      _requestedRegionIdsLock.unlock();
      return;
    }
    _imageQueueLock.lock();
    try
    {
      // if this is a region that wasn't requested, just mark it complete and be done with it
      Assert.expect(_requestedRegionIds != null);
      if (_requestedRegionIds.contains(reconstructionRegion.getRegionId()) == false)
      {
        if (_aborted == false)
          _reconstructedImagesProducer.reconstructionRegionComplete(reconstructionRegion.getTestSubProgram(),
                                                                    reconstructionRegion, false);
        return;
      }

      // See if the incoming image was a re-reconstruct request.
      if (_reReconstructRequestQueue.contains(reconstructionRegion))
      {
        /**
         * @todo RMS Making the call here doesn't bode well with OO...
         * NativeMeoryMonitor just allocated more memory for the new
         * reconstructedImages, we need to cope with that.
         */
        _reconstructedImagesMemoryManager.cacheReconstructedImagesToDiskAsNecessary();

        // Add the re-reconstructed images to the "completed re-reconstructed images table".
        // This call will also remove the reconstruction region from the request queue.
        addCompletedReReconstructedImages(reconstructedImages);

        // we're not counting the memory for this region
        _uncountedReconstructionRegions.add(reconstructionRegion);

        _reReconstructedImageQueueNotEmptyCondition.signalAll();
      }
      // See if the incoming image was a special "lightest image" request.
      else if (_lightestImagesRequestQueue.contains(reconstructionRegion))
      {
        /**
         * @todo RMS Making the call here doesn't bode well with OO...
         * NativeMeoryMonitor just allocated more memory for the new
         * reconstructedImages, we need to cope with that.
         */
        _reconstructedImagesMemoryManager.cacheReconstructedImagesToDiskAsNecessary();

        // Add the lightest images to the "completed lightest images table".
        // This call will also remove the reconstruction region from the request queue.
        addCompletedLightestImages(reconstructedImages);

        // we're not counting the memory for this region
        _uncountedReconstructionRegions.add(reconstructionRegion);

        _lightestImageQueueNotEmptyCondition.signalAll();
      }
      else
      {
        // This is just a regular set of reconstructed images.  Process normally.

        // Add our new record and notify any gets that might be waiting for it.
        if (_numberOfRegularReconstructedImagesImagesReceived.incrementAndGet() == _requestedRegionIds.size())
        {
          setAllImagesAreAcquiredFlag(true);
        }
        _reconstructedRegionsQueue.add(reconstructionRegion);
        long sizeOfImages = _reconstructedImagesMemoryManager.putReconstructedImages(reconstructionRegion,
                                                                                    ImageTypeEnum.AVERAGE,
                                                                                    reconstructedImages);
        _imageQueueNotEmptyCondition.signalAll();
        _memoryConsumedByActiveReconstructedImages.addAndGet(sizeOfImages);

        // Determine if we are at our cap and need to pause.
        if (_reconstructedImagesProducer.getImageAcquisitionMode().equals(ImageAcquisitionModeEnum.OFFLINE) == false &&
            _memoryConsumedByActiveReconstructedImages.get() > _MAX_IMAGE_MEMORY_USAGE_BEFORE_IRP_PAUSE)
        {
          sendPauseToIrps();
        }
      }
    }
    finally
    {
      _requestedRegionIdsLock.unlock();
      _imageQueueLock.unlock();
    }

    // Determine if we are at our cap and need to pause.
    if (nativeMemoryMonitor.getTotalUsedMemoryInMegabytes() >= _MAX_IMAGE_MEMORY_USAGE_BEFORE_STOP_LOADING &&
        _reconstructedImagesProducer.getImageAcquisitionMode().equals(ImageAcquisitionModeEnum.OFFLINE) == true)
    {
      waitUntilEnoughMemoryIsAvailable();
    }
  }

  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  public void freeReconstructedImages(ReconstructionRegion reconstructionRegion) throws XrayTesterException
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(_reconstructedImagesMemoryManager != null);

    Collection<ImageTypeEnum> allTypes = ImageTypeEnum.getAllTypes();
    for (ImageTypeEnum type : allTypes)
    {
      if (_reconstructedImagesMemoryManager.hasReconstructedImages(reconstructionRegion, type))
      {
        _imageQueueLock.lock();
        try
        {
          boolean regionIsNotActive = true;
          long sizeOfFreedImage = 0L;
          synchronized (_reconstructedImagesMemoryManager)
          {
            regionIsNotActive = _reconstructedImagesMemoryManager.areImagesForRegionFinishedInspection(reconstructionRegion, type);
            sizeOfFreedImage = _reconstructedImagesMemoryManager.freeReconstructedImages(reconstructionRegion, type);
          }
          if (regionIsNotActive == false)
          {
            // If there is enough memory available (as per the memory manager), send a resume to the IRPs.
            if (_memoryConsumedByActiveReconstructedImages.addAndGet( -sizeOfFreedImage) <= _MAX_IMAGE_MEMORY_USAGE_BEFORE_IRP_RESUME)
            {
              sendResumeToIrps();
            }
            
            if(_memoryConsumedByActiveReconstructedImages.get() < 0L)
              System.out.println("ReconstructionImagesManager: freeReconstructedImages: Active Region size is " + _memoryConsumedByActiveReconstructedImages.get());
            //Assert.expect(_memoryConsumedByActiveReconstructedImages.get() >= 0L, "Active Region size is: " + _memoryConsumedByActiveReconstructedImages.get());
          }
        }
        finally
        {
          _imageQueueLock.unlock();
        }
      }
    }
  }

  /**
   * @author Patrick Lacz
   */
  public void checkForIRPResumeCondition() throws XrayTesterException
  {
    if (_pauseSentToIrps == true)
    {
      if (_memoryConsumedByActiveReconstructedImages.get() <= _MAX_IMAGE_MEMORY_USAGE_BEFORE_IRP_RESUME)
      {
        sendResumeToIrps();
      }
    }
  }

  /**
   * @author Patrick Lacz
   */
  public void markRegionAsFinishedInspection(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);

    long sizeInMemory = _reconstructedImagesMemoryManager.getSizeInMemory(reconstructionRegion, ImageTypeEnum.AVERAGE);
    _memoryConsumedByActiveReconstructedImages.addAndGet(-sizeInMemory);

    _reconstructedImagesMemoryManager.markRegionAsFinishedInspection(reconstructionRegion, ImageTypeEnum.AVERAGE);
  }

  /**
   * @author Roy Williams
   */
  public static long getMemoryConsumedByActiveReconstructedImages()
  {
    return _memoryConsumedByActiveReconstructedImages.get();
  }

  /**
   * @author Roy Williams
   */
  private void awaitResumeSentToIrps()
  {
    while (pauseSentToIrps() == true && _aborted == false)
    {
      try
      {
        _imageQueueLock.lock();
        _imageQueueReadyForMoreImagesCondition.awaitNanos(10000);
      }
      catch (InterruptedException ex)
      {
        // Do nothing...
      }
      finally
      {
        _imageQueueLock.unlock();
      }
    }
  }

  /**
   * @author Patrick Lacz
   * @author Laura Cormos
   */
  private void waitUntilEnoughMemoryIsAvailable()
  {
    NativeMemoryMonitor nativeMemoryMonitor = NativeMemoryMonitor.getInstance();
    while (_aborted == false &&
           nativeMemoryMonitor.getTotalUsedMemoryInMegabytes() > _MAX_IMAGE_MEMORY_USAGE_BEFORE_STOP_LOADING)
    {
      try
      {
        _imageQueueLock.lock();
        _imageQueueReadyForMoreImagesCondition.awaitNanos(20000000);
      }
      catch (InterruptedException ex)
      {
        // do nothing
      }
      finally
      {
        _imageQueueLock.unlock();
      }
    }
  }

  /**
   * For the offline mode, the 'pause' is to block the current thread. See the
   * This method is synchronized because it modifies _pauseSentToIrps (I think. - pwl)
   *
   * @author Roy Williams
   * @author Patrick Lacz
   */
  private synchronized void sendPauseToIrps() throws XrayTesterException
  {
    if (_pauseSentToIrps)
      return;

    pauseImageAcquisition();
    _pauseSentToIrps = true;
  }
  /**
   * @author Roy Williams
   */
  protected synchronized boolean pauseSentToIrps()
  {
    return _pauseSentToIrps;
  }

  /**
   * @author Roy Williams
   */
  private synchronized void sendResumeToIrps() throws XrayTesterException
  {
    if (_pauseSentToIrps == false)
      return;

    resumeImageAcquisition();
    _pauseSentToIrps = false;
    _imageQueueReadyForMoreImagesCondition.signalAll();
  }

  /**
   * Removes the first image in the image queue.  If there are no images available, this method
   * blocks until images are ready.
   *
   * NOTE: This method could possibly return 'null' if an abort is called while this method is running.
   * As such, this method takes a BooleanRef parameter that is set to true if this occurs.  Any client code
   * which calls this method should check the value of the BooleanRef before trying to use the returned
   * ReconstructedImages object.
   *
   * @author Matt Wharton
   * @author Roy Williams
   * @author Patrick Lacz
   */
  public ReconstructedImages getReconstructedImages(BooleanRef noImagesReturned) throws XrayTesterException
  {
    Assert.expect(noImagesReturned != null);

    Assert.expect(_imageQueueLock != null);
    Assert.expect(_reconstructedRegionsQueue != null);
    Assert.expect(_imageQueueReadyForMoreImagesCondition != null);

    noImagesReturned.setValue(false);

    ReconstructedImages reconstructedImages = null;

    _imageQueueLock.lock();
    try
    {
      while (_reconstructedRegionsQueue.isEmpty())
      {
        try
        {
          _imageQueueNotEmptyCondition.await(200, TimeUnit.MILLISECONDS);
        }
        catch (InterruptedException ex1)
        {
          // Do nothing ...
        }

        // If all images have been acquired and the queue is still empty, we must be done.
        if (_aborted || (areAllImagesAcquired() && _reconstructedRegionsQueue.isEmpty()))
        {
          noImagesReturned.setValue(true);
          return null;
        }

        checkForIRPResumeCondition();
      }

      Assert.expect(_reconstructedRegionsQueue.isEmpty() == false);
      ReconstructionRegion reconstructionRegion = _reconstructedRegionsQueue.poll();
      reconstructedImages = _reconstructedImagesMemoryManager.getReconstructedImages(reconstructionRegion,
                                                                                     ImageTypeEnum.AVERAGE);
    }
    finally
    {
      _imageQueueLock.unlock();
    }

    Assert.expect(reconstructedImages != null);
    Assert.expect(reconstructedImages.getNumberOfSlices() > 0);
    return reconstructedImages;
  }

  /**
   * @author Roy Williams
   */
  public int size()
  {
    return _reconstructedRegionsQueue.size();
  }

  /**
   * @author Roy Williams
   */
  public void pauseImageAcquisition() throws XrayTesterException
  {
    _reconstructedImagesProducer.pauseImageAcquisition();

  }

  /**
   * @author Roy Williams
   */
  public void resumeImageAcquisition() throws XrayTesterException
  {
    _reconstructedImagesProducer.resumeImageAcquisition();
  }

  /**
   * @author Peter Esbensen
   */
  void setRequestedRegions(Collection<ReconstructionRegion> requestedRegions)
  {
    Assert.expect(requestedRegions != null);

    _requestedRegionIds = new ArrayList<Integer>();
    addRequestedRegions(requestedRegions);
  }

  /**
   * @author Roy Williams
   */
  void addRequestedRegions(Collection<ReconstructionRegion> requestedRegions)
  {
    Assert.expect(requestedRegions != null);

    Assert.expect(_requestedRegionIds != null); // this should have been reset at the end of the last inspection
    for (ReconstructionRegion region : requestedRegions)
    {
      _requestedRegionIds.add(region.getRegionId());
    }
  }

  /**
   * @author Patrick Lacz
   */
  public boolean areAllImagesAcquired()
  {
    boolean areAllImagesAcquired;
    _imageQueueLock.lock();
    areAllImagesAcquired = _allImagesAreAcquired;
    _imageQueueLock.unlock();
    return areAllImagesAcquired;
  }

  /**
   * @author Patrick Lacz
   */
/** @todo wpd - Roy  - figure out what this really does and why it is not called with true for online inspections */
  public void setAllImagesAreAcquiredFlag(boolean state)
  {
    Assert.expect(_imageQueueLock != null);

    _imageQueueLock.lock();
    _allImagesAreAcquired = state;
    _imageQueueLock.unlock();
  }

  /**
   * Images that need to be handled for any reason (e.g.
   * @author Roy Williams
   */
  public void setHighPriorityForImagesFromReconstructionRegion(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
  }

  /**
   * Adds the specified reconstruction region to our queue of images that we want lightest images for.
   *
   * @author Matt Wharton
   */
  void addLightestImagesRequest(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(_lightestImagesRequestQueue != null);

    _lightestImagesRequestQueue.add(reconstructionRegion);
  }

  /**
   * Adds the specified reconstruction region to our queue of images that we want re-reconstructed images for.
   *
   * @author Matt Wharton
   */
  void addReReconstructedImagesRequest(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(_reReconstructRequestQueue != null);

    _reReconstructRequestQueue.add(reconstructionRegion);
  }

  /**
   * Adds the specified reconstructed lightest images to the table of completed lightest images.
   * Also removes the associated ReconstructionRegion from the request queue.
   *
   * @author Matt Wharton
   */
  private void addCompletedLightestImages(ReconstructedImages reconstructedLightestImages)
  {
    Assert.expect(reconstructedLightestImages != null);
    Assert.expect(_lightestImagesRequestQueue != null);
    Assert.expect(_reconstructionRegionToCompletedLightestImagesMap != null);

    ReconstructionRegion reconstructionRegion = reconstructedLightestImages.getReconstructionRegion();
    reconstructedLightestImages.incrementReferenceCount();
    _reconstructionRegionToCompletedLightestImagesMap.put(reconstructionRegion, reconstructedLightestImages);
    _lightestImagesRequestQueue.remove(reconstructionRegion);
  }


  /**
   * Adds the specified reconstructed lightest images to the table of completed lightest images.
   * Also removes the associated ReconstructionRegion from the request queue.
   *
   * @author Matt Wharton
   */
  private void addCompletedReReconstructedImages(ReconstructedImages reReconstructedImages)
  {
    Assert.expect(reReconstructedImages != null);
    Assert.expect(_reReconstructRequestQueue != null);
    Assert.expect(_reconstructionRegionToCompletedReRecontructedImagesMap != null);

    ReconstructionRegion reconstructionRegion = reReconstructedImages.getReconstructionRegion();
    reReconstructedImages.incrementReferenceCount();
    _reconstructionRegionToCompletedReRecontructedImagesMap.put(reconstructionRegion, reReconstructedImages);
    _reReconstructRequestQueue.remove(reconstructionRegion);
  }

  /**
   * Waits for the lightest images for the specified reconstruction region.  If acquisition is paused
   * or the inspection is aborted, this method will return null and the specified BooleanRef will indicate that
   * the images are not available.
   *
   * @author Matt Wharton
   */
  public ReconstructedImages waitForLightestImages(ReconstructionRegion reconstructionRegion,
                                                   BooleanRef lightestImagesAvailable)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(lightestImagesAvailable != null);
    Assert.expect(_reconstructionRegionToCompletedLightestImagesMap != null);

    // Wait until the lightest images we want are available.
    while (_reconstructionRegionToCompletedLightestImagesMap.containsKey(reconstructionRegion) == false)
    {
      // If an abort was requested, stop waiting.
      if (_aborted)
      {
        break;
      }

      // Yield to other threads.
      try
      {
        _imageQueueLock.lock();
        _lightestImageQueueNotEmptyCondition.awaitNanos(20000000);
      }
      catch (InterruptedException iex)
      {
        // Do nothing ...
      }
      finally
      {
        _imageQueueLock.unlock();
      }
    }

    // Try to get the entry from the map.  If, at this point, there is still no table entry for the
    // reconstruction region, we conclude that lightest images are NOT available.
    ReconstructedImages lightestImages = _reconstructionRegionToCompletedLightestImagesMap.remove(reconstructionRegion);
    lightestImagesAvailable.setValue(lightestImages != null);

    return lightestImages;
  }

  /**
   * Waits for the re-reconstructed images for the specified reconstruction region.  If acquisition is paused
   * or the inspection is aborted, this method will return null and the specified BooleanRef will indicate that
   * the images are not available.
   *
   * @author Matt Wharton
   */
  public ReconstructedImages waitForReReconstructedImages(ReconstructionRegion reconstructionRegion,
                                                          BooleanRef reReconstructedImagesAvailable)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(reReconstructedImagesAvailable != null);
    Assert.expect(_reconstructionRegionToCompletedReRecontructedImagesMap != null);

    // Wait until the lightest images we want are available.
    while (_reconstructionRegionToCompletedReRecontructedImagesMap.containsKey(reconstructionRegion) == false)
    {
      // If an abort was requested, stop waiting.
      if (_aborted)
      {
        break;
      }

      // Yield to other threads.
      try
      {
        _imageQueueLock.lock();
        _reReconstructedImageQueueNotEmptyCondition.awaitNanos(20000000);
      }
      catch (InterruptedException iex)
      {
        // Do nothing ...
      }
      finally
      {
        _imageQueueLock.unlock();
      }
    }

    // Try to get the entry from the map.  If, at this point, there is still no table entry for the
    // reconstruction region, we conclude that re-reconstructed images are NOT available.
    ReconstructedImages reReconstructedImages = _reconstructionRegionToCompletedReRecontructedImagesMap.remove(reconstructionRegion);
    reReconstructedImagesAvailable.setValue(reReconstructedImages != null);

    return reReconstructedImages;
  }

  /**
   * @author Roy Williams
   */
  public boolean isReconstructionRegionActive(int reconstructionRegionId)
  {
    _imageQueueLock.lock();
    try
    {
      for (ReconstructionRegion reconstructionRegion : _reconstructedRegionsQueue)
      {
        if (reconstructionRegion.getRegionId() == reconstructionRegionId)
          return true;
      }
    }
    finally
    {
      _imageQueueLock.unlock();
    }
    return false;
  }

  /**
   * @author George A. David
   */
  void abort()
  {
    _aborted = true;
    _requestedRegionIdsLock.lock();
    _requestedRegionIds = null;
    _requestedRegionIdsLock.unlock();

    try
    {
      _imageQueueLock.lock();
      _imageQueueNotEmptyCondition.signalAll();
      _imageQueueReadyForMoreImagesCondition.signalAll();
    }
    finally
    {
      _imageQueueLock.unlock();
    }
  }
}
