package com.axi.v810.business.imageAcquisition;

import java.awt.geom.*;
import java.io.*;
import java.net.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.communication.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class ImagingChainProgramGenerator
{
  private List<ScanStrategyEnum> _scanStrategies = new ArrayList<ScanStrategyEnum>();
  private ImageAcquisitionModeEnum _acquisitionMode = ImageAcquisitionModeEnum.NOT_IN_USE;
  private Set<ImageReconstructionEngine> _imageReconstructionEnginesInUse;

  private Map<ReconstructionRegion, Pair<ImageGroup, AlignmentScanPathSubSection>> _alignmentRegionInfoMap;
  private List<ImageGroup> _imageGroups;   // and don't want to assume position in _imageGroups

  private static Map<ImageReconstructionEngineEnum, ImageReconstructionEngine> _idToImageReconstructionEngineMap = new HashMap<ImageReconstructionEngineEnum, ImageReconstructionEngine>();
  private static List<ImageReconstructionEngine> _imageReconstructionEngines = new ArrayList<ImageReconstructionEngine>();
//  private static int _imageableWidthOfAnyCamera = -1;
  private static XrayCameraArray _xrayCameraArray;
  private static List<AbstractXrayCamera> _xRayCameras;

  // Hard limits for step size.
  private static int _minStepSizeLimitInNanometers = (int)MathUtil.convertMilsToNanoMeters((double)80); // why 80 mils??

  public static int _maxStepSizeLimitInNanometers = -1;
  // Default mean step size.
  private static int _defaultMeanStepSizeLimitInNanometers = -1;
  private static double _ditherMaxInNanometers = -1;
  private static double _stepOverridePercentage = -1;
  // XCR1529, New Scan Route, khang-wah.chnee
  private static Map<Integer, Pair<Integer, String>> _xLocationToPassCodeMap = new HashMap<Integer, Pair<Integer, String>>();
  
  // Disable step size optimization based on panel thickness instead of using fixed maximum panel thickness allowed
  private static boolean _disableStepSizeOptimization = false;

  // Debug around components or regions.
  private static boolean _debug = false;
  private static List<String>               _debugComponents = null;
  private static List<ReconstructionRegion> _debugRegions    = null;
  
  // Real Time PSH Constant
  final int DISTANCE_THRESHOLD_IN_MILS = 1500;
  private final int _REGION_TO_MARK_COMPLETED_FOR_MOTION_BREAKPOINT = 0;
  final double DISTANGE_THRESHOLD_IN_NM   = MathUtil.convertMilsToNanoMeters(DISTANCE_THRESHOLD_IN_MILS);

  
  //REGION_NOT_IMAGED investigation
  static private Map<Integer, Point2D> _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap = new TreeMap<Integer, Point2D>();
  static private Map<Integer, Point2D> _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap = new TreeMap<Integer, Point2D>();
  static private Map<Integer, Point2D> _systemFiducialXYLocationInPixelsMap = new TreeMap<Integer, Point2D>();
  static private Map<Integer, Point2D> _systemFiducialXYLocationInPixelsHighMagMap = new TreeMap<Integer, Point2D>();
  static private Map<Integer, Point2D> _xyPosOfCamerasInXraySpotCoordPixelsMap = new TreeMap<Integer, Point2D>();
  static private Map<Integer, Point2D> _xyPosOfCamerasInXraySpotCoordPixelsHighMagMap = new TreeMap<Integer, Point2D>();
          
  /**
   * @author Roy Williams
   */
  public ImagingChainProgramGenerator(ImageAcquisitionModeEnum acquisitionMode,
                                      List<ScanStrategyEnum> strategies,
      double stepOverridePercentage)
  {
    Assert.expect(acquisitionMode != null);
    Assert.expect(stepOverridePercentage >= 0);

//    _debugComponents = new ArrayList<String>();
//    _debugComponents.add("U59");
//    _debugComponents.add("U17");
//    _debugRegions = new ArrayList<ReconstructionRegion>();

    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    try
    {
      if (Config.isDeveloperSystemEnabled())
      {
        if (DeveloperDebugConfigEnum.isConfigEnumExists(DeveloperDebugConfigEnum.IMAGING_CHAIN_PROGRAM_GENERATOR))
          _debug = Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.IMAGING_CHAIN_PROGRAM_GENERATOR);
      }
    }
    catch (Exception ex)
    {
        //do nothing
    }    
    
    initialize();

    _acquisitionMode = acquisitionMode;
    _stepOverridePercentage = stepOverridePercentage;

//    _imageableWidthOfAnyCamera = _xrayCameraArray.getImageableWidthInNanometers(MagnificationEnum.MIN_SLICE_HEIGHT);

    // Initialize the strategies to be used.
    for (ScanStrategyEnum strategyEnum : strategies)
      _scanStrategies.add(strategyEnum);

    // Calculate step size(s) - calibrations want to use special step sizes.
    calculateMaxStepSizeInNanometers();
    
    // Determine if debug output is allowed
    // Put this in constructor, so that each time the config is updated, it will update this _debug flag correctly.
    try
    {
      if (Config.isDeveloperSystemEnabled())
      {
        if (DeveloperDebugConfigEnum.isConfigEnumExists(DeveloperDebugConfigEnum.IMAGING_CHAIN_PROGRAM_GENERATOR))
          _debug = Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.IMAGING_CHAIN_PROGRAM_GENERATOR);
      }
    }
    catch (Exception ex)
    {
      // Do nothing
    }
  }

  /**
   * @author Roy Williams
   */
  public void initialize()
  {
    if (_xrayCameraArray == null)
      _xrayCameraArray = XrayCameraArray.getInstance();
    if (_xRayCameras == null)
      _xRayCameras = XrayCameraArray.getCameras();
    // Turn on some debugging flags
    if (_debug)
    {
      RescanShadedRegionsBasedUponDripSourceComponent.setDebug(true);
      ScanStrategy.setDebug(true);
    }

  }

  /**
   * @author Roy Williams
   */
  public boolean useShadingCompensation()
  {
    for (ScanStrategyEnum strategy : _scanStrategies)
    {
      if (ScanStrategyEnum.NO_SHADING_COMPENSATION.equals(strategy))
        return false;
    }
    return true;
  }

  /**
   * @author Roy Williams
   * @author Kee Chin Seong - We not allowed generate camera acquisiton parameters if is empty
   */
  public void generateProgramsForLowerImagingChainHardware(TestProgram testProgram,
                                                           TestExecutionTimer testExecutionTimer,
                                                           double zHeightDeltaAboveNominalInNanometers,
                                                           double zHeightDeltaBelowNominalInNanometers) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);

    // Generate Scan Path
    testExecutionTimer.startScanPassGeneratorTimer();
    _alignmentRegionInfoMap = new HashMap<ReconstructionRegion,Pair<ImageGroup,AlignmentScanPathSubSection>>();
    _imageGroups = new ArrayList<ImageGroup>();
    generateScanPathForTestProgram(testProgram,
                                   zHeightDeltaAboveNominalInNanometers,
                                   zHeightDeltaBelowNominalInNanometers);
    testExecutionTimer.stopScanPassGeneratorTimer();
    
    // Generate camera program
    if(testProgram.getFilteredScanPathTestSubPrograms().isEmpty() == true)
      return;
    
    generateCameraAcquisitionParameters(testProgram);
    
    // Khang Wah, New Scan Route Debug
    // IRP will detect REGION_NOT_IMAGED on certain region occasionally on machine
    // I will migrate the code from CPP to Java so it make offline debug possible and much easier.
    if(false)
      calculateScheduleOrder(testProgram);
  }

  /**
   * @author Roy Williams
   */
  public List<ImageReconstructionEngine> getParticipatingImageReconstructionEngines()
  {
    return new ArrayList<ImageReconstructionEngine>(_imageReconstructionEnginesInUse);
  }

  /**
   * @author Roy Williams
   * @author Chin-Seong, Kee 
   */
  private void updateImageReconstructionEnginesInUseBasedUponAdditionalScanPathInfo(ScanPath scanPath)
  {
    Assert.expect(scanPath != null);

    List<ProcessorStrip> processorStrips = scanPath.getMechanicalConversions().getProcessorStrips();

    if(_imageReconstructionEnginesInUse == null)
      _imageReconstructionEnginesInUse = new HashSet<ImageReconstructionEngine>();

    //When Virtual Live Mode Is ON, V810 will fully occupied the irp resources no matter how much the region is have.
    if(VirtualLiveManager.getInstance().isVirtualLiveMode() && ImageAcquisitionEngine.getInstance().isGenerateImageForExposureLearning() == false &&
      (_acquisitionMode.equals(ImageAcquisitionModeEnum.VERIFICATION) || _acquisitionMode.equals(ImageAcquisitionModeEnum.IMAGE_SET)))
    {
      for(int ireID = 0 ; ireID < VirtualLiveManager.getInstance().getTotalIREAssigned(); ireID ++)
         _imageReconstructionEnginesInUse.add(_idToImageReconstructionEngineMap.get(ImageReconstructionEngineEnum.getEnumList().get(ireID)));
    }
    else
    {
      for(ProcessorStrip strip : processorStrips)
      {
        _imageReconstructionEnginesInUse.add(_idToImageReconstructionEngineMap.get(strip.getReconstructionEngineId()));
      }
    }
  }

  /**
   * This method will generate the necessary scan path to inspect the panel.
   *
   * @author Roy Williams
   */
  private void generateScanPathForTestProgram(TestProgram testProgram,
                                              double zHeightDeltaAboveNominalInNanometers,
                                              double zHeightDeltaBelowNominalInNanometers) throws XrayTesterException
  {
    Assert.expect(_acquisitionMode != null);
    Assert.expect(testProgram != null);

    boolean isAlignmentAcquisitionMode = _acquisitionMode.equals(ImageAcquisitionModeEnum.ALIGNMENT);

    // No IREs/IPRs are in use when we begin program generation.
    _imageReconstructionEnginesInUse = null;
    testProgram.setFilteredTestSubProgramInScanPath(_acquisitionMode);
    for (TestSubProgram testSubProgram : testProgram.getFilteredScanPathTestSubPrograms())
    {
      // Start clean
      testSubProgram.clearScanPaths();
      testSubProgram.clearScanPassToImageGroupMap(); // Initialize the scan pass to image group map
      
      // make sure is not same magnification
      if( testSubProgram.isLowMagnification() != XrayTester.isLowMagnification() )
      {
        ImageAcquisitionEngine.changeMagnification(testSubProgram.getMagnificationType());

        if(getEnableStepSizeOptimization())
          forceCalculateMaxStepSizeInNanometers(testProgram.getProject().getPanel().getThicknessInNanometers(), testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers());
      }
      // need to recalculate for every testSubProgram, because different magnification(only) will produce different XLocation.
      _xLocationToPassCodeMap.clear();
      Config.getInstance().calculateXLocationToPassCode();
      _xLocationToPassCodeMap = Config.getInstance().getXLocationToPassCodeInfo();
    
      // Kee Chin Seong - Fiducial suppose need to considering the panel based alignment or board base alignment
      if(testProgram.getProject().getPanel().getPanelSettings().isPanelBasedAlignment())
         ImageGroup.setFiducials(testSubProgram.getFiducialsForTestProgram());
      else
         ImageGroup.setFiducials(testSubProgram.getFiducialsForTestSubProgram(testSubProgram)); // Kee Chin Seong - Fiducial suppose need to considering the panel based alignment or board base alignment
      
      // Inspection/verification/coupon
      int focalRangeUpsliceInNanometers = -1;
      int focalRangeDownsliceInNanometers = -1;
      if (isAlignmentAcquisitionMode == false)
      {
        if (_debug)
          TimerUtil.printCurrentTime("Start generateScanPathForTestSubProgram:" + testSubProgram.getId());
        ScanPath scanPathForMainPanelRectangle = generateScanPathForTestSubProgram(
            testSubProgram,
            zHeightDeltaAboveNominalInNanometers,
            zHeightDeltaBelowNominalInNanometers);
        if (_debug)
          TimerUtil.printCurrentTime("End generateScanPathForTestSubProgram:" + testSubProgram.getId());

        // Everything but alignment will do this.
        MechanicalConversions mechanicalConversions = scanPathForMainPanelRectangle.getMechanicalConversions();

        focalRangeUpsliceInNanometers = (int)mechanicalConversions.getFocalRangeUpsliceInNanometers();
        focalRangeDownsliceInNanometers = (int)mechanicalConversions.getFocalRangeDownsliceInNanometers();
      }
      else
      {
        Collection<ReconstructionRegion> alignmentRegions = testSubProgram.getAlignmentRegions();
        Assert.expect(alignmentRegions.size() > 0);
        List<ScanPath> alignmentScanPathList = new ArrayList<ScanPath>();

        for (ReconstructionRegion alignmentRegion : testSubProgram.getAlignmentRegions())
        {
          alignmentRegion.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
          AffineTransform aggregateAlignmentTransform = testSubProgram.getAggregateManualAlignmentTransform();
          
          ImageGroup imageGroup = null;//generateScanPathForAlignmentRegion(alignmentRegion, aggregateAlignmentTransform);
          if(testSubProgram.getTestProgram().getProject().isGenerateByNewScanRoute())
            imageGroup = generateFixXScanPathForAlignmentRegion(alignmentRegion, aggregateAlignmentTransform, testSubProgram);
          else
            imageGroup = generateScanPathForAlignmentRegion(alignmentRegion, aggregateAlignmentTransform);
          
          ScanPath scanPath = imageGroup.getScanPath();
          updateImageReconstructionEnginesInUseBasedUponAdditionalScanPathInfo(scanPath);
          focalRangeUpsliceInNanometers = (int)scanPath.getMechanicalConversions().getFocalRangeUpsliceInNanometers();
          focalRangeDownsliceInNanometers = (int)scanPath.getMechanicalConversions().getFocalRangeDownsliceInNanometers();
          alignmentScanPathList.add(scanPath);
        }

        List<ScanPass> scanPassList = new ArrayList<ScanPass>();
        for (ScanPath scanPath : alignmentScanPathList)
          scanPassList.addAll(scanPath.getScanPasses());
        
        StageSpeedEnum alignmentStageSpeed = testSubProgram.getAlignmentStageSpeed();
        new UserGainBreakPoint(scanPassList.get(0),
                               _REGION_TO_MARK_COMPLETED_FOR_MOTION_BREAKPOINT,
                               testSubProgram.getAlignmentUserGain().toDouble(),
                               alignmentStageSpeed.toDouble());

        new StageSpeedBreakPoint(scanPassList.get(0),
                                 scanPassList,
                                 _REGION_TO_MARK_COMPLETED_FOR_MOTION_BREAKPOINT,
                                 testSubProgram.getAlignmentStageSpeed(),
                                 testSubProgram.getMagnificationType());
        testSubProgram.setAlignmentScanPaths(alignmentScanPathList);
      }

      SetHardwareConstants.setFocalRangeUpsliceInNanometers(focalRangeUpsliceInNanometers);
      SetHardwareConstants.setFocalRangeDownsliceInNanometers(focalRangeDownsliceInNanometers);
    }

    boolean isolatedPSHRegionFound = false;
    for(TestSubProgram testSubProgram : testProgram.getFilteredScanPathTestSubPrograms())
    {
      if(testSubProgram.isContainIsolatedGlobalSurfaceRegion()==true)
      {
        System.out.println("IMAGING_CHAIN::TestSubProgram "+testSubProgram.getId()+" is having NoNeighborsToPredictZHeightException!");
        isolatedPSHRegionFound=true;
      }
    }
    
    if(isolatedPSHRegionFound)
    {
      //Siew Yeng - XCR-2460
      String fileName = generateIsolatedPSHRegionCSVFile(testProgram.getProject());
      
      System.out.println(StringLocalizer.keyToString(new LocalizedString("HW_TEST_NO_NEIGHBORS_TO_PREDICT_ZHEIGHT_KEY",
                                                      new Object[]{fileName})));
      //throw new NoNeighborsToPredictZHeightException(fileName);
    }
    
    // Swee Yee Wong - Variable DivN scanpass differences will be taken care by motion control now, so we can just
    // pass different length of scanpasses to motion control without any extra handling.
    // Clean up the ordering of these scan passes.
//    if (testProgram.getProject().isVarDivNEnable() &&
//       (_acquisitionMode.equals(ImageAcquisitionModeEnum.IMAGE_SET) ||
//        _acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION))) // XCR1529, New Scan Route, khang-wah.chnee
//    {
//      fixOrderAndStageStartStopPositionsInYForVariableDivn(testProgram);
//    }
//    else
//    {
      fixOrderAndStageStartStopPositionsInY(testProgram);
//    }
   
    // Added by Lee Herng 13 Nov 2013 - Handle new optical image acquisition sequence
    if (_acquisitionMode.equals(ImageAcquisitionModeEnum.IMAGE_SET) ||
        _acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION))
    {
      setupSurfaceMapBreakPoint(testProgram);
    }
    
    boolean dumpScanpath = false;
    // Requires developer license
    if (Config.isDeveloperSystemEnabled() &&
        (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.IMAGING_CHAIN_PROGRAM_GENERATOR_SCANPATH_ONLY) || Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.IMAGING_CHAIN_PROGRAM_GENERATOR)))
        dumpScanpath = true;
    if (dumpScanpath)
    {
      //List<ScanPass> scanPasses = testProgram.getScanPasses();
      for (TestSubProgram testSubProgram : testProgram.getFilteredScanPathTestSubPrograms())
      {
          List<ScanPass> scanPasses = testSubProgram.getScanPasses();
          System.out.println("Scan Passes");
          for (ScanPass sp : scanPasses)
          {
            System.out.println(sp.getId() + ", " + sp.getStartPointInNanoMeters().getXInNanometers() + ", " + sp.getStartPointInNanoMeters().getYInNanometers());
            System.out.println(sp.getId() + ", " + sp.getEndPointInNanoMeters().getXInNanometers() + ", " + sp.getEndPointInNanoMeters().getYInNanometers());
          }
          System.out.println("End of Scan Passes");
      }
    }
  }



  /**
   * This method generates the basic scan path for imaging the entire
   * test sub-program.
   *
   * @return the basic scan path necessary to image the TestSubProgram
   * @author Roy Williams
   * @author Kay Lannen
   */
  private ScanPath generateScanPathForTestSubProgram(
      TestSubProgram testSubProgram,
      double zHeightDeltaAboveNominalInNanometers,
      double zHeightDeltaBelowNominalInNanometers) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);

    if (testSubProgram.getTestProgram().getProject().isGenerateByNewScanRoute())
    {
      if (_acquisitionMode.equals(ImageAcquisitionModeEnum.IMAGE_SET) ||
          _acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION))
      {
        return createFixXScanPathForImageSetOrProductionMVEDR(testSubProgram);
      }
      return createScanPathForCouponOrVerification(testSubProgram,
                                                   zHeightDeltaAboveNominalInNanometers,
                                                   zHeightDeltaBelowNominalInNanometers);
    }
    else
    {
      if (_acquisitionMode.equals(ImageAcquisitionModeEnum.IMAGE_SET) ||
        _acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION))
      {
        return createScanPathForImageSetOrProductionMVEDR(testSubProgram);
      }
      return createScanPathForCouponOrVerification(testSubProgram,
                                                   zHeightDeltaAboveNominalInNanometers,
                                                   zHeightDeltaBelowNominalInNanometers);
    }
  }

  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   * 
   * If you make this method public (just in case),
   * please make sure you handle HomogeneousImageGroup.setScanPathMethod properly below,
   * which is setting a static variable in HomogeneousImageGroup.
   */
//  private ScanPath createFixXScanPathForImageSetOrProduction(TestSubProgram testSubProgram) throws XrayTesterException
//  {
//    Assert.expect(testSubProgram != null);
//    
//    // Make sure all HomogeneousImageGroup objects are created using same & correct method
//    ScanPathMethodEnum currentScanPathMethod = testSubProgram.getTestProgram().getProject().getScanPathMethod();
//    HomogeneousImageGroup.setScanPathMethod(currentScanPathMethod);
//    
//    // Reset memory limit control for Real Time PSH, false by default
//    testSubProgram.setMemorySufficientForRealTimePsh(false);
//    // Reset PSH region condition state, false by default
//    testSubProgram.clearIsolatedGlobalSurfaceRegion();
//    //XCR-3342, Setting IL with different value in Customize Alignment and Virtual Live does not take effect on the scan path
//    resetDefaultMeanStepSizeLimitInNanometers();
//    // need to recalculate for every testSubProgram, because different magnification(only) will produce different XLocation.
////    _xLocationToPassCodeMap.clear();
////    Config.getInstance().calculateXLocationToPassCode();
////    _xLocationToPassCodeMap = Config.getInstance().getXLocationToPassCodeInfo();
//
//    boolean forceMerge = false; // merge signalCompensationGroup into mainGroup without other consideration
//    // Check memory use up by PSH scan path
//    // ====================================
//    Collection<ReconstructionRegion> allInspectionRegions = testSubProgram.getAllScanPathReconstructionRegions();
//    Collection<ReconstructionRegion> regionsRequiringGlobalSurfaceModel = new ArrayList<ReconstructionRegion>();
//    
//    List<HomogeneousImageGroup> surfaceModelGroups = new ArrayList<HomogeneousImageGroup>();
//    if(Config.isRealTimePshEnabled())
//    {
//      divideRegionsRequiringGlobalSurfaceModel(allInspectionRegions, regionsRequiringGlobalSurfaceModel);
//      
//      if(regionsRequiringGlobalSurfaceModel.size() > 0)
//      {
//        surfaceModelGroups = createFixXScanPaths(testSubProgram, regionsRequiringGlobalSurfaceModel, forceMerge);
//    
//        long memoryUsed = 0;//calculateMemoryUsedforEachIRP(surfaceModelGroup.getScanPath(), surfaceModelGroup.getScanPath().getScanPasses().size());
//        long irpMemoryAllocatedForProjectionMemory = ImageReconstructionEngine.getMemoryAllocatedForProjectionMemory();
//        
//        for(HomogeneousImageGroup group : surfaceModelGroups)
//        {
//          memoryUsed += calculateMemoryUsedforEachIRP(group.getScanPath(), group.getScanPath().getScanPasses().size());
//        }
//
//        // We merge PSH scan path into main scanpath and signal compensation scan path as long as memory it consumed 
//        // is less than total memory the IRP have.
//        if(memoryUsed < irpMemoryAllocatedForProjectionMemory)
//        {
//          System.out.println("RT_PSH:: TestSubProgram: "+testSubProgram.getId());
//          System.out.println("RT_PSH:: Running RT PSH!");
//          testSubProgram.setMemorySufficientForRealTimePsh(true);
//        }
//        else
//        {
//          System.out.println("RT_PSH:: TestSubProgram: "+testSubProgram.getId());
//          System.out.println("RT_PSH:: Not Running RT PSH!");
//        }
//        
//        // Perform data clean up on reconstruction region
//        for(HomogeneousImageGroup group : surfaceModelGroups)
//        {
//          for(Pair<ReconstructionRegion, SystemFiducialRectangle> pair : group.getRegionPairsSortedInExecutionOrder())
//          {
//            pair.getFirst().clearScanPasses();
//          }
//          
//          for(ScanPass sp : group.getScanPath().getScanPasses())
//          {
//            testSubProgram.removeScanPassToImageGroup(sp);
//          }
//        }
//        surfaceModelGroups.clear();
//        regionsRequiringGlobalSurfaceModel.clear();
//      }
//    }
//    
//    // Divide ALL inspection regions into one of three buckets.
//    // o   ALL is very important.  Do not use "Filtered" because it will modify
//    //     the resulting inspection scanPath... arguably causing classification error.
//    // o  allInspectionRegions does NOT contain the alignment ReconstructionRegions.
//    Collection<ReconstructionRegion> regionsRequiringInteferenceCompensation = new ArrayList<ReconstructionRegion>();
//    Collection<ReconstructionRegion> regionsWithoutShadingCompensation = new ArrayList<ReconstructionRegion>();
//    Collection<ReconstructionRegion> regionsRequiringShadingCompensation;
//    Map<Integer,Collection<ReconstructionRegion>> regionsRequiringShadingCompensationList = new HashMap<Integer,Collection<ReconstructionRegion>>();
//    Collection<ReconstructionRegion> regionsWithDRO = new ArrayList<ReconstructionRegion>();
//    Map<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>> regionsWithDROList = new HashMap<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>>();
//    divideRegionsBaseOnAdvanceSettings(allInspectionRegions,
//                                       regionsRequiringGlobalSurfaceModel,
//                                       regionsRequiringInteferenceCompensation,
//                                       regionsWithoutShadingCompensation,
//                                       regionsRequiringShadingCompensationList,
//                                       regionsWithDROList,
//                                       true);
//    
//    regionsRequiringShadingCompensation = new ArrayList<ReconstructionRegion>();
//    for ( Collection<ReconstructionRegion> regionList : regionsRequiringShadingCompensationList.values())
//    {
//      regionsRequiringShadingCompensation.addAll(regionList);
//    }
//    
//    // PART 1 - group creation
//    // ========================================================================
//    // 1. Create groups for main
//    HomogeneousImageGroup mainGroup = null;
//    if(regionsWithoutShadingCompensation.size()>0)
//      mainGroup = new HomogeneousImageGroup(regionsWithoutShadingCompensation, false);
//    else
//    {
//      //Siew Yeng - DRO
//      //fix crash when the only component set to test is set to use DRO
//      if(regionsRequiringShadingCompensationList.size() > 0)
//      {
//        int key = regionsRequiringShadingCompensationList.keySet().iterator().next();
//        mainGroup = new HomogeneousImageGroup(regionsRequiringShadingCompensationList.remove(key), false);
//      }
//    }
//    
//    // 2. Create groups for signal compensation
//    List<HomogeneousImageGroup> shadingCompensationGroups = new ArrayList<HomogeneousImageGroup>();
//    for ( Collection<ReconstructionRegion> regions : regionsRequiringShadingCompensationList.values())
//    {
//      HomogeneousImageGroup group = new HomogeneousImageGroup(regions, false);
//      shadingCompensationGroups.add(group);
//    }
//    
//    // 3. Create groups for inteference compensation
//    // Groups for inteference compensation still been created using old way
//    List<HomogeneousImageGroup> InteferenceGroups = null;
//    if (regionsRequiringInteferenceCompensation.size() > 0)
//    {
//      RescanStrategy InteferenceRescanStrategy = selectBestRescanStrategy(testSubProgram, regionsRequiringInteferenceCompensation);
//      InteferenceGroups = InteferenceRescanStrategy.getGroups();
//    }
//    
//    // 4. Create groups for global surface model (PSH)
//    // It will be do together with scan path generation at latter stage
//    
//    // Siew Yeng - XCR-2140 - DRO
//    // 5. Create groups for Dynamic Range Optimization Level
//    //Map<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>> regionsWithDynamicRangeOptimizationList = new HashMap<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>>();
//    //divideRegionsWithDynamicRangeOptimization(allInspectionRegions,regionsWithDynamicRangeOptimizationList);
//    List<HomogeneousImageGroup> droLevelGroups = new ArrayList<HomogeneousImageGroup>();
//    if(regionsWithDROList.isEmpty() == false)
//    {
//      for ( Collection<ReconstructionRegion> regionList : regionsWithDROList.values())
//      {
//        HomogeneousImageGroup group = new HomogeneousImageGroup(regionList, false);
//        droLevelGroups.add(group);
//      }
//    }
//    
//    //Ngie Xing, add collection to get regions using DRO
//    if(mainGroup == null && droLevelGroups.size() > 0)
//    {
//      mainGroup = droLevelGroups.remove(0);
//      DynamicRangeOptimizationLevelEnum key = regionsWithDROList.keySet().iterator().next();
//      createFixXScanPath(testSubProgram, mainGroup, key);
//      regionsWithDRO.addAll(regionsWithDROList.remove(key));
//    }
//    else
//    // PART 2 - scanpath creation
//    // ========================================================================
//    // 1. Create scanpath for main
//    createFixXScanPath(testSubProgram, mainGroup);
//    
//    // 2. Create scanpath for signal compensation groups and try to merge into main group
//    List<HomogeneousImageGroup> toMaintainGroups = new ArrayList<HomogeneousImageGroup>();
//    if(shadingCompensationGroups != null && shadingCompensationGroups.size()>0)
//    {
//      for(HomogeneousImageGroup group : shadingCompensationGroups)
//      {
//        createFixXScanPath(testSubProgram, group);
//        
//        if(forceMerge==true || isWorthToMerge(testSubProgram, mainGroup, group))
//          mainGroup = mergeGroupsAndScanPaths(testSubProgram, mainGroup, group, true);
//        else
//          toMaintainGroups.add(group);
//      }
//    }
//    _imageGroups.add(mainGroup);
//    
//    shadingCompensationGroups.clear();
//    if(toMaintainGroups!=null && toMaintainGroups.size()>0)
//    {
//      // possible to merge within them?
//      if(toMaintainGroups.size()>1)
//        toMaintainGroups = attemptToMergeGroupsAndScanPaths(testSubProgram, toMaintainGroups);
//                
//      shadingCompensationGroups.addAll(toMaintainGroups);
//      _imageGroups.addAll(shadingCompensationGroups);
//    }
//    
//    // 3. Create scanpath for inteference compensation groups
//    // Scanpath for inteference compensation group still been created using old way
//    if (InteferenceGroups != null && InteferenceGroups.size() > 0)
//    {
//      for (HomogeneousImageGroup group : InteferenceGroups)
//      {
//        createScanPath(testSubProgram, group);
//      }
//    }
//    
//    // 4. Create scanpath for global surface model (PSH) groups
//    SurfaceModelBreakPoint breakpoint = null;
//    surfaceModelGroups.clear();
//    if (usingGlobalSurfaceModel() && regionsRequiringGlobalSurfaceModel.size() > 0)
//    {
//      ScanPass breakPointScanPass =  mainGroup.getScanPath().getScanPasses().get(0);
//      
//      if(Config.isRealTimePshEnabled()==false || testSubProgram.isSufficientMemoryForRealTimePsh()==false)
//      {
//        surfaceModelGroups = createFixXScanPaths(testSubProgram, regionsRequiringGlobalSurfaceModel, forceMerge);
//        breakPointScanPass = surfaceModelGroups.get(0).getScanPath().getScanPasses().get(0);
//        _imageGroups.addAll(surfaceModelGroups);
//      }
//      
//      breakpoint = new SurfaceModelBreakPoint(testSubProgram,
//                                              breakPointScanPass,
//                                              surfaceModelGroups,
//                                              allInspectionRegions.size());
//      
//      for(ReconstructionRegion region : regionsRequiringGlobalSurfaceModel)
//        region.setSurfaceModelBreakPoint(breakpoint);
//    }
//    
//    //Siew Yeng - XCR-2140 - DRO
//    // 5. Create scanpath for Dynamic Range Optimization Level groups
//    if(droLevelGroups != null && droLevelGroups.size() > 0)
//    {
//      Iterator<DynamicRangeOptimizationLevelEnum> iterator = regionsWithDROList.keySet().iterator();
//      for (HomogeneousImageGroup group : droLevelGroups)
//      {
//        createFixXScanPath(testSubProgram, group, iterator.next());
//      }
//    }
//    
//    // PART 3 - add the scan route into testSubProgram
//    // ========================================================================
//    // 1. Add main + signal compensation and possible global surface model scanpath
//    testSubProgram.addScanPathOverInspectionOrVerificationArea(mainGroup.getScanPath());
//    ScanPath scanPathForMain = mainGroup.getScanPath();
//        
//    // 2. Add signal compensation scanpath
//    if(shadingCompensationGroups != null && shadingCompensationGroups.size()>0)
//    {
//      for(HomogeneousImageGroup group : shadingCompensationGroups)
//      {
//        testSubProgram.addScanPathOverInspectionOrVerificationArea(group.getScanPath());
//      }
//    }
//    
//    // 3. Add inteference compensation scanpath
//    if (InteferenceGroups != null && InteferenceGroups.size() > 0)
//    {
//      for (ImageGroup group : InteferenceGroups)
//      {
//        testSubProgram.addScanPathOverInspectionOrVerificationArea(group.getScanPath());
//      }
//    }
//    
//    // 4. Add global surface model (PSH) scanpath if no merge into main
//    if ((surfaceModelGroups != null && surfaceModelGroups.size()>0) &&
//        (Config.isRealTimePshEnabled()==false || testSubProgram.isSufficientMemoryForRealTimePsh()==false))
//    {
//      for(HomogeneousImageGroup group : surfaceModelGroups)
//      {
//        testSubProgram.addScanPathOverInspectionOrVerificationArea(group.getScanPath());
//      }
//    }
//    
//    //Siew Yeng - XCR-2140 - DRO
//    // 5. Add dynamic range optimization level (DRO) to testSubProgram
//    if(droLevelGroups != null && droLevelGroups.size() > 0)
//    {
//      for(HomogeneousImageGroup group : droLevelGroups)
//      {
//        testSubProgram.addScanPathOverInspectionOrVerificationArea(group.getScanPath());
//        //Ngie Xing, add all regions into collection
//        regionsWithDRO.addAll(group.getUnsortedRegions());
//      }
//    }
//    
//    // 6. Add the scanPaths to TestSubProgram for alignment regions
//    List<ScanPath> alignmentScanPaths = mergeAlignmentToFixXScanPath(testSubProgram, scanPathForMain);
//    testSubProgram.setAlignmentScanPaths(alignmentScanPaths);
//    
//    // PART 4 - setup neighbor regions and trigger region for global surface model (PSH) region
//    // ========================================================================
//    // only do this for 64bit IRP
//    if ((usingGlobalSurfaceModel() && regionsRequiringGlobalSurfaceModel.size()>0) && 
//            Config.isRealTimePshEnabled() && testSubProgram.isSufficientMemoryForRealTimePsh())
//    {
//      Collection<ReconstructionRegion> mainRegions = new ArrayList<ReconstructionRegion>();
//      
//      mainRegions.addAll(regionsWithoutShadingCompensation);
//      mainRegions.addAll(regionsRequiringShadingCompensation);
//      mainRegions.addAll(regionsRequiringInteferenceCompensation);
//      //Ngie Xing, add all DRO regions to mainRegions
//      mainRegions.addAll(regionsWithDRO);
//      
//      HomogeneousImageGroup mainSGroup = new HomogeneousImageGroup(mainRegions);
//      HomogeneousImageGroup surfaceModelGroup = new HomogeneousImageGroup(regionsRequiringGlobalSurfaceModel);
//      
//      Collection<ReconstructionRegion> regionsWithoutNeighbor = setupNeighborAndTriggerRegionForGlobalSurfaceModalRegion(mainSGroup, surfaceModelGroup);
//      
//      if(regionsWithoutNeighbor.size()>0)
//      {
//        testSubProgram.getIsolatedGlobalSurfaceRegion().addAll(regionsWithoutNeighbor);
//      }
//    }
//
//    return scanPathForMain;
//  }

  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   * 
   * If you make this method public (just in case),
   * please make sure you handle HomogeneousImageGroup.setScanPathMethod properly below,
   * which is setting a static variable in HomogeneousImageGroup.
   */
  private ScanPath createFixXScanPathForImageSetOrProductionMVEDR(TestSubProgram testSubProgram) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    
    // Make sure all HomogeneousImageGroup objects are created using same & correct method
    ScanPathMethodEnum currentScanPathMethod = testSubProgram.getTestProgram().getProject().getScanPathMethod();
    HomogeneousImageGroup.setScanPathMethod(currentScanPathMethod);
    
    // Reset memory limit control for Real Time PSH, false by default
    testSubProgram.setMemorySufficientForRealTimePsh(false);
    // Reset PSH region condition state, false by default
    testSubProgram.clearIsolatedGlobalSurfaceRegion();
    //XCR-3342, Setting IL with different value in Customize Alignment and Virtual Live does not take effect on the scan path
    resetDefaultMeanStepSizeLimitInNanometers();
    // need to recalculate for every testSubProgram, because different magnification(only) will produce different XLocation.
//    _xLocationToPassCodeMap.clear();
//    Config.getInstance().calculateXLocationToPassCode();
//    _xLocationToPassCodeMap = Config.getInstance().getXLocationToPassCodeInfo();

    boolean forceMerge = false; // merge signalCompensationGroup into mainGroup without other consideration
    // Check memory use up by PSH scan path
    // ====================================
    Collection<ReconstructionRegion> allInspectionRegions = testSubProgram.getAllScanPathReconstructionRegions();
    Collection<ReconstructionRegion> regionsRequiringGlobalSurfaceModel = new ArrayList<ReconstructionRegion>();
    
    if(Config.isRealTimePshEnabled())
    {
      testSubProgram.setMemorySufficientForRealTimePsh(true); // make it a must for MVEDR
      /*
      divideRegionsRequiringGlobalSurfaceModel(allInspectionRegions, regionsRequiringGlobalSurfaceModel);
      
      if(regionsRequiringGlobalSurfaceModel.size() > 0)
      {
        // Loop through all user gain and stage speed
        for (StageSpeedEnum stageSpeedEnum : StageSpeedEnum.getOrderedStageSpeed()) 
        {
          for(UserGainEnum userGainEnum : UserGainEnum.getOrderedUserGain())
          {
            Collection<ReconstructionRegion> regions = extractRegionsWithCertainHardwareSettings(userGainEnum, 
                                                                                                 stageSpeedEnum, 
                                                                                                 regionsRequiringGlobalSurfaceModel);
            
            if(regions.size()>0)
            {
              surfaceModelGroups.addAll(createFixXScanPaths(testSubProgram, regionsRequiringGlobalSurfaceModel, forceMerge));
            }
          } 
        }
        
        long memoryUsed = 0;//calculateMemoryUsedforEachIRP(surfaceModelGroup.getScanPath(), surfaceModelGroup.getScanPath().getScanPasses().size());
        long irpMemoryAllocatedForProjectionMemory = ImageReconstructionEngine.getMemoryAllocatedForProjectionMemory();
        
        for(HomogeneousImageGroup group : surfaceModelGroups)
        {
          memoryUsed += calculateMemoryUsedforEachIRP(group.getScanPath(), group.getScanPath().getScanPasses().size());
        }

        // We merge PSH scan path into main scanpath and signal compensation scan path as long as memory it consumed 
        // is less than total memory the IRP have.
        if(memoryUsed < irpMemoryAllocatedForProjectionMemory)
        {
          System.out.println("RT_PSH:: TestSubProgram: "+testSubProgram.getId());
          System.out.println("RT_PSH:: Running RT PSH!");
          testSubProgram.setMemorySufficientForRealTimePsh(true);
        }
        else
        {
          System.out.println("RT_PSH:: TestSubProgram: "+testSubProgram.getId());
          System.out.println("RT_PSH:: Not Running RT PSH!");
        }
        
        // Perform data clean up on reconstruction region
        for(HomogeneousImageGroup group : surfaceModelGroups)
        {
          for(Pair<ReconstructionRegion, SystemFiducialRectangle> pair : group.getRegionPairsSortedInExecutionOrder())
          {
            pair.getFirst().clearScanPasses();
          }
          
          for(ScanPass sp : group.getScanPath().getScanPasses())
          {
            testSubProgram.removeScanPassToImageGroup(sp);
          }
        }
        surfaceModelGroups.clear();
        regionsRequiringGlobalSurfaceModel.clear();
      }
      */
    }
    
    // Divide ALL inspection regions into one of three buckets.
    // o   ALL is very important.  Do not use "Filtered" because it will modify
    //     the resulting inspection scanPath... arguably causing classification error.
    // o  allInspectionRegions does NOT contain the alignment ReconstructionRegions.
    Collection<ReconstructionRegion> regionsRequiringInteferenceCompensation = new ArrayList<ReconstructionRegion>();
    Collection<ReconstructionRegion> regionsWithoutShadingCompensation = new ArrayList<ReconstructionRegion>();
    Collection<ReconstructionRegion> regionsRequiringShadingCompensation;
    Map<Integer,Collection<ReconstructionRegion>> regionsRequiringShadingCompensationList = new HashMap<Integer,Collection<ReconstructionRegion>>();
    Collection<ReconstructionRegion> regionsWithDRO = new ArrayList<ReconstructionRegion>();
    Map<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>> regionsWithDROList = new HashMap<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>>();
    divideRegionsBaseOnAdvanceSettings(allInspectionRegions,
                                       regionsRequiringGlobalSurfaceModel,
                                       regionsRequiringInteferenceCompensation,
                                       regionsWithoutShadingCompensation,
                                       regionsRequiringShadingCompensationList,
                                       regionsWithDROList,
                                       true);
    
    regionsRequiringShadingCompensation = new ArrayList<ReconstructionRegion>();
    for ( Collection<ReconstructionRegion> regionList : regionsRequiringShadingCompensationList.values())
    {
      regionsRequiringShadingCompensation.addAll(regionList);
    }
    
    if(regionsWithDROList.isEmpty() == false)
    {
      for ( Collection<ReconstructionRegion> regionList : regionsWithDROList.values())
        regionsWithDRO.addAll(regionList);
    }
    
    // Proceed to scan path generation for all hardware settings
    ScanPath scanPathForMain = createFixXScanPathForTestSubProgram(testSubProgram, allInspectionRegions, forceMerge);
    
    validateRegionAssociatedScanPass(testSubProgram);
    
    // PART 4 - setup neighbor regions and trigger region for global surface model (PSH) region
    // ========================================================================
    // only do this for 64bit IRP
    if ((usingGlobalSurfaceModel() && regionsRequiringGlobalSurfaceModel.size()>0) && 
            Config.isRealTimePshEnabled() && testSubProgram.isSufficientMemoryForRealTimePsh())
    {
      Collection<ReconstructionRegion> mainRegions = new ArrayList<ReconstructionRegion>();
      
      mainRegions.addAll(regionsWithoutShadingCompensation);
      mainRegions.addAll(regionsRequiringShadingCompensation);
      mainRegions.addAll(regionsRequiringInteferenceCompensation);
      mainRegions.addAll(regionsWithDRO);
      
      HomogeneousImageGroup mainSGroup = new HomogeneousImageGroup(mainRegions);
      HomogeneousImageGroup surfaceModelGroup = new HomogeneousImageGroup(regionsRequiringGlobalSurfaceModel);
      
      Collection<ReconstructionRegion> regionsWithoutNeighbor = setupNeighborAndTriggerRegionForGlobalSurfaceModalRegion(mainSGroup, surfaceModelGroup);
      
      if(regionsWithoutNeighbor.size()>0)
      {
        testSubProgram.getIsolatedGlobalSurfaceRegion().addAll(regionsWithoutNeighbor);
      }
    }

    return scanPathForMain;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  private ScanPath createFixXScanPathForTestSubProgram(TestSubProgram testSubProgram, 
                                                       Collection<ReconstructionRegion> allInspectionRegions,
                                                       boolean forceMerge) throws XrayTesterException
  {
    Collection<Collection<ScanPath>> totalScanPathsList = new ArrayList<Collection<ScanPath>>();
    
    // Proceed to scan path generation for all hardware settings
    ScanPath scanPathForMain = null; //  Add the scanPaths to TestSubProgram for alignment regions
    
    for (StageSpeedEnum stageSpeedEnum : StageSpeedEnum.getOrderedStageSpeed()) 
    {
      List <ScanPass> scanPasses = new ArrayList<ScanPass>();
      
      for(UserGainEnum userGainEnum : UserGainEnum.getOrderedUserGain())
      {
        Collection<ReconstructionRegion> regions = extractRegionsWithCertainHardwareSettings(userGainEnum, 
                                                                                             stageSpeedEnum, 
                                                                                             allInspectionRegions);
        
        if(regions.size()>0)
        {
          ScanPassSetting scanPassSetting = new ScanPassSetting(stageSpeedEnum.toDouble(), 
                                                                userGainEnum.toDouble(), 
                                                                testSubProgram.isLowMagnification());
          List<ScanPath> scanPaths = createFixXScanPathForSpecificHardwareSettings(testSubProgram, 
                                                                                   regions,
                                                                                   forceMerge,
                                                                                   scanPassSetting);
          
          ScanPass scanPassForUserGainBreakPoint = null;
          if(totalScanPathsList.isEmpty())
          {
            boolean isCustomizedAlignmentPathSetting = false;
            if (testSubProgram.isSubProgramRequiredCustomAlignmentPathGeneration() || 
                stageSpeedEnum.equals(testSubProgram.getAlignmentStageSpeed()) == false ||
                userGainEnum.equals(testSubProgram.getAlignmentUserGain()) == false)
            {
              isCustomizedAlignmentPathSetting = true;
            }
            
            scanPathForMain = scanPaths.iterator().next();
            List<ScanPath> alignmentScanPaths = mergeAlignmentToFixXScanPath(testSubProgram,
                                                                             scanPathForMain,
                                                                             isCustomizedAlignmentPathSetting);
            testSubProgram.setAlignmentScanPaths(alignmentScanPaths);
            
            if (isCustomizedAlignmentPathSetting)
            {
              StageSpeedEnum stageSpeedForAlignment = VirtualLiveManager.getInstance().isVirtualLiveMode() ? testSubProgram.getVirtualLiveAlignmentStageSpeed() : testSubProgram.getAlignmentStageSpeed();
              UserGainEnum userGainForAlignment = VirtualLiveManager.getInstance().isVirtualLiveMode() ? testSubProgram.getVirtualLiveAlignmentUserGain() : testSubProgram.getAlignmentUserGain();
              List<ScanPass> alignmentScanPasses = new ArrayList<ScanPass>();
              for (ScanPath scanPath : alignmentScanPaths)
                alignmentScanPasses.addAll(scanPath.getScanPasses());

              new UserGainBreakPoint(alignmentScanPasses.get(0),
                                     _REGION_TO_MARK_COMPLETED_FOR_MOTION_BREAKPOINT,
                                     userGainForAlignment.toDouble(),
                                     stageSpeedForAlignment.toDouble());

              new StageSpeedBreakPoint(alignmentScanPasses.get(0),
                                       alignmentScanPasses,
                                       _REGION_TO_MARK_COMPLETED_FOR_MOTION_BREAKPOINT,
                                       stageSpeedForAlignment,
                                       testSubProgram.getMagnificationType());
            }
            else
            {
              for (ScanPath scanPath : alignmentScanPaths)
                scanPasses.addAll(scanPath.getScanPasses());
            }

            if(scanPasses.isEmpty() == false)
              scanPassForUserGainBreakPoint = scanPasses.get(0);
            else
              scanPassForUserGainBreakPoint = scanPathForMain.getScanPasses().get(0);
          }
          else
            scanPassForUserGainBreakPoint = scanPaths.iterator().next().getScanPasses().get(0);
          
          new UserGainBreakPoint(scanPassForUserGainBreakPoint, 
                                 _REGION_TO_MARK_COMPLETED_FOR_MOTION_BREAKPOINT, 
                                 userGainEnum.toDouble(),
                                 stageSpeedEnum.toDouble());
          
          for (ScanPath scanPath : scanPaths)
            scanPasses.addAll(scanPath.getScanPasses());
        
          totalScanPathsList.add(scanPaths);
        }
      }
      
      if(scanPasses.isEmpty()==false)
      {
        new StageSpeedBreakPoint(scanPasses.get(0),
                                 scanPasses,
                                 _REGION_TO_MARK_COMPLETED_FOR_MOTION_BREAKPOINT,
                                 stageSpeedEnum,
                                 testSubProgram.getMagnificationType());
      }
    }
    
    // Add all scan passes into testSubProgram by sequence
    for (Collection<ScanPath> scanPaths : totalScanPathsList)
    {
      for(ScanPath scanPath : scanPaths)
        testSubProgram.addScanPathOverInspectionOrVerificationArea(scanPath);
    }
    
    return scanPathForMain;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  private void validateRegionAssociatedScanPass(TestSubProgram testSubProgram)
  {
    Assert.expect(testSubProgram != null);
    
    Map<Integer, List<ScanPass>> xLocationToNumberOfScanPassAssociated = new HashMap<Integer, List<ScanPass>>();
    for (ReconstructionRegion reconRegion : testSubProgram.getAllScanPathReconstructionRegions())
    {
      if (reconRegion.useMVEDR() == false)
        continue;
      
      xLocationToNumberOfScanPassAssociated.clear();
      int numberOfSpeedExpected = reconRegion.getStageSpeedList().size();
      
      for (ScanPass sp : reconRegion.getScanPasses())
      {
        int xLoc = sp.getStartPointInNanoMeters().getXInNanometers();
        
        if (xLocationToNumberOfScanPassAssociated.containsKey(xLoc))
          xLocationToNumberOfScanPassAssociated.get(xLoc).add(sp);
        else
        {
          List<ScanPass> scanPassList = new ArrayList<ScanPass>();
          scanPassList.add(sp);
          xLocationToNumberOfScanPassAssociated.put(xLoc, scanPassList);
        }
      }
      
      for (Map.Entry<Integer, List<ScanPass>> entrySet : xLocationToNumberOfScanPassAssociated.entrySet())
      {
        if (entrySet.getValue().size() != numberOfSpeedExpected)
        {
          for (ScanPass sp : entrySet.getValue())
            reconRegion.removeScanPass(sp);
        }
      }
    }
  }
  
  /**
   * @author Khang Wah, 2016-05-06, MVEDR
   */
  private List<ScanPath> createFixXScanPathForSpecificHardwareSettings(TestSubProgram testSubProgram, 
                                                                       Collection<ReconstructionRegion> allInspectionRegions,
                                                                       boolean forceMerge,
                                                                       ScanPassSetting scanPassSetting) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(allInspectionRegions != null);
    Assert.expect(scanPassSetting != null);
    
    List<ScanPath> scanPaths = new ArrayList<ScanPath>();
    
    // Divide ALL inspection regions into one of three buckets.
    // o   ALL is very important.  Do not use "Filtered" because it will modify
    //     the resulting inspection scanPath... arguably causing classification error.
    // o  allInspectionRegions does NOT contain the alignment ReconstructionRegions.
    Collection<ReconstructionRegion> regionsRequiringGlobalSurfaceModel = new ArrayList<ReconstructionRegion>();
    Collection<ReconstructionRegion> regionsRequiringInteferenceCompensation = new ArrayList<ReconstructionRegion>();
    Collection<ReconstructionRegion> regionsWithoutShadingCompensation = new ArrayList<ReconstructionRegion>();
    Collection<ReconstructionRegion> regionsRequiringShadingCompensation;
    Map<Integer,Collection<ReconstructionRegion>> regionsRequiringShadingCompensationList = new HashMap<Integer,Collection<ReconstructionRegion>>();
    Map<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>> regionsWithDROList = new HashMap<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>>();
    divideRegionsBaseOnAdvanceSettings(allInspectionRegions,
                                       regionsRequiringGlobalSurfaceModel,
                                       regionsRequiringInteferenceCompensation,
                                       regionsWithoutShadingCompensation,
                                       regionsRequiringShadingCompensationList,
                                       regionsWithDROList,
                                       false);
    
    regionsRequiringShadingCompensation = new ArrayList<ReconstructionRegion>();
    for ( Collection<ReconstructionRegion> regionList : regionsRequiringShadingCompensationList.values())
    {
      regionsRequiringShadingCompensation.addAll(regionList);
    }
    
    // PART 1 - group creation
    // ========================================================================
    // 1. Create groups for main
    HomogeneousImageGroup mainGroup = null;
    if(regionsWithoutShadingCompensation.size()>0)
      mainGroup = new HomogeneousImageGroup(regionsWithoutShadingCompensation, false);
    else
    {
      //Siew Yeng - DRO
      //fix crash when the only component set to test is set to use DRO
      if(regionsRequiringShadingCompensationList.size() > 0)
      {
        int key = regionsRequiringShadingCompensationList.keySet().iterator().next();
        mainGroup = new HomogeneousImageGroup(regionsRequiringShadingCompensationList.remove(key), false);
      }
    }
    
    // 2. Create groups for signal compensation
    List<HomogeneousImageGroup> shadingCompensationGroups = new ArrayList<HomogeneousImageGroup>();
    for ( Collection<ReconstructionRegion> regions : regionsRequiringShadingCompensationList.values())
    {
      HomogeneousImageGroup group = new HomogeneousImageGroup(regions, false);
      shadingCompensationGroups.add(group);
    }
    
    // 3. Create groups for inteference compensation
    // Groups for inteference compensation still been created using old way
    List<HomogeneousImageGroup> InteferenceGroups = null;
    if (regionsRequiringInteferenceCompensation.size() > 0)
    {
      RescanStrategy InteferenceRescanStrategy = selectBestRescanStrategy(testSubProgram, regionsRequiringInteferenceCompensation);
      InteferenceGroups = InteferenceRescanStrategy.getGroups();
    }
    
    // 4. Create groups for global surface model (PSH)
    // It will be do together with scan path generation at latter stage
    
    // Siew Yeng - XCR-2140 - DRO
    // 5. Create groups for Dynamic Range Optimization Level
    //Map<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>> regionsWithDynamicRangeOptimizationList = new HashMap<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>>();
    //divideRegionsWithDynamicRangeOptimization(allInspectionRegions,regionsWithDynamicRangeOptimizationList);
    List<HomogeneousImageGroup> droLevelGroups = new ArrayList<HomogeneousImageGroup>();
    if(regionsWithDROList.isEmpty() == false)
    {
      for ( Collection<ReconstructionRegion> regionList : regionsWithDROList.values())
      {
        HomogeneousImageGroup group = new HomogeneousImageGroup(regionList, false);
        droLevelGroups.add(group);
      }
    }
    
    //Ngie Xing, add collection to get regions using DRO
    DynamicRangeOptimizationLevelEnum key = DynamicRangeOptimizationLevelEnum.ONE;
    if(mainGroup == null && droLevelGroups.size() > 0)
    {
      mainGroup = droLevelGroups.remove(0);
      key = regionsWithDROList.keySet().iterator().next();

    }
    // PART 2 - scanpath creation
    // ========================================================================
    // 1. Create scanpath for main
    createFixXScanPath(testSubProgram, mainGroup, key, scanPassSetting);
    
    // 2. Create scanpath for signal compensation groups and try to merge into main group
    List<HomogeneousImageGroup> toMaintainGroups = new ArrayList<HomogeneousImageGroup>();
    if(shadingCompensationGroups != null && shadingCompensationGroups.size()>0)
    {
      for(HomogeneousImageGroup group : shadingCompensationGroups)
      {
        createFixXScanPath(testSubProgram, group, scanPassSetting);
        
        if(forceMerge==true || isWorthToMerge(testSubProgram, mainGroup, group))
          mainGroup = mergeGroupsAndScanPaths(testSubProgram, mainGroup, group, true);
        else
          toMaintainGroups.add(group);
      }
    }
    _imageGroups.add(mainGroup);
    
    shadingCompensationGroups.clear();
    if(toMaintainGroups!=null && toMaintainGroups.size()>0)
    {
      // possible to merge within them?
      if(toMaintainGroups.size()>1)
        toMaintainGroups = attemptToMergeGroupsAndScanPaths(testSubProgram, toMaintainGroups, scanPassSetting);
                
      shadingCompensationGroups.addAll(toMaintainGroups);
      _imageGroups.addAll(shadingCompensationGroups);
    }
    
    // 3. Create scanpath for inteference compensation groups
    // Scanpath for inteference compensation group still been created using old way
    if (InteferenceGroups != null && InteferenceGroups.size() > 0)
    {
      for (HomogeneousImageGroup group : InteferenceGroups)
      {
        createScanPath(testSubProgram, group, scanPassSetting);
      }
    }
    
    // 4. Create scanpath for global surface model (PSH) groups
    SurfaceModelBreakPoint breakpoint = null;
    List<HomogeneousImageGroup> surfaceModelGroups = new ArrayList<HomogeneousImageGroup>();
    if (usingGlobalSurfaceModel() && regionsRequiringGlobalSurfaceModel.size() > 0)
    {
      ScanPass breakPointScanPass =  mainGroup.getScanPath().getScanPasses().get(0);
      
      if(Config.isRealTimePshEnabled()==false || testSubProgram.isSufficientMemoryForRealTimePsh()==false)
      {
        surfaceModelGroups = createFixXScanPaths(testSubProgram, regionsRequiringGlobalSurfaceModel, forceMerge, scanPassSetting);
        breakPointScanPass = surfaceModelGroups.get(0).getScanPath().getScanPasses().get(0);
        _imageGroups.addAll(surfaceModelGroups);
      }
      int allInspectionRegionsSize = testSubProgram.getAllScanPathReconstructionRegions().size();
      breakpoint = new SurfaceModelBreakPoint(testSubProgram,
                                              breakPointScanPass,
                                              surfaceModelGroups,
                                              allInspectionRegionsSize,
                                              scanPassSetting.getStageSpeedValue());
      
      for(ReconstructionRegion region : regionsRequiringGlobalSurfaceModel)
        region.setSurfaceModelBreakPoint(breakpoint);
    }
    
    //Siew Yeng - XCR-2140 - DRO
    // 5. Create scanpath for Dynamic Range Optimization Level groups
    if(droLevelGroups != null && droLevelGroups.size() > 0)
    {
      Iterator<DynamicRangeOptimizationLevelEnum> iterator = regionsWithDROList.keySet().iterator();
      for (HomogeneousImageGroup group : droLevelGroups)
      {
        createFixXScanPath(testSubProgram, group, iterator.next(), scanPassSetting);
      }
    }
    
    // PART 3 - add the scan route into testSubProgram
    // ========================================================================
    // 1. Add main + signal compensation and possible global surface model scanpath
    scanPaths.add(mainGroup.getScanPath());
        
    // 2. Add signal compensation scanpath
    if(shadingCompensationGroups != null && shadingCompensationGroups.size()>0)
    {
      for(HomogeneousImageGroup group : shadingCompensationGroups)
      {
        scanPaths.add(group.getScanPath());
      }
    }
    
    // 3. Add inteference compensation scanpath
    if (InteferenceGroups != null && InteferenceGroups.size() > 0)
    {
      for (ImageGroup group : InteferenceGroups)
      {
        scanPaths.add(group.getScanPath());
      }
    }
    
    // 4. Add global surface model (PSH) scanpath if no merge into main
    if ((surfaceModelGroups != null && surfaceModelGroups.size()>0) &&
        (Config.isRealTimePshEnabled()==false || testSubProgram.isSufficientMemoryForRealTimePsh()==false))
    {
      for(HomogeneousImageGroup group : surfaceModelGroups)
      {
        scanPaths.add(group.getScanPath());
      }
    }
    
    //Siew Yeng - XCR-2140 - DRO
    // 5. Add dynamic range optimization level (DRO) to testSubProgram
    if(droLevelGroups != null && droLevelGroups.size() > 0)
    {
      for(HomogeneousImageGroup group : droLevelGroups)
      {
        scanPaths.add(group.getScanPath());
      }
    }
    
    return scanPaths;
  }
  
  /**
   * @author Roy Williams
   * @author Kay Lannen
   * @author Poh Kheng
   * @author Lim, Seng Yew
   * 
   * If you make this method public (just in case),
   * please make sure you handle HomogeneousImageGroup.setScanPathMethod properly below,
   * which is setting a static variable in HomogeneousImageGroup.
   */
//  private ScanPath createScanPathForImageSetOrProduction(TestSubProgram testSubProgram) throws XrayTesterException
//  {
//    Assert.expect(testSubProgram != null);
//
//    // Make sure all HomogeneousImageGroup objects are created using same & correct method
//    ScanPathMethodEnum currentScanPathMethod = testSubProgram.getTestProgram().getProject().getScanPathMethod();
//    HomogeneousImageGroup.setScanPathMethod(currentScanPathMethod);
//    
//    // Reset memory limit control for Real Time PSH, false by default
//    testSubProgram.setMemorySufficientForRealTimePsh(false);
//    // Reset PSH region condition state, false by default
//    testSubProgram.clearIsolatedGlobalSurfaceRegion();
//    //XCR-3342, Setting IL with different value in Customize Alignment and Virtual Live does not take effect on the scan path
//    resetDefaultMeanStepSizeLimitInNanometers();
//    
//    // Check memory use up by PSH scan path
//    // ====================================
//    Collection<ReconstructionRegion> allInspectionRegions = testSubProgram.getAllScanPathReconstructionRegions();
//    Collection<ReconstructionRegion> regionsRequiringGlobalSurfaceModel = new ArrayList<ReconstructionRegion>();
//    if(Config.isRealTimePshEnabled())
//    {
//      divideRegionsRequiringGlobalSurfaceModel(allInspectionRegions, regionsRequiringGlobalSurfaceModel);
//      if(regionsRequiringGlobalSurfaceModel.size() > 0)
//      {
//        RescanStrategy rescanStrategy = selectBestRescanStrategy(testSubProgram, regionsRequiringGlobalSurfaceModel);
//        List<HomogeneousImageGroup> surfaceModelGroups = rescanStrategy.getGroups();
//
//        long maxMemoryUsed = 0;
//        long irpMemoryAllocatedForProjectionMemory = ImageReconstructionEngine.getMemoryAllocatedForProjectionMemory();
//        for (HomogeneousImageGroup group : surfaceModelGroups)
//        {
////          double range = getMaxStepSizeLimitInNanometers() + DISTANGE_THRESHOLD_IN_NM;
//          ScanPath scanPath = createScanPath(testSubProgram, group);
////          int maxScanPassNumber = getMaxNumberOfScanPassesWithinRanges(scanPath, range);
//          
//          long memoryUsed = calculateMemoryUsedforEachIRP(scanPath, scanPath.getScanPasses().size());
//          maxMemoryUsed = maxMemoryUsed + memoryUsed;
//          
//          // Perform data clean up on reconstruction region, and image group
//          for(Pair<ReconstructionRegion, SystemFiducialRectangle> pair : group.getRegionPairsSortedInExecutionOrder())
//          {
//            pair.getFirst().clearScanPasses();
//          }
//          _imageGroups.remove(group);
//        }
//        
//        // We merge PSH scan path into main scanpath as long as memory it consumed is less than total memory the IRP have.
//        if(maxMemoryUsed < irpMemoryAllocatedForProjectionMemory)
//        {
//          System.out.println("RT_PSH:: TestSubProgram: "+testSubProgram.getId());
//          System.out.println("RT_PSH:: Running RT PSH!");
//          testSubProgram.setMemorySufficientForRealTimePsh(true);
//        }
//        else
//        {
//          System.out.println("RT_PSH:: TestSubProgram: "+testSubProgram.getId());
//          System.out.println("RT_PSH:: Not Running RT PSH!!!!!");
//        }
//        
//        regionsRequiringGlobalSurfaceModel.clear();
//      }
//    }
//
//    // Divide ALL inspection regions into one of three buckets.
//    // o   ALL is very important.  Do not use "Filtered" because it will modify
//    //     the resulting inspection scanPath... arguably causing classification error.
//    // o  allInspectionRegions does NOT contain the alignment ReconstructionRegions.
//    Collection<ReconstructionRegion> regionsRequiringInteferenceCompensation = new ArrayList<ReconstructionRegion>();
//    Collection<ReconstructionRegion> regionsWithoutShadingCompensation = new ArrayList<ReconstructionRegion>();
//    Collection<ReconstructionRegion> regionsRequiringShadingCompensation = new ArrayList<ReconstructionRegion>();
//    Map<Integer,Collection<ReconstructionRegion>> regionsRequiringShadingCompensationList = new HashMap<Integer,Collection<ReconstructionRegion>>();
//    Collection<ReconstructionRegion> regionsWithDRO = new ArrayList<ReconstructionRegion>();
//    Map<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>> regionsWithDROList = new HashMap<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>>();
//    divideRegionsBaseOnAdvanceSettings(allInspectionRegions,
//                                       regionsRequiringGlobalSurfaceModel,
//                                       regionsRequiringInteferenceCompensation,
//                                       regionsWithoutShadingCompensation,
//                                       regionsRequiringShadingCompensationList,
//                                       regionsWithDROList,
//                                       true);
//    
//    // Only optimized if NOT using original scan path method.
//    if ((regionsWithoutShadingCompensation.size() < allInspectionRegions.size()/2) &&
//         (currentScanPathMethod.getId() >= ScanPathMethodEnum.METHOD_OPTIMIZED_LOADING.getId()))
//    {
//      int largestGroupId = SignalCompensationEnum.DEFAULT_LOW.getId();
//      int largestGroupCount = 0;
//      for ( Map.Entry<Integer,Collection<ReconstructionRegion>> regionPair : regionsRequiringShadingCompensationList.entrySet())
//      {
//        regionsRequiringShadingCompensation = regionPair.getValue();
//        if (regionsRequiringShadingCompensation.size() > largestGroupCount)
//        {
//          largestGroupId = regionsRequiringShadingCompensation.iterator().next().getSignalCompensation().getId();
//          largestGroupCount = regionsRequiringShadingCompensation.size();
//        }
//      }
//      if (_debug)
//        System.out.println("largestGroupId:" + largestGroupId + ",largestGroupCount:" + largestGroupCount);
//      if (largestGroupCount > regionsWithoutShadingCompensation.size())
//      {
//        //swap the largest group as main group
//        if (regionsWithoutShadingCompensation.size()>0)
//        {
//          regionsRequiringShadingCompensationList.put(SignalCompensationEnum.DEFAULT_LOW.getId(), regionsWithoutShadingCompensation);
//        }
//        regionsWithoutShadingCompensation = regionsRequiringShadingCompensationList.get(largestGroupId);
//        regionsRequiringShadingCompensationList.remove(largestGroupId);
//      }
//    }
//
//    // Dump out the current situation
//    if (_debug || (Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.IMAGING_CHAIN_PROGRAM_GENERATOR_SUMMARY_ONLY)))
//    {
//        System.out.println("Project name : " + testSubProgram.getTestProgram().getProject().getName());
//        System.out.println("Scanpath method : " + currentScanPathMethod);
//        System.out.println("TestSubProgram ID : " + testSubProgram.getId());
//        System.out.println("regionsRequiringGlobalSurfaceModel.size()  : " + regionsRequiringGlobalSurfaceModel.size());
//        System.out.println("regionsRequiringInteferenceCompensation.size()  : " + regionsRequiringInteferenceCompensation.size());
//        System.out.println("regionsWithoutShadingCompensation.size()   : " + regionsWithoutShadingCompensation.size());
//        System.out.println("regionsRequiringInteferenceCompensation.size() : " + regionsRequiringInteferenceCompensation.size());
//        System.out.println("regionsRequiringShadingCompensationList.size() : " + regionsRequiringShadingCompensationList.size());
//        for ( Map.Entry<Integer,Collection<ReconstructionRegion>> regionPair : regionsRequiringShadingCompensationList.entrySet())
//        {
//          System.out.println("  signalCompensation:" + regionPair.getKey() + ", size:" + regionPair.getValue().size());
//        }
//    }
//
//    regionsRequiringShadingCompensation = new ArrayList<ReconstructionRegion>();
//    for ( Collection<ReconstructionRegion> regionList : regionsRequiringShadingCompensationList.values())
//    {
//      regionsRequiringShadingCompensation.addAll(regionList);
//    }
//    
//    // put IC regions back into required shading regions
//    // Why we need this step?
//    // Because when we are comparing regions group size above, we don want IC to be mix within IL2, IL3 or IL4 group.
//    // So that we have a 'cleaner' group size to compare.
//    // We don want to have a group which apparently have largest size, but after taking IC out from it, it's actually not the largest.
//    if (regionsRequiringInteferenceCompensation.size() > 0)
//    {
//      regionsRequiringShadingCompensation.addAll(regionsRequiringInteferenceCompensation);
//    }
//    
//    // Create all groups for main, shading compensation, and global surface model.
//    if (_debug)
//      TimerUtil.printCurrentTime("Start selectBestRescanStrategy for regionsRequiringShadingCompensation");
//    RescanStrategy shadingRescanStrategy = selectBestRescanStrategy(testSubProgram, regionsRequiringShadingCompensation);
//    List<HomogeneousImageGroup> signalCompensatedGroups = shadingRescanStrategy.getGroups();
//    if (_debug)
//    {
//      TimerUtil.printCurrentTime("End selectBestRescanStrategy regionsRequiringShadingCompensation signalCompensatedGroups size:" + signalCompensatedGroups.size());
//      TimerUtil.printCurrentTime("End selectBestRescanStrategy regionsRequiringShadingCompensation shadingRescanStrategy type:" + shadingRescanStrategy.getScanStrategy().getId());
//    }
//
//    // Create main group
//    // If any regionsWithoutShadingCompensation, then we MUST promote one of the
//    // other (shading-compensated) groups to the "main" group.   Picking one is
//    // simply based upon the largest area to cover.
//    HomogeneousImageGroup mainAreaImageGroup = null;
//    if (regionsWithoutShadingCompensation.size() > 0)
//      mainAreaImageGroup = new HomogeneousImageGroup(regionsWithoutShadingCompensation);
//    else
//    {
//      //Siew Yeng - XCR-2140 - DRO
//      //fix crash when the only component set to test is set to use DRO
//      if(signalCompensatedGroups.size() > 0)
//        mainAreaImageGroup = extractLargestGroup(signalCompensatedGroups);
//    }
//    
//    if (_debug)
//    {
//      System.out.println("mainAreaImageGroup.getMeanStepSizeInNanoMeters():"+mainAreaImageGroup.getMeanStepSizeInNanoMeters());
//      System.out.println("mainAreaImageGroup.getDitherAmplitudeInNanoMeters():"+mainAreaImageGroup.getDitherAmplitudeInNanoMeters());
//      System.out.println("ImagingChainProgramGenerator.getMinStepSizeLimitInNanometers():"+ImagingChainProgramGenerator.getMinStepSizeLimitInNanometers());
//    }
//
//    List<HomogeneousImageGroup> surfaceModelGroups = null;
//    if (usingGlobalSurfaceModel() &&
//        regionsRequiringGlobalSurfaceModel.size() > 0)
//    {
//      if (_debug)
//        TimerUtil.printCurrentTime("Start selectBestRescanStrategy for regionsRequiringGlobalSurfaceModel");
//      RescanStrategy rescanStrategy = selectBestRescanStrategy(testSubProgram, regionsRequiringGlobalSurfaceModel);
//      surfaceModelGroups = rescanStrategy.getGroups();
//      if (_debug)
//      {
//        TimerUtil.printCurrentTime("End selectBestRescanStrategy regionsRequiringGlobalSurfaceModel surfaceModelGroups size:" + surfaceModelGroups.size());
//        TimerUtil.printCurrentTime("End selectBestRescanStrategy regionsRequiringGlobalSurfaceModel rescanStrategy type:" + rescanStrategy.getScanStrategy().getId());
//      }
//    }
//    
//    //Siew Yeng - XCR-2140 - DRO
//    //Map<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>> regionsWithDynamicRangeOptimizationList = new HashMap<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>>();
//    //divideRegionsWithDynamicRangeOptimization(allInspectionRegions,regionsWithDynamicRangeOptimizationList);
//    List<HomogeneousImageGroup> droLevelGroups  = new ArrayList<HomogeneousImageGroup>();
//    if(regionsWithDROList.isEmpty() == false)
//    {
//      for ( Collection<ReconstructionRegion> regionList : regionsWithDROList.values())
//      {
//        HomogeneousImageGroup group = new HomogeneousImageGroup(regionList);
//        droLevelGroups.add(group);
//      }
//    }
//    
//    // Create ScanPaths
//    if (_debug)
//      TimerUtil.printCurrentTime("Start mainAreaImageGroup imagezone size:" + mainAreaImageGroup.getImageZones().size());
//    
//    //Siew Yeng - XCR-2140 - DRO
//    ScanPath scanPathForMain = null;
//    //Ngie Xing, add collection to get regions using DRO
//    if(mainAreaImageGroup == null && droLevelGroups.size() > 0)
//    {
//      DynamicRangeOptimizationLevelEnum key = regionsWithDROList.keySet().iterator().next();
//      mainAreaImageGroup = droLevelGroups.remove(0);
//      scanPathForMain = createScanPath(testSubProgram, mainAreaImageGroup, key);
//      regionsWithDRO.addAll(regionsWithDROList.remove(key));
//    }
//    else
//      scanPathForMain = createScanPath(testSubProgram, mainAreaImageGroup);
//    
//    if (_debug)
//      TimerUtil.printCurrentTime("End mainAreaImageGroup scanpath size:" + scanPathForMain.getScanPasses().size());
//    if (signalCompensatedGroups != null && signalCompensatedGroups.size() > 0)
//    {
//      for (HomogeneousImageGroup group : signalCompensatedGroups)
//      {
//        if (_debug)
//          TimerUtil.printCurrentTime("Start group imagezone size:" + group.getImageZones().size());
//        createScanPath(testSubProgram, group);
//        if (_debug)
//          TimerUtil.printCurrentTime("End group scanpath size:" + group.getScanPath().getScanPasses().size());
//      }
//    }
//    // Khang Wah, 2013-06-17, New PSH handling
//    // disable below for temp
//    SurfaceModelBreakPoint breakPoint = null;
//    if (surfaceModelGroups != null)
//    {
//      if(Config.isRealTimePshEnabled() && testSubProgram.isSufficientMemoryForRealTimePsh())
//      {
//        // breakPointScanPass carry no meaning in this case, just pass a dummy to fullfil method recquirement.
//        ScanPass breakPointScanPass = scanPathForMain.getScanPasses().get(0);
//        breakPoint = new SurfaceModelBreakPoint(testSubProgram,
//                                                breakPointScanPass,
//                                                surfaceModelGroups,
//                                                allInspectionRegions.size());
//        
//        for (ReconstructionRegion region: regionsRequiringGlobalSurfaceModel)
//          region.setSurfaceModelBreakPoint(breakPoint);
//      }
//      else
//      {
//        if (_debug)
//          TimerUtil.printCurrentTime("Start GSM group size:" + surfaceModelGroups.size());
//        breakPoint = createGlobalSurfaceModelPaths(testSubProgram,
//                                                   surfaceModelGroups,
//                                                   allInspectionRegions,
//                                                   regionsWithoutShadingCompensation,
//                                                   regionsRequiringShadingCompensation);
//        if (_debug)
//          TimerUtil.printCurrentTime("End GSM group");
//      }
//    }
//
//    // Optimize by merging (some or all) signal compensated groups into main.
//    ImageGroup postMergeMainAreaImageGroup = mainAreaImageGroup;
//    if (signalCompensatedGroups.size() > 0)
//    {
//      // Now we do some optimization of the scan path.  Where it is faster, we will scan
//      // areas requiring signal compensation during the main pass.
//      if (hasStrategyToMergeRescanImageGroupsIntoMainImageGroup())
//      {
//        if (_debug)
//          TimerUtil.printCurrentTime("Start IC group - signalCompensatedGroups size:" + signalCompensatedGroups.size() + ",mainAreaImageGroup ImageZone size:"+mainAreaImageGroup.getImageZones().size());
//        MergeRescanGroupsIntoMainGroup mergeStrategy =
//            (MergeRescanGroupsIntoMainGroup)ScanStrategy.implementStrategy(ScanStrategyEnum.MERGE_TO_MAIN);
//        mergeStrategy.mergeToMain(mainAreaImageGroup, signalCompensatedGroups);
//        _imageGroups = mergeStrategy.getAllPostMergeImageGroups();
//        updateReconstructionRegionsForIntegratedScanPath(_imageGroups);
//        postMergeMainAreaImageGroup = mergeStrategy.getMergedMainImageGroup();
//        signalCompensatedGroups = mergeStrategy.getUnmergedRescanGroups();
//        if (_debug)
//        {
//          TimerUtil.printCurrentTime("End IC group main group size:" + postMergeMainAreaImageGroup.getScanPath().getScanPasses().size());
//          TimerUtil.printCurrentTime("End IC group unmerged group size:" + signalCompensatedGroups.size());
//        }
//      }
//    }
//
//    // Add the scanPaths to TestSubProgram for main and signal compensated groups.
//    scanPathForMain = postMergeMainAreaImageGroup.getScanPath();
//    testSubProgram.addScanPathOverInspectionOrVerificationArea(postMergeMainAreaImageGroup.getScanPath());
//    for (ImageGroup group : signalCompensatedGroups)
//      testSubProgram.addScanPathOverInspectionOrVerificationArea(group.getScanPath());
//    // Khang Wah, 2013-06-17, New PSH handling
//    // disable below for 64bit irp
//    if (surfaceModelGroups != null && 
//        (Config.isRealTimePshEnabled()==false || testSubProgram.isSufficientMemoryForRealTimePsh()==false))
//    {
//      for (ImageGroup group : surfaceModelGroups)
//        testSubProgram.addScanPathOverInspectionOrVerificationArea(group.getScanPath());
//    }
//    
//    //Siew Yeng - XCR-2140 - DRO
//    if(droLevelGroups != null && droLevelGroups.size() > 0)
//    {
//      Iterator<DynamicRangeOptimizationLevelEnum> iterator = regionsWithDROList.keySet().iterator();
//      for (HomogeneousImageGroup group : droLevelGroups)
//      {
//        createScanPath(testSubProgram, group, iterator.next());
//        testSubProgram.addScanPathOverInspectionOrVerificationArea(group.getScanPath());
//        //Ngie Xing, add all regions into collection
//        regionsWithDRO.addAll(group.getUnsortedRegions());
//      }
//    }
//    
//    //  Add the scanPaths to TestSubProgram for alignment regions
//    if (_debug)
//      TimerUtil.printCurrentTime("Start merge alignment to main scanpath");
//    List<ScanPath> alignmentScanPaths = mergeAlignmentToScanPath(
//      testSubProgram,
//      mainAreaImageGroup,
//      scanPathForMain);
//    if (_debug)
//      TimerUtil.printCurrentTime("End merge alignment to main scanpath");
//    testSubProgram.setAlignmentScanPaths(alignmentScanPaths);
//    
//    // only do this for 64bit IRP
//    if (surfaceModelGroups != null && Config.isRealTimePshEnabled() && testSubProgram.isSufficientMemoryForRealTimePsh())
//    {
//      Collection<ReconstructionRegion> mainRegions = new ArrayList<ReconstructionRegion>();
//      
//      mainRegions.addAll(regionsWithoutShadingCompensation);
//      mainRegions.addAll(regionsRequiringShadingCompensation);
//      mainRegions.addAll(regionsRequiringInteferenceCompensation);
//      //Ngie Xing, add all DRO regions to mainRegions
//      mainRegions.addAll(regionsWithDRO);
//      
//      HomogeneousImageGroup mainGroup = new HomogeneousImageGroup(mainRegions);
//      HomogeneousImageGroup surfaceModelGroup = new HomogeneousImageGroup(regionsRequiringGlobalSurfaceModel);
//      
//      Collection<ReconstructionRegion> regionsWithoutNeighbor = setupNeighborAndTriggerRegionForGlobalSurfaceModalRegion(mainGroup, surfaceModelGroup);
//      
//      if(regionsWithoutNeighbor.size()>0)
//      {
//        testSubProgram.getIsolatedGlobalSurfaceRegion().addAll(regionsWithoutNeighbor);
//      }
//    }
//    return scanPathForMain;
//  }
  
  /**
   * @author Chnee Khang Wah, 2016-05-06, MVEDR
   */
  private ScanPath createScanPathForImageSetOrProductionMVEDR(TestSubProgram testSubProgram) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);

    // Make sure all HomogeneousImageGroup objects are created using same & correct method
    ScanPathMethodEnum currentScanPathMethod = testSubProgram.getTestProgram().getProject().getScanPathMethod();
    HomogeneousImageGroup.setScanPathMethod(currentScanPathMethod);
    
    // Reset memory limit control for Real Time PSH, false by default
    testSubProgram.setMemorySufficientForRealTimePsh(false);
    // Reset PSH region condition state, false by default
    testSubProgram.clearIsolatedGlobalSurfaceRegion();
    //XCR-3342, Setting IL with different value in Customize Alignment and Virtual Live does not take effect on the scan path
    resetDefaultMeanStepSizeLimitInNanometers();
    
    // Check memory use up by PSH scan path
    // ====================================
    Collection<ReconstructionRegion> allInspectionRegions = testSubProgram.getAllScanPathReconstructionRegions();
    Collection<ReconstructionRegion> regionsRequiringGlobalSurfaceModel = new ArrayList<ReconstructionRegion>();
    if(Config.isRealTimePshEnabled())
    {
      testSubProgram.setMemorySufficientForRealTimePsh(true); // make it a must for MVEDR
      /*
      divideRegionsRequiringGlobalSurfaceModel(allInspectionRegions, regionsRequiringGlobalSurfaceModel);
      if(regionsRequiringGlobalSurfaceModel.size() > 0)
      {
        List<HomogeneousImageGroup> surfaceModelGroups = new ArrayList<HomogeneousImageGroup>();
        // Loop through all user gain and stage speed
        for (StageSpeedEnum stageSpeedEnum : StageSpeedEnum.getOrderedStageSpeed()) 
        {
          for(UserGainEnum userGainEnum : UserGainEnum.getOrderedUserGain())
          {
            Collection<ReconstructionRegion> regions = extractRegionsWithCertainHardwareSettings(userGainEnum, 
                                                                                                 stageSpeedEnum, 
                                                                                                 regionsRequiringGlobalSurfaceModel);
            
            if(regions.size()>0)
            {
              RescanStrategy rescanStrategy = selectBestRescanStrategy(testSubProgram, regions);
              surfaceModelGroups.addAll(rescanStrategy.getGroups());
            }
          } 
        }
        
        long maxMemoryUsed = 0;
        long irpMemoryAllocatedForProjectionMemory = ImageReconstructionEngine.getMemoryAllocatedForProjectionMemory();
        for (HomogeneousImageGroup group : surfaceModelGroups)
        {
//        double range = getMaxStepSizeLimitInNanometers() + DISTANGE_THRESHOLD_IN_NM;
          ScanPath scanPath = createScanPath(testSubProgram, group);
//        int maxScanPassNumber = getMaxNumberOfScanPassesWithinRanges(scanPath, range);

          long memoryUsed = calculateMemoryUsedforEachIRP(scanPath, scanPath.getScanPasses().size());
          maxMemoryUsed = maxMemoryUsed + memoryUsed;
          
          // Perform data clean up on reconstruction region, and image group
          for(Pair<ReconstructionRegion, SystemFiducialRectangle> pair : group.getRegionPairsSortedInExecutionOrder())
          {
            pair.getFirst().clearScanPasses();
          }
          _imageGroups.remove(group);
        }
        
        // We merge PSH scan path into main scanpath as long as memory it consumed is less than total memory the IRP have.
        if(maxMemoryUsed < irpMemoryAllocatedForProjectionMemory)
        {
          System.out.println("RT_PSH:: TestSubProgram: "+testSubProgram.getId());
          System.out.println("RT_PSH:: Running RT PSH!");
          testSubProgram.setMemorySufficientForRealTimePsh(true);
        }
        else
        {
          System.out.println("RT_PSH:: TestSubProgram: "+testSubProgram.getId());
          System.out.println("RT_PSH:: Not Running RT PSH!!!!!");
        }
        
        regionsRequiringGlobalSurfaceModel.clear();
      }
      */
    }
    
    // Regions checking
    Collection<ReconstructionRegion> regionsRequiringInteferenceCompensation = new ArrayList<ReconstructionRegion>();
    Collection<ReconstructionRegion> regionsWithoutShadingCompensation = new ArrayList<ReconstructionRegion>();
    Collection<ReconstructionRegion> regionsRequiringShadingCompensation = new ArrayList<ReconstructionRegion>();
    Map<Integer,Collection<ReconstructionRegion>> regionsRequiringShadingCompensationList = new HashMap<Integer,Collection<ReconstructionRegion>>();
    Collection<ReconstructionRegion> regionsWithDRO = new ArrayList<ReconstructionRegion>();
    Map<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>> regionsWithDROList = new HashMap<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>>();
    divideRegionsBaseOnAdvanceSettings(allInspectionRegions,
                                       regionsRequiringGlobalSurfaceModel,
                                       regionsRequiringInteferenceCompensation,
                                       regionsWithoutShadingCompensation,
                                       regionsRequiringShadingCompensationList,
                                       regionsWithDROList,
                                       true);
    
    if (regionsRequiringShadingCompensationList.isEmpty()==false)
    {
      for (Collection<ReconstructionRegion> regionList : regionsRequiringShadingCompensationList.values())
        regionsRequiringShadingCompensation.addAll(regionList);
    }
    
    if(regionsWithDROList.isEmpty() == false)
    {
      for ( Collection<ReconstructionRegion> regionList : regionsWithDROList.values())
        regionsWithDRO.addAll(regionList);
    }
    
    // Proceed to scan path generation for all hardware settings
    ScanPath scanPathForMain = createScanPathForTestSubProgram(testSubProgram, allInspectionRegions);
    
    // only do this for 64bit IRP
    if (regionsRequiringGlobalSurfaceModel.isEmpty() == false && Config.isRealTimePshEnabled() && testSubProgram.isSufficientMemoryForRealTimePsh())
    {
      Collection<ReconstructionRegion> mainRegions = new ArrayList<ReconstructionRegion>();
      
      mainRegions.addAll(regionsWithoutShadingCompensation);
      mainRegions.addAll(regionsRequiringShadingCompensation);
      mainRegions.addAll(regionsRequiringInteferenceCompensation);
      mainRegions.addAll(regionsWithDRO);
      
      HomogeneousImageGroup mainGroup = new HomogeneousImageGroup(mainRegions);
      HomogeneousImageGroup surfaceModelGroup = new HomogeneousImageGroup(regionsRequiringGlobalSurfaceModel);
      
      Collection<ReconstructionRegion> regionsWithoutNeighbor = setupNeighborAndTriggerRegionForGlobalSurfaceModalRegion(mainGroup, surfaceModelGroup);
      
      if(regionsWithoutNeighbor.isEmpty() == false)
      {
        testSubProgram.getIsolatedGlobalSurfaceRegion().addAll(regionsWithoutNeighbor);
      }
    }
    return scanPathForMain;
  }
  
  /**
   * @author YOng Sheng Chuan
   */
  private ScanPath createScanPathForTestSubProgram(TestSubProgram testSubProgram, 
                                                   Collection<ReconstructionRegion> allInspectionRegions) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(allInspectionRegions != null);
    
    Collection<Collection<ScanPath>> totalScanPathsList = new ArrayList<Collection<ScanPath>>();
    
    // Proceed to scan path generation for all hardware settings
    ScanPath scanPathForMain = null; //  Add the scanPaths to TestSubProgram for alignment regions
    for (StageSpeedEnum stageSpeedEnum : StageSpeedEnum.getOrderedStageSpeed())
    {
      List<ScanPass> scanPasses = new ArrayList<ScanPass>();

      for (UserGainEnum userGainEnum : UserGainEnum.getOrderedUserGain())
      {
        Collection<ReconstructionRegion> regions = extractRegionsWithCertainHardwareSettings(userGainEnum,
          stageSpeedEnum,
          allInspectionRegions);

        if (regions.isEmpty() == false)
        {
          ScanPassSetting scanPassSetting = new ScanPassSetting(stageSpeedEnum.toDouble(),
                                                                userGainEnum.toDouble(),
                                                                testSubProgram.isLowMagnification());
          List<ScanPath> scanPaths = createScanPathForSpecificHardwareSettings(testSubProgram,
                                                                               regions,
                                                                               scanPassSetting);
          // Will not create breakpoint for first stage speed at this point
          // need to consider alignment scan path, will do that after alignment merging
          ScanPass scanPassForUserGainBreakPoint = null;
          if (totalScanPathsList.isEmpty())
          {
            scanPathForMain = scanPaths.iterator().next();
            // The first image group in the list is mainAreaImageGroup
            HomogeneousImageGroup mainAreaImageGroup = new HomogeneousImageGroup(_imageGroups.get(0).getUnsortedRegions());

            boolean isCustomizedAlignmentPathSetting = false;
            if (testSubProgram.isSubProgramRequiredCustomAlignmentPathGeneration() || 
                stageSpeedEnum.equals(testSubProgram.getAlignmentStageSpeed()) == false ||
                userGainEnum.equals(testSubProgram.getAlignmentUserGain()) == false)
            {
              isCustomizedAlignmentPathSetting = true;
            }

            List<ScanPath> alignmentScanPaths = mergeAlignmentToScanPath(testSubProgram,
                                                                         mainAreaImageGroup,
                                                                         scanPathForMain,
                                                                         isCustomizedAlignmentPathSetting);
            testSubProgram.setAlignmentScanPaths(alignmentScanPaths);
            if (isCustomizedAlignmentPathSetting)
            {
              StageSpeedEnum stageSpeedForAlignment = VirtualLiveManager.getInstance().isVirtualLiveMode() ? testSubProgram.getVirtualLiveAlignmentStageSpeed() : testSubProgram.getAlignmentStageSpeed();
              UserGainEnum userGainForAlignment = VirtualLiveManager.getInstance().isVirtualLiveMode() ? testSubProgram.getVirtualLiveAlignmentUserGain() : testSubProgram.getAlignmentUserGain();
              List<ScanPass> alignmentScanPasses = new ArrayList<ScanPass>();
              for (ScanPath scanPath : alignmentScanPaths)
                alignmentScanPasses.addAll(scanPath.getScanPasses());

              new UserGainBreakPoint(alignmentScanPasses.get(0),
                                     _REGION_TO_MARK_COMPLETED_FOR_MOTION_BREAKPOINT,
                                     userGainForAlignment.toDouble(),
                                     stageSpeedForAlignment.toDouble());

              new StageSpeedBreakPoint(alignmentScanPasses.get(0),
                                       alignmentScanPasses,
                                       _REGION_TO_MARK_COMPLETED_FOR_MOTION_BREAKPOINT,
                                       stageSpeedForAlignment,
                                       testSubProgram.getMagnificationType());
            }
            else
            {
              for (ScanPath scanPath : alignmentScanPaths)
                scanPasses.addAll(scanPath.getScanPasses());
            }

            if (scanPasses.isEmpty() == false)
              scanPassForUserGainBreakPoint = scanPasses.get(0);
            else
              scanPassForUserGainBreakPoint = scanPathForMain.getScanPasses().get(0);
          }
          else
            scanPassForUserGainBreakPoint = scanPaths.iterator().next().getScanPasses().get(0);

          new UserGainBreakPoint(scanPassForUserGainBreakPoint,
                                 _REGION_TO_MARK_COMPLETED_FOR_MOTION_BREAKPOINT,
                                 userGainEnum.toDouble(),
                                 stageSpeedEnum.toDouble());

          for (ScanPath scanPath : scanPaths)
            scanPasses.addAll(scanPath.getScanPasses());

          totalScanPathsList.add(scanPaths);
        }
      }

      if (scanPasses.isEmpty() == false)
      {
        new StageSpeedBreakPoint(scanPasses.get(0),
                                 scanPasses,
                                 _REGION_TO_MARK_COMPLETED_FOR_MOTION_BREAKPOINT,
                                 stageSpeedEnum,
                                 testSubProgram.getMagnificationType());
      }
    }
    // Add all scan passes into testSubProgram by sequence
    for (Collection<ScanPath> scanPaths : totalScanPathsList)
    {
      for(ScanPath scanPath : scanPaths)
        testSubProgram.addScanPathOverInspectionOrVerificationArea(scanPath);
    }
    return scanPathForMain;
  }
  
  /**
   * @author Khang Wah, 2016-05-06, MVEDR
   */
  private Collection<ReconstructionRegion> extractRegionsWithCertainHardwareSettings(UserGainEnum userGainEnum, 
                                                                                     StageSpeedEnum stageSpeedEnum, 
                                                                                     Collection<ReconstructionRegion> regions)
  {
    Assert.expect(userGainEnum != null);
    Assert.expect(stageSpeedEnum != null);
    Assert.expect(regions != null);
    
    Collection<ReconstructionRegion> extractedRegions = new ArrayList<ReconstructionRegion>();
    
    for(ReconstructionRegion region : regions)
    {
      if(region.getUserGainEnum().equals(userGainEnum) && region.getStageSpeedList().contains(stageSpeedEnum))
        extractedRegions.add(region);
    }
    
    return extractedRegions;
  }
  
  /**
   * @author Khang Wah, 2016-05-06, MVEDR
   */
  private List<ScanPath> createScanPathForSpecificHardwareSettings(TestSubProgram testSubProgram, 
                                                                   Collection<ReconstructionRegion> allInspectionRegions,
                                                                   ScanPassSetting scanPassSetting) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(allInspectionRegions != null);
    Assert.expect(scanPassSetting != null);
    
    ScanPathMethodEnum currentScanPathMethod = testSubProgram.getTestProgram().getProject().getScanPathMethod();
    List<ScanPath> scanPaths = new ArrayList<ScanPath>();
    
    // Divide ALL inspection regions into one of three buckets.
    // o   ALL is very important.  Do not use "Filtered" because it will modify
    //     the resulting inspection scanPath... arguably causing classification error.
    // o  allInspectionRegions does NOT contain the alignment ReconstructionRegions.
    Collection<ReconstructionRegion> regionsRequiringGlobalSurfaceModel = new ArrayList<ReconstructionRegion>();
    Collection<ReconstructionRegion> regionsRequiringInteferenceCompensation = new ArrayList<ReconstructionRegion>();
    Collection<ReconstructionRegion> regionsWithoutShadingCompensation = new ArrayList<ReconstructionRegion>();
    Collection<ReconstructionRegion> regionsRequiringShadingCompensation;
    Map<Integer,Collection<ReconstructionRegion>> regionsRequiringShadingCompensationList = new HashMap<Integer,Collection<ReconstructionRegion>>();
    Map<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>> regionsWithDROList = new HashMap<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>>();
    divideRegionsBaseOnAdvanceSettings(allInspectionRegions,
                                       regionsRequiringGlobalSurfaceModel,
                                       regionsRequiringInteferenceCompensation,
                                       regionsWithoutShadingCompensation,
                                       regionsRequiringShadingCompensationList,
                                       regionsWithDROList,
                                       false);
    
    // Only optimized if NOT using original scan path method.
    if ((regionsWithoutShadingCompensation.size() < allInspectionRegions.size()/2) &&
         (currentScanPathMethod.getId() >= ScanPathMethodEnum.METHOD_OPTIMIZED_LOADING.getId()))
    {
      int largestGroupId = SignalCompensationEnum.DEFAULT_LOW.getId();
      int largestGroupCount = 0;
      for ( Map.Entry<Integer,Collection<ReconstructionRegion>> regionPair : regionsRequiringShadingCompensationList.entrySet())
      {
        regionsRequiringShadingCompensation = regionPair.getValue();
        if (regionsRequiringShadingCompensation.size() > largestGroupCount)
        {
          largestGroupId = regionsRequiringShadingCompensation.iterator().next().getSignalCompensation().getId();
          largestGroupCount = regionsRequiringShadingCompensation.size();
        }
      }
      if (_debug)
        System.out.println("largestGroupId:" + largestGroupId + ",largestGroupCount:" + largestGroupCount);
      if (largestGroupCount > regionsWithoutShadingCompensation.size())
      {
        //swap the largest group as main group
        if (regionsWithoutShadingCompensation.size()>0)
        {
          regionsRequiringShadingCompensationList.put(SignalCompensationEnum.DEFAULT_LOW.getId(), regionsWithoutShadingCompensation);
        }
        regionsWithoutShadingCompensation = regionsRequiringShadingCompensationList.get(largestGroupId);
        regionsRequiringShadingCompensationList.remove(largestGroupId);
      }
    }

    // Dump out the current situation
    if (_debug || (Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.IMAGING_CHAIN_PROGRAM_GENERATOR_SUMMARY_ONLY)))
    {
        System.out.println("Project name : " + testSubProgram.getTestProgram().getProject().getName());
        System.out.println("Scanpath method : " + currentScanPathMethod);
        System.out.println("TestSubProgram ID : " + testSubProgram.getId());
        System.out.println("regionsRequiringGlobalSurfaceModel.size()  : " + regionsRequiringGlobalSurfaceModel.size());
        System.out.println("regionsRequiringInteferenceCompensation.size()  : " + regionsRequiringInteferenceCompensation.size());
        System.out.println("regionsWithoutShadingCompensation.size()   : " + regionsWithoutShadingCompensation.size());
        System.out.println("regionsRequiringInteferenceCompensation.size() : " + regionsRequiringInteferenceCompensation.size());
        System.out.println("regionsRequiringShadingCompensationList.size() : " + regionsRequiringShadingCompensationList.size());
        for ( Map.Entry<Integer,Collection<ReconstructionRegion>> regionPair : regionsRequiringShadingCompensationList.entrySet())
        {
          System.out.println("  signalCompensation:" + regionPair.getKey() + ", size:" + regionPair.getValue().size());
        }
    }

    regionsRequiringShadingCompensation = new ArrayList<ReconstructionRegion>();
    for ( Collection<ReconstructionRegion> regionList : regionsRequiringShadingCompensationList.values())
    {
      regionsRequiringShadingCompensation.addAll(regionList);
    }
    
    // put IC regions back into required shading regions
    // Why we need this step?
    // Because when we are comparing regions group size above, we don want IC to be mix within IL2, IL3 or IL4 group.
    // So that we have a 'cleaner' group size to compare.
    // We don want to have a group which apparently have largest size, but after taking IC out from it, it's actually not the largest.
    if (regionsRequiringInteferenceCompensation.size() > 0)
    {
      regionsRequiringShadingCompensation.addAll(regionsRequiringInteferenceCompensation);
    }
    
    // Create all groups for main, shading compensation, and global surface model.
    if (_debug)
      TimerUtil.printCurrentTime("Start selectBestRescanStrategy for regionsRequiringShadingCompensation");
    RescanStrategy shadingRescanStrategy = selectBestRescanStrategy(testSubProgram, regionsRequiringShadingCompensation);
    List<HomogeneousImageGroup> signalCompensatedGroups = shadingRescanStrategy.getGroups();
    if (_debug)
    {
      TimerUtil.printCurrentTime("End selectBestRescanStrategy regionsRequiringShadingCompensation signalCompensatedGroups size:" + signalCompensatedGroups.size());
      TimerUtil.printCurrentTime("End selectBestRescanStrategy regionsRequiringShadingCompensation shadingRescanStrategy type:" + shadingRescanStrategy.getScanStrategy().getId());
    }

    // Create main group
    // If any regionsWithoutShadingCompensation, then we MUST promote one of the
    // other (shading-compensated) groups to the "main" group.   Picking one is
    // simply based upon the largest area to cover.
    HomogeneousImageGroup mainAreaImageGroup = null;
    if (regionsWithoutShadingCompensation.size() > 0)
      mainAreaImageGroup = new HomogeneousImageGroup(regionsWithoutShadingCompensation);
    else
    {
      //Siew Yeng - XCR-2140 - DRO
      //fix crash when the only component set to test is set to use DRO
      if (signalCompensatedGroups.size() > 0)
        mainAreaImageGroup = extractLargestGroup(signalCompensatedGroups);
    }
    
    if (_debug)
    {
      System.out.println("mainAreaImageGroup.getMeanStepSizeInNanoMeters():"+mainAreaImageGroup.getMeanStepSizeInNanoMeters());
      System.out.println("mainAreaImageGroup.getDitherAmplitudeInNanoMeters():"+mainAreaImageGroup.getDitherAmplitudeInNanoMeters());
      System.out.println("ImagingChainProgramGenerator.getMinStepSizeLimitInNanometers():"+ImagingChainProgramGenerator.getMinStepSizeLimitInNanometers());
    }

    List<HomogeneousImageGroup> surfaceModelGroups = null;
    if (usingGlobalSurfaceModel() &&
        regionsRequiringGlobalSurfaceModel.size() > 0)
    {
      if (_debug)
        TimerUtil.printCurrentTime("Start selectBestRescanStrategy for regionsRequiringGlobalSurfaceModel");
      RescanStrategy rescanStrategy = selectBestRescanStrategy(testSubProgram, regionsRequiringGlobalSurfaceModel);
      surfaceModelGroups = rescanStrategy.getGroups();
      if (_debug)
      {
        TimerUtil.printCurrentTime("End selectBestRescanStrategy regionsRequiringGlobalSurfaceModel surfaceModelGroups size:" + surfaceModelGroups.size());
        TimerUtil.printCurrentTime("End selectBestRescanStrategy regionsRequiringGlobalSurfaceModel rescanStrategy type:" + rescanStrategy.getScanStrategy().getId());
      }
    }
    
    //Siew Yeng - XCR-2140 - DRO
    //Map<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>> regionsWithDynamicRangeOptimizationList = new HashMap<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>>();
    //divideRegionsWithDynamicRangeOptimization(allInspectionRegions,regionsWithDynamicRangeOptimizationList);
    List<HomogeneousImageGroup> droLevelGroups  = new ArrayList<HomogeneousImageGroup>();
    if(regionsWithDROList.isEmpty() == false)
    {
      if(regionsWithDROList.isEmpty() == false)
      {
        for ( Collection<ReconstructionRegion> regionList : regionsWithDROList.values())
        {
          HomogeneousImageGroup group = new HomogeneousImageGroup(regionList);
          droLevelGroups.add(group);
        }
      }
    }
    
    // Create ScanPaths
    if (_debug)
      TimerUtil.printCurrentTime("Start mainAreaImageGroup imagezone size:" + mainAreaImageGroup.getImageZones().size());
    
    // Siew Yeng - XCR-2140 - DRO
    ScanPath scanPathForMain = null;
    DynamicRangeOptimizationLevelEnum key = DynamicRangeOptimizationLevelEnum.ONE;
    if(mainAreaImageGroup == null && droLevelGroups.size() > 0)
    {
      key = regionsWithDROList.keySet().iterator().next();
      mainAreaImageGroup = droLevelGroups.remove(0);
    }
    
    scanPathForMain = createScanPath(testSubProgram, mainAreaImageGroup, key, scanPassSetting);
    
    if (_debug)
      TimerUtil.printCurrentTime("End mainAreaImageGroup scanpath size:" + scanPathForMain.getScanPasses().size());
    if (signalCompensatedGroups != null && signalCompensatedGroups.size() > 0)
    {
      for (HomogeneousImageGroup group : signalCompensatedGroups)
      {
        if (_debug)
          TimerUtil.printCurrentTime("Start group imagezone size:" + group.getImageZones().size());
        createScanPath(testSubProgram, group, scanPassSetting);
        if (_debug)
          TimerUtil.printCurrentTime("End group scanpath size:" + group.getScanPath().getScanPasses().size());
      }
    }
    // Khang Wah, 2013-06-17, New PSH handling
    // disable below for temp
    int allInspectionRegionsSize = testSubProgram.getAllScanPathReconstructionRegions().size();
    SurfaceModelBreakPoint breakPoint = null;
    if (surfaceModelGroups != null)
    {
      if(Config.isRealTimePshEnabled() && testSubProgram.isSufficientMemoryForRealTimePsh())
      {
        // breakPointScanPass carry no meaning in this case, just pass a dummy to fullfil method recquirement.
        ScanPass breakPointScanPass = scanPathForMain.getScanPasses().get(0);
        breakPoint = new SurfaceModelBreakPoint(testSubProgram,
                                                breakPointScanPass,
                                                surfaceModelGroups,
                                                allInspectionRegionsSize,
                                                scanPassSetting.getStageSpeedValue());
        
        for (ReconstructionRegion region: regionsRequiringGlobalSurfaceModel)
          region.setSurfaceModelBreakPoint(breakPoint);
      }
      else
      {
        if (_debug)
          TimerUtil.printCurrentTime("Start GSM group size:" + surfaceModelGroups.size());
        breakPoint = createGlobalSurfaceModelPaths(testSubProgram,
                                                   surfaceModelGroups,
                                                   allInspectionRegions,
                                                   regionsWithoutShadingCompensation,
                                                   regionsRequiringShadingCompensation,
                                                   scanPassSetting);
        if (_debug)
          TimerUtil.printCurrentTime("End GSM group");
      }
    }

    // Optimize by merging (some or all) signal compensated groups into main.
    ImageGroup postMergeMainAreaImageGroup = mainAreaImageGroup;
    if (signalCompensatedGroups.size() > 0)
    {
      // Now we do some optimization of the scan path.  Where it is faster, we will scan
      // areas requiring signal compensation during the main pass.
      if (hasStrategyToMergeRescanImageGroupsIntoMainImageGroup())
      {
        if (_debug)
          TimerUtil.printCurrentTime("Start IC group - signalCompensatedGroups size:" + signalCompensatedGroups.size() + ",mainAreaImageGroup ImageZone size:"+mainAreaImageGroup.getImageZones().size());
        MergeRescanGroupsIntoMainGroup mergeStrategy =
            (MergeRescanGroupsIntoMainGroup)ScanStrategy.implementStrategy(ScanStrategyEnum.MERGE_TO_MAIN);
        mergeStrategy.mergeToMain(mainAreaImageGroup, signalCompensatedGroups);
        _imageGroups = mergeStrategy.getAllPostMergeImageGroups();
        updateReconstructionRegionsForIntegratedScanPath(_imageGroups);
        postMergeMainAreaImageGroup = mergeStrategy.getMergedMainImageGroup();
        signalCompensatedGroups = mergeStrategy.getUnmergedRescanGroups();
        if (_debug)
        {
          TimerUtil.printCurrentTime("End IC group main group size:" + postMergeMainAreaImageGroup.getScanPath().getScanPasses().size());
          TimerUtil.printCurrentTime("End IC group unmerged group size:" + signalCompensatedGroups.size());
        }
      }
    }

    // Add the scanPaths to ScanPathsList for all image groups.
    scanPaths.add(postMergeMainAreaImageGroup.getScanPath());
    for (ImageGroup group : signalCompensatedGroups)
      scanPaths.add(group.getScanPath());
    // Khang Wah, 2013-06-17, New PSH handling
    // disable below for 64bit irp
    if (surfaceModelGroups != null && 
        (Config.isRealTimePshEnabled()==false || testSubProgram.isSufficientMemoryForRealTimePsh()==false))
    {
      for (ImageGroup group : surfaceModelGroups)
        scanPaths.add(group.getScanPath());
    }
    
    //Siew Yeng - XCR-2140 - DRO
    if(droLevelGroups != null && droLevelGroups.size() > 0)
    {
      Iterator<DynamicRangeOptimizationLevelEnum> iterator = regionsWithDROList.keySet().iterator();
      for (HomogeneousImageGroup group : droLevelGroups)
      {
        createScanPath(testSubProgram, group, iterator.next(), scanPassSetting);
        scanPaths.add(group.getScanPath());
      }
    }
    
    return scanPaths;
  }

   /**
   * @author Roy Williams
   */
  private ScanPath createScanPathForCouponOrVerification(
      TestSubProgram testSubProgram,
      double zHeightDeltaAboveNominalInNanometers,
      double zHeightDeltaBelowNominalInNanometers) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);

    List<ProcessorStrip> processorStrips = getProcessorStripsFromTestSubProgam(testSubProgram, _acquisitionMode);

    // Get the area to image relative to the homed stage
    PanelRectangle panelRectangle = getPanelRectangle(testSubProgram);
    Pair<SystemFiducialRectangle, AffineTransform> pair =
      getAreaToImageRelativeToSystemFiducial(testSubProgram);
    MechanicalConversions mechanicalConversions = new MechanicalConversions(
        processorStrips,
        pair.getSecond(),
        Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
        ProcessorStrip.getOverlapNeededForLargestPossibleJointInNanometers(),
        zHeightDeltaAboveNominalInNanometers,
        zHeightDeltaBelowNominalInNanometers,
        _xRayCameras,
        pair.getFirst());
    List<ScanPass> scanPasses = generateScanPathOverArea(mechanicalConversions,
                                                         _defaultMeanStepSizeLimitInNanometers,
                                                         XrayTester.getMaxStepSizeDitherAmplitudeInNanometers());
    
    new UserGainBreakPoint(scanPasses.get(0), 
                           _REGION_TO_MARK_COMPLETED_FOR_MOTION_BREAKPOINT, 
                           testSubProgram.getVerificationUserGain().toDouble(),
                           testSubProgram.getVerificationStageSpeed().toDouble());
      
    new StageSpeedBreakPoint(scanPasses.get(0),
                             scanPasses,
                             _REGION_TO_MARK_COMPLETED_FOR_MOTION_BREAKPOINT,
                             testSubProgram.getVerificationStageSpeed(),
                             testSubProgram.getMagnificationType());
    // iterator over all reconstruction regions in this test sub program
    // and add all scan passes to as elements for the sceduling calculation
    // performed by the IRP to the reconstruction regions.
    for(ReconstructionRegion reconstructionRegion: testSubProgram.getVerificationRegions())
    {
      reconstructionRegion.clearScanPasses();
      for (ScanPass scanPass : scanPasses)
      {
        reconstructionRegion.addScanPass(scanPass);
      }
    }

    ScanPath scanPathForMain = new ScanPath(mechanicalConversions, scanPasses);
    testSubProgram.addScanPathOverInspectionOrVerificationArea(scanPathForMain);
    updateImageReconstructionEnginesInUseBasedUponAdditionalScanPathInfo(scanPathForMain);

    return scanPathForMain;
  }


  /**
   * @author Chnee Khang Wah, 2013-11-06, Real Time PSH 
   */
  private void divideRegionsRequiringGlobalSurfaceModel(
      Collection<ReconstructionRegion> allInspectionRegions,
      Collection<ReconstructionRegion> regionsRequiringGlobalSurfaceModel)
      throws XrayTesterException
  {
    if (useShadingCompensation() == false)
      return;
    
    boolean usingGlobalSurfaceModel = usingGlobalSurfaceModel();
    for (ReconstructionRegion reconstructionRegion : allInspectionRegions)
    {
      if (usingGlobalSurfaceModel && reconstructionRegionRequiresGlobalSurfaceModel(reconstructionRegion))
      {
        regionsRequiringGlobalSurfaceModel.add(reconstructionRegion);
      }
    }
  }
  
  /**
   * @author Roy Williams
   * @author Kay Lannen
   *
   */
  private void divideRegionsBaseOnAdvanceSettings(
      Collection<ReconstructionRegion> allInspectionRegions,
      Collection<ReconstructionRegion> regionsRequiringGlobalSurfaceModel,
      Collection<ReconstructionRegion> regionsRequiringInteferenceCompensation,
      Collection<ReconstructionRegion> regionsWithoutShadingCompensation,
      Map<Integer,Collection<ReconstructionRegion>> regionsRequiringShadingCompensationList,
      Map<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>> regionsWithDROList,
      boolean checkingIsolatedPSHRegion)
      throws XrayTesterException
  {    
    // Khang Wah, 2013-06-17, New PSH handling
    // turn this only to true when use PSH = true & have 64bit IRP
    // set to false by default just in case it have been turn true before
    TestSubProgram testSubProgram = allInspectionRegions.iterator().next().getTestSubProgram();
    for (ReconstructionRegion reconstructionRegion : allInspectionRegions)
    {
      reconstructionRegion.setWaitForKickStart(false);
      reconstructionRegion.clearConsumerAndProducer();
    }
    
    // All of the calls in the if statement above do lots of work to order/cull
    // scanpaths and then ultimately call the same place as the else does below...
    // Lets put the stuff into the list first then we will sort it in a minute.
    if (useShadingCompensation() == false)
    {
      regionsWithoutShadingCompensation = allInspectionRegions;
    }

    // Else lets separate out the shaded regions.
    else
    {
      boolean usingGlobalSurfaceModel = usingGlobalSurfaceModel();
      for (ReconstructionRegion reconstructionRegion : allInspectionRegions)
      {    
        String output = null;
        if (_debugComponents != null)
        {
          List<String> componentAndPin = debugComponent(reconstructionRegion);
          if (componentAndPin != null)
          {
            _debugRegions.add(reconstructionRegion);
            output = "adding: " + componentAndPin +
                     "    RR: " +reconstructionRegion.getRegionId();;
          }
        }

        Assert.expect(reconstructionRegion.isAlignmentRegion() == false);
        
        if (usingGlobalSurfaceModel &&
            reconstructionRegionRequiresGlobalSurfaceModel(reconstructionRegion))
        {
          if (output != null)
            System.out.println("Global " + output);
          regionsRequiringGlobalSurfaceModel.add(reconstructionRegion);
          
          // Khang Wah, 2013-06-17, New PSH handling
          // turn this only to true when use PSH = true & have 64bit IRP
          if(Config.isRealTimePshEnabled() && testSubProgram.isSufficientMemoryForRealTimePsh())
            reconstructionRegion.setWaitForKickStart(true);
          else
            continue;
        }
        
        //Siew Yeng - XCR-2140 - DRO
        // filter reconstuction region with DRO
        if(reconstructionRegion.useDynamicRangeOptimization())
        {
          Collection<ReconstructionRegion> list = regionsWithDROList.get(reconstructionRegion.getDynamicRangeOptimizationLevel());
          if (list == null)
          {
            list = new ArrayList<ReconstructionRegion>();
            regionsWithDROList.put(reconstructionRegion.getDynamicRangeOptimizationLevel(), list);
          }
          list.add(reconstructionRegion);
        }
        
        // Khang Wah, 2013-06-17, New PSH handling
        if (reconstructionRegion.requiresShadingCompensation())
        {
          // Ask how big it wants to be.
          int maxCandidateStepSize = RescanStrategy.calculateMaxCandidateStepSize(reconstructionRegion);

          // Chnee Khang Wah, 2012-07-19
          if (reconstructionRegion.isCompensated() && reconstructionRegion.isAffectedByInterferencePattern())
          {
            regionsRequiringInteferenceCompensation.add(reconstructionRegion);
          }
          else if (maxCandidateStepSize == _defaultMeanStepSizeLimitInNanometers)
          {
            if (output != null)
              System.out.println("Regular " + output);
            regionsWithoutShadingCompensation.add(reconstructionRegion);
          }
          else
          {
            if (output != null)
              System.out.println("Shading " + output + " step: " + maxCandidateStepSize);
            Collection<ReconstructionRegion> list = regionsRequiringShadingCompensationList.get(reconstructionRegion.getSignalCompensation().getId());
            if (list == null)
            {
              list = new ArrayList<ReconstructionRegion>();
              regionsRequiringShadingCompensationList.put(reconstructionRegion.getSignalCompensation().getId(), list);
            }
            list.add(reconstructionRegion);
          }
        }
        else
        {
          if (output != null)
            System.out.println("Regular " + output);
          regionsWithoutShadingCompensation.add(reconstructionRegion);
        }
      }
    }
    
    if(checkingIsolatedPSHRegion == true)
    {
      if (allInspectionRegions.size() == regionsRequiringGlobalSurfaceModel.size())
      {
        System.out.println("IMAGING_CHAIN::TestSubProgram "+testSubProgram.getId()+" is having ZeroNeighborsToPredictZHeightException!");

        //Siew Yeng - XCR-2460
        testSubProgram.addIsolatedGlobalSurfaceRegions(allInspectionRegions);
        String fileName = generateIsolatedPSHRegionCSVFile(testSubProgram.getTestProgram().getProject());

        throw new ZeroNeighborsToPredictZHeightException(testSubProgram.getStageSpeed().toString(),
                                                         testSubProgram.getUserGain().toString(),
                                                         fileName);
      }
    }
  }

  /**
   * @author Roy Williams
   */
  private HomogeneousImageGroup extractLargestGroup(List<HomogeneousImageGroup> signalCompensatedGroups)
  {
    Assert.expect(signalCompensatedGroups != null);
    Assert.expect(signalCompensatedGroups.size() > 0);

    SystemFiducialRectangle sfrCurrent   = null;
    SystemFiducialRectangle sfrAlternate = null;
    HomogeneousImageGroup maxSizedGroup = null;
    for (HomogeneousImageGroup group : signalCompensatedGroups)
    {
      if (maxSizedGroup == null)
      {
        maxSizedGroup = group;
      }
      else
      {
        sfrCurrent   = maxSizedGroup.getNominalAreaToImage();
        sfrAlternate = group.getNominalAreaToImage();
        if ((sfrAlternate.getWidth() * sfrAlternate.getHeight()) >
            (sfrCurrent.getWidth() * sfrCurrent.getHeight()))
        {
          maxSizedGroup = group;
        }
      }
    }
    signalCompensatedGroups.remove(maxSizedGroup);
    return maxSizedGroup;
  }

  /**
   * @author Roy Williams
   */
  public static boolean reconstructionRegionRequiresGlobalSurfaceModel(ReconstructionRegion region)
  {
    JointInspectionData joint = region.getJointInspectionDataList().get(0);
    JointTypeEnum jointType = joint.getJointTypeEnum();
    Subtype subtype = joint.getSubtype();

    if (UnitTest.unitTesting())
    {
      if (jointType.equals(JointTypeEnum.THROUGH_HOLE) ||
          jointType.equals(JointTypeEnum.OVAL_THROUGH_HOLE) || //Siew Yeng - XCR-3318 - Oval PTH
          jointType.equals(JointTypeEnum.PRESSFIT))
      {
        return true;
      }
      return false;
    }

    // CR1085 fix by LeeHerng - Previous design of PSH is that if we turn on PSH, and no neighbors available,
    // system will not use signal compensation for scan path generation. Now I change the way PSH behave in such a way
    // that if PSH is turn on and it found out there is no neighbors available, it will use signal compensation instead.
    if (subtype.usePredictiveSliceHeight())
    {
      //Siew Yeng - XCR-2460 - remove this checking as it will auto set to to run auto focus 
      //                       when the testSubProgram is full PSH without any message.
//        if (region.hasNeighbors())
//        {
      if (Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_PSH_DEBUG))
          System.out.println("enablePSHDebug - Region " + region.getRegionId() + " (component " + region.getComponent().getReferenceDesignator() + ") has neighbors and use surface model.");
      return true;
//        }
//        else
//        {
//            if (Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_PSH_DEBUG))
//                System.out.println("enablePSHDebug - Region " + region.getRegionId() + " (component " + region.getComponent().getReferenceDesignator() + ") has no neighbors and do not use surface model. Use IL if available.");
//            return false;
//        }
    }
    else
    {
        if (Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_PSH_DEBUG))
            if(region.hasComponent())
            	System.out.println("enablePSHDebug - Region " + region.getRegionId() + " (component " + region.getComponent().getReferenceDesignator() + ") do not use surface model. Use IL if available.");
            else
                System.out.println("enablePSHDebug - Region " + region.getRegionId() + " (component null) don not use surface model. Use IL is available.");
        return false;
    }
  }


  /**
   * @author Roy Williams
   */
  private boolean hasStrategyToMergeRescanImageGroupsIntoMainImageGroup()
  {
    for (ScanStrategyEnum strategyEnum : _scanStrategies)
    {
      if (ScanStrategyEnum.MERGE_TO_MAIN.equals(strategyEnum))
      {
        return true;
      }
    }
    return false;
  }


  /**
   * @author Roy Williams
   */
  private RescanStrategy selectBestRescanStrategy(
      TestSubProgram testSubProgram,
      Collection<ReconstructionRegion> regionsRequiringShadingCompensation) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(regionsRequiringShadingCompensation != null);

    // Make sure the ImageGroup does not try to include fiducials into creation
    // of ImageGroups requiring shading compensation.
    ImageGroup.unsetFiducials();

    List<ScanStrategy> strategies = new ArrayList<ScanStrategy>();
    for (ScanStrategyEnum strategyEnum : _scanStrategies)
    {
      if (strategyEnum.isRescanStrategy())
      {
        if(_debug)
          TimerUtil.printCurrentTime("  Start inside for loop");
        ScanStrategy scanStrategy = ScanStrategy.implementStrategy(strategyEnum);
        Assert.expect(scanStrategy instanceof RescanStrategy);
        RescanStrategy shadingStrategy = (RescanStrategy)scanStrategy;
        shadingStrategy.setDebug(_debug);
        shadingStrategy.arrangeGroupsInExecutionOrder(regionsRequiringShadingCompensation);
        strategies.add(shadingStrategy);
        if(_debug)
          TimerUtil.printCurrentTime("  End inside for loop");
      }
    }

    Comparator<ScanStrategy> comparator = new Comparator<ScanStrategy>()
    {
      public int compare(ScanStrategy strategy1, ScanStrategy strategy2)
      {
        Assert.expect(strategy1 != null);
        Assert.expect(strategy2 != null);
        double strategyEstimateA = strategy1.getEstimatedExecutionTime();
        double strategyEstimateB = strategy2.getEstimatedExecutionTime();
        if (strategyEstimateA < strategyEstimateB)
          return -1;
        else if (strategyEstimateA > strategyEstimateB)
          return 1;
        return 0;
      }
    };
    if(_debug)
      TimerUtil.printCurrentTime("  Start sorting strategies size:"+strategies.size());
    Collections.sort(strategies, comparator);
    if(_debug)
      TimerUtil.printCurrentTime("  End sorting");

    // Restore the system fiducials so non RescanStrategy image groups can be created.
    // Kee Chin Seong - Fiducial suppose need to considering the panel based alignment or board base alignment
    if(testSubProgram.getTestProgram().getProject().getPanel().getPanelSettings().isPanelBasedAlignment())
         ImageGroup.setFiducials(testSubProgram.getFiducialsForTestProgram());
    else
         ImageGroup.setFiducials(testSubProgram.getFiducialsForTestSubProgram(testSubProgram));

    return ((RescanStrategy)strategies.get(0));
  }

  /**
   * This will act as the Starting point for first fix x scan pass
   * 
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  private int getStartingStageLocationForFixXScanPath()
  {
    // int xLoc = Config.getInstance().getIntValue(HardwareConfigEnum.RIGHT_PIP_SENSOR_X_LOCATION_IN_SYSTEM_FIDUCIAL_COORDINATES_NANOMETERS);
    // MachineRectangle cameraArrayRectangle = MechanicalConversions.getCameraArrayRectangleAtMinSliceHeight();
    // after discussion with Tracy, instead to put the starting point to PIP, or 
    // camera array rectangle, we actually can put any number as long as
    // it's constant. 
    // int xLoc = cameraArrayRectangle.getMinX();
    return 0;//xLoc;
  }
  
  /**
   * This will return xTravelRequired for every imageGroup
   * currently only implement for new scan route
   * 
   * @author Chnee Khang Wah, 2013-02-28, new scan route
   */
  private int getXTravelRequired(TestSubProgram testSubProgram, SystemFiducialRectangle rectangle, boolean forceUseLargerTomographicAngle)
  {
    MachineRectangle cameraArrayRectangle = MechanicalConversions.getCameraArrayRectangleAtMinSliceHeight();
    int imageableWidthOfFirstCamera = _xrayCameraArray.getImageableWidthInNanometers(MagnificationEnum.getCurrentMinSliceHeight());

    // First, last and all cameras have the same imageable width.
    int imageableWidthOfLastCamera = imageableWidthOfFirstCamera;
    
    // subtract imageableWidth of left-most camera from xTravelRequired
    // already has subtracted 4 pixels
    // already has added alignment uncertainty
    int xTravelRequired = cameraArrayRectangle.getWidth() +
                          rectangle.getWidth();
    
    if(testSubProgram.getTestProgram().getProject().isUseLargeTomoAngle()==false //Smaller tomographic angle
       && forceUseLargerTomographicAngle==false)
    {
      xTravelRequired -= (imageableWidthOfFirstCamera+imageableWidthOfLastCamera);
    }
    
    return xTravelRequired;
  }
  
  /**
   * This will return startingStagePosition for every imageGroup
   * currently only implement for new scan route
   * 
   * @author Chnee Khang Wah, 2013-02-28, new scan route
   */
  private int getStartingStagePosition(TestSubProgram testSubProgram, SystemFiducialRectangle rectangle, boolean forceUseLargerTomographicAngle)
  {
    MachineRectangle cameraArrayRectangle = MechanicalConversions.getCameraArrayRectangleAtMinSliceHeight();
    int imageableWidthOfFirstCamera = _xrayCameraArray.getImageableWidthInNanometers(MagnificationEnum.getCurrentMinSliceHeight());

    // add imageableWidth of left-most camera from startXPosition
    int startingStagePosition = cameraArrayRectangle.getMinX() -
                                rectangle.getMaxX();
    
    if(testSubProgram.getTestProgram().getProject().isUseLargeTomoAngle()==false //Smaller tomographic angle
       && forceUseLargerTomographicAngle==false)
    {
      startingStagePosition += imageableWidthOfFirstCamera;
    }
    
    return startingStagePosition;
  }
  
  /**
   * 
   * 
   */
  private List<HomogeneousImageGroup> createFixXScanPaths(
          TestSubProgram testSubProgram,
          Collection<ReconstructionRegion> unSortRegions,
          boolean forceMerge,
          ScanPassSetting scanPassSetting) throws XrayTesterException
  {
    List<HomogeneousImageGroup> groups = new ArrayList<HomogeneousImageGroup>();
    
    // Distribute regions according to signal compensation settings
    Map<Integer,Collection<ReconstructionRegion>> regionsList = new HashMap<Integer,Collection<ReconstructionRegion>>();

    for(ReconstructionRegion region : unSortRegions)
    {
      Collection<ReconstructionRegion> list = regionsList.get(region.getSignalCompensation().getId());
      if (list == null)
      {
        list = new ArrayList<ReconstructionRegion>();
        regionsList.put(region.getSignalCompensation().getId(), list);
      }
      list.add(region);
    }

    // Create group
    for ( Collection<ReconstructionRegion> regions : regionsList.values())
    {
      HomogeneousImageGroup group = new HomogeneousImageGroup(regions, false);
      groups.add(group);
    }

    List<HomogeneousImageGroup> toMaintainGroups = new ArrayList<HomogeneousImageGroup>();
    HomogeneousImageGroup mainGroup = groups.remove(0);
    createFixXScanPath(testSubProgram, mainGroup, scanPassSetting);

    // Create scanpath for other groups and try to merge into main group
    for(HomogeneousImageGroup group : groups)
    {
      createFixXScanPath(testSubProgram, group, scanPassSetting);

      if(forceMerge==true || isWorthToMerge(testSubProgram, mainGroup, group))
        mainGroup = mergeGroupsAndScanPaths(testSubProgram, mainGroup, group, true);
      else
        toMaintainGroups.add(group);
    }

    groups.clear();
    groups.add(mainGroup);
    if(toMaintainGroups!=null && toMaintainGroups.size()>0)
    {
      // possible to merge within them?
      if(toMaintainGroups.size()>1)
        toMaintainGroups = attemptToMergeGroupsAndScanPaths(testSubProgram, toMaintainGroups, scanPassSetting);

      groups.addAll(toMaintainGroups);
    }
    
    return groups;
  }
  
  /**
   * This will generate scanpath for the group and add it to the testSubProgram.
   * Make the Image group are homogeneous (only one IL)!!
   *
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  private void createFixXScanPath(
      TestSubProgram testSubProgram,
      HomogeneousImageGroup imageGroup,
      ScanPassSetting scanPassSetting) throws XrayTesterException
  {
    createFixXScanPath(testSubProgram,imageGroup,DynamicRangeOptimizationLevelEnum.ONE, scanPassSetting);
  }
  
  /**
   * This will generate scanpath for the group and add it to the testSubProgram.
   * Make the Image group are homogeneous (only one IL)!!
   *
   * @author XCR1529, New Scan Route, khang-wah.chnee
   * @author XCR-2140, Siew Yeng - added drolevel
   */
  private void createFixXScanPath(TestSubProgram testSubProgram,
                                  HomogeneousImageGroup imageGroup,
                                  DynamicRangeOptimizationLevelEnum droLevel,
                                  ScanPassSetting scanPassSetting) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(imageGroup != null);
    Assert.expect(droLevel != null);
    Assert.expect(scanPassSetting != null);
    
    double appropriateMaxSliceHeight = XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(testSubProgram.getTestProgram().getProject().getPanel().getThicknessInNanometers(), testSubProgram.getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers());
    
    int startingFixXStagePosition = getStartingStageLocationForFixXScanPath();
    // System x motor position limit
    int xStagePositionLimit = 0;
    try
    {
      xStagePositionLimit = StringUtil.convertStringToInt(Config.getInstance().getStringValue(HardwareConfigEnum.X_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS));
    }
    catch (Exception e)
    {
      // do nothing
    }
    
    // We've redefined the processor strips for the shaded regions so reassign
    // the IRE to map into the correct processor strip
    List<ProcessorStrip> processorStrips = createProcessorStripsForImageGroupReusingAreasDefinedInOriginalProcessorStrips(
        testSubProgram.getProcessorStripsForInspectionRegions(),
        imageGroup);

    MechanicalConversions mechanicalConversions = new MechanicalConversions(
      processorStrips,
      imageGroup.getAffineTransform(),
      Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
      ProcessorStrip.getOverlapNeededForLargestPossibleJointInNanometers(),
      appropriateMaxSliceHeight,
      Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers(testSubProgram.getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers())),
      _xRayCameras,
      imageGroup.getNominalAreaToImage());

    // retrieve the rectangleToImage from the MechanicalConversions.   The original
    // rectangle was modified/ to add on the alignment uncertainty in x, y, width, height.
    SystemFiducialRectangle rectangleToImageWithAlignmentUncertainty =
        mechanicalConversions.rectangleToImageWithAlignmentUncertaintyAddedToBorders();

    boolean forceUseLargerTomographicAngle = false;
    // Calculate x travel properties.
    int xTravelRequired = getXTravelRequired(testSubProgram, rectangleToImageWithAlignmentUncertainty, forceUseLargerTomographicAngle);
    int startingStagePosition = getStartingStagePosition(testSubProgram, rectangleToImageWithAlignmentUncertainty, forceUseLargerTomographicAngle);
    
    // Create a list of reconstruction regions that are in-scope.
    ReconstructionRegionsInScanPass regionsInScopeFromPriorScanPass = new ReconstructionRegionsInScanPass();

    // Get filtered fix x scan pass
    ReconstructionRegion firstRegion = imageGroup.getUnsortedRegions().iterator().next();
    Map<Integer, Pair<Integer, String>> xLocationToSignalCompensationMap = new HashMap<Integer, Pair<Integer, String>>();
    xLocationToSignalCompensationMap = filterScanPassBaseOnCode(firstRegion.getSignalCompensation());
    
    // Build up the scan path
    List<ScanPass> scanPasses = new ArrayList<ScanPass>();
    int scanPassNumber = 0;
    int xTravelTaken = 0;
    int XLocationInNanometer = 0;
    int nextXLocation = 0;
    int lastXLocation = 0;
    boolean log = false; //debug purpose
    int k = 0;
    for (int i=0; (int)(i/droLevel.toDouble())<xLocationToSignalCompensationMap.size(); i++)
    {      
      k = (int)(i/droLevel.toDouble());
      
      XLocationInNanometer = xLocationToSignalCompensationMap.get(k).getFirst() + startingFixXStagePosition;
      String passCode = xLocationToSignalCompensationMap.get(k).getSecond();
      
      if(k>=xLocationToSignalCompensationMap.size() || testSubProgram.getTestProgram().getProject().isUseLargeTomoAngle()==true)
        nextXLocation = XLocationInNanometer;
      else
        nextXLocation = xLocationToSignalCompensationMap.get(k+1).getFirst() + startingFixXStagePosition;
      
      if(nextXLocation <= startingStagePosition)
        continue;
      
      if(log==true)
      {
        log = false;
        System.out.println("ScanPathExtraS: "+testSubProgram.getTestProgram().getProject().getName()+", "+(startingStagePosition-XLocationInNanometer)+"nm");
      }
      
      if(k == 0 || testSubProgram.getTestProgram().getProject().isUseLargeTomoAngle()==true)
        lastXLocation = XLocationInNanometer;
      else
        lastXLocation = xLocationToSignalCompensationMap.get(k-1).getFirst() + startingFixXStagePosition;
      
      xTravelTaken = lastXLocation - startingStagePosition;
      if (xTravelTaken > xTravelRequired)
      {
        break;
      }
      
      ReconstructionRegionsInScanPass scanPassForReconstructionRegion =
          createNextFixXScanPass(scanPassNumber,
                                 passCode,
                                 XLocationInNanometer,
                                 imageGroup.getRegionPairsSortedInExecutionOrder(),
                                 regionsInScopeFromPriorScanPass);

      ScanPass scanPass = scanPassForReconstructionRegion.getScanPass();
      scanPass.setUserGainValue(scanPassSetting.getUserGainValue());
      scanPass.setStageSpeedSetting(scanPassSetting.getStageSpeedValue());
      scanPass.setIsLowMagnification(scanPassSetting.isLowMagnification());
      // Make sure that some regions are using this scanPass.  If everything gets
      // deferred, we will skip it entirely.
      regionsInScopeFromPriorScanPass = scanPassForReconstructionRegion;
      int numberOfReconstructionRegionsUsingProposedScanPass =
        scanPassForReconstructionRegion.getNumberOfReconstructionRegionsUsingProposedScanPass();
      if (numberOfReconstructionRegionsUsingProposedScanPass > 0)
      {
        // Make sure the scan pass is assign for region's signal Compensation level
        Collection<ReconstructionRegion> regions = new ArrayList<ReconstructionRegion>();
        for(ReconstructionRegion region : scanPassForReconstructionRegion.getRegionsExitingScanPassScope())
        {
          if (isPassCodeMatchWithSignalCompensation(scanPass.getCode(), region.getSignalCompensation()))
          {
            regions.add(region);
          }
        }
        for(Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair :scanPassForReconstructionRegion.getImagedReconstructionRegionPairs())
        {
          if (isPassCodeMatchWithSignalCompensation(scanPass.getCode(), regionPair.getFirst().getSignalCompensation()))
          {
            regions.add(regionPair.getFirst());
          }
        }
        
        if(regions.size()>0)
        {
          scanPass.getStartPointInNanoMeters().setYInNanometers(mechanicalConversions.stageTravelStart());
          scanPass.getEndPointInNanoMeters().setYInNanometers(mechanicalConversions.stageTravelEnd());
        
          // Don't worry about setting them all forward because it will get re-organized
          // after all scan passes are figured out.   It will be reordered later!
          scanPass.directionOfTravel(ScanPassDirectionEnum.FORWARD);
          
          commitReconstructionRegionsToScanPass(regions, scanPass);
          scanPasses.add(scanPass);

          ++scanPassNumber;
        }
      }
      
      // scan pass should not exist x position limit, we will adjust the first 
      // scan pass which exist, but will break this loop after that.
      if(XLocationInNanometer > xStagePositionLimit)
      {
        break;
      }
    }

    Assert.expect(scanPasses.isEmpty()==false);
    
    ScanPath scanPath = new ScanPath(mechanicalConversions, scanPasses);
    imageGroup.setScanPath(scanPath);
    for (ScanPass scanPass : scanPath.getScanPasses())
    {
      scanPass.setProjectionType(ProjectionTypeEnum.INSPECTION_OR_VERIFICATION);
    }
    
    if(testSubProgram.getTestProgram().getProject().isVarDivNEnable())// enableVarDivN
      applyVariableDivnOnScanPath(testSubProgram, imageGroup, scanPassSetting);
    
    updateImageReconstructionEnginesInUseBasedUponAdditionalScanPathInfo(scanPath);
//    _imageGroups.add(imageGroup);
  }
  
  private Map<Integer, Pair<Integer, String>> filterScanPassBaseOnCode(SignalCompensationEnum signalEnum)
  {
    Map<Integer, Pair<Integer, String>> filteredScanPassBaseOnCode = new HashMap<Integer, Pair<Integer, String>>();
    
    int j = 0;
    for (int i=0; i<_xLocationToPassCodeMap.size(); i++)
    {
      if(isPassCodeMatchWithSignalCompensation(_xLocationToPassCodeMap.get(i).getSecond(), signalEnum))
      {
        Pair<Integer, String> pair = new Pair<Integer, String>(_xLocationToPassCodeMap.get(i).getFirst(), _xLocationToPassCodeMap.get(i).getSecond());
        filteredScanPassBaseOnCode.put(j, pair);
        j++;
      }
    }
    
    return filteredScanPassBaseOnCode;
  }
   
  /**
   * @param passCode
   * @param region
   * @return 
   * 
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  private static boolean isPassCodeMatchWithSignalCompensation(String passCode, SignalCompensationEnum signalEnum)
  {
    // IL1
    if(signalEnum.equals(SignalCompensationEnum.DEFAULT_LOW))
    {
      if (passCode.equalsIgnoreCase("A"))
        return true;
    }
    // IL1.5 
    else if (signalEnum.equals(SignalCompensationEnum.IL_1_5))
    {
      if (passCode.equalsIgnoreCase("A") || passCode.equalsIgnoreCase("E"))
        return true;
    }
    // IL2
    else if (signalEnum.equals(SignalCompensationEnum.MEDIUM))
    {
      if (passCode.equalsIgnoreCase("A") || passCode.equalsIgnoreCase("E") || passCode.equalsIgnoreCase("I"))
        return true;
    }
    // IL3
    else if (signalEnum.equals(SignalCompensationEnum.MEDIUM_HIGH))
    {
      if (passCode.equalsIgnoreCase("A") || passCode.equalsIgnoreCase("D") || passCode.equalsIgnoreCase("G"))
        return true;
    }
    // IL4
    else if (signalEnum.equals(SignalCompensationEnum.HIGH))
    {
      if (passCode.equalsIgnoreCase("A") || passCode.equalsIgnoreCase("C") || passCode.equalsIgnoreCase("E") || 
          passCode.equalsIgnoreCase("G") || passCode.equalsIgnoreCase("I"))
        return true;
    }
    // IL5
    else if (signalEnum.equals(SignalCompensationEnum.IL_5))
    {
      if (passCode.equalsIgnoreCase("A") || passCode.equalsIgnoreCase("B") || passCode.equalsIgnoreCase("D") || 
          passCode.equalsIgnoreCase("F") || passCode.equalsIgnoreCase("H"))
        return true;
    }
    // IL6
    else if (signalEnum.equals(SignalCompensationEnum.IL_6))
    {
      if (passCode.equalsIgnoreCase("A") || passCode.equalsIgnoreCase("C") || passCode.equalsIgnoreCase("D") || 
          passCode.equalsIgnoreCase("E") || passCode.equalsIgnoreCase("F") || passCode.equalsIgnoreCase("G") || 
          passCode.equalsIgnoreCase("I"))
        return true;
    }
    // IL7
    else if (signalEnum.equals(SignalCompensationEnum.IL_7))
    {
      if (passCode.equalsIgnoreCase("A") || passCode.equalsIgnoreCase("B") || passCode.equalsIgnoreCase("C") || 
          passCode.equalsIgnoreCase("D") || passCode.equalsIgnoreCase("E") || passCode.equalsIgnoreCase("F") || 
          passCode.equalsIgnoreCase("H") || passCode.equalsIgnoreCase("I"))
        return true;
    }
    // IL8
    else if (signalEnum.equals(SignalCompensationEnum.SUPER_HIGH))
    {
      return true;
    }
    
    return false;
  }
  
  /**
   * This will generate your main scanpath and add it to the testSubProgram.
   *
   * @author Roy Williams
   */
  private ScanPath createScanPath(
      TestSubProgram testSubProgram,
      HomogeneousImageGroup group,
      ScanPassSetting scanPassSetting) throws XrayTesterException
  {
    return createScanPath(testSubProgram, group, DynamicRangeOptimizationLevelEnum.ONE, scanPassSetting);
  }
  
  /**
   * This will generate your main scanpath and add it to the testSubProgram.
   *
   * @author Roy Williams
   * @author Siew Yeng - added droLevel - XCR-2140
   */
  private ScanPath createScanPath(
      TestSubProgram testSubProgram,
      HomogeneousImageGroup group,
      DynamicRangeOptimizationLevelEnum droLevel,
      ScanPassSetting scanPassSetting) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(group != null);
    Assert.expect(droLevel != null);
    Assert.expect(scanPassSetting != null);
    
    int meanCandidateStepSizeInNanometers = group.getMeanStepSizeInNanoMeters();
    int ditherAmplitude = group.getDitherAmplitudeInNanoMeters();

    if (_debugComponents != null)
    {
      List<String> componentAndPins = debugComponent(group);
      if (componentAndPins != null)
      {
        for (String componentAndPin : componentAndPins)
        {
          if ("U59.8".equals(componentAndPin))
          {
            int i = 1;
            _debug = true;
            break;
          }
        }
      }
    }

    // We've redefined the processor strips for the shaded regions so reassign
    // the IRE to map into the correct processor strip
    List<ProcessorStrip> processorStrips = createProcessorStripsForImageGroupReusingAreasDefinedInOriginalProcessorStrips(
        testSubProgram.getProcessorStripsForInspectionRegions(),
        group);

    double appropriateMaxSliceHeight = XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(testSubProgram.getTestProgram().getProject().getPanel().getThicknessInNanometers(), testSubProgram.getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers());
    MechanicalConversions mechanicalConversions = new MechanicalConversions(
        processorStrips,
        group.getAffineTransform(),
        Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
        ProcessorStrip.getOverlapNeededForLargestPossibleJointInNanometers(),
        appropriateMaxSliceHeight,
        Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers(testSubProgram.getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers())),
        _xRayCameras,
        group.getNominalAreaToImage());

    List<ScanPass> scanPasses = generateScanPathForCustomListOfReconstructionRegions(
        group.getRegionPairsSortedInExecutionOrder(),
        mechanicalConversions,
        meanCandidateStepSizeInNanometers,
        ditherAmplitude,
        droLevel,
        scanPassSetting);

    ScanPath scanPath = new ScanPath(mechanicalConversions, scanPasses);
    group.setScanPath(scanPath);
    
    for (ScanPass scanPass : scanPath.getScanPasses())
      scanPass.setProjectionType(ProjectionTypeEnum.INSPECTION_OR_VERIFICATION);
    
    if(testSubProgram.getTestProgram().getProject().isVarDivNEnable())// enableVarDivN
      applyVariableDivnOnScanPath(testSubProgram, group, scanPassSetting);
    
    updateImageReconstructionEnginesInUseBasedUponAdditionalScanPathInfo(scanPath);
    // Don't update the testSubProgram yet--wait until after scan path optimization is done.
    //    testSubProgram.addScanPathOverInspectionOrVerificationArea(scanPathForMainPanelRectangle);
    _imageGroups.add(group);
    return scanPath;
  }
  
  /**
   * Set scan pass id to be 0,1,2,3,4... AND.... check/correct start/end of scan pass
   *
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  private void fixOrderAndStageStartStopPositionsInYForVariableDivn(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);
    
    //Scan pass numbers must be unique for the entire program.
    int currentScanPassNumber = 0;
    
    for(TestSubProgram testSubProgram : testProgram.getFilteredScanPathTestSubPrograms())
    {
      currentScanPassNumber = fixOrderAndStageStartStopPositionsInYForVariableDivn(testSubProgram, testSubProgram.getScanPasses(), currentScanPassNumber);
    }
  }
   
  /**
   * Set scan pass id to be 0,1,2,3,4... AND.... check/correct start/end of scan pass
   *
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  private int fixOrderAndStageStartStopPositionsInYForVariableDivn(TestSubProgram testSubProgram, List<ScanPass> scanPasses, int currentScanPassNumber)
  {
    Assert.expect(testSubProgram != null);

    // **Threshold to trigger scan pass start/end adjustment**
    // As long as the different is less than 25% of alignment uncertainty, we will adjust start/end without reCalculate 
    // processor strips
	  // This is a very important threshold as it will cause ghost image and image off center if it's too loose, as from
    // experiment, 635000 (AlignmentUncertaintyBorderInNanoMeters/2) is too loose and it will cause ghost image on 
    // small board, off center on medium board (A4 size)
    // 317500 (AlignmentUncertaintyBorderInNanoMeters/4) seem like quite a good golden number for this case. 
    // Keep monitoring.
    int adjustThreshold = 317500;
    // As long as the error is less than one pulse (500nm), we will adjust start/end without worrying insufficient trigger error.
    int errorThreshold = 500;
    
    
    List<ScanPass> scanPath = scanPasses;
    List<ScanPass> scanPath2 = new ArrayList<ScanPass>(scanPasses);

    if (scanPath.size()>0)
    {
      ScanPass prePass = scanPath2.get(0);
      for (ScanPass scanPass : scanPath)
      {
        scanPass.setId(currentScanPassNumber);

        int preY1=prePass.getEndPointInNanoMeters().getYInNanometers();
        int preY2=prePass.getStartPointInNanoMeters().getYInNanometers();
        int curY1=scanPass.getEndPointInNanoMeters().getYInNanometers();
        int curY2=scanPass.getStartPointInNanoMeters().getYInNanometers();

        // Dictate direction in y we're going to move
        if ((currentScanPassNumber % 2) == 0)
        {
          scanPass.directionOfTravel(ScanPassDirectionEnum.FORWARD); 
          if (scanPass.getProjectionType().equals(ProjectionTypeEnum.ALIGNMENT)==false &&
              prePass.getProjectionType().equals(ProjectionTypeEnum.ALIGNMENT)==false)
          {
            MechanicalConversions mc = scanPass.getScanPassMechanicalConversions();
            StagePosition start = scanPass.getStartPointInNanoMeters();
            int minStart = Math.min(curY1,Math.min(curY2,Math.min(preY1, preY2)));
            int diff = mc.stageTravelStart()-minStart;

            // less than 50% of alignment uncertainty, apply the modification directly.
            if(diff < adjustThreshold && diff > 0)
            {
              if(_debug)
                System.out.println("SCAN_ROUTE::ScanPassID (FOR)="+scanPass.getId()+", Modify without extra consideration!");
              // ===== MODIFY Starting location to PREVIOUS END POINT =====
              start.setYInNanometers(minStart);
              scanPass.setStartPointInNanoMeters(start);
            }
            // more than 50% of alignment uncertainty, re-calculate processor strips.
            else if(diff>=adjustThreshold)
            {
              int b1 = mc.stageTravelStart();
              int b2 = mc.stageTravelEnd();

              reCalculateChangesNeededOnProcessorStrips(testSubProgram, testSubProgram.getScanPassImageGroup(scanPass), mc, diff, ScanPassDirectionEnum.FORWARD);

              if(_debug)
              {
                System.out.println("SCAN_ROUTE::ScanPassID (FOR)="+scanPass.getId()+", START--> before="+b1+" , desired="+minStart+", after="+mc.stageTravelStart()+", change Needed="+diff+", Error="+(minStart-mc.stageTravelStart()));
                System.out.println("SCAN_ROUTE::ScanPassID (FOR)="+scanPass.getId()+", END----> before="+b2+" , after="+mc.stageTravelEnd()+", Different="+(b2-mc.stageTravelEnd()));
              }

              // startDiff > 0 ---> stage start later than desired, no big problem as long as less than 25% of alignment uncertainty
              // startDiff < 0 ---> stage start earlier than desired, need to cater to avoid insuficient trigger.
              int startDiff = mc.stageTravelStart()-minStart;
              if(_debug)
                System.out.print("SCAN_ROUTE::ScanPassID (FOR)="+scanPass.getId());
              int startPt = getCorrectPoint(startDiff, adjustThreshold, errorThreshold, minStart, mc.stageTravelStart());
              start.setYInNanometers(startPt);
              scanPass.setStartPointInNanoMeters(start);  

              if(_debug)
                System.out.println("SCAN_ROUTE::ScanPassID (FOR)="+scanPass.getId()+", Final Start Location="+start.getYInNanometers());
            }
          }
        }
        else
        {
          scanPass.directionOfTravel(ScanPassDirectionEnum.REVERSE);     
          if (scanPass.getProjectionType().equals(ProjectionTypeEnum.ALIGNMENT)==false &&
              prePass.getProjectionType().equals(ProjectionTypeEnum.ALIGNMENT)==false)
          {
            MechanicalConversions mc = scanPass.getScanPassMechanicalConversions();
            StagePosition start = scanPass.getStartPointInNanoMeters();
            StagePosition end = scanPass.getEndPointInNanoMeters();
            int maxStart = Math.max(curY1,Math.max(curY2,Math.max(preY1, preY2)));
            // Update new start location to mechanical conversion
            // Because the travel direction is reverse, starting point is actually
            // ending point for mechanical conversion, mc always assume position 
            // near to front panel as start.
            int diff = maxStart-mc.stageTravelEnd();
            // less than 50% of alignment uncertainty, apply the modification directly.
            if(diff < adjustThreshold && diff > 0)
            {
              if(_debug)
                System.out.println("SCAN_ROUTE::ScanPassID (REV)="+scanPass.getId()+", Modify without extra consideration!");
              // ===== MODIFY Starting location to PREVIOUS END POINT =====
              start.setYInNanometers(maxStart);
              scanPass.setStartPointInNanoMeters(start);
            }
            else if(diff>=adjustThreshold)
            {
              int b1 = mc.stageTravelEnd();
              int b2 = mc.stageTravelStart();

              reCalculateChangesNeededOnProcessorStrips(testSubProgram, testSubProgram.getScanPassImageGroup(scanPass), mc, diff, ScanPassDirectionEnum.REVERSE);

              if(_debug)
              {
                System.out.println("SCAN_ROUTE::ScanPassID (REV)="+scanPass.getId()+", START--> before="+b1+" , desired="+maxStart+", after="+mc.stageTravelEnd()+", change Needed="+diff+", Error="+(mc.stageTravelEnd()-maxStart));
                System.out.println("SCAN_ROUTE::ScanPassID (REV)="+scanPass.getId()+", END----> before="+b2+" , after="+mc.stageTravelStart()+", Different="+(b2-mc.stageTravelStart()));
              }

              // somehow it will affect ending point of the scan pass, so need to check it.
              // endDiff > 0 ---> stage end earlier than original
              // endDiff < 0 ---> stage end later than original
              int endDiff = mc.stageTravelStart() - b2;
              if(_debug)
                System.out.print("SCAN_ROUTE::ScanPassID (REV)="+scanPass.getId());
              int endPt = getCorrectPoint(endDiff, adjustThreshold, errorThreshold, b2, mc.stageTravelStart());
              end.setYInNanometers(endPt);
              scanPass.setEndPointInNanoMeters(end);

              // startDiff > 0 ---> stage start later than desired
              // startDiff < 0 ---> stage start earlier than desired
              int startDiff = maxStart - mc.stageTravelEnd();
              if(_debug)
                System.out.print("SCAN_ROUTE::ScanPassID (REV)="+scanPass.getId());
              int startPt = getCorrectPoint(startDiff, adjustThreshold, errorThreshold, maxStart, mc.stageTravelEnd());
              start.setYInNanometers(startPt);
              scanPass.setStartPointInNanoMeters(start);
            }
          }
        }
        currentScanPassNumber++;
        prePass = scanPath2.remove(0);
      }
    }
    fixStopPositionsInYForVariableDivn(testSubProgram, scanPasses);
    return currentScanPassNumber;
  }
  
  private void fixStopPositionsInYForVariableDivn(TestSubProgram testSubProgram, List<ScanPass> scanPasses)
  {
    List<ScanPass> scanPath = testSubProgram.getScanPasses();
    List<ScanPass> scanPath2 = new ArrayList<ScanPass>(testSubProgram.getScanPasses());

    if (scanPath.size()>0)
    {
      for (ScanPass scanPass : scanPath)
      {
        int curY1=scanPass.getEndPointInNanoMeters().getYInNanometers();
        int curY2=scanPass.getStartPointInNanoMeters().getYInNanometers();
        int nexY1=-1, nexY2=-1;
        if (scanPath2.size()>1)
        {
          nexY1=scanPath2.get(1).getEndPointInNanoMeters().getYInNanometers();
          nexY2=scanPath2.get(1).getStartPointInNanoMeters().getYInNanometers();
        }

        if(scanPass.getId() % 2 == 0)
        {
          if (scanPath2.size()>1) 
          {
            if(scanPass.getProjectionType().equals(ProjectionTypeEnum.ALIGNMENT)==false)
            {
              StagePosition end = scanPass.getEndPointInNanoMeters();
              // ===== MODIFY Ending location to SCANPASS END =====
              end.setYInNanometers(Math.max(curY1,Math.max(curY2,Math.max(nexY1, nexY2))));
              scanPass.setEndPointInNanoMeters(end);
            }
          }
        }
        else
        {
          if (scanPath2.size()>1)
          {
            if(scanPass.getProjectionType().equals(ProjectionTypeEnum.ALIGNMENT)==false)
            {
              StagePosition end = scanPass.getEndPointInNanoMeters();
              end.setYInNanometers(Math.min(curY1,Math.min(curY2,Math.min(nexY1, nexY2))));
              scanPass.setEndPointInNanoMeters(end);
            }
          }
        }
        scanPath2.remove(0);
      }
    }
     //Lim Lay Ngor add: Set to testSubProgram here.
    testSubProgram.setEstimateTestSubProgramExecutionTime(estimateTestSubProgramExecutionTime(testSubProgram));
  }
  
  /**
   * @author Chnee Khang Wah, 2013-09-09, New Scan Route
   */
  private void reCalculateChangesNeededOnProcessorStrips(
          TestSubProgram testSubProgram,
          ImageGroup spImageGroup, 
          MechanicalConversions spMC, 
          int diff,
          ScanPassDirectionEnum scanDirection)
  {
    PanelRectangle panel = spImageGroup.getPanelRectangleToImage();
    int minx    = panel.getMinX()-diff*(scanDirection.equals(ScanPassDirectionEnum.FORWARD)?1:0); 
    int miny    = panel.getMinY();
    int width   = panel.getWidth()+diff;
    int height  = panel.getHeight();
    spImageGroup.getPanelRectangleToImage().setRect(minx, miny, width, height);
    spImageGroup.reComputeNominalAreaToImage();
    List<ProcessorStrip> strips = reCreateProcessorStripsForImageGroupReusingAreasDefinedInOriginalProcessorStrips(
    testSubProgram.getProcessorStripsForInspectionRegions(),
    spImageGroup,
    scanDirection);
    
    try
    {
      spMC.reInitialize(strips, spImageGroup.getNominalAreaToImage());
    }
    catch(XrayTesterException xe)
    {
      // do nothing
    }
  }
  
  /**
   * @author Chnee Khang Wah, 2013-09-09, New Scan Route
   */
  private int getCorrectPoint(int diff, int adjustThreshold, int errorThreshold, int desiredLoc, int currentLoc)
  {
    if(diff>=0)
    {
      // The error is less than 25% of alignment uncertainty, safe to use desired start/end
      if(diff < adjustThreshold)
      {
        if(_debug)
          System.out.println(", PASS ALIGNMNET UNCERTAINTY, GOOD!!");
        // ===== MODIFY Starting location to PREVIOUS END POINT =====
        return desiredLoc;
      }
      // Not safe, use start point from mechanical conversions
      else
      {
        if(_debug)
          System.out.println(", FAIL ALIGNMNET UNCERTAINTY, TIME WASTED!!");
        // ===== MODIFY Starting location to MECHANICAL_CONVERSIONS START =====
        return currentLoc;
      }
    }
    else
    {
      // The error is less than one pulse, safe to use desired start/end
      if(Math.abs(diff) < errorThreshold)
      {
        if(_debug)
          System.out.println(", PASS CAMERA PULSE ERROR, GOOD!!");
        // ===== MODIFY Starting location to PREVIOUS END POINT =====
        return desiredLoc;
      }
      // Not safe, use start point from mechanical conversions
      else
      {
        if(_debug)
          System.out.println(", FAIL CAMERA PULSE ERROR, TIME WASTED!!");
        // ===== MODIFY Starting location to MECHANICAL_CONVERSIONS START =====
        return currentLoc;
      }
    }
  }
  
  /**
   * Set scan pass id to be 0,1,2,3,4... AND.... check/correct start/end of scan pass
   * LLN comment: Arrange scan path order here.
   * @author Roy Williams
   */
  private void fixOrderAndStageStartStopPositionsInY(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    //Scan pass numbers must be unique for the entire program.
    int currentScanPassNumber = 0;
    //Khaw Chek Hau - XCR2541: Assert when misalignment happened during production
    for(TestSubProgram testSubProgram : testProgram.getFilteredScanPathTestSubPrograms())
    {
      List<ScanPass> scanPath = testSubProgram.getScanPasses();
      for (ScanPass scanPass : scanPath)
      {
        scanPass.setId(currentScanPassNumber);

        // Dictate direction in y we're going to move
        if ((currentScanPassNumber % 2) == 0)
        {
          scanPass.directionOfTravel(ScanPassDirectionEnum.FORWARD);
        }
        else
        {
          scanPass.directionOfTravel(ScanPassDirectionEnum.REVERSE);
        }
        currentScanPassNumber++;
      }
      
      //Lim Lay Ngor add: Set to testSubProgram here.
      testSubProgram.setEstimateTestSubProgramExecutionTime(estimateTestSubProgramExecutionTime(testSubProgram));
    }
  }

  /**
   * @author Roy Williams
   */
  private PanelRectangle getPanelRectangle(TestSubProgram testSubProgram)
  {

    Assert.expect(testSubProgram != null);

    PanelRectangle rectangleToConvert = null;
    if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION) ||
        _acquisitionMode.equals(ImageAcquisitionModeEnum.IMAGE_SET))
    {
      rectangleToConvert = testSubProgram.getInspectionRegionsBoundsInNanoMeters();
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.ALIGNMENT))
    {
      Assert.expect(false, "getAreaToImageRelativeToSystemFiducial needs to be done on each Alignment point");
  //      rectangleToConvert = testSubProgram.getAlignmentRegionBoundsInNanoMeters();
  //      affineTransformToConvertToSystemFiducialCoordinates = testSubProgram.getAggregateAlignmentTransform();
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.VERIFICATION))
    {
      rectangleToConvert = testSubProgram.getVerificationRegionsBoundsInNanoMeters();
//      affineTransformToConvertToSystemFiducialCoordinates = testSubProgram.getDefaultAggregateAlignmentTransform();
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.COUPON))
    {
      rectangleToConvert = testSubProgram.getVerificationRegionsBoundsInNanoMeters();

      // COUPON images already expresed their Rectangles in SystemFiducialCoordinates.
//      affineTransformToConvertToSystemFiducialCoordinates = new AffineTransform();
    }
    else
      Assert.expect(false, "scanPath can only be generated for PRODUCTION, COUPON or VERIFICATION acquisition modes");
    return rectangleToConvert;
  }

  /**
   * Get the imageable area we want to inspect relative to the system fiducial
   *
   * @author Roy Williams
   */
  private Pair<SystemFiducialRectangle, AffineTransform> getAreaToImageRelativeToSystemFiducial(TestSubProgram testSubProgram)
  {
    Assert.expect(testSubProgram != null);

    PanelRectangle rectangleToConvert = null;
    AffineTransform affineTransformToConvertToSystemFiducialCoordinates = null;
    if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION) ||
        _acquisitionMode.equals(ImageAcquisitionModeEnum.IMAGE_SET))
    {
      rectangleToConvert = testSubProgram.getInspectionRegionsBoundsInNanoMeters();
      affineTransformToConvertToSystemFiducialCoordinates = testSubProgram.getAggregateManualAlignmentTransform();
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.ALIGNMENT))
    {
      Assert.expect(false, "getAreaToImageRelativeToSystemFiducial needs to be done on each Alignment point");
//      rectangleToConvert = testSubProgram.getAlignmentRegionBoundsInNanoMeters();
//      affineTransformToConvertToSystemFiducialCoordinates = testSubProgram.getAggregateAlignmentTransform();
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.VERIFICATION))
    {
      rectangleToConvert = testSubProgram.getVerificationRegionsBoundsInNanoMeters();
      affineTransformToConvertToSystemFiducialCoordinates = testSubProgram.getDefaultAggregateAlignmentTransform();
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.COUPON))
    {
      rectangleToConvert = testSubProgram.getVerificationRegionsBoundsInNanoMeters();

      // COUPON images already expresed their Rectangles in SystemFiducialCoordinates.
      affineTransformToConvertToSystemFiducialCoordinates = new AffineTransform();
    }
    else
      Assert.expect(false, "scanPath can only be generated for PRODUCTION, COUPON or VERIFICATION acquisition modes");

    // Calculate the bounding region for only those areas to image on each subProgram.
    // Get the nominal area to image - no uncertainty
    SystemFiducialRectangle nominalAreaToImage =
      MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(
        rectangleToConvert,
        affineTransformToConvertToSystemFiducialCoordinates);

    return new Pair<SystemFiducialRectangle,AffineTransform>(nominalAreaToImage,
                                                             affineTransformToConvertToSystemFiducialCoordinates);
  }

  /**
   * @author Greg Esparza
   */
  private void setXrayCameraAcquisitionParameters(
      TestProgram testProgram,
      Map<AbstractXrayCamera, XrayCameraAcquisitionParameters> xrayCameraAcquisitionParametersMap)
  {
    List<XrayCameraAcquisitionParameters> xrayCameraAcquisitionParametersList = new ArrayList<XrayCameraAcquisitionParameters>();
    for (Map.Entry<AbstractXrayCamera, XrayCameraAcquisitionParameters> entry : xrayCameraAcquisitionParametersMap.entrySet())
      xrayCameraAcquisitionParametersList.add(entry.getValue());

    testProgram.setCameraAcquisitionParameters(xrayCameraAcquisitionParametersList);
  }

  /**
   * Initialize the cameras.  Create the acquisition parameters and send
   * them to the x-ray cameras.
   *
   * @author Roy Williams
   * @author Greg Esparza
   */
  private void generateCameraAcquisitionParameters(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    // Generate camera acquisition parameters for each TestSubProgram
    int numberOfImageReconstructionEngines = _imageReconstructionEnginesInUse.size();
    Map<AbstractXrayCamera, XrayCameraAcquisitionParameters> xrayCameraAcquisitionParameters = new HashMap<AbstractXrayCamera, XrayCameraAcquisitionParameters>();
    testProgram.setFilteredTestSubProgramInScanPath(_acquisitionMode);
    for (TestSubProgram testSubProgram : testProgram.getFilteredScanPathTestSubPrograms())
    {
      if(testSubProgram.isLowMagnification() != XrayTester.isLowMagnification() )
      {
        ImageAcquisitionEngine.changeMagnification(testSubProgram.getMagnificationType());
        if(getEnableStepSizeOptimization())
          forceCalculateMaxStepSizeInNanometers(testProgram.getProject().getPanel().getThicknessInNanometers(), testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers());
      }
      for (ScanPath scanPath : testSubProgram.getScanPaths())
      {
        if (testProgram.getProject().isVarDivNEnable()) // XCR1529, New Scan Route, khang-wah.chnee
        {
          appendXrayCameraAcquisitionParametersForVariableDivN(numberOfImageReconstructionEngines,
                                                               scanPath,
                                                               xrayCameraAcquisitionParameters);
        }
        else
        {
          appendXrayCameraAcquisitionParameters(numberOfImageReconstructionEngines,
                                                scanPath,
                                                xrayCameraAcquisitionParameters);
        }
      }
    }

    setXrayCameraAcquisitionParameters(testProgram, xrayCameraAcquisitionParameters);
  }

  /**
   * @author Roy Williams
   */
  public static int getMaxAverageStepSizeInNanometers()
  {
    // This is a transitional implementation.  Till we get converted over to this
    // implementation (and deprecate calculateMaxStepSizeInNanometers) we will
    // still rely upon it to do initializations.
    if (_defaultMeanStepSizeLimitInNanometers <= 0)
      calculateMaxStepSizeInNanometers();
    return _defaultMeanStepSizeLimitInNanometers;
  }

  /**
   * @author Roy Williams
   */
  public static int getMinAverageStepSizeInNanometers()
  {
    return _minStepSizeLimitInNanometers + XrayTester.getMaxStepSizeDitherAmplitudeInNanometers();
  }

  /**
   * @author Roy Williams
   */
  public static int getMinStepSizeInNanometers()
  {
    return _minStepSizeLimitInNanometers;
  }

  /**
   * @author Roy Williams
   */
  public static Pair<Integer, Integer> defaultCandidateStepRange()
  {
    return new Pair<Integer, Integer>(getMinAverageStepSizeInNanometers(),
                                      getMaxAverageStepSizeInNanometers());
  }

  /**
   * The maximum step size the system can support (508) is computed.
   *
   * Also a mean step size is computed based upon the max.  Even though
   * the actual maximum (based upon dithering to remove interference patterns)
   * is 591.  As the stage is stepped through the area, the advertised maximum
   * will be dithered +/- 83 mils.  The dither amount is defined in XrayTester.
   *
   * @author Roy Williams
   */
  public static int calculateMaxStepSizeInNanometers()
  {
    if (_maxStepSizeLimitInNanometers > 0)
      return _maxStepSizeLimitInNanometers;

    // Max step size is the camera width at the max slice height.
    _maxStepSizeLimitInNanometers  = getMaxStepSizeLimitInNanometers();
    double maxDither = getDitherMax();
    _defaultMeanStepSizeLimitInNanometers = (int) Math.round(_maxStepSizeLimitInNanometers - maxDither);
    return _maxStepSizeLimitInNanometers;
  }

 /*
  * @author Seng-Yew Lim
  */
 public static int forceCalculateMaxStepSizeInNanometers(double panelThickness, double boardZOffset)
 {
   // Max step size is the camera width at the max slice height.
   if( XrayTester.isLowMagnification())
     _maxStepSizeLimitInNanometers  = calculatePanelMaxStepSizeLimitInNanometers(panelThickness, boardZOffset);
   else
     _maxStepSizeLimitInNanometers  = calculatePanelMaxStepSizeLimitInNanometersInHighMag(panelThickness, boardZOffset);
   double maxDither = getDitherMax();
  _defaultMeanStepSizeLimitInNanometers = (int) Math.round(_maxStepSizeLimitInNanometers - maxDither);
   return _maxStepSizeLimitInNanometers;
 }

 /*
   * @author Seng-Yew Lim
   */
  public static boolean getEnableStepSizeOptimization()
  {
    if (_disableStepSizeOptimization)
      return false;
    return Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_STEP_SIZE_OPTIMIZATION);
  }

  public static void disableStepSizeOptimization(boolean disableStepSizeOptimization)
  {
    _disableStepSizeOptimization = disableStepSizeOptimization;
  }
  /**
   * @author Roy Williams
   */
  public static double getDitherMax()
  {
    if (_ditherMaxInNanometers > 0)
      return _ditherMaxInNanometers;

//    int amplitude = (int)(45.3 * 25400);
    int amplitude = XrayTester.getMaxStepSizeDitherAmplitudeInNanometers(); // 53
    double period = MechanicalConversions.getCameraArrayRectangleAtMinSliceHeight().getWidth(); // 2747

//    dead code - needs to be removed - talk to tracy about computation
    // use Taylor series expansion to solve for x
    double threeFactorial = 6; // 1 * 2 * 3;
    double n = Math.PI / period;

    double d = -(2 * amplitude * (Math.pow(n, 3))/ (threeFactorial));
    double a = 0;
    double b = 1 + 2 * n * amplitude;
    double c = -_maxStepSizeLimitInNanometers;

    double eqn[] = {c, b, a, d};
    double res[] = {0, 0, 0};

    int roots = CubicCurve2D.solveCubic(eqn, res);
    double solution = -1;
    for (int ndx=0; ndx<roots; ndx++)
    {
      if (res[ndx] > _minStepSizeLimitInNanometers && res[ndx] < _maxStepSizeLimitInNanometers)
      {
        solution = res[ndx];
        break;
      }
    }
    Assert.expect(solution > 0);

    _ditherMaxInNanometers = 2 * amplitude * Math.cos(Math.PI * _maxStepSizeLimitInNanometers / period);
    return _ditherMaxInNanometers;
  }
  
  /**
   * This method will calculate the step sizes.
   *
   * @return a list of step sizes to be used when building the scan path
   * @author Roy Williams
   */
  private static int ditheredPositionInNanometers(int xPosition, int ditherAmplitude)
  {
    double period = MechanicalConversions.getCameraArrayRectangleAtMinSliceHeight().getWidth();
    double phase  = 2 * Math.PI * xPosition / period;
    double stepDither = ditherAmplitude * Math.sin(phase);
    return xPosition + (int)Math.round(stepDither);
  }


  /**
   * @author Roy Williams
   * @author Siew Yeng - added droLevel - XCR-2140
   */
  private List<ScanPass> generateScanPathForCustomListOfReconstructionRegions(
      List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionsInExecutionOrder,
      MechanicalConversions mechanicalConversions,
      final int defaultMeanStepSizeInNanomters,
      final int ditherAmplitude,
      DynamicRangeOptimizationLevelEnum droLevel,
      ScanPassSetting scanPassSetting) throws XrayTesterException
  {
    Assert.expect(regionsInExecutionOrder != null);
    Assert.expect(mechanicalConversions != null);
    Assert.expect(scanPassSetting != null);
//    Assert.expect(defaultMeanStepSizeInNanomters + ditherAmplitude <= ImagingChainProgramGenerator.getMaxStepSizeLimitInNanometers());

    List<ReconstructionRegionsInScanPass> currentScanPathReconstructionRegions = new ArrayList<ReconstructionRegionsInScanPass>();

    // retrieve the rectangleToImage from the MechanicalConversions.   The original
    // rectangle was modified to add on the alignment uncertainty in x, y, width, height.
    SystemFiducialRectangle rectangleToImageWithAlignmentUncertainty =
        mechanicalConversions.rectangleToImageWithAlignmentUncertaintyAddedToBorders();

    // Calculate x travel properties.
    MachineRectangle cameraArrayRectangle = MechanicalConversions.getCameraArrayRectangleAtMinSliceHeight();

    // First, last and all cameras have the same imageable width.
    int imageableWidthOfFirstCamera = _xrayCameraArray.getImageableWidthInNanometers(MagnificationEnum.getCurrentMinSliceHeight());;
    int imageableWidthOfLastCamera  = _xrayCameraArray.getImageableWidthInNanometers(MagnificationEnum.getCurrentMinSliceHeight());;

    int xTravelRequired = cameraArrayRectangle.getWidth()                     +
                          rectangleToImageWithAlignmentUncertainty.getWidth() -
                          imageableWidthOfFirstCamera                         -
                          imageableWidthOfLastCamera;

    // add imageableWidth of left-most camera from startXPosition
    int startingXStagePosition = cameraArrayRectangle.getMinX()                     -
                                 rectangleToImageWithAlignmentUncertainty.getMaxX() +
                                 imageableWidthOfFirstCamera;

    // Fix up the y positions based upon calculations done by MechanicalConversions.
    int startYPosition = mechanicalConversions.stageTravelStart();
    int endYPosition   = mechanicalConversions.stageTravelEnd();

    // Create a list of reconstruction regions that are in-scope.
    ReconstructionRegionsInScanPass regionsInScopeFromPriorScanPass = new ReconstructionRegionsInScanPass();

    // Build up the scan path
    List<ScanPass> scanPasses = new ArrayList<ScanPass>();
    int scanPassNumber = 0;
    int xTravelTaken = 0;
    boolean useShadingCompensation = useShadingCompensation();
    int unDitheredTravel = 0;
    int meanStepSizeInNanomters = defaultMeanStepSizeInNanomters;
    int dro = (int)droLevel.toDouble();
    
    while (xTravelTaken <= xTravelRequired)
    {
      if (useShadingCompensation == false)
        meanStepSizeInNanomters = defaultMeanStepSizeInNanomters;
      ReconstructionRegionsInScanPass scanPassForReconstructionRegions =
          createNextScanPass(scanPassNumber,
                             startingXStagePosition,
                             unDitheredTravel,
                             meanStepSizeInNanomters,
                             ditherAmplitude,
                             regionsInScopeFromPriorScanPass,
                             regionsInExecutionOrder);
      
      regionsInScopeFromPriorScanPass = scanPassForReconstructionRegions;

      if(dro == 1)
      {
        unDitheredTravel = unDitheredTravel + scanPassForReconstructionRegions.getMeanStepSizeTaken();
      }
      
      ScanPass scanPass = scanPassForReconstructionRegions.getScanPass();
      xTravelTaken = scanPass.getEndPointInNanoMeters().getXInNanometers() - startingXStagePosition;
      // Make sure that some regions are using this scanPass.  If everything gets
      // deferred, we will skip it entirely.
      int numberOfReconstructionRegionsUsingProposedScanPass =
        scanPassForReconstructionRegions.getNumberOfReconstructionRegionsUsingProposedScanPass();
      if (numberOfReconstructionRegionsUsingProposedScanPass > 0)
      {
        scanPass.setUserGainValue(scanPassSetting.getUserGainValue());
        scanPass.setStageSpeedSetting(scanPassSetting.getStageSpeedValue());
        scanPass.setIsLowMagnification(scanPassSetting.isLowMagnification());
        currentScanPathReconstructionRegions.add(scanPassForReconstructionRegions);
        commitReconstructionRegionsToScanPass(scanPassForReconstructionRegions);
        scanPasses.add(scanPass);

        // Set start and stop postions as the basics.
        scanPass.getStartPointInNanoMeters().setYInNanometers(startYPosition);
        scanPass.getEndPointInNanoMeters().setYInNanometers(endYPosition);

        // Don't worry about setting them all forward because it will get re-organized
        // after all scan passes are figured out.   It will be reordered later!
        scanPass.directionOfTravel(ScanPassDirectionEnum.FORWARD);

        ++scanPassNumber;
      }
      
      if(dro == 1)
        dro = (int)droLevel.toDouble();
      else 
        --dro;
      
    }
    return scanPasses;
  }

  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  private ReconstructionRegionsInScanPass createNextFixXScanPass(
      final int scanPassNumber,
      final String passCode, 
      final int startingXStagePosition,
      List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionsInExecutionOrder,
      ReconstructionRegionsInScanPass regionsInScopeFromPriorScanPass) throws XrayTesterException
  {
    Assert.expect(regionsInExecutionOrder != null);

    ReconstructionRegionsInScanPass scanPassForSetOfReconstructionRegionsAtGivenXLocation =
        createFixXScanPass(scanPassNumber,
                           passCode,
                           startingXStagePosition,
                           regionsInExecutionOrder,
                           regionsInScopeFromPriorScanPass);

    return scanPassForSetOfReconstructionRegionsAtGivenXLocation;
  }
  
  /**
   * @author Roy Williams
   */
  private ReconstructionRegionsInScanPass createNextScanPass(
      final int scanPassNumber,
      final int startingXStagePosition,
      final int unDitheredTravel,
      final int meanStepSizeInNanomters,
      final int ditherAmplitude,
      ReconstructionRegionsInScanPass regionsInScopeFromPriorScanPass,
      List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionsInExecutionOrder) throws XrayTesterException
  {
    Assert.expect(regionsInScopeFromPriorScanPass != null);
    Assert.expect(regionsInExecutionOrder != null);

    ReconstructionRegionsInScanPass scanPassForSetOfReconstructionRegionsAtGivenXLocation =
        createScanPass(scanPassNumber,
                       startingXStagePosition,
                       unDitheredTravel,
                       ditherAmplitude,
                       regionsInScopeFromPriorScanPass,
                       regionsInExecutionOrder);

    int preferredStepSize = meanStepSizeInNanomters;
    // See if we are doing shading compensation.
    if (useShadingCompensation())
    {
//      // Some prep work for integrated scanPasses.
//      for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : scanPassForSetOfReconstructionRegionsAtGivenXLocation.getImagedReconstructionRegionPairs())
//      {
//        int maxStepSize = ScanStrategy.calculateMaxCandidateStepSize(regionPair.getFirst());
//        if (maxStepSize > 0)
//          preferredStepSize = Math.min(maxStepSize, preferredStepSize);
//      }
//      if (preferredStepSize != meanStepSizeInNanomters)
//      {
//        scanPassForSetOfReconstructionRegionsAtGivenXLocation =
//            createScanPass(scanPassNumber,
//                           startingXStagePosition,
//                           unDitheredTravel,
//                           preferredStepSize,
//                           ditherAmplitude,
//                           regionsInScopeFromPriorScanPass,
//                           regionsInExecutionOrder);
//      }
//      meanStepSizeInNanomters = preferredStepSize;
    }
    scanPassForSetOfReconstructionRegionsAtGivenXLocation.setMeanStepSizeTaken(preferredStepSize);
    return scanPassForSetOfReconstructionRegionsAtGivenXLocation;
  }

  /**
   * 
   * @param scanPassNumber
   * @param startingXStagePosition
   * @param regionsInExecutionOrder
   * @return
   * @throws XrayTesterException 
   * 
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  private static ReconstructionRegionsInScanPass createFixXScanPass(
      final int scanPassNumber,
      final String passCode,
      final int startingXStagePosition,
      List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionsInExecutionOrder,
      ReconstructionRegionsInScanPass regionsInScopeFromPriorScanPass) throws XrayTesterException
  {
    Assert.expect(regionsInExecutionOrder != null);

    // Create the new ScanPass
    StagePositionMutable startPosition = new StagePositionMutable();
    StagePositionMutable endPosition = new StagePositionMutable();
    int xStagePosition = startingXStagePosition;
    xStagePosition = adjustXStagePosition(xStagePosition);

    startPosition.setXInNanometers(xStagePosition);
    endPosition.setXInNanometers(xStagePosition);
    ScanPass proposedScanPass = new ScanPass(scanPassNumber,
                                             passCode,
                                             startPosition,
                                             endPosition);

    // Create a list of ReconstructionRegions that would like to participate in
    // this scanPass at the current x step size.
    ReconstructionRegionsInScanPass scanPassForSetOfReconstructionRegionsAtGivenXLocation =
        identifyReconstructionRegionsImagedByProposedScanPass(
            proposedScanPass,
            regionsInExecutionOrder,
            regionsInScopeFromPriorScanPass);
    
    return scanPassForSetOfReconstructionRegionsAtGivenXLocation;
  }
  
  /**
   * @author Roy Williams
   */
  private static ReconstructionRegionsInScanPass createScanPass(
      final int scanPassNumber,
      final int startingXStagePosition,
      final int unDitheredTravel,
      final int ditherAmplitude,
      ReconstructionRegionsInScanPass regionsInScopeFromPriorScanPass,
      List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionsInExecutionOrder) throws XrayTesterException
  {
    Assert.expect(regionsInScopeFromPriorScanPass != null);
    Assert.expect(regionsInExecutionOrder != null);

    // Get offset ready for the next ScanPass position
    int ditheredXPosition = ditheredPositionInNanometers(unDitheredTravel, ditherAmplitude);

    // Create the new ScanPass
    StagePositionMutable startPosition = new StagePositionMutable();
    StagePositionMutable endPosition = new StagePositionMutable();
    int xStagePosition = startingXStagePosition + ditheredXPosition;
    xStagePosition = adjustXStagePosition(xStagePosition);

    startPosition.setXInNanometers(xStagePosition);
    endPosition.setXInNanometers(xStagePosition);
    ScanPass proposedScanPass = new ScanPass(scanPassNumber,
                                             startPosition,
                                             endPosition);

    // Create a list of ReconstructionRegions that would like to participate in
    // this scanPass at the current x step size.
    ReconstructionRegionsInScanPass scanPassForSetOfReconstructionRegionsAtGivenXLocation =
        identifyReconstructionRegionsImagedByProposedScanPass(
            proposedScanPass,
            regionsInExecutionOrder,
            regionsInScopeFromPriorScanPass);
    return scanPassForSetOfReconstructionRegionsAtGivenXLocation;
  }

  /**
   * @author Roy Williams
   */
  private static ReconstructionRegionsInScanPass identifyReconstructionRegionsImagedByProposedScanPass(
      ScanPass proposedScanPass,
      List<Pair<ReconstructionRegion, SystemFiducialRectangle>> allRegionsToInclude,
      ReconstructionRegionsInScanPass regionsInScopeFromPriorScanPass)
  {
    Assert.expect(allRegionsToInclude != null);
    Assert.expect(regionsInScopeFromPriorScanPass != null);
    Assert.expect(proposedScanPass != null);

    MachineRectangle cameraArrayRectangle = MechanicalConversions.getCameraArrayRectangleAtMinSliceHeight();
    int leftSideOfCameraArray  = cameraArrayRectangle.getMinX();
    int rightSideOfCameraArray = cameraArrayRectangle.getMaxX();

    ReconstructionRegionsInScanPass regionsInScopeFromProposedScanPass =
      new ReconstructionRegionsInScanPass(proposedScanPass);

    SystemFiducialRectangle regionInSystemFiducialCoordinates;
    int xStagePosition = proposedScanPass.getEndPointInNanoMeters().getXInNanometers();
    int leftEdgeOfRegionInMachineCoordinates = -1;
    int rightEdgeOfRegionInMachineCoordinates  = -1;
    int count = 0;
    int proposedScanPassId = proposedScanPass.getId();
//    boolean printedFirst = false;

    // Retain those regions that are still in scope from regionsInScopeFromPriorScanPass
    // This is where we can/will notice when some ReconstructionRegions are no longer
    // imaged.
    for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair :
         regionsInScopeFromPriorScanPass.getImagedReconstructionRegionPairs())
    {
      regionInSystemFiducialCoordinates = regionPair.getSecond();
      leftEdgeOfRegionInMachineCoordinates = xStagePosition + regionInSystemFiducialCoordinates.getMinX();
      if (leftEdgeOfRegionInMachineCoordinates <= rightSideOfCameraArray)
      {
        regionsInScopeFromProposedScanPass.add(regionPair);
      }
      else
      {
        // Attrition of a ReconstructionRegion.
        regionsInScopeFromProposedScanPass.addRegionsExitingReconstructionScope(regionPair.getFirst());
      }
    }

    // Identify the spot in the list to resume if possible.
    Pair<ReconstructionRegion, SystemFiducialRectangle> fastForwardToThisReconstructionRegion = null;
    boolean fastForwardToLastReconstructionRegionPair = false;
    if (regionsInScopeFromPriorScanPass.hasLastReconstructionRegionPairBeginScopeLocation())
    {
      fastForwardToLastReconstructionRegionPair = true;
      fastForwardToThisReconstructionRegion = regionsInScopeFromPriorScanPass.getLastReconstructionRegionPairBeginScopeLocation();
    }
    else
    {
      if (proposedScanPassId > 0)
        return regionsInScopeFromProposedScanPass;
    }

    // Add starting regions.
    for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : allRegionsToInclude)
    {
      // slide down the list to where we left off last time.
      if (fastForwardToLastReconstructionRegionPair &&
          (regionPair.equals(fastForwardToThisReconstructionRegion) == false))
      {
        count++;
        continue;
      }

      else
        fastForwardToLastReconstructionRegionPair = false;
      
      // Add regions till we get one that fails.  It will handled on the next scanpass.
      regionInSystemFiducialCoordinates = regionPair.getSecond();

      rightEdgeOfRegionInMachineCoordinates = xStagePosition + regionInSystemFiducialCoordinates.getMaxX();

      if (rightEdgeOfRegionInMachineCoordinates >= leftSideOfCameraArray)
      {
        regionsInScopeFromProposedScanPass.add(regionPair);
        regionsInScopeFromProposedScanPass.addRegionsEnteringReconstructionScope(regionPair.getFirst());
      }
      else
      {
        fastForwardToThisReconstructionRegion = regionPair;
        regionsInScopeFromProposedScanPass.setLastReconstructionRegionPairBeginScopeLocation(regionPair);
        break;
      }
    }

    return regionsInScopeFromProposedScanPass;
  }

  /**
   * @author Roy Williams
   */
  private void commitReconstructionRegionsToScanPass(ReconstructionRegionsInScanPass scanPassReconstructionRegions)
  {
    Assert.expect(scanPassReconstructionRegions != null);

    ScanPass scanPass = scanPassReconstructionRegions.getScanPass();
    for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair :
         scanPassReconstructionRegions.getImagedReconstructionRegionPairs())
    {
      ReconstructionRegion reconstructionRegion = regionPair.getFirst();
      reconstructionRegion.addScanPass(scanPass);
    }

    for (ReconstructionRegion reconstructionRegion : scanPassReconstructionRegions.getRegionsExitingScanPassScope())
    {
      reconstructionRegion.addScanPass(scanPass);
    }
  }
  
  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  private void commitReconstructionRegionsToScanPass(Collection<ReconstructionRegion> reconstructionRegions, ScanPass scanPass)
  {
    Assert.expect(reconstructionRegions != null);
    Assert.expect(scanPass != null);

    for (ReconstructionRegion reconstructionRegion : reconstructionRegions)
    {
      reconstructionRegion.addScanPass(scanPass);
    }
  }

  /**
   * This method generates the basic scan path for imaging over an area defined by
   * the MechanicalConversions.
   *
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  private List<ScanPass> generateFixXScanPathOverArea(
          SignalCompensationEnum signalEnum, 
          HomogeneousImageGroup imageGroup, 
          MechanicalConversions mechanicalConversions,
          TestSubProgram testSubProgram) throws XrayTesterException
  {
    Assert.expect(mechanicalConversions != null);

    // retrieve the rectangleToImage from the MechanicalConversions.   The original
    // rectangle was modified to add on the alignment uncertainty in x, y, width, height.
    SystemFiducialRectangle rectangleToImageWithAlignmentUncertainty =
        mechanicalConversions.rectangleToImageWithAlignmentUncertaintyAddedToBorders();

    // put it as true at this moment, should be able to further optimize to reduce number of scan passes.
    boolean forceUseLargerTomographicAngle = true;//signalEnum.equals(SignalCompensationEnum.DEFAULT_LOW) || signalEnum.equals(SignalCompensationEnum.IL_1_5);
    // Calculate x travel properties.
    int xTravelRequired = getXTravelRequired(testSubProgram, rectangleToImageWithAlignmentUncertainty, forceUseLargerTomographicAngle);
    int startingStagePosition = getStartingStagePosition(testSubProgram, rectangleToImageWithAlignmentUncertainty, forceUseLargerTomographicAngle);
    // 0 as the main static start point
    int fixXStartingStagePosition = getStartingStageLocationForFixXScanPath();
    
    Map<Integer, Pair<Integer, String>> xLocationToSignalCompensationMap = new HashMap<Integer, Pair<Integer, String>>();
    xLocationToSignalCompensationMap = filterScanPassBaseOnCode(signalEnum);
    
    // System x motor position limit
    int xStagePositionLimit = 0;
    try
    {
      xStagePositionLimit = StringUtil.convertStringToInt(Config.getInstance().getStringValue(HardwareConfigEnum.X_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS));
    }
    catch (Exception e)
    {
      // do nothing
    }
    
    // Fix up the y positions based upon calculations done by MechanicalConversions.
    int startYPosition = mechanicalConversions.stageTravelStart();
    int endYPosition   = mechanicalConversions.stageTravelEnd();

    // Build up the scan path
    List<ScanPass> scanPasses = new ArrayList<ScanPass>();
    int currentScanPassNumber = -1;
    int xTravelTaken = 0;
    int nextXLocation = 0;
    int lastXLocation = 0;
    boolean log=false; //debug purpose
    
    for (int i=0; i<xLocationToSignalCompensationMap.size(); i++)
    {
      int XLocationInNanometer = xLocationToSignalCompensationMap.get(i).getFirst() + fixXStartingStagePosition;
      String passCode = xLocationToSignalCompensationMap.get(i).getSecond();
      
      if(i>=xLocationToSignalCompensationMap.size() || testSubProgram.getTestProgram().getProject().isUseLargeTomoAngle()==true
              || forceUseLargerTomographicAngle==true)
        nextXLocation = XLocationInNanometer;
      else
        nextXLocation = xLocationToSignalCompensationMap.get(i+1).getFirst() + fixXStartingStagePosition;
      
      if(nextXLocation <= startingStagePosition)
        continue;
      
      if(log==true && testSubProgram.getTestProgram().getProject().isUseLargeTomoAngle()==false)
      {
        log = false;
        System.out.println("ScanPathExtraS (Alignment): "+testSubProgram.getTestProgram().getProject().getName()+", "+(startingStagePosition-XLocationInNanometer)+"nm");
      }
      
      if(i==0 || testSubProgram.getTestProgram().getProject().isUseLargeTomoAngle()==true
              || forceUseLargerTomographicAngle==true)
        lastXLocation = XLocationInNanometer;
      else
        lastXLocation = xLocationToSignalCompensationMap.get(i-1).getFirst() + fixXStartingStagePosition;
      
      xTravelTaken = lastXLocation - startingStagePosition;
              
      if (xTravelTaken > xTravelRequired)
      {
        break;
      }
      
      if (isPassCodeMatchWithSignalCompensation(passCode, signalEnum)==false)
      {
        continue;
      }
      
      if (XLocationInNanometer >= startingStagePosition)
      {
        currentScanPassNumber++;
        
        // Create the new ScanPass
        StagePositionMutable startPosition = new StagePositionMutable();
        StagePositionMutable endPosition = new StagePositionMutable();
        startPosition.setXInNanometers(adjustXStagePosition(XLocationInNanometer));
        endPosition.setXInNanometers(adjustXStagePosition(XLocationInNanometer));
        
        ScanPass scanPass = new ScanPass(currentScanPassNumber,
                                         passCode,
                                         startPosition,
                                         endPosition);
        
        // Determine which direction in y we're going
        if ((currentScanPassNumber % 2) == 0)
        {
          startPosition.setYInNanometers(startYPosition);
          endPosition.setYInNanometers(endYPosition);
          scanPass.directionOfTravel(ScanPassDirectionEnum.FORWARD);
        }
        else
        {
          startPosition.setYInNanometers(endYPosition);
          endPosition.setYInNanometers(startYPosition);
          scanPass.directionOfTravel(ScanPassDirectionEnum.REVERSE);
        }

        // Add it to the list.
        scanPass.setScanPassMechanicalConversions(mechanicalConversions);
        scanPasses.add(scanPass);
        testSubProgram.addScanPassToImageGroup(scanPass,imageGroup);
      }
      
      // scan pass should not exist x position limit, we will adjust the first 
      // scan pass which exist, but will break this loop after that.
      if(XLocationInNanometer > xStagePositionLimit)
      {
        break;
      }
    }
    
    return scanPasses;
  }
  
  /**
   * This method generates the basic scan path for imaging over an area defined by
   * the MechanicalConversions.
   *
   * @author Dave Ferguson
   * @author Roy Williams
   */
  private List<ScanPass> generateScanPathOverArea(MechanicalConversions mechanicalConversions,
                                                  int meanStepSizeInNanomters,
                                                  int ditherAmplitude) throws XrayTesterException
  {
    Assert.expect(mechanicalConversions != null);

    // retrieve the rectangleToImage from the MechanicalConversions.   The original
    // rectangle was modified to add on the alignment uncertainty in x, y, width, height.
    SystemFiducialRectangle rectangleToImageWithAlignmentUncertainty =
        mechanicalConversions.rectangleToImageWithAlignmentUncertaintyAddedToBorders();

    // Calculate x travel properties.
    MachineRectangle cameraArrayRectangle = MechanicalConversions.getCameraArrayRectangleAtMinSliceHeight();
    int imageableWidthOfFirstCamera = _xrayCameraArray.getImageableWidthInNanometers(MagnificationEnum.getCurrentMinSliceHeight());

    // First, last and all cameras have the same imageable width.
    int imageableWidthOfLastCamera        = imageableWidthOfFirstCamera;

    // subtract imageableWidth of left-most camera from xTravelRequired
    // already has subtracted 4 pixels
    // already has added alignment uncertainty
    int xTravelRequired = cameraArrayRectangle.getWidth()                     +
                          rectangleToImageWithAlignmentUncertainty.getWidth() -
                          imageableWidthOfFirstCamera                         -
                          imageableWidthOfLastCamera;

    // add imageableWidth of left-most camera from startXPosition
    int startingStagePosition = cameraArrayRectangle.getMinX()                     -
                                rectangleToImageWithAlignmentUncertainty.getMaxX() +
                                imageableWidthOfFirstCamera;

    // Fix up the y positions based upon calculations done by MechanicalConversions.
    int startYPosition = mechanicalConversions.stageTravelStart();
    int endYPosition   = mechanicalConversions.stageTravelEnd();

    // Build up the scan path
    List<ScanPass> scanPasses = new ArrayList<ScanPass>();
    int currentScanPassNumber = -1;
    int xTravel = 0;
    int xTravelTaken = 0;
    int unDitheredXPosition = 0;
    while (xTravelTaken <= xTravelRequired)
    {
      currentScanPassNumber++;

      // Create the new ScanPass
      StagePositionMutable startPosition = new StagePositionMutable();
      StagePositionMutable endPosition = new StagePositionMutable();
      int xStagePosition = startingStagePosition + xTravel;
      xStagePosition = adjustXStagePosition(xStagePosition);

      xTravelTaken = xTravel;
      startPosition.setXInNanometers(xStagePosition);
      endPosition.setXInNanometers(xStagePosition);
      ScanPass scanPass = new ScanPass(currentScanPassNumber,
                                       startPosition,
                                       endPosition);

      // Determine which direction in y we're going
      if ((currentScanPassNumber % 2) == 0)
      {
        startPosition.setYInNanometers(startYPosition);
        endPosition.setYInNanometers(endYPosition);
        scanPass.directionOfTravel(ScanPassDirectionEnum.FORWARD);
      }
      else
      {
        startPosition.setYInNanometers(endYPosition);
        endPosition.setYInNanometers(startYPosition);
        scanPass.directionOfTravel(ScanPassDirectionEnum.REVERSE);
      }

      // Add it to the list.
      scanPasses.add(scanPass);

      // Get offset ready for the next ScanPass (in this loop)
      if (_stepOverridePercentage > 0)
      {
        int step = (int) Math.round(_maxStepSizeLimitInNanometers * _stepOverridePercentage);
        xTravel = xTravel + step;
      }
      else
      {
        unDitheredXPosition += meanStepSizeInNanomters;
        xTravel = ditheredPositionInNanometers(unDitheredXPosition, ditherAmplitude);
      }
    }
    return scanPasses;
  }
  
  /**
   * Updates the reconstruction regions in a heterogeneous image group to reference the scan paths
   * that image them.  This is necessary after scan path integration.
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  private void applyVariableDivnOnScanPath(
          TestSubProgram testSubProgram, 
          ImageGroup imageGroup,
          ScanPassSetting scanPassSetting) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(imageGroup != null);
    Assert.expect(scanPassSetting != null);
    
    double appropriateMaxSliceHeight = XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(testSubProgram.getTestProgram().getProject().getPanel().getThicknessInNanometers(), testSubProgram.getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers());
    
    // First, we have to clear out the old scan passes.
    List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionsInExecutionOrder = imageGroup.getRegionPairsSortedInExecutionOrder();

    for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : regionsInExecutionOrder)
    {
      ReconstructionRegion region = regionPair.getFirst();
      region.clearScanPasses(scanPassSetting);
    }
    
    // Now we can update the reconstruction regions with the new scan passes.
    ReconstructionRegionsInScanPass regionsInScopeFromPriorScanPass = new ReconstructionRegionsInScanPass();
    for (ScanPass scanPass : imageGroup.getScanPath().getScanPasses())
    {
      // Create a list of ReconstructionRegions that will be imaged by this scan pass.
      ReconstructionRegionsInScanPass regionsInScopeFromCurrentScanPass =
          identifyReconstructionRegionsImagedByProposedScanPass(
              scanPass,
              regionsInExecutionOrder,
              regionsInScopeFromPriorScanPass);
      regionsInScopeFromPriorScanPass = regionsInScopeFromCurrentScanPass;
      
      // Add a local mechanicalConversion for each scanpass
      if(regionsInScopeFromCurrentScanPass.getNumberOfReconstructionRegionsUsingProposedScanPass()>0)
      {
        Collection<ReconstructionRegion> regions = new ArrayList<ReconstructionRegion>();
        for(ReconstructionRegion region : regionsInScopeFromCurrentScanPass.getRegionsExitingScanPassScope())
          regions.add(region);
        for(Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair :regionsInScopeFromCurrentScanPass.getImagedReconstructionRegionPairs())
          regions.add(regionPair.getFirst());    
        
        HomogeneousImageGroup group = new HomogeneousImageGroup(regions, false);
        
        List<ProcessorStrip> strips = createProcessorStripsForImageGroupReusingAreasDefinedInOriginalProcessorStrips(
          testSubProgram.getProcessorStripsForInspectionRegions(),
          group);
        
//        List<ProcessorStrip> strips = reCreateProcessorStripsForImageGroupReusingAreasDefinedInOriginalProcessorStrips(
//          testSubProgram.getProcessorStripsForInspectionRegions(),
//          group, ScanPassDirectionEnum.BIDIRECTION);
            
        MechanicalConversions scanPassMechanicalConversions = new MechanicalConversions(
        strips,
        imageGroup.getAffineTransform(),
        Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
        ProcessorStrip.getOverlapNeededForLargestPossibleJointInNanometers(),
        appropriateMaxSliceHeight,
        Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers(testSubProgram.getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers())),
        _xRayCameras,
        group.getNominalAreaToImage());
        
        scanPass.getStartPointInNanoMeters().setYInNanometers(scanPassMechanicalConversions.stageTravelStart());
        scanPass.getEndPointInNanoMeters().setYInNanometers(scanPassMechanicalConversions.stageTravelEnd());
        scanPass.setScanPassMechanicalConversions(scanPassMechanicalConversions);
        testSubProgram.addScanPassToImageGroup(scanPass,group);
        
        commitReconstructionRegionsToScanPass(regions, scanPass);
      }
    }
    
    updateImageReconstructionEnginesInUseBasedUponAdditionalScanPathInfo(imageGroup.getScanPath());
  }
  
  /**
   * Updates the reconstruction regions in a heterogeneous image group to reference the scan paths
   * that image them.  This is necessary after scan path integration.
   * @author Kay Lannen
   */
  private void updateReconstructionRegionsForIntegratedScanPath(List<ImageGroup> imageGroups)
  {
    // First, we have to clear out the old scan passes.
    for (ImageGroup imageGroup : imageGroups)
    {
      List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionsInExecutionOrder = imageGroup.getRegionPairsSortedInExecutionOrder();

      for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : regionsInExecutionOrder)
      {
        ReconstructionRegion region = regionPair.getFirst();
        region.clearScanPasses();
      }
    }

    // Now we can update the reconstruction regions with the new scan passes.
    for (ImageGroup imageGroup : imageGroups)
    {
      List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionsInExecutionOrder = imageGroup.getRegionPairsSortedInExecutionOrder();

      ReconstructionRegionsInScanPass regionsInScopeFromPriorScanPass = new ReconstructionRegionsInScanPass();
      for (ScanPass scanPass : imageGroup.getScanPath().getScanPasses())
      {
        // Create a list of ReconstructionRegions that will be imaged by this scan pass.
        ReconstructionRegionsInScanPass regionsInScopeFromCurrentScanPass =
            identifyReconstructionRegionsImagedByProposedScanPass(
                scanPass,
                regionsInExecutionOrder,
                regionsInScopeFromPriorScanPass);
        commitReconstructionRegionsToScanPass(regionsInScopeFromCurrentScanPass);
        regionsInScopeFromPriorScanPass = regionsInScopeFromCurrentScanPass;
      }
    }
  }

  /**
   * @author Roy Williams
   */
  private void printPanelRectangle(String name, PanelRectangle panelRectangle)
  {
    System.out.println(name + " " +
                       panelRectangle.getMinX() + " " +
                       panelRectangle.getMinY() + " " +
                       panelRectangle.getWidth() + " " +
                       panelRectangle.getHeight());
  }

  /**
   * @author Roy Williams
   */
  List<Pair<ImageGroup, AlignmentScanPathSubSection>> alignmentRegionsBasedUponScanOrder(
      List<Pair<ImageGroup, AlignmentScanPathSubSection>> list)
  {
    Comparator<Pair<ImageGroup, AlignmentScanPathSubSection>> comparator = new Comparator<Pair<ImageGroup, AlignmentScanPathSubSection>>()
    {
      public int compare(Pair<ImageGroup, AlignmentScanPathSubSection> p1,
                         Pair<ImageGroup, AlignmentScanPathSubSection> p2)
      {
        Assert.expect(p1 != null);
        Assert.expect(p2 != null);
        int start1 = p1.getSecond().getStartingScanPass();
        int start2 = p2.getSecond().getStartingScanPass();
        if (start1 < start2)
          return -1;
        else if (start1 > start2)
          return 1;
        return 0;
      }
    };
    Collections.sort(list, comparator);
    return list;
  }

  /**
   * @author Roy Williams
   */
  private int scanPassesDeductedFromSetForAlignmentSection(
      Set<Integer> scanPassSet,
      AlignmentScanPathSubSection alignmentScanSection,
      boolean commit)
  {
    Assert.expect(scanPassSet != null);
    Assert.expect(alignmentScanSection != null);

    int deductedScanPasses = 0;
    for (int i=alignmentScanSection.getStartingScanPass();
               i<=alignmentScanSection.getLastScanPass();
               i++)
    {
      if (scanPassSet.contains(i))
      {
        deductedScanPasses++;
        if (commit)
          scanPassSet.remove(i);
      }
    }
    return deductedScanPasses;
  }

  /**
   * Creates a new scan path consisting of three segments for alignment, traversal
   * of stage (incrementally to start of inspection), and inspection.   The resulting
   * scan path will be re-ordered (0,1,2, etc) and start/stop positions will be
   * alternating.
   *
   * @author Roy Williams
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  private List<ScanPath> mergeAlignmentToFixXScanPath(
      TestSubProgram        testSubProgram,
      ScanPath              mainScanPath,
      boolean               isCustomizedAlignmentPathSetting) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(mainScanPath != null);

    if(testSubProgram.isLowMagnification() != XrayTester.isLowMagnification() )
    {
      ImageAcquisitionEngine.changeMagnification( testSubProgram.getMagnificationType() );
      if(getEnableStepSizeOptimization())
        forceCalculateMaxStepSizeInNanometers(testSubProgram.getTestProgram().getProject().getPanel().getThicknessInNanometers(), testSubProgram.getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers());
    }
    
    // need to recalculate for every testSubProgram, because different magnification(only) will produce different XLocation.
    _xLocationToPassCodeMap.clear();
    Config.getInstance().calculateXLocationToPassCode();
    _xLocationToPassCodeMap = Config.getInstance().getXLocationToPassCodeInfo();
    
    Collection<ReconstructionRegion> originalAlignmentRegions = testSubProgram.getAlignmentRegions();
    AffineTransform aggregateAlignmentTransform = testSubProgram.getAggregateManualAlignmentTransform();

    // Initially (and only once) let's go ahead and generate a scanPath (and
    // ImageGroup) for each alignment region.   The resulting ImageGroup is
    // especially useful later.
    for (ReconstructionRegion alignmentRegion : originalAlignmentRegions)
    {
      alignmentRegion.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
      ImageGroup imageGroup = generateFixXScanPathForAlignmentRegion(alignmentRegion, aggregateAlignmentTransform, testSubProgram);
      Pair<ImageGroup, AlignmentScanPathSubSection> infoPair =
         new Pair<ImageGroup, AlignmentScanPathSubSection>(imageGroup, null);
      _alignmentRegionInfoMap.put(alignmentRegion, infoPair);
    }

    // Find all scan passes (under the main scanPath) that image any part of an
    // alignment region.  If things to well we will discard the prevoiusly generated
    // individual scanPaths.
    List<Pair<ImageGroup, AlignmentScanPathSubSection>> orderedAlignmentRegions =
        identifyAlignmentScanPassSectionsInBasicScanPath(
            originalAlignmentRegions,
            mainScanPath);

    boolean alignmentAcquisitionMode = _acquisitionMode.equals(ImageAcquisitionModeEnum.ALIGNMENT);

    // First generate the camera acquisiton parameters so we can use the ProjectionSettings
    // to determine the maximum amount of memory consumed.   The amount of memory consumed will
    // regulate the number of scanPasses allowed before we must scan the alignment
    // regions
    Map<AbstractXrayCamera, XrayCameraAcquisitionParameters> xrayCameraAcquisitionParameters = new HashMap<AbstractXrayCamera,XrayCameraAcquisitionParameters>();

    MechanicalConversions mechanicalConversions = mainScanPath.getMechanicalConversions();
    int numberParticipants = mechanicalConversions.getNumberProcessorStrips();
    int maxNumberOfLinesCapturedPerScanPass = appendXrayCameraAcquisitionParameters(
        numberParticipants,
        mainScanPath,
        xrayCameraAcquisitionParameters);
    long irpMemoryAllocatedForProjectionMemory = ImageReconstructionEngine.getMemoryAllocatedForProjectionMemory();
    
    // Memory control - to avoid RT PSH and Merge Alignment fight for same memory resource
    if(Config.isRealTimePshEnabled() && testSubProgram.isSufficientMemoryForRealTimePsh())
    {
      irpMemoryAllocatedForProjectionMemory = (long)(irpMemoryAllocatedForProjectionMemory * 0.35);
    }

    // We can just get any camera so we can get the sensor attributes
    // Walk through the list of alignment scan paths.  And, as long as the full alignment
    // scan pass will fit into the projection memory, use it.
    AbstractXrayCamera xRayCamera = _xrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
    // Swee Yee Wong - XCR-2630 Support New X-Ray Camera
	long irpMemoryUsedPerScanPass = maxNumberOfLinesCapturedPerScanPass *
                                    xRayCamera.getSensorWidthInPixelsWithOverHeadByte() *
                                    XrayCameraArray.getNumberOfCameras();
    
    long maxNumberScanPassesBeforeMemoryConsumed = irpMemoryAllocatedForProjectionMemory /
                                                   irpMemoryUsedPerScanPass;
    int halfScanPassesDone = mainScanPath.getScanPasses().size() / 2 + 1;

    // Initially deduct enough scanPasses to get completely acrsoss the camera
    // array just in case the alignment regions barely fit... thus leaving no more
    // room for anything else.
    MachineRectangle cameraArrayRectangle = MechanicalConversions.getCameraArrayRectangleAtMinSliceHeight();
    
    // max step size of new scan is 0.89 of max step size limit of system.
    // 0.89 is calculated from Tracy's model
    double maxStepSize = getMaxStepSizeLimitInNanometers()*0.89; 
    double highestSignalCompensation = 1;
    for (Pair<ImageGroup,AlignmentScanPathSubSection> alignmentRegionInfo : orderedAlignmentRegions)
    {
      double maxIL = alignmentRegionInfo.getSecond().getHighestSignalCompensation();
      if (maxIL > highestSignalCompensation)
        highestSignalCompensation = maxIL;
    }
    
    int singleRegionAcrossArrayStepCount = cameraArrayRectangle.getWidth() /
                                           (int)(maxStepSize/highestSignalCompensation);
    long remainingScanPassesBeforeProjectionMemoryConsumed =
        maxNumberScanPassesBeforeMemoryConsumed -
        singleRegionAcrossArrayStepCount;
    Set<Integer> scanPassSet = new HashSet<Integer>();
    for (ScanPass scanPass : mainScanPath.getScanPasses())
      scanPassSet.add(scanPass.getId());

    // Assemble the scan passes that image the alignment regions first.
    List<ScanPath> customScanPaths = new ArrayList<ScanPath>();
    List<ScanPass> orderedMainScanPath = new ArrayList<ScanPass>();
    for (Pair<ImageGroup,AlignmentScanPathSubSection> alignmentRegionInfo : orderedAlignmentRegions)
    {
      ImageGroup imageGroup = alignmentRegionInfo.getFirst();
      AlignmentScanPathSubSection alignmentScanSection = alignmentRegionInfo.getSecond();
      ReconstructionRegion alignmentRegion = alignmentScanSection.getAlignmentRegion();
      ScanPath scanPathForAlignmentRegion = imageGroup.getScanPath();

      // Five conditions will trigger a separate scanPath:
      // a) This TestSubProgram contains Optical Region
      // b) This is an Alignment acquisition mode thus doing only alignment regions.
      // c) Intersect of ImageGroup with area of Main scanPath was insufficient (or non-existant).
      // d) The alignment region is greater than half way through the board.
      // e) We do not have enough memory in the IRP to bundle the region with inspection scanPasses.
      
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP) && testSubProgram.hasEnabledOpticalCameraRectangles())
      {
        _alignmentRegionInfoMap.remove(alignmentRegion);
        _imageGroups.add(imageGroup);
        customScanPaths.add(scanPathForAlignmentRegion);
        continue;
      }
      
      if (isCustomizedAlignmentPathSetting)
      {
        _alignmentRegionInfoMap.remove(alignmentRegion);
        _imageGroups.add(imageGroup);
        customScanPaths.add(scanPathForAlignmentRegion);
        continue;
      }
      

      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      if (testSubProgram.getTestProgram().getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod())
      {
        _alignmentRegionInfoMap.remove(alignmentRegion);
        _imageGroups.add(imageGroup);
        customScanPaths.add(scanPathForAlignmentRegion);
        continue;
      }
      
      if (alignmentScanSection.forceCustomPathGeneration())
      {
        _alignmentRegionInfoMap.remove(alignmentRegion);
        _imageGroups.add(imageGroup);
        customScanPaths.add(scanPathForAlignmentRegion);
        continue;
      }

      int lastScanPassForAlignmentSection = alignmentScanSection.getLastScanPass();
      int deductScanPassCount = scanPassesDeductedFromSetForAlignmentSection(scanPassSet, alignmentScanSection, false);
      if (remainingScanPassesBeforeProjectionMemoryConsumed - deductScanPassCount > 0)
      {
        remainingScanPassesBeforeProjectionMemoryConsumed -= scanPassesDeductedFromSetForAlignmentSection(scanPassSet, alignmentScanSection, true);
      }
      else if (alignmentAcquisitionMode || alignmentScanSection == null)
      {
        // This case demonstrates "a" and "b" above.   So, this will definitely be
        // a customScanPath.
        _alignmentRegionInfoMap.remove(alignmentRegion);
        _imageGroups.add(imageGroup);
        customScanPaths.add(scanPathForAlignmentRegion);
        continue;
      }
      else if (lastScanPassForAlignmentSection > halfScanPassesDone ||
          lastScanPassForAlignmentSection > maxNumberScanPassesBeforeMemoryConsumed)
      {
        // before we put this region into death, we give it a chance by go back the orderedMainScanPath to check 
        // wheater the scanpass going to use by this region is already used by previous region.
        // if yes, yeah! we can merge this region.
        int startingScanPass = alignmentScanSection.getStartingScanPass();
        int scanPassCount = -1;
        boolean noMerge=false;
        for (ScanPass scanPass : mainScanPath.getScanPasses())
        {
          scanPassCount++;
          if (scanPassCount >= startingScanPass)
          {
            if (scanPassCount <= lastScanPassForAlignmentSection)
            {
              // Only turn scan pass which match with alignment region's IL
              //if (isPassCodeMatchWithSignalCompensation(scanPass.getCode(), alignmentRegion.getSignalCompensation())==false)
              //  continue;
              if (orderedMainScanPath.contains(scanPass) == false)
              {
                noMerge = true;
                break;
              }
            }
            else
              break;
          }
        }
        
        // This case demonstrates "c" and "d" above.   So, this will definitely be
        // a customScanPath.
        if (noMerge==true)
        {
          _alignmentRegionInfoMap.remove(alignmentRegion);
          _imageGroups.add(imageGroup);
          customScanPaths.add(scanPathForAlignmentRegion);
          continue;
        }
      }
      
      // Finally!  We can (potentially) integrate this alignmentRegion into the main ScanPath.
      // Work is assigned to the IRP covering the area above a ProcessorStrip.  If
      // we can get an IRP assigned,
      boolean assignedProcessorStrip = false;
      List<ProcessorStrip> processorStrips = mainScanPath.getMechanicalConversions().getProcessorStrips();
      PanelRectangle alignmentRegionPanelRectangle= alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters();
      for (ProcessorStrip processorStrip : processorStrips)
      {
        if (processorStrip.getRegion().contains(alignmentRegionPanelRectangle))
        {
          alignmentRegion.setReconstructionEngineId(processorStrip.getReconstructionEngineId());
          assignedProcessorStrip = true;
          break;
        }
      }

      // AlignmentRegions are not guaranteed to be in the imageable area of the
      // board.   Thus, we will dedicate scanPasses to cover the region.
      if (assignedProcessorStrip == false)
      {
        _alignmentRegionInfoMap.remove(alignmentRegion);
        customScanPaths.add(scanPathForAlignmentRegion);
        continue;
      }

      // Iterate through the basicScanPath and assign this ReconstructionRegion to be
      // reconstructed using them.
      alignmentRegion.clearScanPasses();
      int scanPassCount = -1;
      int startingScanPass = alignmentScanSection.getStartingScanPass();
      for (ScanPass scanPass : mainScanPath.getScanPasses())
      {
        scanPassCount++;
        if (scanPassCount >= startingScanPass)
        {
          if (scanPassCount <= lastScanPassForAlignmentSection)
          {
            // Only turn scan pass which match with alignment region's IL
            //if (isPassCodeMatchWithSignalCompensation(scanPass.getCode(), alignmentRegion.getSignalCompensation())==false)
            //  continue;
            
            scanPass.setCoversAlignmentRegions(true);
            if (orderedMainScanPath.contains(scanPass) == false)
              orderedMainScanPath.add(scanPass);
            scanPass.setProjectionType(ProjectionTypeEnum.INSPECTION_AND_ALIGNMENT);
            alignmentRegion.addScanPass(scanPass);
            
            // Adjust Scan pass start end to consider the alighment region when variable div n enable
            if(testSubProgram.getTestProgram().getProject().isVarDivNEnable())
            {
              Collection<ReconstructionRegion> regions = new ArrayList<ReconstructionRegion>();
              regions.add(alignmentRegion);
              HomogeneousImageGroup group = putRegionsIntoExistingScanPass(testSubProgram, scanPass, regions);
                            
              testSubProgram.removeScanPassToImageGroup(scanPass);
              testSubProgram.addScanPassToImageGroup(scanPass,group);
            }
          }
          else
            break;
        }
      }
    }

    // Iterate through the mainScanPath and append only those scanpasses that
    // aren't already included.
    for (ScanPass scanPass : mainScanPath.getScanPasses())
    {
      if (orderedMainScanPath.contains(scanPass) == false)
        orderedMainScanPath.add(scanPass);
    }
    mainScanPath.setScanPasses(orderedMainScanPath);

    // CR1022 fix by LeeHerng. We want to make sure we add custom scan paths here for
    // certain alignment regions which are not covered by orderedAlignmentRegions.
    // Normally we handle this case for No Load/No Test
    if (originalAlignmentRegions.size() > orderedAlignmentRegions.size())
    {
      customScanPaths.addAll(getCustomScanPaths(originalAlignmentRegions,orderedAlignmentRegions));
    }

    // Adding in Alignment scanPath to ImageReconstructionEnginesInUse for customeScanPaths
    for (ScanPath scanPath : customScanPaths)
    {
      updateImageReconstructionEnginesInUseBasedUponAdditionalScanPathInfo(scanPath);
    }

    return customScanPaths;
  }
  
  /**
   * Creates a new scan path consisting of three segments for alignment, traversal
   * of stage (incrementally to start of inspection), and inspection.   The resulting
   * scan path will be re-ordered (0,1,2, etc) and start/stop positions will be
   * alternating.
   *
   * @author Roy Williams
   */
  private List<ScanPath> mergeAlignmentToScanPath(
      TestSubProgram        testSubProgram,
      HomogeneousImageGroup mainAreaImageGroup,
      ScanPath              mainScanPath,
      boolean               isCustomizedAlignmentPathSetting) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(mainAreaImageGroup != null);
    Assert.expect(mainScanPath != null);

    if(testSubProgram.isLowMagnification() != XrayTester.isLowMagnification() )
    {
      ImageAcquisitionEngine.changeMagnification( testSubProgram.getMagnificationType() );
      if(getEnableStepSizeOptimization())
        forceCalculateMaxStepSizeInNanometers(testSubProgram.getTestProgram().getProject().getPanel().getThicknessInNanometers(), testSubProgram.getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers());
    }
    Collection<ReconstructionRegion> originalAlignmentRegions = testSubProgram.getAlignmentRegions();
    AffineTransform aggregateAlignmentTransform = testSubProgram.getAggregateManualAlignmentTransform();

    // Initially (and only once) let's go ahead and generate a scanPath (and
    // ImageGrouup) for each alignment region.   The resulting ImageGrouup is
    // especially useful later.
    for (ReconstructionRegion alignmentRegion : originalAlignmentRegions)
    {
      alignmentRegion.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
      ImageGroup imageGroup = generateScanPathForAlignmentRegion(alignmentRegion, aggregateAlignmentTransform);
      Pair<ImageGroup, AlignmentScanPathSubSection> infoPair =
         new Pair<ImageGroup, AlignmentScanPathSubSection>(imageGroup, null);
      _alignmentRegionInfoMap.put(alignmentRegion, infoPair);
    }

    // Find all scan passes (under the main scanPath) that image any part of an
    // alignment region.  If things to well we will discard the prevoiusly generated
    // individual scanPaths.
    List<Pair<ImageGroup, AlignmentScanPathSubSection>> orderedAlignmentRegions =
        identifyAlignmentScanPassSectionsInBasicScanPath(
            originalAlignmentRegions,
            mainScanPath);

    boolean alignmentAcquisitionMode = _acquisitionMode.equals(ImageAcquisitionModeEnum.ALIGNMENT);

    // First generate the camera acquisiton parameters so we can use the ProjectionSettings
    // to determine the maximum amount of memory consumed.   The amount of memory consumed will
    // regulate the number of scanPasses allowed before we must scan the alignment
    // regions
    Map<AbstractXrayCamera, XrayCameraAcquisitionParameters> xrayCameraAcquisitionParameters = new HashMap<AbstractXrayCamera,XrayCameraAcquisitionParameters>();

    MechanicalConversions mechanicalConversions = mainScanPath.getMechanicalConversions();
    int numberParticipants = mechanicalConversions.getNumberProcessorStrips();
    int maxNumberOfLinesCapturedPerScanPass = appendXrayCameraAcquisitionParameters(
        numberParticipants,
        mainScanPath,
        xrayCameraAcquisitionParameters);
    long irpMemoryAllocatedForProjectionMemory = ImageReconstructionEngine.getMemoryAllocatedForProjectionMemory();
    
    // Memory control - to avoid RT PSH and Merge Alignment fight for same memory resource
    if(Config.isRealTimePshEnabled() && testSubProgram.isSufficientMemoryForRealTimePsh())
    {
      irpMemoryAllocatedForProjectionMemory = (long)(irpMemoryAllocatedForProjectionMemory * 0.35);
    }

    // We can just get any camera so we can get the sensor attributes
    // Walk through the list of alignment scan paths.  And, as long as the full alignment
    // scan pass will fit into the projection memory, use it.
    AbstractXrayCamera xRayCamera = _xrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
    // Swee Yee Wong - XCR-2630 Support New X-Ray Camera
	long irpMemoryUsedPerScanPass = maxNumberOfLinesCapturedPerScanPass *
                                    xRayCamera.getSensorWidthInPixelsWithOverHeadByte() *
                                    XrayCameraArray.getNumberOfCameras();
    long maxNumberScanPassesBeforeMemoryConsumed = irpMemoryAllocatedForProjectionMemory /
                                                   irpMemoryUsedPerScanPass;
    int halfScanPassesDone = mainScanPath.getScanPasses().size() / 2 + 1;

    // Initially deduct enough scanPasses to get completely acrsoss the camera
    // array just in case the alignment regions barely fit... thus leaving no more
    // room for anything else.
    MachineRectangle cameraArrayRectangle = MechanicalConversions.getCameraArrayRectangleAtMinSliceHeight();
    int meanStepSizeInNanoMeters = mainAreaImageGroup.getMeanStepSizeInNanoMeters();
    int singleRegionAcrossArrayStepCount = cameraArrayRectangle.getWidth() /
                                           meanStepSizeInNanoMeters;
    long remainingScanPassesBeforeProjectionMemoryConsumed =
        maxNumberScanPassesBeforeMemoryConsumed -
        singleRegionAcrossArrayStepCount;
    Set<Integer> scanPassSet = new HashSet<Integer>();
    for (ScanPass scanPass : mainScanPath.getScanPasses())
      scanPassSet.add(scanPass.getId());

    // Assemble the scan passes that image the alignment regions first.
    List<ScanPath> customScanPaths = new ArrayList<ScanPath>();
    List<ScanPass> orderedMainScanPath = new ArrayList<ScanPass>();
    for (Pair<ImageGroup,AlignmentScanPathSubSection> alignmentRegionInfo : orderedAlignmentRegions)
    {
      ImageGroup imageGroup = alignmentRegionInfo.getFirst();
      AlignmentScanPathSubSection alignmentScanSection = alignmentRegionInfo.getSecond();
      ReconstructionRegion alignmentRegion = alignmentScanSection.getAlignmentRegion();
      ScanPath scanPathForAlignmentRegion = imageGroup.getScanPath();

      // Five conditions will trigger a separate scanPath:
      // a) This TestSubProgram contains Optical Region
      // b) This is an Alignment acquisition mode thus doing only alignment regions.
      // c) Intersect of ImageGroup with area of Main scanPath was insufficient (or non-existant).
      // d) The alignment region is greater than half way through the board.
      // e) We do not have enough memory in the IRP to bundle the region with inspection scanPasses.

      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP) && testSubProgram.hasEnabledOpticalCameraRectangles())
      {
        _alignmentRegionInfoMap.remove(alignmentRegion);
        _imageGroups.add(imageGroup);
        customScanPaths.add(scanPathForAlignmentRegion);
        continue;
      }
      
      if (isCustomizedAlignmentPathSetting)
      {
        _alignmentRegionInfoMap.remove(alignmentRegion);
        _imageGroups.add(imageGroup);
        customScanPaths.add(scanPathForAlignmentRegion);
        continue;
      }
      
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      if (testSubProgram.getTestProgram().getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod())
      {
        _alignmentRegionInfoMap.remove(alignmentRegion);
        _imageGroups.add(imageGroup);
        customScanPaths.add(scanPathForAlignmentRegion);
        continue;
      }
      
      if (alignmentScanSection.forceCustomPathGeneration())
      {
        _alignmentRegionInfoMap.remove(alignmentRegion);
        _imageGroups.add(imageGroup);
        customScanPaths.add(scanPathForAlignmentRegion);
        continue;
      }

      int lastScanPassForAlignmentSection = alignmentScanSection.getLastScanPass();
      int deductScanPassCount = scanPassesDeductedFromSetForAlignmentSection(scanPassSet, alignmentScanSection, false);
      if (remainingScanPassesBeforeProjectionMemoryConsumed - deductScanPassCount > 0)
      {
        remainingScanPassesBeforeProjectionMemoryConsumed -= scanPassesDeductedFromSetForAlignmentSection(scanPassSet, alignmentScanSection, true);
      }

      else if (alignmentAcquisitionMode || alignmentScanSection == null)
      {
        // This case demonstrates "a" and "b" above.   So, this will definitely be
        // a customScanPath.
        _alignmentRegionInfoMap.remove(alignmentRegion);
        _imageGroups.add(imageGroup);
        customScanPaths.add(scanPathForAlignmentRegion);
        continue;
      }

      else if (lastScanPassForAlignmentSection > halfScanPassesDone ||
          lastScanPassForAlignmentSection > maxNumberScanPassesBeforeMemoryConsumed)
      {
        // This case demonstrates "c" and "d" above.   So, this will definitely be
        // a customScanPath.
        _alignmentRegionInfoMap.remove(alignmentRegion);
        _imageGroups.add(imageGroup);
        customScanPaths.add(scanPathForAlignmentRegion);
        continue;
      }

      // Finally!  We can (potentially) integrate this alignmentRegion into the main ScanPath.
      // Work is assigned to the IRP covering the area above a ProcessorStrip.  If
      // we can get an IRP assigned,
      boolean assignedProcessorStrip = false;
      List<ProcessorStrip> processorStrips = mainScanPath.getMechanicalConversions().getProcessorStrips();
      PanelRectangle alignmentRegionPanelRectangle= alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters();

      for (ProcessorStrip processorStrip : processorStrips)
      {
        if (processorStrip.getRegion().contains(alignmentRegionPanelRectangle))
        {
          alignmentRegion.setReconstructionEngineId(processorStrip.getReconstructionEngineId());
          assignedProcessorStrip = true;
          break;
        }
      }

      // AlignmentRegions are not guaranteed to be in the imageable area of the
      // board.   Thus, we will dedicate scanPasses to cover the region.
      if (assignedProcessorStrip == false)
      {
        _alignmentRegionInfoMap.remove(alignmentRegion);
        customScanPaths.add(scanPathForAlignmentRegion);
        continue;
      }

      // Iterate through the basicScanPath and assign this ReconstructionRegion to be
      // reconstructed using them.
      alignmentRegion.clearScanPasses();
      int scanPassCount = -1;
      int startingScanPass = alignmentScanSection.getStartingScanPass();
      for (ScanPass scanPass : mainScanPath.getScanPasses())
      {
        scanPassCount++;
        if (scanPassCount >= startingScanPass)
        {
          if (scanPassCount <= lastScanPassForAlignmentSection)
          {
            scanPass.setCoversAlignmentRegions(true);
            if (orderedMainScanPath.contains(scanPass) == false)
              orderedMainScanPath.add(scanPass);
            scanPass.setProjectionType(ProjectionTypeEnum.INSPECTION_AND_ALIGNMENT);
            alignmentRegion.addScanPass(scanPass);
            
            // Adjust Scan pass start end to consider the alignment region when variable div n enable
            if(testSubProgram.getTestProgram().getProject().isVarDivNEnable())
            {
              Collection<ReconstructionRegion> regions = new ArrayList<ReconstructionRegion>();
              regions.add(alignmentRegion);
              HomogeneousImageGroup group = putRegionsIntoExistingScanPass(testSubProgram, scanPass, regions);
                            
              testSubProgram.removeScanPassToImageGroup(scanPass);
              testSubProgram.addScanPassToImageGroup(scanPass,group);
            }
          }
          else
            break;
        }
      }
    }
    
    // Iterate through the mainScanPath and append only those scanpasses that
    // aren't already included.
    for (ScanPass scanPass : mainScanPath.getScanPasses())
    {
      if (orderedMainScanPath.contains(scanPass) == false)
        orderedMainScanPath.add(scanPass);
    }
    mainScanPath.setScanPasses(orderedMainScanPath);
    
    // CR1022 fix by LeeHerng. We want to make sure we add custom scan paths here for
    // certain alignment regions which are not covered by orderedAlignmentRegions.
    // Normally we handle this case for No Load/No Test
    if (originalAlignmentRegions.size() > orderedAlignmentRegions.size())
    {
      customScanPaths.addAll(getCustomScanPaths(originalAlignmentRegions,orderedAlignmentRegions));
    }

    // Adding in Alignment scanPath to ImageReconstructionEnginesInUse for customeScanPaths
    for (ScanPath scanPath : customScanPaths)
    {
      updateImageReconstructionEnginesInUseBasedUponAdditionalScanPathInfo(scanPath);
    }

    return customScanPaths;
  }
  
  /**
   *
   * @param originalAlignmentRegions
   * @param orderedAlignmentRegions
   * @author Cheah Lee Herng
   */
  private List<ScanPath> getCustomScanPaths(Collection<ReconstructionRegion> originalAlignmentRegions,
                                            List<Pair<ImageGroup, AlignmentScanPathSubSection>> orderedAlignmentRegions)
  {
      Assert.expect(originalAlignmentRegions != null);
      Assert.expect(orderedAlignmentRegions != null);

      List<ScanPath> customScanPaths = new ArrayList<ScanPath>();
      List<ReconstructionRegion> orderedReconstructionRegions = new ArrayList<ReconstructionRegion>();

      for (Pair<ImageGroup,AlignmentScanPathSubSection> alignmentRegionInfo : orderedAlignmentRegions)
      {
          AlignmentScanPathSubSection alignmentScanPathSubSection = alignmentRegionInfo.getSecond();
          ReconstructionRegion alignmentRegion = alignmentScanPathSubSection.getAlignmentRegion();
          orderedReconstructionRegions.add(alignmentRegion);
      }

      for (ReconstructionRegion currentAlignmentRegion : originalAlignmentRegions)
      {
          if (orderedReconstructionRegions.contains(currentAlignmentRegion) == false)
          {
              if (Config.isDeveloperDebugModeOn())
                System.out.println("Adding custom scan path for alignment region " + currentAlignmentRegion.getRegionId());

              Pair<ImageGroup,AlignmentScanPathSubSection> infoPair = _alignmentRegionInfoMap.get(currentAlignmentRegion);
              ImageGroup unSelectedImageGroup = infoPair.getFirst();
              ScanPath unSelectedScanPath = unSelectedImageGroup.getScanPath();

              if (customScanPaths.contains(unSelectedScanPath) == false)
                customScanPaths.add(unSelectedScanPath);
          }
      }
      return customScanPaths;
  }

  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  private ImageGroup generateFixXScanPathForAlignmentRegion(
      ReconstructionRegion alignmentRegion,
      AffineTransform aggregateAlignmentTransform,
      TestSubProgram testSubProgram) throws XrayTesterException
  {
    List<ReconstructionRegion> regions = new ArrayList<ReconstructionRegion>(1);
    regions.add(alignmentRegion);
    HomogeneousImageGroup imageGroup = new HomogeneousImageGroup(regions, false);
    
    PanelRectangle panelRectangle = alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    SystemFiducialRectangle nominalAreaToImage = MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(
      panelRectangle,
      aggregateAlignmentTransform);
    
    List<ProcessorStrip> processorStrips = createProcessorStripsForIrps(panelRectangle);
    
    double appropriateMaxSliceHeight = XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(alignmentRegion.getTestSubProgram().getTestProgram().getProject().getPanel().getThicknessInNanometers(), alignmentRegion.getTestSubProgram().getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers());

    MechanicalConversions mechanicalConversions = new MechanicalConversions(
        processorStrips,
        aggregateAlignmentTransform,
        Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
        ProcessorStrip.getOverlapNeededForLargestPossibleJointInNanometers(),
        appropriateMaxSliceHeight,
        Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers(testSubProgram.getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers())),
        _xRayCameras,
        nominalAreaToImage);

    List<ScanPass> scanPasses = generateFixXScanPathOverArea(alignmentRegion.getSignalCompensation(), imageGroup, mechanicalConversions, testSubProgram);
    
    alignmentRegion.clearScanPasses();
    for (ScanPass scanPass : scanPasses)
    {
      alignmentRegion.addScanPass(scanPass);
      scanPass.setProjectionType(ProjectionTypeEnum.ALIGNMENT);
    }
    
    ScanPath scanPath = new ScanPath(mechanicalConversions, scanPasses);
    
    imageGroup.setScanPath(scanPath);
    return imageGroup;
  }
  
  /**
   * @author Roy Williams
   */
  private ImageGroup generateScanPathForAlignmentRegion(
      ReconstructionRegion alignmentRegion,
      AffineTransform aggregateAlignmentTransform) throws XrayTesterException
  {
    List<ReconstructionRegion> regions = new ArrayList<ReconstructionRegion>(1);
    regions.add(alignmentRegion);
    HomogeneousImageGroup imageGroup = new HomogeneousImageGroup(regions);
    ScanPath scanPath = generateScanPathForAlignmentRegion(alignmentRegion,
                                                           aggregateAlignmentTransform,
                                                           imageGroup);
    imageGroup.setScanPath(scanPath);
    return imageGroup;
  }


  /**
   * @author Roy Williams
   */
  private ScanPath generateScanPathForAlignmentRegion(
      ReconstructionRegion alignmentRegion,
      AffineTransform aggregateAlignmentTransform,
      HomogeneousImageGroup imageGroup) throws XrayTesterException
  {
    Assert.expect(aggregateAlignmentTransform != null);
    Assert.expect(alignmentRegion != null);

    PanelRectangle panelRectangle = alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    SystemFiducialRectangle nominalAreaToImage = MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(
      panelRectangle,
      aggregateAlignmentTransform);

    List<ProcessorStrip> processorStrips = createProcessorStripsForIrps(panelRectangle);

    double appropriateMaxSliceHeight = XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(alignmentRegion.getTestSubProgram().getTestProgram().getProject().getPanel().getThicknessInNanometers(), alignmentRegion.getTestSubProgram().getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers());

    MechanicalConversions mechanicalConversions = new MechanicalConversions(
        processorStrips,
        aggregateAlignmentTransform,
        Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
        ProcessorStrip.getOverlapNeededForLargestPossibleJointInNanometers(),
        appropriateMaxSliceHeight,
        Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers(alignmentRegion.getTestSubProgram().getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers())),
        _xRayCameras,
        nominalAreaToImage);

    int stepSize = imageGroup.getMeanStepSizeInNanoMeters();
    int dither = imageGroup.getDitherAmplitudeInNanoMeters();
    List<ScanPass> scanPasses = generateScanPathOverArea(mechanicalConversions,
                                                         stepSize,
                                                         dither);
    alignmentRegion.clearScanPasses();
    for (ScanPass scanPass : scanPasses)
    {
      alignmentRegion.addScanPass(scanPass);
      scanPass.setProjectionType(ProjectionTypeEnum.ALIGNMENT);
    }

    return new ScanPath(mechanicalConversions, scanPasses);
  }

  /**
   * Creates the set of information required to instruct the cameras on how
   * to capture and route projection data.  This includes creating ProjectionRegions
   * and projection settings for each camera.
   *
   * @return the number of lines to capture
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  private int appendXrayCameraAcquisitionParametersForVariableDivN(
      int numberOfParticipatingIRPs,
      ScanPath scanPath,
      Map<AbstractXrayCamera, XrayCameraAcquisitionParameters> xRayCameraAcquisitionParametersMap)
  {
    Assert.expect(scanPath != null);
    Assert.expect(xRayCameraAcquisitionParametersMap != null);

    List<ScanPass> scanPasses = scanPath.getScanPasses();

    // For every camera, create a ProjectionSettings for each scan pass
    int maxNumberOfLinesToCapture = 0;
    for(AbstractXrayCamera camera : _xRayCameras)
    {
      // if we are starting to set up the acquisition parameters for the first time, then create a new list of projection
      // settings for each camera.  Otherwise, we will append on to the pre-existing projection settings list that was set up
      // for each XrayCameraAcquisitionParameters object.
      XrayCameraAcquisitionParameters parametersForCurrentCamera = xRayCameraAcquisitionParametersMap.get(camera);
      if (parametersForCurrentCamera == null)
      {
        parametersForCurrentCamera = new XrayCameraAcquisitionParameters(camera);
      }

      for(ScanPass scanPass : scanPasses)
      {
        // create the projection regions, lines to capture and start capture lines
        // for each cameras on a given scan pass.
        MechanicalConversions mechanicalConversions = scanPass.getMechanicalConversions();
        
        List<ProjectionRegion> projectionRegions = getProjectionRegions(numberOfParticipatingIRPs,
                                                                        mechanicalConversions, camera);
        ImageRectangle projectionRegionsRectangle = calculateAreaOfProjectionRegionsToCapture(projectionRegions);
        int numberOfLinesToCapture = projectionRegionsRectangle.getHeight();
        int startCaptureLine = 0;

        // Create a new ProjectionSettings for this projection.
        ProjectionSettings projectionSettingsForCurrentScanPass = new ProjectionSettings(scanPass,
                                                                                         startCaptureLine,
                                                                                         numberOfLinesToCapture,
                                                                                         projectionRegions);

        parametersForCurrentCamera.addProjectionSettings(projectionSettingsForCurrentScanPass);

        // Identify the largest numberOfLinesToCapture.
        maxNumberOfLinesToCapture = Math.max(maxNumberOfLinesToCapture,
                                             calculateMaxHeightOfProjectionRegionsToCapture(projectionRegions));
      }

      xRayCameraAcquisitionParametersMap.put(camera, parametersForCurrentCamera);
    }

    return maxNumberOfLinesToCapture;
  }
  
  /**
   * Creates the set of information required to instruct the cameras on how
   * to capture and route projection data.  This includes creating ProjectionRegions
   * and projection settings for each camera.
   *
   * @return the number of lines to capture
   * @author Roy Williams
   * @author Greg Esparza
   */
  private int appendXrayCameraAcquisitionParameters(
      int numberOfParticipatingIRPs,
      ScanPath scanPath,
      Map<AbstractXrayCamera, XrayCameraAcquisitionParameters> xRayCameraAcquisitionParametersMap)
  {
    Assert.expect(scanPath != null);
    Assert.expect(xRayCameraAcquisitionParametersMap != null);

    MechanicalConversions mechanicalConversions = scanPath.getMechanicalConversions();
    List<ScanPass> scanPasses = scanPath.getScanPasses();

    // For every camera, create a ProjectionSettings for each scan pass
    int maxNumberOfLinesToCapture = 0;
    for(AbstractXrayCamera camera : _xRayCameras)
    {
      // if we are starting to set up the acquisition parameters for the first time, then create a new list of projection
      // settings for each camera.  Otherwise, we will append on to the pre-existing projection settings list that was set up
      // for each XrayCameraAcquisitionParameters object.
      XrayCameraAcquisitionParameters parametersForCurrentCamera = xRayCameraAcquisitionParametersMap.get(camera);
      if (parametersForCurrentCamera == null)
        parametersForCurrentCamera = new XrayCameraAcquisitionParameters(camera);

      for(ScanPass scanPass : scanPasses)
      {
        // create the projection regions, lines to capture and start capture lines
        // for each cameras on a given scan pass.
        List<ProjectionRegion> projectionRegions = getProjectionRegions(numberOfParticipatingIRPs,
                                                                        mechanicalConversions, camera);
        ImageRectangle projectionRegionsRectangle = calculateAreaOfProjectionRegionsToCapture(projectionRegions);
        int numberOfLinesToCapture = projectionRegionsRectangle.getHeight();
        int startCaptureLine = 0;

        // Create a new ProjectionSettings for this projection.
        ProjectionSettings projectionSettingsForCurrentScanPass = new ProjectionSettings(scanPass,
                                                                                         startCaptureLine,
                                                                                         numberOfLinesToCapture,
                                                                                         projectionRegions);

        parametersForCurrentCamera.addProjectionSettings(projectionSettingsForCurrentScanPass);

        // Identify the largest numberOfLinesToCapture.
        maxNumberOfLinesToCapture = Math.max(maxNumberOfLinesToCapture,
                                             calculateMaxHeightOfProjectionRegionsToCapture(projectionRegions));
      }

      xRayCameraAcquisitionParametersMap.put(camera, parametersForCurrentCamera);
    }

    return maxNumberOfLinesToCapture;
  }

  /**
   * Return the projection regions imaged
   * @author Rex Shang
   * @author Roy Williams
   */
  private List<ProjectionRegion> getProjectionRegions(
      int numberOfParticipatingIRPs,
      MechanicalConversions mechanicalConversions,
      AbstractXrayCamera xRayCamera)
  {
    Assert.expect(mechanicalConversions != null);
    Assert.expect(xRayCamera != null);

    List<ProjectionRegion> projectionRegions = new ArrayList<ProjectionRegion>();
    TreeMap<ImageReconstructionEngineEnum, ProcessorStrip> ireIdToProcessorStripMap = new TreeMap<ImageReconstructionEngineEnum,ProcessorStrip>();
    Map<ProcessorStrip, ImageRectangle> processorStripToProjectionRectangleMap = mechanicalConversions.getProcessorStripToProjectionRectangleMap(xRayCamera);

    // Build a map so we can go from ire to processor strip to image rectangle.
    // Down the road, it would be nice to have the mechanical convesion just get
    // us a map of IRE to image rectangle.
    for (ProcessorStrip processorStrip : mechanicalConversions.getProcessorStrips())
    {
      ireIdToProcessorStripMap.put(processorStrip.getReconstructionEngineId(), processorStrip);
    }
    Assert.expect(ireIdToProcessorStripMap.isEmpty() == false);

    int irpCount = 0;
    for (ImageReconstructionEngine ire : _imageReconstructionEnginesInUse)
    {
      ProcessorStrip processorStrip = ireIdToProcessorStripMap.get(ire.getId());

      if (irpCount >= numberOfParticipatingIRPs || processorStrip == null)
      {
        // In case of alignment region, we only have 1 processor strip...
        // So when no strip is assigned to the current IRE, let's feed it the
        // one used by the first IRE to keep it busy.
        processorStrip = ireIdToProcessorStripMap.get(ireIdToProcessorStripMap.firstKey());
      }
      Assert.expect(processorStrip != null);

      ImageRectangle projectionRectangle = processorStripToProjectionRectangleMap.get(processorStrip);

      projectionRegions.addAll(splitImageRectangleIntoTwoProjectionRegions(projectionRectangle, ire));
      irpCount++;
    }

    Assert.expect(projectionRegions.size() > 0, "projectionRegions should be at least 1.");
    return projectionRegions;
  }

  /**
   * @author Roy Williams
   */
  private List<Pair<ImageGroup, AlignmentScanPathSubSection>> identifyAlignmentScanPassSectionsInBasicScanPath(
      Collection<ReconstructionRegion> alignmentRegions,
      ScanPath mainScanPath)
  {
    Assert.expect(alignmentRegions != null);
    Assert.expect(mainScanPath != null);

    ReconstructionRegionsInScanPass regionsInScopeForScanPass = new ReconstructionRegionsInScanPass();
    List<ScanPass> inspectionScanPath = mainScanPath.getScanPasses();
    for (ScanPass scanPass : inspectionScanPath)
      scanPass.setCoversAlignmentRegions(false);

    List<Pair<ImageGroup, AlignmentScanPathSubSection>> sortedListOfPairs =
        new ArrayList<Pair<ImageGroup, AlignmentScanPathSubSection>>();
    List<Pair<ImageGroup, AlignmentScanPathSubSection>> finalListOfPairs = new ArrayList<Pair<ImageGroup, AlignmentScanPathSubSection>>();
    for (ReconstructionRegion region : alignmentRegions)
    {
      //ReconstructionRegionsInScanPass regionsInScopeForScanPass = new ReconstructionRegionsInScanPass();
      Pair<ImageGroup, AlignmentScanPathSubSection> alignmentRegionInfo =
          _alignmentRegionInfoMap.get(region);
      ImageGroup imageGroup = alignmentRegionInfo.getFirst();
      AlignmentScanPathSubSection alignmentScanPathSubSection = alignmentRegionInfo.getSecond();

      List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionPairs =
          imageGroup.getRegionPairsSortedInExecutionOrder();
      Assert.expect(regionPairs.size() == 1);

      for (ScanPass scanPass : inspectionScanPath)
      {
        int scanPassId = scanPass.getId();
        regionsInScopeForScanPass =
            identifyReconstructionRegionsImagedByProposedScanPass(scanPass,
                                                                  regionPairs,
                                                                  regionsInScopeForScanPass);
        int numberOfReconstructionRegionsUsingProposedScanPass =
            regionsInScopeForScanPass.getNumberOfReconstructionRegionsUsingProposedScanPass();
        if (numberOfReconstructionRegionsUsingProposedScanPass > 0)
        {
          scanPass.setCoversAlignmentRegions(true);
          if (alignmentScanPathSubSection == null)
          {
            // Insert into the front of the list insures we see the alignment points
            // based upon the last one at the beginning of the list.
            alignmentScanPathSubSection = new AlignmentScanPathSubSection(region);
            alignmentScanPathSubSection.setStartingScanPass(scanPassId);
            alignmentRegionInfo.setSecond(alignmentScanPathSubSection);
          }
          else
            alignmentScanPathSubSection.setLastScanPass(scanPassId);
          
          // scan pass with code = 'Z' only when it's from old scan path
          boolean newScanRoute = region.getTestSubProgram().getTestProgram().getProject().isGenerateByNewScanRoute();
          if(newScanRoute && scanPass.getCode().equalsIgnoreCase("Z")==false) 
            alignmentScanPathSubSection.addScanPassCode(scanPass.getCode());
        }
        else
        {
          if (alignmentScanPathSubSection != null &&
              alignmentScanPathSubSection.lastScanPassHasBeenSet())
            break;
        }
      }

      // Now validate that our subsection has enough scanPasses to image the entire
      // region.  There can be an incomplete set due to noload.
      List<ScanPass> selectedScanPasses = new ArrayList<ScanPass>();
      if (alignmentScanPathSubSection == null)
      {
        alignmentScanPathSubSection = new AlignmentScanPathSubSection(region);
        alignmentScanPathSubSection.forceCustomPathGeneration(true);
        alignmentRegionInfo.setSecond(alignmentScanPathSubSection);
        continue;
      }
      int lastScanPass = alignmentScanPathSubSection.getLastScanPass();
      for (int i=alignmentScanPathSubSection.getStartingScanPass(); i<=lastScanPass; i++)
        selectedScanPasses.add(inspectionScanPath.get(i));
      if (selectedScanPasses.size() > 0)
      {
        ScanPath selectedScanPath = new ScanPath(mainScanPath.getMechanicalConversions(), selectedScanPasses);
        Area selectedScanPathArea = getImagableAreaOfScanPathInNanoMeters(selectedScanPath);
        Area alignmentRegionArea  = getImagableAreaOfScanPathInNanoMeters(imageGroup.getScanPath());
        if (selectedScanPathArea.getBounds2D().contains(alignmentRegionArea.getBounds2D()) == false)
        {
          alignmentScanPathSubSection.forceCustomPathGeneration(true);
          alignmentRegionInfo.setSecond(alignmentScanPathSubSection);
        }
      }

      sortedListOfPairs.add(alignmentRegionInfo);
    }
    finalListOfPairs.addAll(alignmentRegionsBasedUponScanOrder(sortedListOfPairs));
    return finalListOfPairs;
  }
  
  /**
   * @author Chnee Khang Wah, 2013-11-06
   */
  private int getMaxNumberOfScanPassesWithinRanges(ScanPath scanPath, double range)
  {
    Assert.expect(range>0);
    int max = 0;
    List<ScanPass> scanPasses = new ArrayList<ScanPass>(scanPath.getScanPasses());
    
    for (ScanPass scanPass : scanPath.getScanPasses())
    {
      int staX = scanPass.getStartPointInNanoMeters().getXInNanometers();
      int endX = staX + (int)range;
      int count = 0;
      
      for (ScanPass sp : scanPasses)
      {
        int curX = sp.getStartPointInNanoMeters().getXInNanometers();
        if(curX>=staX && curX<=endX)
        {
          count++;
          continue;
        }
        break;
      }
      
      if (scanPasses.size()>0)
        scanPasses.remove(0);
      
      if(count>max)
         max = count;
    }
    
    return max;
  }
 
  /**
   * 
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  private long calculateMemoryUsedforEachIRP(ScanPath scanpath, int numberOfScanPass)
  {
    Assert.expect(scanpath != null);
    
    // First generate the camera acquisiton parameters so we can use the ProjectionSettings
    // to determine the maximum amount of memory consumed.   The amount of memory consumed will
    // regulate the number of scanPasses allowed before we must scan the alignment
    // regions
    Map<AbstractXrayCamera, XrayCameraAcquisitionParameters> xrayCameraAcquisitionParameters = 
            new HashMap<AbstractXrayCamera,XrayCameraAcquisitionParameters>();
    
    MechanicalConversions mechanicalConversions = scanpath.getMechanicalConversions();
    int numberParticipants = mechanicalConversions.getNumberProcessorStrips();
    int maxNumberOfLinesCapturedPerScanPass = appendXrayCameraAcquisitionParameters(
                                                        numberParticipants,
                                                        scanpath,
                                                        xrayCameraAcquisitionParameters);
//    long irpMemoryAllocatedForProjectionMemory = ImageReconstructionEngine.getMemoryAllocatedForProjectionMemory();

    // We can just get any camera so we can get the sensor attributes
    // Walk through the list of alignment scan paths.  And, as long as the full alignment
    // scan pass will fit into the projection memory, use it.
    AbstractXrayCamera xRayCamera = _xrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
    // Swee Yee Wong - XCR-2630 Support New X-Ray Camera
	long irpMemoryUsedPerScanPass = maxNumberOfLinesCapturedPerScanPass *
                                    xRayCamera.getSensorWidthInPixelsWithOverHeadByte() *
                                    XrayCameraArray.getNumberOfCameras();
    
//    long maxNumberScanPassesBeforeMemoryConsumed = irpMemoryAllocatedForProjectionMemory /
//                                                   irpMemoryUsedPerScanPass;
   
    long memoryUsed = irpMemoryUsedPerScanPass*numberOfScanPass;
    
    return memoryUsed;
  }

  /**
   * Returns the area to capture for a given set of projection regions.
   *
   * @author Roy Williams
   */
  private static ImageRectangle calculateAreaOfProjectionRegionsToCapture(List<ProjectionRegion> projectionRegions)
  {
    Assert.expect(projectionRegions != null);
    Assert.expect(projectionRegions.size() > 0, "projectionRegions size should be at least 1.");

    ImageRectangle combinedProjectionRegions = null;
    for(ProjectionRegion projectionRegion : projectionRegions)
    {
      if (combinedProjectionRegions == null)
        combinedProjectionRegions = new ImageRectangle(
            projectionRegion.getProjectionRegionRectangle());
      else
        combinedProjectionRegions.add(projectionRegion.getProjectionRegionRectangle());
    }

    Assert.expect(combinedProjectionRegions != null, "combinedProjectionRegions is null.");
    return combinedProjectionRegions;
  }

  /**
   * Returns the area to capture for a given set of projection regions.
   *
   * @author Roy Williams
   */
  private static int calculateMaxHeightOfProjectionRegionsToCapture(List<ProjectionRegion> projectionRegions)
  {
    Assert.expect(projectionRegions != null);
    Assert.expect(projectionRegions.size() > 0, "projectionRegions size should be at least 1.");

    int maxHeight = 0;
    for(ProjectionRegion projectionRegion : projectionRegions)
      maxHeight = Math.max(maxHeight, projectionRegion.getProjectionRegionRectangle().getHeight());
    return maxHeight;
  }

  /**
   * Given an image rectangle for a processor strip create the
   * ProjectionRegion(s).
   *
   * @author Roy Williams
   */
  private static List<ProjectionRegion> splitImageRectangleIntoTwoProjectionRegions(
      ImageRectangle projectionRect,
      ImageReconstructionEngine ire)
  {
    Assert.expect(projectionRect != null);
    Assert.expect(ire != null);

    List<InetAddress> ipAddressesForIre = ire.getIpAddresses();
    int numberIpAddresses = ipAddressesForIre.size();
    Assert.expect(ipAddressesForIre != null);
    Assert.expect(numberIpAddresses > 0);

    List<ProjectionRegion> projectionRegions = new ArrayList<ProjectionRegion>(numberIpAddresses);

    // To utilize bandwith in an optimal way each projection region is divided
    // into subregions which are sent to the IRP over separate ports
    int height = projectionRect.getHeight() / ipAddressesForIre.size();
    int extraHeight = 0;
    for(int subRegion = 0; subRegion < numberIpAddresses; ++subRegion)
    {
      InetAddress projectionDestinationAddress = ipAddressesForIre.get(subRegion);

      // If this is the last port then add any remainder of the orginal projection region
      // to this sub region
      if ((subRegion + 1) == numberIpAddresses)
      {
        extraHeight = projectionRect.getHeight() % numberIpAddresses;
      }
      else
      {
        extraHeight = 0;
      }

      // Create the sub region
      ImageRectangle region = new ImageRectangle(projectionRect.getMinX(),
                                                 projectionRect.getMinY() + (height * subRegion),
                                                 projectionRect.getWidth(),
                                                 height + extraHeight);

      // Create the projection regions and add it to the list of projection regions for this scan pass.
      // Single IRP - Added ire in order to get the current start port number
      ProjectionRegion projectionRegion = new ProjectionRegion(region,
          ire.getId(),
          projectionDestinationAddress,
          ire.getStartPortNumber());

      projectionRegions.add(projectionRegion);
    }

    return projectionRegions;
  }

  /**
   * @return returns the list of IREs based on number of ire requested.
   * @author Rex Shang
   */
  static final private List<ImageReconstructionEngine> createReconstructionEnginesToUse(int numberOfIreNeeded)
  {
    Assert.expect(numberOfIreNeeded > 0, "Using 0 IRP.");
    Assert.expect(numberOfIreNeeded <= _imageReconstructionEngines.size(), "Using too many IRPs than what we have. " + numberOfIreNeeded + " requested.");

    List<ImageReconstructionEngine> ires =
      new ArrayList<ImageReconstructionEngine>(_imageReconstructionEngines.subList(0, numberOfIreNeeded));
    return ires;
  }

  /**
   * @author Rex Shang
   * @author Roy Williams
   */
  static final private List<ImageReconstructionEngine> getReconstructionEnginesToUseBasedOnPanelWidth(int areaToImageWidth)
  {
    //Siew Yeng - XCR-2218 - Increase inspectable pad size
    double maxInspectionRegionSize;
    if(XrayTester.isLowMagnification())
      maxInspectionRegionSize = Math.max(ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters(), ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters());
    else
      maxInspectionRegionSize = Math.max(ReconstructionRegion.getMaxHighMagInspectionRegionLengthInNanoMeters(), ReconstructionRegion.getMaxHighMagInspectionRegionWidthInNanoMeters());
    
    int numberOfIreNeeded = areaToImageWidth /
                            _xrayCameraArray.getInchesInNanometersAlignedUpToPixelBoundary(Math.max(maxInspectionRegionSize, 25400000.0));
    // Have to use at least 1 ire.
    if (numberOfIreNeeded == 0)
      numberOfIreNeeded = 1;

    // Do not use more ires than what we have.
    numberOfIreNeeded = Math.min(numberOfIreNeeded, _imageReconstructionEngines.size());
    return createReconstructionEnginesToUse(numberOfIreNeeded);
  }

  /**
   * This method could retrieve a value stored in the software.config.
   *
   * @author Roy Williams
   */
  public static List<ScanStrategyEnum> getDefaultScanStrategy()
  {
    // Now let's do those that do need for shading compensated regions.
    List<ScanStrategyEnum> scanStrategy = new ArrayList<ScanStrategyEnum>();
//    scanStrategy = ScanStrategyEnum.GROUP_ON_MAX_CANDIDATE_STEP_SIZE;
//    scanStrategy = ScanStrategyEnum.GROUP_ON_COMPONENTS_WITH_AREA_COMPRESSION;

    // Good pair to have enabled.  1st one alone or both.
    scanStrategy.add(ScanStrategyEnum.GROUP_ON_COMPONENTS_WITH_AREA_AND_TIMING_COMPRESSION);

    // Scan pass integration may be turned off in software.config (for internal use only)
    if (isScanPassIntegrationEnabled())
      scanStrategy.add(ScanStrategyEnum.MERGE_TO_MAIN);

    // This one will turn off shading.
//    scanStrategy.clear();
//    scanStrategy.add(ScanStrategyEnum.NO_SHADING_COMPENSATION);

    return scanStrategy;
  }

  /**
   * @author Roy Williams
   * @author Rex Shang
   */
  private static List<ProcessorStrip> getProcessorStripsFromTestSubProgam(
      TestSubProgram testSubProgram,
      ImageAcquisitionModeEnum acquisitionMode) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(acquisitionMode != null);

    List<ProcessorStrip> processorStrips = null;
    if (acquisitionMode.equals(ImageAcquisitionModeEnum.VERIFICATION) ||
        acquisitionMode.equals(ImageAcquisitionModeEnum.COUPON))
    {
      processorStrips = testSubProgram.getProcessorStripsForVerificationRegions();
    }
    else if (acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION) ||
             acquisitionMode.equals(ImageAcquisitionModeEnum.IMAGE_SET))
    {
      processorStrips = testSubProgram.getProcessorStripsForInspectionRegions();
    }
    else if (acquisitionMode.equals(ImageAcquisitionModeEnum.ALIGNMENT))
    {
      processorStrips = testSubProgram.getFakeProcessorStrips();
    }

    Assert.expect(processorStrips != null, "Invalid acquisitionMode.");
    return processorStrips;
  }

  /**
   * @author Roy Williams
   * @author Rex Shang
   */
  public static void defineProcessorStripsForVerificationRegions(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    for(TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
    {
      List<ProcessorStrip> processorStrips = defineProcessorStrips(testSubProgram, ImageAcquisitionModeEnum.VERIFICATION);
      testSubProgram.setProcessorStripsForVerificationRegions(processorStrips);
    }
  }

  /**
   * @author Roy Williams
   * @author Rex Shang
   */
  public static void defineProcessorStripsForInspectionRegions(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    for(TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
    {
      List<ProcessorStrip> processorStrips = defineProcessorStrips(testSubProgram, ImageAcquisitionModeEnum.PRODUCTION);
      if (_debug)
      {
        for (ProcessorStrip processorStrip : processorStrips)
        {
          printRectangle("ProcessorStrip " + processorStrip.getLocation(), processorStrip.getRegion());
          printRectangle("StripNominal " + processorStrip.getLocation(), processorStrip.getNominalRegion());
        }
      }
      testSubProgram.setProcessorStripsForInspectionRegions(processorStrips);
    }
  }

  /**
   * This would be modified in a future release to be for an individual TestSubProgram
   * when we want to produce separate scanPaths for L shaped boards.
   *
   * @param testSubProgram will be used to produce a union of ReconstructionRegions
   * @author Roy Williams
   */
  private static List<ProcessorStrip> defineProcessorStrips(
      TestSubProgram testSubProgram,
      ImageAcquisitionModeEnum acquisitionMode)
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(acquisitionMode != null);

    // Get the area of the panel we need to image.  This is a function
    // of our acquisition mode - verification or production.
    PanelRectangle panelRectangleToImage = null;
    if (acquisitionMode.equals(ImageAcquisitionModeEnum.VERIFICATION) ||
        acquisitionMode.equals(ImageAcquisitionModeEnum.COUPON))
    {
      panelRectangleToImage = testSubProgram.getVerificationRegionsBoundsInNanoMeters();
    }
    else if (acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION) ||
             acquisitionMode.equals(ImageAcquisitionModeEnum.IMAGE_SET))
    {
      panelRectangleToImage = testSubProgram.getInspectionRegionsBoundsInNanoMeters();
    }
    else if (acquisitionMode.equals(ImageAcquisitionModeEnum.ALIGNMENT))
    {
      // Shouldn't get here.  Only Verification & Production modes allowed.
      Assert.expect(false);
    }

    // Select the number of IPRs to use.
    int panelWidth = panelRectangleToImage.getWidth();
    return createProcessorStripsForIrps(panelRectangleToImage);
  }

  /**
   * Creates a list of ProcessorStrips where x is a function of panel width and
   * y is a function of the scan length for the union of all Inspection and
   * Alignment ReconstructionRegions for an individual TestProgram.
   *
   * @author Roy Williams
   */
  public static List<ProcessorStrip> createProcessorStripsForIrps(PanelRectangle panelRectangleToImage)
  {
    Assert.expect(panelRectangleToImage != null);

    int panelRectangleWidth = panelRectangleToImage.getWidth();
    List<ImageReconstructionEngine> iresToUse = getReconstructionEnginesToUseBasedOnPanelWidth(panelRectangleWidth);

    final int numberOfReconstructionEnginesToUse = iresToUse.size();
    List<ProcessorStrip> processorStrips = new ArrayList<ProcessorStrip>(numberOfReconstructionEnginesToUse);

    // Simply divide the panel width by the number of IRP's we're using
    int nominalProcessorStripWidthInNanometers =
        (int)Math.ceil((double)panelRectangleWidth / (double)numberOfReconstructionEnginesToUse);

    // Build the processor strips
    for(int stripNumber = 0; stripNumber < numberOfReconstructionEnginesToUse; ++stripNumber)
    {
      int start = stripNumber * nominalProcessorStripWidthInNanometers;

      // Rounding error on last ProcessorStrip.
      int width = Math.min(nominalProcessorStripWidthInNanometers, panelRectangleWidth - start);

      // Use the nominal for all except the last ProcessorStrip.   It will adjust
      // the nominal to account for the remainder.
      PanelRectangle nominalRegion =
          new PanelRectangle(panelRectangleToImage.getMinX() + start,
                             panelRectangleToImage.getMinY(),
                             width,
                             panelRectangleToImage.getHeight());

      // Create a processor strip
      ProcessorStrip processorStrip = new ProcessorStrip(stripNumber,
                                                         nominalRegion,
                                                         numberOfReconstructionEnginesToUse);
      processorStrip.setNominalStartInNanometers(start);
      processorStrip.setNominalStopInNanometers (start + width);
      processorStrip.setReconstructionEngineId(iresToUse.get(stripNumber).getId());
      processorStrips.add(processorStrip);

//      PanelRectangle processorStripRectangle = processorStrip.getRegion();
//      int leadAreaStart         = processorStripRectangle.getMinX();
//      int leadAreaEnd           = leadAreaStart + processorStrip.getLeadingEdgeWidth();
//      int nominalAreaStart      = nominalRegion.getMinX();
//      int nominalAreaEnd        = nominalRegion.getMaxX();
//      int tailAreaEnd           = processorStripRectangle.getMaxX();
//      int tailAreaStart         = tailAreaEnd - processorStrip.getTrailingEdgeWidth();
//      int i = 1;
    }

    return processorStrips;
  }

//  /**
//   * @author Roy Williams
//   */
//  private int getProcessorStripStart(List<ProcessorStrip> originalProcessorStrips, int location)
//  {
//    int nominalProcessorStripWidthInNanometers =
//        (int)Math.ceil((double)panelRectangleWidth / (double)numberOfReconstructionEnginesToUse);
//    return stripNumber * nominalProcessorStripWidthInNanometers;
//  }
//
  /**
   * Reuse the processorStrips created for the TestSubProgram.   Since all
   * ReconstructionRegions and their neighbors were created to fit, we do not have
   * to fret over moving AutoFocus information from one IRP to another.
   *
   * @author Roy Williams
   */
  private static List<ProcessorStrip> createProcessorStripsForImageGroupReusingAreasDefinedInOriginalProcessorStrips(
      List<ProcessorStrip> originalProcessorStrips,
      ImageGroup imageGroup)
  {
    Assert.expect(originalProcessorStrips != null);
    Assert.expect(imageGroup != null);

    // Intermediate results.
    int numberOfReconstructionEnginesToUse = imageGroup.getNumberReconstructionEngines();
    List<PanelRectangle> processorStripRectangles = new ArrayList<PanelRectangle>(numberOfReconstructionEnginesToUse);
    List<Integer> leadWidth = new ArrayList<Integer>(numberOfReconstructionEnginesToUse);
    List<Integer> tailWidth = new ArrayList<Integer>(numberOfReconstructionEnginesToUse);

    if (_debug)
    {
      System.out.println("start accumWidth leadWidth nomX nomY nomWidth nomHeight tailWidth");
    }

    int start = 0;
    int stripNumber = 0;
    int processorStripsUsedCount = 0;
    int reconstructionEnginesWithRegions = imageGroup.getNumberReconstructionEngines();
    PanelRectangle panelRectangleToImage = imageGroup.getPanelRectangleToImage();
    List<ProcessorStrip> originalProcessorStripsUsed = new ArrayList<ProcessorStrip>();
    List<ProcessorStrip> unUsedOriginalProcessorStrips = new ArrayList<ProcessorStrip>();

    for (ProcessorStrip origProcessorStrip : originalProcessorStrips)
    {
      boolean usesReconstructionEngine = imageGroup.usesReconstructionEngine(origProcessorStrip.getReconstructionEngineId());
      if (usesReconstructionEngine == false)
      {
        if (stripNumber == 0)
          continue;
        else if (processorStripsUsedCount >= reconstructionEnginesWithRegions)
          break;
        else
        {
          originalProcessorStripsUsed.add(origProcessorStrip);
          unUsedOriginalProcessorStrips.add(origProcessorStrip);

          // Do not increment!  This IRP has no regions
          // but must be present in the contiguous series for reconstruction.
          // processorStripsUsedCount++;
        }
      }
      else
      {
        originalProcessorStripsUsed.add(origProcessorStrip);
        processorStripsUsedCount++;
      }
      PanelRectangle processorStripRectangle = origProcessorStrip.getRegion();
      Assert.expect(processorStripRectangle.intersects(panelRectangleToImage));
      PanelRectangle nominalRectangleFromOriginalProcessorStrip = origProcessorStrip.getNominalRegion();
      int widthOfNominalArea = nominalRectangleFromOriginalProcessorStrip.getWidth();

      // Although this starts as the larger area (processor strip area) it will
      // be resized to be used as the nominalArea for our new processor strip.
      PanelRectangle intersectedPanelRectangleWithProcessorStrip =
          processorStripRectangle.createIntersectingPanelRectangle(panelRectangleToImage);
      PanelRectangle nominalRectangle = intersectedPanelRectangleWithProcessorStrip;

      // Easily identify 3 bands/sections for original processor strip areas.
      //   |            |                           |            |
      //   |   lead     |        nominal area       |   tail     |
      //   |   area     |       of processor strip  |   area     |
      //   |            |                           |            |
      int leadAreaStart    = processorStripRectangle.getMinX();
      int leadAreaEnd      = leadAreaStart + origProcessorStrip.getLeadingEdgeWidth();
      int nominalAreaStart = nominalRectangleFromOriginalProcessorStrip.getMinX();
      int nominalAreaEnd   = nominalRectangleFromOriginalProcessorStrip.getMaxX();
      int tailAreaEnd      = processorStripRectangle.getMaxX();
      int tailAreaStart    = tailAreaEnd - origProcessorStrip.getTrailingEdgeWidth();

      // intersection start (left edge) and end (right edge)
      int intersectStart = intersectedPanelRectangleWithProcessorStrip.getMinX();
      int intersectEnd   = intersectedPanelRectangleWithProcessorStrip.getMaxX();

      // Does intersect start in lead section?
      if ((leadAreaEnd - leadAreaStart) > 0 && intersectStart < leadAreaEnd)
      {
        if ((leadAreaEnd - leadAreaStart) > 0 && intersectEnd <= leadAreaEnd)
        {
          // intersect starts and ends in lead area section
          widthOfNominalArea = 0;
          nominalRectangle.setRect(nominalAreaStart,
                                   nominalRectangle.getMinY(),
                                   widthOfNominalArea,
                                   nominalRectangle.getHeight());
          leadWidth.add(leadAreaEnd - intersectStart);
          tailWidth.add(0);
        }

        else if ((nominalAreaEnd - nominalAreaStart) > 0 && intersectEnd <= nominalAreaEnd)
        {
          // intersect starts in lead section, intersect ends in nom area
          widthOfNominalArea = intersectEnd - nominalAreaStart;
          nominalRectangle.setRect(nominalAreaStart,
                                   nominalRectangle.getMinY(),
                                   widthOfNominalArea,
                                   nominalRectangle.getHeight());
          leadWidth.add(leadAreaEnd - intersectStart);
          tailWidth.add(0);
        }

        else if ((tailAreaEnd - tailAreaStart) > 0 && intersectEnd <= tailAreaEnd)
        {
          // intersect starts in lead section, continues thru nom area, intersect ends in tail section
          widthOfNominalArea = nominalAreaEnd - nominalAreaStart;
          nominalRectangle.setRect(nominalAreaStart,
                                   nominalRectangle.getMinY(),
                                   widthOfNominalArea,
                                   nominalRectangle.getHeight());
          leadWidth.add(leadAreaEnd - intersectStart);
          tailWidth.add(intersectEnd - tailAreaStart);
        }

        // This cannot happen because we did get an intersection.
        else
          Assert.expect(false);
      }

      // Does intersect start in the nominal section.
      else if ((nominalAreaEnd - nominalAreaStart) > 0 && intersectStart < nominalAreaEnd)
      {
        if ((leadAreaEnd - leadAreaStart) > 0 && intersectEnd <= leadAreaEnd)
        {
          Assert.expect(false, "cannot end scan in lead area before it starts in nom");
        }

        else if ((nominalAreaEnd - nominalAreaStart) > 0 && intersectEnd <= nominalAreaEnd)
        {
          // no lead section, intersect starts in nom area, intersect ends in nom area, no tail section
          widthOfNominalArea = intersectEnd - intersectStart;
          nominalRectangle.setRect(intersectStart,
                                   nominalRectangle.getMinY(),
                                   widthOfNominalArea,
                                   nominalRectangle.getHeight());
          leadWidth.add(0);
          tailWidth.add(0);
        }

        else if ((tailAreaEnd - tailAreaStart) > 0 && intersectEnd <= tailAreaEnd)
        {
          // No lead section, intersect starts in nom area, intersect ends in tail section
          widthOfNominalArea = nominalAreaEnd - intersectStart;
          nominalRectangle.setRect(intersectStart,
                                   nominalRectangle.getMinY(),
                                   widthOfNominalArea,
                                   nominalRectangle.getHeight());
          leadWidth.add(0);
          tailWidth.add(intersectEnd - tailAreaStart);
        }

        // This cannot happen because we did get an intersection.
        else
          Assert.expect(false);
      }

      // Does intersect start in the trailing section.
      else if ((tailAreaEnd - tailAreaStart) > 0 && intersectStart < tailAreaEnd)
      {
        if ((leadAreaEnd - leadAreaStart) > 0 && intersectEnd <= leadAreaEnd)
        {
          Assert.expect(false, "cannot end scan in lead area before it starts in trailing");
        }

        else if ((nominalAreaEnd - nominalAreaStart) > 0 && intersectEnd <= nominalAreaEnd)
        {
          Assert.expect(false, "cannot end scan in nom area before it starts in trailing");
        }

        else if ((tailAreaEnd - tailAreaStart) > 0 && intersectEnd <= tailAreaEnd)
        {
          // No lead section, no nom area, intersect starts and ends in tail section
          widthOfNominalArea = 0;
          nominalRectangle.setRect(tailAreaStart,
                                   nominalRectangle.getMinY(),
                                   widthOfNominalArea,
                                   nominalRectangle.getHeight());
          leadWidth.add(0);
          tailWidth.add(intersectEnd - intersectStart);
        }

        // This cannot happen because we did get an intersection.
        else
          Assert.expect(false);
      }

      // This cannot happen because we did get an intersection.
      else
        Assert.expect(false);


      if (_debug)
      {
        System.out.println(start + " " +
                           leadWidth.get(stripNumber)   + " " +
                           nominalRectangle.getMinX()   + " " +
                           nominalRectangle.getMinY()   + " " +
                           nominalRectangle.getWidth()  + " " +
                           nominalRectangle.getHeight() + " " +
                           tailWidth.get(stripNumber));
//        System.out.println("start: " + start + "       width: " + widthOfNominalArea + "       accumulatedWidth: " + sumOfAllProcessorStripWidths);
      }
      start = start + widthOfNominalArea;

      processorStripRectangles.add(intersectedPanelRectangleWithProcessorStrip);
      stripNumber++;
    }
    if (_debug)
    {
      System.out.println();
    }

    // Create the new ProcessorStrips - always using the bottom set (lowest index)
    // to create these "temp" versions of the real ProcessorStrip.  HOWEVER, please
    // keep in mind that we REALLY AREN'T CHANGING THE NUMBERS ON THE ORIGINAL.
    // Thus, while we are calling these ProcessorStrips (i.e. IRE's) 0, 1, 2 the
    // original ProcessorStrip may be calling for 4, 5 and 6.
    stripNumber = 0;  //
    start = 0;
    List<ProcessorStrip> processorStrips = new ArrayList<ProcessorStrip>(numberOfReconstructionEnginesToUse);
    for (ProcessorStrip origProcessorStrip : originalProcessorStripsUsed)
    {
      // Create a new processor strip
      PanelRectangle nominalRegion = processorStripRectangles.get(stripNumber);
      ProcessorStrip processorStrip = new ProcessorStrip(stripNumber,
                                                         nominalRegion,
                                                         numberOfReconstructionEnginesToUse);
      int width = nominalRegion.getWidth();
      boolean startingPositionOfStrip = (start == 0 && width == 0);
      processorStrip.setLeadingEdgeWidth(leadWidth.get(stripNumber));
      processorStrip.setTrailingEdgeWidth(tailWidth.get(stripNumber), startingPositionOfStrip);
      processorStrip.setNominalStartInNanometers(start);
      processorStrip.setNominalStopInNanometers(start + width);

      // some processor strips aren't actually used, but we need them to help us
      // calculate the regions for the other strips. So let's not add them to the
      // list of used processor strips.
      processorStrip.setReconstructionEngineId(origProcessorStrip.getReconstructionEngineId());
      if (unUsedOriginalProcessorStrips.contains(origProcessorStrip) == false)
        processorStrips.add(processorStrip);

      // Manage the selection of strip number and walk start through.
      start = start + width;
      stripNumber++;
    }
    return processorStrips;
  }
  
  /**
   * Reuse the processorStrips created for the TestSubProgram.   Since all
   * ReconstructionRegions and their neighbors were created to fit, we do not have
   * to fret over moving AutoFocus information from one IRP to another.
   *
   * @author Chnee Khang Wah, 2013-08-07, New Scan Route
   */
  private static List<ProcessorStrip> reCreateProcessorStripsForImageGroupReusingAreasDefinedInOriginalProcessorStrips(
      List<ProcessorStrip> originalProcessorStrips,
      ImageGroup imageGroup,
      ScanPassDirectionEnum scanDirection)
  {
    Assert.expect(originalProcessorStrips != null);
    Assert.expect(imageGroup != null);

    // Intermediate results.
    int numberOfReconstructionEnginesToUse = imageGroup.getNumberReconstructionEngines();
    List<PanelRectangle> processorStripRectangles = new ArrayList<PanelRectangle>(/*numberOfReconstructionEnginesToUse*/);
    List<Integer> leadWidth = new ArrayList<Integer>(/*numberOfReconstructionEnginesToUse*/);
    List<Integer> tailWidth = new ArrayList<Integer>(/*numberOfReconstructionEnginesToUse*/);

    if (_debug)
    {
      System.out.println("start accumWidth leadWidth nomX nomY nomWidth nomHeight tailWidth");
    }

    int start = 0;
    int stripNumber = 0;
    int processorStripsUsedCount = 0;
    PanelRectangle panelRectangleToImage = imageGroup.getPanelRectangleToImage();
    List<ProcessorStrip> originalProcessorStripsUsed = new ArrayList<ProcessorStrip>();
    List<ProcessorStrip> unUsedOriginalProcessorStrips = new ArrayList<ProcessorStrip>();

    // Find out the min and max IRE id use by the imagegroup
    int minId = ImageReconstructionEngineEnum.getEnumList().size();
    int maxId = 0;
    for (ProcessorStrip origProcessorStrip : originalProcessorStrips)
    {
      if(imageGroup.usesReconstructionEngine(origProcessorStrip.getReconstructionEngineId()))
      {
        int ireId = origProcessorStrip.getReconstructionEngineId().getId();
        if(ireId<minId)
          minId = ireId;
        
        if(ireId>maxId)
          maxId = ireId;
      }
    }
    
    for (ProcessorStrip origProcessorStrip : originalProcessorStrips)
    {
      PanelRectangle processorStripRectangle = origProcessorStrip.getRegion();
      
      boolean usesReconstructionEngine = false;
      
      if(processorStripRectangle.intersects(panelRectangleToImage) && scanDirection.equals(ScanPassDirectionEnum.FORWARD))
      {
        int ireId = origProcessorStrip.getReconstructionEngineId().getId();
        if(imageGroup.usesReconstructionEngine(origProcessorStrip.getReconstructionEngineId()) || ireId<=minId)
        {
          usesReconstructionEngine= true;
        }
      }
      else if (processorStripRectangle.intersects(panelRectangleToImage) && scanDirection.equals(ScanPassDirectionEnum.REVERSE))
      {
        int ireId = origProcessorStrip.getReconstructionEngineId().getId();
        if(imageGroup.usesReconstructionEngine(origProcessorStrip.getReconstructionEngineId()) || ireId>=maxId)
        {
          usesReconstructionEngine= true;
        }
      }
      else if(processorStripRectangle.intersects(panelRectangleToImage) && scanDirection.equals(ScanPassDirectionEnum.BIDIRECTION))
      {
        int ireId = origProcessorStrip.getReconstructionEngineId().getId();
        if(imageGroup.usesReconstructionEngine(origProcessorStrip.getReconstructionEngineId()) ||
           (ireId<minId && Math.abs(ireId-minId)==1)||(ireId>maxId && Math.abs(ireId-maxId)==1))
        {
          usesReconstructionEngine= true;
        }
      }
      
      if (usesReconstructionEngine == false)
      {
        if (stripNumber == 0)
          continue;
        else
        {
          originalProcessorStripsUsed.add(origProcessorStrip);
          unUsedOriginalProcessorStrips.add(origProcessorStrip);

          // Do not increment!  This IRP has no regions
          // but must be present in the contiguous series for reconstruction.
          // processorStripsUsedCount++;
        }
      }
      else
      {
        originalProcessorStripsUsed.add(origProcessorStrip);
        processorStripsUsedCount++;
      }
      
      PanelRectangle nominalRectangleFromOriginalProcessorStrip = origProcessorStrip.getNominalRegion();
      int widthOfNominalArea = nominalRectangleFromOriginalProcessorStrip.getWidth();

      // Although this starts as the larger area (processor strip area) it will
      // be resized to be used as the nominalArea for our new processor strip.
      PanelRectangle intersectedPanelRectangleWithProcessorStrip =
          processorStripRectangle.createIntersectingPanelRectangle(panelRectangleToImage);
      PanelRectangle nominalRectangle = intersectedPanelRectangleWithProcessorStrip;

      // Easily identify 3 bands/sections for original processor strip areas.
      //   |            |                           |            |
      //   |   lead     |        nominal area       |   tail     |
      //   |   area     |       of processor strip  |   area     |
      //   |            |                           |            |
      int leadAreaStart    = processorStripRectangle.getMinX();
      int leadAreaEnd      = leadAreaStart + origProcessorStrip.getLeadingEdgeWidth();
      int nominalAreaStart = nominalRectangleFromOriginalProcessorStrip.getMinX();
      int nominalAreaEnd   = nominalRectangleFromOriginalProcessorStrip.getMaxX();
      int tailAreaEnd      = processorStripRectangle.getMaxX();
      int tailAreaStart    = tailAreaEnd - origProcessorStrip.getTrailingEdgeWidth();

      // intersection start (left edge) and end (right edge)
      int intersectStart = intersectedPanelRectangleWithProcessorStrip.getMinX();
      int intersectEnd   = intersectedPanelRectangleWithProcessorStrip.getMaxX();

      // Does intersect start in lead section?
      if ((leadAreaEnd - leadAreaStart) > 0 && intersectStart < leadAreaEnd)
      {
        if ((leadAreaEnd - leadAreaStart) > 0 && intersectEnd <= leadAreaEnd)
        {
          // intersect starts and ends in lead area section
          widthOfNominalArea = 0;
          nominalRectangle.setRect(nominalAreaStart,
                                   nominalRectangle.getMinY(),
                                   widthOfNominalArea,
                                   nominalRectangle.getHeight());
          leadWidth.add(leadAreaEnd - intersectStart);
          tailWidth.add(0);
        }

        else if ((nominalAreaEnd - nominalAreaStart) > 0 && intersectEnd <= nominalAreaEnd)
        {
          // intersect starts in lead section, intersect ends in nom area
          widthOfNominalArea = intersectEnd - nominalAreaStart;
          nominalRectangle.setRect(nominalAreaStart,
                                   nominalRectangle.getMinY(),
                                   widthOfNominalArea,
                                   nominalRectangle.getHeight());
          leadWidth.add(leadAreaEnd - intersectStart);
          tailWidth.add(0);
        }

        else if ((tailAreaEnd - tailAreaStart) > 0 && intersectEnd <= tailAreaEnd)
        {
          // intersect starts in lead section, continues thru nom area, intersect ends in tail section
          widthOfNominalArea = nominalAreaEnd - nominalAreaStart;
          nominalRectangle.setRect(nominalAreaStart,
                                   nominalRectangle.getMinY(),
                                   widthOfNominalArea,
                                   nominalRectangle.getHeight());
          leadWidth.add(leadAreaEnd - intersectStart);
          tailWidth.add(intersectEnd - tailAreaStart);
        }

        // This cannot happen because we did get an intersection.
        else
          Assert.expect(false);
      }

      // Does intersect start in the nominal section.
      else if ((nominalAreaEnd - nominalAreaStart) > 0 && intersectStart < nominalAreaEnd)
      {
        if ((leadAreaEnd - leadAreaStart) > 0 && intersectEnd <= leadAreaEnd)
        {
          Assert.expect(false, "cannot end scan in lead area before it starts in nom");
        }

        else if ((nominalAreaEnd - nominalAreaStart) > 0 && intersectEnd <= nominalAreaEnd)
        {
          // no lead section, intersect starts in nom area, intersect ends in nom area, no tail section
          widthOfNominalArea = intersectEnd - intersectStart;
          nominalRectangle.setRect(intersectStart,
                                   nominalRectangle.getMinY(),
                                   widthOfNominalArea,
                                   nominalRectangle.getHeight());
          leadWidth.add(0);
          tailWidth.add(0);
        }

        else if ((tailAreaEnd - tailAreaStart) > 0 && intersectEnd <= tailAreaEnd)
        {
          // No lead section, intersect starts in nom area, intersect ends in tail section
          widthOfNominalArea = nominalAreaEnd - intersectStart;
          nominalRectangle.setRect(intersectStart,
                                   nominalRectangle.getMinY(),
                                   widthOfNominalArea,
                                   nominalRectangle.getHeight());
          leadWidth.add(0);
          tailWidth.add(intersectEnd - tailAreaStart);
        }

        // This cannot happen because we did get an intersection.
        else
          Assert.expect(false);
      }

      // Does intersect start in the trailing section.
      else if ((tailAreaEnd - tailAreaStart) > 0 && intersectStart < tailAreaEnd)
      {
        if ((leadAreaEnd - leadAreaStart) > 0 && intersectEnd <= leadAreaEnd)
        {
          Assert.expect(false, "cannot end scan in lead area before it starts in trailing");
        }

        else if ((nominalAreaEnd - nominalAreaStart) > 0 && intersectEnd <= nominalAreaEnd)
        {
          Assert.expect(false, "cannot end scan in nom area before it starts in trailing");
        }

        else if ((tailAreaEnd - tailAreaStart) > 0 && intersectEnd <= tailAreaEnd)
        {
          // No lead section, no nom area, intersect starts and ends in tail section
          widthOfNominalArea = 0;
          nominalRectangle.setRect(tailAreaStart,
                                   nominalRectangle.getMinY(),
                                   widthOfNominalArea,
                                   nominalRectangle.getHeight());
          leadWidth.add(0);
          tailWidth.add(intersectEnd - intersectStart);
        }

        // This cannot happen because we did get an intersection.
        else
          Assert.expect(false);
      }

      // This cannot happen because we did get an intersection.
      else
        Assert.expect(false);


      if (_debug)
      {
        System.out.println(start + " " +
                           leadWidth.get(stripNumber)   + " " +
                           nominalRectangle.getMinX()   + " " +
                           nominalRectangle.getMinY()   + " " +
                           nominalRectangle.getWidth()  + " " +
                           nominalRectangle.getHeight() + " " +
                           tailWidth.get(stripNumber));
//        System.out.println("start: " + start + "       width: " + widthOfNominalArea + "       accumulatedWidth: " + sumOfAllProcessorStripWidths);
      }
      start = start + widthOfNominalArea;

      processorStripRectangles.add(intersectedPanelRectangleWithProcessorStrip);
      stripNumber++;
    }
    if (_debug)
    {
      System.out.println();
    }

    // Create the new ProcessorStrips - always using the bottom set (lowest index)
    // to create these "temp" versions of the real ProcessorStrip.  HOWEVER, please
    // keep in mind that we REALLY AREN'T CHANGING THE NUMBERS ON THE ORIGINAL.
    // Thus, while we are calling these ProcessorStrips (i.e. IRE's) 0, 1, 2 the
    // original ProcessorStrip may be calling for 4, 5 and 6.
    stripNumber = 0;  //
    start = 0;
    List<ProcessorStrip> processorStrips = new ArrayList<ProcessorStrip>(processorStripsUsedCount/*numberOfReconstructionEnginesToUse*/);
    for (ProcessorStrip origProcessorStrip : originalProcessorStripsUsed)
    {
      // Create a new processor strip
      PanelRectangle nominalRegion = processorStripRectangles.get(stripNumber);
      ProcessorStrip processorStrip = new ProcessorStrip(stripNumber,
                                                         nominalRegion,
                                                         processorStripsUsedCount);//numberOfReconstructionEnginesToUse);
      int width = nominalRegion.getWidth();
      boolean startingPositionOfStrip = (start == 0 && width == 0);
      processorStrip.setLeadingEdgeWidth(leadWidth.get(stripNumber));
      processorStrip.setTrailingEdgeWidth(tailWidth.get(stripNumber), startingPositionOfStrip);
      processorStrip.setNominalStartInNanometers(start);
      processorStrip.setNominalStopInNanometers(start + width);

      // some processor strips aren't actually used, but we need them to help us
      // calculate the regions for the other strips. So let's not add them to the
      // list of used processor strips.
      processorStrip.setReconstructionEngineId(origProcessorStrip.getReconstructionEngineId());
      if (unUsedOriginalProcessorStrips.contains(origProcessorStrip) == false)
        processorStrips.add(processorStrip);

      // Manage the selection of strip number and walk start through.
      start = start + width;
      stripNumber++;
    }
    return processorStrips;
  }
  
  /**
   * @author Roy Williams
   */
  public static void setImageReconstructionEngines(List<ImageReconstructionEngine> ires)
  {
    Assert.expect(ires != null);
    for(ImageReconstructionEngine ire : ires)
      _idToImageReconstructionEngineMap.put(ire.getId(), ire);
    _imageReconstructionEngines = ires;
  }

  /**
   * @author Roy Williams
   */
  public static int getMinStepSizeLimitInNanometers()
  {
    return _minStepSizeLimitInNanometers;
  }

  /**
   * @author Roy Williams
   */
  public static int getMaxStepSizeLimitInNanometers()
  {
    if (_maxStepSizeLimitInNanometers < 0)
    {
      int pixelSizeInNanometers = MagnificationEnum.getCurrentMaxSliceHeight().getNanoMetersPerPixel();
      // We can just get any camera so we can get the sensor attributes
      AbstractXrayCamera xRayCamera = _xrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
      // Swee Yee Wong - XCR-2630 Support New X-Ray Camera
	  _maxStepSizeLimitInNanometers = xRayCamera.getSensorWidthInPixelsWithOverHeadByte() * pixelSizeInNanometers;
    }
    return _maxStepSizeLimitInNanometers;
  }

  /**
   * @author Seng-Yew Lim
   */
  private static int calculatePanelMaxStepSizeLimitInNanometers(double panelThickness, double boardZOffset)
  {
    int pixelSizeInNanometers = MagnificationEnum.getMaxPanelSliceHeightMagnificationNanometersPerPixel(panelThickness, boardZOffset);
    // We can just get any camera so we can get the sensor attributes
    AbstractXrayCamera xRayCamera = _xrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
    // Swee Yee Wong - XCR-2630 Support New X-Ray Camera
	_maxStepSizeLimitInNanometers = xRayCamera.getSensorWidthInPixelsWithOverHeadByte() * pixelSizeInNanometers;
    return _maxStepSizeLimitInNanometers;
  }

  /**
   * @author Seng-Yew Lim
   */
  private static int calculatePanelMaxStepSizeLimitInNanometersInHighMag(double panelThickness, double boardZOffset)
  {
    int pixelSizeInNanometers = MagnificationEnum.getMaxHighMagPanelSliceHeightMagnificationNanometersPerPixel(panelThickness, boardZOffset);
    // We can just get any camera so we can get the sensor attributes
    AbstractXrayCamera xRayCamera = _xrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
    // Swee Yee Wong - XCR-2630 Support New X-Ray Camera
	_maxStepSizeLimitInNanometers = xRayCamera.getSensorWidthInPixelsWithOverHeadByte() * pixelSizeInNanometers;
    return _maxStepSizeLimitInNanometers;
  }
  
  /**
   * @author Roy Williams
   */
  public List<ImageGroup> getImageGroups()
  {
    return _imageGroups;
  }

  /**
   * @author Roy Williams
   */
  public static void setDebug(boolean state)
  {
    _debug = state;
  }

  /**
   * Debug utility function.  May appear to be unused, however, very useful if
   * trying to debug at various locations.
   *
   * @author Roy Williams
   */
  private static void printComponents(String intentOfList, Collection<ReconstructionRegion> regionsRequiringShadingCompensation)
  {
    try
    {
      System.out.println(intentOfList + ": \n");
      for (ReconstructionRegion region : regionsRequiringShadingCompensation)
      {
        System.out.println(region.getComponent().getBoardNameAndReferenceDesignator() + " " +
                           region.getRegionId() +  " " +
                           RescanStrategy.calculateMaxCandidateStepSize(region) + "\r\n");
      }
    }
    catch (Exception e)
    {
      // do nothing
    }
  }

  /**
   * Debug utility function.
   *
   * @author Roy Williams
   */
  private Pair<Integer, ImageGroup> printScanPassesForRegionAndIdentifyImageGroup(int regionId)
  {
    Pair<Integer, ImageGroup> result = null;
    try
    {
      int groupCount = 0;
      for (ImageGroup group : _imageGroups)
      {
        ReconstructionRegion region = null;
        for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : group.getRegionPairsSortedInExecutionOrder())
        {
          region = regionPair.getFirst();
          if (region.getRegionId() == regionId)
          {
            String referenceDesignator = region.getComponent().getReferenceDesignator();
            System.out.println(referenceDesignator + ": " + region.getRegionId());
            PanelRectangle groupPanelRectangle = group.getPanelRectangleToImage();
            printRectangle("ImageGroup " + groupCount + " ",  groupPanelRectangle);
            printRegionAndNeighbors(region);
            printScanPasses(region);
            System.out.println("Region SFR:" + group.getSystemFiducialRectangle(region).toString());
            System.out.println();
            result = new Pair<Integer,ImageGroup>(groupCount, group);
            return result;
          }
        }
        groupCount++;
      }
    }
    catch (Exception e)
    {
      // do nothing
    }
    return result;
  }

  /**
   * Debug utility function.
   *
   * @author Roy Williams
   */
  private void findRegionIdInImageGroups(int regiongId, List<ImageGroup> signalCompensatedGroups)
  {
    for (ImageGroup imageGroup : signalCompensatedGroups)
    {
      for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : imageGroup.getRegionPairsSortedInExecutionOrder())
      {
        ReconstructionRegion region = regionPair.getFirst();
        if (region.getRegionId() == regiongId)
        {
          this.printRectangle("ImageGroup", imageGroup.getPanelRectangleToImage());
          printRegionAndNeighbors(region);

        }
      }
    }
  }

  /**
   * Debug utility function.
   *
   * @author Roy Williams
   */
  private void printRegionAndNeighbors(ReconstructionRegion region)
  {
    String title = region.getRegionId() + " regionOfInterest";
    printRectangle(title, region.getRegionRectangleRelativeToPanelInNanoMeters());
    Set<Integer> identifiedNeighbors = new HashSet<Integer>();
    for (FocusGroup focusGroup : region.getFocusGroups())
    {
      if (focusGroup.hasNeighbors())
      {
        for (ReconstructionRegion neighbor : focusGroup.getNeighbors())
        {
          int neighborRegionId = neighbor.getRegionId();
          if (identifiedNeighbors.contains(neighborRegionId) == false)
          {
            identifiedNeighbors.add(neighborRegionId);
            title = neighbor.getRegionId() + " neighbor";
            printRectangle(title, neighbor.getRegionRectangleRelativeToPanelInNanoMeters());
          }
        }
      }
    }
  }

  /**
   * Debug utility function.
   *
   * @author Roy Williams
   */
  public static void printRectangle(String title, PanelRectangle panelRectangle)
  {
    Assert.expect(title != null);
    Assert.expect(panelRectangle != null);

    System.out.println(title +  " " +
        panelRectangle.getMinX() +  " " +
        panelRectangle.getMinY() +  " " +
        panelRectangle.getWidth() +  " " +
        panelRectangle.getHeight());
  }

  /**
   * Debug utility function.
   *
   * @author Roy Williams
   */
  private static void printScanPasses(ReconstructionRegion region)
  {
    List<ScanPass> scanPasses = region.getScanPasses();
     System.out.print("ReconstructionRegion: " + region.getRegionId() + "  scanPassCount: " + scanPasses.size() + " on pass numbers '");
    for (ScanPass scanPass : scanPasses)
    {
      System.out.print(scanPass.getId() + " ");
    }
    System.out.println("'");
  }

  /**
   * @author Kay Lannen
   */
  private static boolean isScanPassIntegrationEnabled()
  {
    return(Config.getInstance().getBooleanValue(SoftwareConfigEnum.USE_SCAN_PASS_INTEGRATION));
  }

  /**
   * @author Kay Lannen
   */
  private static boolean isScanPathLoggingEnabled()
  {
    // Uncomment this if you want to be able to log the scan path.
    // This has been removed from the config so that the option should be used only on developer builds,
    // not on customer systems.
    if (Config.isDeveloperDebugModeOn())
      return (true);
    else
      return (false);
  }

  /**
   * @author Roy Williams
   */
  private SurfaceModelBreakPoint createGlobalSurfaceModelPaths(
      TestSubProgram testSubProgram,
      List<HomogeneousImageGroup> surfaceModelGroups,
      Collection<ReconstructionRegion> allInspectionRegions,
      Collection<ReconstructionRegion> regionsWithoutShadingCompensation,
      Collection<ReconstructionRegion> regionsRequiringShadingCompensation,
      ScanPassSetting scanPassSetting) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(surfaceModelGroups != null);
    Assert.expect(allInspectionRegions != null);
    Assert.expect(regionsWithoutShadingCompensation != null);
    Assert.expect(regionsRequiringShadingCompensation != null);
    Assert.expect(scanPassSetting != null);
    
    // Last (but not least) lets add the rescan areas for areas that require an
    // assist from the global surface model.

    boolean firstGroupRequiringGlobalSurfaceModel = true;
    int count = 0;
    SurfaceModelBreakPoint breakPoint = null; // Khang Wah, 2013-08-16, RealTime PSH
    for (HomogeneousImageGroup group : surfaceModelGroups)
    {
      count += group.getRegionPairsSortedInExecutionOrder().size();
      if (_debug)
        TimerUtil.printCurrentTime("Start create group scanpath - imageZone group size:" + group.getImageZones().size());
      ScanPath scanPath = createScanPath(testSubProgram, group, scanPassSetting);
      if (_debug)
      {
        TimerUtil.printCurrentTime("End create group scanpath size:" + scanPath.getScanPasses().size());
        TimerUtil.printCurrentTime("End create group scanpath area:" + scanPath.getScanPathExtents().getRectangle2D().toString());
        TimerUtil.printCurrentTime("End create group area:" + group.getNominalAreaToImage().toString());
      }
      if (firstGroupRequiringGlobalSurfaceModel)
      {
        // We are setting the breakpoint on the first scanPass of ImageGroup
        // created specifically for GSM.
        ScanPass breakPointScanPass = scanPath.getScanPasses().get(0);
        breakPoint = new SurfaceModelBreakPoint(testSubProgram,
                                                breakPointScanPass,
                                                surfaceModelGroups,
                                                allInspectionRegions.size(),
                                                scanPassSetting.getStageSpeedValue());
        firstGroupRequiringGlobalSurfaceModel = false;
      }
    }
    count += regionsWithoutShadingCompensation.size();
    count += regionsRequiringShadingCompensation.size();
//    if(Config.isRealTimePshEnabled()==false || testSubProgram.isSufficientMemoryForRealTimePsh()==false)
//      Assert.expect(count == allInspectionRegions.size(),"regionsWithoutShadingCompensation.size():"+regionsWithoutShadingCompensation.size()+",regionsRequiringShadingCompensation.size():"+regionsRequiringShadingCompensation.size());
    return breakPoint;
  }

  /**
   * @author Roy Williams
   */
  public boolean usingGlobalSurfaceModel()
  {
    return _acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION);
  }


  /**
   * this is the actual area taken up by the scan passes which is not to be
   * confused with the bounds of the scan passes combined. The area will know
   * where the empty space is.
   *
   * @author George A. David
   * @author Roy Williams
   */
  private java.awt.geom.Area getImagableAreaOfScanPathInNanoMeters(ScanPath scanPath)
  {
    Assert.expect(scanPath != null);

    MachineRectangle machineRect = scanPath.getScanPathExtents();
    machineRect.setRect(
      machineRect.getMinX(),
      machineRect.getMinY(),
      machineRect.getWidth() + _xrayCameraArray.getImageableWidthInNanometers(MagnificationEnum.getCurrentMinSliceHeight()),
      machineRect.getHeight());
    return new Area(machineRect.getRectangle2D());
  }

  /**
   * @author Roy Williams
   */
  public static List<String> debugComponent(ReconstructionRegion region)
  {
    if (_debugComponents == null)
      return null;

    List<String> componentAndPins = null;
    List<JointInspectionData> jointInspectionData = region.getJointInspectionDataList();
    Component component = jointInspectionData.get(0).getComponent();
    String referenceDesignator = component.getReferenceDesignator();
    for (String wantedRefDesignator : _debugComponents)
    {
      if (referenceDesignator.equals(wantedRefDesignator))
      {
        if (componentAndPins == null)
          componentAndPins = new ArrayList<String>();
        componentAndPins.add(referenceDesignator + "." + jointInspectionData.get(0).getPad().getName());
      }
    }
    return componentAndPins;
  }

  /**
   * @author Roy Williams
   */
  public static List<String> debugComponent(ImageGroup group)
  {
    if (_debugComponents == null)
      return null;
    List<String> componentAndPins = null;
    for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : group.getRegionPairsSortedInExecutionOrder())
    {
      List<String> componentAndPin = debugComponent(regionPair.getFirst());
      if (componentAndPin != null)
      {
        Assert.expect(group.getNominalAreaToImage().contains(regionPair.getSecond()));
        if (componentAndPins == null)
          componentAndPins = new ArrayList<String>();
        componentAndPins.addAll(componentAndPin);
      }
    }
    return componentAndPins;
  }

  /**
   * @author Roy Williams
   */
  public static void main(final String[] args)
  {
    Config config = Config.getInstance();
    try
    {
      config.loadIfNecessary();
    }
    catch (DatastoreException de)
    {
      de.printStackTrace();
      Assert.expect(false);
    }

//    String projectName = "vie_XXL_board7";
    String projectName = null;
    if (args.length == 1)
    {
      projectName = args[0];
      disableStepSizeOptimization(true); // Forced to disable by default, so that existing regression test won't fail.
    }
    else if (args.length == 2)
    {
      projectName = args[0];
      if (args[1].equalsIgnoreCase("true"))
        disableStepSizeOptimization(true);
      else if (args[1].equalsIgnoreCase("false"))
        disableStepSizeOptimization(false);
    }

    // Field separator
    String fieldSeparator = ", ";
    String eol = "\n";
    try
    {
      if (Config.isDeveloperSystemEnabled() == false)
      {
        System.out.println("A developer license required to run this program");
        System.exit(1);
      }

      XrayTester.setXXLBased(LicenseManager.isXXLEnabled());
      XrayTester.setS2EXEnabled(LicenseManager.isS2EXEnabled());
      XrayTester.setIsVariableMagEnabled(LicenseManager.isVariableMagnificationEnabled());
      // Generate the program.
      // delete the .project file so when we run this time (i.e. again), it
      // regenerates the program
      FileUtilAxi.delete(FileName.getProjectSerializedFullPath(projectName));
      File outputFile = new File(Directory.getProjectDir(projectName) +
                                 File.separator +
                                 "scanPathInfo.csv");
      System.out.println("Output file: " + outputFile.getCanonicalFile());
      if (outputFile.exists())
        outputFile.delete();
      if (outputFile.exists())
      {
        System.out.println("Cannot remove file: " + outputFile);
        System.out.println("Another process may have this file open.");
        System.exit(2);
      }
      PrintWriter printWriter = new PrintWriter(outputFile);
      // for debug
      // printWriter = new PrintWriter(System.out);
      BooleanRef abortedDuringLoad = new BooleanRef();
      Project project = Project.load(projectName, abortedDuringLoad);
      if(abortedDuringLoad.getValue())
      {
        System.out.println("The load of the project was aborted.");
        System.exit(2);
      }
      ImagingChainProgramGenerator imagingChainProgramGenerator = new ImagingChainProgramGenerator(
          ImageAcquisitionModeEnum.PRODUCTION,
          ImagingChainProgramGenerator.getDefaultScanStrategy(),
          0);
      TestProgram testProgram = project.getTestProgram();
      testProgram.setUseOnlyInspectedRegionsInScanPath(true);
      double appropriateMaxSliceHeight = XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(project.getPanel().getThicknessInNanometers(), testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers());
      imagingChainProgramGenerator.generateProgramsForLowerImagingChainHardware(
          testProgram,
          new TestExecutionTimer(),
          appropriateMaxSliceHeight,
          Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers(testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers())));

      // Print the project name.
      printWriter.println("projectName");
      printWriter.println(projectName);
      printWriter.println();

      // Go through the subprograms and print out the relevant data.
      String alignment  = "A";
      String inspection = "I";
      int testSubProgramNdx = 0;
      double totalProgramEstimate = 0;
      List<TestSubProgram> testSubPrograms = testProgram.getFilteredTestSubPrograms();
      StringBuffer scanPassesBuffer = new StringBuffer("scanPassId" +
                                                       fieldSeparator +
                                                       "scanPassX" +
                                                       fieldSeparator +
                                                       "scanPassY" +
                                                       fieldSeparator +
                                                       "imageGroupName" +
                                                       fieldSeparator +
                                                       "usage" +
                                                       fieldSeparator +
                                                       "SurfaceModelBreakPoint"   +
                                                       eol);
      List<Pair<String, ImageGroup>> imageGroups = new ArrayList<Pair<String,ImageGroup>>();
      for (TestSubProgram testSubProgram : testSubPrograms)
      {
        ImageGroup mainImageGroup = null;
        List<Pair<String, ImageGroup>> imageGroupsForTestSubProgram = new ArrayList<Pair<String, ImageGroup>>();
        List<ScanPath> scanPaths = testSubProgram.getScanPaths();

        // Print each scan path.
        int scanPathNdx = 0;
        for (ScanPath scanPath : scanPaths)
        {
          String imageGroupName = null;
          ImageGroup imageGroup = null;
          Pair<Integer, ImageGroup> locationPair = imagingChainProgramGenerator.getAlignmentImageGroupNdx(scanPath);
          int imageGroupNdx = locationPair.getFirst();
          if (imageGroupNdx >= 0)
          {
            imageGroupName = alignment + Integer.toString(scanPathNdx);
            imageGroup = locationPair.getSecond();
          }
          else
          {
            imageGroupNdx = imagingChainProgramGenerator.getImageGroupNdx(scanPath, imagingChainProgramGenerator._imageGroups);
            imageGroupName = inspection + Integer.toString(scanPathNdx);
            imageGroup = imagingChainProgramGenerator._imageGroups.get(imageGroupNdx);
            if (mainImageGroup == null)
              mainImageGroup = imageGroup;
          }

          // Print out which scanpasses image a specific region (for debugging only).
          if (_debugRegions != null)
          {
            List<ReconstructionRegion> printRegions = new ArrayList<ReconstructionRegion>();
            for (ReconstructionRegion region : _debugRegions)
            {
              SystemFiducialRectangle sfr = imageGroup.getSystemFiducialRectangle(region);
              if (sfr != null)
              {
                List<JointInspectionData> jointInspectionData = region.getJointInspectionDataList();
                Component component = jointInspectionData.get(0).getComponent();
                String referenceDesignator = component.getReferenceDesignator();
                System.out.println("ImageGroup: "  + imageGroupName +
                                   "  Component: " + referenceDesignator +
                                   "       RRid: " + region.getRegionId() +
                                   "       sfr:  " + sfr.toString());
                printRegions.add(region);
              }
            }
            for (ReconstructionRegion region : printRegions)
            {
              printScanPasses(region);
            }
          }

          scanPathNdx++;
          Assert.expect(imageGroupName != null);
          Assert.expect(imageGroup != null);
          Pair<String, ImageGroup> pair = new Pair<String, ImageGroup>(imageGroupName, imageGroup);
          imageGroupsForTestSubProgram.add(pair);
//          if (mainImageGroup != null && (mainImageGroup.equals(imageGroup) == false))
            imageGroups.add(pair);

          for (ScanPass sp : scanPath.getScanPasses())
          {
            ProjectionTypeEnum projectionType = sp.getProjectionType();
            String usage = null;
            if (projectionType.equals(ProjectionTypeEnum.ALIGNMENT))
              usage = alignment;
            else if (projectionType.equals(ProjectionTypeEnum.INSPECTION_OR_VERIFICATION))
              usage = inspection;
            else if (projectionType.equals(ProjectionTypeEnum.INSPECTION_AND_ALIGNMENT))
              usage = alignment + inspection;
            else
              Assert.expect(usage != null);

            scanPassesBuffer.append(sp.getId() +
                                    fieldSeparator +
                                    sp.getStartPointInNanoMeters().getXInNanometers() +
                                    fieldSeparator +
                                    sp.getStartPointInNanoMeters().getYInNanometers() +
                                    fieldSeparator +
                                    imageGroupName +
                                    fieldSeparator +
                                    usage          +
                                    fieldSeparator +
                                    sp.hasBreakPoints() +
                                    eol);
            scanPassesBuffer.append(sp.getId() +
                                    fieldSeparator +
                                    sp.getEndPointInNanoMeters().getXInNanometers() +
                                    fieldSeparator +
                                    sp.getEndPointInNanoMeters().getYInNanometers() +
                                    fieldSeparator +
                                    imageGroupName +
                                    fieldSeparator +
                                    usage          +
                                    fieldSeparator +
                                    sp.hasBreakPoints() +
                                    eol);

          }
        }

        // Print out the global (i.e. summed up) data.
        printWriter.println();
        double testSubProgramEstimate = 0;
        printWriter.println("TestSubProgram"    + fieldSeparator +
                            "ImageGroup"        + fieldSeparator +
                            "StepSize"          + fieldSeparator +
                            "ExecutionTime"     + fieldSeparator +
                            "AccumulatedTime"   + fieldSeparator +
                            "BeginingScanPass"  + fieldSeparator +
                            "ImagedArea_X"      + fieldSeparator +
                            "ImagedArea_Y"      + fieldSeparator +
                            "ImagedArea_W"      + fieldSeparator +
                            "ImagedArea_H"      + fieldSeparator +
                            "SysFiducialRect_X" + fieldSeparator +
                            "SysFiducialRect_Y" + fieldSeparator +
                            "SysFiducialRect_W" + fieldSeparator +
                            "SysFiducialRect_H");
        for (Pair<String, ImageGroup> imageGroupPair : imageGroupsForTestSubProgram)
        {
          ImageGroup imageGroup = imageGroupPair.getSecond();
          if (imageGroup instanceof HomogeneousImageGroup)
          {
            double imageGroupEstimate = ((HomogeneousImageGroup)imageGroup).estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSizeMethodB();
            testSubProgramEstimate += imageGroupEstimate;
            totalProgramEstimate   += imageGroupEstimate;
            PanelRectangle panelRectangleToImage = imageGroup.getPanelRectangleToImage();
            SystemFiducialRectangle systemFiducialRectangle = imageGroup.getNominalAreaToImage();
            int firstScanPass = imageGroup.getScanPath().getScanPasses().get(0).getId();
            int stepSize = imageGroupPair.getSecond().getImageZones().last().getMeanStepSizeInNanoMeters();
            printWriter.println(testSubProgramNdx                   + fieldSeparator +
                                imageGroupPair.getFirst()           + fieldSeparator +
                                stepSize                            + fieldSeparator +
                                imageGroupEstimate                  + fieldSeparator +
                                totalProgramEstimate                + fieldSeparator +
                                firstScanPass                       + fieldSeparator +
                                panelRectangleToImage.getMinX()     + fieldSeparator +
                                panelRectangleToImage.getMinY()     + fieldSeparator +
                                panelRectangleToImage.getWidth()    + fieldSeparator +
                                panelRectangleToImage.getHeight()   + fieldSeparator +
                                systemFiducialRectangle.getMinX()   + fieldSeparator +
                                systemFiducialRectangle.getMinY()   + fieldSeparator +
                                systemFiducialRectangle.getWidth()  + fieldSeparator +
                                systemFiducialRectangle.getHeight());
          }
        }
        testSubProgramNdx++;
      }

      // Okay, now we will send it all out to the output file.
      printWriter.println();
      printWriter.println(scanPassesBuffer.toString());
      scanPassesBuffer = null;

      // Print out the reconstruction region in formation for each of the shaded groups.
      printWriter.println();
      printWriter.println("imageGroup" +
                          fieldSeparator +
                          "regionId" +
                          fieldSeparator +
                          "isAlignment" +
                          fieldSeparator +
                          "board" +
                          fieldSeparator +
                          "component" +
                          fieldSeparator +
                          "pin" +
                          fieldSeparator +
                          "compensation type" +
                          fieldSeparator +
                          "ScanPasses" +
                          eol);
      String shaded = "S";
      String gsm    = "G";
      boolean firstImageGroup = true;
      for (Pair<String, ImageGroup> imageGroupPair : imageGroups)
      {
        ImageGroup imageGroup = imageGroupPair.getSecond();
        if (imageGroup instanceof HomogeneousImageGroup)
        {
          String imageGroupName = imageGroupPair.getFirst();
         List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionPairs =
             imageGroupPair.getSecond().getRegionPairsSortedInExecutionOrder();
         if (firstImageGroup)
          {
            firstImageGroup = false;
            List<Pair<ReconstructionRegion, SystemFiducialRectangle>> tempRegionPairs =
               new ArrayList<Pair<ReconstructionRegion,SystemFiducialRectangle>>();
            for (ReconstructionRegion alignmentRegion : imagingChainProgramGenerator._alignmentRegionInfoMap.keySet())
            {
              Pair<ReconstructionRegion, SystemFiducialRectangle> tempPair =
                  new Pair<ReconstructionRegion, SystemFiducialRectangle>(
                      alignmentRegion, new SystemFiducialRectangle(0, 0, 1, 1));
              tempRegionPairs.add(tempPair);
            }
            tempRegionPairs.addAll(regionPairs);
            regionPairs = tempRegionPairs;
          }
          for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : regionPairs)
          {
            ReconstructionRegion region = regionPair.getFirst();
            List<JointInspectionData> jointInspectionData = region.getJointInspectionDataList();
            Component component = jointInspectionData.get(0).getComponent();
            StringBuffer compensation = new StringBuffer();
            if (imagingChainProgramGenerator.usingGlobalSurfaceModel() &&
                reconstructionRegionRequiresGlobalSurfaceModel(region))
              compensation.append(gsm);
            else if (region.requiresShadingCompensation())
              compensation.append(shaded);
            printWriter.print(imageGroupName +
                              fieldSeparator +
                              region.getRegionId() +
                              fieldSeparator +
                              ((region.isAlignmentRegion()) ? alignment : inspection) +
                              fieldSeparator +
                              component.getBoard().getName() +
                              fieldSeparator +
                              component.getReferenceDesignator() +
                              fieldSeparator +
                              region.getJointInspectionDataList().iterator().next().getPad().getName() +
                              fieldSeparator +
                              compensation.toString());
            for (ScanPass scanPass : region.getScanPasses())
            {
              printWriter.print(fieldSeparator + scanPass.getId());
            }
            printWriter.println();
          }
        }
      }
      printWriter.println();
      printWriter.flush();
      printWriter.close();

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Roy Williams
   */
  private Pair<Integer, ImageGroup> getAlignmentImageGroupNdx(ScanPath scanPath)
  {
    Assert.expect(scanPath != null);

    Set<ReconstructionRegion> regionsKeySet = _alignmentRegionInfoMap.keySet();

    int ndx = 0;
    for (ReconstructionRegion region : regionsKeySet)
    {
      Pair<ImageGroup, AlignmentScanPathSubSection> alignmentRegionInfo = _alignmentRegionInfoMap.get(region);
      if (alignmentRegionInfo == null)
      {
        ndx++;
        continue;
      }
      ImageGroup imageGroup = alignmentRegionInfo.getFirst();
      if (imageGroup.getScanPath().equals(scanPath))
        return new Pair<Integer,ImageGroup>(ndx, imageGroup);
      ndx++;
    }
    return new Pair<Integer,ImageGroup>(-1, null);
  }

  /**
   * @author Roy Williams
   */
  private int getImageGroupNdx(ScanPath scanPath,
                               List<ImageGroup> imageGroups)
  {
    Assert.expect(scanPath != null);
    Assert.expect(imageGroups != null);

    int ndx = 0;
    for (ImageGroup imageGroup : imageGroups)
    {
      if (imageGroup.getScanPath().equals(scanPath))
        return ndx;
      ndx++;
    }
    return -1;
  }

  /**
   * @author Poh Kheng
   */
  public void clearProjectInfo () throws XrayTesterException
  {
    if (_debugRegions != null)
      _debugRegions.clear();

    if (_alignmentRegionInfoMap != null)
      _alignmentRegionInfoMap.clear();

    if (_imageGroups != null)
      _imageGroups.clear();

  }

  /**
   * @author Cheah, Lee Herng
   */
  private static int getXaxisMaximumPositionLimitInNanometers() throws XrayTesterException
  {
      int xAxisMaximumPositionLimit = 0;
      HardwareConfigEnum xAxisMaximumPositionLimitEnum = HardwareConfigEnum.X_AXIS_MAXIMUM_POSITION_LIMIT_IN_NANOMETERS;
      String xAxisMaximumPositionLimitString = Config.getInstance().getStringValue(xAxisMaximumPositionLimitEnum);
      
      try
      {
          xAxisMaximumPositionLimit = StringUtil.convertStringToInt(xAxisMaximumPositionLimitString);
      }
      catch(BadFormatException ex)
      {
        InvalidValueDatastoreException invalidValueException = new InvalidValueDatastoreException(xAxisMaximumPositionLimitEnum.getKey(),
                                                                                                  xAxisMaximumPositionLimitString.trim(),
                                                                                                  xAxisMaximumPositionLimitEnum.getFileName());
        invalidValueException.initCause(ex);
        throw invalidValueException;
      }
      
      return xAxisMaximumPositionLimit;
  }

  /**
   * This is to fix the X axis position error limit.
   * If we found that X position exceeds the x axis maximum position limit,
   * we will confine the x axis position to 0.01% from the x axis maximum position limit.
   *
   * @author Cheah Lee Herng
   */
  private static int adjustXStagePosition(int xStagePosition) throws XrayTesterException
  {
      int xAxisMaximumPositionLimit = getXaxisMaximumPositionLimitInNanometers();
      if (xStagePosition > xAxisMaximumPositionLimit)
      {
          // We offset 0.01% from the x axis position limit
          int offset = (int)(0.0001 * xAxisMaximumPositionLimit);
          int adjustedXStagePosition = xAxisMaximumPositionLimit - offset;
          return adjustedXStagePosition;
      }
      else
          return xStagePosition;
  }

  /**
   * @param defaultMeanStepSizeLimitInNanometers
   *
   *  @author Chong Wei Chin
   */
  public static void setDefaultMeanStepSizeLimitInNanometers(ReconstructionRegion reconstructionRegion)
  {
    int maxCandidateStepSize = RescanStrategy.calculateMaxCandidateStepSize(reconstructionRegion);
    _defaultMeanStepSizeLimitInNanometers = maxCandidateStepSize;
  }

  /**
   * @param defaultMeanStepSizeLimitInNanometers
   *
   * @author Chong Wei Chin
   */
  public static void resetDefaultMeanStepSizeLimitInNanometers()
  {
    _defaultMeanStepSizeLimitInNanometers = -1;
    _maxStepSizeLimitInNanometers = -1;
    _ditherMaxInNanometers = -1;
    calculateMaxStepSizeInNanometers();
  }
  
  /**
   * This function will calculate the estimation of Scan Path execution time.
   * Note: The UI ScanPath is not the same with actual TestSubProgram ScanPath
   * due to reassign scanPath at function void calculateDataSeriesNeeded().
   *
   * @author Lim Lay Ngor
   */
  private double estimateTestSubProgramExecutionTime(TestSubProgram testSubProgram)
  {
    //Consider stages move X & Y separately  
    double xAxisScanMotionVelocity = 0.0f;
    double yAxisScanMotionVelocity = 0.0f;
    double xAxisPointToPointScanMotionVelocity = 0.0f;
    double yAxisPointToPointScanMotionVelocity = 0.0f;

    if (testSubProgram.isLowMagnification())//LowMagnification using Profile 0.
    {
      String xAxisScanMotionProfile0VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
      String yAxisScanMotionProfile0VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
      String xAxisPointToPointScanMotionProfile0VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
      String yAxisPointToPointScanMotionProfile0VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);

      try
      {
        xAxisScanMotionVelocity = StringUtil.convertStringToDouble(xAxisScanMotionProfile0VelocityString);
        yAxisScanMotionVelocity = StringUtil.convertStringToDouble(yAxisScanMotionProfile0VelocityString);
        xAxisPointToPointScanMotionVelocity = StringUtil.convertStringToDouble(xAxisPointToPointScanMotionProfile0VelocityString);
        yAxisPointToPointScanMotionVelocity = StringUtil.convertStringToDouble(yAxisPointToPointScanMotionProfile0VelocityString);
      }
      catch (BadFormatException ex)
      {
        //Assert.logException(ex);
        //System.out.println( (new Throwable()).getStackTrace()[0].toString()+ ex); //more detail but slower
        System.out.println("estimateTestSubProgramExecutionTime" + ex);
        return 0.0f;
      }
    }
    else//HighMagnification using Profile 1.
    {
      String xAxisScanMotionProfile1VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
      String yAxisScanMotionProfile1VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
      String xAxisPointToPointScanMotionProfile1VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
      String yAxisPointToPointScanMotionProfile1VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);

      try
      {
        xAxisScanMotionVelocity = StringUtil.convertStringToDouble(xAxisScanMotionProfile1VelocityString);
        yAxisScanMotionVelocity = StringUtil.convertStringToDouble(yAxisScanMotionProfile1VelocityString);
        xAxisPointToPointScanMotionVelocity = StringUtil.convertStringToDouble(xAxisPointToPointScanMotionProfile1VelocityString);
        yAxisPointToPointScanMotionVelocity = StringUtil.convertStringToDouble(yAxisPointToPointScanMotionProfile1VelocityString);
      }
      catch (BadFormatException ex)
      {
        //Assert.logException(ex);
        //System.out.println( (new Throwable()).getStackTrace()[0].toString()+ ex); //more detail but slower
        System.out.println("estimateTestSubProgramExecutionTime" + ex);
        return 0.0f;
      }
    }

    double totalEstimatedTime = 0.0f;
    int xPreviousPosition = 0;
    List<ScanPath> scanPathList = testSubProgram.getScanPaths();

    //Use arrays to keep different type of estimatedTime to ease user retrieve more detail estimated time in future.
    int scanPathCounter = 0;
    double[] xEstimatedScanTime;
    double[] yEstimatedScanTime;
    double[] xEstimatedPointToPointTime;
    double[] yEstimatedPointToPointTime;
    xEstimatedScanTime = new double[scanPathList.size()];
    yEstimatedScanTime = new double[scanPathList.size()];
    xEstimatedPointToPointTime = new double[scanPathList.size()];
    yEstimatedPointToPointTime = new double[scanPathList.size()];
    
    for (ScanPath scanPath : scanPathList)
    {
      //initializing...
      xEstimatedScanTime[scanPathCounter] = 0.0f;
      yEstimatedScanTime[scanPathCounter] = 0.0f;
      xEstimatedPointToPointTime[scanPathCounter] = 0.0f;
      yEstimatedPointToPointTime[scanPathCounter] = 0.0f;

      int totalScanPass = 0;//Total path in a scanPath
      List<ScanPass> scanPassList = scanPath.getScanPasses();
      for (ScanPass scanPass : scanPassList)
      {
        //Calculate X estimation time         
        //xPreviousPosition = 0 means first point of the testSubProgram. Skip to calculate X distance.
        if (xPreviousPosition != 0)
        {
          //For X axis, skip the first point cause scanPaths only contain Y paths.
          if (totalScanPass == 0)//first point of next new scanPath
          {
            //Point to point velocity           
            xEstimatedPointToPointTime[scanPathCounter] += ((double) (Math.abs(xPreviousPosition
              - scanPass.getStartPointInNanoMeters().getXInNanometers())) / xAxisPointToPointScanMotionVelocity);
          }
          else
          {
            xEstimatedScanTime[scanPathCounter] += ((double) (Math.abs(xPreviousPosition
              - scanPass.getStartPointInNanoMeters().getXInNanometers())) / xAxisScanMotionVelocity);
          }
        }

        //Calculate Y estimation time         
        if (totalScanPass == 0 && xPreviousPosition != 0)//first point of next new scanPath
        {
          yEstimatedPointToPointTime[scanPathCounter] += ((double) (Math.abs(scanPass.getEndPointInNanoMeters().getYInNanometers()
            - scanPass.getStartPointInNanoMeters().getYInNanometers())) / yAxisPointToPointScanMotionVelocity);
        }
        else
        {
          yEstimatedScanTime[scanPathCounter] += ((double) (Math.abs(scanPass.getEndPointInNanoMeters().getYInNanometers()
            - scanPass.getStartPointInNanoMeters().getYInNanometers())) / yAxisScanMotionVelocity);
        }

        xPreviousPosition = scanPass.getStartPointInNanoMeters().getXInNanometers();
        ++totalScanPass;
      }

      totalEstimatedTime += xEstimatedScanTime[scanPathCounter] + yEstimatedScanTime[scanPathCounter]
        + xEstimatedPointToPointTime[scanPathCounter] + yEstimatedPointToPointTime[scanPathCounter];

      ++scanPathCounter;
    }

    return totalEstimatedTime;
  }

  /**
   * @author Cheah Lee Herng 
   * edited by sheng chuan , changed from previous scan pass break point to current scan pass break point
   */
  private void setupSurfaceMapBreakPoint(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);
    
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP))
    {
      // XCR-3780 Additional Surface Map Info log
      FileLoggerAxi surfaceMapLogger = new FileLoggerAxi(FileName.getSurfaceMapInfoLogFullPath(testProgram.getProject().getName(), 
                                                                                               Directory.getDirName(System.currentTimeMillis())));
      
      for(TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
      {
        boolean hasEnabledOpticalCameraRectangle = testSubProgram.hasEnabledOpticalCameraRectangles() ? true : false;              
        
        if (hasEnabledOpticalCameraRectangle)
        {
          for(ScanPass scanPass : testSubProgram.getScanPasses())
          {
            if (hasEnabledOpticalCameraRectangle)
            {
              if (scanPass.getProjectionType().equals(ProjectionTypeEnum.INSPECTION_OR_VERIFICATION))
              {
                new SurfaceMapBreakPoint(testSubProgram,scanPass, testSubProgram.getAlignmentStageSpeed().toDouble(), surfaceMapLogger);                        
                break;
              } 
            }
          }
          //no scenario found to go in this condition, comment out temporary
//          if (hasOpticalRegions && hasBreakPoint == false)
//          {
//            // Once we reach here, it means whole TestSubProgram scan pass contains A+I
//            new SurfaceMapBreakPoint(testSubProgram,prevScanPass);
//          }
        }
      }
    }
  }
  
  /**
   * @author Chnee Khang Wah
   */
  private Collection<ReconstructionRegion> setupNeighborAndTriggerRegionForGlobalSurfaceModalRegion(ImageGroup mainGroup, 
                                                                                                    ImageGroup surfaceModelGroup)
  {
    List<Pair<ReconstructionRegion, SystemFiducialRectangle>> mainList = mainGroup.getRegionPairsSortedInExecutionOrder();
    List<Pair<ReconstructionRegion, SystemFiducialRectangle>> surfaceModelList = surfaceModelGroup.getRegionPairsSortedInExecutionOrder();
    
    Collection<ReconstructionRegion> regionsWithCompensation = new ArrayList<ReconstructionRegion>();
    Collection<ReconstructionRegion> regionsWithHighDynamicRange = new ArrayList<ReconstructionRegion>();
    ReconstructionRegion lastRegion = null;
    
    // Assign Neighbor to all PSH region
    for (Pair<ReconstructionRegion, SystemFiducialRectangle> mainPair: mainList)
    {
      ReconstructionRegion mainRegion = mainPair.getFirst();
      // Region with PSH turn on or IC turn on are not allowed to be neighbor or trigger to others.
      if (mainRegion.useAutoPredictiveSliceHeight() || 
          reconstructionRegionRequiresGlobalSurfaceModel(mainRegion) ||
          doesRegionPassFilters(mainRegion) == false)
        continue;
      
      if(mainRegion.isCompensated()==true)
        regionsWithCompensation.add(mainRegion);
      
      if(mainRegion.useDynamicRangeOptimization()==true)
        regionsWithHighDynamicRange.add(mainRegion);

      lastRegion = mainRegion;
      
      Point2D mainPoint = new Point2D.Double(mainPair.getSecond().getCenterX(), mainPair.getSecond().getCenterY());
      
      for (Pair<ReconstructionRegion, SystemFiducialRectangle> surfaceModelPair: surfaceModelList)
      {
        ReconstructionRegion surfaceModelRegion = surfaceModelPair.getFirst();
        
        //Siew Yeng - XCR-3781 - handle selective component
        PshSettings pshSettings = surfaceModelRegion.getTestSubProgram().getTestProgram().getProject().getPanel().getPshSettings();
        if(pshSettings.isUsingSelectiveComponent(surfaceModelRegion.getComponent()))
        {
          if (doesRegionPassFilters(surfaceModelRegion) && 
              pshSettings.isNeighborOfPshComponent(mainRegion.getComponent().getComponentType(), surfaceModelRegion.getComponent().getComponentType()))
          {
            mainRegion.addConsumerRegion(surfaceModelRegion);
            surfaceModelRegion.incrementProducerCount();
          }
          
          continue;
        }
        
        // PSH Region is not allow to refer to IC region if itself is not ICed
        if(mainRegion.isCompensated()==true && surfaceModelRegion.isCompensated()==false)
          continue;
        
        //Ngie Xing, PSH region without high DRO level is not allowed to be refered
        if(mainRegion.useDynamicRangeOptimization()==true && surfaceModelRegion.useDynamicRangeOptimization()==false)
          continue;

        Point2D surfaceModelPoint = new Point2D.Double(surfaceModelPair.getSecond().getCenterX(), surfaceModelPair.getSecond().getCenterY());
        
        // XCR-3568 Software crash when skip serial number of panel based multiboard due to PSH neighbour is being skipped
        if (surfaceModelPoint.distance(mainPoint)<=DISTANGE_THRESHOLD_IN_NM)
        {
          if (doesRegionPassFilters(surfaceModelRegion))
          {
            mainRegion.addConsumerRegion(surfaceModelRegion);
            surfaceModelRegion.incrementProducerCount();
          }
        }
      }
    }
    
    // Identify the very last non-PSH region
    if(regionsWithHighDynamicRange.size()>0)
    {
      HomogeneousImageGroup icGroup = new HomogeneousImageGroup(regionsWithHighDynamicRange);
      List<Pair<ReconstructionRegion, SystemFiducialRectangle>> icList = icGroup.getRegionPairsSortedInExecutionOrder();
      lastRegion = icList.get(icList.size()-1).getFirst();
    }
    else if(regionsWithCompensation.size()>0)
    {
      HomogeneousImageGroup icGroup = new HomogeneousImageGroup(regionsWithCompensation);
      List<Pair<ReconstructionRegion, SystemFiducialRectangle>> icList = icGroup.getRegionPairsSortedInExecutionOrder();
      lastRegion = icList.get(icList.size()-1).getFirst();
    }
    
    // System will trigget exception when it detect PSH region without neighbor and asking for user input.
    // However we still try to assign neighbor to them as precaution, to prevent IRP hung.
    
    // We may come to a situation where no neighbor can be found for some PSH region,
    // in this case, we just assign the last region to act as their neighbor, 
    // to trigger them to perform reconstruction at the end of production.
    Collection<ReconstructionRegion> regionsWithoutNeighbor = new ArrayList<ReconstructionRegion>();
    
    for (Pair<ReconstructionRegion, SystemFiducialRectangle> surfaceModelPair: surfaceModelList)
    {
      // XCR-3568 Software crash when skip serial number of panel based multiboard due to PSH neighbour is being skipped
      if (doesRegionPassFilters(surfaceModelPair.getFirst()) == false)
      {
        surfaceModelPair.getFirst().setWaitForKickStart(false);
        surfaceModelPair.getFirst().clearConsumerAndProducer();
        continue;
      }
      
      // the region has no neighbor to refer to
      // Try to assign last region to those no neighbor PSH regions, so that someone will trigger them
      if(surfaceModelPair.getFirst().GetTotalProducerCount()==0)
      {
        regionsWithoutNeighbor.add(surfaceModelPair.getFirst());
        
        // use last region to act as their trigger
        if(lastRegion != null)
        {
          lastRegion.addConsumerRegion(surfaceModelPair.getFirst());
          surfaceModelPair.getFirst().incrementProducerCount();
          System.out.println("RT_PSH::PSH Region ("+surfaceModelPair.getFirst().getFullDescription() + 
            ") on Board " + surfaceModelPair.getFirst().getBoard().getName() + 
            " no neighbor, refer to "+lastRegion.getFullDescription()+" as neighbor.");
        }
      }
    }
    
    // Precaution, should not reach here, if there are really no neighbor, put it as autofocus.
    if(regionsWithoutNeighbor.size()>0)
    {
      for (ReconstructionRegion region: regionsWithoutNeighbor)
      {
        if(region.GetTotalProducerCount()==0)
        {
          region.setWaitForKickStart(false);
          region.clearConsumerAndProducer();
          System.out.println("RT_PSH::PSH Region ("+region.getFullDescription() + 
              ") on Board " + region.getBoard().getName() + 
              " no neighbor, refer to "+lastRegion.getFullDescription()+" as neighbor, switch to autofocus.");
        }
      }
    }
    
    return regionsWithoutNeighbor;
  }

/**
   * This function will calculate the estimation of Scan Path execution time.
   * Note: The UI ScanPath is not the same with actual TestSubProgram ScanPath
   * due to reassign scanPath at function void calculateDataSeriesNeeded().
   *
   * @author Lim Lay Ngor
   * @author Chnee Khang Wah, add second parameter: List<ScanPath>
   */
  private double estimateVariableDivNScanPathExecutionTime(TestSubProgram testSubProgram, List<ScanPath> scanPaths)
  {
    //Consider stages move X & Y separately  
    double xAxisScanMotionVelocity = 0.0f;
    double yAxisScanMotionVelocity = 0.0f;
    double xAxisPointToPointScanMotionVelocity = 0.0f;
    double yAxisPointToPointScanMotionVelocity = 0.0f;

    if (testSubProgram.isLowMagnification())//LowMagnification using Profile 0.
    {
      String xAxisScanMotionProfile0VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
      String yAxisScanMotionProfile0VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
      String xAxisPointToPointScanMotionProfile0VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);
      String yAxisPointToPointScanMotionProfile0VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_0_VELOCITY_IN_NANOMETERS_PER_SECOND);

      try
      {
        xAxisScanMotionVelocity = StringUtil.convertStringToDouble(xAxisScanMotionProfile0VelocityString);
        yAxisScanMotionVelocity = StringUtil.convertStringToDouble(yAxisScanMotionProfile0VelocityString);
        xAxisPointToPointScanMotionVelocity = StringUtil.convertStringToDouble(xAxisPointToPointScanMotionProfile0VelocityString);
        yAxisPointToPointScanMotionVelocity = StringUtil.convertStringToDouble(yAxisPointToPointScanMotionProfile0VelocityString);
      }
      catch (BadFormatException ex)
      {
        //Assert.logException(ex);
        //System.out.println( (new Throwable()).getStackTrace()[0].toString()+ ex); //more detail but slower
        System.out.println("estimateTestSubProgramExecutionTime" + ex);
        return 0.0f;
      }
    }
    else//HighMagnification using Profile 1.
    {
      String xAxisScanMotionProfile1VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.X_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
      String yAxisScanMotionProfile1VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.Y_AXIS_SCAN_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
      String xAxisPointToPointScanMotionProfile1VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.X_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);
      String yAxisPointToPointScanMotionProfile1VelocityString = Config.getInstance().getStringValue(HardwareConfigEnum.Y_AXIS_POINT_TO_POINT_MOTION_PROFILE_1_VELOCITY_IN_NANOMETERS_PER_SECOND);

      try
      {
        xAxisScanMotionVelocity = StringUtil.convertStringToDouble(xAxisScanMotionProfile1VelocityString);
        yAxisScanMotionVelocity = StringUtil.convertStringToDouble(yAxisScanMotionProfile1VelocityString);
        xAxisPointToPointScanMotionVelocity = StringUtil.convertStringToDouble(xAxisPointToPointScanMotionProfile1VelocityString);
        yAxisPointToPointScanMotionVelocity = StringUtil.convertStringToDouble(yAxisPointToPointScanMotionProfile1VelocityString);
      }
      catch (BadFormatException ex)
      {
        //Assert.logException(ex);
        //System.out.println( (new Throwable()).getStackTrace()[0].toString()+ ex); //more detail but slower
        System.out.println("estimateTestSubProgramExecutionTime" + ex);
        return 0.0f;
      }
    }

    double totalEstimatedTime = 0.0f;
    int xPreviousPosition = 0;
    List<ScanPath> scanPathList = scanPaths;

    //Use arrays to keep different type of estimatedTime to ease user retrieve more detail estimated time in future.
    int scanPathCounter = 0;
    double[] xEstimatedScanTime;
    double[] yEstimatedScanTime;
    double[] xEstimatedPointToPointTime;
    double[] yEstimatedPointToPointTime;
    xEstimatedScanTime = new double[scanPathList.size()];
    yEstimatedScanTime = new double[scanPathList.size()];
    xEstimatedPointToPointTime = new double[scanPathList.size()];
    yEstimatedPointToPointTime = new double[scanPathList.size()];
    
    for (ScanPath scanPath : scanPathList)
    {
      //initializing...
      xEstimatedScanTime[scanPathCounter] = 0.0f;
      yEstimatedScanTime[scanPathCounter] = 0.0f;
      xEstimatedPointToPointTime[scanPathCounter] = 0.0f;
      yEstimatedPointToPointTime[scanPathCounter] = 0.0f;

      int totalScanPass = 0;//Total path in a scanPath
      List<ScanPass> scanPassList = scanPath.getScanPasses();
      List<ScanPass> scanPassList2 = new ArrayList<ScanPass>(scanPassList);
      ScanPass prePass = scanPassList2.get(0);
      for (ScanPass scanPass : scanPassList)
      {
        int preYEnd=prePass.getEndPointInNanoMeters().getYInNanometers();
        int preYStart=prePass.getStartPointInNanoMeters().getYInNanometers();
        int curYEnd=scanPass.getEndPointInNanoMeters().getYInNanometers();
        int curYStart=scanPass.getStartPointInNanoMeters().getYInNanometers();
        int nexYEnd=-1, nexYStart=-1;
        if (scanPassList2.size()>1)
        {
          nexYEnd=scanPassList2.get(1).getEndPointInNanoMeters().getYInNanometers();
          nexYStart=scanPassList2.get(1).getStartPointInNanoMeters().getYInNanometers();
        }
        
        if ((totalScanPass % 2) == 0)
        {
          if (scanPass.getProjectionType().equals(ProjectionTypeEnum.ALIGNMENT)==false &&
              prePass.getProjectionType().equals(ProjectionTypeEnum.ALIGNMENT)==false)
          {
            curYStart = Math.min(curYEnd,Math.min(curYStart,Math.min(preYEnd, preYStart)));
            curYEnd   = Math.max(curYEnd,Math.max(curYStart,Math.max(nexYEnd, nexYStart)));
          }
        }
        else
        {
          if (scanPass.getProjectionType().equals(ProjectionTypeEnum.ALIGNMENT)==false &&
              prePass.getProjectionType().equals(ProjectionTypeEnum.ALIGNMENT)==false)
          {
            curYStart = Math.max(curYEnd,Math.max(curYStart,Math.max(preYEnd, preYStart)));
            curYEnd   = Math.min(curYEnd,Math.min(curYStart,Math.min(nexYEnd, nexYStart)));
          }
        }
        
        //Calculate X estimation time         
        //xPreviousPosition = 0 means first point of the testSubProgram. Skip to calculate X distance.
        if (xPreviousPosition != 0)
        {
          //For X axis, skip the first point cause scanPaths only contain Y paths.
          if (totalScanPass == 0)//first point of next new scanPath
          {
            //Point to point velocity           
            xEstimatedPointToPointTime[scanPathCounter] += ((double) (Math.abs(xPreviousPosition
              - scanPass.getStartPointInNanoMeters().getXInNanometers())) / xAxisPointToPointScanMotionVelocity);
          }
          else
          {
            xEstimatedScanTime[scanPathCounter] += ((double) (Math.abs(xPreviousPosition
              - scanPass.getStartPointInNanoMeters().getXInNanometers())) / xAxisScanMotionVelocity);
          }
        }

        //Calculate Y estimation time         
        if (totalScanPass == 0 && xPreviousPosition != 0)//first point of next new scanPath
        {
          yEstimatedPointToPointTime[scanPathCounter] += ((double) (Math.abs(curYEnd - curYStart)) / yAxisPointToPointScanMotionVelocity);
        }
        else
        {
          yEstimatedScanTime[scanPathCounter] += ((double) (Math.abs(curYEnd - curYStart)) / yAxisScanMotionVelocity);
        }

        xPreviousPosition = scanPass.getStartPointInNanoMeters().getXInNanometers();
        ++totalScanPass;
        prePass = scanPassList2.remove(0);
      }

      totalEstimatedTime += xEstimatedScanTime[scanPathCounter] + yEstimatedScanTime[scanPathCounter]
        + xEstimatedPointToPointTime[scanPathCounter] + yEstimatedPointToPointTime[scanPathCounter];

      ++scanPathCounter;
    }

    return totalEstimatedTime;
  }
  
  /*
   * @author Chnee Khang Wah
   */
  private List<HomogeneousImageGroup> attemptToMergeGroupsAndScanPaths(TestSubProgram testSubProgram, 
                                                                       List<HomogeneousImageGroup> imageGroups,
                                                                       ScanPassSetting scanPassSetting) throws XrayTesterException
  {
    Assert.expect(imageGroups.size()>1);
    
    HomogeneousImageGroup baseGroup = imageGroups.remove(0);
    createFixXScanPath(testSubProgram, baseGroup, scanPassSetting);
    
    List<HomogeneousImageGroup> toMaintainGroups = new ArrayList<HomogeneousImageGroup>();
    for(HomogeneousImageGroup group : imageGroups)
    {
      if(isWorthToMerge(testSubProgram, baseGroup, group))
        baseGroup = mergeGroupsAndScanPaths(testSubProgram, baseGroup, group, true);
      else
        toMaintainGroups.add(group);
    }
    
    List<HomogeneousImageGroup> mergedGroups = new ArrayList<HomogeneousImageGroup>();
    mergedGroups.add(baseGroup);
    
    if(toMaintainGroups.isEmpty()==false)
      mergedGroups.addAll(toMaintainGroups);
    
    return mergedGroups;
  }
  
  /**
   * @author Chnee Khang Wah
   * @param testSubProgram
   * @param imageGroup1
   * @param imageGroup2
   * @return
   * @throws XrayTesterException 
   */
  private boolean isWorthToMerge(TestSubProgram testSubProgram, 
                                 HomogeneousImageGroup imageGroup1, 
                                 HomogeneousImageGroup imageGroup2) throws XrayTesterException
  {
    // Determine merging decision base on:
    // 1. intersection of imageGrous' rectangle (in X)
    // 2. total travel time of scan path
    
    // #1 intersection of imageGroups' scanpath rectangle
    MachineRectangle rect1 = imageGroup1.getScanPath().getScanPathExtents();
    MachineRectangle rect2 = imageGroup2.getScanPath().getScanPathExtents();
    if(rect1.intersectsX(rect2)==false)
      return true;
    
    // Cloned the scan path, not possible to use "CreateFixXScanPath" because this function cater only 
    // on image group which only have 1 signal compensation only, while both imageGroup1 and imageGroup2 
    // are potentially polluted with more than one signal compensation.
    ScanPath clonedScanPath1 = new ScanPath(imageGroup1.getScanPath().getMechanicalConversions(), imageGroup1.getScanPath().getScanPasses());
    ScanPath clonedScanPath2 = new ScanPath(imageGroup2.getScanPath().getMechanicalConversions(), imageGroup2.getScanPath().getScanPasses());
    
    List<ScanPass> clonedScanPasses1 = new ArrayList<ScanPass>();
    for(ScanPass sp : imageGroup1.getScanPath().getScanPasses())
    {
      HomogeneousImageGroup clonedGroup = new HomogeneousImageGroup(testSubProgram.getScanPassImageGroup(sp).getUnsortedRegions(), false);
      ScanPass clonedSp = new ScanPass(sp);
      clonedScanPasses1.add(clonedSp);
      testSubProgram.addScanPassToImageGroup(clonedSp,clonedGroup);
    }
    clonedScanPath1.setScanPasses(clonedScanPasses1);
    
    List<ScanPass> clonedScanPasses2 = new ArrayList<ScanPass>();
    for(ScanPass sp : imageGroup2.getScanPath().getScanPasses())
    {
      HomogeneousImageGroup clonedGroup = new HomogeneousImageGroup(testSubProgram.getScanPassImageGroup(sp).getUnsortedRegions(), false);
      ScanPass clonedSp = new ScanPass(sp);
      clonedScanPasses2.add(clonedSp);
      testSubProgram.addScanPassToImageGroup(clonedSp,clonedGroup);
    }
    clonedScanPath2.setScanPasses(clonedScanPasses2);
    
    // #2 total travel time of scan path
    // checking scanpath time fors both scanpaths
    HomogeneousImageGroup clonedGroup1 = new HomogeneousImageGroup(imageGroup1.getUnsortedRegions(), false);
    HomogeneousImageGroup clonedGroup2 = new HomogeneousImageGroup(imageGroup2.getUnsortedRegions(), false);
    clonedGroup1.setScanPath(clonedScanPath1);
    clonedGroup2.setScanPath(clonedScanPath2);
    
    List <ScanPath> scanPathList = new ArrayList<ScanPath>();
    scanPathList.add(clonedScanPath1);
    scanPathList.add(clonedScanPath2);
    double beforeMergeTime = estimateVariableDivNScanPathExecutionTime(testSubProgram, scanPathList);
    
    HomogeneousImageGroup mergeGroup = mergeGroupsAndScanPaths(testSubProgram, clonedGroup1, clonedGroup2, false);
    scanPathList.clear();
    scanPathList.add(mergeGroup.getScanPath());
    double afterMergeTime = estimateVariableDivNScanPathExecutionTime(testSubProgram, scanPathList);
   
    // Perform cleanup before leaving
    for(ScanPass sp : mergeGroup.getScanPath().getScanPasses())
      testSubProgram.removeScanPassToImageGroup(sp);
    
    return beforeMergeTime >= afterMergeTime;
  }
  
  /**
   * @author Chnee Khang Wah
   * @param testSubProgram
   * @param imageGroup1
   * @param imageGroup2
   * @param updateData
   * @return
   * @throws XrayTesterException 
   */
  private HomogeneousImageGroup mergeGroupsAndScanPaths(TestSubProgram testSubProgram, 
                                                        HomogeneousImageGroup imageGroup1, 
                                                        HomogeneousImageGroup imageGroup2,
                                                        boolean updateData) throws XrayTesterException
  {
    ScanPath scanPath1 = imageGroup1.getScanPath();
    ScanPath scanPath2 = imageGroup2.getScanPath();
             
    double appropriateMaxSliceHeight = XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(testSubProgram.getTestProgram().getProject().getPanel().getThicknessInNanometers(), testSubProgram.getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers());
    
    Map<Integer,ScanPass> scanPassList = new HashMap<Integer,ScanPass>();
    
    if(updateData)
    {
      // Renumbering scanpasses id
      int id=0;
      for(ScanPass sp : scanPath1.getScanPasses())
      {
        sp.setId(id);
        id++;
      }
      
      for(ScanPass sp : scanPath2.getScanPasses())
      {
        sp.setId(id);
        id++;
      }
    }
    
    // Preparation for merging
    for(ScanPass sp : scanPath1.getScanPasses())
    {
      scanPassList.put(sp.getStartPointInNanoMeters().getXInNanometers(), sp);
    }
    
    List<ScanPass> toBeMergeSP = new ArrayList<ScanPass>();
    for(ScanPass sp : scanPath2.getScanPasses())
    {
      if(scanPassList.containsKey(sp.getStartPointInNanoMeters().getXInNanometers())==false)
      {
        scanPassList.put(sp.getStartPointInNanoMeters().getXInNanometers(), sp);
      }
      else
        toBeMergeSP.add(sp);
    }
    
    // Merge those duplicated scan passes
    if(toBeMergeSP.size()>0)
    {
      for(ScanPass sp : toBeMergeSP)
      {
        ScanPass baseSp = scanPassList.get(sp.getStartPointInNanoMeters().getXInNanometers());

        if(updateData)
        {
          // remove old scan pass from to be merge's regions
          for(ReconstructionRegion rg : testSubProgram.getScanPassImageGroup(sp).getUnsortedRegions())
            rg.removeScanPass(sp);

          commitReconstructionRegionsToScanPass(testSubProgram.getScanPassImageGroup(sp).getUnsortedRegions(), baseSp);
        }
        
        Collection<ReconstructionRegion> regions = new ArrayList<ReconstructionRegion>();
        regions.addAll(testSubProgram.getScanPassImageGroup(sp).getUnsortedRegions());
        HomogeneousImageGroup group = putRegionsIntoExistingScanPass(testSubProgram, baseSp, regions);
        
        if(updateData)
        {
          // remove 2 old image group from map
          testSubProgram.removeScanPassToImageGroup(sp);
          testSubProgram.removeScanPassToImageGroup(baseSp);
          // add a new image group into map
          testSubProgram.addScanPassToImageGroup(baseSp,group);
        }
      }
    }
    
    // Sort the scan pass sequence base on x location 
    List<ScanPass> mergedScanPasses = new ArrayList<ScanPass>(scanPassList.values());
    
    Comparator<ScanPass> comparator = new Comparator<ScanPass>()
    {
      public int compare(ScanPass sp1, ScanPass sp2)
      {
        Assert.expect(sp1 != null);
        Assert.expect(sp2 != null);
        int x1 = sp1.getStartPointInNanoMeters().getXInNanometers();
        int x2 = sp2.getStartPointInNanoMeters().getXInNanometers();
        if (x1 < x2)
          return -1;
        else if (x1 > x2)
          return 1;
        return 0;
      }
    };
    Collections.sort(mergedScanPasses, comparator);
    
    // Create new merged image group
    Collection<ReconstructionRegion> mergedRegions = new ArrayList<ReconstructionRegion>(imageGroup1.getUnsortedRegions());
    mergedRegions.addAll(imageGroup2.getUnsortedRegions());
    
    HomogeneousImageGroup mergedGroup = new HomogeneousImageGroup(mergedRegions, false);
    
    List<ProcessorStrip> processorStrips = createProcessorStripsForImageGroupReusingAreasDefinedInOriginalProcessorStrips(
        testSubProgram.getProcessorStripsForInspectionRegions(),
        mergedGroup);

    MechanicalConversions mechanicalConversions = new MechanicalConversions(
      processorStrips,
      mergedGroup.getAffineTransform(),
      Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
      ProcessorStrip.getOverlapNeededForLargestPossibleJointInNanometers(),
      appropriateMaxSliceHeight,
      Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers(testSubProgram.getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers())),
      _xRayCameras,
      mergedGroup.getNominalAreaToImage());
    
    // Renumbering scanpasses id
    int scanPassId = 0;
    if(updateData)
    {
      for (ScanPass sp : mergedScanPasses)
      {
        sp.setId(scanPassId);
        scanPassId++;
      }
    }
    
    ScanPath mergedScanPath = new ScanPath(mechanicalConversions, mergedScanPasses);
    mergedGroup.setScanPath(mergedScanPath);
    
    return mergedGroup;
  }
  
  private HomogeneousImageGroup putRegionsIntoExistingScanPass(TestSubProgram testSubProgram, 
                                                               ScanPass scanPass, 
                                                               Collection<ReconstructionRegion> regions) throws XrayTesterException
  {
    double appropriateMaxSliceHeight = XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(testSubProgram.getTestProgram().getProject().getPanel().getThicknessInNanometers(), testSubProgram.getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers());
              
    regions.addAll(testSubProgram.getScanPassImageGroup(scanPass).getUnsortedRegions());

    HomogeneousImageGroup group = new HomogeneousImageGroup(regions, false);

    List<ProcessorStrip> strips = createProcessorStripsForImageGroupReusingAreasDefinedInOriginalProcessorStrips(
      testSubProgram.getProcessorStripsForInspectionRegions(),
      group);
    
//    List<ProcessorStrip> strips = reCreateProcessorStripsForImageGroupReusingAreasDefinedInOriginalProcessorStrips(
//      testSubProgram.getProcessorStripsForInspectionRegions(),
//      group, ScanPassDirectionEnum.BIDIRECTION);
    
    MechanicalConversions scanPassMechanicalConversions = new MechanicalConversions(
    strips,
    group.getAffineTransform(),
    Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
    ProcessorStrip.getOverlapNeededForLargestPossibleJointInNanometers(),
    appropriateMaxSliceHeight,
    Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers()),
    _xRayCameras,
    group.getNominalAreaToImage());

    // Set start and stop positions as the basics.
    scanPass.getStartPointInNanoMeters().setYInNanometers(scanPassMechanicalConversions.stageTravelStart());
    scanPass.getEndPointInNanoMeters().setYInNanometers(scanPassMechanicalConversions.stageTravelEnd());
    scanPass.setScanPassMechanicalConversions(scanPassMechanicalConversions);
    
    return group;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  private void calculateScheduleOrder(TestProgram testProgram)
  {  
    setupSystemFiducialLocation();
    setupCameraLocation();
    
    List<XrayCameraAcquisitionParameters> parameters = testProgram.getCameraAcquisitionParameters();
            
    List<ScanPass> allScanPasses = testProgram.getScanPasses();
    
    for(TestSubProgram testSubProgram : testProgram.getFilteredScanPathTestSubPrograms())
    {
      MechanicalConversions mechanicalConversions = testSubProgram.getScanPasses().get(0).getScanPath().getMechanicalConversions();

      int focalRangeUpsliceInNanometers = (int)mechanicalConversions.getFocalRangeUpsliceInNanometers();
      int focalRangeDownsliceInNanometers = -(int)mechanicalConversions.getFocalRangeDownsliceInNanometers();
         
      AffineTransform manualAlignmentTranform = testSubProgram.getAggregateManualAlignmentTransform();
      
      boolean log = false;

      Collection<ReconstructionRegion> regions = new ArrayList<ReconstructionRegion>();
      regions.addAll(testSubProgram.getAlignmentRegions());
      regions.addAll(testSubProgram.getAllScanPathReconstructionRegions());
      
      for(ReconstructionRegion region : regions)
      { 
        if(log)
        {
          if(region.isAlignmentRegion())
            System.out.println("CalculateScheduleOrder::Region ID: "+region.getRegionId()+", refDeg: Alignment Region");
          else
            System.out.println("CalculateScheduleOrder::Region ID: "+region.getRegionId()+", refDeg: "+region.getFullDescription());
        }

        Set<Integer> scanPassIds = new TreeSet<Integer>();
        for(ScanPass sp : region.getScanPasses())
          scanPassIds.add(sp.getId());
        
        // Correcpond to Region Rectangle Before aligned in IRP
        PanelRectangle panelRet = region.getRegionRectangleRelativeToPanelInNanoMeters();
        // Correcpond to Region Rectangle After aligned in IRP
        SystemFiducialRectangle sysFidRet = MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(panelRet, manualAlignmentTranform);
        
        if(log)
        {
          System.out.println("CalculateScheduleOrder::Region ID: "+region.getRegionId()+", Panel Rectangle: "+panelRet);
          System.out.println("CalculateScheduleOrder::Region ID: "+region.getRegionId()+", System Fiducial Rectangle: "+sysFidRet); 
          System.out.println("CalculateScheduleOrder::Region ID: "+region.getRegionId()+", ScanPasses list: "+scanPassIds);
          System.out.println("CalculateScheduleOrder::Region ID: "+region.getRegionId()+", Integration Lvl: "+region.getSignalCompensation().toString());
        }

        ImageReconstructionEngineEnum ire = region.getReconstructionEngineId();
        Set<Integer> numCam = new TreeSet<Integer>(); 

        for(XrayCameraAcquisitionParameters cam : parameters)
        {
          int cameraId = cam.getCamera().getId();
          for(ProjectionSettings ps : cam.getProjectionSettings())
          {
            int scanPassNum = ps.getScanPassNumber();
            if(scanPassIds.contains(scanPassNum)==false)
              continue;
            
            for(ProjectionRegion pr : ps.getProjectionRegions())
            {
              if(pr.getProjectionDataDestinationId().equals(ire) == false)
                continue;
               
              ScanPass scanPass = allScanPasses.get(scanPassNum);
              // Convert sysFidRet to pixel recolution, and spatial space
              // Correcpond to topSide, BottomSide and BothSide Rectangle in IRP
              Rectangle2D topSideRet  = getCameraSystemCoordinatesInPixels(testSubProgram, sysFidRet, focalRangeUpsliceInNanometers, cam.getCamera(), scanPass);
              Rectangle2D botSideRet  = getCameraSystemCoordinatesInPixels(testSubProgram, sysFidRet, focalRangeDownsliceInNanometers, cam.getCamera(), scanPass);
              Rectangle2D bothSideRet = getCameraSystemCoordinatesInPixels(testSubProgram, sysFidRet, focalRangeUpsliceInNanometers, cam.getCamera(), scanPass);
              bothSideRet.add(botSideRet);
              
              // Correcpond to Projection Rectangle in IRP
              ImageRectangle imgRet = pr.getProjectionRegionRectangle(); 
              IntRectangle bothSideIntRet = new IntRectangle((int)bothSideRet.getMinX(),
                                                             (int)bothSideRet.getMinY(),
                                                             (int)bothSideRet.getWidth(),
                                                             (int)bothSideRet.getHeight());
                
              if(bothSideIntRet.intersects(imgRet) && bothSideIntRet.getMinY()>imgRet.getMinY() && bothSideIntRet.getMaxY()<imgRet.getMaxY())
              {
                numCam.add(cameraId);
                if(log)
                {
                  System.out.println("CalculateScheduleOrder::Region ID: "+region.getRegionId()+", TestSubProgram ID: "+testSubProgram.getId()+", IRE= "+ire.getId()+", ScanPass: "+scanPass.getId()+", Camera ID: "+cam.getCamera().getId()+", Projection Region Rectangle: "+imgRet);
                  System.out.println("CalculateScheduleOrder::Region ID: "+region.getRegionId()+", TestSubProgram ID: "+testSubProgram.getId()+", topSideRet: "+topSideRet.getMinX()+" "+topSideRet.getMinY()+" "+topSideRet.getWidth()+" "+topSideRet.getHeight());
                  System.out.println("CalculateScheduleOrder::Region ID: "+region.getRegionId()+", TestSubProgram ID: "+testSubProgram.getId()+", botSideRet: "+botSideRet.getMinX()+" "+botSideRet.getMinY()+" "+botSideRet.getWidth()+" "+botSideRet.getHeight());
                  System.out.println("CalculateScheduleOrder::Region ID: "+region.getRegionId()+", TestSubProgram ID: "+testSubProgram.getId()+", bothSideRet:"+bothSideRet.getMinX()+" "+bothSideRet.getMinY()+" "+bothSideRet.getWidth()+" "+bothSideRet.getHeight());
                }
              }
            }
          }
        }
        if(numCam.size()!=XrayCameraArray.getNumberOfCameras())
        {
          if(region.isAlignmentRegion())
            System.out.println("CalculateScheduleOrder::Region ID: "+region.getRegionId()+", TestSubProgram ID: "+testSubProgram.getId()+", Alignment Region, IRE: "+ire.getId()+", scanpasses: "+scanPassIds+" did not have projection from every camera, only have from "+numCam.size()+" cameras "+numCam);
          else
            System.out.println("CalculateScheduleOrder::Region ID: "+region.getRegionId()+", TestSubProgram ID: "+testSubProgram.getId()+", "+region.getFullDescription()+", IRE: "+ire.getId()+", scanpasses: "+scanPassIds+" did not have projection from every camera, only have from "+numCam.size()+" cameras "+numCam);
        }
      }
    }
  }
  
  private Rectangle2D getCameraSystemCoordinatesInPixels(TestSubProgram testSubProgram, 
                                                         final SystemFiducialRectangle rect, 
                                                         int deltaZ, 
                                                         AbstractXrayCamera camera,
                                                         final ScanPass scanPass)
  {
    int cameraId = camera.getId();
    Point2D coordinatesInPixels = null;
    AbstractXrayCamera xRayCamera = XrayCameraArray.getInstance().getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
    
    double magnificationAtReferencePlane = 0.0;
    double xPixelPitchInNanometersAtReferencePlane = 0.0;
    double inverseXPixelPitchInNanometersAtReferencePlane = 0.0;
    double inverseYPixelPitchInNanometers = 0.0;
    double inverseZDistanceFromCameraArrayToXRaySource = 0.0;
    
    float stageXPosition = 0;
    double colInProjection = 0.0;
    int xInPixels = 0;
    
    float stageYPosition = 0;
    double rowInProjection = 0.0;
    double tempY = 0.0;
    int yInPixels = 0;
    
    if(testSubProgram.isHighMagnification())
    {
      magnificationAtReferencePlane = Config.getInstance().getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION);
      xPixelPitchInNanometersAtReferencePlane = xRayCamera.getSensorColumnPitchInNanometers();
      xPixelPitchInNanometersAtReferencePlane /= magnificationAtReferencePlane;
      
      inverseXPixelPitchInNanometersAtReferencePlane = 1.0 / xPixelPitchInNanometersAtReferencePlane;
      inverseYPixelPitchInNanometers = 1.0 / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
      inverseZDistanceFromCameraArrayToXRaySource = 1.0 / XrayCameraArray.getZHeightFromCameraArrayTo2ndXraySourceInNanometers();
      
      // Stage x position while the projection was acquired
      stageXPosition = scanPass.getStartPointInNanoMeters().getXInNanometers();
      colInProjection = rect.getCenterX();
      colInProjection += stageXPosition;
      colInProjection -= _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.get(cameraId).getX();
      colInProjection *= inverseXPixelPitchInNanometersAtReferencePlane;
      colInProjection += _systemFiducialXYLocationInPixelsHighMagMap.get(cameraId).getX();
      
      xInPixels = irpRound(applyRadialOffsetInX(testSubProgram, colInProjection, deltaZ, cameraId));
      
      // Stage y position when the 1st row of the projection was acquired
      stageYPosition = scanPass.getMechanicalConversions().yLocationForReconstructionOnCamera(camera);
      //Note the minus sign is due to the fact that the coordinates are with respect to the upper left
      //corner of the (projection) image (image coordinates)
      rowInProjection = _systemFiducialXYLocationInPixelsHighMagMap.get(cameraId).getY();
      
      tempY = _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.get(cameraId).getY();
      tempY -= stageYPosition;
      tempY -= rect.getCenterY();
      tempY *= inverseYPixelPitchInNanometers;
      rowInProjection += tempY;

      tempY = deltaZ;
      tempY *= inverseZDistanceFromCameraArrayToXRaySource;
      tempY *= _xyPosOfCamerasInXraySpotCoordPixelsHighMagMap.get(cameraId).getY();
      rowInProjection -= tempY;
      
      yInPixels = irpRound(rowInProjection);
    }
    else
    {
      magnificationAtReferencePlane = Config.getInstance().getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION);
      xPixelPitchInNanometersAtReferencePlane = xRayCamera.getSensorColumnPitchInNanometers();
      xPixelPitchInNanometersAtReferencePlane /= magnificationAtReferencePlane;
      
      inverseXPixelPitchInNanometersAtReferencePlane = 1.0 / xPixelPitchInNanometersAtReferencePlane;
      inverseYPixelPitchInNanometers = 1.0 / MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
      inverseZDistanceFromCameraArrayToXRaySource = 1.0 / XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers();
      
      // Stage x position while the projection was acquired
      stageXPosition = scanPass.getStartPointInNanoMeters().getXInNanometers();
      colInProjection = rect.getCenterX();
      colInProjection += stageXPosition;
      colInProjection -= _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.get(cameraId).getX();
      colInProjection *= inverseXPixelPitchInNanometersAtReferencePlane;
      colInProjection += _systemFiducialXYLocationInPixelsMap.get(cameraId).getX();
      
      xInPixels = irpRound(applyRadialOffsetInX(testSubProgram, colInProjection, deltaZ, cameraId));
      
      // Stage y position when the 1st row of the projection was acquired
      stageYPosition = scanPass.getMechanicalConversions().yLocationForReconstructionOnCamera(camera);
      //Note the minus sign is due to the fact that the coordinates are with respect to the upper left
      //corner of the (projection) image (image coordinates)
      rowInProjection = _systemFiducialXYLocationInPixelsMap.get(cameraId).getY();
      
      tempY = _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.get(cameraId).getY();
      tempY -= stageYPosition;
      tempY -= rect.getCenterY();
      tempY *= inverseYPixelPitchInNanometers;
      rowInProjection += tempY;

      tempY = deltaZ;
      tempY *= inverseZDistanceFromCameraArrayToXRaySource;
      tempY *= _xyPosOfCamerasInXraySpotCoordPixelsMap.get(cameraId).getY();
      rowInProjection -= tempY;
      
      yInPixels = irpRound(rowInProjection);
    }
            
    coordinatesInPixels = new Point2D.Double(xInPixels, yInPixels);
    
    double width  = 1 + getProjectedWidthInPixels(testSubProgram, rect.getWidth(), deltaZ);
    double length = 1 + getProjectedLengthInPixels(testSubProgram, rect.getHeight());
    
    return new Rectangle2D.Double((double)(irpRound(coordinatesInPixels.getX() - 0.5 * width)),
                                  (double)(irpRound(coordinatesInPixels.getY() - 0.5 * length)), 
                                   width, length);
  }
  
  /**
   * @author Chnee Khang Wah
   */
  private double applyRadialOffsetInX(TestSubProgram testSubProgram,
                                      double xInCameraSystem0,
                                      double deltaZ,
                                      int cameraId)
  {
    if(testSubProgram.isHighMagnification())
    {
      double inverseZDistanceFromCameraArrayToXRaySource = 1.0 / XrayCameraArray.getZHeightFromCameraArrayTo2ndXraySourceInNanometers();
      double magnificationAtReferencePlane = Config.getInstance().getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION);
      
      double dz = deltaZ * inverseZDistanceFromCameraArrayToXRaySource;
      return ((xInCameraSystem0 + _xyPosOfCamerasInXraySpotCoordPixelsHighMagMap.get(cameraId).getX()) /
              (1 - (magnificationAtReferencePlane * dz)) - _xyPosOfCamerasInXraySpotCoordPixelsHighMagMap.get(cameraId).getX());
    }
    else
    {
      double inverseZDistanceFromCameraArrayToXRaySource = 1.0 / XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers();
      double magnificationAtReferencePlane = Config.getInstance().getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION);
      double dz = deltaZ * inverseZDistanceFromCameraArrayToXRaySource;
      return ((xInCameraSystem0 + _xyPosOfCamerasInXraySpotCoordPixelsMap.get(cameraId).getX()) /
              (1 - (magnificationAtReferencePlane * dz)) - _xyPosOfCamerasInXraySpotCoordPixelsMap.get(cameraId).getX());
    }
  }
  
  /**
   * @author Chnee Khang Wah
   */
  private int getProjectedWidthInPixels(TestSubProgram testSubProgram, 
                                        double widthInNanometer,
                                        double deltaZInNanometer)
  {
    double magnificationAtReferencePlane = Config.getInstance().getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION);
    double xPixelPitchInNanometersAtReferencePlane = XrayCameraArray.getInstance().getCamera(XrayCameraIdEnum.XRAY_CAMERA_0).getSensorColumnPitchInNanometers();
    xPixelPitchInNanometersAtReferencePlane /= magnificationAtReferencePlane;
    
    double inverseXPixelPitchInNanometersAtReferencePlane = 1.0 / xPixelPitchInNanometersAtReferencePlane;
    double inverseZDistanceFromCameraArrayToXRaySource = 1.0 / XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers();
    
    if(testSubProgram.isHighMagnification())
    {
      magnificationAtReferencePlane = Config.getInstance().getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION);
      xPixelPitchInNanometersAtReferencePlane = XrayCameraArray.getInstance().getCamera(XrayCameraIdEnum.XRAY_CAMERA_0).getSensorColumnPitchInNanometers();
      xPixelPitchInNanometersAtReferencePlane /= magnificationAtReferencePlane;
    
      inverseXPixelPitchInNanometersAtReferencePlane = 1.0 / xPixelPitchInNanometersAtReferencePlane;
      inverseZDistanceFromCameraArrayToXRaySource = 1.0 / XrayCameraArray.getZHeightFromCameraArrayTo2ndXraySourceInNanometers();
    }
    
    if(deltaZInNanometer==0)
      return irpRound(widthInNanometer * inverseXPixelPitchInNanometersAtReferencePlane);
    else
      return irpRound(widthInNanometer * inverseXPixelPitchInNanometersAtReferencePlane /
              (1 - (magnificationAtReferencePlane * deltaZInNanometer * inverseZDistanceFromCameraArrayToXRaySource)));
  }
  
  /**
   * @author Chnee Khang Wah
   */
  private int getProjectedLengthInPixels(TestSubProgram testSubProgram, 
                                         double lengthInNanometer)
  {
    double inverseYPixelPitchInNanometers = 1.0 / MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
    
    if(testSubProgram.isHighMagnification())
      inverseYPixelPitchInNanometers = 1.0 / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
    
    return irpRound(lengthInNanometer * inverseYPixelPitchInNanometers);
  }
  
  /**
   * @author Chnee Khang Wah
   */
  private void setupSystemFiducialLocation()
  {
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.clear();
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.clear();
    _systemFiducialXYLocationInPixelsMap.clear();
    _systemFiducialXYLocationInPixelsHighMagMap.clear();
    
    int cameraID, x, y = 0;
    
    // Camera 0
    cameraID = 0;
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_ROW);
    _systemFiducialXYLocationInPixelsMap.put(cameraID, new Point2D.Double(x,y)); 
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_ROW);
    _systemFiducialXYLocationInPixelsHighMagMap.put(cameraID, new Point2D.Double(x,y));
    
    // Camera 1
    cameraID = 1;
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_ROW);
    _systemFiducialXYLocationInPixelsMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_ROW);
    _systemFiducialXYLocationInPixelsHighMagMap.put(cameraID, new Point2D.Double(x,y));
    
    // Camera 2
    cameraID = 2;
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_ROW);
    _systemFiducialXYLocationInPixelsMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_ROW);
    _systemFiducialXYLocationInPixelsHighMagMap.put(cameraID, new Point2D.Double(x,y));
    
    // Camera 3
    cameraID = 3;
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_ROW);
    _systemFiducialXYLocationInPixelsMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_ROW);
    _systemFiducialXYLocationInPixelsHighMagMap.put(cameraID, new Point2D.Double(x,y));
    
    // Camera 4
    cameraID = 4;
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_ROW);
    _systemFiducialXYLocationInPixelsMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_ROW);
    _systemFiducialXYLocationInPixelsHighMagMap.put(cameraID, new Point2D.Double(x,y));
    
    // Camera 5
    cameraID = 5;
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_ROW);
    _systemFiducialXYLocationInPixelsMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_ROW);
    _systemFiducialXYLocationInPixelsHighMagMap.put(cameraID, new Point2D.Double(x,y));
    
    // Camera 6
    cameraID = 6;
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_ROW);
    _systemFiducialXYLocationInPixelsMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_ROW);
    _systemFiducialXYLocationInPixelsHighMagMap.put(cameraID, new Point2D.Double(x,y));
    
    // Camera 7
    cameraID = 7;
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_ROW);
    _systemFiducialXYLocationInPixelsMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_ROW);
    _systemFiducialXYLocationInPixelsHighMagMap.put(cameraID, new Point2D.Double(x,y));
    
    // Camera 8
    cameraID = 8;
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_ROW);
    _systemFiducialXYLocationInPixelsMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_ROW);
    _systemFiducialXYLocationInPixelsHighMagMap.put(cameraID, new Point2D.Double(x,y));
    
    // Camera 9
    cameraID = 9;
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_ROW);
    _systemFiducialXYLocationInPixelsMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_ROW);
    _systemFiducialXYLocationInPixelsHighMagMap.put(cameraID, new Point2D.Double(x,y));
    
    // Camera 10
    cameraID = 10;
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_ROW);
    _systemFiducialXYLocationInPixelsMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_ROW);
    _systemFiducialXYLocationInPixelsHighMagMap.put(cameraID, new Point2D.Double(x,y));
    
    // Camera 11
    cameraID = 11;
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_ROW);
    _systemFiducialXYLocationInPixelsMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_ROW);
    _systemFiducialXYLocationInPixelsHighMagMap.put(cameraID, new Point2D.Double(x,y));
    
    // Camera 12
    cameraID = 12;
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_ROW);
    _systemFiducialXYLocationInPixelsMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_ROW);
    _systemFiducialXYLocationInPixelsHighMagMap.put(cameraID, new Point2D.Double(x,y));
    
    // Camera 13
    cameraID = 13;
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS);
    _stageXYPosForSystemFiducialProjectionInNmMachineCoordHighMagMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_ROW);
    _systemFiducialXYLocationInPixelsMap.put(cameraID, new Point2D.Double(x,y));
    x = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_COLUMN);
    y = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_ROW);
    _systemFiducialXYLocationInPixelsHighMagMap.put(cameraID, new Point2D.Double(x,y));
  }
  
  /**
   * @author Chnee Khang Wah
   */
  private void setupCameraLocation()
  {
    _xyPosOfCamerasInXraySpotCoordPixelsMap.clear();
    _xyPosOfCamerasInXraySpotCoordPixelsHighMagMap.clear();
  
    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    
    int xSpot = Config.getInstance().getIntValue(HardwareCalibEnum.XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
    int ySpot = Config.getInstance().getIntValue(HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
    
    int xSpotForHigMag = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
    int ySpotForHighMag = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
    
    double inverseYPixelPitchInNanometers = 1.0 / MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
    double inverseYPixelPitchInNanometersHighMag = 1.0 / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
    
    for (AbstractXrayCamera xRayCamera : XrayCameraArray.getCameras())
    {
      MachineRectangle machineRet = xRayCameraArray.getCameraSensorRectangle(xRayCamera);
      MachineRectangle machineRetHighMag = xRayCameraArray.getCameraSensorRectangleForHighMag(xRayCamera);
      
      Point2D cameraOrigin = new Point2D.Double(machineRet.getMinX()-xSpot, machineRet.getCenterY()-ySpot);
      Point2D cameraOriginHighMag = new Point2D.Double(machineRetHighMag.getMinX()-xSpotForHigMag, machineRetHighMag.getCenterY()-ySpotForHighMag); 
      
      int xpos = irpRound(cameraOrigin.getX() / xRayCamera.getSensorColumnPitchInNanometers());
      int ypos = irpRound(cameraOrigin.getY() * inverseYPixelPitchInNanometers);
      int xposHighMag = irpRound(cameraOriginHighMag.getX() / xRayCamera.getSensorColumnPitchInNanometers());
      int yposHighMag = irpRound(cameraOriginHighMag.getY() * inverseYPixelPitchInNanometersHighMag);
      
      _xyPosOfCamerasInXraySpotCoordPixelsMap.put(xRayCamera.getId(), new Point2D.Double(xpos, ypos));
      _xyPosOfCamerasInXraySpotCoordPixelsHighMagMap.put(xRayCamera.getId(), new Point2D.Double(xposHighMag, yposHighMag));
    }
  }
  
  /**
   * @author Chnee Khang Wah
   */
  private int irpRound(double value)
  {
    if(value<0)
      return (int)(value-0.5);
    else
      return (int)(value+0.5);
  }
  
  /**
   * @author Siew Yeng - XCR-2140 - DRO
   */
  /*
  private void divideRegionsWithDynamicRangeOptimization(
      Collection<ReconstructionRegion> allInspectionRegions,
      Map<DynamicRangeOptimizationLevelEnum,Collection<ReconstructionRegion>> regionsWithDynamicRangeOptimizationList)
      throws XrayTesterException
  {    
    
    for (ReconstructionRegion reconstructionRegion : allInspectionRegions)
    {
      if(reconstructionRegion.getDynamicRangeOptimizationLevel() == DynamicRangeOptimizationLevelEnum.ONE)
        continue;
      
      Collection<ReconstructionRegion> list = regionsWithDynamicRangeOptimizationList.get(reconstructionRegion.getDynamicRangeOptimizationLevel());
      if (list == null)
      {
        list = new ArrayList<ReconstructionRegion>();
        regionsWithDynamicRangeOptimizationList.put(reconstructionRegion.getDynamicRangeOptimizationLevel(), list);
      }
      list.add(reconstructionRegion);
    }
  }
  */
  
  /**
   * @author Swee Yee, Wong
   */
  public void updateReconstructionRegionStartPortNumber(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);
    List<XrayCameraAcquisitionParameters> xrayCameraAcquisitionParameters = testProgram.getCameraAcquisitionParameters();

    for (XrayCameraAcquisitionParameters cameraParameters : xrayCameraAcquisitionParameters)
    {
      for (ProjectionSettings settings : cameraParameters.getProjectionSettings())
      {
        List<ProjectionRegion> regions = settings.getProjectionRegions();
        if (regions.size() > 0)
        {
          for (ProjectionRegion region : regions)
          {
            region.setStartPortNumber(_idToImageReconstructionEngineMap.get(region.getProjectionDataDestinationId()).getStartPortNumber());
          }
        }
      }
    }
  }
  
  /**
   * @author Siew Yeng
   */
  private String generateIsolatedPSHRegionCSVFile(Project project)
  {
    String fileName = FileName.getProjectIsolatedPSHRegionCSVFullPath(project.getName());

    try
    {
      if (FileUtilAxi.exists(fileName))
      {
        FileUtilAxi.delete(fileName);
      }
      
      ProjectWriter.getInstance().writeIsolatedRegionList(project);
    }
    catch (DatastoreException e)
    {
      System.out.println("RT_PSH:: " + e.getMessage());
    }
    
    return fileName;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private boolean doesRegionPassFilters(ReconstructionRegion region)
  {
    Assert.expect(region != null);
    
    TestProgram testProgram = region.getTestSubProgram().getTestProgram();
    boolean isPanelBasedAlignment = testProgram.getProject().getPanel().getPanelSettings().isPanelBasedAlignment();
    
    if ((isPanelBasedAlignment && testProgram.doesRegionPassFilters(region)) || 
         isPanelBasedAlignment == false)    
    {
      return true;
    }
    return false;
  }
}
