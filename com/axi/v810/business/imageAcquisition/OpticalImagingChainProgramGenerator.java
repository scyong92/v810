package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalImagingChainProgramGenerator 
{
  public static final int MAXIMUM_PANEL_THICKNESS_FOR_OPTICAL_PROFILE_1_IN_NANOMETERS = (int)MathUtil.convertMillimetersToNanometers(3.00);

  private static int _id = 0;   
  private SurfaceMapping _surfaceMapping = SurfaceMapping.getInstance();

  /**
   * @author Cheah Lee Herng 
   */
  public OpticalImagingChainProgramGenerator()
  {
    // Do nothing
  }
    
  /**
   * @author Cheah Lee Herng
   * @author Kok Chun, Tan
   */
  public void generateOpticalCameraRectangleScanPathForTestProgram(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    
    String opticalRegionName = null;
    
    // Get panel thickness. This is important because this will determine Optica Camera Profile to be used.
    int panelThicknessInNanometers = testProgram.getProject().getPanel().getThicknessInNanometers();
    OpticalCameraIdEnum cameraId = OpticalCameraIdEnum.OPTICAL_CAMERA_1;
    Map<TestSubProgram, List<OpticalCameraRectangle>> opticalCameraRectangleMap = _surfaceMapping.getOpticalCameraRectangleBasedOnTestSubProgram(testProgram.getAllOpticalCameraRectangles(), testProgram);
    for (Map.Entry<TestSubProgram, java.util.List<OpticalCameraRectangle>> entry : opticalCameraRectangleMap.entrySet())
    {
      entry.getKey().clearOpticalPointToPointScans();
      for (OpticalCameraRectangle opticalCameraRectangle : entry.getValue())
      {
        // Kok Chun, Tan - XCR2490 - 24/2/2014
        // Check the optical camera used is enable or not. If not enable, skip the rectangle.
        if (_surfaceMapping.isOpticalCameraEnable(opticalCameraRectangle) == false)
        {
          continue;
        }
        
        opticalRegionName = null;
        for (OpticalRegion opticalRegion : testProgram.getAllOpticalRegion())
        {
          if (testProgram.getProject().getPanel().getPanelSettings().isPanelBasedAlignment())
          {
            if (opticalRegion.getAllOpticalCameraRectangles().contains(opticalCameraRectangle))
            {
              opticalRegionName = opticalRegion.getName();
              break;
            }
          }
          else
          {
            if (opticalRegion.getOpticalCameraRectangles(entry.getKey().getBoard()).contains(opticalCameraRectangle))
            {
              opticalRegionName = opticalRegion.getName();
              break;
            }
          }
        }

        // Added by Lee Herng, Cheah 06 Aug 2014
        // Filter OpticalCameraRectangle that contains within TestSubProgram imageable region and is inspectable                    
        if (opticalCameraRectangle.getInspectableBool() == true)
        {
          PanelRectangle panelRectangle = opticalCameraRectangle.getRegion();
          PanelCoordinate panelCoordinate = new PanelCoordinate();
          panelCoordinate.setLocation(panelRectangle.getCenterX(), panelRectangle.getCenterY());

          String pointCoordinateName = panelCoordinate.getX() + "_" + panelCoordinate.getY();

          PanelSettings panelSettings = testProgram.getProject().getPanel().getPanelSettings();
          PanelRectangle bottomPanelRectangle = panelSettings.getBottomSectionOfOpticalLongPanel();

          int opticalFiducialStageCoordinateLocationXInNanometers = -1;
          int opticalFiducialStageCoordinateLocationYInNanometers = -1;
          
          if (bottomPanelRectangle.contains(panelCoordinate.getX(), panelCoordinate.getY()))
          {
            cameraId = OpticalCameraIdEnum.OPTICAL_CAMERA_1;
            
            opticalFiducialStageCoordinateLocationXInNanometers = PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers();
            opticalFiducialStageCoordinateLocationYInNanometers = PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers();
          }
          else
          {
            cameraId = OpticalCameraIdEnum.OPTICAL_CAMERA_2;
            
            opticalFiducialStageCoordinateLocationXInNanometers = PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers();
            opticalFiducialStageCoordinateLocationYInNanometers = PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers();
          }
          
          StagePosition stagePosition = OpticalMechanicalConversions.convertPanelCoordinateToStageCoordinate(entry.getKey(),
                                                                    panelCoordinate,
                                                                    entry.getKey().getAggregateRuntimeAlignmentTransform(),
                                                                    entry.getKey().getPanelLocationInSystem(),
                                                                    PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationXInNanometers(),
                                                                    PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationYInNanometers(),
                                                                    opticalFiducialStageCoordinateLocationXInNanometers,
                                                                    opticalFiducialStageCoordinateLocationYInNanometers,
                                                                    new BooleanRef());

          OpticalPointToPointScan opticalPointToPointScan = new OpticalPointToPointScan();
          opticalPointToPointScan.setId(_id++);
          opticalPointToPointScan.setOpticalCameraIdEnum(cameraId);
          opticalPointToPointScan.setRegionPositionName(opticalRegionName);
          opticalPointToPointScan.setOpticalCameraRectangleName(opticalCameraRectangle.getName());
          opticalPointToPointScan.setPointPositionName(pointCoordinateName);
          opticalPointToPointScan.setPointToPointStagePositionInNanoMeters(stagePosition);
          opticalPointToPointScan.addPointPanelPositionName(entry.getKey(),
                                                            opticalCameraRectangle.getPanelCoordinateList(), 
                                                            entry.getKey().getAggregateRuntimeAlignmentTransform(),
                                                            PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationXInNanometers(),
                                                            PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationYInNanometers(),
                                                            opticalFiducialStageCoordinateLocationXInNanometers,
                                                            opticalFiducialStageCoordinateLocationYInNanometers);
          opticalPointToPointScan.setRoiWidth(PspHardwareEngine.getInstance().getImageRoiWidth());
          opticalPointToPointScan.setRoiHeight(PspHardwareEngine.getInstance().getImageRoiHeight());

          if (panelThicknessInNanometers <= MAXIMUM_PANEL_THICKNESS_FOR_OPTICAL_PROFILE_1_IN_NANOMETERS)
          {
            opticalPointToPointScan.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_1);
          }
          else
          {
            opticalPointToPointScan.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_2);
          }

          entry.getKey().addOpticalPointToPointScan(opticalPointToPointScan);
        }
      }
    }
  }
    
  /**
   * @author Cheah Lee Herng
   */
  public void generateAlignmentOpticalCameraRectangleScanPathForTestProgram(TestProgram testProgram) throws XrayTesterException
  {        
    Assert.expect(testProgram != null);
        
    // Get panel thickness. This is important because this will determine Optica Camera Profile to be used.
    int panelThicknessInNanometers = testProgram.getProject().getPanel().getThicknessInNanometers();

    for (TestSubProgram testSubProgram : testProgram.getAllInspectableAlignmentSurfaceMapTestSubPrograms())
    {
      testSubProgram.clearAlignmentOpticalPointToPointScans();
      testSubProgram.clearEstimatedBoardThicknessInNanometers();

      BooleanRef useCameraOne = new BooleanRef(false);
      OpticalCameraIdEnum cameraId = OpticalCameraIdEnum.OPTICAL_CAMERA_1;

      for (OpticalRegion opticalRegion : testSubProgram.getAlignmentOpticalRegions())
      {
        // User can actually enable/disable the usage of PSP Alignment OpticalRegion
        // for board thickness estimation purpose. We handle this scenario here.
        if (opticalRegion.isSetToUsePspForAlignment())
        {
          for (OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getAllOpticalCameraRectangles())
          {
            // Added by Lee Herng, Cheah 06 Aug 2014
            // Filter OpticalCameraRectangle that contains within TestSubProgram imageable region and is inspectable                    
            if (opticalCameraRectangle.getInspectableBool() == true && 
                opticalCameraRectangle.getRegion().intersects(testSubProgram.getImageableRegionInNanoMeters()))
            {
              PanelRectangle panelRectangle = opticalCameraRectangle.getRegion();
              PanelCoordinate panelCoordinate = new PanelCoordinate();
              panelCoordinate.setLocation(panelRectangle.getCenterX(), panelRectangle.getCenterY());

              String pointCoordinateName = panelCoordinate.getX() + "_" + panelCoordinate.getY();

              PanelSettings panelSettings = testProgram.getProject().getPanel().getPanelSettings();
              PanelRectangle bottomPanelRectangle = panelSettings.getBottomSectionOfOpticalLongPanel();
              
              int opticalFiducialStageCoordinateLocationXInNanometers = -1;
              int opticalFiducialStageCoordinateLocationYInNanometers = -1;

              if (bottomPanelRectangle.contains(panelCoordinate.getX(), panelCoordinate.getY()))
              {
                cameraId = OpticalCameraIdEnum.OPTICAL_CAMERA_1;

                opticalFiducialStageCoordinateLocationXInNanometers = PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers();
                opticalFiducialStageCoordinateLocationYInNanometers = PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers();
              }
              else
              {
                cameraId = OpticalCameraIdEnum.OPTICAL_CAMERA_2;

                opticalFiducialStageCoordinateLocationXInNanometers = PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers();
                opticalFiducialStageCoordinateLocationYInNanometers = PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers();
              }

              StagePosition stagePosition = OpticalMechanicalConversions.convertPanelCoordinateToStageCoordinate(testSubProgram,
                      panelCoordinate,
                      testSubProgram.getAggregateRuntimeAlignmentTransform(),
                      testSubProgram.getPanelLocationInSystem(),
                      PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationXInNanometers(),
                      PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationYInNanometers(),
                      opticalFiducialStageCoordinateLocationXInNanometers,
                      opticalFiducialStageCoordinateLocationYInNanometers,
                      useCameraOne);

              OpticalPointToPointScan opticalPointToPointScan = new OpticalPointToPointScan();
              opticalPointToPointScan.setId(_id++);
              opticalPointToPointScan.setOpticalCameraIdEnum(cameraId);
              opticalPointToPointScan.setRegionPositionName(opticalRegion.getName());
              opticalPointToPointScan.setOpticalCameraRectangleName(opticalCameraRectangle.getName());
              opticalPointToPointScan.setAlignmentGroupName(opticalRegion.getAlignmentGroupName());
              opticalPointToPointScan.setPointPositionName(pointCoordinateName);
              opticalPointToPointScan.setPointToPointStagePositionInNanoMeters(stagePosition);
              opticalPointToPointScan.addPointPanelPositionName(testSubProgram,
                                                                opticalCameraRectangle.getPanelCoordinateList(), 
                                                                testSubProgram.getAggregateRuntimeAlignmentTransform(),
                                                                PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationXInNanometers(),
                                                                PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationYInNanometers(),
                                                                opticalFiducialStageCoordinateLocationXInNanometers,
                                                                opticalFiducialStageCoordinateLocationYInNanometers);             
              opticalPointToPointScan.setRoiWidth(PspHardwareEngine.getInstance().getImageRoiWidth());
              opticalPointToPointScan.setRoiHeight(PspHardwareEngine.getInstance().getImageRoiHeight());

              if (panelThicknessInNanometers <= MAXIMUM_PANEL_THICKNESS_FOR_OPTICAL_PROFILE_1_IN_NANOMETERS)
              {
                opticalPointToPointScan.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_1);
              }
              else
              {
                opticalPointToPointScan.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_2);
              }

              testSubProgram.addAlignmentOpticalPointToPointScan(opticalPointToPointScan);
            }
          }
        }
      }
    }
  }
}
