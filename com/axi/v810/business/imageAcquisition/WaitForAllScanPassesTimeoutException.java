package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class WaitForAllScanPassesTimeoutException extends XrayTesterException
{
  public WaitForAllScanPassesTimeoutException()
  {
    super(new LocalizedString("HW_IMAGE_ACQUISITION_ENGINE_WAIT_FOR_ALL_SCANPASSES_TIMEOUT_EXCEPTION_KEY",
          new Object[] {}));
  }
}
