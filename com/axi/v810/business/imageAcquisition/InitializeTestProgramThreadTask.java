package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class InitializeTestProgramThreadTask extends ThreadTask<Object>
{
  private ImageReconstructionEngine _imageReconstructionEngine;
  private ImageAcquisitionModeEnum  _acquisitionMode;
  private boolean                   _irpMustParticipateDuringCollection;
  private TestProgram               _testProgram;
  private boolean                   _transmitAlignmentRegions;
  private boolean                   _cancelled;

  /**
   * @author Roy Williams
   */
  public InitializeTestProgramThreadTask(ImageReconstructionEngine imageReconstructionEngine,
                                         ImageAcquisitionModeEnum acquisitionMode,
                                         boolean irpMustParticipateDuringNextCollection,
                                         TestProgram testProgram,
                                         boolean transmitAlignmentRegions)
  {
    super("InitializeTestProgramThreadTask:" + imageReconstructionEngine.getId());

    Assert.expect(imageReconstructionEngine != null);
    Assert.expect(acquisitionMode != null);
    Assert.expect(testProgram != null);

    _imageReconstructionEngine          = imageReconstructionEngine;
    _acquisitionMode                    = acquisitionMode;
    _irpMustParticipateDuringCollection = irpMustParticipateDuringNextCollection;
    _testProgram                        = testProgram;
    _transmitAlignmentRegions           = transmitAlignmentRegions;
    _cancelled                          = false;
  }

  /**
   * @author Roy Williams
   */
  public Object executeTask() throws Exception
  {
    // Between each step check to see if we should continue or cancel.
    if (_cancelled)
      return null;

    // Tell the IRE to initialize, it will do the right thing Abort or Reset.
    // And then, it will download the constants.
    _imageReconstructionEngine.initializeSystem();

    // Between each step check to see if we should continue or cancel.
    if (_cancelled)
      return null;

    if (_irpMustParticipateDuringCollection)
    {
      _imageReconstructionEngine.initializeTestProgram(_testProgram,
                                                       _acquisitionMode,
                                                       _transmitAlignmentRegions);
    }

    return null;
  }

  /**
   * @author Roy Williams
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Roy Williams
   */
  protected void cancel() throws XrayTesterException
  {
    _cancelled = true;
  }
}
