package com.axi.v810.business.imageAcquisition;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Reid Hayhow
 */
public class ProjectedImagesProducerWithSyntheticTriggers extends ProjectedImagesProducer
{
  /**
   * @author Roy Williams
   */
  ProjectedImagesProducerWithSyntheticTriggers()
  {
    super();
  }

  /**
   * Method to run a scan pass with synthetic triggers. It kicks off the consumers
   * and starts synthetic triggers. It does NOT turn triggers off. That is handled
   * later to guarantee synchronization.
   *
   * @author Reid Hayhow
   */
  protected void runNextScanPass(int scanPassNumber) throws XrayTesterException
  {
    Assert.expect(_panelPositioner != null);

    Assert.expect(scanPassNumber >= 0);

    waitForCamerasToBecomeIdleThenTurnOnSyntheticTriggers();

    // Start all of the ProjectionConsumers.
    for (ProjectionConsumer projectionConsumer : getProjectionConsumers())
    {
      projectionConsumer.startScanPass(scanPassNumber);
    }
  }

  /**
   * Method to generate the scan path. This is very different from what other
   * Projection Producers need/want. It wants to capture an image from all cameras
   * at the same time, with no stage movement
   *
   * @author Reid Hayhow
   */
  protected List<ProjectionScanPass> generateScanPath(List<ProjectionRequestThreadTask> projectionRequests,
                                                      boolean optimizeProjectionRequestOrder,
                                                      Map<AbstractXrayCamera, XrayCameraAcquisitionParameters> cameraAcquisitionParameters) throws XrayTesterException
  {
    Assert.expect(projectionRequests != null);
    Assert.expect(cameraAcquisitionParameters != null);

    _currentScanPassId = 0;

    //Create the list to hold all of the scan pass entries
    _scanPassEntries = new ArrayList<ProjectionRequestThreadTask>();

    //Create the list to hold the only scan path
    List<ProjectionScanPass> scanPath = new ArrayList<ProjectionScanPass>();

    // Create a list of processorStrips to indicate the number of IRPs to use.
    List<ProcessorStrip> processorStrips = new ArrayList<ProcessorStrip>();
    ProcessorStrip processorStrip = new ProcessorStrip(0);
    processorStrip.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
    processorStrips.add(processorStrip);

    //Completely bogus push-ups needed to get the stage mutable fooled
    _mechanicalConversions = new MechanicalConversions(processorStrips, // numberOfIRPs = lie because these images are coming back through XrayCameras
                                                       new AffineTransform(),
                                                       0, // alignmentUncertainty => none because we are always scanning system fiducical
                                                       0, // largest undivisible pad.   None for System Fiducial.
                                                       0, // FocalRangeUpslice on calibration measurements is 0!
                                                       0, // FocalRangeDownslice on calibration measurements is 0!
                                                       getCameraList(projectionRequests),
                                                       checkSystemFiducialRectangle(projectionRequests));

    int startYPosition = _mechanicalConversions.stageTravelStart();
    int endYPosition = _mechanicalConversions.stageTravelEnd();

    //Set up the stage mutables for the start and end
    StagePositionMutable startPosition = new StagePositionMutable();
    startPosition.setXInNanometers(0);
    startPosition.setYInNanometers(startYPosition);

    StagePositionMutable endPostion = new StagePositionMutable();
    endPostion.setXInNanometers(0);
    endPostion.setYInNanometers(endYPosition);

    ProjectionScanPass scanPass = new ProjectionScanPass(0, startPosition, endPostion, _scanPassEntries);

    Iterator<ProjectionRequestThreadTask> projectionRequestsIter = projectionRequests.iterator();
    ProjectionRequestThreadTask projectionRequest = null;

    while (projectionRequestsIter.hasNext())
    {
      projectionRequest = projectionRequestsIter.next();
      projectionRequest.scanPassNumber(0);

      ScanPassDirectionEnum desiredStageDirection = projectionRequest.getStageDirection();
      Assert.expect(desiredStageDirection != null);
      _currentStageDirection = desiredStageDirection;

      projectionRequest.alignSystemFiducialRectangleRight(_currentStageDirection, 0);

      _scanPassEntries.add(projectionRequest);
    }
    scanPass.directionOfTravel(_currentStageDirection);

    addCurrentScanPassToPath(scanPath, startYPosition, endYPosition, cameraAcquisitionParameters);

    return scanPath;
  }

  /**
   * Empty method to override any stage related work. Synthetic triggers don't
   * want or need to move the stage.
   *
   * @author Reid Hayhow
   */
  protected void initializePanelPositionerForScanPath(final List<ScanPass> scanPath,
                                                      TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    //do nothing since we aren't going to move the stage
  }

  /**
   * Base class will make a call to globally turn on xraySource at the beginning
   * of all scans.  Since we are making measurements that require finer control,
   * we will not participate.
   *
   * @author Roy Williams
   */
  protected void turnXraysOn(TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    // do nothing.  Polymorphism override implementation of base class.
  }

  /**
   * @author Roy Williams
   */
  public boolean areAllImagesAcquired() throws XrayTesterException
  {
    boolean allImagesAcquired = super.areAllImagesAcquired();
    if (allImagesAcquired)
      turnOffSyntheticTriggersThenWaitForCamerasToBecomeIdle();
    return allImagesAcquired;
  }
}
