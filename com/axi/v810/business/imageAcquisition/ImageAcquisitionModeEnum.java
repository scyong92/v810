package com.axi.v810.business.imageAcquisition;

/**
 * Enumeration for ImageAcquisitionEngine modes
 *
 * @author Dave Ferguson
 */
public class ImageAcquisitionModeEnum extends com.axi.util.Enum
{
  private static int _index = 0;
  public static final ImageAcquisitionModeEnum NOT_IN_USE = new ImageAcquisitionModeEnum(_index++);
  public static final ImageAcquisitionModeEnum ALIGNMENT = new ImageAcquisitionModeEnum(_index++);
  public static final ImageAcquisitionModeEnum VERIFICATION = new ImageAcquisitionModeEnum(_index++);
  public static final ImageAcquisitionModeEnum PRODUCTION = new ImageAcquisitionModeEnum(_index++);
  public static final ImageAcquisitionModeEnum IMAGE_SET = new ImageAcquisitionModeEnum(_index++);
  public static final ImageAcquisitionModeEnum PROJECTION = new ImageAcquisitionModeEnum(_index++);
  public static final ImageAcquisitionModeEnum PROJECTION_WITH_SYNTHETIC_TRIGGERS = new ImageAcquisitionModeEnum(_index++);
  public static final ImageAcquisitionModeEnum COUPON = new ImageAcquisitionModeEnum(_index++);
  public static final ImageAcquisitionModeEnum OFFLINE = new ImageAcquisitionModeEnum(_index++);

  /**
   * @author Dave Ferguson
   */
  private ImageAcquisitionModeEnum(int id)
  {
    super(id);
  }
}
