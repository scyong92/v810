package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * ReconstructedImagesProducer and ProjectedImagesProducer
 * <dl></dl>
 * Instantiates and manages shared hardware resources used by any producer of images.
 * With these resources, a set of mechanical conversions will be constructed by the
 * individual instance based upon characteristics of the panel (or system fiducial)
 * area to image.
 * <p>
 * Coodination/synchronization of this effort is controlled through a strategy pattern
 * when callers use the <tt>runAcquisitionSequence()</tt> function.
 *
 * @author Roy Williams
 */
public abstract class ProjectionProducer implements Observer
{
  // Administrative shared references.
  protected static Config _config;
  protected static boolean _debug = false;
  protected static int _sleepInterval = 200;

  // References to hardware objects
  protected static XrayTester _xRayTester;
  protected static List<AbstractXrayCamera> _xRayCameras;
  protected static XrayCameraArray _xrayCameraArray;
  protected static PanelHandler _panelHandler;
  protected static PanelPositioner _panelPositioner;
  protected static AbstractXraySource _xRaySource;
  protected static CameraTrigger _cameraTriggerBoard;
  protected static ImageAcquisitionProgressMonitor _progressMonitor;
  protected static HardwareObservable _hardwareObservable;
  protected static boolean _printProgramDownloadedToStage;
  protected static boolean _printProgramDownloadedToCamera;

  // Instance member to describe the translations for camera delay, stage travel, etc.
  private boolean _imageAcquisitionInProgress = false;
  protected Object _abortCompleteCondition = new Object();
  private int _abortControlParticipants = 0;
  private List<? extends ProjectionConsumer> _projectionConsumers = null;

  // Different ways to get out of imageAcquisition are abort (i.e. failure) and
  // a request to cancel from the user.
  private XrayTesterException _xrayTesterException = null;
  private boolean _userCanceledAcquisitionRequest = false;
  
  protected TestExecutionTimer _fastTestExecutionTimer;
  
  protected TestExecutionTimer _testExecutionTimer;
  private int _currentScanPassNumberId = -1;
  protected List<? extends ScanPass> _scanPath = null;
  private int _numberOfScanPassInPath = -1;

  private static PerformanceLogUtil _performanceLog = PerformanceLogUtil.getInstance();
  
  private static String _me = "ProjectionProducer";
  
  private final static Object _abortObj = new Object();
  
  //added by sheng chuan to counter stage too slow assert when using slow velocity feature
  protected static double _scanMotionTimeOutMultiplyFactor = 1;
  protected static BooleanLock _alignmentLock = new BooleanLock(false);
  protected final List<ScanPass> _totalScanPath = new ArrayList<ScanPass>();

  /**
   * @author Roy Williams
   */
  static
  {
    _config = Config.getInstance();
    _xRayTester = XrayTester.getInstance();
    _xRayCameras = XrayCameraArray.getCameras();
    _xrayCameraArray = XrayCameraArray.getInstance();
    _panelHandler = PanelHandler.getInstance();
    _panelPositioner = PanelPositioner.getInstance();
    _xRaySource = AbstractXraySource.getInstance();
    _cameraTriggerBoard = CameraTrigger.getInstance();
    _progressMonitor = ImageAcquisitionProgressMonitor.getInstance();
    _hardwareObservable = HardwareObservable.getInstance();
    String enableMotionControlPvtLog = _config.getStringValue(HardwareConfigEnum.ENABLE_MOTION_CONTROL_PVT_LOG);
    _printProgramDownloadedToStage  = enableMotionControlPvtLog.equalsIgnoreCase("true");
    _printProgramDownloadedToCamera =  _config.getBooleanValue(HardwareConfigEnum.XRAY_CAMERA_LOGGING_ENABLED) ||
                                       _config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE);
  }

  /**
   * Gets the image acquisition done.   Whomever the projection consumer(s) are
   * will be passed in and this function will iterate through them for each scan
   * pass to insure all images are collected.
   *
   * @author Dave Ferguson
   * @author Roy Williams
   */
  protected synchronized void runAcquisitionSequence(TestExecutionTimer testExecutionTimer,
                                                     List<? extends ProjectionConsumer> projectionConsumers,
                                                     List<? extends ScanPass> scanPath,
                                                     int expectedNumberOfScanPasses,
                                                     boolean initializeProjectionConsumers,
                                                     ImageAcquisitionModeEnum acquisitionMode,
                                                     boolean isLastTestSubProgram ) throws XrayTesterException
  {
    Assert.expect(testExecutionTimer != null);
    Assert.expect(projectionConsumers != null);
    Assert.expect(scanPath != null && scanPath.size() > 0);

    Assert.expect(_panelPositioner != null);
    Assert.expect(_xRaySource != null);
      TimerUtil timer = new TimerUtil();   
    try
    {
      _testExecutionTimer = testExecutionTimer;
      _imageAcquisitionInProgress = true;
      _scanPath = scanPath;

      if (isAbortInProgress())
        return;
      
      // Set the projectionConsumers for execution of this scanPath.
      _projectionConsumers = projectionConsumers;
      if (initializeProjectionConsumers)
      {
        for (ProjectionConsumer projectionConsumer : _projectionConsumers)
        {
          projectionConsumer.initializeForNextScanPath();
        }
      }

      if (isAbortInProgress())
        return;

      // We may not leave until the entire scanPath has been completed.
      testExecutionTimer.startAlignmentTimer();
      testExecutionTimer.startStageScanTimer();
      testExecutionTimer.startImageAcquisitionTimer();

      _numberOfScanPassInPath = scanPath.size();

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.SCAN_PATH_SIZE, _numberOfScanPassInPath);

      _currentScanPassNumberId = -1;
      boolean timeFlag0 = false;
      boolean timeFlag1 = false;
      boolean timeFlag2 = false;
      
             
      timer.start();
            
      for (ScanPass currentScanPass : scanPath)
      {
        if (timeFlag0 == false && currentScanPass.getProjectionType().equals(ProjectionTypeEnum.INSPECTION_OR_VERIFICATION))
        {
          timeFlag0 = true;
          _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_ALIGNMENT_INSPECTION_SCAN);
          _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_INSPECTION_SCAN);
        }
        else if (timeFlag1 == false && currentScanPass.getProjectionType().equals(ProjectionTypeEnum.INSPECTION_AND_ALIGNMENT))
        {
          timeFlag1 = true;
          _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_ALIGNMENT_ONLY_SCAN);
          _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_ALIGNMENT_INSPECTION_SCAN);
        }
        else if (timeFlag2 == false && currentScanPass.getProjectionType().equals(ProjectionTypeEnum.ALIGNMENT))
        {
          timeFlag2 = true;
          _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_ALIGNMENT_ONLY_SCAN);
        }

        _currentScanPassNumberId = currentScanPass.getId();

        testExecutionTimer.startStagePausedTimer();
        waitTillProjectionConsumersAreReadyForScanPass(currentScanPass);
        testExecutionTimer.stopStagePausedTimer();
        if (_xrayTesterException != null)
          throw _xrayTesterException;
        if (_userCanceledAcquisitionRequest)
        {
          return;
        }
        runNextScanPass(_currentScanPassNumberId);
        _progressMonitor.scanPassSentToIrps();
      } 
      timer.stop();      
      debug("waitTillAllScanPassDone = " + timer.getElapsedTimeInMillis());
            
      if (_xRayTester.isSimulationModeOn() == false)
      {
        
        debug("isLastTestSubProgram = " + isLastTestSubProgram);        
        debug("_currentScanPassNumberId = " + _currentScanPassNumberId);
        debug("expectedNumberOfScanPasses = " + expectedNumberOfScanPasses);
        //For Speed Up
        if(isLastTestSubProgram && acquisitionMode == ImageAcquisitionModeEnum.PRODUCTION)
        {
          if(_currentScanPassNumberId == (expectedNumberOfScanPasses-1))
          {
            _panelPositioner.waitTillScanPassDone(_scanMotionTimeOutMultiplyFactor);
            _panelPositioner.waitTillScanPathDone(_scanMotionTimeOutMultiplyFactor);
            
            debug("turnXraysOffAndMoveStageToPanelUnloadPositionForProductionInspection start ");
            //For Speed Up
            turnXraysOffAndMoveStageToPanelUnloadPositionForProductionInspection(acquisitionMode);
            debug("turnXraysOffAndMoveStageToPanelUnloadPositionForProductionInspection end ");
         }
        }
        
      }

      timer.reset();
      timer.start();
      // Wait here to be sure that IRP's have acknowledged all scanPasses.
      if (_xRayTester.isSimulationModeOn() == false)
      {
        waitForLastScanPassToBeAcknowledged(expectedNumberOfScanPasses);
                    
        timer.stop();      
        debug("waitForLastScanPassToBeAcknowledged = " + timer.getElapsedTimeInMillis());
        timer.reset();
        timer.start();
        _panelPositioner.waitTillScanPassDone(_scanMotionTimeOutMultiplyFactor);
        
        timer.stop();      
        debug("waitTillScanPassDone = " + timer.getElapsedTimeInMillis());
        
        //added by sheng chuan, this code here is to move the stage to next scan motion position
        //this is needed because with slow velocity feature, motion control need to initialize scan path testsubprogram by testsubprogram
        //so stage no longer move to ready position after existing testsubprogram finish motion, therefore we move it manually here
        if (_totalScanPath.isEmpty() == false && isLastTestSubProgram == false)
        {
          //read the first scan pass position (next testsubprogram first scan pass)
          StagePosition nextReadyPosition = _totalScanPath.get(0).getStartPointInNanoMeters();
          int yStartActualPosition = 0;

          //the 2446500 is stage default prepare distance.
          if (_totalScanPath.get(0).isStageDirectionForward())
            yStartActualPosition = nextReadyPosition.getYInNanometers() - PanelPositioner._STAGE_READY_OFFSET_POSITION;
          else
            yStartActualPosition = nextReadyPosition.getYInNanometers() + PanelPositioner._STAGE_READY_OFFSET_POSITION;

          StagePositionMutable stagePosition;
          // Define stage position
          stagePosition = new StagePositionMutable();
          stagePosition.setXInNanometers(nextReadyPosition.getXInNanometers());
          stagePosition.setYInNanometers(yStartActualPosition);

          // Save off the previously loaded motion profile
          MotionProfile originalMotionProfile = _panelPositioner.getActiveMotionProfile();
          _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
          _panelPositioner.pointToPointMoveAllAxes(stagePosition);
          // Now that we are done with the move, restore the motion profile.
          _panelPositioner.setMotionProfile(originalMotionProfile);
        }
      }
      
      _progressMonitor.scanPathCompleted();
      
      // when we get here all scan passes are done and IRP's have read all projections
      testExecutionTimer.stopStageScanTimer();

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_INSPECTION_SCAN);
      
      timer.reset();
      timer.start();
      // Now that all scan passes have been sent we must wait for all the images to
      // arrive OR an error has occured.
      waitTillAllImagesReturned();
      timer.stop();      
      debug("waitTillAllImagesReturned = " + timer.getElapsedTimeInMillis());
      
      timer.reset();
      timer.start();      
      // Manually clear breakpoint for Real Time PSH
      // This method perform nothing if Real Time PSH is off
      performClearBreakPointActions();
      
      timer.stop();      
      debug("performClearBreakPointActions = " + timer.getElapsedTimeInMillis());
      //For Speed Up
      //turnXraysOffAndMoveStageToPanelUnloadPositionForProductionInspection(acquisitionMode); 

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_RECONSTRUCT);

      _testExecutionTimer.stopImageAcquisitionTimer();
    }
    finally
    {
      //reclose outer barrier if user abort inspection process
      if(_panelHandler.isInLineModeEnabled() && _userCanceledAcquisitionRequest)
      {
        PanelHandler.getInstance().closeSecondaryOuterBarrier();
      }
      
      //XCR-2949, Set this to true, system should be able to read latest exception status and decide to open outer barrrier or not at this stage
      _alignmentLock.setValue(true);
      timer.reset();
      timer.start();  
      
      turnOffSyntheticTriggersThenWaitForCamerasToBecomeIdle();
      
      timer.stop();      
      debug("turnOffSyntheticTriggersThenWaitForCamerasToBecomeIdle = " + timer.getElapsedTimeInMillis());
      
      _imageAcquisitionInProgress = false;
      _projectionConsumers = null;
    }
  }
  
  
  /**
   * @author Anthony Fong
   */  
  public void turnXraysOffAndMoveStageToPanelUnloadPositionForProductionInspection(final ImageAcquisitionModeEnum acquisitionMode) throws XrayTesterException
  {

    ExecuteParallelThreadTasks<XrayTesterException> parallelThread = new ExecuteParallelThreadTasks<XrayTesterException>();
    ThreadTask<XrayTesterException> task1 = new ThreadTask<XrayTesterException>("turnXraysOff()")
    {

      protected XrayTesterException executeTask() throws XrayTesterException
      {
        XrayTesterException exception = null;
        try
        {
          turnXraysOff();
        }
        catch (XrayTesterException xte)
        {
          exception = xte;
          throw xte;
        }

        return exception;
      }
    };

    ThreadTask<XrayTesterException> task2 = new ThreadTask<XrayTesterException>("TurnXraysOnForProductionInspection")
    {

      protected XrayTesterException executeTask() throws XrayTesterException
      {
        XrayTesterException exception = null;
        try
        {
          if (acquisitionMode == ImageAcquisitionModeEnum.PRODUCTION && _panelHandler.isPanelLoaded())
          {
            PanelHandler.getInstance().prepareToUnloadPanel();
          }

        }
        catch (XrayTesterException xte)
        {
          exception = xte;
          throw xte;
        }

        return exception;
      }
    };
    try
    {
      parallelThread.submitThreadTask(task1);
      parallelThread.submitThreadTask(task2);
      parallelThread.waitForTasksToComplete();

      //XCR-2949, change the open outer barrier process to done in thread so that the waittilltrue flag is not pausing everything while waiting
      WorkerThread alignmentImageAcquisitionWorker = new WorkerThread("alignment image acquisition worker thread");

      alignmentImageAcquisitionWorker.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            if (acquisitionMode == ImageAcquisitionModeEnum.PRODUCTION && _panelHandler.isPanelLoaded())
            {
              if (ImageAcquisitionEngine.getInstance().isGeneratingPrecisionImageSet() == false)
              {
                boolean isOverridePanelEjectForTestExec = Config.getInstance().getBooleanValue(SoftwareConfigEnum.OVERRIDE_PANEL_EJECT_FOR_TEST_EXEC);
                boolean isSimulatePanelEjectForTestExec = Config.getInstance().getBooleanValue(SoftwareConfigEnum.SIMULATE_PANEL_EJECT_FOR_TEST_EXEC);
                if ((isOverridePanelEjectForTestExec == true) || (isSimulatePanelEjectForTestExec == true))
                {
                  //skip this
                }
                else
                {
                  if (_panelHandler.isInLineModeEnabled())
                  {
                    _alignmentLock.waitUntilTrue();
                    if (isAbortInProgress() == false && _panelHandler.isEnabledOuterBarrierOpenAfterCompleteScanningForS2EX())
                    {
                      DigitalIo.getInstance().openPanelClamps();
                      PanelHandler.getInstance().openSecondaryOuterBarrier();
                    }
                  }
                }
              }
            }
          }
          catch (XrayTesterException xex)
          {
            _xrayTesterException = xex;
          }
          catch (InterruptedException ex)
          {
            //do nothing
          }
        }
      });
    }
    catch (XrayTesterException xex)
    {
      throw xex;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }     
  }


  /**
   * @author Roy Williams
   */
  protected boolean checkAllImagesReturned() throws XrayTesterException
  {
    return areAllImagesAcquired();
  }

  /**
   * @author Roy Williams
   */
  private void waitTillAllImagesReturned() throws XrayTesterException
  {
    boolean waitForCompletion = true;
    while (waitForCompletion)
    {
      if (_xrayTesterException != null)
      {
        throw _xrayTesterException;
      }
      if (_userCanceledAcquisitionRequest)
        return;
      if (checkAllImagesReturned())
        break;
      try
      {
        wait(_sleepInterval);
      }
      catch (InterruptedException e)
      {
        // do nothing till all images are acquired or an exception has been thrown
      }
    }
  }

  /**
   * @author Roy Williams
   */
  protected List<? extends ProjectionConsumer> getProjectionConsumers()
  {
    Assert.expect(_projectionConsumers != null);
    return _projectionConsumers;
  }

  /**
   * @author Roy Williams
   */
  public boolean isImageAcquisitionInProgress()
  {
    return _imageAcquisitionInProgress;
  }

  /**
   * @author Roy Williams
   */
  protected void waitTillProjectionConsumersAreReadyForScanPass(ScanPass scanPass) throws XrayTesterException
  {
    Assert.expect(scanPass != null);
    waitTillProjectionConsumersAreReadyForScanPass(scanPass.getId());
    performBreakPointActionsForScanPass(scanPass);
  }
  
  /**
   * @author Roy Williams
   */
  protected synchronized void waitTillProjectionConsumersAreReadyForScanPass(int scanPassNumber) throws XrayTesterException
  {

    int retryCount = 1;
    int maximumNumberOfRetries = 300;
    Assert.expect(scanPassNumber >= 0);

    if (_xrayTesterException != null)
      throw _xrayTesterException;
    if (_userCanceledAcquisitionRequest)
      return;

    boolean waitingForImages = true;
    List<? extends ProjectionConsumer> projectionConsumers = getProjectionConsumers();
    //Siew Yeng - XCR-3078 - temporary comment out this checking and continue to monitor if any hang issue occur.
    while (waitingForImages /*&& (retryCount <= maximumNumberOfRetries)*/)
    {
      boolean canProceed = true;

      // The caller to this function is synchronized.  Thus, it has captured the
      // monitor.  The wait(10) below will allow other threads an opportunity to
      // temporarily take over the monitor in case an abort needs to be performed.
      try
      {
        for (ProjectionConsumer projectionConsumer : projectionConsumers)
        {
          canProceed = canProceed && projectionConsumer.canProceedWithScanPassWithWait(scanPassNumber);
          if (_xrayTesterException != null)
            throw _xrayTesterException;
          if (_userCanceledAcquisitionRequest)
            return;
        }
        if (canProceed == false)
        {
          ++retryCount;
          wait(_sleepInterval);   // ImageAcquisiton relinquish thread so abort can happen.
        }
      }
      catch (InterruptedException e)
      {
        // do nothing
      }
      if (_xrayTesterException != null)
      {
        throw _xrayTesterException;
      }
      if (_userCanceledAcquisitionRequest)
        return;
      if (canProceed == true)
        waitingForImages = false;
    }
    //Siew Yeng - XCR-3078 - temporary comment out this checking and continue to monitor if any hang issue occur.
    //                     - according to Swee Yee this is an infinite loop that will cause hang.
//    if (retryCount > maximumNumberOfRetries)
//      abort(ImageAcquisitionHungBusinessException.getProjectionConsumersNotReadyForScanPassHungException());

  }

  /**
   * @author Roy Williams
   */
  protected void performBreakPointActionsForScanPass(ScanPass scanPass) throws XrayTesterException
  {
  }

   /**
   * @author Chnee Khang Wah, 2013-08-16, Realtime PSH
   */
  protected void performClearBreakPointActions() throws XrayTesterException
  {
  }
  
  /**
   * Run the next scan pass.
   *
   * @author Roy Williams
   */
  protected void runNextScanPass(int scanPassNumber) throws XrayTesterException
  {
    Assert.expect(_panelPositioner != null);
    Assert.expect(scanPassNumber >= 0);

    if (isAbortInProgress())
      return;

    // Start all of the ProjectionConsumers.
    for (ProjectionConsumer projectionConsumer : _projectionConsumers)
    {
      projectionConsumer.startScanPass(scanPassNumber);
    }

    if (_userCanceledAcquisitionRequest)
      return;

    // Take advantage of this last step to make sure there aren't any xrayTesterExceptions
    if (_xrayTesterException != null)
      throw _xrayTesterException;

    // Move the stage to produce the triggers.   This just enqueues the work on the
    // stage.  We really don't know if it happened.
    _panelPositioner.runNextScanPass();

    // Give stage an opportunity to complain if a problem is encoutered.   The
    // problem will be expressed by throwing an exception and the call to
    // isScanPassDone is the vehicle to get the exception back from the
    // asynchronsous communications with the panelPositioner.
    //
    // NOTE: Only do this once as waiting till it is fully acknowledged will cause
    // a turn and stop (in stage programming terminology).
    _panelPositioner.isScanPassDone();
  }

  /**
   * Make sure the x-rays are on.
   * @author Dave Ferguson
   */
  protected void turnXraysOn(TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(_xRaySource != null);
    Assert.expect(testExecutionTimer != null);

    _testExecutionTimer = testExecutionTimer;
    // If x-rays are off, turn them on.
    if (_xRaySource.areXraysOn() == false)
    {
      _testExecutionTimer.startXrayOnTimer();
      _xRaySource.on();
      _testExecutionTimer.stopXrayOnTimer();
    }
  }

  /**
   * @author Roy Williams
   */
  protected void turnXraysOff() throws XrayTesterException
  {
    // Turn off the xraySource (if needed).
    boolean isCalibrationRun = isCalibrationRun();
    _testExecutionTimer.startXrayOffTimer();
    try
    {
      if (hasXrayTesterException() || isCalibrationRun == false)
        _xRaySource.off();
    }
    catch (XrayTesterException ex)
    {
      throw ex;
    }
    finally
    {
      _testExecutionTimer.stopXrayOffTimer();
    }
  }

  /**
   * Prepares the PanelPositioner for the scan path.
   *
   * @author Matt Wharton
   * @author Dave Ferguson
   */
  protected void initializePanelPositionerForScanPath(final List<ScanPass> scanPath, TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(scanPath != null);
    Assert.expect(testExecutionTimer != null);

    testExecutionTimer.startInitStageScanPassTimer();
    CameraTrigger cameraTrigger = CameraTrigger.getInstance();
    final int numberOfStartingPulses = cameraTrigger.getNumberOfRequiredSampleTriggers();
    final int distanceInNanoMetersPerPulse = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
    
    //if is low Mag, 
    if(XrayTester.isLowMagnification())
    {
    // Set the scan motion profile that corresponds to the magnification
    //Ngie Xing - S2EX PRofile Setup
    //set the motion profile according to hardware config
        String zAxisMotorPositionForLowMag = Config.getSystemCurrentLowMagnification();
        if (_debug || _printProgramDownloadedToStage)
          System.out.println("USE FAST PROFILE "+ zAxisMotorPositionForLowMag);
        _panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.getScanMotionProfileEnum(zAxisMotorPositionForLowMag)));      
      }
      else
      {
        String zAxisMotorPositionForHighMag = Config.getSystemCurrentHighMagnification();
        if (_debug || _printProgramDownloadedToStage)
          System.out.println("USE SLOW PROFILE " + zAxisMotorPositionForHighMag);
        _panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.getScanMotionProfileEnum(zAxisMotorPositionForHighMag)));
      }
      if (_debug || _printProgramDownloadedToStage || _config.isMotionControlLogEnabled())
      {
        System.out.println("ScanPath sent to stage/panelPositioner:");
        // Swee Yee Wong - log scan path stage commands sent from java
        // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
        logIaeStageCommands("Pass #\tdivnstart\tdivnstop\tdirection");
        for (ScanPass scanPass : scanPath)
        {
          System.out.println("pass number: " + scanPass.getId() +
                             "    direction: " + scanPass.getStageDirection());
          StagePosition startPosition = scanPass.getStartPointInNanoMeters();
          System.out.println("    start x: " + startPosition.getXInNanometers() +
                             "    start y: " + startPosition.getYInNanometers());
          StagePosition endPosition = scanPass.getEndPointInNanoMeters();
          System.out.println("    start x: " + endPosition.getXInNanometers() +
                             "    start y: " + endPosition.getYInNanometers());
          logIaeStageCommands(scanPass.getId() + "\t" + startPosition.getYInNanometers() + "\t" + endPosition.getYInNanometers() + "\t" + scanPass.getStageDirection());
        }
      }
      
    _panelPositioner.disableScanPath();
    // Swee Yee Wong - Insufficient trigger fix, include starting pulse information for scanpass adjustment
    // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
    _panelPositioner.enableScanPath(scanPath, distanceInNanoMetersPerPulse, numberOfStartingPulses);

    // Set the position pulse
    _panelPositioner.setPositionPulse(distanceInNanoMetersPerPulse, numberOfStartingPulses);

    testExecutionTimer.stopInitStageScanPassTimer();
  }

  /**
   * This object is going to get notifications from ImageAcqquisitionExceptionObservable.
   *
   * Keep in mind that there are three instances of ProjectionConsumer's amd each
   * may be at a different state of readiness.
   * <dl>
   * <li> ImageReconstructionEngine</li>
   * <li> ProjectionEngine</li>
   * <li> ProjectedImagesProducerWithSyntheticTriggers</li>
   * </dl>
   * @author Roy Williams
   */
  public void update(Observable observable, Object object)
  {
    Assert.expect(observable != null);
    Assert.expect(object != null);

    if (observable instanceof ImageAcquisitionExceptionObservable)
    {
      Assert.expect(object instanceof XrayTesterException);
      abort((XrayTesterException)object);
    }
    else
    {
      Assert.expect(false);
    }
  }

  /**
   * Aborts the panelPostioner and cameras.
   *
   * @author Roy Williams
   */
  public void userAbort() throws XrayTesterException
  {
    // Kok Chun, Tan - XCR3088 - place the synchronized here instead of at the method declaration to avoid synchronization problem.
    // This sychronization also will fix the xray tube crash with inner barrier issue (XCR2781).
    synchronized(_abortObj)
    {
      // Insure this only happens once.
      if (_userCanceledAcquisitionRequest == false)
      {
        _userCanceledAcquisitionRequest = true;

        // Classic case of dynamic binding here.
        // Look for all the references on this call to performAbortProcedure() method.
        // Notice that ReconstructedImagesProducer also implements this method that
        // overrides this implementation.  ReconstructedImagesProducer will get called first
        // and it is up to that declaration to decide whether to call super.performAbortProcedure().
        //
        // See the default implementation here in ProjectionProducer.
        performAbortProcedure();
      }
    }
  }

  /**
   * @author Roy Williams
   */
  protected void incrementAbortControlParticipants()
  {
    synchronized (_abortCompleteCondition)
    {
      _abortControlParticipants++;
    }
  }

  /**
   * @author Roy Williams
   */
  protected void decrementAbortParticipants()
  {
    synchronized (_abortCompleteCondition)
    {
      if (_abortControlParticipants > 0)
      {
        _abortControlParticipants--;
        if (_abortControlParticipants == 0)
        {
          _abortCompleteCondition.notifyAll();
        }
      }
    }
  }

  /**
   * @author Roy Williams
   */
  public void waitIfAbortInProgress()
  {
    if (isAbortInProgress())
    {
     while (_abortControlParticipants > 0)
      {
        synchronized (_abortCompleteCondition)
        {
          try
          {
            _abortCompleteCondition.wait(1000);
          }
          catch (InterruptedException ex)
          {
            // Do nothing...
          }
        }
      }
    }
  }

  /**
   *
   * @author Roy Williams
   */
  protected synchronized void abortImageReconstructionProcessors() throws XrayTesterException
  {
    // do nothing by default.   ProjectedImagesProducer doesn't need to do this
    // because it doesn't use them.   ReconstructedImagesProducer (a separate
    // instance does and it overrides this implementation.
  }

  /**
   * Aborts panelPostioner, cameras, etc one at a time.  Each routine added to this
   * method is expected to eat exceptions
   *
   * @author Roy Williams
   */
  private synchronized void performAbortProcedure() throws XrayTesterException
  {
    try
    {
      incrementAbortControlParticipants();

      _progressMonitor.stopMonitoring();

      abortImageReconstructionProcessors();

      abortPanelPositioner();

      abortXrayCameras();

      try
      {
        // Fixing CR28168 interlocks being tripped because xraySource is not off.
        turnXraysOff();
      }
      catch (XrayTesterException ex)
      {
        evaluateNewXrayTesterException(ex);
      }
    }
    finally
    {
      if (_xrayTesterException != null)
      {
        System.out.println();
        System.out.println("####### Stacktrace from the exception that caused shutdown to occur:");
        _xrayTesterException.printStackTrace();

        System.out.println();
        String stackTraceExplanation = "####### Stacktrace from thread performing shutdown:";
        System.out.println(stackTraceExplanation);
        try
        {
          throw new XrayTesterException(new LocalizedString(stackTraceExplanation, null));
        }
        catch (XrayTesterException threadPerformingShutdown)
        {
          threadPerformingShutdown.printStackTrace();
        }
      }
      else
      {
        System.out.println("####### Shutdown due to user abort/cancel");
        try
        {
          throw new Exception();
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
        }
      }

      // Last participant in the abort process will cause flag to be set denoting
      // done with abort.
      decrementAbortParticipants();
    }
  }

  /**
   * Access is open to protected so any derived implementor of the ProjectionProducer
   * interface (i.e. abstract methods below) can stop execution of the XrayTester.
   *
   * @return true to DENOTE this is the FIRST TIME the EXCEPTION SET.  NOT EACH TIME CHANGED!
   *
   * @author Roy Williams
   */
   protected synchronized boolean evaluateNewXrayTesterException(XrayTesterException ex)
  {
    Assert.expect(ex != null);

    boolean firstTimeExceptionSet = false;

    // Is this the first exception we have ever seen?   Set this before we do anything
    // else that might result in subsequent exceptions.
    if (_xrayTesterException == null)
    {
      _xrayTesterException = ex;
      firstTimeExceptionSet = true;
      debug("XrayTesterException changed: " + _xrayTesterException.getClass().getName());
    }

    // If the exception is from a communication channel with an IRP we need to close
    // its connections.  This must be done regardless of whether this exception is
    // ultimately saved away as the cause reported to the GUI.
    if (ex instanceof ImageReconstructionProcessorCommunicationException)
    {
      ImageReconstructionProcessorCommunicationException communicationException =
          (ImageReconstructionProcessorCommunicationException) ex;
      ImageReconstructionEngine ire = communicationException.getImageReconstructionEngine();
      ire.closeConnections();
    }

    // Not the first exception we have seen below here.
    determineExceptionBasedOnPriority(ex);

    return firstTimeExceptionSet;
  }

  /**
   * @author Roy Williams
   */
  private synchronized void determineExceptionBasedOnPriority(XrayTesterException possibleNewXrayTesterException)
  {
    Assert.expect(possibleNewXrayTesterException != null);

    // If the current exception is ALREADY an interlock exception then we will keep the one we have.
    if (_xrayTesterException instanceof InterlockHardwareException)
    {
      clearPanelPostionerException();
      return;
    }

    // If there is an interlock door open then we will allow it to preempt the
    // current exception with another.
    if (possibleNewXrayTesterException instanceof InterlockHardwareException)
    {
      _xrayTesterException = possibleNewXrayTesterException;
      clearPanelPostionerException();
      debug("XrayTesterException changed: " + _xrayTesterException.getClass().getName());
      return;
    }

    // This possibleNewXrayTesterException is definitely NOT a InterlockHardwareException!
    // If this next statement is true, then we MUST pre-emptively change the current
    // _xrayTesterException with the ONE WE WANT... not the one it thinks it has!
    try
    {
      if (Interlock.getInstance().isInterlockOpen())
      {
        _xrayTesterException = InterlockHardwareException.getInterlockChainIsOpenException();
        clearPanelPostionerException();
        return;
      }
    }
    catch (XrayTesterException ex)
    {
      if (ex instanceof InterlockHardwareException)
      {
        // It could be we are the first to discover.
        _xrayTesterException = ex;
        clearPanelPostionerException();
        return;
      }
    }
    catch (Throwable throwable)
    {
      Assert.logException(throwable);
    }

    // See if this exception is a MotionControlHardwareException
    if (possibleNewXrayTesterException instanceof MotionControlHardwareException)
    {
      _xrayTesterException = possibleNewXrayTesterException;
      debug("XrayTesterException changed: " + _xrayTesterException.getClass().getName());
      return;
    }

    // The DigitalIoException wins!
    if (possibleNewXrayTesterException instanceof DigitalIoException)
    {
      _xrayTesterException = possibleNewXrayTesterException;
      debug("XrayTesterException changed: " + _xrayTesterException.getClass().getName());
      return;
    }
  }

  /**
   * @author Roy Williams
   */
  private void clearPanelPostionerException()
  {
    try
    {
      _panelPositioner.isScanPathDone();
    }
    catch (XrayTesterException ex)
    {
      // do nothing.   Some XrayTesterExceptions (like InterlockHardwareException)
      // cause the panel positioner to create an exception that is held and returned
      // asynchronously.   We must clear this or the cached exception will be
      // emitted much, much further downstream when you least suspect it.
      int i = 1;   // just a spot so we can set a breakpoint for testing.
    }
  }


  /**
   * @author Roy Williams
   */
  public synchronized void abort(XrayTesterException abortException)
  {
    Assert.expect(abortException != null);

    // Although this may be the first time the exception was set, we could already
    // be processing a user cancel.  In this case, the exception was a byproduct
    // of a user abort.  The currently running user abort will be allowed to continue.
    boolean exceptionWasSet = evaluateNewXrayTesterException(abortException);

    // We did (in previous step) record the fact that an exception did occur.
    if (_userCanceledAcquisitionRequest)
      return;

    if (exceptionWasSet == false)
      return;

    // Well that leaves us with the need to run the abort procedure.
    try
    {
      performAbortProcedure();
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author George A. David
   */
  private void abortPanelPositioner()
  {
    try
    {
      _panelPositioner.abort();
    }
    catch (XrayTesterException ex)
    {
      evaluateNewXrayTesterException(ex);
    }
  }

  /**
   * @author George A. David
   */
  private void abortXrayCameras()
  {
    try
    {
      _xrayCameraArray.abort();
    }
    catch (XrayTesterException xte)
    {
      evaluateNewXrayTesterException(xte);
    }
  }

  /**
   * Abort includes the semantics for user and hardware aborts.
   *
   * @author Roy Williams
   */
  public boolean checkForAbortInProgress() throws XrayTesterException
  {
    if (_xrayTesterException != null)
      throw _xrayTesterException;
    if (_userCanceledAcquisitionRequest)
      return true;
    return false;
  }

  /**
   * Abort includes the semantics for user and hardware aborts.
   *
   * @author Roy Williams
   */
  public boolean isAbortInProgress()
  {
    if (_xrayTesterException != null)
      return true;
    if (_userCanceledAcquisitionRequest)
      return true;
    return false;
  }

  /**
   * @author Roy Williams
   */
  public XrayTesterException getXrayTesterException()
  {
    Assert.expect(_xrayTesterException != null);
    return _xrayTesterException;
  }

  /**
   * @author Roy Williams
   */
  public boolean hasXrayTesterException()
  {
    return _xrayTesterException != null;
  }

  /**
   * @author Roy Williams
   */
  public void clearForNextRun()
  {
    _xrayTesterException = null;
    debug("XrayTesterException changed: null");
    _userCanceledAcquisitionRequest = false;
    
    if (_scanPath != null)
      _scanPath.clear();
  }

  /**
   * Allow debugging to be dynamically enabled/disabled because this is very
   * problematic.
   *
   * @author Roy Williams
   */
  public static void setDebug(boolean state)
  {
    _debug = state;
  }

  /**
   * Cases where you'll need to turn off synthetic triggers:
   * a) Before programming.   Be sure to wait till quiet period established afterwards.
   * b) After running a confirmation we will need to turn off triggers to complete
   *    a scan pass.
   * @author Roy Williams
   */
  protected void turnOffSyntheticTriggersThenWaitForCamerasToBecomeIdle() throws XrayTesterException
  {
    _cameraTriggerBoard.off();
    _xrayCameraArray.waitUntilCamerasAreReadyForAcquisition();
  }

  /**
   * Used during convirmations to simulate stage movement.
   *
   * @author Roy Williams
   */
  protected void waitForCamerasToBecomeIdleThenTurnOnSyntheticTriggers() throws XrayTesterException
  {
    _xrayCameraArray.waitUntilCamerasAreReadyForAcquisition();
    _cameraTriggerBoard.on();
  }

  /**
   * @author George A. David
   */
  public int getCurrentScanPassNumber()
  {
    Assert.expect(_currentScanPassNumberId >= 0);

    return _currentScanPassNumberId;
  }

  /**
   * Get the total number of scan pass in the path.
   * @author Rex Shang
   */
  public int getTotalScanPassNumber()
  {
    Assert.expect(_scanPath != null);

    return _scanPath.size();
  }

  /**
   * @author Roy Williams
   */
  protected boolean isCalibrationRun()
  {
    return true;
  }

  /**
   * @author Roy Williams
   */
  public boolean isAbortProcedureInProgress()
  {
    return true;
  }

  /**
   * Make sure the x-rays tube is home and in safe position.
   * @author Anthony Fong
   */
  protected void moveXrayZaxisHome() throws XrayTesterException
  {  
    synchronized(_abortObj)
    {
      if(XrayActuator.isInstalled() == false)
        return;

      if (XrayCPMotorActuator.isInstalled() == true) 
        XrayActuator.getInstance().home(false,false);
    }
  }
  
  /**
   * Make sure the x-rays are up.
   * @author Wei Chin
   */
  protected void moveXrayCylinderUp() throws XrayTesterException
  {    
    synchronized(_abortObj)
    {
      if(_userCanceledAcquisitionRequest)
        return;
      
      if(XrayActuator.isInstalled() == false)
        return;

      XrayActuator.getInstance().up(true);
      // re-calculate magnification 
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    }
  }

  /**
   * Make sure the x-rays are down.
   * @author Wei Chin
   */
  protected void moveXrayCylinderDown() throws XrayTesterException
  {    
    synchronized(_abortObj)
    {
      if(_userCanceledAcquisitionRequest)
        return;
      
      if(XrayActuator.isInstalled() == false)
        return;

      XrayActuator.getInstance().down(true);
      // re-calculate magnification 
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.HIGH);
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  private void debug(String message)
  {
    if (_debug)
      System.out.println(_me + ": " + message);
  }
  
  /**
   * Log scan path information
   * Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
   * @author Swee-Yee.Wong
   */
  private void logIaeStageCommands(String message)
  {
    if (_config.isMotionControlLogEnabled())
    {
      try
      {
        IaeStageCommandLogUtil.getInstance().log(message);
      }
      catch (XrayTesterException e)
      {
        System.out.println("Failed to log iaeStageCommands. \n" + e.getMessage());
      }
    }
  }

  /**
   * By default this does nothing because not all images acquired involve a panel.
   * Some involve TestProgram which have multiple ScanPaths.  Calibrations (or
   * adjustments) have List<ProjectionRequest> that has a single ScanPath.
   * <p>
   * No matter.   It is the duty of the implementer to define its terms for last
   * scan pass.  Look at each implementation for details.
   *
   * @author Roy Williams
   */
  abstract protected void waitForLastScanPassToBeAcknowledged(int expectedNumberOfScanPasses) throws XrayTesterException;

  /**
   * @author Roy Williams
   */
  abstract protected boolean waitForScanPassToBeAcknowledged(int expectedNumberOfScanPasses) throws XrayTesterException;

  /**
   * Derived classes must indicate when all images are produced and accounted for.
   *
   * @author Roy Williams
   */
  public abstract boolean areAllImagesAcquired() throws XrayTesterException;
  
}
