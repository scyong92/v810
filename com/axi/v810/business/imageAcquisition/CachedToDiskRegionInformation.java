package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;

/**
 * Used by ReconstructedImagesMemoryManager to keep track of ReconstructionRegion
 * that is cached to disk.
 * @author Patrick Lacz
 */
public class CachedToDiskRegionInformation
{
  private long _sizeInMemory = 0;
  private Map<SliceNameEnum, String> _imagePaths = new HashMap<SliceNameEnum, String>();
  private boolean _isSavedForRepair = false;

  /**
   * @author Patrick Lacz
   */
  public long getSizeInMemory()
  {
    return _sizeInMemory;
  }

  /**
   * @author Patrick Lacz
   */
  public Map<SliceNameEnum, String> getSliceNameToFullPathOfCachedImageMap()
  {
    Assert.expect(_imagePaths != null);
    return _imagePaths;
  }

  /**
   * @author Patrick Lacz
   */
  public void setSizeInMemory(long size)
  {
    _sizeInMemory = size;
  }

  /**
   * @author Patrick Lacz
   */
  public void addSliceNameToFullPathOfImage(SliceNameEnum sliceNameEnum, String imagePath)
  {
    Assert.expect(_imagePaths != null);
    Assert.expect(imagePath != null);
    Assert.expect(sliceNameEnum != null);

    _imagePaths.put(sliceNameEnum, imagePath);
  }

  /**
   * @author Patrick Lacz
   */
  public boolean isSavedForRepair()
  {
    return _isSavedForRepair;
  }

  /**
   * @author Patrick Lacz
   */
  public void setIsSavedForRepair(boolean savedForRepair)
  {
    _isSavedForRepair = savedForRepair;
  }
}
