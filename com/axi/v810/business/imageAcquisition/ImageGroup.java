package com.axi.v810.business.imageAcquisition;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 * @author Kay Lannen
 */
public abstract class ImageGroup
{
  private List<Pair<ReconstructionRegion, SystemFiducialRectangle>> _regionPairsSortedInExecutionOrder;
  private Collection<ReconstructionRegion> _unSortedRegions = new ArrayList<ReconstructionRegion>();
  private SystemFiducialRectangle _nominalAreaToImage;
  private PanelRectangle _inspectionRegionsBoundsInNanoMeters;
  private ScanPath _scanPath;
  private Set<ImageReconstructionEngineEnum> _imageReconstructionEngines = new HashSet<ImageReconstructionEngineEnum>();
  private SortedSet<ImageZone> _imageZones = new TreeSet<ImageZone>();
  private AffineTransform _alignmentTransform = null;

  protected static List<Fiducial> _fiducials;
  protected static XrayCameraArray _xrayCameraArray = XrayCameraArray.getInstance();
  protected static int _cameraArrayWidth = XrayCameraArray.getInstance().getCameraArrayWidthProjectedOnMinimumSlicePlane();

  static protected final boolean LARGEST = true;
  static protected final boolean SMALLEST = false;

  /**
   * @author Roy Williams
   */
  public ImageGroup(Collection<ReconstructionRegion> unsortedRegions)
  {
    Assert.expect(unsortedRegions != null);
    initialize(unsortedRegions);
  }

  /**
   * @author Kay Lannen
   */
  protected ImageGroup()
  {
    // Null initialization; used by subclasses only for efficiency
    // Subclass is responsible for correctly initializing all class data!
  }

  /**
   * This estimator uses the actual scan path within this ImageGroup to estimate
   * the test execution time.  It should not be used before the scan path has
   * been generated!
   * @author Roy Williams
   * @author Kay Lannen
   */
  public double estimateExecutionTimeForScanPassesInSecondsUsingScanPath()
  {
    ScanPath scanPath = getScanPath();
    Assert.expect( scanPath != null );
    MachineRectangle scanPathExtents = scanPath.getScanPathExtents();
    int numberOfSteps = scanPath.getScanPasses().size();
    int yTravelRequired = scanPathExtents.getHeight();
    double estimatedTravel = (double) numberOfSteps *  yTravelRequired;
    double estimatedTravelTime = estimatedTravel/PanelPositioner.getVelocityForScanMotionProfileInNanometersPerSecond();
    double estimatedTurnaroundTime = numberOfSteps * PanelPositioner.getAverageTurnaroundTimeInSeconds();
    return estimatedTravelTime + estimatedTurnaroundTime;
  }

  /**

   * @author Kay Lannen
   */
 // abstract public double estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSize();


  /**
   * @author Kay Lannen
   */
  public SortedSet<ImageZone> getImageZones()
  {
    return (_imageZones);
  }

  /**
   * When this function uses the term sorted (or unsorted), it is refering to the
   * assignment of execution order when the area is being scanned.  This order is
   * determined by adding all unsortedRegions into a list then sorting them using a
   * PositionOfReconstructionRegionsWithSystemFiducialComparator.
   *
   * @see PositionOfReconstructionRegionsWithSystemFiducialComparator
   * @author Roy Williams
   */
  protected void initialize(Collection<ReconstructionRegion> unsortedRegions)
  {
    Assert.expect(unsortedRegions != null);
    Assert.expect(unsortedRegions.size() > 0);

    _inspectionRegionsBoundsInNanoMeters = TestSubProgram.calculateRegionBounds(unsortedRegions, _fiducials);
    _regionPairsSortedInExecutionOrder = new ArrayList<Pair<ReconstructionRegion, SystemFiducialRectangle>>(unsortedRegions.size());

    PanelRectangle panelRectangleForIndividualRegion = null;
    PanelRectangle panelRectangleWithAlignmentUncertainty = new PanelRectangle(0, 0, 1, 1);
    int alignmentUncertainty = Alignment.getAlignmentUncertaintyBorderInNanoMeters();

    for (ReconstructionRegion region : unsortedRegions)
    {

      if (_imageReconstructionEngines.contains(region.getReconstructionEngineId()) == false)
      {
        _imageReconstructionEngines.add(region.getReconstructionEngineId());
      }

      if (_alignmentTransform == null)
      {
        _alignmentTransform = region.getManualAlignmentMatrix();
      }

      // ok, now add the alignment uncertainty to all sides for our PanelRectangle
      // and use it to compute the Nominal area to image.
      panelRectangleForIndividualRegion = region.getRegionRectangleRelativeToPanelInNanoMeters();
      panelRectangleWithAlignmentUncertainty.setRect(panelRectangleForIndividualRegion.getMinX() - alignmentUncertainty,
                                                     panelRectangleForIndividualRegion.getMinY() - alignmentUncertainty,
                                                     panelRectangleForIndividualRegion.getWidth() + (2 * alignmentUncertainty),
                                                     panelRectangleForIndividualRegion.getHeight() + (2 * alignmentUncertainty));
      SystemFiducialRectangle regionSystemFiducialRectangle =
          MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(
              panelRectangleWithAlignmentUncertainty,
              _alignmentTransform);

      // Add this Pair<ReconstructionRegion + SystemFiducialRectangle> to the list
      // that we will sort once done in the loop.
      _regionPairsSortedInExecutionOrder.add(new Pair<ReconstructionRegion, SystemFiducialRectangle>(region, regionSystemFiducialRectangle));
      _unSortedRegions.add(region);
    }

    // Now that we have the alignmentTransform, lets convert _inspectionRegionsBoundsInNanoMeters
    //  to a SystemFiducialRectangle.
    _nominalAreaToImage = MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(
        _inspectionRegionsBoundsInNanoMeters,
        _alignmentTransform);

    // Last thing we will do is to sort the list so we can determine which scanPass
    // they will participate on.
    PositionOfReconstructionRegionsWithSystemFiducialComparator comparator =
        new PositionOfReconstructionRegionsWithSystemFiducialComparator(true, true);
    Collections.sort(_regionPairsSortedInExecutionOrder, comparator);
  }

  /**
   * @author Roy Williams
   */
  public SystemFiducialRectangle getSystemFiducialRectangle(ReconstructionRegion region)
  {
    SystemFiducialRectangle sfr = null;
    for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : _regionPairsSortedInExecutionOrder)
    {
      if (region.equals(regionPair.getFirst()))
        sfr = regionPair.getSecond();
    }
    return sfr;
  }

  /**
   * @author Chnee Khang Wah
   */
  public Collection<ReconstructionRegion> getUnsortedRegions()
  {
    return _unSortedRegions;
  }
  
  /**
   * @author Roy Williams
   */
  public List<Pair<ReconstructionRegion, SystemFiducialRectangle>> getRegionPairsSortedInExecutionOrder()
  {
    return _regionPairsSortedInExecutionOrder;
  }

  /**
   * @author Chnee Khang Wah
   */
  protected void setUnSortedRegions(Collection<ReconstructionRegion> unSortedRegions)
  {
    _unSortedRegions = unSortedRegions;
  }
  /**
   * @author Kay Lannen
   */
  protected void setRegionPairsSortedInExecutionOrder(
      List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionPairsSortedInExecutionOrder)
  {
    _regionPairsSortedInExecutionOrder = regionPairsSortedInExecutionOrder;
  }

  /**
   * @author Roy Williams
   */
  public static void setFiducials(List<Fiducial> fiducials)
  {
    Assert.expect(fiducials != null);
    _fiducials = fiducials;
  }

  /**
   * @author Roy Williams
   */
  public static void unsetFiducials()
  {
    _fiducials = new ArrayList<Fiducial>();
  }

  /**
   * Calculates nominal area to image based upon intersection of all SystemFiducialRectangles
   * in the list.
   *
   * @author Chnee Khang Wah, New Scan Route
   */
  public void reComputeNominalAreaToImage()
  {
    _nominalAreaToImage = MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(
        _inspectionRegionsBoundsInNanoMeters,
        _alignmentTransform);
  }
  
  /**
   * Calculates nominal area to image based upon intersection of all SystemFiducialRectangles
   * in the list.
   *
   * @author Roy Williams
   */
  public SystemFiducialRectangle getNominalAreaToImage()
  {
    return _nominalAreaToImage;
  }

  /**
   * @author Kay Lannen
   */
  public void setNominalAreaToImage(SystemFiducialRectangle nominalAreaToImage)
  {
    _nominalAreaToImage = nominalAreaToImage;
  }

  /**
   * @author Roy Williams
   */
  public PanelRectangle getPanelRectangleToImage()
  {
    return _inspectionRegionsBoundsInNanoMeters;
  }

  /**
   * @author Kay Lannen
   */
  public void setPanelRectangleToImage(PanelRectangle inspectionRegionsBoundsInNanoMeters)
  {
    _inspectionRegionsBoundsInNanoMeters = inspectionRegionsBoundsInNanoMeters;
  }


  /**
   * @author Roy Williams
   */
  public void setScanPath(ScanPath scanPath)
  {
    _scanPath = scanPath;
  }

  /**
   * @author Roy Williams
   */
  public ScanPath getScanPath()
  {
    return _scanPath;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {
    if (_scanPath != null)
      _scanPath.clear();
  }

  /**
   * @author Roy Williams
   */
  public int getNumberReconstructionEngines()
  {
    return _imageReconstructionEngines.size();
  }

  /**
   * @author Roy Williams
   */
  public boolean usesReconstructionEngine(ImageReconstructionEngineEnum engine)
  {
    return _imageReconstructionEngines.contains(engine);
  }

  /**
   * @author Kay Lannen
   */
  public void setImageReconstructionEngines(Set<ImageReconstructionEngineEnum> imageReconstructionEngines)
  {
    _imageReconstructionEngines = imageReconstructionEngines;
  }

  /**
   * @author Kay Lannen
   */
  public Set<ImageReconstructionEngineEnum> getImageReconstructionEngines()
  {
    return(_imageReconstructionEngines);
  }

  /**
   * This estimator matches the computation used in generateScanPathForCustomListOfReconstructionRegions.
   * @author Kay Lannen
   */
  static public double estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSizeMethodA(int nominalWidth, int nominalHeight,
                                                                                    int meanStepSizeInNanoMeters)
  {
    MachineRectangle cameraArrayRectangle = MechanicalConversions.getCameraArrayRectangleAtMinSliceHeight();
    int imageableWidthOfSingleCamera = _xrayCameraArray.getImageableWidthInNanometers(MagnificationEnum.getCurrentMinSliceHeight());
    int xTravelRequired = cameraArrayRectangle.getWidth() +
                          nominalWidth +
                          2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters() -
                          imageableWidthOfSingleCamera - // imageableWidthOfFirstCamera
                          imageableWidthOfSingleCamera; // imageableWidthOfLastCamera;

    int numberOfSteps = calculateNumberOfScanSteps(xTravelRequired, meanStepSizeInNanoMeters) + 1;
    int yTravelRequired = nominalHeight + cameraArrayRectangle.getHeight() +
                          2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters();
    double estimatedTravel = (double)numberOfSteps * yTravelRequired;
    double estimatedTravelTime = estimatedTravel / PanelPositioner.getVelocityForScanMotionProfileInNanometersPerSecond();
    double estimatedTurnaroundTime = numberOfSteps * PanelPositioner.getAverageTurnaroundTimeInSeconds();
    return estimatedTravelTime + estimatedTurnaroundTime;
  }

  /**
   * Calculate the number of scan steps required to cover the x travel.
   *
   * Steps sizes can be variable and are stored in a List.
   *
   * @param xTravelRequired
   * @return the number of steps required to image the panel
   * @author Roy Williams
   */
  static protected int calculateNumberOfScanSteps(int xTravelRequired, int meanStepSizeInNanoMeters)
  {
    Assert.expect(xTravelRequired >= 0);

    int xTravel = 0;
    int numberOfSteps = 0;
    while (xTravel <= xTravelRequired)
    {
      xTravel += meanStepSizeInNanoMeters;

      numberOfSteps++;
    }
    return numberOfSteps;
  }

  /**
   * @author Roy Williams
   */
  public boolean contains(int reconstructionRegionId)
  {
    for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : _regionPairsSortedInExecutionOrder)
    {
      ReconstructionRegion region = regionPair.getFirst();
      if (region.getRegionId() == reconstructionRegionId)
      {
        return true;
      }
    }
    return false;
  }

  /**
   * @author Roy Williams
   */
  public AffineTransform getAffineTransform()
  {
    return _alignmentTransform;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
//  private Comparator<Pair<ReconstructionRegion, SystemFiducialRectangle>> _sortRegionComparator = new Comparator<Pair<ReconstructionRegion, SystemFiducialRectangle>>()
//  {
//    public int compare(Pair<ReconstructionRegion, SystemFiducialRectangle> lhs, Pair<ReconstructionRegion, SystemFiducialRectangle> rhs)
//    {
//      SubtypeAdvanceSettings lhsSubtypeAdvanceSetting = lhs.getFirst().getSubtypes().iterator().next().getSubtypeAdvanceSettings();
//      SubtypeAdvanceSettings rhsSubtypeAdvanceSetting = rhs.getFirst().getSubtypes().iterator().next().getSubtypeAdvanceSettings();
//      
//        if (lhsSubtypeAdvanceSetting.getHighestStageSpeedSetting().toDouble() == rhsSubtypeAdvanceSetting.getHighestStageSpeedSetting().toDouble())
//        {
//          if (lhsSubtypeAdvanceSetting.getUserGain().toDouble() == rhsSubtypeAdvanceSetting.getUserGain().toDouble())
//          {
//            // Leading edge is the max x because panel is being moved left to right.
//            int maxXofRegion1 = lhs.getSecond().getMaxX();
//            int maxXofRegion2 = rhs.getSecond().getMaxX();
//            if (maxXofRegion1 == maxXofRegion2)
//              return 0;
//            else
//              return (maxXofRegion1 > maxXofRegion2) ? -1 : 1;
//          }
//          else if (lhsSubtypeAdvanceSetting.getUserGain().toDouble() > rhsSubtypeAdvanceSetting.getUserGain().toDouble())
//           return 1;
//         else
//           return -1;
//        }
//        else if (lhsSubtypeAdvanceSetting.getHighestStageSpeedSetting().toDouble() > rhsSubtypeAdvanceSetting.getHighestStageSpeedSetting().toDouble())
//          return 1;
//        else 
//          return -1;
//      }
//  };
}
