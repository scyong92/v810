package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;

/**
 * @author Roy Williams
 */
public class ProjectionRequestThreadTaskHysterisisImagingOrderComparator implements Comparator<ProjectionRequestThreadTask>
{
  /**
   * @author Roy Williams
   */
  public int compare(ProjectionRequestThreadTask projectionRequestThreadTask1, ProjectionRequestThreadTask projectionRequestThreadTask2)
  {
    Assert.expect(projectionRequestThreadTask1 != null);
    Assert.expect(projectionRequestThreadTask2 != null);

    int xDiff = projectionRequestThreadTask1.xSortPosition() - projectionRequestThreadTask2.xSortPosition();
    if (xDiff != 0)
    {
      return xDiff < 0 ? -1 : 1;
    }

    int yDiff = projectionRequestThreadTask1.getProjectionInfo().getYStageWhereFirstRowImagedOnVerticalCenterOfCamera() -
                projectionRequestThreadTask2.getProjectionInfo().getYStageWhereFirstRowImagedOnVerticalCenterOfCamera();

    return yDiff == 0 ? 0 : (yDiff < 0) ? -1 : 1;
  }
}
