package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class DownloadProgramToCameraThreadTask extends ThreadTask<Object>
{
  private AbstractXrayCamera       _camera;
  private List<ScanPass>           _scanPasses;
  private List<ProjectionSettings> _projectionSettingsListForCamera;
  private boolean                  _cancelled;

  /**
   * @author Roy Williams
   */
  public DownloadProgramToCameraThreadTask(AbstractXrayCamera camera,
                                           List<ProjectionSettings> projectionSettingsListForCamera,
                                           List<ScanPass> scanPasses)
  {
    super("DownloadProgramToCameraThreadTask: " + camera.getId());

    Assert.expect(camera != null);
    Assert.expect(scanPasses != null);
    Assert.expect(scanPasses.size() > 0);
    Assert.expect(projectionSettingsListForCamera != null);

    _camera                          = camera;
    _scanPasses                      = scanPasses;
    _projectionSettingsListForCamera = projectionSettingsListForCamera;
    _cancelled                       = false;
  }

  /**
   * @author Roy Williams
   */
  public Object executeTask() throws Exception
  {
    // Between each step check to see if we should continue or cancel.
    if (_cancelled)
      return null;
    {
      _camera.enableTriggerDetection();
//      MechanicalConversions mechanicalConversions = _scanPasses.get(0).getMechanicalConversions();
      MechanicalConversions mechanicalConversions =  _projectionSettingsListForCamera.get(0).getMechanicalConversions();      
      _camera.setScan(_scanPasses,
                      mechanicalConversions.getNumberOfLinesToSkipDuringPositiveYScan(_camera),
                      mechanicalConversions.getNumberOfLinesToSkipDuringNegativeYScan(_camera),
                      _projectionSettingsListForCamera);
    }


    return null;
  }

  /**
   * @author Roy Williams
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Roy Williams
   */
  protected void cancel() throws XrayTesterException
  {
    _cancelled = true;
  }
}
