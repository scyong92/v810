package com.axi.v810.business.imageAcquisition;

import com.axi.v810.hardware.*;
import com.axi.util.Assert;


/**
 * @author Eddie Williamson
 */
public class ImageAcquisitionEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

  public static final ImageAcquisitionEventEnum IMAGE_ACQUISITION = new ImageAcquisitionEventEnum(++_index, "Image Acquisition, Image Acquistion");
  public static final ImageAcquisitionEventEnum DOWNLOAD_TEST_PROGRAM_TO_HARDWARE = new ImageAcquisitionEventEnum(++_index, "Image Acquisition, Download test program to hardware");
  public static final ImageAcquisitionEventEnum OPTICAL_IMAGE_ACQUISITION = new ImageAcquisitionEventEnum(++_index, "Optical Image Acquisition, Optical Image Acquistion");

  private String _name;

  /**
   * @author Eddie Williamson
   * @author George Booth
   */
  private ImageAcquisitionEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author George Booth
   */
  public String toString()
  {
    return _name;
 }
}
