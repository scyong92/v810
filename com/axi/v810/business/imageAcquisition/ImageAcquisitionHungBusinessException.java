package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author George A. David
 */
class ImageAcquisitionHungBusinessException extends BusinessException
{
  /**
   * @author George A. David
   */
  private ImageAcquisitionHungBusinessException(ImageAcquisitionHangReasonEnum hangReason)
  {
    super(createLocalizedString(hangReason));
  }

  /**
   * @author George A. David
   */
  private ImageAcquisitionHungBusinessException(ImageAcquisitionHangReasonEnum hangReason, Collection<Integer> ids)
  {
    super(createLocalizedString(hangReason, ids));
  }

  /**
   * @author Rex Shang
   */
  static ImageAcquisitionHungBusinessException getAlgorithmsHungException()
  {
    ImageAcquisitionHungBusinessException newException = new ImageAcquisitionHungBusinessException(ImageAcquisitionHangReasonEnum.ALGORITHMS);
    return newException;
  }

  /**
   * @author Rex Shang
   */
  static ImageAcquisitionHungBusinessException getSavingImagesHungException()
  {
    ImageAcquisitionHungBusinessException newException = new ImageAcquisitionHungBusinessException(ImageAcquisitionHangReasonEnum.SAVING_IMAGES);
    return newException;
  }

  /**
   * @author Rex Shang
   */
  static ImageAcquisitionHungBusinessException getAlgorithmsOrIrpsHungException()
  {
    ImageAcquisitionHungBusinessException newException = new ImageAcquisitionHungBusinessException(ImageAcquisitionHangReasonEnum.ALGORITHMS_IRPS);
    return newException;
  }

  /**
   * @author Rex Shang
   */
  static ImageAcquisitionHungBusinessException getAllIrpsHungException()
  {
    ImageAcquisitionHungBusinessException newException = new ImageAcquisitionHungBusinessException(ImageAcquisitionHangReasonEnum.ALL_IRPS);
    return newException;
  }

  /**
   * @author Rex Shang
   */
  static ImageAcquisitionHungBusinessException getSomeIrpsHungException(Collection<Integer> ids)
  {
    ImageAcquisitionHungBusinessException newException = new ImageAcquisitionHungBusinessException(ImageAcquisitionHangReasonEnum.SOME_IRPS, ids);
    return newException;
  }

  /**
   * @author Rex Shang
   */
  static ImageAcquisitionHungBusinessException getAllCamerasHungException()
  {
    ImageAcquisitionHungBusinessException newException = new ImageAcquisitionHungBusinessException(ImageAcquisitionHangReasonEnum.ALL_CAMERAS);
    return newException;
  }

  /**
   * @author Rex Shang
   */
  static ImageAcquisitionHungBusinessException getSomeCamerasHungException(Collection<Integer> ids)
  {
    ImageAcquisitionHungBusinessException newException = new ImageAcquisitionHungBusinessException(ImageAcquisitionHangReasonEnum.SOME_CAMERAS, ids);
    return newException;
  }

  /**
   * @author Rex Shang
   */
  static ImageAcquisitionHungBusinessException getNoTriggersReceivedHungException()
  {
    ImageAcquisitionHungBusinessException newException = new ImageAcquisitionHungBusinessException(ImageAcquisitionHangReasonEnum.NO_TRIGGERS_RECEIVED);
    return newException;
  }

  /**
   * @author Rex Shang
   */
  static ImageAcquisitionHungBusinessException getSomeTriggersReceivedHungException()
  {
    ImageAcquisitionHungBusinessException newException = new ImageAcquisitionHungBusinessException(ImageAcquisitionHangReasonEnum.SOME_TRIGGERS_RECEIVED);
    return newException;
  }
  
    /**
   * @author Swee Yee
   */
  static ImageAcquisitionHungBusinessException getProjectionConsumersNotReadyForScanPassHungException()
  {
    ImageAcquisitionHungBusinessException newException = new ImageAcquisitionHungBusinessException(ImageAcquisitionHangReasonEnum.PROJECTION_CONSUMERS_NOT_READY);
    return newException;
  }

  /**
   * @author George A. David
   * @author Rex Shang
   */
  private static LocalizedString createLocalizedString(ImageAcquisitionHangReasonEnum hangReason)
  {
    Assert.expect(hangReason != null);

    LocalizedString localString = new LocalizedString(hangReason.getKey(), null);

    return localString;
  }

  /**
   * Handles creating a localized string for those reasons that require specific ids, like some cameras or some irps.
   * @author George A. David
   * @author Rex Shang
   */
  private static LocalizedString createLocalizedString(ImageAcquisitionHangReasonEnum hangReason, Collection<Integer> ids)
  {
    Assert.expect(hangReason != null);
    Assert.expect(ids != null);

    LocalizedString localString = null;

    String idString = "";
    for (int id : ids)
      idString += id + " ";

    localString = new LocalizedString(hangReason.getKey(), new Object[] {idString});

    return localString;
  }

}
