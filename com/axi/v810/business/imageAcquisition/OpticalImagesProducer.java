package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public abstract class OpticalImagesProducer implements Observer
{    
    //Motion profile to use when moving stage, use the fastest
    protected PointToPointMotionProfileEnum _motionProfile = PointToPointMotionProfileEnum.PROFILE1;    
    
    // Administrative shared references.
    protected static Config _config;
    protected static int _sleepInterval = 200;
    
    // References to hardware objects
    protected static XrayTester _xRayTester;
    protected static PanelPositioner _panelPositioner;
    protected static PanelHandler _panelHandler;
    protected static HardwareObservable _hardwareObservable;
    //protected static OpticalImageAcquisitionProgressMonitor _progressMonitor;
    protected static PspEngine _pspEngine;
    protected static PspImageAcquisitionEngine _pspImageAcquisitionEngine;
    
    // Instance member to describe the translations for camera delay, stage travel, etc.
    private boolean _opticalImageAcquisitionInProgress = false;
    protected Object _abortCompleteCondition = new Object();
    private int _abortControlParticipants = 0;
    
    // Different ways to get out of imageAcquisition are abort (i.e. failure) and
    // a request to cancel from the user.
    private XrayTesterException _xrayTesterException = null;
    private boolean _userCanceledAcquisitionRequest = false;
    
    protected TestExecutionTimer _testExecutionTimer;
    protected List<? extends OpticalPointToPointScan> _opticalScanPath = null;        
    private int _numberOfPointToPointScan = -1;
    
    private static PerformanceLogUtil _performanceLog = PerformanceLogUtil.getInstance();
    
    protected static InspectionEventObservable _inspectionEventObservable;
    
    protected boolean _isPspOfflineReconstructionEnabled = isPspOfflineReconstructionEnabled();
        
    /**
     * @author Cheah Lee Herng
    */
    static
    {
        _config = Config.getInstance();
        _xRayTester = XrayTester.getInstance();        
        _panelPositioner = PanelPositioner.getInstance();
        _panelHandler = PanelHandler.getInstance();
        //_progressMonitor = OpticalImageAcquisitionProgressMonitor.getInstance();
        _hardwareObservable = HardwareObservable.getInstance();
        _inspectionEventObservable = InspectionEventObservable.getInstance();
        _pspEngine = PspEngine.getInstance();
        _pspImageAcquisitionEngine = PspImageAcquisitionEngine.getInstance();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void runAcquisitionSequence(TestExecutionTimer testExecutionTimer,
                                           List<? extends OpticalPointToPointScan> opticalScanPath,
                                           int expectedNumberOfPointToPointScan,
                                           String projectName,
                                           String opticalImageSetName) throws XrayTesterException
    {
      Assert.expect(testExecutionTimer != null);
      Assert.expect(opticalScanPath != null && opticalScanPath.size() > 0);
      Assert.expect(projectName != null);
      Assert.expect(opticalImageSetName != null);

      try
      {
        _testExecutionTimer = testExecutionTimer;
        _opticalImageAcquisitionInProgress = true;
        _opticalScanPath = opticalScanPath;

        if (isAbortInProgress())
            return;

        // We may not leave until the entire optical ScanPath has been completed.
        _testExecutionTimer.startOpticalImageAcquisitionTimer();

        _numberOfPointToPointScan = opticalScanPath.size();

        _performanceLog.logMilestone(PerformanceLogMilestoneEnum.OPTICAL_POINT_TO_POINT_SCAN_SIZE, _numberOfPointToPointScan);

        for (OpticalPointToPointScan currentOpticalPointToPointScan : opticalScanPath)
        {                
          if (_userCanceledAcquisitionRequest)
          {
            return;
          }
          if (_xrayTesterException != null)
            throw _xrayTesterException;

          if (_isPspOfflineReconstructionEnabled == false)
              runNextOpticalPointToPointScan(currentOpticalPointToPointScan, projectName, opticalImageSetName);
          else
              runNextOpticalPointToPointScanOffline(currentOpticalPointToPointScan, projectName, opticalImageSetName);

          postSurfaceMapOnOpticalImageEvent();
        }

        //_progressMonitor.pointToPointCompleted();

        _testExecutionTimer.stopOpticalImageAcquisitionTimer();
      }
      catch (XrayTesterException xte)
      {
        throw xte;
      }
      finally
      {
        _opticalImageAcquisitionInProgress = false;          
      }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void runAlignmentAcquisitionSequence(TestExecutionTimer testExecutionTimer,
                                                   List<? extends OpticalPointToPointScan> opticalScanPath,
                                                   int expectedNumberOfPointToPointScan) throws XrayTesterException
    {
      Assert.expect(testExecutionTimer != null);
      Assert.expect(opticalScanPath != null && opticalScanPath.size() > 0);      

      try
      {            
        _testExecutionTimer = testExecutionTimer;
        _opticalImageAcquisitionInProgress = true;
        _opticalScanPath = opticalScanPath;

        if (isAbortInProgress())
          return;

        // We may not leave until the entire optical ScanPath has been completed.
        _testExecutionTimer.startOpticalImageAcquisitionTimer();

        _numberOfPointToPointScan = opticalScanPath.size();

        _performanceLog.logMilestone(PerformanceLogMilestoneEnum.OPTICAL_POINT_TO_POINT_SCAN_SIZE, _numberOfPointToPointScan);
        
        for (OpticalPointToPointScan currentOpticalPointToPointScan : opticalScanPath)
        {
          if (_userCanceledAcquisitionRequest)
          {
            return;
          }
          if (_xrayTesterException != null)
            throw _xrayTesterException;

          runNextAlignmentOpticalPointToPointScan(currentOpticalPointToPointScan);

          postAlignmentSurfaceMapOnOpticalImageEvent();
        }

        //_progressMonitor.pointToPointCompleted();

        _testExecutionTimer.stopOpticalImageAcquisitionTimer();
      }
      finally
      {          
        _opticalImageAcquisitionInProgress = false;          
      }
    }
    
    /**
     * Abort includes the semantics for user and hardware aborts.
     *
     * @author Cheah Lee Herng
    */
    public boolean isAbortInProgress()
    {
        if (_xrayTesterException != null)
          return true;
        if (_userCanceledAcquisitionRequest)
          return true;
        return false;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void runNextOpticalPointToPointScan(OpticalPointToPointScan opticalPointToPointScan, String projectName, String opticalImageSetName) throws XrayTesterException
    {
      Assert.expect(_panelPositioner != null);
      Assert.expect(opticalPointToPointScan != null);
      Assert.expect(projectName != null);
      Assert.expect(opticalImageSetName != null);

      if (isAbortInProgress())
          return;

      if (_userCanceledAcquisitionRequest)
        return;

      // Take advantage of this last step to make sure there aren't any xrayTesterExceptions
      if (_xrayTesterException != null)
        throw _xrayTesterException;

      // Perform height map image operation
      _pspEngine.performHeightMapOperation(opticalPointToPointScan, projectName, opticalImageSetName);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    protected void runNextOpticalPointToPointScanOffline(OpticalPointToPointScan opticalPointToPointScan, String projectName, String opticalImageSetName) throws XrayTesterException
    {
       Assert.expect(opticalPointToPointScan != null);
       Assert.expect(projectName != null);
       Assert.expect(opticalImageSetName != null);
       
       if (isAbortInProgress())
         return;
        
       if (_userCanceledAcquisitionRequest)
         return;

       // Take advantage of this last step to make sure there aren't any xrayTesterExceptions
       if (_xrayTesterException != null)
         throw _xrayTesterException;
       
       // Perform offline height map image operation
       _pspEngine.performOfflineHeightMapOperation(opticalPointToPointScan, projectName, opticalImageSetName);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void runNextAlignmentOpticalPointToPointScan(OpticalPointToPointScan opticalPointToPointScan) throws XrayTesterException
    {
      Assert.expect(_panelPositioner != null);
      Assert.expect(opticalPointToPointScan != null);      

      if (isAbortInProgress())
          return;

      if (_userCanceledAcquisitionRequest)
        return;

      // Take advantage of this last step to make sure there aren't any xrayTesterExceptions
      if (_xrayTesterException != null)
        throw _xrayTesterException;
      
      // Perform alignment height map image operation
      _pspEngine.performAlignmentHeightMapOperation(opticalPointToPointScan);
    }
    
    /**
     * @author Cheah Lee Herng
    */
    protected boolean checkAllOpticalImagesReturned() throws XrayTesterException
    {
      return areAllOpticalImagesAcquired();
    }
    
    /**
     * This object is going to get notifications from OpticalImageAcqquisitionExceptionObservable.
     *
     * @author Cheah Lee Herng
    */
    public void update(Observable observable, Object object) 
    {
      Assert.expect(observable != null);
      Assert.expect(object != null);

      // On shutdown (from XrayTester) we will be notified and we are going to
      // make the currently downloaded program invalid.
      if (observable instanceof HardwareObservable)
      {
        if (object instanceof HardwareEvent)
        {
          HardwareEvent hardwareEvent = (HardwareEvent)object;
        }
      }
      else if (observable instanceof OpticalImageAcquisitionExceptionObservable)
      {
        Assert.expect(object instanceof XrayTesterException);
        abort((XrayTesterException)object);
      }
      else
      {
        Assert.expect(false);
      }
    }
    
    /**
     * @author Cheah Lee Herng
    */
    public synchronized void abort(XrayTesterException abortException)
    {
      Assert.expect(abortException != null);

      // Although this may be the first time the exception was set, we could already
      // be processing a user cancel.  In this case, the exception was a byproduct
      // of a user abort.  The currently running user abort will be allowed to continue.
      boolean exceptionWasSet = evaluateNewXrayTesterException(abortException);

      // We did (in previous step) record the fact that an exception did occur.
      if (_userCanceledAcquisitionRequest)
          return;

      if (exceptionWasSet == false)
          return;

      // Well that leaves us with the need to run the abort procedure.
      try
      {
          performAbortProcedure();
      }
      catch (XrayTesterException ex)
      {
          ex.printStackTrace();
      }
    }
    
    /**
     * Aborts panelPostioner, optical cameras, etc one at a time.  Each routine added to this
     * method is expected to eat exceptions
     *
     * @author Cheah Lee Herng
    */
    private synchronized void performAbortProcedure() throws XrayTesterException
    {
        try
        {
          incrementAbortControlParticipants();

          //_progressMonitor.stopMonitoring();          

          abortPanelPositioner();

          _pspEngine.abort();       
        }
        finally
        {
          if (_xrayTesterException != null)
          {
            System.out.println();
            System.out.println("####### Stacktrace from the exception that caused shutdown to occur:");
            _xrayTesterException.printStackTrace();

            System.out.println();
            String stackTraceExplanation = "####### Stacktrace from thread performing shutdown:";
            System.out.println(stackTraceExplanation);
            try
            {
              throw new XrayTesterException(new LocalizedString(stackTraceExplanation, null));
            }
            catch (XrayTesterException threadPerformingShutdown)
            {
              threadPerformingShutdown.printStackTrace();
            }
          }
          else
          {
            System.out.println("####### Shutdown due to user abort/cancel");
            try
            {
              throw new Exception();
            }
            catch (Exception ex)
            {
              ex.printStackTrace();
            }
          }

          // Last participant in the abort process will cause flag to be set denoting
          // done with abort.
          decrementAbortParticipants();
        }
    }
    
    /**     
     * @author Cheah Lee Herng
    */
    protected synchronized boolean evaluateNewXrayTesterException(XrayTesterException ex)
    {
        Assert.expect(ex != null);

        boolean firstTimeExceptionSet = false;

        // Is this the first exception we have ever seen?   Set this before we do anything
        // else that might result in subsequent exceptions.
        if (_xrayTesterException == null)
        {
            _xrayTesterException = ex;
            firstTimeExceptionSet = true;
            //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
            if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.OPTICAL_IMAGES_PRODUCER_DEBUG))
                System.out.println("XrayTesterException changed: " + _xrayTesterException.getClass().getName());
        }

        // Not the first exception we have seen below here.
        determineExceptionBasedOnPriority(ex);

        return firstTimeExceptionSet;
    }
   
    /**
     * @author Roy Williams
    */
    private synchronized void determineExceptionBasedOnPriority(XrayTesterException possibleNewXrayTesterException)
    {
        Assert.expect(possibleNewXrayTesterException != null);

        // If the current exception is ALREADY an interlock exception then we will keep the one we have.
        if (_xrayTesterException instanceof InterlockHardwareException)
        {
          clearPanelPostionerException();
          return;
        }

        // If there is an interlock door open then we will allow it to preempt the
        // current exception with another.
        if (possibleNewXrayTesterException instanceof InterlockHardwareException)
        {
          _xrayTesterException = possibleNewXrayTesterException;
          clearPanelPostionerException();
          //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
          if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.OPTICAL_IMAGES_PRODUCER_DEBUG))
            System.out.println("XrayTesterException changed: " + _xrayTesterException.getClass().getName());
          return;
        }

        // This possibleNewXrayTesterException is definitely NOT a InterlockHardwareException!
        // If this next statement is true, then we MUST pre-emptively change the current
        // _xrayTesterException with the ONE WE WANT... not the one it thinks it has!
        try
        {
          if (Interlock.getInstance().isInterlockOpen())
          {
            _xrayTesterException = InterlockHardwareException.getInterlockChainIsOpenException();
            clearPanelPostionerException();
            return;
          }
        }
        catch (XrayTesterException ex)
        {
          if (ex instanceof InterlockHardwareException)
          {
            // It could be we are the first to discover.
            _xrayTesterException = ex;
            clearPanelPostionerException();
            return;
          }
        }
        catch (Throwable throwable)
        {
          Assert.logException(throwable);
        }

        // See if this exception is a MotionControlHardwareException
        if (possibleNewXrayTesterException instanceof MotionControlHardwareException)
        {
          _xrayTesterException = possibleNewXrayTesterException;
          //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
          if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.OPTICAL_IMAGES_PRODUCER_DEBUG))
            System.out.println("XrayTesterException changed: " + _xrayTesterException.getClass().getName());
          return;
        }

        // The DigitalIoException wins!
        if (possibleNewXrayTesterException instanceof DigitalIoException)
        {
          _xrayTesterException = possibleNewXrayTesterException;
          //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
          if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.OPTICAL_IMAGES_PRODUCER_DEBUG))
            System.out.println("XrayTesterException changed: " + _xrayTesterException.getClass().getName());
          return;
        }
    }
    
    /**
     * @author Cheah Lee Herng
    */
    private void abortPanelPositioner()
    {
        try
        {
          _panelPositioner.abort();
        }
        catch (XrayTesterException ex)
        {
          evaluateNewXrayTesterException(ex);
        }
    }
   
    /**
    * @author Roy Williams
    */
    private void clearPanelPostionerException()
    {
        try
        {
          _panelPositioner.isScanPathDone();
        }
        catch (XrayTesterException ex)
        {
          // do nothing.   Some XrayTesterExceptions (like InterlockHardwareException)
          // cause the panel positioner to create an exception that is held and returned
          // asynchronously.   We must clear this or the cached exception will be
          // emitted much, much further downstream when you least suspect it.          
        }
    }
    
    /**
     * @author Roy Williams
    */
    protected void incrementAbortControlParticipants()
    {
        synchronized (_abortCompleteCondition)
        {
          _abortControlParticipants++;
        }
    }
    
    /**
     * @author Cheah Lee Herng
    */
    public XrayTesterException getXrayTesterException()
    {
        Assert.expect(_xrayTesterException != null);
        return _xrayTesterException;
    }

    /**
     * @author Cheah Lee Herng
    */
    public boolean hasXrayTesterException()
    {
        return _xrayTesterException != null;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public boolean checkForAbortInProgress() throws XrayTesterException
    {
        if (_xrayTesterException != null)
          throw _xrayTesterException;
        if (_userCanceledAcquisitionRequest)
          return true;
        return false;
    }

    /**
     * @author Roy Williams
    */
    protected void decrementAbortParticipants()
    {
        synchronized (_abortCompleteCondition)
        {
          if (_abortControlParticipants > 0)
          {
            _abortControlParticipants--;
            if (_abortControlParticipants == 0)
            {
              _abortCompleteCondition.notifyAll();
            }
          }
        }
    }   
    
    /**
     * @author Cheah Lee Herng
     */
    public synchronized void userAbort() throws XrayTesterException
    {
        // Insure this only happens once.
        if (_userCanceledAcquisitionRequest == false)
        {
          _userCanceledAcquisitionRequest = true;

          // Classic case of dynamic binding here.
          // Look for all the references on this call to performAbortProcedure() method.
          // Notice that ReconstructedImagesProducer also implements this method that
          // overrides this implementation.  ReconstructedImagesProducer will get called first
          // and it is up to that declaration to decide whether to call super.performAbortProcedure().
          //
          // See the default implementation here in ProjectionProducer.
          performAbortProcedure();
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void clearForNextRun()
    {
        _xrayTesterException = null;
        //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
        if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.OPTICAL_IMAGES_PRODUCER_DEBUG))
          System.out.println("XrayTesterException changed: null");
        _userCanceledAcquisitionRequest = false;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void postSurfaceMapOnOpticalImageEvent()
    {
      SurfaceMapInspectionEvent surfaceMapOnOpticalImageSurfaceMapEvent = 
              new SurfaceMapInspectionEvent(InspectionEventEnum.SURFACE_MAP_ON_OPTICAL_IMAGE);
      _inspectionEventObservable.sendEventInfo(surfaceMapOnOpticalImageSurfaceMapEvent);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void postAlignmentSurfaceMapOnOpticalImageEvent()
    {
      SurfaceMapInspectionEvent surfaceMapOnOpticalImageSurfaceMapEvent = 
              new SurfaceMapInspectionEvent(InspectionEventEnum.ALIGNMENT_SURFACE_MAP_ON_OPTICAL_IMAGE);
      _inspectionEventObservable.sendEventInfo(surfaceMapOnOpticalImageSurfaceMapEvent);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void postRunTimeSurfaceMapCompletedEvent()
    {
      SurfaceMapInspectionEvent runTimeSurfaceMapCompletedEvent = 
              new SurfaceMapInspectionEvent(InspectionEventEnum.RUNTIME_SURFACE_MAP_COMPLETED);
      _inspectionEventObservable.sendEventInfo(runTimeSurfaceMapCompletedEvent);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void postRunTimeAlignmentSurfaceMapCompletedEvent()
    {
      SurfaceMapInspectionEvent runTimeAlignmentSurfaceMapCompletedEvent = 
              new SurfaceMapInspectionEvent(InspectionEventEnum.RUNTIME_ALIGNMENT_SURFACE_MAP_COMPLETED);
      _inspectionEventObservable.sendEventInfo(runTimeAlignmentSurfaceMapCompletedEvent);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public boolean isPspOfflineReconstructionEnabled()
    {
      return _config.getBooleanValue(SoftwareConfigEnum.OFFLINE_RECONSTRUCTION);
    }
    
    public abstract boolean areAllOpticalImagesAcquired() throws XrayTesterException;
}
