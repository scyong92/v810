package com.axi.v810.business.imageAcquisition;

import com.axi.v810.hardware.OpticalCalibrationPlaneEnum;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.ethernetBasedLightEngine.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PspImageAcquisitionEngineVersion2 extends PspImageAcquisitionEngine 
{
  private static final int WAIT_FOR_TRIGGER_MODE_READY_IN_MILISECONDS   = 1;
  private static final int _MAXIMUM_WAIT_FOR_TRIGGER_MODE_READY_DURATION_IN_MILLISECONDS = 5000;  // 5 seconds
  
  private BooleanLock _completeEbleLock = new BooleanLock(false);
  private WorkerThread _ebleThread = new WorkerThread("EBLE worker thread");
  
  private XrayTesterException _xrayTesterException;
  
  /**
   * @author Cheah Lee Herng
   */
  protected void initializeOpticalImageAcquistion(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException 
  {
    // Do nothing
  }

  /**
   * @author Cheah Lee Herng
   */
  protected void performOpticalImageAcquisition(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException  
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalShiftEnum != null);
    
    try
    {
      _pspHardwareEngine.sendTriggerSequence(moduleEnum, opticalShiftEnum);
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  protected void finishOpticalImageAcquisition(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    
    _pspHardwareEngine.turnOffLED(moduleEnum);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void handleActualOpticalView(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalShiftEnum != null);
    
    // Initialize optical camera properties
    OpticalCameraSetting opticalCameraSetting = new OpticalCameraSetting(getFrontModuleCropImageOffsetXInPixel(),
                                                                         getRearModuleCropImageOffsetXInPixel());
    initializeOpticalCamera(moduleEnum, opticalCameraSetting);
    
    // Enable and turn on projector LED
    _pspHardwareEngine.turnOnLED(moduleEnum);
    
    if (opticalShiftEnum.equals(OpticalShiftNameEnum.WHITE_LIGHT))
      _pspHardwareEngine.displayWhiteLight(moduleEnum);
    else
      Assert.expect(false, "Invalid shift name enum");
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void handlePostOpticalView(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException 
  {
    _pspHardwareEngine.turnOffLED(moduleEnum);
  }

  /**
   * For module version 2, nothing need to be done to handle post image display 
   * because all are handled by Ethernet-based Light Engine.
   * 
   * @author Cheah Lee Herng 
   */
  public void handlePostImageDisplay(PspModuleEnum moduleEnum) throws XrayTesterException 
  {
    // Do nothing
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  protected String getCalibrationFullPath(PspModuleEnum moduleEnum, OpticalCalibrationProfileEnum opticalCalibrationProfileEnum) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalCalibrationProfileEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      return AbstractOpticalCamera.getInstance(OpticalCameraIdEnum.OPTICAL_CAMERA_1).getOpticalCalibrationDirectoryFullPath();
    else
      return AbstractOpticalCamera.getInstance(OpticalCameraIdEnum.OPTICAL_CAMERA_2).getOpticalCalibrationDirectoryFullPath();
  }

  /**
   * @author Cheah Lee Herng 
   */
  protected void runCalibrationImageAcquisition(PspModuleEnum moduleEnum, 
                                                OpticalCalibrationPlaneEnum planeEnum, 
                                                OpticalShiftNameEnum opticalShiftEnum, 
                                                String imageFullPath) throws XrayTesterException 
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(planeEnum != null);
    Assert.expect(opticalShiftEnum != null);
    Assert.expect(imageFullPath != null);
    
    final PspModuleEnum finalModuleEnum = moduleEnum;
    final OpticalCalibrationPlaneEnum finalPlaneEnum = planeEnum;
    final String finalImageFullPath = imageFullPath;
    
    _xrayTesterException = null; // reset this flag.
        
    _completeEbleLock.setValue(false);
    _ebleThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _pspHardwareEngine.acquireCalibrationImage(finalModuleEnum, finalPlaneEnum, finalImageFullPath, true);
        }
        catch(XrayTesterException ex)
        {
          _xrayTesterException = ex;
        }
        finally
        {
          _completeEbleLock.setValue(true);
        }
      }
    });

    int waitForTriggerModeReadyInMilliseconds = 0;
    boolean isTriggerModeReady = _pspHardwareEngine.isTriggerModeReady(finalModuleEnum);
    while (isTriggerModeReady == false && 
           waitForTriggerModeReadyInMilliseconds < _MAXIMUM_WAIT_FOR_TRIGGER_MODE_READY_DURATION_IN_MILLISECONDS)
    {
      try
      {
        Thread.sleep(WAIT_FOR_TRIGGER_MODE_READY_IN_MILISECONDS);
        waitForTriggerModeReadyInMilliseconds += WAIT_FOR_TRIGGER_MODE_READY_IN_MILISECONDS;
      }
      catch(InterruptedException ex)
      {
        // Do nothing
      }
      
      isTriggerModeReady = _pspHardwareEngine.isTriggerModeReady(finalModuleEnum);
    }
    
    if (isTriggerModeReady)
    {
      // Prepare light engine by turning on LED and fringe projection
      try
      {
        performOpticalImageAcquisition(finalModuleEnum, opticalShiftEnum);
      }
      catch (XrayTesterException xte)
      {
        _xrayTesterException = xte;
        throw _xrayTesterException;
      }
    }
    else
    {
      _xrayTesterException = EthernetBasedLightEngineHardwareException.getFailedToTriggerEthernetBasedLightEngineException(finalModuleEnum);
      throw _xrayTesterException;
    }
    
    try
    {    
      _completeEbleLock.waitUntilTrue();           
    }
    catch(InterruptedException ex)
    {
        // Do nothing
    }
    
    // Always turn off LED after operation
    finishOpticalImageAcquisition(moduleEnum);
    
    if (_xrayTesterException != null)
      throw _xrayTesterException;
  }

  /**
   * @author Cheah Lee Herng 
   */
  protected void runHeightMapImageAcquisition(final OpticalInspectionData opticalInspectionData, 
                                              OpticalShiftNameEnum opticalShiftEnum, 
                                              String imageFullPath) throws XrayTesterException 
  {
    Assert.expect(opticalInspectionData != null);
    Assert.expect(opticalShiftEnum != null);
    Assert.expect(imageFullPath != null);
    
    // Get Psp module enum
    final PspModuleEnum finalModuleEnum = opticalInspectionData.getPspModuleEnum();
    final String finalImageFullPath = imageFullPath;
    
    _xrayTesterException = null; // reset this flag.
        
    _completeEbleLock.setValue(false);
    _ebleThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          // Next, capture optical image
          if (_config.getBooleanValue(SoftwareConfigEnum.SAVE_PSP_INSPECTION_IMAGES))
          {
            _pspHardwareEngine.acquireHeightMapImage(finalModuleEnum, 
                                                     finalImageFullPath,
                                                     opticalInspectionData);
          }
          else
          {
            _pspHardwareEngine.acquireHeightMapImage(finalModuleEnum, 
                                                     null,
                                                     opticalInspectionData);
          }
        }
        catch(XrayTesterException ex)
        {
          _xrayTesterException = ex;
        }
        finally
        {
          _completeEbleLock.setValue(true);
        }
      }
    });        
    
    int waitForTriggerModeReadyInMilliseconds = 0;
    boolean isTriggerModeReady = _pspHardwareEngine.isTriggerModeReady(finalModuleEnum);
    while (isTriggerModeReady == false && 
           waitForTriggerModeReadyInMilliseconds < _MAXIMUM_WAIT_FOR_TRIGGER_MODE_READY_DURATION_IN_MILLISECONDS)
    {
      try
      {
        Thread.sleep(WAIT_FOR_TRIGGER_MODE_READY_IN_MILISECONDS);
        waitForTriggerModeReadyInMilliseconds += WAIT_FOR_TRIGGER_MODE_READY_IN_MILISECONDS;
      }
      catch(InterruptedException ex)
      {
        // Do nothing
      }
      
      isTriggerModeReady = _pspHardwareEngine.isTriggerModeReady(finalModuleEnum);
    }
    
    if (isTriggerModeReady)
    {
      // Prepare light engine by turning on LED and fringe projection
      try
      {
        performOpticalImageAcquisition(finalModuleEnum, opticalShiftEnum);
      }
      catch (XrayTesterException xte)
      {
        _xrayTesterException = xte;
        throw _xrayTesterException;
      }
    }
    else
    {
      _xrayTesterException = EthernetBasedLightEngineHardwareException.getFailedToTriggerEthernetBasedLightEngineException(finalModuleEnum);
      throw _xrayTesterException;
    }
    
    try
    {    
      _completeEbleLock.waitUntilTrue();           
    }
    catch(InterruptedException ex)
    {
        // Do nothing
    }
    
    // Always turn off LED after operation
    finishOpticalImageAcquisition(finalModuleEnum);
    
    if (_xrayTesterException != null)
    {
      throw _xrayTesterException;
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  protected void runSingleImageAcquisition(final OpticalInspectionData opticalInspectionData, OpticalShiftNameEnum opticalShiftEnum, String imageFullPath) throws XrayTesterException 
  {
    Assert.expect(opticalInspectionData != null);
    Assert.expect(opticalShiftEnum != null);
    Assert.expect(imageFullPath != null);
    
    // Get Psp module enum
    final PspModuleEnum finalModuleEnum = opticalInspectionData.getPspModuleEnum();
    final String finalImageFullPath = imageFullPath;   
    
    _xrayTesterException = null; // reset this flag.
        
    _completeEbleLock.setValue(false);
    _ebleThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _pspHardwareEngine.acquireSingleImage(finalModuleEnum, finalImageFullPath, true);
        }
        catch(XrayTesterException ex)
        {
          _xrayTesterException = ex;
        }
        finally
        {
          _completeEbleLock.setValue(true);
        }
      }
    });
  
    int waitForTriggerModeReadyInMilliseconds = 0;
    boolean isTriggerModeReady = _pspHardwareEngine.isTriggerModeReady(finalModuleEnum);
    while (isTriggerModeReady == false && 
           waitForTriggerModeReadyInMilliseconds < _MAXIMUM_WAIT_FOR_TRIGGER_MODE_READY_DURATION_IN_MILLISECONDS)
    {
      try
      {
        Thread.sleep(WAIT_FOR_TRIGGER_MODE_READY_IN_MILISECONDS);
        waitForTriggerModeReadyInMilliseconds += WAIT_FOR_TRIGGER_MODE_READY_IN_MILISECONDS;
      }
      catch(InterruptedException ex)
      {
        // Do nothing
      }
      
      isTriggerModeReady = _pspHardwareEngine.isTriggerModeReady(finalModuleEnum);
    }
    
    if (isTriggerModeReady)
    {
      // Prepare light engine by turning on LED and fringe projection
      try
      {
        performOpticalImageAcquisition(finalModuleEnum, opticalShiftEnum);
      }
      catch (XrayTesterException xte)
      {
        _xrayTesterException = xte;
        throw _xrayTesterException;
      }
    }
    else
    {
      _xrayTesterException = EthernetBasedLightEngineHardwareException.getFailedToTriggerEthernetBasedLightEngineException(finalModuleEnum);
      throw _xrayTesterException;
    }
    
    try
    {    
      _completeEbleLock.waitUntilTrue();           
    }
    catch(InterruptedException ex)
    {
        // Do nothing
    }
    
    // Always turn off LED after operation
    finishOpticalImageAcquisition(finalModuleEnum);
    
    if (_xrayTesterException != null)
      throw _xrayTesterException;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getFrontModuleCropImageOffsetXInPixel()
  {
    return _config.getIntValue(HardwareConfigEnum.FRONT_MODULE_CROP_IMAGE_OFFSET_X_IN_PIXEL);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getRearModuleCropImageOffsetXInPixel()
  {
    int rearModuleOffsetXFromLeftInPixel = PspHardwareEngine.getInstance().getMaxImageWidth() - 
                                           (_config.getIntValue(HardwareConfigEnum.REAR_MODULE_CROP_IMAGE_OFFSET_X_IN_PIXEL) + 
                                            PspHardwareEngine.getInstance().getImageWidth());
    return rearModuleOffsetXFromLeftInPixel;    
  }
  
  /**
   * @author Ying-Huan.Chu
   * @return the frontModuleCropImageOffsetXInPixel for Larger FOV model.
   */
  public int getLargerFovFrontModuleCropImageOffsetXInPixel()
  {
    //Ying-Huan.Chu - temporary commented Larger FOV feature for 5.7 release.
    //return _config.getIntValue(HardwareConfigEnum.LARGER_FOV_FRONT_MODULE_CROP_IMAGE_OFFSET_X_IN_PIXEL);
    return 0;
  }

  /**
   * @author Ying-Huan.Chu
   * @return the rearModuleCropImageOffsetXInPixel for Larger FOV model.
   */
  public int getLargerFovRearModuleCropImageOffsetXInPixel()
  {
    //Ying-Huan.Chu - temporary commented Larger FOV feature for 5.7 release.
    //int rearModuleOffsetXFromLeftInPixel = PspHardwareEngine.getInstance().getMaxImageWidth() - 
    //                                       (_config.getIntValue(HardwareConfigEnum.LARGER_FOV_REAR_MODULE_CROP_IMAGE_OFFSET_X_IN_PIXEL) + 
    //                                        PspHardwareEngine.getInstance().getImageWidth());
    //return rearModuleOffsetXFromLeftInPixel;
    return 0;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void checkIfHeightMapIsAvailable(PspModuleEnum pspModuleEnum) throws XrayTesterException
  {
    if (_pspHardwareEngine.isHeightMapAvailable(pspModuleEnum) == false)
    {
      throw EthernetBasedLightEngineHardwareException.getFailedToRetrieveHeightMapException(pspModuleEnum);
    }
  }
  
  /**
   * XCR-2413 Prompt error and ask user to run Optical Calibration task if there is no existing calibration data
   * This is to make sure we have correct optical calibration data exists for optical image acquisition.
   * 
   * @author Cheah Lee Herng 
   */
  protected void checkOpticalCalibrationDataExist() throws XrayTesterException
  {
    if (XrayTester.getInstance().isSimulationModeOn() == false)
    {
      String frontOpticalCalibrationDataFullPath  = FileName.getFrontOpticalCalibrationDataFullPath();
      String rearOpticalCalibrationDataFullPath   = FileName.getRearOpticalCalibrationDataFullPath();
      
      String frontOpticalCalibrationReferenceRawDataFullPath  = FileName.getFrontOpticalCalibrationReferenceRawDataFullPath();
      String rearOpticalCalibrationReferenceRawDataFullPath   = FileName.getRearOpticalCalibrationReferenceRawDataFullPath();
      
      PspSettingEnum pspSettingEnum = PspHardwareEngine.getSettingEnum();
      if (pspSettingEnum.equals(PspSettingEnum.SETTING_BOTH) || pspSettingEnum.equals(PspSettingEnum.SETTING_FRONT))
      {
        if (FileUtilAxi.exists(frontOpticalCalibrationDataFullPath) == false)
        {
          DatastoreException dex = new FileNotFoundDatastoreException(frontOpticalCalibrationDataFullPath);
          throw dex;
        }

        if (FileUtilAxi.exists(frontOpticalCalibrationReferenceRawDataFullPath) == false)
        {
          DatastoreException dex = new FileNotFoundDatastoreException(frontOpticalCalibrationReferenceRawDataFullPath);
          throw dex;
        }
      }
      
      if (pspSettingEnum.equals(PspSettingEnum.SETTING_BOTH) || pspSettingEnum.equals(PspSettingEnum.SETTING_REAR))
      {
        if (FileUtilAxi.exists(rearOpticalCalibrationDataFullPath) == false)
        {
          DatastoreException dex = new FileNotFoundDatastoreException(rearOpticalCalibrationDataFullPath);
          throw dex;
        }
        
        if (FileUtilAxi.exists(rearOpticalCalibrationReferenceRawDataFullPath) == false)
        {
          DatastoreException dex = new FileNotFoundDatastoreException(rearOpticalCalibrationReferenceRawDataFullPath);
          throw dex;
        }
      }
    }
  }
}
