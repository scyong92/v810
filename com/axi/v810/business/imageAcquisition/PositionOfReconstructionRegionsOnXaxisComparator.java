package com.axi.v810.business.imageAcquisition;

import com.axi.util.Assert;
import com.axi.v810.business.testProgram.ReconstructionRegion;
import java.util.Comparator;

/**
 * @author Roy Williams
 */
public class PositionOfReconstructionRegionsOnXaxisComparator implements Comparator<ReconstructionRegion>
{
  /**
   * @author Roy Williams
   */
  private boolean _sortBasedOnLeadingEdge;

  /**
   * @author Roy Williams
   */
  public PositionOfReconstructionRegionsOnXaxisComparator(boolean sortBasedOnLeadingEdge)
  {
    _sortBasedOnLeadingEdge = sortBasedOnLeadingEdge;
  }

  /**
   * @author Roy Williams
   */
  public int compare(ReconstructionRegion reconstructionRegion1,
                     ReconstructionRegion reconstructionRegion2)
  {
    Assert.expect(reconstructionRegion1 != null);
    Assert.expect(reconstructionRegion2 != null);

    if (_sortBasedOnLeadingEdge)
    {
      int maxYofRegion1 = getStartingLocation(reconstructionRegion1);
      int maxYofRegion2 = getStartingLocation(reconstructionRegion2);
      if (maxYofRegion1 == maxYofRegion2)
        return 0;
      return (maxYofRegion1 > maxYofRegion2) ? 1 : -1;
    }

    // else sorting based on trailing edge.
    int minYofRegion1 = getEndingLocation(reconstructionRegion1);
    int minYofRegion2 = getEndingLocation(reconstructionRegion2);
    if (minYofRegion1 == minYofRegion2)
      return 0;
    return (minYofRegion1 > minYofRegion2) ? 1 : -1;
  }

  /**
   * @author Roy Williams
   */
  public static int getStartingLocation(ReconstructionRegion region)
  {
    return region.getBoundingRegionOfPadsInNanoMeters().getMaxY();
  }

  /**
   * @author Roy Williams
   */
  public static int getEndingLocation(ReconstructionRegion region)
  {
    return region.getBoundingRegionOfPadsInNanoMeters().getMinY();
  }

}
