package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class ZeroNeighborsToPredictZHeightException extends XrayTesterException
{
  /**
   * @author Roy Williams
   * @edited by Siew Yeng - added parameter stageSpeed, userGain and fileName
   */
  public ZeroNeighborsToPredictZHeightException(String stageSpeed, String userGain, String fileName)
  {
    
    super(new LocalizedString("HW_TEST_ZERO_NEIGHBORS_TO_PREDICT_ZHEIGHT_KEY",
                                                      new Object[]{stageSpeed, userGain, fileName}));
  }
}
