package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.net.*;
import java.nio.*;

import com.axi.v810.business.*;
import com.axi.v810.datastore.config.*;

/**
 * Write a command over a socket provided by the base class to an Image
 * Reconstruction Processor.
 *
 * @author Roy Williams
 */
public class CommandSender extends ImageReconstructionSocket
{
  /**
   * Constructor creates the socket for sending commands through the base class
   * constructor.
   *
   * @author Roy Williams
   */
  public CommandSender(ImageReconstructionEngine imageReconstructionEngine,
                       InetAddress address,
                       int initialConnectPort)
      throws BusinessException
  {
    super("CommandSender",
          imageReconstructionEngine,
          address,
          initialConnectPort,
          ImageReconstructionMessageEnum.ConnectToCommandExecutor);
//    waitForReadyAcknowledgement(ImageReconstructionMessageEnum.ConnectToCommandExecutor);
  }

  /**
   * Send an individual message to the Image Reconstruction Processor.
   * <p>
   * This is a strategy pattern that all implementors of the ImageReconstructionMessage
   * class must fulfill.  The sequence is a required protocol as described by the
   * ImageReconstructionProcessor.
   *
   * @author Roy Williams
   */
  public synchronized void write(ByteBuffer buffer) throws BusinessException
  {
    if (ImageReconstructionEngine.isDeepSimulation() == false)
    {
      if (getImageReconstructionEngine().isSimulationModeOn() && ImageReconstructionSocket.isLoopback())
        return;
    }
    super.write(buffer);
  }
}
