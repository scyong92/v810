package com.axi.v810.business.imageAcquisition.imageReconstruction;

import com.axi.util.*;

/**
 * This class contains the results from an auto-focus search on a reconstruction
 * region.
 *
 * @author Horst Mueller
 */

public class FocusResult
{

  // Focus profile peak & edges.
  private int _zPeakInNanometers = 0;
  private int _zUpperEdgeInNanometers = 0;
  private int _zLowerEdgeInNanometers = 0;
  private int _numSurfaceModelNeighborsUsed = 0;
  private int _surfaceModelUncertainty = 0;
  private int _surfaceModelPredictedZ = 0;

  // Description of which reconstruction region and focus group
  // the FocusResult is associated with.
  int _reconstructionRegionId = 0;
  int _focusGroupId = 0;


  /**
   * @author Dave Ferguson
   */
  public FocusResult(int zPeakInNanometers,
                     int zUpperEdgeInNanometers,
                     int zLowerEdgeInNanometers,
                     int numSurfaceModelNeighborsUsed,
                     int surfaceModelUncertainty,
                     int surfaceModelPredictedZ,
                     int reconstructionRegionId,
                     int focusGroupId)
  {
    Assert.expect(reconstructionRegionId >= 0);
    Assert.expect(focusGroupId >= 0);
    Assert.expect(numSurfaceModelNeighborsUsed >= 0);
    Assert.expect(surfaceModelUncertainty >= 0);

    _zPeakInNanometers = zPeakInNanometers;
    _zUpperEdgeInNanometers = zUpperEdgeInNanometers;
    _zLowerEdgeInNanometers = zLowerEdgeInNanometers;
    _numSurfaceModelNeighborsUsed = numSurfaceModelNeighborsUsed;
    _surfaceModelUncertainty = surfaceModelUncertainty;
    _surfaceModelPredictedZ = surfaceModelPredictedZ;
    _reconstructionRegionId = reconstructionRegionId;
    _focusGroupId = focusGroupId;
  }

  /**
   * @author Dave Ferguson
   */
  public int getZPeakInNanometers()
  {
    return _zPeakInNanometers;
  }

  /**
   * @author Dave Ferguson
   */
  public int getZUpperEdgeInNanometers()
  {
    return _zUpperEdgeInNanometers;
  }

  /**
   * @author Dave Ferguson
   */
  public int getZLowerEdgeInNanometers()
  {
    return _zLowerEdgeInNanometers;
  }

  /**
   * @author Dave Ferguson
   */
  public int getReconstructionRegionId()
  {
    return _reconstructionRegionId;
  }

  /**
   * @author Dave Ferguson
   */
  public int getFocusGroupId()
  {
    return _focusGroupId;
  }

  /**
   * @author Roy Williams
   */
  public int getNumberSurfaceModelNeighborsUsed()
  {
    return _numSurfaceModelNeighborsUsed;
  }

  /**
   * @author Roy Williams
   */
  public float getSurfaceModelUncertainty()
  {
    return _surfaceModelUncertainty;
  }

  /**
   * @author Roy Williams
   */
  public int getSurfaceModelPredictedZ()
  {
    return _surfaceModelPredictedZ;
  }
}
