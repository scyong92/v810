package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.util.*;

/**
 * Abstract class provides a thread dedicated to read data from the input port
 * and then output the data to waiting Observables for distribution.
 *
 * @author Roy Williams
 */
public abstract class ImageReconstructionReceiverThread
              extends ImageReconstructionSocket
           implements Runnable
{

  private Thread _thread = null;
  private boolean _active = true;

  /**
   * @author Roy Williams
   */
  ImageReconstructionReceiverThread(ImageReconstructionEngine imageReconstructionEngine,
                                    InetAddress address,
                                    int port,
                                    ImageReconstructionMessageEnum usageID)
      throws BusinessException
  {
    this(imageReconstructionEngine, address, port, usageID, 0);
  }

  /**
   * @author Roy Williams
   */
  protected ImageReconstructionReceiverThread(ImageReconstructionEngine imageReconstructionEngine,
                                        InetAddress address,
                                        int port,
                                        ImageReconstructionMessageEnum usageID,
                                        int receiveBufferSize)
      throws BusinessException
  {
    super(imageReconstructionEngine, address, port, usageID, receiveBufferSize);

    waitForReadyAcknowledgement(usageID);

    initializeThread();
  }


  /**
   * Most derived classes will use this default implementation.
   * <p>
   * CommandResultReader wants to be on the thread of the caller and thus does not require it.
   *
   * @author Roy Williams
   */
  protected void initializeThread()
  {
    _active = true;
    _thread = new Thread(this, getClass().getName() + ": " +
                         getImageReconstructionEngine().getId());
    _thread.start();
  }

  /**
   * Expect derived classes to readData from the socked provided by the base class,
   * create instances of the Event, Enum, Exception required by the Observable
   * and send it out.
   *
   * @author Roy Williams
   */
  public final void run()
  {
    while (_active  && isConnected())
    {
      try
      {
        ByteBuffer byteBuffer = readData();
        if (byteBuffer == null || byteBuffer.capacity() == 0)
        {
          // Do nothing.  It is legal for a readData to return a zero length read.
          // System.out.println("ImageReconstructionReceiver: No bytes received from socket!  IRE: " + ire.getId());
        }
        else
        {
          // Read can be completed and then abort closes connections.  Must not
          // dispatch if we are aborting.
          if (_active && isConnected())
            dispatchDataToObservable(byteBuffer);
        }
      }
      catch(XrayTesterException xex)
      {
        if (_active)
          abort(xex);
      }
      catch (Throwable cause)
      {
        if (_active)
        {
          if (UnitTest.unitTesting())
            cause.printStackTrace();
          Assert.logException(cause);
        }
      }
    }
  }

  /**
   * Close the socket channel.   Do not be expecting to use it again.  You will
   * need to construct a new one.
   *
   * @author Roy Williams
   */
  public void close()
  {
    _active = false;
    super.close();
  }

  /**
   * Expect derived classes to readData from the socketChannel and get a
   * byteBuffer from this base class.
   *
   * @author Roy Williams
   */
  public abstract ByteBuffer readData() throws BusinessException;

  /**
   * Once data is read into the bytebuffer it must be read back in exactly the
   * format prescribed by the individual reader.
   *
   * @author Roy Williams
   */
  public abstract void dispatchDataToObservable(ByteBuffer readData) throws XrayTesterException;

}
