package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.communication.DisableSurfaceModeling;

/**
 * This enum defines a numerical index for messages sent to the ImageReconstrcutionProcessor.
 * Any message ID in this class will be interpreted as a command.   If the command
 * requires sending a payload (or data) to the IRP those fields should be defined
 * by the individual object implementing that specific command.
 *
 * @author Roy Williams
 */
public class ImageReconstructionMessageEnum extends com.axi.util.Enum
{
  private static Map<Integer, ImageReconstructionMessageEnum>
    _idToInstanceMap = new HashMap<Integer, ImageReconstructionMessageEnum>();

  // 1100 series - Establish connections usage/message id
  public static final ImageReconstructionMessageEnum ConnectToExceptionSender = new ImageReconstructionMessageEnum(1111);
  public static final ImageReconstructionMessageEnum ConnectToImageSender = new ImageReconstructionMessageEnum(1112);
  public static final ImageReconstructionMessageEnum ConnectToPriorityImageSender = new ImageReconstructionMessageEnum(1113);
  public static final ImageReconstructionMessageEnum ConnectToStateSender = new ImageReconstructionMessageEnum(1114);
  public static final ImageReconstructionMessageEnum ConnectToFocusResultSender = new ImageReconstructionMessageEnum(1115);
  public static final ImageReconstructionMessageEnum ConnectToResultSender = new ImageReconstructionMessageEnum(1116);
  public static final ImageReconstructionMessageEnum ConnectToCommandExecutor = new ImageReconstructionMessageEnum(1117);

  // 2100 series - control state machine of IRP.
  public static final ImageReconstructionMessageEnum Reset = new ImageReconstructionMessageEnum(2110);
  public static final ImageReconstructionMessageEnum Pause = new ImageReconstructionMessageEnum(2111);
  public static final ImageReconstructionMessageEnum Run = new ImageReconstructionMessageEnum(2112);
  public static final ImageReconstructionMessageEnum Abort = new ImageReconstructionMessageEnum(2113);
  public static final ImageReconstructionMessageEnum Resume = new ImageReconstructionMessageEnum(2114);
  public static final ImageReconstructionMessageEnum WaitForPriorCommandsToComplete = new ImageReconstructionMessageEnum(2115);

  // 2200 series - Initialization sequence of messages.
  public static final ImageReconstructionMessageEnum InitializeSystem = new ImageReconstructionMessageEnum(2210);
  public static final ImageReconstructionMessageEnum InitializeTestProgram = new ImageReconstructionMessageEnum(2211);
  public static final ImageReconstructionMessageEnum InitializeTest = new ImageReconstructionMessageEnum(2212);

  // 2300 series - Setting constants messages
  public static final ImageReconstructionMessageEnum SetHardwareConstants = new ImageReconstructionMessageEnum(2310);
  public static final ImageReconstructionMessageEnum SetCameraAttributes = new ImageReconstructionMessageEnum(2311);
  public static final ImageReconstructionMessageEnum SetIRPNetworkAttributes = new ImageReconstructionMessageEnum(2312);
  public static final ImageReconstructionMessageEnum ConnectToCameras = new ImageReconstructionMessageEnum(2313);
  // XCR-3325, Intermittent IRP-Camera Socket Connection Crash in Magnification Adjustment
  public static final ImageReconstructionMessageEnum GetCameraConnectionStatus = new ImageReconstructionMessageEnum(2314);

  // 2400 series
  public static final ImageReconstructionMessageEnum SetPanelAttributes = new ImageReconstructionMessageEnum(2410);
  public static final ImageReconstructionMessageEnum SetScanPassAttributes = new ImageReconstructionMessageEnum(2411);
  public static final ImageReconstructionMessageEnum SetProjectionAttributes = new ImageReconstructionMessageEnum(2412);
  public static final ImageReconstructionMessageEnum AddTestSubProgramTableEntry = new ImageReconstructionMessageEnum(2413);
  public static final ImageReconstructionMessageEnum AddReconstructionRegionTableEntry = new ImageReconstructionMessageEnum(2414);
  public static final ImageReconstructionMessageEnum AddFocusGroupTableEntry = new ImageReconstructionMessageEnum(2415);
  public static final ImageReconstructionMessageEnum AddSliceTableEntry = new ImageReconstructionMessageEnum(2416);
  public static final ImageReconstructionMessageEnum AddFocusInstructionTableEntry = new ImageReconstructionMessageEnum(2417);
  public static final ImageReconstructionMessageEnum AddFocusSearchParameterTableEntry = new ImageReconstructionMessageEnum(2418);
  public static final ImageReconstructionMessageEnum AddNeighborTableEntry = new ImageReconstructionMessageEnum(2419);
  public static final ImageReconstructionMessageEnum AddScanPassTableEntry = new ImageReconstructionMessageEnum(2420);
  public static final ImageReconstructionMessageEnum AddVelocityMappingDataEntry = new ImageReconstructionMessageEnum(2421);

  public static final ImageReconstructionMessageEnum TestProgramComplete = new ImageReconstructionMessageEnum(2450);

  // 2500 series
  public static final ImageReconstructionMessageEnum SetCalibratedXRaySpotLocation = new ImageReconstructionMessageEnum(2510);
  public static final ImageReconstructionMessageEnum SetCalibratedMagnification = new ImageReconstructionMessageEnum(2511);
  public static final ImageReconstructionMessageEnum SetCalibratedSystemFiducialLocation = new ImageReconstructionMessageEnum(2512);
  public static final ImageReconstructionMessageEnum SetEdgeSpreadFunctionRate = new ImageReconstructionMessageEnum(2513);
  //
  public static final ImageReconstructionMessageEnum AddFocusResult = new ImageReconstructionMessageEnum(2610);
  public static final ImageReconstructionMessageEnum ReReconstructSpecificRegion = new ImageReconstructionMessageEnum(2611);
  public static final ImageReconstructionMessageEnum MarkReconstructionRegionComplete = new ImageReconstructionMessageEnum(2612);
  public static final ImageReconstructionMessageEnum SetRuntimeAlignmentMatrix = new ImageReconstructionMessageEnum(2613);
  // Chnee Khang Wah, 2013-06-17, New PSH Handling
  public static final ImageReconstructionMessageEnum KickStartSpecificRegion = new ImageReconstructionMessageEnum(2614); 

  // Used with VirtualLive
  public static final ImageReconstructionMessageEnum ModifyFocusInstruction    = new ImageReconstructionMessageEnum(2710);
  public static final ImageReconstructionMessageEnum ModifyFocusProfileShape   = new ImageReconstructionMessageEnum(2711);
  public static final ImageReconstructionMessageEnum DisableSurfaceModeling    = new ImageReconstructionMessageEnum(2712);
  public static final ImageReconstructionMessageEnum ResetThinPlateSpline      = new ImageReconstructionMessageEnum(2713);
  public static final ImageReconstructionMessageEnum EnableSurfaceModeling    = new ImageReconstructionMessageEnum(2714);

  // Wei Chin for Variable Magnification
  public static final ImageReconstructionMessageEnum ResetSurfaceModel      = new ImageReconstructionMessageEnum(2810);
  
  // Wei Chin for XXL
  public static final ImageReconstructionMessageEnum SetAutoFocusAttribues = new ImageReconstructionMessageEnum(2910);  

  // 4000 series - query IRP for a result to be returned by its result sender
  public static final ImageReconstructionMessageEnum GetVersion = new ImageReconstructionMessageEnum(4000);

  /**
   * @author Roy Williams
   */
  public ImageReconstructionMessageEnum(int id)
  {
    super(id);

    _idToInstanceMap.put(new Integer(id), this);
  }

  /**
   * @author Rex Shang
   */
  public static ImageReconstructionMessageEnum getEnumById(int id)
  {
    ImageReconstructionMessageEnum me = _idToInstanceMap.get(id);

    Assert.expect(me != null, "Invalid id");
    return me;
  }

}
