package com.axi.v810.business.imageAcquisition.imageReconstruction;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * Thrown when we get a null pointer back from allocating native memory.
 *
 * @author Roy Williams
 */
public class FailedImageMemoryAllocationBusinessException extends ImageReconstructionProcessorCommunicationException
{
  /**
   * @author Roy Williams
   */
  public FailedImageMemoryAllocationBusinessException(ImageReconstructionEngine ire,
                                                      String ipAddress,
                                                      int portNumber,
                                                      int regionId,
                                                      int slice,
                                                      int width,
                                                      int height)
  {
    super(ire, new LocalizedString("BS_FAILED_IMAGE_MEMORY_ALLOCATION_KEY",
                                   new Object[] {ipAddress,
                                   Integer.toString(portNumber),
                                   Integer.toString(regionId),
                                   Integer.toString(slice),
                                   Integer.toString(width),
                                   Integer.toString(height)}));

    Assert.expect(ipAddress != null);
    Assert.expect(portNumber > 0);
  }
}
