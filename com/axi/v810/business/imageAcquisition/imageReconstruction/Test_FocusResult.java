package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.io.*;

import com.axi.util.*;

/**
 * @author Dave Ferguson
 */
class Test_FocusResult extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public Test_FocusResult()
  {
  }


  /**
   * @author Dave Ferguson
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    FocusResult focusResult = null;

    try
    {
      // Bad Reconstruction Region Id, expect an assertion failure
      focusResult = new FocusResult(1,2,3,4,5,6,-7,8);

      Expect.expect(false);
    }
    catch ( AssertException ae )
    {
      // do nothing, we expect an assertion failure here!
    }

    try
    {
      // Bad Focus Group Id, expect an assertion failure
      focusResult = new FocusResult(1,2,3,20,1,1,1,-5);

      Expect.expect(false);
    }
    catch ( AssertException ae )
    {
      // do nothing, we expect an assertion failure here!
    }

    focusResult = new FocusResult(0,0,0,0,0,0,0,0);
    Expect.expect(focusResult.getZPeakInNanometers() == 0);
    Expect.expect(focusResult.getZUpperEdgeInNanometers() == 0);
    Expect.expect(focusResult.getZLowerEdgeInNanometers() == 0);
    Expect.expect(focusResult.getNumberSurfaceModelNeighborsUsed() == 0);
    Expect.expect(focusResult.getSurfaceModelUncertainty() == 0);
    Expect.expect(focusResult.getSurfaceModelPredictedZ() == 0);
    Expect.expect(focusResult.getReconstructionRegionId() == 0);
    Expect.expect(focusResult.getFocusGroupId() == 0);

    focusResult = new FocusResult(200000,150000,250000,1,2,4,42,1);
    Expect.expect(focusResult.getZPeakInNanometers() == 200000);
    Expect.expect(focusResult.getZUpperEdgeInNanometers() == 150000);
    Expect.expect(focusResult.getZLowerEdgeInNanometers() == 250000);
    Expect.expect(focusResult.getNumberSurfaceModelNeighborsUsed() == 1);
    Expect.expect(focusResult.getSurfaceModelUncertainty() == 2);
    Expect.expect(focusResult.getSurfaceModelPredictedZ() == 4);
    Expect.expect(focusResult.getReconstructionRegionId() == 42);
    Expect.expect(focusResult.getFocusGroupId() == 1);

    focusResult = new FocusResult(-100,-110,-90,11,22,33,100,0);
    Expect.expect(focusResult.getZPeakInNanometers() == -100);
    Expect.expect(focusResult.getZUpperEdgeInNanometers() == -110);
    Expect.expect(focusResult.getZLowerEdgeInNanometers() == -90);
    Expect.expect(focusResult.getNumberSurfaceModelNeighborsUsed() == 11);
    Expect.expect(focusResult.getSurfaceModelUncertainty() == 22);
    Expect.expect(focusResult.getSurfaceModelPredictedZ() == 33);
    Expect.expect(focusResult.getReconstructionRegionId() == 100);
    Expect.expect(focusResult.getFocusGroupId() == 0);
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute(new Test_FocusResult());
  }
}
