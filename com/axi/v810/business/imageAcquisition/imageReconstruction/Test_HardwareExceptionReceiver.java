package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Test class for the ImageAcquisitionEngine communication exceptions from the IRE/IRP.
 *
 * @author Roy Williams
 */
public class Test_HardwareExceptionReceiver extends UnitTestWithSupportingProcesses implements Observer
{
  private String _baseFileName = "IRPSpewExceptions.";
  private ImageAcquisitionEngine _imageAcquisitionEngine = null;
  private File _outputFile = null;
  private static Config _config = Config.getInstance();
  private TimerUtil _timer = new TimerUtil();
  private int _accumulatedTime = 0;
  private int _timeout = 10000; // milliseconds
  private PrintWriter _logger = null;

  /**
   * @author Roy Williams
   */
  public Test_HardwareExceptionReceiver() throws DatastoreException, NullPointerException, FileNotFoundException
  {
    super();

    _logger = new PrintWriter(new File("Test_HardwareExceptionReceiver.log"));
      _config.loadIfNecessary();
    }

  /**
   * @author Bob Balliew
   */
  public void logMsg(String msg)
  {
    if (_logger != null)
    {
      Assert.expect(msg != null);
      _logger.println("Test_HardwareExceptionReceiver " + (new Date()).toString() + " " + Thread.currentThread().getId() + " '" + msg + "'");
      _logger.flush();
    }
  }

  /**
   * @author Bob Balliew
   */
  private synchronized int getAccumulatedTime()
  {
    return _accumulatedTime;
  }

  /**
   * @author Bob Balliew
   */
  private synchronized void incrementAccumulatedTime(int deltaTime)
  {
    logMsg("incrementAccumulatedTime by " + deltaTime + " milliseconds.");
    _accumulatedTime += deltaTime;
  }

  /**
   * @author Bob Balliew
   */
  private synchronized void setAccumulatedTime(int time)
  {
    logMsg("setAccumulatedTime to " + time + " milliseconds.");
    _accumulatedTime = time;
  }

  /**
   * @author Roy Williams
   */
  public void test(BufferedReader stdin, PrintWriter stdout)
  {
    try
    {
      // startup the IRPs.
      logMsg("test started.");
      int port = starupIrpExceptionSpewer(stdin, stdout);
      logMsg("IRPSpewExceptions started on port " + port + ".");
      Expect.expect(port > 0);

      if (port > 0)
      {
        setConfigFileValues(port);
        ImageAcquisitionEngine.setDebug(true);
        _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
        _imageAcquisitionEngine.getReconstructedImagesProducer().setupUnitTestForHardwareExceptionReceiver(this);
        ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0).establishCommunicationPathsToImageReconstructionProcessor();
      }
    }
    catch (Exception e)
    {
      logMsg("Exception " + e.toString() + " encountered.");
      e.printStackTrace();
    }
    finally
    {
      BooleanLock waitHere = new BooleanLock(true);
      _timer.start();

      while (waitHere.isTrue())
      {
        try
        {
          Thread.sleep(200);
          logMsg("sleep(200)");
          incrementAccumulatedTime(200);
          if (getAccumulatedTime() > _timeout)
            break;
        }
        catch (InterruptedException ex)
        {
          // do nothing.
        }
      }

      cleanupIrpExceptionSpewer();
      logMsg("IRPSpewExceptions terminated.");
      Assert.expect(_logger != null);
      _logger.close();
      _logger = null;
    }
  }

  /**
   * @author Roy Williams
   */
  private void setConfigFileValues(int port) throws IPaddressException, DatastoreException
  {
    String irpAddress = IPaddressUtil.getRemoteLoopbackAddress().getHostAddress();
    _config.setValueInMemoryOnly(HardwareConfigEnum.NUMBER_OF_IMAGE_RECONSTRUCTION_ENGINES, new Integer(1));
    _config.setValueInMemoryOnly(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_0_IP_ADDRESS_1, irpAddress);
    _config.setValueInMemoryOnly(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_0_IP_ADDRESS_2, irpAddress);
    _config.setValueInMemoryOnly(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_0_PORT, new Integer(port));
  }

  /**
   * @return the port where the spewer was started.
   * @author Roy Williams
   */
  private synchronized int starupIrpExceptionSpewer(BufferedReader stdin, PrintWriter stderr)
      throws IOException, InterruptedException
  {
    // setup for launch.
    ImageReconstructionEngine.setDeepSimulation(true);
    ImageReconstructionSocket.setLoopback(false);
    final int maxWaitForLaunchedProcessToRespondInMilliseconds = 30000;
    final int waitTimeInMilliseconds = 100;
    final int loopTimes = (int)Math.ceil(((double) maxWaitForLaunchedProcessToRespondInMilliseconds)/((double) waitTimeInMilliseconds));

        File baseDir = new File(getTestDataDir());

        if (baseDir.exists() == false)
          baseDir.mkdirs();

    logMsg("Ensure data directory is present.");

    PrintWriter logConfig = new PrintWriter(new File(getTestDataDir() + File.separator + "IRPInstrument.cfg"));
    logConfig.println("reconstructionRegions = all");
    logConfig.flush();
    logConfig.close();

    logMsg("Ensure IRPInstrument.cfg file is present.");

    BufferedReader stdoutReader = launch(_baseFileName + "exe ", getUnitTestSystemEnvironment(), baseDir, false, stderr);

    logMsg("launch " + _baseFileName + ".");

    for (int i = 0; i < loopTimes; ++i)
    {
      if (stdoutReader.ready())
      {
        logMsg("Read stdout from " + _baseFileName + ".");
        String portNumberString = stdoutReader.readLine();
        logMsg("Content of stdout from " + _baseFileName + " was '" + portNumberString + "'.");
        int irpPortSelected = Integer.parseInt(portNumberString);
        _outputFile = new File(getTestDataDir() + _baseFileName + portNumberString);

        if (_outputFile.exists())
          _outputFile.delete();

    return irpPortSelected;
  }

      logMsg("Waiting for output on stdout from " + _baseFileName + ".");
      wait(waitTimeInMilliseconds);
    }

    throw new IOException("IrpExceptionSpewer failed to start server and return port number in " + maxWaitForLaunchedProcessToRespondInMilliseconds + " milliseconds.");
  }

  /**
   * @author Roy Williams
   */
  private void cleanupIrpExceptionSpewer()
  {
    terminateAllLaunchedProcesses(20000);

    if (_outputFile == null)  return;

    // Cat the log files into the expect file for this unit test.  We have
    // sent the output into seperate files... otherwise, the output from our
    // IRP's would be interleaved and non deterministic in order.
    if (_outputFile.exists())
    {
      logMsg("Read contents of '" + _outputFile.getName() + "'.");
      BufferedReader fileReader = null;

      try
      {
        fileReader = new BufferedReader(new FileReader(_outputFile));
        String line = null;
        System.out.println();
        System.out.println("Output from file: " + _outputFile.getCanonicalFile());

        while ((line = fileReader.readLine()) != null)
          System.out.println(line);
      }
      catch (Exception e)
      {
        Assert.expect(false);
      }
      finally
      {
        if (fileReader != null)
          try
          {
            fileReader.close();
          }
          catch (Exception e)
          {
            Assert.expect(false);
          }

        // Finally, clean up the output files from the launched processes.
        _outputFile.delete();
      }
    }
  }

  /**
   * @author Roy Williams
   */
  public synchronized void update(Observable observable, Object object)
  {
    logMsg("update called.");
    setAccumulatedTime(0);

    if (object instanceof XrayTesterException)
    {
      // Always, always let abort function choose whether this exception is more
      // important than the one it is currently servicing.
      _timer.restart();
      XrayTesterException xrayTesterException = ((XrayTesterException)object);
      System.out.println(xrayTesterException.getLocalizedMessage());
      System.out.println("=====================================================");
    }
    else
      Assert.expect(false);
  }

  /**
   * @author Roy Williams
   */
  public static void main(String[] args)
  {
    //Utils class allows backup/restore of config files
    TestUtils utils = new TestUtils();
    UnitTest unitTest = null;

    try
    {
      unitTest = new Test_HardwareExceptionReceiver();
      UnitTest.execute(unitTest);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    finally
    {
      //Finally, restore the hardware.config
      if ((utils != null) && (unitTest != null))
        utils.restoreHWConfig(unitTest.getTestSourceDir());
    }
  }
}
