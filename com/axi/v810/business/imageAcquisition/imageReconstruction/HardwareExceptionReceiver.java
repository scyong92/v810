
package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.net.*;
import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Reads from a socket connected to an IRP to collect Exceptions that may be
 * occuring in the ImageReconstructionProcessor.
 *
 * @author Roy Williams
 */
public class HardwareExceptionReceiver extends ImageReconstructionReceiverThread
{
  private static boolean                             _debug              = false;
  private static ImageReconstructionProcessorManager _imageReconstructionProcessorManager =
      ImageReconstructionProcessorManager.getInstance();

  /**
   * @author Roy Williams
   */
  public HardwareExceptionReceiver(ImageReconstructionEngine imageReconstructionEngine,
                                   InetAddress address,
                                   int port)
      throws BusinessException
  {
    super(imageReconstructionEngine, address, port, ImageReconstructionMessageEnum.ConnectToExceptionSender);
  }

  /**
   * @author Roy Williams
   */
  public void dispatchDataToObservable(ByteBuffer byteBuffer) throws XrayTesterException
  {
    Assert.expect(byteBuffer != null);
    int keyPosition = byteBuffer.getInt();
    int keyLength = byteBuffer.getInt();
    int firstParameterBlockPosition = byteBuffer.getInt();
    int numberParameterBlocks = byteBuffer.getInt();
    int messagePosition = byteBuffer.getInt();
    int messageLength = byteBuffer.getInt();

    String originalKey = getStringFromByteBuffer(byteBuffer, keyPosition, keyLength);
    String rawMessageFromIrp = getStringFromByteBuffer(byteBuffer, messagePosition, messageLength);
    String parmsFromIrp[] = getParms(byteBuffer, firstParameterBlockPosition, numberParameterBlocks);

    // The next few statements will try to improve on a localizedException.  The
    // rawMessageFromIrp is a default only.
    String localizedWrappedException = rawMessageFromIrp;
    ImageReconstructionEngine imageReconstructionEngine = getImageReconstructionEngine();
    String irpNumber = imageReconstructionEngine.getId().toString();
    String newKey = "IRP_EXCEPTION_KEY";
    if (ImageReconstructionProcessorException.isExceptionResultOfPoorAlignmentPointSelection(originalKey))
    {
      newKey = ImageReconstructionProcessorException.poorAlignmentPointSelectionKey();
    }
    else if (XrayCameraHardwareException.isXrayCameraError(originalKey))
    {
      List<String> args = new ArrayList<String>(parmsFromIrp.length);
      for (int i=0; i<parmsFromIrp.length; i++)
        args.add(parmsFromIrp[i]);
      XrayCameraHardwareException xrayCameraException = XrayCameraHardwareException.getException(originalKey, args);
      localizedWrappedException = xrayCameraException.getLocalizedMessageWithoutHeaderAndFooter();
    }
    else
    {
      LocalizedString localizedString = new LocalizedString(originalKey, parmsFromIrp);
      localizedWrappedException = StringLocalizer.keyToString(localizedString);
    }

    // Wrap/encapsulate the exception in one that will identify the IRP.
    ImageReconstructionProcessorException imageReconstructionProcessorException =
      new ImageReconstructionProcessorException(
        newKey,
        irpNumber,
        localizedWrappedException,
        originalKey);

    if (UnitTest.unitTesting() == false)
    {
      System.out.println(rawMessageFromIrp);
    }
    System.out.println(imageReconstructionProcessorException.toString());

    // Tell the ImageReconstructionProcessorManager that it should restart the
    // IRP's next time it is asked to startup.
    _imageReconstructionProcessorManager.setStartupRequired();

    throw imageReconstructionProcessorException;
  }

  /**
   *  This exception is used to manually test in the DEBUGGER how an exception
   *  arrives and is handled when arriving from an IRP.   You must adopt the
   *  host process.
   *
   * @author Roy Williams
   */
  private void throwExceptionForTestingInDebugger() throws XrayTesterException
  {
    ImageReconstructionEngine imageReconstructionEngine = getImageReconstructionEngine();

    // NOTE: THIS IS FOR IRE0 ONLY.   The remaining IRP's do not do this.   IRE0
    // is particularly helpful because it also (exclusively) participates in alignment.
    if (imageReconstructionEngine.getId().equals(ImageReconstructionEngineEnum.IRE0))
    {
      boolean throwException = false;
      while (throwException == false)
      {
        try
        {
          Thread.sleep(1000);
        }
        catch (InterruptedException ex)
        {
          // do nothing.   Waiting till we throw exception.
        }
        if (throwException)
        {
          ImageReconstructionProcessorException imageReconstructionProcessorException =
            new ImageReconstructionProcessorException(
              "ASSERT_EXCEPTION_KEY",
              "0",
              "abc",
              "123");
          throw imageReconstructionProcessorException;
        }
      }
    }
  }

  /**
   * @author Roy Williams
   */
  private String getStringFromByteBuffer(ByteBuffer byteBuffer, int startingPosition, int numberBytes)
  {
    byteBuffer.rewind();
    byteBuffer.position(startingPosition);
    StringBuffer messageBuffer = new StringBuffer();
    for (int i = 0; i < numberBytes; i++)
    {
      char ch = (char) byteBuffer.get();
      // CR28907: Do NOT do a check to see if the character is a control character.
      // Some of the messages coming up from the IRP's are Windows generated messages.
      // These messages and those coming directly from an "Error:" in the camera
      // can have "\n\r".   Roy   4/30/2007
      messageBuffer.append(ch);
    }
    String str = messageBuffer.toString();
    byteBuffer.rewind();
    return str;
  }

  /**
   * @author Roy Williams
   */
  private String[] getParms(ByteBuffer byteBuffer, int firstParameterBlockPosition, int numberParameterBlocks)
  {
    int paramterInfoSize = JavaTypeSizeEnum.INT.getSizeInBytes() +
                           JavaTypeSizeEnum.INT.getSizeInBytes();

    String parms[] = new String[numberParameterBlocks];
    for (int i=0; i<numberParameterBlocks; i++)
    {
      int parameterOffset = firstParameterBlockPosition + (i * paramterInfoSize);
      byteBuffer.rewind();
      byteBuffer.position(parameterOffset);
      int relativeOffsetFromFirstByteOfExceptionBlock = byteBuffer.getInt();
      int lengthOfParameter = byteBuffer.getInt();
      parms[i] = getStringFromByteBuffer(byteBuffer,
                                         relativeOffsetFromFirstByteOfExceptionBlock,
                                         lengthOfParameter);
    }
    return parms;
  }

  /**
   * @author Roy Williams
   */
  public ByteBuffer readData() throws BusinessException
  {
    ByteBuffer byteBuffer = getByteBuffer(JavaTypeSizeEnum.INT.getSizeInBytes());
    int bytesRead = read(byteBuffer);
    if (isConnected() == false)
      return null;

    byteBuffer.rewind();
    int  moreBytesToRead = byteBuffer.getInt();

    if (moreBytesToRead > 0)
    {
      // Pull the entire message across from the IRP.
      int totalRead = 0;
      ByteBuffer returnBuffer = getByteBuffer(moreBytesToRead);
      int capacity = byteBuffer.capacity();
      while (totalRead < capacity)
      {
        bytesRead = read(returnBuffer);
        if (isConnected() == false)
          return returnBuffer;
        totalRead += bytesRead;
      }
      Assert.expect(totalRead == moreBytesToRead);
      returnBuffer.rewind();
      return returnBuffer;
    }
    else
      return null;
  }

  /**
   * @author Roy Williams
   */
  public static void setDebug(boolean state)
  {
    _debug = state;
  }
}
