package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.net.*;
import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.communication.*;
import com.axi.v810.util.*;

/**
 * Reads from a socket attached to the Image Reconstruction Processor to receive
 * generic information.
 *
 * @author Rex Shang
 */
public class ImageReconstructionInformationReceiver extends ImageReconstructionReceiverThread
{
  private static boolean _debug = false;
  private static String _me = "ImageReconstructionInfoReceiver";

  private List<SynchronizedInformationMessage> _infoReceiverObservers = null;

  /**
   * @author Rex Shang
   */
  public ImageReconstructionInformationReceiver(
      ImageReconstructionEngine imageReconstructionEngine,
      InetAddress address,
      int port
      ) throws BusinessException
  {
    super(imageReconstructionEngine, address, port, ImageReconstructionMessageEnum.ConnectToResultSender);

    _infoReceiverObservers = new ArrayList<SynchronizedInformationMessage>();
  }

  /**
   * @author Rex Shang
   */
  public ByteBuffer readData() throws BusinessException
  {
    if (getImageReconstructionEngine().isDeepSimulation() == false)
    {
      if (getImageReconstructionEngine().isSimulationModeOn() == true)
      {
        return null;
      }
    }

    // Extract the information type header from the byteBuffer.
    ByteBuffer messageIdTypeByteBuffer = readByteBuffer(JavaTypeSizeEnum.INT.getSizeInBytes());

    return messageIdTypeByteBuffer;
  }

  /**
   * @author Rex Shang
   */
  public void dispatchDataToObservable(ByteBuffer byteBuffer) throws XrayTesterException
  {
    Assert.expect(byteBuffer != null);

    int messageTypeId = byteBuffer.getInt();
    ImageReconstructionMessageEnum messageType = ImageReconstructionMessageEnum.getEnumById(messageTypeId);

    // Extract the information from the byteBuffer
    // The info message follows this format:
    // <integer   ><integer        ><integer  ><      bytes      >
    // <Message Id><Messeage Format><# of byte><Message Structure>
    ByteBuffer resultFormatByteBuffer = readByteBuffer(JavaTypeSizeEnum.INT.getSizeInBytes());
    if (isConnected() == false)
      return;

    int resultFormatId = resultFormatByteBuffer.getInt();
    ImageReconstructionResultEnum resultFormat = ImageReconstructionResultEnum.getEnumById(resultFormatId);

    ByteBuffer lengthByteBuffer = readByteBuffer(JavaTypeSizeEnum.INT.getSizeInBytes());
    if (isConnected() == false)
      return;

    int length = lengthByteBuffer.getInt();
    ByteBuffer infoMessage = readByteBuffer(length);
    if (isConnected() == false)
      return;

    // Okay!   Let's process it!
    Object obj = ImageReconstructionInformationFactory.getNewObject(messageType, resultFormat, infoMessage);

    debug("Notify Observers.  Message: " + obj);
    for (SynchronizedInformationMessage observer : _infoReceiverObservers)
    {
      int messageId = observer.getMessageId();
      if (messageId == messageTypeId)
      {
        observer.informationReceived(obj);
      }
    }
  }

  /**
   * @author Rex Shang
   */
  public void addNotifier(SynchronizedInformationMessage receiver)
  {
    Assert.expect(receiver != null);

    _infoReceiverObservers.add(receiver);
  }

  /**
   * @author Rex Shang
   */
  public static void setDebug(boolean state)
  {
    _debug = state;
  }

  /**
   * @author Rex Shang
   */
  private void debug(String message)
  {
    if (_debug)
      System.out.println(_me + ": " + message);
  }

}
