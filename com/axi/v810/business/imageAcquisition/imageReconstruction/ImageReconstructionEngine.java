package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.awt.geom.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.ImageAcquisitionModeEnum;
import com.axi.v810.business.imageAcquisition.imageReconstruction.communication.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class will provide the interface for controlling remote image
 * reconstruction processors.  Additionally this class will maintain
 * the reconstruction data receivers for images, focus results, exceptions,
 * and status.
 *
 * @author Horst Mueller
 * @author Dave Ferguson
 * @author Roy Williams
 */
public class ImageReconstructionEngine extends ProjectionConsumer implements Observer
{
  /**
   * Begin of the private data members for a single image reconstruction engine.
   * @author Roy Williams
   */
  private ImageReconstructionEngineEnum _imageReconstructionEngineId;
  private List<InetAddress> _inetAddresses = null;
  private int _connectionBrokerPort;
  private ImageAcquisitionModeEnum _acquisitionMode = null;

  private ImageReceiver _imageReceiver = null;
  private PriorityImageReceiver _priorityImageReceiver = null;
  private ImageStateReceiver _imageStateReceiver = null;
  private HardwareExceptionReceiver _hardwareExceptionReceiver = null;
  private ImageReconstructionInformationReceiver _infoReceiver = null;
  private CommandSender _commandSender = null;
  private boolean _closeConnectionsOnAbort = false;
  private boolean _irpHasTestProgram = false;
  private boolean _irpHasConnectedToCameras = false;

  // Atomic integer that holds the next scan pass number.
  // It will be accessed by multiple threads.
  private int _receivedScanPassNumber = -1;
  private Object _scanPassDeliveryLock = new Object();
  private int _waitingForPriorityRegionImage = -1;

  private Map<Integer, ReconstructedImages> _reconstructedImagesMap = new ConcurrentHashMap<Integer, ReconstructedImages>();
  private Set<Integer> _reconstructionRegionIdSet = new HashSet<Integer>();

  // Class instance will initialize these and make them available for all IRE's.
  private static ImageAcquisitionExceptionObservable _imageAcquisitionExceptionObservable = ImageAcquisitionExceptionObservable.getInstance();
  private static boolean _debug = false;
  private static boolean _isDeepSimulation = false;
  private static int _numberOfScanPassesStageCanGetAheadBecauseImagesCachedInCameraBuffers;
  private static Map<ImageReconstructionEngineEnum, ImageReconstructionEngine> _imageReconstructionEngines = new HashMap<ImageReconstructionEngineEnum, ImageReconstructionEngine>();
  private static Config _config;
  private ImageReconstructionProcessorEnum _hostIrpId;

  private static HardwareObservable _hardwareObservable;
  public boolean _hasReconstructionRegions = false;

  private static PerformanceLogUtil _performanceLog = PerformanceLogUtil.getInstance();
  private PanelLocationInSystemEnum _prevPanelLocationInSystem=null;
  
  //Bee Hoon - Single Server
  private static final int _startNumber = 1024;
  private int _startPort = _startNumber;
  private int _numberOfCameras = -1;
  private int _numberOfIREs = -1;
  
  //Swee Yee Wong
  private ImageReconstructionObservable _imageReconstructionObservable = ImageReconstructionObservable.getInstance();
  // Kok Chun, Tan - XCR-3031 - this key is used to check whether the starting port in each projection region need to update or not.
  private boolean _isUpdateStartPort = false;
  
  /**
   * @author Roy Williams
   */
  static
  {
    _config = getConfig();
    _isDeepSimulation = _config.getBooleanValue(SoftwareConfigEnum.OFFLINE_RECONSTRUCTION);
    _numberOfScanPassesStageCanGetAheadBecauseImagesCachedInCameraBuffers = _config.getIntValue(SoftwareConfigEnum.NUMBER_OF_SCAN_PASSES_IRPS_CAN_GET_AHEAD_BECAUSE_IMAGES_STORED_IN_CAMERA_BUFFERS);
    AbstractXrayCamera xRayCamera = XrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_1);
    int maximumNumberOfImagesCameraCanStore = xRayCamera.getMaximumNumberOfImagesCameraCanStore();
    Assert.expect(_numberOfScanPassesStageCanGetAheadBecauseImagesCachedInCameraBuffers <= maximumNumberOfImagesCameraCanStore);
    _hardwareObservable = HardwareObservable.getInstance();
  }
  
  /**
   * @author Eric Littlefield
   */
  static public void setCameraBuffersAvailableForTestProgram( int camerBufferDepth )
  {
    Assert.expect(camerBufferDepth > 0);
    _numberOfScanPassesStageCanGetAheadBecauseImagesCachedInCameraBuffers = camerBufferDepth;
  }

  /**
   * @author Rex Shang
   * @author Roy Williams
   */
  static public synchronized ImageReconstructionEngine getInstance(ImageReconstructionEngineEnum id)
  {
    Assert.expect(id != null, "id is null.");

    ImageReconstructionEngine ire = _imageReconstructionEngines.get(id);

    if (ire == null)
    {
      ire = new ImageReconstructionEngine(id);
      _imageReconstructionEngines.put(id, ire);
    }

    Assert.expect(ire!= null, "IRE is null.");
    return ire;
  }

  /**
   * @author Rex Shang
   * @author Roy Williams
   */
  private ImageReconstructionEngine(ImageReconstructionEngineEnum id)
  {
    _imageReconstructionEngineId = id;

    // We need to listen to certain hardware events.
    _hardwareObservable.addObserver(this);

    int residentIrpId = -1;

    if (id.equals(ImageReconstructionEngineEnum.IRE0))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_0_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_0_PORT);
    }
    else if (id.equals(ImageReconstructionEngineEnum.IRE1))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_1_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_1_PORT);
    }
    else if (id.equals(ImageReconstructionEngineEnum.IRE2))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_2_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_2_PORT);
    }
    else if (id.equals(ImageReconstructionEngineEnum.IRE3))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_3_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_3_PORT);
    }
    else if (id.equals(ImageReconstructionEngineEnum.IRE4))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_4_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_4_PORT);
    }
    else if (id.equals(ImageReconstructionEngineEnum.IRE5))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_5_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_5_PORT);
    }
    else if (id.equals(ImageReconstructionEngineEnum.IRE6))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_6_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_6_PORT);
    }
    else if (id.equals(ImageReconstructionEngineEnum.IRE7))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_7_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_7_PORT);
    }
    else if (id.equals(ImageReconstructionEngineEnum.IRE8))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_8_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_8_PORT);
    }
    else if (id.equals(ImageReconstructionEngineEnum.IRE9))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_9_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_9_PORT);
    }
    else if (id.equals(ImageReconstructionEngineEnum.IRE10))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_10_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_10_PORT);
    }
    else if (id.equals(ImageReconstructionEngineEnum.IRE11))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_11_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_11_PORT);
    }
    else if (id.equals(ImageReconstructionEngineEnum.IRE12))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_12_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_12_PORT);
    }
    else if (id.equals(ImageReconstructionEngineEnum.IRE13))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_13_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_13_PORT);
    }
    else if (id.equals(ImageReconstructionEngineEnum.IRE14))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_14_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_14_PORT);
    }
    else if (id.equals(ImageReconstructionEngineEnum.IRE15))
    {
      residentIrpId = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_15_HOST_PROCESSOR);
      _connectionBrokerPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_15_PORT);
    }
    else
      Assert.expect(false, "Undefined ImageReconstructionEngine number: " + id);

    switch (residentIrpId)
    {
      case 0:
        _hostIrpId = ImageReconstructionProcessorEnum.IRP0;
        break;
      case 1:
        _hostIrpId = ImageReconstructionProcessorEnum.IRP1;
        break;
      case 2:
        _hostIrpId = ImageReconstructionProcessorEnum.IRP2;
        break;
      case 3:
        _hostIrpId = ImageReconstructionProcessorEnum.IRP3;
        break;
      default:
        Assert.expect(false, "Invalide IRP number: " + residentIrpId);
    }

    List<InetAddress> myAddresses = ImageReconstructionProcessor.getHostIpAddresses(_hostIrpId);

    /** @todo RMS need to formalize the assignment */
    // Divide up the availabe addresses.
    int myIndex = _imageReconstructionEngineId.getEnumList().indexOf(_imageReconstructionEngineId);
    int numberOfEngines = ImageReconstructionProcessorManager.getNumberOfImageReconstructionEngines();
    // khang-wah Changes
    int numberOfProcessors = ImageReconstructionProcessorManager.getNumberOfImageReconstructionProcessors();
    // int numberOfProcessors = ImageReconstructionProcessorEnum.getEnumList().size();

    int numberOfPool = (int)Math.ceil((double)numberOfEngines / (double)numberOfProcessors);
    double addressPoolSize = (double)myAddresses.size() /(double)numberOfPool;

    // Single IRP testing
//    Assert.expect(myAddresses.size() >= numberOfPool);
//    Assert.expect(addressPoolSize > 0);

    int startIndex = (int)Math.floor((double)myIndex / (double)numberOfProcessors * addressPoolSize);
    int stopIndex = (int)Math.ceil(startIndex + addressPoolSize);

    _inetAddresses = myAddresses.subList(startIndex, stopIndex);
    
    List<XrayCameraIdEnum> idEnums = XrayCameraIdEnum.getAllEnums();
    _numberOfCameras = idEnums.size();
    //swee yee wong - brought this value to the hardware.config
    _startPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_STARTING_PORT_NUMBER) + (_numberOfCameras * id.getId());
    _numberOfIREs = Config.getInstance().getIntValue(HardwareConfigEnum.NUMBER_OF_IMAGE_RECONSTRUCTION_ENGINES);
  }

  /**
   * @author Roy Williams
   */
  public ReconstructedImagesProducer getReconstructedImagesProducer()
  {
    return (ReconstructedImagesProducer) getProjectionProducer();
  }

  /**
   * @author George A. David
   */
  public boolean isAbortInProgress()
  {
    return _closeConnectionsOnAbort || getProjectionProducer().isAbortInProgress();
  }

  /**
   * The ReconstructedImagesProducer is the only one that knows we are about to
   * run.   We will be instructed to clean up when this occurs.  Since all
   * ReconstructionRegions are going to be different for each TestProgram, this
   * must be followed by a call to initializeReconstructedImages().
   *
   * @author Roy Williams
   */
  public void clearImagesMaps()
  {
    // Out with the old, in with the new.
    _reconstructedImagesMap.clear();
    _reconstructionRegionIdSet.clear();
  }

  /**
   * @author Roy Williams
   */
  static public void setDebug(boolean state)
  {
    _debug = state;
  }

  /**
   * @author Roy Williams
   */
  public void initializeForNextScanPath() throws XrayTesterException
  {
    // Reset the counter for the current scan pass number.
    _receivedScanPassNumber = -1;
  }

  /**
   * @author Roy Williams
   */
  public static void setDeepSimulation(boolean state)
  {
    _isDeepSimulation = state;
  }

  /**
   * @author Roy Williams
   */
  public static boolean isDeepSimulation()
  {
    return _isDeepSimulation;
  }

  /**
   * @author Roy Williams
   */
  public synchronized ReconstructedImages getReconstructedImages(int regionId)
  {
    Assert.expect(regionId >= 0);
    ReconstructedImages reconstructedImages = _reconstructedImagesMap.get(regionId);

    Assert.expect(reconstructedImages != null);
    return reconstructedImages;
  }

  /**
   * @author Roy Williams
   * @author Matt Wharton
   */
  public synchronized boolean hasReconstructionRegion(int regionId)
  {
    Assert.expect(regionId >= 0);
    return _reconstructionRegionIdSet.contains(regionId);
  }

  /**
   * @author Roy Williams
   */
  public List<InetAddress> getIpAddresses()
  {
    Assert.expect(_inetAddresses != null);
    return _inetAddresses;
  }

  /**
   * @author Roy Williams
   */
  public InetAddress getFirstIpAddress()
  {
    Assert.expect(_inetAddresses != null);
    Assert.expect(_inetAddresses.size() > 0);
    return _inetAddresses.get(0);
  }

  /**
   *
   * @author Roy Williams
   */
  public int getPort()
  {
    Assert.expect(_connectionBrokerPort > 0);
    return _connectionBrokerPort;
  }

  /**
   * @author Roy Williams
   */
  public boolean canProceedWithScanPassWithWait(int scanPassNumber)
  {
    Assert.expect(scanPassNumber >= 0);

    if (canProceedWithScanPassNoWait(scanPassNumber) == false)
    {
      synchronized (_scanPassDeliveryLock)
      {
        try
        {
          _scanPassDeliveryLock.wait(100);
        }
        catch (InterruptedException ie)
        {
          // do nothing.
          // This object will be re-awakened by incrementRunnableScanPass()
        }
      }
    }
    return canProceedWithScanPassNoWait(scanPassNumber);
  }

  /**
   * canProceedWithScanPass without wait.
   *
   * @author Roy Williams
   */
  public boolean canProceedWithScanPassNoWait(int scanPassNumber)
  {
    Assert.expect(scanPassNumber >= 0);

    // if we are in IAE Deep Simulation, the scan path is incramented by the IRP
    if (_isDeepSimulation == false)
    {
      if (isSimulationModeOn() && (_receivedScanPassNumber < scanPassNumber))
      {
        incrementRunnableScanPass();
      }
    }

    // _testProgram is intentionally set to null during initializeSystem and only
    // set to a valid testProgram if this IRE participates in image acquisition.
    if (_irpHasTestProgram == false)
      return true;

    return isReadyForScanPass(scanPassNumber);
  }

  /**
   * @author Roy Williams
   */
  public void startScanPass(int scanPassNumber) throws XrayTesterException
  {
    Assert.expect(scanPassNumber >= 0);

    // do nothing.
  }

  /**
   * Abort activities on all the IRP.    This method is called indiscriminately
   * on all IRE's by the ReconstructedImagesProducer.  This IRE should do the
   * appropriate things.
   *
   * @author Roy Williams
   */
  public void abort() throws XrayTesterException
  {
    _irpHasConnectedToCameras = false;
    updateStartPort();
    
    if (_closeConnectionsOnAbort)
    {
      _closeConnectionsOnAbort = false;
      closeConnections();
      return;
    }

    if (areCommunicationPathsToImageReconstructionProcessorEstablished())
    {
      try
      {
        if (_debug)
          System.out.println("Begin Abort IRP: " + _imageReconstructionEngineId);

        new Abort(_commandSender, _imageStateReceiver).send();

        if (_debug)
          System.out.println("End Abort IRP: " + _imageReconstructionEngineId);
      }
      catch(XrayTesterException xex)
      {
        closeConnections();
        throw xex;
      }
      finally
      {
        if (_imageReceiver != null)
          _imageReceiver.setPause(false);
        if (_priorityImageReceiver != null)
          _priorityImageReceiver.setPause(false);
      }
    }
  }

  /**
   * Initialize the Image Reconstruction Engine.
   *
   * This instructs the image reconstruction processor to do the following:
   * - Establish network communications to cameras
   * - Establish network communications to data receivers on the controller
   * - Sets physical hardware constants.
   *
   * This initialization must occur at power up or after an abort.
   *
   * @author Dave Ferguson
   * @author Roy Williams
   */
  public synchronized void initializeSystem() throws XrayTesterException
  {
    if(isAbortInProgress())
      return;

    // Make sure our communication path to the designated IRP is established.
    establishCommunicationPathsToImageReconstructionProcessor();

    if(isAbortInProgress())
      return;

    // initialize IRP use System as value = indicator that all is ready to go.
    new InitializeSystem(_commandSender).send();

    if(isAbortInProgress())
      return;

    // set hw constants = from config through appropriate object.
    new SetHardwareConstants(_commandSender).send();

    if(isAbortInProgress())
      return;

    // set camera attibutes = from config through appropriate object.
    new SetCameraAttributes(_commandSender).send();

    if(isAbortInProgress())
      return;

    // set network attributes
    new SetImageReconstructionProcessorNetworkAttributes(_commandSender, _inetAddresses).send();

    if(isAbortInProgress())
      return;
   
    // Check the cameras connection status and connect to cameras
    int connectAttempt = 0;
    final int maxConnectAttempt = 50;
    _isUpdateStartPort = false;
    _imageReconstructionObservable.stateChanged(new ImageReconstructionEvent(ImageReconstructionEventEnum.BEGIN_IRP_CONNECT_TO_CAMERAS));
    while(_irpHasConnectedToCameras == false)
    { 
      ConnectToCamerasEnum currentStatus = new ConnectToCameras(_commandSender, _imageStateReceiver, _startPort).sendAndSync();

      if(currentStatus == ConnectToCamerasEnum.CONNECTION_COMPLETE)
      {
        _irpHasConnectedToCameras = true;
        break;
      }

      Assert.expect(currentStatus == ConnectToCamerasEnum.CONNECTION_FAILED_ADDRESS_IN_USE || currentStatus == ConnectToCamerasEnum.CONNECTION_FAILED_ASSIGNING_PORT || currentStatus == ConnectToCamerasEnum.CONNECTION_FAILED_OTHERS);
      if(++connectAttempt == maxConnectAttempt)
      {
        _imageReconstructionObservable.stateChanged(new ImageReconstructionEvent(ImageReconstructionEventEnum.END_IRP_CONNECT_TO_CAMERAS));
        throw new IRPToCameraConnectionStatusUnknownException(new LocalizedString("IRP_TO_CAMERAS_CONNECTION_STATUS_UNKNOWN_EXCEPTION_KEY",null));   
      }
      else
      {
        _isUpdateStartPort = true;
        updateStartPort();
      }
      
      if(isAbortInProgress())
      {
        _imageReconstructionObservable.stateChanged(new ImageReconstructionEvent(ImageReconstructionEventEnum.END_IRP_CONNECT_TO_CAMERAS));
        return;  
      }
    }
    _imageReconstructionObservable.stateChanged(new ImageReconstructionEvent(ImageReconstructionEventEnum.END_IRP_CONNECT_TO_CAMERAS));
  }

  /**
   * Send command to IRP to request its version information.
   * NOTE:  Make sure all communication channels are connected.
   * see establishCommunicationPathsToImageReconstructionProcessor()
   * @author Rex Shang
   */
  public ImageReconstructionProcessorApplicationVersion getImageReconstructionEngineVersionInfo() throws XrayTesterException
  {
    ImageReconstructionProcessorApplicationVersion irpVersionInfo = null;

    // Get version information from IRP.
    RequestApplicationVersion versionRequest = new RequestApplicationVersion(_commandSender, _infoReceiver);
    versionRequest.send();
    irpVersionInfo = versionRequest.getVersionInfo();

    return irpVersionInfo;
  }

  /**
   * Initializes the calibration data on the image reconstruction processor.
   *
   * This includes magnification calibration and system fiducial location
   * calibration.
   *
   * @author Dave Ferguson
   * @author Roy Williams
   */
  private void initializeCalibrationData() throws XrayTesterException
  {
    Config config = getConfig();

    // set calibrated Xray spot location
    new SetCalibratedXraySpotLocation(_commandSender).send();

    if(isAbortInProgress())
      return;

    // set calibrated magnification
    double mag = config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION);
    // set 2nd calibrated magnification
    double highMag = config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION);
    new SetCalibratedMagnification(_commandSender, mag, highMag).send();

    if(isAbortInProgress())
      return;

    // set calibrated system fiducial location
    new SetCalibratedSystemFiducialLocations(_commandSender).send();

    // set the edge spread function rate values
    new SetEdgeSpreadFunctionRate(_commandSender).send();
  }

  /**
   * Sets just the calibrated magnification data on the image reconstruction processor.
   *
   * @author Roy Williams
   */
  public void setCalibratedMagnification(double mag) throws XrayTesterException
  {
    Config config = getConfig();

    // set calibrated Xray spot location
    new SetCalibratedXraySpotLocation(_commandSender).send();

    // set calibrated magnification
    new SetCalibratedMagnification(_commandSender, mag, mag).send();

    // set calibrated system fiducial location
    new SetCalibratedSystemFiducialLocations(_commandSender).send();

    // set the edge spread function rate values
    new SetEdgeSpreadFunctionRate(_commandSender).send();
  }
  
  /**
   * Sets just the calibrated magnification data on the image reconstruction processor.
   *
   * @author Roy Williams
   */
  public void setCalibratedMagnification(double mag, double highMag) throws XrayTesterException
  {
    Config config = getConfig();

    // set calibrated Xray spot location
    new SetCalibratedXraySpotLocation(_commandSender).send();

    // set calibrated magnification
    new SetCalibratedMagnification(_commandSender, mag, highMag).send();

    // set calibrated system fiducial location
    new SetCalibratedSystemFiducialLocations(_commandSender).send();

    // set the edge spread function rate values
    new SetEdgeSpreadFunctionRate(_commandSender).send();

  }

  /**
   * Each ImageReconstructionEngine will only add those
   *
   * @author Roy Williams
   */
  private void addReconstructedImageAssignedToThisReconstructionEngine(ReconstructionRegion region)
  {
    Assert.expect(region != null);

    if (region.getReconstructionEngineId().equals(_imageReconstructionEngineId) &&
         region.getNumberOfSlicesToBeReconstructed() > 0)
    {
      int regionId = region.getRegionId();
      ReconstructedImages reconstructedImage = new ReconstructedImages(region);
      Integer regionIdKey = new Integer(regionId);
      Assert.expect((_reconstructedImagesMap.get(regionIdKey) == null), "Duplicate ReconstructionRegion id's");
      _reconstructedImagesMap.put(regionIdKey, reconstructedImage);
      _reconstructionRegionIdSet.add(regionId);
    }
  }

  /**
   * @author Roy Williams
   */
  public void initializeReconstructedImages(TestProgram testProgram, ImageAcquisitionModeEnum acquisitionMode)
  {
    Assert.expect(testProgram != null);
    Assert.expect(acquisitionMode != null);

    _acquisitionMode = acquisitionMode;

    // PRODUCTION mode, get Alignment and Inspection Regions
    testProgram.setFilteredTestSubProgramInScanPath(_acquisitionMode);
    List<TestSubProgram> testSubPrograms = testProgram.getFilteredScanPathTestSubPrograms();
    if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION) ||
        _acquisitionMode.equals(ImageAcquisitionModeEnum.IMAGE_SET))
    {
      for (TestSubProgram subProgram : testSubPrograms)
      {
        // InspectionRegions
        Collection<ReconstructionRegion> reconstructionRegions = null;
//        reconstructionRegions = subProgram.getAllInspectionRegions();
        reconstructionRegions = subProgram.getAllScanPathReconstructionRegions();
        for (ReconstructionRegion region : reconstructionRegions)
        {
          addReconstructedImageAssignedToThisReconstructionEngine(region);
        }

        // AlignmentRegions
        reconstructionRegions = subProgram.getAlignmentRegions();
        for (ReconstructionRegion region : reconstructionRegions)
        {
          addReconstructedImageAssignedToThisReconstructionEngine(region);
        }
      }
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.ALIGNMENT))
    {
      // AlignmentRegions
      for (TestSubProgram subProgram : testSubPrograms)
      {
        Collection<ReconstructionRegion> reconstructionRegions = subProgram.getAlignmentRegions();
        for (ReconstructionRegion region : reconstructionRegions)
        {
          addReconstructedImageAssignedToThisReconstructionEngine(region);
        }
      }
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.VERIFICATION) ||
             _acquisitionMode.equals(ImageAcquisitionModeEnum.COUPON))
    {
      // VerificationRegions
      for (TestSubProgram subProgram : testSubPrograms)
      {
        Collection<ReconstructionRegion> reconstructionRegions = subProgram.getVerificationRegions();
        for (ReconstructionRegion region : reconstructionRegions)
        {
          addReconstructedImageAssignedToThisReconstructionEngine(region);
        }
      }
    }
    else
    {
      Assert.expect(false, "Undefined ImageAcquisitionModeEnum");
    }
  }

  /**
   * Initialize the test program specific pararmeters on the image
   * image reconstruction processor
   *
   * @author Dave Ferguson
   * @author Roy Williams
   */
  public synchronized void initializeTestProgram(TestProgram testProgram,
                                                 ImageAcquisitionModeEnum acquisitionMode,
                                                 boolean transmitAlignmentRegions) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(acquisitionMode != null);

    if(isAbortInProgress())
      return;

    _hasReconstructionRegions = false;
    _acquisitionMode = acquisitionMode;

    if(isAbortInProgress())
      return;

    // Tell the IRP a new TestProgram is being sent.
    new InitializeTestProgram(_commandSender).send();

    if(isAbortInProgress())
      return;

    // set panel attributes
    new SetPanelAttributes(_commandSender, testProgram).send();

    // set autoFocus attributes
    new SetAutoFocusAttributes(_commandSender, testProgram.getProject().getPanel().getThicknessInNanometers(), testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers()).send();

    if(isAbortInProgress())
      return;

    // initialize calibration
    initializeCalibrationData();

    if(isAbortInProgress())
      return;

    // set projection attributes - iterate cameraParameters
    new SetProjectionAttributes(_commandSender,
                                _imageReconstructionEngineId,
                                testProgram).send();

    if(isAbortInProgress())
      return;
    
    // set velocity mapping data
    if (_config.getBooleanValue(SoftwareConfigEnum.USE_VELOCITY_MAPPING))
      initializeVelocityMappingData(testProgram);

    if(isAbortInProgress())
      return;

    testProgram.setFilteredTestSubProgramInScanPath(_acquisitionMode);
    for (TestSubProgram testSubProgram : testProgram.getFilteredScanPathTestSubPrograms())
    {

      new AddTestSubProgramTableEntry(_commandSender,
                                      testSubProgram,
                                      _acquisitionMode).send();

      if(isAbortInProgress())
        return;

      // send the proper regions to the IRP based upon the acquisition mode.
      if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION) ||
          _acquisitionMode.equals(ImageAcquisitionModeEnum.IMAGE_SET))
      {
        // InspectionRegions must go out when in productionOrLearningMode.
        addReconstructionRegionTableEntries(testSubProgram, ReconstructionRegionTypeEnum.INSPECTION);

        // AlignmentRegions must go out if and only if the ImageAcquisitionEngine
        if (transmitAlignmentRegions)
          addReconstructionRegionTableEntries(testSubProgram, ReconstructionRegionTypeEnum.ALIGNMENT);
      }
      // send the proper regions to the IRP based upon the acquisition mode.
      else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.ALIGNMENT))
      {
        addReconstructionRegionTableEntries(testSubProgram, ReconstructionRegionTypeEnum.ALIGNMENT);
      }
      else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.VERIFICATION) ||
               _acquisitionMode.equals(ImageAcquisitionModeEnum.COUPON))
      {
        // VerificationRegions must go out.
        addReconstructionRegionTableEntries(testSubProgram, ReconstructionRegionTypeEnum.VERIFICATION);
      }
      else
      {
        Assert.expect(false);
      }

      if(isAbortInProgress())
        return;

      // set Scan pass atributes - for each in scan path
      List<ScanPass> scanPassesForTestSubProgram = testSubProgram.getScanPasses();
      new SetScanPassAttributes(_commandSender, scanPassesForTestSubProgram, testSubProgram.getId()).send();
    }

    if(isAbortInProgress())
      return;

    // Tell the IRP the Test Program load is complete.
    new TestProgramComplete(_commandSender).send();
    _irpHasTestProgram = true;
  }

  /**
   * @author George A. David
   */
  private void addReconstructionRegionTableEntries(TestSubProgram testSubProgram, ReconstructionRegionTypeEnum reconstructionRegionType) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(reconstructionRegionType != null);
    Collection<ReconstructionRegion> reconstructionRegions = null;

    if(reconstructionRegionType.equals(ReconstructionRegionTypeEnum.ALIGNMENT))
      reconstructionRegions = testSubProgram.getAlignmentRegions();
    else if(reconstructionRegionType.equals(ReconstructionRegionTypeEnum.VERIFICATION))
      reconstructionRegions = testSubProgram.getVerificationRegions();
    else if(reconstructionRegionType.equals(ReconstructionRegionTypeEnum.INSPECTION))
      reconstructionRegions = testSubProgram.getAllScanPathReconstructionRegions();
    else
      Assert.expect(false);

    Collection<ReconstructionRegion> qualifiedCollectionRegions = getReconstructionRegionsForThisReconstructionEngine(reconstructionRegions);
    if (qualifiedCollectionRegions.size() > 0)
    {
      _hasReconstructionRegions = true;
      List< ReconstructionRegion > sortedRegion = new ArrayList< ReconstructionRegion >( qualifiedCollectionRegions );
      if (reconstructionRegionType.equals(ReconstructionRegionTypeEnum.INSPECTION) && VirtualLiveManager.getInstance().isVirtualLiveMode() == false)
        Collections.sort(sortedRegion, _sortRegionComparator);
      new AddReconstructionRegionTableEntries(_commandSender,
                                              testSubProgram.getId(),
                                              testSubProgram.getSubProgramRefferenceIdForAlignment(),
                                              reconstructionRegionType,
                                              sortedRegion).send();

      if(isAbortInProgress())
        return;
    }
  }

  /**
   * Set the alignment matrix
   *
   * @author Dave Ferguson
   * @author Roy Williams
   */
  public synchronized void setAlignment(AffineTransform affineTransform,
                                        int testSubProgramId) throws XrayTesterException
  {
    Assert.expect(affineTransform != null);
    Assert.expect(testSubProgramId >= 0);

    if(isAbortInProgress())
      return;

    if(_hasReconstructionRegions == false)
      return;
    // set runtime alignment matrix values - 6 doubles
    new SetRuntimeAlignmentMatrixValues(_commandSender,
                                        testSubProgramId,
                                        affineTransform).send();
  }

  /**
   * Re-aquire the image(s) for the specified region
   *
   * @author Dave Ferguson
   * @author Roy Williams
   */
  public synchronized void reacquireImage(int testSubProgramId,
                                          ReconstructionRegion reconstructionRegion,
                                          int reconstructionZOffsetInNanometers) throws XrayTesterException
  {
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(reconstructionRegion != null);

    TestExecutionTimer testExecutionTimer = ImageAcquisitionEngine.getInstance().getTestExecutionTimer();
    if (testExecutionTimer != null)
      testExecutionTimer.startReReconstructAcquireTimer();
    _waitingForPriorityRegionImage = reconstructionRegion.getRegionId();

    if (isAbortInProgress())
      return;

    // Make sure this reconstruction region is in the _reconstructedImagesMap
    int regionId = reconstructionRegion.getRegionId();
    Integer regionIdKey = new Integer(regionId);
    Assert.expect((_reconstructedImagesMap.get(regionIdKey) == null), "Duplicate ReconstructionRegion id's");
    ReconstructedImages reconstructedImage = new ReconstructedImages(reconstructionRegion);
    _reconstructedImagesMap.put(regionIdKey, reconstructedImage);

    // re-Reconstruct region(enum that has value AVERAGE)
    new ReconstructSpecificRegion(_commandSender,
                                  testSubProgramId,
                                  regionId,
                                  ImageTypeEnum.AVERAGE,
                                  reconstructionZOffsetInNanometers).send();
  }

  /**
   * Re-aquire the lightest image(s) for the specified region
   *
   * @author Dave Ferguson
   * @author Roy Williams
   */
  public synchronized void reacquireLightestImage(int testSubProgramId,
                                                  ReconstructionRegion reconstructionRegion,
                                                  int reconstructionZOffsetInNanometers) throws XrayTesterException
  {
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(reconstructionRegion != null);

    if (isAbortInProgress())
      return;

    // Make sure this reconstruction region is in the _reconstructedImagesMap
    int regionId = reconstructionRegion.getRegionId();
    Integer regionIdKey = new Integer(regionId);
    // If a LIGHTEST image request is made, we assume that this IRE no longer needs to keep track
    // of the corresponding AVERAGE image.  Ergo, the map entry is overwritten in this case.
    ReconstructedImages reconstructedImage = new ReconstructedImages(reconstructionRegion);
    _reconstructedImagesMap.put(regionIdKey, reconstructedImage);

    // re-Reconstruct region(enum that has value LIGHTEST)
    new ReconstructSpecificRegion(_commandSender,
                                  testSubProgramId,
                                  regionId,
                                  ImageTypeEnum.LIGHTEST,
                                  reconstructionZOffsetInNanometers).send();
  }

  /**
   * @author Scott Richardson
   */
  public synchronized void modifyFocusInstruction(int testSubProgramId,
                                                  int reconstructionRegionId,
                                                  int sliceId,
                                                  int zHeightFocusInNanoMeters,
                                                  int percent,
                                                  int zOffset,
                                                  byte focusMethodId) throws XrayTesterException
  {
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(reconstructionRegionId >= 0);
    Assert.expect(sliceId >= 0);
    // Assert.expect(zHeightFocusInNanoMeters >= 0);   // can be positive or negative.
    // Assert.expect(percent >= 0);                    // can be positive or negative.
    // Assert.expect(zOffset >= 0);                    // can be positive or negative.
    Assert.expect(focusMethodId >= 0);

    // tell the IRP to modifiy the zHeight of the focusInstruciton of the slice
    new ModifyFocusInstruction(_commandSender,
                               testSubProgramId,
                               reconstructionRegionId,
                               sliceId,
                               zHeightFocusInNanoMeters,
                               percent,
                               zOffset,
                               focusMethodId).send();
  }

  /**
   * @author Cheah Lee Herng
   * @author Roy Williams
   */
  public synchronized void modifyFocusProfileShape(int testSubProgramId,
                                                   int reconstructionRegionId,
                                                   int focusGroupId,
                                                   byte shape,
                                                   int zGuessFromPredictiveSliceHeightInNanometers,
                                                   boolean usePhaseShiftProfiliometry,
                                                   int searchRangeLowLimitInNanometers,
                                                   int searchRangeHighLimitInNanometers,
                                                   int pspZOffsetInNanometers) throws XrayTesterException
  {
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(reconstructionRegionId >= 0);
    Assert.expect(focusGroupId >= 0);

    // tell the IRP to modifiy the zHeight of the focusInstruciton of the slice
    new ModifyFocusProfileShape(_commandSender,
                                testSubProgramId,
                                reconstructionRegionId,
                                focusGroupId,
                                shape,
                                zGuessFromPredictiveSliceHeightInNanometers,
                                usePhaseShiftProfiliometry,
                                searchRangeLowLimitInNanometers,
                                searchRangeHighLimitInNanometers,
                                pspZOffsetInNanometers).send();
  }
  
  /**
   * @author Chnee Khang Wah, 2013-06-17, New PSH Handling
   */
  public synchronized void kickStartSpecificRegion(int testSubProgramId,
                                         int reconstructionRegionId) throws XrayTesterException
  {
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(reconstructionRegionId >= 0);

    // tell the IRP kik start
    new KickStartSpecificRegion(_commandSender,
                                testSubProgramId,
                                reconstructionRegionId).send();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public synchronized void enableSurfaceModeling() throws XrayTesterException
  {
    // tell the IRP to modifiy the zHeight of the focusInstruciton of the slice
    new EnableSurfaceModeling(_commandSender, _imageStateReceiver).send();
  }

  /**
   * @author Roy Williams
   */
  public synchronized void disableSurfaceModeling() throws XrayTesterException
  {
    // tell the IRP to modifiy the zHeight of the focusInstruciton of the slice
    new DisableSurfaceModeling(_commandSender, _imageStateReceiver).send();
  }

  /**
   * @author Roy Williams
   */
  public synchronized void waitForPriorIrpCommandsToComplete() throws XrayTesterException
  {
    // wait for the IRP to acknowledge that all prior commands have been completed.
    new WaitForPriorIrpCommandsToComplete(_commandSender, _imageStateReceiver).send();
  }

  /**
   * Notify the image reconstruction engine that a given reconstruction
   * region has been fully processed.  Reconstruction resources can be
   * freed.
   *
   * @author Dave Ferguson
   * @author Roy Williams
   */
  public synchronized void reconstructionRegionComplete(int testSubProgramId,
      int reconstructionRegionId,
      boolean hasTestFailure) throws XrayTesterException
  {
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(reconstructionRegionId >= 0);

    // mark reconstruction region complete
    ClassificationStatusEnum state = hasTestFailure ?
                                     ClassificationStatusEnum.FAIL :
                                     ClassificationStatusEnum.PASS;
    if (isAbortInProgress())
      return;

    new MarkReconstructionRegionComplete(_commandSender, testSubProgramId, reconstructionRegionId, state).send();
  }

  /**
   * @author Matt Wharton
   */
  public synchronized void allImagesReceivedForRegion(ReconstructedImages reconstructeImages)
  {
    Assert.expect(reconstructeImages != null);
    Assert.expect(_reconstructedImagesMap != null);

    int reconstructionRegionId = reconstructeImages.getReconstructionRegionId();
    if (_waitingForPriorityRegionImage > 0)
    {
      if (reconstructionRegionId == _waitingForPriorityRegionImage)
      {
        TestExecutionTimer testExecutionTimer = ImageAcquisitionEngine.getInstance().getTestExecutionTimer();
        if (testExecutionTimer != null)
          testExecutionTimer.stopReReconstructAcquireTimer(reconstructeImages.getNumberOfSlices());
        _waitingForPriorityRegionImage = -1;
      }
    }
    ReconstructedImages removedImages = _reconstructedImagesMap.remove(reconstructionRegionId);
    Assert.expect(removedImages != null);
  }

  /**
   * Pause the reconstruction.  This will instruct the image reconstruction
   * engine to stop sending reconstructed images to the main pc as soon as
   * possible.
   *
   * @throws HardwareException
   * @author Dave Ferguson
   * @author Roy Williams
   */
  public synchronized void pauseReconstruction() throws XrayTesterException
  {
    // CR27551: commandSender is showing up at ImageReconstructionMessage as null.
    // cache it before checking abort.
    CommandSender commandSender = _commandSender;
    ImageReceiver imageReceiver = _imageReceiver;

    // If in an abort we can return without masking an error.
    if (isAbortInProgress())
      return;

    if (commandSender == null || imageReceiver == null)
    {
      throw new FailedConnectingToImageReconstructionSocketBusinessException(
          this,
          _inetAddresses.get(0).getHostAddress(),
          _connectionBrokerPort);
    }

    // set IRP state (enum value of PAUSE)
    new PauseReconstruction(commandSender).send();
    imageReceiver.setPause(true);
  }

  /**
   * Resume reconstruction.  This will instruct the image reconstruction
   * engine to resume sending images to the main PC
   *
   * @author Dave Ferguson
   * @author Roy Williams
   */
  public synchronized void resumeReconstruction() throws XrayTesterException
  {
    // CR27551: commandSender is showing up at ImageReconstructionMessage as null.
    // cache it before checking abort.
    CommandSender commandSender = _commandSender;
    ImageReceiver imageReceiver = _imageReceiver;

    // If in an abort we can return without masking an error.
    if (isAbortInProgress())
      return;

    if (commandSender == null || imageReceiver == null)
    {
      throw new FailedConnectingToImageReconstructionSocketBusinessException(
          this,
          _inetAddresses.get(0).getHostAddress(),
          _connectionBrokerPort);
    }

    // set IRP state (enum value of RESUME)
    imageReceiver.setPause(false);
    new ResumeReconstruction(commandSender).send();
  }

  /**
   * @return the unique Id of the image reconstruction process this engine is running on.
   * @author Rex Shang
   */
  public ImageReconstructionProcessorEnum getHostIrpId()
  {
    return _hostIrpId;
  }

  /**
   * @return the unique Id of the image reconstruction engine
   * @author Dave Ferguson
   * @author Horst Mueller
   */
  public ImageReconstructionEngineEnum getId()
  {
    return _imageReconstructionEngineId;
  }

  /**
   * @author Roy Williams
   */
  public synchronized void setFocusResult(FocusResult focusResult) throws XrayTesterException
  {
    Assert.expect(focusResult != null);

    new AddFocusResult(_commandSender, focusResult).send();
  }

  /**
   * Instruct the image reconstruction engine to begin reconstruction
   * This used to be a function called createImages().   It need only be called
   * once.
   *
   * @author Dave Ferguson
   * @author Roy Williams
   */
  public synchronized void start(final TestSubProgram testSubProgram,
                                 boolean initializeTestRequired,
                                 boolean resetSurfaceModelRequired,
                                 TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(testExecutionTimer != null);

    int testSubProgramId=testSubProgram.getId();
    if(_hasReconstructionRegions == false)
      return;

    if(isAbortInProgress())
      return;

    // CR28960: TestExecution:Failure when add a slice/image to reconstructionRegion after abort
    // So, we also need to initialize the ImageReceiver. It might have some
    // lingering variables set from a previous user abort.
    _imageReceiver.initialize();

    if (initializeTestRequired)
    {
      if (_debug)
        System.out.println("Begin InitializeTest IRP: " + _imageReconstructionEngineId);

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_INITIALIZE_TEST);

      new InitializeTest(_commandSender, _imageStateReceiver).send();

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_INITIALIZE_TEST);

      if (_debug)
        System.out.println("End InitializeTest IRP: " + _imageReconstructionEngineId);
    }
    else
    {
      if (_debug)
        System.out.println("InitializeTest not required/  IRP: " + _imageReconstructionEngineId);
    }
    
    if(resetSurfaceModelRequired)
    {
      if (_debug)
        System.out.println("Begin ResetSurfaceModel IRP: " + _imageReconstructionEngineId);

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_INITIALIZE_TEST);

//      new ResetSurfaceModel(_commandSender, _imageStateReceiver).send();
//      new InitializeTest(_commandSender, _imageStateReceiver).send();

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_INITIALIZE_TEST);

      if (_debug)
        System.out.println("End ResetSurfaceModel IRP: " + _imageReconstructionEngineId);
    }
    else
    {
      if (_debug)
        System.out.println("ResetSurfaceModel not required/  IRP: " + _imageReconstructionEngineId);
    }

    // reset Thin Plate Spline model
    //XCR1168 : implement multi gain, reset model when change panel location. 
    //So that even only has one component in subprogram still can use GSM 
    if(testSubProgram.isSubProgramPerformAlignment())
    {
      new ResetThinPlateSpline(_commandSender, _imageStateReceiver).send();
    }
    else
    {
      if(testSubProgram.getPanelLocationInSystem()!=_prevPanelLocationInSystem)
      {
         new ResetThinPlateSpline(_commandSender, _imageStateReceiver).send();
         _prevPanelLocationInSystem=testSubProgram.getPanelLocationInSystem();
      }
    }

    if (isAbortInProgress())
     return;

   // set IRP state (enum RUN)
    new RunReconstruction(_commandSender, testSubProgramId).send();
  }

  /**
   * @author Roy Williams
   */
  public static int getNumberOfScanPassesStageCanGetAheadBecauseImagesCachedInCameraBuffers()
  {
    return _numberOfScanPassesStageCanGetAheadBecauseImagesCachedInCameraBuffers;
  }

  /**
   * Called by the ImageAcquisition.runAcquisitionSequence method to insure this
   * implementation of a ProjectionConsumer is ready (for it) to initiate the next
   * scanPass.   Once the next scanPass is initiated, this ProjectionConsumer must
   * be ready to acquuire the next set of images.
   *
   * @author Roy Williams
   */
  private synchronized boolean isReadyForScanPass(int scanPassNumber)
  {
    Assert.expect(scanPassNumber >= 0);
    boolean ready = false;

    if (_receivedScanPassNumber >= 0)
    {
      if (scanPassNumber < _receivedScanPassNumber ||
          (scanPassNumber - _receivedScanPassNumber) <= _numberOfScanPassesStageCanGetAheadBecauseImagesCachedInCameraBuffers)
        ready = true;
    }
    else if (scanPassNumber < _numberOfScanPassesStageCanGetAheadBecauseImagesCachedInCameraBuffers)
      ready = true;

    return ready;
  }

  /**
   * Bumps the max runnable scan pass number this reconstruction engine can accomodate.
   * An int was used as the input parameter because it is highly desired to have the
   * ImageReconstructionProcessor indicate which scan pass has been completed.
   * <p>
   * Here is an example of what would be optimal. Notice that an IRE does not need
   * to participate in each and every scan pass.
   * <blockquote>
   * <pre>
   *  ------------------------------------------------------------------------
   *    scan pass # | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
   *    current     | x | x |   |   | x
   *    received    | x | x |
   *  </pre>
   *  </blockquote>
   * <p>
   *
   * @author Dave Ferguson
   * @author Roy Williams
   */
  public synchronized int incrementRunnableScanPass()
  {
    // we don't need to have the lock on this object to increment the scanPass count.
    _receivedScanPassNumber++;
    if (_isDeepSimulation == false)
    {
      if (isSimulationModeOn())
        getReconstructedImagesProducer().setAllImagesAreAcquiredFlag(true);
    }
    synchronized (_scanPassDeliveryLock)
    {
      _scanPassDeliveryLock.notifyAll();
    }
    return _receivedScanPassNumber;
  }

  /**
   * @author George A. David
   */
  public boolean hasReceivedScanPasses()
  {
    return _receivedScanPassNumber >= 0;
  }

  /**
   * @author Roy Williams
   */
  public synchronized int getLastReceivedScanPassNumber()
  {
    Assert.expect(_receivedScanPassNumber >= 0);
    return _receivedScanPassNumber;
  }

  /**
   * Re-establish all socket connections from the IRE to the IRP.  If there were
   * prior connections open, close them before trying to connect.
   *
   * @author Roy Williams
   */
  public void establishCommunicationPathsToImageReconstructionProcessor() throws XrayTesterException
  {
    if (_debug)
    {
      HardwareExceptionReceiver.setDebug(_debug);
      ImageReceiver.setDebug(_debug);
      PriorityImageReceiver.setDebug(_debug);
      ImageStateReceiver.setDebug(_debug);
      ImageReconstructionInformationReceiver.setDebug(_debug);
    }

    // Make sure we have an IRP to talk with.
    if (areCommunicationPathsToImageReconstructionProcessorEstablished())
    {
      // Sending this abort to IRP's tests the communication channel.  A common
      // problem is that a reference is still valid to the Socket but the
      // transport is not operational.   We cannot tell this is the case by querying
      // the object.   We must exercise the transport.
      new Abort(_commandSender, _imageStateReceiver).send();
    }
    else
    {
      // Put ALL connections into the same state => closed.   If a partial set
      // is open this is not a healthy situation.  Reset them all and we will
      // reopen them in the next block.
      closeConnections();
      _irpHasTestProgram = false;
    }

    // Open connections to the IRP only if we do not have a prior/working connection.
    if (areCommunicationPathsToImageReconstructionProcessorEstablished() == false)
    {
      _irpHasConnectedToCameras = false;
      updateStartPort();
      
      // Select an inet address.   If really doing work (default), we'll just use
      // the first of the two IP addresses of the IRP.
      InetAddress inetAddress = _inetAddresses.get(0);
      int port = _connectionBrokerPort;

      _hardwareExceptionReceiver = new HardwareExceptionReceiver(this, inetAddress, port);
      _imageReceiver = new ImageReceiver(this, inetAddress, port);
      _priorityImageReceiver = new PriorityImageReceiver(this, inetAddress, port);
      _imageStateReceiver = new ImageStateReceiver(this, inetAddress, port);
      _infoReceiver = new ImageReconstructionInformationReceiver(this, inetAddress, port);
      // The _commandSender connection must be made after the _imageStateReceiver and _infoReceiver connections.
      _commandSender = new CommandSender(this, inetAddress, port);

      // Sending a reset to the IRP will destroy its copy of the test program.
      _irpHasTestProgram = false;

      // Send a reset to the IRP only the first time we are connecting.
      if (_isDeepSimulation == false)
      {
        if (_debug)
          System.out.println("Begin Reset IRP: " + _imageReconstructionEngineId);
        new Reset(_commandSender, _imageStateReceiver).send();
        _irpHasConnectedToCameras = false;
        updateStartPort();
        if (_debug)
          System.out.println("End Reset IRP: " + _imageReconstructionEngineId);
      }
    }

    // we also need to initialize the ImageReceiver. It might have some
    // lingering variables set from a previous user abort.
    _imageReceiver.initialize();
  }

  /**
   * Simple check to see if we have existing object references to the IRP connections.
   *
   * @author Roy Williams
   */
  public boolean areCommunicationPathsToImageReconstructionProcessorEstablished()
  {
    if (_hardwareExceptionReceiver != null &&
        _hardwareExceptionReceiver.isConnected() &&
        _imageReceiver != null &&
        _imageReceiver.isConnected() &&
        _priorityImageReceiver != null &&
        _priorityImageReceiver.isConnected() &&
        _imageStateReceiver != null &&
        _imageStateReceiver.isConnected() &&
        _infoReceiver != null &&
        _infoReceiver.isConnected() &&
        _commandSender != null &&
        _commandSender.isConnected())
    {
      return true;
    }
    return false;
  }

  /**
   * @author Roy Williams
   */
  public boolean isImageReceiverBusy()
  {
    boolean imageReceiverBusy = _imageReceiver != null && _imageReceiver.isReceiverBusy();
    if (imageReceiverBusy)
      return true;
    imageReceiverBusy = _priorityImageReceiver != null && _priorityImageReceiver.isReceiverBusy();
    if (imageReceiverBusy)
      return true;
    return false;
  }

  /**
   * @author Roy Williams
   */
  public void closeConnectionsOnNextAbort()
  {
    _closeConnectionsOnAbort = true;
  }

  /**
   * @author Roy Williams
   */
  public void closeConnections()
  {
    if (_commandSender != null)
    {
      _commandSender.close();
      _commandSender = null;
    }

    if (_infoReceiver != null)
    {
      _infoReceiver.close();
      _infoReceiver = null;
    }

    if (_imageStateReceiver != null)
    {
      _imageStateReceiver.close();
      _imageStateReceiver = null;
    }

    if (_priorityImageReceiver != null)
    {
      _priorityImageReceiver.close();
      _priorityImageReceiver = null;
    }

    if (_imageReceiver != null)
    {
      _imageReceiver.close();
      _imageReceiver = null;
    }

    if (_hardwareExceptionReceiver != null)
    {
      _hardwareExceptionReceiver.close();
      _hardwareExceptionReceiver = null;
    }
  }

  /**
   * Returns a sublist of the specified ReconstructionRegions that are to be
   * reconstructed by this image reconstruction engine
   *
   * @param reconstructionRegions a list of ReconstructionRegions to search.
   * @return a sublist of ReconstructionRegions that are to be reconstructed by this IRE.
   * @author Matt Wharton
   * @author Dave Ferguson
   */
  private Collection<ReconstructionRegion> getReconstructionRegionsForThisReconstructionEngine(
      Collection<ReconstructionRegion> reconstructionRegions)
  {
    Assert.expect(reconstructionRegions != null);

    Collection<ReconstructionRegion> reconstructionRegionsForIre = new ArrayList<ReconstructionRegion>();

    for (ReconstructionRegion reconstructionRegion : reconstructionRegions)
    {
      if (reconstructionRegion.getReconstructionEngineId().equals(_imageReconstructionEngineId))
      {
        reconstructionRegionsForIre.add(reconstructionRegion);
      }
    }

    return reconstructionRegionsForIre;
  }

  /**
   * @author Roy Williams
   */
  public ReconstructedImagesManager getReconstructedImagesManager()
  {
    return getReconstructedImagesProducer().getReconstructedImagesManager();
  }

  /**
   * @author Roy Williams
   */
  public CommandSender getCommandSender() throws XrayTesterException
  {
    Assert.expect(_commandSender != null);
    return _commandSender;
  }

  /**
   * @author Roy Williams
   */
  public ImageReceiver getImageReceiver() throws XrayTesterException
  {
    Assert.expect(_imageReceiver != null);
    return _imageReceiver;
  }

  /**
   * @author Roy Williams
   */
  public ImageAcquisitionExceptionObservable getImageAcquisitionExceptionObservable()
  {
    Assert.expect(_imageAcquisitionExceptionObservable != null);
    return _imageAcquisitionExceptionObservable;
  }

  /**
   * @author Roy Williams
   */
  public InetSocketAddress getBindInetAddress()
  {
    InetSocketAddress localSocketAddress = null;

    boolean useFirstNicCard = false;
    if (getId().equals(ImageReconstructionEngineEnum.IRE0) || getId().equals(ImageReconstructionEngineEnum.IRE2))
      useFirstNicCard = true;

    try
    {
      InetAddress localInetAddress;
      if (_isDeepSimulation)
        localInetAddress = IPaddressUtil.getRemoteLoopbackAddress();
      else if (ImageAcquisitionEngine.getInstance().isOfflineReconstructionEnabled())
        localInetAddress = IPaddressUtil.getHostInetAddressOnPrivateNetwork();
      else
        localInetAddress = IPaddressUtil.getHostInetAddressOnPrivateNetwork(useFirstNicCard);

      localSocketAddress = new InetSocketAddress(localInetAddress, 0);
    }
    catch (IPaddressException ex)
    {
      Assert.expect(false);
    }
    return localSocketAddress;
  }

  /**
   * @author Roy Williams
   */
  static public long getMemoryAllocatedForProjectionMemory()
  {
    long megaByteToByte = 1024 * 1024;
    return _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_ALLOCATED_PROJECTION_MEMORY_IN_MEGABYTES) * megaByteToByte;
  }

  /**
   * @author George A. David
   */
  public boolean isPaused()
  {
    Assert.expect(_imageReceiver != null);

    return _imageReceiver.isPaused();
  }

  /**
   * @author Roy Williams
   */
  public void update(Observable observable, Object arg)
  {
    if (observable instanceof HardwareObservable)
    {
      if (arg instanceof HardwareEvent)
      {
        HardwareEvent hardwareEvent = (HardwareEvent)arg;
        if (hardwareEvent.getEventEnum().equals(XrayTesterEventEnum.SHUTDOWN) &&  hardwareEvent.isStart())
          closeConnections();
      }
    }
  }

  /**
   * @author George A. David
   */
  public boolean hasReconstructionRegions()
  {
    return _hasReconstructionRegions;
  }

  /**
   * @author Roy Williams
   */
  public synchronized void clearProjectInfo()
  {
    if(_imageReceiver != null)
      _imageReceiver.initialize();
    if (_priorityImageReceiver != null)
      _priorityImageReceiver.initialize();
  }

  /**
   * @author Roy Williams
   */
  public boolean hasConnectedToCamerasBeenSent()
  {
    return _irpHasConnectedToCameras;

  }

  /**
   * @author Cheah Lee Herng
   */
  public void decrementReconstructedImages()
  {
      Assert.expect(_reconstructedImagesMap != null);

      for(Map.Entry<Integer, ReconstructedImages> entry : _reconstructedImagesMap.entrySet())
      {
          ReconstructedImages reconstructedImages = (ReconstructedImages)entry.getValue();
          if (reconstructedImages.isImageBeingReferenced())
            reconstructedImages.decrementReferenceCount();
      }
  }
  
  /**
   * @author Bee Hoon
   */
  public int getStartPortNumber()
  {
    return _startPort;
  }
  
  /**
   * @author Bee Hoon
   */
  private void updateStartPort()
  {
    Assert.expect(_numberOfCameras != -1);
    Assert.expect(_numberOfIREs != -1);
    //swee yee wong - brought this value to the hardware.config
    if (_config.getBooleanValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_FLOATING_PORT_NUMBER))
    {
      int portRandomFactor = 5;

      int limit = (65535 - (((_numberOfCameras * _numberOfIREs) * portRandomFactor) * 2));
      if (_startPort > limit)
      {
        //swee yee wong - brought this value to the hardware.config
        _startPort = _config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_ENGINE_STARTING_PORT_NUMBER) + (_numberOfCameras * _imageReconstructionEngineId.getId());
      }
      else
      {
        _startPort = _startPort + ((_numberOfCameras * _numberOfIREs) * portRandomFactor);
      }
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean isUpdateStartPort()
  {
    return _isUpdateStartPort;
  }

 /**
  * XCR-3325, Intermittent IRP-Camera Socket Connection Crash in Magnification Adjustment
  * XCR-3336, Assert when run CDNA after production error
  * @author Yong Sheng Chuan
  */
  public boolean isIrpToCameraConnectionBroken() throws XrayTesterException
  {

    if (areCommunicationPathsToImageReconstructionProcessorEstablished())
    {
      ConnectToCamerasEnum connectionStatus = new GetCameraConnectionStatus(_commandSender, _imageStateReceiver).sendAndSync();
      if (connectionStatus.equals(ConnectToCamerasEnum.CONNECTION_COMPLETE))
      {
        return false;
      }
      else
      {
        _irpHasConnectedToCameras = false;
        return true;
      }
    }
    else
    {
      return true;
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void initializeVelocityMappingData(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    for (VelocityMappingParameters data : testProgram.getVelocityMappingParameters())
    {
      new AddVelocityMappingDataEntries(_commandSender, data).send();
    }
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  private Comparator<ReconstructionRegion> _sortRegionComparator = new Comparator<ReconstructionRegion>()
  {
    public int compare(ReconstructionRegion lhs, ReconstructionRegion rhs)
    {
      SubtypeAdvanceSettings lhsSubtypeAdvanceSetting = lhs.getSubtypes().iterator().next().getSubtypeAdvanceSettings();
      SubtypeAdvanceSettings rhsSubtypeAdvanceSetting = rhs.getSubtypes().iterator().next().getSubtypeAdvanceSettings();
      
        if (lhsSubtypeAdvanceSetting.getHighestStageSpeedSetting().toDouble() == rhsSubtypeAdvanceSetting.getHighestStageSpeedSetting().toDouble())
        {
          if (lhsSubtypeAdvanceSetting.getUserGain().toDouble() == rhsSubtypeAdvanceSetting.getUserGain().toDouble())
            return 0;
          else if (lhsSubtypeAdvanceSetting.getUserGain().toDouble() > rhsSubtypeAdvanceSetting.getUserGain().toDouble())
           return 1;
         else
           return -1;
        }
        else if (lhsSubtypeAdvanceSetting.getHighestStageSpeedSetting().toDouble() > rhsSubtypeAdvanceSetting.getHighestStageSpeedSetting().toDouble())
          return 1;
        else 
          return -1;
      }
  };
}
