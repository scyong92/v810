package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import com.axi.v810.business.BusinessException;

/**
 * Commands sent from the ImageReconstructionEngine to the ImageReconstructionProcessor
 * are structured into a header and payload.   This class enforces the formatting
 * required to send messages in this prescribed format by implementing a strategy
 * pattern in the run method.   All derived classes must implement a set of
 * methods to complete the mission.
 *
 * @author Roy Williams
 */
public abstract class ImageReconstructionMessage
{
  protected ImageReconstructionMessageEnum _imageReconstructionMessage;
  protected CommandSender _writer;
  private int _count = -1;
  private final int _HEADER_SIZE = JavaTypeSizeEnum.SHORT.getSizeInBytes() +
                                   JavaTypeSizeEnum.SHORT.getSizeInBytes() +
                                   JavaTypeSizeEnum.INT.getSizeInBytes();
  protected static Config  _config = Config.getInstance();
  protected static boolean _debug = _config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE);

  /**
   * @author Roy Williams
   */
  public ImageReconstructionMessage(ImageReconstructionMessageEnum messageId, CommandSender writer) throws XrayTesterException
  {
    Assert.expect(messageId != null);

    _imageReconstructionMessage = messageId;
    if (writer == null)
    {
      // XrayTesterException based abort
      ReconstructedImagesProducer reconstructedImagesProducer = ImageAcquisitionEngine.getInstance().getReconstructedImagesProducer();
      if (reconstructedImagesProducer.isAbortInProgress())
      {
        // This is a real exception.   Throw it.
        if (reconstructedImagesProducer.hasXrayTesterException())
          throw reconstructedImagesProducer.getXrayTesterException();

        // This could be a user abort (i.e. no exception being processed)
        // isAbortInProgress is true so - do nothing except return.
        return;
      }
    }

    Assert.expect(writer != null);
    _writer = writer;
  }


  /**
   * @author Roy Williams
   */
  protected boolean isWriterValid()
  {
    if (_writer != null && _writer.isConnected())
      return true;
    return false;
  }

  /**
   * Sending a message to the ImageReconstructionProcess.  Each message
   * id represents a unique command as defined by the the constructor of the derived
   * class.   The header will be output then the derived class is responsible to
   * construct a payload (i.e. data) to be transmittted.
   *
   * @author Roy Williams
   */
  public void send() throws XrayTesterException
  {
    ByteBuffer buffer = getMessageBuffer();
    buffer.rewind();
    _writer.write(buffer);

//    public static boolean throwException = false;
//    if (throwException)
//    {
//      throwException = false;
//      throw new FailedOpeningImageReconstructionSocketBusinessException(
//        this.getImageReconstructionEngine(),
//        _writer.getInetAddress().getHostAddress(),
//        _writer.getPortNumber());
//
//    }
    // The message has been sent.
    // Expect a response back from the IRP on each message
    _writer.waitForReadyAcknowledgement(_imageReconstructionMessage);

    emitAssociations(_writer);
  }

  /**
   * @author Roy Williams
   */
  protected int getNumberOfRecordsSentInHeader()
  {
    Assert.expect(_count > 0);

    return _count;
  }

  /**
   * Return the number of bytes in a message header.
   *
   * @author Bob Balliew
   */
  public int getMessageHeaderSize()
  {
    return _HEADER_SIZE;
  }

  /**
   * Return the message in a byte buffer.
   *
   * @author Bob Balliew
   */
  public final ByteBuffer getMessageBuffer() throws XrayTesterException
  {
    ByteBuffer buffer = getByteBuffer(getMessageHeaderSize() + (getSizeOfEachRecord() * getNumberOfRecords()));
    buffer = addMessageHeaderBuffer(buffer);

    if (hasPayLoad())
      buffer = addPayLoad(buffer);

    return buffer;
  }

  /**
   * Composes a message message header consisting of the number of records, size
   * of each record and message id to send to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addMessageHeaderBuffer(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getMessageHeaderSize());
    _count = getNumberOfRecords();
    int size  = getSizeOfEachRecord();
    int usage = _imageReconstructionMessage.getId();
    if (_config.getBooleanValue(SoftwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_COMMUNICATIONS_HANDSHAKE))
    {
      usage = usage | 0x1000;
    }
    buffer.putShort((short)usage);
    buffer.putShort((short)size);
    buffer.putInt(_count);
    return buffer;
  }

  /**
   * Sending a message to the IRP does not require the use of direct ByteBuffers.
   * This is a convenience function for derived classes to get the right kind.
   *
   * @author Roy Williams
   */
  protected ByteBuffer getByteBuffer(int size) throws BusinessException
  {
    Assert.expect(size > 0);

    ByteBuffer buffer = null;
    try
    {
      buffer = ByteBuffer.allocate(size);
      buffer.order(ByteOrder.LITTLE_ENDIAN);
    }
    catch (Throwable th)
    {
      // IllegalArgumentException and OutOfMemoryError will be caught here.
      FailedMessageMemoryAllocationBusinessException ex = new FailedMessageMemoryAllocationBusinessException(size);
      ex.initCause(th);
      throw ex;
    }
    return buffer;
  }

  /**
   * This is an opportunity to emit any Object Oriented associations to other
   * Java objects.   Most messages sent to the IRP do not have any of these
   * associations.   Thus, a default implementation is provided to prevent
   * those (that do not have associations) from having to implement this method.
   * <p>
   * When it is called, it is expected that the writer will be re-used to send
   * the details to the same IRP.   For example, a ReconstructionRegions has many
   * FocusGroups that are unique to it.  When the ReconstructionRegion is emitted,
   * so too will the FocusGroups.   And each FocusGroup must identify the region
   * from whence it came.
   *
   * @author Roy Williams
   */
  protected void emitAssociations(CommandSender writer) throws XrayTesterException
  {
    Assert.expect(writer != null);

    // do nothing - by default.   Derived classes are expected to override
    // if their class warrants different behaviour.
  }

  /**
   * @author Roy Williams
   */
  public int getMessageId()
  {
    Assert.expect(_imageReconstructionMessage != null);

    return _imageReconstructionMessage.getId();
  }

  /**
    * @author Roy Williams
    */
  public ImageReconstructionEngine getImageReconstructionEngine()
  {
    Assert.expect(_writer != null);

    return _writer.getImageReconstructionEngine();
  }

  /**
   * Return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  abstract protected int getNumberOfRecords();

  /**
   * Return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  abstract protected int getSizeOfEachRecord();

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning a ByteBuffer filled with those records to be sent to the
   * ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  abstract protected ByteBuffer addPayLoad(ByteBuffer buffer) throws XrayTesterException;

  /**
   * some will not have a payload and we need to know about it.
   * @author George A. David
   */
  abstract protected boolean hasPayLoad();
}
