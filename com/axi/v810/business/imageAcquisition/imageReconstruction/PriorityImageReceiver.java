package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.net.*;

import com.axi.v810.business.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Roy Williams
 */
public class PriorityImageReceiver extends ImageReceiver
{
  /**
   * @author Roy Williams
   */
  public PriorityImageReceiver(ImageReconstructionEngine imageReconstructionEngine,
                               InetAddress address, int port)
      throws BusinessException
  {
    super("PriorityImageReceiver",
          imageReconstructionEngine,
          address,
          port,
          ImageReconstructionMessageEnum.ConnectToPriorityImageSender,
          Config.getInstance().getIntValue(SoftwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_PRIORITY_IMAGE_RECEIVER_BUFFER_SIZE_MEGEBYTES) * 1000000);
  }
}
