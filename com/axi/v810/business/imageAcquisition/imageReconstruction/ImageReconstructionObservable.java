package com.axi.v810.business.imageAcquisition.imageReconstruction;

import com.axi.util.*;
import java.util.*;

/**
 * Allows you to observe events that come from the image reconstruction
 *
 * @author swee-yee.wong
 */
public class ImageReconstructionObservable extends Observable
{
  private static ImageReconstructionObservable _instance;

  /**
   * @author swee-yee.wong
   */
  private ImageReconstructionObservable()
  {
    //do nothing
  }

  /**
   * @author swee-yee.wong
   */
  public static synchronized ImageReconstructionObservable getInstance()
  {
    if (_instance == null)
      _instance = new ImageReconstructionObservable();

    return _instance;
  }


  /**
   * @author swee-yee.wong
   */
  public void stateChanged(ImageReconstructionEvent imageReconstructionEvent)
  {
    Assert.expect(imageReconstructionEvent != null);

    setChanged();
    notifyObservers(imageReconstructionEvent);
  }
}

