package com.axi.v810.business.imageAcquisition.imageReconstruction;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.StringLocalizer;


/**
 * We are creating a map of exceptions that can be thrown in the Image Reconstruction
 * Processors.  When an exception is thrown with an specified integer, that value
 * will be looked up.   For now, the meaning of that exception is only unique in
 * the message sent.  The key must be recorded in the properites file if added/deleted.
 *
 * @author Roy Williams
 */
public class ImageReconstructionProcessorException extends HardwareException
{

  static private String _notImageOnAnyCameraKey         = "IRP_RECONSTRUCTION_REGION_NOT_IMAGED_KEY";
  static private String _verticalStitchingErrorKey      = "IRP_VERTICAL_STITCHING_ERROR_KEY";
  static private String _poorAlignmentPointSelectionKey = "IRP_ALIGNMENT_POINT_SELECTION_IS_ROOT_CAUSE_OF_EXCEPTION_KEY";

  private String _key;
  private String _irpNumber;
  private String _message;
  private String _originalKey;

  /**
   * @author Roy Williams
   */
  ImageReconstructionProcessorException(String key,
                                        String irpNumber,
                                        String message,
                                        String originalIrpKey)
  {
    super(new LocalizedString(key, new Object[] { irpNumber, message, originalIrpKey }));

    Assert.expect(key != null);
    Assert.expect(irpNumber != null);
    Assert.expect(message != null);
    Assert.expect(originalIrpKey != null);

    _key         = key;
    _irpNumber   = irpNumber;
    _message     = message;
    _originalKey = originalIrpKey;
  }

  /**
   * @author Roy Williams
   */
  public String key()
  {
    return _key;
  }

  /**
   * @author Roy Williams
   */
  public String irpNumber()
  {
    return _irpNumber;
  }

  /**
   * @author Roy Williams
   */
  public String message()
  {
    Assert.expect(_message != null);
    return _message;
  }

  /**
   * Method to wrap the messages from IRP with a standard header and footer.
   *
   * @author Bob Balliew
   */
  public String getLocalizedMessage()
  {
    // Get the localized header
    StringBuilder finalMessage = new StringBuilder(StringLocalizer.keyToString("HW_NOTIFY_USER_MUST_BE_TRAINED_FOR_RECOVERY_PROCESS_WARNING_KEY"));

    // Append the message from the constructor. This message is already localized
    finalMessage.append(StringLocalizer.keyToString(getLocalizedString()));

    // Get the localized footer and append it
    finalMessage.append(StringLocalizer.keyToString("GUI_COMPANY_CONTACT_INFORMATION_KEY"));

    return finalMessage.toString();
  }

  /**
   * @author Roy Williams
   */
  public String toString()
  {
    return super.getLocalizedMessage();
  }

  /**
   * @author Roy Williams
   */
  static public boolean isExceptionResultOfPoorAlignmentPointSelection(String key)
  {
    return (_notImageOnAnyCameraKey.equals(key) ||
            _verticalStitchingErrorKey.equals(key));
  }

  /**
   * @author Roy Williams
   */
  static public String poorAlignmentPointSelectionKey()
  {
    return _poorAlignmentPointSelectionKey;
  }
}

