package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * Most derived classes will use the default implementation but not CommandResultReader.
 * It wants to wants to subtlely use the thread of the caller to wait for a
 * result to be returned from the IRP.  For example, this would be used when a command
 * (or message) was sent to the IRP asking for its version.
 *
 * @author Roy Williams
 */
public class CommandResultReader extends ImageReconstructionReceiverThread
{
  /**
   *  @author Roy Williams
   */
  public CommandResultReader(ImageReconstructionEngine imageReconstructionEngine,
                             InetAddress address,
                             int port)
      throws BusinessException
  {
    super(imageReconstructionEngine, address, port, ImageReconstructionMessageEnum.ConnectToResultSender);
  }

  /**
   * Most derived classes will use this default implementation.   CommandResultReader
   * wants to be on the thread of the caller and thus does not require it.
   *
   * @author Roy Williams
   */
  protected void initializeThread()
  {
    // do nothing
  }

  /**
   * Read data from the socket to obtain a result.  This object intentionally
   * does not have a thread because this operation will be called when a command
   * is sent to the IRP requiring a result.  On occasions when one of these commands
   * is issued the same calling thread will be diverted to this readData() method.
   * <p>
   * Reason is because we would like to have any error thrown to represent the
   * stack backtrace to this function.
   *
   * @author Roy Williams
   */
  public ByteBuffer readData() throws BusinessException
  {
    if (ImageReconstructionEngine.isDeepSimulation() == false)
    {
      if (getImageReconstructionEngine().isSimulationModeOn() == true)
      {
        return getByteBuffer(4);
      }
    }

    ByteBuffer byteBuffer = getByteBuffer(4);
    int size = read(byteBuffer);
    return byteBuffer;
  }

  /**
   * Expect derived classes to send readData to an Observable.   In this case,
   * there is no Observable.  So the caller must extract the information from the
   * byte[] returned by readData.
   *
   * @author Roy Williams
   */
  public void dispatchDataToObservable(ByteBuffer byteBuffer)
  {
    // do nothing
  }
}
