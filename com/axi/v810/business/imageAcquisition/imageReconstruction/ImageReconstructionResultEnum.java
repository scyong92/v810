package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.util.*;

import com.axi.util.*;

/**
 * This enum defines a numerical index for results returned from the ImageReconstrcutionProcessor.
 * Any message ID in this class will be interpreted as a result type.   The payload of the
 * specific results should be defined by the individual object implementing that specific result.
 *
 * @author Bob Balliew
 */
public class ImageReconstructionResultEnum extends com.axi.util.Enum
{
  private static Map<Integer, ImageReconstructionResultEnum>
      _idToInstanceMap = new HashMap<Integer, ImageReconstructionResultEnum>();

  public static final ImageReconstructionResultEnum Void0Result = new ImageReconstructionResultEnum(100);
  public static final ImageReconstructionResultEnum Integer8Result = new ImageReconstructionResultEnum(101);
  public static final ImageReconstructionResultEnum Integer16Result = new ImageReconstructionResultEnum(102);
  public static final ImageReconstructionResultEnum Integer32Result = new ImageReconstructionResultEnum(104);
  public static final ImageReconstructionResultEnum Integer64Result = new ImageReconstructionResultEnum(108);
  public static final ImageReconstructionResultEnum Float32Result = new ImageReconstructionResultEnum(204);
  public static final ImageReconstructionResultEnum Float64Result = new ImageReconstructionResultEnum(208);
  public static final ImageReconstructionResultEnum Char8String = new ImageReconstructionResultEnum(301);

  /**
   * @author Bob Balliew
   */
  private ImageReconstructionResultEnum(int id)
  {
    super(id);

    _idToInstanceMap.put(new Integer(id), this);
  }

  /**
   * @author Rex Shang
   */
  public static ImageReconstructionResultEnum getEnumById(int id)
  {
    ImageReconstructionResultEnum me = _idToInstanceMap.get(id);

    Assert.expect(me != null, "Invalid id");
    return me;
  }

}
