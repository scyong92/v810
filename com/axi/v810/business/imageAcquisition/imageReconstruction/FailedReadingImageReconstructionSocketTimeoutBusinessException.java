package com.axi.v810.business.imageAcquisition.imageReconstruction;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * Thrown when we cannot read from the ImageReconstructioSocket within a timeout period
 *
 * @author Roy Williams
 */
public class FailedReadingImageReconstructionSocketTimeoutBusinessException extends ImageReconstructionProcessorCommunicationException
{
  /**
   * @author Roy Williams
   */
  public FailedReadingImageReconstructionSocketTimeoutBusinessException(
      ImageReconstructionEngine ire,
      String ipAddress,
      int portNumber,
      int timeoutInSeconds)
  {
    super(ire, new LocalizedString("BS_FAILED_READING_IMAGE_RECONSTRUCTION_SOCKET_TIMEOUT_KEY",
                                   new Object[]{ipAddress,
                                                Integer.toString(portNumber),
                                                Integer.toString(timeoutInSeconds)}));
    Assert.expect(ipAddress != null);
    Assert.expect(portNumber > 0);
    Assert.expect(timeoutInSeconds > 0);
  }
}
