package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.channels.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This proxy provides a communication path to/from the ImageReconstructionProcessors.
 *
 * @author Roy Williams
 */
public class ImageReconstructionSocket
{

  private SocketChannel _socketChannel;
  private ImageReconstructionEngine _imageReconstructionEngine;
  protected InetAddress _inetAddress;
  private long _lastReadTimeInMilliseconds = 0;
  protected int _port = -1;

  private static boolean _loopback = true;
  protected static ImageAcquisitionExceptionObservable _iaeExceptionObservable = ImageAcquisitionExceptionObservable.getInstance();
  protected static Config  _config = Config.getInstance();
  private static int _idleCommunicationsIntervalDetectionInMilliseconds = 1000;

  /**
   * @author Roy Williams
   */
  public static void setLoopback(boolean state)
  {
    _loopback = state;
  }

  /**
   * @author Roy Williams
   */
  public static boolean isLoopback()
  {
    return _loopback;
  }

  /**
   * @author Roy Williams
   */
  protected ImageReconstructionSocket(String threadName,
                                      ImageReconstructionEngine imageReconstructionEngine,
                                      InetAddress address,
                                      int port,
                                      ImageReconstructionMessageEnum usageID) throws BusinessException
  {
    this(imageReconstructionEngine, address, port, usageID, 0);
  }

  /**
   * Constructor creates a socket connection that can be used for communications
   * to or from the IRP.  When the socket is opened, a messageID will be immediately
   * pushed out to identify the caller.   This identifies the caller to the IRP and
   * allows it to shift the connection to an appropriate, transient port in lieu of
   * the "well-known" port provided as an input parameter.
   *
   * @author Roy Williams
   */
  protected ImageReconstructionSocket(ImageReconstructionEngine imageReconstructionEngine,
                                      InetAddress address,
                                      int port,
                                      ImageReconstructionMessageEnum usageID,
                                      int receiveBufferSize) throws BusinessException
  {
//    Assert.expect(imageReconstructionEngine != null);
    Assert.expect(address != null);
    Assert.expect(port > 0);
    Assert.expect(usageID != null);

    _inetAddress = address;
    _port = port;
    _imageReconstructionEngine = imageReconstructionEngine;


    if (ImageReconstructionEngine.isDeepSimulation() == false)
    {
      if (_imageReconstructionEngine.isSimulationModeOn() == true)
      {
        if (_loopback == false )
        {
          try
          {
            _inetAddress = InetAddress.getLocalHost();
          }
          catch (UnknownHostException ex)
          {
            Assert.expect(false);
          }
        }
        else
          return;
      }
    }

    openSocketChannel();
    initializeSocket(receiveBufferSize);
    connectSocketChannel();

    // Tell the Image Reconstruction Processor who we are by sending the messageId.
    ByteBuffer buffer = getByteBuffer(JavaTypeSizeEnum.SHORT.getSizeInBytes() +
                                      JavaTypeSizeEnum.SHORT.getSizeInBytes() +
                                      JavaTypeSizeEnum.INT.getSizeInBytes());
    buffer.rewind();
    int usage = usageID.getId();
    if (_config.getBooleanValue(SoftwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_COMMUNICATIONS_HANDSHAKE))
    {
      usage = usage | 0x1000;
    }
    buffer.putShort((short)usage);
    buffer.putShort((short)0);   // size of payload
    buffer.putInt(1);            // count/number of payload(s)
    buffer.rewind();
    write(buffer);
  }

  /**
   * @author George A. David
   */
  private void openSocketChannel() throws BusinessException
  {
    try
    {
      _socketChannel = SocketChannel.open();
    }
    catch (IOException ex)
    {
      BusinessException bex = new FailedOpeningImageReconstructionSocketBusinessException(
          getImageReconstructionEngine(), _inetAddress.getHostAddress(), _port);
      bex.initCause(ex);
      throw bex;
    }

  }

  /**
   * @author George A. David
   */
  private void initializeSocket(int receiveBufferSize) throws BusinessException
  {
    Assert.expect(receiveBufferSize >= 0);
    Assert.expect(_socketChannel != null);

    Socket socket = _socketChannel.socket();
    try
    {
      socket.setTcpNoDelay(true);
    }
    catch (SocketException ex2)
    {
      BusinessException bex = new FailedInitializingImageReconstructionSocketBusinessException(
          getImageReconstructionEngine(), _inetAddress.getHostAddress(), _port);
      bex.initCause(ex2);
      throw bex;
    }

    try
    {
      socket.bind(_imageReconstructionEngine.getBindInetAddress());
    }
    catch (IOException ex4)
    {
      BusinessException bex = new FailedInitializingImageReconstructionSocketBusinessException(
          getImageReconstructionEngine(), _inetAddress.getHostAddress(), _port);
      bex.initCause(ex4);
      throw bex;
    }
  }

  /**
   * @author George A. David
   */
  private void connectSocketChannel() throws BusinessException
  {
    try
    {
      _socketChannel.connect(new InetSocketAddress(_inetAddress, _port));
    }
    catch (IOException ex)
    {
      BusinessException bex = new FailedConnectingToImageReconstructionSocketBusinessException(
          getImageReconstructionEngine(), _inetAddress.getHostAddress(), _port);
      bex.initCause(ex);
      throw bex;
    }
  }

  /**
   * @author George A. David
   */
  public void write(ByteBuffer buffer) throws BusinessException
  {
    Assert.expect(buffer != null);
    try
    {
      if (_socketChannel.isConnected())
        _socketChannel.write(buffer);
    }

    catch (IOException ex)
    {
      BusinessException bex = new FailedWritingToImageReconstructionSocketBusinessException(
      getImageReconstructionEngine(), _inetAddress.getHostAddress(), _port);
      bex.initCause(ex);
      throw bex;
    }
  }

  /**
   * @author Roy Williams
   * @author George A. David
   */
  protected int read(ByteBuffer buffer) throws BusinessException
  {
    Assert.expect(buffer != null);

    int bytesRead = 0;
    try
    {
      bytesRead = _socketChannel.read(buffer);
      _lastReadTimeInMilliseconds = System.currentTimeMillis();
    }
    catch (IOException ex)
    {
      // During shutdown the _socketChannel is closed and then set to null.
      if (_socketChannel != null)
      {
        BusinessException bex = new FailedReadingImageReconstructionSocketBusinessException(
            getImageReconstructionEngine(), _inetAddress.getHostAddress(), _port);
        bex.initCause(ex);
        throw bex;
      }
      return 0;
    }

    if (_socketChannel != null)
    {
      if (bytesRead <= 0)
        throw new FailedReadingImageReconstructionSocketBusinessException(
            getImageReconstructionEngine(), _inetAddress.getHostAddress(), _port);
    }
    return bytesRead;
  }

  /**
   * @author George A. David
   */
  public boolean isConnected()
  {
    return _socketChannel != null && _socketChannel.socket().isConnected();
  }

  /**
   * Return the imageReconstructionEngine this reader or writer is working on behalf of.
   *
   * @author Roy Williams
   */
  public ImageReconstructionEngine getImageReconstructionEngine()
  {
    Assert.expect(_imageReconstructionEngine != null);

    return _imageReconstructionEngine;
  }

  /**
   * Return the imageReconstructionEngine this reader or writer is working on behalf of.
   *
   * @author Roy Williams
   */
  public int getPortNumber()
  {
    return _port;
  }

  /**
   * Return the InetAddress and thus imageReconstructionEngine this reader or writer
   * is working on behalf of.
   *
   * @author Roy Williams
   */
  public InetAddress getInetAddress()
  {
    return _inetAddress;
  }

  /**
   * Return a ByteBuffer to be used for loading data into from the inputStream.
   * @author Roy Williams
   */
  protected ByteBuffer getByteBuffer(int capacity)
  {
    Assert.expect(capacity > 0);

    ByteBuffer byteBuffer = ByteBuffer.allocate(capacity);
    byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
    return byteBuffer;
  }

  /**
   * Close the socket connection to the remote process.  Closing the SocketChannel
   * also closes the Socket as its subordinate member.
   *
   * @author Roy Williams
   */
  protected synchronized void abort(XrayTesterException xrayTesterException)
  {
    Assert.expect(_iaeExceptionObservable != null);
    Assert.expect(xrayTesterException != null);

    // CR28915: Divide by zero (or some other fatal problem) causes IRP to shutdown
    // all open sockets.   Our only recourse is to also shutdown connections to
    // avoid the hang.  Roy    5/1/2007
    if (UnitTest.unitTesting() == false)
      _imageReconstructionEngine.closeConnectionsOnNextAbort();

    // Note: this results in the exception being delivered to the ReconstructedImagesProducer
    // update(update(Observable observable, Object object)) function where xrayTesterException
    // is the object.
    _iaeExceptionObservable.notifyObserversOfException(xrayTesterException);
  }

  /**
   * Close the socket channel.   Do not be expecting to use it again.  You will
   * need to construct a new one.
   *
   * @author Roy Williams
   */
  public void close()
  {
    try
    {
      if (_socketChannel != null)
      {
        SocketChannel tempSocketChannel = _socketChannel;
        _socketChannel = null;
        tempSocketChannel.close();
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * This is turned off by default.   We would like to leave it in the code for
   * debugging purposes in the field.
   *
   * @author Roy Williams
   */
  public void waitForReadyAcknowledgement(ImageReconstructionMessageEnum usageID) throws BusinessException
  {
    if (_config.getBooleanValue(SoftwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_COMMUNICATIONS_HANDSHAKE))
    {
      // Get an acknowledgement message to indicate that IRP is up and running on this socket.
      ByteBuffer buffer = readByteBuffer(JavaTypeSizeEnum.SHORT.getSizeInBytes());
      if (isConnected() == false)
        return;

      // Okay!   Let's process it!
      short ackUsageId = buffer.getShort();
      if (ackUsageId != (short)usageID.getId())
        throw new FailedCommunicationHandshakeImageReconstructionSocketBusinessException(
            getImageReconstructionEngine(), _inetAddress.getHostAddress());
    }
  }

  /**
   * Allocate a ByteBuffer of the given size and read the bytes from the channel
   * to fill it.
   *
   * @author Roy Williams
   */
  protected ByteBuffer readByteBuffer(int size) throws BusinessException
  {
    Assert.expect(size > 0);

    ByteBuffer byteBuffer = getByteBuffer(size);
    return readBuffer(byteBuffer, size);
  }

  /**
   * Read from the socketChannel to fill the provided ByteBuffer.  This function
   * is handy because it returns the ByteBuffer back to the caller with the position
   * reset to the beginning.  This is an attempt to thwart BufferOverflowExceptions.
   *
   * @author Roy Williams
   */
  protected ByteBuffer readBuffer(ByteBuffer byteBuffer, int capacity) throws BusinessException
  {
    // Rewind the position back to the beginning so we can fill without an OverFlow.
    byteBuffer.rewind();
    int totalRead = 0;
    while (totalRead < capacity)
    {
      int bytesRead = read(byteBuffer);
      if (isConnected() == false)
        return byteBuffer;
      totalRead += bytesRead;
    }

    // The read advances the position of the buffer.  Lets be polite and reset
    // it back to the beginning.
    byteBuffer.rewind();
    return byteBuffer;
  }

  /**
   * @author Roy Williams
   */
  public boolean isReceiverBusy()
  {
    return (System.currentTimeMillis() - _lastReadTimeInMilliseconds) < _idleCommunicationsIntervalDetectionInMilliseconds;
  }
}
