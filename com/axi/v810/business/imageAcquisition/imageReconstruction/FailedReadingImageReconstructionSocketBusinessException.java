package com.axi.v810.business.imageAcquisition.imageReconstruction;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * Thrown when we cannot read from the ImageReconstructioSocket
 *
 * @author George David
 */
public class FailedReadingImageReconstructionSocketBusinessException extends ImageReconstructionProcessorCommunicationException
{
  /**
   * @author George David
   */
  public FailedReadingImageReconstructionSocketBusinessException(ImageReconstructionEngine ire, String ipAddress, int portNumber)
  {
    super(ire, new LocalizedString("BS_FAILED_READING_IMAGE_RECONSTRUCTION_SOCKET_KEY",
                                   new Object[]{ipAddress, Integer.toString(portNumber)}));
    Assert.expect(ipAddress != null);
    Assert.expect(portNumber > 0);
  }
}
