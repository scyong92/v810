package com.axi.v810.business.imageAcquisition.imageReconstruction;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * Thrown when we cannot read from the ImageReconstructioSocket within a timeout period
 *
 * @author Roy Williams
 */
public class FailedCommunicationHandshakeImageReconstructionSocketBusinessException extends ImageReconstructionProcessorCommunicationException
{
  /**
   * @author Roy Williams
   */
  public FailedCommunicationHandshakeImageReconstructionSocketBusinessException(ImageReconstructionEngine ire, String ipAddress)
  {
    super(ire, new LocalizedString("BS_FAILED_READING_IMAGE_RECONSTRUCTION_HANDSHAKE_KEY",
                                   new Object[]{ipAddress}));

    Assert.expect(ipAddress != null);
  }
}
