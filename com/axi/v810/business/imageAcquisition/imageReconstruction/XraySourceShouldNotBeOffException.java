package com.axi.v810.business.imageAcquisition.imageReconstruction;

import com.axi.v810.hardware.*;
import com.axi.util.Assert;
import com.axi.util.LocalizedString;

/**
 * @author Roy Williams
 */
public class XraySourceShouldNotBeOffException extends HardwareException
{

  /**
   * @author Roy Williams
   */
  public XraySourceShouldNotBeOffException()
  {
    super(new LocalizedString("HW_XRAY_SOURCE_SHOULD_NOT_BE_OFF_EXCEPTION_KEY", new Object[]{}));
  }
}
