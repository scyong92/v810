package com.axi.v810.business.imageAcquisition.imageReconstruction;

/**
 * @author Roy Williams
 */
public interface AcknowledgeImageAcquisitionProcessorSyncInt
{
  public void acknowledgeMessage();
  public int  getMessageId();
}
