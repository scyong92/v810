package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 * Help ImageReconstructionInfoReceiver to build data structures (result)
 * to be passed to the observer.
 */
public class ImageReconstructionInformationFactory
{
  private ImageReconstructionInformationFactory()
  {
  }

  /**
   * Based on what type of message it is, dispatch the work to other internal
   * methods.
   * @author Rex Shang
   */
  static Object getNewObject(ImageReconstructionMessageEnum messageType,
                             ImageReconstructionResultEnum resultFormatType,
                             ByteBuffer byteBuffer)
  {
    Assert.expect(messageType != null);
    Assert.expect(resultFormatType != null);
    Assert.expect(byteBuffer != null);

    if (messageType.equals(ImageReconstructionMessageEnum.GetVersion))
      return getApplicationVersion(resultFormatType, byteBuffer);
    else
    {
      Assert.expect(false, "Invalid message type.");
    }

    return null;
  }

  /**
   * Extract the data from byte buffer and build a real java object here.
   * NOTE: Do not pass byte buffer into the object to be build.  This way,
   * we can determine if there is something wrong with the message ASAP.  Also,
   * it is easier to manage change down the road.
   * @author Rex Shang
   */
  private static Object getApplicationVersion(ImageReconstructionResultEnum resultFormatType, ByteBuffer byteBuffer)
  {
    Assert.expect(resultFormatType != null);
    Assert.expect(byteBuffer != null);

    if (resultFormatType.equals(ImageReconstructionResultEnum.Integer32Result))
    {
      int versionId = byteBuffer.getInt();
      return new ImageReconstructionProcessorApplicationVersion(versionId);
    }
    else
    {
      Assert.expect(false, "Invalid result format.");
    }

    Assert.expect(false, "Can not create ImageReconstructionProcessorApplicationVersion.");
    return null;
  }

}
