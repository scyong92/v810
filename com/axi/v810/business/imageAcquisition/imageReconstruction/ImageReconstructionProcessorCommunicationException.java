package com.axi.v810.business.imageAcquisition.imageReconstruction;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Roy Williams
 */
abstract public class ImageReconstructionProcessorCommunicationException extends BusinessException
{

  private ImageReconstructionEngine _imageReconstructionEngine;

  protected ImageReconstructionProcessorCommunicationException(
      ImageReconstructionEngine ire,
      LocalizedString localizedString)
  {
    super(localizedString);

    Assert.expect(ire != null);
    _imageReconstructionEngine = ire;
  }

  /**
   * @author Roy Williams
   */
  public ImageReconstructionEngine getImageReconstructionEngine()
  {
    return _imageReconstructionEngine;
  }
}
