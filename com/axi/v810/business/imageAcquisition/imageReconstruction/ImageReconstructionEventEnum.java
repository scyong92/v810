package com.axi.v810.business.imageAcquisition.imageReconstruction;

import com.axi.util.Assert;

/**
 * Enumeration for ImageReconstructionEngine events.
 *
 * @author Dave Ferguson
 */
public class ImageReconstructionEventEnum extends com.axi.util.Enum
{
  private static int _index = 1;
  // All projections for the current scan pass have been received
  public static final ImageReconstructionEventEnum PROJECTIONS_FOR_SCAN_PASS_RECEIVED
      = new ImageReconstructionEventEnum(_index++);
  public static final ImageReconstructionEventEnum BEGIN_IRP_CONNECT_TO_CAMERAS
      = new ImageReconstructionEventEnum(_index++);
  public static final ImageReconstructionEventEnum END_IRP_CONNECT_TO_CAMERAS
      = new ImageReconstructionEventEnum(_index++);

  /**
   * @author Dave Ferguson
   */
  private ImageReconstructionEventEnum(int id)
  {
    super(id);
  }
}
