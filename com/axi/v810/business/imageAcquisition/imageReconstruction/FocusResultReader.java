package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * Reads FocusResult objects back from an ImageReconstructionProcessor through
 * a socket provided by the ImageReconstructionReader (base clase).   When a
 * FocusResult is obtained it will be distributed through the FocusResultsObservable.
 *
 * @author Roy Williams
 */
public class FocusResultReader extends ImageReconstructionReceiverThread
{
  private int FOCUS_RESULT_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes() +
                                  JavaTypeSizeEnum.INT.getSizeInBytes() +
                                  JavaTypeSizeEnum.INT.getSizeInBytes() +
                                  JavaTypeSizeEnum.INT.getSizeInBytes() +
                                  JavaTypeSizeEnum.INT.getSizeInBytes() +
                                  JavaTypeSizeEnum.INT.getSizeInBytes() +
                                  JavaTypeSizeEnum.INT.getSizeInBytes() +
                                  JavaTypeSizeEnum.INT.getSizeInBytes();

  /**
   * @author Roy Williams
   */
  public FocusResultReader(ImageReconstructionEngine imageReconstructionEngine,
                           InetAddress address,
                           int port)
      throws BusinessException
  {
    super(imageReconstructionEngine, address, port, ImageReconstructionMessageEnum.ConnectToFocusResultSender);
  }

  /**
   * @author Roy Williams
   */
  public ByteBuffer readData() throws BusinessException
  {
    if (ImageReconstructionEngine.isDeepSimulation() == false)
    {
      if (getImageReconstructionEngine().isSimulationModeOn() == true)
      {
        return null;
      }
    }

    return readByteBuffer(FOCUS_RESULT_SIZE);
  }

  /**
   * @author Roy Williams
   */
  public void dispatchDataToObservable(ByteBuffer byteBuffer)
  {
    byteBuffer.rewind();
    FocusResult focusResult = new FocusResult(
      byteBuffer.getInt(),   // _reconstructionRegionId
      byteBuffer.getInt(),   // _focusGroupId
      byteBuffer.getInt(),   // _zPeakInNanometers
      byteBuffer.getInt(),   // _zUpperEdgeInNanometers
      byteBuffer.getInt(),   // _zLowerEdgeInNanometers
      byteBuffer.getInt(),   // _numSurfaceModelNeighborsUsed
      byteBuffer.getInt(),   // _surfaceModelUncertainty
      byteBuffer.getInt());  // _surfaceModelPredictedZ
    FocusResultsObservable.getInstance().setFocusResult(focusResult);
  }
}
