package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.net.*;
import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class presumes that all images delivered from the IRP are for the same
 * reconstruction region.   They may be delivered out of order.  Once delivered,
 * a new reconstruction region will be handled for the next incoming region.
 *
 * @author Roy Williams
 */
public class ImageReceiver extends ImageReconstructionReceiverThread
{
  private static boolean _debug = false;
  private ReconstructedImages _reconstructedImages;
  private boolean _currentReconstructedImagesIsComplete = false;
  private boolean _paused = false;
  private ImageAcquisitionProgressMonitor _progressMonitor = ImageAcquisitionProgressMonitor.getInstance();
  private ReconstructedImages _previousReconstructedImages = null;
  private ReconstructedImagesProducer _reconstructedImagesProducer = ImageAcquisitionEngine.getInstance().getReconstructedImagesProducer();

  /**
   * @author Roy Williams
   */
  public ImageReceiver(ImageReconstructionEngine imageReconstructionEngine, InetAddress address, int port)
      throws BusinessException
  {
    super(imageReconstructionEngine,
          address,
          port,
          ImageReconstructionMessageEnum.ConnectToImageSender,
          Config.getInstance().getIntValue(SoftwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_IMAGE_RECEIVER_BUFFER_SIZE_MEGEBYTES) * 1000000);
  }

  /**
   * @author Roy Williams
   */
  protected ImageReceiver(String threadName,
                          ImageReconstructionEngine imageReconstructionEngine,
                          InetAddress address,
                          int port,
                          ImageReconstructionMessageEnum usageID,
                          int receiveBufferSize)
      throws BusinessException
  {
    super(imageReconstructionEngine, address, port, usageID, receiveBufferSize);
  }

  /**
   * @author George A. David
   */
  public synchronized void initialize()
  {
    _reconstructedImages = null;
    _previousReconstructedImages = null;
  }

  /**
   * @author Roy Williams
   */
  public synchronized void setPause(boolean state)
  {
    _paused = state;
    if (_paused == false)
      notifyAll();
  }

  /**
   * @author Roy Williams
   */
  private synchronized void waitForResume()
  {
    if (_paused == true)
    {
      try
      {
        wait(200);
      }
      catch (InterruptedException ex)
      {
        // do nothing
      }
    }
  }

  /**
   * Direct ByteBuffers are used when the buffer contents may be passed down to
   * JNI (or C++) code.  They are allocated outside of the JVM.
   * <p>
   * Allocate a ByteBuffer of the given size and read the bytes from the channel
   * to fill it.
   *
   * @author Roy Williams
   * @author Patrick Lacz
   */
  protected Image getImage(int regionId, int sliceId, int width, int height) throws BusinessException
  {
    Assert.expect(width > 0);
    Assert.expect(height > 0);

    // This code may not be ideal. We could also read in a byte array and assign it into
    // the allocated image or cast its data in JNI to floats. At some point we should experiment with
    // these options to decide which is best.
    Image contiguousImage = null;
    Image image = null;
    try
    {
      contiguousImage = Image.createContiguousFloatImage(width, height);
      int pixels = width * height;
      ByteBuffer byteBuffer = readBuffer(contiguousImage.getByteBuffer(), pixels * 4);
      image = new Image(width, height);
      if (image != null)
        Transform.copyImageIntoImage(contiguousImage, image);
    }
    finally
    {
      if (contiguousImage == null)
      {
        throw new FailedImageMemoryAllocationBusinessException(
            getImageReconstructionEngine(),
            _inetAddress.getHostAddress(),
            _port,
            regionId,
            sliceId,
            width,
            height);
      }
      contiguousImage.decrementReferenceCount();

      if (image == null)
      {
        throw new FailedImageMemoryAllocationBusinessException(
            getImageReconstructionEngine(),
            _inetAddress.getHostAddress(),
            _port,
            regionId,
            sliceId,
            width,
            height);
      }
    }
    return image;
  }

  /**
   * @author Roy Williams
   */
  private int getHeaderSize()
  {
    return JavaTypeSizeEnum.INT.getSizeInBytes() +
        JavaTypeSizeEnum.INT.getSizeInBytes() +
        JavaTypeSizeEnum.INT.getSizeInBytes() +
        JavaTypeSizeEnum.INT.getSizeInBytes() +
        JavaTypeSizeEnum.INT.getSizeInBytes();
  }

  /**
   * @author Roy Williams
   * @author Lim Boon Hong
   */
  public ByteBuffer readData() throws BusinessException
  {
    // Wait here till the IRP's are resumed.
    while (_paused == true)
      waitForResume();

    /**
     * Content of header - version 1
     * ===============================
     * 1. Header version - 4 byte, unsigned int
     * 2. Header's size - 4 byte, unsigned int
     * 3. RRID - 4 byte, unsigned int
     * 4. SLID - 4 byte, unsigned int
     * 5. Reconstructed height - 4 byte, unsigned int
     * 6. Sharpness profile's size - 4 byte, unsigned int
     *    6.1 zheight array (initial value) - 4 byte each element, float
     *    6.2 sharpness array (initial value) - 4 byte each element, float
     * 7. Width - 4 byte, unsigned int
     * 8. Height - 4 byte, unsigned int
     */
    
    // Add by Khang Wah, Get the header version first.
    ByteBuffer byteBuffer = readByteBuffer(4); 
    int headerVersion = byteBuffer.getInt(); 
    
    // Add by Boon Hong, Get the header size
    byteBuffer = readByteBuffer(4);
    int headerSize = byteBuffer.getInt();
    
    // Add by Boon Hong, first 8 bytes already taken by headerVersion & headerSize
    byteBuffer = readByteBuffer(headerSize-8);
    //ByteBuffer byteBuffer = readByteBuffer(getHeaderSize());
    
    if (isConnected() == false || _reconstructedImagesProducer.isAbortInProgress())
    {
      if (_reconstructedImages != null)
      {
        _reconstructedImages.decrementReferenceCount();
        _reconstructedImages = null;
      }
    }

    // Okay!   Let's process it!
    byteBuffer.rewind();
    Assert.expect(byteBuffer.order().equals(ByteOrder.LITTLE_ENDIAN));

    // Reconstruction Region Id: int32
    int regionId = byteBuffer.getInt();
    
    // Slice Id: int32
    int sliceId = byteBuffer.getInt();
    
    // Fiducial relative z-height in nanometers
    int fiducialRelativeZHeightInNanometers = byteBuffer.getInt();
    
    //Get profile size.//Add by BH.
    int profileSize = byteBuffer.getInt();
    
    List<Float> arrayZHeight = new ArrayList<Float>();
    List<Float> arraySharpness = new ArrayList<Float>();
    
    //Get array of Z-Height.//Add by BH.
    for(int i = 0; i <profileSize; ++i)
    {
      Float zh = new Float(byteBuffer.getFloat());
      arrayZHeight.add(zh);
    }
    
    //Get array of sharpness.//Add by BH.
    for(int j = 0; j <profileSize; ++j)
    {      
      Float sh = new Float(byteBuffer.getFloat());
      arraySharpness.add(sh);
    }   
    
    int speedCombinationSize = byteBuffer.getInt();
    List<StageSpeedEnum> speedCombination = new ArrayList<StageSpeedEnum>();
    for(int k = 0; k <speedCombinationSize; ++k)
    {      
      Integer speed = new Integer(byteBuffer.getInt());
      
      double speedFloat = (float)speed / 10f;
      speedCombination.add(EnumStringLookup.getInstance().getstageSpeedEnum(speedFloat+""));
    }
    List<StageSpeedEnum> speedToRemoved = new ArrayList<StageSpeedEnum>();
    for (int i = 0; i <(speedCombination.size() - Config.getInstance().getIntValue(SoftwareConfigEnum.MAXIMUM_SPEED_SELECTION)); i++)
      speedToRemoved.add(speedCombination.get(1 + i));
    speedCombination.removeAll(speedToRemoved);
    // Image Width (pixels): int32
    int width = byteBuffer.getInt();
    
    // Image Length (pixels): int32
    int height = byteBuffer.getInt();
    
    if (width == 0 || height == 0)
    {
      // When the IRP is offended it will close connections resulting in a buffer
      // that is initialized to zeros.
      throw new FailedReadingImageReconstructionSocketBusinessException(
          getImageReconstructionEngine(), _inetAddress.getHostAddress(), _port);
    }

    // Either abort by reseting _reconstructedImages, jump to a new
    // _reconstructedImages,
    if (_reconstructedImagesProducer.isAbortInProgress())
    {
      resetReconstructedImages();
    }
    else if (_reconstructedImages == null)
    {
      _reconstructedImages = getImageReconstructionEngine().getReconstructedImages(regionId);
      _currentReconstructedImagesIsComplete = false;
    }
    else
    {
      if (regionId != _reconstructedImages.getReconstructionRegionId())
      {
        // Putting the test external so we don't have to create the message to
        // send to the Assert each time we test for the condition.
        Assert.expect(false, createDebugInfo(regionId, sliceId, width, height));
      }
    }

    // Now get the image now that we know how big it is.
    Image image = getImage(regionId, sliceId, width, height);

    // Prior getImage() flushes the image out of the network buffer of the IRP.  If
    // we are aborted or _reconstructedImages == null ... get out!
    if (_reconstructedImagesProducer.isAbortInProgress() ||
        _reconstructedImages == null)
    {
      resetReconstructedImages();

      // CR1045 fix by LeeHerng - De-reference image in case of aborting
      image.decrementReferenceCount();
      
      return byteBuffer;
    }

    if (_reconstructedImages.getReconstructionRegion().isRegionUsedForExposureLearning())
    {
      Subtype subtype = _reconstructedImages.getReconstructionRegion().getSubtypes().iterator().next();
      InspectionEvent inspectionEvent = new SubtypeExposureLearningEvent(subtype, speedCombination);
      InspectionEventObservable.getInstance().sendEventInfo(inspectionEvent);
    }
    
    // Adding to _reconstructedImages will increment the reference count and we
    // must decrement the extra reference count resulting from image creation.
    //Add arrayZHeight, arraySharpness by BH
    _reconstructedImages.addImage(image, SliceNameEnum.getEnumFromIndex(sliceId), fiducialRelativeZHeightInNanometers, arrayZHeight, arraySharpness);
    image.decrementReferenceCount();

    // If we've gotten all the individual images for this region, we can remove the ReconstructedImages object from the IRE.
    _currentReconstructedImagesIsComplete = _reconstructedImages.hasImageForEachReconstructableSlice();
    if (_currentReconstructedImagesIsComplete)
    {
      // The current _reconstructedImages is okay.  Save it to previous for next time through loop.
      //_previousReconstructedImages = _reconstructedImages;

      getImageReconstructionEngine().allImagesReceivedForRegion(_reconstructedImages);
    }

    if (UnitTest.unitTesting() || _debug)
    {
      ImageReconstructionEngine ire = getImageReconstructionEngine();

      String space = " ";
      System.out.println("IRP: " + ire.getId() + " received image: " +
                         regionId + "-"     +
                         sliceId  + space   +
                         (fiducialRelativeZHeightInNanometers / 25400.0f) +
                         space + (width + " x " + height));
    }

    // ok, we got an image, let's notify the progress monitor
    _progressMonitor.imageReceived();
    return byteBuffer;
  }

  /**
   */
  private void resetReconstructedImages()
  {
    if (_reconstructedImages != null &&   _reconstructedImages.isImageBeingReferenced())
      _reconstructedImages.decrementReferenceCount();
    _reconstructedImages = null;
  }

  /**
   * @author Roy Williams
   */
  public void dispatchDataToObservable(ByteBuffer unused) throws XrayTesterException
  {
    Assert.expect(unused != null);

    // ReconstructedImages are created (one per ReconstructionRegion) by the
    // ImageAcquisitionEngine.   If we have a complete image, we will send it
    // to the ReconstrcutedImagesManager queue.
    if (_currentReconstructedImagesIsComplete && _reconstructedImages != null)
    {
      getImageReconstructionEngine().getReconstructedImagesManager().putReconstructedImages(_reconstructedImages);

      _reconstructedImages.decrementReferenceCount();
      // now that our images are delivered lets get ready for another set.
      _reconstructedImages = null;
    }
  }

  /**
   * @author Roy Williams
   */
  private String createDebugInfo(int incomingRegionId, int sliceId, int width, int height)
  {
    ReconstructionRegion currentRegion = _reconstructedImages.getReconstructionRegion();
    String newline = StringUtil.getLineSeparator();
    String indent  = "  ";
    StringBuffer messageBuffer = new StringBuffer(
        "Insufficient ReconstructedImages slices (" + _reconstructedImages.getNumberOfSlices() +
        " of " + _reconstructedImages.getNumberOfSlicesExpected() + ")" + newline);
    messageBuffer.append(indent + "ReconstructionRegion: " + currentRegion.getRegionId() +
        " expected slices: " + currentRegion.getNumberOfSlicesToBeReconstructed() + newline);
    messageBuffer.append(indent + "Receiver type: " + getClass().getName() + newline);
    messageBuffer.append(indent + "TestProgram isCreateOnlyInspectedSlicesEnabled: " +
                         currentRegion.getTestSubProgram().getTestProgram().isCreateOnlyInspectedSlicesEnabled() +
                         newline);
    messageBuffer.append(indent + "ReconstructedImagesProducer.isAborting: " +
                         getImageReconstructionEngine().getReconstructedImagesProducer().isAbortInProgress() +
                         newline);
    messageBuffer.append(newline);
    if (_previousReconstructedImages != null)
      messageBuffer.append(getRegionInfo("Previous region: ", _previousReconstructedImages));
    messageBuffer.append(getRegionInfo("Current  region: ", _reconstructedImages));

    // Arriving image info.
    ReconstructedImages  incomingReconstructedImages = getImageReconstructionEngine().getReconstructedImages(incomingRegionId);
    ReconstructionRegion incomingRegion              = incomingReconstructedImages.getReconstructionRegion();
    messageBuffer.append(indent + "Arriving image  info"   + newline);
    messageBuffer.append(indent + indent + "New image slice size: " + width + " x " + height + newline);
    messageBuffer.append(indent + indent + "region  id: " + incomingRegionId + newline);
    messageBuffer.append(indent + indent + "slice   id: " + sliceId + newline);
    for (Slice slice : incomingRegion.getSlices())
    {
      SliceNameEnum sliceNameEnum = slice.getSliceName();
      if (sliceNameEnum.getId() == sliceId)
      {
        messageBuffer.append(indent + indent + "slice name: " + sliceNameEnum.getName() + newline);
        messageBuffer.append(indent + indent + "isCreateSlice: " + slice.createSlice() + newline);
        messageBuffer.append(indent + indent + "isReconstructionSlice: " + slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION) + newline);
        break;
      }
    }
    return messageBuffer.toString();
  }

  /**
   * @author Roy Williams
   */
  private String getRegionInfo(String header, ReconstructedImages reconstructedImages)
  {
    ReconstructionRegion region = reconstructedImages.getReconstructionRegion();
    String space  = " ";
    String indent = "  ";
    String newline = StringUtil.getLineSeparator();
    StringBuffer buf = new StringBuffer(indent + header + region.getRegionId()  +
                                        indent + "Number of slices received: "  +
                                        reconstructedImages.getNumberOfSlices() +
                                        newline);
    buf.append(indent + indent + "Component: " + region.getComponent().getComponentType().getReferenceDesignator() + newline);
    List<ScanPass> scanPasses = region.getScanPasses();
    buf.append(indent + indent + "ScanPassCount: " + scanPasses.size() + " on pass numbers '");
    for (ScanPass scanPass : scanPasses)
      buf.append(scanPass.getId() + space);
    buf.append("'" + newline);
    for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
    {
      Slice slice = region.getSlice(reconstructedSlice.getSliceNameEnum());
      buf.append(indent + indent + "'" + slice.getSliceName().getName()    +
                 "' isCreateSlice: " + slice.createSlice()                 +
                 "' isReconstructionSlice: '"                              +
                 slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION) +
                 newline);
    }
    buf.append(newline);
    return buf.toString();
  }

  /**
   * @author Roy Williams
   */
  public int getNumberOfReconstructedImageSlices()
  {
    return (_reconstructedImages == null) ? 0 :
        _reconstructedImages.getNumberOfSlices();
  }

  /**
   * @author Roy Williams
   */
  public static void setDebug(boolean state)
  {
    _debug = state;
  }

  /**
   * @author George A. David
   */
  public boolean isPaused()
  {
    return _paused;
  }
}
