package com.axi.v810.business.imageAcquisition.imageReconstruction;

import com.axi.util.*;

/**
 * This class allows Observers to be updated when Focus Results are
 * available.
 *
 * @author Dave Ferguson
 */
public class FocusResultsObservable extends ThreadSafeObservable
{
  private static FocusResultsObservable _instance;

  /**
   * @author Dave Ferguson
   */
  public static synchronized FocusResultsObservable getInstance()
  {
    if (_instance == null)
      _instance = new FocusResultsObservable();
    return _instance;
  }

  /**
   * @author Dave Ferguson
   */
  private FocusResultsObservable()
  {
    super("focus results observable");
  }

  /**
   * @author Dave Ferguson
   */
  public void setFocusResult(FocusResult focusResult)
  {
    Assert.expect(focusResult != null);
    setChanged();
    notifyObservers(focusResult);
  }

}
