package com.axi.v810.business.imageAcquisition.imageReconstruction;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * Thrown when we get a null pointer back from allocating native memory.
 *
 * @author Roy Williams
 */
public class FailedMessageMemoryAllocationBusinessException extends BusinessException
{
  /**
   * @author Roy Williams
   */
  public FailedMessageMemoryAllocationBusinessException(int size)
  {
    super(new LocalizedString("BS_FAILED_MESSAGE_MEMORY_ALLOCATION_KEY",
                              new Object[] {Integer.toString(size)}));
  }
}
