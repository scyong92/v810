package com.axi.v810.business.imageAcquisition.imageReconstruction;

import com.axi.util.*;


/**
 * @author swee-yee.wong
 */
public class ImageReconstructionEvent
{
  private ImageReconstructionEventEnum _imageReconstructionEventEnum;
  // this is used for the begin cases. It tells the observers how many events to expect.
  int _numEventsToExpect = -1;

  /**
   * @author swee-yee.wong
   */
  public ImageReconstructionEvent(ImageReconstructionEventEnum imageReconstructionEventEnum)
  {
    Assert.expect(imageReconstructionEventEnum != null);

    _imageReconstructionEventEnum = imageReconstructionEventEnum;
  }

  /**
   * @author swee-yee.wong
   */
  public void setImageReconstructionEventEnum(ImageReconstructionEventEnum imageReconstructionEventEnum)
  {
    Assert.expect(imageReconstructionEventEnum != null);

    _imageReconstructionEventEnum = imageReconstructionEventEnum;
  }

  /**
   * @author swee-yee.wong
   */
  public ImageReconstructionEventEnum getImageReconstructionEventEnum()
  {
    Assert.expect(_imageReconstructionEventEnum != null);

    return _imageReconstructionEventEnum;
  }
}
