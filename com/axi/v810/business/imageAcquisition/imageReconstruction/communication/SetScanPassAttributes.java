package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * @author Roy Williams
 */
public class SetScanPassAttributes extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes() +
                             JavaTypeSizeEnum.INT.getSizeInBytes() +
                             JavaTypeSizeEnum.INT.getSizeInBytes() +
                             JavaTypeSizeEnum.INT.getSizeInBytes() +
                             JavaTypeSizeEnum.INT.getSizeInBytes() +
                             JavaTypeSizeEnum.INT.getSizeInBytes() +
                             JavaTypeSizeEnum.INT.getSizeInBytes();
  private List<ScanPass> _scanPath;
  private int _testSubProgramId = -1;

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public SetScanPassAttributes(CommandSender writer,
                               List<ScanPass> scanPath,
                               int testSubProgramId)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.SetScanPassAttributes, writer);
    Assert.expect(scanPath != null);
    Assert.expect(testSubProgramId >= 0);
    _scanPath = scanPath;
    _testSubProgramId = testSubProgramId;
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    Assert.expect(_scanPath != null);
    return _scanPath.size();
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    int size = getNumberOfRecords();
    int numberOfScanPasses = _scanPath.size();
    Assert.expect(numberOfScanPasses >= 0);

    for (ScanPass scanPass : _scanPath)
    {

      StagePosition startStagePosition = scanPass.getStartPointInNanoMeters();
      StagePosition endStagePosition = scanPass.getEndPointInNanoMeters();

      buffer.putInt(scanPass.getId());

      // Number of ScanPasses in ScanPath
      buffer.putInt(numberOfScanPasses);

      // Test Sub Program
      buffer.putInt(_testSubProgramId);

      // Start x location of stage in machine coordinates nanometers: int32
      buffer.putInt(startStagePosition.getXInNanometers());

      // Start y location of stage in machine coordinates nanometers: int32
      buffer.putInt(startStagePosition.getYInNanometers());

      // End x location of stage in machine coordinates nanometers: int32
      buffer.putInt(endStagePosition.getXInNanometers());

      // End y location of stage in machine coordinates nanometers: int32
      buffer.putInt(endStagePosition.getYInNanometers());
    }
    return buffer;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }
}


