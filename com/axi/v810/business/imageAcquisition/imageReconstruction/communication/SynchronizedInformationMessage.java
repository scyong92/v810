package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Rex Shang
 */
public abstract class SynchronizedInformationMessage extends ImageReconstructionMessage
{
  private static Config _config = Config.getInstance();
  private ImageReconstructionInformationReceiver _syncReceiver;
  private BooleanLock _messageAcknowledgeReceived;
  private static long   _currentTimeOutMilliSeconds =
      ImageAcquisitionProgressMonitor.getInstance().getCurrentTimeOutInMilliSeconds() / 3;

  /**
   * Create a message with my designated message ID.
   *
   * @author Rex Shang
   */
  protected SynchronizedInformationMessage(ImageReconstructionMessageEnum messageId,
                                           CommandSender writer,
                                           ImageReconstructionInformationReceiver syncReceiver)
      throws XrayTesterException
  {
    super(messageId, writer);

    Assert.expect(syncReceiver != null);

    _syncReceiver = syncReceiver;
    // Register with the ImageStateReceiver to have it notify me when the abort
    // acknowledgement arrives.
    _syncReceiver.addNotifier(this);
    _messageAcknowledgeReceived = new BooleanLock(false);
  }

  /**
   * @author Rex Shang
   */
  public void send() throws XrayTesterException
  {
    _messageAcknowledgeReceived.setValue(false);

    // Send the message.
    super.send();

    // Await for the reply before returning only if we are not in sim mode.
    if (_config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION))
      return;

    try
    {
      _messageAcknowledgeReceived.waitUntilTrue(_currentTimeOutMilliSeconds);
    }
    catch (InterruptedException iex)
    {
      Assert.expect(false, "unexpected interruption of wait for abort ack");
    }

    if (_messageAcknowledgeReceived.isFalse())
    {
      ImageReconstructionEngine ire = getImageReconstructionEngine();
      BusinessException bex = new FailedReadingImageReconstructionSocketTimeoutBusinessException(
          getImageReconstructionEngine(),
          ire.getFirstIpAddress().getHostAddress(),
          ire.getPort(),
          (int)_currentTimeOutMilliSeconds/1000);
      throw bex;
    }
  }

  /**
   * @return the number of records to be sent to the IRP.  Default is 1.
   *
   * @author Rex Shang
   */
  protected int getNumberOfRecords()
  {
    return 1;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Rex Shang
   */
  protected int getSizeOfEachRecord()
  {
    return 0;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(false);
    return null;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return false;
  }

  /**
   * @author Rex Shang
   */
  public void informationReceived(Object arg)
  {
    setMessage(arg);

    // Unlock the send().
    _messageAcknowledgeReceived.setValue(true);
  }

  /**
   * @author Rex Shang
   */
  abstract public void setMessage(Object arg);

}
