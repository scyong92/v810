package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 *
 * @author wei-chin.chong
 */
public class SetAutoFocusAttributes extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.FLOAT.getSizeInBytes() +
                                JavaTypeSizeEnum.FLOAT.getSizeInBytes() +
                                JavaTypeSizeEnum.FLOAT.getSizeInBytes() +
                                JavaTypeSizeEnum.FLOAT.getSizeInBytes() +
                                JavaTypeSizeEnum.FLOAT.getSizeInBytes() +   
                                JavaTypeSizeEnum.FLOAT.getSizeInBytes() + 
                                JavaTypeSizeEnum.FLOAT.getSizeInBytes();

  // standard machine bottom Offset
  int _bottomOfBoardOffsetInNanometer = Config.getInstance().getIntValue(SoftwareConfigEnum.BOTTOM_OF_BOARD_OFFSET_IN_NANOMETERS);
  // XXL machine top board offset
  int _topOfBoardOffsetInNanometer = Config.getInstance().getIntValue(SoftwareConfigEnum.TOP_OF_BOARD_OFFSET_IN_NANOMETERS);
  
  int _updwardCarrierUncertaintyInNanometer =XrayTester.getMaximumCarrierUpWarpInNanoMeters();
  int _downwardCarrierUncertaintyInNanometer = XrayTester.getMaximumCarrierDownWarpInNanoMeters();
  int _upwardBoardBottomUncertaintyInNanometer = Config.getInstance().getIntValue(SoftwareConfigEnum.UPWARD_BOARD_BOTTOM_UNCERTAINTY_IN_NANOMETERS);
  int _downwardBoardBottomUncertaintyInNanometer = Config.getInstance().getIntValue(SoftwareConfigEnum.DOWNWARD_BOARD_BOTTOM_UNCERTAINTY_IN_NANOMETERS);
  int _upwardWarpInNanometer = XrayTester.getMaximumPanelUpWarpInNanoMeters();
  int _downwardWarpInNanometer = XrayTester.getMaximumPanelDownWarpInNanoMeters();
  int _maxComponentHeightInNanometer = XrayTester.getMaximumJointHeightInNanoMeters();
  float _boardThicknessVariationRatio = (float)Config.getInstance().getDoubleValue(SoftwareConfigEnum.BOARD_THICKNESS_VARIATION_RATIO);
  
  float _boardCenterMaxZNM;
  float _boardCenterMinZNM;
  float _topSideComponentMaxZNM;
  float _topSideComponentMinZNM;
  float _bottomSideComponentMaxZNM;
  float _bottomSideComponentMinZNM;
  
  int _thicknessNm;
  int _meanBoardsOffsetInNanometer;

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public SetAutoFocusAttributes(CommandSender writer,int boardThickness, int meanBoardsOffsetInNanometer) throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.SetAutoFocusAttribues, writer);
    
    _thicknessNm = boardThickness;
   _meanBoardsOffsetInNanometer = meanBoardsOffsetInNanometer;
    computeComponentRangeLimits();
    dataValidation();
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return 1;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    
    buffer.putFloat(_boardCenterMaxZNM);
    buffer.putFloat(_boardCenterMinZNM);
    buffer.putFloat(_topSideComponentMaxZNM);
    buffer.putFloat(_topSideComponentMinZNM);
    buffer.putFloat(_bottomSideComponentMaxZNM);
    buffer.putFloat(_bottomSideComponentMinZNM);
    buffer.putInt(_maxComponentHeightInNanometer);

    return buffer;
  }
  
  // Compute the expected range for both bottom side and top side
  // components given the system specifications and board thickness
  // Author: Rick Gaudette
  // @author Wei Chin (modify for XXL)
  final void computeComponentRangeLimits()
  {
    if(XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
    {
      _topOfBoardOffsetInNanometer += _meanBoardsOffsetInNanometer;
      _bottomOfBoardOffsetInNanometer = _topOfBoardOffsetInNanometer - _thicknessNm;      
    }
    else
    {
      _bottomOfBoardOffsetInNanometer += _meanBoardsOffsetInNanometer ;
    }
    
    if(XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
    {
      _boardCenterMaxZNM = _topOfBoardOffsetInNanometer 
        - 0.5f * _thicknessNm
      * (1.0f + _boardThicknessVariationRatio)
      + _upwardBoardBottomUncertaintyInNanometer + _updwardCarrierUncertaintyInNanometer + _upwardWarpInNanometer;
    }
    else
    {
      _boardCenterMaxZNM = 0.5f * _thicknessNm
        * (1.0f + _boardThicknessVariationRatio)
        +_bottomOfBoardOffsetInNanometer + _upwardBoardBottomUncertaintyInNanometer + _updwardCarrierUncertaintyInNanometer + _upwardWarpInNanometer;
    }
    
    _boardCenterMaxZNM = (float)Math.min(_boardCenterMaxZNM, SetHardwareConstants.getFocalRangeUpsliceInNanometers());

    if(XrayTester.isXXLBased())
    {
      _boardCenterMinZNM = _topOfBoardOffsetInNanometer 
        - 0.5f * _thicknessNm
        * (1.0f - _boardThicknessVariationRatio) 
        - _downwardBoardBottomUncertaintyInNanometer - _downwardCarrierUncertaintyInNanometer - _downwardWarpInNanometer;
    }
    else
    {
      _boardCenterMinZNM = 0.5f * _thicknessNm
        * (1.0f - _boardThicknessVariationRatio) 
        + _bottomOfBoardOffsetInNanometer - _downwardBoardBottomUncertaintyInNanometer - _downwardCarrierUncertaintyInNanometer - _downwardWarpInNanometer;
    }
    
    _boardCenterMinZNM = (float)Math.max(_boardCenterMinZNM, SetHardwareConstants.getFocalRangeDownsliceInNanometers());

    if(XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
    {
      _topSideComponentMaxZNM = _topOfBoardOffsetInNanometer
      + _upwardBoardBottomUncertaintyInNanometer
      + _updwardCarrierUncertaintyInNanometer
      + _upwardWarpInNanometer
      + _maxComponentHeightInNanometer;
    }
    else
    {
      _topSideComponentMaxZNM = _thicknessNm
      * (1.0f + _boardThicknessVariationRatio)
      + _bottomOfBoardOffsetInNanometer
      + _upwardBoardBottomUncertaintyInNanometer
      + _updwardCarrierUncertaintyInNanometer
      + _upwardWarpInNanometer
      + _maxComponentHeightInNanometer;
    }
    
    _topSideComponentMaxZNM = (float)Math.min(_topSideComponentMaxZNM, SetHardwareConstants.getFocalRangeUpsliceInNanometers());

    if(XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
    {
      _topSideComponentMinZNM = _topOfBoardOffsetInNanometer 
        - _downwardBoardBottomUncertaintyInNanometer 
        - _downwardCarrierUncertaintyInNanometer
        - _downwardWarpInNanometer;
    }
    else
    {
      _topSideComponentMinZNM = _thicknessNm * (1.0f - _boardThicknessVariationRatio)  
        + _bottomOfBoardOffsetInNanometer 
        - _downwardBoardBottomUncertaintyInNanometer 
        - _downwardCarrierUncertaintyInNanometer
        - _downwardWarpInNanometer;
    }
    
    _topSideComponentMinZNM =  (float)Math.max(_topSideComponentMinZNM, SetHardwareConstants.getFocalRangeDownsliceInNanometers());

    if(XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
    {
      _bottomSideComponentMaxZNM = _topOfBoardOffsetInNanometer 
        - _thicknessNm * (1.0f - _boardThicknessVariationRatio)
        + _upwardBoardBottomUncertaintyInNanometer
        + _updwardCarrierUncertaintyInNanometer
        + _upwardWarpInNanometer;
    }
    else
    {
      _bottomSideComponentMaxZNM = _bottomOfBoardOffsetInNanometer
        + _upwardBoardBottomUncertaintyInNanometer
        + _updwardCarrierUncertaintyInNanometer
        + _upwardWarpInNanometer;
    }
    
    _bottomSideComponentMaxZNM = (float)Math.min(_bottomSideComponentMaxZNM, SetHardwareConstants.getFocalRangeUpsliceInNanometers());    

    if(XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
    {
      _bottomSideComponentMinZNM = _topOfBoardOffsetInNanometer 
        - _thicknessNm * (1.0f + _boardThicknessVariationRatio)
        - _downwardBoardBottomUncertaintyInNanometer
        - _downwardCarrierUncertaintyInNanometer
        - _downwardWarpInNanometer
        - _maxComponentHeightInNanometer;
    }
    else
    {
      _bottomSideComponentMinZNM = _bottomOfBoardOffsetInNanometer
        - _downwardBoardBottomUncertaintyInNanometer
        - _downwardCarrierUncertaintyInNanometer
        - _downwardWarpInNanometer
        - _maxComponentHeightInNanometer;
    }
    
    _bottomSideComponentMinZNM =  (float)Math.max(_bottomSideComponentMinZNM, SetHardwareConstants.getFocalRangeDownsliceInNanometers());
  }
  
  /**
   * @author Wei Chin
   */
  final void dataValidation() throws AutofocusAttributesBusinessException
  {
    if(false)
    {
      System.out.println("_topSideComponentMaxZNM : " + _topSideComponentMaxZNM);
      System.out.println("_topSideComponentMinZNM : " + _topSideComponentMinZNM);
      System.out.println("_bottomSideComponentMaxZNM : " + _bottomSideComponentMaxZNM);
      System.out.println("_bottomSideComponentMinZNM : " + _bottomSideComponentMinZNM);
      System.out.println("_boardCenterMaxZNM : " + _boardCenterMaxZNM);
      System.out.println("_boardCenterMinZNM : " + _boardCenterMinZNM);
    }
    if(_topSideComponentMaxZNM < _topSideComponentMinZNM)
    {
      throw new AutofocusAttributesBusinessException(new LocalizedString("AUTOFOCUS_ATTRIBUTES_EXCEPTION_KEY",null));
    }
    if(_bottomSideComponentMaxZNM < _bottomSideComponentMinZNM)
    {
      throw new AutofocusAttributesBusinessException(new LocalizedString("AUTOFOCUS_ATTRIBUTES_EXCEPTION_KEY",null));
    }
    if(_boardCenterMaxZNM < _boardCenterMinZNM)
    {
      throw new AutofocusAttributesBusinessException(new LocalizedString("AUTOFOCUS_ATTRIBUTES_EXCEPTION_KEY",null));
    }
  }
}
