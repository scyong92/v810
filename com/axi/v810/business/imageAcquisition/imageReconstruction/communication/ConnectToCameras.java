package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;
import java.nio.ByteBuffer;

/**
 * @author Dave Ferguson
 * @edited by Beh Kheng Ling
 */
public class ConnectToCameras extends ImageReconstructionMessage
{
  private ImageStateReceiver _synchNotifier;
  private boolean _isNotified = false;
  private ConnectToCamerasEnum _cameraConnectionStatus = null;
  
  private static final int _bitMaskFailedOthers = 0x80000000;
  private static final int _bitMaskFailedAddressInUse = 0x40000000;
  
  private final Object _syncBarrier = new Object();
  
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes();
  
  private int _startPortNumber = -1;
  
  private ConnectToCamerasComplete _completeNotifier;
  private ConnectToCamerasFailedOthers _failedOthersNotifier;
  private ConnectToCamerasFailedAddressInUse _failedAddressInUseNotifier;
  
  private TimerUtil _timer = new TimerUtil();
  
  private static class ConnectToCamerasComplete implements AcknowledgeImageAcquisitionProcessorSyncInt
  {
    ConnectToCameras _owner;
    public ConnectToCamerasComplete(ConnectToCameras owner)
    {
      _owner = owner;
    }
    
    @Override
    public void acknowledgeMessage() 
    {
      _owner.connectComplete();
    }

    @Override
    public int getMessageId() 
    {
      return ImageReconstructionMessageEnum.ConnectToCameras.getId();
    }
  }
  
  private static class ConnectToCamerasFailedOthers implements AcknowledgeImageAcquisitionProcessorSyncInt
  {
    ConnectToCameras _owner;
    public ConnectToCamerasFailedOthers(ConnectToCameras owner)
    {
      _owner = owner;
    }
    
    @Override
    public void acknowledgeMessage() 
    {
      _owner.connectFailedOthers();
    }

    @Override
    public int getMessageId() {
      return ImageReconstructionMessageEnum.ConnectToCameras.getId() | _bitMaskFailedOthers;
    }
  }
  
  private static class ConnectToCamerasFailedAddressInUse implements AcknowledgeImageAcquisitionProcessorSyncInt
  {
    ConnectToCameras _owner;
    public ConnectToCamerasFailedAddressInUse(ConnectToCameras owner)
    {
      _owner = owner;
    }
    
    @Override
    public void acknowledgeMessage() 
    {
      _owner.connectFailedAddressInUse();
    }

    @Override
    public int getMessageId() {
      return ImageReconstructionMessageEnum.ConnectToCameras.getId() | _bitMaskFailedAddressInUse;
    }
  }
  
  /**
   * Create a message with my designated message ID.
   *
   * @author Dave Ferguson
   */
  public ConnectToCameras(CommandSender writer, ImageStateReceiver synchNotifier, int startPortNumber)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.ConnectToCameras, writer);
    
    Assert.expect(synchNotifier != null);
    _synchNotifier = synchNotifier;
    
    _startPortNumber = startPortNumber;
    
    _completeNotifier = new ConnectToCamerasComplete(this);
    _failedOthersNotifier = new ConnectToCamerasFailedOthers(this);
    _failedAddressInUseNotifier = new ConnectToCamerasFailedAddressInUse(this);
    
    _synchNotifier.notifyMessage(_completeNotifier);
    _synchNotifier.notifyMessage(_failedOthersNotifier);
    _synchNotifier.notifyMessage(_failedAddressInUseNotifier);
  }

  @Override
  protected int getNumberOfRecords() 
  {
    return 1;
  }

  @Override
  protected int getSizeOfEachRecord() 
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  @Override
  protected ByteBuffer addPayLoad(ByteBuffer buffer) throws XrayTesterException 
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    
    buffer.putInt(_startPortNumber);
    
    return buffer;
  }

  @Override
  protected boolean hasPayLoad() 
  {
    return true;
  }
  
  public ConnectToCamerasEnum sendAndSync() throws XrayTesterException
  {  
    _isNotified = false;
    
    super.send();
    _timer.reset();
    
    synchronized (_syncBarrier) 
    {
      _timer.start();
      for (int i = 0; i < 400; ++i) 
      {
        try 
        {
          _syncBarrier.wait(100);
          
          if (_isNotified)
            return _cameraConnectionStatus;
        } 
        catch (InterruptedException ex) 
        {
          // do nothing
        }
      }
      _timer.stop();
    }
    System.out.println("Time for Camera Connection Status is notified: " + _timer.getElapsedTimeInMillis());
    _cameraConnectionStatus = ConnectToCamerasEnum.CONNECTION_FAILED_ASSIGNING_PORT;
    return _cameraConnectionStatus;
  }
  
  public void connectComplete()
  {
    _synchNotifier.denotifyMessage(_failedOthersNotifier);
    _synchNotifier.denotifyMessage(_failedAddressInUseNotifier);

    synchronized (_syncBarrier) 
    {
      _cameraConnectionStatus = ConnectToCamerasEnum.CONNECTION_COMPLETE;
      _isNotified = true;
      _syncBarrier.notify();
    }
  }
  
  public void connectFailedOthers()
  {
    _synchNotifier.denotifyMessage(_completeNotifier);
    _synchNotifier.denotifyMessage(_failedAddressInUseNotifier);
   
    synchronized (_syncBarrier) 
    {
      _cameraConnectionStatus = ConnectToCamerasEnum.CONNECTION_FAILED_OTHERS;
      _isNotified = true;
      _syncBarrier.notify();
    }
  }
  
  public void connectFailedAddressInUse()
  {
    _synchNotifier.denotifyMessage(_completeNotifier);
    _synchNotifier.denotifyMessage(_failedOthersNotifier);
   
    synchronized (_syncBarrier) 
    {
      _cameraConnectionStatus = ConnectToCamerasEnum.CONNECTION_FAILED_ADDRESS_IN_USE;
      _isNotified = true;
      _syncBarrier.notify();
    }
  }
}
