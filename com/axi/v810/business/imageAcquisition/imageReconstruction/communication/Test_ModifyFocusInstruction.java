package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the ModifyFocusInstructionZHeight message.
 * @author Scott Richardson
 */
public class Test_ModifyFocusInstruction extends UnitTest
{
  /**
   * @author Scott Richardson
   */
  public void test( BufferedReader in, PrintWriter out )
  {

    testIrpMessage(42, 2, 5, 6, 7, (byte)8);
    testIrpMessage(420, 10, -2, -3, -4, (byte)5);
  }

  /**
   * @author Scott Richardson
   */
  private void testIrpMessage(int reconstructionRegionId,
                              int sliceId,
                              int zHeightInNanoMeters,
                              int percent,
                              int zOffsetInNanoMeters,
                              byte focusMethodId)
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      ModifyFocusInstruction modifyFocusInstructionMsg =
        new ModifyFocusInstruction(commandWriter,
                                   1,
                                   reconstructionRegionId,
                                   sliceId,
                                   zHeightInNanoMeters,
                                   percent,
                                   zOffsetInNanoMeters,
                                   focusMethodId);

      ByteBuffer msg = modifyFocusInstructionMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 25));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.ModifyFocusInstruction.getId());
      Expect.expect(messageSize == 25);
      Expect.expect(messageCount == 1);
      int payloadTestSubProgramId = msg.getInt();
      int payloadReconstructionRegionId = msg.getInt();
      int payloadSliceId = msg.getInt();
      int payloadZHeight = msg.getInt();
      Expect.expect(payloadTestSubProgramId == 1);
      Expect.expect(payloadReconstructionRegionId == reconstructionRegionId);
      Expect.expect(payloadSliceId == sliceId);
      Expect.expect(payloadZHeight == zHeightInNanoMeters);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Scott Richardson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_ModifyFocusInstruction() );
  }
}
