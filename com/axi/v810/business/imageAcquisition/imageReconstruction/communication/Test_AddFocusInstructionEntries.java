package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Test class for the AddFocusInstructionEntries message.
 *
 * @author Dave Ferguson
 */
public class Test_AddFocusInstructionEntries extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    testIrpMessage(42,43,1,FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,(byte)2,-20,0,5768);
    testIrpMessage(10,20,1,FocusMethodEnum.PERCENT_BETWEEN_EDGES,(byte)1,80,0,20);
    testIrpMessage(2000,1,2,FocusMethodEnum.USE_Z_HEIGHT,(byte)0,0,254001,-15);
  }

  /**
   * @author Dave Ferguson
   */
  private void testIrpMessage(int reconstructionRegionId,
                              int focusGroupId,
                              int sliceId,
                              FocusMethodEnum focusMethod,
                              byte expectedFocusMethodEnumId,
                              int percentValue,
                              int zHeight,
                              int zOffset)
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      Slice slice1 = new Slice();
      FocusInstruction focusInstruction1 = new FocusInstruction();
      focusInstruction1.setFocusMethod(focusMethod);
      focusInstruction1.setPercentValue(percentValue);
      focusInstruction1.setZHeightInNanoMeters(zHeight);
      focusInstruction1.setZOffsetInNanoMeters(zOffset);
      slice1.setFocusInstruction(focusInstruction1);
      slice1.setSliceName(SliceNameEnum.PAD);

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      AddFocusInstructionEntries addFocusInstructionEntriesMsg =
        new AddFocusInstructionEntries(commandWriter, reconstructionRegionId, focusGroupId, slice1);

      ByteBuffer msg = addFocusInstructionEntriesMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 25));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.AddFocusInstructionTableEntry.getId());
      Expect.expect(messageSize == 25);
      Expect.expect(messageCount == 1);
      int payloadSlice = msg.getInt();
      int payloadFocusGroupId = msg.getInt();
      int payloadReconstructionRegionId = msg.getInt();
      byte payloadFocusMethod = msg.get();
      int payloadZHeightInNanometers = msg.getInt();
      int payloadPercentValue = msg.getInt();
      int payloadZOffsetInNanometers = msg.getInt();
      Expect.expect(payloadSlice == slice1.getSliceName().getId());
      Expect.expect(payloadFocusGroupId == focusGroupId);
      Expect.expect(payloadReconstructionRegionId == reconstructionRegionId);
      Expect.expect(payloadFocusMethod == expectedFocusMethodEnumId);
      Expect.expect(payloadZHeightInNanometers == zHeight);
      Expect.expect(payloadPercentValue == percentValue);
      Expect.expect(payloadZOffsetInNanometers == zOffset);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }


  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_AddFocusInstructionEntries() );
  }
}
