package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
* @author Roy Williams
*/
public class ModifyFocusProfileShape extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() + 
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() + 
                              JavaTypeSizeEnum.INT.getSizeInBytes();
  private int  _testSubProgramId = -1;
  private int  _regionID         = -1;
  private int  _focusGroupId     = -1;
  private byte _profileShape     = -1;
  private int _zGuessFromPredictiveSliceHeightInNanometers = -1;
  private byte _usePhaseShiftProfiliometry = (byte) 0x00;
  private int _searchRangeLowLimitInNanometers = -1;
  private int _searchRangeHighLimitInNanometers = -1;
  private int _pspZOffsetInNanometers = -1;

  /**
   * Create a message with my designated message ID.
   * @author Roy Williams
   */
  public ModifyFocusProfileShape(CommandSender writer,
                                 int testSubProgramId,
                                 int regionId,
                                 int focusGroup,
                                 byte profileShape,
                                 int zGuessFromPredictiveSliceHeightInNanometers,
                                 boolean usePhaseShiftProfiliometry,
                                 int searchRangeLowLimitInNanometers,
                                 int searchRangeHighLimitInNanometers,
                                 int pspZOffsetInNanometers
      ) throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.ModifyFocusProfileShape, writer);
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(regionId >= 0);
    Assert.expect(focusGroup >= 0);
    Assert.expect(profileShape >= 0);

    _testSubProgramId = testSubProgramId;
    _regionID         = regionId;
    _focusGroupId     = focusGroup;
    _profileShape     = profileShape;
    _zGuessFromPredictiveSliceHeightInNanometers = zGuessFromPredictiveSliceHeightInNanometers;
    _usePhaseShiftProfiliometry = usePhaseShiftProfiliometry ? (byte) 0x01 : (byte) 0x00;
    _searchRangeLowLimitInNanometers = searchRangeLowLimitInNanometers;
    _searchRangeHighLimitInNanometers = searchRangeHighLimitInNanometers;
    _pspZOffsetInNanometers = pspZOffsetInNanometers;

    // For testing only.   Roy
    if (_debug)
    {
      System.out.println("ModifyFocusProfileShape, " +
                         _testSubProgramId + ", " +
                         _regionID + ", " +
                         profileShape + ", " +
                         _zGuessFromPredictiveSliceHeightInNanometers + ", " +
                         usePhaseShiftProfiliometry + ", " +
                         _searchRangeLowLimitInNanometers + ", " +
                         _searchRangeHighLimitInNanometers + ", " + 
                         _pspZOffsetInNanometers);
    }
  }

  /**
   * @return the number of records to be sent to the IRP.
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return 1;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    buffer.putInt(_testSubProgramId);
    buffer.putInt(_regionID);
    buffer.putInt(_focusGroupId);
    buffer.put(_profileShape);
    buffer.putInt(_zGuessFromPredictiveSliceHeightInNanometers);
    buffer.put(_usePhaseShiftProfiliometry);
    buffer.putInt(_searchRangeLowLimitInNanometers);
    buffer.putInt(_searchRangeHighLimitInNanometers);
    buffer.putInt(_pspZOffsetInNanometers);
    return buffer;
  }

  /**
   * @author Roy Williams
   */
  protected boolean hasPayLoad()
  {
    return true;
  }
}
