package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class Reset extends SynchronizedAbstractStateMessage
{
  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public Reset(CommandSender writer, ImageStateReceiver synchNotifier) throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.Reset, writer, synchNotifier);
  }
}
