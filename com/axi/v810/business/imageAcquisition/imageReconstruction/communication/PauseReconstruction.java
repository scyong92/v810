package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class PauseReconstruction extends AbstractStateMessage
{
  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public PauseReconstruction(CommandSender writer) throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.Pause, writer);
  }
}
