package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.awt.geom.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class SetRuntimeAlignmentMatrixValues extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes()    +
                              JavaTypeSizeEnum.DOUBLE.getSizeInBytes() +
                              JavaTypeSizeEnum.DOUBLE.getSizeInBytes() +
                              JavaTypeSizeEnum.DOUBLE.getSizeInBytes() +
                              JavaTypeSizeEnum.DOUBLE.getSizeInBytes() +
                              JavaTypeSizeEnum.DOUBLE.getSizeInBytes() +
                              JavaTypeSizeEnum.DOUBLE.getSizeInBytes();

  private int _testSubProgramId;
  private double _matrix[] = new double[6];

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public SetRuntimeAlignmentMatrixValues(CommandSender writer,
                                         int    testSubProgramId,
                                         AffineTransform affineTransform)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.SetRuntimeAlignmentMatrix, writer);
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(affineTransform != null);

    _testSubProgramId = testSubProgramId;

    _matrix[0] = affineTransform.getScaleX();
    _matrix[1] = affineTransform.getShearX();
    _matrix[2] = affineTransform.getTranslateX();
    _matrix[3] = affineTransform.getShearY();
    _matrix[4] = affineTransform.getScaleY();
    _matrix[5] = affineTransform.getTranslateY();
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return 1;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    buffer.putInt(_testSubProgramId);

    for (int i=0; i<_matrix.length; i++)
    {
      buffer.putDouble(_matrix[i]);
    }

    return buffer;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }
}

