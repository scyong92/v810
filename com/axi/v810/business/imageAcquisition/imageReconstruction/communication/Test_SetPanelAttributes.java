package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;

/**
 * Test class for the SetPanelAttributes message.
 *
 * @author Dave Ferguson
 */
public class Test_SetPanelAttributes extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      TestProgram testProgram = new TestProgram();
      Project project = new Project(false);
      testProgram.setProject(project);
      Panel panel = new Panel();
      PanelSettings panelSettings = new PanelSettings();
      panelSettings.setPanel(panel);
      panel.setPanelSettings(panelSettings);
      project.setPanel(panel);
      panel.setWidthInNanoMeters(500);
      panel.setLengthInNanoMeters(600);
      panel.setThicknessInNanoMeters(100);
      panelSettings.setDegreesRotationRelativeToCad(0);

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      SetPanelAttributes setPanelAttributesMsg =
        new SetPanelAttributes(commandWriter, testProgram);

      ByteBuffer msg = setPanelAttributesMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 12));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.SetPanelAttributes.getId());
      Expect.expect(messageSize == 12);
      Expect.expect(messageCount == 1);
      Expect.expect(msg.getInt() == 500);
      Expect.expect(msg.getInt() == 600);
      Expect.expect(msg.getInt() == 100);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_SetPanelAttributes() );
  }
}
