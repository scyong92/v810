package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class DisableSurfaceModeling extends SynchronizedAbstractStateMessage
{
  public DisableSurfaceModeling(CommandSender writer, ImageStateReceiver synchNotifier)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.DisableSurfaceModeling, writer, synchNotifier);
  }
}
