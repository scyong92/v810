package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the AddScanPassTableEntries message.
 *
 * @author Bob Balliew
 */
public class Test_AddScanPassTableEntries extends UnitTest
{
  /**
   * @author Bob Balliew
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      ReconstructionRegion reconstructionRegion = new ReconstructionRegion();
      ScanPass scanPass1 = new ScanPass(1, new StagePositionMutable(), new StagePositionMutable());
      ScanPass scanPass2 = new ScanPass(2, new StagePositionMutable(), new StagePositionMutable());
      reconstructionRegion.addScanPass(scanPass1);
      reconstructionRegion.addScanPass(scanPass2);

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      AddScanPassTableEntries addScanPassTableEntriesMsg =
        new AddScanPassTableEntries(commandWriter, reconstructionRegion);

      ByteBuffer msg = addScanPassTableEntriesMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 16));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.AddScanPassTableEntry.getId());
      Expect.expect(messageSize == 8);
      Expect.expect(messageCount == 2);
      Expect.expect(msg.getInt() == reconstructionRegion.getRegionId());
      Expect.expect(msg.getInt() == scanPass1.getId());
      Expect.expect(msg.getInt() == reconstructionRegion.getRegionId());
      Expect.expect(msg.getInt() == scanPass2.getId());
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Bob Balliew
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_AddScanPassTableEntries() );
  }
}
