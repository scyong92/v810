package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.business.testProgram.ReconstructionRegion;

/**
 * Test class for the SetHardwareConstants message.
 *
 * @author Dave Ferguson
 */
public class Test_SetHardwareConstants extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);
    try
    {
      Config config = Config.getInstance();
//      List<AbstractXrayCamera> listOfCameras = new ArrayList<AbstractXrayCamera>();
//      listOfCameras.add(XrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0));

      // We can just get any camera so we can get the sensor attributes
      XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
      AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);

      int bottomLeftX = 0;
      int bottomLeftY = 0;
      int width = 10;
      int height = 10;
      SystemFiducialRectangle systemFiducialRectangle = new SystemFiducialRectangle(bottomLeftX, bottomLeftY, width, height);

      // Create a list of processorStrips to indicate the number of IRPs to use.
      List<ProcessorStrip> processorStrips = new ArrayList<ProcessorStrip>();
      processorStrips.add(new ProcessorStrip(0));

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      SetHardwareConstants.setFocalRangeUpsliceInNanometers((int)XrayTester.getMaximumSliceHeightFromNominalSliceInNanometers());
      SetHardwareConstants.setFocalRangeDownsliceInNanometers((int)Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers()));
      SetHardwareConstants setHardwareConstantsMsg = new SetHardwareConstants(commandWriter);

      ByteBuffer msg = setHardwareConstantsMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 56));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.SetHardwareConstants.getId());
      Expect.expect(messageSize == 56);
      Expect.expect(messageCount == 1);
	  // Swee Yee Wong - XCR-2630 Support New X-Ray Camera
      Expect.expect(msg.getInt() == xRayCamera.getSensorWidthInPixelsWithOverHeadByte());
      Expect.expect(msg.getInt() == xRayCamera.getNumberOfUnusedPixelsOnEndOfSensor());
      Expect.expect(msg.getInt() == xRayCamera.getSensorColumnPitchInNanometers());
      Expect.expect(msg.getInt() == XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers());
      Expect.expect(msg.getInt() == XrayCameraArray.getZHeightFromCameraArrayTo2ndXraySourceInNanometers());
      int uncertaintyNominalToReferencePlane = SetHardwareConstants.getUncertaintyReferencePlaneToNominalPlaneInNanoMeters();
      Expect.expect(msg.getInt() == (int)XrayTester.getMaximumSliceHeightFromNominalSliceInNanometers() - uncertaintyNominalToReferencePlane);
      Expect.expect(msg.getInt() == (int)( - (Math.abs( XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers()) - uncertaintyNominalToReferencePlane)));
      Expect.expect(msg.getInt() == SetHardwareConstants.getMaxHardwareReconstructionRegionWidthInNanoMeters());
      Expect.expect(msg.getInt() == SetHardwareConstants.getMaxHardwareReconstructionRegionLengthInNanoMeters());
      Expect.expect(msg.getInt() == SetHardwareConstants.getMaxFocusRegionWidthInNanoMeters());
      Expect.expect(msg.getInt() == SetHardwareConstants.getMaxFocusRegionLengthInNanoMeters());
      long megaByteToByte = 1024 * 1024;
      Expect.expect(msg.getLong() == config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_ALLOCATED_PROJECTION_MEMORY_IN_MEGABYTES) * megaByteToByte);
      Expect.expect(msg.getLong() == config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_ALLOCATED_RECONSTRUCTION_MEMORY_IN_MEGABYTES) * megaByteToByte);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_SetHardwareConstants() );
  }
}
