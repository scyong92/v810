package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Test class for the AddFocusResult message.
 *
 * @author Dave Ferguson
 */
public class Test_AddFocusResult extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      FocusResult focusResult = new FocusResult(25400, 25801, 25202, 4253, 5, 0, 0, 0);

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      AddFocusResult addFocusResultMsg = new AddFocusResult(commandWriter, focusResult);

      ByteBuffer msg = addFocusResultMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 20));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.AddFocusResult.getId());
      Expect.expect(messageSize == 20);
      Expect.expect(messageCount == 1);
      Expect.expect(msg.getInt() == focusResult.getReconstructionRegionId());
      Expect.expect(msg.getInt() == focusResult.getFocusGroupId());
      Expect.expect(msg.getInt() == focusResult.getZPeakInNanometers());
      Expect.expect(msg.getInt() == focusResult.getZUpperEdgeInNanometers());
      Expect.expect(msg.getInt() == focusResult.getZLowerEdgeInNanometers());
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_AddFocusResult() );
  }
}
