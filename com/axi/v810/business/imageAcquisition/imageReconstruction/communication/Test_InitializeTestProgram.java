package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the InitializeTestProgram message.
 *
 * @author Dave Ferguson
 */
public class Test_InitializeTestProgram extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      InitializeTestProgram initializeTestProgramMsg = new InitializeTestProgram(commandWriter);

      ByteBuffer msg = initializeTestProgramMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == 8);
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.InitializeTestProgram.getId());
      Expect.expect(messageSize == 0);
      Expect.expect(messageCount == 1);
      Expect.expect(initializeTestProgramMsg.hasPayLoad() == false);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_InitializeTestProgram() );
  }
}
