package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Scott Richardson
 */
public class ModifyFocusInstruction extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes();
  private int _testSubProgramId = -1;
  private int _regionID  = -1;
  private int _sliceID = -1;
  private int _zHeightFocusInNanoMeters = -1;
  private int _percent = -1;
  private int _zOffsetInNanoMeters = -1;
  private byte _focusMethodId = -1;

  /**
   * Create a message with my designated message ID.
   * @author Roy Williams
   * @author Scott Richardson
   */
  public ModifyFocusInstruction(CommandSender writer,
                                int testSubProgramId,
                                int regionId,
                                int sliceId,
                                int zHeightFocusInNanoMeters,
                                int percent,
                                int zOffsetInNanoMeters,
                                byte focusMethodId)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.ModifyFocusInstruction, writer);
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(regionId >= 0);
    Assert.expect(sliceId >= 0);
    // zHeightFocusInNanoMeters can be positive or negative => no assert
    // percent                  can be positive or negative => no assert
    // zOffsetInNanoMeters      can be positive or negative => no assert
    Assert.expect(focusMethodId >= 0);

    _testSubProgramId = testSubProgramId;
    _regionID  = regionId;
    _sliceID = sliceId;
    _zHeightFocusInNanoMeters = zHeightFocusInNanoMeters;
    _percent = percent;
    _zOffsetInNanoMeters = zOffsetInNanoMeters;
    _focusMethodId = focusMethodId;

//    // For testing only.   Roy
    if (_debug)
    {
      System.out.println("ModifyFocusInstruction, " +
                         _testSubProgramId + ", " +
                         _regionID + ", " +
                         _sliceID + ", " +
                         _zHeightFocusInNanoMeters + ", " +
                         _percent + ", " +
                         _zOffsetInNanoMeters + ", " +
                         _focusMethodId);
    }
  }

  /**
   * @return the number of records to be sent to the IRP.
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return 1;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   * @author Roy Williams
   * @author Scott Richardson
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    buffer.putInt(_testSubProgramId);
    buffer.putInt(_regionID);
    buffer.putInt(_sliceID);
    buffer.putInt(_zHeightFocusInNanoMeters);
    buffer.putInt(_percent);
    buffer.putInt(_zOffsetInNanoMeters);
    buffer.put(_focusMethodId);
    return buffer;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }
}
