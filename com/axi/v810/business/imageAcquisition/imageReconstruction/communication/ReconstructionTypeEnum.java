package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;

/**
 * @author Roy Williams
 */
public class ReconstructionTypeEnum extends com.axi.util.Enum
{
  static private int _index = -1;

  public static final ReconstructionTypeEnum Z_HEIGHT                      = new ReconstructionTypeEnum(++_index);
  public static final ReconstructionTypeEnum PERCENT_BETEEN_EDGES          = new ReconstructionTypeEnum(++_index);
  public static final ReconstructionTypeEnum PERCENT_FROM_SHARPEST_TO_EDGE = new ReconstructionTypeEnum(++_index);
  public static final ReconstructionTypeEnum SHARPEST = new ReconstructionTypeEnum(++_index);

  /**
   * @author Roy Williams
   */
  private ReconstructionTypeEnum(int ndx)
  {
    super(ndx);
  }

  /**
   * @author Roy Williams
   */
  static public byte map(FocusMethodEnum focusMethodEnum)
  {
    if (focusMethodEnum.equals(FocusMethodEnum.USE_Z_HEIGHT))
      return (byte) Z_HEIGHT.getId();
    if (focusMethodEnum.equals(FocusMethodEnum.PERCENT_BETWEEN_EDGES))
      return (byte) PERCENT_BETEEN_EDGES.getId();
    if (focusMethodEnum.equals(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE))
      return (byte) PERCENT_FROM_SHARPEST_TO_EDGE.getId();
    if (focusMethodEnum.equals(FocusMethodEnum.SHARPEST))
      return (byte) SHARPEST.getId();
    Assert.expect(false, "Unknown mapping from FocusMethodEnum to ReconstructionTypeEnum");
    return 0;
  }
}
