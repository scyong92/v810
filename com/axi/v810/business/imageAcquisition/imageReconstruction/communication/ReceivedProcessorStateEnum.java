package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;


/**
 * @author Roy Williams
 */
public class ReceivedProcessorStateEnum extends com.axi.util.Enum
{
  static private int _index = -1;

  public static final ReceivedProcessorStateEnum RUN    = new ReceivedProcessorStateEnum(++_index);
  public static final ReceivedProcessorStateEnum PAUSE  = new ReceivedProcessorStateEnum(++_index);
  public static final ReceivedProcessorStateEnum RESUME = new ReceivedProcessorStateEnum(++_index);
  public static final ReceivedProcessorStateEnum ABORT  = new ReceivedProcessorStateEnum(++_index);
  public static final ReceivedProcessorStateEnum RESET  = new ReceivedProcessorStateEnum(++_index);

  /**
   * @author Roy Williams
   */
  private ReceivedProcessorStateEnum(int ndx)
  {
    super(ndx);
  }
}
