package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the ReconstructSpecificRegion message.
 *
 * @author Dave Ferguson
 */
public class Test_ReconstructSpecificRegion extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    testIrpMessage(42,ImageTypeEnum.AVERAGE, (byte) 0x00, -42000);
    testIrpMessage(420,ImageTypeEnum.LIGHTEST, (byte) 0x01, 25400);
  }

  /**
   * @author Dave Ferguson
   */
  private void testIrpMessage(int reconstructionRegionId,
                              ImageTypeEnum imageTypeEnum,
                              byte expectedImageTypeEnumId,
                              int reconstructionZOffsetInNanometers)
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      ReconstructSpecificRegion reconstructSpecificRegionMsg =
        new ReconstructSpecificRegion(commandWriter,
                                      1,
                                      reconstructionRegionId,
                                      imageTypeEnum,
                                      reconstructionZOffsetInNanometers);

      ByteBuffer msg = reconstructSpecificRegionMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 13));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.ReReconstructSpecificRegion.getId());
      Expect.expect(messageSize == 13);
      Expect.expect(messageCount == 1);
      int testSubProgramId = msg.getInt();
      int payloadReconstructionRegionId = msg.getInt();
      byte payloadImageType = msg.get();
      int payloadReconstructionZOffsetInNanometers = msg.getInt();
      Expect.expect(testSubProgramId == 1);
      Expect.expect(payloadReconstructionRegionId == reconstructionRegionId);
      Expect.expect(payloadImageType == expectedImageTypeEnumId);
      Expect.expect(payloadReconstructionZOffsetInNanometers == reconstructionZOffsetInNanometers);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }


  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_ReconstructSpecificRegion() );
  }
}
