package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.util.Assert;
import com.axi.util.JavaTypeSizeEnum;
import com.axi.v810.business.imageAcquisition.imageReconstruction.CommandSender;
import com.axi.v810.business.imageAcquisition.imageReconstruction.ImageReconstructionMessage;
import com.axi.v810.business.imageAcquisition.imageReconstruction.ImageReconstructionMessageEnum;
import com.axi.v810.business.panelSettings.MagnificationTypeEnum;
import com.axi.v810.datastore.config.HardwareCalibEnum;
import com.axi.v810.hardware.XrayCameraArray;
import com.axi.v810.util.XrayTesterException;

import java.nio.ByteBuffer;
import java.util.List;

/**
 * @author Rick Gaudette
 * @since 5.4
 */
public class SetEdgeSpreadFunctionRate extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = 2 * JavaTypeSizeEnum.INT.getSizeInBytes() + 2 * JavaTypeSizeEnum.FLOAT.getSizeInBytes();
  private int _numberOfCameras = -1;
  private int _numberOfMagnifications = -1;

  // @author Rick Gaudette
  public SetEdgeSpreadFunctionRate(CommandSender writer) throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.SetEdgeSpreadFunctionRate, writer);
    _numberOfCameras = XrayCameraArray.getNumberOfCameras();
    _numberOfMagnifications = MagnificationTypeEnum.getMagnificationToEnumMap().size();
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Rick Gaudette
   */
  protected int getNumberOfRecords()
  {
    Assert.expect(_numberOfCameras > 0);
    Assert.expect(_numberOfMagnifications > 0);

    return _numberOfCameras * _numberOfMagnifications;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Rick Gaudette
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }


  /**
   * @author Rick Gaudette
   */
  protected boolean hasPayLoad()
  {
    return true;
  }


  /**
   * Add the edge spread function rate for each camera and all magnifications
   *
   * @author Rick Gaudette
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());

    String[] magnificationIDs = {"LOW_MAG", "HIGH_MAG"};
    String[] dimensionIDs = {"X", "Y"};

    for (int magnificationIndex = 0; magnificationIndex < magnificationIDs.length; magnificationIndex++)
    {
      // The index is used as the value to send to the IRPs, the string is used to get the correct HardwareCalibEnum
      String magnificationID = magnificationIDs[magnificationIndex];
      for (int cameraID = 0; cameraID < 14; cameraID++)
      {
        buffer.putInt(magnificationIndex);
        buffer.putInt(cameraID);

        for (String dimensionID : dimensionIDs)
        {
          HardwareCalibEnum hardwareCalibEnum = HardwareCalibEnum.getEdgeSpreadFunctionRateEnum(magnificationID,
                                                                                                cameraID,
                                                                                                dimensionID);
          float value = (float) _config.getDoubleValue(hardwareCalibEnum);
          buffer.putFloat(value);
        }
      }
    }
    return buffer;
  }

}