package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Test class for the AddNeighborEntries message.
 *
 * @author Dave Ferguson
 */
public class Test_AddNeighborEntries extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      int reconstructionRegionId = 4242;
      ReconstructionRegion neighbor1 = new ReconstructionRegion();
      ReconstructionRegion neighbor2 = new ReconstructionRegion();
      ReconstructionRegion neighbor3 = new ReconstructionRegion();
      ReconstructionRegion neighbor4 = new ReconstructionRegion();
      ReconstructionRegion neighbor5 = new ReconstructionRegion();
      FocusGroup focusGroup = new FocusGroup();
      focusGroup.addNeighbor(neighbor1);
      focusGroup.addNeighbor(neighbor2);
      focusGroup.addNeighbor(neighbor3);
      focusGroup.addNeighbor(neighbor4);
      focusGroup.addNeighbor(neighbor5);

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      AddNeighborEntries addNeighborEntriesMsg =
        new AddNeighborEntries(commandWriter, reconstructionRegionId, focusGroup);

      ByteBuffer msg = addNeighborEntriesMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 60));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.AddNeighborTableEntry.getId());
      Expect.expect(messageSize == 12);
      Expect.expect(messageCount == 5);
      Expect.expect(msg.getInt() == focusGroup.getId());
      Expect.expect(msg.getInt() == reconstructionRegionId);
      Expect.expect(msg.getInt() == neighbor1.getRegionId());
      Expect.expect(msg.getInt() == focusGroup.getId());
      Expect.expect(msg.getInt() == reconstructionRegionId);
      Expect.expect(msg.getInt() == neighbor2.getRegionId());
      Expect.expect(msg.getInt() == focusGroup.getId());
      Expect.expect(msg.getInt() == reconstructionRegionId);
      Expect.expect(msg.getInt() == neighbor3.getRegionId());
      Expect.expect(msg.getInt() == focusGroup.getId());
      Expect.expect(msg.getInt() == reconstructionRegionId);
      Expect.expect(msg.getInt() == neighbor4.getRegionId());
      Expect.expect(msg.getInt() == focusGroup.getId());
      Expect.expect(msg.getInt() == reconstructionRegionId);
      Expect.expect(msg.getInt() == neighbor5.getRegionId());
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_AddNeighborEntries() );
  }
}
