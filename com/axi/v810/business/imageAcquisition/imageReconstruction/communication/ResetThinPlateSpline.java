package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class ResetThinPlateSpline extends SynchronizedAbstractStateMessage
{
    public ResetThinPlateSpline(CommandSender writer, ImageStateReceiver synchNotifier)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.ResetThinPlateSpline, writer, synchNotifier);
  }
}
