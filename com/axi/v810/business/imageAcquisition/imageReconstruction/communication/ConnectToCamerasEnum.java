package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

/**
 *
 * @author bee-hoon.goh
 */

public class ConnectToCamerasEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static ConnectToCamerasEnum CONNECTION_COMPLETE = new ConnectToCamerasEnum(++_index);
  public static ConnectToCamerasEnum CONNECTION_FAILED_OTHERS = new ConnectToCamerasEnum(++_index);
  public static ConnectToCamerasEnum CONNECTION_FAILED_ADDRESS_IN_USE = new ConnectToCamerasEnum(++_index);
  public static ConnectToCamerasEnum CONNECTION_FAILED_ASSIGNING_PORT = new ConnectToCamerasEnum(++_index);
  
  private ConnectToCamerasEnum(int id)
  {
    super(id);
  }
}
