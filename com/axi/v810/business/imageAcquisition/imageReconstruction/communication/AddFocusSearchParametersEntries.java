package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class AddFocusSearchParametersEntries extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes();

  private int _reconstructionRegionId = -1;
  private FocusGroup _focusGroup;


  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public AddFocusSearchParametersEntries(CommandSender writer,
                                         int reconstructionRegionId,
                                         FocusGroup focusGroup)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.AddFocusSearchParameterTableEntry, writer);
    Assert.expect(reconstructionRegionId >= 0);
    Assert.expect(focusGroup != null);
    Assert.expect(focusGroup.getId() >= 0);

    _reconstructionRegionId = reconstructionRegionId;
    _focusGroup = focusGroup;
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return 1;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    int                   focusGroup            = _focusGroup.getId();
    FocusSearchParameters focusSearchParameters = _focusGroup.getFocusSearchParameters();
    FocusRegion           focusRegion           = focusSearchParameters.getFocusRegion();
    PanelRectangle        focusRegionRectangle  = focusRegion.getRectangleRelativeToPanelInNanometers();

    // Focus Group Id: int32
    buffer.putInt(focusGroup);

    // Reconstruction Region Id: int32
    buffer.putInt(_reconstructionRegionId);

    // Focus region x origin in panel coordinates nanometers: int32
    buffer.putInt(focusRegionRectangle.getMinX());

    // Focus region y origin in panel coordinates nanometers: int32
    buffer.putInt(focusRegionRectangle.getMinY());

    // Focus region width in nanometers: int32
    int focusRegionWidth = focusRegionRectangle.getWidth();
    Assert.expect(focusRegionWidth > 0);
    buffer.putInt(focusRegionWidth);

    // Focus region height in nanometers: int32
    int focusRegionHeight = focusRegionRectangle.getHeight();
    Assert.expect(focusRegionHeight > 0);
    buffer.putInt(focusRegionHeight);

    // Focus Profile Shape Enumerated Type: int8
//    if (UnitTest.unitTesting())
//      buffer.put(FocusProfileShapeTypeEnum.map(FocusProfileShapeEnum.NO_SEARCH));// focusSearchParameters.getFocusProfileShape()));
//    else
    buffer.put(FocusProfileShapeTypeEnum.map(focusSearchParameters.getFocusProfileShape()));

    //Learned Z Curve Width in nanometers: int32
    buffer.putInt(focusSearchParameters.getZCurveWidthInNanoMeters());

    // Gradient weighting magnitude in percent: int32
    buffer.putInt(focusSearchParameters.getGradientWeightingMagnitudePercentage());

    // Gradient weighting orientation in degrees: int32
    buffer.putInt(focusSearchParameters.getGradientWeightingOrientationInDegrees());

    // Horizontal sharpness component percentage: int32
    buffer.putInt(focusSearchParameters.getHorizontalSharpnessComponentPercentage());
    
    // Defined initial wavelet level: int32
    buffer.putInt(focusSearchParameters.getInitialWaveletLevel());

    return buffer;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }
}
