package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class SetCalibratedMagnification extends ImageReconstructionMessage
{
  private int    _PAYLOAD_SIZE = JavaTypeSizeEnum.DOUBLE.getSizeInBytes() +
                                JavaTypeSizeEnum.INT.getSizeInBytes() + 
                                JavaTypeSizeEnum.DOUBLE.getSizeInBytes() +
                                JavaTypeSizeEnum.INT.getSizeInBytes();

  private double _mag;
  private double _highMag;

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public SetCalibratedMagnification(CommandSender writer, double mag, double highMag) throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.SetCalibratedMagnification, writer);
    Assert.expect(mag >= 0);
    Assert.expect(highMag >= 0);    

    _mag = mag;
    _highMag = highMag;
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return 1;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    // Step direction magnification at reference plane: double
    buffer.putDouble(_mag);

    // Scan direction sampling rate: int32
    int samplingRate = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
    buffer.putInt(samplingRate);
    
     // Step direction magnification at reference plane: double
    buffer.putDouble(_highMag);
    
    // Scan direction sampling rate: int32
    int samplingRateInHighMag = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
    buffer.putInt(samplingRateInHighMag);

    return buffer;
  }
}
