package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;


/**
 * @author Dave Ferguson
 */
public class ImageReconstructionSliceTypeEnum extends com.axi.util.Enum
{
  static private int _index = -1;

  public static final ImageReconstructionSliceTypeEnum RECONSTRUCTION = new ImageReconstructionSliceTypeEnum(++_index);
  public static final ImageReconstructionSliceTypeEnum SURFACE        = new ImageReconstructionSliceTypeEnum(++_index);
  public static final ImageReconstructionSliceTypeEnum PROJECTION     = new ImageReconstructionSliceTypeEnum(++_index);

  /**
   * @author Roy Williams
   */
  private ImageReconstructionSliceTypeEnum(int ndx)
  {
    super(ndx);
  }

  /**
   * @author Roy Williams
   */
  static public byte map(SliceTypeEnum sliceType)
  {
    if (sliceType.equals(SliceTypeEnum.RECONSTRUCTION))
      return (byte) RECONSTRUCTION.getId();
    if (sliceType.equals(SliceTypeEnum.SURFACE))
      return (byte) SURFACE.getId();
    if (sliceType.equals(SliceTypeEnum.PROJECTION))
      return (byte) PROJECTION.getId();

    Assert.expect(false, "Invalid SliceTypeEnum");
    return -1;
  }
}
