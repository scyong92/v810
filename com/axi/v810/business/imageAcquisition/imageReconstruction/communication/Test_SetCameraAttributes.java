package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the SetCameraAttributes message.
 *
 * @author Dave Ferguson
 */
public class Test_SetCameraAttributes extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
      int numberOfCameras = xRayCameraArray.getNumberOfCameras();

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      SetCameraAttributes setCameraAttributesMsg = new SetCameraAttributes(commandWriter);

      ByteBuffer msg = setCameraAttributesMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + (24 * numberOfCameras)));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.SetCameraAttributes.getId());
      Expect.expect(messageSize == 24);
      Expect.expect(messageCount == numberOfCameras);
      List<AbstractXrayCamera> xRayCameras = XrayCameraArray.getCameras();

      for (AbstractXrayCamera xRayCamera : xRayCameras)
      {
        MachineRectangle machineRectangle = xRayCameraArray.getCameraSensorRectangle(xRayCamera);
        byte[] ipAddressOctets = null;
        int portNumber = 0;
        CommunicationType communicationType = xRayCamera.getCommunicationType();

        if (communicationType.getCommunicationType().equals(CommunicationTypeEnum.TCPIP))
        {
          TcpipProperties tcpipProperties = communicationType.getTcpipProperties();
          ipAddressOctets = tcpipProperties.getIpAddressOctets();
          portNumber = tcpipProperties.getPortNumber();
        }

        Expect.expect(msg.getInt() == xRayCamera.getId());
        Expect.expect(msg.getInt() == numberOfCameras);
        Expect.expect(msg.getInt() == machineRectangle.getMinX());
        Expect.expect(msg.getInt() == machineRectangle.getCenterY());

        for (int i = 0; i < ipAddressOctets.length; ++i)
          Expect.expect(msg.get() == ipAddressOctets[i]);

        Expect.expect(msg.getInt() == portNumber);
      }
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_SetCameraAttributes() );
  }
}
