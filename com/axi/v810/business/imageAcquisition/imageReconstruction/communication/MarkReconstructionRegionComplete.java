package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;


/**
 * @author Roy Williams
 */
public class MarkReconstructionRegionComplete extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes()  +
                             JavaTypeSizeEnum.INT.getSizeInBytes()  +
                             JavaTypeSizeEnum.BYTE.getSizeInBytes();

  private int _testSubProgramId = -1;
  private int _regionID = -1;
  private ClassificationStatusEnum _state = null;

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public MarkReconstructionRegionComplete(CommandSender writer,
                                          int testSubProgramId,
                                          int regionID,
                                          ClassificationStatusEnum state)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.MarkReconstructionRegionComplete, writer);
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(regionID >= 0);
    Assert.expect(state != null);

    _testSubProgramId = testSubProgramId;
    _regionID = regionID;
    _state    = state;
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return 1;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    // camera width in pixels
    buffer.putInt(_testSubProgramId);
    buffer.putInt(_regionID);
    buffer.put((byte) _state.getId());
    return buffer;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }
}

