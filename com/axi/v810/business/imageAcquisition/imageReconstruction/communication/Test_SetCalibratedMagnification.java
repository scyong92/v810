package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the SetCalibratedMagnification message.
 *
 * @author Dave Ferguson
 */
public class Test_SetCalibratedMagnification extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    Config config = Config.getInstance();

    // Save original values for restore.
    double originalReferencePlaneMagnfication =
     config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION);

    try
    {

      double expectedReferencePlaneMagnfication = 6.24;
      int samplingRate = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
      int expectedScanDirectionSamplingRateInNanometersPerPixel = samplingRate;

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      SetCalibratedMagnification setCalibratedMagnificationMsg =
        new SetCalibratedMagnification(commandWriter, expectedReferencePlaneMagnfication, expectedReferencePlaneMagnfication);

      ByteBuffer msg = setCalibratedMagnificationMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 12));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.SetCalibratedMagnification.getId());
      Expect.expect(messageSize == 12);
      Expect.expect(messageCount == 1);
      Expect.expect(msg.getDouble() == expectedReferencePlaneMagnfication);
      Expect.expect(msg.getInt() == expectedScanDirectionSamplingRateInNanometersPerPixel);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_SetCalibratedMagnification() );
  }
}
