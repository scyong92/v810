package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.awt.geom.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the AddTestSubProgramTableEntry message.
 *
 * @author Dave Ferguson
 */
public class Test_AddTestSubProgramTableEntry extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      Project project = new Project(false);
      project.setName("Test_AddTestSubProgramTableEntry");
      // force state machine to be enabled since we are not doing a project load or import
      // but we still want the state machine to run
      project.getProjectState().disable();
      project.getProjectState().enable();

      Panel panel = new Panel();
      panel.disableChecksForNdfParsers();
      PanelSettings panelSettings = new PanelSettings();
      panelSettings.setPanel(panel);
      panel.setPanelSettings(panelSettings);
      panel.setWidthInNanoMeters(100);
      panel.setLengthInNanoMeters(100);
      panel.setThicknessInNanoMeters(1000);
      panelSettings.setDegreesRotationRelativeToCad(0);
      project.setPanel(panel);
      project.getTestProgram();
      TestProgram testProgram = new TestProgram();
      testProgram.setProject(project);
      TestSubProgram testSubProgram = new TestSubProgram(testProgram);
      testSubProgram.setId(42);

      List<ScanPass> scanPasses = new ArrayList<ScanPass>();
      ScanPass scanPass1 = new ScanPass(0,
                                        new StagePositionMutable(),
                                        new StagePositionMutable());
      ScanPass scanPass2 = new ScanPass(0,
                                        new StagePositionMutable(),
                                        new StagePositionMutable());
      scanPasses.add(scanPass1);
      scanPasses.add(scanPass2);

      List<AbstractXrayCamera> cameraList = new ArrayList<AbstractXrayCamera>();
      cameraList.add(XrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0));

      // Create a list of processorStrips to indicate the number of IRPs to use.
      List<ProcessorStrip> processorStrips = new ArrayList<ProcessorStrip>();
      ProcessorStrip processorStrip = new ProcessorStrip(0);
      processorStrip.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
      processorStrips.add(processorStrip);

      MechanicalConversions mechanicalConversions = null;
      try
      {
        SystemFiducialRectangle systemFiducialRectangle = new SystemFiducialRectangle(0, 0, 1088, 200);
        mechanicalConversions = new MechanicalConversions(processorStrips, // numberOfIRPs = lie because these images are coming back through XrayCameras
                                                          new AffineTransform(),
                                                          0, // alignmentUncertainty => none because we are always scanning system fiducical
                                                          0, // largest undivisible pad.   None for System Fiducial.
                                                          0, // FocalRangeUpslice on calibration measurements is 0!
                                                          0, // FocalRangeDownslice on calibration measurements is 0!
                                                          cameraList,
                                                          systemFiducialRectangle);
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
      ScanPath scanPath = new ScanPath(mechanicalConversions, scanPasses);
      testSubProgram.addScanPathOverInspectionOrVerificationArea(scanPath);
      
      testSubProgram.setMagnificationType(MagnificationTypeEnum.LOW);

      // Message to IRP order (note receiver's indexing).
      double scaleX     = 1.23; // m00;
      double shearX     = 2.34; // m01;
      double translateX = 3.45; // m02;
      double shearY     = 4.56; // m10;
      double scaleY     = 5.67; // m11;
      double translateY = 6.78; // m12;

      // Constructor order (m00, m10, m01, m11, m02, m12)
      panelSettings.setRightManualAlignmentTransform(
        new java.awt.geom.AffineTransform(scaleX, shearY, shearX, scaleY, translateX, translateY));

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      AddTestSubProgramTableEntry addTestSubProgramTableEntryMsg =
        new AddTestSubProgramTableEntry(commandWriter, testSubProgram, ImageAcquisitionModeEnum.PRODUCTION);

      ByteBuffer msg = addTestSubProgramTableEntryMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 60));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.AddTestSubProgramTableEntry.getId());
      Expect.expect(messageSize == 60);
      Expect.expect(messageCount == 1);

      // Get the expected values for the aggreagate manual alignment transform.
      AffineTransform aggregateManualAlignmentTransform = testSubProgram.getAggregateManualAlignmentTransform();
      double expectedScaleX = aggregateManualAlignmentTransform.getScaleX();
      double expectedShearX = aggregateManualAlignmentTransform.getShearX();
      double expectedTranslateX = aggregateManualAlignmentTransform.getTranslateX();
      double expectedShearY = aggregateManualAlignmentTransform.getShearY();
      double expectedScaleY = aggregateManualAlignmentTransform.getScaleY();
      double expectedTranslateY = aggregateManualAlignmentTransform.getTranslateY();

      Expect.expect(msg.getInt() == 42);
      Expect.expect(msg.getInt() == scanPasses.size());
      Expect.expect(msg.getInt() == scanPasses.size() * XrayCameraArray.getNumberOfCameras());
      Expect.expect(msg.getDouble() == expectedScaleX);
      Expect.expect(msg.getDouble() == expectedShearX);
      Expect.expect(msg.getDouble() == expectedTranslateX);
      Expect.expect(msg.getDouble() == expectedShearY);
      Expect.expect(msg.getDouble() == expectedScaleY);
      Expect.expect(msg.getDouble() == expectedTranslateY);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_AddTestSubProgramTableEntry() );
  }
}
