package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class InitializeSystem extends AbstractStateMessage
{
  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public InitializeSystem(CommandSender writer) throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.InitializeSystem, writer);
  }
}
