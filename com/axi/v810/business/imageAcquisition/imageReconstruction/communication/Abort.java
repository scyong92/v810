package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class Abort extends SynchronizedAbstractStateMessage
{
  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public Abort(CommandSender writer, ImageStateReceiver synchNotifier) throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.Abort, writer, synchNotifier);
  }
}
