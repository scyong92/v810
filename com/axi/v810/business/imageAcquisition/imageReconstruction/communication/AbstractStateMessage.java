package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * <p>@author Roy Williams</p>
 */
abstract public class AbstractStateMessage extends ImageReconstructionMessage
{
  /**
   * @author Roy Williams
   */
  public AbstractStateMessage(ImageReconstructionMessageEnum messageID, CommandSender writer) throws XrayTesterException
  {
    super(messageID, writer);
  }

  /**
   * @return the number of records to be sent to the IRP.  Default is 1.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return 1;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    return 0;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return false;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(false);
    return null;
  }
}
