package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the ConnectToCameras message.
 *
 * @author Dave Ferguson
 */
public class Test_ConnectToCameras extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      InetAddress address = InetAddress.getLocalHost();
      ImageStateReceiver imageStateReceiver = new ImageStateReceiver(ire, address, 9119);     
      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      ConnectToCameras connectToCamerasMsg = new ConnectToCameras(commandWriter, imageStateReceiver, 49152);

      ByteBuffer msg = connectToCamerasMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == 8);
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      Expect.expect(messageId == ConnectToCamerasEnum.CONNECTION_COMPLETE.getId());
      Expect.expect(connectToCamerasMsg.hasPayLoad() == true);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_ConnectToCameras() );
  }
}
