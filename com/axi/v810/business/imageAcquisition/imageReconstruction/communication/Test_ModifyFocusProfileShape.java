package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.business.testProgram.FocusProfileShapeEnum;


/**
 * Test class for the ModifyFocusProfileShape message.
 * @author Roy Williams
 */
public class Test_ModifyFocusProfileShape extends UnitTest
{
  /**
   * @author Roy Williams
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    testIrpMessage(42, 2, 5,    FocusProfileShapeTypeEnum.map(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE));
    testIrpMessage(420, 10, 2, FocusProfileShapeTypeEnum.map(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE));
  }

  /**
   * @author Roy Williams
   */
  private void testIrpMessage(int testSubProgramId, int reconstructionRegionId, int focusGroupId, byte profileShapeId)
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      ModifyFocusProfileShape modifyModifyFocusProfileShapeMsg =
        new ModifyFocusProfileShape(commandWriter, 1, reconstructionRegionId, focusGroupId, profileShapeId, -1, false, 0, 0, 0);

      ByteBuffer msg = modifyModifyFocusProfileShapeMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 13));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.ModifyFocusProfileShape.getId());
      Expect.expect(messageSize == 13);
      Expect.expect(messageCount == 1);
      int payloadTestSubProgramId = msg.getInt();
      int payloadReconstructionRegionId = msg.getInt();
      int payloadFocusGroupId = msg.getInt();
      byte payloadProfileShapeId = msg.get();
      Expect.expect(payloadTestSubProgramId == 1);
      Expect.expect(payloadReconstructionRegionId == reconstructionRegionId);
      Expect.expect(payloadFocusGroupId == focusGroupId);
      Expect.expect(payloadProfileShapeId == profileShapeId);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Roy Williams
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_ModifyFocusProfileShape() );
  }
}
