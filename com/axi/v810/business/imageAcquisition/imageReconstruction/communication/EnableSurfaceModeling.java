package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class EnableSurfaceModeling extends SynchronizedAbstractStateMessage
{
  public EnableSurfaceModeling(CommandSender writer, ImageStateReceiver synchNotifier)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.EnableSurfaceModeling, writer, synchNotifier);
  }
}
