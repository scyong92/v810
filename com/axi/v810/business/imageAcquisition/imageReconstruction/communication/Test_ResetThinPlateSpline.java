package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the ResetThinPlateSpline message.
 *
 * @author Roy Williams
 */
public class Test_ResetThinPlateSpline extends UnitTest
{
  /**
   * @author Cheah Lee Herng
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      InetAddress address = InetAddress.getLocalHost();
      ImageStateReceiver imageStateReceiver = new ImageStateReceiver(ire, address, 9119);
      CommandSender commandWriter = new CommandSender(ire, address, 9119);
      ResetThinPlateSpline resetThinPlateSpline = new ResetThinPlateSpline(commandWriter, imageStateReceiver);
      ByteBuffer msg = resetThinPlateSpline.getMessageBuffer();
      Expect.expect(msg.capacity() == 8);
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.ResetThinPlateSpline.getId());
      Expect.expect(messageSize == 0);
      Expect.expect(messageCount == 1);
      Expect.expect(resetThinPlateSpline.hasPayLoad() == false);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_InitializeTest() );
  }
}
