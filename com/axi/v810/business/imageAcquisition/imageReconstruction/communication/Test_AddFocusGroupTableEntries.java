package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the AddFocusGroupTableEntries message.
 *
 * @author Dave Ferguson
 */
public class Test_AddFocusGroupTableEntries extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      ReconstructionRegion reconstructionRegion = new ReconstructionRegion();
      FocusGroup focusGroup1 = new FocusGroup();
      FocusGroup focusGroup2 = new FocusGroup();
      Slice slice1 = new Slice();
      slice1.setSliceName(SliceNameEnum.PAD);
      slice1.setCreateSlice(true);
      Slice slice2 = new Slice();
      slice2.setSliceName(SliceNameEnum.PAD);
      slice2.setCreateSlice(true);

      focusGroup1.setRelativeZoffsetFromSurfaceInNanoMeters(123456);
      focusGroup1.addSlice(slice1);
      focusGroup1.setSingleSidedRegionOfBoard(true);
      focusGroup2.setRelativeZoffsetFromSurfaceInNanoMeters(25400);
      focusGroup2.addSlice(slice2);
      focusGroup2.setSingleSidedRegionOfBoard(false);

      reconstructionRegion.addFocusGroup(focusGroup1);
      reconstructionRegion.addFocusGroup(focusGroup2);

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      AddFocusGroupTableEntries addFocusGroupTableEntriesMsg =
        new AddFocusGroupTableEntries(commandWriter, reconstructionRegion);

      ByteBuffer msg = addFocusGroupTableEntriesMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 26));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.AddFocusGroupTableEntry.getId());
      Expect.expect(messageSize == 13);
      Expect.expect(messageCount == 2);
      byte expectedFocusGroup1SingleSided = focusGroup1.isSingleSidedRegionOfBoard() ? (byte) 0x01 : (byte) 0x00;
      byte expectedFocusGroup2SingleSided = focusGroup2.isSingleSidedRegionOfBoard() ? (byte) 0x01 : (byte) 0x00;
      Expect.expect(msg.getInt() == focusGroup1.getId());
      Expect.expect(msg.getInt() == reconstructionRegion.getRegionId());
      Expect.expect(msg.getInt() == focusGroup1.getRelativeZoffsetFromSurfaceInNanoMeters());
      Expect.expect(msg.get() == expectedFocusGroup1SingleSided);
      Expect.expect(msg.getInt() == focusGroup2.getId());
      Expect.expect(msg.getInt() == reconstructionRegion.getRegionId());
      Expect.expect(msg.getInt() == focusGroup2.getRelativeZoffsetFromSurfaceInNanoMeters());
      Expect.expect(msg.get() == expectedFocusGroup2SingleSided);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_AddFocusGroupTableEntries() );
  }
}
