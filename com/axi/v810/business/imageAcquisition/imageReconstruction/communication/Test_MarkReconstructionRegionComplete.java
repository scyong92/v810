package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the MarkReconstructionRegionComplete message.
 *
 * @author Dave Ferguson
 */
public class Test_MarkReconstructionRegionComplete extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    testIrpMessage(0, 42, ClassificationStatusEnum.FAIL, (byte) 0x00);
    testIrpMessage(1, 12345, ClassificationStatusEnum.PASS, (byte) 0x01);
  }

  /**
   * @author Dave Ferguson
   */
  private void testIrpMessage(int testSubProgramId,
                              int reconstructionRegionId,
                              ClassificationStatusEnum classificationStatusEnum,
                              byte expectedClassificationStatusEnumId)
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      MarkReconstructionRegionComplete markReconstructionRegionCompleteMsg =
        new MarkReconstructionRegionComplete(commandWriter, testSubProgramId, reconstructionRegionId, classificationStatusEnum);

      ByteBuffer msg = markReconstructionRegionCompleteMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 9));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.MarkReconstructionRegionComplete.getId());
      Expect.expect(messageSize == 9);
      Expect.expect(messageCount == 1);
      int payloadTestSubProgramId = msg.getInt();
      int payloadReconstructionRegionId = msg.getInt();
      byte payloadClassificationStatusEnum = msg.get();
      Expect.expect(payloadTestSubProgramId == testSubProgramId);
      Expect.expect(payloadReconstructionRegionId == reconstructionRegionId);
      Expect.expect(payloadClassificationStatusEnum == expectedClassificationStatusEnumId);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }


  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_MarkReconstructionRegionComplete() );
  }
}
