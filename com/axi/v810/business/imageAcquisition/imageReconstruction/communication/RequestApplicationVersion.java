package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Rex Shang
 */
public class RequestApplicationVersion extends SynchronizedInformationMessage
{
  private ImageReconstructionProcessorApplicationVersion _versionInfo;

  /**
   * Create a message with my designated message ID.
   *
   * @author Rex Shang
   */
  public RequestApplicationVersion(CommandSender writer, ImageReconstructionInformationReceiver infoReceiver)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.GetVersion, writer, infoReceiver);
  }

  /**
   * Used by base class to pass us the content of the new message.
   * @author Rex Shang
   */
  public void setMessage(Object object)
  {
    Assert.expect(object instanceof ImageReconstructionProcessorApplicationVersion);
    _versionInfo = (ImageReconstructionProcessorApplicationVersion) object;
  }

  /**
   * @author Rex Shang
   */
  public ImageReconstructionProcessorApplicationVersion getVersionInfo()
  {
    Assert.expect(_versionInfo != null);
    return _versionInfo;
  }
}
