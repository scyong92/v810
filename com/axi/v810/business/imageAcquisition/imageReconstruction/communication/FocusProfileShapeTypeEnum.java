package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;


/**
 * @author Roy Williams
 */
public class FocusProfileShapeTypeEnum extends com.axi.util.Enum
{
  static private int _index = -1;

  public static final FocusProfileShapeTypeEnum GAUSSIAN  = new FocusProfileShapeTypeEnum(++_index);
  public static final FocusProfileShapeTypeEnum SQUARE    = new FocusProfileShapeTypeEnum(++_index);
  public static final FocusProfileShapeTypeEnum PEAK = new FocusProfileShapeTypeEnum(++_index);
  public static final FocusProfileShapeTypeEnum EDGE = new FocusProfileShapeTypeEnum(++_index);
  public static final FocusProfileShapeTypeEnum NO_SEARCH_REFERENCE_PLANE = new FocusProfileShapeTypeEnum(++_index);
  public static final FocusProfileShapeTypeEnum NO_SEARCH_PREDICTED_SURFACE = new FocusProfileShapeTypeEnum(++_index);
  public static final FocusProfileShapeTypeEnum GAUSSIAN_EDGE = new FocusProfileShapeTypeEnum(++_index);

  /**
   * @author Roy Williams
   */
  private FocusProfileShapeTypeEnum(int ndx)
  {
    super(ndx);
  }

  /**
   * @author Roy Williams
   * @author Rick Gaudette
   */
  static public byte map(FocusProfileShapeEnum focusProfileShape)
  {
    if (focusProfileShape.equals(FocusProfileShapeEnum.GAUSSIAN))
      return (byte) GAUSSIAN.getId();
    if (focusProfileShape.equals(FocusProfileShapeEnum.SQUARE_WAVE))
      return (byte) SQUARE.getId();
    if (focusProfileShape.equals(FocusProfileShapeEnum.PEAK))
      return (byte) PEAK.getId();
    if (focusProfileShape.equals(FocusProfileShapeEnum.EDGE))
      return (byte) EDGE.getId();
    if (focusProfileShape.equals(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE))
      return (byte) NO_SEARCH_REFERENCE_PLANE.getId();
    if (focusProfileShape.equals(FocusProfileShapeEnum.NO_SEARCH_PREDICTED_SURFACE))
      return (byte) NO_SEARCH_PREDICTED_SURFACE.getId();
    if (focusProfileShape.equals(FocusProfileShapeEnum.GAUSSIAN_EDGE))
      return (byte) GAUSSIAN_EDGE.getId();

    Assert.expect(false, "Invalid FocusProfileShapeEnum");
    return -1;
  }
}
