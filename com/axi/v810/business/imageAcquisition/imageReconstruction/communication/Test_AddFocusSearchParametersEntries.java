package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the AddFocusSearchParametersEntries message.
 *
 * @author Dave Ferguson
 */
public class Test_AddFocusSearchParametersEntries extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    testIrpMessage(42,FocusProfileShapeEnum.GAUSSIAN,(byte)0,1,2,3,4,5,6,7,8);
    testIrpMessage(43,FocusProfileShapeEnum.SQUARE_WAVE,(byte)1,1000,2000,3000,4000,5000,6000,70,80);
    testIrpMessage(44,FocusProfileShapeEnum.PEAK,(byte)2,11,21,31,41,51,61,71,81);
    testIrpMessage(44,FocusProfileShapeEnum.EDGE,(byte)3,110,210,310,410,510,610,72,82);
    testIrpMessage(45,FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE,(byte)4,10,20,30,40,254001,60,73,83);
  }

  /**
   * @author Dave Ferguson
   */
  private void testIrpMessage(int reconstructionRegionId,
                              FocusProfileShapeEnum focusProfileShapeEnum,
                              byte expectedFocusProfileShapeEnumId,
                              int focusRegionX,
                              int focusRegionY,
                              int focusRegionWidth,
                              int focusRegionHeight,
                              int zCurveWidth,
                              int gradientWeightingMagnitudePercentage,
                              int gradientWeightingOrientationInDegrees,
                              int horizontalSharpnessComponentPercentage)
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      FocusGroup focusGroup = new FocusGroup();
      FocusSearchParameters fsp = new FocusSearchParameters();
      fsp.setFocusProfileShape(focusProfileShapeEnum);
      FocusRegion focusRegion = new FocusRegion();
      PanelRectangle panelRectangle = new PanelRectangle(focusRegionX,
                                                         focusRegionY,
                                                         focusRegionWidth,
                                                         focusRegionHeight);

      focusRegion.setRectangleRelativeToPanelInNanoMeters(panelRectangle);
      fsp.setFocusRegion(focusRegion);
      fsp.setZCurveWidthInNanoMeters(zCurveWidth);
      fsp.setGradientWeightingMagnitudePercentage(gradientWeightingMagnitudePercentage);
      fsp.setGradientWeightingOrientationInDegrees(gradientWeightingOrientationInDegrees);
      fsp.setHorizontalSharpnessComponentPercentage(horizontalSharpnessComponentPercentage);
      focusGroup.setFocusSearchParameters(fsp);

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      AddFocusSearchParametersEntries addFocusSearchParametersEntriesMsg =
        new AddFocusSearchParametersEntries(commandWriter, reconstructionRegionId, focusGroup);

      ByteBuffer msg = addFocusSearchParametersEntriesMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 41));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.AddFocusSearchParameterTableEntry.getId());
      Expect.expect(messageSize == 41);
      Expect.expect(messageCount == 1);
      int payloadFocusGroupId = msg.getInt();
      int payloadReconstructionRegionId = msg.getInt();
      int payloadRegionX = msg.getInt();
      int payloadRegionY = msg.getInt();
      int payloadRegionWidth = msg.getInt();
      int payloadRegionHeight = msg.getInt();
      byte payloadFocusProfileShape = msg.get();
      int payloadZCurveWidth = msg.getInt();
      int payloadGradientWeightingMagnitude = msg.getInt();
      int payloadGradientWeightingOrientation = msg.getInt();
      int payloadHorizontalSharpnessComponent = msg.getInt();
      Expect.expect(payloadFocusGroupId == focusGroup.getId());
      Expect.expect(payloadReconstructionRegionId == reconstructionRegionId);
      Expect.expect(payloadRegionX == focusRegionX);
      Expect.expect(payloadRegionY == focusRegionY);
      Expect.expect(payloadRegionWidth == focusRegionWidth);
      Expect.expect(payloadRegionHeight == focusRegionHeight);
      Expect.expect(payloadFocusProfileShape == expectedFocusProfileShapeEnumId);
      Expect.expect(payloadZCurveWidth == zCurveWidth);
      Expect.expect(payloadGradientWeightingMagnitude == gradientWeightingMagnitudePercentage);
      Expect.expect(payloadGradientWeightingOrientation == gradientWeightingOrientationInDegrees);
      Expect.expect(payloadHorizontalSharpnessComponent == horizontalSharpnessComponentPercentage);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }


  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_AddFocusSearchParametersEntries() );
  }
}
