package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

/**
 * @author Roy Williams
 */
public class BoardSideEnum extends com.axi.util.Enum
{
  static private int _index = -1;

  public static final BoardSideEnum TOP    = new BoardSideEnum(++_index);
  public static final BoardSideEnum BOTTOM = new BoardSideEnum(++_index);

  /**
   *
   * @author Roy Williams
   */
  private BoardSideEnum(int ndx)
  {
    super(ndx);
  }
}
