package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * XCR-3325, Intermittent IRP-Camera Socket Connection Crash in Magnification Adjustment
 * @author sheng-chuan.yong
 */
public class GetCameraConnectionStatus extends AbstractStateMessage
{
  private ImageStateReceiver _synchNotifier;
  private boolean _isNotified = false;
  private ConnectToCamerasEnum _cameraConnectionStatus = null;
  
  private static final int _bitMaskFailedOthers = 0x80000000;
  
  private final Object _syncBarrier = new Object();
  
  private ConnectToCamerasComplete _completeNotifier;
  private ConnectToCamerasFailedOthers _failedOthersNotifier;
  
  private final TimerUtil _timer = new TimerUtil();
  
  /**
   * @author Yong Sheng Chuan
   */
  private static class ConnectToCamerasComplete implements AcknowledgeImageAcquisitionProcessorSyncInt
  {
    GetCameraConnectionStatus _owner;
    public ConnectToCamerasComplete(GetCameraConnectionStatus owner)
    {
      _owner = owner;
    }
    
    public void acknowledgeMessage() 
    {
      _owner.connectComplete();
    }

    public int getMessageId() 
    {
      return ImageReconstructionMessageEnum.ConnectToCameras.getId();
    }
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  private static class ConnectToCamerasFailedOthers implements AcknowledgeImageAcquisitionProcessorSyncInt
  {
    GetCameraConnectionStatus _owner;
    public ConnectToCamerasFailedOthers(GetCameraConnectionStatus owner)
    {
      _owner = owner;
    }
    
    public void acknowledgeMessage() 
    {
      _owner.connectFailedOthers();
    }

    public int getMessageId() {
      return ImageReconstructionMessageEnum.ConnectToCameras.getId() | _bitMaskFailedOthers;
    }
  }
  
  /**
   * Create a message with my designated message ID.
   *
   * @author Yong Sheng Chuan
   */
  public GetCameraConnectionStatus(CommandSender writer, ImageStateReceiver synchNotifier) throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.GetCameraConnectionStatus, writer);
    
    Assert.expect(synchNotifier != null);
    _synchNotifier = synchNotifier;
    
    
    _completeNotifier = new ConnectToCamerasComplete(this);
    _failedOthersNotifier = new ConnectToCamerasFailedOthers(this);
    
    _synchNotifier.notifyMessage(_completeNotifier);
    _synchNotifier.notifyMessage(_failedOthersNotifier);
  }

  /**
   * @author Yong Sheng Chuan
   */
  protected int getNumberOfRecords() 
  {
    return 1;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public ConnectToCamerasEnum sendAndSync() throws XrayTesterException
  {  
    _isNotified = false;
    
    super.send();
    _timer.reset();
    
    synchronized (_syncBarrier) 
    {
      _timer.start();
      for (int i = 0; i < 400; ++i) 
      {
        try 
        {
          _syncBarrier.wait(100);
          
          if (_isNotified)
            return _cameraConnectionStatus;
        } 
        catch (InterruptedException ex) 
        {
          // do nothing
        }
      }
      _timer.stop();
    }
    _cameraConnectionStatus = ConnectToCamerasEnum.CONNECTION_FAILED_ASSIGNING_PORT;
    return _cameraConnectionStatus;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void connectComplete()
  {
    _synchNotifier.denotifyMessage(_failedOthersNotifier);

    synchronized (_syncBarrier) 
    {
      _cameraConnectionStatus = ConnectToCamerasEnum.CONNECTION_COMPLETE;
      _isNotified = true;
      _syncBarrier.notify();
    }
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void connectFailedOthers()
  {
    _synchNotifier.denotifyMessage(_completeNotifier);
   
    synchronized (_syncBarrier) 
    {
      _cameraConnectionStatus = ConnectToCamerasEnum.CONNECTION_FAILED_OTHERS;
      _isNotified = true;
      _syncBarrier.notify();
    }
  }
}
