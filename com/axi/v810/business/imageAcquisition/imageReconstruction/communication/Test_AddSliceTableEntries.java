package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Test class for the AddSliceTableEnties message.
 *
 * @author Dave Ferguson
 */
public class Test_AddSliceTableEntries extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      int reconstructionRegionId = 42;
      FocusGroup focusGroup = new FocusGroup();
      Slice slice1 = new Slice();
      slice1.setCreateSlice(true);
      slice1.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
      slice1.setSliceType(SliceTypeEnum.RECONSTRUCTION);
      slice1.setSliceName(SliceNameEnum.THROUGHHOLE_BARREL);

      Slice slice2 = new Slice();
      slice2.setCreateSlice(true);
      slice2.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
      slice2.setSliceType(SliceTypeEnum.SURFACE);
      slice2.setSliceName(SliceNameEnum.THROUGHHOLE_PIN_SIDE);

      Slice slice3 = new Slice();
      slice3.setCreateSlice(false); // Don't serialize this one
      slice3.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
      slice3.setSliceType(SliceTypeEnum.RECONSTRUCTION);
      slice3.setSliceName(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE);

      Slice slice4 = new Slice();
      slice4.setCreateSlice(true);
      slice4.setReconstructionMethod(ReconstructionMethodEnum.LIGHTEST);
      slice4.setSliceType(SliceTypeEnum.RECONSTRUCTION);
      slice4.setSliceName(SliceNameEnum.THROUGHHOLE_PROTRUSION);

      focusGroup.addSlice(slice1);
      focusGroup.addSlice(slice2);
      focusGroup.addSlice(slice3);
      focusGroup.addSlice(slice4);

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      AddSliceTableEntries addSliceTableEntriesMsg =
        new AddSliceTableEntries(commandWriter, reconstructionRegionId, focusGroup);

      ByteBuffer msg = addSliceTableEntriesMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + (14 * 3)));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.AddSliceTableEntry.getId());
      Expect.expect(messageSize == 14);

      // Only 3 slices are sent down.  One has useProduction = false;
      Expect.expect(messageCount == 3);
      Expect.expect(msg.getInt() == SliceNameEnum.THROUGHHOLE_BARREL.getId());
      Expect.expect(msg.getInt() == focusGroup.getId());
      Expect.expect(msg.getInt() == 42);
      Expect.expect(msg.get() == (byte) 0x00);
      Expect.expect(msg.get() == (byte) 0x00);

      Expect.expect(msg.getInt() == SliceNameEnum.THROUGHHOLE_PIN_SIDE.getId());
      Expect.expect(msg.getInt() == focusGroup.getId());
      Expect.expect(msg.getInt() == 42);
      Expect.expect(msg.get() == (byte) 0x00);
      Expect.expect(msg.get() == (byte) 0x01);

      Expect.expect(msg.getInt() == SliceNameEnum.THROUGHHOLE_PROTRUSION.getId());
      Expect.expect(msg.getInt() == focusGroup.getId());
      Expect.expect(msg.getInt() == 42);
      Expect.expect(msg.get() == (byte) 0x01);
      Expect.expect(msg.get() == (byte) 0x00);

    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_AddSliceTableEntries() );
  }
}
