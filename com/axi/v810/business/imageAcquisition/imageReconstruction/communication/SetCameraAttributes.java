package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.net.*;
import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;



/**
 * @author Roy Williams
 */
public class SetCameraAttributes extends ImageReconstructionMessage
{
  private int    _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes()  +
                                JavaTypeSizeEnum.INT.getSizeInBytes()  +
                                JavaTypeSizeEnum.INT.getSizeInBytes()  +
                                JavaTypeSizeEnum.INT.getSizeInBytes()  +
                                JavaTypeSizeEnum.INT.getSizeInBytes()  +
                                JavaTypeSizeEnum.INT.getSizeInBytes()  +          
                                JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                                JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                                JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                                JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                                JavaTypeSizeEnum.INT.getSizeInBytes();

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public SetCameraAttributes(CommandSender writer) throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.SetCameraAttributes, writer);
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return XrayCameraArray.getNumberOfCameras();
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * @author George A. David
   */
  public boolean hasPayLoad()
  {
    return true;
  }

  /**
   * @author Greg Esparza
   */
  private void setCommunicationInformation(AbstractXrayCamera xRayCamera, ByteBuffer buffer) throws XrayTesterException
  {
    Assert.expect(xRayCamera != null);
    Assert.expect(buffer != null);

    try
    {
      CommunicationType communicationType = xRayCamera.getCommunicationType();
      if (communicationType.getCommunicationType().equals(CommunicationTypeEnum.TCPIP))
      {
        TcpipProperties tcpipProperties = communicationType.getTcpipProperties();

        byte[] ipAddressOctets = tcpipProperties.getIpAddressOctets();
        for (int i = 0; i < ipAddressOctets.length; i++)
        {
          buffer.put(ipAddressOctets[i]);
        }

        buffer.putInt(tcpipProperties.getPortNumber());
      }
    }
    catch (UnknownHostException uhe)
    {
      List<String> exceptionParameters = new ArrayList<String>();
      exceptionParameters.add(uhe.getMessage());
      HardwareException he = new HardwareException(new LocalizedString("BUS_IMAGE_RECONSTRUCTION_FAILED_SETTING_COMMUNICATION_INFORMATION_KEY", exceptionParameters.toArray()));
      he.initCause(uhe);
      throw he;
    }
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer) throws XrayTesterException
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    int numberOfCameras = xRayCameraArray.getNumberOfCameras();

    for (AbstractXrayCamera xRayCamera : XrayCameraArray.getCameras())
    {
      MachineRectangle machineRectangle = xRayCameraArray.getCameraSensorRectangle(xRayCamera);
      MachineRectangle machineRectangleForHighMag = xRayCameraArray.getCameraSensorRectangleForHighMag(xRayCamera);

      //  Camera Id: int32
      buffer.putInt(xRayCamera.getId());

      //  Number of Cameras: int32
      buffer.putInt(numberOfCameras);

      //  X location (left-side midpoint) of camera in machine coordinates nanometers: int32
      buffer.putInt(machineRectangle.getMinX());

      //  Y location (left-side midpoint) of camera in machine coordinates nanometers: int32
      buffer.putInt(machineRectangle.getCenterY());
      
      //  X location (left-side midpoint) of camera in machine coordinates nanometers: int32
      buffer.putInt(machineRectangleForHighMag.getMinX());

      //  Y location (left-side midpoint) of camera in machine coordinates nanometers: int32
      buffer.putInt(machineRectangleForHighMag.getCenterY());
      
      setCommunicationInformation(xRayCamera, buffer);
    }

    return buffer;
  }
}
