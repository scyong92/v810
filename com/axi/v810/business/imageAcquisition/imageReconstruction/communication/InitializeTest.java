package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class InitializeTest extends SynchronizedAbstractStateMessage
{
  /**
   * @author Roy Williams
   */
  public InitializeTest(CommandSender writer, ImageStateReceiver synchNotifier)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.InitializeTest, writer, synchNotifier);
  }
}
