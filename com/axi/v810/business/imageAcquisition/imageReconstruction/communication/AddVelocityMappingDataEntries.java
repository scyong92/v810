package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * @author Kok Chun, Tan
 */
public class AddVelocityMappingDataEntries extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes() + 
                              JavaTypeSizeEnum.INT.getSizeInBytes() + 
                              JavaTypeSizeEnum.DOUBLE.getSizeInBytes() +
                              JavaTypeSizeEnum.DOUBLE.getSizeInBytes() + 
                              JavaTypeSizeEnum.DOUBLE.getSizeInBytes();
  private VelocityMappingParameters _velocityMappingParameters = null;

  /**
   * @author Kok Chun, Tan
   */
  public AddVelocityMappingDataEntries(CommandSender writer, VelocityMappingParameters data) throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.AddVelocityMappingDataEntry, writer);
    Assert.expect(data != null);
    _velocityMappingParameters = data;
  }

  /**
   * @author Kok Chun, Tan
   */
  protected int getNumberOfRecords()
  {
    return 1;
  }

  /**
   * @author Kok Chun, Tan
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * @author Kok Chun, Tan
   */
  protected boolean hasPayLoad()
  {
    return true;
  }

  /**
   * @author Kok Chun, Tan
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    
    // first velocity
	  buffer.putInt(_velocityMappingParameters.getFirstVelocity());
    
    // second velocity
	  buffer.putInt(_velocityMappingParameters.getSecondVelocity());
    
    // slope
    buffer.putDouble(_velocityMappingParameters.getSlope());
    
    // offset
    buffer.putDouble(_velocityMappingParameters.getOffset());
    
    // R2
    buffer.putDouble(_velocityMappingParameters.getR2());

    return buffer;
  }
}
