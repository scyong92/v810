package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the RunReconstruction message.
 *
 * @author Dave Ferguson
 */
public class Test_RunReconstruction extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    testIrpMessage(2);
    testIrpMessage(1);
    testIrpMessage(123);
  }

  /**
   * @author Dave Ferguson
   */
  private void testIrpMessage(int testSubProgramId)
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      RunReconstruction runReconstructionMsg =
        new RunReconstruction(commandWriter, testSubProgramId);

      ByteBuffer msg = runReconstructionMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 4));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.Run.getId());
      Expect.expect(messageSize == 4);
      Expect.expect(messageCount == 1);
      int payloadTestSubProgramId = msg.getInt();
      Expect.expect(payloadTestSubProgramId == testSubProgramId);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }


  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_RunReconstruction() );
  }
}
