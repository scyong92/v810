package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the AddReconstructionRegionTableEntries message.
 *
 * @author Dave Ferguson
 */
public class Test_AddReconstructionRegionTableEntries extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ReconstructionRegion reconstructionRegion1 = new ReconstructionRegion();
    reconstructionRegion1.setRegionId(42);
    reconstructionRegion1.setRegionRectangleRelativeToPanelInNanoMeters(new PanelRectangle(1,2,3,4));
    reconstructionRegion1.setTopSide(false);
    reconstructionRegion1.setFocusPriority(FocusPriorityEnum.FOURTH);

    ReconstructionRegion reconstructionRegion2 = new ReconstructionRegion();
    reconstructionRegion2.setRegionId(52);
    reconstructionRegion2.setRegionRectangleRelativeToPanelInNanoMeters(new PanelRectangle(10000,20000,30000,40000));
    reconstructionRegion2.setTopSide(true);
    reconstructionRegion2.setFocusPriority(FocusPriorityEnum.THIRD);

    ReconstructionRegion reconstructionRegion3 = new ReconstructionRegion();
    reconstructionRegion3.setRegionId(62);
    reconstructionRegion3.setRegionRectangleRelativeToPanelInNanoMeters(new PanelRectangle(9,8,7,6));
    reconstructionRegion3.setTopSide(true);
    reconstructionRegion3.setFocusPriority(FocusPriorityEnum.SECOND);

    ReconstructionRegion reconstructionRegion4 = new ReconstructionRegion();
    reconstructionRegion4.setRegionId(1042);
    reconstructionRegion4.setRegionRectangleRelativeToPanelInNanoMeters(new PanelRectangle(10,100,1000,10000));
    reconstructionRegion4.setTopSide(false);
    reconstructionRegion4.setFocusPriority(FocusPriorityEnum.FIRST);

    Collection<ReconstructionRegion> reconstructionRegions = new ArrayList<ReconstructionRegion>();

    reconstructionRegions.add(reconstructionRegion1);
    reconstructionRegions.add(reconstructionRegion2);
    reconstructionRegions.add(reconstructionRegion3);
    reconstructionRegions.add(reconstructionRegion4);

    testIrpMessage(12345, ReconstructionRegionTypeEnum.ALIGNMENT, reconstructionRegions);
    testIrpMessage(8, ReconstructionRegionTypeEnum.INSPECTION, reconstructionRegions);
    testIrpMessage(2, ReconstructionRegionTypeEnum.VERIFICATION, reconstructionRegions);
  }

  /**
   * @author Dave Ferguson
   */
  private void testIrpMessage(int testSubProgramId,
                              ReconstructionRegionTypeEnum reconstructionRegionTypeEnum,
                              Collection<ReconstructionRegion> reconstructionRegions)
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    int expectedRegionTypeEnum = -1;

    if (reconstructionRegionTypeEnum.equals(ReconstructionRegionTypeEnum.VERIFICATION))
    {
      expectedRegionTypeEnum = 0;
    }
    else if (reconstructionRegionTypeEnum.equals(ReconstructionRegionTypeEnum.ALIGNMENT))
    {
      expectedRegionTypeEnum = 1;
    }
    else if (reconstructionRegionTypeEnum.equals(ReconstructionRegionTypeEnum.INSPECTION))
    {
      expectedRegionTypeEnum = 2;
    }
    else
    {
      Assert.expect(false, "Invalid ReconstructionRegionTypeEnum");
    }

    try
    {
      TestProgram testProgram = new TestProgram();
      TestSubProgram testSubProgram = new TestSubProgram(testProgram);
      testSubProgram.setId(testSubProgramId);

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      AddReconstructionRegionTableEntries addReconstructionRegionTableEntriesMsg =
        new AddReconstructionRegionTableEntries(commandWriter,
                                                testSubProgram.getId(),
                                                testSubProgram.getSubProgramRefferenceIdForAlignment(),
                                                reconstructionRegionTypeEnum,
                                                reconstructionRegions);

      ByteBuffer msg = addReconstructionRegionTableEntriesMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + (30 * reconstructionRegions.size())));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.AddReconstructionRegionTableEntry.getId());
      Expect.expect(messageSize == 30);
      Expect.expect(messageCount == reconstructionRegions.size());

      for (ReconstructionRegion reconstructionRegion : reconstructionRegions)
      {
        int payloadReconstructionRegionId = msg.getInt();
        int payloadTestSubProgramId = msg.getInt();
        byte payloadRegionTypeEnum = msg.get();
        int payloadRegionX = msg.getInt();
        int payloadRegionY = msg.getInt();
        int payloadRegionWidth = msg.getInt();
        int payloadRegionHeight = msg.getInt();
        byte payloadBoardSide = msg.get();
        int payloadFocusPriority = msg.getInt();
        byte expectedBoardSide = reconstructionRegion.isTopSide() ? (byte) 0x00 : (byte) 0x01;
        Expect.expect(payloadReconstructionRegionId == reconstructionRegion.getRegionId());
        Expect.expect(payloadTestSubProgramId == testSubProgramId);
        Expect.expect(payloadRegionTypeEnum == expectedRegionTypeEnum);
        Expect.expect(payloadRegionX == reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinX());
        Expect.expect(payloadRegionY == reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinY());
        Expect.expect(payloadRegionWidth == reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters().getWidth());
        Expect.expect(payloadRegionHeight == reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters().getHeight());
        Expect.expect(payloadBoardSide == expectedBoardSide);
        Expect.expect(payloadFocusPriority == reconstructionRegion.getFocusPriority().getId());
      }
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }


  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_AddReconstructionRegionTableEntries() );
  }
}
