package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class WaitForPriorIrpCommandsToComplete extends SynchronizedAbstractStateMessage
{
  public WaitForPriorIrpCommandsToComplete(CommandSender writer, ImageStateReceiver synchNotifier)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.WaitForPriorCommandsToComplete, writer, synchNotifier);
  }
}
