package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.net.*;
import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;



/**
 * @author Roy Williams
 */
public class SetImageReconstructionProcessorNetworkAttributes extends ImageReconstructionMessage
{
  private int    _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes()  +
                                JavaTypeSizeEnum.INT.getSizeInBytes()  +
                                JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                                JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                                JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                                JavaTypeSizeEnum.BYTE.getSizeInBytes();
  private List<InetAddress> _imageReconstructionEngineIpAddresses;

  /**
   * Create a message with my designated message ID.
   *
   * @author Dave Ferguson
   */
  public SetImageReconstructionProcessorNetworkAttributes(CommandSender writer,
                                                          List<InetAddress> imageReconstructionEngineIpAddresses)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.SetIRPNetworkAttributes, writer);
    Assert.expect(imageReconstructionEngineIpAddresses != null);
    _imageReconstructionEngineIpAddresses = imageReconstructionEngineIpAddresses;
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Dave Ferguson
   */
  protected int getNumberOfRecords()
  {
    Assert.expect(_imageReconstructionEngineIpAddresses != null);
    return _imageReconstructionEngineIpAddresses.size();
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Dave Ferguson
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    int numberOfIpAddresses = _imageReconstructionEngineIpAddresses.size();

    for (int ip = 0; ip < numberOfIpAddresses; ++ip)
    {
      InetAddress inetAddr = _imageReconstructionEngineIpAddresses.get(ip);

      byte[] ipAddr = inetAddr.getAddress();

      //  Id: int32
      buffer.putInt(ip);

      //  Number of IP Addresses: int32
      buffer.putInt(numberOfIpAddresses);

      // IP Address: byte[4]
      for (int i = 0; i < ipAddr.length; ++i)
      {
        buffer.put(ipAddr[i]);
      }
    }
    return buffer;
  }

  /**
   * @author George A. David
   */
  public boolean hasPayLoad()
  {
    return true;
  }
}
