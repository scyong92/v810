package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the SetRuntimeAlignmentMatrixValues message.
 *
 * @author Dave Ferguson
 */
public class Test_SetRuntimeAlignmentMatrixValues extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      //scaleX = m00;
      //scaleY = m11;
      //shearX = m01;
      //shearY = m10;
      //translateX = m02;
      //translateY = m12;
      java.awt.geom.AffineTransform affineTransform =
        new java.awt.geom.AffineTransform(30.45, 20.34, 10.23, 40.56, 50.67, 60.78);

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      SetRuntimeAlignmentMatrixValues setRuntimeAlignmentMatrixValuesMsg =
        new SetRuntimeAlignmentMatrixValues(commandWriter, 5, affineTransform);

      ByteBuffer msg = setRuntimeAlignmentMatrixValuesMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 52));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.SetRuntimeAlignmentMatrix.getId());
      Expect.expect(messageSize == 52);
      Expect.expect(messageCount == 1);
      Expect.expect(msg.getInt() == 5);
      Expect.expect(msg.getDouble() == 30.45);
      Expect.expect(msg.getDouble() == 10.23);
      Expect.expect(msg.getDouble() == 50.67);
      Expect.expect(msg.getDouble() == 20.34);
      Expect.expect(msg.getDouble() == 40.56);
      Expect.expect(msg.getDouble() == 60.78);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_SetRuntimeAlignmentMatrixValues() );
  }
}
