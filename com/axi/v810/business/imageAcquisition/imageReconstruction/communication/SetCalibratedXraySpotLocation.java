package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.XrayTester;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class SetCalibratedXraySpotLocation extends ImageReconstructionMessage
{
  private int    _PAYLOAD_SIZE = 4 * JavaTypeSizeEnum.INT.getSizeInBytes();

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public SetCalibratedXraySpotLocation(CommandSender writer) throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.SetCalibratedXRaySpotLocation, writer);
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return 1;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    int xSpot = _config.getIntValue(HardwareCalibEnum.XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
    int ySpot = _config.getIntValue(HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
    
    int xSpotForHigMag = _config.getIntValue(HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
    int ySpotForHighMag = _config.getIntValue(HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);    
    
    buffer.putInt(xSpot);
    buffer.putInt(ySpot);
    buffer.putInt(xSpotForHigMag);
    buffer.putInt(ySpotForHighMag);
    return buffer;
  }
}
