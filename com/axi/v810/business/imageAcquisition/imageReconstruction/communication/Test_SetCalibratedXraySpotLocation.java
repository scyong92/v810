package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the SetCalibratedXraySpotLocation message.
 *
 * @author Dave Ferguson
 */
public class Test_SetCalibratedXraySpotLocation extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      Config config = Config.getInstance();

      // Save original values for restore.
      int originalXLocation =
        config.getIntValue(HardwareCalibEnum.XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);

      int originalYLocation =
        config.getIntValue(HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);

      int expectedXLocation = 254000;
      int expectedYLocation = 612345;

      config.setValue(HardwareCalibEnum.XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS, expectedXLocation);
      config.setValue(HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS, expectedYLocation);

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      SetCalibratedXraySpotLocation setCalibratedXraySpotLocationMsg =
        new SetCalibratedXraySpotLocation(commandWriter);

      ByteBuffer msg = setCalibratedXraySpotLocationMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 8));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.SetCalibratedXRaySpotLocation.getId());
      Expect.expect(messageSize == 8);
      Expect.expect(messageCount == 1);
      Expect.expect(msg.getInt() == expectedXLocation);
      Expect.expect(msg.getInt() == expectedYLocation);

      // Restore Config
      config.setValue(HardwareCalibEnum.XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS, originalXLocation);
      config.setValue(HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS, originalYLocation);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_SetCalibratedXraySpotLocation() );
  }
}
