package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.awt.geom.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.business.imageAcquisition.*;

/**
 * Test class for the SetProjectionAttributes message.
 *
 * @author Dave Ferguson
 */
public class Test_SetProjectionAttributes extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      TestProgram testProgram = new TestProgram();
      testProgram.setFilteredTestSubProgramInScanPath(ImageAcquisitionModeEnum.NOT_IN_USE);
      TestSubProgram testSubProgram = new TestSubProgram(testProgram);
      testProgram.addTestSubProgram(testSubProgram);

      StagePositionMutable startPosition = new StagePositionMutable();
      startPosition.setXInNanometers(100);
      startPosition.setYInNanometers(100);

      StagePositionMutable endPosition   = new StagePositionMutable();
      endPosition.setXInNanometers(100);
      endPosition.setYInNanometers(200);

      ScanPass scanPass = new ScanPass(0, startPosition, endPosition);
      List<ScanPass> scanPasses = new ArrayList<ScanPass>();
      scanPasses.add(scanPass);

      ProjectionRegion projRegion1 = new ProjectionRegion(new ImageRectangle(0, 100, 1088, 142), ImageReconstructionEngineEnum.IRE2, InetAddress.getLocalHost());
      ProjectionRegion projRegion2 = new ProjectionRegion(new ImageRectangle(0, 242, 1088, 50),  ImageReconstructionEngineEnum.IRE2, InetAddress.getLocalHost());
      ProjectionRegion projRegion3 = new ProjectionRegion(new ImageRectangle(0, 292, 1088, 200), ImageReconstructionEngineEnum.IRE3, InetAddress.getLocalHost());

      // Create a systemFiducialRectancle with ofsets 1000, 1000.
      SystemFiducialRectangle systemFiducialRectangle = new SystemFiducialRectangle(1000 + 0,
                                                                                    1000 + 100,
                                                                                    1088,
                                                                                    142 + 50 + 200);

      List<ProjectionRegion> projRegions = new ArrayList<ProjectionRegion>();
      projRegions.add(projRegion1);
      projRegions.add(projRegion2);
      projRegions.add(projRegion3);

      ProjectionSettings projSettings = new ProjectionSettings(scanPass, 0, 1000, projRegions);

      AbstractXrayCamera xRayCamera = XrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_6);
      XrayCameraAcquisitionParameters camAcqParms1 = new XrayCameraAcquisitionParameters(xRayCamera);
      camAcqParms1.addProjectionSettings(projSettings);

      List<XrayCameraAcquisitionParameters> cameraParametersList = new ArrayList<XrayCameraAcquisitionParameters>();
      cameraParametersList.add(camAcqParms1);
      testProgram.setCameraAcquisitionParameters(cameraParametersList);

      List<AbstractXrayCamera> cameraList = new ArrayList<AbstractXrayCamera>();
      cameraList.add(xRayCamera);

      // Create a list of processorStrips to indicate the number of IRPs to use.
      List<ProcessorStrip> processorStrips = new ArrayList<ProcessorStrip>();
      ProcessorStrip processorStrip = new ProcessorStrip(0);
      processorStrip.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
      processorStrips.add(processorStrip);

      MechanicalConversions mechanicalConversions = new MechanicalConversions(
        processorStrips, // numberOfIRPs = lie because these images are coming back through XrayCameras
        new AffineTransform(),
        0,     // alignmentUncertainty => none because we are always scanning system fiducical
        0,     // largest undivisible pad.   None for System Fiducial.
        0,     // FocalRangeUpslice on calibration measurements is 0!
        0,     // FocalRangeDownslice on calibration measurements is 0!
        cameraList,
        systemFiducialRectangle);
      ScanPath scanPath = new ScanPath(mechanicalConversions, scanPasses);
      testSubProgram.addScanPathOverInspectionOrVerificationArea(scanPath);

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      SetProjectionAttributes setProjectionAttributesMsg = new SetProjectionAttributes(commandWriter, ImageReconstructionEngineEnum.IRE2, testProgram);
      ByteBuffer msg = setProjectionAttributesMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + 29));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.SetProjectionAttributes.getId());
      Expect.expect(messageSize == 29);
      Expect.expect(messageCount == 1);

      // first payload
      Expect.expect(msg.getInt() == xRayCamera.getId()); // camera id
      Expect.expect(msg.getInt() == scanPass.getId()); // scan pass
      Expect.expect(msg.getInt() == 1);              // testSubProgramId
      Expect.expect(msg.getInt() == 100);            // x stage pos
      int yPos = msg.getInt();
      Expect.expect(yPos == 543579112, Integer.toString(yPos)); // y position at row 0 in machine coordinates.
      Expect.expect(msg.getInt() == 100);            // capture line of projection relative to row 0 of overall projection
      Expect.expect(msg.getInt() == 192);           // projection lines
      Expect.expect(msg.get()    == 1);              //
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_SetProjectionAttributes() );
  }
}
