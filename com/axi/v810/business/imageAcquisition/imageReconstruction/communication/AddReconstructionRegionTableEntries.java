package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;


/**
 * @author Roy Williams
 */
public class AddReconstructionRegionTableEntries extends ImageReconstructionMessage
{
  // see payload method below for fields ordering as they are added to ByteBuffer.
  private int  _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes()  +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes()  +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes()  +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes()  +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes()  +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes()  +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes()  +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes();
  private int _testSubProgramId = -1;
  private int _testSubProgramsGroupId = -1;
  private byte _reconstructionRegionType = -1;
  private int _numberOfRecords = -1;
  private Collection<ReconstructionRegion> _reconstructionRegions;  
  
  // 225 * 40 <= 9000 bytes
  private int _splitIndex = 225;
  private Map<Integer, List<ReconstructionRegion>> _splitRecordInfoMap;

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public AddReconstructionRegionTableEntries(CommandSender writer,
                                             int testSubProgramId,
                                             int testSubProgramsGroupId,
                                             ReconstructionRegionTypeEnum reconstructionRegionType,
                                             Collection<ReconstructionRegion> reconstructionRegions)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.AddReconstructionRegionTableEntry, writer);
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(reconstructionRegionType != null);
    Assert.expect(reconstructionRegions != null);

    _numberOfRecords = reconstructionRegions.size();
    _testSubProgramId = testSubProgramId;
    _testSubProgramsGroupId = testSubProgramsGroupId;
    _reconstructionRegions = reconstructionRegions;
    _reconstructionRegionType = (byte) reconstructionRegionType.getId();
        
    _splitRecordInfoMap = new TreeMap<Integer,List<ReconstructionRegion>>();    
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return _numberOfRecords;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    
    int extraSizeFor64bitIrp = 0;
    // Remove waitForKickStart flag if it is 32bit irp.
    if (Config.is64bitIrp()==false)
      extraSizeFor64bitIrp = JavaTypeSizeEnum.BYTE.getSizeInBytes();
    
    return _PAYLOAD_SIZE - extraSizeFor64bitIrp;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= (getSizeOfEachRecord() * _reconstructionRegions.size()));

    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.RECONSTRUCTION_REGIONS_DEBUG))
      System.out.println("ReconstructionRegions: (id x y w h)");
    for (ReconstructionRegion region : _reconstructionRegions)
    {
      if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.RECONSTRUCTION_REGIONS_DEBUG) && region.isTopSide())  
      {
        System.out.println(region.getRegionId() + " " +
                           region.getRegionRectangleRelativeToPanelInNanoMeters());
//        SystemFiducialRectangle nominalAreaToImage =
//            MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(
//                region.getRegionRectangleRelativeToPanelInNanoMeters(),
//                _testSubProgram.getManualAlignmentTransform());
//        int x = 1;
      }

      // Reconstruction Region Id: int32
      buffer.putInt(region.getRegionId());

      // Test Sub-program Id: int32
      buffer.putInt(_testSubProgramId);

      // Test Sub-program Id: int32
      buffer.putInt(_testSubProgramsGroupId);

      // Reconstruction Region Enumeration Type: byte
      buffer.put(_reconstructionRegionType);

      // Obtain the panelRectangle that will be used to obtain subsequent values.
      PanelRectangle panelRectangle = region.getRegionRectangleRelativeToPanelInNanoMeters();
      Assert.expect(panelRectangle != null);

      // Region x origin in panel coordinate nanometers: int32
      buffer.putInt(panelRectangle.getMinX());

      // Region y origin in panel coordinate nanometers: int32
      buffer.putInt(panelRectangle.getMinY());

      // Region width in nanometers
      buffer.putInt(panelRectangle.getWidth());

      // Region length in nanometers
      buffer.putInt(panelRectangle.getHeight());

      // Board Side Enumerated Type
      buffer.put(region.isTopSide() ? (byte) 0x00 : (byte) 0x01);

      // Focus Priority
      buffer.putInt(region.getFocusPriority().getId());

      // Global Surface Model: byte
      buffer.put((byte)region.getGlobalSurfaceModelEnum().getId());

      // UseToBuildGlobalSurfaceModel (boolean): byte
      buffer.put(region.useToBuildGlobalSurfaceModel() ? (byte) 0x01 : (byte) 0x00);
      
      // WaitForKickStart (boolean): byte
      // only add this key if irp is 64bit
      if(Config.is64bitIrp())
        buffer.put(region.waitForKickStart() ? (byte) 0x01 : (byte) 0x00);
      
      buffer.put(region.isRegionUsedForExposureLearning() ? (byte) 0x01 : (byte) 0x00);

      buffer.put((region.getCameraEnabledIDList().contains(com.axi.v810.hardware.XrayCameraIdEnum.XRAY_CAMERA_0.getId()))? (byte) 0x01 : (byte) 0x00); 
      buffer.put((region.getCameraEnabledIDList().contains(com.axi.v810.hardware.XrayCameraIdEnum.XRAY_CAMERA_1.getId()))? (byte) 0x01 : (byte) 0x00); 
      buffer.put((region.getCameraEnabledIDList().contains(com.axi.v810.hardware.XrayCameraIdEnum.XRAY_CAMERA_2.getId()))? (byte) 0x01 : (byte) 0x00);
      buffer.put((region.getCameraEnabledIDList().contains(com.axi.v810.hardware.XrayCameraIdEnum.XRAY_CAMERA_3.getId()))? (byte) 0x01 : (byte) 0x00); 
      buffer.put((region.getCameraEnabledIDList().contains(com.axi.v810.hardware.XrayCameraIdEnum.XRAY_CAMERA_4.getId()))? (byte) 0x01 : (byte) 0x00); 
      buffer.put((region.getCameraEnabledIDList().contains(com.axi.v810.hardware.XrayCameraIdEnum.XRAY_CAMERA_5.getId()))? (byte) 0x01 : (byte) 0x00); 
      buffer.put((region.getCameraEnabledIDList().contains(com.axi.v810.hardware.XrayCameraIdEnum.XRAY_CAMERA_6.getId()))? (byte) 0x01 : (byte) 0x00); 
      buffer.put((region.getCameraEnabledIDList().contains(com.axi.v810.hardware.XrayCameraIdEnum.XRAY_CAMERA_7.getId()))? (byte) 0x01 : (byte) 0x00); 
      buffer.put((region.getCameraEnabledIDList().contains(com.axi.v810.hardware.XrayCameraIdEnum.XRAY_CAMERA_8.getId()))? (byte) 0x01 : (byte) 0x00); 
      buffer.put((region.getCameraEnabledIDList().contains(com.axi.v810.hardware.XrayCameraIdEnum.XRAY_CAMERA_9.getId()))? (byte) 0x01 : (byte) 0x00); 
      buffer.put((region.getCameraEnabledIDList().contains(com.axi.v810.hardware.XrayCameraIdEnum.XRAY_CAMERA_10.getId()))? (byte) 0x01 : (byte) 0x00); 
      buffer.put((region.getCameraEnabledIDList().contains(com.axi.v810.hardware.XrayCameraIdEnum.XRAY_CAMERA_11.getId()))? (byte) 0x01 : (byte) 0x00); 
      buffer.put((region.getCameraEnabledIDList().contains(com.axi.v810.hardware.XrayCameraIdEnum.XRAY_CAMERA_12.getId()))? (byte) 0x01 : (byte) 0x00); 
      buffer.put((region.getCameraEnabledIDList().contains(com.axi.v810.hardware.XrayCameraIdEnum.XRAY_CAMERA_13.getId()))? (byte) 0x01 : (byte) 0x00);
    }
    return buffer;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }

  /**
   * @author Roy Williams
   */
  protected void emitAssociations(CommandSender writer) throws XrayTesterException
  {
    for (ReconstructionRegion reconstructionRegion : _reconstructionRegions)
    {
      if (reconstructionRegion.getFocusGroups().size() > 0)
        new AddFocusGroupTableEntries(writer, reconstructionRegion).send();

      if (reconstructionRegion.getScanPasses().size() > 0)
        new AddScanPassTableEntries(writer, reconstructionRegion).send();
    }
  }
  
  /**   
   * @throws XrayTesterException 
   */
  public void send() throws XrayTesterException
  {
    if (VirtualLiveManager.getInstance().isVirtualLiveMode() == false || ImageAcquisitionEngine.getInstance().isGenerateImageForExposureLearning())
    {
      ByteBuffer buffer = getMessageBuffer();
      buffer.rewind();
      _writer.write(buffer);

      // The message has been sent.
      // Expect a response back from the IRP on each message
      _writer.waitForReadyAcknowledgement(_imageReconstructionMessage);

      emitAssociations(_writer);
    }
    else
    {
      // Split all virtual live regions into several groups
      setupReconstructionRegionSplitInfo();

      for(Map.Entry<Integer,List<ReconstructionRegion>> entry : _splitRecordInfoMap.entrySet())
      {
        _reconstructionRegions = entry.getValue();

        ByteBuffer buffer = getSplitMessageBuffer(_reconstructionRegions.size());
        buffer.rewind();
        _writer.write(buffer);

        // The message has been sent.
        // Expect a response back from the IRP on each message
        _writer.waitForReadyAcknowledgement(_imageReconstructionMessage);

        emitAssociations(_writer);
      }

      // Clear map
      for(Map.Entry<Integer,List<ReconstructionRegion>> entry : _splitRecordInfoMap.entrySet())
      {
        entry.getValue().clear();
      }

      _splitRecordInfoMap.clear();
    }        
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setupReconstructionRegionSplitInfo()
  {
    int counter = 1;
    int groupId = 1;
    
    for(ReconstructionRegion region : _reconstructionRegions)
    {
      if ((counter % _splitIndex) == 0)
      {
        counter = 1;
        ++groupId;
      }
      
      if (_splitRecordInfoMap.containsKey(groupId) == false)
      {
        _splitRecordInfoMap.put(groupId, new ArrayList<ReconstructionRegion>());
        _splitRecordInfoMap.get(groupId).add(region);
      }
      else
      {
        _splitRecordInfoMap.get(groupId).add(region);
      }
      
      ++counter;
    }
  }
  
  /**   
   * @author Cheah Lee Herng 
   */
  public final ByteBuffer getSplitMessageBuffer(int totalSplitRecords) throws XrayTesterException
  {
    ByteBuffer buffer = getByteBuffer(getMessageHeaderSize() + (getSizeOfEachRecord() * totalSplitRecords));
    buffer = addSplitMessageHeaderBuffer(buffer, totalSplitRecords);

    if (hasPayLoad())
      buffer = addPayLoad(buffer);

    return buffer;
  }
  
  /**   
   * @author Cheah Lee Herng 
   */
  protected ByteBuffer addSplitMessageHeaderBuffer(ByteBuffer buffer, int totalSplitRecords)
  {
    Assert.expect(buffer.remaining() >= getMessageHeaderSize());
    int size  = getSizeOfEachRecord();
    int usage = _imageReconstructionMessage.getId();
    if (_config.getBooleanValue(SoftwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_COMMUNICATIONS_HANDSHAKE))
    {
      usage = usage | 0x1000;
    }
    buffer.putShort((short)usage);
    buffer.putShort((short)size);
    buffer.putInt(totalSplitRecords);
    return buffer;
  }
}
