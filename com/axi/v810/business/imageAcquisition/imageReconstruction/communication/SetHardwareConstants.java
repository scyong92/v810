package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * @author Roy Williams
 */
public class SetHardwareConstants extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() + 
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.LONG.getSizeInBytes() +
                              JavaTypeSizeEnum.LONG.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() + 
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes();

  // These two parameters vary based upon the acquisition in progress.
  static private int _focalRangeUpsliceInNanometers;
  static private int _focalRangeDownsliceInNanometers;
  static private int _uncertaintyReferencePlaneToNominalPlane = 381000;

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public SetHardwareConstants(CommandSender writer) throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.SetHardwareConstants, writer);
  }

  /**
   * @author Roy Williams
   */
  static public void setFocalRangeUpsliceInNanometers(int focalRangeUpsliceInNanometers)
  {
    Assert.expect(focalRangeUpsliceInNanometers   > 0);
    _focalRangeUpsliceInNanometers = focalRangeUpsliceInNanometers;
  }

  /**
   * @author Roy Williams
   */
  static public void setFocalRangeDownsliceInNanometers(int focalRangeDownsliceInNanometers)
  {
    Assert.expect(focalRangeDownsliceInNanometers > 0);
    _focalRangeDownsliceInNanometers = focalRangeDownsliceInNanometers;
  }
  
  /**
   * @author Wei Chin
   */
  static public int getFocalRangeUpsliceInNanometers()
  {
    return _focalRangeUpsliceInNanometers - _uncertaintyReferencePlaneToNominalPlane;
  }

  /**
   * @author Wei Chin
   */
  static public int getFocalRangeDownsliceInNanometers()
  {
    return - (_focalRangeDownsliceInNanometers - _uncertaintyReferencePlaneToNominalPlane);
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return 1;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    // Camera width in pixels
    // We can just get any camera so we can get the sensor attributes
    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
    // Swee Yee Wong - XCR-2630 Support New X-Ray Camera
	buffer.putInt(xRayCamera.getSensorWidthInPixelsWithOverHeadByte());

    // Number of unued pixels on end of camera
    buffer.putInt(xRayCamera.getNumberOfUnusedPixelsOnEndOfSensor());

    // Physical size of camera pixel in nanometers
    buffer.putInt(xRayCamera.getSensorColumnPitchInNanometers());

    // Distance from camera plane to x-ray source in nanometers
    buffer.putInt(XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers());

    // Distance from camera plane to2nd  x-ray source in nanometers
    buffer.putInt(XrayCameraArray.getZHeightFromCameraArrayTo2ndXraySourceInNanometers());
    
    // Maximum fiducial relative slice height in nanometers
    int uncertaintyNominalToReferencePlane = getUncertaintyReferencePlaneToNominalPlaneInNanoMeters();
    buffer.putInt((int)_focalRangeUpsliceInNanometers - uncertaintyNominalToReferencePlane);

    // Minimum fiducial relative slice height in nanometers
    buffer.putInt(- ((int)_focalRangeDownsliceInNanometers - uncertaintyNominalToReferencePlane));

    // Maximum reconstruction region geometry supported on the IRP's
    buffer.putInt(getMaxHardwareReconstructionRegionWidthInNanoMeters());
    buffer.putInt(getMaxHardwareReconstructionRegionLengthInNanoMeters());

    // Maximum focus region geometry supported on the IRP's
    buffer.putInt(getMaxFocusRegionWidthInNanoMeters());
    buffer.putInt(getMaxFocusRegionLengthInNanoMeters());

    // Amount of IRP memory used for projections.  Convert to bytes.
    long megaByteToByte = 1024 * 1024;
    buffer.putLong((long)_config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_ALLOCATED_PROJECTION_MEMORY_IN_MEGABYTES) * megaByteToByte);

    // Amount of IRP memory used for reconstructed images.  Convert to bytes.
    buffer.putLong((long)_config.getIntValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_ALLOCATED_RECONSTRUCTION_MEMORY_IN_MEGABYTES) * megaByteToByte);
    
    // maximum and minimum value for masking velocity mapping
    buffer.putInt(getMinimumMaskingThreshold());
    buffer.putInt(getMaximumMaskingThreshold());

    // ignore center camera flag
    buffer.putInt(isIgnoreCenterCamera());
    
    return buffer;
  }

  /**
     // Note that width and length are reversed.  ReconstructionRegions keep the one and
     // only value.  However, HW and cals have a machine HW orientation (i.e. not rotated
     // 90 degrees).
   * @author Roy Williams
   */
  static public int getMaxHardwareReconstructionRegionWidthInNanoMeters()
  {
    return ReconstructionRegion.getMaxAlignmentRegionLengthInNanoMeters();
  }

  /**
   * @author Roy Williams
   */
  static public int getMaxHardwareReconstructionRegionLengthInNanoMeters()
  {
    return ReconstructionRegion.getMaxAlignmentRegionWidthInNanoMeters();
  }

  /**
   * @author Roy Williams
   */
  public static int getMaxFocusRegionWidthInNanoMeters()
  {
    return FocusRegion.getMaxFocusRegionLengthInNanoMeters();
  }

  /**
   * @author Roy Williams
   */
  public static int getMaxFocusRegionLengthInNanoMeters()
  {
    return FocusRegion.getMaxFocusRegionWidthInNanoMeters();
  }

  /**
   * @author Roy Williams
   */
  public static int getUncertaintyReferencePlaneToNominalPlaneInNanoMeters()
  {
    return _uncertaintyReferencePlaneToNominalPlane;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public int getMinimumMaskingThreshold()
  {
    return _config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_CAL_POINT_PIXEL_MIN) + _config.getIntValue(SoftwareConfigEnum.MASKING_THRESHOLD);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public int getMaximumMaskingThreshold()
  {
    return _config.getIntValue(HardwareConfigEnum.AXI_TDI_CAMERA_CAL_POINT_PIXEL_MAX) - _config.getIntValue(SoftwareConfigEnum.MASKING_THRESHOLD);
  }

  /**
   * @author Kok Chun, Tan
   * return 1 if does not include camera 0, return 0 if include camera 0
   */
  private int isIgnoreCenterCamera()
  {
    if (Project.isCurrentProjectLoaded())
    {
      if (Project.getCurrentlyLoadedProject().getDynamicRangeOptimizationVersion().equals( DynamicRangeOptimizationVersionEnum.VERSION_0))
      {
        return 1;
      }
      else
      {
        return 0;
      }
    }
    else
    {
      return 1;
    }
  }
}
