package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Bob Balliew
 */
public class AddScanPassTableEntries extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes();

  private ReconstructionRegion _reconstructionRegion = null;
  private List<ScanPass> _scanPasses = null;

  /**
   * Create a message with my designated message ID.
   *
   * @author Bob Balliew
   */
  public AddScanPassTableEntries(CommandSender writer,
                                 ReconstructionRegion reconstructionRegion)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.AddScanPassTableEntry, writer);
    Assert.expect(reconstructionRegion != null);
    _reconstructionRegion = reconstructionRegion;
    _scanPasses = new ArrayList<ScanPass>();

    for (ScanPass scanPass : _reconstructionRegion.getScanPasses())
    {
      _scanPasses.add(scanPass);
    }
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Bob Balliew
   */
  protected int getNumberOfRecords()
  {
    return _scanPasses.size();
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Bob Balliew
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Bob Balliew
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());

    for (ScanPass scanPass : _scanPasses)
    {
      // Reconstruction Region Id: int32
      int reconstructionRegionId = _reconstructionRegion.getRegionId();
      Assert.expect(reconstructionRegionId >= 0);
      buffer.putInt(reconstructionRegionId);

      // Scan Pass Id: int32
      int scanPassId = scanPass.getId();
      Assert.expect(scanPassId >= 0);
      buffer.putInt(scanPassId);
    }

    return buffer;
  }

  /**
   * @author Bob Balliew
   */
  protected boolean hasPayLoad()
  {
    return true;
  }
}
