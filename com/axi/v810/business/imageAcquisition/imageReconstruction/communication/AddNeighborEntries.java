package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;


/**
 * @author Roy Williams
 */
public class AddNeighborEntries extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes();

  private int _reconstructionRegionId = -1;
  private FocusGroup _focusGroup;

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public AddNeighborEntries(CommandSender writer,
                            int reconstructionRegionId,
                            FocusGroup focusGroup)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.AddNeighborTableEntry, writer);
    Assert.expect(reconstructionRegionId >= 0);
    Assert.expect(focusGroup != null);
    Assert.expect(focusGroup.hasNeighbors());

    _reconstructionRegionId = reconstructionRegionId;
    _focusGroup = focusGroup;
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    Assert.expect(_focusGroup != null);
    return _focusGroup.getNeighbors().size();
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    int focusGroup = _focusGroup.getId();
    Assert.expect(focusGroup >= 0);
    for (ReconstructionRegion neighbor : _focusGroup.getNeighbors())
    {
      // Focus Group Id: int32
      buffer.putInt(focusGroup);

      // Reconstruction Region Id: int32
      buffer.putInt(_reconstructionRegionId);

      // Neighbor Reconstruction Region Id: int32
      int neighborReconstructionId = neighbor.getRegionId();
      Assert.expect(neighborReconstructionId >= 0);
      buffer.putInt(neighborReconstructionId);
    }
    return buffer;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }
}

