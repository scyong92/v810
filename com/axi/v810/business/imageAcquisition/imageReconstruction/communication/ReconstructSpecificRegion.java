package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;


/**
 * @author Roy Williams
 */
public class ReconstructSpecificRegion extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes() +
                             JavaTypeSizeEnum.INT.getSizeInBytes() +
                             JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                             JavaTypeSizeEnum.INT.getSizeInBytes();
  private int  _testSubProgramId = -1;
  private int  _regionID  = -1;
  private byte _imageType = -1;
  private int _reconstructionZOffsetInNanometers = 0;

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public ReconstructSpecificRegion(CommandSender writer,
                                   int testSubProgramId,
                                   int regionId,
                                   ImageTypeEnum imageType,
                                   int reconstructionZOffsetInNanometers)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.ReReconstructSpecificRegion, writer);
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(regionId >= 0);
    Assert.expect(imageType != null);
    // reconstructionZOffsetInNanometers can be either negative or positive - no assert

    _testSubProgramId = testSubProgramId;
    _regionID  = regionId;
    _imageType = (byte) imageType.getId();
    _reconstructionZOffsetInNanometers = reconstructionZOffsetInNanometers;
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return 1;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE >= 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    buffer.putInt(_testSubProgramId);
    buffer.putInt(_regionID);
    buffer.put(_imageType);
    buffer.putInt(_reconstructionZOffsetInNanometers);
    return buffer;
  }
}
