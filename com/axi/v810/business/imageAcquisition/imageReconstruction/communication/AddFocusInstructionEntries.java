package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;


/**
 * @author Roy Williams
 */
public class AddFocusInstructionEntries extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes();

  private int _reconstructionRegion = -1;
  private int _focusGroup = -1;
  private Slice _slice = null;
  private int _numberOfRecords = 1;

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public AddFocusInstructionEntries(CommandSender writer,
                                    int reconstructionRegionId,
                                    int focusGroup,
                                    Slice slice)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.AddFocusInstructionTableEntry, writer);
    Assert.expect(slice != null);
    Assert.expect(reconstructionRegionId >= 0);
    Assert.expect(focusGroup >= 0);

    _reconstructionRegion = reconstructionRegionId;
    _focusGroup = focusGroup;
    _slice = slice;
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    Assert.expect(_numberOfRecords >= 0);

    return _numberOfRecords;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    int sliceNameIndex = _slice.getSliceName().getId();
    FocusInstruction focusInstruction = _slice.getFocusInstruction();

    // Slice Id: int32
    buffer.putInt(sliceNameIndex);

    // Focus Group Id: int32
    buffer.putInt(_focusGroup);

    // Reconstruction Region Id: int32
    buffer.putInt(_reconstructionRegion);

    // Focus Method Enumerated Type: byte
    buffer.put((byte)ReconstructionTypeEnum.map(focusInstruction.getFocusMethod()));

    // z height in nanometers: int32
    buffer.putInt(focusInstruction.getZHeightInNanoMeters());

    // Percent between: int32
    buffer.putInt(focusInstruction.getPercentValue());

    // z offset in nanometers: int32
    buffer.putInt(focusInstruction.getZOffsetInNanoMeters());

    return buffer;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }
}


