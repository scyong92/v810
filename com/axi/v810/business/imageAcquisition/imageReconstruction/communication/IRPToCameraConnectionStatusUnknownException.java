package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 *
 * @author swee-yee.wong
 */
public class IRPToCameraConnectionStatusUnknownException extends BusinessException
{
  /**
   * @param localizedString 
   */
  public IRPToCameraConnectionStatusUnknownException(LocalizedString localizedString)
  {
    super(localizedString);
  }
}

