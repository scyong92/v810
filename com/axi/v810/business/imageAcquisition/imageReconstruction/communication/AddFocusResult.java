package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class AddFocusResult extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes() +
                             JavaTypeSizeEnum.INT.getSizeInBytes() +
                             JavaTypeSizeEnum.INT.getSizeInBytes() +
                             JavaTypeSizeEnum.INT.getSizeInBytes() +
                             JavaTypeSizeEnum.INT.getSizeInBytes();
  FocusResult _focusResult = null;


  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public AddFocusResult(CommandSender writer, FocusResult focusResult) throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.AddFocusResult, writer);
    Assert.expect(focusResult != null);
    _focusResult = focusResult;
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return 1;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    buffer.putInt(_focusResult.getReconstructionRegionId());
    buffer.putInt(_focusResult.getFocusGroupId());
    buffer.putInt(_focusResult.getZPeakInNanometers());
    buffer.putInt(_focusResult.getZUpperEdgeInNanometers());
    buffer.putInt(_focusResult.getZLowerEdgeInNanometers());
    return buffer;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }
}
