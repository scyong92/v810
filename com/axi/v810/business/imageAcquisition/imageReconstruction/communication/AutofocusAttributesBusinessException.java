package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 *
 * @author wei-chin.chong
 */
public class AutofocusAttributesBusinessException extends BusinessException
{
  /**
   * @param localizedString 
   */
  public AutofocusAttributesBusinessException(LocalizedString localizedString)
  {
    super(localizedString);
  }
}
