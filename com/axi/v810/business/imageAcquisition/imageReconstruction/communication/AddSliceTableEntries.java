package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Agilent Technogies, Inc.</p>
 * <p>@author Roy Williams</p>
 */
public class AddSliceTableEntries extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.INT.getSizeInBytes()  +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes();

  private int _reconstructionRegion = -1;
  private FocusGroup _focusGroup = null;
  private int _numberOfRecords = 0;

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public AddSliceTableEntries(CommandSender writer,
                              int reconstructionRegionId,
                              FocusGroup focusGroup)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.AddSliceTableEntry, writer);
    Assert.expect(reconstructionRegionId >= 0);
    Assert.expect(focusGroup != null);

    _reconstructionRegion = reconstructionRegionId;
    _focusGroup = focusGroup;
    for (Slice slice : _focusGroup.getSlices())
    {
      if (slice.createSlice())
        _numberOfRecords++;
    }
  }

  /**
   * Return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    Assert.expect(_numberOfRecords >= 0);
    return _numberOfRecords;
  }

  /**
   * Return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    int focusGroupId = _focusGroup.getId();
    Assert.expect(focusGroupId >= 0);

    for (Slice slice : _focusGroup.getSlices())
    {
      if (slice.createSlice())
      {
        // Slice Id: int32
        int sliceNameIndex = slice.getSliceName().getId();
        Assert.expect(sliceNameIndex >= 0);
        buffer.putInt(sliceNameIndex);

        // Focus Group Id: int32
        buffer.putInt(focusGroupId);

        // Reconstruction Region Id: int32
        buffer.putInt(_reconstructionRegion);

        // Image Type Enumeration: byte
        ReconstructionMethodEnum reconstructionMethod = slice.getReconstructionMethod();
        if (reconstructionMethod.equals(ReconstructionMethodEnum.LIGHTEST))
          buffer.put((byte)0x01);
        else if (reconstructionMethod.equals(ReconstructionMethodEnum.AVERAGE))
          buffer.put((byte)0x00);
        else if (reconstructionMethod.equals(ReconstructionMethodEnum.STRETCH))
          buffer.put((byte)0x02);
        else if (reconstructionMethod.equals(ReconstructionMethodEnum.BLEND))
          buffer.put((byte)0x03);
        else
          Assert.expect(false, "Invalid ReconstructionMethodEnum");

        // Slice Type Enumeration: int8
        buffer.put(ImageReconstructionSliceTypeEnum.map(slice.getSliceType()));

        // UseToBuildGlobalSurfaceModel: byte
        buffer.put(slice.useToBuildGlobalSurfaceModel() ? (byte) 0x01 : (byte) 0x00);
      }
    }
    return buffer;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }

  /**
   * @author Roy Williams
   */
  protected void emitAssociations(CommandSender writer) throws XrayTesterException
  {
    for (Slice slice : _focusGroup.getSlices())
    {
      if (slice.createSlice())
      {
        new AddFocusInstructionEntries(writer,
                                       _reconstructionRegion,
                                       _focusGroup.getId(),
                                       slice).send();
      }
    }
  }
}
