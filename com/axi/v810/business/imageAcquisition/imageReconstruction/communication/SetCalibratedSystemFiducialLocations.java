package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.panelSettings.MagnificationTypeEnum;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class SetCalibratedSystemFiducialLocations extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = 6 * JavaTypeSizeEnum.INT.getSizeInBytes(); // 6 int32's
  private int _numberOfCameras = -1;
  private int _numberOfMagnification = -1;

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public SetCalibratedSystemFiducialLocations(CommandSender writer) throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.SetCalibratedSystemFiducialLocation, writer);

    _numberOfCameras = XrayCameraArray.getInstance().getNumberOfCameras();
    _numberOfMagnification = MagnificationTypeEnum.getMagnificationToEnumMap().size();
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   * @author Wei Chin
   */
  protected int getNumberOfRecords()
  {
    Assert.expect(_numberOfCameras > 0);
    Assert.expect(_numberOfMagnification > 0);
    
    return _numberOfCameras * _numberOfMagnification;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   * @author Wei Chin
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    
    addPayloadForLowMag(buffer);
    addPayloadForHighMag(buffer);
    
    return buffer;
  } 
  
  /**
   * @param buffer 
   * @author Wei Chin
   */
  void addPayloadForLowMag(ByteBuffer buffer)
  {
    int magnificationId = MagnificationTypeEnum.LOW.getId();
    for (AbstractXrayCamera xRayCamera : XrayCameraArray.getCameras())
    {
      int cameraId = xRayCamera.getId();
      buffer.putInt(cameraId);
      buffer.putInt(magnificationId);
      switch (cameraId)
      {
        case 0:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_ROW));
          break;
        }
        case 1:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_ROW));
          break;
        }
        case 2:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_ROW));
          break;
        }
        case 3:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_ROW));
          break;
        }
        case 4:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_ROW));
          break;
        }
        case 5:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_ROW));
          break;
        }
        case 6:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_ROW));
          break;
        }
        case 7:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_ROW));
          break;
        }
        case 8:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_ROW));
          break;
        }
        case 9:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_ROW));
          break;
        }
        case 10:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_ROW));
          break;
        }
        case 11:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_ROW));
          break;
        }
        case 12:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_ROW));
          break;
        }
        case 13:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_ROW));
          break;
        }
        default:
          Assert.expect(false);
      }
    }
  }
  
/**
   * @param buffer 
   * @author Wei Chin
   */
  void addPayloadForHighMag(ByteBuffer buffer)
  {
    int magnificationId = MagnificationTypeEnum.HIGH.getId();
    for (AbstractXrayCamera xRayCamera : XrayCameraArray.getCameras())
    {
      int cameraId = xRayCamera.getId();
      buffer.putInt(cameraId);
      buffer.putInt(magnificationId);
      switch (cameraId)
      {
        case 0:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_ROW));
          break;
        }
        case 1:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_ROW));
          break;
        }
        case 2:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_ROW));
          break;
        }
        case 3:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_ROW));
          break;
        }
        case 4:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_ROW));
          break;
        }
        case 5:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_ROW));
          break;
        }
        case 6:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_ROW));
          break;
        }
        case 7:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_ROW));
          break;
        }
        case 8:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_ROW));
          break;
        }
        case 9:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_ROW));
          break;
        }
        case 10:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_ROW));
          break;
        }
        case 11:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_ROW));
          break;
        }
        case 12:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_ROW));
          break;
        }
        case 13:
        {
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_COLUMN));
          buffer.putInt(_config.getIntValue(HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_ROW));
          break;
        }
        default:
          Assert.expect(false);
      }
    }
  }
}
