package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

/**
 * @author Roy Williams
 */
public class ClassificationStatusEnum extends com.axi.util.Enum
{
  static private int _index = -1;

  public static final ClassificationStatusEnum FAIL = new ClassificationStatusEnum(++_index);
  public static final ClassificationStatusEnum PASS = new ClassificationStatusEnum(++_index);

  /**
   *
   * @author Roy Williams
   */
  private ClassificationStatusEnum(int ndx)
  {
    super(ndx);
  }
}
