package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the SetImageReconstructionProcessorNetworkAttributes message.
 *
 * @author Dave Ferguson
 */
public class Test_SetImageReconstructionProcessorNetworkAttributes extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      List<InetAddress> inetAddress = new ArrayList<InetAddress>();

      inetAddress.add(InetAddress.getByName("100.200.1.142"));
      inetAddress.add(InetAddress.getByName("192.121.42.2"));

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      SetImageReconstructionProcessorNetworkAttributes setImageReconstructionProcessorNetworkAttributesMsg =
        new SetImageReconstructionProcessorNetworkAttributes(commandWriter, inetAddress);

      ByteBuffer msg = setImageReconstructionProcessorNetworkAttributesMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + (12 * 2)));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.SetIRPNetworkAttributes.getId());
      Expect.expect(messageSize == 12);
      Expect.expect(messageCount == 2);
      Expect.expect(msg.getInt() == 0);
      Expect.expect(msg.getInt() == 2);
      Expect.expect(msg.get() == (byte) 100);
      Expect.expect(msg.get() == (byte) 200);
      Expect.expect(msg.get() == (byte) 1);
      Expect.expect(msg.get() == (byte) 142);
      Expect.expect(msg.getInt() == 1);
      Expect.expect(msg.getInt() == 2);
      Expect.expect(msg.get() == (byte) 192);
      Expect.expect(msg.get() == (byte) 121);
      Expect.expect(msg.get() == (byte) 42);
      Expect.expect(msg.get() == (byte) 2);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_SetImageReconstructionProcessorNetworkAttributes() );
  }
}
