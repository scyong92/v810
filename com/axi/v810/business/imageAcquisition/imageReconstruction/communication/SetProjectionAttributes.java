package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * @author Roy Williams
 */
public class SetProjectionAttributes extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes();

  private ImageReconstructionEngineEnum _reconstructionEngineId;
  private TestProgram _testProgram = null;
  private boolean _requiredToSplit = false;
  // Wei Chin : PayLoadSize * SplitIndex < 9000 bytes
  // so splitIndex is 300 at this point.
  // todo: make it flexible which handle > 600 number of records...
  private int _splitIndex = 300; 
//  static private boolean _debug = _config.isDeveloperDebugModeOn();

  private Map<Integer, Pair<Integer,Integer>> _splitRecordMap;
  private Map<Integer, IntegerRef> _splitRecordInfoMap;
  private Map<Integer, Integer> _scanPassNumberToTestSubProgramIdMap;
  
  private int _totalNonSplitRecords;

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public SetProjectionAttributes(CommandSender writer,
                                 final ImageReconstructionEngineEnum reconstructionEngineId,
                                 TestProgram testProgram)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.SetProjectionAttributes, writer);
    Assert.expect(reconstructionEngineId != null);
    Assert.expect(testProgram != null);

    _reconstructionEngineId = reconstructionEngineId;
    _testProgram = testProgram;
    
    _splitRecordMap = new TreeMap<Integer, Pair<Integer,Integer>>();
    _splitRecordInfoMap = new TreeMap<Integer,IntegerRef>();
    _scanPassNumberToTestSubProgramIdMap = new TreeMap<Integer,Integer>();
    
    _totalNonSplitRecords = 0;
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return _totalNonSplitRecords;
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());

    if (_debug)
    {
      System.out.println();
      System.out.println("Image Reconstruction Engine: " + _reconstructionEngineId);
      System.out.println("ProjectionRegions: (cameraId scanPassNumber startingStagePosition.getXInNanometers imageRectanglePerCameraPerScanPass.getMinY numberOfCaptureLines");
    }

    int count = 0;
    List<ScanPass> scanPasses = _testProgram.getScanPasses();
    for (XrayCameraAcquisitionParameters cameraParameters : _testProgram.getCameraAcquisitionParameters())
    {
      // cameraId
      AbstractXrayCamera camera = cameraParameters.getCamera();

      for (ProjectionSettings projectionSettings : cameraParameters.getProjectionSettings())
      {        
        count++;        
        int scanPassNumber = projectionSettings.getScanPassNumber();
        
        if(_requiredToSplit && scanPassNumber > _splitIndex)
          break;

        // Coalesce all projectionRegions for each scanpass - emit only one to the IRP's.
        ImageRectangle imageRectanglePerCameraPerScanPass = null;
        for (ProjectionRegion projectionRegion : projectionSettings.getProjectionRegions())
        {
          if (projectionRegion.getProjectionDataDestinationId().equals(_reconstructionEngineId))
          {
            // These are the only ones that we are sending data to.
            if (imageRectanglePerCameraPerScanPass == null)
              imageRectanglePerCameraPerScanPass = new ImageRectangle(projectionRegion.getProjectionRegionRectangle());
            else
              imageRectanglePerCameraPerScanPass.add(projectionRegion.getProjectionRegionRectangle());
          }
        }
        Assert.expect(imageRectanglePerCameraPerScanPass != null);

        // wei chin modify for handle multiboard alignment
        int testSubProgramId = -1;
        testSubProgramId = _scanPassNumberToTestSubProgramIdMap.get(scanPassNumber);

        Assert.expect(testSubProgramId != -1);

        ScanPass scanPass = scanPasses.get(scanPassNumber);
        StagePosition startingStagePosition = scanPass.getStartPointInNanoMeters();
        int yPositionAtOverallProjectionOriginInMachineCoordinates = scanPass.getMechanicalConversions().yLocationForReconstructionOnCamera(camera);

        int nanometerPerPixel = 0;
        if (scanPass.isLowMagnification())
          nanometerPerPixel = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
        else
          nanometerPerPixel = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
        int stageSpeed = (int)(scanPass.getStageSpeedValue() * 10);
        // cameraId
        buffer.putInt(camera.getId());

        // scanPassNumber
        buffer.putInt(scanPassNumber);
        
        buffer.putInt(nanometerPerPixel);

        buffer.putInt((int)scanPass.getUserGainValue());
        
        buffer.putInt(stageSpeed);
        // _testSubProgramId
        buffer.putInt(testSubProgramId);

        // Stage x position in machine coordinates nanometers during scan pass
        buffer.putInt(startingStagePosition.getXInNanometers());

        // Stage y position at overall projection origin (row 0) in machine coordinates.
        buffer.putInt(yPositionAtOverallProjectionOriginInMachineCoordinates);

        // Start capture line of projection relative to row 0 of overall projection
        String space = " ";
        buffer.putInt(imageRectanglePerCameraPerScanPass.getMinY());

        // Total number of lines to capture for this processor strip.
        buffer.putInt(imageRectanglePerCameraPerScanPass.getHeight());
        
        if (_debug)
          System.out.println(camera +
                             space +
                             scanPassNumber +
                             space +
                             testSubProgramId +
                             space +
                             stageSpeed +
                             space +
                             (int)scanPass.getUserGainValue() +
                             space +
                             nanometerPerPixel +
                             space +
                             startingStagePosition.getXInNanometers() +
                             space +
                             yPositionAtOverallProjectionOriginInMachineCoordinates +
                             space +
                             imageRectanglePerCameraPerScanPass.getMinY() +
                             space +
                             imageRectanglePerCameraPerScanPass.getHeight() +
                             space +
                             scanPass.getProjectionType().getId());
        // Projection Type Enumeration
        buffer.put((byte)scanPass.getProjectionType().getId());        
      }
    }
    return buffer;
  }
  
  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Wei Chin
   */
  protected ByteBuffer addSplitPayLoad(ByteBuffer buffer, int startIndex, int endIndex, int totalSplitRecords)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * totalSplitRecords);

    if (_debug)
    {
      System.out.println("Splitting...");
      System.out.println("Image Reconstruction Engine: " + _reconstructionEngineId);
      System.out.println("ProjectionRegions: (cameraId scanPassNumber startingStagePosition.getXInNanometers imageRectanglePerCameraPerScanPass.getMinY numberOfCaptureLines");
    }

    int count = -1;
    List<ScanPass> scanPasses = _testProgram.getScanPasses();
    for (XrayCameraAcquisitionParameters cameraParameters : _testProgram.getCameraAcquisitionParameters())
    {
      // cameraId
      AbstractXrayCamera camera = cameraParameters.getCamera();

      for (ProjectionSettings projectionSettings : cameraParameters.getProjectionSettings())
      {        
        count++;        
        int scanPassNumber = projectionSettings.getScanPassNumber();
        
        if(_requiredToSplit && scanPassNumber > startIndex && scanPassNumber <= endIndex)
        {
          // Coalesce all projectionRegions for each scanpass - emit only one to the IRP's.
          ImageRectangle imageRectanglePerCameraPerScanPass = null;
          for (ProjectionRegion projectionRegion : projectionSettings.getProjectionRegions())
          {
            if (projectionRegion.getProjectionDataDestinationId().equals(_reconstructionEngineId))
            {
              // These are the only ones that we are sending data to.
              if (imageRectanglePerCameraPerScanPass == null)
                imageRectanglePerCameraPerScanPass = new ImageRectangle(projectionRegion.getProjectionRegionRectangle());
              else
                imageRectanglePerCameraPerScanPass.add(projectionRegion.getProjectionRegionRectangle());
            }
          }
          Assert.expect(imageRectanglePerCameraPerScanPass != null);

          // wei chin modify for handle multiboard alignment
          int testSubProgramId = -1;
          testSubProgramId = _scanPassNumberToTestSubProgramIdMap.get(scanPassNumber);

          Assert.expect(testSubProgramId != -1);

          ScanPass scanPass = scanPasses.get(scanPassNumber);
          StagePosition startingStagePosition = scanPass.getStartPointInNanoMeters();
          int yPositionAtOverallProjectionOriginInMachineCoordinates = scanPass.getMechanicalConversions().yLocationForReconstructionOnCamera(camera);

          // cameraId
          buffer.putInt(camera.getId());

          // scanPassNumber
          buffer.putInt(scanPassNumber);

          int nanometerPerPixel = 0;
          if (scanPass.isLowMagnification())
            nanometerPerPixel = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
          else
            nanometerPerPixel = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
          int stageSpeed = (int)(scanPass.getStageSpeedValue() * 10);

          buffer.putInt(nanometerPerPixel);

          buffer.putInt((int)scanPass.getUserGainValue());

          buffer.putInt(stageSpeed);

          // _testSubProgramId
          buffer.putInt(testSubProgramId);

          // Stage x position in machine coordinates nanometers during scan pass
          buffer.putInt(startingStagePosition.getXInNanometers());

          // Stage y position at overall projection origin (row 0) in machine coordinates.
          buffer.putInt(yPositionAtOverallProjectionOriginInMachineCoordinates);

          // Start capture line of projection relative to row 0 of overall projection
          String space = " ";
          buffer.putInt(imageRectanglePerCameraPerScanPass.getMinY());

          // Total number of lines to capture for this processor strip.
          buffer.putInt(imageRectanglePerCameraPerScanPass.getHeight());

          if (_debug)
            System.out.println(camera +
                              space +
                              scanPassNumber +
                              space +
                              testSubProgramId +
                              space +
                              startingStagePosition.getXInNanometers() +
                              space +
                              yPositionAtOverallProjectionOriginInMachineCoordinates +
                              space +
                              imageRectanglePerCameraPerScanPass.getMinY() +
                              space +
                              imageRectanglePerCameraPerScanPass.getHeight() +
                              space +
                              scanPass.getProjectionType().getId());
          // Projection Type Enumeration
          buffer.put((byte)scanPass.getProjectionType().getId());
        }        
      }
    }
    return buffer;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }
  
  /**
   * Sending a message to the ImageReconstructionProcess.  Each message
   * id represents a unique command as defined by the the constructor of the derived
   * class.   The header will be output then the derived class is responsible to
   * construct a payload (i.e. data) to be transmittted.
   *
   * @author Roy Williams
   */
  public void send() throws XrayTesterException
  {
    setupScanPassInfo();
    setupProjectionAttributesInfo();
    
    ByteBuffer buffer = getMessageBuffer();
    buffer.rewind();
    _writer.write(buffer);

    // The message has been sent.
    // Expect a response back from the IRP on each message
    _writer.waitForReadyAcknowledgement(_imageReconstructionMessage);

    emitAssociations(_writer);
    
    if(_requiredToSplit)
    {     
      for(Map.Entry<Integer,Pair<Integer,Integer>> entry : _splitRecordMap.entrySet())
      {
        int entryId = entry.getKey();
        int startIndex = entry.getValue().getFirst();
        int endIndex = entry.getValue().getSecond();
        int totalRecord = _splitRecordInfoMap.get(entryId).getValue();
        
        buffer.clear();
        buffer = null;
        
        buffer = getSplitMessageBuffer(startIndex, endIndex, totalRecord);
        buffer.rewind();
        _writer.write(buffer);
        
        // The message has been sent.
        // Expect a response back from the IRP on each message
        _writer.waitForReadyAcknowledgement(_imageReconstructionMessage);
        
        emitAssociations(_writer);
      }
      
      // Clear map
      _splitRecordMap.clear();
      _splitRecordInfoMap.clear();
    }
    
    _scanPassNumberToTestSubProgramIdMap.clear();
  }

  /**
   * Return the message in a byte buffer.
   *
   * @author Wei Chin
   */
  public final ByteBuffer getSplitMessageBuffer(int startIndex, int endIndex, int totalSplitRecords) throws XrayTesterException
  {
    ByteBuffer buffer = getByteBuffer(getMessageHeaderSize() + (getSizeOfEachRecord() * totalSplitRecords));
    buffer = addSplitMessageHeaderBuffer(buffer, totalSplitRecords);

    if (hasPayLoad())
      buffer = addSplitPayLoad(buffer, startIndex, endIndex, totalSplitRecords);

    return buffer;
  }
  

  /**
   * Composes a message message header consisting of the number of records, size
   * of each record and message id to send to the ImageReconstructionProcess.
   *
   * @author Wei Chin
   */
  protected ByteBuffer addSplitMessageHeaderBuffer(ByteBuffer buffer, int totalSplitRecords)
  {
    Assert.expect(buffer.remaining() >= getMessageHeaderSize());
    int size  = getSizeOfEachRecord();
    int usage = _imageReconstructionMessage.getId();
    if (_config.getBooleanValue(SoftwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_COMMUNICATIONS_HANDSHAKE))
    {
      usage = usage | 0x1000;
    }
    buffer.putShort((short)usage);
    buffer.putShort((short)size);
    buffer.putInt(totalSplitRecords);
    return buffer;
  }
  
  /**
   * Setup total number of records for non-split and split sections.
   * When we have more than 300 scan passes, we need to split it into 
   * several packets.
   * 
   * @author Cheah Lee Herng
   */
  private void setupProjectionAttributesInfo()
  {
    List<ScanPass> scanPasses = _testProgram.getScanPasses();
    int numberOfScanPassesForProgram = scanPasses.size();
    Assert.expect(numberOfScanPassesForProgram > 0);    
    boolean cameraImagesPerScanPass[] = new boolean[numberOfScanPassesForProgram];
    
    if(numberOfScanPassesForProgram > _splitIndex)
      _requiredToSplit = true;
    
    // Clear prior to setting anything
    _splitRecordMap.clear();
    _splitRecordInfoMap.clear();
    
    // Initialize total split records information
    if (_requiredToSplit)
    {
      int totalSplitRecords = numberOfScanPassesForProgram - _splitIndex;
      int totalSplitGroup = (int)Math.ceil((double)totalSplitRecords / _splitIndex);
      for(int i=1; i <= totalSplitGroup; i++)
      {
        _splitRecordMap.put(i, new Pair<Integer,Integer>(i * _splitIndex, (i+1) * _splitIndex));
        _splitRecordInfoMap.put(i, new IntegerRef(0));
      }
    }
    
    for (XrayCameraAcquisitionParameters cameraParameters : _testProgram.getCameraAcquisitionParameters())
    {
      // cameraId
      AbstractXrayCamera camera = cameraParameters.getCamera();

      // Initialize an array to insure we have a ProjectionRegion(s) to capture on
      // each IRP per camera.
      for (int i = 0; i < numberOfScanPassesForProgram; ++i)
        cameraImagesPerScanPass[i] = false;

      for (ProjectionSettings projectionSettings : cameraParameters.getProjectionSettings())
      {
        int scanPassNumber = projectionSettings.getScanPassNumber();
        Assert.expect(scanPassNumber >= 0);

        ScanPass scanPass = scanPasses.get(scanPassNumber);
        Assert.expect(scanPass != null);

        StagePosition startingStagePosition = scanPass.getStartPointInNanoMeters();
        Assert.expect(startingStagePosition != null);

        StagePosition endStagePosition = scanPass.getEndPointInNanoMeters();
        Assert.expect(endStagePosition != null);

        // Both start and end should have the same x position.
        Assert.expect(startingStagePosition.getXInNanometers() == endStagePosition.getXInNanometers());

        for (ProjectionRegion projectionRegion : projectionSettings.getProjectionRegions())
        {
          if (projectionRegion.getProjectionDataDestinationId().equals(_reconstructionEngineId))
          {
            if (cameraImagesPerScanPass[scanPassNumber] == false)
            {
              cameraImagesPerScanPass[scanPassNumber] = true;
              
              if(_requiredToSplit == false || scanPassNumber <= _splitIndex)
              {
                _totalNonSplitRecords++;
              }
              else
              {
                // Handle split records condition                
                for(Map.Entry<Integer,Pair<Integer,Integer>> entry : _splitRecordMap.entrySet())
                {
                  int entryId = entry.getKey();
                  Pair<Integer,Integer> range = entry.getValue();
                  if (scanPassNumber > range.getFirst() && scanPassNumber <= range.getSecond())
                  {
                    _splitRecordInfoMap.get(entryId).increment();
                  }
                }
              }
            }
          }
        }
      }

      // Check to insure we have a ProjectionRegion(s) to capture on
      // each IRP per camera.
      for (int i = 0; i < numberOfScanPassesForProgram; i++)
      {
        Assert.expect(cameraImagesPerScanPass[i] != false,  "No images for scanpass: " + i + " on camera: " + camera.getId());
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setupScanPassInfo()
  {
    Assert.expect(_scanPassNumberToTestSubProgramIdMap != null);
    
    _scanPassNumberToTestSubProgramIdMap.clear();
    
    for (TestSubProgram testSubProgram : _testProgram.getFilteredTestSubPrograms())
    {
      for(ScanPass scanPass : testSubProgram.getScanPasses())
      {
        _scanPassNumberToTestSubProgramIdMap.put(scanPass.getId(), testSubProgram.getId());
      }
    }
  }
}
