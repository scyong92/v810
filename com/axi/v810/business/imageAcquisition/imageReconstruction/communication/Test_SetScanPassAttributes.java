package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Test class for the SetScanPassAttributes message.
 *
 * @author Dave Ferguson
 */
public class Test_SetScanPassAttributes extends UnitTest
{
  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      int testSubProgramId = 42;
      List<ScanPass> scanPath = new ArrayList<ScanPass>();
      int numScanPasses = 22;
      int xStartBase = 100;
      int yStartBase = 1;
      int xEndBase = 101;
      int yEndBase = 12;

      for (int i = 0; i < numScanPasses; ++i)
      {
        StagePositionMutable start = new StagePositionMutable();
        start.setXInNanometers(xStartBase * i);
        start.setYInNanometers(yStartBase * i);

        StagePositionMutable end = new StagePositionMutable();
        end.setXInNanometers(xEndBase * i);
        end.setYInNanometers(yEndBase * i);

        scanPath.add(new ScanPass(i, start, end));
      }

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      SetScanPassAttributes setScanPassAttributesMsg =
        new SetScanPassAttributes(commandWriter, scanPath, testSubProgramId);

      ByteBuffer msg = setScanPassAttributesMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + (28 * numScanPasses)));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.SetScanPassAttributes.getId());
      Expect.expect(messageSize == 28);
      Expect.expect(messageCount == numScanPasses);
      for (ScanPass scanPass : scanPath)
      {
        Expect.expect(msg.getInt() == scanPass.getId());
        Expect.expect(msg.getInt() == numScanPasses);
        Expect.expect(msg.getInt() == testSubProgramId);
        Expect.expect(msg.getInt() == scanPass.getStartPointInNanoMeters().getXInNanometers());
        Expect.expect(msg.getInt() == scanPass.getStartPointInNanoMeters().getYInNanometers());
        Expect.expect(msg.getInt() == scanPass.getEndPointInNanoMeters().getXInNanometers());
        Expect.expect(msg.getInt() == scanPass.getEndPointInNanoMeters().getYInNanometers());
      }
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();

      // Shouldn't happen...
      Expect.expect(false);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_SetScanPassAttributes() );
  }
}
