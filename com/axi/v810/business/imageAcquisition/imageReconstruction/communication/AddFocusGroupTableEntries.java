package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 * @author Khaw Chek Hau
 * @XCR3554: Package on package (PoP) development
 */
public class AddFocusGroupTableEntries extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() + 
                              JavaTypeSizeEnum.BYTE.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() + 
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes() +
                              JavaTypeSizeEnum.INT.getSizeInBytes();

  private ReconstructionRegion _reconstructionRegion = null;
  private List<FocusGroup> _focusGroups = null;

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public AddFocusGroupTableEntries(CommandSender writer,
                                   ReconstructionRegion reconstructionRegion)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.AddFocusGroupTableEntry, writer);
    Assert.expect(reconstructionRegion != null);

    _reconstructionRegion = reconstructionRegion;
    _focusGroups = new ArrayList<FocusGroup>();
    for (FocusGroup focusGroup : _reconstructionRegion.getFocusGroups())
    {
      if (focusGroup.hasSlicesToCreate())
      {
        _focusGroups.add(focusGroup);
      }
    }
  }

  /**
   * @return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return _focusGroups.size();
  }

  /**
   * @return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());

    for (FocusGroup focusGroup : _focusGroups)
    {
      // Focus Group Id: int32
      int focusGroupId = focusGroup.getId();
      Assert.expect(focusGroupId >= 0);
      buffer.putInt(focusGroupId);

      // Reconstruction Region Id: int32
      int reconstructionRegionId = _reconstructionRegion.getRegionId();
      Assert.expect(reconstructionRegionId >= 0);
      buffer.putInt(reconstructionRegionId);

      // Learned Z-offset in nanometers: int32
      buffer.putInt(focusGroup.getRelativeZoffsetFromSurfaceInNanoMeters());

      // Single sided region (boolean): byte
      buffer.put(focusGroup.isSingleSidedRegionOfBoard() ? (byte) 0x01 : (byte) 0x00);

      // AutoFocus Mid-Board in nanometers: int32
      buffer.putInt(focusGroup.getAutoFocusMidBoardOffsetInNanometers());
      
      // Save Sharpness profile for this focus group? (boolean): byte
      buffer.put(focusGroup.isSaveSharpnessProfile() ? (byte) 0x01 : (byte) 0x00);
      
      // Save UsePhaseShiftProfiliometry for this focus group? (boolean): byte
      buffer.put(focusGroup.isUsePhaseShiftProfiliometry() ? (byte) 0x01 : (byte) 0x00);
      
      // AutoFocus Search Range - Low Limit in nanometers: int32
      buffer.putInt(focusGroup.getSearchRangeLowLimitInNanometers());
      
      // AutoFocus Search Range - High Limit in nanometers: int32
      buffer.putInt(focusGroup.getSearchRangeHighLimitInNanometers());
 
      // PSP Z-Offset - in nanometers: int32
      buffer.putInt(focusGroup.getPspZOffsetInNanometers());
      
      //Khaw Chek Hau - XCR3554: Package on package (PoP) development
      // POP Total Layer number: int32
      buffer.putInt(focusGroup.getPOPTotalLayerNumber()); 
      
      // POP Current Layer number: int32
      buffer.putInt(focusGroup.getPOPLayerId().toInteger()); 
      
      // POP Z-Height with base layer: int32
      buffer.putInt(focusGroup.getPOPZHeightInNanometers()); 
    }
    return buffer;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }

  /**
   * @author Roy Williams
   */
  protected void emitAssociations(CommandSender writer) throws XrayTesterException
  {
    int reconstructionRegionId = _reconstructionRegion.getRegionId();
    for (FocusGroup focusGroup : _focusGroups)
    {
      if (focusGroup.getSlices().size() > 0)
        new AddSliceTableEntries(writer, reconstructionRegionId, focusGroup).send();

      if (focusGroup.hasNeighbors())
        new AddNeighborEntries(writer, reconstructionRegionId, focusGroup).send();

      new AddFocusSearchParametersEntries(writer, reconstructionRegionId, focusGroup).send();
    }
  }
}
