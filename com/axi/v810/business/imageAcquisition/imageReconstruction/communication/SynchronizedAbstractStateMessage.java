package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class SynchronizedAbstractStateMessage extends AbstractStateMessage implements AcknowledgeImageAcquisitionProcessorSyncInt
{
  private ImageStateReceiver _synchNotifier;
  private BooleanLock _messageAcknowledgeReceived = new BooleanLock(false);

  private static Config _config = Config.getInstance();

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  protected SynchronizedAbstractStateMessage(ImageReconstructionMessageEnum messageId,
                                             CommandSender writer,
                                             ImageStateReceiver synchNotifier)
      throws XrayTesterException
  {
    super(messageId, writer);

    Assert.expect(synchNotifier != null);

    _synchNotifier = synchNotifier;
  }

  /**
   * @author Roy Williams
   */
  public void send() throws XrayTesterException
  {
    // Register with the ImageStateReceiver to have it notify me when the abort
    // acknowledgement arrives.
    _synchNotifier.notifyMessage(this);

    _messageAcknowledgeReceived.setValue(false);

    // Send the message.
    super.send();
    if (isWriterValid() == false)
      return;

    // Await for the reply before returning only if we are not in sim mode.
    ImageAcquisitionEngine iae = ImageAcquisitionEngine.getInstance();
    if (_config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) &&
        iae.isOfflineReconstructionEnabled() == false)
    {
      return;
    }

    int waitTimeInMilliSeconds = 200;
    int accumulatedWaitTimeInMilliSeconds = 0;
    int maximumAccumulatedWaitTimeInMilliSeconds = 300000;
    while (_messageAcknowledgeReceived.isFalse() && accumulatedWaitTimeInMilliSeconds < maximumAccumulatedWaitTimeInMilliSeconds)
    {
      try
      {
        _messageAcknowledgeReceived.waitUntilTrue(waitTimeInMilliSeconds);
        accumulatedWaitTimeInMilliSeconds += waitTimeInMilliSeconds;
        if (iae.getReconstructedImagesProducer().areHardwareMessagingCommunicationsCancelled())
          return;
      }
      catch (InterruptedException iex)
      {
        Assert.expect(false, "unexpected interruption of wait for abort ack");
      }
    }

    if (_messageAcknowledgeReceived.isFalse())
    {
      ImageReconstructionEngine ire = getImageReconstructionEngine();
      BusinessException bex = new FailedReadingImageReconstructionSocketTimeoutBusinessException(
          getImageReconstructionEngine(),
          ire.getFirstIpAddress().getHostAddress(),
          ire.getPort(),
          (int) accumulatedWaitTimeInMilliSeconds/1000);
      throw bex;
    }
  }

  /**
   * @author Roy Williams
   */
  public void acknowledgeMessage()
  {
    _messageAcknowledgeReceived.setValue(true);
  }
}
