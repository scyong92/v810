package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.nio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;


/**
 * @author Roy Williams
 */
public class SetPanelAttributes extends ImageReconstructionMessage
{
  private int _PAYLOAD_SIZE = JavaTypeSizeEnum.INT.getSizeInBytes() +
                             JavaTypeSizeEnum.INT.getSizeInBytes() +
                             JavaTypeSizeEnum.INT.getSizeInBytes();

  private int _width;
  private int _length;
  private int _thickness;

  /**
   * Create a message with my designated message ID.
   *
   * @author Roy Williams
   */
  public SetPanelAttributes(CommandSender writer,
                            TestProgram testProgram)
      throws XrayTesterException
  {
    super(ImageReconstructionMessageEnum.SetPanelAttributes, writer);
    Assert.expect(testProgram != null);

    Panel panel = testProgram.getProject().getPanel();
    _width = panel.getWidthAfterAllRotationsInNanoMeters();
    _length = panel.getLengthAfterAllRotationsInNanoMeters();
    _thickness = panel.getThicknessInNanometers();
  }

  /**
   * Return the number of records to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getNumberOfRecords()
  {
    return 1;
  }

  /**
   * Return the sizeof each record to be sent to the IRP.
   *
   * @author Roy Williams
   */
  protected int getSizeOfEachRecord()
  {
    Assert.expect(_PAYLOAD_SIZE > 0);
    return _PAYLOAD_SIZE;
  }

  /**
   * @author George A. David
   */
  protected boolean hasPayLoad()
  {
    return true;
  }

  /**
   * This method will fulfill the contract of the previous countAndSize method
   * by returning add the payload record(s) to the specified ByteBuffer to be
   * sent to the ImageReconstructionProcess.
   *
   * @author Roy Williams
   */
  protected ByteBuffer addPayLoad(ByteBuffer buffer)
  {
    Assert.expect(buffer.remaining() >= getSizeOfEachRecord() * getNumberOfRecords());
    buffer.putInt(_width);
    buffer.putInt(_length);
    buffer.putInt(_thickness);
    return buffer;
  }
}

