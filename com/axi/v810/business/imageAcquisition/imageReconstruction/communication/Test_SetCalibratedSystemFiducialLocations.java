package com.axi.v810.business.imageAcquisition.imageReconstruction.communication;

import java.io.*;
import java.net.*;
import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Test class for the SetCalibratedSystemFiducialLocations message.
 *
 * @author Dave Ferguson
 */
public class Test_SetCalibratedSystemFiducialLocations extends UnitTest
{
  private Config _config;
  private Map<Integer, Integer> _cameraIdToStageXoriginalDataMap;
  private Map<Integer, Integer> _cameraIdToStageYoriginalDataMap;
  private Map<Integer, Integer> _cameraIdToRowOriginalDataMap;
  private Map<Integer, Integer> _cameraIdToColumnOriginalDataMap;
  private Map<Integer, Integer> _cameraIdToStageXexpectDataMap;
  private Map<Integer, Integer> _cameraIdToStageYexpectDataMap;
  private Map<Integer, Integer> _cameraIdToRowExpectDataMap;
  private Map<Integer, Integer> _cameraIdToColumnExpectDataMap;

  /**
   * @author Greg Esparza
   */
  public Test_SetCalibratedSystemFiducialLocations()
  {
    _cameraIdToStageXexpectDataMap = new HashMap<Integer, Integer>();
    _cameraIdToStageYexpectDataMap = new HashMap<Integer, Integer>();
    _cameraIdToRowExpectDataMap = new HashMap<Integer, Integer>();
    _cameraIdToColumnExpectDataMap = new HashMap<Integer, Integer>();

    _cameraIdToStageXoriginalDataMap = new HashMap<Integer,Integer>();
    _cameraIdToStageYoriginalDataMap = new HashMap<Integer,Integer>();
    _cameraIdToRowOriginalDataMap = new HashMap<Integer,Integer>();
    _cameraIdToColumnOriginalDataMap = new HashMap<Integer,Integer>();

    _config = Config.getInstance();
  }

  /**
   * @author Dave Ferguson
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    ImageReconstructionEngine ire = ImageReconstructionEngine.getInstance(ImageReconstructionEngineEnum.IRE0);

    try
    {
      backupOriginalData();
      setUpExpectedDataMaps();

      CommandSender commandWriter = new CommandSender(ire, InetAddress.getLocalHost(), 9119);
      SetCalibratedSystemFiducialLocations setCalibratedSystemFiducialLocationsMsg =
        new SetCalibratedSystemFiducialLocations(commandWriter);

      ByteBuffer msg = setCalibratedSystemFiducialLocationsMsg.getMessageBuffer();
      Expect.expect(msg.capacity() == (8 + (20 * XrayCameraArray.getNumberOfCameras())));
      Expect.expect(msg.remaining() == 0);
      msg.rewind();
      short messageId = msg.getShort();
      short messageSize = msg.getShort();
      int messageCount = msg.getInt();
      Expect.expect(messageId == ImageReconstructionMessageEnum.SetCalibratedSystemFiducialLocation.getId());
      Expect.expect(messageSize == 20);
      Expect.expect(messageCount == XrayCameraArray.getNumberOfCameras());
      checkExpectedData(msg);
      restoreOriginalData();
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Dave Ferguson
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_SetCalibratedSystemFiducialLocations() );
  }

  /**
   * @author Greg Esparza
   */
  private void setUpExpectedDataMaps() throws XrayTesterException
  {
    int baseValue = 0;
    int baseValueFactor = 10;
    int cameraId = 0;

    List<AbstractXrayCamera> xRayCameras = XrayCameraArray.getCameras();
    for (AbstractXrayCamera xRayCamera : xRayCameras)
    {
      cameraId = xRayCamera.getId();
      baseValue = baseValueFactor * cameraId;

      if (cameraId == 0)
      {
        _cameraIdToStageXexpectDataMap.put(new Integer(cameraId), new Integer(baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS, baseValue);

        _cameraIdToStageYexpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS, baseValue);

        _cameraIdToRowExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_ROW, baseValue);

        _cameraIdToColumnExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_COLUMN, baseValue);
      }
      else if (cameraId == 1)
      {
        _cameraIdToStageXexpectDataMap.put(new Integer(cameraId), new Integer(baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS, baseValue);

        _cameraIdToStageYexpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS, baseValue);

        _cameraIdToRowExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_ROW, baseValue);

        _cameraIdToColumnExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_COLUMN, baseValue);
      }
      else if (cameraId == 2)
      {
        _cameraIdToStageXexpectDataMap.put(new Integer(cameraId), new Integer(baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS, baseValue);

        _cameraIdToStageYexpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS, baseValue);

        _cameraIdToRowExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_ROW, baseValue);

        _cameraIdToColumnExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_COLUMN, baseValue);
      }
      else if (cameraId == 3)
      {
        _cameraIdToStageXexpectDataMap.put(new Integer(cameraId), new Integer(baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS, baseValue);

        _cameraIdToStageYexpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS, baseValue);

        _cameraIdToRowExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_ROW, baseValue);

        _cameraIdToColumnExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_COLUMN, baseValue);
      }
      else if (cameraId == 4)
      {
        _cameraIdToStageXexpectDataMap.put(new Integer(cameraId), new Integer(baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS, baseValue);

        _cameraIdToStageYexpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS, baseValue);

        _cameraIdToRowExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_ROW, baseValue);

        _cameraIdToColumnExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_COLUMN, baseValue);
      }
      else if (cameraId == 5)
      {
        _cameraIdToStageXexpectDataMap.put(new Integer(cameraId), new Integer(baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS, baseValue);

        _cameraIdToStageYexpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS, baseValue);

        _cameraIdToRowExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_ROW, baseValue);

        _cameraIdToColumnExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_COLUMN, baseValue);
      }
      else if (cameraId == 6)
      {
        _cameraIdToStageXexpectDataMap.put(new Integer(cameraId), new Integer(baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS, baseValue);

        _cameraIdToStageYexpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS, baseValue);

        _cameraIdToRowExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_ROW, baseValue);

        _cameraIdToColumnExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_COLUMN, baseValue);
      }
      else if (cameraId == 7)
      {
        _cameraIdToStageXexpectDataMap.put(new Integer(cameraId), new Integer(baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS, baseValue);

        _cameraIdToStageYexpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS, baseValue);

        _cameraIdToRowExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_ROW, baseValue);

        _cameraIdToColumnExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_COLUMN, baseValue);
      }
      else if (cameraId == 8)
      {
        _cameraIdToStageXexpectDataMap.put(new Integer(cameraId), new Integer(baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS, baseValue);

        _cameraIdToStageYexpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS, baseValue);

        _cameraIdToRowExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_ROW, baseValue);

        _cameraIdToColumnExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_COLUMN, baseValue);
      }
      else if (cameraId == 9)
      {
        _cameraIdToStageXexpectDataMap.put(new Integer(cameraId), new Integer(baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS, baseValue);

        _cameraIdToStageYexpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS, baseValue);

        _cameraIdToRowExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_ROW, baseValue);

        _cameraIdToColumnExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_COLUMN, baseValue);
      }
      else if (cameraId == 10)
      {
        _cameraIdToStageXexpectDataMap.put(new Integer(cameraId), new Integer(baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS, baseValue);

        _cameraIdToStageYexpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS, baseValue);

        _cameraIdToRowExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_ROW, baseValue);

        _cameraIdToColumnExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_COLUMN, baseValue);
      }
      else if (cameraId == 11)
      {
        _cameraIdToStageXexpectDataMap.put(new Integer(cameraId), new Integer(baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS, baseValue);

        _cameraIdToStageYexpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS, baseValue);

        _cameraIdToRowExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_ROW, baseValue);

        _cameraIdToColumnExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_COLUMN, baseValue);
      }
      else if (cameraId == 12)
      {
        _cameraIdToStageXexpectDataMap.put(new Integer(cameraId), new Integer(baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS, baseValue);

        _cameraIdToStageYexpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS, baseValue);

        _cameraIdToRowExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_ROW, baseValue);

        _cameraIdToColumnExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_COLUMN, baseValue);
      }
      else if (cameraId == 13)
      {
        _cameraIdToStageXexpectDataMap.put(new Integer(cameraId), new Integer(baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS, baseValue);

        _cameraIdToStageYexpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS, baseValue);

        _cameraIdToRowExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_ROW, baseValue);

        _cameraIdToColumnExpectDataMap.put(new Integer(cameraId), new Integer(++baseValue));
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_COLUMN, baseValue);
      }
      else
        Assert.expect(false);
    }
  }

  /**
   * @author Greg Esparza
   */
  private void checkExpectedData(ByteBuffer msg)
  {
    List<AbstractXrayCamera> xRayCameras = XrayCameraArray.getCameras();

    for (AbstractXrayCamera xRayCamera : xRayCameras)
    {
      Integer cameraId = msg.getInt();
      Integer stageX = _cameraIdToStageXexpectDataMap.get(cameraId);
      Assert.expect(stageX != null);
      Expect.expect(msg.getInt() == stageX.intValue());

      Integer stageY = _cameraIdToStageYexpectDataMap.get(cameraId);
      Assert.expect(stageY != null);
      Expect.expect(msg.getInt() == stageY.intValue());

      Integer column = _cameraIdToColumnExpectDataMap.get(cameraId);
      Assert.expect(column != null);
      Expect.expect(msg.getInt() == column.intValue());

      Integer row = _cameraIdToRowExpectDataMap.get(cameraId);
      Assert.expect(row != null);
      Expect.expect(msg.getInt() == row.intValue());
    }
  }

  /**
   * @author Greg Esparza
   */
  private void backupOriginalData()
  {
    List<AbstractXrayCamera> xRayCameras = XrayCameraArray.getCameras();
    for (AbstractXrayCamera xRayCamera : xRayCameras)
    {
      Integer cameraId = xRayCamera.getId();
      if (cameraId.intValue() == 0)
      {
        _cameraIdToStageXoriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS));
        _cameraIdToStageYoriginalDataMap.put(cameraId,  _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS));
        _cameraIdToRowOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_ROW));
        _cameraIdToColumnOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_COLUMN));
      }
      else if (cameraId.intValue() == 1)
      {
        _cameraIdToStageXoriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS));
        _cameraIdToStageYoriginalDataMap.put(cameraId,  _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS));
        _cameraIdToRowOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_ROW));
        _cameraIdToColumnOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_COLUMN));
      }
      else if (cameraId.intValue() == 2)
      {
        _cameraIdToStageXoriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS));
        _cameraIdToStageYoriginalDataMap.put(cameraId,  _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS));
        _cameraIdToRowOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_ROW));
        _cameraIdToColumnOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_COLUMN));
      }
      else if (cameraId.intValue() == 3)
      {
        _cameraIdToStageXoriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS));
        _cameraIdToStageYoriginalDataMap.put(cameraId,  _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS));
        _cameraIdToRowOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_ROW));
        _cameraIdToColumnOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_COLUMN));
      }
      else if (cameraId.intValue() == 4)
      {
        _cameraIdToStageXoriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS));
        _cameraIdToStageYoriginalDataMap.put(cameraId,  _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS));
        _cameraIdToRowOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_ROW));
        _cameraIdToColumnOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_COLUMN));
      }
      else if (cameraId.intValue() == 5)
      {
        _cameraIdToStageXoriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS));
        _cameraIdToStageYoriginalDataMap.put(cameraId,  _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS));
        _cameraIdToRowOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_ROW));
        _cameraIdToColumnOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_COLUMN));
      }
      else if (cameraId.intValue() == 6)
      {
        _cameraIdToStageXoriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS));
        _cameraIdToStageYoriginalDataMap.put(cameraId,  _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS));
        _cameraIdToRowOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_ROW));
        _cameraIdToColumnOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_COLUMN));
      }
      else if (cameraId.intValue() == 7)
      {
        _cameraIdToStageXoriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS));
        _cameraIdToStageYoriginalDataMap.put(cameraId,  _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS));
        _cameraIdToRowOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_ROW));
        _cameraIdToColumnOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_COLUMN));
      }
      else if (cameraId.intValue() == 8)
      {
        _cameraIdToStageXoriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS));
        _cameraIdToStageYoriginalDataMap.put(cameraId,  _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS));
        _cameraIdToRowOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_ROW));
        _cameraIdToColumnOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_COLUMN));
      }
      else if (cameraId.intValue() == 9)
      {
        _cameraIdToStageXoriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS));
        _cameraIdToStageYoriginalDataMap.put(cameraId,  _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS));
        _cameraIdToRowOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_ROW));
        _cameraIdToColumnOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_COLUMN));
      }
      else if (cameraId.intValue() == 10)
      {
        _cameraIdToStageXoriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS));
        _cameraIdToStageYoriginalDataMap.put(cameraId,  _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS));
        _cameraIdToRowOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_ROW));
        _cameraIdToColumnOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_COLUMN));
      }
      else if (cameraId.intValue() == 11)
      {
        _cameraIdToStageXoriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS));
        _cameraIdToStageYoriginalDataMap.put(cameraId,  _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS));
        _cameraIdToRowOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_ROW));
        _cameraIdToColumnOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_COLUMN));
      }
      else if (cameraId.intValue() == 12)
      {
        _cameraIdToStageXoriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS));
        _cameraIdToStageYoriginalDataMap.put(cameraId,  _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS));
        _cameraIdToRowOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_ROW));
        _cameraIdToColumnOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_COLUMN));
      }
      else if (cameraId.intValue() == 13)
      {
        _cameraIdToStageXoriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS));
        _cameraIdToStageYoriginalDataMap.put(cameraId,  _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS));
        _cameraIdToRowOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_ROW));
        _cameraIdToColumnOriginalDataMap.put(cameraId, _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_COLUMN));
      }
      else
        Assert.expect(false);
    }
  }

  /**
   * @author Greg Esparza
   */
  private void restoreOriginalData() throws XrayTesterException
  {
    List<AbstractXrayCamera> xRayCameras = XrayCameraArray.getCameras();
    for (AbstractXrayCamera xRayCamera : xRayCameras)
    {
      Integer cameraId = xRayCamera.getId();

      Integer stageX = _cameraIdToStageXoriginalDataMap.get(cameraId);
      Assert.expect(stageX != null);
      Integer stageY = _cameraIdToStageYoriginalDataMap.get(cameraId);
      Assert.expect(stageY != null);
      Integer row = _cameraIdToRowOriginalDataMap.get(cameraId);
      Assert.expect(row != null);
      Integer column = _cameraIdToColumnOriginalDataMap.get(cameraId);
      Assert.expect(column != null);

      if (cameraId == 0)
      {
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS, stageX);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS, stageY);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_ROW, row);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_COLUMN, column);
      }
      else if (cameraId == 1)
      {
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS, stageX);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS, stageY);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_ROW, row);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_COLUMN, column);
      }
      else if (cameraId == 2)
      {
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS, stageX);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS, stageY);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_ROW, row);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_COLUMN, column);
      }
      else if (cameraId == 3)
      {
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS, stageX);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS, stageY);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_ROW, row);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_COLUMN, column);
      }
      else if (cameraId == 4)
      {
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS, stageX);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS, stageY);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_ROW, row);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_COLUMN, column);
      }
      else if (cameraId == 5)
      {
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS, stageX);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS, stageY);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_ROW, row);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_COLUMN, column);
      }
      else if (cameraId == 6)
      {
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS, stageX);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS, stageY);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_ROW, row);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_COLUMN, column);
      }
      else if (cameraId == 7)
      {
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS, stageX);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS, stageY);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_ROW, row);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_COLUMN, column);
      }
      else if (cameraId == 8)
      {
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS, stageX);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS, stageY);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_ROW, row);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_COLUMN, column);
      }
      else if (cameraId == 9)
      {
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS, stageX);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS, stageY);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_ROW, row);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_COLUMN, column);
      }
      else if (cameraId == 10)
      {
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS, stageX);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS, stageY);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_ROW, row);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_COLUMN, column);
      }
      else if (cameraId == 11)
      {
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS, stageX);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS, stageY);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_ROW, row);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_COLUMN, column);
      }
      else if (cameraId == 12)
      {
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS, stageX);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS, stageY);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_ROW, row);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_COLUMN, column);
      }
      else if (cameraId == 13)
      {
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS, stageX);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS, stageY);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_ROW, row);
        _config.setValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_COLUMN, column);
      }
      else
        Assert.expect(false);
    }
  }
}
