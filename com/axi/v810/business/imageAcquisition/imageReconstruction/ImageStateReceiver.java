package com.axi.v810.business.imageAcquisition.imageReconstruction;

import java.net.*;
import java.nio.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.util.*;

/**
 * Reads from a socket attached to the Image Reconstruction Processor to receive
 * packets reflecting its ability to handle (or process) more data.
 *
 * @author Roy Williams
 */
public class ImageStateReceiver extends ImageReconstructionReceiverThread
{
  private static boolean _debug = false;
  private List<AcknowledgeImageAcquisitionProcessorSyncInt> _acknowledgeMessages = null;

  /**
   * @author Roy Williams
   */
  public ImageStateReceiver(ImageReconstructionEngine imageReconstructionEngine,
                          InetAddress address,
                          int port)
      throws BusinessException
  {
    super(imageReconstructionEngine, address, port, ImageReconstructionMessageEnum.ConnectToStateSender);
    _acknowledgeMessages = new ArrayList<AcknowledgeImageAcquisitionProcessorSyncInt>(5);
  }

  /**
   * @author Roy Williams
   */
  public ByteBuffer readData() throws BusinessException
  {
    if (getImageReconstructionEngine().isDeepSimulation() == false)
    {
      if (getImageReconstructionEngine().isSimulationModeOn() == true)
      {
        return null;
      }
    }

    // Extract the information from the byteBuffer
    ByteBuffer byteBuffer = readByteBuffer(JavaTypeSizeEnum.INT.getSizeInBytes());
    return byteBuffer;
  }

  /**
   * @author Roy Williams
   */
  public void dispatchDataToObservable(ByteBuffer byteBuffer) throws XrayTesterException
  {
    Assert.expect(byteBuffer != null);

    int selection = byteBuffer.getInt();
    
    if (_acknowledgeMessages.size() > 0)
    {
      AcknowledgeImageAcquisitionProcessorSyncInt ackReceiver = null;
      for (AcknowledgeImageAcquisitionProcessorSyncInt ackMessage : _acknowledgeMessages)
      {
        int messageId = ackMessage.getMessageId();
        if (selection == messageId)
        {
          ackReceiver = ackMessage;
          break;
        }
      }
      if (ackReceiver != null)
      {
        _acknowledgeMessages.remove(ackReceiver);
        ackReceiver.acknowledgeMessage();
        return;  // return here because there's a connectFailed message that won't be deal when connectComplete have been dealt. 
      }

      // Always, always return till the _acknowledgeMessages have been dealt with.
      //return;
    }
    
    // Talk directly to IRE to prevent unnecessary delay going through observervable.
    ImageReconstructionEngine ire = getImageReconstructionEngine();
    int num = ire.incrementRunnableScanPass();
    if (ImageReconstructionEventEnum.PROJECTIONS_FOR_SCAN_PASS_RECEIVED.getId() == selection)
    {
      if (_debug)
      {
        System.out.println("IRP: " + ire.getId() + " received ScanPass: " + num);
      }
    }
    else
    {
      Assert.expect(false, "Invalid ImageReconstructionEventEnum");
    }
  }

  /**
   * @author Roy Williams
   */
  public static void setDebug(boolean state)
  {
    _debug = state;
  }

  /**
   * @author Roy Williams
   */
  public void notifyMessage(AcknowledgeImageAcquisitionProcessorSyncInt receiver)
  {
    Assert.expect(receiver != null);

    _acknowledgeMessages.add(receiver);
  }
  
  /**
   * @author Beh Kheng Ling
   * Remove the message receiver which never been used
   */
  public void denotifyMessage(AcknowledgeImageAcquisitionProcessorSyncInt receiver)
  {
    Assert.expect(receiver != null);
    
    _acknowledgeMessages.remove(receiver);
  }
}
