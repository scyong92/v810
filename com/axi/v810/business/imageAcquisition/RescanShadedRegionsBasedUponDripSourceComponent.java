package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
abstract public class RescanShadedRegionsBasedUponDripSourceComponent extends RescanStrategy
{
  private List<ReconstructionRegion> _regionsDripAffectedByMoreThanOneComponent = new ArrayList<ReconstructionRegion>();
  private List<ReconstructionRegion> _regionsDripAffectedByZeroComponents = new ArrayList<ReconstructionRegion>();
  Map<String, List<ReconstructionRegion>> _regionsWithSameDripSourceComponent = null;
  Map<String, List<ReconstructionRegion>> _regionsFromSameSignalCompensatedComponent = null;
  Map<String, List<ReconstructionRegion>> _regionsWithSameSignalCompensation = null;


  private static boolean _debug = false;

  /**
   * @author Roy Williams
   */
  protected RescanShadedRegionsBasedUponDripSourceComponent(ScanStrategyEnum scanStrategy)
  {
    super(scanStrategy);
  }

  /**
   * @author Roy Williams
   */
  public List<HomogeneousImageGroup> createGroups(Collection<ReconstructionRegion> unSortedRegions) throws XrayTesterException
  {
    Assert.expect(unSortedRegions != null);

    // Group them first based upon maxCandidateStepSize for each region.
    if (_debug)
      TimerUtil.printCurrentTime("      Start createMapsOfReconstructionRegionsWithSameSourceComponent size:"+unSortedRegions.size());
    createMapsOfReconstructionRegionsWithSameSourceComponent(unSortedRegions);
    if (_debug)
      TimerUtil.printCurrentTime("      End createMapsOfReconstructionRegionsWithSameSourceComponent");

    // Create a sorted list for each group.
    List<HomogeneousImageGroup> groups = new ArrayList<HomogeneousImageGroup>();

    // Add groups which is same in singla compensation
    if(_regionsWithSameSignalCompensation!=null)
    {
      add(groups, _regionsWithSameSignalCompensation);
    }
    
    // Add groups for signal compensated parts.
    add(groups, _regionsFromSameSignalCompensatedComponent);

    // Add groups for drip affected parts.
    add(groups, _regionsWithSameDripSourceComponent);

    // Create an ImageGroup for each ReconstructionRegion that is drip affected
    // by more than one drip source.  The hope is it will be merged with another
    // group later during mergeGroups().
    List<ReconstructionRegion> individualRegionsToScan = null;

    // Create an ImageGroup for each ReconstructionRegion that is drip affected
    // by more than one drip source.  The hope is it will be merged with another
    // group later during mergeGroups().
    if (_debug)
      TimerUtil.printCurrentTime("      Start _regionsDripAffectedByZeroComponents loop:"+_regionsDripAffectedByZeroComponents.size());
    for (ReconstructionRegion region : _regionsDripAffectedByZeroComponents)
    {
      individualRegionsToScan = new ArrayList<ReconstructionRegion>(1);
      individualRegionsToScan.add(region);
      HomogeneousImageGroup imageGroup = new HomogeneousImageGroup(individualRegionsToScan);
      groups.add(imageGroup);
    }
    if (_debug)
      TimerUtil.printCurrentTime("      End _regionsDripAffectedByZeroComponents loop");

    // Create an ImageGroup for each ReconstructionRegion that is drip affected
    // by more than one drip source.  The hope is it will be merged with another
    // group later during mergeGroups().
    if (_debug)
      TimerUtil.printCurrentTime("      Start _regionsDripAffectedByMoreThanOneComponent loop size:"+_regionsDripAffectedByMoreThanOneComponent.size());
    for (ReconstructionRegion region : _regionsDripAffectedByMoreThanOneComponent)
    {
      individualRegionsToScan = new ArrayList<ReconstructionRegion>(1);
      individualRegionsToScan.add(region);
      HomogeneousImageGroup imageGroup = new HomogeneousImageGroup(individualRegionsToScan);
      groups.add(imageGroup);
    }
    if (_debug)
      TimerUtil.printCurrentTime("      End _regionsDripAffectedByMoreThanOneComponent loop");

    return groups;
  }

  /**
   * @author Roy Williams
   */
  private void add(List<HomogeneousImageGroup> groups, Map<String, List<ReconstructionRegion>> map)
  {
    for (Map.Entry<String, List<ReconstructionRegion>> groupEntry : map.entrySet())
    {
      List<ReconstructionRegion> regionsToScan = groupEntry.getValue();
      Assert.expect(regionsToScan.size() > 0);
      HomogeneousImageGroup imageGroup = new HomogeneousImageGroup(regionsToScan);

      // Add an output line for UnitTesting because
      if (_debug)
      {
        System.out.println("ImageGroup size: " + regionsToScan.size());
//      // Print the constituents of the group so we can look at them as a pack.
//      Test_ImagingChainProgramGenerator.printComponentData(groupEntry.getKey(), imageGroup);
      }
      groups.add(imageGroup);
    }
  }

  /**
   * Creates and ordered list of Components based upon an implementation of
   * @author Roy Williams
   */
  private void createMapsOfReconstructionRegionsWithSameSourceComponent(Collection<ReconstructionRegion> unSortedRegions)
  {
    // Here is how we are going to create an ordered list.
    Comparator<String> comparator = new Comparator<String>()
    {
      public int compare(String componentName1, String componentName2)
      {
        Assert.expect(componentName1 != null);
        Assert.expect(componentName2 != null);
        return componentName1.compareTo(componentName2);
      }
    };

    // Now we have a comparator, we can create a TreeMap to create order with our comparator.
    _regionsWithSameDripSourceComponent = new TreeMap<String, List<ReconstructionRegion>>(comparator);
    _regionsFromSameSignalCompensatedComponent = new TreeMap<String, List<ReconstructionRegion>>(comparator);
    _regionsWithSameSignalCompensation = new HashMap<String,List<ReconstructionRegion>>();

    // Do the groupings.
    List<ReconstructionRegion> list = null;
    String componentBoardNameAndReferenceDesignator = null;
    for (ReconstructionRegion region : unSortedRegions)
    {
      componentBoardNameAndReferenceDesignator = null;
      
      // Chnee Khang Wah, 2013-04-23
      // for new Entropy based IC 
      // XCR-3137 Kee Chin Seong - System crash when run Virtual Live
      if(region.hasVirtualLiveSetting() == false)
      {
        if(region.getBoard().getPanel().getProject().isGenerateByNewScanRoute())
        {
            String il = region.getSignalCompensation().toString();
            list = _regionsWithSameSignalCompensation.get(il);
            if (list == null)
            {
              list = new ArrayList<ReconstructionRegion>();
              _regionsWithSameSignalCompensation.put(il, list);
            }
            list.add(region);
            continue;
        }
      }
      
      if (region.hasNonDefaultSignalCompensation()) // IL = 2, 3, 4, 8....
      {
        if (region.hasVirtualLiveSetting() == false)
        {
          componentBoardNameAndReferenceDesignator = region.getComponent().getBoardNameAndReferenceDesignator();
        }
        else
        {
          componentBoardNameAndReferenceDesignator = "1_VirtualLive";
        }
        list = _regionsFromSameSignalCompensatedComponent.get(componentBoardNameAndReferenceDesignator);
        if (list == null)
        {
          list = new ArrayList<ReconstructionRegion>();
          _regionsFromSameSignalCompensatedComponent.put(componentBoardNameAndReferenceDesignator, list);
        }
        list.add(region);
        continue;
      }

      // See if this component is drip affected by 0, 1, or more than one part.
      List<Component> dripSourceComponents = region.getComponentsThatCauseInterferencePattern();
      if (dripSourceComponents.size() == 0)
      {
        _regionsDripAffectedByZeroComponents.add(region);
        continue;
      }

      if (dripSourceComponents.size() > 1)
      {
        _regionsDripAffectedByMoreThanOneComponent.add(region);
        continue;
      }

      // Use the one (and only) component to cluster this region.
      componentBoardNameAndReferenceDesignator = dripSourceComponents.get(0).getBoardNameAndReferenceDesignator();
      list = _regionsWithSameDripSourceComponent.get(componentBoardNameAndReferenceDesignator);
      if (list == null)
      {
        list = new ArrayList<ReconstructionRegion>();
        _regionsWithSameDripSourceComponent.put(componentBoardNameAndReferenceDesignator, list);
      }
      list.add(region);
    }
  }

  /**
   * @author Roy Williams
   */
  public static void setDebug(boolean state)
  {
    _debug = state;
  }
}
