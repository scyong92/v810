package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.util.*;
import com.axi.v810.hardware.AbstractXrayCamera;

/**
 * @author Roy Williams
 */
public class CollectImagesFromCamerasRunnable implements Runnable
{
  private ProjectionEngine _projectionEngine;
  private List<ProjectionScanPass> _projectionScanPath;
  private int _scanPassNumber = -1;

  private static ExecuteParallelThreadTasks<Image> _collectionThreadTasks;

  /**
   * @author Roy Williams
   */
  static
  {
    _collectionThreadTasks = new ExecuteParallelThreadTasks<Image>();
  }

  /**
   * @author Roy Williams
   */
  public CollectImagesFromCamerasRunnable(ProjectionEngine projectionEngine,
                                          List<ProjectionScanPass> projectionScanPath,
                                          int scanPassNumber)
  {
    Assert.expect(projectionEngine!= null);
    Assert.expect(projectionScanPath != null);
    Assert.expect(scanPassNumber >= 0);

    _projectionEngine = projectionEngine;
    _projectionScanPath = projectionScanPath;
    _scanPassNumber = scanPassNumber;

  }


  /**
   * @author Roy Williams
   */
  public void run()
  {
    try
    {
      ProjectionScanPass projectionScanPass = _projectionScanPath.get(_scanPassNumber);

      List<ProjectionRequestThreadTask> threadTasks = projectionScanPass.getProjectionRequestsToBeRun();
      List<ProjectionRequestThreadTask> clearCameraTasks = projectionScanPass.projectionRequestsToClearCameras();
      for (ProjectionRequestThreadTask projectionRequest : threadTasks)
      {
        projectionRequest.scanPassNumber(_scanPassNumber);
        _collectionThreadTasks.submitThreadTask(projectionRequest);
      }

      // Wait for all of the images!
      _projectionEngine.setAllCamerasCollectorsLaunched();
      _collectionThreadTasks.waitForTasksToComplete();
    }
    catch (XrayTesterException ex)
    {
      _projectionEngine.getProjectedImagesProducer().abort(ex);
    }
    catch (Throwable ex)
    {
      Assert.logException(ex);
    }
    finally
    {
      _projectionEngine.incrementRunnableScanPass();
    }
  }

  /**
   * @author Roy Williams
   */
  public static void abort()
  {
    try
    {
      _collectionThreadTasks.cancel();
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }
  }
}
