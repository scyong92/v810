package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class RescanShadedRegionsBasedUponMaxCandidateStepSizeScanStrategy extends RescanStrategy
{
  public RescanShadedRegionsBasedUponMaxCandidateStepSizeScanStrategy()
  {
    super(ScanStrategyEnum.GROUP_ON_MAX_CANDIDATE_STEP_SIZE);
  }

  /**
   * @author Roy Williams
   */
  public List<HomogeneousImageGroup> createGroups(
      Collection<ReconstructionRegion> unSortedRegions) throws XrayTesterException
  {
    // Group them first based upon maxCandidateStepSize for each region.
    Map<Integer, List<ReconstructionRegion>> regionsOfSameStepSize = sortReconstructionRegionsWithLikeMaxStepSize(unSortedRegions);

    // Create a sorted list for each group.
    // AND, convert from the max step size to mean step size so we can dither the path.
    List<HomogeneousImageGroup> groups = new ArrayList<HomogeneousImageGroup>();
    for (Map.Entry<Integer, List<ReconstructionRegion>> groupEntry : regionsOfSameStepSize.entrySet())
    {
      int maxScanStepSizeForRegion = groupEntry.getKey();
      List<ReconstructionRegion> regionsToScan = groupEntry.getValue();

      // Since they all agreed to the same scan step size, we should expect they
      // will compute the same value (again).
      HomogeneousImageGroup imageGroup = new HomogeneousImageGroup(regionsToScan, maxScanStepSizeForRegion);
      groups.add(imageGroup);

      // Print the constituents of the group so we can look at them as a pack.
      List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionPairs =
          imageGroup.getRegionPairsSortedInExecutionOrder();
      System.out.println(regionPairs.size() +
                         " ReconstructionRegions with scan step size: " +
                         maxScanStepSizeForRegion + " nM / " +
                         maxScanStepSizeForRegion/25400  + " mils");
      for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : regionPairs)
      {
        System.out.println("   " + regionPair.getFirst().getRegionId());
      }
    }
    return groups;
  }


  /**
   * It is my job to compress and/or merge groups.  However, there is no compression
   * done by this class.  Create a new strategy like this one to do otherwise.
   *
   * @author Roy Williams
   */
  public List<HomogeneousImageGroup> mergeGroups(List<HomogeneousImageGroup> currentGroups)
  {
    return currentGroups;
  };

  /**
   * @author Roy Williams
   */
  protected static Map<Integer, List<ReconstructionRegion>>
      sortReconstructionRegionsWithLikeMaxStepSize(Collection<ReconstructionRegion> unSortedRegions)
  {
    Map<Integer, List<ReconstructionRegion>> regionsOfSameStepSize =
        new HashMap<Integer, List<ReconstructionRegion>>();
    List<ReconstructionRegion> list = null;
    for (ReconstructionRegion region : unSortedRegions)
    {
      int maxCandidateStepSize = calculateMaxCandidateStepSize(region);
      list = regionsOfSameStepSize.get(maxCandidateStepSize);
      if (list == null)
      {
        list = new ArrayList<ReconstructionRegion>();
        regionsOfSameStepSize.put(maxCandidateStepSize, list);
      }
      list.add(region);
    }
    return regionsOfSameStepSize;
  }

}
