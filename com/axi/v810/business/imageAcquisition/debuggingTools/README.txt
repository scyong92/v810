This directory contains a set of files used to analyze whether a 
ReconstructionRegion (and its neighbors) is imaged on a given IRP.
This is needed to figure out the problem when an IRP throws an 
exception to complain that a "Region is not imaged by all cameras".
It will tell you which reconstruction region number is offended.


1) In business/imageAcquisition/ImagingChainProgramGenerator.java,
   uncomment call site for function and insert the number...

      printScanPassesForRegionAndIdentifyImageGroup(4155);


2) Place the project under directory testRel/unitTest/projects.


3) Change the project being analyzed by the Test_ImagingChainProgramGenerator
   to conform to the project identified in step #2 (above).


4) Run the unit test.   Capture only those lines in the output window 
   with the following format and put them into a text file...

	Region from program definition ( Panel Coordinate Nm )
	Origin (7.65831e+007, 2.94955e+008) Width: 4.24179e+006 Length: 2.6416e+006 
	Aligned Region (System Fiducial Nm)
	Origin (-8.99138e+007, 2.37918e+008)  Width: 2.64731e+006 Length: 4.24535e+006 
	Region expected to lie on projection(0, 5) origin(-6818, 4660) Width: 1670 Length:266  origin(0, 4251) Width: 1088 Length:3081
	Region expected to lie on projection(1, 5) origin(-8254, 4590) Width: 1677 Length:406  origin(0, 4251) Width: 1088 Length:3230
	Region expected to lie on projection(2, 5) origin(-7733, 4787) Width: 1598 Length:570  origin(0, 4251) Width: 1088 Length:3408
	Region expected to lie on projection(3, 5) origin(-6994, 4053) Width: 1657 Length:644  origin(0, 4251) Width: 1088 Length:3489
	Region expected to lie on projection(4, 5) origin(-5549, 5281) Width: 1448 Length:495  origin(0, 4251) Width: 1088 Length:3327
	Region expected to lie on projection(5, 5) origin(-6113, 4050) Width: 1779 Length:584  origin(0, 4251) Width: 1088 Length:3423
	Region expected to lie on projection(6, 5) origin(-5933, 4284) Width: 1775 Length:441  origin(0, 4251) Width: 1088 Length:3268
	Region expected to lie on projection(7, 5) origin(-5341, 3550) Width: 1537 Length:381  origin(0, 4251) Width: 1088 Length:3202
	Region expected to lie on projection(8, 5) origin(-5580, 3875) Width: 1712 Length:389  origin(0, 4251) Width: 1088 Length:3212
	Region expected to lie on projection(9, 5) origin(-6682, 4465) Width: 1841 Length:597  origin(0, 4251) Width: 1088 Length:3437
	Region expected to lie on projection(10, 5) origin(-7061, 4702) Width: 1759 Length:503  origin(0, 4251) Width: 1088 Length:3335
	Region expected to lie on projection(11, 5) origin(-8056, 4900) Width: 1691 Length:635  origin(0, 4251) Width: 1088 Length:3479
	Region expected to lie on projection(12, 5) origin(-8294, 4675) Width: 1815 Length:442  origin(0, 4251) Width: 1088 Length:3269
	Region expected to lie on projection(13, 5) origin(-7629, 4548) Width: 1641 Length:353  origin(0, 4251) Width: 1088 Length:3173

5) Run the splitFields.sh scrip on the file captured in step 4 (above).

	0 5 -4546 12089 1155 160 0 11181 1088 3671
	1 5 -5954 12092 1156 300 0 11181 1088 3819
	2 5 -5820 12096 1156 463 0 11181 1088 3997
	3 5 -4791 12099 1155 538 0 11181 1088 4078
	4 5 -4360 12094 1155 389 0 11181 1088 3916
	5 5 -3314 12096 1154 478 0 11181 1088 4013
	6 5 -3149 12095 1154 335 0 11181 1088 3857
	7 5 -3715 12089 1155 275 0 11181 1088 3792
	8 5 -3104 12098 1154 283 0 11181 1088 3801
	9 5 -3580 12109 1154 490 0 11181 1088 4026
	10 5 -4362 12106 1155 396 0 11181 1088 3924
	11 5 -5687 12116 1155 529 0 11181 1088 4068
	12 5 -5327 12101 1156 336 0 11181 1088 3859
	13 5 -5500 12096 1155 248 0 11181 1088 3763

6) import the result file into an Excell spreadsheet using the column 
   definitions as found in Projections.xls.   This will show you how
   far off the IAE was in its calculations.

7) A second spreadsheet called ProcessorStrips is provided to plot out
   the physical locations of the region to be image.  Previous bugs 
   have demonstrated that this falls on IRP (i.e. ProcessorStrip) boundries.


