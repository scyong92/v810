
cat $1 |
sed 's#[\\(\\)\\,\\:]# #g' |
awk '
  /^Region/ {
    printf("%s %s %s %s %s %s %s %s %s %s\n", $7, $8, $10, $11, $13, $15, $17, $18, $20, $22);
  }
' | grep -v ^Nm > `basename $1 .txt`.split.txt

