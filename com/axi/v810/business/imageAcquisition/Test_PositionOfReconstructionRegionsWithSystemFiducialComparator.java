package com.axi.v810.business.imageAcquisition;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class Test_PositionOfReconstructionRegionsWithSystemFiducialComparator extends UnitTest
{

  /**
   * @author Roy Williams
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    PositionOfReconstructionRegionsWithSystemFiducialComparator comparator =
      new PositionOfReconstructionRegionsWithSystemFiducialComparator(true, true);

    ReconstructionRegion rr1 = new ReconstructionRegion();
    rr1.setRegionId(1);
    SystemFiducialRectangle sfr1 = new SystemFiducialRectangle(10, 0, 20, 20);
    Pair<ReconstructionRegion, SystemFiducialRectangle> pair1 = new Pair<ReconstructionRegion,SystemFiducialRectangle>(rr1, sfr1);

    ReconstructionRegion rr2 = new ReconstructionRegion();
    rr2.setRegionId(2);
    SystemFiducialRectangle sfr2 = new SystemFiducialRectangle(10, 0, 30, 20);
    Pair<ReconstructionRegion, SystemFiducialRectangle> pair2 = new Pair<ReconstructionRegion,SystemFiducialRectangle>(rr2, sfr2);

    ReconstructionRegion rr3 = new ReconstructionRegion();
    rr3.setRegionId(3);
    SystemFiducialRectangle sfr3 = new SystemFiducialRectangle(10, 0, 40, 20);
    Pair<ReconstructionRegion, SystemFiducialRectangle> pair3 = new Pair<ReconstructionRegion,SystemFiducialRectangle>(rr3, sfr3);

    // Create a "scrambled list".
    List<Pair<ReconstructionRegion, SystemFiducialRectangle>> list = new ArrayList<Pair<ReconstructionRegion,SystemFiducialRectangle>>();
    list.add(pair2);
    list.add(pair3);
    list.add(pair1);

    // Before
    for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : list)
    {
      System.out.println(regionPair.getFirst().getRegionId());
    }

    // Sort them.
    Collections.sort(list, comparator);
    System.out.println("================");

    // After
    int actualPositionOrder = 3;
    for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : list)
    {
      System.out.println(regionPair.getFirst().getRegionId());
      Expect.expect(regionPair.getFirst().getRegionId() == actualPositionOrder);
      actualPositionOrder--;
    }
  }


  /**
   * @author Roy Williams
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_PositionOfReconstructionRegionsWithSystemFiducialComparator() );
  }
}
