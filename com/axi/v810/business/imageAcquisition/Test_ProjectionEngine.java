package com.axi.v810.business.imageAcquisition;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Test class for the ProjectionEngine.
 *
 * @author Roy Williams
 */
public class Test_ProjectionEngine extends UnitTest
{
  private ImageAcquisitionEngine  _imageAcquisitionEngine  = null;
  private HardwareWorkerThread    _hardwareWorkerThread    = null;
  private SystemFiducialRectangle _systemFiducialRectangle = new SystemFiducialRectangle( -3810000, -2540000, 7620000, XrayCameraArray.getOneInchInNanometersAlignedUpToPixelBoundary());
  private static ProjectionRequestThreadTaskCameraIdComparator _cameraOrderComparator;
  private static ProjectionRequestThreadTaskSystemFiducialImagingOrderComparator _systemFiducialImagingOrderComparator;
  private ProjectionRequestThreadTaskHysterisisImagingOrderComparator  _hysterisisImagingOrderComparator;

  /**
   * @author Roy Williams
   */
  private Test_ProjectionEngine()
  {
    super();

    // Create a set of comparators to be used below.
    _cameraOrderComparator                = new ProjectionRequestThreadTaskCameraIdComparator();
    _systemFiducialImagingOrderComparator = new ProjectionRequestThreadTaskSystemFiducialImagingOrderComparator();
    _hysterisisImagingOrderComparator     = new ProjectionRequestThreadTaskHysterisisImagingOrderComparator();

    // Start the hardware running if (and only if) we are not in simulation mode.
    _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  }

  /**
   * @author Roy Williams
   */
  public void test(final BufferedReader in, final PrintWriter out)
  {
     try
    {
      _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
      {
        public void run() throws Exception
        {
          testOnHardwareWorkerThread(in, out);
        }
      });
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Roy Williams
   */
  private void testOnHardwareWorkerThread(BufferedReader in, PrintWriter out )
  {

    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.PROJECTION_PRODUCER_DEBUG))
      ImageAcquisitionEngine.setDebug(true);

    _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
    XrayCameraArray xRayCameraArray = null;
    boolean useCameraSimulator = false;

    try
    {
      if (useCameraSimulator)
      {
        String imageDataTestDirectory = getTestDataDir() + "\\needToDefineSubDirectory";
        xRayCameraArray = XrayCameraArray.getInstance();
        xRayCameraArray.enableUnitTestMode(imageDataTestDirectory);
      }

      XrayTester.getInstance().startup();

      // Test Xray Spot
      testXraySpot();

      // Test System Fiducial
      testSystemFiducialWhenScanDirectionsNotSpecified();

      // Test System Fiducial
      testSystemFiducialWhenScanDiretionAlwaysUP();

      // Test Hysteresis
      testHysteresis();
    }
    catch (AssertException aex)
    {
      aex.printStackTrace();
      // Shouldn't happen...
      Assert.expect(false);
    }

    catch ( XrayTesterException hex )
    {
      hex.printStackTrace();

      // Shouldn't occur.
      Assert.expect( false );
    }
    catch (Exception e)
    {
      e.printStackTrace();

      Assert.expect(false, e.getMessage());
    }
    finally
    {
      if (useCameraSimulator)
      {
        xRayCameraArray.disableUnitTestMode();
      }
    }
  }

  /**
   * @author Greg Esparza
   */
  private void cleanUpProjectionRequests(List<ProjectionRequestThreadTask> projectionRequests)
  {
    Assert.expect(projectionRequests != null);

    for (ProjectionRequestThreadTask projectionRequest : projectionRequests)
      projectionRequest.clear();
  }

  /**
   * @author Roy Williams
   */
  private void testXraySpot() throws XrayTesterException
  {
    String testName = "XraySpotTest";
    ProjectionRequestThreadTask projectionRequest = new ProjectionRequestThreadTask(XrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0), ScanPassDirectionEnum.FORWARD, _systemFiducialRectangle);
    List<ProjectionRequestThreadTask> projectionRequests = new ArrayList<ProjectionRequestThreadTask>();
    projectionRequests.add(projectionRequest);
    TestExecutionTimer testExecutionTimer = new TestExecutionTimer();
    _imageAcquisitionEngine.acquireProjections(projectionRequests, testExecutionTimer, true);

    saveImageResults(testName, projectionRequests);

    // The printResults function will Assert if the comparator is null.  It is silly
    // implementa a function/class to sort a list of size one.   So, any old comparator
    // will do.
    Comparator<ProjectionRequestThreadTask> comparatorToSortOneProjectionRequest = _hysterisisImagingOrderComparator;
    printResults(testName, projectionRequests, comparatorToSortOneProjectionRequest);
    cleanUpProjectionRequests(projectionRequests);
  }

  /**
   * @author Roy Williams
   */
  private void testHysteresis() throws XrayTesterException
  {
    String testName = "HysteresisTest";
    List<ProjectionRequestThreadTask> projectionRequests = new ArrayList<ProjectionRequestThreadTask>();
    for (AbstractXrayCamera camera : XrayCameraArray.getCameras())
    {
      projectionRequests.add(new ProjectionRequestThreadTask(camera, ScanPassDirectionEnum.FORWARD, _systemFiducialRectangle));
    }
    List<ProjectionScanPass> projectionScanPath =
      _imageAcquisitionEngine.getProjectedImagesProducer().generateScanPath(projectionRequests);

    List<ProjectionRequestThreadTask> hysterisisProjectionRequests = new ArrayList<ProjectionRequestThreadTask>();
    for (ProjectionScanPass projectionScanPass : projectionScanPath)
    {
      for (ProjectionRequestThreadTask projectionRequest : projectionScanPass.getProjectionRequestsToBeRun())
      {
        hysterisisProjectionRequests.add(projectionRequest);
      }
      for (ProjectionRequestThreadTask projectionRequest : projectionScanPass.getProjectionRequestsToBeRun())
      {
        hysterisisProjectionRequests.add(new ProjectionRequestThreadTask(projectionRequest.getCamera(),
                                                                         ScanPassDirectionEnum.REVERSE,
                                                                         _systemFiducialRectangle));
      }
    }
    TestExecutionTimer testExecutionTimer = new TestExecutionTimer();
    _imageAcquisitionEngine.acquireProjections(hysterisisProjectionRequests, testExecutionTimer, false);

    saveImageResults(testName, hysterisisProjectionRequests);
    // It really doesn't matter what comparator we use here since there is only one ProjectionRequest
    printResults(testName, hysterisisProjectionRequests, _hysterisisImagingOrderComparator);
    cleanUpProjectionRequests(projectionRequests);
  }

  /**
   * Insists upon an UP scan for each scanPass.
   *
   * @author Roy Williams
   */
  private void testSystemFiducialWhenScanDiretionAlwaysUP() throws XrayTesterException
  {
    String testName = "SystemFiducialTestWhenScanDiretionAlwaysUP";
    List<ProjectionRequestThreadTask> projectionRequests = new ArrayList<ProjectionRequestThreadTask>();
    for (AbstractXrayCamera camera : XrayCameraArray.getCameras())
    {
      projectionRequests.add(new ProjectionRequestThreadTask(camera, ScanPassDirectionEnum.FORWARD, _systemFiducialRectangle));
    }
    TestExecutionTimer testExecutionTimer = new TestExecutionTimer();
    _imageAcquisitionEngine.acquireProjections(projectionRequests, testExecutionTimer, true);

    saveImageResults(testName, projectionRequests);
    printResults(testName + " (usage order)",  projectionRequests, _systemFiducialImagingOrderComparator);
    printResults(testName + " (camera order)", projectionRequests, _cameraOrderComparator);
    cleanUpProjectionRequests(projectionRequests);
  }

  /**
   * Let's the IAE choose the direction of scan.
   *
   * @author Roy Williams
   */
  private void testSystemFiducialWhenScanDirectionsNotSpecified() throws XrayTesterException
  {
    String testName = "SystemFiducialTestWhenScanDirectionsNotSpecified";

    List<ProjectionRequestThreadTask> projectionRequests = new ArrayList<ProjectionRequestThreadTask>();
    for (AbstractXrayCamera camera : XrayCameraArray.getCameras())
    {
      projectionRequests.add(new ProjectionRequestThreadTask(camera, ScanPassDirectionEnum.BIDIRECTION, _systemFiducialRectangle));
    }
    TestExecutionTimer testExecutionTimer = new TestExecutionTimer();
    _imageAcquisitionEngine.acquireProjections(projectionRequests, testExecutionTimer, true);

    saveImageResults(testName, projectionRequests);
    printResults(testName + " (usage order)",  projectionRequests, _systemFiducialImagingOrderComparator);
    printResults(testName + " (camera order)", projectionRequests, _cameraOrderComparator);
    cleanUpProjectionRequests(projectionRequests);
  }

  /**
   * @author Roy Williams
   */
  private void printResults(String testName,
                            List<ProjectionRequestThreadTask> projectionRequests,
                            Comparator<ProjectionRequestThreadTask> comparator)
  {
    Assert.expect(testName != null);
    Assert.expect(projectionRequests != null);
    Assert.expect(comparator != null);

    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (!Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.PROJECTION_PRODUCER_DEBUG))
      return;

    System.out.println();
    System.out.println(testName);
    System.out.println(ProjectionRequestThreadTask.toStringOrder());
    if (comparator != null)
      Collections.sort(projectionRequests, comparator);
    for (ProjectionRequestThreadTask projectionRequest : projectionRequests)
    {
      System.out.println(projectionRequest.toString());
    }
    System.out.println();
  }


  /**
   * @author Roy Williams
   */
  static public void saveImageResults(String testName, List<ProjectionRequestThreadTask> projectionRequests)
  {
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (!Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.PROJECTION_PRODUCER_DEBUG))
      return;

    for (ProjectionRequestThreadTask projectionRequest : projectionRequests)
    {
      ProjectionInfo projectionInfo = projectionRequest.getProjectionInfo();
      try
      {
        AbstractXrayCamera camera = projectionRequest.getCamera();
        String directory = "c:/temp/cameras/" + testName;
        File directoryFile = new File(directory);
        directoryFile.mkdirs();   // make directories if they do not exist.
        XrayImageIoUtil.saveJpegImage(projectionInfo.getProjectionImage().getBufferedImage(),
                                      directory + File.separator +
                                      "camera" + camera.getId() + "." +
                                      projectionRequest.getStageDirection().toString() +
                                      ".jpg");
        System.out.println("camera: " + camera.getId() + " image acquired");
        projectionRequests.clear();
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   * @author Roy Williams
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_ProjectionEngine() );
  }
}
