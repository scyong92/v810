package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;

/**
 * @author sheng-chuan.yong
 */
public class ScanPassSetting
{
  private double _stageSpeed = 1;
  private double _userGain = 1;
  private boolean _isLowMagnification = true;
  
  /**
   * @author Yong Sheng Chuan
   */
  public ScanPassSetting()
  {
    //do nothing
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public ScanPassSetting(double stageSpeedEnum, double userGain, boolean isLowMagnification)
  {
    Assert.expect(stageSpeedEnum > 0);
    Assert.expect(userGain > 0);
    
    _stageSpeed = stageSpeedEnum;
    _userGain = userGain;
    _isLowMagnification = isLowMagnification;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setStageSpeedValue(double stageSpeedVal)
  {
    Assert.expect(stageSpeedVal > 0);
    _stageSpeed = stageSpeedVal;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setUserGainValue(double userGain)
  {
    Assert.expect(userGain > 0);
    _userGain = userGain;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setIsLowMagnification(boolean isLowMagnification)
  {
    _isLowMagnification = isLowMagnification;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public double getUserGainValue()
  {
    return _userGain;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public double getStageSpeedValue()
  {
    return _stageSpeed;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public boolean isLowMagnification()
  {
    return _isLowMagnification;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public boolean equals(Object obj)
  {
    ScanPassSetting scanPassSetting = (ScanPassSetting)obj;
    if (getUserGainValue() == scanPassSetting.getUserGainValue() &&
        getStageSpeedValue() == scanPassSetting.getStageSpeedValue() &&
        isLowMagnification() == scanPassSetting.isLowMagnification())
      return true;
    else 
      return false;
  }
}
