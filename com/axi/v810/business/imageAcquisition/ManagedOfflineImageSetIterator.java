package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;

/**
 * This is a custom iterator for the ManagedOfflineImageSet class.
 *
 * @author Peter Esbensen
 * @author Patrick Lacz
 */
public class ManagedOfflineImageSetIterator
{
  private ManagedOfflineImageSet _managedOfflineImageSet;
  private int _samplingFrequency = 1;
  private int _numberOfRegionsPassed = 0;

  private Collection<ImageSetData> _imageSets;

  private Iterator<ImageSetData> _imageSetIterator;
  private Iterator<ReconstructionRegion> _reconstructionRegionIterator;

  private ImageSetData _currentImageSet;
  private ReconstructionRegion _currentReconstructionRegion;

  private boolean _haveCalledFinishedWithRegion = true;

  private static ImageManager _imageManager = ImageManager.getInstance();

  /**
   * @author Peter Esbensen
   */
  public ManagedOfflineImageSetIterator(ManagedOfflineImageSet managedOfflineImageSet,
                                        Collection<ImageSetData> imageSets,
                                        Collection<ReconstructionRegion> reconstructionRegions)
  {
    Assert.expect(managedOfflineImageSet != null);
    Assert.expect(imageSets != null);
    Assert.expect(reconstructionRegions != null);

    _managedOfflineImageSet = managedOfflineImageSet;
    _imageSets = imageSets;

    _imageSetIterator = _imageSets.iterator();
    _reconstructionRegionIterator = null;
  }

  /**
   * @author Patrick Lacz
   */
  private void advanceToNextImageSet()
  {
    Assert.expect(_imageSetIterator != null);
    if (_imageSetIterator.hasNext() == false)
    {
      _currentImageSet = null;
      _reconstructionRegionIterator = null;
    }
    else
    {
      _currentImageSet = _imageSetIterator.next();
      _reconstructionRegionIterator = _managedOfflineImageSet.getRegionsForImageSet(_currentImageSet).iterator();
    }
  }

  /**
   * This method may return null if there are no more images to load.
   * (this might not easily predicted due to sampling or partial image sets or some other reason)
   *
   * @author Patrick Lacz
   */
  public ReconstructedImages getNext() throws DatastoreException
  {
    // test to verify that the user is correctly using 'finishedWithCurrentRegion' so that we can safely remove that set of
    // images from memory if necessary.
    Assert.expect(_haveCalledFinishedWithRegion);
    _haveCalledFinishedWithRegion = false;

    // have we started yet? If not, begin with the first image set.
    if (_currentImageSet == null)
    {
      advanceToNextImageSet();
      // if there are not any image sets, just return null
      if (_currentImageSet == null)
        return null;
    }

    ReconstructedImages reconstructedImages = null;
    boolean finishedWithAllImages = false;
    do
    {
      // return every _samplyingFrequency'th image. To return every image, set _samplingFrequency to 1 (the default)
      boolean returnThisImage = _numberOfRegionsPassed % _samplingFrequency == 0;
      if (_reconstructionRegionIterator.hasNext())
      {
        // simply increment the region
        _currentReconstructionRegion = _reconstructionRegionIterator.next();

        _numberOfRegionsPassed++;

        // only try to load the images if we're going to return them.
        if (returnThisImage)
            reconstructedImages = _managedOfflineImageSet.getReconstructedImages(_currentImageSet, _currentReconstructionRegion);
      }
      else
      {
        // increment the image set
        advanceToNextImageSet();
        if (_currentImageSet == null)
          finishedWithAllImages = true;
      }
    } while (reconstructedImages == null &&  finishedWithAllImages == false);
    return reconstructedImages;
  }

  /**
   * This method may return null if there are no more regions to visit.
   * (this might not easily predicted due to sampling or partial image sets or some other reason)
   *
   * @author Patrick Lacz
   */
  public ReconstructionRegion getNextRegionWithoutLoadingImages()
  {
    Assert.expect(_haveCalledFinishedWithRegion);
    _haveCalledFinishedWithRegion = false;
    _currentReconstructionRegion = null;

    if (_currentImageSet == null)
    {
      advanceToNextImageSet();
      if (_currentImageSet == null)
        return null;
    }

    boolean returnThisImage = false;
    do
    {
      returnThisImage = _numberOfRegionsPassed % _samplingFrequency == 0;
      if (_reconstructionRegionIterator.hasNext())
      {
        // simply increment the region
        _currentReconstructionRegion = _reconstructionRegionIterator.next();
        _numberOfRegionsPassed++;
      }
      else
      {
        returnThisImage = false; // we can not return the image - we need to go on to the next image set.
        // increment the image set
        advanceToNextImageSet();
        if (_currentImageSet == null)
          break; // done with all the image sets ; there is nothing to go on to.
      }
    } while (returnThisImage == false);
    return _currentReconstructionRegion;
  }

  /**
   * @author Patrick Lacz
   */
  public ImageSetData getCurrentImageSetData()
  {
    Assert.expect(_currentImageSet != null);
    return _currentImageSet;
  }

  /**
   * @author Patrick Lacz
   */
  public ReconstructionRegion getCurrentReconstructionRegion()
  {
    Assert.expect(_currentReconstructionRegion != null);
    return _currentReconstructionRegion;
  }

  /**
   * @author Patrick Lacz
   */
  public void finishedWithCurrentRegion()
  {
    Assert.expect(_currentImageSet != null);
    Assert.expect(_currentReconstructionRegion != null);

    _haveCalledFinishedWithRegion = true;
    _managedOfflineImageSet.finishedWithReconstructionRegion(_currentImageSet, _currentReconstructionRegion);
  }

  /**
   * Sets the iterator to return every <i>n</i>th region.
   * Default is 1 where it returns every region. (2 would return every other region, 3 every third region, and so on.)
   * @author Patrick Lacz
   */
  public void setSamplingFrequency(int frequency)
  {
    Assert.expect(frequency > 0);
    _samplingFrequency = frequency;
  }
}
