package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

/**
 * All rescan strategies should be derived from this class.
 * Rescan strategies take a list of reconstruction regions which require
 * a rescan and create image groups for those regions.
 *
 * @author Roy Williams
 * @author Kay Lannen
 */

abstract class RescanStrategy extends ScanStrategy
{
  protected List<HomogeneousImageGroup> _groups;

  /**
   * @author Kay Lannen
   */
  protected RescanStrategy(ScanStrategyEnum scanStrategy)
  {
    super(scanStrategy);
  }

  /**
   * Strategy pattern involves asking the instance to create its basic groups.
   * then an iteration will be done on those groups till they do not shrink in
   * size anymore.   The resulting compressed list will be put into execution
   * order then returned.
   * <p>
   * This method is marked as final because the methods below should never override
   * this strategy pattern (see pattern design books).
   *
   * @author Roy Williams
   */
  public final List<HomogeneousImageGroup> arrangeGroupsInExecutionOrder(Collection<ReconstructionRegion> regions) throws XrayTesterException
  {
    Assert.expect(regions != null);

    if (_debug)
      TimerUtil.printCurrentTime("    Start createGroups size:"+regions.size());
    _groups = createGroups(regions);
    if (_debug)
      TimerUtil.printCurrentTime("    End createGroups groupsize:"+_groups.size());
    int resolutionSize;
    // Iterate over the groups at least one time.
    //
    // Check to see if the size of the group has changed.   If it did, then it compressed
    // more than the previous iteration.   This would imply that it could potentially
    // compress some more.   So, do it again to see if we can get more compression.
    if (_debug)
      TimerUtil.printCurrentTime("    Start merging while loop");
    int groupSize = 1;
    do
    {
      resolutionSize = _groups.size();
      if (_debug)
        TimerUtil.printCurrentTime("      Start merging");
      _groups = mergeGroups(_groups);
      groupSize = _groups.size();
      if (_debug)
        System.out.println("      Resolving merge: " + resolutionSize + " to " + groupSize);
      if (_debug)
        TimerUtil.printCurrentTime("      End merging");
      if (groupSize == 1)
      {
        break;
      }
    }
    while (resolutionSize != groupSize);
    if (_debug)
      TimerUtil.printCurrentTime("    End merging while loop");

    // Sort the groups returned so they are executed in order of appearance.
    PositionOfImageGroupsOnXaxisComparator comparator = new PositionOfImageGroupsOnXaxisComparator(true);
    Collections.sort(_groups, comparator);
    return _groups;
  }

  /**
   * @author Roy Williams
   */
  protected List<HomogeneousImageGroup> compress(List<HomogeneousImageGroup> groups)
  {
    int groupSize = 1;
    int resolutionSize;
    do
    {
      resolutionSize = groups.size();
      groups = HomogeneousImageGroup.mergeGroupsBasedOnExecutionTimeEstimates(groups);
      groupSize = groups.size();
      if (groupSize == 1)
      {
        break;
      }
    }
    while (resolutionSize != groupSize);
    return groups;
  }

  /**
   * @author Roy Williams
   */
  public List<HomogeneousImageGroup> getGroups()
  {
    return _groups;
  }

  /**
   * @author Roy Williams
   */
  public double getEstimatedExecutionTime()
  {
    double executionTime = 0;
    for (HomogeneousImageGroup group : _groups)
    {
      double groupTime = group.estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSizeMethodB();
      executionTime = executionTime + groupTime;
    }
    return executionTime;
  }

  /**
   * @author Roy Williams
   */
  abstract public List<HomogeneousImageGroup> createGroups(Collection<ReconstructionRegion> unSortedRegions) throws XrayTesterException;

  /**
   * It is my job to compress and/or merge groups.  The resulting compressed groups
   * will be returned.
   * <p>
   * If no additional compression can be done, then just return the same groups
   * passed in.  This is the default behaviour.   A group returned of the same
   * size will terminate the inquiries for more compression.
   *
   * @author Roy Williams
   */
  abstract public List<HomogeneousImageGroup> mergeGroups(List<HomogeneousImageGroup> currentGroups);

  /**
   * @author Roy Williams
   */
  public static int calculateMaxCandidateStepSize(ReconstructionRegion region)
  {
    List<Pair<Integer, Integer>> candidateStepSizeRanges = region.getCandidateStepRanges().getRanges();
    if (candidateStepSizeRanges.size() == 0)
    {
      return -1;
    }

    int maxScanStepSizeForRegion = ImagingChainProgramGenerator.getMinStepSizeLimitInNanometers();
    int minScanStepSizeForRegion = ImagingChainProgramGenerator.getMaxStepSizeLimitInNanometers();

    int candidateStepRangeMin = 0;
    int candidateStepRangeMax = 0;
    for (Pair<Integer, Integer> candidateStepRange : candidateStepSizeRanges)
    {
      // Compare the current max with those we have already seen.
      candidateStepRangeMax = candidateStepRange.getSecond();
      if (candidateStepRangeMax > maxScanStepSizeForRegion)
      {
        maxScanStepSizeForRegion = candidateStepRangeMax;
      }

      // Compare the current min with those we have already seen.
      candidateStepRangeMin = candidateStepRange.getFirst();
      if (minScanStepSizeForRegion > candidateStepRangeMin)
      {
        minScanStepSizeForRegion = candidateStepRangeMin;
      }
    }
    return maxScanStepSizeForRegion;
  }

}
