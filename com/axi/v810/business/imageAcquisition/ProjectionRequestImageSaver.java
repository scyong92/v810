package com.axi.v810.business.imageAcquisition;

import com.axi.v810.util.XrayImageIoUtil;
import java.io.File;
import com.axi.util.image.Image;
import com.axi.v810.hardware.*;

/**
 * This class allows multiple running instances of ProjectionRequests to funnel
 * through a synchronized object to write an image to disk.
 *
 * @author Roy Williams
 */
public class ProjectionRequestImageSaver
{
  /**
   * @author Roy Williams
   */
  public synchronized void saveImageToDisk(AbstractXrayCamera camera,
                                           int scanPassNumber,
                                           String stageDirection,
                                           Image image) throws Exception
  {
    System.out.println("Getting image for pass: " + scanPassNumber + "      camera: " + camera.getId());
/** @todo wpd - Roy - never hard code a directory or drive letter.  All directories need to come from  Directory */
    String directory = "c:/temp/cameras/" + "pass" +
                       Integer.toString(scanPassNumber);
    File directoryFile = new File(directory);
    directoryFile.mkdirs();
    XrayImageIoUtil.saveJpegImage(image.getBufferedImage(),
                                  directory + File.separator + "camera" + camera.getId() + "." + stageDirection + ".jpg");
    System.out.println("camera: " + camera.getId() + " image acquired");
  }
}
