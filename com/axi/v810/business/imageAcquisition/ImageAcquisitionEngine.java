package com.axi.v810.business.imageAcquisition;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * <p>
 * This class is used to obtain X-Ray images on Printed Circuit Boards (PCB)
 * from a coordinated set of hardware objects for many different types of requests.
 * </p>
 * <p>
 * The consumers of these images are calibration, confirmation
 * and diagnostics, inspection, and alignment.  This list is not comprehensive.
 * These are intended to be representative samples from two distinct
 * acquisition models: projections and reconstructed images.
 * </p>
 * <p>
 * The first two are mostly interested in 2D projection images.  Calibration only
 * requires a 2D image to locate the x-ray spot, system fiducials, etc.  The offsets
 * are recorded in tables for later use.
 * </p>
 * <p>
 * Inspection is exclusively interested in <tt>ReconstructedImages</tt>.  Each
 * <tt>ReconstructedImage</tt> is composed of one or more slices (images) at different
 * Z heights.  There are a series of calls on the <tt> ImageAcqusitionEngine</tt>
 * to <i>acquire&lt;Purpose&gt;Images()</i> for each of these purposes.  These will
 * be discussed in more detail later.
 * </p>
 * <p>
 * Hardware is coordinated for acquisition of all images.  Many require a unique
 * program be downloaded into the device.
 * <dl>
 * <li>panel positioner is used to move PCB's in/out of system</li>
 * <li>digital IO opens and closes doors, clamps PCB in stage, etc.</li>
 * <li>stage moves the PCB over the eye of the cameras.</li>
 * <li>Cameras record the image as the stage moves.</li>
 * <li>ImageReconstructionProcessors - shift, add, and stitch images together and return them up to the ImageAcquisitionEngine. </li>
 * </dl>
 * <p>
 * All requests to the <tt>ImageAcqusitionEngine</tt> to "acquire images" are
 * <b>synchronous</b>.   When the call returns, all images requested must be accounted
 * for.  This closure can be accomplished by either marking the reconstruction
 * regions complete or aborting the request.
 * </p>
 * <p>
 * At anytime during the acquisition process, a caller may call the <i>abort(</i>)
 * function on the <tt>ImageAcquisitionEngine</tt>.  It must be called from a
 * different thread and all processing will terminate at the end of the current
 * scan pass.
 * </p>
 * <p>
 * <b><i><u>Projection images:</u></i></b>
 * </p>
 * <p>
 * <tt>ProjectionRequestThreadTasks</tt> are provided by the caller to identify the 2D area
 * to image.  As each area is acquired it will simply be attached to the
 * <tt>ProjectionInfo</tt>.  This is an attachment to the original request.
 * </p>
 * <pre>
 * &nbsp;&nbsp;public synchronized void acquireProjections(
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;List&lt;ProjectionRequest&gt;projectionRequests,
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;boolean optimizeProjectionRequestOrder)
 * </pre>
 * <p>
 * A <tt>ProjectionRequestThreadTask</tt> is a description of a 2D area to image relative
 * to the <tt>SystemFiducial</tt>.
 * </p>
 * <p>
 * Various implementations of this function exist to provide images using stage
 * motion or synthetic triggers.
 * </p>
 * <b><i><u>Production images:</u></i></b>
 * <p>
 * These are requests for <tt>ReconstructedImages</tt>.  As previously mentioned,
 * these are a set of images (one per slice).  In simple terms, one <tt>ReconstructedImage</tt>
 * is created per <tt>ReconstructionRegion</tt> identified in a <tt>TestProgram</tt>.
 * The reader is encouraged to read the <tt>TestProgram</tt> class diagram.
 * </p>
 * <p>
 * To create <tt>ReconstructedImages</tt> for each <tt>ReconstructionRegion</tt>, one
 * must call...
 * </p>
 * <pre>
 * &nbsp;&nbsp;public synchronized void acquireVerificationImages(TestProgram testProgram)
 * &nbsp;&nbsp;public synchronized void acquireAlignmentImages(TestProgram testProgram)
 * &nbsp;&nbsp;public synchronized void acquireProductionImages(TestProgram testProgram)
 * </pre>
 * where there are multiple types/purposes of requests for <tt>ReconstructedImages</tt>:
 * <dl>
 * <li>"Verification" used to render a confounded image during development.</li>
 * <li>>"Alignment" the development process will identify 3 alignment points.</li>
 * <li>>"Production" are inspection regions to be characterized for good/bad call results.</li>
 * </dl>
 * <p>
 * <tt>ReconstructionRegions</tt> manifest as <tt>ReconstructedImages</tt> is huge.
 * Thus, not all images are kept in RAM.  So, the caller is obliged to do two things:
 * </p>
 * <nl>
 * <li>Launch a separate thread for reading these images.</li>
 * <li>Mark <tt>ReconstructionRegions</tt> as complete when you are done with them. </li>
 * </nl>
 * <pre>
 * &nbsp;&nbsp;void reconstructionRegionComplete(
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TestSubProgram testSubProgram,
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReconstructionRegion reconstructionRegion)
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;boolean hasTestFailures)
 * </pre>
 * <p>
 * It is important to note that a <tt>ReconstructionRegion</tt> may be requested
 * multiple times.  This is typically done during the inspection process.  These
 * additional acquisitions must be done on the image prior to marking it complete.
 * Here is an example of such a call:
 * </p>
 * <pre>
 * &nbsp;&nbsp;void reacquireImages(
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int testSubProgramId,
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReconstructionRegion reconstructionRegion)
 * </pre>
 * <b><i><u>Class diagram:</u></i></b>
 * <p>
 * <IMG SRC="doc-files/ImageAcquistionEngine.gif">
 *
 * @author Roy Williams
 */
public class ImageAcquisitionEngine
{
  private static Config _config = null;
  private static ImageAcquisitionEngine _instance = null;

  private ImageAcquisitionModeEnum _acquisitionMode = ImageAcquisitionModeEnum.NOT_IN_USE;
  private ReconstructedImagesProducer _reconstructedImagesProducer;
  private ProjectedImagesProducer _projectedImagesProducer;
  private ProjectedImagesProducerWithSyntheticTriggers _projectedImagesProducerWithSyntheticTriggers;
  private HardwareObservable _hardwareObservable;
  private boolean _userAborted = false;
  private PanelPositioner _panelPositioner;
  private XrayCameraArray _xrayCameraArray;
  private ImageAcquisitionProgressMonitor _progressMonitor;
  private boolean _isDiagnosticsModeEnabled = false;
  private String _normalizedScanStepSizesString = "0.9934,0.7534,1.0000,0.5200,0.9867,0.9133";
  private boolean _alignmentExceptionCaught = false;
  private boolean _isGeneratingPrecisionImageSet = false;

  static private  TestExecutionTimer _testExecutionTimer = null;

  private boolean _isGenerateImageForExposureLearning = false;
  private static boolean _debug = false;
  private static String _me = "ImageAcquisitionEngine";

  /**
   * @author Rex Shang
   */
  static
  {
    _config = Config.getInstance();
  }

  /**
   * @author Roy Williams
   */
  public static synchronized ImageAcquisitionEngine getInstance()
  {
    // If there isn't an instance, then make one using the default values in the
    // config file.
    if (_instance == null)
    {
      new ImageAcquisitionEngine();
    }
    return _instance;
  }

  /**
   * @author Roy Williams
   */
  private ImageAcquisitionEngine()
  {
    int numberOfIRPs = _config.getIntValue(HardwareConfigEnum.NUMBER_OF_IMAGE_RECONSTRUCTION_ENGINES);
    Assert.expect(numberOfIRPs > 0);

    //Set developer debug mode
//    setDebug(_config.isDeveloperDebugModeOn());
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    setDebug(Config.isDeveloperSystemEnabled() & DeveloperDebugConfigEnum.isConfigEnumExists(DeveloperDebugConfigEnum.PROJECTION_PRODUCER_DEBUG) );
    
    // Create the set of ProductionProducers which provide the various projections
    // or reconstructed regions.
    _progressMonitor = ImageAcquisitionProgressMonitor.getInstance();
    _reconstructedImagesProducer = new ReconstructedImagesProducer(numberOfIRPs);
    _projectedImagesProducer = new ProjectedImagesProducer();
    _projectedImagesProducerWithSyntheticTriggers = new ProjectedImagesProducerWithSyntheticTriggers();
    _hardwareObservable = HardwareObservable.getInstance();
    _panelPositioner = PanelPositioner.getInstance();
    _xrayCameraArray = XrayCameraArray.getInstance();
    _instance = this;
  }

  /**
   * This method will return the number 1 for the first version of hardware that
   * we ship with.  If we ever change the hardware in such a way that it would require
   * previous programs to no longer work properly, then the number returned
   * by this method would be incremented.  The business code will use this to
   * determine if it has to regenerate existing programs or not.
   *
   * @author Bill Darbie
   */
  int getHardwareCompatibilityVersion()
  {
    // wpd - need to handle forward and backward compatibilty
    return 1;
  }

  /**
   * @author Roy Williams
   */
  public ReconstructedImagesProducer getReconstructedImagesProducer()
  {
    Assert.expect(_reconstructedImagesProducer != null);

    return _reconstructedImagesProducer;
  }

  /**
   * @author Roy Williams
   */
  public ProjectedImagesProducer getProjectedImagesProducer()
  {
    Assert.expect(_projectedImagesProducer != null);

    return _projectedImagesProducer;
  }

  /**
   * @author Matt Wharton
   */
  public ImageAcquisitionModeEnum getImageAcquisitionMode()
  {
    Assert.expect(_acquisitionMode != null);

    return _acquisitionMode;
  }

  /**
   * @author Dave Ferguson
   */
  public boolean isOfflineReconstructionEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.OFFLINE_RECONSTRUCTION);
  }

  /**
   * @author Matt Wharton
   */
  public boolean isAbortInProgress()
  {
    boolean abortInProgress = false;
    if (isLegalReconstructedImagesMode(_acquisitionMode))
    {
      abortInProgress = _reconstructedImagesProducer.isAbortInProgress();
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PROJECTION))
    {
      abortInProgress = _projectedImagesProducer.isAbortInProgress();
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PROJECTION_WITH_SYNTHETIC_TRIGGERS))
    {
      abortInProgress = _projectedImagesProducerWithSyntheticTriggers.isAbortInProgress();
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.NOT_IN_USE))
    {
      abortInProgress = false;
    }
    else
      Assert.expect(false);

    return abortInProgress;
  }

  /**
   * @author Roy Williams
   */
  public synchronized void acquireProductionImages(TestProgram testProgram, 
                                                   boolean isDiagnosticsModeEnabled, 
                                                   TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);

    _testExecutionTimer = testExecutionTimer;
    _hardwareObservable.stateChangedBegin(this, ImageAcquisitionEventEnum.IMAGE_ACQUISITION);
    _hardwareObservable.setEnabled(false);

    try
    {
      _isDiagnosticsModeEnabled = isDiagnosticsModeEnabled;
      _reconstructedImagesProducer.clearForNextRun();
      configureForReconstructedImages(ImageAcquisitionModeEnum.PRODUCTION);
      if ((XrayTester.getInstance().isSimulationModeOn() == false) || isOfflineReconstructionEnabled())
      {
        PanelHandler.getInstance().setIsProduction(true);
        double appropriateMaxSliceHeight = XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(testProgram.getProject().getPanel().getThicknessInNanometers(), testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers());
        if(TestExecution.getFastSpeedEnable())
        {
 
//           TestExecution.getInstance().setFastSpeedEnabledAcquireImagesParameters(testProgram,
//                                                   testExecutionTimer,
//                                                   appropriateMaxSliceHeight,
//                                                   Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers(testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers())),
//                                                   ImageAcquisitionModeEnum.PRODUCTION,
//                                                   0);
     
          _reconstructedImagesProducer.fastSpeedEnabledAcquireImages(testProgram,
                                                   testExecutionTimer,
                                                   appropriateMaxSliceHeight,
                                                   Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers(testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers())),
                                                   ImageAcquisitionModeEnum.PRODUCTION,
                                                   0);
          
          TestExecution.resetFastSpeedEnable();
        }
        else
        {
          if (_userAborted)
          {
            return;
          }
        _reconstructedImagesProducer.acquireImages(testProgram,
                                                   testExecutionTimer,
                                                   appropriateMaxSliceHeight,
                                                   Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers(testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers())),
                                                   ImageAcquisitionModeEnum.PRODUCTION,
                                                   0);
        }

      }
      else
      {
        _reconstructedImagesProducer.acquireSimulationProductionImages(testProgram, testExecutionTimer);
      }
    }
    catch(XrayTesterException xex)
    {
      abort(xex);
    }
    finally
    {
      _isDiagnosticsModeEnabled = false;
      _acquisitionMode = ImageAcquisitionModeEnum.NOT_IN_USE;
      _reconstructedImagesProducer.waitIfAbortInProgress();
      _hardwareObservable.setEnabled(true);
      if (_reconstructedImagesProducer.hasXrayTesterException())
        throw _reconstructedImagesProducer.getXrayTesterException();
    }
    _hardwareObservable.stateChangedEnd(this, ImageAcquisitionEventEnum.IMAGE_ACQUISITION);
  }

  /**
   * @author Roy Williams
   */
  public synchronized void acquireImageSetImages(TestProgram testProgram, 
                                                 boolean isDiagnosticsModeEnabled, 
                                                 TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);

    _testExecutionTimer = testExecutionTimer;

    _hardwareObservable.stateChangedBegin(this, ImageAcquisitionEventEnum.IMAGE_ACQUISITION);
    _hardwareObservable.setEnabled(false);

    try
    {
      _isDiagnosticsModeEnabled = isDiagnosticsModeEnabled;
      _reconstructedImagesProducer.clearForNextRun();
      configureForReconstructedImages(ImageAcquisitionModeEnum.IMAGE_SET);
      if ((XrayTester.getInstance().isSimulationModeOn() == false) || isOfflineReconstructionEnabled())
      {
        double appropriateMaxSliceHeight = XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(testProgram.getProject().getPanel().getThicknessInNanometers(), testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers());
        if(_userAborted)
        {
          return;
        }
        _reconstructedImagesProducer.acquireImages(testProgram,
                                                   testExecutionTimer,
                                                   appropriateMaxSliceHeight,
                                                   Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers(testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers())),
                                                   ImageAcquisitionModeEnum.IMAGE_SET,
                                                   0);
      }
      else
      {
        _reconstructedImagesProducer.acquireSimulationProductionImages(testProgram, testExecutionTimer);
      }
    }
    catch(XrayTesterException xex)
    {
      abort(xex);
    }
    finally
    {
      _isDiagnosticsModeEnabled = false;
      _acquisitionMode = ImageAcquisitionModeEnum.NOT_IN_USE;
      _reconstructedImagesProducer.waitIfAbortInProgress();
      _hardwareObservable.setEnabled(true);
      if (_reconstructedImagesProducer.hasXrayTesterException())
        throw _reconstructedImagesProducer.getXrayTesterException();
    }
    _hardwareObservable.stateChangedEnd(this, ImageAcquisitionEventEnum.IMAGE_ACQUISITION);
  }

  /**
   * @author Roy Williams
   */
  public synchronized void acquireAlignmentImages(TestProgram testProgram, 
                                                  TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(configuredForProjectedImages() == false);

    // elw -- Don't send event here. We don't need to run grayscale
    // for alignment images assuming we were in cal recently
//    _hardwareObservable.stateChangedBegin(this, ImageAcquisitionEventEnum.IMAGE_ACQUISITION);
//    _hardwareObservable.setEnabled(false);

    try
    {
      _reconstructedImagesProducer.clearForNextRun();
      configureForReconstructedImages(ImageAcquisitionModeEnum.ALIGNMENT);
      if ((XrayTester.getInstance().isSimulationModeOn() == false) || isOfflineReconstructionEnabled())
      {

        double appropriateMaxSliceHeight = XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(testProgram.getProject().getPanel().getThicknessInNanometers(), testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers());

        if(_userAborted)
        {
          return;
        }
        _reconstructedImagesProducer.acquireImages(testProgram,
                                                   testExecutionTimer,
                                                   appropriateMaxSliceHeight,
                                                   Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers(testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers())),
                                                   ImageAcquisitionModeEnum.ALIGNMENT,
                                                   0);
      }
      else
      {
        _reconstructedImagesProducer.acquireSimulationAlignmentImages(testProgram, testExecutionTimer);
      }
    }
    catch(XrayTesterException xex)
    {
      abort(xex);
    }
    finally
    {
      _acquisitionMode = ImageAcquisitionModeEnum.NOT_IN_USE;
      _reconstructedImagesProducer.waitIfAbortInProgress();
//      _hardwareObservable.setEnabled(true);
      if (_reconstructedImagesProducer.hasXrayTesterException())
        throw _reconstructedImagesProducer.getXrayTesterException();
    }
//    _hardwareObservable.stateChangedEnd(this, ImageAcquisitionEventEnum.IMAGE_ACQUISITION);
  }

  /**
   * @author George A. David
   */
  public synchronized void acquireAlignmentImages(TestSubProgram subProgram, TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(subProgram != null);

    TestProgram testProgram = subProgram.getTestProgram();
    try
    {
      testProgram.clearTestSubProgramFilters();
      testProgram.addFilter(subProgram);
      acquireAlignmentImages(testProgram, testExecutionTimer);
    }
    finally
    {
      testProgram.clearTestSubProgramFilters();
    }
  }

  /**
   * @author Roy Williams
   */
  public void acquireOfflineProductionImages(TestProgram testProgram,
                                             TestExecutionTimer testExecutionTimer,
                                             ImageSetData imageSetData) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(imageSetData != null);
    Assert.expect(configuredForProjectedImages() == false);

    try
    {
      configureForReconstructedImages(ImageAcquisitionModeEnum.OFFLINE);
      _reconstructedImagesProducer.acquireOfflineProductionImages(testProgram, imageSetData);
    }
    catch(XrayTesterException xex)
    {
      abort(xex);
    }
    finally
    {
      _acquisitionMode = ImageAcquisitionModeEnum.NOT_IN_USE;
      if (_reconstructedImagesProducer.hasXrayTesterException())
        throw _reconstructedImagesProducer.getXrayTesterException();
    }
  }

  /**
   * @author Roy Williams
   */
  public synchronized void acquireVerificationImages(TestProgram testProgram, 
                                                     TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(configuredForProjectedImages() == false);

    _hardwareObservable.stateChangedBegin(this, ImageAcquisitionEventEnum.IMAGE_ACQUISITION);
    _hardwareObservable.setEnabled(false);

    try
    {
      _reconstructedImagesProducer.clearForNextRun();
      configureForReconstructedImages(ImageAcquisitionModeEnum.VERIFICATION);
      if ((XrayTester.getInstance().isSimulationModeOn() == false) || isOfflineReconstructionEnabled())
      {
        if (_userAborted)
        {
          return;
        }
        _reconstructedImagesProducer.acquireImages(testProgram,
                                                   testExecutionTimer,
                                                   XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(testProgram.getProject().getPanel().getThicknessInNanometers(), testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers()),
                                                   Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers(testProgram.getProject().getPanel().getMeansBoardZOffsetInNanometers())),
                                                   ImageAcquisitionModeEnum.VERIFICATION,
                                                   0);
      }
      else
      {
        _reconstructedImagesProducer.acquireSimulationVerificationImages(testProgram, testExecutionTimer);
      }
    }
    catch(XrayTesterException xex)
    {
      abort(xex);
    }
    finally
    {
      _acquisitionMode = ImageAcquisitionModeEnum.NOT_IN_USE;
      _reconstructedImagesProducer.waitIfAbortInProgress();
      _hardwareObservable.setEnabled(true);
      if (_reconstructedImagesProducer.hasXrayTesterException())
        throw _reconstructedImagesProducer.getXrayTesterException();
    }
    _hardwareObservable.stateChangedEnd(this, ImageAcquisitionEventEnum.IMAGE_ACQUISITION);
  }

  /**
   * @author George A. David
   */
  public synchronized void acquireVerificationImages(TestSubProgram subProgram, TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(subProgram  != null);

    TestProgram testProgram = subProgram.getTestProgram();
    try
    {
      testProgram.clearTestSubProgramFilters();
      testProgram.addFilter(subProgram);
      acquireVerificationImages(testProgram, testExecutionTimer);
    }
    finally
    {
      testProgram.clearTestSubProgramFilters();
    }
  }

  /**
   * @author Roy Williams
   */
  public synchronized void acquireCouponImages(TestProgram testProgram,
                                               TestExecutionTimer testExecutionTimer,
                                               double zFocusHeightAboveNominalPlaneInNanometers,
                                               double zFocusHeightBelowNominalPlaneInNanometers,
                                               double stepOverridePercentage) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(configuredForProjectedImages() == false);
    Assert.expect(zFocusHeightAboveNominalPlaneInNanometers >= 0);
    Assert.expect(zFocusHeightBelowNominalPlaneInNanometers >= 0);

    try
    {
      _reconstructedImagesProducer.clearForNextRun();
      configureForReconstructedImages(ImageAcquisitionModeEnum.COUPON);
      if (_userAborted)
      {
        return;
      }
      _reconstructedImagesProducer.acquireImages(testProgram,
                                                 testExecutionTimer,
                                                 zFocusHeightAboveNominalPlaneInNanometers,
                                                 zFocusHeightBelowNominalPlaneInNanometers,
                                                 ImageAcquisitionModeEnum.COUPON,
                                                 stepOverridePercentage);
    }
    catch(XrayTesterException xex)
    {
      abort(xex);
    }
    finally
    {
      _acquisitionMode = ImageAcquisitionModeEnum.NOT_IN_USE;
      if (_reconstructedImagesProducer.hasXrayTesterException())
        throw _reconstructedImagesProducer.getXrayTesterException();
    }
  }

  /**
   * @author Matt Wharton
   */
  public ReconstructedImages acquireOfflineLightestImages(ImageSetData imageSetData,
                                                          ReconstructionRegion reconstructionRegion,
                                                          BooleanRef lightestImagesAvailable) throws XrayTesterException
  {
    Assert.expect(imageSetData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(lightestImagesAvailable != null);

    ReconstructedImages lightestImages = _reconstructedImagesProducer.acquireOfflineLightestImages(imageSetData,
                                                                                                   reconstructionRegion,
                                                                                                   lightestImagesAvailable);

    return lightestImages;
  }

  /**
   * @author Matt Wharton
   */
  public ReconstructedImages acquireOfflineReReconstructedImages(ImageSetData imageSetData,
                                                                 ReconstructionRegion reconstructionRegion,
                                                                 int sliceOffsetInNanometers,
                                                                 BooleanRef reReconstructedImagesAvailable) throws XrayTesterException
  {
    Assert.expect(imageSetData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(reReconstructedImagesAvailable != null);

    ReconstructedImages reReconstructedImages =
        _reconstructedImagesProducer.acquireOfflineReReconstructedImages(imageSetData,
                                                                         reconstructionRegion,
                                                                         sliceOffsetInNanometers,
                                                                         reReconstructedImagesAvailable);

    return reReconstructedImages;
  }

  /**
   * @author Roy Williams
   */
  public ReconstructedImages getReconstructedImages(BooleanRef noImagesReturned) throws XrayTesterException
  {
    Assert.expect(noImagesReturned != null);
    Assert.expect(_reconstructedImagesProducer != null);

    // Intentionally not doing next assert because race condition exists to
    // startup threads for getting image.   Roy
    // Assert.expect(configuredForReconstructedImages());

    return _reconstructedImagesProducer.getReconstructedImages(noImagesReturned);
  }

  /**
   * @author Matt Wharton
   */
  public void freeReconstructedImages(ReconstructionRegion reconstructionRegion) throws XrayTesterException
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(_reconstructedImagesProducer != null);

    _reconstructedImagesProducer.freeReconstructedImages(reconstructionRegion);
  }

  /**
   * @author Roy Williams
   */
  private void initializeReconstructedImagesList() throws DatastoreException
  {
    Assert.expect(_reconstructedImagesProducer != null);
    _reconstructedImagesProducer.initializeReconstructedImagesList();
  }

  /**
   * @author Matt Wharton
   */
  private void initializeLightestImagesQueue()
  {
    Assert.expect(_reconstructedImagesProducer != null);

    ReconstructedImagesManager reconstructedImagesManager = _reconstructedImagesProducer.getReconstructedImagesManager();
    reconstructedImagesManager.initializeLightestImagesQueue();
  }

  /**
   * @author Matt Wharton
   */
  public void initialize() throws DatastoreException
  {
    Assert.expect(_reconstructedImagesProducer != null);

    _userAborted = false;
    _reconstructedImagesProducer.clearForNextRun();
    _projectedImagesProducer.clearForNextRun();
    _projectedImagesProducerWithSyntheticTriggers.clearForNextRun();

    ReconstructedImagesManager reconstructedImagesManager = _reconstructedImagesProducer.getReconstructedImagesManager();
    reconstructedImagesManager.initialize();
    
    _alignmentExceptionCaught =  false;
  }

  /**
   * @author Roy Williams
   */
  public synchronized void acquireProjections(List<ProjectionRequestThreadTask> projectionRequestThreadTasks,
                                              TestExecutionTimer testExecutionTimer,
                                              boolean optimizeProjectionRequestOrder) throws XrayTesterException
  {
    acquireProjections(projectionRequestThreadTasks, testExecutionTimer, optimizeProjectionRequestOrder, true);
  }
  
  /**
   * @author Roy Williams
   */
  public synchronized void acquireProjections(List<ProjectionRequestThreadTask> projectionRequestThreadTasks,
                                              TestExecutionTimer testExecutionTimer,
                                              boolean optimizeProjectionRequestOrder,
                                              boolean turnOnXray) throws XrayTesterException
  {
    Assert.expect(projectionRequestThreadTasks != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(projectionRequestThreadTasks.size() > 0);

    try
    {
      configureForProjectedImages(ImageAcquisitionModeEnum.PROJECTION);

      if (_userAborted)
      {
        return;
      }
      // Acquire images.
      _projectedImagesProducer.acquireProjections(projectionRequestThreadTasks, testExecutionTimer, optimizeProjectionRequestOrder, turnOnXray);
    }
    catch(XrayTesterException xex)
    {
      abort(xex);
    }
    finally
    {
      _acquisitionMode = ImageAcquisitionModeEnum.NOT_IN_USE;
      if (_projectedImagesProducer.hasXrayTesterException())
        throw _projectedImagesProducer.getXrayTesterException();
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public synchronized void acquireHighMagProjections(List<ProjectionRequestThreadTask> projectionRequestThreadTasks,
                                              TestExecutionTimer testExecutionTimer,
                                              boolean optimizeProjectionRequestOrder) throws XrayTesterException
  {
    acquireHighMagProjections(projectionRequestThreadTasks, testExecutionTimer, optimizeProjectionRequestOrder, true);
  }
  
  /**
   * @author Wei Chin
   */
  public synchronized void acquireHighMagProjections(List<ProjectionRequestThreadTask> projectionRequestThreadTasks,
                                              TestExecutionTimer testExecutionTimer,
                                              boolean optimizeProjectionRequestOrder,
                                              boolean turnOnXray) throws XrayTesterException
  {
    Assert.expect(projectionRequestThreadTasks != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(projectionRequestThreadTasks.size() > 0);

    try
    {
      configureForProjectedImages(ImageAcquisitionModeEnum.PROJECTION);

      if (_userAborted)
      {
        return;
      }
      // Acquire images.
      _projectedImagesProducer.acquireHighMagProjections(projectionRequestThreadTasks, testExecutionTimer, optimizeProjectionRequestOrder, turnOnXray);
    }
    catch(XrayTesterException xex)
    {
      abort(xex);
    }
    finally
    {
      _acquisitionMode = ImageAcquisitionModeEnum.NOT_IN_USE;
      if (_projectedImagesProducer.hasXrayTesterException())
        throw _projectedImagesProducer.getXrayTesterException();
    }
  }

  /**
   * Method to acquire Projection Images without moving the stage. It program the
   * cameras (silently ignoring unwanted cameras), turn on triggers, acquire images
   * and turn off xrays. It will NOT turn on xrays, since there are cases when
   * when we want to capture dark images .
   *
   * @author Reid Hayhow
   */
  public synchronized void acquireProjectionsWithSyntheticTriggers(List<ProjectionRequestThreadTask> projectionRequestThreadTasks,
                                                                   TestExecutionTimer testExecutionTimer,
                                                                   boolean optimizeProjectionRequestOrder,
                                                                   boolean turnOnXray) throws XrayTesterException
  {
    Assert.expect(projectionRequestThreadTasks != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(projectionRequestThreadTasks.size() > 0);

    try
    {
      configureForProjectedImages(ImageAcquisitionModeEnum.PROJECTION_WITH_SYNTHETIC_TRIGGERS);

      if (_userAborted)
      {
        return;
      }
      // Acquire images.
      if(XrayTester.isLowMagnification())
        _projectedImagesProducerWithSyntheticTriggers.acquireProjections(projectionRequestThreadTasks, testExecutionTimer, optimizeProjectionRequestOrder, turnOnXray);
      else
        _projectedImagesProducerWithSyntheticTriggers.acquireHighMagProjections(projectionRequestThreadTasks, testExecutionTimer, optimizeProjectionRequestOrder, turnOnXray);
    }
    catch(XrayTesterException xex)
    {
      abort(xex);
    }
    finally
    {
      _acquisitionMode = ImageAcquisitionModeEnum.NOT_IN_USE;
      if (_projectedImagesProducerWithSyntheticTriggers.hasXrayTesterException())
        throw _projectedImagesProducerWithSyntheticTriggers.getXrayTesterException();
    }
  }

  /**
   * ESTIMATES the number of milli-seconds required to acquire images for the specified TestProgram.
   *
   * @author Matt Wharton
   */
  public int getEstimatedScanTimeInMillis(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    /** @todo wpd - Matt - put some meaningful code here... probably do not need this anymore talk to Andy.
     */

    return 35000;
  }

  /**
   * Re-acquire the specified ReconstructedImages (assumes no focal offset).
   *
   * @author Roy Williams
   */
  public void reacquireImages(ReconstructionRegion reconstructionRegion) throws XrayTesterException
  {
    reacquireImages(reconstructionRegion, 0);
  }

  /**
   * Re-acquire the specified ReconstructedImages at the specified focal offset.
   *
   * @author Roy Williams
   */
  public void reacquireImages(ReconstructionRegion reconstructionRegion,
                              int reconstructionZOffsetInNanometers) throws XrayTesterException
  {
    Assert.expect(reconstructionRegion != null);

    if ((XrayTester.getInstance().isSimulationModeOn() == false) || isOfflineReconstructionEnabled())
    {
      int testSubProgramId = reconstructionRegion.getTestSubProgram().getId();
      _reconstructedImagesProducer.reacquireImages(testSubProgramId,
                                                   reconstructionRegion,
                                                   reconstructionZOffsetInNanometers);
    }
    else
    {
      _reconstructedImagesProducer.reacquireSimulationImages(reconstructionRegion, reconstructionZOffsetInNanometers);
    }
  }

  /**
   * Re-acquire the specified ReconstructedImages at the specified focal offset.
   * Blocks until the images come back up.
   *
   * @author Matt Wharton
   */
  public ReconstructedImages reacquireImagesAndWait(ReconstructionRegion reconstructionRegion,
                                                    int reconstructionZOffsetInNanometers,
                                                    BooleanRef reReconstructedImagesAvailable) throws XrayTesterException
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(reReconstructedImagesAvailable != null);

    ReconstructedImages reReconstructedImages = null;
    if (isAbortInProgress() == false)
    {
      _testExecutionTimer.startReReconstructTimer();
      reacquireImages(reconstructionRegion, reconstructionZOffsetInNanometers);

      // Block until our re-reconstructed images are available.
      reReconstructedImages = waitForReReconstructedImages(reconstructionRegion, reReconstructedImagesAvailable);
      _testExecutionTimer.stopReReconstructTimer();
    }
    else
    {
      reReconstructedImagesAvailable.setValue(false);
    }

    return reReconstructedImages;
  }

  /**
   * Re-acquire the specified ReconstructedImages (assumes no focal offset).
   * Blocks until the images come back up.
   *
   * @author Matt Wharton
   */
  public ReconstructedImages reacquireImagesAndWait(ReconstructionRegion reconstructionRegion,
                                                    BooleanRef reReconstructedImagesAvailable) throws XrayTesterException
  {
    return reacquireImagesAndWait(reconstructionRegion, 0, reReconstructedImagesAvailable);
  }

  /**
   * Re-acquire lightest images for the specified region (assumes no focal offset).
   *
   * @author Roy Williams
   */
  public void reacquireLightestImages(ReconstructionRegion reconstructionRegion) throws XrayTesterException
  {
    reacquireLightestImages(reconstructionRegion, 0);
  }

  /**
   * Re-acquire lightest images for the specified region at the specified focal offset.
   *
   * @author Roy Williams
   */
  public void reacquireLightestImages(ReconstructionRegion reconstructionRegion,
                                      int reconstructionZOffsetInNanometers) throws XrayTesterException
  {
    Assert.expect(reconstructionRegion != null);

    if ((XrayTester.getInstance().isSimulationModeOn() == false) || isOfflineReconstructionEnabled())
    {
      int testSubProgramId = reconstructionRegion.getTestSubProgram().getId();
      _reconstructedImagesProducer.reacquireLightestImages(testSubProgramId,
                                                           reconstructionRegion,
                                                           reconstructionZOffsetInNanometers);
    }
    else
    {
      _reconstructedImagesProducer.reacquireSimulationLightestImages(reconstructionRegion);
    }
  }

  /**
   * Re-acquire lightest images for the specified region at the specified focal offset.
   * Blocks until the lightest images come back up.

   * @author Matt Wharton
   */
  public ReconstructedImages reacquireLightestImagesAndWait(ReconstructionRegion reconstructionRegion,
                                                            int reconstructionZOffsetInNanometers,
                                                            BooleanRef lightestImagesAvailable) throws XrayTesterException
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(lightestImagesAvailable != null);

    ReconstructedImages lightestImages = null;
    if (isAbortInProgress() == false)
    {
      reacquireLightestImages(reconstructionRegion, reconstructionZOffsetInNanometers);

      // Block until our lightest images are available.
      lightestImages = waitForLightestImages(reconstructionRegion, lightestImagesAvailable);
    }
    else
    {
      lightestImagesAvailable.setValue(false);
    }

    return lightestImages;
  }

  /**
   * Re-acquire lightest images for the specified region (assumes no focal offset).
   * Blocks until the lightest images come back up.

   * @author Matt Wharton
   */
  public ReconstructedImages reacquireLightestImagesAndWait(ReconstructionRegion reconstructionRegion,
                                                            BooleanRef lightestImagesAvailable) throws XrayTesterException
  {
    return reacquireLightestImagesAndWait(reconstructionRegion, 0, lightestImagesAvailable);
  }

  /**
   * Tell the IRP to change the zHeight of the specified focusInstruction of the slice of the reconstructionRegion
   * @author Scott Richardson
   */
  public void modifyFocusInstructionZHeight(int testSubProgramId,
                                            ReconstructionRegion reconstructionRegion,
                                            int sliceId,
                                            int zHeightFocusInNanoMeters,
                                            int percent,
                                            int zOffset,
                                            byte focusMethodId) throws XrayTesterException
  {
    Assert.expect(testSubProgramId >= 0);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceId >= 0);

    _reconstructedImagesProducer.modifyFocusInstruction(testSubProgramId,
                                                        reconstructionRegion,
                                                        sliceId,
                                                        zHeightFocusInNanoMeters,
                                                        percent,
                                                        zOffset,
                                                        focusMethodId);
  }

  /**
   * Waits for the lightest images for the specified reconstruction region to be acquired.
   *
   * @author Matt Wharton
   */
  public ReconstructedImages waitForLightestImages(ReconstructionRegion reconstructionRegion,
                                                   BooleanRef lightestImagesAvailable)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(lightestImagesAvailable != null);
    Assert.expect(_reconstructedImagesProducer != null);

    ReconstructedImagesManager reconstructedImagesManager = _reconstructedImagesProducer.getReconstructedImagesManager();
    Assert.expect(reconstructedImagesManager != null);
    return reconstructedImagesManager.waitForLightestImages(reconstructionRegion, lightestImagesAvailable);
  }

  /**
   * Waits for the re-reconstructed images for the specified reconstruction region to be acquired.

   * @author Matt Wharton
   */
  public ReconstructedImages waitForReReconstructedImages(ReconstructionRegion reconstructionRegion,
                                                          BooleanRef reReconstructedImagesAvailable)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(reReconstructedImagesAvailable != null);
    Assert.expect(_reconstructedImagesProducer != null);

    ReconstructedImagesManager reconstructedImagesManager = _reconstructedImagesProducer.getReconstructedImagesManager();
    Assert.expect(reconstructedImagesManager != null);
    return reconstructedImagesManager.waitForReReconstructedImages(reconstructionRegion, reReconstructedImagesAvailable);
  }

  /**
   * Indicate that higher level modules are finished with a
   * specified region so resources can be freed.
   *
   * @author Roy Williams
   */
  public void reconstructionRegionComplete(final ReconstructionRegion reconstructionRegion,
                                           final boolean hasTestFailure) throws XrayTesterException
  {
    Assert.expect(reconstructionRegion != null);

    _reconstructedImagesProducer.reconstructionRegionComplete(reconstructionRegion.getTestSubProgram(), reconstructionRegion, hasTestFailure);
  }

  /**
   * @author Roy Williams
   */
  public void defineProcessorStripsForVerificationRegions(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    // Do not do the following Assert because the board can be undergoing program
    // genration while the project is being loaded OR prior to run in GUI
    // Assert.expect(configuredForReconstructedImages());

    _reconstructedImagesProducer.defineProcessorStripsForVerificationRegions(testProgram);
  }

  /**
   * @author Roy Williams
   */
  public void defineProcessorStripsForInspectionRegions(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    // Do not do the following Assert because the board can be undergoing program
    // genration while the project is being loaded OR prior to run in GUI
    // Assert.expect(configuredForReconstructedImages());

    _reconstructedImagesProducer.defineProcessorStripsForInspectionRegions(testProgram);
  }

  /**
   * @author Roy Williams
   */
  public void applyAlignmentResult(AffineTransform affineTransform,
                                   int testSubProgramId) throws XrayTesterException
  {
    Assert.expect(affineTransform != null);
    Assert.expect(testSubProgramId >= 0);
    if (_reconstructedImagesProducer.checkForAbortInProgress())
      return;
    _progressMonitor.alignmentReadyToBeSent();
    _reconstructedImagesProducer.applyAlignmentResult(affineTransform, testSubProgramId);
    _progressMonitor.alignmentCompleted();
  }

  /**
   * Send the calibrated magnification constants as found in the hardware.calib
   * to the IRP's.
   *
   * @author Roy Williams
   */
  public void sendCalibratedMagnifications(double mag, double highMag) throws XrayTesterException
  {
    Assert.expect(configuredForReconstructedImages());
    Assert.expect(mag > 0);

    _reconstructedImagesProducer.sendCalibratedMagnifications(mag, highMag);
  }

  /**
   * User has canceled the currently running image acquisition request.
   * Abort all hardware subsystems involved in image acquisition (cameras, IREs,
   * stage, and tube) where the reason for aborting is not given.
   *
   * @author Dave Ferguson
   * @author Roy Williams
   * @author Matt Wharton
   */
  public void abort() throws XrayTesterException
  {
    // some users may already be waiting for images, let's make sure
    // we abort this so they don't get stuck
    _reconstructedImagesProducer.getReconstructedImagesManager().abort();

    // If we are not in use, then this call should never be made.
    if (_acquisitionMode.equals(ImageAcquisitionModeEnum.NOT_IN_USE))
      return;

    if(_userAborted)
      return;

    try
    {
      _userAborted = true;

      _progressMonitor.stopMonitoring();
      if (isLegalReconstructedImagesMode(_acquisitionMode))
      {
        _reconstructedImagesProducer.userAbort();
      }
      else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PROJECTION))
      {
        _projectedImagesProducer.userAbort();
      }
      else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PROJECTION_WITH_SYNTHETIC_TRIGGERS))
      {
        _projectedImagesProducerWithSyntheticTriggers.userAbort();
      }
      else
        Assert.expect(false);
    }
    catch(XrayTesterException xex)
    {
      abort(xex);
    }
  }

  /**
   * @author George A. David
   */
  public void abort(XrayTesterException ex)
  {
    Assert.expect(ex != null);

    // If we are not in use, then this call should never be made.
    if (_acquisitionMode.equals(ImageAcquisitionModeEnum.NOT_IN_USE))
      return;

    _progressMonitor.stopMonitoring();

    if (isLegalReconstructedImagesMode(_acquisitionMode))
    {
      _reconstructedImagesProducer.abort(ex);
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PROJECTION))
    {
      _projectedImagesProducer.abort(ex);
    }
    else if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PROJECTION_WITH_SYNTHETIC_TRIGGERS))
    {
      _projectedImagesProducerWithSyntheticTriggers.abort(ex);
    }
    else
      Assert.expect(false, "unexpected acquisition mode: " + _acquisitionMode);
  }

  /**
   * Prior to execution by <tt>runAcquisition(List<ProjectionRequest>)</tt>>, it
   * is helpful to understand the scanpath that will be used.   For hysterisis,
   * the calibration system will tune this list and ask for a special/modified
   * sequence.
   *
   * @author Roy Williams
   */
  public List<ProjectionScanPass> generateScanPath(List<ProjectionRequestThreadTask> projectionRequestThreadTasks) throws XrayTesterException
  {
    return _projectedImagesProducer.generateScanPath(projectionRequestThreadTasks);
  }

  /**
   * Define the set of acquisition modes that return ReconstructedImages.  And
   * thus, the requestor must mark the reconstructions complete before the acquisition
   * will complete.
   *
   * @author Roy Williams
   */
  private boolean isLegalReconstructedImagesMode(ImageAcquisitionModeEnum acquisitionMode)
  {
    Assert.expect(acquisitionMode != null);

    if (acquisitionMode.equals(ImageAcquisitionModeEnum.ALIGNMENT)  ||
        acquisitionMode.equals(ImageAcquisitionModeEnum.COUPON)     ||
        acquisitionMode.equals(ImageAcquisitionModeEnum.OFFLINE)    ||
        acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION) ||
        acquisitionMode.equals(ImageAcquisitionModeEnum.IMAGE_SET)  ||
        acquisitionMode.equals(ImageAcquisitionModeEnum.VERIFICATION))
      return true;
    return false;
  }

  /**
   * Define the set of acquisition modes that return ProjectedImages.  These are
   * also known as 2D images.
   *
   * @author Roy Williams
   */
  private boolean isLegalProjectedImagesMode(ImageAcquisitionModeEnum acquisitionMode)
  {
    return (acquisitionMode.equals(ImageAcquisitionModeEnum.PROJECTION) ||
            acquisitionMode.equals(ImageAcquisitionModeEnum.PROJECTION_WITH_SYNTHETIC_TRIGGERS));
  }

  /**
   * @author Roy Williams
   */
  private void configureForReconstructedImages(ImageAcquisitionModeEnum acquisitionMode)
  {
    Assert.expect(acquisitionMode != null);
    Assert.expect(_acquisitionMode.equals(ImageAcquisitionModeEnum.NOT_IN_USE));
    Assert.expect(isLegalReconstructedImagesMode(acquisitionMode));

    _acquisitionMode = acquisitionMode;
  }

  /**
   * @author Roy Williams
   */
  private boolean configuredForReconstructedImages()
  {
    Assert.expect(_acquisitionMode != null);

    return isLegalReconstructedImagesMode(_acquisitionMode);
  }

  /**
   * @author Roy Williams
   */
  private void configureForProjectedImages(ImageAcquisitionModeEnum acquisitionMode)
  {
    Assert.expect(acquisitionMode != null);
    Assert.expect(_acquisitionMode.equals(ImageAcquisitionModeEnum.NOT_IN_USE));
    Assert.expect(isLegalProjectedImagesMode(acquisitionMode));

    _reconstructedImagesProducer.calibrationExecutionHasChangedCameraAndStagePrograms();

    _acquisitionMode = acquisitionMode;
  }

  /**
   * @author Roy Williams
   */
  private boolean configuredForProjectedImages()
  {
    Assert.expect(_acquisitionMode != null);

    return isLegalProjectedImagesMode(_acquisitionMode);
  }

  /**
   * @author George A. David
   */
  void checkAcquisitionProgress()
  {
    try
    {
      // for now, let's not do anything while in diagnostics mode.
      if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION) && _isDiagnosticsModeEnabled)
        return;

      if (_progressMonitor.areIrpsRunning())
      {
        // check if the cameras are ok
        Map<Integer, List<AbstractXrayCamera>> scanPassesCompletedToXrayCamerasMap = new HashMap<Integer, List<AbstractXrayCamera>>();
        for (AbstractXrayCamera xrayCamera : _xrayCameraArray.getCameras())
        {
          int numScanPassesCompeted = xrayCamera.getNumberOfLineScanImagesAcquired();
          if (scanPassesCompletedToXrayCamerasMap.containsKey(numScanPassesCompeted))
          {
            scanPassesCompletedToXrayCamerasMap.get(numScanPassesCompeted).add(xrayCamera);
          }
          else
          {
            List<AbstractXrayCamera> cameras = new LinkedList<AbstractXrayCamera>();
            cameras.add(xrayCamera);
            Object previous = scanPassesCompletedToXrayCamerasMap.put(numScanPassesCompeted, cameras);
            Assert.expect(previous == null);
          }
        }

        if (scanPassesCompletedToXrayCamerasMap.size() == 1)
        {
          int numScanPassesComplete = scanPassesCompletedToXrayCamerasMap.keySet().iterator().next();
          if (numScanPassesComplete == 0)
          {
            // the cameras have not received any triggers
            // see if the stage is ok
            try
            {
              _panelPositioner.isScanPassDone();

              // if we are here, that means the stage moved, but the
              // cameras still did not receive triggers
              if (UnitTest.unitTesting() == false)
                System.out.println("ImageAcquisitionProgressMonitor: Cameras have not recevied any triggers after the stage has moved");
              abort(ImageAcquisitionHungBusinessException.getNoTriggersReceivedHungException());
            }
            catch (XrayTesterException xex)
            {
              // ok, the panel positioner was hosed, let's abort
              if(UnitTest.unitTesting() == false)
                xex.printStackTrace();
              abort(xex);
              return;
            }
          }
          else
          {
            // the cameras have received some scans
            // the stage seems to be working fine

            String message = "The following IRPs are hung (maybe due to algorithms): ";

            if (_panelPositioner.isScanPathDone())
            {
              if (UnitTest.unitTesting() == false)
              {
                System.out.println("all scans are finished.");
                System.out.println("number of image completed by camera: " + numScanPassesComplete);
              }
            }
            else
            {
              if (UnitTest.unitTesting() == false)
                System.out.println("scans are not finished.");
            }

            // see if all the IRPs are on the same page.
            boolean canAllIrpsProceed = true;
            int currentScanPassNumber = _reconstructedImagesProducer.getCurrentScanPassNumber();
            for (ImageReconstructionEngine ire : _reconstructedImagesProducer.getImageReconstructionEngines())
            {
              if (ire.canProceedWithScanPassNoWait(currentScanPassNumber) == false)
              {
                message += (ire.getId()) + " ";
                canAllIrpsProceed &= false;
              }
              else
                canAllIrpsProceed &= true;
            }

            if (canAllIrpsProceed)
            {
              if (UnitTest.unitTesting() == false)
                System.out.println("currentScanPassNumber: " + currentScanPassNumber + " total: " + _reconstructedImagesProducer.getTotalScanPassNumber());

              if (_reconstructedImagesProducer.areAllImagesAcquired())
              {
                if (UnitTest.unitTesting() == false)
                {
                  System.out.println("test is running, all images acquired, algorithms are hung");
                }
              }
              else
              {
                if (UnitTest.unitTesting() == false)
                {
                  System.out.println("image to mark complete: " + _reconstructedImagesProducer.getReconstructionRegionsToMarkComplete()
                                     + " completed: " + _reconstructedImagesProducer.getNumberOfReconstructionRegionsCompleted());
                  System.out.println("test is running, algorithms/irps are hung");
                }
              }

              abort(ImageAcquisitionHungBusinessException.getAlgorithmsOrIrpsHungException());
            }
            else
            {
              // currently we cannot distinguish between who is at fault
              if (UnitTest.unitTesting() == false)
                System.out.println(message);
              abort(ImageAcquisitionHungBusinessException.getAlgorithmsOrIrpsHungException());
            }
          }
        }
        else
        {
          // determine if it's just one camera that got out of sync.
          if (scanPassesCompletedToXrayCamerasMap.size() == 2)
          {
            // one last check to see if multiple cameras are out of sync.
            for (Map.Entry<Integer, List<AbstractXrayCamera>> entry : scanPassesCompletedToXrayCamerasMap.entrySet())
            {
              if (entry.getValue().size() == 1)
              {
                List<Integer> ids = new LinkedList<Integer>();
                ids.add(entry.getValue().get(0).getId());
                // ok, this camera is bad
                if (UnitTest.unitTesting() == false)
                  System.out.println("ImageAcquisitionProgressMonitor: Camera " + entry.getValue().get(0).getId() + " is out of sync");
                abort(ImageAcquisitionHungBusinessException.getSomeCamerasHungException(ids));
                return;
              }
            }
          }

          // if we get here, that means multiple cameras are out of sync,
          // something has to be wrong with the trigger board.
          if (UnitTest.unitTesting() == false)
            System.out.println("ImageAcquisitionProgressMonitor: multiple cameras are out of sync, the trigger board may be bad");
          abort(ImageAcquisitionHungBusinessException.getSomeTriggersReceivedHungException());
        }
      }
      else // the irps are paused, it's a problem with the algorithms or saving images (for the image set).
      {
        if (UnitTest.unitTesting() == false)
          System.out.println("ImageAcquisitionProgressMonitor: test is paused, algorithms are hung");
        ImageAcquisitionHungBusinessException businessException = null;
        if (_acquisitionMode.equals(ImageAcquisitionModeEnum.PRODUCTION))
        {
          long memoryUsed = _reconstructedImagesProducer.getReconstructedImagesManager().getMemoryConsumedByActiveReconstructedImages();
          System.out.println("MemoryConsumedByActiveReconstructedImages: " + memoryUsed);
          businessException = ImageAcquisitionHungBusinessException.getAlgorithmsHungException();
        }
        else
          businessException = ImageAcquisitionHungBusinessException.getSavingImagesHungException();
        abort(businessException);
      }
    }
    catch(XrayTesterException xex)
    {
      abort(xex);
    }
    finally
    {
      _progressMonitor.resetTimeOut();
    }
  }

  /**
   * Allow debugging to be dynamically enabled/disabled
   *
   * @author Roy Williams
   */
  static public void setDebug(boolean state)
  {
    ReconstructedImagesProducer.setDebug(state);
  }

  /**
   *
   * @author Reid Hayhow
   */
  public void setNormalizedScanStepSizesString(String newNormalizedScanStepSizes)
  {
    _normalizedScanStepSizesString = newNormalizedScanStepSizes;
  }

  /**
   *
   * @author Reid Hayhow
   */
  public String getNormalizedScanStepSizesString()
  {
    return _normalizedScanStepSizesString;
  }

  /**
   * @author Roy Williams
   */
  public void setHardwareNeedsToBeReprogrammed()
  {
    _reconstructedImagesProducer.setHardwareNeedsToBeReprogrammedOnNextRun();
  }

  /**
   * @author Roy Williams
   */
  public TestExecutionTimer getTestExecutionTimer()
  {
    return _testExecutionTimer;
  }

  /**
   * @author Roy Williams
   */
  public void clearProjectInfo () throws XrayTesterException
  {
     // do local cleanup then pass it down
    _acquisitionMode = ImageAcquisitionModeEnum.NOT_IN_USE;
    _reconstructedImagesProducer.clearProjectInfo();
  }

  /**
   * @return the _alignmentExceptionCaught
   * @author Wei Chin
   */
  public boolean isAlignmentExceptionCaught()
  {
    return _alignmentExceptionCaught;
  }

  /**
   * @param alignmentExceptionCaught the _alignmentExceptionCaught to set
   * @author Wei Chin
   */
  public void setAlignmentExceptionCaught(boolean alignmentExceptionCaught)
  {
    _alignmentExceptionCaught = alignmentExceptionCaught;
  }
  
    /**
   * @author Wei Chin
   */
  public static synchronized void changeMagnification(MagnificationTypeEnum magnificationType)
  {
    if(magnificationType.equals(MagnificationTypeEnum.HIGH))
    {
      if( XrayTester.isLowMagnification())
      {
        XrayTester.changeMagnification(MagnificationEnum.H_NOMINAL);
        MechanicalConversions.reset();
        ImagingChainProgramGenerator.resetDefaultMeanStepSizeLimitInNanometers();
      }      
    }
    else if(magnificationType.equals(MagnificationTypeEnum.LOW))
    {
      if( XrayTester.isHighMagnification())
      {
        XrayTester.changeMagnification(MagnificationEnum.NOMINAL);
        MechanicalConversions.reset();
        ImagingChainProgramGenerator.resetDefaultMeanStepSizeLimitInNanometers();
      }
    }
  }
  
  /**
   * @author sham
   */
  public void clear()
  {
    Assert.expect(_reconstructedImagesProducer != null);
   
	  _reconstructedImagesProducer.clear();
  }

  /**
   * @author Seng Yew
   */
  public void setIsGeneratingPrecisionImageSet(boolean isGeneratingPrecisionImageSet)
  {
    _isGeneratingPrecisionImageSet = isGeneratingPrecisionImageSet;
  }

  /**
   * @author Seng Yew
   */
  public boolean isGeneratingPrecisionImageSet()
  {
    return _isGeneratingPrecisionImageSet;
  }

  /**
   * @author Yong Sheng Chuan
   */
  public void setIsGenerateImageForExposureLearning(boolean isGenerateImageForExposureLearning)
  {
    _isGenerateImageForExposureLearning = isGenerateImageForExposureLearning;
  }

  /**
   * @author Yong Sheng Chuan
   */
  public boolean isGenerateImageForExposureLearning()
  {
    return _isGenerateImageForExposureLearning;
  }
  
  /**
   * @author Seng Yew
   */
  private void debug(String message)
  {
    if (_debug)
      System.out.println(_me + ": " + message);
  }
}
