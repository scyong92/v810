package com.axi.v810.business.imageAcquisition;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

/**
 * @author Matt Wharton
 */
public class Test_ReconstructedImagesMemoryManager extends UnitTest
{
  private ReconstructedImagesMemoryManager _memoryManager = ReconstructedImagesMemoryManager.getInstance();

  /**
   * @author Matt Wharton
   */
  private Test_ReconstructedImagesMemoryManager()
  {
    // Do nothing ...
  }

  /**
   * Main test entry point.
   *
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      _memoryManager.initialize();

      final int NUMBER_OF_IMAGES = 100;

      // Prepopulate some reconstruction regions.
      List<ReconstructionRegion> reconstructionRegions = new LinkedList<ReconstructionRegion>();
      for (int i = 0; i < NUMBER_OF_IMAGES; ++i)
      {                        
        ReconstructionRegion reconstructionRegion = new ReconstructionRegion();
        reconstructionRegion.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.INSPECTION);
        reconstructionRegion.setRegionId(i);        
        PanelRectangle panelRectangle = new PanelRectangle(0, 0, i * 25400, i * 25400);
        reconstructionRegion.setRegionRectangleRelativeToPanelInNanoMeters(panelRectangle);
        reconstructionRegion.setTopSide(true);

        reconstructionRegions.add(reconstructionRegion);
      }

      for (int i = 0; i < NUMBER_OF_IMAGES; ++i)
      {
        ReconstructionRegion reconstructionRegion = reconstructionRegions.get(i);

        ReconstructedImages reconstructedImages = new ReconstructedImages(reconstructionRegion);
        // 1024 * 1024 images are quite large -- we reduced the amount of memory on June 4 (software.config)
        // and there's not enough RAM now to store 100 of these, so I reduced the image size so they all can fit
        //Image image = new Image(1024, 1024);
        Image image = new Image(1024, 512);
        
        List<Float> arrayZHeight = new ArrayList<Float>();//add by BH
        List<Float> arraySharpness = new ArrayList<Float>();//add by BH
        reconstructedImages.addImage(image, SliceNameEnum.PAD, 0, arrayZHeight, arraySharpness);//add by BH
        image.decrementReferenceCount();

//        System.out.println("Before put -- fraction of memory used: " + _memoryManager.getFractionOfMemoryUsed());
        _memoryManager.putReconstructedImages(reconstructionRegion, ImageTypeEnum.AVERAGE, reconstructedImages);
//        System.out.println("After put -- fraction of memory used: " + _memoryManager.getFractionOfMemoryUsed());
      }

      for (int i = 0; i < NUMBER_OF_IMAGES; ++i)
      {
        ReconstructionRegion reconstructionRegion = reconstructionRegions.get(i);

        ReconstructedImages reconstructedImages = _memoryManager.getReconstructedImages(reconstructionRegion, ImageTypeEnum.AVERAGE);
        reconstructedImages.decrementReferenceCount();
      }

      for (int i = 0; i < NUMBER_OF_IMAGES; ++i)
      {
        ReconstructionRegion reconstructionRegion = reconstructionRegions.get(i);

//        System.out.println("Before remove -- fraction of memory used: " + _memoryManager.getFractionOfMemoryUsed());
        _memoryManager.freeReconstructedImages(reconstructionRegion, ImageTypeEnum.AVERAGE);
//        System.out.println("After remove -- fraction of memory used: " + _memoryManager.getFractionOfMemoryUsed());
      }
    }
    catch (Throwable ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ReconstructedImagesMemoryManager());
  }
}
