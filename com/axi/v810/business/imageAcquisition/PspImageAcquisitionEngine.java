package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * @author Cheah Lee Herng
 */
public abstract class PspImageAcquisitionEngine 
{  
  private static Map<PspModuleVersionEnum, PspImageAcquisitionEngine> _versionToInstanceMap;
  private static PerformanceLogUtil _performanceLog = PerformanceLogUtil.getInstance();
  
  protected static Config _config;
  
  protected PspHardwareEngine _pspHardwareEngine;
  protected PanelPositioner _panelPositioner;
  
  // Keep track of last loaded module and profile
  private PspModuleEnum _previousPspModuleEnum = null;
  private OpticalCalibrationProfileEnum _previousOpticalCalibrationProfileEnum = null;
  
  //Motion profile to use when moving stage, use the fastest
  // XCR-3695 XY-Axis Position Error Limit During PSP Setup
  protected PointToPointMotionProfileEnum _motionProfile = PointToPointMotionProfileEnum.PROFILE2;
  
  static
  {
    _config = Config.getInstance();
    _versionToInstanceMap = new HashMap<PspModuleVersionEnum, PspImageAcquisitionEngine>();
  }
  
  // This will compare the list of Pair<OpticalShiftNameEnum, String>
  private Comparator<Pair<OpticalShiftNameEnum, String>> _opticalShiftNamePairComparator = new Comparator<Pair<OpticalShiftNameEnum, String>>()
  {
    /**
     * @author Cheah Lee Herng
     */
    public int compare(Pair<OpticalShiftNameEnum, String> lhs, Pair<OpticalShiftNameEnum, String> rhs)
    {
      OpticalShiftNameEnum lhsOpticalShiftNameEnum = lhs.getFirst();
      OpticalShiftNameEnum rhsOpticalShiftNameEnum = rhs.getFirst();
      
      String lhsImageFullPath = lhs.getSecond();
      String rhsImageFullPath = rhs.getSecond();
      
      if (lhsOpticalShiftNameEnum.getId() < rhsOpticalShiftNameEnum.getId())
        return -1;
      else if (lhsOpticalShiftNameEnum.getId() > rhsOpticalShiftNameEnum.getId())
        return 1;
      else
      {
        return lhsImageFullPath.compareTo(rhsImageFullPath);
      }
    }
  };
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized static PspImageAcquisitionEngine getInstance()
  {    
    PspImageAcquisitionEngine instance = null;
    
    PspModuleVersionEnum versionEnum = getPspModuleVersionEnum();
    
    if (_versionToInstanceMap.containsKey(versionEnum))
    {
      instance = _versionToInstanceMap.get(versionEnum);
    }
    else
    {
      if (versionEnum.equals(PspModuleVersionEnum.VERSION_1))
      {
        instance = new PspImageAcquisitionEngineVersion1();
      }
      else if (versionEnum.equals(PspModuleVersionEnum.VERSION_2))
      {
        instance = new PspImageAcquisitionEngineVersion2();
      }      
      else
        Assert.expect(false);
    }

    Assert.expect(instance != null);
    _versionToInstanceMap.put(versionEnum, instance);

    return instance;
  }
  
  /**
   * Default Constructor
   * 
   * @author Cheah Lee Herng
   */
  protected PspImageAcquisitionEngine()
  {
    _panelPositioner = PanelPositioner.getInstance();
    _pspHardwareEngine = PspHardwareEngine.getInstance();
  }
  
  /**
    * @author Cheah Lee Herng
    */
  public static PspModuleVersionEnum getPspModuleVersionEnum()
  {
    int version = _config.getIntValue(HardwareConfigEnum.PSP_MODULE_VERSION);
    return PspModuleVersionEnum.getEnum(version);
  }
  
  /**
   * @author Cheah Lee Herng
  */
  public OpticalCameraIdEnum getOpticalCameraIdEnum(PspModuleEnum moduleEnum)
  {
    Assert.expect(moduleEnum != null);
    
    OpticalCameraIdEnum cameraId = null;
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      cameraId = OpticalCameraIdEnum.OPTICAL_CAMERA_1;
    else
      cameraId = OpticalCameraIdEnum.OPTICAL_CAMERA_2;
    return cameraId;
  }
  
  /**
    * @author Cheah Lee Herng
    */
  public PspModuleEnum getPspModuleEnum(OpticalCameraIdEnum cameraId)
  {
    Assert.expect(cameraId != null);

    PspModuleEnum moduleEnum = null;
    if (cameraId.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1))
      moduleEnum = PspModuleEnum.FRONT;
    else
      moduleEnum = PspModuleEnum.REAR;
    Assert.expect(moduleEnum != null);

    return moduleEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void moveStage(StagePosition stagePosition) throws XrayTesterException
  {
    Assert.expect(stagePosition != null);
    
    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_POINT_TO_POINT_MOVE_STAGE);
       
    // Save off the previously loaded motion profile
    MotionProfile originalMotionProfile = _panelPositioner.getActiveMotionProfile();

    // First, move stage to Object plane position
    _panelPositioner.setMotionProfile(new MotionProfile(_motionProfile));
    _panelPositioner.pointToPointMoveAllAxes(stagePosition);

    // Now that we are done with the move, restore the motion profile.
    _panelPositioner.setMotionProfile(originalMotionProfile);
    
    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_POINT_TO_POINT_MOVE_STAGE);
  }
  
  /**
   * This function will initialize the necessary hardware (eg: Light Engine, Projector, MicroController).
   * It will handle the optical calibration profile loading and image ROI setup.
   * 
   * @author Cheah Lee Herng 
  */
  public void initialize(PspModuleEnum moduleEnum, OpticalPointToPointScan opticalPointToPointScan) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalPointToPointScan != null);
    
    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_OPTICAL_CAMERA_INITIALIZATION);
    
    // For debugging purpose. This is important for Manufacturing to check the buy-off
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
    {
      System.out.println("PspImageAcquisitionEngine: Initialize PSP Image Acquisition Engine");
    }
    
    // XCR-2413 Prompt error and ask user to run Optical Calibration task if there is no existing calibration data 
    checkOpticalCalibrationDataExist();
    
    OpticalCalibrationProfileEnum opticalCalibrationProfileEnum = opticalPointToPointScan.getOpticalCalibrationProfileEnum();
    
    if (_previousPspModuleEnum == null || _previousPspModuleEnum.equals(moduleEnum) == false ||
        _previousOpticalCalibrationProfileEnum == null || _previousOpticalCalibrationProfileEnum.equals(opticalCalibrationProfileEnum) == false)
    {
      _previousPspModuleEnum = moduleEnum;
      _previousOpticalCalibrationProfileEnum = opticalCalibrationProfileEnum;
      
      String opticalCalibrationFullPath = getCalibrationFullPath(moduleEnum, opticalCalibrationProfileEnum);
      loadOpticalCameraCalibrationData(moduleEnum, opticalCalibrationProfileEnum, opticalCalibrationFullPath);
      
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
      {
        System.out.println("PspEngine: initializeOpticalImageAcquisition: Optical Calibration Profile " + 
                            opticalCalibrationProfileEnum.getId() + " for module " + moduleEnum.getId() + " has been loaded. Path is " + 
                            opticalCalibrationFullPath);
      }
    }
      
    // Setup magnification data. Sequence of input data are as follows:
    // These sequence must be followed or it will cause inconsistent result.
    //
    // -2,0,+3 / camera1 / [@-2]
    // -2,0,+3 / camera1 / [@0]
    // -2,0,+3 / camera1 / [@+3]
    // -2,0,+3 / camera2 / [@-2]
    // -2,0,+3 / camera2 / [@0]
    // -2,0,+3 / camera2 / [@+3]
    // -1,0,+4 / camera1 / [@-1]
    // -1,0,+4 / camera1 / [@0]
    // -1,0,+4 / camera1 / [@+4]
    // -1,0,+4 / camera2 / [@-1]
    // -1,0,+4 / camera2 / [@0]
    // -1,0,+4 / camera2 / [@+4]

    double[] magnificationData = new double[12];

    // Magnification data for Profile 1
    magnificationData[0]  = _config.getDoubleValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION);
    magnificationData[1]  = _config.getDoubleValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_MAGNIFICATION);
    magnificationData[2]  = _config.getDoubleValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION);
    magnificationData[3]  = _config.getDoubleValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION);
    magnificationData[4]  = _config.getDoubleValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_MAGNIFICATION);
    magnificationData[5]  = _config.getDoubleValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION);

    // Magnification data for Profile 2
    magnificationData[6]  = _config.getDoubleValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION);
    magnificationData[7]  = _config.getDoubleValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_MAGNIFICATION);
    magnificationData[8]  = _config.getDoubleValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION);
    magnificationData[9]  = _config.getDoubleValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION);
    magnificationData[10] = _config.getDoubleValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_MAGNIFICATION);
    magnificationData[11] = _config.getDoubleValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION);

    Assert.expect(magnificationData.length == 12);

    // Setup processing ROI
    Map<String,Pair<Integer,Integer>> pointPositionInPixels = opticalPointToPointScan.getPointPanelPositionNameToPointPositionInPixel();
    int roiCount = pointPositionInPixels.size();
    int[] centerX = new int[roiCount];
    int[] centerY = new int[roiCount];
    int[] width = new int[roiCount];
    int[] height = new int[roiCount];

    int counter = 0;
    for(Map.Entry<String,Pair<Integer,Integer>> entry : pointPositionInPixels.entrySet())
    {
        Pair<Integer,Integer> pointPositionInPixel = entry.getValue();
        centerX[counter] = pointPositionInPixel.getFirst().intValue();
        centerY[counter] = pointPositionInPixel.getSecond().intValue();
        //width[counter] = getImageRoiWidth();
        //height[counter] = getImageRoiHeight();
        width[counter] = opticalPointToPointScan.getRoiWidth();
        height[counter] = opticalPointToPointScan.getRoiHeight();

        ++counter;
    }
    Assert.expect(counter == roiCount);

    AbstractOpticalCamera abstractOpticalCamera = AbstractOpticalCamera.getInstance(_pspHardwareEngine.getOpticalCameraIdEnum(moduleEnum));
    //Ying-Huan.Chu - temporary commented Larger FOV feature for 5.7 release.
    //int frontModuleCropImageOffsetXInPixel = abstractOpticalCamera.isLargerFov() ? getLargerFovFrontModuleCropImageOffsetXInPixel() : getFrontModuleCropImageOffsetXInPixel();
    //int rearModuleCropImageOffsetXInPixel = abstractOpticalCamera.isLargerFov() ? getLargerFovRearModuleCropImageOffsetXInPixel() : getRearModuleCropImageOffsetXInPixel();
    int frontModuleCropImageOffsetXInPixel = getFrontModuleCropImageOffsetXInPixel();
    int rearModuleCropImageOffsetXInPixel = getRearModuleCropImageOffsetXInPixel();
    
    OpticalCameraSetting opticalCameraSetting = new OpticalCameraSetting(magnificationData,
                                                                          roiCount, 
                                                                          centerX, 
                                                                          centerY, 
                                                                          width, 
                                                                          height, 
                                                                          uEyeOpticalCamera.OPTICAL_CAMERA_FRAMES_PER_SECOND,
                                                                          false,
                                                                          false,
                                                                          false,
                                                                          frontModuleCropImageOffsetXInPixel,
                                                                          rearModuleCropImageOffsetXInPixel);

    // Initialize PSP Optical Camera properties
    initializeOpticalCamera(moduleEnum, opticalCameraSetting);
    
    // De-reference object
    if (opticalCameraSetting != null)
    {
      opticalCameraSetting.clear();
      opticalCameraSetting = null;
    }
    
    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_OPTICAL_CAMERA_INITIALIZATION);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void initializeOpticalCamera(PspModuleEnum moduleEnum, OpticalCameraSetting cameraSetting) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(cameraSetting != null);
    
    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_ENABLE_OPTICAL_CAMERA_HARDWARE_TRIGGER);
    
    // Prepare camere in good known state
    if (cameraSetting.isLiveViewMode())
      _pspHardwareEngine.initializeOpticalCamera(moduleEnum);
    else
      _pspHardwareEngine.enableOpticalCameraHardwareTrigger(moduleEnum);
    
    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_ENABLE_OPTICAL_CAMERA_HARDWARE_TRIGGER);
    
    if (cameraSetting.hasMagnificationData())
      _pspHardwareEngine.setOpticalCameraMagnificationData(moduleEnum, cameraSetting.getMagnificationData());
    
    if (cameraSetting.hasRoiData())
      _pspHardwareEngine.setOpticalCameraInspectionRoi(moduleEnum, 
                                                       cameraSetting.getRoiCount(), 
                                                       cameraSetting.getRoiCenterX(), 
                                                       cameraSetting.getRoiCenterY(), 
                                                       cameraSetting.getRoiWidth(), 
                                                       cameraSetting.getRoiHeight());
    
    _pspHardwareEngine.enableOpticalCameraAutoGain(moduleEnum, cameraSetting.isAutoGainEnabled());
    _pspHardwareEngine.enableOpticalCameraAutoSensorGain(moduleEnum, cameraSetting.isAutoSensorGainEnabled());
    _pspHardwareEngine.enableOpticalCameraGainBoost(moduleEnum, cameraSetting.isGainBoostEnabled());
    
    _pspHardwareEngine.setOpticalCameraFrameRatePerSecond(moduleEnum, cameraSetting.getFrameRatePerSecond());
    _pspHardwareEngine.setOpticalCameraMasterGainFactor(moduleEnum, (int)getOpticalCameraGainFactor(moduleEnum));
        
    _pspHardwareEngine.setOpticalCameraCropImageOffset(moduleEnum, 
                                                       cameraSetting.getFrontModuleCropImageOffsetXInPixel(), 
                                                       cameraSetting.getRearModuleCropImageOffsetXInPixel());
    
    // If we enable developer debug mode, we will dump out the information into log file (for C++ side)
    // Cheah Lee Herng 18 Oct 2013 - Temporary disable info dumping because it will cause system crash.
    //if (Config.isDeveloperDebugModeOn())
    //  _pspHardwareEngine.setOpticalCameraDebugMode(moduleEnum, true);
    //else
    //  _pspHardwareEngine.setOpticalCameraDebugMode(moduleEnum, false);
    _pspHardwareEngine.setOpticalCameraDebugMode(moduleEnum, false);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void loadOpticalCameraCalibrationData(PspModuleEnum moduleEnum, 
                                               OpticalCalibrationProfileEnum opticalCalibrationProfileEnum,
                                               String opticalCalibrationFullPath) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalCalibrationProfileEnum != null);
    Assert.expect(opticalCalibrationFullPath != null);
    
    // If there is no directory created, there will be no calibration data
    // generated, which caused PSP not functioning.
    if (FileUtilAxi.existsDirectory(opticalCalibrationFullPath) == false)
    {
      FileUtilAxi.createDirectory(opticalCalibrationFullPath);
    }
    
    _pspHardwareEngine.loadCalibrationData(moduleEnum, opticalCalibrationFullPath);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private double getOpticalCameraGainFactor(PspModuleEnum moduleEnum)
  {
    Assert.expect(moduleEnum != null);

    double cameraGainFactor = 0.0;
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      cameraGainFactor = _config.getDoubleValue(PspCalibEnum.OPTICAL_CAMERA_1_GAIN_FACTOR);
    else if (moduleEnum.equals(PspModuleEnum.REAR))
      cameraGainFactor = _config.getDoubleValue(PspCalibEnum.OPTICAL_CAMERA_2_GAIN_FACTOR);
    else
      Assert.expect(false, "Invalid camera id.");
    return cameraGainFactor;
  }
  
//  /**
//   * @author Cheah Lee Herng
//   */
//  public int getImageRoiCenterX()
//  {
//    return _pspHardwareEngine.getImageRoiCenterX();
//  }
//  
//  /**
//   * @author Cheah Lee Herng
//   */
//  public int getImageRoiCenterY()
//  {
//    return _pspHardwareEngine.getImageRoiCenterY();
//  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getImageRoiWidth()
  {
    return _pspHardwareEngine.getImageRoiWidth();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getImageRoiHeight()
  {
    return _pspHardwareEngine.getImageRoiWidth();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void abort() throws XrayTesterException
  {
    _pspHardwareEngine.abort();
  }
  
  /**
   * This is main function to perform calibration image acquisition.
   * It will loop through all the necessary stage position defined in OpticalCalibrationData
   * for acquiring calibration images.
   * 
   * @author Cheah Lee Herng 
   */
  public void acquireCalibrationImage(OpticalCalibrationData opticalCalibrationData) throws XrayTesterException
  {
    Assert.expect(opticalCalibrationData != null);
    
    for(int i=0; i < opticalCalibrationData.getTotalRun(); i++)
    {
      Map<OpticalCalibrationProfileEnum, Map<OpticalCalibrationPlaneEnum, List<Pair<OpticalShiftNameEnum, String>>>> profileStagePositionMap = opticalCalibrationData.getProfileStagePositionMap();
      for(Map.Entry<OpticalCalibrationProfileEnum, Map<OpticalCalibrationPlaneEnum, List<Pair<OpticalShiftNameEnum, String>>>> entry : profileStagePositionMap.entrySet())
      {
        Map<OpticalCalibrationPlaneEnum, List<Pair<OpticalShiftNameEnum, String>>> stagePositionMap = entry.getValue();
        for(Map.Entry<OpticalCalibrationPlaneEnum, List<Pair<OpticalShiftNameEnum, String>>> entry2 : stagePositionMap.entrySet())
        {
          OpticalCalibrationPlaneEnum planeEnum = entry2.getKey();
          List<Pair<OpticalShiftNameEnum, String>> imageFullPathList = entry2.getValue();
          Collections.sort(imageFullPathList, _opticalShiftNamePairComparator);
          
          // Get stage position
          StagePositionMutable stagePositionMutable = opticalCalibrationData.getStagePosition(planeEnum);
                    
          // Move to correct calibration plate stage position
          moveStage(stagePositionMutable);
          
          // Start actual image acquisition
          for(Pair<OpticalShiftNameEnum, String> pair : imageFullPathList)
          {
            OpticalShiftNameEnum opticalShiftNameEnum = pair.getFirst();
            String imageFullPath = pair.getSecond();
            
            runCalibrationImageAcquisition(opticalCalibrationData.getPspModuleEnum(), planeEnum, opticalShiftNameEnum, imageFullPath);
          }
        }
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void acquireHeightMapImage(OpticalInspectionData opticalInspectionData, boolean moveStage) throws XrayTesterException
  {
    Assert.expect(opticalInspectionData != null);
    try
    {
      Map<StagePosition, List<Pair<OpticalShiftNameEnum, String>>> stagePositionMap = opticalInspectionData.getStagePositionMap();
      for(Map.Entry<StagePosition, List<Pair<OpticalShiftNameEnum, String>>> entry : stagePositionMap.entrySet())
      {
        StagePosition stagePosition = entry.getKey();
        List<Pair<OpticalShiftNameEnum, String>> imageFullPathList = entry.getValue();
        Collections.sort(imageFullPathList, _opticalShiftNamePairComparator);

        if (moveStage)
        {
          // Move to correct calibration plate stage position
          moveStage(stagePosition);
        }

        // Start actual image acquisition
        for(Pair<OpticalShiftNameEnum, String> pair : imageFullPathList)
        {
          OpticalShiftNameEnum opticalShiftNameEnum = pair.getFirst();
          String imageFullPath = pair.getSecond();

          _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_ACQUIRE_OPTICAL_IMAGE);

          runHeightMapImageAcquisition(opticalInspectionData, 
                                       opticalShiftNameEnum,  
                                       imageFullPath);
          _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_ACQUIRE_OPTICAL_IMAGE);
        }
        
        if (moveStage)
        {
          checkIfHeightMapIsAvailable(opticalInspectionData.getPspModuleEnum());
        }
      }
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void acquireOfflineHeightMapImage(PspModuleEnum moduleEnum, 
                                           OpticalCalibrationProfileEnum opticalCalibrationProfileEnum, 
                                           final String imageFullPath) throws XrayTesterException
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(opticalCalibrationProfileEnum != null);
    Assert.expect(imageFullPath != null);
    
    com.axi.util.image.Image image = null;    
    try
    {
      // Load offline image
      image = XrayImageIoUtil.loadPngImage(imageFullPath);
      com.axi.util.image.RegionOfInterest regionOfInterest = RegionOfInterest.createRegionFromImage(image);

      _pspHardwareEngine.generateOfflineHeightMap(moduleEnum,
                                                  image,
                                                  regionOfInterest,
                                                  opticalCalibrationProfileEnum.getHeightAdjustment(), 
                                                  opticalCalibrationProfileEnum.getId());
    }
    catch(XrayTesterException ex)
    {
      throw ex;
    }
    finally
    {
      if (image != null)
      {
        image.decrementReferenceCount();
        image = null;
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void acquireSingleImage(OpticalInspectionData opticalInspectionData) throws XrayTesterException
  {
    Assert.expect(opticalInspectionData != null);
    
    Map<StagePosition, List<Pair<OpticalShiftNameEnum, String>>> stagePositionMap = opticalInspectionData.getStagePositionMap();
    for(Map.Entry<StagePosition, List<Pair<OpticalShiftNameEnum, String>>> entry : stagePositionMap.entrySet())
    {
      StagePosition stagePosition = entry.getKey();
      List<Pair<OpticalShiftNameEnum, String>> imageFullPathList = entry.getValue();
      Collections.sort(imageFullPathList, _opticalShiftNamePairComparator);
      
      // Move to correct calibration plate stage position, if a stage position is defined
      if (stagePosition.getXInNanometers() >= 0 &&
          stagePosition.getYInNanometers() >= 0)
      {
        moveStage(stagePosition);
      }

      // Start actual image acquisition
      for(Pair<OpticalShiftNameEnum, String> pair : imageFullPathList)
      {
        OpticalShiftNameEnum opticalShiftNameEnum = pair.getFirst();
        String imageFullPath = pair.getSecond();

        runSingleImageAcquisition(opticalInspectionData, 
                                  opticalShiftNameEnum,  
                                  imageFullPath);
      }
    }
  }
  
   /**
   * @author Cheah Lee Herng 
   */
  public void acquireAlignmentHeightMapImage(OpticalInspectionData opticalInspectionData) throws XrayTesterException
  {
    Assert.expect(opticalInspectionData != null);
    
    try
    {
      Map<StagePosition, List<Pair<OpticalShiftNameEnum, String>>> stagePositionMap = opticalInspectionData.getStagePositionMap();
      for(Map.Entry<StagePosition, List<Pair<OpticalShiftNameEnum, String>>> entry : stagePositionMap.entrySet())
      {
        StagePosition stagePosition = entry.getKey();
        List<Pair<OpticalShiftNameEnum, String>> imageFullPathList = entry.getValue();
        Collections.sort(imageFullPathList, _opticalShiftNamePairComparator);

        // Move to correct calibration plate stage position
        Assert.expect(stagePosition != null);

        // Save off the previously loaded motion profile
        MotionProfile originalMotionProfile = _panelPositioner.getActiveMotionProfile();

        // First, move stage to Object plane position
        _panelPositioner.setMotionProfile(new MotionProfile(_motionProfile));
        _panelPositioner.pointToPointMoveAllAxes(stagePosition);

        // Now that we are done with the move, restore the motion profile.
        _panelPositioner.setMotionProfile(originalMotionProfile);

        // Start actual image acquisition
        for(Pair<OpticalShiftNameEnum, String> pair : imageFullPathList)
        {
          OpticalShiftNameEnum opticalShiftNameEnum = pair.getFirst();
          String imageFullPath = pair.getSecond();

          runHeightMapImageAcquisition(opticalInspectionData, 
                                       opticalShiftNameEnum,  
                                       imageFullPath);
        }
        checkIfHeightMapIsAvailable(opticalInspectionData.getPspModuleEnum());
      }
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
  }
  
  // Public abstract function
  public abstract void handleActualOpticalView(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException;
  public abstract void handlePostOpticalView(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException;
  public abstract void handlePostImageDisplay(PspModuleEnum moduleEnum) throws XrayTesterException;
  public abstract int getFrontModuleCropImageOffsetXInPixel();
  public abstract int getRearModuleCropImageOffsetXInPixel();
  public abstract int getLargerFovFrontModuleCropImageOffsetXInPixel();
  public abstract int getLargerFovRearModuleCropImageOffsetXInPixel();
  public abstract void checkIfHeightMapIsAvailable(PspModuleEnum pspModuleEnum) throws XrayTesterException;
  
  // Protected abstract function
  protected abstract void initializeOpticalImageAcquistion(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException;
  protected abstract void performOpticalImageAcquisition(PspModuleEnum moduleEnum, OpticalShiftNameEnum opticalShiftEnum) throws XrayTesterException;
  protected abstract void finishOpticalImageAcquisition(PspModuleEnum moduleEnum) throws XrayTesterException;
  protected abstract void runCalibrationImageAcquisition(PspModuleEnum moduleEnum, OpticalCalibrationPlaneEnum planeEnum, OpticalShiftNameEnum opticalShiftEnum, String imageFullPath) throws XrayTesterException;
  protected abstract void runHeightMapImageAcquisition(final OpticalInspectionData opticalInspectionData, OpticalShiftNameEnum opticalShiftEnum, String imageFullPath) throws XrayTesterException;
  protected abstract void runSingleImageAcquisition(final OpticalInspectionData opticalInspectionData, OpticalShiftNameEnum opticalShiftEnum, String imageFullPath) throws XrayTesterException;
  protected abstract String getCalibrationFullPath(PspModuleEnum moduleEnum, OpticalCalibrationProfileEnum opticalCalibrationProfileEnum) throws XrayTesterException;
  protected abstract void checkOpticalCalibrationDataExist() throws XrayTesterException;
}
