package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

/**
 * A heterogeneous image group is an image group in which the entire region
 * to be imaged may use more than one step size.  This type of image group
 * is created during scan pass optimization.
 *
 * @author Roy Williams
 * @author Kay Lannen
 */
public class HeterogeneousImageGroup extends ImageGroup
{
  /**
   * @author Roy Williams
   */
  public HeterogeneousImageGroup(Collection<ReconstructionRegion> regions)
  {
    super(regions);
  }

  /**
   * @author Kay Lannen
   */
  public HeterogeneousImageGroup(ImageGroup imageGroup)
  {
    setUnSortedRegions(imageGroup.getUnsortedRegions());
    setRegionPairsSortedInExecutionOrder( imageGroup.getRegionPairsSortedInExecutionOrder() );
    setNominalAreaToImage( imageGroup.getNominalAreaToImage() );
    setPanelRectangleToImage( imageGroup.getPanelRectangleToImage() );
    setScanPath( imageGroup.getScanPath() );
    setImageReconstructionEngines( imageGroup.getImageReconstructionEngines() );
    SortedSet<ImageZone> myImageZones = getImageZones();
    for (ImageZone imageZone : imageGroup.getImageZones())
    {
      myImageZones.add(imageZone);
    }
  }

  /**
   * @author Roy Williams
   */
  public void add(ImageGroup otherGroup)
  {
    List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionPairsForMain = getRegionPairsSortedInExecutionOrder();
    List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionPairsForOther = otherGroup.getRegionPairsSortedInExecutionOrder();
    List<ReconstructionRegion> allRegions = new ArrayList<ReconstructionRegion>(regionPairsForMain.size() +
                                                                                regionPairsForOther.size());
    for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : regionPairsForMain)
    {
      allRegions.add(regionPair.getFirst());
    }
    for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : regionPairsForOther)
    {
      allRegions.add(regionPair.getFirst());
    }
    super.initialize(allRegions);
  }

}
