package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testGen.ProgramGeneration;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * The main testSubProgram with its main ScanPath plus Rescan groups each
 * constructing their own ScanPath use this.
 *
 * A homogeneous image group is an image group in which the entire region
 * to be imaged uses the same mean step size.
 *
 * @author Roy Williams
 * @author Kay Lannen
 */
public class HomogeneousImageGroup extends ImageGroup
{
  // _intersectionMethod = METHOD_ORIGINAL means using legacy method for software version 3.1 or older
  // _intersectionMethod = METHOD_OPTIMIZED_LOADING means optimized loading only for software version 3.2 or newer. (this setting does not affect this class)
  // _intersectionMethod = METHOD_OPTIMIZED_LOADING_AND_STEP_SIZE means corrected maximum step size in RangeUtil in ReconstructionRegion for software version 3.2 or newer
  // Default value is 2.
  // Value of 2 is the correct way of intersection, but to make sure older recipes does not change their behaviour, this variable is used to turn off for older recipes.
  private static ScanPathMethodEnum _intersectionMethod = ScanPathMethodEnum.METHOD_OPTIMIZED_LOADING_AND_STEP_SIZE;

  /**
   * This is only used by RescanShadedRegionsBasedUponMaxCandidateStepSizeScanStrategy.
   *
   * @author Roy Williams
   */
  public HomogeneousImageGroup(Collection<ReconstructionRegion> unsortedRegions, int overrideComputedCandidateStepSizeValue)
  {
    super(unsortedRegions);
    Assert.expect(unsortedRegions != null);
    Pair<Integer, Integer> validRange = new Pair<Integer, Integer>(overrideComputedCandidateStepSizeValue,
                                                                   overrideComputedCandidateStepSizeValue);
    List<Pair<Integer, Integer>> confirmedCandidateStepRanges = new ArrayList<Pair<Integer, Integer>>();
    confirmedCandidateStepRanges.add(validRange);
    initImageZone( overrideComputedCandidateStepSizeValue, XrayTester.getMaxStepSizeDitherAmplitudeInNanometers(),
       confirmedCandidateStepRanges);
  }

  /**
   * @author Roy Williams
   */
  public HomogeneousImageGroup(Collection<ReconstructionRegion> unsortedRegions)
  {
    super(unsortedRegions);
    Assert.expect(unsortedRegions != null);

    // Figure out what ranges were provided during ProgramGeneration.
    List<Pair<Integer, Integer>> confirmedCandidateStepRanges = identifyCandidateStepIntersections(unsortedRegions);
    if (confirmedCandidateStepRanges.size() == 0)
    {
      confirmedCandidateStepRanges.add(ImagingChainProgramGenerator.defaultCandidateStepRange());
    }
    
    // Calculate the max in the range list which should be our mean step.   We must
    // be able to add the dither amplitude for the sine wave.
    int meanStepSizeInNanoMeters, ditherAmplitude;
    meanStepSizeInNanoMeters = RangeUtil.getMaxInRangeList(confirmedCandidateStepRanges);
    ditherAmplitude = XrayTester.getMaxStepSizeDitherAmplitudeInNanometers();
//    Assert.expect(meanStepSizeInNanoMeters + ditherAmplitude <= ImagingChainProgramGenerator.getMaxStepSizeLimitInNanometers());

    // The min is a bit more flexible. We are allowed to reduce the dither amplitude.
    //Assert.expect(candidateStepRange.getSecond() <= ImagingChainProgramGenerator.getMinStepSizeLimitInNanometers());
    int minStepSizeLimitInNanometers = ImagingChainProgramGenerator.getMinStepSizeLimitInNanometers();
    if (meanStepSizeInNanoMeters - ditherAmplitude < minStepSizeLimitInNanometers)
    {
      ditherAmplitude = meanStepSizeInNanoMeters - minStepSizeLimitInNanometers;
    }
    
    if(ditherAmplitude <= 0)
      ditherAmplitude = 0;
    
    Assert.expect(ditherAmplitude >= 0);

    initImageZone( meanStepSizeInNanoMeters, ditherAmplitude, confirmedCandidateStepRanges);

  }
  
  /**
   * add for faster generating time for new scan route.
   * 
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public HomogeneousImageGroup(Collection<ReconstructionRegion> unsortedRegions, boolean bDummy)
  {
    super(unsortedRegions);
    Assert.expect(unsortedRegions != null);

    // Figure out what ranges were provided during ProgramGeneration.
    List<Pair<Integer, Integer>> confirmedCandidateStepRanges = new ArrayList<Pair<Integer, Integer>>();
    confirmedCandidateStepRanges.add(ImagingChainProgramGenerator.defaultCandidateStepRange());

    // Calculate the max in the range list which should be our mean step.   We must
    // be able to add the dither amplitude for the sine wave.
    int meanStepSizeInNanoMeters, ditherAmplitude;
    meanStepSizeInNanoMeters = RangeUtil.getMaxInRangeList(confirmedCandidateStepRanges);
    ditherAmplitude = XrayTester.getMaxStepSizeDitherAmplitudeInNanometers();
//    Assert.expect(meanStepSizeInNanoMeters + ditherAmplitude <= ImagingChainProgramGenerator.getMaxStepSizeLimitInNanometers());

    // The min is a bit more flexible. We are allowed to reduce the dither amplitude.
    //Assert.expect(candidateStepRange.getSecond() <= ImagingChainProgramGenerator.getMinStepSizeLimitInNanometers());
    int minStepSizeLimitInNanometers = ImagingChainProgramGenerator.getMinStepSizeLimitInNanometers();
    if (meanStepSizeInNanoMeters - ditherAmplitude < minStepSizeLimitInNanometers)
    {
      ditherAmplitude = meanStepSizeInNanoMeters - minStepSizeLimitInNanometers;
    }
    
    if(ditherAmplitude <= 0)
      ditherAmplitude = 0;
    
    Assert.expect(ditherAmplitude >= 0);

    initImageZone( meanStepSizeInNanoMeters, ditherAmplitude, confirmedCandidateStepRanges);
  }

  /**
   * @author Kay Lannen
   */
  private void initImageZone( int meanStepSizeInNanoMeters, int ditherAmplitude,
      List<Pair<Integer, Integer>> confirmedCandidateStepRanges)
  {
    SortedSet<ImageZone> imageZoneSet = super.getImageZones();
    Assert.expect( imageZoneSet.size() == 0 );  // Exactly one image zone in a HomogeneousImageGroup

    ImageZone imageZone = new ImageZone();
    imageZone.setMeanStepSizeInNanoMeters( meanStepSizeInNanoMeters );
    imageZone.setDitherAmplitude( ditherAmplitude );
    imageZone.setScanPath( getScanPath() );
    imageZone.setConfirmedCandidateStepRanges( confirmedCandidateStepRanges );
    imageZoneSet.add(imageZone);
  }

  /**
   * @author Kay Lannen
   */
  public SortedSet<ImageZone> getImageZones()
  {
    SortedSet<ImageZone> imageZones = super.getImageZones();
    Assert.expect( imageZones.size() == 1 );
    return (imageZones);
  }

  /**
   * @author Roy Williams
   */
  public int getMaxStepSizeInNanoMeters()
  {
    ImageZone imageZone = getImageZones().first();
    return ( imageZone.getMeanStepSizeInNanoMeters() + imageZone.getDitherAmplitude() );
  }

  /**
   * @author Roy Williams
   */
  public int getMeanStepSizeInNanoMeters()
  {
    return( getImageZones().first().getMeanStepSizeInNanoMeters() );
  }

  /**
   * @author Roy Williams
   */
  public int getMinStepSizeInNanoMeters()
  {
    ImageZone imageZone = getImageZones().first();
    return ( imageZone.getMeanStepSizeInNanoMeters() - imageZone.getDitherAmplitude() );
  }

  /**
   * @author Roy Williams
   */
  public int getDitherAmplitudeInNanoMeters()
  {
    return( getImageZones().first().getDitherAmplitude() );
  }

  /**
   * @author Roy Williams
   */
  public List<Pair<Integer, Integer>> confirmedCandidateStepRanges()
  {
    return( getImageZones().first().getConfirmedCandidateStepRanges() );
  }


  /**
   * This estimator matches the computation used in generateScanPathForCustomListOfReconstructionRegions.
   * It is a more accurate estimator than estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSize()
   * but the old version is being kept for now for backward compatibility purposes.  There is some
   * concern that generating scan passes differently may cause problems in the field.  Hence we have
   * a "MethodA" (including alignment border) and a "MethodB" (backwards compatible, not including alignment
   * border).
   * @author Kay Lannen
   */
  public double estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSizeMethodA()
  {
    SystemFiducialRectangle nominalAreaToImage = getNominalAreaToImage();
    return(ImageGroup.estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSizeMethodA(
      nominalAreaToImage.getWidth(),
      nominalAreaToImage.getHeight(),
      getMeanStepSizeInNanoMeters() ));
  }

  /**
   * This estimator uses the nominal area to image and the mean step size over that area
   * to estimate the test execution time.  It can be used prior to generating the scan
   * path.  It does not include the scan passes for the alignment border, so it may be
   * inaccurate particularly for small regions, but it is being retained for backward compatibility.     There is some
   * concern that generating scan passes differently may cause problems in the field.
   * @author Roy Williams
   */
  public double estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSizeMethodB()
  {
    SystemFiducialRectangle nominalAreaToImage = getNominalAreaToImage();
    MachineRectangle cameraArrayRectangle = MechanicalConversions.getCameraArrayRectangleAtMinSliceHeight();
//    ScanStrategy.printRectangle(cameraArrayRectangle);
    int imageableWidthOfSingleCamera = _xrayCameraArray.getImageableWidthInNanometers(MagnificationEnum.getCurrentMinSliceHeight());
    int maxOffsetDeductedFromCameraArrayForOverlap = imageableWidthOfSingleCamera + // imageableWidthOfFirstCamera;
                                                     imageableWidthOfSingleCamera; // imageableWidthOfLastCamera

    // If nominal area to image is a single reconstruction region, then its size is significantly
    // less than the size of the camera array.  Use the lesser of the camera array widths and
    // the nominal area to image width.
    int offsetDeductedFromCameraArrayForOverlap = Math.min(maxOffsetDeductedFromCameraArrayForOverlap,
                                                           nominalAreaToImage.getWidth());

    int xTravelRequired = cameraArrayRectangle.getWidth() +
                          nominalAreaToImage.getWidth() -
                          offsetDeductedFromCameraArrayForOverlap;

    int meanStepSizeInNanoMeters = getMeanStepSizeInNanoMeters();
    int numberOfSteps = calculateNumberOfScanSteps(xTravelRequired, meanStepSizeInNanoMeters);
    int yTravelRequired = nominalAreaToImage.getHeight() + cameraArrayRectangle.getHeight();
    double estimatedTravel = (double)numberOfSteps * yTravelRequired;
    double estimatedTravelTime = estimatedTravel / PanelPositioner.getVelocityForScanMotionProfileInNanometersPerSecond();
    double estimatedTurnaroundTime = numberOfSteps * PanelPositioner.getAverageTurnaroundTimeInSeconds();
    return estimatedTravelTime + estimatedTurnaroundTime;
  }

  /**
   * @author Chnee Khang Wah, 2013-04-14
   * Calculate Entropy of step range (base on IL) for each regions
   * Choose the step size with highest entropy among steps
   */
  private static List<Pair<Integer, Integer>> identifyCandidateStepBaseOnEntropy(Collection<ReconstructionRegion> regions,
                                                                                 List<Pair<Integer, Integer>> stepRanges)
  {
    double maxEntropy=0.0;
    int step = 0;
    
    if (stepRanges.size() == 0)
    {
      stepRanges.add(ImagingChainProgramGenerator.defaultCandidateStepRange());
    }
    
    for (Pair<Integer, Integer> range : stepRanges)
    {
      if (range.getFirst().equals(range.getSecond()))
      {
        step=range.getFirst();
        break;
      }
        
      for (int i=range.getFirst(); i<range.getSecond(); i=i+(int)MathUtil.NANOMETERS_PER_MIL)
      {
        double minEntropy=999999;
        // calculate entropy for each step size each region
        for (ReconstructionRegion region : regions)
        {
          // get the minimum entropy value for each step size among all region
          minEntropy = Math.min(minEntropy, region.getEntropy(i));
        }
        // get the maximum entropy value from the entropy values and find out
        // associated step size, this is the step size we need
        if(minEntropy > maxEntropy)
        {
          step = i;
          maxEntropy = minEntropy;
        }
      }
    }
    
    Assert.expect(step != 0);
    
    List<Pair<Integer, Integer>> confirmedRanges = new ArrayList<Pair<Integer, Integer>>(1);
    confirmedRanges.add(new Pair<Integer, Integer>(step, step));
    
    return confirmedRanges;
  }
  
  /**
   * @author Roy Williams
   * @author Lim, Seng Yew
   * 
   * Modified by Lim, Seng Yew on 22-Dec-2011 for 3.2 release.
   * I make this method private because it is only used within this class.
   */
  public static List<Pair<Integer, Integer>> identifyCandidateStepIntersections(Collection<ReconstructionRegion> regions)
  {
    Assert.expect(regions != null);

    int proposedCandidateStepRangeBegin = 0;
    int proposedCandidateStepRangeEnd = 0;

    int begin = -1;
    int end = -1;
    List<Pair<Integer, Integer>> proposedRanges = new ArrayList<Pair<Integer, Integer>>(1);
    List<Pair<Integer, Integer>> confirmedRanges = new ArrayList<Pair<Integer, Integer>>(1);
    int count = 1;
    boolean newScanRoute=false; // Chnee Khang Wah, 2013-04-16
    for (ReconstructionRegion region : regions)
    {
      List<Pair<Integer, Integer>> candidateStepSizeRanges = region.getCandidateStepRanges().getRanges();
      // If this component has not turn on IC, then it should follow IL settings
      // Take note that candidateStepSizeRanges is a copy of the existing ranges in the region object, so this will not affect the values originally inside it.
      // _intersectionMethod value larger than or equals to METHOD_OPTIMIZED_LOADING_AND_STEP_SIZE will use this, meaning all future recipes should use this.
      if (region.hasComponent() && _intersectionMethod.getId()>=ScanPathMethodEnum.METHOD_OPTIMIZED_LOADING_AND_STEP_SIZE.getId())
      {
        boolean notArtifactCompensated = true;
        Subtype currentSubtype = region.getSubtypes().iterator().next();
        // Need to preserve whatever behavior in ScanPathMethod 2 and below, so fix it in ScanPathMethod 3 and above
        if(_intersectionMethod.getId()>=ScanPathMethodEnum.METHOD_COMMON_STEP_FIX_AND_IC_NA_FIX.getId())
        {
          notArtifactCompensated = currentSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_COMPENSATED) ||
                                   currentSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE);
          // Chnee Khang Wah, 2013-04-16
          if (!notArtifactCompensated)
          {
            if (region.getComponent().getBoard().getPanel().getProject().isGenerateByNewScanRoute())
            {
              newScanRoute=true;
            }
          }
        }
        else
        {
          notArtifactCompensated = currentSubtype.getSubtypeAdvanceSettings().getArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_COMPENSATED);
        }
        if (notArtifactCompensated)
        {
          // Find the max value pair
          int max = 0;
          Pair<Integer,Integer> highestRange = null;
          for (Pair<Integer,Integer> range: candidateStepSizeRanges)
          {
            if (range.getFirst() > max)
            {
              max = range.getFirst();
              highestRange = range;
            }
          }
          // Replace the max value in highest range to follow IL max value instead.
          int maxAverageStepSizeInNanos = ImagingChainProgramGenerator.getMaxAverageStepSizeInNanometers();
          int maxStepSizeInNanos = ImagingChainProgramGenerator.calculateMaxStepSizeInNanometers();
    
          int signalCompensationMax = ProgramGeneration.getInstance().getMaxStepSizeInNanos(region.getSignalCompensation(), maxStepSizeInNanos, maxAverageStepSizeInNanos);
          if (highestRange != null && signalCompensationMax != max)
          {
            int minn = highestRange.getFirst();
            int maxx = signalCompensationMax;
            
            // Super Bad luck, sometime when we set the region to IL8, 
            // we may end up getting min step size larget than max step size, so need to catter for this when 
            // scan path method = 4
            if(_intersectionMethod.getId()>=ScanPathMethodEnum.METHOD_COMMON_STEP_FIX_AND_IC_NA_MIN_MAX_FIX.getId())
            {
              if(minn>maxx) 
              {
                minn=signalCompensationMax;
              }
            }
            //candidateStepSizeRanges.add(new Pair<Integer, Integer>(highestRange.getFirst(), signalCompensationMax));
            candidateStepSizeRanges.add(new Pair<Integer, Integer>(minn, maxx));
            candidateStepSizeRanges.remove(highestRange);
          }
          
          if (Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.HOMOGENOUSIMAGEGROUP_STEPSIZE))
          {
            // If not found, just let it through and output a warning message.
            if (highestRange == null)
            {
              System.out.println("Cannot find highest range for " + region.getComponent().getReferenceDesignator() + ". Number of range is " + candidateStepSizeRanges.size());
            }
          }
        }
      }

      if (Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.HOMOGENOUSIMAGEGROUP_STEPSIZE))
      {
        String temp="";
        if(region.hasComponent())
        {
          temp = "Candidate:" + region.getComponent().getReferenceDesignator() + "-" + region.getRegionId()+":";
        }
        else if (region.isAlignmentRegion())
        {
          temp = "Candidate:" + "AlignmentRegion" + "-" + region.getRegionId()+":";
        }
        else if (region.isVerificationRegion())
        {
          temp = "Candidate:" + "VerificationRegion" + "-" + region.getRegionId()+":";
        }
        else
        {
          temp = "Candidate:" + "UnknownRegion" + "-" + region.getRegionId()+":";
        }
        for (Pair<Integer, Integer> candidateStepSizeRange : candidateStepSizeRanges)
        {
          begin = candidateStepSizeRange.getFirst();
          end = candidateStepSizeRange.getSecond();
          temp = temp + begin + "-" + end + ",";
        }
        System.out.println(temp);
      }

      // Initialize the "confirmedRanges" list with the first set of candidate steps.
      if (count == 1)
      {
        for (Pair<Integer, Integer> candidateStepSizeRange : candidateStepSizeRanges)
        {
          begin = candidateStepSizeRange.getFirst();
          end = candidateStepSizeRange.getSecond();
          proposedRanges.add(new Pair<Integer, Integer>(begin, end));
        }
        confirmedRanges.addAll(proposedRanges);
        if (confirmedRanges.size() > 0)
        {
          count++;
        }
        continue;
      }

      count++;

      // At this point it is our responsibility to cull the "good" list down.
      proposedRanges.clear();
      proposedRanges.addAll(confirmedRanges);
      confirmedRanges.clear();
      for (Pair<Integer, Integer> proposedRange : proposedRanges)
      {
        proposedCandidateStepRangeBegin = proposedRange.getFirst();
        proposedCandidateStepRangeEnd = proposedRange.getSecond();

        // We'd better have at least one of these candidateStepSizeRanges agree
        // to each goodRange.  Otherwise, the goodRange is invalid (i.e. NOT GOOD)
        // and should be removed.
        for (Pair<Integer, Integer> candidateStepSizeRange : candidateStepSizeRanges)
        {
          begin = candidateStepSizeRange.getFirst();
          end = candidateStepSizeRange.getSecond();

          if ((begin >= proposedCandidateStepRangeBegin && begin <= proposedCandidateStepRangeEnd) ||
              (end >= proposedCandidateStepRangeBegin && end <= proposedCandidateStepRangeEnd))
          {
            if(_intersectionMethod.getId()>=ScanPathMethodEnum.METHOD_COMMON_STEP_FIX_AND_IC_NA_FIX.getId())
            {
              proposedRange.setFirst(Math.max(proposedCandidateStepRangeBegin, begin));
              proposedRange.setSecond(Math.min(proposedCandidateStepRangeEnd, end));
            }
            // original settings
            else
            {
              proposedCandidateStepRangeBegin = Math.max(proposedCandidateStepRangeBegin, begin);
              proposedCandidateStepRangeEnd = Math.min(proposedCandidateStepRangeEnd, end);
              proposedRange.setFirst(proposedCandidateStepRangeBegin);
              proposedRange.setSecond(proposedCandidateStepRangeEnd);
            }
            confirmedRanges.add(proposedRange);
          }
        }
      }
    }
    if (Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.HOMOGENOUSIMAGEGROUP_STEPSIZE))
    {
      String temp = "Confirmed ranges:";
      for (Pair<Integer, Integer> confirmedStepSizeRange : confirmedRanges)
      {
        begin = confirmedStepSizeRange.getFirst();
        end = confirmedStepSizeRange.getSecond();
        temp = temp + begin + "-" + end + ",";
      }
      System.out.println(temp);
    }
    
    // Chnee Khang Wah, 2013-04-16
    if(newScanRoute)
    {
      return identifyCandidateStepBaseOnEntropy(regions, confirmedRanges);
    }
    else
    {
      return confirmedRanges;
    }
  }

  /**
   * @author Roy Williams
   * @author Dave Ferguson
   */
  public static List<HomogeneousImageGroup> mergeGroupsBasedOnExecutionTimeEstimates(List<HomogeneousImageGroup> imageGroups)
  {
    Assert.expect(imageGroups != null);

    // Try to coalesce these groups.   As groups increase in size, there is an opportunity
    // to include ReconstructionRegions that were prevoiusly forced to be in a separate
    // group.
    List<HomogeneousImageGroup> compressedGroups = new ArrayList<HomogeneousImageGroup>();
    HomogeneousImageGroup deprecatedGroupA;
    HomogeneousImageGroup deprecatedGroupB;

    for (HomogeneousImageGroup groupA : imageGroups)
    {

      // See if we can fit it into any of the finalGroups
      HomogeneousImageGroup mergedComboOfGroupAandB = null;
      deprecatedGroupA = null;
      deprecatedGroupB = null;
      for (HomogeneousImageGroup groupB : compressedGroups)
      {
        List<String> componentAndPinsGroupA = ImagingChainProgramGenerator.debugComponent(groupA);
        List<String> componentAndPinsGroupB = ImagingChainProgramGenerator.debugComponent(groupB);
        List<Pair<Integer, Integer>> groupRangesA = groupA.confirmedCandidateStepRanges();
        List<Pair<Integer, Integer>> groupRangesB = groupB.confirmedCandidateStepRanges();
        List<Pair<Integer, Integer>> intersectingRanges = RangeUtil.getIntersectingRanges(groupRangesA, groupRangesB);
        int combinedMaxStep = RangeUtil.getMaxInRangeList(intersectingRanges);
        if (combinedMaxStep > 0)
        {
          // Get the list from both groups to merge.
          List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionPairsFromGroupB = groupB.getRegionPairsSortedInExecutionOrder();
          List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionPairsFromGroupA = groupA.getRegionPairsSortedInExecutionOrder();

         if (componentAndPinsGroupA != null || componentAndPinsGroupB != null)
          {
            System.out.println("Trying to combine");
            if (componentAndPinsGroupA != null)
            {
              System.out.print("  groupA: ");
              for (String str : componentAndPinsGroupA)
                System.out.print(str + " ");
              System.out.println();
            }
            if (componentAndPinsGroupB != null)
            {
              System.out.print("  groupB: ");
              for (String str : componentAndPinsGroupB)
                System.out.print(str + " ");
              System.out.println();
            }
            int i = 1;
          }
          int size = regionPairsFromGroupA.size() + regionPairsFromGroupB.size();
          Collection<ReconstructionRegion> combinedRegions = new ArrayList<ReconstructionRegion>(size);
          for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : regionPairsFromGroupA)
          {
            String name = regionPair.getFirst().getName();
            combinedRegions.add(regionPair.getFirst());
          }
          for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : regionPairsFromGroupB)
          {
            combinedRegions.add(regionPair.getFirst());
          }

          // Create a new combined ImageGroup where there used to be one.
          mergedComboOfGroupAandB = new HomogeneousImageGroup(combinedRegions);

          // Compute estimates for each option.
          double costOfExecutingMergedGroup = mergedComboOfGroupAandB.estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSizeMethodB();
          double costOfExecutingGroupA = groupA.estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSizeMethodB();
          double costOfExecutingGroupB = groupB.estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSizeMethodB();
          double costOfExecutingGroupsSeparately = costOfExecutingGroupA + costOfExecutingGroupB;

          // Decide whether combining them is better or worse.
          if (costOfExecutingMergedGroup <= costOfExecutingGroupsSeparately)
          {
//            if (groupA.contains(2685) || groupB.contains(2685))
//            {
//              int groupAStepSize = groupA.getMeanStepSizeInNanoMeters();
////              ScanStrategy.printRectangle(groupA._inspectionRegionsBoundsInNanoMeters);
//
//              int groupBStepSize = groupB.getMeanStepSizeInNanoMeters();
////              ScanStrategy.printRectangle(groupB._inspectionRegionsBoundsInNanoMeters);
//
//              int mergedStepSize = mergedComboOfGroupAandB.getMeanStepSizeInNanoMeters();
//              int i = 1;
//              groupA.estimateExecutionTimeForScanPassesInSeconds();
//              groupB.estimateExecutionTimeForScanPassesInSeconds();
//              mergedComboOfGroupAandB.estimateExecutionTimeForScanPassesInSeconds();
//            }

            // Get out of for loop now that we know merge is desired/done/
            deprecatedGroupA = groupA;
            deprecatedGroupB = groupB;
            if (componentAndPinsGroupA != null || componentAndPinsGroupB != null)
            {
              System.out.println("Combined as");
               List<String> componentAndPinsCombined = ImagingChainProgramGenerator.debugComponent(mergedComboOfGroupAandB);
              System.out.print("  merged: ");
              for (String str : componentAndPinsCombined)
                System.out.print(str + " ");
              System.out.println();
              System.out.println();
            }

            break;
          }
          else
          {
            if (componentAndPinsGroupA != null || componentAndPinsGroupB != null)
            {
              System.out.println("Not combined");
              System.out.println();
            }

            // Let garbage collector eat up unwanted ImageGroup.
            mergedComboOfGroupAandB = null;
          }
        }
        mergedComboOfGroupAandB = null;
      } // end of ...  for (ImageGroup groupB : compressedGroups)

      // Clean up the finalGroups list.   We had to wait till not iterating over the
      // compressedGroups list to prevent a ConcurrentModificationException.
      if (mergedComboOfGroupAandB != null)
      {
        Assert.expect(deprecatedGroupA != null);
        Assert.expect(deprecatedGroupB != null);

        // Get the job done - remove 2 unmerged add 1 combined/merged group
        compressedGroups.remove(deprecatedGroupA); // must remove old group A
        compressedGroups.remove(deprecatedGroupB); // must remove old group B
        compressedGroups.add(mergedComboOfGroupAandB); // must add new combined group of A and B
      }
      else
      {
        compressedGroups.add(groupA);
      }
    } // end of ... for (ImageGroup groupA : imageGroups)
    return compressedGroups;
  }
  
  /**
   * @author Kay Lannen
   */
  public void setScanPath(ScanPath scanPath)
  {
    super.setScanPath( scanPath );
    getImageZones().first().setScanPath( scanPath );
  }
  
  /**
   * @author Lim, Seng Yew
   */
  public static void setScanPathMethod(ScanPathMethodEnum intersectionMethod)
  {
    _intersectionMethod = intersectionMethod;
  }
}
