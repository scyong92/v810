package com.axi.v810.business.imageAcquisition;

import java.awt.geom.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class SetAlignmentThreadTask extends ThreadTask<Object>
{
  private ImageReconstructionEngine _imageReconstructionEngine = null;
  private AffineTransform           _affineTransform = null;
  private int                       _testSubProgramId = -1;
  private boolean                   _cancelled;

  /**
   * @author Roy Williams
   */
  public SetAlignmentThreadTask(ImageReconstructionEngine imageReconstructionEngine,
                                AffineTransform affineTransform,
                                int testSubProgramId)
  {
    super("SetAlignmentThreadTask:" + imageReconstructionEngine.getId());

    Assert.expect(imageReconstructionEngine != null);
    Assert.expect(affineTransform != null);
    Assert.expect(testSubProgramId >= 0);

    _imageReconstructionEngine = imageReconstructionEngine;
    _affineTransform           = affineTransform;
    _testSubProgramId          = testSubProgramId;
  }

  /**
   * @author Roy Williams
   */
  public Object executeTask() throws Exception
  {
    // Between each step check to see if we should continue or cancel.
    if (_cancelled)
      return null;

    _imageReconstructionEngine.setAlignment(_affineTransform, _testSubProgramId);
    return null;
  }

  /**
   * @author Roy Williams
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Roy Williams
   */
  protected void cancel() throws XrayTesterException
  {
    _cancelled = true;
  }
}
