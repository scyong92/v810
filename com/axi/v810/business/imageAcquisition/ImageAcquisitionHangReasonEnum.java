package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;

/**
 * @author George A. David
 */
class ImageAcquisitionHangReasonEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static ImageAcquisitionHangReasonEnum ALGORITHMS = new ImageAcquisitionHangReasonEnum(++_index, "BS_IMAGE_ACQUISITION_HUNG_ALGORITHMS_KEY");
  public static ImageAcquisitionHangReasonEnum SAVING_IMAGES = new ImageAcquisitionHangReasonEnum(++_index, "BS_IMAGE_ACQUISITION_HUNG_SAVING_IMAGES_KEY");
  public static ImageAcquisitionHangReasonEnum ALGORITHMS_IRPS = new ImageAcquisitionHangReasonEnum(++_index, "BS_IMAGE_ACQUISITION_HUNG_ALGORITHMS_IRPS_KEY");
  public static ImageAcquisitionHangReasonEnum ALL_IRPS = new ImageAcquisitionHangReasonEnum(++_index, "BS_IMAGE_ACQUISITION_HUNG_ALL_IRPS_KEY");
  public static ImageAcquisitionHangReasonEnum SOME_IRPS = new ImageAcquisitionHangReasonEnum(++_index, "BS_IMAGE_ACQUISITION_HUNG_SOME_IRPS_KEY");
  public static ImageAcquisitionHangReasonEnum ALL_CAMERAS = new ImageAcquisitionHangReasonEnum(++_index, "BS_IMAGE_ACQUISITION_HUNG_ALL_CAMERAS_KEY");
  public static ImageAcquisitionHangReasonEnum SOME_CAMERAS = new ImageAcquisitionHangReasonEnum(++_index, "BS_IMAGE_ACQUISITION_HUNG_SOME_CAMERAS_KEY");
  public static ImageAcquisitionHangReasonEnum NO_TRIGGERS_RECEIVED = new ImageAcquisitionHangReasonEnum(++_index, "BS_IMAGE_ACQUISITION_HUNG_NO_TRIGGERS_RECEIVED_KEY");
  public static ImageAcquisitionHangReasonEnum SOME_TRIGGERS_RECEIVED = new ImageAcquisitionHangReasonEnum(++_index, "BS_IMAGE_ACQUISITION_HUNG_SOME_TRIGGERS_RECEIVED_KEY");
  public static ImageAcquisitionHangReasonEnum PROJECTION_CONSUMERS_NOT_READY = new ImageAcquisitionHangReasonEnum(++_index, "BS_IMAGE_ACQUISITION_HUNG_PROJECTION_CONSUMERS_NOT_READY_KEY");

  private String _key;
  /**
   * @author George A. David
   */
  private ImageAcquisitionHangReasonEnum(int id, String key)
  {
    super(id);

    Assert.expect(key != null);
    _key = key;
  }

  /**
   * @author George A. David
   */
  String getKey()
  {
    Assert.expect(_key != null);

    return _key;
  }
}
