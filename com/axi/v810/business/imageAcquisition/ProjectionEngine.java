package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This ProjectionConsumer will retrieve images from the XrayCameras based upon
 * ProjectionRequests that enumerate which cameras contain images for calibration
 * or other general viewing.
 *
 * @author Roy Williams
 */
public class ProjectionEngine extends ProjectionConsumer
{
  private List<ProjectionScanPass> _projectionScanPath = null;

  private boolean _allCamerasCollectorsLaunched = false;
  private boolean _cameraImageCollectionInProgress = false;
  private CollectImagesFromCamerasRunnable _collectImagesFromCamerasRunnable;

  private static Config _config;
  private static WorkerThread _workerThread;

  /**
   * @author Roy Williams
   */
  static
  {
    _config = Config.getInstance();
    _workerThread = new WorkerThread("ProjectionEngine camera data collector");
  }

  /**
   * @author Roy Williams
   */
  public ProjectionEngine(ProjectedImagesProducer projectedImagesProducer,
                          List<ProjectionScanPass> projectionScanPath) throws HardwareException
  {
    super(projectedImagesProducer);

    Assert.expect(projectedImagesProducer != null);
    Assert.expect(projectionScanPath != null && projectionScanPath.size() > 0);

    _projectionScanPath = projectionScanPath;
  }

  /**
   * @author Roy Williams
   */
  List<? extends ScanPass> scanPath()
  {
    Assert.expect(_projectionScanPath != null);
    return _projectionScanPath;
  }

  /**
   * @author Roy Williams
   */
  public void initializeForNextScanPath() throws XrayTesterException
  {
    _allCamerasCollectorsLaunched = false;
    _cameraImageCollectionInProgress = false;
  }

  /**
   * Called by the ImageAcquisition.runAcquisitionSequence method to insure this
   * implementation of a ProjectionConsumer is ready (for it) to initiate the next
   * scanPass.   Once the next scanPass is initiated, this ProjectionConsumer must
   * be ready to acquuire the next set of images.
   *
   * @author Roy Williams
   */
  public synchronized void collectScanPass(int scanPassNumber)
  {
    Assert.expect(scanPassNumber >= 0);

    if (_cameraImageCollectionInProgress == false)
    {
      _cameraImageCollectionInProgress = true;

      // Wait till all threads are launched as receivers of images.
      _allCamerasCollectorsLaunched = false;
      _collectImagesFromCamerasRunnable = new CollectImagesFromCamerasRunnable(this, _projectionScanPath, scanPassNumber);
      _workerThread.invokeLater(_collectImagesFromCamerasRunnable);

      // Wait here till the thread reports the CollectImagesFromCamerasRunnable is running.
      while (_allCamerasCollectorsLaunched == false)
      {
        try
        {
          // wait is tripped early by a notifyAll if this can exit the wait early.
          wait(100);
        }
        catch (InterruptedException e)
        {
          // do nothing
        }
      }
      _allCamerasCollectorsLaunched = false;
    }
  }

  /**
   * @author Roy Williams
   */
  ProjectedImagesProducer getProjectedImagesProducer()
  {
    return (ProjectedImagesProducer)getProjectionProducer();
  }

  /**
   * @author Roy Williams
   */
  public synchronized void setAllCamerasCollectorsLaunched()
  {
    _allCamerasCollectorsLaunched = true;
    notifyAll();
  }

  /**
   * @author Roy Williams
   */
  public boolean areAllImagesAcquired()
  {
//    System.out.println("areAllImagesAcquired: " + _cameraImageCollectionInProgress);
    return _cameraImageCollectionInProgress ? false : true;
  }

  /**
   * @author Roy Williams
   */
  public synchronized boolean canProceedWithScanPassWithWait(int scanPassNumber) throws XrayTesterException
  {
    Assert.expect(scanPassNumber >= 0);

    ProjectionProducer projectionProducer = getProjectionProducer();
    if (projectionProducer.hasXrayTesterException())
      throw projectionProducer.getXrayTesterException();

    if (_cameraImageCollectionInProgress)
    {
      return false;
    }

    // Check with the cameras to see if they have any free buffers on all scan
    // passes except the last one.
    if (scanPassNumber < _projectionScanPath.size())
    {
      ProjectionScanPass projectionScanPass = _projectionScanPath.get(scanPassNumber);
      List<ProjectionRequestThreadTask> clearCameraTasks = projectionScanPass.projectionRequestsToClearCameras();

      for (ProjectionRequestThreadTask projectionRequest : clearCameraTasks)
      {
        if (projectionRequest.getCamera().isReadyForAcquisition() == false)
          return false;
      }
    }

    return true;
  }

  /**
   * @author Roy Williams
   */
  public void startScanPass(int scanPassNumber) throws XrayTesterException
  {
    Assert.expect(scanPassNumber >= 0);

    collectScanPass(scanPassNumber);
  }

  /**
   * @author Roy Williams
   */
  public synchronized int incrementRunnableScanPass()
  {
    _cameraImageCollectionInProgress = false;
    _collectImagesFromCamerasRunnable = null;

//    try
//    {
//      wait(100);
//    }
//    catch (InterruptedException e)
//    {
//      // do nothing
//    }
    notifyAll();

    return 0;
  }

  /**
   * Nothing to do for this derived class from ProjectionConsumer.
   *
   * @author Roy Williams
   */
  public void start() throws HardwareException
  {
    // do nothing
  }

  /**
   * @author Roy Williams
   */
  public void abort()
  {
    if (_collectImagesFromCamerasRunnable != null)
    {
      _collectImagesFromCamerasRunnable.abort();
      _cameraImageCollectionInProgress = false;
    }
  }
}
