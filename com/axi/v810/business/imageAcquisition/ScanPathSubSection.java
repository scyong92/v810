package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.Assert;

/**
 * @author Roy Williams
 */
public class ScanPathSubSection
{
  private int     _startingScanPass;
  private int     _lastScanPass;
  private boolean _forcePathGeneration = true;
  
  private Set<String> _scanPassesCode = new TreeSet<String>();

  public ScanPathSubSection(int startingScanPass)
  {
    Assert.expect(startingScanPass >= 0);

    _startingScanPass = startingScanPass;
    _lastScanPass     = startingScanPass;
    _forcePathGeneration = false;
    _scanPassesCode.clear();
  }
  public ScanPathSubSection()
  {
  }

  /**
   * @author Roy Williams
   */
  int getStartingScanPass()
  {
    return _startingScanPass;
  }

  /**
   * @author Roy Williams
   */
  void setStartingScanPass(int startingScanPass)
  {
    _startingScanPass    = startingScanPass;
    _forcePathGeneration = false;
  }

  void setLastScanPass(int nextScanPass)
  {
    Assert.expect(nextScanPass >= _startingScanPass);
    Assert.expect(nextScanPass > _lastScanPass);

    _lastScanPass        = nextScanPass;
    _forcePathGeneration = false;
  }

  /**
   * @author Roy Williams
   */
  int getLastScanPass()
  {
    return _lastScanPass;
  }

  /**
   * @author Roy Williams
   */
  boolean lastScanPassHasBeenSet()
  {
    return (_lastScanPass > _startingScanPass);
  }
  
  /**
   * @author Chnee Khang Wah, new scan route
   * @param code
   */
  public void addScanPassCode(String code)
  {
    _scanPassesCode.add(code);
  }
  
  /*
   * @author Chnee Khang Wah, new scan route
   */
  public double getHighestSignalCompensation()
  {
    double size = 1D;
    if (_scanPassesCode.isEmpty()==false)
    {
      size = _scanPassesCode.size();
      if(size==9 || size==8 || size==7)// IL8, IL7, IL6
      {
        return size-1;
      }
      else if(size==5)// IL5 or IL4
      {
        if(_scanPassesCode.contains(Character.toString('I'))) // IL4
        {
          return 4;
        }
        //IL5 if not contain 'I'
        return 5;
      }
      else if(size==3)
      {
        if(_scanPassesCode.contains(Character.toString('I'))) // IL2
        {
          return 2;
        }
        //IL3 if not contain 'I'
        return 3;
      }
      else if(size==2)
      {
        return 1.5;
      }
      else if(size==1)
      {
        return 1;
      }
    }
    return size;
  }
  
  public void forceCustomPathGeneration(boolean state)
  {
    _forcePathGeneration = state;
  }
  
  public boolean forceCustomPathGeneration()
  {
    return _forcePathGeneration;
  }

  /**
   * @author Roy Williams
   */
  int size()
  {
    return _lastScanPass - _startingScanPass + 1;
  }
}
