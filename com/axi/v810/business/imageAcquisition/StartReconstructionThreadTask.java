package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
class StartReconstructionThreadTask extends ThreadTask<Object>
{
  private ImageReconstructionEngine _imageReconstructionEngine;
  private boolean                   _initializeTestRequired = true;
  private boolean                   _resetSurfaceModelRequired = false;
  private boolean                   _cancelled        = false;
  private TestExecutionTimer        _testExecutionTimer;
  private TestSubProgram _testSubProgram;
 
  /**
   * @author Roy Williams
   */
  public StartReconstructionThreadTask(ImageReconstructionEngine imageReconstructionEngine,
                                       TestSubProgram testSubProgram,
                                       boolean initializeTestRequired,
                                       boolean resetSurfaceModelRequired,
                                       TestExecutionTimer testExecutionTimer)
  {
    super("StartReconstructionThreadTask:" + imageReconstructionEngine.getId());

    Assert.expect(imageReconstructionEngine != null);
    Assert.expect(testSubProgram !=null);
    Assert.expect(testExecutionTimer != null);

    _testSubProgram=testSubProgram;
    _imageReconstructionEngine = imageReconstructionEngine;
    _initializeTestRequired    = initializeTestRequired;
    _resetSurfaceModelRequired = resetSurfaceModelRequired;
    _testExecutionTimer        = testExecutionTimer;
  }

  /**
   * @author Roy Williams
   */
  protected Object executeTask() throws Exception
  {
    // Between each step check to see if we should continue or cancel.
    if (_cancelled)
      return null;

    // XCR1168: pass whole subprogram instead of sub program id only
    // so that will  pass both user gain and id.
    _imageReconstructionEngine.start(_testSubProgram, _initializeTestRequired, _resetSurfaceModelRequired, _testExecutionTimer);
    return null;
  }

  /**
   * @author Roy Williams
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Roy Williams
   */
  protected void cancel() throws XrayTesterException
  {
    _cancelled = true;
  }
}
