package com.axi.v810.business.imageAcquisition;

import java.awt.geom.*;
import java.net.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class ProjectedImagesProducer extends ProjectionProducer
{
  private final static int DELAY_FOR_HTUBE_MOMENT_IN_MILLIS = Config.getInstance().getIntValue(HardwareConfigEnum.HTUBE_XRAY_SOURCE_MOMENT_DELAY_IN_MILLIS);
  private ProjectionEngine _projectionEngine = null;
  protected List<ProjectionRequestThreadTask> _scanPassEntries = null;
  protected ScanPassDirectionEnum   _currentStageDirection = null;
  protected int _currentScanPassId = 0;
  private int _xPositionOfStage = 0;
  protected MechanicalConversions _mechanicalConversions = null;
  
  protected static boolean _debug = Config.isDeveloperDebugModeOn();
  private static String _me = "ProjectedImagesProducer";

  /**
   * @author Roy Williams
   */
//  public ProjectedImagesProducer(ProjectionProducer projectionProducer)
//  {
//    super();
//  }

  /**
   * @author Roy Williams
   */
  public synchronized void acquireProjections(List<ProjectionRequestThreadTask> projectionRequestThreadTasks,
                                              TestExecutionTimer testExecutionTimer,
                                              boolean optimizeProjectionRequestOrder,
                                              MagnificationTypeEnum magnification,
                                              boolean turnOnXray) throws XrayTesterException
  {
    Assert.expect(projectionRequestThreadTasks != null);
    Assert.expect(projectionRequestThreadTasks.size() > 0);
    Assert.expect(testExecutionTimer != null);

    //Clear out any prior exceptions before acquiring projections this time
    clearForNextRun();

    try
    {
      // Make sure the XraySource is on.
      if (turnOnXray)
      {
        turnXraysOn(testExecutionTimer);
        
        if(magnification.equals(MagnificationTypeEnum.LOW))
        {
          moveXrayCylinderUp();
        }
        // assume there is only 2 magnification (low and high), must change if there is more then 2 magnification (weichin remark)
        else
        {
          moveXrayCylinderDown();
        }
      }

      // sleep a while to make sure the htube is reach the position
      try
      {
        Thread.sleep(DELAY_FOR_HTUBE_MOMENT_IN_MILLIS);
      }
      catch (InterruptedException ix)
      {
        // do nothing
      }

      // Turn off synthetic triggers so we can program the cameras.
      turnOffSyntheticTriggersThenWaitForCamerasToBecomeIdle();

      // Initialize with the projectionRequests.
      initialize(projectionRequestThreadTasks, optimizeProjectionRequestOrder, testExecutionTimer);
      
      // Run scan path
      List<ProjectionEngine> projectionEngineList = new ArrayList<ProjectionEngine>(1);
      projectionEngineList.add(_projectionEngine);

      List<? extends ScanPass> scanPasses = _projectionEngine.scanPath();
      runAcquisitionSequence(testExecutionTimer, projectionEngineList, scanPasses, scanPasses.size(), true, ImageAcquisitionModeEnum.PROJECTION, true);
    }
    finally
    {
      // CR26139:  IAE should not turn xrays off after a calibration or confirmation is done running.
      // turnXraysOff();

      waitForCamerasToBecomeIdleThenTurnOnSyntheticTriggers();

      // Always dispose of the ProjectionEngine after each use.
      _projectionEngine = null;

      // This is just a suggestion to the garbage collector.
      MemoryUtil.garbageCollect();
    }
  }  
  
  /**
   * @author Roy Williams
   */
  public synchronized void acquireProjections(List<ProjectionRequestThreadTask> projectionRequestThreadTasks,
                                              TestExecutionTimer testExecutionTimer,
                                              boolean optimizeProjectionRequestOrder,
                                              boolean turnOnXray) throws XrayTesterException
  {
    acquireProjections(projectionRequestThreadTasks, testExecutionTimer, optimizeProjectionRequestOrder, MagnificationTypeEnum.LOW, turnOnXray);
  }
  
  /**
   * @author Wei Chin
   */
  public synchronized void acquireHighMagProjections(List<ProjectionRequestThreadTask> projectionRequestThreadTasks,
                                              TestExecutionTimer testExecutionTimer,
                                              boolean optimizeProjectionRequestOrder,
                                              boolean turnOnXray) throws XrayTesterException
  {
    acquireProjections(projectionRequestThreadTasks, testExecutionTimer, optimizeProjectionRequestOrder, MagnificationTypeEnum.HIGH, turnOnXray);
  }

  /**
   * Initialize the image acquisition engine and it's constituent subsystems to
   * acquire a set of images for a list of ProjectionRequests.   The ProjectionEngine
   * will be used for this purpose instead of the ImageReconstructionEngine.  At
   * the time it was written, this is/was used exclusively by Calibration.
   *
   * If hardware initialization is required this method will:
   * - build the scan path
   * - initialize cameras for acquisition
   * - initialize panel positioner
   *
   * @author Roy Williams
   */
  private void initialize(List<ProjectionRequestThreadTask> projectionRequestThreadTasks,
                          boolean optimizeProjectionRequestOrder,
                          TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(projectionRequestThreadTasks != null);
    Assert.expect(testExecutionTimer != null);

    // Create a scan path.   This function will also initializeCamerasForAcquisition.
    // This is because the number of cameras to program vary from one scan pass
    // to the next.

    Map<AbstractXrayCamera, XrayCameraAcquisitionParameters> xRayCameraToAcquisitionParametersMap = new HashMap<AbstractXrayCamera,XrayCameraAcquisitionParameters>();
    List<ProjectionScanPass> projectionScanPath = generateScanPath(projectionRequestThreadTasks, optimizeProjectionRequestOrder,  xRayCameraToAcquisitionParametersMap);
    Assert.expect(projectionScanPath != null && projectionScanPath.size() > 0);

    // Initialize the panel positioner.
    initializePanelPositionerForScanPath(new ArrayList<ScanPass>(projectionScanPath), testExecutionTimer);

    // Create a ProjectionEngine to be destroyed by the caller.
    _projectionEngine = new ProjectionEngine(this, projectionScanPath);
    _projectionEngine.setProjectionProducer(this);

    List<XrayCameraAcquisitionParameters> cameraAcquisitionParametersList = new ArrayList<XrayCameraAcquisitionParameters>();
    for (Map.Entry<AbstractXrayCamera, XrayCameraAcquisitionParameters> entry : xRayCameraToAcquisitionParametersMap.entrySet())
      cameraAcquisitionParametersList.add(entry.getValue());

    // Add a projectionSetting for each and every camera.
    setUpUnusedCamerasPerScanPass(cameraAcquisitionParametersList, projectionScanPath);

    prepareCamerasForAcquisition(projectionScanPath, cameraAcquisitionParametersList);

    _scanPassEntries = null;
    _currentStageDirection = null;
  }

  /**
   * Sends each camera the necessary acquisition parameters.  This instructs the cameras on which projections to
   * aquire and where to send the projection data.   This is for the case of all cameras.
   *
   * @author Dave Ferguson
   * @author Roy Williams
   * @author Greg Esparza
   */
  protected void prepareCamerasForAcquisition(List<? extends ScanPass> scanPath,
                                              List<XrayCameraAcquisitionParameters> xrayCameraAcquisitionParameters) throws XrayTesterException
  {
    Assert.expect(xrayCameraAcquisitionParameters != null);
    Assert.expect(scanPath != null);

    for (XrayCameraAcquisitionParameters cameraParameters : xrayCameraAcquisitionParameters)
    {
      List<ProjectionSettings> projectionSettingsListForCamera = cameraParameters.getProjectionSettings();
      AbstractXrayCamera camera = cameraParameters.getCamera();
      camera.enableTriggerDetection();

      camera.setScan(scanPath,
                     _mechanicalConversions.getNumberOfLinesToSkipDuringPositiveYScan(camera),
                     _mechanicalConversions.getNumberOfLinesToSkipDuringNegativeYScan(camera),
                     projectionSettingsListForCamera);

      camera.initializeAcquisition();
    }
  }

  /**
   * When we define a scan path it describes how many cameras we will need images from.  However, not all cameras participate in each scan pass.
   * As a result, we need to finalize the scan path definition by making sure that the cameras that do not participate in a specific
   * scan pass will be setup to acquire image data that will ultimitately be thrown away.  Specifically, the unused image data is defined
   * as a zero size image.
   *
   * @author Roy Williams
   * @author Greg Esparza
   */
  private void setUpUnusedCamerasPerScanPass(List<XrayCameraAcquisitionParameters> cameraAcquisitionParameters, List<ProjectionScanPass> projectionScanPath)
  {
    Assert.expect(cameraAcquisitionParameters != null);
    Assert.expect(projectionScanPath != null);

    // Define zero size image data settings for the cameras that do no participate in the current scan pass
    int numberOfLinesToCapture = 0;
    SystemFiducialRectangle systemFiducialRectangle = new SystemFiducialRectangle(0, 0, 0, 0);
    ImageRectangle imageRectagle = new ImageRectangle(0, 0, 0, 0);
    ProjectionRegion sampleProjectionRegion = new ProjectionRegion(imageRectagle, ImageReconstructionEngineEnum.IRE0, IPaddressUtil.getRemoteLoopbackAddress());

    for (XrayCameraAcquisitionParameters acquisitionParameters : cameraAcquisitionParameters)
    {
      ProjectionSettings arrayOfProjectionSettings[] = new ProjectionSettings[projectionScanPath.size()];

      // Figure out how many cameras were initially requested and initialize some values to be reused later.
      if (acquisitionParameters.getNumberOfProjectionSettings() > 0)
      {
        for (ProjectionSettings projectionSettings : acquisitionParameters.getProjectionSettings())
            arrayOfProjectionSettings[projectionSettings.getScanPassNumber()] = projectionSettings;
      }

      // Now add a camera program for those scanPasses where a camera does not participate.
      // We can/will use the sparsely populated "arrayOfSetting"  variable to determine
      // which need to be populated.
      //
      // There is an opportunity for some to be missing due to ProjectionRequests
      // used for calibration.
      List<ProjectionSettings> orderedListOfProjectionSettings = new ArrayList<ProjectionSettings>();
      for (int scanPassNumber = 0; scanPassNumber < projectionScanPath.size(); scanPassNumber++)
      {
        ProjectionScanPass projectionScanPass = projectionScanPath.get(scanPassNumber);
        ProjectionSettings projectionSettings = arrayOfProjectionSettings[scanPassNumber];

        if (projectionSettings == null)
        {
          ProjectionRequestThreadTask projectionRequest = new ProjectionRequestThreadTask(acquisitionParameters.getCamera(),
                                                                                          projectionScanPass.getStageDirection(),
                                                                                          systemFiducialRectangle);

          List<ProjectionRegion> projectionRegions = new ArrayList<ProjectionRegion>();
          ImageRectangle imageRectangle = sampleProjectionRegion.getProjectionRegionRectangle();
          projectionRequest.setNumberOfLinesToCapture(numberOfLinesToCapture);
          projectionRequest.getProjectionInfo().setImageRectangleForRequestedProjection(imageRectangle);
          projectionRequest.setRealRequestNeeded(false);

          //if (XrayCameraArray.isDeepSimulationModeOn() == false)
          //{
            projectionScanPass.addProjectionRequestToClearCamera(projectionRequest);
          //}

          projectionSettings = new ProjectionSettings(projectionScanPass,
                                            0, // startCaptureLine,
                                            numberOfLinesToCapture,
                                            projectionRegions);
        }

        orderedListOfProjectionSettings.add(projectionSettings);
      }

      acquisitionParameters.setProjectionSettings(orderedListOfProjectionSettings);
    }

    // Lets go through and set the scan pass number on all projectionRequests.
    for (int scanPassNumber = 0; scanPassNumber < projectionScanPath.size(); scanPassNumber++)
    {
      ProjectionScanPass projectionScanPass = projectionScanPath.get(scanPassNumber);

      for (ProjectionRequestThreadTask projectionRequest : projectionScanPass.getProjectionRequestsToBeRun())
        projectionRequest.scanPassNumber(scanPassNumber);

      for (ProjectionRequestThreadTask projectionRequest : projectionScanPass.projectionRequestsToClearCameras())
        projectionRequest.scanPassNumber(scanPassNumber);
    }
  }

  /**
   * @author Roy Williams
   */
  public SystemFiducialRectangle checkSystemFiducialRectangle(List<ProjectionRequestThreadTask> projectionRequestThreadTasks)
  {
    Assert.expect(projectionRequestThreadTasks != null);

    SystemFiducialRectangle maxSystemFiducialRectangle = null;
    for (ProjectionRequestThreadTask projectionRequest : projectionRequestThreadTasks)
    {
      SystemFiducialRectangle systemFiducialRectangle = projectionRequest.getSystemFiducialRectangle();
      Assert.expect(systemFiducialRectangle != null);
//      Assert.expect(systemFiducialRectangle.getHeight() >= 25400000, "systemFiducialRectangle.getHeight() below limit: " + systemFiducialRectangle.getHeight());
      //Assert.expect(systemFiducialRectangle.getHeight() >= 18000000, "systemFiducialRectangle.getHeight() below limit: " + systemFiducialRectangle.getHeight());
      if (maxSystemFiducialRectangle == null)
        maxSystemFiducialRectangle = systemFiducialRectangle;
      else
        Assert.expect(systemFiducialRectangle.equals(maxSystemFiducialRectangle));
    }
    return maxSystemFiducialRectangle;
  }

  /**
   * Prior to execution by <tt>runAcquisition(List<ProjectionRequest>)</tt>>, it
   * is helpful to understand the scanpath that will be used.   For hysterisis,
   * the calibration system will tune this list and ask for a special/modified
   * sequence.
   *
   * @author Roy Williams
   */
  public List<ProjectionScanPass> generateScanPath(List<ProjectionRequestThreadTask> projectionRequests) throws XrayTesterException
  {
    Map<AbstractXrayCamera, XrayCameraAcquisitionParameters> xRayCameraToAcquisitionParametersMap = new HashMap<AbstractXrayCamera,XrayCameraAcquisitionParameters>();
    boolean optimizeProjectionRequestOrder = true;
    List<ProjectionScanPass> projectionScanPath = generateScanPath(projectionRequests, optimizeProjectionRequestOrder, xRayCameraToAcquisitionParametersMap);

    return projectionScanPath;
  }

  /**
   * As the scan path is generated, all the values needed for the ProjectionInfo
   * are filled in except the actual image from the cameras.  These will be
   * acquired later when we actually run the ProjectionRequest.
   * <p>
   * Another interesting sidenote deals with the ScanPass.  A special derived class
   * called ProjectionScanPass will be created to accumulate a List<ProjectionRegion>.
   * When each ProjectionScanPass is executed (later), it will contain the list of
   * ProjectionRegions (i.e. cameras) participating in the pass.   Each camera
   * must have the image extracted.
   * <p>
   * This function will also initializeCamerasForAcquisition.  This is because
   * the number of cameras to program vary from one scan pass to the next.
   *
   * @author Roy Williams
   */
  protected List<ProjectionScanPass> generateScanPath(List<ProjectionRequestThreadTask> projectionRequests,
                                                      boolean optimizeProjectionRequestOrder,
                                                      Map<AbstractXrayCamera, XrayCameraAcquisitionParameters> xRayCameraToAcquisitionParametersMap) throws XrayTesterException
  {
    Assert.expect(projectionRequests != null);
    Assert.expect(xRayCameraToAcquisitionParametersMap != null);

    _scanPassEntries       = new ArrayList<ProjectionRequestThreadTask>();
    _currentStageDirection = ScanPassDirectionEnum.FORWARD;
    _currentScanPassId = 0;
    List<ProjectionScanPass> scanPath = new ArrayList<ProjectionScanPass>();

    // Create a list of processorStrips to indicate the number of IRPs to use.
    List<ProcessorStrip> processorStrips = new ArrayList<ProcessorStrip>();
    ProcessorStrip processorStrip = new ProcessorStrip(0);
    processorStrip.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
    processorStrips.add(processorStrip);

    _mechanicalConversions = new MechanicalConversions(processorStrips, // numberOfIRPs = lie because these images are coming back through XrayCameras
                                                       new AffineTransform(),
                                                       0, // alignmentUncertainty => none because we are always scanning system fiducical
                                                       0, // largest undivisible pad.   None for System Fiducial.
                                                       0, // FocalRangeUpslice on calibration measurements is 0!
                                                       0, // FocalRangeDownslice on calibration measurements is 0!
                                                       getCameraList(projectionRequests), // listOfCameras
                                                       checkSystemFiducialRectangle(projectionRequests));

    // Compute the yTravelRequired for the set of cameras involved in the list.
    int startYPosition = _mechanicalConversions.stageTravelStart();
    int endYPosition   = _mechanicalConversions.stageTravelEnd();

    // Align each ProjectionRequest above its desired camera and sort based upon
    // this position.
    if (optimizeProjectionRequestOrder)
      reorderProjectionRequestsAboveCameras(projectionRequests);

    // Use an Iterator so we can look ahead 1 (at end of block) to insure last element flushed.
    Iterator<ProjectionRequestThreadTask> projectionRequestsIter = projectionRequests.iterator();
    _xPositionOfStage = 0;
    boolean retryCurrentProjectionRequest = false;
    ProjectionRequestThreadTask projectionRequest = null;
    while (projectionRequestsIter.hasNext())
    {
      if ( retryCurrentProjectionRequest == false)
        projectionRequest = projectionRequestsIter.next();
      else
        retryCurrentProjectionRequest = false;

      AbstractXrayCamera camera = projectionRequest.getCamera();
      ScanPassDirectionEnum desiredStageDirection = projectionRequest.getStageDirection();
      Assert.expect(desiredStageDirection != null);

      // Somehow the stage is on the wrong side of where this projectionRequest
      // wants us to be.   Sigh.
      if ( (desiredStageDirection.equals(_currentStageDirection) == false) &&
           (desiredStageDirection.equals(ScanPassDirectionEnum.BIDIRECTION) == false))
      {
        addCurrentScanPassToPath(scanPath, startYPosition, endYPosition, xRayCameraToAcquisitionParametersMap);
      }

      // First element in current scan pass?
      if (_scanPassEntries.size() == 0)
      {
        int newXPositionOfStage = addFirstRectangleToScanPass(projectionRequest);
        Assert.expect(newXPositionOfStage >= _xPositionOfStage);
        _xPositionOfStage = newXPositionOfStage;
      }
      else
      {
        // Not first ScanPass in path.
        try
        {
          // addRectangleToCurrentScanPass can/will throw a SystemFiducialRectangleFitException
          // when it is full!!!!   Full means it cannot get the WHOLE area/rectangle to fit
          // given the constraint of where the currentStage x position is now.
          int yStagePosition = _mechanicalConversions.yLocationForReconstructionOnCamera(camera);
          projectionRequest.alignSystemFiducialRectangleToCurrentStagePosition(_xPositionOfStage, yStagePosition, _currentStageDirection);
          _scanPassEntries.add(projectionRequest);
        }
        catch (SystemFiducialRectangleFitException e)
        {
          // Emit current ScanPass.
          addCurrentScanPassToPath(scanPath, startYPosition, endYPosition, xRayCameraToAcquisitionParametersMap);

          // Try again to add this ProjectionRequest, except this time, we'll just
          // start a new ScanPass.
          retryCurrentProjectionRequest = true;
        }
      }

      // Flush out any uncommited ScanPasses.  Corner case (in logic) can have a
      // single entry in the list that would not be emitted.
      if ((projectionRequestsIter.hasNext()  == false) && (_scanPassEntries.size() > 0))
        addCurrentScanPassToPath(scanPath, startYPosition, endYPosition, xRayCameraToAcquisitionParametersMap);
    }

    // Okay, now we know the number of ScanPasses we'll have.  We

    // Clean up temp variables.
    _scanPassEntries = null;
    return scanPath;
  }

  /**
   * Adds a MachineRectangle to a list that will become a single ScanPass when emitted.
   *
   * @return the maximum stage x position required to pass this rectangle over the camera.
   * @author Roy Williams
   */
  private int addFirstRectangleToScanPass(ProjectionRequestThreadTask projectionRequest)
       throws XrayTesterException
  {
    Assert.expect(projectionRequest != null);

    int yStagePosition = _mechanicalConversions.yLocationForReconstructionOnCamera(projectionRequest.getCamera());

    int xStagePosition = projectionRequest.alignSystemFiducialRectangleRight(_currentStageDirection, yStagePosition);
    _scanPassEntries.add(projectionRequest);

    return xStagePosition;
  }

  /**
   * Commit the current ScanPass as one we need to do when the data is aquired.
   * @author Roy Williams
   */
  protected void addCurrentScanPassToPath(List<ProjectionScanPass> scanPath,
                                          int startYPosition,
                                          int endYPosition,
                                          Map<AbstractXrayCamera, XrayCameraAcquisitionParameters> xRayCameraToAcquisitionParametersMap) throws XrayTesterException
  {
    Assert.expect(scanPath != null);
    Assert.expect(xRayCameraToAcquisitionParametersMap != null);

    // Set stage sweep range for this ScanPass.
    StagePositionMutable startPosition = new StagePositionMutable();
    startPosition.setXInNanometers(_xPositionOfStage);
    startPosition.setYInNanometers(startYPosition);

    StagePositionMutable endPostion    = new StagePositionMutable();
    endPostion.setXInNanometers(_xPositionOfStage);
    endPostion.setYInNanometers(endYPosition);

    // Lets add this ScanPass to the ScanPath list.
    ProjectionScanPass scanPass = new ProjectionScanPass(_currentScanPassId++,
                                                         startPosition, endPostion,
                                                         _scanPassEntries);
    scanPass.directionOfTravel(_currentStageDirection);
    scanPath.add(scanPass);

    // Update the ProjectionRequest now that a diretion has been assigned.
    for (ProjectionRequestThreadTask projectionRequest : scanPass.getProjectionRequestsToBeRun())
    {
      projectionRequest.setStageDirection(_currentStageDirection);
    }

    // Program the camera delays for this scanPass.
    programCameraDelaysForProjectionRegions(scanPass, xRayCameraToAcquisitionParametersMap);

    // Finally, a little cleanup work to prepare for the next scanPass.
    _scanPassEntries = new ArrayList<ProjectionRequestThreadTask>();

    // Last thing, let's flip the ScanDirection
    if (_currentStageDirection.equals(ScanPassDirectionEnum.FORWARD))
      _currentStageDirection = ScanPassDirectionEnum.REVERSE;
    else
      _currentStageDirection = ScanPassDirectionEnum.FORWARD;
  }

  /**
   * Update the XrayCameraAcquisitionParameters list for each camera involved
   * in the scan pass.
   *
   * @author Roy Williams
   */
  private void programCameraDelaysForProjectionRegions(ProjectionScanPass scanPass,
                                                       Map<AbstractXrayCamera, XrayCameraAcquisitionParameters> xRayCameraAcquisitionParametersMap) throws XrayTesterException
  {
    Assert.expect(scanPass != null);
    Assert.expect(xRayCameraAcquisitionParametersMap != null);

    // For every camera, create a ProjectionSettings for each scan pass
    ImageReconstructionEngineEnum mainControllerId = ImageReconstructionEngineEnum.IRE1;

    // We can just get any camera so we can get the sensor attributes
    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);

    List<ProjectionRequestThreadTask> projectionRequestsToBeRun = scanPass.getProjectionRequestsToBeRun();
    for (ProjectionRequestThreadTask projectionRequest : projectionRequestsToBeRun)
    {

      // We will foribly tell the new ProjectionRegion to capture a rectangle of
      // the full width of the camera.   The current (Genesis) camearas do not
      // clip.
      int heightOfScan = projectionRequest.getProjectionInfo().getHeightOfScanPixels();
      // Swee Yee Wong - XCR-2630 Support New X-Ray Camera
	  ImageRectangle imageRectangle = new ImageRectangle(0,
                                                         0,
                                                         xRayCamera.getSensorWidthInPixelsWithOverHeadByte(),
                                                         heightOfScan);

      AbstractXrayCamera camera = projectionRequest.getCamera();
      InetAddress systemControllerAddress = IPaddressUtil.getHostInetAddressOnPrivateNetwork();
      ProjectionRegion projectionRegion = new ProjectionRegion(imageRectangle,
                                                               mainControllerId,
                                                               systemControllerAddress);

      // Create a new ProjectionSettings for this projection.
      int startCaptureLine = 0; // (int)Math.round((float)systemFiducialRectangle.getMaxY()   / nanoMetersPerPixel);
      List<ProjectionRegion> projectionRegions = new ArrayList<ProjectionRegion>(1);
      projectionRegions.add(projectionRegion);
      int numberOfLinesToCapture = _mechanicalConversions.numberOfLinesToCapture(camera);
      projectionRequest.setNumberOfLinesToCapture(numberOfLinesToCapture);

      ProjectionSettings projectionSettingsForCurrentScanPass = new ProjectionSettings(scanPass,
                                                                                       startCaptureLine,
                                                                                       numberOfLinesToCapture,
                                                                                       projectionRegions);

      XrayCameraAcquisitionParameters parametersForCurrentCamera = xRayCameraAcquisitionParametersMap.get(camera);
      if (parametersForCurrentCamera == null)
        parametersForCurrentCamera = new XrayCameraAcquisitionParameters(camera);

      parametersForCurrentCamera.addProjectionSettings(projectionSettingsForCurrentScanPass);
      xRayCameraAcquisitionParametersMap.put(camera, parametersForCurrentCamera);
    }
  }

  /**
   * @author Roy Williams
   */
  protected List<AbstractXrayCamera> getCameraList(List<ProjectionRequestThreadTask> projectionRequests)
  {
    Assert.expect(projectionRequests != null);
    List<AbstractXrayCamera> cameras = new ArrayList<AbstractXrayCamera>();

    for (ProjectionRequestThreadTask projectionRequest : projectionRequests)
    {
      cameras.add(projectionRequest.getCamera());
    }

    return cameras;
  }

  /**
   * If we center the SystemFiducialRectangle for each ProjectionRequest above the
   * requested camera, this will identify the leftmost edge of the rectangle to
   * be imaged.   This edge is used to sort the ProjectionRegions into an ordered
   * list so we can (later) sweep the stage from left-to-right and encounter the
   * image areas in a predictive order.
   *
   * @author Roy Williams
   */
  private void reorderProjectionRequestsAboveCameras(List<ProjectionRequestThreadTask> projectionRequests)
  {
    Assert.expect(projectionRequests != null);

    // Now sort all the projectionRequests.
    Collections.sort(projectionRequests, new ReorderProjectionRequestsAboveCamerasComparator());
  }

  /**
   * @author Roy Williams
   */
  public MachineRectangle getBoundingRectangle(List<ProjectionRequestThreadTask> projectionRequests)
  {
    Assert.expect(projectionRequests != null);
    Assert.expect(projectionRequests.size() > 0);

    MachineRectangle rectangle = null;
    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    for (ProjectionRequestThreadTask projectionRequest : projectionRequests)
    {
      MachineRectangle currentRectangle = xRayCameraArray.getCameraSensorRectangle(projectionRequest.getCamera(), MagnificationEnum.getCurrentNorminal());
      if (rectangle == null)
        rectangle = new MachineRectangle(currentRectangle);
      else
        rectangle.add(currentRectangle);
    }
    return rectangle;
  }

  /**
   * @author Roy Williams
   */
  public int getMaxSystemFiducialRectangleHeight(List<ProjectionRequestThreadTask> projectionRequests)
  {
    Assert.expect(projectionRequests != null);
    Assert.expect(projectionRequests.size() > 0);
    int height = -1;
    for (ProjectionRequestThreadTask projectionRequest : projectionRequests)
    {
      if (height == -1)
        height = projectionRequest.getSystemFiducialRectangle().getHeight();
      else
      {
        if (height != projectionRequest.getSystemFiducialRectangle().getHeight())
          Assert.expect(false, "Calculation for ScanPass require uniform height of SystemFiducialRectangle");
      }
    }
    Assert.expect(height > 0);
    return height;
  }

  /**
   * @author Roy Williams
   */
  public boolean areAllImagesAcquired() throws XrayTesterException
  {
    return _projectionEngine.areAllImagesAcquired();
  }

  /**
   * @author Roy Williams
   */
  protected boolean waitForScanPassToBeAcknowledged(int expectedNumberOfScanPasses) throws XrayTesterException
  {
    Assert.expect(false, "Callers should use waitForLastScanPassToBeAcknowledged");
    return false;
  }

  /**
   * Send an event to begin unloading the panel.
   *
   * @author Roy Williams
   */
  protected void waitForLastScanPassToBeAcknowledged(int expectedNumberOfScanPasses) throws XrayTesterException
  {
    Assert.expect(expectedNumberOfScanPasses > 0);

    waitTillProjectionConsumersAreReadyForScanPass(expectedNumberOfScanPasses);
//    AbstractXrayCamera.waitHere = true;
  }

  /**
   * Aborts the panelPostioner and cameras.
   *
   * @author Roy Williams
   */
  public void userAbort() throws XrayTesterException
  {
    if (isAbortInProgress() == false)
    {
      super.userAbort();

      projectedImagesProducerAbort();
    }
  }

  /**
   * @author Roy Williams
   */
  public void abort(XrayTesterException abortException)
  {
    Assert.expect(abortException != null);

    if (isAbortInProgress() == false)
    {
      super.abort(abortException);
      // Call the private abort() to perform the things independent of whether
      // user canceled or an abort occured.
      projectedImagesProducerAbort();
    }
  }

  /**
   * @author Roy Williams
   */
  private void projectedImagesProducerAbort()
  {
    if (_projectionEngine != null)
      _projectionEngine.abort();
  }

  /**
   * @author Roy Williams
   */
  public void clearForNextRun()
  {
    super.clearForNextRun();
    _mechanicalConversions = null;
  }
  
  /**
   * @author Swee Yee Wong
   */
  private void debug(String message)
  {
    if (_debug)
      System.out.println(_me + ": " + message);
  }
}
