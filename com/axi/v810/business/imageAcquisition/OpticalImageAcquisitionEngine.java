package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class is used to obtain Optical Camera images on Printed Circuit Board
 * (PCB).
 * 
 * @author Cheah Lee Herng
 */
public class OpticalImageAcquisitionEngine 
{
    private static Config _config = null;
    private static OpticalImageAcquisitionEngine _instance = null;
    
    private OpticalImageAcquisitionModeEnum _acquisitionMode = OpticalImageAcquisitionModeEnum.NOT_IN_USE;
    private ReconstructedOpticalImagesProducer _reconstructedOpticalImagesProducer;
    private HardwareObservable _hardwareObservable;
    private boolean _userAborted = false;
    private OpticalImageAcquisitionProgressMonitor _progressMonitor;
    
    private TestExecutionTimer _testExecutionTimer = null;
    
    /**
     * @author Cheah Lee Herng
    */
    static
    {
        _config = Config.getInstance();
    }

    /**
     * @author Cheah Lee Herng
    */
    public static synchronized OpticalImageAcquisitionEngine getInstance()
    {
        if (_instance == null)
        {
          _instance = new OpticalImageAcquisitionEngine();
        }
        return _instance;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private OpticalImageAcquisitionEngine()
    {
        _hardwareObservable = HardwareObservable.getInstance();
        _reconstructedOpticalImagesProducer = new ReconstructedOpticalImagesProducer();
        _progressMonitor = OpticalImageAcquisitionProgressMonitor.getInstance();
    }
    
    /**
     * @author Cheah Lee Herng      
     */
    public ReconstructedOpticalImagesProducer getReconstructedOpticalImagesProducer()
    {
        Assert.expect(_reconstructedOpticalImagesProducer != null);
        return _reconstructedOpticalImagesProducer;
    }
    
//    /**
//     * @author Cheah Lee Herng 
//     */
//    public void acquireProductionOpticalImages(TestProgram testProgram, boolean isDiagnosticsModeEnabled, TestExecutionTimer testExecutionTimer) throws XrayTesterException
//    {
//        Assert.expect(testProgram != null);
//        Assert.expect(testExecutionTimer != null);
//        
//        _userAborted = false;
//        _testExecutionTimer = testExecutionTimer;
//        _hardwareObservable.stateChangedBegin(this, ImageAcquisitionEventEnum.OPTICAL_IMAGE_ACQUISITION);
//        _hardwareObservable.setEnabled(false);
//
//        try
//        {
//            _reconstructedOpticalImagesProducer.clearForNextRun();
//            configureForReconstructedImages(OpticalImageAcquisitionModeEnum.PRODUCTION);
//            if ((XrayTester.getInstance().isSimulationModeOn() == false) || isPspOfflineReconstructionEnabled())
//            {
//                _reconstructedOpticalImagesProducer.acquireProductionOpticalImages(testProgram, testExecutionTimer, _acquisitionMode);
//            }
//        }
//        catch(XrayTesterException xex)
//        {
//          abort(xex);
//          throw xex;
//        }
//        finally
//        {
//            _acquisitionMode = OpticalImageAcquisitionModeEnum.NOT_IN_USE;
//            _hardwareObservable.setEnabled(true);
//        }
//        _hardwareObservable.stateChangedEnd(this, ImageAcquisitionEventEnum.OPTICAL_IMAGE_ACQUISITION);
//    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void acquireAlignmentOpticalImages(TestProgram testProgram, boolean isDiagnosticsModeEnabled, TestExecutionTimer testExecutionTimer) throws XrayTesterException
    {
      Assert.expect(testProgram != null);
      Assert.expect(testExecutionTimer != null);

      _userAborted = false;
      _testExecutionTimer = testExecutionTimer;
      _hardwareObservable.stateChangedBegin(this, ImageAcquisitionEventEnum.OPTICAL_IMAGE_ACQUISITION);
      _hardwareObservable.setEnabled(false);

      try
      {
        _reconstructedOpticalImagesProducer.clearForNextRun();
        configureForReconstructedImages(OpticalImageAcquisitionModeEnum.PRODUCTION);
        if ((XrayTester.getInstance().isSimulationModeOn() == false) || isPspOfflineReconstructionEnabled())
        {
          _reconstructedOpticalImagesProducer.acquireAlignmentOpticalImages(testProgram, testExecutionTimer, _acquisitionMode);
        }
      }
      catch(XrayTesterException xex)
      {
        abort(xex);
      }
      finally
      {
        _acquisitionMode = OpticalImageAcquisitionModeEnum.NOT_IN_USE;
        _hardwareObservable.setEnabled(true);
      }
      _hardwareObservable.stateChangedEnd(this, ImageAcquisitionEventEnum.OPTICAL_IMAGE_ACQUISITION);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    private void configureForReconstructedImages(OpticalImageAcquisitionModeEnum acquisitionMode)
    {
        Assert.expect(acquisitionMode != null);
        Assert.expect(_acquisitionMode.equals(OpticalImageAcquisitionModeEnum.NOT_IN_USE));
        Assert.expect(isLegalOpticalReconstructedImagesMode(acquisitionMode));

        _acquisitionMode = acquisitionMode;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    private boolean isLegalOpticalReconstructedImagesMode(OpticalImageAcquisitionModeEnum acquisitionMode)
    {
        Assert.expect(acquisitionMode != null);
        
        if (acquisitionMode.equals(OpticalImageAcquisitionModeEnum.PRODUCTION) ||
            acquisitionMode.equals(OpticalImageAcquisitionModeEnum.SINGLE_REGION_IMAGE) ||
            acquisitionMode.equals(OpticalImageAcquisitionModeEnum.CALIBRATION))
            return true;
        return false;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void abort() throws XrayTesterException
    {
        // If we are not in use, then this call should never be made.
        if (_acquisitionMode.equals(OpticalImageAcquisitionModeEnum.NOT_IN_USE))              
          return;

        if(_userAborted)
          return;
        
        try
        {
            _userAborted = true;
            
            _progressMonitor.stopMonitoring();
            if (isLegalOpticalReconstructedImagesMode(_acquisitionMode))
            {
                _reconstructedOpticalImagesProducer.userAbort();
            }
            else
                Assert.expect(false);
        }
        catch(XrayTesterException xex)
        {
          abort(xex);
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void abort(XrayTesterException ex)
    {
        Assert.expect(ex != null);

        // If we are not in use, then this call should never be made.
        if (_acquisitionMode.equals(ImageAcquisitionModeEnum.NOT_IN_USE))
          return;

        _progressMonitor.stopMonitoring();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public boolean isUserAborted()
    {
        return _userAborted;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public boolean isPspOfflineReconstructionEnabled()
    {
       return _config.getBooleanValue(SoftwareConfigEnum.OFFLINE_RECONSTRUCTION);
    }
}
