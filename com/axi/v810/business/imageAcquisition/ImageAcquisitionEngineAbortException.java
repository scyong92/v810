package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class ImageAcquisitionEngineAbortException extends XrayTesterException
{
  public ImageAcquisitionEngineAbortException()
  {
    super(new LocalizedString("HW_IMAGE_ACQUISITION_ENGINE_ABORT_KEY", null));
  }
}
