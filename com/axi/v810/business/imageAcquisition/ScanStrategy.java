package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;

/**
 * @author Roy Williams
 */
abstract public class ScanStrategy
{
  private ScanStrategyEnum _scanStrategy;

  static protected boolean _debug = false;
  protected static final boolean LARGEST  = true;
  protected static final boolean SMALLEST = false;

  /**
   * You can only set the strategy in constructor because it reflects a binding
   * of function implementations for create, merge, etc.
   *
   * @author Roy Williams
   */
  protected ScanStrategy(ScanStrategyEnum scanStrategy)
  {
    Assert.expect(scanStrategy != null);

    _scanStrategy = scanStrategy;
  }

  /**
   * @author Roy Williams
   */
  public ScanStrategyEnum getScanStrategy()
  {
    return _scanStrategy;
  }

  /**
   * @author Roy Williams
   */
  abstract public double getEstimatedExecutionTime();

  /**
   * @author Roy Williams
   */
  public static ScanStrategy implementStrategy(ScanStrategyEnum scanStrategyEnum)
  {
    Assert.expect(scanStrategyEnum != null);
    Assert.expect(scanStrategyEnum.equals(ScanStrategyEnum.DEPRECATED) == false);

    ScanStrategy scanStrategy = null;
    if (scanStrategyEnum.equals(ScanStrategyEnum.GROUP_ON_MAX_CANDIDATE_STEP_SIZE))
      scanStrategy = new RescanShadedRegionsBasedUponMaxCandidateStepSizeScanStrategy();
    else if (scanStrategyEnum.equals(ScanStrategyEnum.GROUP_ON_COMPONENTS_WITH_AREA_COMPRESSION))
      scanStrategy = new RescanShadedRegionsBasedUponComponentsWithAreaCompression();
    else if (scanStrategyEnum.equals(ScanStrategyEnum.GROUP_ON_COMPONENTS_WITH_AREA_AND_TIMING_COMPRESSION))
      scanStrategy = new RescanShadedRegionsBasedUponComponentsWithAreaAndTimingCompression();
    else if (scanStrategyEnum.equals(ScanStrategyEnum.MERGE_TO_MAIN))
      scanStrategy = new MergeRescanGroupsIntoMainGroup();
    Assert.expect(scanStrategy != null);
    return scanStrategy;
  }

  /**
   * This function is handy for printing a rectangle in a format good for Excel.
   * Seeing an ImageGroup is helpful when trying to understand proximity during
   * the merge function.
   *
   * @author Roy Williams
   */
  public static void printRectangle(String seriesName, IntRectangle rectangle)
  {
    int minX = rectangle.getMinX();
    int minY = rectangle.getMinY();
    int maxX = rectangle.getMaxX();
    int maxY = rectangle.getMaxY();

//    // Do x's first then y's
//    System.out.println(seriesName + " " + minX + " " + minX + " " + maxX + " " + maxX + " " + minX);
//    System.out.println(seriesName + " " + minY + " " + maxY + " " + maxY + " " + minY + " " + minY);

    System.out.print(minX + " " + minY);
    System.out.print(minX + " " + maxY);
    System.out.print(maxX + " " + maxY);
    System.out.print(maxX + " " + minY);
    System.out.print(minX + " " + minY);
    System.out.println();
//
//    System.out.print(minX + " " + minY + " ");
//    System.out.print(minX + " " + maxY + " ");
//    System.out.print(maxX + " " + maxY + " ");
//    System.out.print(maxX + " " + minY + " ");
//    System.out.println(minX + " " + minY);

  }

  /**
   * 152 scan passes
   * Estimated execution time: 3.328346924074853 minutes
   *
   * ImageGroup    #ReconstructionRegions  StepMin(mils)   StepMax(mils)
   * Main, 2493, 133, 509,
   * 0, 700, 133, 142,
   *
   * @author Roy Williams
   */
  public static List<List<HomogeneousImageGroup>> sortBasedOnStepSize(List<HomogeneousImageGroup> groups, final boolean size)
  {
    Comparator<HomogeneousImageGroup> comparator = new Comparator<HomogeneousImageGroup>()
    {
      public int compare(HomogeneousImageGroup g1, HomogeneousImageGroup g2)
      {
        int maxStepSizeG1 = g1.getMeanStepSizeInNanoMeters();
        int maxStepSizeG2 = g2.getMeanStepSizeInNanoMeters();
        if (size == LARGEST)
        {
          if (maxStepSizeG1 > maxStepSizeG2)
            return -1;
          else if (maxStepSizeG1 < maxStepSizeG2)
            return 1;
        }
        else
        {
          if (maxStepSizeG1 < maxStepSizeG2)
            return -1;
          else if (maxStepSizeG1 > maxStepSizeG2)
            return 1;
        }
        return 0;
      }
    };
    if (_debug)
      TimerUtil.printCurrentTime("        Start sorting groups size:"+groups.size());
    Collections.sort(groups, comparator);
    if (_debug)
      TimerUtil.printCurrentTime("        End sorting groups");

    // group into lists where each individual list has a homogeneous step size.
    List<List<HomogeneousImageGroup>> arrayOfLists = new ArrayList<List<HomogeneousImageGroup>>();
    List<HomogeneousImageGroup> list = null;
    int currentMeanStepSize = -1;
    if (_debug)
      TimerUtil.printCurrentTime("        Start creating arrays size:"+arrayOfLists.size());
    for (HomogeneousImageGroup group : groups)
    {
      int meanStepSizeForGroup = group.getMeanStepSizeInNanoMeters();
      if (currentMeanStepSize != meanStepSizeForGroup)
      {
        currentMeanStepSize = meanStepSizeForGroup;
        list = new ArrayList<HomogeneousImageGroup>();
        arrayOfLists.add(list);
      }
      list.add(group);
    }
    if (_debug)
      TimerUtil.printCurrentTime("        End creating arrays. arrayOfLists");
    return arrayOfLists;
  }


  /**
   * @author Roy Williams
   */
  public static void setDebug(boolean state)
  {
    _debug = state;
  }
}
