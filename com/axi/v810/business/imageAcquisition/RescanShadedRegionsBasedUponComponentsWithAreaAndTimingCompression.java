package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;

/**
 * @author Roy Williams
 */
public class RescanShadedRegionsBasedUponComponentsWithAreaAndTimingCompression extends RescanShadedRegionsBasedUponDripSourceComponent
{

  /**
   * @author Roy Williams
   */
  RescanShadedRegionsBasedUponComponentsWithAreaAndTimingCompression()
  {
    super(ScanStrategyEnum.GROUP_ON_COMPONENTS_WITH_AREA_AND_TIMING_COMPRESSION);
  }

  /**
   * @author Roy Williams
   * @author Dave Ferguson
   */
  public List<HomogeneousImageGroup> mergeGroups(List<HomogeneousImageGroup> imageGroups)
  {
    Assert.expect(imageGroups != null);
    List<List<HomogeneousImageGroup>> lists = sortBasedOnStepSize(imageGroups, LARGEST);

    return HomogeneousImageGroup.mergeGroupsBasedOnExecutionTimeEstimates(imageGroups);
  }
}
