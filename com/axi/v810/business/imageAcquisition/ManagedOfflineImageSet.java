package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.NativeMemoryMonitor;
import com.axi.v810.business.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * This class manages attempting to keep as many images as possible in memory from a set of image sets.
 *
 * This object supercedes the use of the ReconstructedImagesProducers, ReconstructedImagesManager, and
 * ReconstructedImagesMemoryManager. These objects work well for a single image set such as during inspection.
 *
 * This object is designed for Initial Tuning aka. Learning when we want to work with several image sets.
 *
 * @todo Comment with the order images are removed : LRU, Smallest first, largest first, ???
 *
 * @author Patrick Lacz
 */
public class ManagedOfflineImageSet
{
  private final ImageManager _imageManager = ImageManager.getInstance();
  private final NativeMemoryMonitor _nativeMemoryMonitor = NativeMemoryMonitor.getInstance();

  private Map<Pair<ImageSetData, ReconstructionRegion>, ReconstructedImages> _regionsInMemory = new HashMap<Pair<ImageSetData,ReconstructionRegion>, ReconstructedImages>();

  private Queue<Pair<ImageSetData, ReconstructionRegion>> _inactiveRegionRemovalQueue = new LinkedList<Pair<ImageSetData, ReconstructionRegion>>();
  private Set<Pair<ImageSetData, ReconstructionRegion>> _activeRegionsInMemory = new HashSet<Pair<ImageSetData,ReconstructionRegion>>();

  private Collection<ReconstructionRegion> _reconstructionRegions = null;

  private Collection<ImageSetData> _imageSetDataCollection = null;

  private float _loadLimit = 0;

  private int _size = 0;
  private Map<ImageSetData, List<ReconstructionRegion>> _imageSetToRegionListMap = new HashMap<ImageSetData,List<ReconstructionRegion>>();


  /**
   * @author Peter Esbensen
   */
  public ManagedOfflineImageSet(TestProgram testProgram, Collection<ImageSetData> imageSetDataCollection)
  {
    Assert.expect(testProgram != null);
    Assert.expect(imageSetDataCollection != null);

    _imageSetDataCollection = imageSetDataCollection;
    _reconstructionRegions = testProgram.getFilteredInspectionRegions();

    _loadLimit = Config.getInstance().getIntValue(SoftwareConfigEnum.IMAGE_INITIAL_TUNING_LOAD_LIMIT_IN_MEGABYTES);
    setupSize(testProgram);
  }

  /**
   * Marks the indicated ReconstructedImages object as active and managed by this object.
   * This method takes ownership of the ReconstructedImages object. Only this object should subsequently call freeImages() on it.
   * @author Patrick Lacz
   */
  private void addReconstructedImages(
      ImageSetData imageSetData,
      ReconstructionRegion reconstructionRegion,
      ReconstructedImages reconstructedImages)
  {
    Assert.expect(imageSetData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(reconstructedImages != null);

    Assert.expect(_imageSetDataCollection != null);
    Assert.expect(_imageSetDataCollection.contains(imageSetData));

    Pair<ImageSetData, ReconstructionRegion> regionKey = new Pair<ImageSetData, ReconstructionRegion>(imageSetData, reconstructionRegion);
    _activeRegionsInMemory.add(regionKey);
    _regionsInMemory.put(regionKey, reconstructedImages);
  }


  /**
   * @author Patrick Lacz
   */
  public ReconstructedImages getReconstructedImages(
      ImageSetData imageSetData,
      ReconstructionRegion reconstructionRegion) throws DatastoreException
  {
    Assert.expect(imageSetData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(_loadLimit > 100.f);

    // check to see if that region exists for that image set.
    List<ReconstructionRegion> regionListForImageSet = _imageSetToRegionListMap.get(imageSetData);
    if (regionListForImageSet.contains(reconstructionRegion) == false)
      return null;

    Pair<ImageSetData, ReconstructionRegion> regionKey = new Pair<ImageSetData, ReconstructionRegion>(imageSetData, reconstructionRegion);

    ReconstructedImages reconstructedImages = null;
    if (_regionsInMemory.containsKey(regionKey))
    {
      //Jack Hwee - make sure the regionKey in _activeRegionsInMemory will be removed
      if (_activeRegionsInMemory.contains(regionKey) == true)
        _activeRegionsInMemory.remove(regionKey);
      // already in memory. Make the region active and return it.
      Assert.expect(_activeRegionsInMemory.contains(regionKey) == false);
      // if we ever need the assert above to be false, we will need to go to some reference counting scheme (presumably in addition to the
      // per-image reference counting we're already doing. ick.)

      _activeRegionsInMemory.add(regionKey);
      _inactiveRegionRemovalQueue.remove(regionKey);

      reconstructedImages = _regionsInMemory.get(regionKey);
    }
    else
    {
      // region is not yet in memory. load it then return it.

      // check to see if we should off-load some images.
      while (_nativeMemoryMonitor.getTotalUsedMemoryInMegabytes() > _loadLimit
             && _inactiveRegionRemovalQueue.isEmpty() == false)
      {
        // free up some space
        Pair<ImageSetData, ReconstructionRegion> unusedRegionKey = _inactiveRegionRemovalQueue.remove();
        ReconstructedImages unusedReconstructedImages = _regionsInMemory.remove(unusedRegionKey);
        unusedReconstructedImages.decrementReferenceCount();
      }

      reconstructedImages = _imageManager.loadOnlyInspectedImages(imageSetData, reconstructionRegion);
      reconstructedImages.ensureInspectedSlicesAreReadyForInspection();
      addReconstructedImages(imageSetData, reconstructionRegion, reconstructedImages);
    }

    Assert.expect(reconstructedImages != null);
    return reconstructedImages;
  }

  /**
   * @author Patrick Lacz
   */
  public void finishedWithReconstructionRegion(
      ImageSetData imageSetData,
      ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(imageSetData != null);
    Assert.expect(reconstructionRegion != null);

    Pair<ImageSetData, ReconstructionRegion> regionKey = new Pair<ImageSetData, ReconstructionRegion>(imageSetData, reconstructionRegion);

    if (_activeRegionsInMemory.contains(regionKey))
    {
      _activeRegionsInMemory.remove(regionKey);
      Assert.expect(_activeRegionsInMemory.contains(regionKey) == false);

      _inactiveRegionRemovalQueue.add(regionKey);
    }
  }

  /**
   * @todo create variants of this method for different iterators one might want to use.
   * @author Patrick Lacz
   */
  public ManagedOfflineImageSetIterator iterator()
  {
    Assert.expect(_imageSetDataCollection != null);
    Assert.expect(_reconstructionRegions != null);

    return new ManagedOfflineImageSetIterator(this, _imageSetDataCollection, _reconstructionRegions);
  }

  /**
   * @author Patrick Lacz
   */
  public Collection<ReconstructionRegion> getReconstructionRegions()
  {
    Assert.expect(_reconstructionRegions != null);
    return _reconstructionRegions;
  }

  /**
   * @author Patrick Lacz
   */
  public void cleanUp()
  {
    for (ReconstructedImages reconstructedImages : _regionsInMemory.values())
    {
      reconstructedImages.decrementReferenceCount();
    }

    _regionsInMemory.clear();
    _activeRegionsInMemory.clear();
    _inactiveRegionRemovalQueue.clear();
  }

  /**
   * Returns the number of regions with images of all image sets.
   *
   * @author Patrick Lacz
   */
  public int size()
  {
    return _size;
  }

  /**
   * @author Peter Esbensen
   */
  private void setupSize(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);
    Assert.expect(_imageSetDataCollection != null);

    for (ImageSetData imageSetData : _imageSetDataCollection)
    {
      List<ReconstructionRegion> regionsInImageSetDataSet = new ArrayList<ReconstructionRegion>();

      for (ReconstructionRegion reconstructionRegion : testProgram.getFilteredInspectionRegions())
      {
        if (_imageManager.reconstructedImagesFilesExist(imageSetData, reconstructionRegion))
        {
          regionsInImageSetDataSet.add(reconstructionRegion);
          _size++;
        }
      }

      Collections.shuffle(regionsInImageSetDataSet);
      _imageSetToRegionListMap.put(imageSetData, regionsInImageSetDataSet);
    }
  }

  /**
   * @author Patrick Lacz
   */
  public Collection<ReconstructionRegion> getRegionsForImageSet(ImageSetData imageSetData)
  {
    Assert.expect(imageSetData != null);
    Assert.expect(_imageSetToRegionListMap != null);
    Assert.expect(_imageSetToRegionListMap.containsKey(imageSetData));

    return _imageSetToRegionListMap.get(imageSetData);
  }
}
