package com.axi.v810.business.imageAcquisition;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;




/**
 * @author Roy Williams
 */
public class Test_ReconstructedImagesManager extends UnitTest
{
  private TestableReconstructedImagesManager _reconstructedImagesManager = null;

  // Hard coding the value of dummyRecordSize because this is the value that should
  // be confirmed when executing the testMemorySizeCalulation() test.
  private int _dummyRecordSize = 2048;

  private int _generatedRegionCount = -1;
  static final int _MAXIMUM_IMAGE_MEMORY_SIZE = 32*1024*1024;

  /**
   * @author Roy Williams
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    NativeMemoryMonitor.initialize(_MAXIMUM_IMAGE_MEMORY_SIZE, (int)(.2*_MAXIMUM_IMAGE_MEMORY_SIZE));
    try
    {
      _generatedRegionCount = 0;

      ReconstructedImagesProducer reconstructedImagesProducer = new ReconstructedImagesProducer(1);
      _reconstructedImagesManager = new TestableReconstructedImagesManager(reconstructedImagesProducer);
      _reconstructedImagesManager.initialize();

      // Size of Images summer.
      testMemorySizeCalulation();

      // put a record then get it back.
      testPutRecordThenGetItBack();

      /** @todo: mdw - we may no longer need to do pause/resume now that we're using a new memory management scheme */
      // Test that a get blocks correctly when the queue is empty.
      testGetBlocksWhenQueueIsEmpty();

      // put a record, make it believe its full then check that block works.
      /** @todo PWL : Re-enable this test when you figure out how to test it. */
//      testPutsCauseIrpPauseWhenQueueIsFull();
    }
    catch (Throwable ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * Create a dummy record to be used in exercising puts and gets to the
   * ReconstructedImagesManager.
   *
   * @author Roy Williams
   */
  private ReconstructedImages createDummyReconstructedImages(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    ReconstructedImages reconstructedImages = new ReconstructedImages(reconstructionRegion);
    ImageRectangle imageRectangle = reconstructionRegion.getRegionRectangleInPixels();
    Image midballImage = new Image(imageRectangle.getWidth(), imageRectangle.getHeight());
    Image throughholeImage = new Image(imageRectangle.getWidth(), imageRectangle.getHeight());
    
    List<Float> arrayZHeight = new ArrayList<Float>();//add by BH
    List<Float> arraySharpness = new ArrayList<Float>();//add by BH
              
    reconstructedImages.addImage(midballImage, SliceNameEnum.MIDBALL, 10, arrayZHeight, arraySharpness);//add by BH
    reconstructedImages.addImage(throughholeImage, SliceNameEnum.THROUGHHOLE_BARREL, 10, arrayZHeight, arraySharpness);//add by BH);
    midballImage.decrementReferenceCount();
    throughholeImage.decrementReferenceCount();

    return reconstructedImages;
  }

  /**
   * @author Patrick Lacz
   */
  private ReconstructionRegion createDummyReconstructionRegion()
  {
    ReconstructionRegion reconstructionRegion = new ReconstructionRegion();
    reconstructionRegion.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.INSPECTION);
    PanelRectangle regionRectangle = new PanelRectangle( _generatedRegionCount * 254000, _generatedRegionCount * 254000, 254000, 254000);
    reconstructionRegion.setRegionRectangleRelativeToPanelInNanoMeters(regionRectangle);
    reconstructionRegion.setTopSide((_generatedRegionCount % 2) == 0);
    ++_generatedRegionCount;
    return reconstructionRegion;
  }

  /**
   * Make sure the size of the record returned matches the hardcoded value found
   * in _dummyRecordSize.
   *
   * @author Roy Williams
   */
  private void testMemorySizeCalulation()
  {
    ReconstructedImages reconstructedImages = createDummyReconstructedImages(createDummyReconstructionRegion());

    Expect.expect(reconstructedImages.getMaximumExpectedMemorySize() == 1568); //2048 is for Magnification = 6.25, 1568 is for Magnification = 5.26
    reconstructedImages.decrementReferenceCount();
  }

  /**
   * Put a single ReconstructedImages object.
   *
   * @author Roy Williams
   */
  private ReconstructedImages testPut() throws DatastoreException, XrayTesterException
  {
    int initialSize = _reconstructedImagesManager.size();
    ReconstructedImages reconstructedImages = createDummyReconstructedImages(createDummyReconstructionRegion());
    Collection<ReconstructionRegion> requestedRegions = new ArrayList<ReconstructionRegion>();
    requestedRegions.add(reconstructedImages.getReconstructionRegion());
    _reconstructedImagesManager.setRequestedRegions(requestedRegions);
    _reconstructedImagesManager.putReconstructedImages(reconstructedImages);
    Expect.expect(initialSize + 1 == _reconstructedImagesManager.size());
    return reconstructedImages;
  }

  /**
   * Get a single ReconstructedImages object.
   *
   * @author Roy Williams
   */
  private void testGet(ReconstructedImages matchingReconstructedImages) throws XrayTesterException
  {
    int initialSize = _reconstructedImagesManager.size();
    BooleanRef abortedDuringGet = new BooleanRef(false);
    ReconstructedImages reconstructedImages = _reconstructedImagesManager.getReconstructedImages(abortedDuringGet);
    Expect.expect(initialSize - 1 == _reconstructedImagesManager.size());
    if (matchingReconstructedImages != null)
    {
      Expect.expect(reconstructedImages.equals(matchingReconstructedImages));
    }
    reconstructedImages.decrementReferenceCount();
  }

  /**
   * Push a record into the ReconstructedImages and retrieve it.  Make sure the
   * object id's are the same.
   *
   * @author Roy Williams
   */
  private void testPutRecordThenGetItBack() throws DatastoreException, XrayTesterException
  {
    ReconstructedImages reconstructedImages = testPut();
    testGet(reconstructedImages);
    _reconstructedImagesManager.freeReconstructedImages(reconstructedImages.getReconstructionRegion());
  }

  /**
   * Put a record, make it believe its full then check that block works.
   *
   * @author Roy Williams
   */
  private void testPutsCauseIrpPauseWhenQueueIsFull() throws DatastoreException, XrayTesterException
  {
    // Reset the pause/resume state.
    _reconstructedImagesManager.setPauseOccured(false);
    _reconstructedImagesManager.setResumeOccured(false);

    _reconstructedImagesManager.initialize();

    long currentMemoryUsed = _reconstructedImagesManager.setMemoryUsedRelativeToPause(-_dummyRecordSize);

    ReconstructedImages reconstructedImages1 = createDummyReconstructedImages(createDummyReconstructionRegion());
    final ReconstructionRegion reconstructionRegion2 = createDummyReconstructionRegion();

    Collection<ReconstructionRegion> requestedRegions = new ArrayList<ReconstructionRegion>();
    requestedRegions.add(reconstructedImages1.getReconstructionRegion());
    requestedRegions.add(reconstructionRegion2);
    _reconstructedImagesManager.setRequestedRegions(requestedRegions);

    _reconstructedImagesManager.putReconstructedImages(reconstructedImages1);
    long currentTime = System.currentTimeMillis();

    // Put thread will wait until resume.
    Thread putThread = new Thread(new Runnable()
    {
      public void run()
      {
        try
        {
          ReconstructedImages reconstructedImages2 = createDummyReconstructedImages(reconstructionRegion2);
          _reconstructedImagesManager.putReconstructedImages(reconstructedImages2);
          Expect.expect(_reconstructedImagesManager.getPauseOccured() == true);
        }
        catch (DatastoreException dex)
        {
          Expect.expect(false, dex.getMessage());
        }
        catch (XrayTesterException xex)
        {
          Expect.expect(false, xex.getMessage());
        }
      }
    }, getClass().getName());
    putThread.start();

    // Suspend this thread.   If we just go fetch (i.e. getReconstructedImages())
    // then a race condition manifests and we cannot adequately test that a
    // blocking condition has been met.
    try
    {
      Thread.sleep(2000);
    }
    catch (InterruptedException e)
    {
      // Shouldn't occur.
      Assert.expect(false);
    }

    BooleanRef abortedDuringGet = new BooleanRef(false);
    ReconstructedImages reconstructedImages3 = _reconstructedImagesManager.getReconstructedImages(abortedDuringGet);

    // For this test to complete, we must wait for the thread to complete.
    try
    {
      putThread.join();
    }
    catch (InterruptedException e)
    {
      // Shouldn't occur.
      Assert.expect(false);
    }


    Expect.expect(reconstructedImages3 != null);
    Expect.expect(reconstructedImages1.equals(reconstructedImages3));

    reconstructedImages3.decrementReferenceCount();
    _reconstructedImagesManager.freeReconstructedImages(reconstructedImages1.getReconstructionRegion());
    _reconstructedImagesManager.freeReconstructedImages(reconstructionRegion2);

//    _reconstructedImagesManager.initialize();
//
//    // Reset the pause/resume state.
//    _reconstructedImagesManager.setPauseOccured(false);
//    _reconstructedImagesManager.setResumeOccured(false);
//
//    // Make sure there are 2 records to retrieve.
//    ReconstructedImages reconstructedImages3 = createDummyReconstructedImages();
//    _reconstructedImagesManager.putReconstructedImages(reconstructedImages1);
//    _reconstructedImagesManager.putReconstructedImages(reconstructedImages3);
//
//    _reconstructedImagesManager.getReconstructedImages(abortedDuringGet);
//    _reconstructedImagesManager.freeReconstructedImages(reconstructedImages1.getReconstructionRegion());
//    Expect.expect(_reconstructedImagesManager.getResumeOccured() == false);
//
//    _reconstructedImagesManager.getReconstructedImages(abortedDuringGet);
//    _reconstructedImagesManager.freeReconstructedImages(reconstructedImages3.getReconstructionRegion());
//    Expect.expect(_reconstructedImagesManager.getResumeOccured() == true);
  }

  /**
   * Get is supposed to block when the queue is empty.  Nothing has been delivered.
   * This is not to say that something won't be delivered shortly.  So, when the
   * put occurs, be sure the waiting get is notified.
   *
   * @author Roy Williams
   * @author Matt Wharton
   */
  private void testGetBlocksWhenQueueIsEmpty() throws DatastoreException, XrayTesterException
  {
    final int getDelayTimeInMillis = 1000;
    final ReconstructedImages reconstructedImages1 = createDummyReconstructedImages(createDummyReconstructionRegion());
    _reconstructedImagesManager.initialize();
    Collection<ReconstructionRegion> requestedRegions = new ArrayList<ReconstructionRegion>();
    requestedRegions.add(reconstructedImages1.getReconstructionRegion());
    _reconstructedImagesManager.setRequestedRegions(requestedRegions);

    Thread thread = new Thread(new Runnable()
               {
                 public void run()
                 {
                   try
                   {
                     BooleanRef abortedDuringGet = new BooleanRef(false);
                     TimerUtil timer = new TimerUtil();
                     timer.start();
                     ReconstructedImages reconstructedImages2 = _reconstructedImagesManager.getReconstructedImages(
                         abortedDuringGet);
                     timer.stop();
                     long elapsedTimeInMillis = timer.getElapsedTimeInMillis();

                     Expect.expect(reconstructedImages1.equals(reconstructedImages2));
                     reconstructedImages2.decrementReferenceCount();
                     Expect.expect((elapsedTimeInMillis + 100) >= getDelayTimeInMillis);
                   }
                   catch (XrayTesterException xtex)
                   {
                     Expect.expect(false, xtex.getMessage());
                   }
                 }
               },
               getClass().getName());
    thread.start();

    try
    {
      Thread.sleep(getDelayTimeInMillis);
    }
    catch (InterruptedException e)
    {
      // Shouldn't occur.
      Assert.expect(false);
    }

    _reconstructedImagesManager.putReconstructedImages(reconstructedImages1);

    // Wait for the thread to finish.
    try
    {
      thread.join();
    }
    catch (InterruptedException iex)
    {
      // Shouldn't occur.
      Assert.expect(false);
    }

    _reconstructedImagesManager.freeReconstructedImages(reconstructedImages1.getReconstructionRegion());
  }

  /**
   * @author Roy Williams
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_ReconstructedImagesManager() );
  }
}

/**
 * @author Roy Williams
 */
class TestableReconstructedImagesManager extends ReconstructedImagesManager
{
  private volatile boolean _pauseOccured = false;
  private volatile boolean _resumeOccured = false;

  private static final String _MEMORY_MANAGER_MEMORY_USED_FIELD_NAME = "_memoryUsed";


  private Set<Image> _images = new HashSet<Image>();
  /**
   * @author Matt Wharton
   */
  TestableReconstructedImagesManager(ReconstructedImagesProducer reconstructedImagesProducer)
  {
    super(reconstructedImagesProducer);
  }

  /**
   * @author Roy Williams
   */
  public synchronized void pauseImageAcquisition() throws HardwareException
  {
    _pauseOccured = true;
  }

  /**
   * @author Roy Williams
   */
  public synchronized void resumeImageAcquisition() throws HardwareException
  {
    _resumeOccured = true;
  }

  /**
   * @author Roy Williams
   */
  public void abort(XrayTesterException e)
  {
  }

  /**
   * @author Roy Williams
   */
  public XrayTesterException aborted()
  {
    return null;
  }

  /**
   * @author Roy Williams
   * @author Matt Wharton
   */
  public long setMemoryUsedRelativeToMaximum(long deltaAboveOrBelowMaximumCap)
  {
    Assert.expect(_reconstructedImagesMemoryManager != null);

    long newMemoryUsed = Test_ReconstructedImagesManager._MAXIMUM_IMAGE_MEMORY_SIZE + (int)deltaAboveOrBelowMaximumCap;

    long currentMemoryUsed = Test_ReconstructedImagesManager._MAXIMUM_IMAGE_MEMORY_SIZE - NativeMemoryMonitor.getInstance().getTotalFreeMemory();

    for (Image image : _images)
      image.decrementReferenceCount();
    _images.clear();
    long allocatedSpace = currentMemoryUsed;
    while (allocatedSpace < newMemoryUsed)
    {
      if (newMemoryUsed - allocatedSpace > 1024*10*4)
      {
        Image newImage = new Image(1024, 10);
        _images.add(newImage);
        allocatedSpace += 1024*10*4;
      }
      else
      {
        long remainingSpace = newMemoryUsed - allocatedSpace;
        Image newImage = new Image(1, (int)remainingSpace/4);
        _images.add(newImage);
        allocatedSpace += ((int)remainingSpace/4)*4;
      }
    }

    return allocatedSpace;
  }

  /**
   * @author Roy Williams
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  public long setMemoryUsedRelativeToResume(long deltaAboveOrBelowResumeThreshold)
  {
    Assert.expect(_reconstructedImagesMemoryManager != null);

    int resumeMemorySize = (int)Math.round(_MAX_IMAGE_MEMORY_USAGE_BEFORE_IRP_RESUME);
    int baseOffset = resumeMemorySize - Test_ReconstructedImagesManager._MAXIMUM_IMAGE_MEMORY_SIZE;
    return setMemoryUsedRelativeToMaximum(baseOffset + (int)deltaAboveOrBelowResumeThreshold);
  }

  /**
   * @author Patrick Lacz
   */
  public long setMemoryUsedRelativeToPause(long deltaAboveOrBelowResumeThreshold)
  {
    Assert.expect(_reconstructedImagesMemoryManager != null);

    int pauseMemorySize = (int)Math.round(_MAX_IMAGE_MEMORY_USAGE_BEFORE_IRP_PAUSE);
    int baseOffset = pauseMemorySize - Test_ReconstructedImagesManager._MAXIMUM_IMAGE_MEMORY_SIZE;
    return setMemoryUsedRelativeToMaximum(baseOffset + (int)deltaAboveOrBelowResumeThreshold);
  }

  /**
   * @author Matt Wharton
   */
  synchronized boolean getPauseOccured()
  {
    return _pauseOccured;
  }

  /**
   * @author Matt Wharton
   */
  synchronized void setPauseOccured(boolean pauseOccured)
  {
    _pauseOccured = pauseOccured;
  }

  /**
   * @author Matt Wharton
   */
  synchronized boolean getResumeOccured()
  {
    return _resumeOccured;
  }

  /**
   * @author Matt Wharton
   */
  synchronized void setResumeOccured(boolean resumeOccured)
  {
    _resumeOccured = resumeOccured;
  }
}
