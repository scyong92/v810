package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 * This class allows Observers to be updated each time an execption occurs in
 * the OpticalImageAcqusitionEngine.
 * 
 * @author Cheah Lee Herng
 */
public class OpticalImageAcquisitionExceptionObservable extends Observable 
{
    private static OpticalImageAcquisitionExceptionObservable _instance;
    
    /**
     * @author Roy Williams
    */
    private OpticalImageAcquisitionExceptionObservable()
    {
        // do nothing
    }

    /**
     * @return an instance of this object.
     * @author Roy Williams
    */
    public static synchronized OpticalImageAcquisitionExceptionObservable getInstance()
    {
        if (_instance == null)
          _instance = new OpticalImageAcquisitionExceptionObservable();

        return _instance;
    }

    /**
     * Notify observers of the problem.
     *
     * @author Roy Williams
    */
    public void notifyObserversOfException(XrayTesterException xRayTesterException)
    {
        Assert.expect(xRayTesterException != null);
        setChanged();
        notifyObservers(xRayTesterException);
    }
}
