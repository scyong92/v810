package com.axi.v810.business.imageAcquisition;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class MergeRescanGroupsIntoMainGroup extends ScanStrategy
{
  private ImageGroup _mergedMainImageGroup = null;
  private List<HomogeneousImageGroup> _unmergedRescanGroups = null;

  /**
   * @author Roy Williams
   */
  MergeRescanGroupsIntoMainGroup()
  {
    super(ScanStrategyEnum.MERGE_TO_MAIN);
  }

  /**
   * @author Kay Lannen
   */
  public double getEstimatedExecutionTime()
  {
    double totalTime = 0.0;
    List<ImageGroup> imageGroups = getAllPostMergeImageGroups();
    for (ImageGroup imageGroup : imageGroups)
    {
      totalTime += imageGroup.estimateExecutionTimeForScanPassesInSecondsUsingScanPath();
    }
    return (totalTime);
  }

  /**
   * @author Kay Lannen
   */
  public ImageGroup getMergedMainImageGroup()
  {
    return (_mergedMainImageGroup);
  }

  /**
   * @author Kay Lannen
   */
  public List<HomogeneousImageGroup> getUnmergedRescanGroups()
  {
    return (_unmergedRescanGroups);
  }

  /**
    * @author Kay Lannen
   */
  public List<ImageGroup> getAllPostMergeImageGroups()
  {
    Assert.expect(_mergedMainImageGroup != null);
    Assert.expect(_unmergedRescanGroups != null);
    List<ImageGroup> imageGroups = new ArrayList<ImageGroup>();
    imageGroups.add(_mergedMainImageGroup);
    for (HomogeneousImageGroup rescanGroup : _unmergedRescanGroups)
    {
      imageGroups.add(rescanGroup);
    }
    return (imageGroups);
  }

  /**
   * @author Kay Lannen
   */
  public void mergeToMain(HomogeneousImageGroup mainImageGroup, List<HomogeneousImageGroup> signalCompensatedGroups)
  {
    _unmergedRescanGroups = new ArrayList<HomogeneousImageGroup>(signalCompensatedGroups);

    // Make sure scan paths for rescan groups overlap sufficiently with the scan paths for the main group.
    // Identify eligible groups for the merge.
    List<HomogeneousImageGroup> eligibleRescanGroups = new ArrayList<HomogeneousImageGroup>(signalCompensatedGroups);
    testForOverlapWithMain(mainImageGroup, eligibleRescanGroups);

    // Reorder the eligible groups based on time savings of merging them with the mainImageGroup.
    sortByDecreasingTimeSavingsForMerge(mainImageGroup, eligibleRescanGroups);

    // Go through all eligible rescan image groups, starting with the ones that have the largest potential
    // time savings, and merge them if they do not overlap with other merged rescan image groups.
    List<HomogeneousImageGroup> mergedRescanGroups = new ArrayList<HomogeneousImageGroup>();
    for (HomogeneousImageGroup shadedGroup : eligibleRescanGroups)
    {

      // Time could be saved by merging this group in.  However, we also need to check for overlap conflicts
      // with other scan groups that have also been merged in.
      boolean preventMergeDueToConflict = false;
      for (HomogeneousImageGroup previouslyMergedGroup : mergedRescanGroups)
      {
        // We won't allow any intersections of the Scan Paths.
        MachineRectangle prevScanPathExtents = previouslyMergedGroup.getScanPath().getScanPathExtents();
        MachineRectangle currScanPathExtents = shadedGroup.getScanPath().getScanPathExtents();
        Pair<Integer, Integer> prevRange = new Pair<Integer, Integer>(prevScanPathExtents.getMinX(),
                                                                      prevScanPathExtents.getMaxX());

        Pair<Integer, Integer> currRange = new Pair<Integer, Integer>(currScanPathExtents.getMinX(),
                                                                      currScanPathExtents.getMaxX());
        List<Pair<Integer, Integer>> a = new ArrayList<Pair<Integer, Integer>>();
        a.add(prevRange);
        List<Pair<Integer, Integer>> b = new ArrayList<Pair<Integer, Integer>>();
        b.add(currRange);
        List<Pair<Integer, Integer>> results = RangeUtil.getIntersectingRanges(a, b);
        if (results.size() > 0)
        {
          preventMergeDueToConflict = true;
          break;
        }
      }

      if (preventMergeDueToConflict == false)
      {
        _unmergedRescanGroups.remove(shadedGroup);
        mergedRescanGroups.add(shadedGroup);
      }

    }

    // Update the merged main image group.
    if (mergedRescanGroups.size() > 0)
    {
      _mergedMainImageGroup = mergeImageGroups(mainImageGroup, mergedRescanGroups);
    }
    else
    {
      _mergedMainImageGroup = mainImageGroup;
    }
  }

  /**
    * @author Kay Lannen
    * @author Roy Williams
   */
  protected void testForOverlapWithMain(HomogeneousImageGroup mainImageGroup,
                                        List<HomogeneousImageGroup> eligibleRescanGroups)
  {
    MachineRectangle mainScanPathExtents = mainImageGroup.getScanPath().getScanPathExtents();

    // Ideally, we want rescan regions to be contained entirely within the main region.
    // However, sometimes a rescan region may have a small portion whose scan path does
    // not overlap the extents of the main region, and that is okay.  This test allows up
    // to one main mean step size worth of overhang on each side of the main region.
    int allowedOverhangInNanoMeters = mainImageGroup.getMeanStepSizeInNanoMeters();
    MachineRectangle expandedMainScanPathExtents = new MachineRectangle(
        mainScanPathExtents.getMinX() - allowedOverhangInNanoMeters,
        mainScanPathExtents.getMinY() - allowedOverhangInNanoMeters,
        mainScanPathExtents.getWidth() + 2 * allowedOverhangInNanoMeters,
        mainScanPathExtents.getHeight() + 2 * allowedOverhangInNanoMeters);

    List<HomogeneousImageGroup> removalList = null;
    for (HomogeneousImageGroup rescanImageGroup : eligibleRescanGroups)
    {
      MachineRectangle rescanScanPathExtents = rescanImageGroup.getScanPath().getScanPathExtents();
      if (!expandedMainScanPathExtents.contains(rescanScanPathExtents))
      {
        if (removalList == null)
          removalList = new ArrayList<HomogeneousImageGroup>();
        removalList.add(rescanImageGroup);
      }
    }

    // Finally, remove any groups that need to be removed now that the evaluation
    // is overwith.  This is done here to prevent a ConcurrentModificationException.
    if (removalList != null)
    {
      for (HomogeneousImageGroup rescanImageGroup : removalList)
      {
        eligibleRescanGroups.remove(rescanImageGroup);
      }
    }
  }

  /**
   * @author Kay Lannen
   */
  protected void sortByDecreasingTimeSavingsForMerge(HomogeneousImageGroup mainImageGroup,
                                                     List<HomogeneousImageGroup> eligibleRescanGroups)
  {
    Comparator<Pair<HomogeneousImageGroup, Double>> comparator = new Comparator<Pair<HomogeneousImageGroup, Double>>()
    {
      public int compare(Pair<HomogeneousImageGroup, Double> groupA, Pair<HomogeneousImageGroup, Double> groupB)
      {
        double aGroupTimeSavings = groupA.getSecond();
        double bGroupTimeSavings = groupB.getSecond();
        if (aGroupTimeSavings > bGroupTimeSavings)
        {
          return -1;
        }
        else if (aGroupTimeSavings < bGroupTimeSavings)
        {
          return 1;
        }
        return 0;
      }
    };

    // Compute the time savings for each image group if it is integrated with the main image group.
    List<Pair<HomogeneousImageGroup, Double>> imageGroupTimeSavingsPairs = new ArrayList<Pair<HomogeneousImageGroup, Double>>();
    for (HomogeneousImageGroup shadedGroup : eligibleRescanGroups)
    {
      double estimateForShadedGroup = shadedGroup.estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSizeMethodA();
      SystemFiducialRectangle shadedGroupRectangle = shadedGroup.getNominalAreaToImage();
      int shadedGroupRectangleWidth = shadedGroupRectangle.getWidth();
      double estimateOverExtendedShadedAreaAtMainMeanStepSize =
          ImageGroup.estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSizeMethodA(
              shadedGroupRectangleWidth,
              mainImageGroup.getNominalAreaToImage().getHeight(),
              mainImageGroup.getMeanStepSizeInNanoMeters());
      double estimateOverExtendedShadedAreaAtShadedMeanStepSize =
          mainImageGroup.estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSizeMethodA(
              shadedGroupRectangleWidth,
              mainImageGroup.getNominalAreaToImage().getHeight(),
              shadedGroup.getMeanStepSizeInNanoMeters());
      double timeSavings = (estimateForShadedGroup + estimateOverExtendedShadedAreaAtMainMeanStepSize) -
                           estimateOverExtendedShadedAreaAtShadedMeanStepSize;
      if (timeSavings > 0)
      {
        Pair<HomogeneousImageGroup, Double> imageGroupTimeSavingsPair =
            new Pair<HomogeneousImageGroup, Double>(shadedGroup, new Double(timeSavings));
        imageGroupTimeSavingsPairs.add(imageGroupTimeSavingsPair);
      }
    }

    // Sort the image groups which would have positive time savings in order of decreasing time savings.
    // Image groups which do not have any time savings are not included in the pairs.
    Collections.sort(imageGroupTimeSavingsPairs, comparator);

    // Update the eligibleRescanGroups so that imageGroups with no time savings are removed, and the
    // image groups which do have time savings are sorted in decreasing order.
    eligibleRescanGroups.clear();
    for (Pair<HomogeneousImageGroup, Double> imageGroupTimeSavingsPair : imageGroupTimeSavingsPairs)
    {
      eligibleRescanGroups.add(imageGroupTimeSavingsPair.getFirst());
    }
  }

  /**
   * Merges the rescanImageGroups into the mainImageGroup.
   * This routine assumes that the rescanImageGroups are eligible for the merge, which includes having
   * their x-ranges non-overlapping with each other.
   * @author Kay Lannen
   */
  protected HeterogeneousImageGroup mergeImageGroups(HomogeneousImageGroup mainImageGroup,
                                                     List<HomogeneousImageGroup> rescanImageGroups)
  {
    Assert.expect(mainImageGroup != null);
    Assert.expect(rescanImageGroups != null);
    Assert.expect(rescanImageGroups.size() > 0);

    MachineRectangle mainScanPathExtents = mainImageGroup.getScanPath().getScanPathExtents();
    HeterogeneousImageGroup mergedMainImageGroup = new HeterogeneousImageGroup(mainImageGroup);
    mergedMainImageGroup.getImageZones().clear();

    // Determine superset Y-travel which will cover both the original main group and the
    // merged rescan groups.
    int yTravelStart = mainImageGroup.getScanPath().getMechanicalConversions().stageTravelStart();
    int yTravelEnd = mainImageGroup.getScanPath().getMechanicalConversions().stageTravelEnd();
    for (HomogeneousImageGroup rescanGroup : rescanImageGroups)
    {
      yTravelStart = Math.min(yTravelStart, rescanGroup.getScanPath().getMechanicalConversions().stageTravelStart());
      yTravelEnd = Math.max(yTravelEnd, rescanGroup.getScanPath().getMechanicalConversions().stageTravelEnd());
    }

    // Copy the scan passes from the rescan image zones to the extended rescan image zones,
    // extending the length of the scan passes if necessary.
    for (HomogeneousImageGroup rescanGroup : rescanImageGroups)
    {
      mergedMainImageGroup.add(rescanGroup);
      ImageZone rescanImageZone = rescanGroup.getImageZones().first();
      MachineRectangle rescanScanPathExtents = rescanImageZone.getScanPathExtents();
      ImageZone extendedRescanZone = new ImageZone();
      copyStepSizeSettingsToImageZone(extendedRescanZone, rescanImageZone);
      copyScanPassesToImageZone(extendedRescanZone, rescanImageZone,
                                rescanScanPathExtents.getMinX(),
                                rescanScanPathExtents.getMaxX(),
                                yTravelStart,
                                yTravelEnd);
      mergedMainImageGroup.getImageZones().add(extendedRescanZone);
    }

    // Fill in the gaps between extended rescan image zones with scan passes from the original main
    // image group.
    ImageZone originalMainZone = mainImageGroup.getImageZones().first();
    ImageZone previousRescanZone = null;
    List<ImageZone> imageZonesAtMainStepSize = new ArrayList<ImageZone>();

    // Image zones are sorted from left-most to right-most.
    for (ImageZone extendedRescanZone : mergedMainImageGroup.getImageZones())
    {
      if (extendedRescanZone == mergedMainImageGroup.getImageZones().first())
      {
        if (mainScanPathExtents.getMinX() < extendedRescanZone.getScanPathExtents().getMinX())
        {
          // There may be scan passes to the left of the first merged rescan zone which need to be copied.
          ImageZone leftMainZone = new ImageZone();
          copyStepSizeSettingsToImageZone(leftMainZone, originalMainZone);
          copyScanPassesToImageZone(leftMainZone, originalMainZone,
                                    originalMainZone.getScanPathExtents().getMinX(),
                                    extendedRescanZone.getScanPathExtents().getMinX() - 1,
                                    yTravelStart,
                                    yTravelEnd);
          if (leftMainZone.getScanPath().getScanPasses().size() > 0)
          {
            imageZonesAtMainStepSize.add(leftMainZone);
          }
        }
      }
      else
      {
        if (previousRescanZone.getScanPathExtents().getMaxX() < (extendedRescanZone.getScanPathExtents().getMaxX() + 1))
        {
          // There may be scan passes between the previousRescanZone and the extendedRescanZone which need to be
          // copied.
          ImageZone middleMainZone = new ImageZone();
          copyStepSizeSettingsToImageZone(middleMainZone, originalMainZone);
          copyScanPassesToImageZone(middleMainZone, originalMainZone,
                                    previousRescanZone.getScanPathExtents().getMaxX() + 1,
                                    extendedRescanZone.getScanPathExtents().getMinX() - 1,
                                    yTravelStart,
                                    yTravelEnd);
          if (middleMainZone.getScanPath().getScanPasses().size() > 0)
          {
            imageZonesAtMainStepSize.add(middleMainZone);
          }
        }
      }

      if (extendedRescanZone == mergedMainImageGroup.getImageZones().last())
      {
        // There may be scan passes to the right of the first merged rescan zone which need to be copied.
        ImageZone rightMainZone = new ImageZone();
        copyStepSizeSettingsToImageZone(rightMainZone, originalMainZone);
        copyScanPassesToImageZone(rightMainZone, originalMainZone,
                                  extendedRescanZone.getScanPathExtents().getMaxX() + 1,
                                  originalMainZone.getScanPathExtents().getMaxX(),
                                  yTravelStart,
                                  yTravelEnd);
        if (rightMainZone.getScanPath().getScanPasses().size() > 0)
        {
          imageZonesAtMainStepSize.add(rightMainZone);
        }
      }
      previousRescanZone = extendedRescanZone;
    }

    mergedMainImageGroup.getImageZones().addAll(imageZonesAtMainStepSize);

    // Copy scan passes from individual image zones within the merged image group to the scan path for the
    // merged main image group.
    updateScanPathForImageGroup(mergedMainImageGroup);
    return (mergedMainImageGroup);
  }

  /**
   * Copies scan passes from the sourceImageZone to the destImageZone which have x coordinates between
   * minXTravel and maxXTravel.  The scan passes will be extended in length if necessary to cover the
   * distance between minYTravel and maxYTravel.
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */

  protected void copyScanPassesToImageZone2(ImageZone mergedImageZone, ImageZone toBeMergeImageZone,
                                           SortedSet<ImageZone> mainMergeImageZones)
  {
    int scanPassID = 0;
    List<ScanPass> mergedScanPasses = new ArrayList<ScanPass>();

    int minX = mainMergeImageZones.first().getScanPathExtents().getMinX();
    int maxX = mainMergeImageZones.last().getScanPathExtents().getMaxX();
    
    for (ScanPass srcScanPass : toBeMergeImageZone.getScanPath().getScanPasses())
    {
      StagePosition start = srcScanPass.getStartPointInNanoMeters();
      StagePosition end = srcScanPass.getEndPointInNanoMeters();
      int scanPassX = start.getXInNanometers();
      
      if (scanPassX >= minX && scanPassX <= maxX)
      {
        boolean found = false;
        
        int minYTravel = 0;
        int maxYTravel = 0;
        for (ImageZone imageZone : mainMergeImageZones)
        {
          for(ScanPass scanpass:imageZone.getScanPath().getScanPasses())
          {
            if(scanpass.getStartPointInNanoMeters().getXInNanometers() == scanPassX)
            {
              minYTravel = scanpass.getStartPointInNanoMeters().getYInNanometers();
              maxYTravel = scanpass.getEndPointInNanoMeters().getYInNanometers();
              found = true;
            }
          }
        }
        
        if (found==true)
        {
          boolean update = false;

          if (start.getYInNanometers() > minYTravel)
          {
            start.setYInNanometers(minYTravel);
            update = true;
          }
          if (end.getYInNanometers() < maxYTravel)
          {
            end.setYInNanometers(maxYTravel);
            update = true;
          }

          if(update == true)
          {
            mergedScanPasses.add(new ScanPass(scanPassID++, start, end));
          }
        }
      }
      else
      {
        mergedScanPasses.add(new ScanPass(scanPassID++, start, end));
      }
    }
    
    ScanPath scanPath = new ScanPath(toBeMergeImageZone.getScanPath().getMechanicalConversions(), 
                                     toBeMergeImageZone.getScanPath().getScanPasses());
    
    if (mergedScanPasses != null)
    {
      scanPath = new ScanPath(toBeMergeImageZone.getScanPath().getMechanicalConversions(), mergedScanPasses);
    }
    
    mergedImageZone.setScanPath(scanPath);
  }
  
  /**
   * Copies scan passes from the sourceImageZone to the destImageZone which have x coordinates between
   * minXTravel and maxXTravel.  The scan passes will be extended in length if necessary to cover the
   * distance between minYTravel and maxYTravel.
   * @author Kay Lannen
   */

  protected void copyScanPassesToImageZone(ImageZone destImageZone, ImageZone sourceImageZone,
                                           int minXTravel, int maxXTravel,
                                           int minYTravel, int maxYTravel)
  {
    int scanPassID = 0;
    List<ScanPass> destScanPasses = new ArrayList<ScanPass>();

    for (ScanPass srcScanPass : sourceImageZone.getScanPath().getScanPasses())
    {
      int scanPassX = srcScanPass.getStartPointInNanoMeters().getXInNanometers();
      if ((scanPassX >= minXTravel) && (scanPassX <= maxXTravel))
      {
        StagePosition start = srcScanPass.getStartPointInNanoMeters();
        StagePosition end = srcScanPass.getEndPointInNanoMeters();
        if (start.getYInNanometers() > minYTravel)
        {
          start.setYInNanometers(minYTravel);
        }
        if (end.getYInNanometers() < maxYTravel)
        {
          end.setYInNanometers(maxYTravel);
        }
        destScanPasses.add(new ScanPass(scanPassID++, start, end));
      }
    }
    ScanPath scanPath = new ScanPath(sourceImageZone.getScanPath().getMechanicalConversions(), destScanPasses);
    destImageZone.setScanPath(scanPath);
  }

  /**
   * Copies mean step size, dither Amplitude, and confirmedCandidateStepRanges from the
   * sourceImageZone to the destImageZone.
   * @author Kay Lannen
   */
  protected void copyStepSizeSettingsToImageZone(ImageZone destZone, ImageZone sourceZone)
  {
    destZone.setMeanStepSizeInNanoMeters(sourceZone.getMeanStepSizeInNanoMeters());
    destZone.setDitherAmplitude(sourceZone.getDitherAmplitude());
    destZone.setConfirmedCandidateStepRanges(sourceZone.getConfirmedCandidateStepRanges());
  }

  /**
   * Updates the scan path in a heterogeneous image group to contain the superset of the scan
   * paths in all of its image zones.
   * @author Kay Lannen
   */
  protected void updateScanPathForImageGroup(HeterogeneousImageGroup imageGroup)
  {
    int scanPassID = 0;
    List<ScanPass> combinedScanPasses = new ArrayList<ScanPass>();

    Iterator<ImageZone> imageZoneIter = imageGroup.getImageZones().iterator();
    while (imageZoneIter.hasNext())
    {
      ImageZone imageZone = imageZoneIter.next();

      for (ScanPass scanPass : imageZone.getScanPath().getScanPasses())
      {
        combinedScanPasses.add(new ScanPass(scanPassID++, scanPass.getStartPointInNanoMeters(),
                                            scanPass.getEndPointInNanoMeters()));
      }
    }
    imageGroup.getScanPath().setScanPasses(combinedScanPasses);
  }
}
