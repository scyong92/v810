package com.axi.v810.business.imageAcquisition;

import java.awt.geom.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.business.imageAnalysis.ZHeightEstimator;

/**
 * @author Roy Williams
 * @author Poh Kheng
 *
 * The format for the input file will be as example below
 * testProject - this project must exist in testRel/unitTest/projects
 * testGlobalSurfaceModel - (true/false) this will enable Global Surface Model for the project
 * turnOnShadingCompensationForAll - (true/false) this will enable Shading Compesation for the project
 * systemTypeEnum - (standard/throughput) this will be set the system type for the project
 */

public class Test_ImagingChainProgramGenerator extends UnitTest
{
  private String _projectName;
  private Project _project;
  private Config _config;
  private ImagingChainProgramGenerator _imagingChainProgramGenerator;
  private boolean _testGlobalSurfaceModel = false;
  private boolean _turnOnShadingCompensationForAll = false;
  private SystemTypeEnum _systemTypeEnum;
  boolean _printDetailReport = false;

  /**
   * @author Roy Williams
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    try
    {
      // All projects must be physically under testRel/unitTest/projects.

      // Unit tests will read the project name from the input file.  If you want
      // to manually run a project, set the name in the else clause below.
      String testProject = null;
      if (in.ready())
        testProject = in.readLine();
      else
// This is for normal mag usage. Low mag will different.
//        testProject = "73-9622-52_A0_KAUAI_shaded false false";
        testProject = "DepopulatedCenterBoard false false";

      String array[] = Pattern.compile("\\s").split(testProject);
      if (array.length > 0)
        _projectName = array[0];

      // Check for shading compensation and global surface model parameters
      if (array.length > 1 && array[1].equals("true"))
      {
        _testGlobalSurfaceModel = true;
        System.out.println("Deferring Pressfit and PTH reconstruction till GlobalSurfaceModel built");
      }

      if (array.length > 2 && array[2].equals("true"))
      {
        _turnOnShadingCompensationForAll = true;
        System.out.println("Turning on shading compensation for ALL pads.");
      }

      // Check for system type load the project as. eg. standard/throughput
      if (array.length > 3 && array[3] != null)
      {
        _systemTypeEnum = SystemTypeEnum.getSystemType(array[3]);
        System.out.println("Loading project as system type: "+_systemTypeEnum.getName());
      }

      ImagingChainProgramGenerator.disableStepSizeOptimization(true); // Forced to disable by default, so that existing regression test won't fail.
      if (array.length > 4 && array[4] != null)
      {
        if (array[1].equalsIgnoreCase("false"))
        {
          ImagingChainProgramGenerator.disableStepSizeOptimization(false);
          System.out.println("Turning on step size optimization based on panel thickness");
        }
      }

      System.out.println(_projectName);
      System.out.println();

      // Turn on some debugging flags
      ImagingChainProgramGenerator.setDebug(true);
      RescanShadedRegionsBasedUponDripSourceComponent.setDebug(true);
      ScanStrategy.setDebug(true);

      // delete the .project file so when we run this again, it regenerates a program
      FileUtilAxi.delete(FileName.getProjectSerializedFullPath(_projectName));
      BooleanRef abortedDuringLoad = new BooleanRef();
      _project = Project.load(_projectName, abortedDuringLoad);
      Assert.expect(abortedDuringLoad.getValue() == false);
      Panel panel = _project.getPanel();
      ZHeightEstimator.getInstance().resetForNewInspection(panel.getThicknessInNanometers(), _project.getName());

      // set all pad types to be shading compensated if possible.
      if (_turnOnShadingCompensationForAll)
      {
        for (PadType padType : _project.getPanel().getPadTypesUnsorted())
        {
          PadTypeSettings settings = padType.getPadTypeSettings();
          if (settings.getEffectiveArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false)
            settings.setArtifactCompensationState(ArtifactCompensationStateEnum.COMPENSATED);
        }
      }

      // to set the project system type
      _project.setSystemType(_systemTypeEnum);

      TestProgram testProgram = _project.getTestProgram();
      testProgram.setUseOnlyInspectedRegionsInScanPath(true);

      for (TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
      {
        PanelSettings panelSettings = _project.getPanel().getPanelSettings();
        try
        {
          panelSettings.getRightManualAlignmentTransform();
        }
        catch (AssertException ex)
        {
          AffineTransform transform = testSubProgram.getDefaultAggregateAlignmentTransform();
          panelSettings.setRightManualAlignmentTransform(transform);
        }
      }

      // Get the scan step sizes.
      _config = Config.getInstance();
      try
      {
        _config.loadIfNecessary();
      }
      catch (DatastoreException de)
      {
        de.printStackTrace();
        Assert.expect(false);
      }

      // Initialize the xRayCameraArray.
      XrayCameraArray xrayCameraArray = XrayCameraArray.getInstance();
      xrayCameraArray.refresh();
      Assert.expect(_project != null);

      testShadingCompensationProgramGen();
      testGlobalSurfaceModelBreakPointReprogrammingOfFocusInstructions();
      checkThatAllReconstructionRegionsHaveMinimumScanPasses(testProgram, 4);
    }
    catch (IOException ex)
    {
      Assert.logException(ex);
    }
    catch (XrayTesterException ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * @author Roy Williams
   */
  public void testShadingCompensationProgramGen() throws DatastoreException
  {
    if (_testGlobalSurfaceModel)
    {
      _imagingChainProgramGenerator = new ImagingChainProgramGenerator(
          ImageAcquisitionModeEnum.PRODUCTION,
          ImagingChainProgramGenerator.getDefaultScanStrategy(),
          0);

      // for this unit test (GSM) we must populate the LSM with a little data
      // so some neighbors will be found.  The elevations for these points were
      // determined by hooking up to a running version of this program on the
      // actual tester.   Look at the "true, 4" in the adjusted command (like below)
      // to identify components that are using the neighbor information.
      // SurfaceModelBreakPointAdjustment, false, 389, PTH, 4022, 10, true, 4, 145, 0, -92, 0
      ZHeightEstimator zHeightEstimator = ZHeightEstimator.getInstance();
      zHeightEstimator.addDataPointForTesting(false, new Point2D.Double( -7704410.0, 4.6585905E7), new Integer(1334450));
      zHeightEstimator.addDataPointForTesting(false, new Point2D.Double( -1.151546E7, 4.6588238E7), new Integer(1347840));
      zHeightEstimator.addDataPointForTesting(false, new Point2D.Double( -2619513.0, 5.2299758E7), new Integer(1477945));
    }
    else
    {
      _imagingChainProgramGenerator = new ImagingChainProgramGenerator(
          ImageAcquisitionModeEnum.IMAGE_SET,
          ImagingChainProgramGenerator.getDefaultScanStrategy(),
          0);
    }

    try
    {
      TestProgram testProgram = _project.getTestProgram();
      _imagingChainProgramGenerator.generateProgramsForLowerImagingChainHardware(
          testProgram,
          new TestExecutionTimer(),
          XrayTester.getMaximumSliceHeightFromNominalSliceInNanometers(),
          Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers()));

      List<ImageGroup> imageGroups = _imagingChainProgramGenerator.getImageGroups();
      ImageGroup mainAreaImageGroup = imageGroups.get(0);
      List<ImageGroup> signalCompensatedGroups = new ArrayList<ImageGroup>(imageGroups);
      signalCompensatedGroups.remove(mainAreaImageGroup);

      // Print out the estimated test time for all.

      if (signalCompensatedGroups == null)
        signalCompensatedGroups = new ArrayList<ImageGroup>();
      printStats(mainAreaImageGroup, signalCompensatedGroups);

      // Print the scan path.
      List<ScanPass> scanPasses = _project.getTestProgram().getScanPasses();
      System.out.println();
      System.out.println("Scan Passes");
      for (ScanPass sp : scanPasses)
      {
        System.out.println(sp.getId() + ", " + sp.getStartPointInNanoMeters().getXInNanometers() + ", " + sp.getStartPointInNanoMeters().getYInNanometers());
        System.out.println(sp.getId() + ", " + sp.getEndPointInNanoMeters().getXInNanometers() + ", " + sp.getEndPointInNanoMeters().getYInNanometers());
      }

      // Print out a detailed report so autoFocus can be validated.
      if (_printDetailReport)
      {
        // details for imageGroups
        System.out.println();
        printComponentData("Main", mainAreaImageGroup);
        int groupNum = 0;
        for (ImageGroup group : signalCompensatedGroups)
        {
          printComponentData(Integer.toString(groupNum), group);
          groupNum++;
        }
      }
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();
      Assert.expect(false);
    }
  }

  /**
   * @author Roy Williams
   */
  private void testGlobalSurfaceModelBreakPointReprogrammingOfFocusInstructions() throws XrayTesterException
  {
    if (_imagingChainProgramGenerator.usingGlobalSurfaceModel() == false)
      return;

    TestProgram testProgram = _project.getTestProgram();
    ScanPassBreakPoint breakPoint = null;
    for (TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
    {
      for (ScanPass scanPass : testSubProgram.getScanPasses())
      {
        if (scanPass.hasBreakPoints())
        {
          for(ScanPassBreakPoint breakpoint : scanPass.getBreakPoints())
          {
            if (breakpoint instanceof SurfaceModelBreakPoint)
            {
              breakPoint = breakpoint;
              break;
            }
          }
        }
      }
      breakPoint.executeAction();
    }
  }

  /**
   * @author Roy Williams
   */
  private void printStats(ImageGroup mainAreaImageGroup, List<ImageGroup> signalCompensatedGroups)
  {
    Assert.expect(mainAreaImageGroup != null);
    Assert.expect(signalCompensatedGroups != null);
    System.out.println();

    // Print the test time for each individual ImageGroup.
    double totalTime = mainAreaImageGroup.estimateExecutionTimeForScanPassesInSecondsUsingScanPath();
    System.out.println("Main Group Test Time (scan path estimate) = " + totalTime);
    if (mainAreaImageGroup instanceof HomogeneousImageGroup)
    {
      double altEstimate = ((HomogeneousImageGroup)mainAreaImageGroup).estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSizeMethodA();
      System.out.println("Group Test Time (mean step size method A estimate)= " + altEstimate);
    }

    for (ImageGroup group : signalCompensatedGroups)
    {
      double groupTime = group.estimateExecutionTimeForScanPassesInSecondsUsingScanPath();
      totalTime = totalTime + groupTime;
      System.out.println("Group Test Time (scan path estimate) = " + groupTime);
      if (group instanceof HomogeneousImageGroup)
      {
        double altEstimate = ((HomogeneousImageGroup)group).estimateExecutionTimeForScanPassesInSecondsUsingMeanStepSizeMethodA();
        System.out.println("Group Test Time (mean step size method A estimate)= " + altEstimate);
      }
    }
    System.out.println("Estimated total execution time (scan path estimate): " + totalTime / 60 + " minutes");
    System.out.println();

    // Main ImageGroup stats.
    System.out.println("ImageGroup, #ReconstructionRegions, ImageZone, StepMin(mils), StepMax(mils)");
    {
      System.out.print("Main, ");
      System.out.print(mainAreaImageGroup.getRegionPairsSortedInExecutionOrder().size() + ", ");
      SortedSet<ImageZone> imageZones = mainAreaImageGroup.getImageZones();
      int zoneIndex = 0;
      for (ImageZone imageZone : imageZones)
      {
        System.out.print("ZoneM-" + zoneIndex + ", ");
        zoneIndex++;
        List<Pair<Integer, Integer>> candidateSteps = imageZone.getConfirmedCandidateStepRanges();
        for (Pair<Integer, Integer> candidateStep : candidateSteps)
        {
          System.out.print(candidateStep.getFirst() / 25400 + ", " + candidateStep.getSecond() / 25400 + ", ");
        }
      }
      System.out.println();
    }

    // stats for signal compensated groups.
    int groupNum = 0; // ImageGroups do not have a name/number we artificially give one.
    for (ImageGroup group : signalCompensatedGroups)
    {
      System.out.print(groupNum++ +", ");
      System.out.print(group.getRegionPairsSortedInExecutionOrder().size() + ", ");
      SortedSet<ImageZone> imageZones = group.getImageZones();
      int zoneIndex = 0; // ImageZones don't have names either so we make one.
      for (ImageZone imageZone : imageZones)
      {
        System.out.print("Zone" + groupNum + "-" + zoneIndex + ", ");
        zoneIndex++;
        List<Pair<Integer, Integer>> candidateSteps = imageZone.getConfirmedCandidateStepRanges();
        for (Pair<Integer, Integer> candidateStep : candidateSteps)
        {
          System.out.print(candidateStep.getFirst() / 25400 + ", " + candidateStep.getSecond() / 25400 + ", ");
        }
      }

      System.out.println();
    }
  }

  /**
   * Output to give to Tracy for validation.
   *
   * format:
   *
   * GroupName
   * MeanStepSizeTakenByGroup
   * BoardNumber
   * RefDesignatorOfComponent
   * RR_id
   * BoardSide
   * CandidateStepRange
   *
   * @author Roy Williams
   */
  private static void printComponentData(String groupName, ImageGroup imageGroup)
  {

    if (UnitTest.unitTesting() == false)
      return;

    System.out.println("GroupName MeanStepSizeTakenByGroup BoardNumber RefDesignatorOfComponent RR_id BoardSide x y w h CandidateStepRange");
    List<Pair<ReconstructionRegion, SystemFiducialRectangle>> regionPairs = imageGroup.getRegionPairsSortedInExecutionOrder();
    for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : regionPairs)
    {
      System.out.print(groupName + "   ");
      SortedSet<ImageZone> imageZones = imageGroup.getImageZones();
      if (imageZones.size() > 1)
        System.out.print("MergedStepSizes   ");
      else
        System.out.print(imageZones.first().getMeanStepSizeInNanoMeters() + "    ");
      ReconstructionRegion region = regionPair.getFirst();
      Component component = region.getComponent();
      System.out.print(component.getBoard().getName() + "   ");
      System.out.print(component.getReferenceDesignator() + "   ");
      System.out.print(region.getRegionId() + "   ");
      System.out.print((component.getSideBoard().isTopSide()) ? "TOP" : "BOTTOM");
      System.out.print("   ");

      System.out.print(region.getRegionRectangleRelativeToPanelInNanoMeters() + "   ");
      List<Pair<Integer, Integer>> candidateStepSizeRanges = region.getCandidateStepRanges().getRanges();
      //if (candidateStepSizeRanges.size() == 0)
      //  candidateStepSizeRanges = imageGroup.confirmedCandidateStepRanges();
      int currentRange = 1;

      for (Pair<Integer, Integer> candidateStepRange : candidateStepSizeRanges)
      {
        System.out.print(candidateStepRange.getFirst() + "-" + candidateStepRange.getSecond());
        if (currentRange < candidateStepSizeRanges.size())
          System.out.print(",");
      }
      System.out.println();
    }
  }

  /**
   * @author Roy Williams
   */
  private void complainAboutInsufficientScanPasses(
      ReconstructionRegion region,
      int minScanPasses)
  {
    System.out.println("Region " +
                       region.getRegionId() +
                       " is not imaged on at least " +
                       minScanPasses + " scan passes");
  }

  /**
   * @author Roy Williams
   */
  private void checkThatAllReconstructionRegionsHaveMinimumScanPasses(
      TestProgram testProgram,
      int minScanPasses)
  {
    Assert.expect(testProgram != null);

    boolean insufficientScanPasses = false;
    for (TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
    {
      for (ReconstructionRegion region : testSubProgram.getAlignmentRegions())
      {
        if (region.getScanPasses().size() < minScanPasses)
        {
          insufficientScanPasses = true;
          complainAboutInsufficientScanPasses(region, minScanPasses);
        }
      }
    }

    for (TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
    {
      for (ReconstructionRegion region : testSubProgram.getAlignmentRegions())
      {
        if (region.getScanPasses().size() < minScanPasses)
        {
          insufficientScanPasses = true;
          complainAboutInsufficientScanPasses(region, minScanPasses);
        }
      }
      for (ReconstructionRegion region : testSubProgram.getAllScanPathReconstructionRegions())
      {
        if (region.getScanPasses().size() < minScanPasses)
        {
          insufficientScanPasses = true;
          complainAboutInsufficientScanPasses(region, minScanPasses);
        }
      }
    }
    Assert.expect(insufficientScanPasses == false,
                  "Insufficient Scan Passes on reconstruction regions");
  }

  /**
   * @author Roy Williams
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_ImagingChainProgramGenerator() );
  }
}
