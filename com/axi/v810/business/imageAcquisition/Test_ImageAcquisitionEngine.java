package com.axi.v810.business.imageAcquisition;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Test class for the ImageAcquisitionEngine.
 *
 * @author Matt Wharton
 */
public class Test_ImageAcquisitionEngine extends UnitTest
{
  private HardwareWorkerThread   _hardwareWorkerThread   = null;
  private XrayTester             _xrayTester             = null;
  private ImageAcquisitionEngine _imageAcquisitionEngine = null;

  /**
   * @author Matt Wharton
   */
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_ImageAcquisitionEngine() );
  }

  /**
   * @author Matt Wharton
   */
  protected Test_ImageAcquisitionEngine()
  {
    try
    {
      Config.getInstance().loadIfNecessary();
    }
    catch (DatastoreException de)
    {
      de.printStackTrace();
      Assert.expect(false);
    }
    // Start the hardware running if (and only if) we are not in simulation mode.
    _hardwareWorkerThread = HardwareWorkerThread.getInstance();
    _xrayTester = XrayTester.getInstance();
    try
    {
      _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
      {
        public void run() throws Exception
        {
          _xrayTester.startup();
        }
      });
    }
    catch (XrayTesterException e)
    {
      Expect.expect(false);
    }
  }

  /**
   * @author Roy Williams
   */
  public void test( BufferedReader in, PrintWriter out )
  {
    try
    {
      if (_imageAcquisitionEngine == null)
        _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();

      List<LocalizedString> warnings = new ArrayList<LocalizedString>();
      Project project = Project.importProjectFromNdfs("0027parts", warnings);
      TestProgram program = project.getTestProgram();

      _imageAcquisitionEngine.initialize();

      // testMarkReconstructionRegionsCompleteThread
      testMarkReconstructionRegionsComplete(program);

      // test AbortOnNonHardwareThread
      testAbortOnNonHardwareThread();

      TestExecutionTimer testExecutionTimer = new TestExecutionTimer();
      _imageAcquisitionEngine.acquireProductionImages(program, false, testExecutionTimer);
    }
    catch (ImageAcquisitionEngineAbortException ex)
    {
      // expecting one of these.
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }


  /**
   * @author Roy Williams
   */
  private void testAbortOnNonHardwareThread()
  {
    try
    {

      AbortOnNonHardwareThread abortOnNonHardwareThread =
        new AbortOnNonHardwareThread(_imageAcquisitionEngine);
      abortOnNonHardwareThread.start();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Roy Williams
   */
  private void testMarkReconstructionRegionsComplete(TestProgram program)
  {
    try
    {
      MarkReconstructionRegionsCompleteThread markReconstructionRegionsCompleteThread =
        new MarkReconstructionRegionsCompleteThread(_imageAcquisitionEngine, program);
      markReconstructionRegionsCompleteThread.start();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Roy Williams
   */
  class MarkReconstructionRegionsCompleteThread extends Thread
  {
    private ImageAcquisitionEngine _imageAcquisitionEngine;
    private TestProgram _testProgram;

    /**
     * @author Roy Williams
     */
    MarkReconstructionRegionsCompleteThread(ImageAcquisitionEngine imageAcquisitionEngine, TestProgram testProgram)
    {
      Assert.expect(imageAcquisitionEngine != null);
      Assert.expect(testProgram != null);

      _imageAcquisitionEngine = imageAcquisitionEngine;
      _testProgram  = testProgram;
    }

    /**
     * @author Roy Williams
     */
    public void run()
    {
      try
      {
        Thread.sleep(5000);
      }
      catch (InterruptedException e)
      {
        // do nothing;
      }
      for (TestSubProgram testSubProgram : _testProgram.getFilteredTestSubPrograms())
      {
        for (ReconstructionRegion reconstructionRegion : testSubProgram.getAllInspectionRegions())
        {
          try
          {
            _imageAcquisitionEngine.reconstructionRegionComplete(reconstructionRegion, false);
          }
          catch (XrayTesterException e)
          {
            e.printStackTrace();
          }
        }
      }
    }
  }

  /**
   * @author Roy Williams
   */
  class AbortOnNonHardwareThread extends Thread
  {
    private ImageAcquisitionEngine _imageAcquisitionEngine;

    /**
     * @author Roy Williams
     */
    AbortOnNonHardwareThread(ImageAcquisitionEngine imageAcquisitionEngine)
    {
      Assert.expect(imageAcquisitionEngine != null);

      _imageAcquisitionEngine = imageAcquisitionEngine;
    }

    /**
     * @author Roy Williams
     */
    public void run()
    {
      try
      {
        Thread.sleep(10000);
      }
      catch (InterruptedException e)
      {
        // do nothing;
      }

      try
      {
        _imageAcquisitionEngine.abort();
      }
      catch(XrayTesterException xex)
      {
        Assert.logException(xex);
      }
   }
  }
}
