package com.axi.v810.business.imageAcquisition;


/**
 * Scan Strategy reflects a decision on which Rescan, Integrated, Groupings, etc
 * will be used to
 *
 * @author Roy Williams
 */
public class ScanStrategyEnum extends com.axi.util.Enum
{
  private boolean _rescanStrategy = false;

  private static int _index = 0;
  public static final ScanStrategyEnum NO_SHADING_COMPENSATION = new ScanStrategyEnum(_index++, true);
  public static final ScanStrategyEnum GROUP_ON_MAX_CANDIDATE_STEP_SIZE = new ScanStrategyEnum(_index++, true);
  public static final ScanStrategyEnum GROUP_ON_COMPONENTS_WITH_AREA_COMPRESSION = new ScanStrategyEnum(_index++, true);
  public static final ScanStrategyEnum GROUP_ON_COMPONENTS_WITH_AREA_AND_TIMING_COMPRESSION = new ScanStrategyEnum(_index++, true);
  public static final ScanStrategyEnum MERGE_TO_MAIN = new ScanStrategyEnum(_index++, false);
  public static final ScanStrategyEnum DEPRECATED = new ScanStrategyEnum(_index++, true);

  /**
   * @author Roy Williams
   */
  private ScanStrategyEnum(int id, boolean rescanStrategy)
  {
    super(id);

    _rescanStrategy = rescanStrategy;
  }

  public boolean isRescanStrategy()
  {
    return _rescanStrategy;
  }
}
