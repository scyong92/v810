package com.axi.v810.business.imageAcquisition;

import java.io.*;
import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.testProgram.*;
import java.util.*;
/**
 * Contains information about a single reconstructed image.
 * Its slice height, name, etc.
 * Used as a record in the ReconstructedImages object.
 * Add _arraySharpness and _arrayZHeight by BH
 * @author Patrick Lacz
 * @author Lim Boon Hong
 */
public class ReconstructedSlice
{
  // The image that is received from Reconstruction.
  private Image _image;

  // The inspection region may require a image processing for analysis.
  // This is that enhanced image.
  //LNLim: this image including rotation for inspection use.
  private Image _enhancedImage;
  
  // The inspection region may require a rotation to property orient the joints for analysis.
  // This is that rotated image.
  private Image _orthogonalImage;
  
  //Lim, Lay Ngor
  // The inspection region require a image processing for analysis.
  // This is the solely enhanced image without rotation for VVTS result use.
  // Draw back: this will increase our ReconstructionSlice object memory!
  private Image _enhancedImageWithoutRotation;  

  // The 'name' of the slice. Eg. PAD, PACKAGE, THROUGHHOLE_50.
  private SliceNameEnum _nameEnum;

  // The height of the slice in nanometers.
  private int _sliceHeightInNanometers;
  // Sharpness profile size
  private int _profileSize=0;
  // array of ZHeight. Add by BH
  private List<Float> _arrayZHeight;
  // array of sharpness. Add by BH
  private List<Float> _arraySharpness;

  private final String _ZHEIGHT_PROPERTY_KEY  = "zHeightInNanos";
  private final String _PROFILE_PROPERTY_KEY  = "profileSize";
  private final String _ZHEIGHT_PROFILE_KEY   = "zHeightInNanosProfile";
  private final String _SHARPNESS_PROFILE_KEY = "sharpnessProfile";
  
  /**
   * Populate the data structure with the required information.
   * The corrected image is not set up here; it is assigned with setCorrectedImage.
   * Add arrayZHeight and arraySharpness
   * @author Patrick Lacz
   * @author Lim Boon Hong
   */
  public ReconstructedSlice(Image image, SliceNameEnum sliceNameEnum, int sliceHeightInNanometers, List<Float> arrayZHeight, List<Float> arraySharpness)
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(arrayZHeight.size() == arraySharpness.size()); // Chnee Khang Wah, 2013-02-20

    _image = image;
    _image.incrementReferenceCount();
    _nameEnum = sliceNameEnum;
    _sliceHeightInNanometers = sliceHeightInNanometers;
    _orthogonalImage = null;
    _arrayZHeight = arrayZHeight;//add by Boon Hong
    _arraySharpness = arraySharpness;//add by Boon Hong
    _enhancedImage = null;
    _enhancedImageWithoutRotation = null;

    if (_image.hasImageDescription() == false)
      _image.setImageDescription(new ImageDescription(""));
    _image.getImageDescription().setParameter(_ZHEIGHT_PROPERTY_KEY, _sliceHeightInNanometers);
    
    // Chnee Khang Wah, 2013-02-19, Adding sharpness profile...
    _profileSize = _arrayZHeight.size();
    if (_profileSize>0)
    {
      _image.getImageDescription().setParameter(_PROFILE_PROPERTY_KEY, _profileSize);
      _image.getImageDescription().setParameter(_ZHEIGHT_PROFILE_KEY, _arrayZHeight);
      _image.getImageDescription().setParameter(_SHARPNESS_PROFILE_KEY, _arraySharpness);
    }
  }

  /**
   * Populate the data structure with the required information from an image loaded from disk.
   * The corrected image is not set up here; it is assigned with setCorrectedImage.
   * @author Patrick Lacz
   */
  public ReconstructedSlice(Image image, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);

    _image = image;
    _image.incrementReferenceCount();
    _nameEnum = sliceNameEnum;
    _sliceHeightInNanometers = 0;
    _orthogonalImage = null;
    _arrayZHeight = new ArrayList<Float>();
    _arraySharpness = new ArrayList<Float>();
    _enhancedImage = null;
    _enhancedImageWithoutRotation = null;

    if (_image.hasImageDescription())
    {
      ImageDescription imageDescription = _image.getImageDescription();
      if (imageDescription.hasParameter(_ZHEIGHT_PROPERTY_KEY))
      {
        Object zHeightInNanosObject = imageDescription.getParameter(_ZHEIGHT_PROPERTY_KEY);
        if (zHeightInNanosObject instanceof Number)
        {
          _sliceHeightInNanometers = ((Number)zHeightInNanosObject).intValue();
        }
      }
      
      // Chnee Khang Wah, 2013-02-19
      if (imageDescription.hasParameter(_PROFILE_PROPERTY_KEY))
      {
        Object profileSizeObject = imageDescription.getParameter(_PROFILE_PROPERTY_KEY);
        if (profileSizeObject instanceof Number)
        {
          _profileSize = ((Number)profileSizeObject).intValue();
        }
      }
      
      // Chnee Khang Wah, 2013-02-19
      if (_profileSize>0 && imageDescription.hasParameter(_ZHEIGHT_PROFILE_KEY))
      {
        Object zHeightProfileObject = imageDescription.getParameter(_ZHEIGHT_PROFILE_KEY);
        String[] zHeight = zHeightProfileObject.toString().split(",");
        
        for (String str: zHeight)
        {
          _arrayZHeight.add(Float.parseFloat(str));
        }
        Assert.expect(_profileSize == _arrayZHeight.size());
      }
      
      // Chnee Khang Wah, 2013-02-19
      if (_profileSize>0 && imageDescription.hasParameter(_SHARPNESS_PROFILE_KEY))
      {
        Object zHeightProfileObject = imageDescription.getParameter(_SHARPNESS_PROFILE_KEY);
        String[] zHeight = zHeightProfileObject.toString().split(",");
        
        for (String str: zHeight)
        {
          _arraySharpness.add(Float.parseFloat(str));
        }
        Assert.expect(_profileSize == _arraySharpness.size());
      }
    }
  }

  /**
   * Return an image for analysis.
   * If a corrected image is available, that image will be returned instead of the raw image.
   * @author Patrick Lacz
   */
  public Image getOrthogonalImage()
  {    
    if(_enhancedImage != null)
      return _enhancedImage;
    
    if (_orthogonalImage != null)
      return _orthogonalImage;
    
    Assert.expect(_image != null);
    return _image;
  }
  
  /**
   * Return an original image for analysis (special for short).
   * If a corrected image is available, that image will be returned instead of the raw image.
   * @author Wei Chin
   */
  public Image getOrthogonalImageWithoutEnhanced()
  {
    if (_orthogonalImage != null)
      return _orthogonalImage;    
    
    Assert.expect(_image != null);
    return _image;
  }

  /**
   * @author Patrick Lacz
   */
  public Image getImage()
  {
    Assert.expect(_image != null);
    return _image;
  }

  /**
   * @author Patrick Lacz
   */
  public boolean hasModifiedImageForInspection()
  {
    return (_orthogonalImage != null);
  }
  
  /**
   * @author Wei Chin
   */
  public boolean hasEnhancedImageForInspection()
  {
    return (_enhancedImage != null);
  }

  /**
   * Set the corrected image.
   * Use clearCorrectedImage to set this value to null. (Do not pass null to this method.)
   * @author Patrick Lacz
   */
  public void setOrthogonalImage(Image rotatedImage)
  {
    Assert.expect(rotatedImage != null); // use clearRotatedImage
    _orthogonalImage = rotatedImage;
    _orthogonalImage.incrementReferenceCount();
  }
  
  /**
   * @author Patrick Lacz
   */
  public void clearOrthogonalImage()
  {
    if (_orthogonalImage != null)
    {
      _orthogonalImage.decrementReferenceCount();
      _orthogonalImage = null;
    }
  }

  /**
   * Set the enhanced image.
   * Use clearCorrectedImage to set this value to null. (Do not pass null to this method.)
   * @author Wei Chin
   */
  public void setEnhancedImage(Image enhancedImage)
  {
    Assert.expect(enhancedImage != null); // use clearEnhancedImage
    _enhancedImage = enhancedImage;
    _enhancedImage.incrementReferenceCount();
  }
  
  /**
   * @author Wei Chin
   */
  public void clearEnhancedImage()
  {
    if (_enhancedImage != null)
    {
      _enhancedImage.decrementReferenceCount();
      _enhancedImage = null;
    }
  }
  
  /**
   * Set the enhanced image.
   * Use clearCorrectedImage to set this value to null. (Do not pass null to this method.)
   * @author Lim, Lay Ngor
   */
  public void setEnhancedImageWithoutRotation(Image enhancedImageWithoutRotation)
  {
    Assert.expect(enhancedImageWithoutRotation != null); // use enhancedImageWithoutRotation
    _enhancedImageWithoutRotation = enhancedImageWithoutRotation;
    _enhancedImageWithoutRotation.incrementReferenceCount();
  }
  
  /**
   * @author Lim, Lay Ngor
   */
  public void clearEnhancedImageWithoutRotation()
  {
    if (_enhancedImageWithoutRotation != null)
    {
      _enhancedImageWithoutRotation.decrementReferenceCount();
      _enhancedImageWithoutRotation = null;
    }
  }
  
  /**
   * @author Lim, Lay Ngor
   */
  public boolean hasEnhancedImageWithoutRotationForVVTS()
  {
    return (_enhancedImageWithoutRotation != null);
  }
 
  /**
   * Return an enhanced image without rotation for VVTS result display.
   * Raw image will be return if the correctedImage is not available.
   * @author Lim, Lay Ngor
   */
  public Image getEnhancedImageWithoutRotation()
  {
    if (_enhancedImageWithoutRotation != null)
      return _enhancedImageWithoutRotation;    
    
    Assert.expect(_image != null);
    return _image;
  }
  
  /**
   * @author Patrick Lacz
   */
  public SliceNameEnum getSliceNameEnum()
  {
    return _nameEnum;
  }

  /**
   * @author Patrick Lacz
   */
  public void incrementReferenceCount()
  {
    if (_image != null)
      _image.incrementReferenceCount();
    if(_enhancedImage != null)
      _enhancedImage.incrementReferenceCount();    
    if (_orthogonalImage != null)
      _orthogonalImage.incrementReferenceCount();
    if(_enhancedImageWithoutRotation != null)
      _enhancedImageWithoutRotation.incrementReferenceCount();         
  }

  /**
   * @author Patrick Lacz
   */
  public void decrementReferenceCount()
  {
    if (_image != null)
      _image.decrementReferenceCount();
    if(_enhancedImage != null)
      _enhancedImage.decrementReferenceCount();
    if (_orthogonalImage != null)
      _orthogonalImage.decrementReferenceCount();
    if (_enhancedImageWithoutRotation != null)
      _enhancedImageWithoutRotation.decrementReferenceCount();    
  }

  /**
   * @author Patrick Lacz
   */
  public int getHeightInNanometers()
  {
    return _sliceHeightInNanometers;
  }
  
   /**
   * @author Lim Boon Hong
   */
  public List<Float> getZHeightArray()
  {
      return _arrayZHeight;
  }
  
   /**
   * @author Lim Boon Hong
   */
  public List<Float> getSharpnessArray()
  {
      return _arraySharpness;
  }
         
}
