package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 * This class allows Observers to be updated each time an execption occurs in
 * the ImageAcqusitionEngine.
 *
 * @author Roy Williams
 */
public class ImageAcquisitionExceptionObservable extends Observable
{
  private static ImageAcquisitionExceptionObservable _instance;


  /**
   * @author Roy Williams
   */
  private ImageAcquisitionExceptionObservable()
  {
    // do nothing
  }

  /**
   * @return an instance of this object.
   * @author Roy Williams
   */
  public static synchronized ImageAcquisitionExceptionObservable getInstance()
  {
    if (_instance == null)
      _instance = new ImageAcquisitionExceptionObservable();

    return _instance;
  }

  /**
   * Notify observers of the problem.
   *
   * @author Roy Williams
   */
  public void notifyObserversOfException(XrayTesterException xRayTesterException)
  {
    Assert.expect(xRayTesterException != null);
    setChanged();
    notifyObservers(xRayTesterException);
  }

}
