package com.axi.v810.business.imageAcquisition;

import java.io.*;
import java.net.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Monitors the image acquisition engine. If it is taking
 * too long to progress, it notifies the ImageAcquisitionEngine
 *
 * @author George A. David
 */
public class ImageAcquisitionProgressMonitor
{
  private static ImageAcquisitionProgressMonitor _instance;
  private ImageAcquisitionEngine _imageAcquisitionEngine;
  private boolean _isRunning = false;
  private TimerUtil _timer = new TimerUtil();
  private boolean _isMonitoring = false;
  private WorkerThread _workerThread = new WorkerThread("image acquisition progress monitor");
  private long _currentTimeOutMilliSeconds = -1;
  private LinkedList<String> _stateQueue = new LinkedList<String>();
  private int _imageReceived = 0;
  private boolean _isAlignnmentFailedWaitingUserInput = false;

  /**
   * @author George A. David
   */
  private ImageAcquisitionProgressMonitor()
  {
    // do nothing
    Config _config = Config.getInstance();
    _currentTimeOutMilliSeconds = _config.getLongValue(SoftwareConfigEnum.IMAGE_ACQUISITION_PROGRESS_MONITOR_TIMEOUT_SECONDS) * 1000;
  }

  /**
   * @author Roy Williams
   */
  public long getCurrentTimeOutInMilliSeconds()
  {
    return _currentTimeOutMilliSeconds;
  }

  /**
   * @author George A. David
   */
  public static synchronized ImageAcquisitionProgressMonitor getInstance()
  {
    if(_instance == null)
      _instance = new ImageAcquisitionProgressMonitor();

    return _instance;
  }

  /**
   * @author George A. David
   */
  private void checkTimer()
  {
    //Siew Yeng - XCR-2753 - no need to check timer when waiting user input
    if(_isAlignnmentFailedWaitingUserInput)
      return;
    
    long elapsedTime = _timer.getElapsedTimeInMillis();

    // if we have a number here > 48 hours there is a bug in the code!
    Assert.expect(elapsedTime < 48 * 60 * 60 * 1000);
    // let's check how we are doing
    if (elapsedTime > _currentTimeOutMilliSeconds)
    {
      System.out.println("Checking acquisition progress: " + elapsedTime + " > " + _currentTimeOutMilliSeconds);

      System.out.println("\n***** State Info *****");
      synchronized (_stateQueue)
      {
        for (String state : _stateQueue)
        {
          System.out.println(state);
        }
      }
      System.out.println("***** End State Info *****\n");

//      getIRPInfo();

      // way too long, let's notify the image acquisition engine
      if(_imageAcquisitionEngine.isAlignmentExceptionCaught() == false)
        _imageAcquisitionEngine.checkAcquisitionProgress();
//Siew Yeng - remove this because this condition is handled in XCR-2753
//      else
//        _currentTimeOutMilliSeconds *= 2.0;
    }
  }

  /**
   * @author George A. David
   */
  void resetTimeOut()
  {
    _timer.restart();
    saveState("resetTimeOut");
  }

  /**
   * @author George A. David
   */
  public void startMonitoring()
  {
    if(_imageAcquisitionEngine == null)
      _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();

    // don't monitor if we are saving projection images or using a local (offline) reconstruciton engine
    if(ImageReconstructionProcessor.isSavingImageReconstructionProjections() ||
       _imageAcquisitionEngine.isOfflineReconstructionEnabled() ||
       _imageAcquisitionEngine.isGenerateImageForExposureLearning())
      return;

    if(_isMonitoring)
      return;

    _workerThread.invokeLater(new Runnable()
    {
      public synchronized void run()
      {
        _isRunning = true;
        _isMonitoring = true;
        _timer.reset();
        _timer.start();
        try
        {
          while (_isMonitoring && areAllImagesAcquired() == false)
          {
            checkTimer();

            // wait 1 second
            try
            {
              wait(1000);
            }
            catch (InterruptedException ex)
            {
              // do nothing
            }
          }
        }
        catch (XrayTesterException xe)
        {
          // do nothing
        }
      }
    });
  }

  /**
   * @author George A. David
   */
  public synchronized void stopMonitoring()
  {
    _isMonitoring = false;
    notifyAll();
    saveState("StopMonitoring");
  }
  
  /**
   * @author George A. David
   */
  public void reconstructionIsRunning()
  {
    _isRunning = true;
    _timer.restart();
    saveState("reconstructionIsRunning");
  }

  /**
   * @author George A. David
   */
  public void reconstructionIsPaused()
  {
    _isRunning = false;
    _timer.restart();
    saveState("reconstructionIsPaused");
  }

  /**
   * @author George A. David
   */
  public void imageReceived()
  {
    _timer.restart();
    saveState("imageReceived");
    _imageReceived++;
  }

  /**
   * @author Roy Williams
   */
  public void reacquireImageRequest()
  {
    _timer.restart();
    saveState("reacquireImageRequest");
  }

  /**
   * @author George A. David
   */
  public void finishedTestingImage(int numberOfFinishedImages)
  {
    _timer.restart();
    saveState("finsihedTestingImage, total finished " + numberOfFinishedImages);
  }

  /**
   * @author George A. David
   */
  public boolean areIrpsRunning()
  {
    return _isRunning;
  }

  /**
   * @author George A. David
   */
  private boolean areAllImagesAcquired() throws XrayTesterException
  {
    return _imageAcquisitionEngine.getReconstructedImagesProducer().areAllImagesAcquired();
  }

  /**
   * @author George A. David
   */
  public void alignmentCompleted()
  {
    saveState("alignmentCompleted");
  }

  /**
   * @author George A. David
   */
  public void scanPassSentToIrps()
  {
    _timer.restart();
    saveState("scanPassSentToIrps");
  }

  /**
   * @author George A. David
   */
  void cachingImageToDisk()
  {
    _timer.restart();
    saveState("cachingImageToDisk");
  }

  /**
   * @author George A. David
   */
  void imageCachedToDisk()
  {
    _timer.restart();
    saveState("imageCachedToDisk");
  }

  /**
   * @author George A. David
   */
  void loadingImageFromDisk()
  {
    _timer.restart();
    saveState("loadingImageFromDisk");
  }

  /**
   * @author George A. David
   */
  void imageLoadedFromDisk()
  {
    _timer.restart();
    saveState("imageLoadedFromDisk");
  }

  /**
   * @author Rex Shang
   */
  void scanPathCompleted()
  {
    _timer.restart();
    saveState("allScanPassCompleted");
  }

  /**
   * @author Rex Shang
   */
  public void newWorkStarted()
  {
    saveState("newWorkStarted");
    _imageReceived = 0;
  }
  
  /**
   * @author Siew Yeng
   */
  public void alignmentFailedWaitingUserInput()
  {
    _isAlignnmentFailedWaitingUserInput = true;
    saveState("alignmnetFailedwaitingUserInput");
  }
  
  /**
   * @author Siew Yeng
   */
  public void alignmentFailedUserInputDone()
  {
    _timer.restart();
    _isAlignnmentFailedWaitingUserInput = false;
    saveState("alignmentFailedUserInputDone");
  }

  /**
   * @author Rex Shang
   */
  public void inspectionStarted()
  {
    saveState("inspectionStarted");
  }

  /**
   * @author Rex Shang
   */
  public void getRawAlignmentImageFileDir()
  {
    saveState("getRawAlignmentImageFileDir");
  }

  /**
   * @author Rex Shang
   */
  public void gotRawAlignmentImageFileDir()
  {
    saveState("gotRawAlignmentImageFileDir");
  }

  /**
   * @author Rex Shang
   */
  public void alignmentDone()
  {
    saveState("alignmentDone");
  }

  /**
   * @author Rex Shang
   */
  public void toMarkRegionComplete()
  {
    saveState("toMarkRegionComplete");
  }

  /**
   * @author Rex Shang
   */
  public void alignmentReadyToBeSent()
  {
    saveState("alignmentReadyToBeSent");
  }

  /**
   * @author Rex Shang
   */
  private void saveState(String state)
  {
    synchronized (_stateQueue)
    {
      _stateQueue.add(state);
      if (_stateQueue.size() > 100)
        _stateQueue.remove();
    }
  }

  /**
   * @author Rex Shang
   */
  private void getIRPInfo()
  {
    String[] urls =
        {
        "http://192.168.128.240:19119/qq",
        "http://192.168.128.242:19119/qq",
        "http://192.168.128.244:19119/qq",
        "http://192.168.128.246:19119/qq",
        "http://192.168.128.240:19119/dd",
        "http://192.168.128.242:19119/dd",
        "http://192.168.128.244:19119/dd",
        "http://192.168.128.246:19119/dd"
    };

    for (String url : urls)
    {
      getURLInfo(url);
    }
  }

  /**
   * @author Rex Shang
   */
  private void getURLInfo(String destination)
  {
    try
    {
      URL url = new URL(destination);
      URLConnection connection = url.openConnection();

      BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      String line;
      while ((line = in.readLine()) != null)
      {
        System.out.println(line);
      }
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

}
