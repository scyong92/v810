package com.axi.v810.business.imageAcquisition;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * This class is responsible for managing the memory used to store reconstructed images.  The system
 * will only keeping a certain amount of image data in memory at any one time.  Once this is exceeded,
 * we will need to start swapping images out to disk--this class handles the mechanics of that process.
 *
 * This class should probably be renamed something like 'ReconstructedImagesCachingManager' (PWL)
 *
 * @author Matt Wharton
 * @author Patrick Lacz
 */
public class ReconstructedImagesMemoryManager
{
  // Temporary image cache location.
  private static final String _TEMP_IMAGE_CACHE_DIR = Directory.getTempImageCacheDir();

  protected static final float _MAX_IMAGE_MEMORY_USAGE_BEFORE_CACHING_TO_DISK =
      Config.getInstance().getLongValue(SoftwareConfigEnum.IMAGE_CACHE_TO_DISK_THRESHOLD_IN_MEGABYTES);

  private static final float _THRESHOLD_HYSTERESIS_PERCENTAGE = 0.2f;

  private static ReconstructedImagesMemoryManager _instance = null;

  private ImageAcquisitionProgressMonitor _progressMonitor;
  private NativeMemoryMonitor _nativeMemoryMonitor;

  private Map<Pair<ReconstructionRegion, ImageTypeEnum>, ReconstructedImages> _reconstructedImagesInMemory =
      new LinkedHashMap<Pair<ReconstructionRegion, ImageTypeEnum>, ReconstructedImages>();

  private Map<Pair<ReconstructionRegion, ImageTypeEnum>, CachedToDiskRegionInformation> _reconstructedImagesOnDisk =
      new LinkedHashMap<Pair<ReconstructionRegion, ImageTypeEnum>, CachedToDiskRegionInformation>();

  private Set<Pair<ReconstructionRegion, ImageTypeEnum>> _reconstructionRegionsToCacheRepairImages =
      new HashSet<Pair<ReconstructionRegion,ImageTypeEnum>>();

  private Set<Pair<ReconstructionRegion, ImageTypeEnum>> _reconstructionRegionsUnderInspection =
    new HashSet<Pair<ReconstructionRegion,ImageTypeEnum>>();

  private Set<Pair<ReconstructionRegion, ImageTypeEnum>> _reconstructionRegionsFinishedInspection =
      new HashSet<Pair<ReconstructionRegion,ImageTypeEnum>>();

  private Set<String> _savedImageFileNames = new HashSet<String>();
  
  private Map<ReconstructionRegion, Long> _regionToMemoryConsumedByActiveReconstructedImageMap = new HashMap<ReconstructionRegion, Long> ();

  /**
   * @author Matt Wharton
   */
  private ReconstructedImagesMemoryManager()
  {
    _nativeMemoryMonitor = NativeMemoryMonitor.getInstance();
    _progressMonitor = ImageAcquisitionProgressMonitor.getInstance();
  }

  /**
   * @author Rex Shang
   */
  public static synchronized ReconstructedImagesMemoryManager getInstance()
  {
    if (_instance == null)
      _instance = new ReconstructedImagesMemoryManager();

    Assert.expect(_instance != null, "_instance is null");
    return _instance;
  }

  /**
   * @author Matt Wharton
   */
  public synchronized void initialize() throws DatastoreException
  {
    Assert.expect(_reconstructedImagesInMemory != null);
    Assert.expect(_reconstructedImagesOnDisk != null);

    for ( ReconstructedImages reconstructedImages : _reconstructedImagesInMemory.values())
      reconstructedImages.decrementReferenceCount();

    // Clear all maps and reset the memory used counter.
    _reconstructedImagesInMemory.clear();
    _reconstructedImagesOnDisk.clear();
    _reconstructionRegionsUnderInspection.clear();
    _reconstructionRegionsFinishedInspection.clear();
    _reconstructionRegionsToCacheRepairImages.clear();
    _regionToMemoryConsumedByActiveReconstructedImageMap.clear();

    // Make sure we have a clean image cache directory.
    if (FileUtilAxi.exists(_TEMP_IMAGE_CACHE_DIR))
    {
      FileUtilAxi.deleteDirectoryContents(_TEMP_IMAGE_CACHE_DIR);
    }
    else
    {
      FileUtilAxi.mkdirs(_TEMP_IMAGE_CACHE_DIR);
    }

    _savedImageFileNames.clear();
  }

  /**
   * @return true if the requested image is in memory or on disk, false otherwise.
   *
   * @author Matt Wharton
   */
  public synchronized boolean hasReconstructedImages(ReconstructionRegion reconstructionRegion,
                                                     ImageTypeEnum imageTypeEnum)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(imageTypeEnum != null);
    Assert.expect(_reconstructedImagesInMemory != null);
    Assert.expect(_reconstructedImagesOnDisk != null);

    Pair<ReconstructionRegion, ImageTypeEnum> regionAndImageTypePair =
        new Pair<ReconstructionRegion, ImageTypeEnum>(reconstructionRegion, imageTypeEnum);

    return (_reconstructedImagesInMemory.containsKey(regionAndImageTypePair) ||
            _reconstructedImagesOnDisk.containsKey(regionAndImageTypePair));
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized boolean areImagesForRegionCachedToDisk(ReconstructionRegion reconstructionRegion, ImageTypeEnum imageTypeEnum)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(imageTypeEnum != null);
    Assert.expect(_reconstructedImagesOnDisk != null);


    Pair<ReconstructionRegion, ImageTypeEnum> regionAndImageTypePair =
        new Pair<ReconstructionRegion, ImageTypeEnum>(reconstructionRegion, imageTypeEnum);

    return _reconstructedImagesOnDisk.containsKey(regionAndImageTypePair);
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized boolean areImagesForRegionCachedAsRepairImages(ReconstructionRegion reconstructionRegion, ImageTypeEnum imageTypeEnum)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(imageTypeEnum != null);
    Assert.expect(_reconstructedImagesOnDisk != null);

    Pair<ReconstructionRegion, ImageTypeEnum> regionAndImageTypePair =
        new Pair<ReconstructionRegion, ImageTypeEnum>(reconstructionRegion, imageTypeEnum);

    if (_reconstructedImagesOnDisk.containsKey(regionAndImageTypePair) == false)
      return false;

    CachedToDiskRegionInformation cacheInformation = _reconstructedImagesOnDisk.get(regionAndImageTypePair);

    return cacheInformation.isSavedForRepair();
  }

  /**
   * @author Rex Shang
   */
  public synchronized boolean areImagesForRegionFinishedInspection(ReconstructionRegion reconstructionRegion, ImageTypeEnum imageTypeEnum)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(imageTypeEnum != null);
    Assert.expect(_reconstructedImagesOnDisk != null);

    Pair<ReconstructionRegion, ImageTypeEnum> regionAndImageTypePair =
        new Pair<ReconstructionRegion, ImageTypeEnum>(reconstructionRegion, imageTypeEnum);

    // If region is marked to be cached, it has to be inspected.
    if (_reconstructionRegionsFinishedInspection.contains(regionAndImageTypePair))
      return true;

    return false;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized long getSizeInMemory(ReconstructionRegion reconstructionRegion, ImageTypeEnum imageTypeEnum)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(imageTypeEnum != null);
    Assert.expect(_reconstructedImagesOnDisk != null);
    Assert.expect(_reconstructedImagesInMemory != null);

    Pair<ReconstructionRegion, ImageTypeEnum> regionAndImageTypePair =
        new Pair<ReconstructionRegion, ImageTypeEnum>(reconstructionRegion, imageTypeEnum);

    long size = 0L;
    if (_reconstructedImagesOnDisk.containsKey(regionAndImageTypePair))
    {
      CachedToDiskRegionInformation cacheInformation = _reconstructedImagesOnDisk.get(regionAndImageTypePair);
      size = cacheInformation.getSizeInMemory();
    }
    else
    {
      // Kok Chun, Tan - XCR3784 - Memory not enough hung when save enhanced images.
      if (_regionToMemoryConsumedByActiveReconstructedImageMap.containsKey(reconstructionRegion))
      {
        size = _regionToMemoryConsumedByActiveReconstructedImageMap.get(reconstructionRegion);
        _regionToMemoryConsumedByActiveReconstructedImageMap.remove(reconstructionRegion);
      }
    }
    return size;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized Collection<Pair<ReconstructionRegion, ImageTypeEnum>> getRegionsFlaggedAsRepairImages()
  {
    List<Pair<ReconstructionRegion, ImageTypeEnum>> flaggedRegions =
      new ArrayList<Pair<ReconstructionRegion,ImageTypeEnum>>(_reconstructionRegionsToCacheRepairImages);

    return flaggedRegions;
  }

  /**
   * When a region is done inspecting, we will put it on a list to be a caching
   * candidate.
   * @author Rex Shang
   */
  public synchronized void markRegionAsFinishedInspection(ReconstructionRegion reconstructionRegion, ImageTypeEnum imageTypeEnum)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(imageTypeEnum != null);

    Pair<ReconstructionRegion, ImageTypeEnum> regionAndImageTypePair =
        new Pair<ReconstructionRegion, ImageTypeEnum>(reconstructionRegion, imageTypeEnum);

    if (hasReconstructedImages(reconstructionRegion, imageTypeEnum) == false)
      return;

    Assert.expect(_reconstructionRegionsFinishedInspection.contains(regionAndImageTypePair) == false);

    _reconstructionRegionsUnderInspection.remove(regionAndImageTypePair);
    _reconstructionRegionsFinishedInspection.add(regionAndImageTypePair);

    if (_reconstructedImagesInMemory.containsKey(regionAndImageTypePair))
    {
      ReconstructedImages reconstructedImages = _reconstructedImagesInMemory.get(regionAndImageTypePair);
      reconstructedImages.clearOrthogonalImages(); // we should be done with these at this point. (RISKY!)
      reconstructedImages.clearEnhancedImages(); // we should be done with these at this point. (RISKY!)
    }
  }

  /**
   * @author Patrick Lacz
   * @author Rex Shang
   */
  public synchronized void setRegionToCacheAsRepairImage(ReconstructionRegion reconstructionRegion, ImageTypeEnum imageTypeEnum)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(imageTypeEnum != null);
    Assert.expect(_reconstructionRegionsToCacheRepairImages != null);

    Pair<ReconstructionRegion, ImageTypeEnum> regionAndImageTypePair =
        new Pair<ReconstructionRegion, ImageTypeEnum>(reconstructionRegion, imageTypeEnum);

    if (hasReconstructedImages(reconstructionRegion, imageTypeEnum) == false)
      return;

    Assert.expect(_reconstructionRegionsToCacheRepairImages.contains(regionAndImageTypePair) == false);

    _reconstructionRegionsToCacheRepairImages.add(regionAndImageTypePair);
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized Map<SliceNameEnum, String> getSliceNameToCachedImagePathMap(ReconstructionRegion reconstructionRegion, ImageTypeEnum imageTypeEnum)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(imageTypeEnum != null);

    Pair<ReconstructionRegion, ImageTypeEnum> regionAndImageTypePair =
        new Pair<ReconstructionRegion, ImageTypeEnum>(reconstructionRegion, imageTypeEnum);

    CachedToDiskRegionInformation cacheInformation = _reconstructedImagesOnDisk.get(regionAndImageTypePair);
    Assert.expect(cacheInformation != null);

    return cacheInformation.getSliceNameToFullPathOfCachedImageMap();
  }

  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  public synchronized ReconstructedImages getReconstructedImages(ReconstructionRegion reconstructionRegion,
                                                                 ImageTypeEnum imageTypeEnum) throws DatastoreException
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(imageTypeEnum != null);
    Assert.expect(_reconstructedImagesInMemory != null);
    Assert.expect(_reconstructedImagesOnDisk != null);

    // Check to see if we have such an image in memory.
    Pair<ReconstructionRegion, ImageTypeEnum> regionAndImageTypePair =
        new Pair<ReconstructionRegion, ImageTypeEnum>(reconstructionRegion, imageTypeEnum);
    ReconstructedImages reconstructedImages = _reconstructedImagesInMemory.get(regionAndImageTypePair);
    if (reconstructedImages == null)
    {
      // Images are not in memory, check to see if they are on disk.
      if (_reconstructedImagesOnDisk.containsKey(regionAndImageTypePair))
      {
        // Is there enough memory available to swap the images back in from disk?
        // Swap entries in memory to disk until there is enough memory to bring in the requested images back
        // into memory from disk.
        cacheReconstructedImagesToDiskAsNecessary();

        // Load the images from disk.
        reconstructedImages = loadCachedReconstructedImagesFromDisk(regionAndImageTypePair);
      }
      else
      {
        // If we got here, it means the requested image is neither in memory nor on disk.  Bad times!
        Assert.expect(false, "Requested image is neither in memory nor on disk! " + 
                "\nRegionID: " + regionAndImageTypePair.getFirst().getRegionId() + 
                "(Component: "+regionAndImageTypePair.getFirst().getComponent().getReferenceDesignator()+")");
      }
    }

    _reconstructionRegionsUnderInspection.add(regionAndImageTypePair);
    reconstructedImages.incrementReferenceCount();
    return reconstructedImages;
  }

  /**
   * Adds the specified ReconstructedImages to the memory manager.  Caches to disk if there isn't enough memory.
   *
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  public synchronized long putReconstructedImages(ReconstructionRegion reconstructionRegion,
                                                  ImageTypeEnum imageTypeEnum,
                                                  ReconstructedImages reconstructedImages) throws DatastoreException
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(imageTypeEnum != null);
    Assert.expect(reconstructedImages != null);
    Assert.expect(_reconstructedImagesInMemory != null);
    Assert.expect(_reconstructedImagesOnDisk != null);
    Assert.expect(_regionToMemoryConsumedByActiveReconstructedImageMap.containsKey(reconstructionRegion) == false);

    Pair<ReconstructionRegion, ImageTypeEnum> regionAndImageTypePair =
        new Pair<ReconstructionRegion, ImageTypeEnum>(reconstructionRegion, imageTypeEnum);

    Assert.expect(_reconstructedImagesOnDisk.containsKey(regionAndImageTypePair) == false &&
           _reconstructedImagesInMemory.containsKey(regionAndImageTypePair) == false);

    // Check to see if we have space in memory for the images.
    cacheReconstructedImagesToDiskAsNecessary();

    // We should have enough space in memory.
    // Add an entry to the "in memory" map.
    reconstructedImages.incrementReferenceCount();
    _reconstructedImagesInMemory.put(regionAndImageTypePair, reconstructedImages);
    long sizeOfImages = memorySize(reconstructedImages);
    // Kok Chun, Tan - XCR3784 - Memory not enough hung when save enhanced images.
    _regionToMemoryConsumedByActiveReconstructedImageMap.put(reconstructionRegion, sizeOfImages);
    return sizeOfImages;
  }

  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  synchronized long freeReconstructedImages(ReconstructionRegion reconstructionRegion, ImageTypeEnum imageTypeEnum)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(imageTypeEnum != null);
    Assert.expect(_reconstructedImagesInMemory != null);
    Assert.expect(_reconstructedImagesOnDisk != null);

    Pair<ReconstructionRegion, ImageTypeEnum> regionAndImageTypePair =
        new Pair<ReconstructionRegion, ImageTypeEnum>(reconstructionRegion, imageTypeEnum);

    // Remove the entry from the "in memory" map if needed.
    long sizeOfImages = 0L;

    if (_reconstructionRegionsUnderInspection.contains(regionAndImageTypePair))
    {
      _reconstructionRegionsUnderInspection.remove(regionAndImageTypePair);
    }

    if (_reconstructionRegionsFinishedInspection.contains(regionAndImageTypePair))
    {
      _reconstructionRegionsFinishedInspection.remove(regionAndImageTypePair);
    }

    if (_reconstructionRegionsToCacheRepairImages.contains(regionAndImageTypePair))
    {
      _reconstructionRegionsToCacheRepairImages.remove(regionAndImageTypePair);
    }

    // Is the entry in the "in memory" map?
    if (_reconstructedImagesInMemory.containsKey(regionAndImageTypePair))
    {
      // Remove the image from the "in memory" map.
      ReconstructedImages reconstructedImages = _reconstructedImagesInMemory.remove(regionAndImageTypePair);
      // Kok Chun, Tan - XCR3784 - Memory not enough hung when save enhanced images.
      if (_regionToMemoryConsumedByActiveReconstructedImageMap.containsKey(reconstructionRegion))
      {
        sizeOfImages = _regionToMemoryConsumedByActiveReconstructedImageMap.get(reconstructionRegion);
        _regionToMemoryConsumedByActiveReconstructedImageMap.remove(reconstructionRegion);
      }
      reconstructedImages.decrementReferenceCount();
    }
    // Is the entry in the "on disk" map?
    else if (_reconstructedImagesOnDisk.containsKey(regionAndImageTypePair))
    {
      // Remove the entry from the "on disk" map.
      CachedToDiskRegionInformation cacheInformation = _reconstructedImagesOnDisk.remove(regionAndImageTypePair);
      sizeOfImages = cacheInformation.getSizeInMemory();
    }
    return sizeOfImages;
  }

  /**
   * @author Roy Williams
   * @author Matt Whraton
   */
  private long memorySize(ReconstructedImages reconstructedImages)
  {
    Assert.expect(reconstructedImages != null);

    return reconstructedImages.getMaximumExpectedMemorySize();
  }

  /**
   * Before we put an new set of reconstructed images to memory, we use
   * this method to cache other images to disk as needed.
   * @author Rex Shang
   */
  public synchronized void cacheReconstructedImagesToDiskAsNecessary() throws DatastoreException
  {
    // Check to see if caching is necessary.
    if (_nativeMemoryMonitor.getTotalUsedMemoryInMegabytes() >
        _MAX_IMAGE_MEMORY_USAGE_BEFORE_CACHING_TO_DISK)
    {
      // Calculate hyteresis.
      int memoryLimit = (int)(_MAX_IMAGE_MEMORY_USAGE_BEFORE_CACHING_TO_DISK * (1.0f - _THRESHOLD_HYSTERESIS_PERCENTAGE));

      // First, cache the images that are marked to be candidates.
      Iterator<Pair<ReconstructionRegion, ImageTypeEnum>> regionIterator = _reconstructionRegionsFinishedInspection.iterator();
      while (regionIterator.hasNext()
             && _nativeMemoryMonitor.getTotalUsedMemoryInMegabytes() > memoryLimit)
      {
        Pair<ReconstructionRegion, ImageTypeEnum> swappedOutRegionAndImageTypePair = regionIterator.next();

        // Remove the entry from the "in memory" map.
        ReconstructedImages swappedOutReconstructedImages =
            _reconstructedImagesInMemory.remove(swappedOutRegionAndImageTypePair);

        if (swappedOutReconstructedImages != null)
        {
          // Save the image to disk.
          cacheReconstructedImagesToDisk(swappedOutRegionAndImageTypePair, swappedOutReconstructedImages);
        }
        else
        {
          if (_reconstructedImagesOnDisk.containsKey(swappedOutRegionAndImageTypePair) == false)
          {
            Assert.expect(false, "swappedOutReconstructedImages is not on disk either for region "
                          + swappedOutRegionAndImageTypePair.getFirst().getRegionId());
          }
        }
      }

      // If we still need to cache more, we will start caching uninspected images.
      if (_nativeMemoryMonitor.getTotalUsedMemoryInMegabytes() > memoryLimit)
      {
        Set<Pair<ReconstructionRegion, ImageTypeEnum>> regionSet = _reconstructedImagesInMemory.keySet();
        // Since we need to do Most Recent Used Caching scheme, we need to reverse
        // our list.
        ArrayList<Pair<ReconstructionRegion, ImageTypeEnum>> myList = new ArrayList<Pair<ReconstructionRegion, ImageTypeEnum>>(regionSet);
        Collections.reverse(myList);

        regionIterator = myList.iterator();
        while (regionIterator.hasNext()
               && _nativeMemoryMonitor.getTotalUsedMemoryInMegabytes() > memoryLimit)
        {
          Pair<ReconstructionRegion, ImageTypeEnum> swappedOutRegionAndImageTypePair = regionIterator.next();

          // Make sure we don't cache something that is still under inspection.
          if (_reconstructionRegionsUnderInspection.contains(swappedOutRegionAndImageTypePair) == false)
          {
            // Remove the entry from the "in memory" map.
            ReconstructedImages swappedOutReconstructedImages =
                _reconstructedImagesInMemory.remove(swappedOutRegionAndImageTypePair);

            Assert.expect(swappedOutReconstructedImages != null, "swappedOutReconstructedImages missing!");
            // Save the image to disk.
            cacheReconstructedImagesToDisk(swappedOutRegionAndImageTypePair, swappedOutReconstructedImages);
          }
        }
      }
    }
  }

  /**
   * @author Matt Wharton
   */
  private void cacheReconstructedImagesToDisk(Pair<ReconstructionRegion, ImageTypeEnum> regionAndImageTypePair,
                                              ReconstructedImages reconstructedImages) throws DatastoreException
  {
    Assert.expect(regionAndImageTypePair != null);
    Assert.expect(reconstructedImages != null);

    ReconstructionRegion reconstructionRegion = regionAndImageTypePair.getFirst();
    String reconstructionRegionName = reconstructionRegion.getName();
    ImageTypeEnum imageTypeEnum = regionAndImageTypePair.getSecond();
    int imageTypeId = imageTypeEnum.getId();

    CachedToDiskRegionInformation cacheInformation = new CachedToDiskRegionInformation();
    cacheInformation.setSizeInMemory(memorySize(reconstructedImages));

    _progressMonitor.cachingImageToDisk();

    if (_reconstructionRegionsToCacheRepairImages.contains(regionAndImageTypePair) == true)
    {
      cacheAsRepairImages(reconstructedImages, reconstructionRegion, cacheInformation);
    }
    else
    {
      cacheAsInspectionImages(reconstructedImages, reconstructionRegionName, imageTypeId, cacheInformation);
    }

    // the reconstructed Images object should be decrefed in each of the branches of the above if statement.

    _progressMonitor.imageCachedToDisk();

    _reconstructedImagesOnDisk.put(regionAndImageTypePair, cacheInformation);
  }

  /**
   * @author Patrick Lacz
   */
  private void cacheAsInspectionImages(ReconstructedImages reconstructedImages,
                                       String reconstructionRegionName,
                                       int imageTypeId,
                                       CachedToDiskRegionInformation cacheInformation)
      throws DatastoreException
  {
    cacheInformation.setIsSavedForRepair(false);

    Set<Pair<String, Image>> imagesToSave = new HashSet<Pair<String,Image>>();

    // Save each ReconstructedSlice.
    for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
    {
      int sliceId = reconstructedSlice.getSliceNameEnum().getId();
      String cachedImageFile = FileName.getCachedInspectionImageName(reconstructionRegionName, sliceId, imageTypeId);
      String cachedImageFullPath = _TEMP_IMAGE_CACHE_DIR + File.separator + cachedImageFile;

      // Save the image if it's not already saved.
      if (_savedImageFileNames.contains(cachedImageFullPath) == false)
      {
        Image image = reconstructedSlice.getImage();
        image.incrementReferenceCount();

        cacheInformation.addSliceNameToFullPathOfImage(reconstructedSlice.getSliceNameEnum(), cachedImageFullPath);

        imagesToSave.add(new Pair<String, Image>(cachedImageFullPath, image));
        _savedImageFileNames.add(cachedImageFullPath);
      }
    }

    // potentially clear out all the orthogonal images and any already saved images (?? unlikely)
    reconstructedImages.decrementReferenceCount();

    for (Pair<String, Image> pathImagePair : imagesToSave)
    {
      String cachedImageFullPath = pathImagePair.getFirst();
      Image image = pathImagePair.getSecond();

//      XrayImageIoUtil.savePngImage(image, cachedImageFullPath);
      if (Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType() )
        XrayImageIoUtil.saveTiffImage(image, cachedImageFullPath);
      else
        XrayImageIoUtil.savePngImage(image, cachedImageFullPath);
      
      image.decrementReferenceCount();
    }
  }

  /**
   * @author Patrick Lacz
   * @author Kee Chin Seong - Adding the Width Height to make the image name unique
   * @author Khang Wah, Chnee
   * - to remove 2.5d image from any contrast enhancement activity
   */
  private void cacheAsRepairImages(ReconstructedImages reconstructedImages,
                                   ReconstructionRegion reconstructionRegion,
                                   CachedToDiskRegionInformation cacheInformation)
      throws DatastoreException
  {
    // write out the repair images, not images that we would read in for inspection

    PanelRectangle panelRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    cacheInformation.setIsSavedForRepair(true);

    Map<String, Image> imagesToWriteToDisk = new HashMap<String, Image>();
    Map<SliceNameEnum, Image> imagesToEnhance = new HashMap<SliceNameEnum, Image>();


    // Save each ReconstructedSlice.
    for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
    {
      int sliceId = reconstructedSlice.getSliceNameEnum().getId();
      String cachedImageFile = FileName.getResultImageName(reconstructionRegion.isTopSide(), panelRect.getMinX(), panelRect.getMinY(), 
                                                           panelRect.getWidth(), panelRect.getHeight(), sliceId);
      String cachedImageFullPath = _TEMP_IMAGE_CACHE_DIR + File.separator + cachedImageFile;

      cacheInformation.addSliceNameToFullPathOfImage(reconstructedSlice.getSliceNameEnum(), cachedImageFullPath);

      // Save the image if it's not already saved.
      if (_savedImageFileNames.contains(cachedImageFullPath) == false)
      {
        Image image = new Image(reconstructedSlice.getImage());
        imagesToWriteToDisk.put(cachedImageFullPath, image);
        imagesToEnhance.put(reconstructedSlice.getSliceNameEnum(), image);
        
        _savedImageFileNames.add(cachedImageFullPath);
      }
    }

    reconstructedImages.decrementReferenceCount(); // clear out orthogonal images and other already saved images.

    ImageEnhancement.autoEnhanceContrastInPlace(ImageManager.getInstance().excludeUnnecessaryImageFromEnhancement(imagesToEnhance).values());

    for (Map.Entry<String, Image> repairImageEntry : imagesToWriteToDisk.entrySet())
    {
      String cachedImageFullPath = repairImageEntry.getKey();
      Image enhancedImage = repairImageEntry.getValue();
      XrayImageIoUtil.saveJpegImage(enhancedImage, cachedImageFullPath);
      enhancedImage.decrementReferenceCount();
    }
  }

  /**
   * @author Matt Wharton
   */
  private ReconstructedImages loadCachedReconstructedImagesFromDisk(
      Pair<ReconstructionRegion, ImageTypeEnum> regionAndImageTypePair) throws DatastoreException
  {
    Assert.expect(regionAndImageTypePair != null);

    // images "saved for repair" should not be resurrected for any reason.
    Assert.expect(_reconstructedImagesOnDisk.get(regionAndImageTypePair).isSavedForRepair() == false,
                  "Region is already cached as repair images, region id: " + regionAndImageTypePair.getFirst().getRegionId()
                  + " , region name " + regionAndImageTypePair.getFirst().getName());

    ReconstructionRegion reconstructionRegion = regionAndImageTypePair.getFirst();
    String reconstructionRegionName = reconstructionRegion.getName();
    ImageTypeEnum imageTypeEnum = regionAndImageTypePair.getSecond();
    int imageTypeId = imageTypeEnum.getId();
    ReconstructedImages reconstructedImages = null;

    // Find all image files which match the specified reconstruction region name and image type id.
    String cachedImageFileNameRegEx = ".*" + reconstructionRegionName + "_(\\d+)_" + imageTypeId + ".+";
    Pattern pattern = Pattern.compile(cachedImageFileNameRegEx, Pattern.CASE_INSENSITIVE);
    Collection<String> cachedImageFiles = FileUtilAxi.listAllFilesFullPathInDirectory(_TEMP_IMAGE_CACHE_DIR);
    for (String cachedImageFile : cachedImageFiles)
    {
      Matcher matcher = pattern.matcher(cachedImageFile);
      if (matcher.matches())
      {
        int sliceId = -1;
        try
        {
          sliceId = StringUtil.convertStringToInt(matcher.group(1));
        }
        catch (BadFormatException bfex)
        {
          // If I get this, it means there's a bug in my regex.
          Assert.expect(false);
        }

        _progressMonitor.loadingImageFromDisk();

        // Load the cached image.
        Image image = null;
        String extension = FileUtil.getExtension(cachedImageFile);
        if(extension.equalsIgnoreCase(".png"))
          image = XrayImageIoUtil.loadPngImage(cachedImageFile);
        else if(extension.equalsIgnoreCase(".tiff"))
          image = XrayImageIoUtil.loadTiffImage(cachedImageFile);

        // Create the ReconstructedImages object (if needed) and add the image to it.
        if (reconstructedImages == null)
        {
          reconstructedImages = new ReconstructedImages(reconstructionRegion);
        }
        SliceNameEnum sliceNameEnum = SliceNameEnum.getEnumFromIndex(sliceId);
        reconstructedImages.addImage(image, sliceNameEnum);
        image.decrementReferenceCount();
      }

      _progressMonitor.imageLoadedFromDisk();
    }

    // Add an entry to the "in memory" map.
    Assert.expect(reconstructedImages != null);

    _reconstructedImagesInMemory.put(regionAndImageTypePair, reconstructedImages);


    // Remove the entry from the "on disk" map.
    _reconstructedImagesOnDisk.remove(regionAndImageTypePair);
    return reconstructedImages;
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean isAllReconstructionRegionsFinishedInspection()
  {
    return _reconstructionRegionsUnderInspection.isEmpty();
  }
}
