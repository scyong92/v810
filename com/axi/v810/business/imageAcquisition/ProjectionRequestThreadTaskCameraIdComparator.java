package com.axi.v810.business.imageAcquisition;

import java.util.Comparator;
import com.axi.util.*;

/**
 * @author Roy Williams
 */
public class ProjectionRequestThreadTaskCameraIdComparator implements Comparator<ProjectionRequestThreadTask>
{
  /**
   * @author Roy Williams
   */
  public int compare(ProjectionRequestThreadTask projectionRequestThreadTask1, ProjectionRequestThreadTask projectionRequestThreadTask2)
  {
    Assert.expect(projectionRequestThreadTask1 != null);
    Assert.expect(projectionRequestThreadTask1 != null);

    int diff = projectionRequestThreadTask1.getCamera().getId() - projectionRequestThreadTask2.getCamera().getId();

    if (diff < 0)
      return -1;

    if (diff > 0)
      return 1;

    return 0;
  }
}

