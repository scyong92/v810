package com.axi.v810.business.imageAcquisition;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * A list of ProjectionRequests are given to the ImageAcquisitionEngine to obtain
 * images for a specified rectangle on a camera.   The list allows the retangle to
 * be imaged on multiple cameras.   As the image is captured it will be attached
 * to this original request as a ProjectionInfo object.
 * <p>
 * Sometimes the requestor really cares about the direction of travel of the stage
 * and other times it is not important.  Please leave this as a degree of freedom
 * for the ImageAcquisitionEngine whenever possible.
 * <p>
 * @author Roy Williams
 */
public class ProjectionRequestThreadTask extends ThreadTask<Image>
{
  private AbstractXrayCamera _camera;
  private XrayCameraArray _xRayCameraArray;
  private ScanPassDirectionEnum   _stageDirectionEnum;
  private SystemFiducialRectangle _systemFiducialRectangle;
  private SystemFiducialRectangle _originalRequestedRectangle;
  private int                     _numberOfLinesToCapture         = -1;
  private ProjectionInfo          _projectionInfo; // results
  private int                     _scanPassNumber                 = -1;
  private int                     _minimumScanLengthInNanometers  = 0;
  private int                     _additionalScanHeightNanometers = 0;
  private boolean                 _isRealRequestNeeded            = true;
  private boolean                 _cancelled;

  private static ProjectionRequestImageSaver _imageSaver;

  /**
   * @author Roy Williams
   * @author Eddie Williamson
   */
  public ProjectionRequestThreadTask(AbstractXrayCamera camera,
                                     ScanPassDirectionEnum stageDirectionEnum,
                                     SystemFiducialRectangle systemFiducialRectangle,
                                     int aboveImagePaddingNanometers)
  {
    super("ProjectionRequest capture for camera: " + camera.getId());
    Assert.expect(stageDirectionEnum != null);
    Assert.expect(systemFiducialRectangle != null);

    _camera = camera;
    _xRayCameraArray = XrayCameraArray.getInstance();
    _stageDirectionEnum = stageDirectionEnum;

    if (UnitTest.unitTesting())
    {
      // using hard coded value for unit testing because pre-recorded image files
      // are exactly this length.  And, no hardware is involved in these unit tests.
      _minimumScanLengthInNanometers = _xRayCameraArray.getOneInchInNanometersAlignedUpToPixelBoundary();
    }
    else
    {
      _minimumScanLengthInNanometers = MechanicalConversions.getMinimumStageTravelInNanometers();
    }
    _originalRequestedRectangle = systemFiducialRectangle;
    _additionalScanHeightNanometers = Math.max(0, _minimumScanLengthInNanometers - systemFiducialRectangle.getHeight());
    _additionalScanHeightNanometers = aboveImagePaddingNanometers;
    _systemFiducialRectangle = new SystemFiducialRectangle(systemFiducialRectangle.getMinX(),
        systemFiducialRectangle.getMinY(),
        systemFiducialRectangle.getWidth(),
        systemFiducialRectangle.getHeight() + _additionalScanHeightNanometers);


    _projectionInfo          = new ProjectionInfo();
    if (_imageSaver == null)
    {
      _imageSaver = new ProjectionRequestImageSaver();
    }
  }

  /**
   * @author Roy Williams
   */
  public ProjectionRequestThreadTask(AbstractXrayCamera camera,
                                     ScanPassDirectionEnum stageDirectionEnum,
                                     SystemFiducialRectangle systemFiducialRectangle)
  {
    /** @todo Eddie uncomment this when ready */
    this(camera, stageDirectionEnum, systemFiducialRectangle, 0);

//    super("ProjectionRequest capture for camera: " + cameraId);
//    Assert.expect(cameraId >= 0 && cameraId < XrayCameraArray.getInstance().getNumberOfCameras());
//    Assert.expect(stageDirectionEnum != null);
//    Assert.expect(systemFiducialRectangle != null);
//
//    _cameraId                = cameraId;
//    _stageDirectionEnum      = stageDirectionEnum;
//    _systemFiducialRectangle = systemFiducialRectangle;
//    _projectionInfo          = new ProjectionInfo();
//    if (_imageSaver == null)
//    {
//      _imageSaver = new ProjectionRequestImageSaver();
//    }
  }


  /**
   * @author Eddie Williamson
   */
  public void setRealRequestNeeded(boolean needed)
  {
    _isRealRequestNeeded = needed;
  }

  /**
   * @author Roy Williams
   */
  public AbstractXrayCamera getCamera()
  {
    return _camera;
  }

  /**
   * @author Roy Williams
   */
  public ScanPassDirectionEnum getStageDirection()
  {
    Assert.expect(_stageDirectionEnum != null);
    return _stageDirectionEnum;
  }

  /**
   * @author Roy Williams
   */
  public void setStageDirection(ScanPassDirectionEnum direction)
  {
    Assert.expect(direction != null);
    _stageDirectionEnum = direction;
  }

  /**
   * @author Roy Williams
   */
  public SystemFiducialRectangle getSystemFiducialRectangle()
  {
    Assert.expect(_systemFiducialRectangle != null);

    return _systemFiducialRectangle;
  }

  /**
   * Sets the SystemFiducialRectangle as far right as is feasible for the assigned camera.
   *
   * <p>
   * <blockquote>
   * <pre>
   *                                     ------
   *                                     |    |
   *     --------------------------------|    |
   *     |        camera rectangle       |    |
   *     --------------------------------|    |
   *                                     |    |
   *                                     x-----
   * </pre>
   * </blockquote>
   * <p>
   * "x" marks the bottom-left origin of the resulting MachineRectangle (MR).
   * <p>
   * The entire resulting SystemFiducialRectangle must be moved over the entire
   * camera area to provide an image due to the Time Domain Integration techinque
   * of gathering camera images.
   *
   * @author Roy Williams
   */
  public int alignSystemFiducialRectangleRight(ScanPassDirectionEnum direction, int yStagePosition)
  {
    Assert.expect(direction != null);

    int cameraMaxXWithoutNonImageableMarginInNanometers = _xRayCameraArray.getCameraMaxXWithoutNonImageableMarginInNanometers(getCamera(), MagnificationEnum.getCurrentNorminal());
    int xStagePosition = cameraMaxXWithoutNonImageableMarginInNanometers - _systemFiducialRectangle.getWidth() - _systemFiducialRectangle.getMinX();

    // Record the projection info.
    setProjectionInfo(xStagePosition, yStagePosition, direction);

    return xStagePosition;

  }

  /**
   * Sets the SystemFiducialRectangle as far right as is feasible for the assigned camera.
   * <p>
   * This method also bears the a
   * constraint that the stage X has already been positioned.  And, it can be
   * anywhere in the camera rectangle.  If the entire width of the SystemFiducialRectangle
   * cannot fit on the camera, an Exception will be thrown to complain.
   * <p>
   * <blockquote>
   * <pre>
   *      stage X position
   *          |
   *          ------
   *          |    |
   *     -----|    |--------------------------|
   *     |    |    | camera rectangle         |
   *     -----|    |--------------------------|
   *          |    |
   *          x-----
   *          |
   *      stage X position
   * </pre>
   * </blockquote>
   * <p>
   * "x" marks the bottom-left origin of the resulting MachineRectangle.
   * <p>
   * The entire resulting SystemFiducialRectangle must be moved over the entire
   * camera area to provide an image due to the Time Domain Integration techinque
   * of gathering camera images.
   *
   * @author Roy Williams
   */
  public void alignSystemFiducialRectangleToCurrentStagePosition(int xStagePosition,
                                                                 int yStagePostion,
                                                                 ScanPassDirectionEnum direction) throws SystemFiducialRectangleFitException
  {
    Assert.expect(xStagePosition > 0);
    Assert.expect(direction != null);

    MachineRectangle cameraRectangleAtReferencePlane = _xRayCameraArray.getCameraSensorRectangle(getCamera(), MagnificationEnum.getCurrentNorminal());
    int systemFiducialRectangleWidth  = _systemFiducialRectangle.getWidth();
    int cameraMinX = cameraRectangleAtReferencePlane.getMinX();

    // Let's get out of here immediately if the SystemFiducialRectangle will not
    // fit on the camera...  given the x position of the stage.
    int leftSideOfRectangle = xStagePosition + _systemFiducialRectangle.getMinX();
    if (leftSideOfRectangle < cameraMinX  ||
        leftSideOfRectangle + systemFiducialRectangleWidth > cameraRectangleAtReferencePlane.getMaxX())
      throw new SystemFiducialRectangleFitException();

    // Record the projection info.
    setProjectionInfo(xStagePosition, yStagePostion, direction);
  }

  /**
   * @author Roy Williams
   */
  public void setProjectionInfo(int xStagePosition, int yStagePosition, ScanPassDirectionEnum direction)
  {
    Assert.expect(xStagePosition > 0);
    Assert.expect(direction != null);

    MachineRectangle cameraRectangleAtReferencePlane = _xRayCameraArray.getCameraSensorRectangle(getCamera(), MagnificationEnum.getCurrentNorminal());
    int cameraMinX = cameraRectangleAtReferencePlane.getMinX();
    int nanoMetersPerPixel = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();

    // Record the x stage position.
    _projectionInfo.setXStageWhereFirstRowImagedOnVerticalCenterOfCamera(xStagePosition);

    // Record the y stage position.
    _projectionInfo.setYStageWhereFirstRowImagedOnVerticalCenterOfCamera(yStagePosition);

    // Record the ImageRectangle. This is what will be cropped from the full camera image.
    int leftSideOfRectangle = xStagePosition + _systemFiducialRectangle.getMinX();
    ImageRectangle imageRectangle = new ImageRectangle((int)Math.round((float)(leftSideOfRectangle - cameraMinX) / nanoMetersPerPixel),
                                                       _additionalScanHeightNanometers / nanoMetersPerPixel,
                                                       (int)Math.round((float)(_originalRequestedRectangle.getWidth()) / nanoMetersPerPixel),
                                                       (int)Math.round((float)_originalRequestedRectangle.getHeight() / nanoMetersPerPixel));
    // record the image height used to generate the scan path
    _projectionInfo.setHeightOfScanPixels(_systemFiducialRectangle.getHeight() / nanoMetersPerPixel);
    _projectionInfo.setImageRectangleForRequestedProjection(imageRectangle);
  }


  /**
   * @author Roy Williams
   */
  public ProjectionInfo getProjectionInfo()
  {
    Assert.expect(_projectionInfo != null);
    return _projectionInfo;
  }

  /**
   * @author Roy Williams
   */
  public void setNumberOfLinesToCapture(int numberOfLinesToCapture)
  {
    Assert.expect(numberOfLinesToCapture >= 0);
    _numberOfLinesToCapture = numberOfLinesToCapture;
  }

  /**
   * @author Roy Williams
   */
  public int getNumberOfLinesToCapture()
  {
    Assert.expect(_numberOfLinesToCapture >= 0);
    return _numberOfLinesToCapture;
  }

  /**
   * @author Roy Williams
   */
  public void scanPassNumber(int passNumber)
  {
    _scanPassNumber = passNumber;
  }

  /**
   * Get image from the camera.   The default timeout (3 second) rule should be
   * sufficient so we will only ask for the image once.  This is a very long time
   * since the stage has already been triggered to move.
   *
   * @author Roy Williams
   */
  protected Image executeTask() throws Exception
  {
    Assert.expect(_numberOfLinesToCapture > 0);
    Image image = null;

    // Between each step check to see if we should continue or cancel.
    if (_cancelled)
      return (Image)null;

    // Get the camera used for this request.
    AbstractXrayCamera camera = getCamera();

    // Fetch the image.
    ImageRectangle imageRectangle = _projectionInfo.getImageRectangleForRequestedProjection();
    Image cameraImage = camera.getLineScanImage();
    int orientationInDegrees = 0;
    RegionOfInterest regionOfInterest = new RegionOfInterest(imageRectangle, orientationInDegrees, RegionShapeEnum.RECTANGULAR);
    image = Image.createCopy(cameraImage, regionOfInterest);
    cameraImage.decrementReferenceCount();

    // Between each step check to see if we should continue or cancel.
    if (_cancelled)
    {
      if (image != null)
        image.decrementReferenceCount();
      return (Image)null;
    }
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.SAVE_IMAGE_TO_DISK_DEBUG))
    {
      _imageSaver.saveImageToDisk(_camera,
                                  _scanPassNumber,
                                  getStageDirection().toString(),
                                  image);
    }

    _projectionInfo.setProjection(image);
    image.decrementReferenceCount(); // the ProjectionInfo object takes ownership of this object.
    return image;
  }

  /**
   * @author Roy Williams
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Roy Williams
   */
  protected void cancel() throws XrayTesterException
  {
    _cancelled = true;
    getCamera().abort();
  }

  /**
   * @author Patrick Lacz
   */
  public void clear()
  {
    if (_projectionInfo != null)
      _projectionInfo.clear();
  }


  /**
   * Use the x location of the SystemFiducial relative to the camera being used
   * to sort.   The returned value will be the x position of the stage to place
   * the system fiducial (i.e. a point) in order to place it at the spot
   * designated by (or requested by) the SystemFiducialRectangle.
   *
   * @author Roy Williams
   */
  public int xSortPosition()
  {
    if(XrayTester.isLowMagnification())
    {
      MachineRectangle cameraSensorRectangle = _xRayCameraArray.getCameraSensorRectangle(getCamera());
      int cameraMinX = cameraSensorRectangle.getMinX();
      return  cameraMinX - _systemFiducialRectangle.getMinX();
    }
    else
    {
      MachineRectangle cameraSensorRectangle = _xRayCameraArray.getCameraSensorRectangleForHighMag(getCamera());
      int cameraMinX = cameraSensorRectangle.getMinX();
      return  cameraMinX - _systemFiducialRectangle.getMinX();
    }
  }

  /**
   * @return a string of comma separated field names that can be interpreted as the header of what will come out when a toString operation is performed.
   *
   * @author Roy Williams
   */
  static public String toStringOrder()
  {
    String commandSeparator = ", ";
    StringBuffer buf = new StringBuffer();
    buf.append("stageDir"      + commandSeparator);
    buf.append("cameraId"      + commandSeparator);
    buf.append(ProjectionInfo.toStringOrder());
    return buf.toString();
  }

  /**
   * Returns a comma separated list of fields containing all of the information
   * contained within this ProjectionRequest.
   */
  public String toString()
  {
    String commaSeparator = ", ";
    StringBuffer buf = new StringBuffer(_stageDirectionEnum + commaSeparator);
    buf.append(_camera.getId() + commaSeparator);
    buf.append(_projectionInfo.toString());
    return buf.toString();
  }
}
