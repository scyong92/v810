package com.axi.v810.business;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Wei Chin
 */
public class XoutDetectionMethodEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;
  private static HashMap<String, XoutDetectionMethodEnum> _stringToEnumMap;
  private String _name;

  /**
   * @author Wei Chin
   */
  static
  {
    _stringToEnumMap = new HashMap<String, XoutDetectionMethodEnum>();
  }

  public static final XoutDetectionMethodEnum ALIGNMENT_FAILURE = new XoutDetectionMethodEnum(
      ++_index, "alignmentFailure");

  public static final XoutDetectionMethodEnum DEFECT_THRESHOLD = new XoutDetectionMethodEnum(
      ++_index, "defectThreshold");

  /**
   * @author Wei Chin
   */
  private XoutDetectionMethodEnum(int id, String detectionMethodString)
  {
    super(id);

    Assert.expect(detectionMethodString != null);
    _name = detectionMethodString;

    Assert.expect(_stringToEnumMap.put(detectionMethodString, this) == null);
  }

  /**
   * @author Wei Chin
   */
  static XoutDetectionMethodEnum getEnum(String keyString)
  {
    Assert.expect(keyString != null);

    XoutDetectionMethodEnum modeEnum = _stringToEnumMap.get(keyString);

    Assert.expect(modeEnum != null);
    return modeEnum;
  }

  /**
   * @author Wei Chin
   */
  public static List<XoutDetectionMethodEnum> getAllValues()
  {
    List<XoutDetectionMethodEnum> allValues = new ArrayList<XoutDetectionMethodEnum>();
    allValues.add(XoutDetectionMethodEnum.ALIGNMENT_FAILURE);
    allValues.add(XoutDetectionMethodEnum.DEFECT_THRESHOLD);
    return allValues;
  }

  /**
   * @author Wei Chin
   */
  public String toString()
  {
    return _name;
  }
}
