/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.business;

/**
 *
 * @author chin-seong.kee
 */
public class DefectPackagerTaskEventEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static DefectPackagerTaskEventEnum START_DEFECT_PACKAGER               = new DefectPackagerTaskEventEnum(++_index);
  public static DefectPackagerTaskEventEnum STOP_DEFECT_PACKAGER                = new DefectPackagerTaskEventEnum(++_index);
  
  public static DefectPackagerTaskEventEnum DEFECT_PACKAGER_RUNNING             = new DefectPackagerTaskEventEnum(++_index);
  public static DefectPackagerTaskEventEnum DEFECT_PACKAGER_STOPPED             = new DefectPackagerTaskEventEnum(++_index);
  public static DefectPackagerTaskEventEnum DEFECT_PACKAGER_NOT_INSTALLED       = new DefectPackagerTaskEventEnum(++_index);

  /**
   * @author George A. David
   */
  private DefectPackagerTaskEventEnum(int id)
  {
    super(id);
  }
}