package com.axi.v810.business.testGen;

import Jama.Matrix;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.ArrayList;
import com.axi.util.Assert;
import com.axi.util.AssertException;
import com.axi.util.Expect;
import com.axi.util.MathUtil;
import com.axi.util.MathUtilEnum;
import com.axi.util.UnitTest;
import com.axi.v810.datastore.DatastoreException;
import com.axi.v810.datastore.config.Config;
import com.axi.v810.datastore.config.HardwareConfigEnum;
import com.axi.v810.datastore.config.SoftwareConfigEnum;
import com.axi.v810.util.FileUtilAxi;
import com.axi.v810.hardware.*;

public class Test_VerificationSurfaceEstimator extends UnitTest
{
  private final boolean _twoPassVerificationSurfaceModelingEnabled;

  /**
   * @author Bob Balliew
   */
  private Test_VerificationSurfaceEstimator() throws DatastoreException
  {
    super();

    try
    {
      Config.getInstance().loadIfNecessary();
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }

    _twoPassVerificationSurfaceModelingEnabled = VerificationSurfaceEstimator.isTwoPassVerificationSurfaceModelingEnabled();
  }

  /**
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  public static void main(String[] args)
  {
    try
    {
      UnitTest.execute(new Test_VerificationSurfaceEstimator());
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
  }

  /**
   * Swap the content of zTop and zBottom for each index in swapList.
   * @param zTop an array of data.
   * @param zBottom another array of data.
   * @param swapList an array of indices into zTop/zBottom to be swapped.
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  private void swapPairs(double[] zTop, double[] zBottom, int[] swapList)
  {
    Assert.expect(zTop != null);
    Assert.expect(zBottom != null);
    Assert.expect(swapList != null);
    Assert.expect(zTop != zBottom);
    Assert.expect(zTop.length == zBottom.length);

    for (int i : swapList)
    {
      final double temp = zTop[i];
      zTop[i] = zBottom[i];
      zBottom[i] = temp;
    }
  }

  /**
   * Swap the content of zTop and zBottom for each index in swapList.
   * @param zTop an array of data.
   * @param zBottom another array of data.
   * @param swapList an array of indices into zTop/zBottom to be swapped.
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  private void offsetPairs(double[] zTop, double[] zBottom, int[] offsetList, double delta)
  {
    Assert.expect(zTop != null);
    Assert.expect(zBottom != null);
    Assert.expect(offsetList != null);
    Assert.expect(zTop != zBottom);
    Assert.expect(zTop.length == zBottom.length);

    for (int i : offsetList)
    {
      zTop[i]    += delta;
      zBottom[i] += delta;
    }
  }

  /**
   * Swap the content of zTop and zBottom for each index in swapList.
   * @param zTop an array of data.
   * @param zBottom another array of data.
   * @param swapList an array of indices into zTop/zBottom to be swapped.
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  private void offsetValues(double[] zArray, int[] offsetList, double[] delta)
  {
    Assert.expect(zArray != null);
    Assert.expect(offsetList != null);
    Assert.expect(delta != null);
    Assert.expect(zArray != delta);
    Assert.expect(zArray.length == delta.length);

    for (int i : offsetList)
      zArray[i] += delta[i];
  }

  /**
    * @param array is the array to search for a maximum value.
    * @return the maximum value in the array.
    * @author Bob Balliew
    */
  int maxArray(int[] array)
  {
    Assert.expect(array != null);
    int max = array[0];

    for (int value : array)
      max = Math.max(max, value);

    return max;
  }

  /**
    * @param array is the array to search for a minimum value.
    * @return the minimum value in the array.
    * @author Bob Balliew
    */
  int minArray(int[] array)
  {
    Assert.expect(array != null);
    int min = array[0];

    for (int value : array)
      min = Math.max(min, value);

    return min;
  }

  /**
    * @param array is the array to search for a maximum value.
    * @return the maximum value in the array.
    * @author Bob Balliew
    */
  double maxArray(double[] array)
  {
    Assert.expect(array != null);
    double max = array[0];

    for (double value : array)
      max = Math.max(max, value);

    return max;
  }

  /**
    * @param array is the array to search for a minimum value.
    * @return the minimum value in the array.
    * @author Bob Balliew
    */
  double minArray(double[] array)
  {
    Assert.expect(array != null);
    double min = array[0];

    for (double value : array)
      min = Math.max(min, value);

    return min;
  }

  /**
   * Print out the array.
   * @param pw is the stream to print on.
   * @param array is the array to print.
   * @param name is the name to print as a title.
   * @author Bob Balliew
   */
  void printArray(PrintWriter pw, int[] array, String name)
  {
    Assert.expect(pw != null);
    Assert.expect(array != null);
    Assert.expect(name != null);
    final int rowIndexWidth = (int)Math.log10(Math.max(array.length - 1, 1)) + 1;
    final int maxAbsValue   = Math.max(Math.abs(maxArray(array)), Math.abs(minArray(array)));
    final int valueWidth    = (int)Math.log10(Math.max(maxAbsValue - 1, 1)) + 1;
    final int dataWidth     = 1 + 1 + valueWidth;  // blank space, sign, value.
    final String rowFmt     = "[%" + rowIndexWidth + "d]";
    final String dataFmt    = "%" + dataWidth + "d";
    pw.printf("%s:%n", name);

    for (int i = 0; i < array.length; ++i)
      pw.printf(rowFmt + dataFmt + "%n", i, array[i]);

    pw.printf("%n");
  }

  /**
   * Print out the array.
   * @param pw is the stream to print on.
   * @param array is the array to print.
   * @param name is the name to print as a title.
   * @author Bob Balliew
   */
  void printArray(PrintWriter pw, boolean[] array, String name)
  {
    Assert.expect(pw != null);
    Assert.expect(array != null);
    Assert.expect(name != null);
    final int rowIndexWidth = (int)Math.log10(Math.max(array.length - 1, 1)) + 1;
    final int dataWidth     = 1 + 5;  // blank space, max characters in boolean ("false").
    final String rowFmt     = "[%" + rowIndexWidth + "d]";
    final String dataFmt    = "%" + dataWidth + "b";
    pw.printf("%s:%n", name);

    for (int i = 0; i < array.length; ++i)
      pw.printf(rowFmt + dataFmt + "%n", i, array[i]);

    pw.printf("%n");
  }

  /**
   * Print out the three arrays in three TAB delimited columns (easy for Excel to read).
   * @param pw is the stream to print on.
   * @param width is number othe array to print in the first column.
   * @param array is the array to print in the first column.
   * @author Bob Balliew
   */
  void printZ(PrintWriter pw, int width, double[] array)
  {
    Assert.expect(pw != null);
    Assert.expect(array != null);
    Assert.expect(width > 0);
    Assert.expect((array.length % width) == 0);

    for (int i = 0; i < array.length; ++i)
    {
      pw.printf("%+22.15e", MathUtil.convertUnits(array[i], MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));

      if (((i + 1) % width) == 0)
        pw.printf("%n");
      else
        pw.printf("\t");
    }
  }

  /**
   * Make an array of the specified size with each element initialized to the specified value.
   * @param size the number of elements in the array returned.
   * @param value the initial value to set each element of the array.
   * @return an array.
   * @author Bob Balliew
   */
  private double[] makeArray(int size, double value)
  {
    Assert.expect(size >= 0);
    double[] array = new double[size];

    for (int i = 0; i < size; ++i)
      array[i] = value;

    return array;
  }

  /**
   * Make an array of the specified size with each element initialized to the specified value.
   * @param size the number of elements in the array returned.
   * @param value the initial value to set each element of the array.
   * @return an array.
   * @author Bob Balliew
   */
  private boolean[] makeArray(int size, boolean value)
  {
    Assert.expect(size >= 0);
    boolean[] array = new boolean[size];

    for (int i = 0; i < size; ++i)
      array[i] = value;

    return array;
  }

//  private static Random _randomNumberGenerator = new Random((new Date()).getTime());
  private static Random _randomNumberGenerator = new Random(0);  // always use same RNG seed

  /**
   * Make an array of random size containing random values.
   * @minSize is the smallest size array to return.
   * @minSize is the largest size array to return.
   * @minValue is the smallest value that will be assigned to an element.
   * @maxValue is the largest value that will be assigned to an element.
   * @return a array of random size containing uniformly generated random integer values.
   * @author Bob Balliew
   */
  private int[] makeValues(int minSize, int maxSize, int minValue, int maxValue)
  {
    Assert.expect(minSize >= 0);
    Assert.expect(maxSize >= 0);
    Assert.expect(maxSize >= minSize);
    final long longSizeRange = (long)maxSize - (long)minSize + 1L;
    Assert.expect(longSizeRange <= ((long)Integer.MAX_VALUE));
    Assert.expect(longSizeRange >= 0);
    final int sizeRange = (int) longSizeRange;
    Assert.expect(maxValue >= minValue);
    final long longValueRange = (long)maxValue - (long)minValue + 1L;
    Assert.expect(longValueRange <= ((long)Integer.MAX_VALUE));
    Assert.expect(longValueRange >= 0);
    final int valueRange = (int) longValueRange;
    final int size = _randomNumberGenerator.nextInt(sizeRange) + minSize;
    int[] array = new int[size];

    for (int i = 0; i < size; ++i)
      array[i] = _randomNumberGenerator.nextInt(valueRange) + minValue;

    return array;
  }

  /**
   * Make an array of random size containing random values.
   * @minSize is the smallest size array to return.
   * @minSize is the largest size array to return.
   * @mean is the population mean for the random samples.
   * @sigma is the population standard deviation for the random samples.
   * @return a array of random size containing a normally distribution or random values.
   * @author Bob Balliew
   */
  private double[] makeValues(int minSize, int maxSize, double mean, double sigma)
  {
    Assert.expect(minSize >= 0);
    Assert.expect(maxSize >= 0);
    Assert.expect(maxSize >= minSize);
    final long longSizeRange = (long)maxSize - (long)minSize + 1L;
    Assert.expect(longSizeRange <= ((long)Integer.MAX_VALUE));
    Assert.expect(longSizeRange >= 0);
    final int sizeRange = (int) longSizeRange;
    Assert.expect(sigma >= 0.0);
    final int size = _randomNumberGenerator.nextInt(sizeRange) + minSize;
    double[] array = new double[size];

    for (int i = 0; i < size; ++i)
      array[i] = (sigma * _randomNumberGenerator.nextGaussian()) + mean;

    return array;
  }

  /**
   * Make an array of size booleans with a specified, random fraction true.
   * @size is the number of elements to return
   * @fraction is the fraction which will be randomly set to true
   * @return an array of booleans with specified fraction randomly set true
   * @author John Heumann
   */
  private boolean[] makeValues(int size, double fraction)
  {
    Assert.expect(size >= 0);
    boolean[] array = new boolean[size];
    double temp;

    int threshold = (int)(fraction * 1000000.0);
    for (int i = 0; i < size; ++i)
    {
      temp = _randomNumberGenerator.nextInt(1000001);
      if (temp <= threshold)
        array[i] = true;
      else
        array[i] = false;
    }
    return array;
  }

  /**
   * Make an array of boolean where all elements are false except those in the chanedList.
   * Since the chanedList may be randomly generated and may contain duplicates each time an
   * index is encountered the current state of changed is inverted (not).
   * @size is the size of array of booleans to make.
   * @param changedList an array of indices to flag as changed.
   * @author Bob Balliew
   */
  private boolean[] makeChanged(int size, int[] changedList)
  {
    Assert.expect(size >= 0);
    Assert.expect(changedList != null);
    boolean[] array = makeArray(size, false);

    for (int i : changedList)
      array[i] = (! array[i]);

    return array;
  }

  /**
   * Copy the array.
   * @param array to be copied.
   * @return an array.
   * @author Bob Balliew
   */
  private boolean[] copyArray(boolean[] array)
  {
    Assert.expect(array != null);
    boolean[] c = new boolean[array.length];
    System.arraycopy(array, 0, c, 0, array.length);
    return c;
  }

  /**
   * Copy the array.
   * @param array to be copied.
   * @return an array.
   * @author Bob Balliew
   */
  private double[] copyArray(double[] array)
  {
    Assert.expect(array != null);
    double[] c = new double[array.length];
    System.arraycopy(array, 0, c, 0, array.length);
    return c;
  }

  /**
   * Check the two arrays have matching element values for each element.
   * @param lhs is one array to be compared.
   * @param rhs is another array to be compared.
   * @return an array.
   * @author Bob Balliew
   */
  private void checkArraysMatch(boolean[] lhs, boolean[] rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);
    Assert.expect(lhs != rhs);
    Expect.expect(lhs.length == rhs.length, "lhs.length: " + lhs.length + ", rhs.length: " + rhs.length);
    final int minSize = Math.min(lhs.length, rhs.length);

    for (int i = 0; i < minSize; ++i)
      Expect.expect(lhs[i] == rhs[i], "lhs[" + i + "]: " + lhs[i] + ", rhs[" + i + "]: " + rhs[i]);
  }

  /**
   * Check the two arrays have matching element values for each element.
   * @param lhs is one array to be compared.
   * @param rhs is another array to be compared.
   * @author Bob Balliew
   */
  private void checkArraysMatch(double[] lhs, double[] rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);
    Assert.expect(lhs != rhs);
    Expect.expect(lhs.length == rhs.length, "lhs.length: " + lhs.length + ", rhs.length: " + rhs.length);
    final int minSize = Math.min(lhs.length, rhs.length);

    for (int i = 0; i < minSize; ++i)
      Expect.expect(MathUtil.fuzzyEquals(lhs[i], rhs[i]), "lhs[" + i + "]: " + lhs[i] + ", rhs[" + i + "]: " + rhs[i]);
  }

  /**
   * Check the two arrays are the same length and have at least one differing element.
   * @param lhs is one array to be compared.
   * @param rhs is another array to be compared.
   * @author Bob Balliew
   */
  private void checkArraysDiffer(double[] lhs, double[] rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);
    Assert.expect(lhs != rhs);
    Expect.expect(lhs.length == rhs.length, "lhs.length: " + lhs.length + ", rhs.length: " + rhs.length);
    final int minSize = Math.min(lhs.length, rhs.length);

    for (int i = 0; i < minSize; ++i)
      if (! MathUtil.fuzzyEquals(lhs[i], rhs[i]))
        return;

    Expect.expect(false);
  }

  /**
   * Test countTrue method.
   * @param br input stream from '.input' file for unit test.
   * @param pw output stream to '.expect' file for unit test.
   * @author Bob Balliew
   */
  private void test_countTrue(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_countTrue: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final boolean[] test0 =  {false, false, false};
    final boolean[] test1a = {true,  false, false};
    final boolean[] test1b = {false, true,  false};
    final boolean[] test1c = {false, false, true};
    final boolean[] test2a = {true,  true,  false};
    final boolean[] test2b = {true,  false, true};
    final boolean[] test2c = {false, true,  true};
    final boolean[] test3 =  {true,  true,  true};

    try
    {
      final int x = VerificationSurfaceEstimator.countTrue(null);
      Expect.expect(false, "'countTrue(null)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
    }

    {
      boolean[] v = copyArray(test0);
      final int x = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == 0, "x: " + x);
      checkArraysMatch(v, test0);
    }

    {
      boolean[] v = copyArray(test1a);
      final int x = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == 1, "x: " + x);
      checkArraysMatch(v, test1a);
    }

    {
      boolean[] v = copyArray(test1b);
      final int x = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == 1, "x: " + x);
      checkArraysMatch(v, test1b);
    }

    {
      boolean[] v = copyArray(test1c);
      final int x = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == 1, "x: " + x);
      checkArraysMatch(v, test1c);
    }

    {
      boolean[] v = copyArray(test2a);
      final int x = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == 2, "x: " + x);
      checkArraysMatch(v, test2a);
    }

    {
      boolean[] v = copyArray(test2b);
      final int x = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == 2, "x: " + x);
      checkArraysMatch(v, test2b);
    }

    {
      boolean[] v = copyArray(test2c);
      final int x = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == 2, "x: " + x);
      checkArraysMatch(v, test2c);
    }

    {
      boolean[] v = copyArray(test3);
      final int x = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == 3, "x: " + x);
      checkArraysMatch(v, test3);
    }

    if (debug)  pw.printf("test_countTrue: done%n");
  }

  /**
   * Test swapInvertedPairs method.
   * @param br input stream from '.input' file for unit test.
   * @param pw output stream to '.expect' file for unit test.
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  private void test_swapInvertedPairs(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_swapInvertedPairs: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int nElements      = 6;
    double[] zTop            = {1.0, 2.0, 1.0, 2.0,  2.0, 5.0};
    double[] zTopOriginal    = copyArray(zTop);
    double[] zBottom         = {2.0, 0.0, 2.0, 1.0, -1.0, 5.0};
    double[] zBottomOriginal = copyArray(zBottom);
    double[] zShort          = {4.0};
    double[] zShortOriginal  = copyArray(zShort);
    Assert.expect(zTop.length            == nElements);
    Assert.expect(zTopOriginal.length    == nElements);
    Assert.expect(zBottom.length         == nElements);
    Assert.expect(zBottomOriginal.length == nElements);

    try
    {
      final int nSwaps = VerificationSurfaceEstimator.swapInvertedPairs(null, zBottom);
      Expect.expect(false, "'swapInvertedPairs(null, zBottom)' should assert, nSwaps: " + nSwaps);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zBottom, zBottomOriginal);
    }

    try
    {
      final int nSwaps = VerificationSurfaceEstimator.swapInvertedPairs(zTop, null);
      Expect.expect(false, "'swapInvertedPairs(zTop, null)' should assert, nSwaps: " + nSwaps);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
    }

    try
    {
      final int nSwaps = VerificationSurfaceEstimator.swapInvertedPairs(null, null);
      Expect.expect(false, "'swapInvertedPairs(null, null)' should assert, nSwaps: " + nSwaps);
    }
    catch (AssertException e)
    {
    }

    try
    {
      double[] dupRef = zTop;
      final int nSwaps = VerificationSurfaceEstimator.swapInvertedPairs(zTop, dupRef);
      Expect.expect(false, "'swapInvertedPairs(zTop, dupRef)' should assert, nSwaps: " + nSwaps);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
    }

    try
    {
      final int nSwaps = VerificationSurfaceEstimator.swapInvertedPairs(zShort, zBottom);
      Expect.expect(false, "'swapInvertedPairs(zShort, zBottom)' should assert, nSwaps: " + nSwaps);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zShort,  zShortOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
    }

    try
    {
      final int nSwaps = VerificationSurfaceEstimator.swapInvertedPairs(zTop, zShort);
      Expect.expect(false, "'swapInvertedPairs(zTop, zShort)' should assert, nSwaps: " + nSwaps);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop,   zTopOriginal);
      checkArraysMatch(zShort, zShortOriginal);
    }

    final int nSwaps = VerificationSurfaceEstimator.swapInvertedPairs(zTop, zBottom);
    Expect.expect(nSwaps == 2, "nSwaps: " + nSwaps);

    for (int i = 0; i < zTop.length; i++)
    {
      Expect.expect(zTop[i] >= zBottom[i], "zTop[" + i + "]: " + zTop[i] + ", zBottom[" + i + "]: " + zBottom[i]);
    }

    if (debug)  pw.printf("test_swapInvertedPairs: done%n");
  }

  /**
   * Test cullOutliers method.
   * @param br input stream from '.input' file for unit test.
   * @param pw output stream to '.expect' file for unit test.
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  private void test_cullOutliers(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_cullOutliers: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int nElements          = 3;
    double[] zArray              = {-2.0, 0.0, 2.0};
    double[] zArrayOriginal      = copyArray(zArray);
    double[] zShort              = {0.0};
    double[] zShortOriginal      = copyArray(zShort);
    boolean[] validShort         = {true};
    boolean[] validShortOriginal = copyArray(validShort);
    boolean[] valid_0            = {false, false, false};
    boolean[] valid_1            = {true,  false, false};
    boolean[] valid_2            = {false, true,  false};
    boolean[] valid_4            = {false, false, true};
    boolean[] valid_7            = {true,  true,  true};
    boolean[] valid              = copyArray(valid_7);
    boolean[] validOriginal      = copyArray(valid_7);

    Assert.expect(zArray.length  == nElements);
    Assert.expect(valid_0.length == nElements);
    Assert.expect(valid_1.length == nElements);
    Assert.expect(valid_2.length == nElements);
    Assert.expect(valid_4.length == nElements);
    Assert.expect(valid_7.length == nElements);

    try
    {
      final int x = VerificationSurfaceEstimator.cullOutliers(null, valid, -2.0, +2.0);
      Expect.expect(false, "'cullOutliers(null, valid, -2.0, +2.0)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(valid, validOriginal);
    }

    try
    {
      final int x = VerificationSurfaceEstimator.cullOutliers(zArray, null, -2.0, +2.0);
      Expect.expect(false, "'cullOutliers(zArray, null, -2.0, +2.0)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zArray, zArrayOriginal);
    }

    try
    {
      final int x = VerificationSurfaceEstimator.cullOutliers(null, null, -2.0, +2.0);
      Expect.expect(false, "'cullOutliers(null, null, -2.0, +2.0)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
    }

    try
    {
      final int x = VerificationSurfaceEstimator.cullOutliers(zShort, valid, -2.0, +2.0);
      Expect.expect(false, "'cullOutliers(zShort, valid, -2.0, +2.0)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zShort, zShortOriginal);
      checkArraysMatch(valid, validOriginal);
    }

    try
    {
      final int x = VerificationSurfaceEstimator.cullOutliers(zArray, validShort, -2.0, +2.0);
      Expect.expect(false, "'cullOutliers(zArray, validShort, -2.0, +2.0)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zArray, zArrayOriginal);
      checkArraysMatch(validShort, validShortOriginal);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, +3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, +3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, +3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, +3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, +1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, +1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, +1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, +1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_0);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }


    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, +3.0);
      Expect.expect(x == 1, "x: " + x);
      boolean[] e = {true, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, +3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, +3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, +3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, +1.0);
      Expect.expect(x == 1, "x: " + x);
      boolean[] e = {true, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, +1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, +1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, +1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, -1.0);
      Expect.expect(x == 1, "x: " + x);
      boolean[] e = {true, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_1);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }


    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, +3.0);
      Expect.expect(x == 1, "x: " + x);
      boolean[] e = {false, true, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, +3.0);
      Expect.expect(x == 1, "x: " + x);
      boolean[] e = {false, true, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, +3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, +3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, +1.0);
      Expect.expect(x == 1, "x: " + x);
      boolean[] e = {false, true, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, +1.0);
      Expect.expect(x == 1, "x: " + x);
      boolean[] e = {false, true, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, +1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, +1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_2);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }


    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, +3.0);
      Expect.expect(x == 1, "x: " + x);
      boolean[] e = {false, false, true};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, +3.0);
      Expect.expect(x == 1, "x: " + x);
      boolean[] e = {false, false, true};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, +3.0);
      Expect.expect(x == 1, "x: " + x);
      boolean[] e = {false, false, true};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, +3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, +1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, +1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, +1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, +1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_4);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }


    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, +3.0);
      Expect.expect(x == 3, "x: " + x);
      boolean[] e = {true, true, true};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, +3.0);
      Expect.expect(x == 2, "x: " + x);
      boolean[] e = {false, true, true};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, +3.0);
      Expect.expect(x == 1, "x: " + x);
      boolean[] e = {false, false, true};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, +3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, +1.0);
      Expect.expect(x == 2, "x: " + x);
      boolean[] e = {true, true, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, +1.0);
      Expect.expect(x == 1, "x: " + x);
      boolean[] e = {false, true, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, +1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, +1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, -1.0);
      Expect.expect(x == 1, "x: " + x);
      boolean[] e = {true, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, -1.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -3.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, -1.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +1.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    {
      double[] z = copyArray(zArray);
      boolean[] v = copyArray(valid_7);
      final int x = VerificationSurfaceEstimator.cullOutliers(z, v, +3.0, -3.0);
      Expect.expect(x == 0, "x: " + x);
      boolean[] e = {false, false, false};
      final int n = VerificationSurfaceEstimator.countTrue(v);
      Expect.expect(x == n, "x: " + x + ", n: " + n);
      checkArraysMatch(z, zArray);
      checkArraysMatch(v, e);
    }

    if (debug)  pw.printf("test_cullOutliers: done%n");
  }

  /**
   * Test of combinedValidMedian
   * @param br input stream from '.input' file for unit test.
   * @param pw output stream to '.expect' file for unit test.
   * @author Bob Balliew
   */
  private void test_combinedValidMedian(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_combinedValidMedian: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int nElements1          = 3;
    final int nElements2          = 3;
    double[] array1               = {1.0,   2.0,   3.0};
    double[] array1Original       = copyArray(array1);
    double[] array2               = {4.0,   5.0,   6.0};
    double[] array2Original       = copyArray(array2);
    double[] short1               = {0.0};
    double[] short1Original       = copyArray(short1);
    double[] short2               = {0.0};
    double[] short2Original       = copyArray(short2);
    boolean[] valid1_0            = {false, false, false};
    boolean[] valid1_1            = {true,  false, false};
    boolean[] valid1_2            = {false, true,  false};
    boolean[] valid1_3            = {true,  true,  false};
    boolean[] valid1_4            = {false, false, true};
    boolean[] valid1_5            = {true,  false, true};
    boolean[] valid1_6            = {false, true,  true};
    boolean[] valid1_7            = {true,  true,  true};
    boolean[] valid1              = copyArray(valid1_7);
    boolean[] valid1Original      = copyArray(valid1_7);
    boolean[] valid2_0            = {false, false, false};
    boolean[] valid2_1            = {true,  false, false};
    boolean[] valid2_2            = {false, true,  false};
    boolean[] valid2_3            = {true,  true,  false};
    boolean[] valid2_4            = {false, false, true};
    boolean[] valid2_5            = {true,  false, true};
    boolean[] valid2_6            = {false, true,  true};
    boolean[] valid2_7            = {true,  true,  true};
    boolean[] valid2              = copyArray(valid2_7);
    boolean[] valid2Original      = copyArray(valid2);
    boolean[] validShort1         = {true};
    boolean[] validShort1Original = copyArray(validShort1);
    boolean[] validShort2         = {true};
    boolean[] validShort2Original = copyArray(validShort2);

    Assert.expect(array1.length         == nElements1);
    Assert.expect(array1Original.length == nElements1);
    Assert.expect(valid1_0.length       == nElements1);
    Assert.expect(valid1_1.length       == nElements1);
    Assert.expect(valid1_2.length       == nElements1);
    Assert.expect(valid1_3.length       == nElements1);
    Assert.expect(valid1_4.length       == nElements1);
    Assert.expect(valid1_5.length       == nElements1);
    Assert.expect(valid1_6.length       == nElements1);
    Assert.expect(valid1_7.length       == nElements1);
    Assert.expect(valid1.length         == nElements1);
    Assert.expect(valid1Original.length == nElements1);
    Assert.expect(array2.length         == nElements2);
    Assert.expect(array2Original.length == nElements2);
    Assert.expect(valid2_0.length       == nElements2);
    Assert.expect(valid2_1.length       == nElements2);
    Assert.expect(valid2_2.length       == nElements2);
    Assert.expect(valid2_3.length       == nElements2);
    Assert.expect(valid2_4.length       == nElements2);
    Assert.expect(valid2_5.length       == nElements2);
    Assert.expect(valid2_6.length       == nElements2);
    Assert.expect(valid2_7.length       == nElements2);
    Assert.expect(valid2.length         == nElements1);
    Assert.expect(valid2Original.length == nElements1);

    try
    {
      final double x = VerificationSurfaceEstimator.combinedValidMedian(null, valid1, array2, valid2);
      Expect.expect(false, "'combinedValidMedian(null, valid1, array2, valid2)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(valid1, valid1Original);
      checkArraysMatch(array2, array2Original);
      checkArraysMatch(valid2, valid2Original);
    }

    try
    {
      final double x = VerificationSurfaceEstimator.combinedValidMedian(array1, null, array2, valid2);
      Expect.expect(false, "'combinedValidMedian(array1, null, array2, valid2)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(array1, array1Original);
      checkArraysMatch(array2, array2Original);
      checkArraysMatch(valid2, valid2Original);
    }

    try
    {
      final double x = VerificationSurfaceEstimator.combinedValidMedian(array1, valid1, null, valid2);
      Expect.expect(false, "'combinedValidMedian(array1, valid1, null, valid2)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(array1, array1Original);
      checkArraysMatch(valid1, valid1Original);
      checkArraysMatch(valid2, valid2Original);
    }

    try
    {
      final double x = VerificationSurfaceEstimator.combinedValidMedian(array1, valid1, array2, null);
      Expect.expect(false, "'combinedValidMedian(array1, valid1, array2, null)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(array1, array1Original);
      checkArraysMatch(valid1, valid1Original);
      checkArraysMatch(array2, array2Original);
    }

    try
    {
      double[] dupRef = array1;
      final double x = VerificationSurfaceEstimator.combinedValidMedian(array1, valid1, dupRef, valid2);
      Expect.expect(false, "'combinedValidMedian(array1, valid1, dupRef, valid2)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(array1, array1Original);
      checkArraysMatch(valid1, valid1Original);
      checkArraysMatch(valid2, valid2Original);
    }

    try
    {
      boolean[] dupRef = valid1;
      final double x = VerificationSurfaceEstimator.combinedValidMedian(array1, valid1, array2, dupRef);
      Expect.expect(false, "'combinedValidMedian(array1, valid1, array2, dupRef)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(array1, array1Original);
      checkArraysMatch(valid1, valid1Original);
      checkArraysMatch(array2, array2Original);
    }

    try
    {
      final double x = VerificationSurfaceEstimator.combinedValidMedian(short1, valid1, array2, valid2);
      Expect.expect(false, "'combinedValidMedian(short1, valid1, array2, valid2)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(short1, short1Original);
      checkArraysMatch(valid1, valid1Original);
      checkArraysMatch(array2, array2Original);
      checkArraysMatch(valid2, valid2Original);
    }

    try
    {
      final double x = VerificationSurfaceEstimator.combinedValidMedian(array1, validShort1, array2, valid2);
      Expect.expect(false, "'combinedValidMedian(array1, validShort1, array2, valid2)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(array1, array1Original);
      checkArraysMatch(validShort1, validShort1Original);
      checkArraysMatch(array2, array2Original);
      checkArraysMatch(valid2, valid2Original);
    }

    try
    {
      final double x = VerificationSurfaceEstimator.combinedValidMedian(array1, valid1, short2, valid2);
      Expect.expect(false, "'combinedValidMedian(array1, valid1, short2, valid2)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(array1, array1Original);
      checkArraysMatch(valid1, valid1Original);
      checkArraysMatch(short2, short2Original);
      checkArraysMatch(valid2, valid2Original);
    }

    try
    {
      final double x = VerificationSurfaceEstimator.combinedValidMedian(array1, valid1, array2, validShort2);
      Expect.expect(false, "'combinedValidMedian(array1, valid1, array2, validShort2)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(array1, array1Original);
      checkArraysMatch(valid1, valid1Original);
      checkArraysMatch(array2, array2Original);
      checkArraysMatch(validShort2, validShort2Original);
    }

    try
    {
      final double x = VerificationSurfaceEstimator.combinedValidMedian(array1, valid1, array2, validShort2);
      Expect.expect(false, "'combinedValidMedian(array1, valid1, array2, validShort2)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(array1, array1Original);
      checkArraysMatch(valid1, valid1Original);
      checkArraysMatch(array2, array2Original);
      checkArraysMatch(validShort2, validShort2Original);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_0);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_0);
      try
      {
        final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
        Expect.expect(false, "'combinedValidMedian(a1, v1, a2, v2))' should assert, x: " + x);
      }
      catch (AssertException e)
      {
        checkArraysMatch(a1, array1);
        checkArraysMatch(v1, valid1_0);
        checkArraysMatch(a2, array2);
        checkArraysMatch(v2, valid2_0);
      }
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_0);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_1);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_0);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_1);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_0);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_2);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 5.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_0);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_2);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_0);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_3);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_0);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_3);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_0);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_4);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 6.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_0);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_4);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_0);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_5);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 5.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_0);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_5);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_0);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_6);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 5.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_0);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_6);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_0);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_7);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 5.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_0);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_7);
    }


    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_1);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_0);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 1.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_1);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_0);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_1);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_1);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 2.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_1);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_1);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_1);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_2);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_1);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_2);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_1);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_3);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_1);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_3);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_1);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_4);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_1);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_4);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_1);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_5);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_1);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_5);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_1);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_6);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 5.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_1);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_6);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_1);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_7);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_1);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_7);
    }


    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_2);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_0);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 2.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_2);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_0);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_2);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_1);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_2);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_1);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_2);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_2);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_2);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_2);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_2);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_3);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_2);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_3);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_2);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_4);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_2);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_4);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_2);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_5);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_2);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_5);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_2);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_6);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 5.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_2);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_6);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_2);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_7);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_2);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_7);
    }


    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_3);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_0);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 1.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_3);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_0);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_3);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_1);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 2.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_3);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_1);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_3);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_2);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 2.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_3);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_2);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_3);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_3);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_3);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_3);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_3);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_4);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 2.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_3);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_4);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_3);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_5);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_3);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_5);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_3);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_6);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_3);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_6);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_3);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_7);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_3);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_7);
    }


    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_4);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_0);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_4);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_0);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_4);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_1);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_4);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_1);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_4);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_2);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_4);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_2);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_4);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_3);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_4);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_3);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_4);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_4);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_4);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_4);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_4);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_5);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_4);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_5);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_4);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_6);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 5.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_4);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_6);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_4);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_7);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_4);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_7);
    }


    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_5);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_0);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 2.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_5);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_0);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_5);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_1);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_5);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_1);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_5);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_2);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_5);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_2);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_5);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_3);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_5);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_3);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_5);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_4);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_5);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_4);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_5);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_5);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_5);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_5);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_5);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_6);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_5);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_6);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_5);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_7);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_5);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_7);
    }


    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_6);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_0);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 2.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_6);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_0);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_6);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_1);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_6);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_1);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_6);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_2);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_6);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_2);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_6);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_3);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_6);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_3);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_6);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_4);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_6);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_4);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_6);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_5);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_6);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_5);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_6);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_6);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_6);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_6);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_6);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_7);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 4.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_6);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_7);
    }


    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_7);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_0);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 2.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_7);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_0);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_7);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_1);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 2.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_7);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_1);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_7);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_2);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 2.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_7);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_2);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_7);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_3);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_7);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_3);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_7);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_4);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 2.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_7);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_4);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_7);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_5);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_7);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_5);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_7);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_6);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.0), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_7);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_6);
    }

    {
      double[]  a1 = copyArray(array1);
      boolean[] v1 = copyArray(valid1_7);
      double[]  a2 = copyArray(array2);
      boolean[] v2 = copyArray(valid2_7);
      final double x = VerificationSurfaceEstimator.combinedValidMedian(a1, v1, a2, v2);
      Expect.expect(MathUtil.fuzzyEquals(x, 3.5), "x: " + x);
      checkArraysMatch(a1, array1);
      checkArraysMatch(v1, valid1_7);
      checkArraysMatch(a2, array2);
      checkArraysMatch(v2, valid2_7);
    }

    if (debug)  pw.printf("test_combinedValidMedian: done%n");
  }

  /**
   * Test the constructors of the VerificationSurfaceEstimator class.
   * @param br input stream from '.input' file for unit test.
   * @param pw output stream to '.expect' file for unit test.
   * @author Bob Balliew
   */
  private void test_VerificationSurfaceEstimator(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    String defaultDumpFileName = "C:/temp/dump_chooseVerificationHeight.txt";
    String defaultPrintFileName = "C:/temp/print_chooseVerificationHeight.txt";

    if (debug)  pw.printf("test_VerificationSurfaceEstimator: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    try
    {
      FileUtilAxi.delete(defaultDumpFileName);
    }
    catch (Exception e)
    {
      Expect.expect(false, e.getMessage());
    }

    try
    {
    FileUtilAxi.delete(defaultPrintFileName);
  }
  catch (Exception e)
  {
    Expect.expect(false, e.getMessage());
  }

    try
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
      VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM);
      Expect.expect(vse != null);
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
    }
    catch (AssertException e)
    {
      Expect.expect(false, "'VerificationSurfaceEstimator(width, height, boardThicknessNM)' should not assert.");
    }

    try
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
      VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(0, height, boardThicknessNM);
      Expect.expect(false, "'VerificationSurfaceEstimator(0, height, boardThicknessNM)' should assert.");
    }
    catch (AssertException e)
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
    }

    try
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
      VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, 0, boardThicknessNM);
      Expect.expect(false, "'VerificationSurfaceEstimator(width, 0, boardThicknessNM)' should assert.");
    }
    catch (AssertException e)
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
    }

    try
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
      VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, 0.0);
      Expect.expect(false, "'VerificationSurfaceEstimator(width, height, 0.0)' should assert.");
    }
    catch (AssertException e)
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
    }


    try
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
      VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, false);
      Expect.expect(vse != null);
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
    }
    catch (AssertException e)
    {
      Expect.expect(false, "'VerificationSurfaceEstimator(width, height, boardThicknessNM, false)' should not assert.");
    }

    try
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
      VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(0, height, boardThicknessNM);
      Expect.expect(false, "'VerificationSurfaceEstimator(0, height, boardThicknessNM)' should assert.");
    }
    catch (AssertException e)
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
    }

    try
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
      VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, 0, boardThicknessNM, false);
      Expect.expect(false, "'VerificationSurfaceEstimator(width, 0, boardThicknessNM, false)' should assert.");
    }
    catch (AssertException e)
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
    }

    try
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
      VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, 0.0, false);
      Expect.expect(false, "'VerificationSurfaceEstimator(width, height, 0.0, false)' should assert.");
    }
    catch (AssertException e)
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
    }

    try
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
      VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, true);
      Expect.expect(vse != null);
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
    }
    catch (AssertException e)
    {
      Expect.expect(false, "'VerificationSurfaceEstimator(width, height, boardThicknessNM, true)' should not assert.");
    }

    try
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
      VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(0, height, boardThicknessNM, true);
      Expect.expect(false, "'VerificationSurfaceEstimator(0, height, boardThicknessNM, true)' should assert.");
    }
    catch (AssertException e)
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
    }

    try
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
      VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, 0, boardThicknessNM, true);
      Expect.expect(false, "'VerificationSurfaceEstimator(width, 0, boardThicknessNM, true)' should assert.");
    }
    catch (AssertException e)
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
    }

    try
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
      VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, 0.0, true);
      Expect.expect(false, "'VerificationSurfaceEstimator(width, height, 0.0, true)' should assert.");
    }
    catch (AssertException e)
    {
      Expect.expect(! FileUtilAxi.exists(defaultDumpFileName));
      Expect.expect(! FileUtilAxi.exists(defaultPrintFileName));
    }

    if (debug)  pw.printf("test_VerificationSurfaceEstimator: done%n");
  }

  /**
   * Test of findCloseNeighbors method assert conditions.
   * @param br input stream from '.input' file for unit test.
   * @param pw output stream to '.expect' file for unit test.
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  private void test_findCloseNeighbors_0(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_findCloseNeighbors_1: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width                        = 5;
    final int height                       = 4;
    final int nElements                    = width * height;
    final double boardThicknessNM          = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
    double[] zTop                          = makeArray(nElements, +boardThicknessNM / 2.0);
    double[] zBottom                       = makeArray(nElements, -boardThicknessNM / 2.0);
    double[] zShort                        = makeArray(1, 0.0);
    double[] zTopOriginal                  = copyArray(zTop);
    double[] zBottomOriginal               = copyArray(zBottom);
    double[] zShortOriginal                = copyArray(zShort);
    boolean[] invalidTop                   = makeArray(nElements, false);
    boolean[] invalidBottom                = makeArray(nElements, false);
    boolean[] invalidTopSwapped            = makeArray(nElements, false);
    boolean[] invalidBottomSwapped         = makeArray(nElements, false);
    boolean[] invalidShort                 = makeArray(1, false);
    boolean[] invalidTopOriginal           = copyArray(invalidTop);
    boolean[] invalidBottomOriginal        = copyArray(invalidBottom);
    boolean[] invalidTopSwappedOriginal    = copyArray(invalidTopSwapped);
    boolean[] invalidBottomSwappedOriginal = copyArray(invalidBottomSwapped);
    boolean[] invalidShortOriginal         = copyArray(invalidShort);
    boolean[] validTop                     = makeArray(nElements, true);
    boolean[] validBottom                  = makeArray(nElements, true);
    boolean[] validTopSwapped              = makeArray(nElements, true);
    boolean[] validBottomSwapped           = makeArray(nElements, true);
    boolean[] validShort                   = makeArray(1, true);
    boolean[] validTopOriginal             = copyArray(validTop);
    boolean[] validBottomOriginal          = copyArray(validBottom);
    boolean[] validTopSwappedOriginal      = copyArray(validTopSwapped);
    boolean[] validBottomSwappedOriginal   = copyArray(validBottomSwapped);
    boolean[] validShortOriginal           = copyArray(validShort);
    boolean[] topExcluded                  = makeArray(nElements, false);
    boolean[] bottomExcluded               = makeArray(nElements, false);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    try
    {
      vse.findCloseNeighbors(null, invalidTop, invalidTopSwapped, zBottom, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(null, invalidTop, invalidTopSwapped, zBottom, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidBottom, invalidBottomOriginal);
      checkArraysMatch(invalidBottomSwapped, invalidBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(null, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(null, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
      checkArraysMatch(validBottomSwapped, validBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, null, invalidTopSwapped, zBottom, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, null, invalidTopSwapped, zBottom, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidBottom, invalidBottomOriginal);
      checkArraysMatch(invalidBottomSwapped, invalidBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, null, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, null, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
      checkArraysMatch(validBottomSwapped, validBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, invalidTop, null, zBottom, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidTop, null, zBottom, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidBottom, invalidBottomOriginal);
      checkArraysMatch(invalidBottomSwapped, invalidBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validTop, null, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validTop, null, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
      checkArraysMatch(validBottomSwapped, validBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, null, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, null, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(invalidBottom, invalidBottomOriginal);
      checkArraysMatch(invalidBottomSwapped, invalidBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validTop, validTopSwapped, null, validBottom, validBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validTop, validTopSwapped, null, validBottom, validBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
      checkArraysMatch(validBottomSwapped, validBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, null, invalidBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, null, invalidBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidBottomSwapped, invalidBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, null, validBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, null, validBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottomSwapped, validBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidBottom, null, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidBottom, null, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidBottom, invalidBottomOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, null, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, null, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
    }


    try
    {
      vse.findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zTop, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zTop, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(invalidBottom, invalidBottomOriginal);
      checkArraysMatch(invalidBottomSwapped, invalidBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zTop, validBottom, validBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validTop, validTopSwapped, zTop, validBottom, validBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
      checkArraysMatch(validBottomSwapped, validBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, invalidTop, invalidTop, zBottom, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidTop, invalidTop, zBottom, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidBottom, invalidBottomOriginal);
      checkArraysMatch(invalidBottomSwapped, invalidBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validTop, validTop, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validTop, validTop, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
      checkArraysMatch(validBottomSwapped, validBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidTop, invalidBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidTop, invalidBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidBottomSwapped, invalidBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validTop, validBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validTop, validBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottomSwapped, validBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidBottom, invalidTop, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidBottom, invalidTop, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidBottom, invalidBottomOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validTop, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validTop, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidTopSwapped, invalidBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidTopSwapped, invalidBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidBottomSwapped, invalidBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validTopSwapped, validBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validTopSwapped, validBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottomSwapped, validBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidBottom, invalidTopSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidBottom, invalidTopSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidBottom, invalidBottomOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validTopSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validTopSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidBottom, invalidBottom, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidBottom, invalidBottom, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidBottom, invalidBottomOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottom, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottom, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
    }


    try
    {
      vse.findCloseNeighbors(zShort, invalidTop, invalidTopSwapped, zBottom, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zShort, invalidTop, invalidTopSwapped, zBottom, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zShort, zShortOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidBottom, invalidBottomOriginal);
      checkArraysMatch(invalidBottomSwapped, invalidBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zShort, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zShort, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zShort, zShortOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
      checkArraysMatch(validBottomSwapped, validBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, invalidShort, invalidTopSwapped, zBottom, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidShort, invalidTopSwapped, zBottom, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidShort, invalidShortOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidBottom, invalidBottomOriginal);
      checkArraysMatch(invalidBottomSwapped, invalidBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validShort, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validShort, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validShort, validShortOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
      checkArraysMatch(validBottomSwapped, validBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, invalidTop, invalidShort, zBottom, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidTop, invalidShort, zBottom, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(invalidShort, invalidShortOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidBottom, invalidBottomOriginal);
      checkArraysMatch(invalidBottomSwapped, invalidBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validTop, validShort, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validTop, validShort, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validShort, validShortOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
      checkArraysMatch(validBottomSwapped, validBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zShort, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zShort, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(zShort, zShortOriginal);
      checkArraysMatch(invalidBottom, invalidBottomOriginal);
      checkArraysMatch(invalidBottomSwapped, invalidBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zShort, validBottom, validBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validTop, validTopSwapped, zShort, validBottom, validBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(zShort, zShortOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
      checkArraysMatch(validBottomSwapped, validBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidShort, invalidBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidShort, invalidBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidShort, invalidShortOriginal);
      checkArraysMatch(invalidBottomSwapped, invalidBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validShort, validBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validShort, validBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validShort, validShortOriginal);
      checkArraysMatch(validBottomSwapped, validBottomSwappedOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidBottom, invalidShort, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, invalidTop, invalidTopSwapped, zBottom, invalidBottom, invalidShort, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(invalidTop, invalidTopOriginal);
      checkArraysMatch(invalidTopSwapped, invalidTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(invalidBottom, invalidBottomOriginal);
      checkArraysMatch(invalidShort, invalidShortOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validShort, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validShort, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validTopSwapped, validTopSwappedOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
      checkArraysMatch(validShort, validShortOriginal);
    }

    try
    {
      vse.findCloseNeighbors(zBottom, invalidTop, invalidTopSwapped, zTop, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zBottom, invalidTop, invalidTopSwapped, zTop, invalidBottom, invalidBottomSwapped, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      // DO NOT CHECK boolean arrays since they may have been partially updated prior to encountering the error.
    }

    // Restore boolean arrays in case additional test cases are added.
    invalidTop           = copyArray(invalidTopOriginal);
    invalidTopSwapped    = copyArray(invalidTopSwappedOriginal);
    invalidBottom        = copyArray(invalidBottomOriginal);
    invalidBottomSwapped = copyArray(invalidBottomSwappedOriginal);

    try
    {
      vse.findCloseNeighbors(zBottom, validTop, validTopSwapped, zTop, validBottom, validShort, topExcluded, bottomExcluded);
      Expect.expect(false, "'findCloseNeighbors(zBottom, validTop, validTopSwapped, zTop, validBottom, validShort, topExcluded, bottomExcluded)' should assert.");
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      // DO NOT CHECK boolean arrays since they may have been partially updated prior to encountering the error.
    }

    // Restore boolean arrays in case additional test cases are added.
    validTop           = copyArray(validTopOriginal);
    validTopSwapped    = copyArray(validTopSwappedOriginal);
    validBottom        = copyArray(validBottomOriginal);
    validBottomSwapped = copyArray(validBottomSwappedOriginal);

    if (debug)  pw.printf("test_findCloseNeighbors_1: done%n");
  }

  /**
   * Test of findCloseNeighbors method flat board, no noise in z values, no
   * deviations from board surface.
   * @param br input stream from '.input' file for unit test.
   * @param pw output stream to '.expect' file for unit test.
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  private void test_findCloseNeighbors_1(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_findCloseNeighbors_1: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
    double[] zTop                 = makeArray(nElements, +boardThicknessNM / 2.0);
    double[] zBottom              = makeArray(nElements, -boardThicknessNM / 2.0);
    double[] zTopOriginal         = copyArray(zTop);
    double[] zBottomOriginal      = copyArray(zBottom);
    boolean[] validTop            = new boolean[nElements];
    boolean[] validBottom         = new boolean[nElements];
    boolean[] validTopSwapped     = new boolean[nElements];
    boolean[] validBottomSwapped  = new boolean[nElements];
    boolean[] topExcluded         = makeArray(nElements, false);
    boolean[] bottomExcluded      = makeArray(nElements, false);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);
    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: "           + nValidTop           + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: "    + nValidTopSwapped    + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: "        + nValidBottom        + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: "           + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: "    + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: "        + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: "    + nValidTop    + ", nValidTopSwapped: "    + nValidTopSwapped    + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      final double expectTop    = (+boardThicknessNM / 2.0);
      final double expectBottom = (-boardThicknessNM / 2.0);
      Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
      Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
      Expect.expect(validTop[i],             "i: " + i);
      Expect.expect(! validTopSwapped[i],    "i: " + i);
      Expect.expect(validBottom[i],          "i: " + i);
      Expect.expect(! validBottomSwapped[i], "i: " + i);
    }

    for (int i = 0; i < nElements; ++i)
    {
      validTop[i]           = true;
      validBottom[i]        = true;
      validTopSwapped[i]    = true;
      validBottomSwapped[i] = true;
    }

    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);
    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: "           + nValidTop           + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: "    + nValidTopSwapped    + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: "        + nValidBottom        + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: "           + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: "    + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: "        + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: "    + nValidTop    + ", nValidTopSwapped: "    + nValidTopSwapped    + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      final double expectTop    = (+boardThicknessNM / 2.0);
      final double expectBottom = (-boardThicknessNM / 2.0);
      Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
      Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
      Expect.expect(validTop[i],             "i: " + i);
      Expect.expect(! validTopSwapped[i],    "i: " + i);
      Expect.expect(validBottom[i],          "i: " + i);
      Expect.expect(! validBottomSwapped[i], "i: " + i);
    }

    for (int i = 0; i < nElements; ++i)
    {
      validTop[i]           = false;
      validBottom[i]        = false;
      validTopSwapped[i]    = false;
      validBottomSwapped[i] = false;
    }

    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);
    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: "           + nValidTop           + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: "    + nValidTopSwapped    + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: "        + nValidBottom        + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: "           + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: "    + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: "        + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: "    + nValidTop    + ", nValidTopSwapped: "    + nValidTopSwapped    + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      final double expectTop    = (+boardThicknessNM / 2.0);
      final double expectBottom = (-boardThicknessNM / 2.0);
      Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
      Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
      Expect.expect(validTop[i],             "i: " + i);
      Expect.expect(! validTopSwapped[i],    "i: " + i);
      Expect.expect(validBottom[i],          "i: " + i);
      Expect.expect(! validBottomSwapped[i], "i: " + i);
    }

    if (debug)  pw.printf("test_findCloseNeighbors_1: done%n");
  }

  /**
   * Test of findCloseNeighbors method flat board, no noise in z values, with
   * some z values lifted and dropped too little to remove them from the surface.
   * @param br input stream from '.input' file for unit test.
   * @param pw output stream to '.expect' file for unit test.
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  private void test_findCloseNeighbors_2(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_findCloseNeighbors_2: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
    final double top              = +boardThicknessNM / 2.0;
    final double bottom           = -boardThicknessNM / 2.0;
    final double liftAmount       = 0.5 * (2.0 * (double) VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_DELTA_Z_IN_NANOMETERS);
    final double dropAmount       = 0.5 * (2.0 * (double) VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_DELTA_Z_IN_NANOMETERS);
    double[] zTop                 = makeArray(nElements, top);
    double[] zBottom              = makeArray(nElements, bottom);
    boolean[] topExcluded         = makeArray(nElements, false);
    boolean[] bottomExcluded      = makeArray(nElements, false);

    if (debug)
    {
      VerificationSurfaceEstimator.printArray(pw, zTop, "zTop (preOffset):");
      VerificationSurfaceEstimator.printArray(pw, zBottom, "zBottom (preOffset):");
    }

    int[] iLift                   = {4, 7, 11};  // Lift some top and bottom points,
    offsetPairs(zTop, zBottom, iLift, liftAmount);
    boolean[] lifted              = makeChanged(nElements, iLift);
    int[] iDrop                   = {1, 13};     // Drop some top and bottom points,
    offsetPairs(zTop, zBottom, iDrop, dropAmount);
    boolean[] dropped             = makeChanged(nElements, iDrop);
    double[] zTopOriginal         = copyArray(zTop);
    double[] zBottomOriginal      = copyArray(zBottom);
    boolean[] validTop            = new boolean[nElements];
    boolean[] validBottom         = new boolean[nElements];
    boolean[] validTopSwapped     = new boolean[nElements];
    boolean[] validBottomSwapped  = new boolean[nElements];

    if (debug)
    {
      printArray(pw, iLift, "iLift:");
      printArray(pw, iDrop, "iDrop:");
      VerificationSurfaceEstimator.printArray(pw, zTop, "zTop (postOffset):");
      VerificationSurfaceEstimator.printArray(pw, zBottom, "zBottom (postOffset):");
    }

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);
    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    for (int i = 0; i < nElements; ++i)
    {
      validTop[i]           = true;
      validBottom[i]        = true;
      validTopSwapped[i]    = true;
      validBottomSwapped[i] = true;
    }

    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    for (int i = 0; i < nElements; ++i)
    {
      validTop[i]           = false;
      validBottom[i]        = false;
      validTopSwapped[i]    = false;
      validBottomSwapped[i] = false;
    }

    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    if (debug)  pw.printf("test_findCloseNeighbors_2: done%n");
  }

  /**
   * Test of findCloseNeighbors method flat board, no noise in z values, with
   * some z values lifted and dropped enough to remove them from the surface
   * for same side neighbors but not for opposite side neighbors,
   * but too little to be added to the opposite surface.
   * @param br input stream from '.input' file for unit test.
   * @param pw output stream to '.expect' file for unit test.
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  private void test_findCloseNeighbors_3(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_findCloseNeighbors_3: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
    final double top              = +boardThicknessNM / 2.0;
    final double bottom           = -boardThicknessNM / 2.0;
    final double liftAmount       = (2.0 * (double) VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_DELTA_Z_IN_NANOMETERS) +
                                    (0.5 * (( VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_THICKNESS_CHANGE_PERCENTANGE / 100.0) * boardThicknessNM));
    final double dropAmount       = -((2.0 * (double) VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_DELTA_Z_IN_NANOMETERS) +
                                    (0.5 * ((VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_THICKNESS_CHANGE_PERCENTANGE / 100.0) * boardThicknessNM)));
    double[] zTop                 = makeArray(nElements, top);
    double[] zBottom              = makeArray(nElements, bottom);

    if (debug)
    {
      VerificationSurfaceEstimator.printArray(pw, zTop, "zTop (preOffset):");
      VerificationSurfaceEstimator.printArray(pw, zBottom, "zBottom (preOffset):");
    }

    int[] iLift                   = {4, 7, 11};  // Lift some top and bottom points,
    offsetPairs(zTop, zBottom, iLift, liftAmount);
    boolean[] lifted              = makeChanged(nElements, iLift);
    int[] iDrop                   = {1, 13};     // Drop some top and bottom points,
    offsetPairs(zTop, zBottom, iDrop, dropAmount);
    boolean[] dropped             = makeChanged(nElements, iDrop);
    double[] zTopOriginal         = copyArray(zTop);
    double[] zBottomOriginal      = copyArray(zBottom);
    boolean[] validTop            = new boolean[nElements];
    boolean[] validBottom         = new boolean[nElements];
    boolean[] validTopSwapped     = new boolean[nElements];
    boolean[] validBottomSwapped  = new boolean[nElements];
    boolean[] topExcluded         = makeArray(nElements, false);
    boolean[] bottomExcluded      = makeArray(nElements, false);

    if (debug)
    {
      printArray(pw, iLift, "iLift:");
      printArray(pw, iDrop, "iDrop:");
      VerificationSurfaceEstimator.printArray(pw, zTop, "zTop (postOffset):");
      VerificationSurfaceEstimator.printArray(pw, zBottom, "zBottom (postOffset):");
    }

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);
    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    for (int i = 0; i < nElements; ++i)
    {
      validTop[i]           = true;
      validBottom[i]        = true;
      validTopSwapped[i]    = true;
      validBottomSwapped[i] = true;
    }

    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    for (int i = 0; i < nElements; ++i)
    {
      validTop[i]           = false;
      validBottom[i]        = false;
      validTopSwapped[i]    = false;
      validBottomSwapped[i] = false;
    }

    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    if (debug)  pw.printf("test_findCloseNeighbors_3: done%n");
  }

  /**
   * Test of findCloseNeighbors method flat board, no noise in z values, with
   * some z values lifted and dropped enough to remove them from the surface,
   * but too little to be added to the opposite surface.
   * @param br input stream from '.input' file for unit test.
   * @param pw output stream to '.expect' file for unit test.
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  private void test_findCloseNeighbors_4(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_findCloseNeighbors_4: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
    final double top              = +boardThicknessNM / 2.0;
    final double bottom           = -boardThicknessNM / 2.0;
    final double liftAmount       = (2.0 * (double) VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_DELTA_Z_IN_NANOMETERS) +
                                    (2.0 * (( VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_THICKNESS_CHANGE_PERCENTANGE / 100.0) * boardThicknessNM));
    final double dropAmount       = -((2.0 * (double) VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_DELTA_Z_IN_NANOMETERS) +
                                    (2.0 * (( VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_THICKNESS_CHANGE_PERCENTANGE / 100.0) * boardThicknessNM)));
    double[] zTop                 = makeArray(nElements, top);
    double[] zBottom              = makeArray(nElements, bottom);

    if (debug)
    {
      VerificationSurfaceEstimator.printArray(pw, zTop, "zTop (preOffset):");
      VerificationSurfaceEstimator.printArray(pw, zBottom, "zBottom (preOffset):");
    }

    int[] iLift                   = {4, 7, 11};  // Lift some top and bottom points,
    offsetPairs(zTop, zBottom, iLift, liftAmount);
    boolean[] lifted              = makeChanged(nElements, iLift);
    int[] iDrop                   = {1, 13};     // Drop some top and bottom points,
    offsetPairs(zTop, zBottom, iDrop, dropAmount);
    boolean[] dropped             = makeChanged(nElements, iDrop);
    double[] zTopOriginal         = copyArray(zTop);
    double[] zBottomOriginal      = copyArray(zBottom);
    boolean[] validTop            = new boolean[nElements];
    boolean[] validBottom         = new boolean[nElements];
    boolean[] validTopSwapped     = new boolean[nElements];
    boolean[] validBottomSwapped  = new boolean[nElements];
    boolean[] topExcluded         = makeArray(nElements, false);
    boolean[] bottomExcluded      = makeArray(nElements, false);

    if (debug)
    {
      printArray(pw, iLift, "iLift:");
      printArray(pw, iDrop, "iDrop:");
      VerificationSurfaceEstimator.printArray(pw, zTop, "zTop (postOffset):");
      VerificationSurfaceEstimator.printArray(pw, zBottom, "zBottom (postOffset):");
    }

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);
    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    for (int i = 0; i < nElements; ++i)
    {
      validTop[i]           = true;
      validBottom[i]        = true;
      validTopSwapped[i]    = true;
      validBottomSwapped[i] = true;
    }

    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    for (int i = 0; i < nElements; ++i)
    {
      validTop[i]           = false;
      validBottom[i]        = false;
      validTopSwapped[i]    = false;
      validBottomSwapped[i] = false;
    }

    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    if (debug)  pw.printf("test_findCloseNeighbors_4: done%n");
  }

  /**
   * Test of findCloseNeighbors method flat board, no noise in z values, with
   * some z values lifted and dropped enough to put them on the opposite surface.
   * @param br input stream from '.input' file for unit test.
   * @param pw output stream to '.expect' file for unit test.
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  private void test_findCloseNeighbors_5(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_findCloseNeighbors_5: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
    final double top              = +boardThicknessNM / 2.0;
    final double bottom           = -boardThicknessNM / 2.0;
    final double liftAmount       = boardThicknessNM;
    final double dropAmount       = -boardThicknessNM;
    double[] zTop                 = makeArray(nElements, top);
    double[] zBottom              = makeArray(nElements, bottom);

    if (debug)
    {
      VerificationSurfaceEstimator.printArray(pw, zTop, "zTop (preOffset):");
      VerificationSurfaceEstimator.printArray(pw, zBottom, "zBottom (preOffset):");
    }

    int[] iLift                   = {7, 11};  // Lift some top and bottom points,
    offsetPairs(zTop, zBottom, iLift, liftAmount);
    boolean[] lifted              = makeChanged(nElements, iLift);
    int[] iDrop                   = {6, 13};  // Drop some top and bottom points,
    offsetPairs(zTop, zBottom, iDrop, dropAmount);
    boolean[] dropped             = makeChanged(nElements, iDrop);
    double[] zTopOriginal         = copyArray(zTop);
    double[] zBottomOriginal      = copyArray(zBottom);
    boolean[] validTop            = new boolean[nElements];
    boolean[] validBottom         = new boolean[nElements];
    boolean[] validTopSwapped     = new boolean[nElements];
    boolean[] validBottomSwapped  = new boolean[nElements];
    boolean[] topExcluded         = makeArray(nElements, false);
    boolean[] bottomExcluded      = makeArray(nElements, false);

    if (debug)
    {
      printArray(pw, iLift, "iLift:");
      printArray(pw, iDrop, "iDrop:");
      VerificationSurfaceEstimator.printArray(pw, zTop, "zTop (postOffset):");
      VerificationSurfaceEstimator.printArray(pw, zBottom, "zBottom (postOffset):");
    }

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);
    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    for (int i = 0; i < nElements; ++i)
    {
      validTop[i]           = true;
      validBottom[i]        = true;
      validTopSwapped[i]    = true;
      validBottomSwapped[i] = true;
    }

    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    for (int i = 0; i < nElements; ++i)
    {
      validTop[i]           = false;
      validBottom[i]        = false;
      validTopSwapped[i]    = false;
      validBottomSwapped[i] = false;
    }

    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    if (debug)  pw.printf("test_findCloseNeighbors_5: done%n");
  }

  /**
   * Test of findCloseNeighbors method flat board, no noise in z values, with
   * some z values lifted and dropped enough to put them on the opposite surface.
   * @param br input stream from '.input' file for unit test.
   * @param pw output stream to '.expect' file for unit test.
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  private void test_findCloseNeighbors_6(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_findCloseNeighbors_6: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
    final double top              = +boardThicknessNM / 2.0;
    final double bottom           = -boardThicknessNM / 2.0;
    final double liftAmount       =   boardThicknessNM + (0.5 * (2.0 * (double) VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_DELTA_Z_IN_NANOMETERS));
    final double dropAmount       = -(boardThicknessNM + (0.5 * (2.0 * (double) VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_DELTA_Z_IN_NANOMETERS)));
    double[] zTop                 = makeArray(nElements, top);
    double[] zBottom              = makeArray(nElements, bottom);

    if (debug)
    {
      VerificationSurfaceEstimator.printArray(pw, zTop, "zTop (preOffset):");
      VerificationSurfaceEstimator.printArray(pw, zBottom, "zBottom (preOffset):");
    }

    int[] iLift                   = {7, 11};  // Lift some top and bottom points,
    offsetPairs(zTop, zBottom, iLift, liftAmount);
    boolean[] lifted              = makeChanged(nElements, iLift);
    int[] iDrop                   = {6, 13};  // Drop some top and bottom points,
    offsetPairs(zTop, zBottom, iDrop, dropAmount);
    boolean[] dropped             = makeChanged(nElements, iDrop);
    double[] zTopOriginal         = copyArray(zTop);
    double[] zBottomOriginal      = copyArray(zBottom);
    boolean[] validTop            = new boolean[nElements];
    boolean[] validBottom         = new boolean[nElements];
    boolean[] validTopSwapped     = new boolean[nElements];
    boolean[] validBottomSwapped  = new boolean[nElements];
    boolean[] topExcluded         = makeArray(nElements, false);
    boolean[] bottomExcluded      = makeArray(nElements, false);

    if (debug)
    {
      printArray(pw, iLift, "iLift:");
      printArray(pw, iDrop, "iDrop:");
      VerificationSurfaceEstimator.printArray(pw, zTop, "zTop (postOffset):");
      VerificationSurfaceEstimator.printArray(pw, zBottom, "zBottom (postOffset):");
    }

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);
    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    for (int i = 0; i < nElements; ++i)
    {
      validTop[i]           = true;
      validBottom[i]        = true;
      validTopSwapped[i]    = true;
      validBottomSwapped[i] = true;
    }

    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    for (int i = 0; i < nElements; ++i)
    {
      validTop[i]           = false;
      validBottom[i]        = false;
      validTopSwapped[i]    = false;
      validBottomSwapped[i] = false;
    }

    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    if (debug)  pw.printf("test_findCloseNeighbors_6: done%n");
  }

  /**
   * Test of findCloseNeighbors method flat board, no noise in z values, with
   * some z values lifted and dropped enough to put them on the opposite surface.
   * @param br input stream from '.input' file for unit test.
   * @param pw output stream to '.expect' file for unit test.
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  private void test_findCloseNeighbors_7(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_findCloseNeighbors_7: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
    final double top              = +boardThicknessNM / 2.0;
    final double bottom           = -boardThicknessNM / 2.0;
    final double liftAmount       =   boardThicknessNM + (2.0 * (2.0 * (double) VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_DELTA_Z_IN_NANOMETERS));
    final double dropAmount       = -(boardThicknessNM + (2.0 * (2.0 * (double) VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_DELTA_Z_IN_NANOMETERS)));
    double[] zTop                 = makeArray(nElements, top);
    double[] zBottom              = makeArray(nElements, bottom);

    if (debug)
    {
      VerificationSurfaceEstimator.printArray(pw, zTop, "zTop (preOffset):");
      VerificationSurfaceEstimator.printArray(pw, zBottom, "zBottom (preOffset):");
    }

    int[] iLift                   = {7, 11};  // Lift some top and bottom points,
    offsetPairs(zTop, zBottom, iLift, liftAmount);
    boolean[] lifted              = makeChanged(nElements, iLift);
    int[] iDrop                   = {6, 13};  // Drop some top and bottom points,
    offsetPairs(zTop, zBottom, iDrop, dropAmount);
    boolean[] dropped             = makeChanged(nElements, iDrop);
    double[] zTopOriginal         = copyArray(zTop);
    double[] zBottomOriginal      = copyArray(zBottom);
    boolean[] validTop            = new boolean[nElements];
    boolean[] validBottom         = new boolean[nElements];
    boolean[] validTopSwapped     = new boolean[nElements];
    boolean[] validBottomSwapped  = new boolean[nElements];
    boolean[] topExcluded         = makeArray(nElements, false);
    boolean[] bottomExcluded      = makeArray(nElements, false);

    if (debug)
    {
      printArray(pw, iLift, "iLift:");
      printArray(pw, iDrop, "iDrop:");
      VerificationSurfaceEstimator.printArray(pw, zTop, "zTop (postOffset):");
      VerificationSurfaceEstimator.printArray(pw, zBottom, "zBottom (postOffset):");
    }

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);
    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    for (int i = 0; i < nElements; ++i)
    {
      validTop[i]           = true;
      validBottom[i]        = true;
      validTopSwapped[i]    = true;
      validBottomSwapped[i] = true;
    }

    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    for (int i = 0; i < nElements; ++i)
    {
      validTop[i]           = false;
      validBottom[i]        = false;
      validTopSwapped[i]    = false;
      validBottomSwapped[i] = false;
    }

    vse.findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);

    if (debug)
    {
      printArray(pw, validTop, "validTop:");
      printArray(pw, validTopSwapped, "validTopSwapped:");
      printArray(pw, validBottom, "validBottom:");
      printArray(pw, validBottomSwapped, "validBottomSwapped:");
    }

    checkArraysMatch(zTop,    zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);
    {
      final int nValidTop           = VerificationSurfaceEstimator.countTrue(validTop);
      final int nValidTopSwapped    = VerificationSurfaceEstimator.countTrue(validTopSwapped);
      final int nValidBottom        = VerificationSurfaceEstimator.countTrue(validBottom);
      final int nValidBottomSwapped = VerificationSurfaceEstimator.countTrue(validBottomSwapped);
      Expect.expect(nValidTop           <= nElements, "nValidTop: " + nValidTop + ", nElements: " + nElements);
      Expect.expect(nValidTopSwapped    <= nElements, "nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect(nValidBottom        <= nElements, "nValidBottom: " + nValidBottom + ", nElements: " + nElements);
      Expect.expect(nValidBottomSwapped <= nElements, "nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
      Expect.expect(nValidTop           >= 0, "nValidTop: " + nValidTop);
      Expect.expect(nValidTopSwapped    >= 0, "nValidTopSwapped: " + nValidTopSwapped);
      Expect.expect(nValidBottom        >= 0, "nValidBottom: " + nValidBottom);
      Expect.expect(nValidBottomSwapped >= 0, "nValidBottomSwapped: " + nValidBottomSwapped);
      Expect.expect((nValidTop    + nValidTopSwapped)    <= nElements, "nValidTop: " + nValidTop + ", nValidTopSwapped: " + nValidTopSwapped + ", nElements: " + nElements);
      Expect.expect((nValidBottom + nValidBottomSwapped) <= nElements, "nValidBottom: " + nValidBottom + ", nValidBottomSwapped: " + nValidBottomSwapped + ", nElements: " + nElements);
    }

    for (int i = 0; i < nElements; ++i)
    {
      if (lifted[i] && dropped[i])
      {
        final double expectTop    = top    + liftAmount + dropAmount;
        final double expectBottom = bottom + liftAmount + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      if (dropped[i])
      {
        final double expectTop    = top    + dropAmount;
        final double expectBottom = bottom + dropAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else if (lifted[i])
      {
        final double expectTop    = top    + liftAmount;
        final double expectBottom = bottom + liftAmount;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(! validTop[i],           "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(! validBottom[i],        "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
      else
      {
        final double expectTop    = top;
        final double expectBottom = bottom;
        Expect.expect(MathUtil.fuzzyEquals(zTop[i],    expectTop),    "zTop["    + i + "]: " + zTop[i]    + ", expectTop: "    + expectTop);
        Expect.expect(MathUtil.fuzzyEquals(zBottom[i], expectBottom), "zBottom[" + i + "]: " + zBottom[i] + ", expectBottom: " + expectBottom);
        Expect.expect(validTop[i],             "i: " + i);
        Expect.expect(! validTopSwapped[i],    "i: " + i);
        Expect.expect(validBottom[i],          "i: " + i);
        Expect.expect(! validBottomSwapped[i], "i: " + i);
      }
    }

    if (debug)  pw.printf("test_findCloseNeighbors_7: done%n");
  }

  /**
   * Test of findCloseNeighbors
   * @author Bob Balliew
   */
  private void test_findCloseNeighbors(BufferedReader br, PrintWriter pw)
  {
    Assert.expect(br != null);
    Assert.expect(pw != null);
    test_findCloseNeighbors_0(br, pw);
    test_findCloseNeighbors_1(br, pw);
    test_findCloseNeighbors_2(br, pw);
    test_findCloseNeighbors_3(br, pw);
    test_findCloseNeighbors_4(br, pw);
    test_findCloseNeighbors_5(br, pw);
    test_findCloseNeighbors_6(br, pw);
    test_findCloseNeighbors_7(br, pw);
  }

  private void test_refineBoardThickness_0(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_0: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top                   = +boardThicknessNM / 2.0;
    final double bottom                = -boardThicknessNM / 2.0;
    double[] zTop                      = makeArray(nElements, top);
    double[] zBottom                   = makeArray(nElements, bottom);
    double[] zTopShort                 = makeArray(1, +boardThicknessNM / 2.0);
    double[] zBottomShort              = makeArray(1, -boardThicknessNM / 2.0);
    boolean[] validTop                 = makeArray(nElements, true);
    boolean[] validBottom              = makeArray(nElements, true);
    boolean[] validTopShort            = makeArray(1, true);
    boolean[] validBottomShort         = makeArray(1, true);
    double[] zTopOriginal              = copyArray(zTop);
    double[] zBottomOriginal           = copyArray(zBottom);
    double[] zTopShortOriginal         = copyArray(zTopShort);
    double[] zBottomShortOriginal      = copyArray(zBottomShort);
    boolean[] validTopOriginal         = copyArray(validTop);
    boolean[] validBottomOriginal      = copyArray(validBottom);
    boolean[] validTopShortOriginal    = copyArray(validTopShort);
    boolean[] validBottomShortOriginal = copyArray(validBottomShort);
    double topMedian                   = top;
    double bottomMedian                = bottom;

    try
    {
      double x = vse.refineBoardThickness(null, validTop, zBottom, validBottom, topMedian, bottomMedian);
      Expect.expect(false, "'refineBoardThickness(null, validTop, zBottom, validBottom, topMedian, bottomMedian)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
    }

    try
    {
      double x = vse.refineBoardThickness(zTop, null, zBottom, validBottom, topMedian, bottomMedian);
      Expect.expect(false, "'refineBoardThickness(zTop, null, zBottom, validBottom, topMedian, bottomMedian)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
    }

    try
    {
      double x = vse.refineBoardThickness(zTop, validTop, null, validBottom, topMedian, bottomMedian);
      Expect.expect(false, "'refineBoardThickness(zTop, validTop, null, validBottom, topMedian, bottomMedian)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
    }

    try
    {
      double x = vse.refineBoardThickness(zTop, validTop, zBottom, null, topMedian, bottomMedian);
      Expect.expect(false, "'refineBoardThickness(zTop, validTop, zBottom, null, topMedian, bottomMedian)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
    }

    try
    {
      double[] dupRef = zTop;
      double x = vse.refineBoardThickness(zTop, validTop, dupRef, validBottom, topMedian, bottomMedian);
      Expect.expect(false, "'refineBoardThickness(zTop, validTop, dupRef, validBottom, topMedian, bottomMedian)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
    }

    try
    {
      boolean[] dupRef = validTop;
      double x = vse.refineBoardThickness(zTop, validTop, zBottom, dupRef, topMedian, bottomMedian);
      Expect.expect(false, "'refineBoardThickness(zTop, validTop, zBottom, dupRef, topMedian, bottomMedian)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
    }

    try
    {
      double x = vse.refineBoardThickness(zTopShort, validTop, zBottom, validBottom, topMedian, bottomMedian);
      Expect.expect(false, "'refineBoardThickness(zTopShort, validTop, zBottom, validBottom, topMedian, bottomMedian)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTopShort, zTopShortOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
    }

    try
    {
      double x = vse.refineBoardThickness(zTop, validTopShort, zBottom, validBottom, topMedian, bottomMedian);
      Expect.expect(false, "'refineBoardThickness(zTop, validTopShort, zBottom, validBottom, topMedian, bottomMedian)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTopShort, validTopShortOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
    }

    try
    {
      double x = vse.refineBoardThickness(zTop, validTop, zBottomShort, validBottom, topMedian, bottomMedian);
      Expect.expect(false, "'refineBoardThickness(zTop, validTop, zBottomShort, validBottom, topMedian, bottomMedian)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(zBottomShort, zBottomShortOriginal);
      checkArraysMatch(validBottom, validBottomOriginal);
    }

    try
    {
      double x = vse.refineBoardThickness(zTop, validTop, zBottom, validBottomShort, topMedian, bottomMedian);
      Expect.expect(false, "'refineBoardThickness(zTop, validTop, zBottom, validBottomShort, topMedian, bottomMedian)' should assert, x: " + x);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(validTop, validTopOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
      checkArraysMatch(validBottomShort, validBottomShortOriginal);
    }

    if (debug)  pw.printf("test_refineBoardThickness_0: done%n");
  }

  /**
   * Test of refineBoardThickness, using average of local thicknesses, all points valid, thicker board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_1(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_1: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 + (2.0 * maxDeltaThickness);
    final double underFactor       = 1.0 + (0.5 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top * underFactor);
    double[] zBottom               = makeArray(nElements, bottom * underFactor);
    boolean[] validTop             = makeArray(nElements, true);
    boolean[] validBottom          = makeArray(nElements, true);
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top * overFactor;
    double bottomMedian            = bottom * overFactor;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), (top * underFactor) - (bottom * underFactor)));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_1: done%n");
  }

  /**
   * Test of refineBoardThickness, using differences of medians (global thickness), all points valid, thicker board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_2(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_2: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 + (2.0 * maxDeltaThickness);
    final double underFactor       = 1.0 + (0.5 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top * overFactor);
    double[] zBottom               = makeArray(nElements, bottom * overFactor);
    boolean[] validTop             = makeArray(nElements, true);
    boolean[] validBottom          = makeArray(nElements, true);
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top * underFactor;
    double bottomMedian            = bottom * underFactor;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), (top * underFactor) - (bottom * underFactor)));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_2: done%n");
  }

  /**
   * Test of refineBoardThickness using nominal board thickness, all points valid, thicker board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_3(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_3: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 + (2.0 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top * overFactor);
    double[] zBottom               = makeArray(nElements, bottom * overFactor);
    boolean[] validTop             = makeArray(nElements, true);
    boolean[] validBottom          = makeArray(nElements, true);
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top * overFactor;
    double bottomMedian            = bottom * overFactor;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), boardThicknessNM));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_3: done%n");
  }

  /**
   * Test of refineBoardThickness, using average of local thicknesses, all points valid, thinner board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_4(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_4: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 - (2.0 * maxDeltaThickness);
    final double underFactor       = 1.0 - (0.5 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top * underFactor);
    double[] zBottom               = makeArray(nElements, bottom * underFactor);
    boolean[] validTop             = makeArray(nElements, true);
    boolean[] validBottom          = makeArray(nElements, true);
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top * overFactor;
    double bottomMedian            = bottom * overFactor;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), (top * underFactor) - (bottom * underFactor)));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_4: done%n");
  }

  /**
   * Test of refineBoardThickness, using differences of medians (global thickness), all points valid, thinner board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_5(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_5: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 - (2.0 * maxDeltaThickness);
    final double underFactor       = 1.0 - (0.5 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top * overFactor);
    double[] zBottom               = makeArray(nElements, bottom * overFactor);
    boolean[] validTop             = makeArray(nElements, true);
    boolean[] validBottom          = makeArray(nElements, true);
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top * underFactor;
    double bottomMedian            = bottom * underFactor;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), (top * underFactor) - (bottom * underFactor)));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_5: done%n");
  }

  /**
   * Test of refineBoardThickness using nominal board thickness, all points valid, thinner board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_6(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_6: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 - (2.0 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top * overFactor);
    double[] zBottom               = makeArray(nElements, bottom * overFactor);
    boolean[] validTop             = makeArray(nElements, true);
    boolean[] validBottom          = makeArray(nElements, true);
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top * overFactor;
    double bottomMedian            = bottom * overFactor;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), boardThicknessNM));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_6: done%n");
  }

  /**
   * Test of refineBoardThickness, using average of local thicknesses, one point valid, thicker board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_7(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_7: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 + (2.0 * maxDeltaThickness);
    final double underFactor       = 1.0 + (0.5 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top * overFactor);
    double[] zBottom               = makeArray(nElements, bottom * overFactor);
    boolean[] validTop             = makeArray(nElements, false);
    boolean[] validBottom          = makeArray(nElements, false);
    validTop[0]                    = true;
    validBottom[1]                 = true;
    validTop[2]                    = true;
    validBottom[2]                 = true;
    zTop[2]                        = top * underFactor;
    zBottom[2]                     = bottom * underFactor;
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top;
    double bottomMedian            = bottom;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), (top * underFactor) - (bottom * underFactor)));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_7: done%n");
  }

  /**
   * Test of refineBoardThickness, using differences of medians (global thickness), one point valid, thicker board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_8(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_8: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 + (2.0 * maxDeltaThickness);
    final double underFactor       = 1.0 + (0.5 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top);
    double[] zBottom               = makeArray(nElements, bottom);
    boolean[] validTop             = makeArray(nElements, false);
    boolean[] validBottom          = makeArray(nElements, false);
    validTop[0]                    = true;
    validBottom[1]                 = true;
    validTop[2]                    = true;
    validBottom[2]                 = true;
    zTop[2]                        = top * overFactor;
    zBottom[2]                     = bottom * overFactor;
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top * underFactor;
    double bottomMedian            = bottom * underFactor;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), (top * underFactor) - (bottom * underFactor)));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_8: done%n");
  }

  /**
   * Test of refineBoardThickness using nominal board thickness, one point valid, thicker board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_9(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_9: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 + (2.0 * maxDeltaThickness);
    final double underFactor       = 1.0 + (0.5 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top * underFactor);
    double[] zBottom               = makeArray(nElements, bottom * underFactor);
    boolean[] validTop             = makeArray(nElements, false);
    boolean[] validBottom          = makeArray(nElements, false);
    validTop[0]                    = true;
    validBottom[1]                 = true;
    validTop[2]                    = true;
    validBottom[2]                 = true;
    zTop[2]                        = top * overFactor;
    zBottom[2]                     = bottom * overFactor;
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top * overFactor;
    double bottomMedian            = bottom * overFactor;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), boardThicknessNM));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_9: done%n");
  }

  /**
   * Test of refineBoardThickness, using average of local thicknesses, one point valid, thinner board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_10(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_10: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 - (2.0 * maxDeltaThickness);
    final double underFactor       = 1.0 - (0.5 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top * overFactor);
    double[] zBottom               = makeArray(nElements, bottom * overFactor);
    boolean[] validTop             = makeArray(nElements, false);
    boolean[] validBottom          = makeArray(nElements, false);
    validTop[0]                    = true;
    validBottom[1]                 = true;
    validTop[2]                    = true;
    validBottom[2]                 = true;
    zTop[2]                        = top * underFactor;
    zBottom[2]                     = bottom * underFactor;
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top;
    double bottomMedian            = bottom;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), (top * underFactor) - (bottom * underFactor)));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_10: done%n");
  }

  /**
   * Test of refineBoardThickness, using differences of medians (global thickness), one point valid, thinner board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_11(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_11: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 - (2.0 * maxDeltaThickness);
    final double underFactor       = 1.0 - (0.5 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top);
    double[] zBottom               = makeArray(nElements, bottom);
    boolean[] validTop             = makeArray(nElements, false);
    boolean[] validBottom          = makeArray(nElements, false);
    validTop[0]                    = true;
    validBottom[1]                 = true;
    validTop[2]                    = true;
    validBottom[2]                 = true;
    zTop[2]                        = top * overFactor;
    zBottom[2]                     = bottom * overFactor;
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top * underFactor;
    double bottomMedian            = bottom * underFactor;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), (top * underFactor) - (bottom * underFactor)));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_11: done%n");
  }

  /**
   * Test of refineBoardThickness using nominal board thickness, one point valid, thinner board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_12(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_12: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 - (2.0 * maxDeltaThickness);
    final double underFactor       = 1.0 - (0.5 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top * underFactor);
    double[] zBottom               = makeArray(nElements, bottom * underFactor);
    boolean[] validTop             = makeArray(nElements, false);
    boolean[] validBottom          = makeArray(nElements, false);
    validTop[0]                    = true;
    validBottom[1]                 = true;
    validTop[2]                    = true;
    validBottom[2]                 = true;
    zTop[2]                        = top * overFactor;
    zBottom[2]                     = bottom * overFactor;
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top * overFactor;
    double bottomMedian            = bottom * overFactor;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), boardThicknessNM));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_12: done%n");
  }

  /**
   * Test of refineBoardThickness, using differences of medians (global thickness), zero points valid, thicker board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_13(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_13: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 + (2.0 * maxDeltaThickness);
    final double underFactor       = 1.0 + (0.5 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top * overFactor);
    double[] zBottom               = makeArray(nElements, bottom * overFactor);
    boolean[] validTop             = makeArray(nElements, false);
    boolean[] validBottom          = makeArray(nElements, false);
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top * underFactor;
    double bottomMedian            = bottom * underFactor;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), (top * underFactor) - (bottom * underFactor)));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_13: done%n");
  }

  /**
   * Test of refineBoardThickness using nominal board thickness, zero points valid, thicker board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_14(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_14: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 + (2.0 * maxDeltaThickness);
    final double underFactor       = 1.0 + (0.5 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top * underFactor);
    double[] zBottom               = makeArray(nElements, bottom * underFactor);
    boolean[] validTop             = makeArray(nElements, false);
    boolean[] validBottom          = makeArray(nElements, false);
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top * overFactor;
    double bottomMedian            = bottom * overFactor;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), boardThicknessNM));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_14: done%n");
  }

  /**
   * Test of refineBoardThickness, using differences of medians (global thickness), zero points valid, thinner board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_15(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_15: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 - (2.0 * maxDeltaThickness);
    final double underFactor       = 1.0 - (0.5 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top * overFactor);
    double[] zBottom               = makeArray(nElements, bottom * overFactor);
    boolean[] validTop             = makeArray(nElements, false);
    boolean[] validBottom          = makeArray(nElements, false);
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top * underFactor;
    double bottomMedian            = bottom * underFactor;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), (top * underFactor) - (bottom * underFactor)));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_15: done%n");
  }

  /**
   * Test of refineBoardThickness using nominal board thickness, zero points valid, thinner board.
   * @author Bob Balliew
   */
  private void test_refineBoardThickness_16(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_refineBoardThickness_16: started%n");

    Assert.expect(br != null);
    Assert.expect(pw != null);
    final int width               = 5;
    final int height              = 4;
    final int nElements           = width * height;
    final double boardThicknessNM = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    final double top               = +boardThicknessNM / 2.0;
    final double bottom            = -boardThicknessNM / 2.0;
    final double maxDeltaThickness = VerificationSurfaceEstimator.PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;
    final double overFactor        = 1.0 - (2.0 * maxDeltaThickness);
    final double underFactor       = 1.0 - (0.5 * maxDeltaThickness);
    double[] zTop                  = makeArray(nElements, top * underFactor);
    double[] zBottom               = makeArray(nElements, bottom * underFactor);
    boolean[] validTop             = makeArray(nElements, false);
    boolean[] validBottom          = makeArray(nElements, false);
    double[] zTopOriginal          = copyArray(zTop);
    double[] zBottomOriginal       = copyArray(zBottom);
    boolean[] validTopOriginal     = copyArray(validTop);
    boolean[] validBottomOriginal  = copyArray(validBottom);
    double topMedian               = top * overFactor;
    double bottomMedian            = bottom * overFactor;

    Expect.expect(MathUtil.fuzzyEquals(vse.refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian), boardThicknessNM));
    checkArraysMatch(zTop,        zTopOriginal);
    checkArraysMatch(zBottom,     zBottomOriginal);
    checkArraysMatch(validTop,    validTopOriginal);
    checkArraysMatch(validBottom, validBottomOriginal);

    if (debug)  pw.printf("test_refineBoardThickness_16: done%n");
  }

  /**
   * Test of refineBoardThickness
   * @author Bob Balliew
   */
  private void test_refineBoardThickness(BufferedReader br, PrintWriter pw)
  {
    Assert.expect(br != null);
    Assert.expect(pw != null);
    test_refineBoardThickness_0(br, pw);
    test_refineBoardThickness_1(br, pw);
    test_refineBoardThickness_2(br, pw);
    test_refineBoardThickness_3(br, pw);
    test_refineBoardThickness_4(br, pw);
    test_refineBoardThickness_5(br, pw);
    test_refineBoardThickness_6(br, pw);
    test_refineBoardThickness_7(br, pw);
    test_refineBoardThickness_8(br, pw);
    test_refineBoardThickness_9(br, pw);
    test_refineBoardThickness_10(br, pw);
    test_refineBoardThickness_11(br, pw);
    test_refineBoardThickness_12(br, pw);
    test_refineBoardThickness_13(br, pw);
    test_refineBoardThickness_14(br, pw);
    test_refineBoardThickness_15(br, pw);
    test_refineBoardThickness_16(br, pw);
  }

  /**
   * Test of chooseVerificationHeight method assert conditions.
   * @author Bob Balliew
   */
  private void test_chooseVerificationHeight_0(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_chooseVerificationHeight_0: started%n");

    final int width                = 5;
    final int height               = 5;
    final int nElements            = width * height;
    final double boardThicknessNM  = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    double[] zTop            = makeArray(nElements, +boardThicknessNM / 2.0);
    double[] zBottom         = makeArray(nElements, -boardThicknessNM / 2.0);
    double[] zShort          = makeArray(1, 0.0);
    double[] zTopOriginal    = copyArray(zTop);
    double[] zBottomOriginal = copyArray(zBottom);
    double[] zShortOriginal  = copyArray(zShort);
    boolean[] topExcluded    = makeArray(nElements, false);
    boolean[] bottomExcluded = makeArray(nElements, false);

    try
    {
      int sc = vse.chooseVerificationHeight(null, zBottom, topExcluded, bottomExcluded);
      Expect.expect(false, "'chooseVerificationHeight(null, zBottom)' should assert, sc: " + sc);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zBottom, zBottomOriginal);
    }

    try
    {
      int sc = vse.chooseVerificationHeight(zTop, null, topExcluded, bottomExcluded);
      Expect.expect(false, "'chooseVerificationHeight(zTop, null)' should assert, sc: " + sc);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
    }

    try
    {
      double[] dupRef = zTop;
      int sc = vse.chooseVerificationHeight(zTop, dupRef, topExcluded, bottomExcluded);
      Expect.expect(false, "'chooseVerificationHeight(zTop, dupRef)' should assert, sc: " + sc);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
    }

    try
    {
      int sc = vse.chooseVerificationHeight(zShort, zBottom, topExcluded, bottomExcluded);
      Expect.expect(false, "'chooseVerificationHeight(zShort, zBottom)' should assert, sc: " + sc);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zShort, zShortOriginal);
      checkArraysMatch(zBottom, zBottomOriginal);
    }

    try
    {
      int sc = vse.chooseVerificationHeight(zTop, zShort, topExcluded, bottomExcluded);
      Expect.expect(false, "'chooseVerificationHeight(zTop, zShort)' should assert, sc: " + sc);
    }
    catch (AssertException e)
    {
      checkArraysMatch(zTop, zTopOriginal);
      checkArraysMatch(zShort, zShortOriginal);
    }

    if (debug)  pw.printf("test_chooseVerificationHeight_0: done%n");
  }

  /**
   * Test of chooseVerificationHeight method abnormal return conditions.
   * @author Bob Balliew
   */
  private void test_chooseVerificationHeight_1(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_chooseVerificationHeight_1: started%n");

    final int width                = 4;
    final int height               = 5;
    final int nElements            = width * height;
    final double boardThicknessNM  = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    double[] zTop            = makeArray(nElements, +boardThicknessNM / 2.0);
    double[] zBottom         = makeArray(nElements, -boardThicknessNM / 2.0);
    double[] zTopOriginal    = copyArray(zTop);
    double[] zBottomOriginal = copyArray(zBottom);
    boolean[] topExcluded    = makeArray(nElements, false);
    boolean[] bottomExcluded = makeArray(nElements, false);
    int statusCode;

    try
    {
      Config.getInstance().setValueInMemoryOnly(SoftwareConfigEnum.TWO_PASS_VERIFICATION_SURFACE_MODELING_ENABLED, new Boolean(false));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);
    statusCode = vse.chooseVerificationHeight(zTop, zBottom, topExcluded, bottomExcluded);
    Expect.expect(statusCode == VerificationSurfaceEstimator.TWO_PASS_DISABLED_NC);
    checkArraysMatch(zTop, zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);

    try
    {
      Config.getInstance().setValueInMemoryOnly(SoftwareConfigEnum.TWO_PASS_VERIFICATION_SURFACE_MODELING_ENABLED, new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }

    double[] zTopNoNeighbors           = new double[nElements];
    double[] zBottomNoNeighbors        = new double[nElements];
    final double sameSideThreshold     = (double)(2 * VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_DELTA_Z_IN_NANOMETERS);
    final double oppositeSideThreshold = sameSideThreshold + ((VerificationSurfaceEstimator.NEIGHBOR_MAXIMUM_THICKNESS_CHANGE_PERCENTANGE / 100.0) * boardThicknessNM);
    final double zNotValidNeighbor     = 2.0 * oppositeSideThreshold;

    for (int i = 0; i < nElements; ++i)
    {
      zTopNoNeighbors[i]    = (+boardThicknessNM / 2.0) + ((double)(i + 1) * zNotValidNeighbor);
      zBottomNoNeighbors[i] = (-boardThicknessNM / 2.0) - ((double)(i + 1) * zNotValidNeighbor);
    }

    double[] zTopNoNeighborsOriginal    = copyArray(zTopNoNeighbors);
    double[] zBottomNoNeighborsOriginal = copyArray(zBottomNoNeighbors);

    vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);
    statusCode = vse.chooseVerificationHeight(zTopNoNeighbors, zBottomNoNeighbors, topExcluded, bottomExcluded);
    Expect.expect(statusCode == VerificationSurfaceEstimator.TOO_FEW_CLOSE_NEIGHBORS_NC);
    checkArraysMatch(zTopNoNeighbors, zTopNoNeighborsOriginal);
    checkArraysMatch(zBottomNoNeighbors, zBottomNoNeighborsOriginal);

    statusCode = vse.chooseVerificationHeight(zBottomNoNeighbors, zTopNoNeighbors, topExcluded, bottomExcluded);
    Expect.expect(statusCode == VerificationSurfaceEstimator.TOO_FEW_CLOSE_NEIGHBORS);
    checkArraysDiffer(zTopNoNeighbors, zTopNoNeighborsOriginal);
    checkArraysDiffer(zBottomNoNeighbors, zBottomNoNeighborsOriginal);
    zTopNoNeighbors    = copyArray(zTopNoNeighborsOriginal);
    zBottomNoNeighbors = copyArray(zBottomNoNeighborsOriginal);

    //statusCode = vse.chooseVerificationHeight(zTopNoNeighbors, zBottom, topExcluded, bottomExcluded);
    //Expect.expect(statusCode == VerificationSurfaceEstimator.CANNOT_COMPUTE_TOP_MEDIAN_NC);
    //checkArraysMatch(zTopNoNeighbors, zTopNoNeighborsOriginal);
    //checkArraysMatch(zBottom, zBottomOriginal);

    //statusCode = vse.chooseVerificationHeight(zBottom, zTopNoNeighbors, topExcluded, bottomExcluded);
    //Expect.expect(statusCode == VerificationSurfaceEstimator.CANNOT_COMPUTE_TOP_MEDIAN);
    //checkArraysDiffer(zTopNoNeighbors, zTopNoNeighborsOriginal);
    //checkArraysDiffer(zBottom, zBottomOriginal);
    //zTopNoNeighbors = copyArray(zTopNoNeighborsOriginal);
    //zBottom         = copyArray(zBottomOriginal);

    //statusCode = vse.chooseVerificationHeight(zTop, zBottomNoNeighbors, topExcluded, bottomExcluded);
    //Expect.expect(statusCode == VerificationSurfaceEstimator.CANNOT_COMPUTE_BOTTOM_MEDIAN_NC);
    //checkArraysMatch(zTop, zTopOriginal);
    //checkArraysMatch(zBottomNoNeighbors, zBottomNoNeighborsOriginal);

    //statusCode = vse.chooseVerificationHeight(zBottomNoNeighbors, zTop, topExcluded, bottomExcluded);
    //Expect.expect(statusCode == VerificationSurfaceEstimator.CANNOT_COMPUTE_BOTTOM_MEDIAN);
    //checkArraysDiffer(zTop, zTopOriginal);
    //checkArraysDiffer(zBottomNoNeighbors, zBottomNoNeighborsOriginal);
    //zTop               = copyArray(zTopOriginal);
    //zBottomNoNeighbors = copyArray(zBottomNoNeighborsOriginal);

    final int maxWarp = Math.max(XrayTester.getInstance().getMaximumPanelUpWarpInNanoMeters(),
                                 XrayTester.getInstance().getMaximumPanelDownWarpInNanoMeters());
    final double offset = 2.0 * ((double)maxWarp + (double)XrayTester.getMaximumJointHeightInNanoMeters());

    double[] zTopOffset    = copyArray(zTop);
    double[] zBottomOffset = copyArray(zBottom);

    for (int i = 0; i < nElements; ++i)
      if ((i % 2) == 0)
      {
        zTopOffset[i]    += offset;
        zBottomOffset[i] += offset;
      }
      else
      {
        zTopOffset[i]    -= offset;
        zBottomOffset[i] -= offset;
      }

    double[] zTopOffsetOriginal    = copyArray(zTopOffset);
    double[] zBottomOffsetOriginal = copyArray(zBottomOffset);

    statusCode = vse.chooseVerificationHeight(zTopOffset, zBottomOffset, topExcluded, bottomExcluded);
    Expect.expect(statusCode == VerificationSurfaceEstimator.TOO_FEW_VALID_POINTS_NC);
    checkArraysMatch(zTopOffset, zTopOffsetOriginal);
    checkArraysMatch(zBottomOffset, zBottomOffsetOriginal);

    statusCode = vse.chooseVerificationHeight(zBottomOffset, zTopOffset, topExcluded, bottomExcluded);
    Expect.expect(statusCode == VerificationSurfaceEstimator.TOO_FEW_VALID_POINTS);
    checkArraysDiffer(zTopOffset, zTopOffsetOriginal);
    checkArraysDiffer(zBottomOffset, zBottomOffsetOriginal);

    zTopOffset    = copyArray(zTopOffsetOriginal);
    zBottomOffset = copyArray(zBottomOffsetOriginal);

    if (debug)  pw.printf("test_chooseVerificationHeight_1: done%n");
  }

  /**
   * Test of chooseVerificationHeight method abnormal return conditions.
   * @author Bob Balliew
   */
  private void test_chooseVerificationHeight_2(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_chooseVerificationHeight_2: started%n");

    final int width                = 2;
    final int height               = 2;
    final int nElements            = width * height;
    final double boardThicknessNM  = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    double[] zTop            = makeArray(nElements, +boardThicknessNM / 2.0);
    double[] zBottom         = makeArray(nElements, -boardThicknessNM / 2.0);
    double[] zTopOriginal    = copyArray(zTop);
    double[] zBottomOriginal = copyArray(zBottom);
    boolean[] topExcluded    = makeArray(nElements, false);
    boolean[] bottomExcluded = makeArray(nElements, false);
    int statusCode;

    statusCode = vse.chooseVerificationHeight(zTop, zBottom, topExcluded, bottomExcluded);
    Expect.expect(statusCode == VerificationSurfaceEstimator.ERROR_SOLVING_CULLED_MATRIX_NC);
    checkArraysMatch(zTop, zTopOriginal);
    checkArraysMatch(zBottom, zBottomOriginal);

    statusCode = vse.chooseVerificationHeight(zBottom, zTop, topExcluded, bottomExcluded);
    Expect.expect(statusCode == VerificationSurfaceEstimator.ERROR_SOLVING_CULLED_MATRIX);
    checkArraysDiffer(zTop, zTopOriginal);
    checkArraysDiffer(zBottom, zBottomOriginal);
    zTop    = copyArray(zTopOriginal);
    zBottom = copyArray(zBottomOriginal);

    if (debug)  pw.printf("test_chooseVerificationHeight_2: done%n");
  }

  /**
   * Test of chooseVerificationHeight method abnormal return conditions.
   * @author Bob Balliew
   */
  private void test_chooseVerificationHeight_3(BufferedReader br, PrintWriter pw)
  {
    final boolean debug = false;
    if (debug)  pw.printf("test_chooseVerificationHeight_3: started%n");

    final int width                = 7;
    final int height               = 7;
    final int nElements            = width * height;
    final double boardThicknessNM  = MathUtil.convertUnits(50.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

    VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

    double[] zTopDither     = makeArray(nElements, +boardThicknessNM / 2.0);
    double[] zBottomDither  = makeArray(nElements, -boardThicknessNM / 2.0);
    // final double dither     = (double)(0.5* Config.getInstance().getIntValue(SoftwareConfigEnum.POINT_CAPTURE_MAXIMUM_DELTA_Z_IN_NANOMETERS));
    double dither = 0.0;
    boolean[] topExcluded    = makeArray(nElements, false);
    boolean[] bottomExcluded = makeArray(nElements, false);

    for (int i = 0; i < nElements; ++i)
      if ((i % 2) == 0)
      {
        zTopDither[i]    += dither;
        zBottomDither[i] += dither;
      }
      else
      {
        zTopDither[i]    -= dither;
        zBottomDither[i] -= dither;
      }

    double[] zTopDitherOriginal    = copyArray(zTopDither);
    double[] zBottomDitherOriginal = copyArray(zBottomDither);
    int statusCode;

    statusCode = vse.chooseVerificationHeight(zTopDither, zBottomDither, topExcluded, bottomExcluded);
    Expect.expect(statusCode == VerificationSurfaceEstimator.ERROR_SOLVING_MODEL_MATRIX_NC);
    checkArraysMatch(zTopDither, zTopDitherOriginal);
    checkArraysMatch(zBottomDither, zBottomDitherOriginal);

    statusCode = vse.chooseVerificationHeight(zBottomDither, zTopDither, topExcluded, bottomExcluded);
    Expect.expect(statusCode == VerificationSurfaceEstimator.ERROR_SOLVING_MODEL_MATRIX);
    checkArraysDiffer(zTopDither, zTopDitherOriginal);
    checkArraysDiffer(zBottomDither, zBottomDitherOriginal);
    zTopDither    = copyArray(zTopDitherOriginal);
    zBottomDither = copyArray(zBottomDitherOriginal);

    if (debug)  pw.printf("test_chooseVerificationHeight_3: done%n");
  }

  /**
   * Test of chooseVerificationHeight method by:
   *   1. Generate actual board center (model linear in height, quadratic in width).
   *   2. Generate board surfaces based on board center.
   *   3. Add random noise (normal distrubtion) to all z values.
   *   4. Shift randomly selected (uniform distribution) z values up and down by a random amount (normal distribution).
   *   5. Swap randomly selected (uniform distribution) z values between top and bottom surfaces.
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  private void test_chooseVerificationHeight_10(BufferedReader br, PrintWriter pw)
  {
    final boolean debug          = false;
    final boolean maxErrorSearch = false;
    final int     maxErrorLoops  = maxErrorSearch ? 250 : 1;
    if (debug)  pw.printf("test_chooseVerificationHeight_10: started%n");
    PrintWriter dw = null;

    // Uncomment out the following line to dump information into a file.
    //try { dw = new PrintWriter("C:/temp/test_chooseVerificationHeight_10.txt"); } catch (FileNotFoundException e) { dw = null; }

    Assert.expect(br != null);
    Assert.expect(pw != null);

    int maxLoop              = -1;
    int maxWidth             = -1;
    double maxError          =  0.0;
    double maxBoardWidth     = -1.0;
    double maxBoardHeight    = -1.0;
    double maxBoardThickness = -1.0;
    double maxOffset         = -1.0;
    double maxDeltaHeight    = -1.0;
    double maxDeltaWidth     = -1.0;
    double maxSag            = -1.0;
    double maxNoiseMean      = -1.0;
    double maxNoiseSigma     = -1.0;
    double maxShiftMean      = -1.0;
    double maxShiftSigma     = -1.0;
    double maxFactor         = -1.0;
    double[] maxZBoardCenter = null;
    double[] maxZTop1        = null;
    double[] maxZBot1        = null;
    double[] maxZTop2        = null;
    double[] maxZBot2        = null;
    double[] maxZTopErr      = null;
    double[] maxZBotErr      = null;
    double[] zTopOriginal    = null;
    double[] zBotOriginal    = null;
    int loop;

    for (loop = 0; loop < maxErrorLoops; ++loop)
    {
      if (maxErrorSearch)
        System.out.printf("loop: %3d, maxLoop: %3d, maxError: %21.15e%n", loop, maxLoop, MathUtil.convertUnits(maxError, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));

      // in mils, boardWidths, boardHeights, boardThicknesses, offsets, deltaHeights, deltaWidths, and sags are groups element by element.
      double[] boardWidths      = { 4.0,   4.0,   10.0,  18.0};  // in inches
      double[] boardHeights     = { 4.0,   1.0,   12.0,  24.0};  // in inches
      double[] boardThicknesses = {40.0, 125.0,   50.0,  20.0};  // in mils
      double[] offsets          = {10.0,   0.0,  -10.0,   5.0};  // in mils
      double[] deltaHeights     = {-2.5,   0.625, -6.0,  10.0};  // in mils
      double[] deltaWidths      = {-2.5,   2.5,    5.0, -10.0};  // in mils
      double[] sags             = { 0.0,  15.0,  -50.0, -80.0};  // in mils

      // noiseMeans paired with noiseSigmas element by element.
      double[] noiseMeans  = {0.0, 0.0,  2.0, -2.0};  // in mils
      double[] noiseSigmas = {0.0, 5.0, 10.0, 10.0};  // in mils

      // in mils, sweepShiftMeans paired with sweepShiftSigmas element by element.
      double[] sweepShiftMeans  = {0.0, 10.0, 40.0};  // in mils
      double[] sweepShiftSigmas = {0.0,  5.0, 20.0};  // in mils

      double[] sweepFactors = {0.0, 0.1, 0.25};

      Assert.expect(boardWidths.length == boardHeights.length);
      Assert.expect(boardWidths.length == boardThicknesses.length);
      Assert.expect(boardWidths.length == offsets.length);
      Assert.expect(boardWidths.length == deltaHeights.length);
      Assert.expect(boardWidths.length == deltaWidths.length);
      Assert.expect(boardWidths.length == sags.length);

      for (int iModel = 0; iModel < boardWidths.length; ++iModel)
      {
        final double boardWidthNM               = MathUtil.convertUnits(boardWidths[iModel],      MathUtilEnum.INCHES, MathUtilEnum.NANOMETERS);
        final double boardHeightNM              = MathUtil.convertUnits(boardHeights[iModel],     MathUtilEnum.INCHES, MathUtilEnum.NANOMETERS);
        final double boardThicknessNM           = MathUtil.convertUnits(boardThicknesses[iModel], MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double offset                     = MathUtil.convertUnits(offsets[iModel],          MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double zDeltaOverHeightEdgeToEdge = MathUtil.convertUnits(deltaHeights[iModel],     MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double zDeltaOverWidthEdgeToEdge  = MathUtil.convertUnits(deltaWidths[iModel],      MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double zSagOverWidthEdgeToMiddle  = MathUtil.convertUnits(sags[iModel],             MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);

        Assert.expect(noiseMeans.length == noiseSigmas.length);

        for (int iNoise = 0; iNoise < noiseMeans.length; ++iNoise)
        {
          final double noiseTopMean     = MathUtil.convertUnits(noiseMeans[iNoise],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
          final double noiseTopSigma    = MathUtil.convertUnits(noiseSigmas[iNoise], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
          final double noiseBottomMean  = MathUtil.convertUnits(noiseMeans[iNoise],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
          final double noiseBottomSigma = MathUtil.convertUnits(noiseSigmas[iNoise], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

          Assert.expect(sweepShiftMeans.length == sweepShiftSigmas.length);

          for (int iShift = 0; iShift < sweepShiftMeans.length; ++iShift)
          {
            final double liftTopMean     = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double liftTopSigma    = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double liftBottomMean  = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double liftBottomSigma = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropTopMean     = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropTopSigma    = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropBottomMean  = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropBottomSigma = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

            for (double factor : sweepFactors)
            {
              final double liftTopFactor    = factor;
              final double liftBottomFactor = factor;
              final double dropTopFactor    = factor;
              final double dropBottomFactor = factor;
              final double swapFactor       = factor;

              Assert.expect(noiseTopSigma    >= 0.0);
              Assert.expect(noiseBottomSigma >= 0.0);
              Assert.expect(liftTopMean      >= 0.0);
              Assert.expect(liftTopSigma     >= 0.0);
              Assert.expect(liftTopFactor    >= 0.0);
              Assert.expect(liftBottomMean   >= 0.0);
              Assert.expect(liftBottomSigma  >= 0.0);
              Assert.expect(liftBottomFactor >= 0.0);
              Assert.expect(dropTopMean      >= 0.0);
              Assert.expect(dropTopSigma     >= 0.0);
              Assert.expect(dropTopFactor    >= 0.0);
              Assert.expect(dropBottomMean   >= 0.0);
              Assert.expect(dropBottomSigma  >= 0.0);
              Assert.expect(dropBottomFactor >= 0.0);
              Assert.expect(swapFactor       >= 0.0);

              final double approxTileSideLengthNM = MathUtil.convertUnits(322.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
              final int width                     = (int)Math.ceil(boardWidthNM  / approxTileSideLengthNM);
              final int height                    = (int)Math.ceil(boardHeightNM / approxTileSideLengthNM);
              final double tileWidthNM            = boardWidthNM  / (double)width;
              final double tileHeightNM           = boardHeightNM / (double)height;
              final int nElements                 = width * height;
              final double C                      = offset + (zDeltaOverWidthEdgeToEdge / 2.0) + zSagOverWidthEdgeToMiddle; // Correct for Y axis origin shift from edge to center of the board
              final double H1                     = zDeltaOverHeightEdgeToEdge / boardHeightNM;
              final double W1                     = zDeltaOverWidthEdgeToEdge  / boardWidthNM;
              final double W2                     = -zSagOverWidthEdgeToMiddle * 4.0 / (boardWidthNM * boardWidthNM);

              final double zErrorLimitRMS = MathUtil.convertUnits(4.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS) +
                                            MathUtil.convertUnits(50.0 / Math.sqrt((double)nElements), MathUtilEnum.MILS, MathUtilEnum.NANOMETERS) +
                                            ((Math.abs(noiseTopMean) + Math.abs(noiseBottomMean)) * 2.0) +
                                            ((noiseTopSigma + noiseBottomSigma) * 0.5) +
                                           MathUtil.convertUnits((liftTopFactor + liftBottomFactor + dropTopFactor + dropBottomFactor) * 2.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
              final double zTopErrorLimitRMS    = zErrorLimitRMS;
              final double zBottomErrorLimitRMS = zErrorLimitRMS;

              VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

              // Create a model board center.
              Matrix model = new Matrix(4, 1);
              model.set(0, 0, W2);  // second order Y axis coefficient
              model.set(1, 0, W1);  // first order Y axis coefficient
              model.set(2, 0, H1);  // first order X coefficient
              model.set(3, 0, C);   // zeroth order coefficient

              if (debug)
                VerificationSurfaceEstimator.printMatrixInMils(pw, model, "TVSE:Model");

              Matrix fwdOperator = new Matrix(nElements, 4);
              Matrix xDistance   = new Matrix(nElements, 1);
              Matrix yDistance   = new Matrix(nElements, 1);

              for (int i = 0; i < nElements; i++)
              {
                final int widthIndex = i % width;
                final int heightIndex = i / width;
                final double y = ((double)widthIndex  * tileWidthNM)  + (tileWidthNM  / 2.0) - (boardWidthNM / 2.0); // Y axis distance from center of board to center of ith tile (zero based from board's edge).
                final double x = ((double)heightIndex * tileHeightNM) + (tileHeightNM / 2.0); // X axis distance from edge of board to center of ith tile (zero based from board's edge).
                yDistance.set(i, 0, y);
                xDistance.set(i, 0, x);
                fwdOperator.set(i, 0, y * y);
                fwdOperator.set(i, 1, y);
                fwdOperator.set(i, 2, x);
                fwdOperator.set(i, 3, 1.0);
              }

              if (debug)
                VerificationSurfaceEstimator.printMatrixInMils(pw, fwdOperator, "TVSE:Forward operator");

              Matrix zBoardCenter = fwdOperator.times(model);

              if (debug)
                VerificationSurfaceEstimator.printMatrixInMils(pw, zBoardCenter, "TVSE:Board surface");

              if (dw != null)
              {
                dw.printf("zBoardCenter [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBoardCenter.getColumnPackedCopy());
              }

              // Create actual top and bottom board surfaces.
              double[] zTopActual    = new double[nElements]; // Z axis in perpendicular to the plane of the panel.
              double[] zBottomActual = new double[nElements];

              for (int i = 0; i < nElements; i++)
              {
                zTopActual[i]    = zBoardCenter.get(i, 0) + boardThicknessNM / 2.0;
                zBottomActual[i] = zBoardCenter.get(i, 0) - boardThicknessNM / 2.0;
              }

              // Create first pass z focus results.
              double[] zTopEst     = copyArray(zTopActual);
              double[] zBottomEst  = copyArray(zBottomActual);
              double[] noiseTop    = makeValues(nElements, nElements, noiseTopMean, noiseTopSigma);
              double[] noiseBottom = makeValues(nElements, nElements, noiseBottomMean, noiseBottomSigma);

              // Add noise to measured values.
              for (int i = 0; i < nElements; ++i)
              {
                zTopEst[i]    += noiseTop[i];
                zBottomEst[i] += noiseBottom[i];
              }

              // Randomly lift and drop points
              double[] zTopLift    = makeValues(nElements, nElements, +liftTopMean, liftTopSigma);
              double[] zBottomLift = makeValues(nElements, nElements, +liftBottomMean, liftBottomSigma);
              double[] zTopDrop    = makeValues(nElements, nElements, -dropTopMean, dropTopSigma);
              double[] zBottomDrop = makeValues(nElements, nElements, -dropBottomMean, dropBottomSigma);
              boolean[] topExcluded    = makeArray(nElements, false);
              boolean[] bottomExcluded = makeArray(nElements, false);
              int[] iTopLift       = makeValues(0, (int)Math.ceil((double)nElements * liftTopFactor), 0, nElements - 1);
              int[] iBottomLift    = makeValues(0, (int)Math.ceil((double)nElements * liftBottomFactor), 0, nElements - 1);
              int[] iTopDrop       = makeValues(0, (int)Math.ceil((double)nElements * dropTopFactor), 0, nElements - 1);
              int[] iBottomDrop    = makeValues(0, (int)Math.ceil((double)nElements * dropBottomFactor), 0, nElements - 1);
              offsetValues(zTopEst,    iTopLift,    zTopLift);
              offsetValues(zBottomEst, iBottomLift, zBottomLift);
              offsetValues(zTopEst,    iTopDrop,    zTopDrop);
              offsetValues(zBottomEst, iBottomDrop, zBottomDrop);

              // Randomly swap top and bottom points
              int[] iSwapPairs = makeValues(0, (int)Math.ceil((double)nElements * swapFactor), 0, nElements - 1);
              swapPairs(zTopEst, zBottomEst, iSwapPairs);

              if (dw != null)
              {
                dw.printf("zTopEst (before VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zTopEst);
                dw.printf("zBottomEst (before VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBottomEst);
              }

              if (maxErrorSearch)
              {
                zTopOriginal = copyArray(zTopEst);
                zBotOriginal = copyArray(zBottomEst);
              }

              final int statusCode = vse.chooseVerificationHeight(zTopEst, zBottomEst, topExcluded, bottomExcluded);

              Expect.expect(statusCode == VerificationSurfaceEstimator.NORMAL);

              if (dw != null)
              {
                dw.printf("zTopEst (after VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zTopEst);
                dw.printf("zBottomEst (after VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBottomEst);
              }

              if (debug)
              {
                VerificationSurfaceEstimator.printArray(pw, zTopActual, "zTopActual");
                VerificationSurfaceEstimator.printArray(pw, zTopEst, "zTopEst");
                VerificationSurfaceEstimator.printArray(pw, zBottomActual, "zBottomActual");
                VerificationSurfaceEstimator.printArray(pw, zBottomEst, "zBottomEst");
              }

              double[] zTopError            = copyArray(zTopEst);
              double[] zBottomError         = copyArray(zBottomEst);
              double zTopErrorSquaredSum    = 0.0;
              double zBottomErrorSquaredSum = 0.0;

              for (int i = 0; i < nElements; i++)
              {
                zTopError[i]           -= zTopActual[i];
                zBottomError[i]        -= zBottomActual[i];
                zTopErrorSquaredSum    += (zTopError[i]    * zTopError[i]);
                zBottomErrorSquaredSum += (zBottomError[i] * zBottomError[i]);
              }

              if (dw != null)
              {
                dw.printf("zTopError: [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zTopError);
                dw.printf("zBottomError [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBottomError);
              }

              double zTopErrorRMS    = Math.sqrt(zTopErrorSquaredSum    / (double)nElements);
              double zBottomErrorRMS = Math.sqrt(zBottomErrorSquaredSum / (double)nElements);

              if (debug)
              {
                pw.printf("zTopErrorRMS:    %+21.15e (%+21.15e mils)%n", zTopErrorRMS,    MathUtil.convertUnits(zTopErrorRMS,    MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
                pw.printf("zBottomErrorRMS: %+21.15e (%+21.15e mils)%n", zBottomErrorRMS, MathUtil.convertUnits(zBottomErrorRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
              }

              Expect.expect(zTopErrorRMS    < zTopErrorLimitRMS,    "zTopErrorRMS: "    + zTopErrorRMS    + ",    zTopErrorLimitRMS: " + zTopErrorLimitRMS    + " [" + iModel + "][" + iNoise + "][" + iShift + "] " + factor + " " + nElements);
              Expect.expect(zBottomErrorRMS < zBottomErrorLimitRMS, "zBottomErrorRMS: " + zBottomErrorRMS + ", zBottomErrorLimitRMS: " + zBottomErrorLimitRMS + " [" + iModel + "][" + iNoise + "][" + iShift + "] " + factor + " " + nElements);

/*
              pw.printf("Top:\t%e\t%e\t%e\t%d\t%d\t%d\t%f\t%d\t%b%n",
                        MathUtil.convertUnits(zTopErrorRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        MathUtil.convertUnits(zTopErrorLimitRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        zTopErrorLimitRMS / zTopErrorRMS,
                        iModel,
                        iNoise,
                        iShift,
                        factor,
                        nElements,
                        zTopErrorRMS < zTopErrorLimitRMS);
              pw.printf("Bot:\t%e\t%e\t%e\t%d\t%d\t%d\t%f\t%d\t%b%n",
                        MathUtil.convertUnits(zBottomErrorRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        MathUtil.convertUnits(zBottomErrorLimitRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        zBottomErrorLimitRMS / zBottomErrorRMS,
                        iModel,
                        iNoise,
                        iShift,
                        factor,
                        nElements,
                        zBottomErrorRMS < zBottomErrorLimitRMS);
*/

              if ((maxErrorSearch) && ((maxError < zTopErrorRMS) || (maxError < zBottomErrorRMS)))
              {
                maxError          = Math.max(zTopErrorRMS, zBottomErrorRMS);
                maxLoop           = loop;
                maxWidth          = width;
                maxBoardWidth     = MathUtil.convertUnits(boardWidthNM,               MathUtilEnum.NANOMETERS, MathUtilEnum.INCHES);
                maxBoardHeight    = MathUtil.convertUnits(boardHeightNM,              MathUtilEnum.NANOMETERS, MathUtilEnum.INCHES);
                maxBoardThickness = MathUtil.convertUnits(boardThicknessNM,           MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxOffset         = MathUtil.convertUnits(offset,                     MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxDeltaHeight    = MathUtil.convertUnits(zDeltaOverHeightEdgeToEdge, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxDeltaWidth     = MathUtil.convertUnits(zDeltaOverWidthEdgeToEdge,  MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxSag            = MathUtil.convertUnits(zSagOverWidthEdgeToMiddle,  MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                Assert.expect(MathUtil.fuzzyEquals(noiseTopMean,  noiseBottomMean));
                Assert.expect(MathUtil.fuzzyEquals(noiseTopSigma, noiseBottomSigma));
                maxNoiseMean      = MathUtil.convertUnits(noiseTopMean,               MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxNoiseSigma     = MathUtil.convertUnits(noiseTopSigma,              MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                Assert.expect(MathUtil.fuzzyEquals(liftTopMean,  liftBottomMean));
                Assert.expect(MathUtil.fuzzyEquals(liftTopMean,  dropTopMean));
                Assert.expect(MathUtil.fuzzyEquals(liftTopMean,  dropBottomMean));
                Assert.expect(MathUtil.fuzzyEquals(liftTopSigma, liftBottomSigma));
                Assert.expect(MathUtil.fuzzyEquals(liftTopSigma, dropTopSigma));
                Assert.expect(MathUtil.fuzzyEquals(liftTopSigma, dropBottomSigma));
                maxShiftMean      = MathUtil.convertUnits(liftTopMean,                MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxShiftSigma     = MathUtil.convertUnits(liftTopSigma,               MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                Assert.expect(MathUtil.fuzzyEquals(swapFactor, liftTopFactor));
                Assert.expect(MathUtil.fuzzyEquals(swapFactor, liftBottomFactor));
                Assert.expect(MathUtil.fuzzyEquals(swapFactor, dropTopFactor));
                Assert.expect(MathUtil.fuzzyEquals(swapFactor, dropBottomFactor));
                maxFactor         = swapFactor;
                maxZBoardCenter   = zBoardCenter.getColumnPackedCopy();
                maxZTop1          = copyArray(zTopOriginal);
                maxZBot1          = copyArray(zBotOriginal);
                maxZTop2          = copyArray(zTopEst);
                maxZBot2          = copyArray(zBottomEst);
                maxZTopErr        = copyArray(zTopError);
                maxZBotErr        = copyArray(zBottomError);
              }
            }
          }
        }
      }
    }

    if (maxErrorSearch)
    {
      if (dw != null)  dw.close();
      try { dw = new PrintWriter("C:/temp/test_chooseVerificationHeight_worst.txt"); } catch (FileNotFoundException e) { dw = null; }
      dw.printf("zBoardCenter (worst) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBoardCenter);
      dw.printf("zTopEst (worst, before VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZTop1);
      dw.printf("zBottomEst (worst, before VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBot1);
      dw.printf("zTopEst (worst, after VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZTop2);
      dw.printf("zBottomEst (worst, after VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBot2);
      dw.printf("zTopError: (worst) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZTopErr);
      dw.printf("zBottomError (worst) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBotErr);
      dw.printf("maxError (RMS in mils): %6.3f%n", MathUtil.convertUnits(maxError, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
      dw.printf("%3.0f x %3.0f x %4.0f, C: %6.3f, dh: %6.3f, hw: %6.3f, sag: %6.3f%n", maxBoardWidth, maxBoardHeight, maxBoardThickness, maxOffset, maxDeltaHeight, maxDeltaWidth, maxSag);
      dw.printf("noise: %6.3f, %6.3f; shift: %6.3f, %6.3f; factor: %6.3f%n", maxNoiseMean, maxNoiseSigma, maxShiftMean, maxShiftSigma, maxFactor);
    }

    if (dw != null)  dw.close();

    if (debug)  pw.printf("test_chooseVerificationHeight_10: done%n");
  }

  /**
   * Test of chooseVerificationHeight
   * @author Bob Balliew
   */
  private void test_chooseVerificationHeight(BufferedReader br, PrintWriter pw)
  {
    Assert.expect(br != null);
    Assert.expect(pw != null);
    test_chooseVerificationHeight_0(br, pw);
    test_chooseVerificationHeight_1(br, pw);
    test_chooseVerificationHeight_2(br, pw);
    //test_chooseVerificationHeight_3(br, pw);
    test_chooseVerificationHeight_10(br, pw);
  }

  /**
   * Test of chooseVerificationHeight with excluded data by:
   *   1. Generate actual board center (model linear in height, quadratic in width).
   *   2. Generate board surfaces based on board center.
   *   3. Add random noise (normal distrubtion) to all z values.
   *   4. Shift randomly selected (uniform distribution) z values by a large, fixed amount, but mark as unused
   * @author Rick Gaudette
   * @author Bob Balliew
   * @author John Heumann
   */
  private void test_chooseVerificationHeightWithExcludedData(BufferedReader br, PrintWriter pw)
  {
    final boolean debug          = false;
    final boolean maxErrorSearch = false;
    final int     maxErrorLoops  = maxErrorSearch ? 250 : 1;
    if (debug)  pw.printf("test_chooseVerificationHeightWithExcludedData: started%n");
    PrintWriter dw = null;

    // Uncomment out the following line to dump information into a file.
    //try { dw = new PrintWriter("C:/temp/test_chooseVerificationHeightWithExcludedData.txt"); } catch (FileNotFoundException e) { dw = null; }

    Assert.expect(br != null);
    Assert.expect(pw != null);

    int maxLoop              = -1;
    int maxWidth             = -1;
    double maxError          =  0.0;
    double maxBoardWidth     = -1.0;
    double maxBoardHeight    = -1.0;
    double maxBoardThickness = -1.0;
    double maxOffset         = -1.0;
    double maxDeltaHeight    = -1.0;
    double maxDeltaWidth     = -1.0;
    double maxSag            = -1.0;
    double maxNoiseMean      = -1.0;
    double maxNoiseSigma     = -1.0;
    double maxShiftMean      = -1.0;
    double maxShiftSigma     = -1.0;
    double maxFactor         = -1.0;
    double[] maxZBoardCenter = null;
    double[] maxZTop1        = null;
    double[] maxZBot1        = null;
    double[] maxZTop2        = null;
    double[] maxZBot2        = null;
    double[] maxZTopErr      = null;
    double[] maxZBotErr      = null;
    double[] zTopOriginal    = null;
    double[] zBotOriginal    = null;
    int loop;

    for (loop = 0; loop < maxErrorLoops; ++loop)
    {
      if (maxErrorSearch)
        System.out.printf("loop: %3d, maxLoop: %3d, maxError: %21.15e%n", loop, maxLoop, MathUtil.convertUnits(maxError, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));

      // in mils, boardWidths, boardHeights, boardThicknesses, offsets, deltaHeights, deltaWidths, and sags are groups element by element.
      double[] boardWidths      = { 4.0,   4.0,   10.0,  18.0};  // in inches
      double[] boardHeights     = { 4.0,   1.0,   12.0,  24.0};  // in inches
      double[] boardThicknesses = {40.0, 125.0,   50.0,  20.0};  // in mils
      double[] offsets          = {10.0,   0.0,  -10.0,   5.0};  // in mils
      double[] deltaHeights     = {-2.5,   0.625, -6.0,  10.0};  // in mils
      double[] deltaWidths      = {-2.5,   2.5,    5.0, -10.0};  // in mils
      double[] sags             = { 0.0,  15.0,  -50.0, -80.0};  // in mils

      // noiseMeans paired with noiseSigmas element by element.
      double[] noiseMeans  = {0.0, 0.0,  2.0, -2.0};  // in mils
      double[] noiseSigmas = {0.0, 5.0, 10.0, 10.0};  // in mils

      // in mils, sweepShiftMeans paired with sweepShiftSigmas element by element.
      double[] sweepShiftMeans  = {0.0, 10.0, 40.0};  // in mils
      double[] sweepShiftSigmas = {0.0,  5.0, 20.0};  // in mils

      double[] sweepFactors = {0.0, 0.1, 0.25};

      Assert.expect(boardWidths.length == boardHeights.length);
      Assert.expect(boardWidths.length == boardThicknesses.length);
      Assert.expect(boardWidths.length == offsets.length);
      Assert.expect(boardWidths.length == deltaHeights.length);
      Assert.expect(boardWidths.length == deltaWidths.length);
      Assert.expect(boardWidths.length == sags.length);

      for (int iModel = 0; iModel < boardWidths.length; ++iModel)
      {
        final double boardWidthNM               = MathUtil.convertUnits(boardWidths[iModel],      MathUtilEnum.INCHES, MathUtilEnum.NANOMETERS);
        final double boardHeightNM              = MathUtil.convertUnits(boardHeights[iModel],     MathUtilEnum.INCHES, MathUtilEnum.NANOMETERS);
        final double boardThicknessNM           = MathUtil.convertUnits(boardThicknesses[iModel], MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double offset                     = MathUtil.convertUnits(offsets[iModel],          MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double zDeltaOverHeightEdgeToEdge = MathUtil.convertUnits(deltaHeights[iModel],     MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double zDeltaOverWidthEdgeToEdge  = MathUtil.convertUnits(deltaWidths[iModel],      MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double zSagOverWidthEdgeToMiddle  = MathUtil.convertUnits(sags[iModel],             MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);

        Assert.expect(noiseMeans.length == noiseSigmas.length);

        for (int iNoise = 0; iNoise < noiseMeans.length; ++iNoise)
        {
          final double noiseTopMean     = MathUtil.convertUnits(noiseMeans[iNoise],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
          final double noiseTopSigma    = MathUtil.convertUnits(noiseSigmas[iNoise], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
          final double noiseBottomMean  = MathUtil.convertUnits(noiseMeans[iNoise],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
          final double noiseBottomSigma = MathUtil.convertUnits(noiseSigmas[iNoise], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

          Assert.expect(sweepShiftMeans.length == sweepShiftSigmas.length);

          for (int iShift = 0; iShift < sweepShiftMeans.length; ++iShift)
          {
            final double liftTopMean     = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double liftTopSigma    = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double liftBottomMean  = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double liftBottomSigma = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropTopMean     = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropTopSigma    = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropBottomMean  = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropBottomSigma = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

            for (double factor : sweepFactors)
            {
              final double liftTopFactor    = factor;
              final double liftBottomFactor = factor;
              final double dropTopFactor    = factor;
              final double dropBottomFactor = factor;

              Assert.expect(noiseTopSigma    >= 0.0);
              Assert.expect(noiseBottomSigma >= 0.0);
              Assert.expect(liftTopMean      >= 0.0);
              Assert.expect(liftTopSigma     >= 0.0);
              Assert.expect(liftTopFactor    >= 0.0);
              Assert.expect(liftBottomMean   >= 0.0);
              Assert.expect(liftBottomSigma  >= 0.0);
              Assert.expect(liftBottomFactor >= 0.0);
              Assert.expect(dropTopMean      >= 0.0);
              Assert.expect(dropTopSigma     >= 0.0);
              Assert.expect(dropTopFactor    >= 0.0);
              Assert.expect(dropBottomMean   >= 0.0);
              Assert.expect(dropBottomSigma  >= 0.0);
              Assert.expect(dropBottomFactor >= 0.0);

              final double approxTileSideLengthNM = MathUtil.convertUnits(322.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
              final int width                     = (int)Math.ceil(boardWidthNM  / approxTileSideLengthNM);
              final int height                    = (int)Math.ceil(boardHeightNM / approxTileSideLengthNM);
              final double tileWidthNM            = boardWidthNM  / (double)width;
              final double tileHeightNM           = boardHeightNM / (double)height;
              final int nElements                 = width * height;
              final double C                      = offset + (zDeltaOverWidthEdgeToEdge / 2.0) + zSagOverWidthEdgeToMiddle; // Correct for Y axis origin shift from edge to center of the board
              final double H1                     = zDeltaOverHeightEdgeToEdge / boardHeightNM;
              final double W1                     = zDeltaOverWidthEdgeToEdge  / boardWidthNM;
              final double W2                     = -zSagOverWidthEdgeToMiddle * 4.0 / (boardWidthNM * boardWidthNM);

              final double zErrorLimitRMS = MathUtil.convertUnits(4.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS) +
                                            MathUtil.convertUnits(50.0 / Math.sqrt((double)nElements), MathUtilEnum.MILS, MathUtilEnum.NANOMETERS) +
                                            ((Math.abs(noiseTopMean) + Math.abs(noiseBottomMean)) * 2.0) +
                                            ((noiseTopSigma + noiseBottomSigma) * 0.5) +
                                           MathUtil.convertUnits((liftTopFactor + liftBottomFactor + dropTopFactor + dropBottomFactor) * 2.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
              final double zTopErrorLimitRMS    = zErrorLimitRMS;
              final double zBottomErrorLimitRMS = zErrorLimitRMS;

              VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

              // Create a model board center.
              Matrix model = new Matrix(4, 1);
              model.set(0, 0, W2);  // second order Y axis coefficient
              model.set(1, 0, W1);  // first order Y axis coefficient
              model.set(2, 0, H1);  // first order X coefficient
              model.set(3, 0, C);   // zeroth order coefficient

              if (debug)
                VerificationSurfaceEstimator.printMatrixInMils(pw, model, "TVSE:Model");

              Matrix fwdOperator = new Matrix(nElements, 4);
              Matrix xDistance   = new Matrix(nElements, 1);
              Matrix yDistance   = new Matrix(nElements, 1);

              for (int i = 0; i < nElements; i++)
              {
                final int widthIndex = i % width;
                final int heightIndex = i / width;
                final double y = ((double)widthIndex  * tileWidthNM)  + (tileWidthNM  / 2.0) - (boardWidthNM / 2.0); // Y axis distance from center of board to center of ith tile (zero based from board's edge).
                final double x = ((double)heightIndex * tileHeightNM) + (tileHeightNM / 2.0); // X axis distance from edge of board to center of ith tile (zero based from board's edge).
                yDistance.set(i, 0, y);
                xDistance.set(i, 0, x);
                fwdOperator.set(i, 0, y * y);
                fwdOperator.set(i, 1, y);
                fwdOperator.set(i, 2, x);
                fwdOperator.set(i, 3, 1.0);
              }

              if (debug)
                VerificationSurfaceEstimator.printMatrixInMils(pw, fwdOperator, "TVSE:Forward operator");

              Matrix zBoardCenter = fwdOperator.times(model);

              if (debug)
                VerificationSurfaceEstimator.printMatrixInMils(pw, zBoardCenter, "TVSE:Board surface");

              if (dw != null)
              {
                dw.printf("zBoardCenter [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBoardCenter.getColumnPackedCopy());
              }

              // Create actual top and bottom board surfaces.
              double[] zTopActual    = new double[nElements]; // Z axis in perpendicular to the plane of the panel.
              double[] zBottomActual = new double[nElements];

              for (int i = 0; i < nElements; i++)
              {
                zTopActual[i]    = zBoardCenter.get(i, 0) + boardThicknessNM / 2.0;
                zBottomActual[i] = zBoardCenter.get(i, 0) - boardThicknessNM / 2.0;
              }

              // Create first pass z focus results.
              double[] zTopEst     = copyArray(zTopActual);
              double[] zBottomEst  = copyArray(zBottomActual);
              double[] noiseTop    = makeValues(nElements, nElements, noiseTopMean, noiseTopSigma);
              double[] noiseBottom = makeValues(nElements, nElements, noiseBottomMean, noiseBottomSigma);

              // Add noise to measured values.
              for (int i = 0; i < nElements; ++i)
              {
                zTopEst[i]    += noiseTop[i];
                zBottomEst[i] += noiseBottom[i];
              }

              // Randomly lift and drop points
              double[] zTopLift    = makeValues(nElements, nElements, +liftTopMean, liftTopSigma);
              double[] zBottomLift = makeValues(nElements, nElements, +liftBottomMean, liftBottomSigma);
              double[] zTopDrop    = makeValues(nElements, nElements, -dropTopMean, dropTopSigma);
              double[] zBottomDrop = makeValues(nElements, nElements, -dropBottomMean, dropBottomSigma);
              boolean[] excluded    = makeValues(nElements, 0.6);
              int[] iTopLift       = makeValues(0, (int)Math.ceil((double)nElements * liftTopFactor), 0, nElements - 1);
              int[] iBottomLift    = makeValues(0, (int)Math.ceil((double)nElements * liftBottomFactor), 0, nElements - 1);
              int[] iTopDrop       = makeValues(0, (int)Math.ceil((double)nElements * dropTopFactor), 0, nElements - 1);
              int[] iBottomDrop    = makeValues(0, (int)Math.ceil((double)nElements * dropBottomFactor), 0, nElements - 1);
              offsetValues(zTopEst,    iTopLift,    zTopLift);
              offsetValues(zBottomEst, iBottomLift, zBottomLift);
              offsetValues(zTopEst,    iTopDrop,    zTopDrop);
              offsetValues(zBottomEst, iBottomDrop, zBottomDrop);

              // Shift top and bottom surface together for excluded points
              // If exclusion fails, this will shift the fitted surface drastically and cause the test to fail
              for (int i = 0; i < nElements; ++i)
              {
                if (excluded[i]) {
                  zTopEst[i] += 2540000;
                  zBottomEst[i] += 2540000;
                }
              }

              if (dw != null)
              {
                dw.printf("zTopEst (before VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zTopEst);
                dw.printf("zBottomEst (before VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBottomEst);
              }

              if (maxErrorSearch)
              {
                zTopOriginal = copyArray(zTopEst);
                zBotOriginal = copyArray(zBottomEst);
              }

              final int statusCode = vse.chooseVerificationHeight(zTopEst, zBottomEst, excluded, excluded);

              Expect.expect(statusCode == VerificationSurfaceEstimator.NORMAL);

              if (dw != null)
              {
                dw.printf("zTopEst (after VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zTopEst);
                dw.printf("zBottomEst (after VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBottomEst);
              }

              if (debug)
              {
                VerificationSurfaceEstimator.printArray(pw, zTopActual, "zTopActual");
                VerificationSurfaceEstimator.printArray(pw, zTopEst, "zTopEst");
                VerificationSurfaceEstimator.printArray(pw, zBottomActual, "zBottomActual");
                VerificationSurfaceEstimator.printArray(pw, zBottomEst, "zBottomEst");
              }

              double[] zTopError            = copyArray(zTopEst);
              double[] zBottomError         = copyArray(zBottomEst);
              double zTopErrorSquaredSum    = 0.0;
              double zBottomErrorSquaredSum = 0.0;

              for (int i = 0; i < nElements; i++)
              {
                zTopError[i]           -= zTopActual[i];
                zBottomError[i]        -= zBottomActual[i];
                zTopErrorSquaredSum    += (zTopError[i]    * zTopError[i]);
                zBottomErrorSquaredSum += (zBottomError[i] * zBottomError[i]);
              }

              if (dw != null)
              {
                dw.printf("zTopError: [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zTopError);
                dw.printf("zBottomError [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBottomError);
              }

              double zTopErrorRMS    = Math.sqrt(zTopErrorSquaredSum    / (double)nElements);
              double zBottomErrorRMS = Math.sqrt(zBottomErrorSquaredSum / (double)nElements);

              if (debug)
              {
                pw.printf("zTopErrorRMS:    %+21.15e (%+21.15e mils)%n", zTopErrorRMS,    MathUtil.convertUnits(zTopErrorRMS,    MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
                pw.printf("zBottomErrorRMS: %+21.15e (%+21.15e mils)%n", zBottomErrorRMS, MathUtil.convertUnits(zBottomErrorRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
              }

              Expect.expect(zTopErrorRMS    < zTopErrorLimitRMS,    "zTopErrorRMS: "    + zTopErrorRMS    + ",    zTopErrorLimitRMS: " + zTopErrorLimitRMS    + " [" + iModel + "][" + iNoise + "][" + iShift + "] " + factor + " " + nElements);
              Expect.expect(zBottomErrorRMS < zBottomErrorLimitRMS, "zBottomErrorRMS: " + zBottomErrorRMS + ", zBottomErrorLimitRMS: " + zBottomErrorLimitRMS + " [" + iModel + "][" + iNoise + "][" + iShift + "] " + factor + " " + nElements);

/*
              pw.printf("Top:\t%e\t%e\t%e\t%d\t%d\t%d\t%f\t%d\t%b%n",
                        MathUtil.convertUnits(zTopErrorRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        MathUtil.convertUnits(zTopErrorLimitRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        zTopErrorLimitRMS / zTopErrorRMS,
                        iModel,
                        iNoise,
                        iShift,
                        factor,
                        nElements,
                        zTopErrorRMS < zTopErrorLimitRMS);
              pw.printf("Bot:\t%e\t%e\t%e\t%d\t%d\t%d\t%f\t%d\t%b%n",
                        MathUtil.convertUnits(zBottomErrorRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        MathUtil.convertUnits(zBottomErrorLimitRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        zBottomErrorLimitRMS / zBottomErrorRMS,
                        iModel,
                        iNoise,
                        iShift,
                        factor,
                        nElements,
                        zBottomErrorRMS < zBottomErrorLimitRMS);
*/

              if ((maxErrorSearch) && ((maxError < zTopErrorRMS) || (maxError < zBottomErrorRMS)))
              {
                maxError          = Math.max(zTopErrorRMS, zBottomErrorRMS);
                maxLoop           = loop;
                maxWidth          = width;
                maxBoardWidth     = MathUtil.convertUnits(boardWidthNM,               MathUtilEnum.NANOMETERS, MathUtilEnum.INCHES);
                maxBoardHeight    = MathUtil.convertUnits(boardHeightNM,              MathUtilEnum.NANOMETERS, MathUtilEnum.INCHES);
                maxBoardThickness = MathUtil.convertUnits(boardThicknessNM,           MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxOffset         = MathUtil.convertUnits(offset,                     MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxDeltaHeight    = MathUtil.convertUnits(zDeltaOverHeightEdgeToEdge, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxDeltaWidth     = MathUtil.convertUnits(zDeltaOverWidthEdgeToEdge,  MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxSag            = MathUtil.convertUnits(zSagOverWidthEdgeToMiddle,  MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                Assert.expect(MathUtil.fuzzyEquals(noiseTopMean,  noiseBottomMean));
                Assert.expect(MathUtil.fuzzyEquals(noiseTopSigma, noiseBottomSigma));
                maxNoiseMean      = MathUtil.convertUnits(noiseTopMean,               MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxNoiseSigma     = MathUtil.convertUnits(noiseTopSigma,              MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                Assert.expect(MathUtil.fuzzyEquals(liftTopMean,  liftBottomMean));
                Assert.expect(MathUtil.fuzzyEquals(liftTopMean,  dropTopMean));
                Assert.expect(MathUtil.fuzzyEquals(liftTopMean,  dropBottomMean));
                Assert.expect(MathUtil.fuzzyEquals(liftTopSigma, liftBottomSigma));
                Assert.expect(MathUtil.fuzzyEquals(liftTopSigma, dropTopSigma));
                Assert.expect(MathUtil.fuzzyEquals(liftTopSigma, dropBottomSigma));
                maxShiftMean      = MathUtil.convertUnits(liftTopMean,                MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxShiftSigma     = MathUtil.convertUnits(liftTopSigma,               MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxZBoardCenter   = zBoardCenter.getColumnPackedCopy();
                maxZTop1          = copyArray(zTopOriginal);
                maxZBot1          = copyArray(zBotOriginal);
                maxZTop2          = copyArray(zTopEst);
                maxZBot2          = copyArray(zBottomEst);
                maxZTopErr        = copyArray(zTopError);
                maxZBotErr        = copyArray(zBottomError);
              }
            }
          }
        }
      }
    }

    if (maxErrorSearch)
    {
      if (dw != null)  dw.close();
      try { dw = new PrintWriter("C:/temp/test_chooseVerificationHeightWithExcludedData_worst.txt"); } catch (FileNotFoundException e) { dw = null; }
      dw.printf("zBoardCenter (worst) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBoardCenter);
      dw.printf("zTopEst (worst, before VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZTop1);
      dw.printf("zBottomEst (worst, before VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBot1);
      dw.printf("zTopEst (worst, after VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZTop2);
      dw.printf("zBottomEst (worst, after VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBot2);
      dw.printf("zTopError: (worst) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZTopErr);
      dw.printf("zBottomError (worst) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBotErr);
      dw.printf("maxError (RMS in mils): %6.3f%n", MathUtil.convertUnits(maxError, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
      dw.printf("%3.0f x %3.0f x %4.0f, C: %6.3f, dh: %6.3f, hw: %6.3f, sag: %6.3f%n", maxBoardWidth, maxBoardHeight, maxBoardThickness, maxOffset, maxDeltaHeight, maxDeltaWidth, maxSag);
      dw.printf("noise: %6.3f, %6.3f; shift: %6.3f, %6.3f; factor: %6.3f%n", maxNoiseMean, maxNoiseSigma, maxShiftMean, maxShiftSigma, maxFactor);
    }

    if (dw != null)  dw.close();

    if (debug)  pw.printf("test_chooseVerificationHeightWithExcludedData: done%n");
  }

  /**
   * Test of chooseVerificationHeight method using top side only points by:
   *   1. Generate actual board center (model linear in height, quadratic in width).
   *   2. Generate board surfaces based on board center.
   *   3. Add random noise (normal distrubtion) to all z values.
   *   4. Shift randomly selected (uniform distribution) z values up and down by a random amount (normal distribution).
   *   5. Add a large shift to all the bottom side points, but mark them all unused
   *
   * @author Rick Gaudette
   * @author Bob Balliew
   * @author John Heumann
   */
  private void test_chooseVerificationHeightTopSideOnly(BufferedReader br, PrintWriter pw)
  {
    final boolean debug          = false;
    final boolean maxErrorSearch = false;
    final int     maxErrorLoops  = maxErrorSearch ? 250 : 1;
    if (debug)  pw.printf("test_chooseVerificationHeightTopSideOnly: started%n");
    PrintWriter dw = null;

    // Uncomment out the following line to dump information into a file.
    //try { dw = new PrintWriter("C:/temp/test_chooseVerificationHeightTopSideOnly.txt"); } catch (FileNotFoundException e) { dw = null; }

    Assert.expect(br != null);
    Assert.expect(pw != null);

    int maxLoop              = -1;
    int maxWidth             = -1;
    double maxError          =  0.0;
    double maxBoardWidth     = -1.0;
    double maxBoardHeight    = -1.0;
    double maxBoardThickness = -1.0;
    double maxOffset         = -1.0;
    double maxDeltaHeight    = -1.0;
    double maxDeltaWidth     = -1.0;
    double maxSag            = -1.0;
    double maxNoiseMean      = -1.0;
    double maxNoiseSigma     = -1.0;
    double maxShiftMean      = -1.0;
    double maxShiftSigma     = -1.0;
    double maxFactor         = -1.0;
    double[] maxZBoardCenter = null;
    double[] maxZTop1        = null;
    double[] maxZBot1        = null;
    double[] maxZTop2        = null;
    double[] maxZBot2        = null;
    double[] maxZTopErr      = null;
    double[] maxZBotErr      = null;
    double[] zTopOriginal    = null;
    double[] zBotOriginal    = null;
    int loop;

    for (loop = 0; loop < maxErrorLoops; ++loop)
    {
      if (maxErrorSearch)
        System.out.printf("loop: %3d, maxLoop: %3d, maxError: %21.15e%n", loop, maxLoop, MathUtil.convertUnits(maxError, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));

      // in mils, boardWidths, boardHeights, boardThicknesses, offsets, deltaHeights, deltaWidths, and sags are groups element by element.
      double[] boardWidths      = { 4.0,   4.0,   10.0,  18.0};  // in inches
      double[] boardHeights     = { 4.0,   1.0,   12.0,  24.0};  // in inches
      double[] boardThicknesses = {40.0, 125.0,   50.0,  20.0};  // in mils
      double[] offsets          = {10.0,   0.0,  -10.0,   5.0};  // in mils
      double[] deltaHeights     = {-2.5,   0.625, -6.0,  10.0};  // in mils
      double[] deltaWidths      = {-2.5,   2.5,    5.0, -10.0};  // in mils
      double[] sags             = { 0.0,  15.0,  -50.0, -80.0};  // in mils

      // noiseMeans paired with noiseSigmas element by element.
      double[] noiseMeans  = {0.0, 0.0,  2.0, -2.0};  // in mils
      double[] noiseSigmas = {0.0, 5.0, 10.0, 10.0};  // in mils

      // in mils, sweepShiftMeans paired with sweepShiftSigmas element by element.
      double[] sweepShiftMeans  = {0.0, 10.0, 40.0};  // in mils
      double[] sweepShiftSigmas = {0.0,  5.0, 20.0};  // in mils

      double[] sweepFactors = {0.0, 0.1, 0.25};

      Assert.expect(boardWidths.length == boardHeights.length);
      Assert.expect(boardWidths.length == boardThicknesses.length);
      Assert.expect(boardWidths.length == offsets.length);
      Assert.expect(boardWidths.length == deltaHeights.length);
      Assert.expect(boardWidths.length == deltaWidths.length);
      Assert.expect(boardWidths.length == sags.length);

      for (int iModel = 0; iModel < boardWidths.length; ++iModel)
      {
        final double boardWidthNM               = MathUtil.convertUnits(boardWidths[iModel],      MathUtilEnum.INCHES, MathUtilEnum.NANOMETERS);
        final double boardHeightNM              = MathUtil.convertUnits(boardHeights[iModel],     MathUtilEnum.INCHES, MathUtilEnum.NANOMETERS);
        final double boardThicknessNM           = MathUtil.convertUnits(boardThicknesses[iModel], MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double offset                     = MathUtil.convertUnits(offsets[iModel],          MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double zDeltaOverHeightEdgeToEdge = MathUtil.convertUnits(deltaHeights[iModel],     MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double zDeltaOverWidthEdgeToEdge  = MathUtil.convertUnits(deltaWidths[iModel],      MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double zSagOverWidthEdgeToMiddle  = MathUtil.convertUnits(sags[iModel],             MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);

        Assert.expect(noiseMeans.length == noiseSigmas.length);

        for (int iNoise = 0; iNoise < noiseMeans.length; ++iNoise)
        {
          final double noiseTopMean     = MathUtil.convertUnits(noiseMeans[iNoise],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
          final double noiseTopSigma    = MathUtil.convertUnits(noiseSigmas[iNoise], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
          final double noiseBottomMean  = MathUtil.convertUnits(noiseMeans[iNoise],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
          final double noiseBottomSigma = MathUtil.convertUnits(noiseSigmas[iNoise], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

          Assert.expect(sweepShiftMeans.length == sweepShiftSigmas.length);

          for (int iShift = 0; iShift < sweepShiftMeans.length; ++iShift)
          {
            final double liftTopMean     = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double liftTopSigma    = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double liftBottomMean  = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double liftBottomSigma = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropTopMean     = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropTopSigma    = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropBottomMean  = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropBottomSigma = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

            for (double factor : sweepFactors)
            {
              final double liftTopFactor    = factor;
              final double liftBottomFactor = factor;
              final double dropTopFactor    = factor;
              final double dropBottomFactor = factor;

              Assert.expect(noiseTopSigma    >= 0.0);
              Assert.expect(noiseBottomSigma >= 0.0);
              Assert.expect(liftTopMean      >= 0.0);
              Assert.expect(liftTopSigma     >= 0.0);
              Assert.expect(liftTopFactor    >= 0.0);
              Assert.expect(liftBottomMean   >= 0.0);
              Assert.expect(liftBottomSigma  >= 0.0);
              Assert.expect(liftBottomFactor >= 0.0);
              Assert.expect(dropTopMean      >= 0.0);
              Assert.expect(dropTopSigma     >= 0.0);
              Assert.expect(dropTopFactor    >= 0.0);
              Assert.expect(dropBottomMean   >= 0.0);
              Assert.expect(dropBottomSigma  >= 0.0);
              Assert.expect(dropBottomFactor >= 0.0);

              final double approxTileSideLengthNM = MathUtil.convertUnits(322.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
              final int width                     = (int)Math.ceil(boardWidthNM  / approxTileSideLengthNM);
              final int height                    = (int)Math.ceil(boardHeightNM / approxTileSideLengthNM);
              final double tileWidthNM            = boardWidthNM  / (double)width;
              final double tileHeightNM           = boardHeightNM / (double)height;
              final int nElements                 = width * height;
              final double C                      = offset + (zDeltaOverWidthEdgeToEdge / 2.0) + zSagOverWidthEdgeToMiddle; // Correct for Y axis origin shift from edge to center of the board
              final double H1                     = zDeltaOverHeightEdgeToEdge / boardHeightNM;
              final double W1                     = zDeltaOverWidthEdgeToEdge  / boardWidthNM;
              final double W2                     = -zSagOverWidthEdgeToMiddle * 4.0 / (boardWidthNM * boardWidthNM);

              final double zErrorLimitRMS = MathUtil.convertUnits(4.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS) +
                                            MathUtil.convertUnits(50.0 / Math.sqrt((double)nElements), MathUtilEnum.MILS, MathUtilEnum.NANOMETERS) +
                                            ((Math.abs(noiseTopMean) + Math.abs(noiseBottomMean)) * 2.0) +
                                            ((noiseTopSigma + noiseBottomSigma) * 0.5) +
                                           MathUtil.convertUnits((liftTopFactor + liftBottomFactor + dropTopFactor + dropBottomFactor) * 2.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
              final double zTopErrorLimitRMS    = zErrorLimitRMS;
              final double zBottomErrorLimitRMS = zErrorLimitRMS;

              VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

              // Create a model board center.
              Matrix model = new Matrix(4, 1);
              model.set(0, 0, W2);  // second order Y axis coefficient
              model.set(1, 0, W1);  // first order Y axis coefficient
              model.set(2, 0, H1);  // first order X coefficient
              model.set(3, 0, C);   // zeroth order coefficient

              if (debug)
                VerificationSurfaceEstimator.printMatrixInMils(pw, model, "TVSE:Model");

              Matrix fwdOperator = new Matrix(nElements, 4);
              Matrix xDistance   = new Matrix(nElements, 1);
              Matrix yDistance   = new Matrix(nElements, 1);

              for (int i = 0; i < nElements; i++)
              {
                final int widthIndex = i % width;
                final int heightIndex = i / width;
                final double y = ((double)widthIndex  * tileWidthNM)  + (tileWidthNM  / 2.0) - (boardWidthNM / 2.0); // Y axis distance from center of board to center of ith tile (zero based from board's edge).
                final double x = ((double)heightIndex * tileHeightNM) + (tileHeightNM / 2.0); // X axis distance from edge of board to center of ith tile (zero based from board's edge).
                yDistance.set(i, 0, y);
                xDistance.set(i, 0, x);
                fwdOperator.set(i, 0, y * y);
                fwdOperator.set(i, 1, y);
                fwdOperator.set(i, 2, x);
                fwdOperator.set(i, 3, 1.0);
              }

              if (debug)
                VerificationSurfaceEstimator.printMatrixInMils(pw, fwdOperator, "TVSE:Forward operator");

              Matrix zBoardCenter = fwdOperator.times(model);

              if (debug)
                VerificationSurfaceEstimator.printMatrixInMils(pw, zBoardCenter, "TVSE:Board surface");

              if (dw != null)
              {
                dw.printf("zBoardCenter [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBoardCenter.getColumnPackedCopy());
              }

              // Create actual top and bottom board surfaces.
              double[] zTopActual    = new double[nElements]; // Z axis in perpendicular to the plane of the panel.
              double[] zBottomActual = new double[nElements];

              for (int i = 0; i < nElements; i++)
              {
                zTopActual[i]    = zBoardCenter.get(i, 0) + boardThicknessNM / 2.0;
                zBottomActual[i] = zBoardCenter.get(i, 0) - boardThicknessNM / 2.0;
              }

              // Create first pass z focus results.
              double[] zTopEst     = copyArray(zTopActual);
              double[] zBottomEst  = copyArray(zBottomActual);
              double[] noiseTop    = makeValues(nElements, nElements, noiseTopMean, noiseTopSigma);
              double[] noiseBottom = makeValues(nElements, nElements, noiseBottomMean, noiseBottomSigma);

              // Add noise to measured values.
              for (int i = 0; i < nElements; ++i)
              {
                zTopEst[i]    += noiseTop[i];
                zBottomEst[i] += noiseBottom[i];
              }

              // Randomly lift and drop points
              double[] zTopLift    = makeValues(nElements, nElements, +liftTopMean, liftTopSigma);
              double[] zBottomLift = makeValues(nElements, nElements, +liftBottomMean, liftBottomSigma);
              double[] zTopDrop    = makeValues(nElements, nElements, -dropTopMean, dropTopSigma);
              double[] zBottomDrop = makeValues(nElements, nElements, -dropBottomMean, dropBottomSigma);
              boolean[] topExcluded    = makeArray(nElements, false);
              boolean[] bottomExcluded = makeArray(nElements, true);
              int[] iTopLift       = makeValues(0, (int)Math.ceil((double)nElements * liftTopFactor), 0, nElements - 1);
              int[] iBottomLift    = makeValues(0, (int)Math.ceil((double)nElements * liftBottomFactor), 0, nElements - 1);
              int[] iTopDrop       = makeValues(0, (int)Math.ceil((double)nElements * dropTopFactor), 0, nElements - 1);
              int[] iBottomDrop    = makeValues(0, (int)Math.ceil((double)nElements * dropBottomFactor), 0, nElements - 1);
              offsetValues(zTopEst,    iTopLift,    zTopLift);
              offsetValues(zBottomEst, iBottomLift, zBottomLift);
              offsetValues(zTopEst,    iTopDrop,    zTopDrop);
              offsetValues(zBottomEst, iBottomDrop, zBottomDrop);

              for (int i = 0; i < nElements; ++i)
              {
                zBottomEst[i] -= 1270000;
              }

              if (dw != null)
              {
                dw.printf("zTopEst (before VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zTopEst);
                dw.printf("zBottomEst (before VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBottomEst);
              }

              if (maxErrorSearch)
              {
                zTopOriginal = copyArray(zTopEst);
                zBotOriginal = copyArray(zBottomEst);
              }

              final int statusCode = vse.chooseVerificationHeight(zTopEst, zBottomEst, topExcluded, bottomExcluded);

              Expect.expect(statusCode == VerificationSurfaceEstimator.NORMAL);

              if (dw != null)
              {
                dw.printf("zTopEst (after VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zTopEst);
                dw.printf("zBottomEst (after VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBottomEst);
              }

              if (debug)
              {
                VerificationSurfaceEstimator.printArray(pw, zTopActual, "zTopActual");
                VerificationSurfaceEstimator.printArray(pw, zTopEst, "zTopEst");
                VerificationSurfaceEstimator.printArray(pw, zBottomActual, "zBottomActual");
                VerificationSurfaceEstimator.printArray(pw, zBottomEst, "zBottomEst");
              }

              double[] zTopError            = copyArray(zTopEst);
              double[] zBottomError         = copyArray(zBottomEst);
              double zTopErrorSquaredSum    = 0.0;
              double zBottomErrorSquaredSum = 0.0;

              for (int i = 0; i < nElements; i++)
              {
                zTopError[i]           -= zTopActual[i];
                zBottomError[i]        -= zBottomActual[i];
                zTopErrorSquaredSum    += (zTopError[i]    * zTopError[i]);
                zBottomErrorSquaredSum += (zBottomError[i] * zBottomError[i]);
              }

              if (dw != null)
              {
                dw.printf("zTopError: [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zTopError);
                dw.printf("zBottomError [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBottomError);
              }

              double zTopErrorRMS    = Math.sqrt(zTopErrorSquaredSum    / (double)nElements);
              double zBottomErrorRMS = Math.sqrt(zBottomErrorSquaredSum / (double)nElements);

              if (debug)
              {
                pw.printf("zTopErrorRMS:    %+21.15e (%+21.15e mils)%n", zTopErrorRMS,    MathUtil.convertUnits(zTopErrorRMS,    MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
                pw.printf("zBottomErrorRMS: %+21.15e (%+21.15e mils)%n", zBottomErrorRMS, MathUtil.convertUnits(zBottomErrorRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
              }

              Expect.expect(zTopErrorRMS    < zTopErrorLimitRMS,    "zTopErrorRMS: "    + zTopErrorRMS    + ",    zTopErrorLimitRMS: " + zTopErrorLimitRMS    + " [" + iModel + "][" + iNoise + "][" + iShift + "] " + factor + " " + nElements);
              Expect.expect(zBottomErrorRMS < zBottomErrorLimitRMS, "zBottomErrorRMS: " + zBottomErrorRMS + ", zBottomErrorLimitRMS: " + zBottomErrorLimitRMS + " [" + iModel + "][" + iNoise + "][" + iShift + "] " + factor + " " + nElements);

/*
              pw.printf("Top:\t%e\t%e\t%e\t%d\t%d\t%d\t%f\t%d\t%b%n",
                        MathUtil.convertUnits(zTopErrorRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        MathUtil.convertUnits(zTopErrorLimitRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        zTopErrorLimitRMS / zTopErrorRMS,
                        iModel,
                        iNoise,
                        iShift,
                        factor,
                        nElements,
                        zTopErrorRMS < zTopErrorLimitRMS);
              pw.printf("Bot:\t%e\t%e\t%e\t%d\t%d\t%d\t%f\t%d\t%b%n",
                        MathUtil.convertUnits(zBottomErrorRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        MathUtil.convertUnits(zBottomErrorLimitRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        zBottomErrorLimitRMS / zBottomErrorRMS,
                        iModel,
                        iNoise,
                        iShift,
                        factor,
                        nElements,
                        zBottomErrorRMS < zBottomErrorLimitRMS);
*/

              if ((maxErrorSearch) && ((maxError < zTopErrorRMS) || (maxError < zBottomErrorRMS)))
              {
                maxError          = Math.max(zTopErrorRMS, zBottomErrorRMS);
                maxLoop           = loop;
                maxWidth          = width;
                maxBoardWidth     = MathUtil.convertUnits(boardWidthNM,               MathUtilEnum.NANOMETERS, MathUtilEnum.INCHES);
                maxBoardHeight    = MathUtil.convertUnits(boardHeightNM,              MathUtilEnum.NANOMETERS, MathUtilEnum.INCHES);
                maxBoardThickness = MathUtil.convertUnits(boardThicknessNM,           MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxOffset         = MathUtil.convertUnits(offset,                     MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxDeltaHeight    = MathUtil.convertUnits(zDeltaOverHeightEdgeToEdge, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxDeltaWidth     = MathUtil.convertUnits(zDeltaOverWidthEdgeToEdge,  MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxSag            = MathUtil.convertUnits(zSagOverWidthEdgeToMiddle,  MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                Assert.expect(MathUtil.fuzzyEquals(noiseTopMean,  noiseBottomMean));
                Assert.expect(MathUtil.fuzzyEquals(noiseTopSigma, noiseBottomSigma));
                maxNoiseMean      = MathUtil.convertUnits(noiseTopMean,               MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxNoiseSigma     = MathUtil.convertUnits(noiseTopSigma,              MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                Assert.expect(MathUtil.fuzzyEquals(liftTopMean,  liftBottomMean));
                Assert.expect(MathUtil.fuzzyEquals(liftTopMean,  dropTopMean));
                Assert.expect(MathUtil.fuzzyEquals(liftTopMean,  dropBottomMean));
                Assert.expect(MathUtil.fuzzyEquals(liftTopSigma, liftBottomSigma));
                Assert.expect(MathUtil.fuzzyEquals(liftTopSigma, dropTopSigma));
                Assert.expect(MathUtil.fuzzyEquals(liftTopSigma, dropBottomSigma));
                maxShiftMean      = MathUtil.convertUnits(liftTopMean,                MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxShiftSigma     = MathUtil.convertUnits(liftTopSigma,               MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxZBoardCenter   = zBoardCenter.getColumnPackedCopy();
                maxZTop1          = copyArray(zTopOriginal);
                maxZBot1          = copyArray(zBotOriginal);
                maxZTop2          = copyArray(zTopEst);
                maxZBot2          = copyArray(zBottomEst);
                maxZTopErr        = copyArray(zTopError);
                maxZBotErr        = copyArray(zBottomError);
              }
            }
          }
        }
      }
    }

    if (maxErrorSearch)
    {
      if (dw != null)  dw.close();
      try { dw = new PrintWriter("C:/temp/test_chooseVerificationHeightTopSideOnly_worst.txt"); } catch (FileNotFoundException e) { dw = null; }
      dw.printf("zBoardCenter (worst) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBoardCenter);
      dw.printf("zTopEst (worst, before VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZTop1);
      dw.printf("zBottomEst (worst, before VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBot1);
      dw.printf("zTopEst (worst, after VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZTop2);
      dw.printf("zBottomEst (worst, after VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBot2);
      dw.printf("zTopError: (worst) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZTopErr);
      dw.printf("zBottomError (worst) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBotErr);
      dw.printf("maxError (RMS in mils): %6.3f%n", MathUtil.convertUnits(maxError, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
      dw.printf("%3.0f x %3.0f x %4.0f, C: %6.3f, dh: %6.3f, hw: %6.3f, sag: %6.3f%n", maxBoardWidth, maxBoardHeight, maxBoardThickness, maxOffset, maxDeltaHeight, maxDeltaWidth, maxSag);
      dw.printf("noise: %6.3f, %6.3f; shift: %6.3f, %6.3f; factor: %6.3f%n", maxNoiseMean, maxNoiseSigma, maxShiftMean, maxShiftSigma, maxFactor);
    }

    if (dw != null)  dw.close();

    if (debug)  pw.printf("test_chooseVerificationHeightTopSideOnly: done%n");
  }

  /**
   * Test of chooseVerificationHeight method using bottom side only points by:
   *   1. Generate actual board center (model linear in height, quadratic in width).
   *   2. Generate board surfaces based on board center.
   *   3. Add random noise (normal distrubtion) to all z values.
   *   4. Shift randomly selected (uniform distribution) z values up and down by a random amount (normal distribution).
   *   5. Add a large shift to all the top side points, but mark them all unused
   *
   * @author Rick Gaudette
   * @author Bob Balliew
   * @author John Heumann
   */
  private void test_chooseVerificationHeightBottomSideOnly(BufferedReader br, PrintWriter pw)
  {
    final boolean debug          = false;
    final boolean maxErrorSearch = false;
    final int     maxErrorLoops  = maxErrorSearch ? 250 : 1;
    if (debug)  pw.printf("test_chooseVerificationHeightBottomSideOnly: started%n");
    PrintWriter dw = null;

    // Uncomment out the following line to dump information into a file.
    //try { dw = new PrintWriter("C:/temp/test_chooseVerificationHeightBottomsSideOnly.txt"); } catch (FileNotFoundException e) { dw = null; }

    Assert.expect(br != null);
    Assert.expect(pw != null);

    int maxLoop              = -1;
    int maxWidth             = -1;
    double maxError          =  0.0;
    double maxBoardWidth     = -1.0;
    double maxBoardHeight    = -1.0;
    double maxBoardThickness = -1.0;
    double maxOffset         = -1.0;
    double maxDeltaHeight    = -1.0;
    double maxDeltaWidth     = -1.0;
    double maxSag            = -1.0;
    double maxNoiseMean      = -1.0;
    double maxNoiseSigma     = -1.0;
    double maxShiftMean      = -1.0;
    double maxShiftSigma     = -1.0;
    double maxFactor         = -1.0;
    double[] maxZBoardCenter = null;
    double[] maxZTop1        = null;
    double[] maxZBot1        = null;
    double[] maxZTop2        = null;
    double[] maxZBot2        = null;
    double[] maxZTopErr      = null;
    double[] maxZBotErr      = null;
    double[] zTopOriginal    = null;
    double[] zBotOriginal    = null;
    int loop;

    for (loop = 0; loop < maxErrorLoops; ++loop)
    {
      if (maxErrorSearch)
        System.out.printf("loop: %3d, maxLoop: %3d, maxError: %21.15e%n", loop, maxLoop, MathUtil.convertUnits(maxError, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));

      // in mils, boardWidths, boardHeights, boardThicknesses, offsets, deltaHeights, deltaWidths, and sags are groups element by element.
      double[] boardWidths      = { 4.0,   4.0,   10.0,  18.0};  // in inches
      double[] boardHeights     = { 4.0,   1.0,   12.0,  24.0};  // in inches
      double[] boardThicknesses = {40.0, 125.0,   50.0,  20.0};  // in mils
      double[] offsets          = {10.0,   0.0,  -10.0,   5.0};  // in mils
      double[] deltaHeights     = {-2.5,   0.625, -6.0,  10.0};  // in mils
      double[] deltaWidths      = {-2.5,   2.5,    5.0, -10.0};  // in mils
      double[] sags             = { 0.0,  15.0,  -50.0, -80.0};  // in mils

      // noiseMeans paired with noiseSigmas element by element.
      double[] noiseMeans  = {0.0, 0.0,  2.0, -2.0};  // in mils
      double[] noiseSigmas = {0.0, 5.0, 10.0, 10.0};  // in mils

      // in mils, sweepShiftMeans paired with sweepShiftSigmas element by element.
      double[] sweepShiftMeans  = {0.0, 10.0, 40.0};  // in mils
      double[] sweepShiftSigmas = {0.0,  5.0, 20.0};  // in mils

      double[] sweepFactors = {0.0, 0.1, 0.25};

      Assert.expect(boardWidths.length == boardHeights.length);
      Assert.expect(boardWidths.length == boardThicknesses.length);
      Assert.expect(boardWidths.length == offsets.length);
      Assert.expect(boardWidths.length == deltaHeights.length);
      Assert.expect(boardWidths.length == deltaWidths.length);
      Assert.expect(boardWidths.length == sags.length);

      for (int iModel = 0; iModel < boardWidths.length; ++iModel)
      {
        final double boardWidthNM               = MathUtil.convertUnits(boardWidths[iModel],      MathUtilEnum.INCHES, MathUtilEnum.NANOMETERS);
        final double boardHeightNM              = MathUtil.convertUnits(boardHeights[iModel],     MathUtilEnum.INCHES, MathUtilEnum.NANOMETERS);
        final double boardThicknessNM           = MathUtil.convertUnits(boardThicknesses[iModel], MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double offset                     = MathUtil.convertUnits(offsets[iModel],          MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double zDeltaOverHeightEdgeToEdge = MathUtil.convertUnits(deltaHeights[iModel],     MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double zDeltaOverWidthEdgeToEdge  = MathUtil.convertUnits(deltaWidths[iModel],      MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);
        final double zSagOverWidthEdgeToMiddle  = MathUtil.convertUnits(sags[iModel],             MathUtilEnum.MILS,   MathUtilEnum.NANOMETERS);

        Assert.expect(noiseMeans.length == noiseSigmas.length);

        for (int iNoise = 0; iNoise < noiseMeans.length; ++iNoise)
        {
          final double noiseTopMean     = MathUtil.convertUnits(noiseMeans[iNoise],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
          final double noiseTopSigma    = MathUtil.convertUnits(noiseSigmas[iNoise], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
          final double noiseBottomMean  = MathUtil.convertUnits(noiseMeans[iNoise],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
          final double noiseBottomSigma = MathUtil.convertUnits(noiseSigmas[iNoise], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

          Assert.expect(sweepShiftMeans.length == sweepShiftSigmas.length);

          for (int iShift = 0; iShift < sweepShiftMeans.length; ++iShift)
          {
            final double liftTopMean     = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double liftTopSigma    = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double liftBottomMean  = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double liftBottomSigma = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropTopMean     = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropTopSigma    = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropBottomMean  = MathUtil.convertUnits(sweepShiftMeans[iShift],  MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
            final double dropBottomSigma = MathUtil.convertUnits(sweepShiftSigmas[iShift], MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);

            for (double factor : sweepFactors)
            {
              final double liftTopFactor    = factor;
              final double liftBottomFactor = factor;
              final double dropTopFactor    = factor;
              final double dropBottomFactor = factor;

              Assert.expect(noiseTopSigma    >= 0.0);
              Assert.expect(noiseBottomSigma >= 0.0);
              Assert.expect(liftTopMean      >= 0.0);
              Assert.expect(liftTopSigma     >= 0.0);
              Assert.expect(liftTopFactor    >= 0.0);
              Assert.expect(liftBottomMean   >= 0.0);
              Assert.expect(liftBottomSigma  >= 0.0);
              Assert.expect(liftBottomFactor >= 0.0);
              Assert.expect(dropTopMean      >= 0.0);
              Assert.expect(dropTopSigma     >= 0.0);
              Assert.expect(dropTopFactor    >= 0.0);
              Assert.expect(dropBottomMean   >= 0.0);
              Assert.expect(dropBottomSigma  >= 0.0);
              Assert.expect(dropBottomFactor >= 0.0);

              final double approxTileSideLengthNM = MathUtil.convertUnits(322.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
              final int width                     = (int)Math.ceil(boardWidthNM  / approxTileSideLengthNM);
              final int height                    = (int)Math.ceil(boardHeightNM / approxTileSideLengthNM);
              final double tileWidthNM            = boardWidthNM  / (double)width;
              final double tileHeightNM           = boardHeightNM / (double)height;
              final int nElements                 = width * height;
              final double C                      = offset + (zDeltaOverWidthEdgeToEdge / 2.0) + zSagOverWidthEdgeToMiddle; // Correct for Y axis origin shift from edge to center of the board
              final double H1                     = zDeltaOverHeightEdgeToEdge / boardHeightNM;
              final double W1                     = zDeltaOverWidthEdgeToEdge  / boardWidthNM;
              final double W2                     = -zSagOverWidthEdgeToMiddle * 4.0 / (boardWidthNM * boardWidthNM);

              final double zErrorLimitRMS = MathUtil.convertUnits(4.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS) +
                                            MathUtil.convertUnits(50.0 / Math.sqrt((double)nElements), MathUtilEnum.MILS, MathUtilEnum.NANOMETERS) +
                                            ((Math.abs(noiseTopMean) + Math.abs(noiseBottomMean)) * 2.0) +
                                            ((noiseTopSigma + noiseBottomSigma) * 0.5) +
                                           MathUtil.convertUnits((liftTopFactor + liftBottomFactor + dropTopFactor + dropBottomFactor) * 2.0, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS);
              final double zTopErrorLimitRMS    = zErrorLimitRMS;
              final double zBottomErrorLimitRMS = zErrorLimitRMS;

              VerificationSurfaceEstimator vse = new VerificationSurfaceEstimator(width, height, boardThicknessNM, debug);

              // Create a model board center.
              Matrix model = new Matrix(4, 1);
              model.set(0, 0, W2);  // second order Y axis coefficient
              model.set(1, 0, W1);  // first order Y axis coefficient
              model.set(2, 0, H1);  // first order X coefficient
              model.set(3, 0, C);   // zeroth order coefficient

              if (debug)
                VerificationSurfaceEstimator.printMatrixInMils(pw, model, "TVSE:Model");

              Matrix fwdOperator = new Matrix(nElements, 4);
              Matrix xDistance   = new Matrix(nElements, 1);
              Matrix yDistance   = new Matrix(nElements, 1);

              for (int i = 0; i < nElements; i++)
              {
                final int widthIndex = i % width;
                final int heightIndex = i / width;
                final double y = ((double)widthIndex  * tileWidthNM)  + (tileWidthNM  / 2.0) - (boardWidthNM / 2.0); // Y axis distance from center of board to center of ith tile (zero based from board's edge).
                final double x = ((double)heightIndex * tileHeightNM) + (tileHeightNM / 2.0); // X axis distance from edge of board to center of ith tile (zero based from board's edge).
                yDistance.set(i, 0, y);
                xDistance.set(i, 0, x);
                fwdOperator.set(i, 0, y * y);
                fwdOperator.set(i, 1, y);
                fwdOperator.set(i, 2, x);
                fwdOperator.set(i, 3, 1.0);
              }

              if (debug)
                VerificationSurfaceEstimator.printMatrixInMils(pw, fwdOperator, "TVSE:Forward operator");

              Matrix zBoardCenter = fwdOperator.times(model);

              if (debug)
                VerificationSurfaceEstimator.printMatrixInMils(pw, zBoardCenter, "TVSE:Board surface");

              if (dw != null)
              {
                dw.printf("zBoardCenter [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBoardCenter.getColumnPackedCopy());
              }

              // Create actual top and bottom board surfaces.
              double[] zTopActual    = new double[nElements]; // Z axis in perpendicular to the plane of the panel.
              double[] zBottomActual = new double[nElements];

              for (int i = 0; i < nElements; i++)
              {
                zTopActual[i]    = zBoardCenter.get(i, 0) + boardThicknessNM / 2.0;
                zBottomActual[i] = zBoardCenter.get(i, 0) - boardThicknessNM / 2.0;
              }

              // Create first pass z focus results.
              double[] zTopEst     = copyArray(zTopActual);
              double[] zBottomEst  = copyArray(zBottomActual);
              double[] noiseTop    = makeValues(nElements, nElements, noiseTopMean, noiseTopSigma);
              double[] noiseBottom = makeValues(nElements, nElements, noiseBottomMean, noiseBottomSigma);

              // Add noise to measured values.
              for (int i = 0; i < nElements; ++i)
              {
                zTopEst[i]    += noiseTop[i];
                zBottomEst[i] += noiseBottom[i];
              }

              // Randomly lift and drop points
              double[] zTopLift    = makeValues(nElements, nElements, +liftTopMean, liftTopSigma);
              double[] zBottomLift = makeValues(nElements, nElements, +liftBottomMean, liftBottomSigma);
              double[] zTopDrop    = makeValues(nElements, nElements, -dropTopMean, dropTopSigma);
              double[] zBottomDrop = makeValues(nElements, nElements, -dropBottomMean, dropBottomSigma);
              boolean[] topExcluded    = makeArray(nElements, true);
              boolean[] bottomExcluded = makeArray(nElements, false);
              int[] iTopLift       = makeValues(0, (int)Math.ceil((double)nElements * liftTopFactor), 0, nElements - 1);
              int[] iBottomLift    = makeValues(0, (int)Math.ceil((double)nElements * liftBottomFactor), 0, nElements - 1);
              int[] iTopDrop       = makeValues(0, (int)Math.ceil((double)nElements * dropTopFactor), 0, nElements - 1);
              int[] iBottomDrop    = makeValues(0, (int)Math.ceil((double)nElements * dropBottomFactor), 0, nElements - 1);
              offsetValues(zTopEst,    iTopLift,    zTopLift);
              offsetValues(zBottomEst, iBottomLift, zBottomLift);
              offsetValues(zTopEst,    iTopDrop,    zTopDrop);
              offsetValues(zBottomEst, iBottomDrop, zBottomDrop);

              for (int i = 0; i < nElements; ++i)
              {
                zTopEst[i] += 1270000;
              }

              if (dw != null)
              {
                dw.printf("zTopEst (before VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zTopEst);
                dw.printf("zBottomEst (before VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBottomEst);
              }

              if (maxErrorSearch)
              {
                zTopOriginal = copyArray(zTopEst);
                zBotOriginal = copyArray(zBottomEst);
              }

              final int statusCode = vse.chooseVerificationHeight(zTopEst, zBottomEst, topExcluded, bottomExcluded);

              Expect.expect(statusCode == VerificationSurfaceEstimator.NORMAL);

              if (dw != null)
              {
                dw.printf("zTopEst (after VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zTopEst);
                dw.printf("zBottomEst (after VSE) [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBottomEst);
              }

              if (debug)
              {
                VerificationSurfaceEstimator.printArray(pw, zTopActual, "zTopActual");
                VerificationSurfaceEstimator.printArray(pw, zTopEst, "zTopEst");
                VerificationSurfaceEstimator.printArray(pw, zBottomActual, "zBottomActual");
                VerificationSurfaceEstimator.printArray(pw, zBottomEst, "zBottomEst");
              }

              double[] zTopError            = copyArray(zTopEst);
              double[] zBottomError         = copyArray(zBottomEst);
              double zTopErrorSquaredSum    = 0.0;
              double zBottomErrorSquaredSum = 0.0;

              for (int i = 0; i < nElements; i++)
              {
                zTopError[i]           -= zTopActual[i];
                zBottomError[i]        -= zBottomActual[i];
                zTopErrorSquaredSum    += (zTopError[i]    * zTopError[i]);
                zBottomErrorSquaredSum += (zBottomError[i] * zBottomError[i]);
              }

              if (dw != null)
              {
                dw.printf("zTopError: [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zTopError);
                dw.printf("zBottomError [%d][%d][%d] %f%%%n", iModel, iNoise, iShift, factor * 100.0);
                printZ(dw, width, zBottomError);
              }

              double zTopErrorRMS    = Math.sqrt(zTopErrorSquaredSum    / (double)nElements);
              double zBottomErrorRMS = Math.sqrt(zBottomErrorSquaredSum / (double)nElements);

              if (debug)
              {
                pw.printf("zTopErrorRMS:    %+21.15e (%+21.15e mils)%n", zTopErrorRMS,    MathUtil.convertUnits(zTopErrorRMS,    MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
                pw.printf("zBottomErrorRMS: %+21.15e (%+21.15e mils)%n", zBottomErrorRMS, MathUtil.convertUnits(zBottomErrorRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
              }

              Expect.expect(zTopErrorRMS    < zTopErrorLimitRMS,    "zTopErrorRMS: "    + zTopErrorRMS    + ",    zTopErrorLimitRMS: " + zTopErrorLimitRMS    + " [" + iModel + "][" + iNoise + "][" + iShift + "] " + factor + " " + nElements);
              Expect.expect(zBottomErrorRMS < zBottomErrorLimitRMS, "zBottomErrorRMS: " + zBottomErrorRMS + ", zBottomErrorLimitRMS: " + zBottomErrorLimitRMS + " [" + iModel + "][" + iNoise + "][" + iShift + "] " + factor + " " + nElements);

/*
              pw.printf("Top:\t%e\t%e\t%e\t%d\t%d\t%d\t%f\t%d\t%b%n",
                        MathUtil.convertUnits(zTopErrorRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        MathUtil.convertUnits(zTopErrorLimitRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        zTopErrorLimitRMS / zTopErrorRMS,
                        iModel,
                        iNoise,
                        iShift,
                        factor,
                        nElements,
                        zTopErrorRMS < zTopErrorLimitRMS);
              pw.printf("Bot:\t%e\t%e\t%e\t%d\t%d\t%d\t%f\t%d\t%b%n",
                        MathUtil.convertUnits(zBottomErrorRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        MathUtil.convertUnits(zBottomErrorLimitRMS, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS),
                        zBottomErrorLimitRMS / zBottomErrorRMS,
                        iModel,
                        iNoise,
                        iShift,
                        factor,
                        nElements,
                        zBottomErrorRMS < zBottomErrorLimitRMS);
*/

              if ((maxErrorSearch) && ((maxError < zTopErrorRMS) || (maxError < zBottomErrorRMS)))
              {
                maxError          = Math.max(zTopErrorRMS, zBottomErrorRMS);
                maxLoop           = loop;
                maxWidth          = width;
                maxBoardWidth     = MathUtil.convertUnits(boardWidthNM,               MathUtilEnum.NANOMETERS, MathUtilEnum.INCHES);
                maxBoardHeight    = MathUtil.convertUnits(boardHeightNM,              MathUtilEnum.NANOMETERS, MathUtilEnum.INCHES);
                maxBoardThickness = MathUtil.convertUnits(boardThicknessNM,           MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxOffset         = MathUtil.convertUnits(offset,                     MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxDeltaHeight    = MathUtil.convertUnits(zDeltaOverHeightEdgeToEdge, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxDeltaWidth     = MathUtil.convertUnits(zDeltaOverWidthEdgeToEdge,  MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxSag            = MathUtil.convertUnits(zSagOverWidthEdgeToMiddle,  MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                Assert.expect(MathUtil.fuzzyEquals(noiseTopMean,  noiseBottomMean));
                Assert.expect(MathUtil.fuzzyEquals(noiseTopSigma, noiseBottomSigma));
                maxNoiseMean      = MathUtil.convertUnits(noiseTopMean,               MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxNoiseSigma     = MathUtil.convertUnits(noiseTopSigma,              MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                Assert.expect(MathUtil.fuzzyEquals(liftTopMean,  liftBottomMean));
                Assert.expect(MathUtil.fuzzyEquals(liftTopMean,  dropTopMean));
                Assert.expect(MathUtil.fuzzyEquals(liftTopMean,  dropBottomMean));
                Assert.expect(MathUtil.fuzzyEquals(liftTopSigma, liftBottomSigma));
                Assert.expect(MathUtil.fuzzyEquals(liftTopSigma, dropTopSigma));
                Assert.expect(MathUtil.fuzzyEquals(liftTopSigma, dropBottomSigma));
                maxShiftMean      = MathUtil.convertUnits(liftTopMean,                MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxShiftSigma     = MathUtil.convertUnits(liftTopSigma,               MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
                maxZBoardCenter   = zBoardCenter.getColumnPackedCopy();
                maxZTop1          = copyArray(zTopOriginal);
                maxZBot1          = copyArray(zBotOriginal);
                maxZTop2          = copyArray(zTopEst);
                maxZBot2          = copyArray(zBottomEst);
                maxZTopErr        = copyArray(zTopError);
                maxZBotErr        = copyArray(zBottomError);
              }
            }
          }
        }
      }
    }

    if (maxErrorSearch)
    {
      if (dw != null)  dw.close();
      try { dw = new PrintWriter("C:/temp/test_chooseVerificationHeightBottomSideOnly_worst.txt"); } catch (FileNotFoundException e) { dw = null; }
      dw.printf("zBoardCenter (worst) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBoardCenter);
      dw.printf("zTopEst (worst, before VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZTop1);
      dw.printf("zBottomEst (worst, before VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBot1);
      dw.printf("zTopEst (worst, after VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZTop2);
      dw.printf("zBottomEst (worst, after VSE) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBot2);
      dw.printf("zTopError: (worst) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZTopErr);
      dw.printf("zBottomError (worst) [%d]%n", maxLoop);
      printZ(dw, maxWidth, maxZBotErr);
      dw.printf("maxError (RMS in mils): %6.3f%n", MathUtil.convertUnits(maxError, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
      dw.printf("%3.0f x %3.0f x %4.0f, C: %6.3f, dh: %6.3f, hw: %6.3f, sag: %6.3f%n", maxBoardWidth, maxBoardHeight, maxBoardThickness, maxOffset, maxDeltaHeight, maxDeltaWidth, maxSag);
      dw.printf("noise: %6.3f, %6.3f; shift: %6.3f, %6.3f; factor: %6.3f%n", maxNoiseMean, maxNoiseSigma, maxShiftMean, maxShiftSigma, maxFactor);
    }

    if (dw != null)  dw.close();

    if (debug)  pw.printf("test_chooseVerificationHeightBottomSideOnly: done%n");
  }

  /**
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  public void test(BufferedReader br, PrintWriter pw)
  {
    Assert.expect(br != null);
    Assert.expect(pw != null);

    try
    {
      Config.getInstance().loadIfNecessary();
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }

    try
    {
      if (! _twoPassVerificationSurfaceModelingEnabled)
        Config.getInstance().setValueInMemoryOnly(SoftwareConfigEnum.TWO_PASS_VERIFICATION_SURFACE_MODELING_ENABLED, new Boolean(true));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }

    test_countTrue(br, pw);
    test_swapInvertedPairs(br, pw);
    test_cullOutliers(br, pw);
    test_combinedValidMedian(br, pw);
    test_VerificationSurfaceEstimator(br, pw);
    test_findCloseNeighbors(br, pw);
    test_refineBoardThickness(br, pw);
    test_chooseVerificationHeight(br, pw);
    test_chooseVerificationHeightWithExcludedData(br, pw);
    test_chooseVerificationHeightTopSideOnly(br, pw);
    test_chooseVerificationHeightBottomSideOnly(br, pw);

    try
    {
      if (_twoPassVerificationSurfaceModelingEnabled != Config.getInstance().getBooleanValue(SoftwareConfigEnum.TWO_PASS_VERIFICATION_SURFACE_MODELING_ENABLED))
        Config.getInstance().setValueInMemoryOnly(SoftwareConfigEnum.TWO_PASS_VERIFICATION_SURFACE_MODELING_ENABLED, new Boolean(_twoPassVerificationSurfaceModelingEnabled));
    }
    catch (DatastoreException e)
    {
      Expect.expect(false, e.getMessage());
    }
  }
}
