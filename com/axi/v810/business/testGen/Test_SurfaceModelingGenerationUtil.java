package com.axi.v810.business.testGen;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;

/**
 * @author George A. David
 */
public class Test_SurfaceModelingGenerationUtil extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_SurfaceModelingGenerationUtil());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    TestSubProgram subProgram = new TestSubProgram(new TestProgram());

    int numIrps = 1;
    int numRows = 10;
    int numColumns = 10;
    int regionWidth = MathUtil.convertMilsToNanoMetersInteger(500);
    int regionHeight = MathUtil.convertMilsToNanoMetersInteger(500);
    int spacing = MathUtil.convertMilsToNanoMetersInteger(100);
    // create a 10 x 10 grid of reconstruction regions
    // with widht of 1 and a spacing of 0
    boolean done = false;
    while (done == false)
    {
      List<ReconstructionRegion> regions = createGrid(subProgram,
                                                      numIrps,
                                                      numRows,
                                                      numColumns,
                                                      regionWidth,
                                                      regionHeight,
                                                      spacing);

      SurfaceModelingGenerationUtil modelingUtil = new SurfaceModelingGenerationUtil();
      modelingUtil.setReconstructionRegions(subProgram, regions);
      for(ReconstructionRegion region: regions)
      {
        Collection<ReconstructionRegion> neighbors = modelingUtil.getSurroundingSortedReconstructionRegions(region);
        System.out.println("numIrps: " + numIrps +
                           "   spacing: " + MathUtil.convertNanoMetersToMils(spacing) +
                           "   regionWidth: " + MathUtil.convertNanoMetersToMils(regionWidth) +
                           "   regionHeight: " + MathUtil.convertNanoMetersToMils(regionHeight));
        System.out.println("Region " + region.getRegionId() + " has the following neighbors:");
        for (ReconstructionRegion neighbor : neighbors)
        {
          System.out.print(neighbor.getRegionId() + " ");
        }

        System.out.println();
        System.out.println();
      }
      ++numIrps;
      if (numIrps > 4)
      {
        numIrps = 1;
        regionWidth -= MathUtil.convertMilsToNanoMeters(100);
        regionHeight -= MathUtil.convertMilsToNanoMeters(100);
        spacing -= MathUtil.convertMilsToNanoMeters(10);

        if (regionWidth <= 0 || regionHeight <= 0)
          done = true;
      }
    }

  //    System.out.print("new double[]{");
  //    for (int i = 0; i < 100; i++)
  //    {
  //      System.out.println(Math.random() + ", ");
  //    }
  //
  //    System.out.println("};");

    // ok, now let's create a psuedo random grid
    double[] xMultipliers = new double[]
                            {0.7596701318962482,
                            0.29967046823033483,
                            0.7698021018645738,
                            0.6809667789659755,
                            0.6867761143003114,
                            0.5382324419141183,
                            0.17214737355002008,
                            0.4312380399394452,
                            0.07746636752686908,
                            0.6668305778135795,
                            0.652879351405121,
                            0.9280509814230696,
                            0.5884391172168988,
                            0.920218105835497,
                            0.8651449054204644,
                            0.2885342783917183,
                            0.3047513441655053,
                            0.5994301053972957,
                            0.5779658065128462,
                            0.2702766749964781,
                            0.7060777887211745,
                            0.33960518879856294,
                            0.10824979054944917,
                            0.7685612711989513,
                            0.4831830651499964,
                            0.19558752734432716,
                            0.1378201438801665,
                            0.38749876549554396,
                            0.2959458781259827,
                            0.9640410670835621,
                            0.8354158761988496,
                            0.758186486017762,
                            0.35431358397569745,
                            0.6149092838780811,
                            0.3842792079674343,
                            0.7467454529376797,
                            0.524854476616811,
                            0.44040513153131966,
                            0.7278607761764555,
                            0.9460652738464167,
                            0.5181135913382839,
                            0.5463175528310669,
                            0.5893369353812481,
                            0.8773122976894756,
                            0.5957681759825354,
                            0.6082726939671514,
                            0.595048337186971,
                            0.1407341205462066,
                            0.15161648053324017,
                            0.39468904281633665,
                            0.6376317943380031,
                            0.3500187627819409,
                            0.46672382502759924,
                            0.41851784447809903,
                            0.4515328201524109,
                            0.060444732978779836,
                            0.9165884176402106,
                            0.2214611427258636,
                            0.741749277748058,
                            0.05828373787330121,
                            0.9169783001977462,
                            0.21721187398165576,
                            0.10607119533888221,
                            0.3173949977089232,
                            0.9351304996108531,
                            0.7243615744700924,
                            0.4217456196521532,
                            0.6486761666278773,
                            0.964291621269748,
                            0.8049732245983445,
                            0.32940971543345177,
                            0.5690131817731197,
                            0.9621192510168911,
                            0.7914178337086345,
                            0.940298804158878,
                            0.19859685830137175,
                            0.8872514486576333,
                            0.9319254705076048,
                            0.9171510685505402,
                            0.058951704036017016,
                            0.13339177354499987,
                            0.4144026325415663,
                            0.6348934314955932,
                            0.48874808958882887,
                            0.3565454347468916,
                            0.13841937375573632,
                            0.21410847647721876,
                            0.02044639214971966,
                            0.9185818511492931,
                            0.9968716661642063,
                            0.8384453836617387,
                            0.46804696576688576,
                            0.4290876073701897,
                            0.10167534633200459,
                            0.9165525409479507,
                            0.6646980677280971,
                            0.15751304927655874,
                            0.3419499547077337,
                            0.32977284684635,
                            0.7586388566139342};


    double[] yMultipliers = new double[]
                            {0.6208164344921722,
                            0.39474218605103184,
                            0.20777753055657178,
                            0.49939947022491127,
                            0.8298840242330666,
                            0.7525864102179421,
                            0.5791266620459746,
                            0.6952606092283068,
                            0.8461627134672327,
                            0.8229138969599358,
                            0.6515759505339065,
                            0.4303741275129752,
                            0.9392556468710029,
                            0.5978048683182681,
                            0.005704027966083158,
                            0.019660503096167292,
                            0.7494611990098494,
                            0.5404983023782958,
                            0.537816593245794,
                            0.35295249691631514,
                            0.07953226952991177,
                            0.3113793748906408,
                            0.40403861434993615,
                            0.5878484146600134,
                            0.5367020367119941,
                            0.45407374300720715,
                            0.6491606297787644,
                            0.7129662265277195,
                            0.7918522747767773,
                            0.7667769132946065,
                            0.9868229120960854,
                            0.42596420627344267,
                            0.6924274050020366,
                            0.9052357754757197,
                            0.932971904679858,
                            0.4826092006314232,
                            0.7166730126957216,
                            0.6163388843429823,
                            0.511125550666056,
                            0.9163368683505391,
                            0.2890053168508272,
                            0.8850604021417072,
                            0.04583539703610162,
                            0.30216230413411216,
                            0.2897937949592755,
                            0.19254261856013022,
                            0.13983483916078687,
                            0.5444643123231213,
                            0.6875658818194311,
                            0.02939014351201119,
                            0.790714854993406,
                            0.505079610461357,
                            0.5976196778808005,
                            0.7857885272153214,
                            0.7445695286822357,
                            0.15630792571464014,
                            0.2282564594161285,
                            0.14884892277913775,
                            0.6069058456240178,
                            0.8428430325063988,
                            0.11432204259523071,
                            0.1776193775811835,
                            0.935213211115285,
                            0.6673084613937144,
                            0.7574719745428168,
                            0.29749243605165854,
                            0.5229404210734693,
                            0.9759910494320206,
                            0.29355259518820287,
                            0.5582780921855379,
                            0.35122843046366303,
                            0.4145293450572629,
                            0.08971646091998453,
                            0.6339997468075992,
                            0.23259981425951348,
                            0.04772801992120479,
                            0.2373666831620347,
                            0.20751005831771985,
                            0.9999067654128241,
                            0.5933335781254497,
                            0.7517523342383887,
                            0.579312949511315,
                            0.20419108938250485,
                            0.9065204024824666,
                            0.915012409497588,
                            0.567516369909431,
                            0.6767114435412411,
                            0.4700007596826553,
                            0.6066934385748629,
                            0.024630529164431647,
                            0.983244451092359,
                            0.7940859687224703,
                            0.6542417163719592,
                            0.5869641296122468,
                            0.7528681274548645,
                            0.31169015877753126,
                            0.20569812195123016,
                            0.5936612542807707,
                            0.09878696620819472,
                            0.5651823376423989};

    numIrps = 1;
    regionWidth = MathUtil.convertMilsToNanoMetersInteger(1000);
    regionHeight = MathUtil.convertMilsToNanoMetersInteger(1000);

    int totalWidth = MathUtil.convertMilsToNanoMetersInteger(10000);
    int totalHeight = MathUtil.convertMilsToNanoMetersInteger(20000);
    // create a 10 x 10 grid of reconstruction regions
    // with widht of 1 and a spacing of 0
    done = false;
    while (done == false)
    {
      List<ReconstructionRegion> regions = createPsuedoRandomGrid(subProgram,
                                                                  numIrps,
                                                                  totalWidth,
                                                                  totalHeight,
                                                                  regionWidth,
                                                                  regionHeight,
                                                                  xMultipliers,
                                                                  yMultipliers);

      SurfaceModelingGenerationUtil modelingUtil = new SurfaceModelingGenerationUtil();
      modelingUtil.setReconstructionRegions(subProgram, regions);
      for(ReconstructionRegion region : regions)
      {
        Collection<ReconstructionRegion> neighbors = modelingUtil.getSurroundingSortedReconstructionRegions(region);
        System.out.println("numIrps: " + numIrps +
                           "   regionWidth: " + MathUtil.convertNanoMetersToMils(regionWidth) +
                           "   regionHeight: " + MathUtil.convertNanoMetersToMils(regionHeight));
        System.out.println("Region " + region.getRegionId() + " has the following neighbors:");
        for (ReconstructionRegion neighbor : neighbors)
        {
          System.out.print(neighbor.getRegionId() + " ");
        }

        System.out.println();
        System.out.println();
      }
      ++numIrps;
      if (numIrps > 4)
      {
        numIrps = 1;
        regionWidth -= MathUtil.convertMilsToNanoMeters(100);
        regionHeight -= MathUtil.convertMilsToNanoMeters(100);

        if (regionWidth <= 0 || regionHeight <= 0)
          done = true;
      }
    }
  }

  /**
   * @author George A. David
   * @author Rex Shang
   */
  private List<ProcessorStrip> createProcessorRectangles(int numIrps, int totalWidth, int totalHeight, int regionWidth)
  {
    double processorRectWidth = (double)totalWidth / (double)numIrps;
    // we want there to be an overlap of (spacing + width)
    List<ProcessorStrip> processorStrips = new ArrayList<ProcessorStrip>();
    double buffer = regionWidth / 2.0;

    for (int i = 0; i < numIrps; ++i)
    {
      double x = processorRectWidth * i;
      double width = processorRectWidth;
      if (i == 0 && i + 1 != numIrps)
      {
        width = processorRectWidth + buffer;
      }
      else if (i + 1 == numIrps && i != 0)
      {
        x -= buffer;
        width = processorRectWidth + buffer;
      }
      else
      {
        x -= buffer;
        width = processorRectWidth + (2 * buffer);
      }

      ProcessorStrip processorStrip = new ProcessorStrip(0);
      processorStrip.setReconstructionEngineId(ImageReconstructionEngineEnum.getEnumList().get(i));
      processorStrip.setRegion(new PanelRectangle(new Rectangle2D.Double(x, 0, width, totalHeight)));
      processorStrips.add(processorStrip);
    }

    return processorStrips;
  }

  /**
   * @author George A. David
   */
  private List<ReconstructionRegion> createGrid(TestSubProgram subProgram, int numIrps, int numRows, int numColumns, int regionWidth, int regionHeight, int spacing)
  {
    // create the processorStripRectangles
    int totalWidth = (regionWidth * numColumns) + (spacing * (numColumns - 1));
    int totalHeight = (regionHeight * numRows) + (spacing * (numRows - 1));

    List<ProcessorStrip> processorStrips = createProcessorRectangles(numIrps, totalWidth, totalHeight, regionWidth);
    subProgram.setProcessorStripsForInspectionRegions(processorStrips);

    List<ReconstructionRegion> regions = new ArrayList<ReconstructionRegion>(numColumns * numRows);
    int id = 1;
    for (int yIndex = 0; yIndex < numRows; yIndex++)
    {
      for (int xIndex = 0; xIndex < numColumns; xIndex++)
      {
        ReconstructionRegion region = new ReconstructionRegion();
        PanelRectangle rect = new PanelRectangle((xIndex) * (spacing + regionWidth), yIndex * (spacing + regionHeight), regionWidth, regionHeight);
        region.setRegionRectangleRelativeToPanelInNanoMeters(rect);
        region.setRegionId(id);
        ++id;

        // now create a surface modleing slice
        Slice slice = new Slice();
        slice.setSliceType(SliceTypeEnum.SURFACE);
        slice.setSliceName(SliceNameEnum.SURFACE_MODELING);
        FocusGroup group = new FocusGroup();
        group.addSlice(slice);
        FocusRegion focusRegion = new FocusRegion();
        focusRegion.setRectangleRelativeToPanelInNanoMeters(rect);
        FocusSearchParameters params = new FocusSearchParameters();
        params.setFocusRegion(focusRegion);
        group.setFocusSearchParameters(params);

        region.addFocusGroup(group);

        for (ProcessorStrip processorStrip : processorStrips)
        {
          if (processorStrip.getRegion().contains(rect))
          {
            region.setReconstructionEngineId(processorStrip.getReconstructionEngineId());
            break;
          }
        }

        regions.add(region);
      }
    }

    return regions;
  }

  /**
   * @author George A. David
   */
  private List<ReconstructionRegion> createPsuedoRandomGrid(TestSubProgram subProgram, int numIrps, int totalWidth, int totalHeight, int maxRegionWidth, int maxRegionHeight, double[] xMultipliers, double[] yMultipliers)
  {
    Assert.expect(xMultipliers.length == yMultipliers.length);

    int numRegions = xMultipliers.length;
    List<ProcessorStrip> processorStrips = createProcessorRectangles(numIrps, totalWidth, totalHeight, maxRegionWidth);
    subProgram.setProcessorStripsForInspectionRegions(processorStrips);

    List<ReconstructionRegion> regions = new ArrayList<ReconstructionRegion>(numRegions);
    int id = 1;
    for (int i = 0; i < numRegions; i++)
    {

      ReconstructionRegion region = new ReconstructionRegion();
      double x = totalWidth * xMultipliers[i];
      double y = totalWidth * yMultipliers[i];
      double width = maxRegionWidth * (xMultipliers[i] + yMultipliers[i]) / 2.0;
      double height = maxRegionHeight * (xMultipliers[i] + yMultipliers[i]) / 2.0;
      PanelRectangle rect = new PanelRectangle(new Rectangle2D.Double(x, y, width, height));
      region.setRegionRectangleRelativeToPanelInNanoMeters(rect);
      region.setRegionId(id);
      ++id;

      // now create a surface modleing slice
      Slice slice = new Slice();
      slice.setSliceType(SliceTypeEnum.SURFACE);
      slice.setSliceName(SliceNameEnum.SURFACE_MODELING);
      FocusGroup group = new FocusGroup();
      group.addSlice(slice);
      FocusRegion focusRegion = new FocusRegion();
      focusRegion.setRectangleRelativeToPanelInNanoMeters(rect);
      FocusSearchParameters params = new FocusSearchParameters();
      params.setFocusRegion(focusRegion);
      group.setFocusSearchParameters(params);

      region.addFocusGroup(group);

      boolean fits = false;
      for (ProcessorStrip processorStrip : processorStrips)
      {
        if (processorStrip.getRegion().contains(rect))
        {
          region.setReconstructionEngineId(processorStrip.getReconstructionEngineId());
          fits = true;
          break;
        }
      }

      if(fits)
        regions.add(region);
    }

    return regions;
  }
}



