package com.axi.v810.business.testGen;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.util.*;


/**
 * VirtualLiveProgramGeneration handles the initial program generation for VirtualLive mode.
 * The sole TestSubProgram is composed of reconstructionRegions tiled accross the panel in the X-Y plane.
 * The testSubProgram is used by the VirtualLiveImageReconstructionManger to ferry re-reconstruction
 * requests through the IAE/IRP. It is composed of a static number of reconstruction layers, each with a single
 * slice (or some constant number of slices, each with a unique slice name) per reconstructionRegion.
 * @author Scott Richardson
 */
public class VirtualLiveProgramGeneration extends ProgramGeneration
{
  public static int _ZSTEP_BETWEEN_SLICES_IN_NANOMETERS = (int)MathUtil.convertMilsToNanoMeters(2.0);
  public static int _ZOFFSET_OF_RECONSTRUCTION_LAYER_IN_NANOMETERS = (int)MathUtil.convertMilsToNanoMeters(0.0);
  private static VirtualLiveProgramGeneration _virtualLiveProgramGeneration;

  // Currently, only a single slice can be re-reconstructed at a time. In order to re-reconstruct more than a single
  // slice at a time, the unique slice names must be added to the SliceNameEnum
  private int _numberOfSlicesInReconstructionLayer = 1;
  private int _numberOfReconstructionLayers = 3;

  private Map<VirtualLiveReconstructionColumn, Set<ReconstructionRegion>> _reconstructionColumnWorkers =
      new HashMap<VirtualLiveReconstructionColumn, Set<ReconstructionRegion>>();

  /**
   * @author Scott Richardson
   */
  private VirtualLiveProgramGeneration()
  {
    // ensure that we always have an odd number of slices and reconstruction layers; that way, there will be a equal
    // number of slices above and below the center slice - which is focused at the _zOffsetOfReconstructionLayersInNanoMeters
    if (_numberOfSlicesInReconstructionLayer % 2 == 0)
    {
      _numberOfSlicesInReconstructionLayer += 1;
    }
    if (_numberOfReconstructionLayers % 2 == 0)
    {
      _numberOfReconstructionLayers += 1;
    }
  }

  /**
   * @author Scott Richardson
   * @todo sgr it should not be possible to have an instance of ProgramGeneration and VirtualLiveProgramGeneration. revise.
   */
  public static synchronized VirtualLiveProgramGeneration getInstance()
  {
    if (_virtualLiveProgramGeneration == null)
    {
      _virtualLiveProgramGeneration = new VirtualLiveProgramGeneration();
    }

    return _virtualLiveProgramGeneration;
  }

  /**
   * @author Scott Richardson
   */
  public TestProgram generateVirtualLiveTestProgram(Project project)
  {
    Assert.expect(project != null);

    initialize();

    // create the testSubProgram
    _project = project;
    _testProgram = new TestProgram();
    _testProgram.setProject(_project);

    createTestSubProgram();

    // now define the processor strip rectangles for verification regions
    _imageAcquisitionEngine.defineProcessorStripsForVerificationRegions(_testProgram);

    constructVirtualLiveReconstructionRegions();

    constructVirtualLiveReconstructionColumns();

    return _testProgram;
  }

  /**
   * @author Scott Richardson
   */
  private void constructVirtualLiveReconstructionRegions()
  {
    ArrayList<LinkedList<ReconstructionRegion>> perTestSubProgramReconstructionRegions = new ArrayList<LinkedList<ReconstructionRegion>>();
    for(int j = 0; j < _testProgram.getAllTestSubPrograms().size(); j++)
    {
      perTestSubProgramReconstructionRegions.add(new LinkedList<ReconstructionRegion>());
    }

    // Construct a deck of reconstruction layers centered at the _zOffsetOfReconstructionLayerInNanoMeters.
    _numberOfSlicesInReconstructionLayer = 1;
    for (int i = 0; i < _numberOfReconstructionLayers; i++)
    {
      /** @todo sgr debug */
      // although initially the reconstructionLayers do not overlap, this need not be the case, nor is there any guarantee of this
      _ZOFFSET_OF_RECONSTRUCTION_LAYER_IN_NANOMETERS = i*(_numberOfSlicesInReconstructionLayer+1)*_ZSTEP_BETWEEN_SLICES_IN_NANOMETERS -
          ((_numberOfSlicesInReconstructionLayer+1)*_ZSTEP_BETWEEN_SLICES_IN_NANOMETERS*(_numberOfReconstructionLayers-1))/2;

      createVerificationRegions(false);

      int j = 0;
      for (TestSubProgram subProgram : _testProgram.getAllTestSubPrograms())
      {
        perTestSubProgramReconstructionRegions.get(j++).addAll(subProgram.getVerificationRegions());
      }
    }
    int j = 0;
    for (TestSubProgram subProgram : _testProgram.getAllTestSubPrograms())
    {
      subProgram.setVerificationRegions(perTestSubProgramReconstructionRegions.get(j++));
    }
  }

  /**
   * @author Scott Richardson
   */
  private void constructVirtualLiveReconstructionColumns()
  {
    // Construct the set of reconstructionColumns which divide the panel in X-Y exactly as the reconstructionRegions do.
    VirtualLiveSurroundingSliceUtil<ReconstructionRegion> regionUtil = new VirtualLiveSurroundingSliceUtil<ReconstructionRegion>();
    regionUtil.setReconstructionRegions(_testProgram.getVerificationRegions());

    List<ReconstructionRegion> reconstructionRegions = _testProgram.getVerificationRegions();
    ReconstructionRegion reconstructionRegion;
    while(reconstructionRegions.size() > 0)
    {
      reconstructionRegion = reconstructionRegions.get(0);

      Set<ReconstructionRegion> XYProjectedRegions = regionUtil.getReconstructionRegionsAtReconstructionRegion(reconstructionRegion);
      Assert.expect(XYProjectedRegions.size() == _numberOfReconstructionLayers,
                    "Expect the number of reconstructionRegions that fall in a reconstructionColumn to equal to the number of reconstructionLayers");
      VirtualLiveReconstructionColumn reconstructionColumn = new VirtualLiveReconstructionColumn(XYProjectedRegions.iterator().next());

      _reconstructionColumnWorkers.put(reconstructionColumn, XYProjectedRegions);

      reconstructionRegions.removeAll(XYProjectedRegions);
    }
  }

  /**
   * @author Scott Richardson
   */
  protected FocusGroup createFocusGroupForVerificationOrAlignmentRegion(PanelRectangle focusRegionRectangleInNanoMeters,
      ReconstructionRegion associatedVerificationRegion)
  {
    Assert.expect(focusRegionRectangleInNanoMeters != null);
    Assert.expect(associatedVerificationRegion != null);

    return createFocusGroupForVirtualLiveRegion(focusRegionRectangleInNanoMeters, associatedVerificationRegion);
  }

  /**
   * Create a focus group specific to the Virtual Live Verification Regions
   * @author Scott Richardson
   */
  public FocusGroup createFocusGroupForVirtualLiveRegion(PanelRectangle focusRegionRectangleInNanoMeters,
      ReconstructionRegion region)
  {
    Assert.expect(focusRegionRectangleInNanoMeters != null);
    Assert.expect(region != null);

    FocusRegion focusRegion = new FocusRegion();
    FocusGroup focusGroup = new FocusGroup();
    FocusSearchParameters focusSearchParameters = new FocusSearchParameters();

    focusRegion.setRectangleRelativeToPanelInNanoMeters(focusRegionRectangleInNanoMeters);
    focusSearchParameters.setFocusRegion(focusRegion);
    // set the focus search parameters to "NO_SEARCH" to bypass autofocus
    focusGroup.setFocusSearchParameters(focusSearchParameters);
    focusSearchParameters.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);

    // Currently, only a single slice can be re-reconstructed at a time. In order to re-reconstruct more than a single
    // slice at a time, the unique slice names must be added to the SliceNameEnum
    SliceNameEnum sliceNameEnumArray[] = {SliceNameEnum.VIRTUAL_LIVE_SLICE_1};
    Assert.expect(sliceNameEnumArray.length == _numberOfSlicesInReconstructionLayer);
    for (int i = 0; i < _numberOfSlicesInReconstructionLayer; i++)
    {
      FocusInstruction focusInstruction = new FocusInstruction();
      focusInstruction.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);
      // the deck of slices is centered at the _zOffsetOfReconstructionLayerInNanoMeters.
      /** @todo sgr debug */
      focusInstruction.setZHeightInNanoMeters((_ZSTEP_BETWEEN_SLICES_IN_NANOMETERS*(2*i - _numberOfSlicesInReconstructionLayer+1))/2 +
                                              _ZOFFSET_OF_RECONSTRUCTION_LAYER_IN_NANOMETERS);
      /** @todo sgr note: it seeems that setZOffsetInNanoMeters doesnt do what it sounds like it should */
//      focusInstruction.setZOffsetInNanoMeters(_zOffsetOfDeckInNanoMeters);
      Slice slice = new Slice();
      slice.setSliceName(sliceNameEnumArray[i]);
      slice.setFocusInstruction(focusInstruction);
      slice.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
      slice.setAssociatedReconstructionRegion(region);
      focusGroup.addSlice(slice);
    }

    return focusGroup;
  }

  /**
   * Because a re-reconstruction request can only be requested for an entire reconstruction region, we
   * might as well set the slices that surround the slice of interest to zHeights that we don't already have.
   * @todo sgr debug
   * @author Scott Richardson
   */
  public void reSpecifyReconstructionRegionSliceHeights(VirtualLiveReconstructionColumn reconstructionColumn,
      ReconstructionRegion reconstructionWorker, int zHeightForSliceOfInterest)
  {
    Assert.expect(reconstructionWorker != null);

    List<Slice> reconstructionWorkerSlices = reconstructionWorker.getFocusGroups().get(0).getSlices();

    int currentReconstructionWorkerSliceIndex = 0;
    Slice currentReconstructionWorkerSlice = reconstructionWorkerSlices.get(currentReconstructionWorkerSliceIndex);
    // set the focus height to the zHeight of interest (this will become the median slice)
    currentReconstructionWorkerSlice.getFocusInstruction().setZHeightInNanoMeters(zHeightForSliceOfInterest);

    // search for the closest slices above and below the slice of interest for which we don't have reconstructedImages
    int j = (reconstructionWorkerSlices.size()+1)/2 + 1;
    int k = (reconstructionWorkerSlices.size()+1)/2 - 1 ;
    // although these seem to be initilized backwards, they are correct
    int zHeightOfBottomSlice = Integer.MAX_VALUE;
    int zHeightOfTopSlice = Integer.MIN_VALUE;
    boolean foundEmptyTopSlice = false;
    boolean foundEmptyBottomSlice = false;
    while (++currentReconstructionWorkerSliceIndex <  reconstructionWorkerSlices.size())
    {
      // start a search one step above the slice of interest
      while(zHeightOfTopSlice < _project.getPanel().getThicknessInNanometers() / 2.0 && foundEmptyTopSlice == false)
      {
        zHeightOfTopSlice = (_ZSTEP_BETWEEN_SLICES_IN_NANOMETERS*(2*j - _numberOfSlicesInReconstructionLayer+1))/2 +
                         zHeightForSliceOfInterest;
        if (reconstructionColumn.containsSliceAtZHeight(zHeightOfTopSlice) == false)
        {
          foundEmptyTopSlice = true;
          break;
        }
        j++;
      }

      // start a search one step below the slice of interest
      while(zHeightOfBottomSlice > -_project.getPanel().getThicknessInNanometers() / 2.0 && foundEmptyBottomSlice == false)
      {
        zHeightOfBottomSlice = (_ZSTEP_BETWEEN_SLICES_IN_NANOMETERS*(2*k - _numberOfSlicesInReconstructionLayer+1))/2 +
                         zHeightForSliceOfInterest;
        if (reconstructionColumn.containsSliceAtZHeight(zHeightOfBottomSlice) == false)
        {
          foundEmptyBottomSlice = true;
          break;
        }
        k--;
      }

      if (Math.abs(zHeightOfTopSlice - zHeightForSliceOfInterest) > Math.abs(zHeightOfBottomSlice - zHeightForSliceOfInterest))
      {
        currentReconstructionWorkerSlice.getFocusInstruction().setZHeightInNanoMeters(zHeightOfTopSlice);
        reconstructionColumn.insertSliceAtZHeight(zHeightOfTopSlice);
        currentReconstructionWorkerSlice = reconstructionWorkerSlices.get(++currentReconstructionWorkerSliceIndex);
        foundEmptyTopSlice = false;
      }
      else
      {
        currentReconstructionWorkerSlice.getFocusInstruction().setZHeightInNanoMeters(zHeightOfBottomSlice);
        reconstructionColumn.insertSliceAtZHeight(zHeightOfBottomSlice);
        currentReconstructionWorkerSlice = reconstructionWorkerSlices.get(++currentReconstructionWorkerSliceIndex);
        foundEmptyBottomSlice = false;
      }
    }
    // If all slices have been reconstructed in a region, then we just re-reconstruct the remaining slices
    // at a focus height we already have.
  }

  /**
   * @author Scott Richardson
   */
  public Map<VirtualLiveReconstructionColumn, Set<ReconstructionRegion>> getReconstructionWorkers()
  {
    Assert.expect(_reconstructionColumnWorkers != null);

    return _reconstructionColumnWorkers;
  }

  /**
   * @author Scott Richardson
   */
  public Set<VirtualLiveReconstructionColumn> getVirtualLiveReconstructionColumns()
  {
    Assert.expect(_reconstructionColumnWorkers != null);

    return _reconstructionColumnWorkers.keySet();
  }

  /**
   * @author Scott Richardson
   */
  public static int getZStepBetweenSlicesInNanoMeters()
  {
    return _ZSTEP_BETWEEN_SLICES_IN_NANOMETERS;
  }

  /**
   * @author Scott Richardson
   */
  public static int getZOffsetOfReconstructionLayersInNanoMeters()
  {
    return _ZOFFSET_OF_RECONSTRUCTION_LAYER_IN_NANOMETERS;
  }
}

