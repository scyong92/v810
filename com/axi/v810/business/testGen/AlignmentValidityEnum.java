package com.axi.v810.business.testGen;

import com.axi.util.*;

/**
 * @author George A. David
 */
public class AlignmentValidityEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static AlignmentValidityEnum IS_VALID = new AlignmentValidityEnum(++_index);
  public static AlignmentValidityEnum INVALID_DOES_NOT_FIT = new AlignmentValidityEnum(++_index);
  public static AlignmentValidityEnum INVALID_FEATURES_NOT_ORTHOGONAL = new AlignmentValidityEnum(++_index);
  public static AlignmentValidityEnum NOT_SUPPORT_FIDUCIAL = new AlignmentValidityEnum(++_index);

  /**
   * @author George A. David
   */
  public AlignmentValidityEnum(int id)
  {
    super(id);
  }
}
