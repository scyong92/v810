package com.axi.v810.business.testGen;

import java.awt.geom.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.communication.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @todo PE rewrite these comments!  They are pretty out of date.
 *
 * Creates a TestProgram for use by the TestExecutionEngine.
 * ProgramGeneration also is responsible for writing the RepairCadFile
 * (the legacy rptcad file).
 *
 * <p>
 * ProgramGeneration should not regenerate the TestProgram or the rptcad file every time
 * TestExecution calls getTestProgram.  This will take too much time.  Instead, ProgramGeneration
 * should try to use a previously-generated TestProgram if it is still valid.
 * The previously-generated TestProgram is not valid if any of the following have changed:
 * <ul>
 * <li> CAD (pad locations or sizes) </li>
 * <li> algorithm family assignments </li>
 * <li> imaging settings </li>
 * <li> magnification </li>
 * <li> shading compensation (overscanning) </li>
 * </ul>
 *
 * <p>
 * Obviously, the very first time getTestProgram is called on a Panel, there
 * will be no previously-generated TestProgram, so it will need to generated at that time.
 *
 * <p>
 * ProgramGeneration will use rules to decide how big to make the InspectionRegions.  The currently proposed
 * rule is:
 *
 * <p><code>
 * 1.5 x (max(pad.Dx, pad.Dy) + (5/3) * InterPadDistance)
 * </code>
 *
 * <p>
 * I realize that formula seems really bizarre, but it's based on the biggest
 * region-of-interest currently used in the algorithms.  We might want to
 * have a method in the ImageAnalysisSubsystem that allows ProgramGeneration
 * ask what size to make the InspectionRegions.  It might be something like:
 * getRegionOfInterestSize(PanelRectangle padSize, int interPadDistance)
 *
 * <p>
 * An annoying situation will be pads oriented at a non-cardinal angle.  This
 * can happen either when a regular component is rotated to an off-angle or
 * when a component has a cardinal rotation but has some weird pads that
 * are oriented at off-angles.  In these cases, we will need to compute an
 * initial region-of-interest using the same rules (except that it will be rotated
 * in the same fashion as the pad), but then the final region-of-interest that
 * will be used is the initial ROI's minimum bounding box in cardinal rotation.  In other
 * words, first compute a region-of-interest with the same off-angle rotation
 * as the pad.  Then get the final, actual region-of-interest by defining its
 * top left corner at the minimum x- and y- coordinates of the first ROI and the
 * bottom left corner at the maximum x- and y- coordinates of the first ROI.
 *
 * <p>
 * Slices must also be defined for each pad.  This will depend on algorithm family.  Most algorithm
 * families will only have a single slice.  Others will have multiple slices.  Each slice will be identified
 * by a unique "focus instruction".  An example of a focus instruction might be "maximum sharpness", meaning
 * that the slice with the "best focus" will be used.  Note that we may want to make a prioritized list of
 * focus instructions for each slice, so that if the first instruction fails, we can try the second instruction
 * on the list, and if that fails go to the third instruction, etc.
 *
 * <P>
 * Along with the focus instructions, each slice needs a "focus region" assigned to it.  This is a subregion of
 * the InspectionRegion.  For example, in a large InspectionRegion containing an array of BGA joints, we might
 * choose the focus region to only contain the 9 joints in the middle.  This is done to reduce the computational
 * load on the focus search software.
 *
 * <p>
 * Anyways, the current plan for focus instructions and focus regions is detailed in Tracy Eliasson's
 * "FocusHintDetails" spreadsheet, which is in ClearCase under java/com/axi/v810/business/testGen
 * under the name FocusHintDetails.htm.  Again, since these things really are coupled to the definition of the
 * algorithms, it may be desirable to have a method in the IAS that specifies this table at runtime, rather than
 * having the table hardcoded here in the Program Generation subsystem.
 *
 * @author Peter Esbensen
 */
public class ProgramGeneration
{
  // private static member variable
  private static ProgramGeneration _programGeneration = null;

  private static final int _VERSION_NUMBER = 1;
  private static final double _MIN_PAD_AREA_TO_TOTAL_AREA_RATIO_GRID_ARRAY_FOCUS_REGIONS = 0.7;
  private static final double _MIN_PAD_AREA_TO_TOTAL_AREA_RATIO_THROUGH_HOLE_FOCUS_REGIONS = 0.7;
  private static final double _MIN_PAD_AREA_TO_TOTAL_AREA_RATIO_GULLWING_FOCUS_REGIONS = 0.7;
  private static final double _MIN_PAD_AREA_TO_TOTAL_AREA_RATIO_RECONSTRUCTION_REGIONS = 0.5;
  private static final int _MIN_FOCUS_REGION_BORDER_IN_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(6);
  private static final int _MAX_GULLWING_PADS_IN_FOCUS_REGION = 3;
  private static final double _GRID_ARRAY_OR_THROUGH_HOLE_PITCH_MULTIPLIER = 1.5;
  private static final int _MIN_NEIGHBORS_FOR_GRID_ARRAY_RECONSTRUCTION_REGION = 4;
  private static final double _FOCUS_REGION_BUFFER_FOR_FOCUS_PRIORITY = MathUtil.convertMilsToNanoMeters(200);
  private static final int _UNFOCUSED_REGION_SLICE_INTERVAL_IN_NANOS = MathUtil.convertMilsToNanoMetersInteger(2);
  private static final double _DIFFERENCE_VERIFICATION_REGION_WIDTH_ALLOWED_TO_BE_CONSIDERED_EQUAL = 5.0e-07;

  private static int _STANDARD_MAX_INSPECTABLE_PANEL_WIDTH_NANOS = (int)MathUtil.convertMilsToNanoMetersInteger(17500);
  private static int _THROUGHPUT_MAX_INSPECTABLE_PANEL_WIDTH_NANOS = (int)MathUtil.convertMilsToNanoMetersInteger(16900); // 16900
  private static int _XXL_MAX_INSPECTABLE_PANEL_WIDTH_NANOS = 654400000; // 25.764 inch
  private static int _S2EX_MAX_INSPECTABLE_PANEL_WIDTH_NANOS = (int)MathUtil.convertMilsToNanoMetersInteger(18700);
  
  private static final int _DEGREES_ROTATION_NORMALIZATION_FACTOR = 180;
  
  private static final int _SMT_CONNECTOR_ZOFFSET_FOUR_MILS_IN_NANOS = 101600;
  private static final int _SMT_CONNECTOR_ZOFFSET_EIGHT_MILS_IN_NANOS = 203200;
  private static final int _SMT_CONNECTOR_ZOFFSET_TWELVE_MILS_IN_NANOS = 304800;
  private static final int _SMT_CONNECTOR_ZOFFSET_SIXTEEN_MILS_IN_NANOS = 406400;
  private static final int _SMT_CONNECTOR_ZOFFSET_TWENTY_MILS_IN_NANOS = 508000;

  private static final boolean _TEST_IRP_BOUNDARIES = false;

  private double _minPadAreaToTotalAreaRatioForGridArrayFocusRegions = -1;
  private double _minPadAreaToTotalAreaRatioForThrouhHoleFocusRegions = -1;
  private double _minPadAreaToTotalAreaRatioForGullwingFocusRegions = -1;
  private double _minPadAreaToTotalAreaRatioForReconstructionRegions = -1;

  // private instance member variable
//  private PanelRectangle _rightSectionOfPanel;
//  private boolean _isLongPanel;

  private List<ReconstructionRegion> _rightInspectionRegions = new LinkedList<ReconstructionRegion>();
  private List<ReconstructionRegion> _leftInspectionRegions = new LinkedList<ReconstructionRegion>();

  protected TestProgram _testProgram;
  protected Project _project;
  protected static ImageAcquisitionEngine _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();

  private Map<List<Pad>, Collection<Collection<Set<LandPatternPad>>>> _padsToSeparatedLandPatternPadsMap = new HashMap<List<Pad>, Collection<Collection<Set<LandPatternPad>>>>();
  private Map<List<Pad>, List<java.awt.Shape>> _padsToFocusShapesMap = new HashMap<List<Pad>, List<java.awt.Shape>>();
  private Map<String, List<List<Pad>>> _landPatternKeyToPadListMap = new HashMap<String,List<List<Pad>>>();
//  private List<ReconstructionRegion> _rightSurfaceModelingReconstructionRegions = new LinkedList<ReconstructionRegion>();
//  private List<ReconstructionRegion> _leftSurfaceModelingReconstructionRegions = new LinkedList<ReconstructionRegion>();
//  private Set<PadType> _padTypesThatExtendBeyondImageableRegion = new HashSet<PadType>();
  private Set<Pad> _padsThatExtendBeyondImageableRegion = new HashSet<Pad>();
  private Set<ComponentType> _componentTypesThatExtendBeyondImageableRegion = new HashSet<ComponentType>();

  private boolean _printDebugInfo = false;
  private static TimerUtil _generateProgramTimer = new TimerUtil();
  private boolean _didAlignmentRegionsChange = false;
  private Map<Component, Set<ReconstructionRegion>> _componentToReconstructionRegionsMap = new HashMap<Component, Set<ReconstructionRegion>>();
  private Map<String, Set<Component>> _landPatternRotationKeyToComponentsMap = new HashMap<String, Set<Component>>();

  private Config _config = Config.getInstance();

  private static XrayCameraArray _xrayCameraArray = XrayCameraArray.getInstance();
  private static double _CAMERA_ARRAY_WIDTH_IN_MILS = 0;
  private int _maximumPanelInspectableWidthInNanometers = 0;

  // Chnee Khang Wah, 2012-02-02, 2.5D HIP Development
  private boolean _isGenerateMultiAngleImagesEnabled = false;
  
  private boolean _enablePsp = _config.getBooleanValue(SoftwareConfigEnum.ENABLE_PSP);
  private OpticalProgramGeneration _opticalProgramGeneration = OpticalProgramGeneration.getInstance();
  
  // bee-hoon.goh 
  private List<String> _focusRegionComponentResetList = new ArrayList<String>();
  
  //Khaw Chek Hau - XCR2285: 2D Alignment on v810
  private Pattern _alignment2DImageFileNamePattern = Pattern.compile(FileName.getAlignment2DImageFileNamePattern());

  //Khaw Chek Hau - XCR2943: Alignment Fail Due to Too Close To Edge
  private static int _ADDITIONAL_ADDED_ALIGNMENT_BORDER_IN_NANOMETERS = 508000; //20 mils on each border side
  
  private static final int _DEFAULT_TOTAL_POP_LAYER_NUMBER = 1;
  private static final int _DEFAULT_POP_ZHEIGHT_IN_NANOMETERS = 0;

  // this comparator first sorts in y, then in x.
  private Comparator<Point2D> _pointComparator = new Comparator<Point2D>()
  {
    /**
     * @author George A. David
     */
    public int compare(Point2D lhs, Point2D rhs)
    {
      if(MathUtil.fuzzyEquals(lhs.getY(), rhs.getY(), MathUtil.NANOMETERS_PER_MIL))
      {
        return (int)(lhs.getX() - rhs.getX());
      }
      else
        return (int)(lhs.getY() - rhs.getY());
    }
  };

  // this comparator first sorts in y, then in x.
  private Comparator<PanelRectangle> _panelRectangleComparator = new Comparator<PanelRectangle>()
  {
    /**
     * @author George A. David
     */
    public int compare(PanelRectangle lhs, PanelRectangle rhs)
    {
      if(MathUtil.fuzzyEquals(lhs.getMinY(), rhs.getMinY(), MathUtil.NANOMETERS_PER_MIL))
      {
        return (int)(lhs.getMinX() - rhs.getMinX());
      }
      else
        return (int)(lhs.getMinY() - rhs.getMinY());
    }
  };

  // this will compare the test Sub Program (Wei Chin add for individual Board Alignment)
  private Comparator<TestSubProgram> _testSubProgramComparator = new Comparator<TestSubProgram>()
  {
    /**
     * @author Wei Chin
     */
    public int compare(TestSubProgram lhs, TestSubProgram rhs)
    {
      return _panelRectangleComparator.compare(rhs.getVerificationRegionsBoundsInNanoMeters(), lhs.getVerificationRegionsBoundsInNanoMeters());
    }
  };

  // this comparator sort reconstruction regions.
  private Comparator<ReconstructionRegion> _reconstructionRegionForIdGenerationComparator = new Comparator<ReconstructionRegion>()
  {
    /**
     * @author George A. David
     */
    public int compare(ReconstructionRegion lhs, ReconstructionRegion rhs)
    {
      int result = 0;

      if (lhs.isAlignmentRegion() || rhs.isAlignmentRegion() || 
          lhs.isVerificationRegion() || rhs.isVerificationRegion())
      {
        // If either of the regions are alignment regions, compare based on the panel location enum (left < right).
        PanelLocationInSystemEnum lhsPanelLocationInSystemEnum = lhs.getTestSubProgram().getPanelLocationInSystem();
        PanelLocationInSystemEnum rhsPanelLocationInSystemEnum = rhs.getTestSubProgram().getPanelLocationInSystem();

        if (lhsPanelLocationInSystemEnum.equals(rhsPanelLocationInSystemEnum) == false)
        {
          // If the lhs region's location is left side, return -1, otherwise return 1.
          if (lhsPanelLocationInSystemEnum.equals(PanelLocationInSystemEnum.LEFT))
          {
            return -1;
          }
          else
          {
            return 1;
          }
        }
        else
        {
          // If the panel locations are the same, we'll just compare the object references.
          // If they are the same, return 0; otherwise, return 1;
          if (lhs == rhs)
          {
            return 0;
          }
          else
          {
            return 1;
          }
        }
      }
      else
      {
        result = lhs.getComponent().getReferenceDesignator().compareTo(rhs.getComponent().getReferenceDesignator());
        if (result == 0)
        {
          // ok the ref des match, let's sort on the pad names
          Collection<Pad> lhsPads = lhs.getPads();
          Set<String> lhsNames = new TreeSet<String>();
          for (Pad pad : lhsPads)
            lhsNames.add(pad.getName());

          Collection<Pad> rhsPads = rhs.getPads();
          Set<String> rhsNames = new TreeSet<String>();
          for (Pad pad : rhsPads)
            rhsNames.add(pad.getName());

          // now create the padstrings
          String lhsPadStrings = "";
          for (String name : lhsNames)
            lhsPadStrings += name + "_";

          String rhsPadStrings = "";
          for (String name : rhsNames)
            rhsPadStrings += name + "_";

          // finally compare the names
          result = lhsPadStrings.compareTo(rhsPadStrings);
        }
      }

      return result;
    }
  };

  /**
   * @author Peter Esbensen
   */
  public static synchronized ProgramGeneration getInstance()
  {
    if (_programGeneration == null)
      _programGeneration = new ProgramGeneration();
    return _programGeneration;
  }

  /**
   * @author Peter Esbensen
   */
  protected ProgramGeneration()
  {
//    Initialise licenseManager when it need
//    _licenseManager = LicenseManager.getInstance();
    if( Config.getInstance().getIntValue(HardwareConfigEnum.SYSTEM_INSPECTION_TYPE) == 2)
      _THROUGHPUT_MAX_INSPECTABLE_PANEL_WIDTH_NANOS =  (int)MathUtil.convertMilsToNanoMetersInteger(17126);
    else
      _THROUGHPUT_MAX_INSPECTABLE_PANEL_WIDTH_NANOS =  (int)MathUtil.convertMilsToNanoMetersInteger(16900);
      
    _maximumPanelInspectableWidthInNanometers = getMaxPanelInspectableWidthInNanoMeters();
  }

  /**
   * @author George A. David
   */
  protected void initialize()
  {
    _padsToSeparatedLandPatternPadsMap.clear();
    _padsToFocusShapesMap.clear();
    _landPatternKeyToPadListMap.clear();
    _rightInspectionRegions.clear();
    _leftInspectionRegions.clear();

//    _rightSurfaceModelingReconstructionRegions.clear();
//    _leftSurfaceModelingReconstructionRegions.clear();

    _landPatternRotationKeyToComponentsMap.clear();
    _componentToReconstructionRegionsMap.clear();
  }

  /**
   * @author George A. David
   */
  protected void reset()
  {
    initialize();
    _project = null;
    _testProgram = null;
//    _rightSectionOfPanel = null;
//    _isLongPanel = false;
    _printDebugInfo = false;
  }

  /**
   * @author George A. David
   */
  private TestProgramGenerationData createTestProgramGenerationData()
  {
    TestProgramGenerationData data = new TestProgramGenerationData();

    // inspection region data
    data.setMaxInspectionRegionLengthInNanoMeters(ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters());
    data.setMaxInspectionRegionWidthInNanoMeters(ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters());
    data.setMaxInspectionRegionSizeDueToWarpInNanoMeters(ReconstructionRegion.getMaxInspectionRegionSizeDueToWarpInNanoMeters());

    // alignment data
    data.setAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters(Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters());
    data.setAlignmentUncertaintyInNanoMeters(Alignment.getAlignmentUncertaintyBorderInNanoMeters());
    data.setAlignmentFeatureLengthBufferInNanoMeters(ReconstructionRegion.getAlignmentFeatureLengthBufferInNanoMeters());
    data.setMaxAlignmentRegionLengthInNanoMeters(ReconstructionRegion.getMaxAlignmentRegionLengthInNanoMeters());
    data.setMaxAlignmentRegionWidthInNanoMeters(ReconstructionRegion.getMaxAlignmentRegionWidthInNanoMeters());

    // verification region data
    data.setMaxVerificationRegionLengthInNanoMeters(ReconstructionRegion.getMaxVerificationRegionLengthInNanoMeters());
    data.setMaxVerificationRegionWidthInNanoMeters(ReconstructionRegion.getMaxVerificationRegionWidthInNanoMeters());

    // focus region data
    data.setMaxFocusRegionSizeInNanoMeters(FocusRegion.getMaxFocusRegionSizeInNanoMeters());
    data.setMinFocusRegionSizeInNanoMeters(FocusRegion.getMinFocusRegionSizeInNanoMeters());

    // other
    data.setMaxImageableAreaLengthInNanoMeters(XrayTester.getMaxImageableAreaLengthInNanoMeters());
    data.setMaxInspectablePanelWidthInNanoMeters(_maximumPanelInspectableWidthInNanometers);
    data.setNumberOfAvailableIrps(ImageReconstructionProcessorManager.getNumberOfImageReconstructionEngines());
    data.setNumberOfAvailablePhysicalIrps(ImageReconstructionProcessorManager.getNumberOfImageReconstructionProcessors());

    // Wei Chin comment: currently our system only support 1 system type
    data.setSystemType(XrayTester.getSystemType());

    data.setScanPathOptimization(ImagingChainProgramGenerator.getEnableStepSizeOptimization());
    //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on";
    data.setFocusOptimization(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION));

    // Added by Lee Herng - To control whether large image view feature is enabled
    //data.setLargeImageView(_isEnableLargeImageView); // Bee Hoon, Recipe Settings
    
    // Added by Khang Wah, 2012-02-15, 2.5D HIP
    //data.setGenerateMultiAngleImage(_isGenerateMultiAngleImageEnabled); // Bee Hoon, Recipe Settings
    
    data.setSystemInspectionType(Config.getInstance().getIntValue(HardwareConfigEnum.SYSTEM_INSPECTION_TYPE));
    
    // Added by Jack Hwee - to control whether system PSP is enable/disable
    data.setPspEnable(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP));

    //Siew Yeng - XCR1745 - Semi-automated mode
    data.setSemiAutomatedMode(TestExecution.getInstance().isSemiAutomatedModeEnabled());
    //XCR-3513, Only low magnification alignment is running although VM is enable
    data.setVariableMagnificationMode(XrayTester.isVariableMagEnabled());
    //Ngie Xing
    data.setLowMagnification(Config.getSystemCurrentLowMagnification());
    data.setHighMagnification(Config.getSystemCurrentHighMagnification()); 
    
    return data;
  }

  /**
   * @author George A. David
   */
  private void createProcessorStripBoundingReconstructionRegionsForIrpBoundaryTest()
  {
    Panel panel = _project.getPanel();
    BoardType boardType = panel.getBoardTypes().get(0);
    LandPattern landPattern = panel.getLandPattern("irpBoundaryTestLandPattern");

    _rightInspectionRegions.clear();
    TestSubProgram subProgram = _testProgram.getTestSubProgram(PanelLocationInSystemEnum.RIGHT);

    int stripNumber = subProgram.getProcessorStripsForInspectionRegions().get(0).getLocation();
    Board modifiedBoard = null;
    for(Board board : panel.getBoards())
    {
      if(board.hasComponent("topLeftIrpBoundaryTestComponent" + stripNumber))
      {
        modifiedBoard = board;
        break;
      }
    }

    if(modifiedBoard != null)
    {
      for(ProcessorStrip strip : subProgram.getProcessorStripsForInspectionRegions())
      {
        createInspectionRegions(modifiedBoard.getComponent("topLeftIrpBoundaryTestComponent" + strip.getLocation()));
        createInspectionRegions(modifiedBoard.getComponent("middleTopIrpBoundaryTestComponent" + strip.getLocation()));
        createInspectionRegions(modifiedBoard.getComponent("topRightIrpBoundaryTestComponent" + strip.getLocation()));
        createInspectionRegions(modifiedBoard.getComponent("middleRightIrpBoundaryTestComponent" + strip.getLocation()));
        createInspectionRegions(modifiedBoard.getComponent("middleLeftIrpBoundaryTestComponent" + strip.getLocation()));
        createInspectionRegions(modifiedBoard.getComponent("bottomLeftIrpBoundaryTestComponent" + strip.getLocation()));
        createInspectionRegions(modifiedBoard.getComponent("middleBottomIrpBoundaryTestComponent" + strip.getLocation()));
        createInspectionRegions(modifiedBoard.getComponent("bottomRightIrpBoundaryTestComponent" + strip.getLocation()));
      }
    }
    else
    {
      AffineTransform transform = new AffineTransform();
      boardType.getBoards().get(0).preConcatenateShapeTransform(transform);
      try
      {
        transform.createInverse();
      }
      catch (NoninvertibleTransformException ex)
      {
        Assert.logException(ex);
      }
      SideBoardType sideBoardType = boardType.getSideBoardTypes().get(0);
      for (ProcessorStrip strip : subProgram.getProcessorStripsForInspectionRegions())
      {
        PanelRectangle stripRect = strip.getRegion();
        Point2D point = new Point2D.Double(stripRect.getMinX() + ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters() / 2.0,
                                           stripRect.getMaxY() - ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters() / 2.0);
        transform.transform(point, point);
        ComponentType topLeftComponentType = sideBoardType.createComponentType("topLeftIrpBoundaryTestComponent" + strip.getLocation(),
                                                                               landPattern,
                                                                               new BoardCoordinate((int)point.getX(), (int)point.getY()),
                                                                               0);
        createInspectionRegions(topLeftComponentType.getComponents().get(0));

        point = new Point2D.Double(stripRect.getCenterX(),
                                   stripRect.getMaxY() - ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters() / 2.0);
        transform.transform(point, point);
        ComponentType middleTopComponentType = sideBoardType.createComponentType("middleTopIrpBoundaryTestComponent" + strip.getLocation(),
                                                                                 landPattern,
                                                                                 new BoardCoordinate((int)point.getX(), (int)point.getY()),
                                                                                 0);
        createInspectionRegions(middleTopComponentType.getComponents().get(0));

        point = new Point2D.Double(stripRect.getMaxX() - ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters() / 2.0,
                                   stripRect.getMaxY() - ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters() / 2.0);
        transform.transform(point, point);
        ComponentType topRightComponentType = sideBoardType.createComponentType("topRightIrpBoundaryTestComponent" + strip.getLocation(),
                                                                                landPattern,
                                                                                new BoardCoordinate((int)point.getX(), (int)point.getY()),
                                                                                0);
        createInspectionRegions(topRightComponentType.getComponents().get(0));

        point = new Point2D.Double(stripRect.getMinX() + ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters() / 2.0,
                                   stripRect.getCenterY());
        transform.transform(point, point);
        ComponentType middleLeftComponentType = sideBoardType.createComponentType("middleLeftIrpBoundaryTestComponent" + strip.getLocation(),
                                                                                  landPattern,
                                                                                  new BoardCoordinate((int)point.getX(), (int)point.getY()),
                                                                                  0);
        createInspectionRegions(middleLeftComponentType.getComponents().get(0));

        point = new Point2D.Double(stripRect.getMaxX() - ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters() / 2.0,
                                   stripRect.getCenterY());
        transform.transform(point, point);
        ComponentType middleRightComponentType = sideBoardType.createComponentType("middleRightIrpBoundaryTestComponent" + strip.getLocation(),
                                                                                   landPattern,
                                                                                   new BoardCoordinate((int)point.getX(), (int)point.getY()),
                                                                                   0);
        createInspectionRegions(middleRightComponentType.getComponents().get(0));

        point = new Point2D.Double(stripRect.getMinX() + ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters() / 2.0,
                                   stripRect.getMinY() + ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters() / 2.0);
        transform.transform(point, point);
        ComponentType bottomLeftComponentType = sideBoardType.createComponentType("bottomLeftIrpBoundaryTestComponent" + strip.getLocation(),
                                                                                  landPattern,
                                                                                  new BoardCoordinate((int)point.getX(), (int)point.getY()),
                                                                                  0);
        createInspectionRegions(bottomLeftComponentType.getComponents().get(0));

        point = new Point2D.Double(stripRect.getCenterX(),
                                   stripRect.getMinY() + ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters() / 2.0);
        transform.transform(point, point);
        ComponentType middleBottomComponentType = sideBoardType.createComponentType("middleBottomIrpBoundaryTestComponent" + strip.getLocation(),
                                                                                    landPattern,
                                                                                    new BoardCoordinate((int)point.getX(), (int)point.getY()),
                                                                                    0);
        createInspectionRegions(middleBottomComponentType.getComponents().get(0));

        point = new Point2D.Double(stripRect.getMaxX() - ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters() / 2.0,
                                   stripRect.getMinY() + ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters() / 2.0);
        transform.transform(point, point);
        ComponentType bottomRightComponentType = sideBoardType.createComponentType("bottomRightIrpBoundaryTestComponent" + strip.getLocation(),
                                                                                   landPattern,
                                                                                   new BoardCoordinate((int)point.getX(), (int)point.getY()),
                                                                                   0);
        createInspectionRegions(bottomRightComponentType.getComponents().get(0));
      }
    }
    subProgram.setInspectionRegions(_rightInspectionRegions);
  }

  /**
   * @author George A. David
   */
  private void createPanelBoundingReconstructionRegionsForIrpBoundaryTest()
  {
    // create single pad components such that they are located
    // at the four corners of the panel.
    Panel panel = _project.getPanel();
    String landPatternName = "irpBoundaryTestLandPattern";
    if(panel.isLandPatternNameDuplicate(landPatternName))
    {
      for(Board board : panel.getBoards())
      {
        if(board.hasComponent("topLeftIrpBoundaryTestComponent"))
          createInspectionRegions(board.getComponent("topLeftIrpBoundaryTestComponent"));
        if(board.hasComponent("topRightIrpBoundaryTestComponent"))
          createInspectionRegions(board.getComponent("topRightIrpBoundaryTestComponent"));
        if(board.hasComponent("bottomLeftIrpBoundaryTestComponent"))
          createInspectionRegions(board.getComponent("bottomLeftIrpBoundaryTestComponent"));
        if(board.hasComponent("bottomRightIrpBoundaryTestComponent"))
          createInspectionRegions(board.getComponent("bottomRightIrpBoundaryTestComponent"));
      }
    }
    else
    {
      LandPattern landPattern = new LandPattern();
      landPattern.setPanel(panel);
      landPattern.setName(landPatternName);

      LandPatternPad landPatternPad = new SurfaceMountLandPatternPad();
      landPatternPad.setLandPattern(landPattern);
      landPatternPad.setCoordinateInNanoMeters(new ComponentCoordinate(0, 0));
      landPatternPad.setDegreesRotation(0);
      landPatternPad.setLengthInNanoMeters(30 * 25400);
      landPatternPad.setWidthInNanoMeters(30 * 25400);
      landPatternPad.setShapeEnum(ShapeEnum.RECTANGLE);
      landPatternPad.setName("irpBoundaryTestPad");

      landPatternPad.setPadOne(true);

      BoardType boardType = panel.getBoardTypes().get(0);

      // set all boards to be at 0,0
      AffineTransform transform = new AffineTransform();
      boardType.getBoards().get(0).preConcatenateShapeTransform(transform);
      try
      {
        transform.createInverse();
      }
      catch (NoninvertibleTransformException ex)
      {
        Assert.logException(ex);
      }
      SideBoardType sideBoardType = boardType.getSideBoardTypes().get(0);

      Point2D point = new Point2D.Double(ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters() / 2.0,
                                         panel.getLengthAfterAllRotationsInNanoMeters() - ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters() / 2.0);
      transform.transform(point, point);
      ComponentType topLeftComponentType = sideBoardType.createComponentType("topLeftIrpBoundaryTestComponent",
                                                                             landPattern,
                                                                             new BoardCoordinate((int)point.getX(), (int)point.getY()),
                                                                             0);
      createInspectionRegions(topLeftComponentType.getComponents().get(0));

      point = new Point2D.Double(panel.getWidthAfterAllRotationsInNanoMeters() - ReconstructionRegion. getMaxInspectionRegionWidthInNanoMeters() / 2.0,
                                 panel.getLengthAfterAllRotationsInNanoMeters() - ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters() / 2.0);
      transform.transform(point, point);
      ComponentType topRightComponentType = sideBoardType.createComponentType("topRightIrpBoundaryTestComponent",
                                                                              landPattern,
                                                                              new BoardCoordinate((int)point.getX(), (int)point.getY()),
                                                                              0);
      createInspectionRegions(topRightComponentType.getComponents().get(0));

      point = new Point2D.Double(ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters() /2.0,
                                 ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters() /2.0);
      transform.transform(point, point);
      ComponentType bottomLeftComponentType = sideBoardType.createComponentType("bottomLeftIrpBoundaryTestComponent",
                                                                                landPattern,
                                                                                new BoardCoordinate((int)point.getX(), (int)point.getY()),
                                                                                0);
      createInspectionRegions(bottomLeftComponentType.getComponents().get(0));

      point = new Point2D.Double(panel.getWidthAfterAllRotationsInNanoMeters() - ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters() /2.0,
                                 ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters() /2.0);
      transform.transform(point, point);
      ComponentType bottomRightComponentType = sideBoardType.createComponentType("bottomRightIrpBoundaryTestComponent",
                                                                                 landPattern,
                                                                                 new BoardCoordinate((int)point.getX(), (int)point.getY()),
                                                                                 0);
      createInspectionRegions(bottomRightComponentType.getComponents().get(0));
    }
  }

  /*
   * @author Goh Bee Hoon
   */
  public synchronized void rearrangeTestProgramBoardZOffset(TestProgram testprogram)
  {
    Assert.expect(testprogram != null);
    
    _project = testprogram.getProject();
    _testProgram = testprogram;
    
    try
    {
      _project.getProjectState().disable();
      calculatePadAreaRuleValues();
      createFocusGroups();      
    }
    finally
    {
      _project.getProjectState().enable();
      reset();
    }
  }  

  /*
   * @author Kee Chin Seong
   * - XXL Speed Improvement Development
   */
  public synchronized void resetupArtifacts(TestProgram testprogram)
  {
    Assert.expect(testprogram != null);
    
    _project = testprogram.getProject();
    _testProgram = testprogram;
    
    setupArtifactEffects();
    
    reset();
  }
  
  /*
   * @author Kee Chin Seong
   *  - XXL Speed Improvement Development,
   */
  public synchronized void rearrageTestProgramUserGain(TestProgram testprogram)
  {
    Assert.expect(testprogram != null);
    
    _project = testprogram.getProject();
    _testProgram = testprogram;
    
    try
    {
      _project.getProjectState().disable();
      
      List<ReconstructionRegion> regionList = _testProgram.rearrangeInspectionRegionBasedOnUserGain(_project.getProjectState().getUserGainChangeSubtype());
      for(ReconstructionRegion region : regionList)
      {
        //////need create TSP based on user gain
        addInspectionRegion(region);
      }
      
      resetTestProgramUponGainChanged(_testProgram);
    }
    finally
    {
      _project.getProjectState().enable();            

      try
      {
        if (Config.isDeveloperDebugModeOn() && LicenseManager.isDeveloperSystemEnabled())
        {
          for(TestSubProgram subprogram : _testProgram.getAllInspectableTestSubPrograms())
          {
            if(subprogram.isHighMagnification())
            {
              System.out.println("SubProgram Gain " + subprogram.getUserGain() + " HIGH MAG");
              for(ReconstructionRegion region : subprogram.getAllInspectionRegions())
                System.out.println("Region Component " + region.getComponent().getReferenceDesignator());
            }
            else
            {
              System.out.println("SubProgram Gain " + subprogram.getUserGain() + " LOW MAG");
              for(ReconstructionRegion region : subprogram.getAllInspectionRegions())
                System.out.println("Region Component " + region.getComponent().getReferenceDesignator());
            }
          }
        }
      }
      catch(XrayTesterException dex)
      {
        dex.printStackTrace();
      }

      reset();
    }
  }

  /**
   * Generate a TestProgram for the project passed in.
   * @author Peter Esbensen
   * @author George David
   * Edited By - Kee Chin Seong
   **/
  public synchronized TestProgram generateTestProgram(Project project)
  {
    Assert.expect(project != null);

    try
    {
      project.getProjectState().disable();
      _generateProgramTimer.reset();
      _generateProgramTimer.start();

      // RMS: Clean up some memory reference to make GC more efficient.
      if (_rightInspectionRegions.isEmpty() == false)
      {
        for (ReconstructionRegion region : _rightInspectionRegions)
        {
          for (JointInspectionData data: region.getJointInspectionDataList())
          {
            data.clearJointDistanceRelationships();
            data.clearInspectionRegion();
            data.clearJointInspectionResult();
          }

          region.getJointInspectionDataList().clear();
        }

        _rightInspectionRegions.clear();
      }

      if (_leftInspectionRegions.isEmpty() == false)
      {
        for (ReconstructionRegion region : _leftInspectionRegions)
        {
          for (JointInspectionData data: region.getJointInspectionDataList())
          {
            data.clearJointDistanceRelationships();
            data.clearInspectionRegion();
            data.clearJointInspectionResult();
          }

          region.getJointInspectionDataList().clear();
        }

        _leftInspectionRegions.clear();
      }

      initialize();

      _project = project;

      // reset the pad testability
     // for(PadType padType : _project.getPanel().getPadTypesUnsorted())
      //  padType.getPadTypeSettings().setTestable(true);
 
      // reset artifact info
      for(Pad pad : _project.getPanel().getPadsUnsorted())
      {
        pad.getPadSettings().setTestable(true);
        pad.getPadType().clearComponentTypesThatCauseInterferencePatterns();
      }

      _testProgram = new TestProgram();
      //Siew Yeng - XCR-3599 - move this code after predeterminePadTestability as this function will possible change the max reconstruction region size
      //_testProgram.setTestProgramGenerationData(createTestProgramGenerationData());
      _testProgram.setProject(_project);
      createTestSubProgram();
      if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
      {
        System.out.println("generateTestProgramDebug - createTestSubProgramBaseOnUserGain");
      }
        // Khang Wah - 2016-05-05, MVEDR
        // disable below method to move user-gain and stage speed to within one testSubProgram
        // createTestSubProgramBaseOnUserGain();
      //}
      TimerUtil timer = new TimerUtil();

      //Siew Yeng - XCR-3599 - reset stored value in project
      project.resetMaxInspectionRegionWidthLength();
      project.resetOverlapNeededForLargestPossibleJoint();
      
      //Siew Yeng - XCR-2218 - Increase inspectable pad size
      //always reset before calculate pad testability
      ReconstructionRegion.resetMaxInspectionRegionWidthLength();
      ProcessorStrip.resetOverlapNeededForLargestPossibleJoint();
      
      predeterminePadTestability();
      
      //Siew Yeng - XCR-3599 - Set new processor strip overlap size only if reconstruction region is enlarged
      if(project.isEnlargeReconstructionRegion() && 
         (ReconstructionRegion.isDefaultMaxInspectionRegionWidth() == false || ReconstructionRegion.isDefaultMaxInspectionRegionLength() == false))
      {
        ProcessorStrip.setOverlapNeededForLargestPossibleJointInNanometers(Math.max(ReconstructionRegion.getMaxInspectionRegionPadBoundsWidthInNanoMeters(), 
                                                                             ReconstructionRegion.getMaxInspectionRegionPadBoundsLengthInNanoMeters())/2);
        //store this value to project
        project.setOverlapNeededForLargestPossibleJointInNanometers(ProcessorStrip.getOverlapNeededForLargestPossibleJointInNanometers());

        if(ReconstructionRegion.isDefaultMaxInspectionRegionWidth() == false)
        {
          //store this value to project
          project.setMaxInspectionRegionWidthInNanometers(ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters());
          project.setMaxHighMagInspectionRegionWidthInNanometers(ReconstructionRegion.getMaxHighMagInspectionRegionWidthInNanoMeters());
        }
        
        if(ReconstructionRegion.isDefaultMaxInspectionRegionLength() == false)
        {
          //store this value to project
          project.setMaxInspectionRegionLengthInNanometers(ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters());
          project.setMaxHighMagInspectionRegionLengthInNanometers(ReconstructionRegion.getMaxHighMagInspectionRegionLengthInNanoMeters());
        }
      }
      //Siew Yeng - XCR-3599
      _testProgram.setTestProgramGenerationData(createTestProgramGenerationData());

      timer.start();
      if(_TEST_IRP_BOUNDARIES)
        createPanelBoundingReconstructionRegionsForIrpBoundaryTest();
      else
        createInspectionRegions(_project.getPanel());
      timer.stop();
      if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
        System.out.println("generateTestProgramDebug - Time to create inspection regions " + (_rightInspectionRegions.size() + _leftInspectionRegions.size()) +
                           " Inspection Regions: " + timer.getElapsedTimeInMillis());

// not doing this anymore, but may need to do it later
//      timer.reset();
//      timer.start();
//      markedMirroredComponentsAsCausingInterferencePatterns();
//      timer.stop();
//      if (UnitTest.unitTesting() == false)
//        System.out.println("Time to find mirrored components: " + timer.getElapsedTimeInMillis());

//      for (TestSubProgram subProgram : _testProgram.getAllTestSubPrograms())
//      {
//        if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
//          subProgram.setInspectionRegions(_rightInspectionRegions);
//        else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
//          subProgram.setInspectionRegions(_leftInspectionRegions);
//        else
//        Assert.expect(false);
//      }

      _testProgram.removeUnusedTestSubPrograms();
      
      //no longer need this 
      //_testProgram.addNotInspectableTestSubProgramFilters();
      
      //Siew Yeng -  XCR1752 - change sequence to avoid alignment setup crash
      //initializeSubProgramForAlignment();
      _imageAcquisitionEngine.defineProcessorStripsForInspectionRegions(_testProgram);
 
      if(_TEST_IRP_BOUNDARIES)
        createProcessorStripBoundingReconstructionRegionsForIrpBoundaryTest();

      fitToHardwareLimits();

      if(_TEST_IRP_BOUNDARIES)
      {
        TestSubProgram subProgram = _testProgram.getTestSubProgram(PanelLocationInSystemEnum.RIGHT);
//        Assert.expect(subProgram.getAllInspectionRegions().size() == 8 * subProgram.getProcessorStripsForInspectionRegions().size());
      }
      //Siew Yeng -  XCR1752 - double check before initialize subprogram for alignment
      //XCR-3393, Assert after select "Auto Select Alignment Group"
      timer.reset();
      timer.start();
      initializeAlignmentSubProgram();
      
      timer.stop();
      if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
        System.out.println("generateTestProgramDebug - Time to create alignment regions base on user gain: " + timer.getElapsedTimeInMillis());
      
      timer.reset();
      timer.start();
      createFocusGroups();
      timer.stop();
      if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
        System.out.println("generateTestProgramDebug - Time to create focus groups: " + timer.getElapsedTimeInMillis());
      
      timer.reset();
      timer.start();
      setUpSurroundingPadUtil();
      timer.stop();
      if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
        System.out.println("generateTestProgramDebug - Time to setup surrounding pad: " + timer.getElapsedTimeInMillis());

      timer.reset();
      timer.start();
      setupFocusPriorities();
      timer.stop();
      if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
        System.out.println("generateTestProgramDebug - Time to set focus priorities: " + timer.getElapsedTimeInMillis());

      timer.reset();
      timer.start();
      setupArtifactEffects();
      timer.stop();
      if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
        System.out.println("generateTestProgramDebug - Time to set up artifact effects: " + timer.getElapsedTimeInMillis());

      timer.reset();
      timer.start();
      //SwingWorkerThread.getInstance().invokeLater(new Runnable()
      //{
      //  public void run()
      //  {
     //      setupProjectSurfaceMap();
      //  }
     // });
      timer.stop();
      if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
        System.out.println("generateTestProgramDebug - Time to create PSP Program: " + timer.getElapsedTimeInMillis());
      
      timer.reset();
      timer.start();
      createVerificationRegions(true);
      timer.stop();
      if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
        System.out.println("generateTestProgramDebug - Time to create verification regions:" + _testProgram.getVerificationRegions().size() +
                           " Verification Regions: " + timer.getElapsedTimeInMillis());

      timer.reset();
      timer.start();
      setUpVerificationRegionNeighbors();
      timer.stop();
      if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
        System.out.println("generateTestProgramDebug - Time to setup verification region neighbors: " + timer.getElapsedTimeInMillis());
      
      // now setup the region ids.
      timer.reset();
      timer.start();
      setupRegionIds();
      timer.stop();
      if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
        System.out.println("generateTestProgramDebug - Time to setup region ids: " + timer.getElapsedTimeInMillis());
      
      timer.reset();
      timer.start();
      setUpSurfaceModelingNeighbors();
      timer.stop();
      if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
        System.out.println("generateTestProgramDebug - Time to setup surface modeling neighbors: " + timer.getElapsedTimeInMillis());

      // Determine neighbors for Grid Array Open Outlier
      timer.reset();
      timer.start();
      _testProgram = determineNeighborsForAll(_testProgram);
      timer.stop();
      if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
        System.out.println("generateTestProgramDebug - Time to determine open outlier neighbors: " + timer.getElapsedTimeInMillis());

      // Determine Global Surface Model method for each reconstruction region in each TestSubProgram
      timer.reset();
      timer.start();
      setupGlobalSurfaceModel();
      timer.stop();
      if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
        System.out.println("generateTestProgramDebug - Time to setup GSM: " + timer.getElapsedTimeInMillis());

      if(Config.isDeveloperDebugModeOn())
      {
        try
        {
          if (LicenseManager.isDeveloperSystemEnabled())
          {
            FileWriterUtilAxi writer = new FileWriterUtilAxi(Directory.getProjectDir(_project.getName()) + File.separator +
                                                             "programGenerationInfo.csv", false);
            writer.open();
            writer.writeln("REGION ID, X CENTER, Y CENTER, SUBTYPES, FOCUS GROUP, JOINT TYPE, REF DES, PADS, REGION NAME, INSPECTED, BOARD ID");

            // for debug purposes, we want to write a file
            // that maps from region id to ref des.
            for (ReconstructionRegion region : _testProgram.getAllInspectionRegions())
            {
              writer.write(region.getRegionId() + ",");
              PanelRectangle regionRect = region.getRegionRectangleRelativeToPanelInNanoMeters();
              writer.write(regionRect.getCenterX() + "," + regionRect.getCenterY() + ",");
              for (Subtype subtype : region.getSubtypes())
                writer.write(subtype.getLongName() + " ");
              
              writer.write(",");
              
              for(FocusGroup group : region.getFocusGroups())
              {
                writer.write(" " + group.getId());
              }

              writer.write("," + region.getJointTypeEnum().getName() + "," + region.getComponent().getReferenceDesignator() + ",");

              for (Pad pad : region.getPads())
                writer.write(pad.getName() + " ");
              //writer.writeln("," + region.getName() + ", " + region.isInspected());
              writer.write("," + region.getName() + ", " + region.isInspected());
              writer.writeln(","+ region.getBoard());
            }

            writer.close();
          }
        }
        catch(XrayTesterException dex)
        {
          dex.printStackTrace();
        }
      }
      _testProgram.createTotalReconstructionRegionToSubProgramMap();
       //Kee Chin Seong
      //remove test subProgram the purpose to do this is
      //because fit into hardware limit Might set sone pad to untestable., need remove that test Sub Program
      _testProgram.removeUnusedTestSubPrograms();
      
      //Kee Chin Seong
      //This is when user change from multi gain to single gain and that single gain DON'T HAVE alignment regions,
      //we need to make sure non-inspectable testSubProgram has the alignment regions first then only apply this
      if(_testProgram.hasAlignmentRegionInInspectableTestSubProgram() == false && _testProgram.hasAlignmentRegionInNonInspectableTestSubProgram() == true)
      {
        resetTestProgramUponGainChanged(_testProgram);
        _testProgram.getAlignmentRegionToNewTestProgram();
      }
      
      //Kok Chun, Tan
      if (_enablePsp && _testProgram.getFilteredTestSubPrograms().isEmpty() != true)
      {
        _opticalProgramGeneration.setupNearestOpticalCameraRectangle(_testProgram);
      }
      
      return _testProgram;
    }
    finally
    {
      reset();
      _generateProgramTimer.stop();
      if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
        System.out.println("generateTestProgramDebug - Time to generate program: " + _generateProgramTimer.getElapsedTimeInMillis());

      project.getProjectState().enable();
    }
  }

  /**
   * @author George A. David
   */
  private void setupRegionIds()
  {
    setupRegionIds(0, _testProgram.getAllInspectionRegions());
    setupRegionIds(_testProgram.getAllInspectionRegions().size(), _testProgram.getVerificationRegions());
    setupRegionIds(_testProgram.getAllInspectionRegions().size() + _testProgram.getVerificationRegions().size(), _testProgram.getAlignmentRegions());
  }

  /**
   * @author George A. David
   */
  private void setupVerificationRegionIds()
  {
    setupRegionIds(_testProgram.getAllInspectionRegions().size(), _testProgram.getVerificationRegions());
  }

  /**
   * @author George A. David
   */
  private void setupAlignmentRegionIds()
  {
    setupRegionIds(_testProgram.getAllInspectionRegions().size() + _testProgram.getVerificationRegions().size(), _testProgram.getAlignmentRegions());
  }

  /**
   * @author George A. David
   */
  private void setupRegionIds(int startingRegionId, Collection<ReconstructionRegion> regions)
  {
    Map<String, Set<ReconstructionRegion>> nameToReconstructionRegionMap = new TreeMap<String, Set<ReconstructionRegion>>();
    for (ReconstructionRegion region : regions)
    {
      if (nameToReconstructionRegionMap.containsKey(region.getName()))
      {
        nameToReconstructionRegionMap.get(region.getName()).add(region);
      }
      else
      {
        Set<ReconstructionRegion> newRegions = new TreeSet<ReconstructionRegion>(_reconstructionRegionForIdGenerationComparator);
        newRegions.add(region);
        Object previous = nameToReconstructionRegionMap.put(region.getName(), newRegions);
        Assert.expect(previous == null);
      }
    }

    for (Map.Entry<String, Set<ReconstructionRegion>> mapEntry : nameToReconstructionRegionMap.entrySet())
    {
      for (ReconstructionRegion region : mapEntry.getValue())
      {
        region.setRegionId(startingRegionId++);
      }
    }
  }

  /**
   * @author George A. David
   */
  private void setupUniqueIds(IntegerRef startingReconstructionRegionId, IntegerRef startingFocusRegionId, Collection<ReconstructionRegion> regions)
  {
    int reconstructionId = startingReconstructionRegionId.getValue();
    int focusId = startingFocusRegionId.getValue();

    Map<String, ReconstructionRegion> nameToReconstructionRegionMap = new TreeMap<String, ReconstructionRegion>();
    Map<String, FocusGroup> nameToFocusGroupMap = new TreeMap<String, FocusGroup>();

    for(ReconstructionRegion region : regions)
    {
      Object previous = nameToReconstructionRegionMap.put(region.getName(), region);
      Assert.expect(previous == null, "This is a DEVELOPER DEBUG MODE issue. You will not see this with DEVELOPER DEBUG MODE set to false.");
      for(FocusGroup group : region.getFocusGroups())
      {
        PanelRectangle focusRect = group.getFocusSearchParameters().getFocusRegion().getRectangleRelativeToPanelInNanometers();
        String name = "top_";
        if(region.isTopSide() == false)
          name = "bottom_";
        name += (int)MathUtil.convertNanoMetersToMils(focusRect.getMinX()) + "_" +
                (int)MathUtil.convertNanoMetersToMils(focusRect.getMinY()) + "_" +
                (int)MathUtil.convertNanoMetersToMils(focusRect.getWidth()) + "_" +
                (int)MathUtil.convertNanoMetersToMils(focusRect.getHeight());

        previous = nameToFocusGroupMap.put(name, group);
        Assert.expect(previous == null, "This is a DEVELOPER DEBUG MODE issue. You will not see this with DEVELOPER DEBUG MODE set to false.");
      }
    }
    for(Map.Entry<String, ReconstructionRegion> mapEntry : nameToReconstructionRegionMap.entrySet())
      mapEntry.getValue().setRegionId(reconstructionId++);

    for(Map.Entry<String, FocusGroup> mapEntry : nameToFocusGroupMap.entrySet())
      mapEntry.getValue().setId(focusId++);

    startingReconstructionRegionId.setValue(reconstructionId);
    startingFocusRegionId.setValue(focusId);
  }

  /**
   * @author George A. David
   */
  public void truncateBoundsIfNecessary(PanelRectangle bounds)
  {
    Assert.expect(bounds != null);

    // Depending on which system type we are, if we are in Standard system,
    // we cannot test anything wider than 17.5 inches. If we are in Throughput system,
    // we cannot test anything wider than 16.9 inches.
    if(bounds.getWidth() > _maximumPanelInspectableWidthInNanometers)
    {
      int newX = (int)Math.rint(bounds.getMinX() + (bounds.getWidth() - _maximumPanelInspectableWidthInNanometers) / 2.0);
      bounds.setRect(newX,
                     bounds.getMinY(),
                     _maximumPanelInspectableWidthInNanometers,
                     bounds.getHeight());
    }
  }


  /**
   * @author Sunit Bhalla
   */
  private void determineOpenOutlierNeighbors()
  {
    for (ReconstructionRegion region : _testProgram.getAllInspectionRegions())
      if ((region.getJointTypeEnum().equals(JointTypeEnum.COLLAPSABLE_BGA)) ||
          (region.getJointTypeEnum().equals(JointTypeEnum.NON_COLLAPSABLE_BGA)) ||
          (region.getJointTypeEnum().equals(JointTypeEnum.CGA)) ||
          (region.getJointTypeEnum().equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR)) ||
          (region.getJointTypeEnum().equals(JointTypeEnum.CHIP_SCALE_PACKAGE)) )
      {
        for (JointInspectionData joint : region.getJointInspectionDataList())
        {
          Collection<JointInspectionData> neighbors = determineNeighborsForOpenOutlierTest(joint);
          joint.setNeighboringJointsForOpenOutlier(neighbors);
        }
      }
  }

  /**
   * @author Sunit Bhalla
   * Determines neighbors for all joints in the region containing the joint parameter.  This is used to regenerate
   * neighbors when a joint is marked as "No Test."
   */
  public void determineOpenOutlierNeighbors(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);

    ReconstructionRegion region = jointInspectionData.getInspectionRegion();
    if ((region.getJointTypeEnum().equals(JointTypeEnum.COLLAPSABLE_BGA)) ||
        (region.getJointTypeEnum().equals(JointTypeEnum.NON_COLLAPSABLE_BGA)) ||
        (region.getJointTypeEnum().equals(JointTypeEnum.CGA)) ||
        (region.getJointTypeEnum().equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR)) ||
        (region.getJointTypeEnum().equals(JointTypeEnum.CHIP_SCALE_PACKAGE)) )
    {
      for (JointInspectionData joint : region.getJointInspectionDataList())
      {
        Collection<JointInspectionData> neighbors = determineNeighborsForOpenOutlierTest(joint);
        joint.setNeighboringJointsForOpenOutlier(neighbors);
      }
    }
  }

  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   *
   * This method returns the neighbors to use in the neighbor outlier test.  It will return the
   * three closest joints in the inspection region.  It will also include other joints that are the same
   * distance away as the "set of three."
   */
  public Collection<JointInspectionData> determineNeighborsForOpenOutlierTest(JointInspectionData jointInspectionData)
  {
    List<JointInspectionData> neighboringJointsForOpenOutlier = new ArrayList<JointInspectionData>();

    final boolean PRINT_OPEN_OUTIER_DETAILS = false;
    float JOINT_DISTANCE_COMPARION_PRECISION = 0.001f;

    double ALLOWABLE_DISTANCE_PITCH_MULTIPLIER = 2.5;

    Subtype subtype = jointInspectionData.getSubtype();
    Pad pad = jointInspectionData.getPad();
    
    int pitchInNanometers = pad.getPitchInNanoMeters();
    double allowableDistanceInNanometers = pitchInNanometers * ALLOWABLE_DISTANCE_PITCH_MULTIPLIER;

    List<JointInspectionDataDistancePair> neighboringJointsAndDistances = new ArrayList<JointInspectionDataDistancePair>();

    if (PRINT_OPEN_OUTIER_DETAILS)
    {
      System.out.println("Joint (PG): " + jointInspectionData.getSubtype().getShortName() + ": " + jointInspectionData.getPad().getName() + ": " + pitchInNanometers + ": " + allowableDistanceInNanometers);
    }

    //Siew Yeng - XCR-2762 - neighbour for Gridarray 1x1
    if(subtype.getPanel().getProject().isBGAInspectionRegionSetTo1X1())
    {
      List<JointInspectionData> jointInspectionDataListWholeComponent = new ArrayList();
      for(ReconstructionRegion region : _testProgram.getInspectionRegions(jointInspectionData.getComponent()))
      {
        jointInspectionDataListWholeComponent.addAll(region.getJointInspectionDataList());
      }
      
      jointInspectionData.determineJointDistanceRelationships(jointInspectionDataListWholeComponent);
      
      for(JointInspectionData neighborPad : jointInspectionDataListWholeComponent)
      {
        if(neighborPad != jointInspectionData)
        {
          double distance = jointInspectionData.getDistanceToJointInNanometers(neighborPad);
          if ((distance < allowableDistanceInNanometers))
          {
            neighboringJointsAndDistances.add(new JointInspectionDataDistancePair(neighborPad, distance));
            if (PRINT_OPEN_OUTIER_DETAILS)
            {
              System.out.printf("  " + neighborPad.getFullyQualifiedPadName()+ ": %2.3f ", (float)distance);
            }
          }
        }
      }
    }
    else
    {
      jointInspectionData.determineJointDistanceRelationships(subtype);
      
      for (JointInspectionData neighbor : jointInspectionData.getInspectionRegion().getJointInspectionDataList(subtype))
      {
        if (neighbor != jointInspectionData)
        {
          if (neighbor.getSubtype() == jointInspectionData.getSubtype())
          {
            double distance = jointInspectionData.getDistanceToJointInNanometers(neighbor);
            if ((distance < allowableDistanceInNanometers))
            {
              neighboringJointsAndDistances.add(new JointInspectionDataDistancePair(neighbor, distance));
              if (PRINT_OPEN_OUTIER_DETAILS)
              {
                System.out.printf("  " + neighbor.getPad().getName() + ": %2.3f ", (float)distance);
              }
            }
          }
        }
      }
    }
    Collections.sort(neighboringJointsAndDistances);
    if (PRINT_OPEN_OUTIER_DETAILS)
    {
      System.out.print("\nSorted:    ");
      for (JointInspectionDataDistancePair jointsAndDistances : neighboringJointsAndDistances)
      {
        System.out.printf(" " + jointsAndDistances.getJointInspectionData().getPad().getName() + ": %2.3f ",
                          (float)jointsAndDistances.getDistance());
      }
      System.out.println();
    }

    Iterator<JointInspectionDataDistancePair> iter = neighboringJointsAndDistances.iterator();
    final int MIN_NUMBER_OF_NEIGHBORS_TO_USE = 3;

    // Use the three closest neighbors
    for (int i = 0; i < MIN_NUMBER_OF_NEIGHBORS_TO_USE; i++)
    {
      if (iter.hasNext())
        neighboringJointsForOpenOutlier.add(iter.next().getJointInspectionData());
    }

    // If other neighbors have the same value as the third item, include them.
    while (iter.hasNext())
    {
      JointInspectionDataDistancePair pair = iter.next();
      if (MathUtil.fuzzyEquals(pair.getDistance(),
                               neighboringJointsAndDistances.get(MIN_NUMBER_OF_NEIGHBORS_TO_USE - 1).getDistance(),
                               JOINT_DISTANCE_COMPARION_PRECISION))
      {
        neighboringJointsForOpenOutlier.add(pair.getJointInspectionData());
      }
      else break;
    }

    if (PRINT_OPEN_OUTIER_DETAILS)
    {
      System.out.print("Joints used in open outlier: ");
      for (JointInspectionData joint : neighboringJointsForOpenOutlier)
        System.out.print(joint.getPad().getName() + " ");
      System.out.println();
    }

    Assert.expect(neighboringJointsForOpenOutlier != null);
    return neighboringJointsForOpenOutlier;
  }

  /**
   * @author George Booth
   */
  private void determineQuadFlatNoLeadNeighbors()
  {
    for (ReconstructionRegion region : _testProgram.getAllInspectionRegions())
      if (region.getJointTypeEnum().equals(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD))
      {
        for (JointInspectionData joint : region.getJointInspectionDataList())
        {
          Collection<JointInspectionData> neighbors = determineNeighborsForQuadFlatNoLead(joint);
          joint.setNeighboringJointsForQuadFlatNoLead(neighbors);
        }
      }
  }

  /**
   * Determines neighbors for all joints in the region containing the joint parameter.  This is used to regenerate
   * neighbors when a joint is marked as "No Test."
   *
   * @author George Booth
   */
  public void determineQuadFlatNoLeadNeighbors(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);

    ReconstructionRegion region = jointInspectionData.getInspectionRegion();
    if (region.getJointTypeEnum().equals(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD))
    {
      for (JointInspectionData joint : region.getJointInspectionDataList())
      {
        Collection<JointInspectionData> neighbors = determineNeighborsForQuadFlatNoLead(joint);
        joint.setNeighboringJointsForQuadFlatNoLead(neighbors);
      }
    }
  }

  /**
   * This method returns the neighbors to use in the QFN neighbor test.  It will return the
   * two closest joints in the inspection region.  It will also include other joints that are the same
   * distance away as the "set of two."
   *
   * @author George Booth
   */
  public Collection<JointInspectionData> determineNeighborsForQuadFlatNoLead(JointInspectionData jointInspectionData)
  {
    List<JointInspectionData> neighboringJointsForQuadFlatNoLead = new ArrayList<JointInspectionData>();

    final boolean PRINT_NEIGHBOR_DETAILS = false;
    float JOINT_DISTANCE_COMPARION_PRECISION = 0.001f;

    Subtype subtype = jointInspectionData.getSubtype();
    Pad pad = jointInspectionData.getPad();
    jointInspectionData.determineJointDistanceRelationships(subtype);

    List<JointInspectionDataDistancePair> neighboringJointsAndDistances = new ArrayList<JointInspectionDataDistancePair>();

    if (PRINT_NEIGHBOR_DETAILS)
    {
      System.out.println("Joint (PG): " + jointInspectionData.getSubtype().getShortName() + ": " + jointInspectionData.getPad().getName());
    }

    for (JointInspectionData neighbor : jointInspectionData.getInspectionRegion().getJointInspectionDataList(subtype))
    {
      if (neighbor != jointInspectionData)
      {
        if (neighbor.getSubtype() == jointInspectionData.getSubtype())
        {
          double distance = jointInspectionData.getDistanceToJointInNanometers(neighbor); // /pitchInNanometers;
          neighboringJointsAndDistances.add(new JointInspectionDataDistancePair(neighbor, distance));
        }
      }
    }
    Collections.sort(neighboringJointsAndDistances);
    if (PRINT_NEIGHBOR_DETAILS)
    {
      System.out.println("  Sorted:    ");
      for (JointInspectionDataDistancePair jointsAndDistances : neighboringJointsAndDistances)
      {
        System.out.println("   " + jointsAndDistances.getJointInspectionData().getPad().getName() + ": " +
                          jointsAndDistances.getDistance());
      }
    }

    Iterator<JointInspectionDataDistancePair> iter = neighboringJointsAndDistances.iterator();
    final int MIN_NUMBER_OF_NEIGHBORS_TO_USE = 2;

    // Use the two closest neighbors
    for (int i = 0; i < MIN_NUMBER_OF_NEIGHBORS_TO_USE; i++)
    {
      if (iter.hasNext())
        neighboringJointsForQuadFlatNoLead.add(iter.next().getJointInspectionData());
    }

    // If other neighbors have the same value as the second item, include them.
    while (iter.hasNext())
    {
      JointInspectionDataDistancePair pair = iter.next();
      if (MathUtil.fuzzyEquals(pair.getDistance(),
                               neighboringJointsAndDistances.get(MIN_NUMBER_OF_NEIGHBORS_TO_USE - 1).getDistance(),
                               JOINT_DISTANCE_COMPARION_PRECISION))
      {
        neighboringJointsForQuadFlatNoLead.add(pair.getJointInspectionData());
      }
      else break;
    }

    if (PRINT_NEIGHBOR_DETAILS)
    {
      System.out.print("Joints used in QuadFlatNoLead: ");
      for (JointInspectionData joint : neighboringJointsForQuadFlatNoLead)
        System.out.print(joint.getPad().getName() + " ");
      System.out.println();
    }

    Assert.expect(neighboringJointsForQuadFlatNoLead != null);
    return neighboringJointsForQuadFlatNoLead;
  }


  /**
   * @author George A. David
   * @author Scott Richardson
   */
  protected void createTestSubProgram()
  {
    Panel panel = _project.getPanel();

    calculatePadAreaRuleValues();
    PanelSettings panelSettings = panel.getPanelSettings();

    boolean isLongPanel = panelSettings.isLongPanel();
//    PanelRectangle imageableRegion = null;

    ReconstructionRegion.resetRegionCounter();
    TestSubProgram.resetNumberOfTestSubProgram();
    
    for(MagnificationTypeEnum magnificationTypeEnum : MagnificationTypeEnum.getMagnificationToEnumMap().values()) 
    {
      if((_config.getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED)==false && _config.getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED)==false) &&
         magnificationTypeEnum.equals(MagnificationTypeEnum.HIGH))
      {
        continue;
      }
      
      if(panelSettings.hasHighMagnificationComponent() == false && 
         magnificationTypeEnum.equals(MagnificationTypeEnum.HIGH) )
        continue;
     
      if(panelSettings.hasLowMagnificationComponent() == false && 
         magnificationTypeEnum.equals(MagnificationTypeEnum.LOW) )
        continue;
      
//      _rightSectionOfPanel = panelSettings.getRightSectionOfLongPanel();
      
      if(panelSettings.isPanelBasedAlignment())
      {
        // create the sub programs.
        if (isLongPanel)
        {
          // Create Right TestSubProgram
          TestSubProgram rightProgram = createTestSubProgram(_testProgram, 
                                                             XrayTester.getMaxImageableAreaLengthInNanoMeters(),
                                                             panelSettings.getAlignmentMaxImageableAreaLengthInNanoMeters(),
                                                             PanelLocationInSystemEnum.RIGHT,
                                                             panelSettings.getRightSectionOfLongPanel(), 
                                                             panelSettings.getRightAlignmentSectionOfLongPanel(),
                                                             _project.getPanel().getPanelSettings().getPanelBaseUserGain(),
                                                             _project.getPanel().getPanelSettings().getPanelBaseStageSpeed(),
                                                             magnificationTypeEnum);
          _testProgram.addTestSubProgram(rightProgram);

          // Create Left TestSubProgram
          TestSubProgram leftProgram = createTestSubProgram(_testProgram, 
                                                            XrayTester.getMaxImageableAreaLengthInNanoMeters(),
                                                            panelSettings.getAlignmentMaxImageableAreaLengthInNanoMeters(),
                                                            PanelLocationInSystemEnum.LEFT, 
                                                            panelSettings.getLeftSectionOfLongPanel(), 
                                                            panelSettings.getLeftAlignmentSectionOfLongPanel(),
                                                            _project.getPanel().getPanelSettings().getPanelBaseUserGain(),
                                                            _project.getPanel().getPanelSettings().getPanelBaseStageSpeed(),
                                                            magnificationTypeEnum);
          _testProgram.addTestSubProgram(leftProgram);          
        }
        else
        {
          TestSubProgram subProgram = createTestSubProgram(_testProgram, 
                                                           panel.getLengthAfterAllRotationsInNanoMeters(),
                                                           panel.getLengthAfterAllRotationsInNanoMeters(),
                                                           PanelLocationInSystemEnum.RIGHT, 
                                                           panelSettings.getRightSectionOfLongPanel(), 
                                                           panelSettings.getRightAlignmentSectionOfLongPanel(),
                                                           _project.getPanel().getPanelSettings().getPanelBaseUserGain(),
                                                           _project.getPanel().getPanelSettings().getPanelBaseStageSpeed(),
                                                           magnificationTypeEnum);
          _testProgram.addTestSubProgram(subProgram);
        }
      }
      else
      {
        // board based alignment method
        for(Board board : panel.getBoards())
        {
          if(board.getBoardSettings().isLongBoard())
          {
            TestSubProgram rightProgram  = createTestSubProgram(_testProgram, 
                                                                XrayTester.getMaxImageableAreaLengthInNanoMeters(),
                                                                board.getAlignmentMaxImageableAreaLengthInNanoMeters(),
                                                                PanelLocationInSystemEnum.RIGHT, 
                                                                board.getRightSectionOfLongBoard(),
                                                                board.getRightSectionOfLongBoard(),
                                                                _project.getPanel().getPanelSettings().getPanelBaseUserGain(),
                                                                _project.getPanel().getPanelSettings().getPanelBaseStageSpeed(),
                                                                magnificationTypeEnum,
                                                                board);
            _testProgram.addTestSubProgram(rightProgram);

            TestSubProgram leftProgram = createTestSubProgram(_testProgram, 
                                                              XrayTester.getMaxImageableAreaLengthInNanoMeters(),
                                                              board.getAlignmentMaxImageableAreaLengthInNanoMeters(),
                                                              PanelLocationInSystemEnum.LEFT, 
                                                              board.getLeftSectionOfLongBoard(), 
                                                              board.getLeftSectionOfLongBoard(),
                                                              _project.getPanel().getPanelSettings().getPanelBaseUserGain(),
                                                              _project.getPanel().getPanelSettings().getPanelBaseStageSpeed(),
                                                              magnificationTypeEnum,
                                                              board);
            _testProgram.addTestSubProgram(leftProgram);
          }
          else
          {
            TestSubProgram subProgram;
            if (board.getPanel().getPanelSettings().isLongPanel() && 
                board.getRightSectionOfLongBoard().getMaxY() <= XrayTester.getMaxImageableAreaLengthInNanoMeters())
            {
              subProgram = createTestSubProgram(_testProgram, 
                                                board.getLengthAfterAllRotationsInNanoMeters(),
                                                board.getRightSectionOfLongBoard().getHeight() ,
                                                PanelLocationInSystemEnum.LEFT, 
                                                board.getRightSectionOfLongBoard(), 
                                                board.getRightSectionOfLongBoard(),
                                                _project.getPanel().getPanelSettings().getPanelBaseUserGain(),
                                                _project.getPanel().getPanelSettings().getPanelBaseStageSpeed(),
                                                magnificationTypeEnum,
                                                board);
            }
            else
            {
              subProgram = createTestSubProgram(_testProgram,
                                                board.getLengthAfterAllRotationsInNanoMeters(),
                                                board.getRightSectionOfLongBoard().getHeight(),
                                                PanelLocationInSystemEnum.RIGHT,
                                                board.getRightSectionOfLongBoard(),
                                                board.getRightSectionOfLongBoard(), 
                                                _project.getPanel().getPanelSettings().getPanelBaseUserGain(),
                                                _project.getPanel().getPanelSettings().getPanelBaseStageSpeed(),
                                                magnificationTypeEnum,
                                                board);
            }
            _testProgram.addTestSubProgram(subProgram);
          }
        }
      }
    }
    _testProgram.sortTestSubProgram();
  }

  /**
   * @author George A. David
   */
  public void updateAlignmentRegions(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);
    try
    {
      _testProgram = testProgram;
      _project = testProgram.getProject();
      clearIsSubProgramPerformSurfaceMapSetting();
      clearIsSubProgramPerformAlignmentSetting();
      initializeAlignmentSubProgram();
      setupProjectSurfaceMap();
      if (_didAlignmentRegionsChange)
      {
        createVerificationRegions(true);
        setupVerificationRegionIds();
      }
      setupAlignmentRegionIds();
    }
    finally
    {
      reset();
    }
  }

  /**
   * @author George A. David
   */
  public void createAlignmentRegions()
  {
    _didAlignmentRegionsChange = false;

    PanelSettings panelSettings = _testProgram.getProject().getPanel().getPanelSettings();
    Panel panel = _testProgram.getProject().getPanel();

    Set<AlignmentGroup> groups = new HashSet<AlignmentGroup>();

    if (panelSettings.isPanelBasedAlignment())
    {
      groups.addAll(panelSettings.getAllAlignmentGroups());
    }
    else
    {
      for(Board board : panel.getBoards())
        groups.addAll(board.getBoardSettings().getAllAlignmentGroups());
    }

    for (TestSubProgram subProgram : _testProgram.getAllTestSubPrograms())
    {
      if (panelSettings.isPanelBasedAlignment())
      {
        if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
        {
          if (panelSettings.isLongPanel())
          {
            createAlignmentRegions(subProgram, panelSettings.getRightAlignmentGroupsForLongPanel());
            groups.removeAll(panelSettings.getRightAlignmentGroupsForLongPanel());
          }
          else
          {
            createAlignmentRegions(subProgram, panelSettings.getAlignmentGroupsForShortPanel());
            groups.removeAll(panelSettings.getAlignmentGroupsForShortPanel());
          }
        }
        else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
        {
          createAlignmentRegions(subProgram, panelSettings.getLeftAlignmentGroupsForLongPanel());
          groups.removeAll(panelSettings.getLeftAlignmentGroupsForLongPanel());
        }
        else
        {
          Assert.expect(false);
        }
      }
      else
      {
        // WC: todo the code
        if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
        {
          if (subProgram.getBoard().isLongBoard())
          {
            createAlignmentRegions(subProgram, subProgram.getBoard().getBoardSettings().getRightAlignmentGroupsForLongPanel());
            groups.removeAll(subProgram.getBoard().getBoardSettings().getRightAlignmentGroupsForLongPanel());
          }
          else
          {
            createAlignmentRegions(subProgram, subProgram.getBoard().getBoardSettings().getAlignmentGroupsForShortPanel());
            groups.removeAll(subProgram.getBoard().getBoardSettings().getAlignmentGroupsForShortPanel());
          }
        }
        else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
        {
          createAlignmentRegions(subProgram, subProgram.getBoard().getBoardSettings().getLeftAlignmentGroupsForLongPanel());
          groups.removeAll(subProgram.getBoard().getBoardSettings().getLeftAlignmentGroupsForLongPanel());
        }
        else
        {
          Assert.expect(false);
        }
      }
    }

    // if there are any groups left, it means they are no longer valid
    for(AlignmentGroup group : groups)
    {
      // it's only invalid if it has pads, otherwise they haven't yet tried to set it up!
      if(group.isEmpty() == false)
        _testProgram.addInvalidatedAlignmentGroup(group);
    }
  }

  /**
   * @author George A. David
   */
  public void generateDebugSlices(TestProgram testProgram, int numSlices, int sliceDistanceInNanoMeters)
  {
    Assert.expect(testProgram != null);
    Assert.expect(numSlices > 0);
    Assert.expect(sliceDistanceInNanoMeters > 0);

    for(ReconstructionRegion inspectionRegion : testProgram.getAllInspectionRegions())
    {
      int nextSliceNumber = inspectionRegion.getInspectedSlices().size();
      for(FocusGroup group : inspectionRegion.getFocusGroups())
      {
        List<Slice> newSlices = new LinkedList<Slice>();
        for(Slice slice : group.getSlices())
        {
          FocusInstruction instruction = slice.getFocusInstruction();
          for(int i = -numSlices; i <= numSlices; ++i)
          {
            if(i == 0)
              continue;
            Slice newSlice = new Slice();
            ++nextSliceNumber;
            newSlice.setSliceType(slice.getSliceType());
            newSlice.setSliceName(SliceNameEnum.getAdditionalSliceName(i + numSlices));
            FocusInstruction newInstruction = new FocusInstruction();
            newInstruction.setFocusMethod(instruction.getFocusMethod());
            newInstruction.setPercentValue(instruction.getPercentValue());
            newInstruction.setZOffsetInNanoMeters(instruction.getZOffsetInNanoMeters() + (i * sliceDistanceInNanoMeters));
            newInstruction.setZHeightInNanoMeters(instruction.getZHeightInNanoMeters());
            newSlice.setFocusInstruction(newInstruction);
            newSlice.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
            newSlice.setOriginalSliceForAdditionalSlice(slice);
            newSlices.add(newSlice);
          }
        }

        for(Slice slice : newSlices)
          group.addSlice(slice);
      }
    }
  }

  /**
   * @author George A. David
   */
  private void calculatePadAreaRuleValues()
  {
    Assert.expect(_project != null);

    Panel panel = _project.getPanel();
    double cosineDegreesRotation = Math.cos(Math.toRadians(panel.getDegreesRotationAfterAllRotations()));
    double sineDegreesRotation = Math.sin(Math.toRadians(panel.getDegreesRotationAfterAllRotations()));
    double panelLengthInNanoMeters = panel.getLengthAfterAllRotationsInNanoMeters();
    double panelWidthInNanoMeters = panel.getWidthAfterAllRotationsInNanoMeters();
    double padAreaRuleMultiplier = (panelLengthInNanoMeters * panelWidthInNanoMeters) /
                                   (((panelLengthInNanoMeters * cosineDegreesRotation) + (panelWidthInNanoMeters * sineDegreesRotation)) *
                                    ((panelLengthInNanoMeters * sineDegreesRotation) + (panelWidthInNanoMeters * cosineDegreesRotation)));
    padAreaRuleMultiplier = 1.0;
    _minPadAreaToTotalAreaRatioForGridArrayFocusRegions = _MIN_PAD_AREA_TO_TOTAL_AREA_RATIO_GRID_ARRAY_FOCUS_REGIONS * padAreaRuleMultiplier;
    _minPadAreaToTotalAreaRatioForThrouhHoleFocusRegions = _MIN_PAD_AREA_TO_TOTAL_AREA_RATIO_THROUGH_HOLE_FOCUS_REGIONS * padAreaRuleMultiplier;
    _minPadAreaToTotalAreaRatioForGullwingFocusRegions = _MIN_PAD_AREA_TO_TOTAL_AREA_RATIO_GULLWING_FOCUS_REGIONS * padAreaRuleMultiplier;
    _minPadAreaToTotalAreaRatioForReconstructionRegions = _MIN_PAD_AREA_TO_TOTAL_AREA_RATIO_RECONSTRUCTION_REGIONS * padAreaRuleMultiplier;
  }

  /**
   * @author George A. David
   */
  protected void createVerificationRegions(boolean createBottomReconstructionRegions)
  {
    Assert.expect(_testProgram != null);
    // now define the processor strip rectangles for verification regions
    _imageAcquisitionEngine.defineProcessorStripsForVerificationRegions(_testProgram);

    PanelSettings panelSettings = _project.getPanel().getPanelSettings();

    for (TestSubProgram subProgram : _testProgram.getAllTestSubPrograms())
    {
      if(subProgram.isSubProgramPerformAlignment() == false)
      {
        //clear the unused verification regions to avoid assert at setupRegionIds
        subProgram.setVerificationRegions(new LinkedList<ReconstructionRegion>());
        continue;
      }
      List<ReconstructionRegion> verificationRegions = new LinkedList<ReconstructionRegion>();
      int numIrps = subProgram.getProcessorStripsForVerificationRegions().size();

//      if (UnitTest.unitTesting() == false)
//        System.out.println("Using " + numIrps + " image reconstruction processors for verification images");

      PanelRectangle verificationRegion = subProgram.getVerificationRegionsBoundsInNanoMetersIgnoringAlignmentRegions();
      double irpImageWidthInNanoMeters = (double)verificationRegion.getWidth() / (double)numIrps;

      double irpImageLengthInNanoMeters = verificationRegion.getHeight();

      double largestRegionWidthInNanoMeters = ReconstructionRegion.getMaxVerificationRegionWidthInNanoMeters();
      double largestRegionLengthInNanoMeters = ReconstructionRegion.getMaxVerificationRegionLengthInNanoMeters();
      
      if(subProgram.isHighMagnification())
      {
        largestRegionWidthInNanoMeters = ReconstructionRegion.getMaxHighMagVerificationRegionWidthInNanoMeters();
        largestRegionLengthInNanoMeters = ReconstructionRegion.getMaxHighMagVerificationRegionLengthInNanoMeters();      
      }

      // now create equal size regions
      int numColumnsOfRegions = (int)Math.ceil(irpImageWidthInNanoMeters / largestRegionWidthInNanoMeters);
      int numRowsOfRegions = (int)Math.ceil(irpImageLengthInNanoMeters / largestRegionLengthInNanoMeters);
      double verificationRegionWidthInNanoMeters = irpImageWidthInNanoMeters / numColumnsOfRegions;
      double verificationRegionLengthInNanoMeters = irpImageLengthInNanoMeters / numRowsOfRegions;

      // now determine the largest focus region that fits in each verification region
      // and are multiples of 16 pixels (256 microns)
      double denomiatorInNanoMeters = FocusRegion.getFocusRegionGranularityInNanoMeters();
      double focusRegionWidthInNanoMeters = Math.floor(verificationRegionWidthInNanoMeters / denomiatorInNanoMeters) *
                                                 denomiatorInNanoMeters;
      double focusRegionLengthInNanoMeters = Math.floor(verificationRegionLengthInNanoMeters / denomiatorInNanoMeters) *
                                                  denomiatorInNanoMeters;

      // now figure out the x and y offsets of the focus regions to center them within the verification regions.
      double xFocusRegionOffset = (verificationRegionWidthInNanoMeters - focusRegionWidthInNanoMeters) / 2.0;
      double yFocusRegionOffset = (verificationRegionLengthInNanoMeters - focusRegionLengthInNanoMeters) / 2.0;

      Assert.expect(focusRegionLengthInNanoMeters >= FocusRegion.getMinFocusRegionSizeInNanoMeters());
      Assert.expect(focusRegionWidthInNanoMeters >= FocusRegion.getMinFocusRegionSizeInNanoMeters());

      double minXcoordinate = verificationRegion.getMinX();
      double minYcoordinate = verificationRegion.getMinY();
      double startingXcoordinate = verificationRegion.getMaxX() - verificationRegionWidthInNanoMeters;
      double startingYcoordinate = verificationRegion.getMaxY() - verificationRegionLengthInNanoMeters;

      IntegerRef numRows = new IntegerRef(0);
      IntegerRef numColumns = new IntegerRef(0);
      // top regions
      verificationRegions.addAll(createVerificationRegions(startingXcoordinate,
                                                           startingYcoordinate,
                                                           minXcoordinate,
                                                           minYcoordinate,
                                                           verificationRegionWidthInNanoMeters,
                                                           verificationRegionLengthInNanoMeters,
                                                           xFocusRegionOffset,
                                                           yFocusRegionOffset,
                                                           focusRegionWidthInNanoMeters,
                                                           focusRegionLengthInNanoMeters,
                                                           numRows,
                                                           numColumns,
                                                           subProgram,
                                                           true));

      subProgram.setNumVerificationRegionColumns(numColumns.getValue());
      subProgram.setNumVerificationRegionRows(numRows.getValue());
      if (createBottomReconstructionRegions)
      {
        // bottom regions
        verificationRegions.addAll(createVerificationRegions(startingXcoordinate,
                                                             startingYcoordinate,
                                                             minXcoordinate,
                                                             minYcoordinate,
                                                             verificationRegionWidthInNanoMeters,
                                                             verificationRegionLengthInNanoMeters,
                                                             xFocusRegionOffset,
                                                             yFocusRegionOffset,
                                                             focusRegionWidthInNanoMeters,
                                                             focusRegionLengthInNanoMeters,
                                                             numRows,
                                                             numColumns,
                                                             subProgram,
                                                             false));

        Assert.expect(subProgram.getNumVerificationRegionColumns() == numColumns.getValue());
        Assert.expect(subProgram.getNumVerificationRegionRows() == numRows.getValue());
      }

      subProgram.setVerificationRegions(verificationRegions);
      if(_printDebugInfo)
      {
        PrintWriter pw = null;
        try
        {
          pw = new PrintWriter( Directory.getProjectDir(_project.getName()) + File.separator + "verificationRegion" + subProgram.getId() + ".csv" );

          pw.println(_project.getName() + " - Verification Region");
          pw.println("Center X,Center Y, ,Min X, Min Y");
          for( ReconstructionRegion printRegion : verificationRegions)
          {
            pw.println(printRegion.getCenterCoordinateRelativeToPanelInNanoMeters().getX() + "," +
                       printRegion.getCenterCoordinateRelativeToPanelInNanoMeters().getY() + "," + "," +
                       printRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinX() + "," +
                       printRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinY());
          }
          pw.close();
        }
        catch (Exception e)
        {
          System.out.println("exception : " + e.getMessage());
          pw = null;
        }
      }
    }
  }

  /*
   * Create verification regions for virtual live dummy board
   * @author bee-hoon.goh
   */
   public  void createVerificationRegions(Project project, TestProgram testProgram, boolean createBottomReconstructionRegions)
  {
    Assert.expect(testProgram != null);
    
    _imageAcquisitionEngine.defineProcessorStripsForVerificationRegions(testProgram);

    for (TestSubProgram subProgram : testProgram.getAllTestSubPrograms())
    {
      List<ReconstructionRegion> verificationRegions = new LinkedList<ReconstructionRegion>();
      int numIrps = subProgram.getProcessorStripsForVerificationRegions().size();

      PanelRectangle verificationRegion = subProgram.getVerificationRegionsBoundsInNanoMetersIgnoringAlignmentRegions();
      double irpImageWidthInNanoMeters = (double)verificationRegion.getWidth() / (double)numIrps;

      double irpImageLengthInNanoMeters = verificationRegion.getHeight();

      double largestRegionWidthInNanoMeters = ReconstructionRegion.getMaxVerificationRegionWidthInNanoMeters();
      double largestRegionLengthInNanoMeters = ReconstructionRegion.getMaxVerificationRegionLengthInNanoMeters();
      
      if(subProgram.isHighMagnification())
      {
        largestRegionWidthInNanoMeters = ReconstructionRegion.getMaxHighMagVerificationRegionWidthInNanoMeters();
        largestRegionLengthInNanoMeters = ReconstructionRegion.getMaxHighMagVerificationRegionLengthInNanoMeters();      
      }

      // now create equal size regions
      int numColumnsOfRegions = (int)Math.ceil(irpImageWidthInNanoMeters / largestRegionWidthInNanoMeters);
      int numRowsOfRegions = (int)Math.ceil(irpImageLengthInNanoMeters / largestRegionLengthInNanoMeters);
      double verificationRegionWidthInNanoMeters = irpImageWidthInNanoMeters / numColumnsOfRegions;
      double verificationRegionLengthInNanoMeters = irpImageLengthInNanoMeters / numRowsOfRegions;

      // now determine the largest focus region that fits in each verification region
      // and are multiples of 16 pixels (256 microns)
      double denomiatorInNanoMeters = FocusRegion.getFocusRegionGranularityInNanoMeters();
      double focusRegionWidthInNanoMeters = Math.floor(verificationRegionWidthInNanoMeters / denomiatorInNanoMeters) *
                                                 denomiatorInNanoMeters;
      double focusRegionLengthInNanoMeters = Math.floor(verificationRegionLengthInNanoMeters / denomiatorInNanoMeters) *
                                                  denomiatorInNanoMeters;

      // now figure out the x and y offsets of the focus regions to center them within the verification regions.
      double xFocusRegionOffset = (verificationRegionWidthInNanoMeters - focusRegionWidthInNanoMeters) / 2.0;
      double yFocusRegionOffset = (verificationRegionLengthInNanoMeters - focusRegionLengthInNanoMeters) / 2.0;

      Assert.expect(focusRegionLengthInNanoMeters >= FocusRegion.getMinFocusRegionSizeInNanoMeters());
      Assert.expect(focusRegionWidthInNanoMeters >= FocusRegion.getMinFocusRegionSizeInNanoMeters());

      double minXcoordinate = verificationRegion.getMinX();
      double minYcoordinate = verificationRegion.getMinY();
      double startingXcoordinate = verificationRegion.getMaxX() - verificationRegionWidthInNanoMeters;
      double startingYcoordinate = verificationRegion.getMaxY() - verificationRegionLengthInNanoMeters;

      IntegerRef numRows = new IntegerRef(0);
      IntegerRef numColumns = new IntegerRef(0);
      // top regions
      verificationRegions.addAll(createVerificationRegions(startingXcoordinate,
                                                           startingYcoordinate,
                                                           minXcoordinate,
                                                           minYcoordinate,
                                                           verificationRegionWidthInNanoMeters,
                                                           verificationRegionLengthInNanoMeters,
                                                           xFocusRegionOffset,
                                                           yFocusRegionOffset,
                                                           focusRegionWidthInNanoMeters,
                                                           focusRegionLengthInNanoMeters,
                                                           numRows,
                                                           numColumns,
                                                           subProgram,
                                                           true));

      subProgram.setNumVerificationRegionColumns(numColumns.getValue());
      subProgram.setNumVerificationRegionRows(numRows.getValue());
      if (createBottomReconstructionRegions)
      {
        // bottom regions
        verificationRegions.addAll(createVerificationRegions(startingXcoordinate,
                                                             startingYcoordinate,
                                                             minXcoordinate,
                                                             minYcoordinate,
                                                             verificationRegionWidthInNanoMeters,
                                                             verificationRegionLengthInNanoMeters,
                                                             xFocusRegionOffset,
                                                             yFocusRegionOffset,
                                                             focusRegionWidthInNanoMeters,
                                                             focusRegionLengthInNanoMeters,
                                                             numRows,
                                                             numColumns,
                                                             subProgram,
                                                             false));

        Assert.expect(subProgram.getNumVerificationRegionColumns() == numColumns.getValue());
        Assert.expect(subProgram.getNumVerificationRegionRows() == numRows.getValue());
      }

      subProgram.setVerificationRegions(verificationRegions);
      if(_printDebugInfo)
      {
        PrintWriter pw = null;
        try
        {
          pw = new PrintWriter( Directory.getProjectDir(project.getName()) + File.separator + "verificationRegion" + subProgram.getId() + ".csv" );

          pw.println(project.getName() + " - Verification Region");
          pw.println("Center X,Center Y, ,Min X, Min Y");
          for( ReconstructionRegion printRegion : verificationRegions)
          {
            pw.println(printRegion.getCenterCoordinateRelativeToPanelInNanoMeters().getX() + "," +
                       printRegion.getCenterCoordinateRelativeToPanelInNanoMeters().getY() + "," + "," +
                       printRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinX() + "," +
                       printRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinY());
          }
          pw.close();
        }
        catch (Exception e)
        {
          System.out.println("exception : " + e.getMessage());
          pw = null;
        }
      }
    }
  }
   
  /**
   * @author George A. David
   * @author Yee Seong
   */
  private List<ReconstructionRegion> createVerificationRegions(double startingXcoordinate,
                                                               double startingYcoordinate,
                                                               double minXcoordinate,
                                                               double minYcoordinate,
                                                               double verificationRegionWidthInNanoMeters,
                                                               double verificationRegionLengthInNanoMeters,
                                                               double xFocusRegionOffset,
                                                               double yFocusRegionOffset,
                                                               double focusRegionWidthInNanoMeters,
                                                               double focusRegionLengthInNanoMeters,
                                                               IntegerRef numRegionRows,
                                                               IntegerRef numRegionColumns,
                                                               TestSubProgram subProgram,
                                                               boolean isTopSide)

  {
    Assert.expect(subProgram != null);

    double xCoordinate = startingXcoordinate;
    double yCoordinate = startingYcoordinate;

    List<ReconstructionRegion> verificationRegions = new LinkedList<ReconstructionRegion>();

    int numColumns = 0;
    int numRows = 1;
    while (MathUtil.fuzzyGreaterThan(yCoordinate + verificationRegionLengthInNanoMeters, minYcoordinate))
    {
      // we only need to count the number of columns on the first row.
      if(numRows == 1)
        ++numColumns;

      PanelRectangle regionBounds = new PanelRectangle(0, 0, 0, 0);

      // this is to take care of any rounding errors,
      // we already checked to make sure that it's fuzzy
      // equals or greater than, so this is safe to do here.
      if (yCoordinate < minYcoordinate)
        yCoordinate = minYcoordinate;
      if (xCoordinate < minXcoordinate)
        xCoordinate = minXcoordinate;

      regionBounds.setRect(xCoordinate,
                           yCoordinate,
                           verificationRegionWidthInNanoMeters,
                           verificationRegionLengthInNanoMeters);

      PanelRectangle focusRectangleInNanoMeters = new PanelRectangle((int)(xCoordinate + xFocusRegionOffset),
                                                                     (int)(yCoordinate + yFocusRegionOffset),
                                                                     (int)focusRegionWidthInNanoMeters,
                                                                     (int)focusRegionLengthInNanoMeters);

      ReconstructionRegion verificationRegion = new ReconstructionRegion();
      verificationRegion.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.VERIFICATION);
      verificationRegion.setTopSide(isTopSide);
      verificationRegion.setRegionRectangleRelativeToPanelInNanoMeters(new PanelRectangle(regionBounds));
      verificationRegion.addFocusGroup(createFocusGroupForVerificationOrAlignmentRegion(focusRectangleInNanoMeters,
          verificationRegion));
      verificationRegion.setTestSubProgram(subProgram);
      verificationRegion.setFocusPriority(FocusPriorityEnum.FIRST);
      boolean assigned = assignProcessorStrip(subProgram.getProcessorStripsForVerificationRegions(), verificationRegion);
      Assert.expect(assigned, "assignProcessorStrip failed.");

      verificationRegions.add(verificationRegion);


      if (MathUtil.fuzzyLessThanOrEquals(xCoordinate, minXcoordinate, _DIFFERENCE_VERIFICATION_REGION_WIDTH_ALLOWED_TO_BE_CONSIDERED_EQUAL))
      {
        xCoordinate = startingXcoordinate;
        yCoordinate -= verificationRegionLengthInNanoMeters;
        ++numRows;
      }
      else
        xCoordinate -= verificationRegionWidthInNanoMeters;
    }

    // the last row doesn't really exist. because it won't make in the while statement
    --numRows;

    numRegionRows.setValue(numRows);
    numRegionColumns.setValue(numColumns);

    return verificationRegions;
  }


  /**
   * @author George A. David
   */
  private void setUpVerificationRegionNeighbors()
  {
    for(TestSubProgram subProgram : _testProgram.getAllTestSubPrograms())
    {
      // ok, now setup the neighbors for each verification region
      SurroundingReconstructionRegionUtil<ReconstructionRegion> regionUtil = new SurroundingReconstructionRegionUtil<ReconstructionRegion>();
      regionUtil.setReconstructionRegions(subProgram.getVerificationRegions());
      for(ReconstructionRegion region : subProgram.getVerificationRegions())
      {
        Collection<ReconstructionRegion> neighbors = regionUtil.getSurroundingReconstructionRegions(region, region.isTopSide());
        //Siew Yeng - XCR-2151 - Incorrect Z-Height on Pin Side Slice for PTH When Using PSH on Single-Sided Board
        // add apposite side points as neighbour - handle single sided board for PTH
        neighbors.addAll(regionUtil.getSurroundingReconstructionRegions(region, !region.isTopSide()));
        neighbors.remove(region);
        FocusGroup group = region.getFocusGroups().get(0);
        ImageReconstructionEngineEnum reconstructionEngineId = region.getReconstructionEngineId();
        for(ReconstructionRegion neighbor : neighbors)
        {
          if(neighbor.getReconstructionEngineId().equals(reconstructionEngineId))
            group.addNeighbor(neighbor);
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  private void calculateInterferenceAffectedRegions(Component affectingComponent,
                                                    Map<ComponentType, Set<String>> componentTypeToAffectedIdsMap,
                                                    Map<String, Set<ReconstructionRegion>> idToReconstructionRegionsMap,
                                                    SurroundingReconstructionRegionUtil<ReconstructionRegion> regionUtil,
                                                    AffineTransform normalizeBoardDataTransform)
  {
    Assert.expect(affectingComponent != null);
    Assert.expect(componentTypeToAffectedIdsMap != null);


    Assert.expect(idToReconstructionRegionsMap != null);
    Assert.expect(regionUtil != null);
    Assert.expect(normalizeBoardDataTransform != null);

    Board affectingBoard = affectingComponent.getBoard();
    ComponentType componentType = affectingComponent.getComponentType();
    if(componentTypeToAffectedIdsMap.containsKey(componentType) == false)
      componentTypeToAffectedIdsMap.put(componentType, new HashSet<String>());

    Set<String> affectedIds = componentTypeToAffectedIdsMap.get(componentType);

    // create an area of all the pads on this component I am using an area
    // because later I will see if an inspection region intersects this area.
    // and if there is a big space between pads where an inspection region
    // could lie, I don't want that inspection regions to be marked as
    // interference affected, because it isn't.
    Area padArea = affectingComponent.getPadAreaRelativeToPanelInNanoMeters();


    // create a region to determine which regions are artifact affected.
    PanelRectangle artifactAffectedRegion = new PanelRectangle(padArea);

    Set<ReconstructionRegion> affectedRegions = regionUtil.getIntersectingReconstructionRegionsInRectangle(artifactAffectedRegion, !affectingComponent.isTopSide());
    for (ReconstructionRegion affectedRegion : affectedRegions)
    {
      // we now know this region is intersected by the bounds of the pads
      // now let's check that it is intersected by the actual pads
      if(padArea.intersects(affectedRegion.getRegionRectangleRelativeToPanelInNanoMeters().getRectangle2D()))
      {
        affectedRegion.setIsAffectedByInterferencePattern(true);

        // set the correct component

        Board affectedBoard = affectedRegion.getComponent().getBoard();
        if (affectedBoard == affectingBoard)
          affectedRegion.addComponentThatCausesInterferencePattern(affectingComponent);
        else
        {
          for (Component component : componentType.getComponents())
          {
            if (affectedBoard == component.getBoard())
              affectedRegion.addComponentThatCausesInterferencePattern(component);
          }
        }

        PanelRectangle affectedNormalizedRegion = new PanelRectangle(normalizeBoardDataTransform.createTransformedShape(affectedRegion.
                                                                                                                        getRegionRectangleRelativeToPanelInNanoMeters().
                                                                                                                        getRectangle2D()));
        String affectedId = affectedBoard.getBoardType().getName() + "_" +
                            (int)MathUtil.convertNanometersToMillimeters(affectedNormalizedRegion.getMinX()) + "_" +
                            (int)MathUtil.convertNanometersToMillimeters(affectedNormalizedRegion.getMinY()) + "_" +
                            (int)MathUtil.convertNanometersToMillimeters(affectedNormalizedRegion.getWidth()) + "_" +
                            (int)MathUtil.convertNanometersToMillimeters(affectedNormalizedRegion.getHeight());
        affectedIds.add(affectedId);
        if (idToReconstructionRegionsMap.containsKey(affectedId) == false)
          idToReconstructionRegionsMap.put(affectedId, new HashSet<ReconstructionRegion>());

        idToReconstructionRegionsMap.get(affectedId).add(affectedRegion);
      }
    }
  }

  /**
   * @author George A. David
   */
  public int getMaxStepSizeInNanos(SignalCompensationEnum signalCompensation, int maxStepSizeInNanos, int maxAverageStepSizeInNanos)
  {
    Assert.expect(signalCompensation != null);

    int maxStepSizeForRegion = -1;
    if(signalCompensation.equals(SignalCompensationEnum.DEFAULT_LOW))
    {
      // do nothing
      maxStepSizeForRegion = maxAverageStepSizeInNanos;
    }
    else if(signalCompensation.equals(SignalCompensationEnum.MEDIUM))
    {
      maxStepSizeForRegion = maxStepSizeInNanos / 2;
    }
    else if(signalCompensation.equals(SignalCompensationEnum.MEDIUM_HIGH))
    {
      maxStepSizeForRegion = maxStepSizeInNanos / 3;
    }
    else if(signalCompensation.equals(SignalCompensationEnum.HIGH))
    {
      maxStepSizeForRegion = maxStepSizeInNanos / 4;
    }
    else if (signalCompensation.equals(SignalCompensationEnum.SUPER_HIGH))
    {
      maxStepSizeForRegion = maxStepSizeInNanos / 8;
    }
    // Khang Wah, 2013-05-21, New scan route, did not expected old scan path 
    // generation method to reach the following cases, any how just prepare for
    // in case.
    else  if (signalCompensation.equals(SignalCompensationEnum.IL_1_5))
    {
      maxStepSizeForRegion = (maxStepSizeInNanos / 3) * 2;
    }
    else  if (signalCompensation.equals(SignalCompensationEnum.IL_5))
    {
      maxStepSizeForRegion = maxStepSizeInNanos / 5;
    }
    else  if (signalCompensation.equals(SignalCompensationEnum.IL_6))
    {
      maxStepSizeForRegion = maxStepSizeInNanos / 6;
    }
    else  if (signalCompensation.equals(SignalCompensationEnum.IL_7))
    {
      maxStepSizeForRegion = maxStepSizeInNanos / 7;
    }
    else
    {
      Assert.expect(false);
    }

    Assert.expect(maxStepSizeForRegion > 0);
//    Assert.expect(maxStepSizeForRegion <= maxAverageStepSizeInNanos);
    return maxStepSizeForRegion;
  }
  
  /**
   * @author Chnee Khang wah
   */
  public int getMinStepSizeInNanos(SignalCompensationEnum signalCompensation, int maxStepSizeInNanos, int minStepSizeInNanos)
  {
    Assert.expect(signalCompensation != null);

    int minStepSizeForRegion = -1;
    if(signalCompensation.equals(SignalCompensationEnum.DEFAULT_LOW))
    {
      // min step size of DEFAULT_LOW (IL1) is max step size of MEDIUM (IL1.5)
      minStepSizeForRegion = (maxStepSizeInNanos / 3) * 2;
    }
    else if(signalCompensation.equals(SignalCompensationEnum.IL_1_5))
    {
      // min step size of IL_1_5 (IL1.5) is max step size of MEDIUM (IL2)
      minStepSizeForRegion = maxStepSizeInNanos / 2;
    }
    else if(signalCompensation.equals(SignalCompensationEnum.MEDIUM))
    {
      // min step size of MEDIUM (IL2) is max step size of MEDIUM_HIGH (IL3)
      minStepSizeForRegion = maxStepSizeInNanos / 3;
    }
    else if(signalCompensation.equals(SignalCompensationEnum.MEDIUM_HIGH))
    {
      // min step size of MEDIUM_HIGH (IL3) is max step size of HIGH (IL4)
      minStepSizeForRegion = maxStepSizeInNanos / 4;
    }
    else if(signalCompensation.equals(SignalCompensationEnum.HIGH))
    {
      // min step size of HIGH (IL4) is max step size of IL_5 (IL5)
      minStepSizeForRegion = maxStepSizeInNanos / 5;
    }
    else if(signalCompensation.equals(SignalCompensationEnum.IL_5))
    {
      // min step size of IL_5 (IL5) is max step size of IL_6 (IL6)
      minStepSizeForRegion = maxStepSizeInNanos / 6;
    }
    else if(signalCompensation.equals(SignalCompensationEnum.IL_6))
    {
      // min step size of IL_6 (IL6) is max step size of IL_7 (IL7)
      minStepSizeForRegion = maxStepSizeInNanos / 7;
    }
    else if(signalCompensation.equals(SignalCompensationEnum.IL_7))
    {
      // min step size of IL_7 (IL7) is max step size of IL_8 (IL8)
      minStepSizeForRegion = maxStepSizeInNanos / 8;
    }
    else if (signalCompensation.equals(SignalCompensationEnum.SUPER_HIGH))
    {
      // min step size of DEFAULT_LOW (IL8)
      minStepSizeForRegion = minStepSizeInNanos;
    }
    else
      Assert.expect(false);

    Assert.expect(minStepSizeForRegion > 0);
    return minStepSizeForRegion;
  }

    /**
   * @author Cheah Lee Herng
   */
  public List<ReconstructionRegion> createAlignmentRegionsForVirtualLive(TestSubProgram subProgram,
                                                                         List<AlignmentGroup> alignmentGroups)
  {
    Assert.expect(alignmentGroups != null);

    List<ReconstructionRegion> regions = new LinkedList<ReconstructionRegion>();

    for (AlignmentGroup alignmentGroup : alignmentGroups)
    {
      if (alignmentGroup.isEmpty())
      {
        if (subProgram.getTestProgram().isAlignmentGroupInvalidated(alignmentGroup))
        {
          subProgram.getTestProgram().removeInvalidatedAlignmentGroup(alignmentGroup);
        }

        continue;
      }
      int previousAlignmentRegionId = -1;
      List<Pad> pads = alignmentGroup.getPads();
      List<Fiducial> fiducials = alignmentGroup.getFiducials();

      if (subProgram.hasAlignmentGroup(alignmentGroup))
      {
        ReconstructionRegion alignmentRegion = subProgram.getAlignmentRegion(alignmentGroup);
        previousAlignmentRegionId = alignmentRegion.getRegionId();

        Set<Pad> oldPads = new HashSet<Pad>(alignmentRegion.getPads());
        // check to make sure we need to re-create this region. if not, don't!!!
        if (oldPads.size() == pads.size())
        {
          Set<Fiducial> oldFids = new HashSet<Fiducial>(alignmentRegion.getFiducials());
          if (oldFids.size() == fiducials.size())
          {
            oldPads.removeAll(pads);
            oldFids.removeAll(fiducials);

            if (oldPads.isEmpty() && oldFids.isEmpty())
            {
              // ok, now we know that we have the same pads and fiducials,
              // but the user may have rotated some pads/components, so let's check
              // that they are still valid
              if (arePadsOrthogonal(pads))
              {
                boolean regionIsContainedByProcessorStrip = false;
                for (ProcessorStrip processorStrip : subProgram.getProcessorStripsForInspectionRegions())
                {
                  if (processorStrip.getReconstructionEngineId().equals(alignmentRegion.getReconstructionEngineId())
                    && processorStrip.getRegion().contains(alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters()))
                  {
                    regionIsContainedByProcessorStrip = true;
                    break;
                  }
                }

                if (regionIsContainedByProcessorStrip)
                {
                  regions.add(alignmentRegion);
                  continue;
                }
                else if (alignmentRegionIsOnLeftBoardSideOutsideOfInspectionArea(subProgram, alignmentRegion))
                {
                  regions.add(alignmentRegion);
                  continue;
                }
              }
            }
          }
        }
      }

      ReconstructionRegion region = createAlignmentRegion(pads, fiducials);
      region.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
      if (previousAlignmentRegionId >= 0)
      {
        region.setRegionId(previousAlignmentRegionId);
      }
      region.setAlignmentGroup(alignmentGroup);
      PanelRectangle regionRect = region.getRegionRectangleRelativeToPanelInNanoMeters();
      PanelRectangle featureBounds = region.getAlignmentFeatureBoundsInNanoMeters();

      PanelRectangle imageableRegionInNanoMeters = subProgram.getAlignmentImageableRegionInNanoMeters();
      if (regionRect.getWidth() <= ReconstructionRegion.getMaxAlignmentRegionWidthInNanoMeters()
        && regionRect.getHeight() <= ReconstructionRegion.getMaxAlignmentRegionLengthInNanoMeters()
        && featureBounds.getWidth() <= ReconstructionRegion.getMaxAlignmentFeatureWidthInNanoMeters()
        && featureBounds.getHeight() <= ReconstructionRegion.getMaxAlignmentFeatureLengthInNanoMeters()
        && imageableRegionInNanoMeters.contains(regionRect) && arePadsOrthogonal(pads))
      {
        _didAlignmentRegionsChange = true;
        regions.add(region);
        // ok, add the alignment uncertainty to each side, then truncate it
        // to make it fit it fit within the processor strip bounds.
        // then truncate it again if it's bigger than the max size.
        double xCoord = regionRect.getMinX() - Alignment.getAlignmentUncertaintyBorderInNanoMeters();
        double yCoord = regionRect.getMinY() - Alignment.getAlignmentUncertaintyBorderInNanoMeters();
        regionRect.setRect(xCoord,
          yCoord,
          regionRect.getWidth() + 2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
          regionRect.getHeight()
          + 2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters());
        Area regionArea = new Area(regionRect.getRectangle2D());
        xCoord = regionRect.getCenterX() - ReconstructionRegion.getMaxAlignmentRegionWidthInNanoMeters() / 2.0;
        yCoord = regionRect.getCenterY() - ReconstructionRegion.getMaxAlignmentRegionLengthInNanoMeters() / 2.0;
        Area maxArea = new Area(new Rectangle2D.Double(xCoord,
          yCoord,
          ReconstructionRegion.getMaxAlignmentRegionWidthInNanoMeters(),
          ReconstructionRegion.getMaxAlignmentRegionLengthInNanoMeters()));
        regionArea.intersect(maxArea);

        region.setRegionRectangleRelativeToPanelInNanoMeters(new PanelRectangle(regionArea));
        createFocusGroupForAlignmentRegion(region, subProgram.getMagnificationType());
        if (subProgram.getTestProgram().isAlignmentGroupInvalidated(alignmentGroup))
        {
          subProgram.getTestProgram().removeInvalidatedAlignmentGroup(alignmentGroup);
        }

        // now create the allowable region that can be used to align on.
        generateAllowableAlignmentRegion(region, subProgram);
      }
      else
      {
        if (subProgram.getTestProgram().isAlignmentGroupInvalidated(alignmentGroup) == false)
        {
          _didAlignmentRegionsChange = true;
          subProgram.getTestProgram().addInvalidatedAlignmentGroup(alignmentGroup);
        }
      }
    }

    if (_didAlignmentRegionsChange)
    {
      // Make sure our alignment context pads are up-to-date.
      for (ReconstructionRegion alignmentRegion : regions)
      {
        assignContextPadsForAlignmentRegion(alignmentRegion, subProgram);
      }
    }
    return regions;
  }

  /**
   * @author George A. David
   */
  private Pair<Integer, Integer> calculateMinAndMaxBadPhaseShifts(boolean isTopSide,
                                                                  int pitchInNanos,
                                                                  double zAffectingRegion1inNanos,
                                                                  double zAffectingRegion2inNanos,
                                                                  double zAffectedRegion1inNanos,
                                                                  double zAffectedRegion2inNanos,
                                                                  double zAffectedRegion3inNanos,
                                                                  double zAffectedRegion4inNanos,
                                                                  int minStepSizeInNanos,
                                                                  int maxStepSizeInNanos)
  {
    Assert.expect(pitchInNanos >= 0);
    Assert.expect(minStepSizeInNanos > 0);
    Assert.expect(maxStepSizeInNanos > 0);

    int multiplier = 1;
    if(isTopSide == false)
      multiplier = -1;

    // let's determine the min and max bad phase shifts.
    double phaseShift = calculateBadPhaseShift(pitchInNanos,
                                               zAffectingRegion1inNanos,
                                               zAffectedRegion1inNanos,
                                               minStepSizeInNanos);
    double minShift = phaseShift * multiplier;
    double maxShift = phaseShift * multiplier;
    phaseShift = calculateBadPhaseShift(pitchInNanos,
                                        zAffectingRegion1inNanos,
                                        zAffectedRegion2inNanos,
                                        minStepSizeInNanos);
    minShift = Math.min(minShift, phaseShift * multiplier);
    maxShift = Math.max(maxShift, phaseShift * multiplier);

    phaseShift = calculateBadPhaseShift(pitchInNanos,
                                        zAffectingRegion1inNanos,
                                        zAffectedRegion3inNanos,
                                        minStepSizeInNanos);
    minShift = Math.min(minShift, phaseShift * multiplier);
    maxShift = Math.max(maxShift, phaseShift * multiplier);

    phaseShift = calculateBadPhaseShift(pitchInNanos,
                                        zAffectingRegion1inNanos,
                                        zAffectedRegion4inNanos,
                                        minStepSizeInNanos);
    minShift = Math.min(minShift, phaseShift * multiplier);
    maxShift = Math.max(maxShift, phaseShift * multiplier);

    phaseShift = calculateBadPhaseShift(pitchInNanos,
                                        zAffectingRegion2inNanos,
                                        zAffectedRegion1inNanos,
                                        minStepSizeInNanos);
    minShift = Math.min(minShift, phaseShift * multiplier);
    maxShift = Math.max(maxShift, phaseShift * multiplier);

    phaseShift = calculateBadPhaseShift(pitchInNanos,
                                        zAffectingRegion2inNanos,
                                        zAffectedRegion2inNanos,
                                        minStepSizeInNanos);
    minShift = Math.min(minShift, phaseShift * multiplier);
    maxShift = Math.max(maxShift, phaseShift * multiplier);

    phaseShift = calculateBadPhaseShift(pitchInNanos,
                                        zAffectingRegion2inNanos,
                                        zAffectedRegion3inNanos,
                                        minStepSizeInNanos);
    minShift = Math.min(minShift, phaseShift * multiplier);
    maxShift = Math.max(maxShift, phaseShift * multiplier);

    phaseShift = calculateBadPhaseShift(pitchInNanos,
                                        zAffectingRegion2inNanos,
                                        zAffectedRegion4inNanos,
                                        minStepSizeInNanos);
    minShift = Math.min(minShift, phaseShift * multiplier);
    maxShift = Math.max(maxShift, phaseShift * multiplier);

    phaseShift = calculateBadPhaseShift(pitchInNanos,
                                        zAffectingRegion1inNanos,
                                        zAffectedRegion1inNanos,
                                        maxStepSizeInNanos);
    minShift = Math.min(minShift, phaseShift * multiplier);
    maxShift = Math.max(maxShift, phaseShift * multiplier);

    phaseShift = calculateBadPhaseShift(pitchInNanos,
                                        zAffectingRegion1inNanos,
                                        zAffectedRegion2inNanos,
                                        maxStepSizeInNanos);
    minShift = Math.min(minShift, phaseShift * multiplier);
    maxShift = Math.max(maxShift, phaseShift * multiplier);

    phaseShift = calculateBadPhaseShift(pitchInNanos,
                                        zAffectingRegion1inNanos,
                                        zAffectedRegion3inNanos,
                                        maxStepSizeInNanos);
    minShift = Math.min(minShift, phaseShift * multiplier);
    maxShift = Math.max(maxShift, phaseShift * multiplier);

    phaseShift = calculateBadPhaseShift(pitchInNanos,
                                        zAffectingRegion1inNanos,
                                        zAffectedRegion4inNanos,
                                        maxStepSizeInNanos);
    minShift = Math.min(minShift, phaseShift * multiplier);
    maxShift = Math.max(maxShift, phaseShift * multiplier);

    phaseShift = calculateBadPhaseShift(pitchInNanos,
                                        zAffectingRegion2inNanos,
                                        zAffectedRegion1inNanos,
                                        maxStepSizeInNanos);
    minShift = Math.min(minShift, phaseShift * multiplier);
    maxShift = Math.max(maxShift, phaseShift * multiplier);

    phaseShift = calculateBadPhaseShift(pitchInNanos,
                                        zAffectingRegion2inNanos,
                                        zAffectedRegion2inNanos,
                                        maxStepSizeInNanos);
    minShift = Math.min(minShift, phaseShift * multiplier);
    maxShift = Math.max(maxShift, phaseShift * multiplier);

    phaseShift = calculateBadPhaseShift(pitchInNanos,
                                        zAffectingRegion2inNanos,
                                        zAffectedRegion3inNanos,
                                        maxStepSizeInNanos);
    minShift = Math.min(minShift, phaseShift * multiplier);
    maxShift = Math.max(maxShift, phaseShift * multiplier);

    phaseShift = calculateBadPhaseShift(pitchInNanos,
                                        zAffectingRegion2inNanos,
                                        zAffectedRegion4inNanos,
                                        maxStepSizeInNanos);
    minShift = Math.min(minShift, phaseShift * multiplier);
    maxShift = Math.max(maxShift, phaseShift * multiplier);


    int minBadPhaseShift = (int)Math.floor(Math.max(minShift, 1));
    int maxBadPhaseShift = (int)Math.ceil(Math.min(maxShift, 100));

    return new Pair<Integer, Integer>(minBadPhaseShift, maxBadPhaseShift);
  }

  /**
   * @author George A. David
   */
  private void calculateBadStepRangesInNanoMeters(Component component,
                                                RangeUtil rangeUtil,
                                                int badPhaseShift,
                                                int pitchInNanos,
                                                double zAffectingRegion1inNanos,
                                                double zAffectingRegion2inNanos,
                                                double zAffectedRegion1inNanos,
                                                double zAffectedRegion2inNanos,
                                                int minAverageStepSizeInNanos,
                                                int maxAverageStepSizeInNanos)
  {
    Assert.expect(component != null);
    Assert.expect(rangeUtil != null);
    Assert.expect(pitchInNanos >= 0);

    double step = calculateBadStepSizeInNanoMeters(pitchInNanos,
                                        zAffectingRegion1inNanos,
                                        zAffectedRegion1inNanos,
                                        badPhaseShift);
    double minLongStep = step;
    double maxLongStep = step;

    step = calculateBadStepSizeInNanoMeters(pitchInNanos,
                                        zAffectingRegion1inNanos,
                                        zAffectedRegion2inNanos,
                                        badPhaseShift);
    minLongStep = Math.min(minLongStep, step);
    maxLongStep = Math.max(maxLongStep, step);

    step = calculateBadStepSizeInNanoMeters(pitchInNanos,
                                        zAffectingRegion2inNanos,
                                        zAffectedRegion1inNanos,
                                        badPhaseShift);
    minLongStep = Math.min(minLongStep, step);
    maxLongStep = Math.max(maxLongStep, step);

    step = calculateBadStepSizeInNanoMeters(pitchInNanos,
                                        zAffectingRegion2inNanos,
                                        zAffectedRegion2inNanos,
                                        badPhaseShift);
    minLongStep = Math.min(minLongStep, step);
    maxLongStep = Math.max(maxLongStep, step);

    // make sure the min is not less then the minAverage
    int minStep = (int)Math.floor(Math.max(minLongStep, minAverageStepSizeInNanos));
    // make sure the min is not bigger than the maxAverage
    minStep = Math.min(minStep, maxAverageStepSizeInNanos);

    // make sure the max is not bigger than the maxAverage
    int maxStep = (int)Math.ceil(Math.min(maxLongStep, maxAverageStepSizeInNanos));
    // make sure the max is not lower than the minAverage
    maxStep = Math.max(maxStep, minAverageStepSizeInNanos);

    // ok, now validate the bad step range
    RangeUtil validatedBadRanges = getValidBadStepRanges(minStep, maxStep, component.getLandPattern().getNumRows(), badPhaseShift);

    rangeUtil.subtract(validatedBadRanges);

    if(_printDebugInfo)
    {
      System.out.println("min bad step: " + minStep);
      System.out.println("max bad step: " + maxStep);
      System.out.print("validated bad ranges: " );
      for(Pair<Integer, Integer> pair : validatedBadRanges.getRanges())
      {
        System.out.print(pair.getFirst() + "-" + pair.getSecond() + " ");
      }
      System.out.println();
    }
  }

  /**
   * @author George A. David
   */
  private void determineValidStepSizeRange(Component component,
                                         int minBadPhaseShift,
                                         int maxBadPhaseShift,
                                         int pitchInNanos,
                                         double zAffectingRegion1inNanos,
                                         double zAffectingRegion2inNanos,
                                         double zAffectedRegion1inNanos,
                                         double zAffectedRegion2inNanos,
                                         double zAffectedRegion3inNanos,
                                         double zAffectedRegion4inNanos,
                                         int minAverageStepSizeInNanos,
                                         int maxAverageStepSizeInNanos,
                                         RangeUtil rangeUtil)
  {
    Assert.expect(component != null);
    Assert.expect(minBadPhaseShift > 0);
    Assert.expect(maxBadPhaseShift <= 100);
    Assert.expect(pitchInNanos >= 0);
    Assert.expect(minAverageStepSizeInNanos > 0);
    Assert.expect(maxAverageStepSizeInNanos > 0);
    Assert.expect(rangeUtil != null);

    for (int i = minBadPhaseShift; i <= maxBadPhaseShift; i++)
    {
      int badPhaseShift = i;
      if (component.isTopSide() == false)
        badPhaseShift = -badPhaseShift;

      if(i % 2 == 0)
      {
        calculateBadStepRangesInNanoMeters(component,
                                           rangeUtil,
                                           badPhaseShift,
                                           pitchInNanos,
                                           zAffectingRegion1inNanos,
                                           zAffectingRegion2inNanos,
                                           zAffectedRegion1inNanos,
                                           zAffectedRegion2inNanos,
                                           minAverageStepSizeInNanos,
                                           maxAverageStepSizeInNanos);
      }
      else
      {
        calculateBadStepRangesInNanoMeters(component,
                                           rangeUtil,
                                           badPhaseShift,
                                           pitchInNanos,
                                           zAffectingRegion1inNanos,
                                           zAffectingRegion2inNanos,
                                           zAffectedRegion3inNanos,
                                           zAffectedRegion4inNanos,
                                           minAverageStepSizeInNanos,
                                           maxAverageStepSizeInNanos);
      }
      if (rangeUtil.hasRanges() == false)
        break;
    }
  }
  
  /**
   * @author George A. David
   */
  private int getModeOfDistanceToClosestRowOfPads(Collection<Pad> pads, Map<String, Double> keyToClosestRowDistanceMap)
  {
    Assert.expect(pads != null);
    Assert.expect(keyToClosestRowDistanceMap != null);

    Component component = pads.iterator().next().getComponent();
    String baseKey = getLandPatternKey(component.getLandPattern(), component.getDegreesRotationAfterAllRotations());

    if(_printDebugInfo)
    {
      System.out.println("rotation: " + component.getDegreesRotationAfterAllRotations());
    }
    // if one of the pads exist, the all do
    if(keyToClosestRowDistanceMap.containsKey(baseKey + "_" +  pads.iterator().next().getName()) == false)
    {
      List<Pad> compPads = component.getPads();
      SurroundingPadUtil padUtil = new SurroundingPadUtil();
      padUtil.setPads(compPads);
      for(Pad pad : compPads)
      {
        double distanceToClosestRow = padUtil.getDistanceToClosestRowOfPads(pad);
        // in order to take into account any rounding errors, let's use mil integer values.
        // Otherwise, this will see 2299968 and 2299970 as different values which they are, but
        // nanometers is not an appropriate scale, because these values are 90.55 and 90.54999... in mils
        // respectively, and for our purposes, those are basically equal. so let's just round to the nearest mil.

        keyToClosestRowDistanceMap.put(baseKey + "_" + pad.getName(), Math.rint(MathUtil.convertNanoMetersToMils(distanceToClosestRow)));
      }
    }

    // get the mode
    Map<Double, Integer> distanceToNumPads = new HashMap<Double, Integer>();
    for(Pad pad : pads)
    {
      double distance = keyToClosestRowDistanceMap.get(baseKey + "_" + pad.getName());
      Integer numPads = distanceToNumPads.get(distance);

      if(numPads == null)
        distanceToNumPads.put(distance, 1);
      else
        distanceToNumPads.put(distance, numPads + 1);
    }

     double modeDistance = -1;
     int maxCount = 0;
     for(Map.Entry<Double, Integer> entry : distanceToNumPads.entrySet())
     {
       if(modeDistance == -1 || maxCount < entry.getValue())
       {
         modeDistance = entry.getKey();
         maxCount = entry.getValue();
       }
       // when there is a tie, chosse the larger value
       else if(maxCount == entry.getValue() && modeDistance < entry.getKey())
         modeDistance = entry.getKey();
     }

     // now convert to nanometers and truncate it to an int
     return (int)MathUtil.convertMilsToNanoMeters(modeDistance);
  }

  /**
   * @author George A. David
   */
  private void setupArtifactEffects()
  {
    SurroundingReconstructionRegionUtil<ReconstructionRegion> regionUtil = new SurroundingReconstructionRegionUtil<ReconstructionRegion>();
    regionUtil.setUseBoundsForDeterminingVicinity(true);
    regionUtil.setReconstructionRegions(_testProgram.getAllInspectionRegions());

    Map<String, Set<ReconstructionRegion>> idToReconstructionRegionsMap = new HashMap<String, Set<ReconstructionRegion>>();
    Map<ComponentType, Set<String>> componentTypeToAffectedIdsMap = new HashMap<ComponentType, Set<String>>();

    Map<ComponentType, Boolean> compTypeToCausesInterferencePatternMap = new HashMap<ComponentType, Boolean>();
    for(Board board : _project.getPanel().getBoards())
    {
      // create a transform to go from panel coordinates and rotation to
      // board coordinates and no rotation
      AffineTransform normalizeBoardDataTransform = new AffineTransform();
      board.preConcatenateShapeTransform(normalizeBoardDataTransform);
      try
      {
        normalizeBoardDataTransform = normalizeBoardDataTransform.createInverse();
      }
      catch (NoninvertibleTransformException ex)
      {
        Assert.logException(ex);
      }

      for (Component component : board.getComponents())
      {
        //XCR1718 - All components use IL2, same gain & low mag only, but has 2 scan paths
        //Siew Yeng - no need to calculate artifact effects if component is no load
        if(_project.getScanPathMethod().getId() >= ScanPathMethodEnum.METHOD_MERGE_WITHOUT_NOLOAD_COMPONENTS.getId())
        {
          if(component.isLoaded() == false)
            continue;
        }
        
        if (compTypeToCausesInterferencePatternMap.containsKey(component.getComponentType()) == false)
        {
          boolean causesInterferencePattern = false;
          // ok, it only causes interference if it is within 5 degrees of any 45 degree angle,
          // i.e 0, 45, 90, etc
          // For example rotation -5 is drip, 5 is drip, -6 is not drip, 6 is drip, 45 is drip, 47 is drip, 51 is not drip, etc.
          // normalize the rotation to within 45 degrees
          double normalizedRotation = component.getDegreesRotationAfterAllRotations() % 45;
          if (MathUtil.fuzzyLessThanOrEquals(normalizedRotation - 0.0, 5.0) ||
              MathUtil.fuzzyLessThanOrEquals(45 - normalizedRotation, 5.0))
          {
            for (JointTypeEnum jointType : component.getJointTypeEnums())
            {
              // determine if the region is a drip region
              if (jointType.equals(JointTypeEnum.CGA) ||
                  jointType.equals(JointTypeEnum.COLLAPSABLE_BGA) ||
                  jointType.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) ||
                  jointType.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) ||
                  jointType.equals(JointTypeEnum.GULLWING) ||
                  jointType.equals(JointTypeEnum.SURFACE_MOUNT_CONNECTOR) ||
                  jointType.equals(JointTypeEnum.JLEAD) ||
                  jointType.equals(JointTypeEnum.LGA))
              {
                causesInterferencePattern = true;
                break;
              }
              
              // New scan route, new IC consideration
              // Chnee Khang Wah, 2013-04-08
              if(_testProgram.getProject().isGenerateByNewScanRoute() && 
                 jointType.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
              {
                causesInterferencePattern = true;
                break;
              }
            }
          }

          compTypeToCausesInterferencePatternMap.put(component.getComponentType(), causesInterferencePattern);
        }

        if (compTypeToCausesInterferencePatternMap.get(component.getComponentType()))
        {
          calculateInterferenceAffectedRegions(component,
                                               componentTypeToAffectedIdsMap,
                                               idToReconstructionRegionsMap,
                                               regionUtil,
                                               normalizeBoardDataTransform);
        }
      }
    }
    
    if(_testProgram.getProject().isGenerateByNewScanRoute())
    {
      setupEntropyBaseArtifactEffects(componentTypeToAffectedIdsMap,
                                      idToReconstructionRegionsMap);
    }
    else
    {
      setupArtifactEffects(componentTypeToAffectedIdsMap,
                           idToReconstructionRegionsMap);
    }
  }

  /**
   * @author George A. David
   */
  private void calculateInterferencePatternZheights(boolean isTopSide,
                                                    int maxJointHeight,
                                                    double zRef,
                                                    double zTop,
                                                    double zBottom,
                                                    DoubleRef zAffectingRegion1,
                                                    DoubleRef zAffectingRegion2,
                                                    DoubleRef zAffectedRegion1,
                                                    DoubleRef zAffectedRegion2,
                                                    DoubleRef zAffectedRegion3,
                                                    DoubleRef zAffectedRegion4)

  {
    Assert.expect(maxJointHeight > 0);
    Assert.expect(zAffectingRegion1 != null);
    Assert.expect(zAffectingRegion2 != null);
    Assert.expect(zAffectedRegion1 != null);
    Assert.expect(zAffectedRegion2 != null);
    Assert.expect(zAffectedRegion3 != null);
    Assert.expect(zAffectedRegion4 != null);

   if (isTopSide)
    {
      zAffectingRegion1.setValue(zTop - maxJointHeight);
      zAffectingRegion2.setValue(zTop);
      zAffectedRegion1.setValue(Math.max(zBottom + getRequiredDistanceFromInterferenceAffectedRegionInNanoMetersTypeFull(), Math.max(zAffectingRegion1.getValue(), zAffectingRegion2.getValue()) + 1));
      zAffectedRegion2.setValue(Math.max(zBottom - getRequiredDistanceFromInterferenceAffectedRegionInNanoMetersTypeFull(), Math.max(zAffectingRegion1.getValue(), zAffectingRegion2.getValue()) + 1));
      zAffectedRegion3.setValue(Math.max(zBottom + getRequiredDistanceFromInterferenceAffectedRegionInNanoMetersTypeHalf(), Math.max(zAffectingRegion1.getValue(), zAffectingRegion2.getValue()) + 1));
      zAffectedRegion4.setValue(Math.max(zBottom - getRequiredDistanceFromInterferenceAffectedRegionInNanoMetersTypeHalf(), Math.max(zAffectingRegion1.getValue(), zAffectingRegion2.getValue()) + 1));
    }
    else
    {
      zAffectingRegion1.setValue(zBottom + maxJointHeight);
      zAffectingRegion2.setValue(zBottom);
      zAffectedRegion1.setValue(Math.min(zTop + getRequiredDistanceFromInterferenceAffectedRegionInNanoMetersTypeFull(), Math.min(zAffectingRegion1.getValue(), zAffectingRegion2.getValue()) - 1));
      zAffectedRegion2.setValue(Math.min(zTop - getRequiredDistanceFromInterferenceAffectedRegionInNanoMetersTypeFull(), Math.min(zAffectingRegion1.getValue(), zAffectingRegion2.getValue()) - 1));
      zAffectedRegion3.setValue(Math.min(zTop + getRequiredDistanceFromInterferenceAffectedRegionInNanoMetersTypeHalf(), Math.min(zAffectingRegion1.getValue(), zAffectingRegion2.getValue()) - 1));
      zAffectedRegion4.setValue(Math.min(zTop - getRequiredDistanceFromInterferenceAffectedRegionInNanoMetersTypeHalf(), Math.min(zAffectingRegion1.getValue(), zAffectingRegion2.getValue()) - 1));
    }
  }
  
  private void calculateInterferencePatternZAffecting(Component affectingComponent,
                                                      double zTop,
                                                      double zBottom,
                                                      DoubleRef zAffectingRegion)
  {
    Assert.expect(zAffectingRegion != null);
    
    int dir  = -1;
    double zSide = zTop;
    
    if (affectingComponent.isTopSide())
    {
      dir = -1;
      zSide = zTop;
    }
    else
    {
      dir = 1;
      zSide = zBottom;
    }
    
    for (JointTypeEnum jointType : affectingComponent.getJointTypeEnums())
    {
      if(jointType.equals(JointTypeEnum.PRESSFIT) || 
         jointType.equals(JointTypeEnum.THROUGH_HOLE) ||
         jointType.equals(JointTypeEnum.OVAL_THROUGH_HOLE)) //Siew Yeng - XCR-3318 - Oval PTH
      {
        zAffectingRegion.setValue((zTop + zBottom) / 2);
      }
      else
      {
        int offset = InterferenceCompensationEnum.getOffset(jointType.getName());
        zAffectingRegion.setValue(zSide + dir*offset);
      }
    }
  }

  /**
   * @author George A. David
   */
  
  private void setupEntropyBaseArtifactEffects(Map<ComponentType, Set<String>> componentTypeToAffectedIdsMap,
                                    Map<String, Set<ReconstructionRegion>> idToReconstructionRegionsMap)
  {
    Assert.expect(componentTypeToAffectedIdsMap != null);
    Assert.expect(idToReconstructionRegionsMap != null);

    // high Mag Step Size
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.HIGH);
    
    if(ImagingChainProgramGenerator.getEnableStepSizeOptimization())
      ImagingChainProgramGenerator.forceCalculateMaxStepSizeInNanometers(_project.getPanel().getThicknessInNanometers(), _project.getPanel().getMeansBoardZOffsetInNanometers());
    
    int maxAverageStepSizeInNanosForHighMag = ImagingChainProgramGenerator.getMaxAverageStepSizeInNanometers();
    int minAverageStepSizeInNanosForHighMag = ImagingChainProgramGenerator.getMinAverageStepSizeInNanometers();
    int maxStepSizeInNanosForHighMag = ImagingChainProgramGenerator.calculateMaxStepSizeInNanometers();
    int minStepSizeInNanosForHighMag = ImagingChainProgramGenerator.getMinStepSizeInNanometers();

    double zRefForHighMag = XrayCameraArray.getZHeightFromCameraArrayTo2ndXraySourceInNanometers() / MagnificationEnum.getNominalHighMagnificationAtReferencePlane();
    
    double zBottomForHighMag = zRefForHighMag + StageBelts.getNominalBeltTopDistanceBelowSystemCouponInNanoMeters();
    double zTopForHighMag = zBottomForHighMag - _project.getPanel().getThicknessInNanometers();    
    
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    
    if(ImagingChainProgramGenerator.getEnableStepSizeOptimization())
      ImagingChainProgramGenerator.forceCalculateMaxStepSizeInNanometers(_project.getPanel().getThicknessInNanometers(), _project.getPanel().getMeansBoardZOffsetInNanometers());
    
    int maxAverageStepSizeInNanos = ImagingChainProgramGenerator.getMaxAverageStepSizeInNanometers();
    int minAverageStepSizeInNanos = ImagingChainProgramGenerator.getMinAverageStepSizeInNanometers();
    int maxStepSizeInNanos = ImagingChainProgramGenerator.calculateMaxStepSizeInNanometers();
    int minStepSizeInNanos = ImagingChainProgramGenerator.getMinStepSizeInNanometers();

    double zRef = XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers() / MagnificationEnum.getNominalMagnificationAtReferencePlane();
    
    double zBottom = zRef + StageBelts.getNominalBeltTopDistanceBelowSystemCouponInNanoMeters();
    double zTop = zBottom - _project.getPanel().getThicknessInNanometers();
    
    Map<String, RangeUtil> affectedIdToValidStepRangesMap = new HashMap<String, RangeUtil>();

    Map<Subtype, Integer> subtypeToJointHeightMap = new HashMap<Subtype, Integer>();
    Map<String, Double> keyToClosestRowDistanceMap = new HashMap<String, Double>();
    Set<String> processedRefDesKeys = new HashSet<String>();
    Collection<ReconstructionRegion> affectedRegions= new ArrayList<ReconstructionRegion>();

    int minAverageStepSize = minAverageStepSizeInNanos;
    int minAverageStepSizeForHighMag = minAverageStepSizeInNanosForHighMag;
    // iterate over the list of affecting component types
    for(Map.Entry<ComponentType, Set<String>> mapEntry : componentTypeToAffectedIdsMap.entrySet())
    {         
      minAverageStepSizeInNanos = minAverageStepSize;
      minAverageStepSizeInNanosForHighMag = minAverageStepSizeForHighMag;
      ComponentType componentType = mapEntry.getKey();
      
      // Added by Chnee Khang Wah, 2013-Mar-25
      // gather reconstruction regions' info here
      affectedRegions.clear();
      for (String id : mapEntry.getValue())
      {
        Set<ReconstructionRegion> regions = idToReconstructionRegionsMap.get(id);
        for(ReconstructionRegion re:regions)
        {
          affectedRegions.add(re);
        }
      }
      
      if(componentType.getHigherMagnificationType().equals(MagnificationTypeEnum.LOW))
      {
        int maxStepSizeForRegion = getMaxStepSizeInNanos(componentType.getSignalCompensation(), maxStepSizeInNanos, maxAverageStepSizeInNanos);
      
        if (minAverageStepSizeInNanos > maxStepSizeForRegion)
        {
          minAverageStepSizeInNanos = maxStepSizeForRegion;
        }
        
        for (Component affectingComponent : componentType.getComponents())
        {
          String key = affectingComponent.getReferenceDesignator() + "_" + affectingComponent.isTopSide();
          if (processedRefDesKeys.contains(key) == false)
          {
            processedRefDesKeys.add(key);
            RangeUtil rangeUtil = new RangeUtil(minAverageStepSizeInNanos, maxStepSizeForRegion);

            DoubleRef zAffectingRegion = new DoubleRef(-1);
            
            calculateInterferencePatternZAffecting(affectingComponent,
                                                   zTop,
                                                   zBottom,
                                                   zAffectingRegion);
             
            int effectivePitchInNanos = getModeOfDistanceToClosestRowOfPads(affectingComponent.getPads(), keyToClosestRowDistanceMap);
            
            if (_printDebugInfo)
              System.out.println("effective pitch: " + effectivePitchInNanos);

            if (_printDebugInfo)
            {
              System.out.println("affectingComponentIsTopSide: " + affectingComponent.isTopSide());
              System.out.println("zAffectingRegion: " + zAffectingRegion.getValue());
            }
            
            // Added by Chnee Khang Wah, 2013-Mar-25
            // gather the following paramaters into reconstruction region
            // for entropy calculation after stepranges are decided
            for (ReconstructionRegion reg : affectedRegions)
            {
              reg.addAffectingPicth(Math.max(25400, effectivePitchInNanos)); // to avoid pitch=0
              reg.addzAffectingRegion(zAffectingRegion.getValue());
            }
            
            // for each affected id, calculate the candidate steps
            for (String affectedId : mapEntry.getValue())
            {   
              if (affectedIdToValidStepRangesMap.containsKey(affectedId) == false)
                affectedIdToValidStepRangesMap.put(affectedId, rangeUtil);
              else
              {
                RangeUtil existingRangeUtil = affectedIdToValidStepRangesMap.get(affectedId);
                affectedIdToValidStepRangesMap.put(affectedId, RangeUtil.intersect(existingRangeUtil, rangeUtil));
              }
            }
            _printDebugInfo = false;
          }
        }
      }
      // high Mag
      else
      {
        int maxStepSizeForRegion = getMaxStepSizeInNanos(componentType.getSignalCompensation(), maxStepSizeInNanosForHighMag, maxAverageStepSizeInNanosForHighMag);
        
        if (minAverageStepSizeInNanosForHighMag > maxStepSizeForRegion)
        {
          minAverageStepSizeInNanosForHighMag = maxStepSizeForRegion;
        }

        for (Component affectingComponent : componentType.getComponents())
        {
          String key = affectingComponent.getReferenceDesignator() + "_" + affectingComponent.isTopSide();
          if (processedRefDesKeys.contains(key) == false)
          {
            processedRefDesKeys.add(key);
            RangeUtil rangeUtil = new RangeUtil(minAverageStepSizeInNanosForHighMag, maxStepSizeForRegion);

            DoubleRef zAffectingRegion = new DoubleRef(-1);
            
            calculateInterferencePatternZAffecting(affectingComponent,
                                                   zTop,
                                                   zBottom,
                                                   zAffectingRegion);

            int effectivePitchInNanos = getModeOfDistanceToClosestRowOfPads(affectingComponent.getPads(), keyToClosestRowDistanceMap);
            if (_printDebugInfo)
              System.out.println("effective pitch: " + effectivePitchInNanos);

            if (_printDebugInfo)
            {
              System.out.println("affectingComponentIsTopSide: " + affectingComponent.isTopSide());
              System.out.println("zAffectingRegion1: " + zAffectingRegion);
            }

            // Added by Chnee Khang Wah, 2013-Mar-25
            // gather the following paramaters into reconstruction region
            // for entropy calculation after stepranges are decided
            for (ReconstructionRegion reg : affectedRegions)
            {
              reg.addAffectingPicth(Math.max(25400, effectivePitchInNanos)); // to avoid pitch=0
              reg.addzAffectingRegion(zAffectingRegion.getValue());
            }
            
            // for each affected id, calculate the candidate steps
            for (String affectedId : mapEntry.getValue())
            {
              if (affectedIdToValidStepRangesMap.containsKey(affectedId) == false)
                affectedIdToValidStepRangesMap.put(affectedId, rangeUtil);
              else
              {
                RangeUtil existingRangeUtil = affectedIdToValidStepRangesMap.get(affectedId);
                affectedIdToValidStepRangesMap.put(affectedId, RangeUtil.intersect(existingRangeUtil, rangeUtil));
              }
            }
       
            if (_printDebugInfo)
            {
              System.out.println();
              System.out.println();
            }
            _printDebugInfo = false;
          }
        }
      }
    }

    finalizeArtifactEffects(affectedIdToValidStepRangesMap, 
                            idToReconstructionRegionsMap, 
                            minAverageStepSizeInNanos, 
                            minAverageStepSizeInNanosForHighMag,
                            maxStepSizeInNanos,
                            maxAverageStepSizeInNanos,
                            maxStepSizeInNanosForHighMag,
                            maxAverageStepSizeInNanosForHighMag);
  }
  
  /**
   * @author George A. David
   */
  private void setupArtifactEffects(Map<ComponentType, Set<String>> componentTypeToAffectedIdsMap,
                                    Map<String, Set<ReconstructionRegion>> idToReconstructionRegionsMap)
  {
    Assert.expect(componentTypeToAffectedIdsMap != null);
    Assert.expect(idToReconstructionRegionsMap != null);

    // high Mag Step Size
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.HIGH);
    
    if(ImagingChainProgramGenerator.getEnableStepSizeOptimization())
      ImagingChainProgramGenerator.forceCalculateMaxStepSizeInNanometers(_project.getPanel().getThicknessInNanometers(), _project.getPanel().getMeansBoardZOffsetInNanometers());
    
    int maxAverageStepSizeInNanosForHighMag = ImagingChainProgramGenerator.getMaxAverageStepSizeInNanometers();
    int minAverageStepSizeInNanosForHighMag = ImagingChainProgramGenerator.getMinAverageStepSizeInNanometers();
    int maxStepSizeInNanosForHighMag = ImagingChainProgramGenerator.calculateMaxStepSizeInNanometers();
    int minStepSizeInNanosForHighMag = ImagingChainProgramGenerator.getMinStepSizeInNanometers();

    double zRefForHighMag = XrayCameraArray.getZHeightFromCameraArrayTo2ndXraySourceInNanometers() / MagnificationEnum.getNominalHighMagnificationAtReferencePlane();
    
    double zBottomForHighMag = zRefForHighMag + StageBelts.getNominalBeltTopDistanceBelowSystemCouponInNanoMeters();
    double zTopForHighMag = zBottomForHighMag - _project.getPanel().getThicknessInNanometers();    
    
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    
    if(ImagingChainProgramGenerator.getEnableStepSizeOptimization())
      ImagingChainProgramGenerator.forceCalculateMaxStepSizeInNanometers(_project.getPanel().getThicknessInNanometers(), _project.getPanel().getMeansBoardZOffsetInNanometers());
    
    int maxAverageStepSizeInNanos = ImagingChainProgramGenerator.getMaxAverageStepSizeInNanometers();
    int minAverageStepSizeInNanos = ImagingChainProgramGenerator.getMinAverageStepSizeInNanometers();
    int maxStepSizeInNanos = ImagingChainProgramGenerator.calculateMaxStepSizeInNanometers();
    int minStepSizeInNanos = ImagingChainProgramGenerator.getMinStepSizeInNanometers();

    double zRef = XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers() / MagnificationEnum.getNominalMagnificationAtReferencePlane();
    
    double zBottom = zRef + StageBelts.getNominalBeltTopDistanceBelowSystemCouponInNanoMeters();
    double zTop = zBottom - _project.getPanel().getThicknessInNanometers();
    
    Map<String, RangeUtil> affectedIdToValidStepRangesMap = new HashMap<String, RangeUtil>();

    Map<Subtype, Integer> subtypeToJointHeightMap = new HashMap<Subtype, Integer>();
    Map<String, Double> keyToClosestRowDistanceMap = new HashMap<String, Double>();
    Set<String> processedRefDesKeys = new HashSet<String>();
    Collection<ReconstructionRegion> affectedRegions= new ArrayList<ReconstructionRegion>();

    int minAverageStepSize = minAverageStepSizeInNanos;
    int minAverageStepSizeForHighMag = minAverageStepSizeInNanosForHighMag;
    // iterate over the list of affecting component types
    for(Map.Entry<ComponentType, Set<String>> mapEntry : componentTypeToAffectedIdsMap.entrySet())
    {
      minAverageStepSizeInNanos = minAverageStepSize;
      minAverageStepSizeInNanosForHighMag = minAverageStepSizeForHighMag;
      ComponentType componentType = mapEntry.getKey();
      
      // Added by Chnee Khang Wah, 2013-Mar-25
      // gather reconstruction regions' info here
      affectedRegions.clear();
      for (String id : mapEntry.getValue())
      {
        Set<ReconstructionRegion> regions = idToReconstructionRegionsMap.get(id);
        for(ReconstructionRegion re:regions)
        {
          affectedRegions.add(re);
        }
      }
      
      if(componentType.getHigherMagnificationType().equals(MagnificationTypeEnum.LOW))
      {
        int maxStepSizeForRegion = getMaxStepSizeInNanos(componentType.getSignalCompensation(), maxStepSizeInNanos, maxAverageStepSizeInNanos);
      
        if (minAverageStepSizeInNanos > maxStepSizeForRegion)
        {
          minAverageStepSizeInNanos = maxStepSizeForRegion;
        }

        for (Component affectingComponent : componentType.getComponents())
        {
          String key = affectingComponent.getReferenceDesignator() + "_" + affectingComponent.isTopSide();
          if (processedRefDesKeys.contains(key) == false)
          {
            processedRefDesKeys.add(key);
            RangeUtil rangeUtil = new RangeUtil(minAverageStepSizeInNanos, maxStepSizeForRegion);

            // find the max joint height
            int maxJointHeight = 0;
            for (Subtype subtype : affectingComponent.getComponentType().getSubtypes())
            {
              if (subtypeToJointHeightMap.containsKey(subtype) == false)
                subtypeToJointHeightMap.put(subtype, subtype.getJointHeightInNanoMeters());
              maxJointHeight = Math.max(maxJointHeight, subtypeToJointHeightMap.get(subtype));
            }


            DoubleRef zAffectingRegion1 = new DoubleRef(-1);
            DoubleRef zAffectingRegion2 = new DoubleRef(-1);
            DoubleRef zAffectedRegion1 = new DoubleRef(-1);
            DoubleRef zAffectedRegion2 = new DoubleRef(-1);
            DoubleRef zAffectedRegion3 = new DoubleRef(-1);
            DoubleRef zAffectedRegion4 = new DoubleRef(-1);
            calculateInterferencePatternZheights(affectingComponent.isTopSide(),
                                                 maxJointHeight,
                                                 zRef,
                                                 zTop,
                                                 zBottom,
                                                 zAffectingRegion1,
                                                 zAffectingRegion2,
                                                 zAffectedRegion1,
                                                 zAffectedRegion2,
                                                 zAffectedRegion3,
                                                 zAffectedRegion4);

            int effectivePitchInNanos = getModeOfDistanceToClosestRowOfPads(affectingComponent.getPads(), keyToClosestRowDistanceMap);
            if (_printDebugInfo)
              System.out.println("effective pitch: " + effectivePitchInNanos);

            if (_printDebugInfo)
            {
              System.out.println("affectingComponentIsTopSide: " + affectingComponent.isTopSide());
              System.out.println("maxJointHeight: " + maxJointHeight);
              System.out.println("zAffectingRegion1: " + zAffectingRegion1);
              System.out.println("zAffectingRegion2: " + zAffectingRegion2);
              System.out.println("zAffectedRegion1: " + zAffectedRegion1);
              System.out.println("zAffectedRegion2: " + zAffectedRegion2);
              System.out.println("zAffectedRegion3: " + zAffectedRegion3);
              System.out.println("zAffectedRegion4: " + zAffectedRegion4);
            }

            // let's determine the min and max bad phase shifts.
            Pair<Integer, Integer> minAndMaxPair = calculateMinAndMaxBadPhaseShifts(affectingComponent.isTopSide(),
                                                                                    effectivePitchInNanos,
                                                                                    zAffectingRegion1.getValue(),
                                                                                    zAffectingRegion2.getValue(),
                                                                                    zAffectedRegion1.getValue(),
                                                                                    zAffectedRegion2.getValue(),
                                                                                    zAffectedRegion3.getValue(),
                                                                                    zAffectedRegion4.getValue(),
                                                                                    minStepSizeInNanos,
                                                                                    maxStepSizeInNanos);

            //        if(_printDebugInfo)
            //        {
            //          System.out.println("min phase shift: " + minAndMaxPair.getFirst());
            //          System.out.println("max phase shift: " + minAndMaxPair.getSecond());
            //        }
            determineValidStepSizeRange(affectingComponent,
                                        minAndMaxPair.getFirst(),
                                        minAndMaxPair.getSecond(),
                                        effectivePitchInNanos,
                                        zAffectingRegion1.getValue(),
                                        zAffectingRegion2.getValue(),
                                        zAffectedRegion1.getValue(),
                                        zAffectedRegion2.getValue(),
                                        zAffectedRegion3.getValue(),
                                        zAffectedRegion4.getValue(),
                                        minAverageStepSizeInNanos,
                                        maxAverageStepSizeInNanos,
                                        rangeUtil);

            // for each affected id, calculate the candidate steps
            for (String affectedId : mapEntry.getValue())
            {
              if (affectedIdToValidStepRangesMap.containsKey(affectedId) == false)
                affectedIdToValidStepRangesMap.put(affectedId, rangeUtil);
              else
              {
                RangeUtil existingRangeUtil = affectedIdToValidStepRangesMap.get(affectedId);
                affectedIdToValidStepRangesMap.put(affectedId, RangeUtil.intersect(existingRangeUtil, rangeUtil));
              }
            }

            if (_printDebugInfo)
            {
              System.out.println();
              System.out.println();
            }
            _printDebugInfo = false;
          }
        }
      }
      // high Mag
      else
      {
        int maxStepSizeForRegion = getMaxStepSizeInNanos(componentType.getSignalCompensation(), maxStepSizeInNanosForHighMag, maxAverageStepSizeInNanosForHighMag);
      
        if (minAverageStepSizeInNanosForHighMag > maxStepSizeForRegion)
        {
          minAverageStepSizeInNanosForHighMag = maxStepSizeForRegion;
        }

        for (Component affectingComponent : componentType.getComponents())
        {
          String key = affectingComponent.getReferenceDesignator() + "_" + affectingComponent.isTopSide();
          if (processedRefDesKeys.contains(key) == false)
          {
            processedRefDesKeys.add(key);
            RangeUtil rangeUtil = new RangeUtil(minAverageStepSizeInNanosForHighMag, maxStepSizeForRegion);

            // find the max joint height
            int maxJointHeight = 0;
            for (Subtype subtype : affectingComponent.getComponentType().getSubtypes())
            {
              if (subtypeToJointHeightMap.containsKey(subtype) == false)
                subtypeToJointHeightMap.put(subtype, subtype.getJointHeightInNanoMeters());
              maxJointHeight = Math.max(maxJointHeight, subtypeToJointHeightMap.get(subtype));
            }


            DoubleRef zAffectingRegion1 = new DoubleRef(-1);
            DoubleRef zAffectingRegion2 = new DoubleRef(-1);
            DoubleRef zAffectedRegion1 = new DoubleRef(-1);
            DoubleRef zAffectedRegion2 = new DoubleRef(-1);
            DoubleRef zAffectedRegion3 = new DoubleRef(-1);
            DoubleRef zAffectedRegion4 = new DoubleRef(-1);
            calculateInterferencePatternZheights(affectingComponent.isTopSide(),
                                                 maxJointHeight,
                                                 zRefForHighMag,
                                                 zTopForHighMag,
                                                 zBottomForHighMag,
                                                 zAffectingRegion1,
                                                 zAffectingRegion2,
                                                 zAffectedRegion1,
                                                 zAffectedRegion2,
                                                 zAffectedRegion3,
                                                 zAffectedRegion4);

            int effectivePitchInNanos = getModeOfDistanceToClosestRowOfPads(affectingComponent.getPads(), keyToClosestRowDistanceMap);
            if (_printDebugInfo)
              System.out.println("effective pitch: " + effectivePitchInNanos);

            if (_printDebugInfo)
            {
              System.out.println("affectingComponentIsTopSide: " + affectingComponent.isTopSide());
              System.out.println("maxJointHeight: " + maxJointHeight);
              System.out.println("zAffectingRegion1: " + zAffectingRegion1);
              System.out.println("zAffectingRegion2: " + zAffectingRegion2);
              System.out.println("zAffectedRegion1: " + zAffectedRegion1);
              System.out.println("zAffectedRegion2: " + zAffectedRegion2);
              System.out.println("zAffectedRegion3: " + zAffectedRegion3);
              System.out.println("zAffectedRegion4: " + zAffectedRegion4);
            }

            // let's determine the min and max bad phase shifts.
            Pair<Integer, Integer> minAndMaxPair = calculateMinAndMaxBadPhaseShifts(affectingComponent.isTopSide(),
                                                                                    effectivePitchInNanos,
                                                                                    zAffectingRegion1.getValue(),
                                                                                    zAffectingRegion2.getValue(),
                                                                                    zAffectedRegion1.getValue(),
                                                                                    zAffectedRegion2.getValue(),
                                                                                    zAffectedRegion3.getValue(),
                                                                                    zAffectedRegion4.getValue(),
                                                                                    minStepSizeInNanosForHighMag,
                                                                                    maxStepSizeInNanosForHighMag);

            //        if(_printDebugInfo)
            //        {
            //          System.out.println("min phase shift: " + minAndMaxPair.getFirst());
            //          System.out.println("max phase shift: " + minAndMaxPair.getSecond());
            //        }            
            determineValidStepSizeRange(affectingComponent,
                                        minAndMaxPair.getFirst(),
                                        minAndMaxPair.getSecond(),
                                        effectivePitchInNanos,
                                        zAffectingRegion1.getValue(),
                                        zAffectingRegion2.getValue(),
                                        zAffectedRegion1.getValue(),
                                        zAffectedRegion2.getValue(),
                                        zAffectedRegion3.getValue(),
                                        zAffectedRegion4.getValue(),
                                        minAverageStepSizeInNanosForHighMag,
                                        maxAverageStepSizeInNanosForHighMag,
                                        rangeUtil);

            // for each affected id, calculate the candidate steps
            for (String affectedId : mapEntry.getValue())
            {
              if (affectedIdToValidStepRangesMap.containsKey(affectedId) == false)
                affectedIdToValidStepRangesMap.put(affectedId, rangeUtil);
              else
              {
                RangeUtil existingRangeUtil = affectedIdToValidStepRangesMap.get(affectedId);
                affectedIdToValidStepRangesMap.put(affectedId, RangeUtil.intersect(existingRangeUtil, rangeUtil));
              }
            }

            if (_printDebugInfo)
            {
              System.out.println();
              System.out.println();
            }
            _printDebugInfo = false;
          }
        }
      }
    }

//    System.out.println(_project.getName());
//    for(Map.Entry<String, RangeUtil> entry : refDesToValidStepRangesMap.entrySet())
//    {
//      String key = entry.getKey();
//      RangeUtil rangeUtil = entry.getValue();
//      System.out.print(key + ": ");
//      for (Pair<Integer, Integer> pair : rangeUtil.getRanges())
//        System.out.print(pair.getFirst() + "-" + pair.getSecond());
//      System.out.println();
//    }

    finalizeArtifactEffects(affectedIdToValidStepRangesMap, 
                            idToReconstructionRegionsMap, 
                            minAverageStepSizeInNanos, 
                            minAverageStepSizeInNanosForHighMag,
                            maxStepSizeInNanos,
                            maxAverageStepSizeInNanos,
                            maxStepSizeInNanosForHighMag,
                            maxAverageStepSizeInNanosForHighMag);
  }

  /**
   * @author George A. David
   */
  private void finalizeArtifactEffects(Map<String, RangeUtil> affectedIdToValidStepRangesMap,
                                       Map<String, Set<ReconstructionRegion>> idToReconstructionRegionsMap,
                                       int minAverageStepSizeInNanos,
                                       int minAverageStepSizeInNanosForHighMag,
                                       int maxStepSizeInNanos, 
                                       int maxAverageStepSizeInNanos,
                                       int maxStepSizeInNanosForHighMag, 
                                       int maxAverageStepSizeInNanosForHighMag)
  {
    Set<ReconstructionRegion> signalCompensatedRegions = new HashSet<ReconstructionRegion>();

    // determine which regions have signal compensation.
    for(ReconstructionRegion region : _testProgram.getAllInspectionRegions())
    {
      if (region.hasNonDefaultSignalCompensation())
        signalCompensatedRegions.add(region);

      if (region.isAffectedByInterferencePattern() == false)
      {
        for (Pad pad : region.getPads())
          pad.getPadTypeSettings().setIsInterferenceCompensatable(false);
      }
    }

    // ok, now let's set the candidate steps to the actual regions
    for(Map.Entry<String, RangeUtil> entry : affectedIdToValidStepRangesMap.entrySet())
    {
      Set<ReconstructionRegion> regions = idToReconstructionRegionsMap.get(entry.getKey());

      for(ReconstructionRegion region : regions)
      {
        if(entry.getValue().getRanges().isEmpty() )
        {
          for (Pad pad : region.getPads())
            pad.getPadTypeSettings().setIsInterferenceCompensatable(false);
          // add it to to the signal compensated regions so it gets delt with in the next loop
          if( region.hasNonDefaultSignalCompensation() == false)
            signalCompensatedRegions.add(region);
        }
        else
        {
          if(_testProgram.getProject().isGenerateByNewScanRoute())
          {
            int maxStepSizeForRegion = getMaxStepSizeInNanos(region.getSignalCompensation(), maxStepSizeInNanos, maxAverageStepSizeInNanos);
            int minStepSizeForRegion = getMinStepSizeInNanos(region.getSignalCompensation(), maxStepSizeInNanos, minAverageStepSizeInNanos);
            minStepSizeForRegion = Math.min(minStepSizeForRegion, maxStepSizeForRegion);
            RangeUtil rangeUtil = new RangeUtil(minStepSizeForRegion, maxStepSizeForRegion);

            region.setCandidateStepRanges(rangeUtil);
          }
          else
          {
            region.setCandidateStepRanges(entry.getValue());
          }
          for (Pad pad : region.getPads())
            pad.getPadTypeSettings().setIsInterferenceCompensatable(true);
        }
      }
    }

    writeArtifactInteferenceInfoForDebug(affectedIdToValidStepRangesMap, idToReconstructionRegionsMap);

    int minAverageStepSize = minAverageStepSizeInNanos;
    int minAverageStepSizeForHighMag = minAverageStepSizeInNanosForHighMag;

    for (ReconstructionRegion region : signalCompensatedRegions)
    {
      if(region.getSubtypes().iterator().next().getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.LOW))
      {
        minAverageStepSizeInNanos = minAverageStepSize;
        int maxStepSizeForRegion = getMaxStepSizeInNanos(region.getSignalCompensation(), maxStepSizeInNanos, maxAverageStepSizeInNanos);
        if (minAverageStepSizeInNanos > maxStepSizeForRegion)
        {
          minAverageStepSizeInNanos = maxStepSizeForRegion;
        }

        RangeUtil rangeUtil = new RangeUtil(minAverageStepSizeInNanos, maxStepSizeForRegion);

        if(region.isAffectedByInterferencePattern() == false)
        {
          region.setCandidateStepRanges(rangeUtil);
        }
        else if(region.getCandidateStepRanges().hasRanges() == false)
        {
          region.setCandidateStepRanges(rangeUtil);
        }
      }
      else
      {
        minAverageStepSizeInNanosForHighMag = minAverageStepSizeForHighMag;
        int maxStepSizeForRegion = getMaxStepSizeInNanos(region.getSignalCompensation(), maxStepSizeInNanosForHighMag, maxAverageStepSizeInNanosForHighMag);
        if (minAverageStepSizeInNanosForHighMag > maxStepSizeForRegion)
        {
          minAverageStepSizeInNanosForHighMag = maxStepSizeForRegion;
        }

        RangeUtil rangeUtil = new RangeUtil(minAverageStepSizeInNanosForHighMag, maxStepSizeForRegion);

        if(region.isAffectedByInterferencePattern() == false)
        {
          region.setCandidateStepRanges(rangeUtil);
        }
        else if(region.getCandidateStepRanges().hasRanges() == false)
        {
          region.setCandidateStepRanges(rangeUtil);
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  private void writeArtifactInteferenceInfoForDebug(Map<String, RangeUtil> idToBadStepRangesMap,
                                                    Map<String, Set<ReconstructionRegion>> idToReconstructionRegionsMap)
  {
    if(Config.isDeveloperDebugModeOn())
    {
      try
      {
        if (LicenseManager.isDeveloperSystemEnabled())
        {
          FileWriterUtilAxi writer = new FileWriterUtilAxi(Directory.getProjectDir(_project.getName()) + File.separator +
                                                           "artifactInterferenceInfo.csv", false);
          writer.open();
          writer.writeln("REGION ID, REGION NAME, REF DES, PADS, HAS CANDIDATE STEPS");

          // for debug purposes, we want to write a file
          // that maps from region id to ref des.
          for(Map.Entry<String, RangeUtil> entry : idToBadStepRangesMap.entrySet())
          {
            Set<ReconstructionRegion> regions = idToReconstructionRegionsMap.get(entry.getKey());
            for(ReconstructionRegion region : regions)
            {
              // Added by Chnee Khang Wah, 2013-04-12
              if(_testProgram.getProject().isGenerateByNewScanRoute())
              {
                int maxAverageStepSizeInNanos = ImagingChainProgramGenerator.getMaxAverageStepSizeInNanometers();
                int minAverageStepSizeInNanos = ImagingChainProgramGenerator.getMinAverageStepSizeInNanometers();
                int maxStepSizeInNanos = ImagingChainProgramGenerator.calculateMaxStepSizeInNanometers();
                int maxStepSizeForRegion = getMaxStepSizeInNanos(region.getSignalCompensation(), maxStepSizeInNanos, maxAverageStepSizeInNanos);
                int minStepSizeForRegion = getMinStepSizeInNanos(region.getSignalCompensation(), maxStepSizeInNanos, minAverageStepSizeInNanos);
                minStepSizeForRegion = Math.min(minStepSizeForRegion, maxStepSizeForRegion);
                RangeUtil rangeUtil = new RangeUtil(minStepSizeForRegion, maxStepSizeForRegion);

                region.setCandidateStepRanges(rangeUtil);
              }
              else
              {
                region.setCandidateStepRanges(entry.getValue());
              }

              writer.write(region.getRegionId() + "," + region.getName());

              writer.write("," + region.getComponent().getReferenceDesignator() + ",");

              for (Pad pad : region.getPads())
                writer.write(pad.getName() + " ");

              if(region.getCandidateStepRanges().hasRanges() == false)
                writer.writeln(",no");
              else
                writer.writeln(",yes");
            }
          }
          writer.close();
        }
      }
      catch(XrayTesterException dex)
      {
        dex.printStackTrace();
      }
    }
  }

  /**
   * @author George A. David
   */
  private void setupFocusPriorities()
  {
    SurroundingPadUtil padUtil = _testProgram.getSurroundingPadUtil();

    int thicknessInNanoMeters = _project.getPanel().getThicknessInNanometers();
    double maxWidth = ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters() + (2 * thicknessInNanoMeters);
    double maxLength = ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters() + (2 * thicknessInNanoMeters);

    Map<String, Pair<Boolean, Boolean>> idToInfoPairMap = new HashMap<String, Pair<Boolean, Boolean>>();

    for(ReconstructionRegion region : _testProgram.getAllInspectionRegions())
    {
      PanelRectangle regionRect = new PanelRectangle(region.getRegionRectangleRelativeToPanelInNanoMeters());

      // for a homogenous multi-board case, we only need to check the region once, since it will
      // apply to all boards. So create a unique id based on it's board-based coordinates and dimensions
      // then save it for later regions if necessary

      // create a transform to go from panel coordinates and rotation to
      // board coordinates and no rotation
      Board board = region.getComponent().getBoard();
      AffineTransform normalizeBoardDataTransform = new AffineTransform();
      board.preConcatenateShapeTransform(normalizeBoardDataTransform);
      try
      {
        normalizeBoardDataTransform = normalizeBoardDataTransform.createInverse();
      }
      catch (NoninvertibleTransformException ex)
      {
        Assert.logException(ex);
      }

      boolean isSingleSided = true;
      boolean hasPotentialGridArrayArtifact = false;

      // create the normialized shape and create and ID based on this.
      PanelRectangle normalizedRegion = new PanelRectangle(normalizeBoardDataTransform.createTransformedShape(regionRect.getRectangle2D()));
      String uniqueId = board.getBoardType().getName() + "_" +
                        region.getComponent().getReferenceDesignator() + "_" +
                        (int)MathUtil.convertNanometersToMillimeters(normalizedRegion.getMinX()) + "_" +
                        (int)MathUtil.convertNanometersToMillimeters(normalizedRegion.getMinY()) + "_" +
                        (int)MathUtil.convertNanometersToMillimeters(normalizedRegion.getWidth()) + "_" +
                        (int)MathUtil.convertNanometersToMillimeters(normalizedRegion.getHeight());
      if(idToInfoPairMap.containsKey(uniqueId))
      {
        // we've already done this region on another board,
        // just use those resutls as they will be the same.
        Pair<Boolean, Boolean> pair = idToInfoPairMap.get(uniqueId);
        isSingleSided = pair.getFirst();
        hasPotentialGridArrayArtifact = pair.getSecond();
      }
      else
      {
        // because of the angle of the cameras and the x-rays, we need to expand the region by
        // the thickness on each side to determine if it is single sided.
        regionRect.setRect(regionRect.getCenterX() - maxWidth / 2.0,
                           regionRect.getCenterY() - maxLength / 2.0,
                           maxWidth,
                           maxLength);

        Set<Pad> pads = padUtil.getIntersectingPadsInRectangle(regionRect, !region.isTopSide());
        isSingleSided = pads.isEmpty();

        // if it's any of the following, check if a grid array nearby can cause artifacts.
        // it's one that is within 200 mils of the focus region.
        if (region.getInspectionFamilyEnum().equals(InspectionFamilyEnum.CHIP) ||
            region.getInspectionFamilyEnum().equals(InspectionFamilyEnum.POLARIZED_CAP) ||
            region.getInspectionFamilyEnum().equals(InspectionFamilyEnum.GULLWING))
        {
          PanelRectangle focusRect = null;
          for (FocusGroup group : region.getFocusGroups())
          {
            if (focusRect == null)
              focusRect = new PanelRectangle(group.getFocusSearchParameters().getFocusRegion().getRectangleRelativeToPanelInNanometers());
            else
              focusRect.add(group.getFocusSearchParameters().getFocusRegion().getRectangleRelativeToPanelInNanometers());
          }

          Assert.expect(focusRect != null);
          focusRect.setRect(focusRect.getMinX() - _FOCUS_REGION_BUFFER_FOR_FOCUS_PRIORITY,
                            focusRect.getMinY() - _FOCUS_REGION_BUFFER_FOR_FOCUS_PRIORITY,
                            focusRect.getWidth() + (2 * _FOCUS_REGION_BUFFER_FOR_FOCUS_PRIORITY),
                            focusRect.getHeight() + (2 * _FOCUS_REGION_BUFFER_FOR_FOCUS_PRIORITY));

          for (Pad pad : pads)
          {
            if (focusRect.intersectsShape(pad.getShapeRelativeToPanelInNanoMeters()))
            {
              hasPotentialGridArrayArtifact = true;
              break;
            }
          }
        }
        Pair<Boolean, Boolean> pair = new Pair<Boolean, Boolean>(isSingleSided, hasPotentialGridArrayArtifact);
        Object previous = idToInfoPairMap.put(uniqueId, pair);
        Assert.expect(previous == null);
      }

      setupFocusPriority(region, isSingleSided, hasPotentialGridArrayArtifact);
    }
  }


  /**
   * @author George A. David
   */
  public RangeUtil getValidBadStepRanges(int minStepInNanos, int maxStepInNanos, int numRows, int badPhaseShift)
  {
    Assert.expect(minStepInNanos > 0);
    Assert.expect(maxStepInNanos > 0);

    Assert.expect(numRows >= 0);

    int minStepInMils = (int)Math.floor(MathUtil.convertNanoMetersToMils(minStepInNanos));
    int maxStepInMils = (int)Math.ceil(MathUtil.convertNanoMetersToMils(maxStepInNanos));
    if (_CAMERA_ARRAY_WIDTH_IN_MILS == 0)
      _CAMERA_ARRAY_WIDTH_IN_MILS = MathUtil.convertNanoMetersToMils(_xrayCameraArray.getCameraArrayWidthProjectedOnMinimumSlicePlane());

    RangeUtil rangeUtilMils = new RangeUtil(minStepInMils, maxStepInMils);
    for(int step = minStepInMils; step <= maxStepInMils; ++step)
    {
      int numPasses = (int)Math.ceil(_CAMERA_ARRAY_WIDTH_IN_MILS / step);
      int numRowsNeededToBeBad = (int)Math.ceil(( (double)numPasses / 2) * badPhaseShift + 1);
      if(numRows <= numRowsNeededToBeBad)
        rangeUtilMils.subtract(step);
    }

    RangeUtil rangeUtilNanos = new RangeUtil();
    for(Pair<Integer, Integer> pair : rangeUtilMils.getRanges())
    {
      rangeUtilNanos.add((int)Math.floor(MathUtil.convertMilsToNanoMeters(pair.getFirst())),
                         (int)Math.ceil(MathUtil.convertMilsToNanoMeters(pair.getSecond())));
    }
    return rangeUtilNanos;
  }

  /**
   * @author George A. David
   */
  public double calculateBadPhaseShift(int pitchInNanoMeters,
                                       double zAffectingRegion,
                                       double zAffectedRegion,
                                       int stepSizeInNanoMeters)
  {
    // the equation is as follows
    //  stepSize = -1 * z/zDrip * pitch * pi * badPhaseShift * (1/(1/zSkid - 1/zDrip)) / (2 * pi * z)
    // this reduces to:
    // stepSize = - pitch * badPhaseShift/ (2((zDrip/zSkid) - 1))
    // then solve for badPhaseShift.
    double badPhaseShift = stepSizeInNanoMeters * ((zAffectingRegion/zAffectedRegion - 1) * 2)/ -pitchInNanoMeters;

    return badPhaseShift;
  }


  /**
   * @author George A. David
   */
  private double calculateBadStepSizeInNanoMeters(int pitchInNanoMeters,
                                      double zAffectingRegion,
                                      double zAffectedRegion,
                                      int badPhaseShift)
  {
    // the equation is as follows
    //  -1 * z/zDrip * pitch * pi * badPhaseShift * (1/(1/zSkid - 1/zDrip)) / (2 * pi * z)
    // this reduces to:
    // - pitch * badPhaseShift/ (2((zDrip/zSkid) - 1))

    // assert to avoid any divide by zero errors.
    Assert.expect(zAffectedRegion != 0);
    Assert.expect(zAffectingRegion != zAffectedRegion);

    double stepSizeInNanos = -pitchInNanoMeters * badPhaseShift / (2 * (zAffectingRegion/zAffectedRegion - 1));

    Assert.expect(stepSizeInNanos > 0, "stepSize <= 0: " +
                  "    pitchInMils: "             + pitchInNanoMeters/25400  +
                  "    zAffectingRegion: " + zAffectingRegion +
                  "    zAffectedRegion: "  + zAffectedRegion +
                  "    badPhaseShift: "     + badPhaseShift);
    return stepSizeInNanos;
  }

  /**
   * @author George A. David
   * @author Wei Chin
   */
  private void fitToHardwareLimits()
  {
    Assert.expect(_testProgram != null);

    for (TestSubProgram subProgram : _testProgram.getAllTestSubPrograms())
    {
      List<ReconstructionRegion> inspectionRegions = new LinkedList<ReconstructionRegion>();
      for (ReconstructionRegion inspectionRegion : subProgram.getAllInspectionRegions())
      {
        boolean assigned = assignProcessorStrip(subProgram.getProcessorStripsForInspectionRegions(), inspectionRegion);
        if (assigned)
          inspectionRegions.add(inspectionRegion);
        else
        {
          if (InspectionFamily.isSpecialTwoPinDevice(inspectionRegion.getInspectionFamilyEnum()))
          {
            // chips are not to be split up. if they won't fit because of hardware limits,
            // then they are untestable.
//            if (UnitTest.unitTesting() == false)
//            {
//              PanelRectangle regionRect = inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
//              System.out.println("component " + inspectionRegion.getComponent().getReferenceDesignator() + " with land pattern " +
//                                 inspectionRegion.getComponent().getLandPattern().getName() +
//                                 " is a CHIP that does not fit within the processor strip bounds (width: " +
//                                 MathUtil.convertNanoMetersToMils(regionRect.getWidth()) + " length: " +
//                                 MathUtil.convertNanoMetersToMils(regionRect.getHeight()) + ")");
//            }

            //if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_LARGE_IMAGE_VIEW) == true)
            if(_project.isEnableLargeImageView()) // Bee Hoon, Recipe Settings
            {
              assigned = fitAndAssignProcessorStrip(subProgram.getProcessorStripsForInspectionRegions(), inspectionRegion);
            }
            if (assigned)
              inspectionRegions.add(inspectionRegion);
            else
            {
              for (Pad pad : inspectionRegion.getPads())
              {
                pad.getPadSettings().setTestable(false);
                pad.getPadSettings().setUntestableReason(StringLocalizer.keyToString(new LocalizedString("PAD_UNTESTABLE_CHIP_NOT_FIT_KEY",
                                                      new Object[]{MathUtil.convertNanoMetersToMils(inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters().getWidth()),
                                                                   MathUtil.convertNanoMetersToMils(inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters().getHeight())})));
                
                        //" is a CHIP that does not fit within the processor strip bounds (width: " +
                        //           MathUtil.convertNanoMetersToMils(inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters().getWidth()) + "Mils length: " +
                        //           MathUtil.convertNanoMetersToMils(inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters().getHeight()) + " Mils)");
              }
            }
          }
          else
            inspectionRegions.addAll(fitToHardwareLimits(subProgram.getProcessorStripsForInspectionRegions(),
                inspectionRegion));
        }
      }
      subProgram.setInspectionRegions(inspectionRegions);
    }
  }

  /**
   * @author George A. David
   * @author Rex Shang
   */
  private List<ReconstructionRegion> fitToHardwareLimits(List<ProcessorStrip> processorStrips, ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(processorStrips != null);

    List<ReconstructionRegion> regions = new LinkedList<ReconstructionRegion>();
    Map<ProcessorStrip, List<Pad>> processorStripToPadsMap = new HashMap<ProcessorStrip, List<Pad>>();
    for (Pad pad : reconstructionRegion.getPads())
    {
      PanelRectangle padBounds = ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad);
      boolean fits = false;
      for (ProcessorStrip rect : processorStrips)
      {
        if (rect.getRegion().contains(padBounds))
        {
          fits = true;
          if (processorStripToPadsMap.containsKey(rect))
          {
            processorStripToPadsMap.get(rect).add(pad);
          }
          else
          {
            List<Pad> pads = new LinkedList<Pad>();
            pads.add(pad);
            processorStripToPadsMap.put(rect, pads);
          }

          break;
        }
      }
      // For pads that does not fit processor strip bounds, set testable to false
      if (fits == false)
      {
//        pad.getPadTypeSettings().setTestable(false);
//        pad.getPadTypeSettings().setUntestableReason(StringLocalizer.keyToString(new LocalizedString("PAD_UNTESTABLE_DOES_NOT_FIT_PROCESSOR_STRIP_BOUNDS_KEY",
//                                                     new Object[]{MathUtil.convertNanoMetersToMils(padBounds.getWidth()), MathUtil.convertNanoMetersToMils(padBounds.getHeight())})));
          pad.getPadSettings().setTestable(false);
          pad.getPadSettings().setUntestableReason(StringLocalizer.keyToString(new LocalizedString("PAD_UNTESTABLE_DOES_NOT_FIT_PROCESSOR_STRIP_BOUNDS_KEY",
                                                       new Object[]{MathUtil.convertNanoMetersToMils(padBounds.getWidth()), MathUtil.convertNanoMetersToMils(padBounds.getHeight())})));
//        if (UnitTest.unitTesting() == false)
//          System.out.println("pad " + pad.getName() + " of component " + pad.getComponent().getReferenceDesignator() +
//                             " of land pattern " + pad.getComponent().getLandPattern().getName() +
//                             " does not fit within the processor strip bounds (width : " + MathUtil.convertNanoMetersToMils(padBounds.getWidth()) +
//                             " length: " + MathUtil.convertNanoMetersToMils(padBounds.getHeight()) + ")");
      }
    }

    for (ProcessorStrip processorStrip : processorStripToPadsMap.keySet())
    {
      List<Pad> pads = processorStripToPadsMap.get(processorStrip);
      ReconstructionRegion region = InspectionRegionGeneration.createInspectionRegion(pads);
      region.setReconstructionEngineId(processorStrip.getReconstructionEngineId());
      regions.add(region);
    }

    return regions;
  }

  /**
   * @author Rex Shang
   *
   */
  private boolean assignProcessorStrip(List<ProcessorStrip> processorStrips, ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(processorStrips != null);

    for (ProcessorStrip strip : processorStrips)
    {
      if (strip.getRegion().contains(reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters()))
      {
        // Assign the ire id to region.
        reconstructionRegion.setReconstructionEngineId(strip.getReconstructionEngineId());
        return true;
      }
    }
    return false;
  }

  /**
   * @author Wei Chin
   */
  private boolean fitAndAssignProcessorStrip(List<ProcessorStrip> processorStrips, ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(processorStrips != null);

    for (ProcessorStrip strip : processorStrips)
    {
      PanelRectangle regionRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
      if (strip.getRegion().intersects(regionRect))
      {
        // fit to the hardware limit
        int maximumXForStrip = strip.getRegion().getMaxX();
        int maximumXForReconstructionRegion = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMaxX();

        int differentInX = maximumXForReconstructionRegion - maximumXForStrip;

        regionRect.setRect(
          regionRect.getMinX() - differentInX,
          regionRect.getMinY(),
          regionRect.getWidth() - 2 * differentInX,
          regionRect.getHeight());

        for(Pad pad : reconstructionRegion.getPads())
        {
          PanelRectangle panelRectangle = ReconstructionRegion.getMinimumInspectableRegionforPad(pad);
          if(regionRect.contains(panelRectangle) == false)
            return false;
        }
        // Assign the ire id to region.
        reconstructionRegion.setReconstructionEngineId(strip.getReconstructionEngineId());
        return true;
      }
    }
    return false;
  }

  /**
   * @author George A. David
   */
  private boolean arePadsOrthogonal(List<Pad> pads)
  {
    Assert.expect(pads != null);

    boolean arePadsOrthogonal = false;

    // if there are no pads, lets just call them orthogonal
    if (pads.isEmpty())
      return true;

    // all pad must be orthognal to each other, for exmaple 0 90 180 270,
    // or 12 102 192 282, etc...
    Double normalizedRotation = null;
    for (Pad pad : pads)
    {
      double degreesRotation = pad.getDegreesRotationAfterAllRotations();
      double currentNormalizedRotation = degreesRotation = degreesRotation - (Math.floor(degreesRotation / 90) * 90);
      if (normalizedRotation == null)
      {
        normalizedRotation = new Double(currentNormalizedRotation);
        arePadsOrthogonal = true;
      }
      else
      {
        if (MathUtil.fuzzyEquals(normalizedRotation, currentNormalizedRotation) == false)
        {
          arePadsOrthogonal = false;
          break;
        }
        else
          arePadsOrthogonal = true;
      }
    }

    return arePadsOrthogonal;
  }


  /**
   * @author George A. David
   * @author Yee Seong
   */
  public AlignmentValidityEnum checkAlignmentRegionValidity(TestSubProgram testSubProgram, List<Pad> pads, List<Fiducial> fiducials)
  {
    AlignmentValidityEnum validityEnum = null;
    
    boolean canFitInAlignmentRegion = false;
    if(testSubProgram.isLowMagnification())
      canFitInAlignmentRegion = ReconstructionRegion.fitsInAlignmentRegion(pads, fiducials);
    else
      canFitInAlignmentRegion = ReconstructionRegion.fitsInAlignmentRegionForHighMag(pads, fiducials);
    
    if (canFitInAlignmentRegion)
    {
      // all pad must be orthognal to each other, for exmaple 0 90 180 270,
      // or 12 102 192 282, etc...
      if(arePadsOrthogonal(pads) == false)
        validityEnum = AlignmentValidityEnum.INVALID_FEATURES_NOT_ORTHOGONAL;
      else
      {
        ReconstructionRegion region = createAlignmentRegion(pads, fiducials);
        PanelRectangle imageableRegionInNanoMeters = testSubProgram.getAlignmentImageableRegionInNanoMeters();
        if(imageableRegionInNanoMeters.contains(region.getRegionRectangleRelativeToPanelInNanoMeters()))
        {
          validityEnum = AlignmentValidityEnum.IS_VALID;
        }
        else
        {
          validityEnum = AlignmentValidityEnum.INVALID_DOES_NOT_FIT;
        }
      }
    }
    else
        validityEnum = AlignmentValidityEnum.INVALID_DOES_NOT_FIT;

    Assert.expect(validityEnum != null);
    return validityEnum;
  }

  /**
   * @author George A. David
   */
  public boolean isRightAlignmentRegionValid(Project project, List<Pad> pads, List<Fiducial> fiducials)
  {
    Assert.expect(project != null);
    _project = project;

    AlignmentValidityEnum validityEnum = checkAlignmentRegionValidity(project.getTestProgramForAlignmentRegionsGeneration().getTestSubProgram(PanelLocationInSystemEnum.RIGHT), pads, fiducials);
    return validityEnum.equals(AlignmentValidityEnum.IS_VALID);
  }

  /**
   * @author George A. David
   */
  public boolean isLeftAlignmentRegionValid(Project project, List<Pad> pads, List<Fiducial> fiducials)
  {
    Assert.expect(project != null);
    _project = project;

    AlignmentValidityEnum validityEnum = checkAlignmentRegionValidity(project.getTestProgramForAlignmentRegionsGeneration().getTestSubProgram(PanelLocationInSystemEnum.LEFT), pads, fiducials);
    return validityEnum.equals(AlignmentValidityEnum.IS_VALID);
  }

  /**
   * @author George A. David
   */
  public AlignmentValidityEnum checkRightAlignmentRegionValidity(Project project, List<Pad> pads, List<Fiducial> fiducials)
  {
    Assert.expect(project != null);
    _project = project;

    return checkAlignmentRegionValidity(project.getTestProgramForAlignmentRegionsGeneration().getTestSubProgram(PanelLocationInSystemEnum.RIGHT), pads, fiducials);
  }

  /**
   * @author George A. David
   */
  public AlignmentValidityEnum checkLeftAlignmentRegionValidity(Project project, List<Pad> pads, List<Fiducial> fiducials)
  {
    Assert.expect(project != null);
    _project = project;

    return checkAlignmentRegionValidity(project.getTestProgramForAlignmentRegionsGeneration().getTestSubProgram(PanelLocationInSystemEnum.LEFT), pads, fiducials);
  }

  /**
   * generate the region where the user
   * can choose to add more alignment points.
   * @author George A. David
   */
  public void generateAllowableAlignmentRegion(ReconstructionRegion alignmentRegion, TestSubProgram subProgram)
  {
    Assert.expect(alignmentRegion != null);

    PanelRectangle regionBoundsInNanoMeters = alignmentRegion.getAlignmentFeatureBoundsInNanoMeters();

    int x = regionBoundsInNanoMeters.getMinX();
    int y = regionBoundsInNanoMeters.getMinY();
    int width = regionBoundsInNanoMeters.getWidth();
    int height = regionBoundsInNanoMeters.getHeight();

    int dx = ReconstructionRegion.getMaxAlignmentFeatureWidthInNanoMeters() - width;
    int dy = ReconstructionRegion.getMaxAlignmentFeatureLengthInNanoMeters() - height;
    
    if(subProgram.isHighMagnification())
    {
      dx = ReconstructionRegion.getMaxHighMagAlignmentFeatureWidthInNanoMeters() - width;
      dy = ReconstructionRegion.getMaxHighMagAlignmentFeatureLengthInNanoMeters() - height;
    }

    int newX = x - dx;
    int newY = y - dy;
    int newWidth = width + (2 * dx);
    int newHeight = height + (2 * dy);

    List<ProcessorStrip> processorStrips = subProgram.getProcessorStripsForInspectionRegions();

    Area allowableArea = new Area(new PanelRectangle(newX, newY, newWidth, newHeight).getRectangle2D());
    // now make sure they don't extend the panel boundaries
    Area panelArea = new Area(subProgram.getTestProgram().getProject().getPanel().getShapeInNanoMeters());
    allowableArea.intersect(panelArea);

    // Define the area that contains the alignment region.
    Area processorStripArea = new Area();
    for (ProcessorStrip processorStrip : processorStrips)
    {
      PanelRectangle processorStripRectangle = processorStrip.getRegion();
      if (processorStripRectangle.contains(alignmentRegion.getAlignmentFeatureBoundsInNanoMeters()))
        processorStripArea.add(new Area(processorStripRectangle.getRectangle2D()));
      }
    Assert.expect(processorStripArea != null, "processorStripArea is null.");

    allowableArea.intersect(new Area(subProgram.getAlignmentImageableRegionInNanoMeters().getRectangle2D()));
    Area tempArea = new Area(allowableArea);
    tempArea.intersect(processorStripArea);
    Rectangle2D rect = tempArea.getBounds2D();

    // If the intersected area has any size at all (in both dimensions) then we will use it.
    if (rect.getWidth() > 0 && rect.getHeight() > 0)
      allowableArea = tempArea;

    alignmentRegion.setAllowableAlignmentRegionInNanoMetes(new PanelRectangle(allowableArea));
  }

  /**
   * @author George A. David
   */
  private ReconstructionRegion createAlignmentRegion(List<Pad> pads, List<Fiducial> fiducials)
  {
    Assert.expect(pads != null);
    Assert.expect(fiducials != null);

    ReconstructionRegion region = new ReconstructionRegion();
    region.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.ALIGNMENT);
    region.setFocusPriority(FocusPriorityEnum.FIRST);
    BooleanRef isTopSide = null;
    
    Pad mostHorizontallyExtremePad = null;
    int greatestAbsoluteXDistanceFromComponentCenter = 0;
    int horizontalDirection = 0;
    Pad mostVerticallyExtremePad = null;
    int greatestAbsoluteYDistanceFromComponentCenter = 0;
    int verticalDirection = 0;
    PanelCoordinate padCenter = null;

    // XCR1404 - by Cheah Lee Herng 23 July 2013 - There maybe case where there is no alignment pads for 
    // the alignment group and users directly select fiducial as the alignment point. We need to check if the pads are empty.
    boolean needEnlarge = true;
    if (pads.isEmpty() == false)
    {
      Component componentOfFirstPad = pads.iterator().next().getComponent();
      PanelCoordinate componentCenter = componentOfFirstPad.getCenterCoordinateRelativeToPanelInNanoMeters();
      for (Pad pad : pads)
      {
        if (isTopSide == null)
          isTopSide = new BooleanRef(pad.getComponent().isTopSide());
        else
          Assert.expect(pad.getComponent().isTopSide() == isTopSide.getValue());
        region.addAlignmentFeature(pad);

        //sham, 2012-05-14, Alignment Enhancement
        //enlarge region rectangle if match joint type as bellow
        if (componentOfFirstPad.equals(pad.getComponent()) && needEnlarge)
        {
          JointTypeEnum jointTypesOnComponent = pad.getJointTypeEnum();
          if (jointTypesOnComponent.equals(JointTypeEnum.CGA)
            || jointTypesOnComponent.equals(JointTypeEnum.CHIP_SCALE_PACKAGE)
            || jointTypesOnComponent.equals(JointTypeEnum.COLLAPSABLE_BGA)
            || jointTypesOnComponent.equals(JointTypeEnum.NON_COLLAPSABLE_BGA)
            || jointTypesOnComponent.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR)
            || jointTypesOnComponent.equals(JointTypeEnum.PRESSFIT)
            || jointTypesOnComponent.equals(JointTypeEnum.THROUGH_HOLE)
            || jointTypesOnComponent.equals(JointTypeEnum.OVAL_THROUGH_HOLE)) //Siew Yeng - XCR-3318 - Oval PTH
          {
            pad.getComponent().getCenterCoordinateRelativeToPanelInNanoMeters();
            padCenter = pad.getCenterCoordinateRelativeToPanelInNanoMeters();
            int yDistanceFromComponentCenter = padCenter.getY() - componentCenter.getY();
            int absoluteYDistanceFromComponentCenter = Math.abs(yDistanceFromComponentCenter);
            if (mostVerticallyExtremePad == null)
            {
              greatestAbsoluteYDistanceFromComponentCenter = absoluteYDistanceFromComponentCenter;
              mostVerticallyExtremePad = pad;
              // Y direction is inverted between panel and image coords.  Ergo, we need to take the negative here.
              verticalDirection = (int) Math.signum(yDistanceFromComponentCenter);
            }
            else if (absoluteYDistanceFromComponentCenter > greatestAbsoluteYDistanceFromComponentCenter)
            {
              greatestAbsoluteYDistanceFromComponentCenter = absoluteYDistanceFromComponentCenter;
              mostVerticallyExtremePad = pad;
              // Y direction is inverted between panel and image coords.  Ergo, we need to take the negative here.
              verticalDirection = (int) Math.signum(yDistanceFromComponentCenter);
            }
            int xDistanceFromComponentCenter = padCenter.getX() - componentCenter.getX();
            int absoluteXDistanceFromComponentCenter = Math.abs(xDistanceFromComponentCenter);
            if (mostHorizontallyExtremePad == null)
            {
              greatestAbsoluteXDistanceFromComponentCenter = absoluteXDistanceFromComponentCenter;
              mostHorizontallyExtremePad = pad;
              horizontalDirection = (int) Math.signum(xDistanceFromComponentCenter);
            }
            else if (absoluteXDistanceFromComponentCenter > greatestAbsoluteXDistanceFromComponentCenter)
            {
              greatestAbsoluteXDistanceFromComponentCenter = absoluteXDistanceFromComponentCenter;
              mostHorizontallyExtremePad = pad;
              horizontalDirection = (int) Math.signum(xDistanceFromComponentCenter);
            }
          }
          else
          {
            needEnlarge = false;
          }
        }
        else
        {
          needEnlarge = false;
        }
      }
    }

    for (Fiducial fiducial : fiducials)
    {
      if (isTopSide == null)
        isTopSide = new BooleanRef(fiducial.isTopSide());
      else
        Assert.expect(fiducial.isTopSide() == isTopSide.getValue());

      region.addAlignmentFeature(fiducial);
    }

    if (needEnlarge)
    {
      // We probably need ghost pads, so need enlarge alignment region by atleat 1 pitch.
      if (mostHorizontallyExtremePad != null)
      {
        int pitchInNanometers = mostHorizontallyExtremePad.getInterPadDistanceInNanoMeters();
        PanelRectangle mostHorizontallyExtremeJointRec = mostHorizontallyExtremePad.getPanelRectangleWithLowerLeftOriginInNanoMeters();
        PanelRectangle horizontalGhostRec = new PanelRectangle(mostHorizontallyExtremeJointRec.getMinX() + (horizontalDirection * pitchInNanometers),
        mostHorizontallyExtremeJointRec.getMinY(), mostHorizontallyExtremeJointRec.getWidth(),
        mostHorizontallyExtremeJointRec.getHeight());
        region.enlargeRegionRectangle(horizontalGhostRec);
      }
      if (mostVerticallyExtremePad != null)
      {
        int pitchInNanometers = mostVerticallyExtremePad.getInterPadDistanceInNanoMeters();
        PanelRectangle mostVerticallyExtremeJointRec = mostVerticallyExtremePad.getPanelRectangleWithLowerLeftOriginInNanoMeters();
        PanelRectangle verticalGhostRec = new PanelRectangle(mostVerticallyExtremeJointRec.getMinX(),
        mostVerticallyExtremeJointRec.getMinY() + (verticalDirection * pitchInNanometers),
        mostVerticallyExtremeJointRec.getWidth(), mostVerticallyExtremeJointRec.getHeight());
        region.enlargeRegionRectangle(verticalGhostRec);
      }      
    }
    Assert.expect(isTopSide != null);
    region.setTopSide(isTopSide.getValue());

    // If our new region isAffectedByInterferencePattern, we must set the candidate
    // step range on the region so IAE can adjust the scanPath.
    if(region.isAffectedByInterferencePattern() == false)
    {
      int maxStepSizeInNanos = ImagingChainProgramGenerator.getMaxStepSizeLimitInNanometers();
      int minAverageStepSizeInNanos = ImagingChainProgramGenerator.getMaxAverageStepSizeInNanometers();      
      int maxStepSizeForRegion = getMaxStepSizeInNanos(region.getSignalCompensation(), maxStepSizeInNanos, minAverageStepSizeInNanos );
      if (minAverageStepSizeInNanos > maxStepSizeForRegion)
      {
        minAverageStepSizeInNanos = maxStepSizeForRegion;
      }
      RangeUtil rangeUtil = new RangeUtil(minAverageStepSizeInNanos, maxStepSizeForRegion);
      region.setCandidateStepRanges(rangeUtil);
    }

    return region;
  }

  /**
   * @author Roy Williams
   */
  public static boolean alignmentRegionIsOnLeftBoardSideOutsideOfInspectionArea(TestSubProgram testSubProgram, ReconstructionRegion region)
  {
    // PanelLocationInSystemEnum.RIGHT does not get to position alignment points
    // anywhere else except on processor strips.   PanelLocationInSystemEnum.LEFT
    // has a bit more luxury.
    if (testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
    {
      // If this region is not contained in the inspection region bounds AND
      // the region is in the imageable space then it is an outlier alignment
      // point.   Approve it!
      PanelRectangle regionRectangleRelativeToPanel = region.getRegionRectangleRelativeToPanelInNanoMeters();
      if (testSubProgram.getInspectionRegionsBoundsInNanoMeters().contains(regionRectangleRelativeToPanel) == false &&
          testSubProgram.getAlignmentImageableRegionInNanoMeters().contains(regionRectangleRelativeToPanel))
      {
        return true;
      }
    }
    return false;
  }

  /**
   * @author George A. David
   * @author Peter Esbensen
   * @author Ronald Lim
   */
  private void createAlignmentRegions(TestSubProgram subProgram, List<AlignmentGroup> alignmentGroups)
  {
    Assert.expect(alignmentGroups != null);

    int maximumAlignmentRegionWidthInNanoMeters = ReconstructionRegion.getMaxAlignmentRegionWidthInNanoMeters();
    int maximumAlignmentRegionLengthInNanoMeters = ReconstructionRegion.getMaxAlignmentRegionLengthInNanoMeters();
    int maximumAlignmentFeatureWidthInNanoMeters = ReconstructionRegion.getMaxAlignmentFeatureWidthInNanoMeters();
    int maximumAlignmentFeatureLengthInNanoMeters = ReconstructionRegion.getMaxAlignmentFeatureLengthInNanoMeters();
   
    if(subProgram.isHighMagnification())
    {
      maximumAlignmentRegionWidthInNanoMeters = ReconstructionRegion.getMaxHighMagAlignmentRegionWidthInNanoMeters();
      maximumAlignmentRegionLengthInNanoMeters = ReconstructionRegion.getMaxHighMagAlignmentRegionLengthInNanoMeters();
      maximumAlignmentFeatureWidthInNanoMeters = ReconstructionRegion.getMaxHighMagAlignmentFeatureWidthInNanoMeters();
      maximumAlignmentFeatureLengthInNanoMeters = ReconstructionRegion.getMaxHighMagAlignmentFeatureLengthInNanoMeters();
    }    
    
    List<ReconstructionRegion> regions = new LinkedList<ReconstructionRegion>();
    List<ReconstructionRegion> regions2D = new LinkedList<ReconstructionRegion>();
    boolean hasCompleteAlignmentGroup = true;

    for (AlignmentGroup alignmentGroup : alignmentGroups)
    {
      if (alignmentGroup.isEmpty())
      {
        if(subProgram.getTestProgram().isAlignmentGroupInvalidated(alignmentGroup))
         subProgram.getTestProgram().removeInvalidatedAlignmentGroup(alignmentGroup);

        //Khaw Chek Hau - XCR2285: 2D Alignment on v810
        hasCompleteAlignmentGroup = false;
        
        continue;
      }
      int previousAlignmentRegionId = -1;
      List<Pad> pads = alignmentGroup.getPads();
      List<Fiducial> fiducials = alignmentGroup.getFiducials();

      if (subProgram.hasAlignmentGroup(alignmentGroup))
      {
        //Khaw Chek Hau - XCR2285: 2D Alignment on v810
        ReconstructionRegion alignmentRegion = subProgram.get3DAlignmentRegion(alignmentGroup);
        ReconstructionRegion alignment2DRegion = subProgram.get2DAlignmentRegion(alignmentGroup);
        previousAlignmentRegionId = alignmentRegion.getRegionId();

        Set<Pad> oldPads = new HashSet<Pad>(alignmentRegion.getPads());
        // check to make sure we need to re-create this region. if not, don't!!!
        if (oldPads.size() == pads.size())
        {
          Set<Fiducial> oldFids = new HashSet<Fiducial>(alignmentRegion.getFiducials());
          if (oldFids.size() == fiducials.size())
          {
            oldPads.removeAll(pads);
            oldFids.removeAll(fiducials);

            if (oldPads.isEmpty() && oldFids.isEmpty())
            {
              // ok, now we know that we have the same pads and fiducials,
              // but the user may have rotated some pads/components, so let's check
              // that they are still valid
              if (arePadsOrthogonal(pads))
              {
                boolean regionIsContainedByProcessorStrip = false;
                for (ProcessorStrip processorStrip : subProgram.getProcessorStripsForInspectionRegions())
                {
                  if (processorStrip.getReconstructionEngineId().equals(alignmentRegion.getReconstructionEngineId())
                      && processorStrip.getRegion().contains(alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters()))
                  {
                    regionIsContainedByProcessorStrip = true;
                    break;
                  }
                }

                if (regionIsContainedByProcessorStrip)
                {
                  regions.add(alignmentRegion);
                  regions2D.add(alignment2DRegion);
                  
                  // XCR-2769 Customize Alignment OOF due to doesn't clear previous learned alignment settings - Cheah Lee Herng
                  if(alignmentGroup.useZHeight() == true)
                  {
                    updateFocusGroupForVerificationOrAlignmentRegionByRFP(alignmentRegion, alignmentGroup.getZHeightInNanometer());     
                    updateFocusGroupForVerificationOrAlignmentRegionByRFP(alignment2DRegion, alignmentGroup.getZHeightInNanometer());     
                  }
                  
                  continue;
                }
                else if (alignmentRegionIsOnLeftBoardSideOutsideOfInspectionArea(subProgram, alignmentRegion))
                {
                  regions.add(alignmentRegion);
                  regions2D.add(alignment2DRegion);
                  
                  // XCR-2769 Customize Alignment OOF due to doesn't clear previous learned alignment settings - Cheah Lee Herng
                  if(alignmentGroup.useZHeight() == true)
                  {
                    updateFocusGroupForVerificationOrAlignmentRegionByRFP(alignmentRegion, alignmentGroup.getZHeightInNanometer());
                    updateFocusGroupForVerificationOrAlignmentRegionByRFP(alignment2DRegion, alignmentGroup.getZHeightInNanometer());
                  }
                  
                  continue;
                }
              }
            }
          }
        }
      }

      ReconstructionRegion region = createAlignmentRegion(pads, fiducials);
      region.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
      if(previousAlignmentRegionId >= 0)
        region.setRegionId(previousAlignmentRegionId);
      region.setAlignmentGroup(alignmentGroup);
      
      PanelRectangle regionRect = region.getRegionRectangleRelativeToPanelInNanoMeters();
      PanelRectangle featureBounds = region.getAlignmentFeatureBoundsInNanoMeters();

      PanelRectangle imageableRegionInNanoMeters = subProgram.getAlignmentImageableRegionInNanoMeters();

      if (regionRect.getWidth() <= maximumAlignmentRegionWidthInNanoMeters && 
          regionRect.getHeight() <= maximumAlignmentRegionLengthInNanoMeters &&
          featureBounds.getWidth() <= maximumAlignmentFeatureWidthInNanoMeters &&
          featureBounds.getHeight() <= maximumAlignmentFeatureLengthInNanoMeters &&
          imageableRegionInNanoMeters.contains(regionRect) && arePadsOrthogonal(pads))
      {
        _didAlignmentRegionsChange = true;
        regions.add(region);
        // ok, add the alignment uncertainty to each side, then truncate it
        // to make it fit it fit within the processor strip bounds.
        // then truncate it again if it's bigger than the max size.
        
        //Khaw Chek Hau - XCR2943: Alignment Fail Due to Too Close To Edge
        int additionalAddedAlignmentBorderInNanoMeters = 0;
        
        if (subProgram.getTestProgram().getProject().isEnlargeAlignmentRegion())
        {
          additionalAddedAlignmentBorderInNanoMeters = _ADDITIONAL_ADDED_ALIGNMENT_BORDER_IN_NANOMETERS;  
        }
        
        double xCoord = regionRect.getMinX() - (Alignment.getAlignmentUncertaintyBorderInNanoMeters() + additionalAddedAlignmentBorderInNanoMeters);
        double yCoord = regionRect.getMinY() - (Alignment.getAlignmentUncertaintyBorderInNanoMeters() + additionalAddedAlignmentBorderInNanoMeters);
        regionRect.setRect(xCoord,
                           yCoord,
                           regionRect.getWidth() + 2 * (Alignment.getAlignmentUncertaintyBorderInNanoMeters() + additionalAddedAlignmentBorderInNanoMeters),
                           regionRect.getHeight() +
                           2 * (Alignment.getAlignmentUncertaintyBorderInNanoMeters() + additionalAddedAlignmentBorderInNanoMeters));
        Area regionArea = new Area(regionRect.getRectangle2D());
        xCoord = regionRect.getCenterX() - maximumAlignmentRegionWidthInNanoMeters / 2.0;
        yCoord = regionRect.getCenterY() - maximumAlignmentRegionLengthInNanoMeters / 2.0;
        Area maxArea = new Area(new Rectangle2D.Double(xCoord,
                                                       yCoord,
                                                       maximumAlignmentRegionWidthInNanoMeters,
                                                       maximumAlignmentRegionLengthInNanoMeters));
        regionArea.intersect(maxArea);

        region.setRegionRectangleRelativeToPanelInNanoMeters(new PanelRectangle(regionArea));
        createFocusGroupForAlignmentRegion(region, subProgram.getMagnificationType());
        if(subProgram.getTestProgram().isAlignmentGroupInvalidated(alignmentGroup))
          subProgram.getTestProgram().removeInvalidatedAlignmentGroup(alignmentGroup);

        // now create the allowable region that can be used to align on.
        generateAllowableAlignmentRegion(region, subProgram);
        
        //Wei Chin: update the z-height Option
        if(alignmentGroup.useZHeight() == true)
          updateFocusGroupForVerificationOrAlignmentRegionByRFP(region, alignmentGroup.getZHeightInNanometer());
        //XCR-3393, Assert after select "Auto Select Alignment Group"
        if (alignmentGroup.useCustomizeAlignmentSetting() && alignmentGroup.isEmpty() == false)
        {
          List<StageSpeedEnum> virtualLiveSpeedList = new ArrayList<StageSpeedEnum>();
          virtualLiveSpeedList.add(alignmentGroup.getStageSpeedEnum());
          region.initializeVirtualLiveSetting(virtualLiveSpeedList, 
                                              alignmentGroup.getUserGainEnum(),
                                              alignmentGroup.getSignalCompensationEnum(), 
                                              alignmentGroup.getDynamicRangeOptimizationLevel());
        }
        else
          region.clearVirtualLiveSetting();
      }
      else
      {
        if(subProgram.getTestProgram().isAlignmentGroupInvalidated(alignmentGroup) == false)
        {
          _didAlignmentRegionsChange = true;
          subProgram.getTestProgram().addInvalidatedAlignmentGroup(alignmentGroup);
        }
      }
        
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      ReconstructionRegion region2D = createAlignmentRegion(pads, fiducials);
      region2D.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
      if(previousAlignmentRegionId >= 0)
        region2D.setRegionId(previousAlignmentRegionId);
      region2D.setAlignmentGroup(alignmentGroup);
      
      PanelRectangle region2DRect = region2D.getRegionRectangleRelativeToPanelInNanoMeters();
      PanelRectangle feature2DBounds = region2D.getAlignmentFeatureBoundsInNanoMeters();
      
      PanelRectangle imageableRegion2DInNanoMeters = subProgram.getAlignmentImageableRegionInNanoMeters();

      if (region2DRect.getWidth() <= maximumAlignmentRegionWidthInNanoMeters && 
          region2DRect.getHeight() <= maximumAlignmentRegionLengthInNanoMeters &&
          feature2DBounds.getWidth() <= maximumAlignmentFeatureWidthInNanoMeters &&
          feature2DBounds.getHeight() <= maximumAlignmentFeatureLengthInNanoMeters &&
          imageableRegion2DInNanoMeters.contains(region2DRect) && arePadsOrthogonal(pads))
      {
        _didAlignmentRegionsChange = true;
        regions2D.add(region2D);
        // ok, add the alignment uncertainty to each side, then truncate it
        // to make it fit it fit within the processor strip bounds.
        // then truncate it again if it's bigger than the max size.
        double xCoord2D = region2DRect.getMinX() - Alignment.getAlignmentUncertaintyBorderInNanoMeters();
        double yCoord2D = region2DRect.getMinY() - Alignment.getAlignmentUncertaintyBorderInNanoMeters();
        region2DRect.setRect(xCoord2D,
                           yCoord2D,
                           region2DRect.getWidth() + 2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
                           region2DRect.getHeight() +
                           2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters());
        Area region2DArea = new Area(region2DRect.getRectangle2D());
        xCoord2D = region2DRect.getCenterX() - maximumAlignmentRegionWidthInNanoMeters / 2.0;
        yCoord2D = region2DRect.getCenterY() - maximumAlignmentRegionLengthInNanoMeters / 2.0;
        Area maxArea2D = new Area(new Rectangle2D.Double(xCoord2D,
                                                         yCoord2D,
                                                         maximumAlignmentRegionWidthInNanoMeters,
                                                         maximumAlignmentRegionLengthInNanoMeters));
        region2DArea.intersect(maxArea2D);

        region2D.setRegionRectangleRelativeToPanelInNanoMeters(new PanelRectangle(region2DArea));
        createFocusGroupForAlignmentRegion(region2D, subProgram.getMagnificationType());
        if(subProgram.getTestProgram().isAlignmentGroupInvalidated(alignmentGroup))
          subProgram.getTestProgram().removeInvalidatedAlignmentGroup(alignmentGroup);

        // now create the allowable region that can be used to align on.
        generateAllowableAlignmentRegion(region2D, subProgram);
        
        //Wei Chin: update the z-height Option
        if(alignmentGroup.useZHeight() == true)
          updateFocusGroupForVerificationOrAlignmentRegionByRFP(region2D, alignmentGroup.getZHeightInNanometer());
        
        if (alignmentGroup.useCustomizeAlignmentSetting() && alignmentGroup.isEmpty() == false)
        {
          List<StageSpeedEnum> virtualLiveSpeedList = new ArrayList<StageSpeedEnum>();
          virtualLiveSpeedList.add(alignmentGroup.getStageSpeedEnum());
          region.initializeVirtualLiveSetting(virtualLiveSpeedList, 
                                              alignmentGroup.getUserGainEnum(),
                                              alignmentGroup.getSignalCompensationEnum(), 
                                              alignmentGroup.getDynamicRangeOptimizationLevel());
        }
        else
          region.clearVirtualLiveSetting();
      }
      else
      {
        if(subProgram.getTestProgram().isAlignmentGroupInvalidated(alignmentGroup) == false)
        {
          _didAlignmentRegionsChange = true;
          subProgram.getTestProgram().addInvalidatedAlignmentGroup(alignmentGroup);
        }
      }
    }

    if (_didAlignmentRegionsChange)
    {
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      subProgram.set2DAlignmentRegions(regions2D);
      subProgram.set3DAlignmentRegions(regions);
    }
    
    if (subProgram.getTestProgram().getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod() && 
        subProgram.getTestProgram().hasInvalidatedAlignmentGroups() == false &&
        hasCompleteAlignmentGroup)
    {
      create2DAlignmentRegionsIfAlignmentLearned(subProgram, alignmentGroups);
    }
  }

  /**
   * @author Matt Wharton
   */
  private void assignContextPadsForAlignmentRegion(ReconstructionRegion alignmentRegion, TestSubProgram testSubProgram)
  {
    Assert.expect(alignmentRegion != null);
    Assert.expect(testSubProgram != null);

    // Get all pads contained within the bounding box of the alignment features.
    Collection<Pad> alignmentRegionPads = alignmentRegion.getPads();
    if (alignmentRegionPads.isEmpty())
    {
      return;
    }
    Pad firstAlignmentPad = alignmentRegionPads.iterator().next();
    TestProgram testProgram = testSubProgram.getTestProgram();
    SurroundingPadUtil surroundingPadUtil = testProgram.getSurroundingPadUtil();
    PanelRectangle alignmentFeatureBounds = alignmentRegion.getAlignmentFeatureBoundsInNanoMeters();
    Set<Pad> candidateContextPads = surroundingPadUtil.getContainedPadsInRectangle(alignmentFeatureBounds,
                                                                                   alignmentRegion.isTopSide());

    // Exclude the actual alignment pads.
    candidateContextPads.removeAll(alignmentRegionPads);

    // Prune out any context pads which don't fit in the alignment region.
    PanelRectangle alignmentRegionRectangle = alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    for (Pad contextPad : new ArrayList<Pad>(candidateContextPads))
    {
      PanelRectangle contextPadRectangle = contextPad.getPanelRectangleWithLowerLeftOriginInNanoMeters();
      if (alignmentRegionRectangle.contains(contextPadRectangle) == false)
      {
        candidateContextPads.remove(contextPad);
      }
    }

    // Set the context pads for the region.
    Collection<JointInspectionData> alignmentContextFeatures = new LinkedList<JointInspectionData>();
    for (Pad contextPad : candidateContextPads)
    {
      // Ignore any context pads which aren't orthogonal with respect to the alignment pads.
      int rotationOfFirstAlignmentPad = (int)Math.round(firstAlignmentPad.getDegreesRotationAfterAllRotations());
      int rotationOfContextPad = (int)Math.round(contextPad.getDegreesRotationAfterAllRotations());

      if (((rotationOfFirstAlignmentPad - rotationOfContextPad) % 90) == 0)
      {
        // If the current pad is a two-pin device, we need to have both pads or none of the pads.
        if (ImageAnalysis.isSpecialTwoPinComponent(contextPad.getJointTypeEnum()))
        {
          Component contextComponent = contextPad.getComponent();
          if (candidateContextPads.containsAll(contextComponent.getPads()))
          {
            JointInspectionData jointInspectionData = new JointAlignmentData(contextPad);
            jointInspectionData.setInspectionRegion(alignmentRegion);
            alignmentContextFeatures.add(jointInspectionData);
          }
        }
        else
        {
          JointInspectionData jointInspectionData = new JointAlignmentData(contextPad);
          jointInspectionData.setInspectionRegion(alignmentRegion);
          alignmentContextFeatures.add(jointInspectionData);
        }
      }
    }

    alignmentRegion.setAlignmentContextFeatures(alignmentContextFeatures);
  }

  /**
   * @author George A. David
   */
  private void createFocusGroupForAlignmentRegion(ReconstructionRegion alignmentRegion, MagnificationTypeEnum magnificationType)
  {
    Assert.expect(alignmentRegion != null);
    Assert.expect(magnificationType != null);

    Collection<Pad> pads = alignmentRegion.getPads();
    List<Fiducial> fiducials = alignmentRegion.getFiducials();
    PanelRectangle regionRect = alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    final PanelRectangle focusRegionRect = new PanelRectangle(regionRect);

    if (focusRegionRect.getWidth() > FocusRegion.getMaxFocusRegionSizeInNanoMeters(magnificationType) ||
        focusRegionRect.getHeight() > FocusRegion.getMaxFocusRegionSizeInNanoMeters(magnificationType))
    {
      // try to fit as many pads/fiducials as possible in the focus region.
      List<Object> alignmentFeatures = new LinkedList<Object>(pads);
      alignmentFeatures.addAll(fiducials);
      List<PanelRectangle> focusRegions = new LinkedList<PanelRectangle>();
      Set<Object> processedFeatures = new HashSet<Object>();
      for (Object currentFeature : alignmentFeatures)
      {
        processedFeatures.add(currentFeature);

        PanelRectangle rect = null;
        if (currentFeature instanceof Pad)
          rect = new PanelRectangle(((Pad)currentFeature).getShapeRelativeToPanelInNanoMeters());
        else if (currentFeature instanceof Fiducial)
          rect = new PanelRectangle(((Fiducial)currentFeature).getShapeRelativeToPanelInNanoMeters());
        else
          Assert.expect(false, "Unkown feature: " + currentFeature);

        if (rect.getWidth() <= FocusRegion.getMaxFocusRegionSizeInNanoMeters(magnificationType) &&
            rect.getHeight() <= FocusRegion.getMaxFocusRegionSizeInNanoMeters(magnificationType))
        {
          // the rect fits in a focus region, now try to add more features to this rect.
          focusRegions.add(rect);
        }
      }

      // ok, check to see if we have any valid regions
      if (focusRegions.isEmpty() == false)
      {
        // ok, use the largest region as the focus region
        double largestMilsSquared = 0;
        PanelRectangle largestRegion = null;
        for (PanelRectangle rect : focusRegions)
        {
          double milsSquared = MathUtil.convertNanoMetersToMils(rect.getWidth()) *
                               MathUtil.convertNanoMetersToMils(rect.getHeight());
          if (milsSquared > largestMilsSquared)
          {
            largestMilsSquared = milsSquared;
            largestRegion = rect;
          }
        }

        focusRegionRect.setRect(largestRegion.getMinX(),
                                largestRegion.getMinY(),
                                largestRegion.getWidth(),
                                largestRegion.getHeight());
      }
      else
      {
        // the first procedure to come up with a focus region didn't work,
        // so let's start with the largest region in the center and
        // and move it to include the closest pad to the center.
        double width = focusRegionRect.getWidth();
        if (width > FocusRegion.getMaxFocusRegionSizeInNanoMeters(magnificationType))
          width = FocusRegion.getMaxFocusRegionSizeInNanoMeters(magnificationType);
        double height = focusRegionRect.getHeight();
        if (height > FocusRegion.getMaxFocusRegionSizeInNanoMeters(magnificationType))
          height = FocusRegion.getMaxFocusRegionSizeInNanoMeters(magnificationType);

        focusRegionRect.setRect(focusRegionRect.getCenterX() - (width / 2.0),
                                focusRegionRect.getCenterY() - (height / 2.0),
                                width,
                                height);

        Comparator<PanelRectangle> comparator = new Comparator<PanelRectangle>()
        {
          public double getClosestDistanceToReference(PanelRectangle rect)
          {
            Assert.expect(rect != null);

            double leftDistanceSquared = Line2D.ptSegDist(rect.getMinX(),
                rect.getMinY(),
                rect.getMinX(),
                rect.getMaxY(),
                focusRegionRect.getCenterX(),
                focusRegionRect.getCenterY());
            double rightDistanceSquared = Line2D.ptSegDist(rect.getMaxX(),
                rect.getMinY(),
                rect.getMaxX(),
                rect.getMaxY(),
                focusRegionRect.getCenterX(),
                focusRegionRect.getCenterY());
            double topDistanceSquared = Line2D.ptSegDist(rect.getMinX(),
                rect.getMaxY(),
                rect.getMaxX(),
                rect.getMaxY(),
                focusRegionRect.getCenterX(),
                focusRegionRect.getCenterY());
            double bottomDistanceSquared = Line2D.ptSegDist(rect.getMinX(),
                rect.getMinY(),
                rect.getMaxX(),
                rect.getMinY(),
                focusRegionRect.getCenterX(),
                focusRegionRect.getCenterY());

            return Math.max(leftDistanceSquared,
                            Math.max(rightDistanceSquared, Math.max(topDistanceSquared, bottomDistanceSquared)));

          }

          public int compare(PanelRectangle lhs, PanelRectangle rhs)
          {
            double rhsMinDistanceSquared = getClosestDistanceToReference(rhs);
            double lhsMinDistanceSquared = getClosestDistanceToReference(lhs);

            if (lhsMinDistanceSquared > rhsMinDistanceSquared)
              return 1;
            else if (lhsMinDistanceSquared < rhsMinDistanceSquared)
              return -1;
            else
              return 0;
          }
        };

        Set<PanelRectangle> sortedRects = new TreeSet<PanelRectangle>(comparator);
        Map<PanelRectangle, Pad> rectToPadMap = new HashMap<PanelRectangle, Pad>();
        Map<PanelRectangle, Fiducial> rectToFiducialMap = new HashMap<PanelRectangle, Fiducial>();
        for (Pad pad : pads)
        {
          PanelRectangle rect = new PanelRectangle(pad.getShapeRelativeToPanelInNanoMeters());
          rectToPadMap.put(rect, pad);
          sortedRects.add(rect);
        }

        for (Fiducial fiducial : fiducials)
        {
          PanelRectangle rect = new PanelRectangle(fiducial.getShapeRelativeToPanelInNanoMeters());
          rectToFiducialMap.put(rect, fiducial);
          sortedRects.add(rect);
        }

        // ok, now get the closest rectangle to the focus region
        PanelRectangle closestRect = sortedRects.iterator().next();

        // ok, now set the focus region x , y value to be that of the closestRect
        focusRegionRect.setMinXY(closestRect.getMinX(), closestRect.getMinY());

        // check to make sure the focus region is bigger in at least on dimension, if it isn't,
        // then move it closer to the center by half its width.
        if (focusRegionRect.getWidth() <= closestRect.getWidth() &&
            focusRegionRect.getHeight() <= closestRect.getHeight())
        {
          if (closestRect.getWidth() > focusRegionRect.getWidth())
          {
            if (focusRegionRect.getMinX() > regionRect.getCenterX())
              focusRegionRect.setMinXY((int)Math.rint(focusRegionRect.getMinX() - focusRegionRect.getWidth() / 2.0),
                                       focusRegionRect.getMinY());
            else
              focusRegionRect.setMinXY((int)Math.rint(focusRegionRect.getMinX() + focusRegionRect.getWidth() / 2.0),
                                       focusRegionRect.getMinY());
          }

          if (closestRect.getHeight() > focusRegionRect.getHeight())
          {
            if (focusRegionRect.getMinX() > regionRect.getCenterX())
              focusRegionRect.setMinXY(focusRegionRect.getMinX(),
                                       (int)Math.rint(focusRegionRect.getMinY() - focusRegionRect.getHeight() / 2.0));
            else
              focusRegionRect.setMinXY(focusRegionRect.getMinX(),
                                       (int)Math.rint(focusRegionRect.getMinY() + focusRegionRect.getHeight() / 2.0));
          }
        }

        // ok finally, make sure the focus region fits in the alignment region, if not, make it fit.
        if (focusRegionRect.getMaxX() > regionRect.getMaxX())
          focusRegionRect.setMinXY(focusRegionRect.getMinX() - (focusRegionRect.getMaxX() - regionRect.getMaxX()),
                                   focusRegionRect.getMinY());

        if (focusRegionRect.getMaxY() > regionRect.getMaxY())
          focusRegionRect.setMinXY(focusRegionRect.getMinX(),
                                   focusRegionRect.getMinY() - (focusRegionRect.getMaxY() - regionRect.getMaxY()));
      }
    }

    alignmentRegion.addFocusGroup(createFocusGroupForVerificationOrAlignmentRegion(focusRegionRect, alignmentRegion));
  }

  /**
   * @author George A. David
   */
  private Collection<List<Pad>> separatePadsBasedOnInspectionRegionRules(Component component)
  {
    Assert.expect(component != null);

    Map<String, List<Pad>> keyToPadListMap = new HashMap<String, List<Pad>>();
    List<Double> padSizes = new ArrayList<Double>();
    
    Boolean isLongPanel = component.getBoard().getPanel().getPanelSettings().isLongPanel();
    PanelRectangle rightSectionOfPanel = component.getBoard().getPanel().getPanelSettings().getRightSectionOfLongPanel();
    
    //Khaw Chek Hau - XCR3601: Auto distribute the pressfit region by direction of artifact
    boolean isPressfitUsingArtifactRegionSeperation = false;
    
    // iterate over the pads and seprate them into separate lists
    // by family, subtype, and rotation. Also, if the panel is divided,
    // split the pad lists into different regions based on which side of
    // the panel they are on.
    for (Pad pad : component.getPads())
    {
      if(pad.isTestable() == false)
        continue;
      InspectionFamilyEnum inspectionFamily = pad.getSubtype().getInspectionFamilyEnum();

      if(_padsThatExtendBeyondImageableRegion.contains(pad))
      {
        Assert.expect(pad.getPadSettings().isTestable() == false);
        continue;
      }

      // if the pad is too big to fit in an inspection region,
      // it cannot be tested so don't bother adding it to any list.
      // check that the resulting inspection region size is not too big.
      
      // Siew Yeng - comment out this condition as this is already check at predeterminePadTestability() function.
//      if (ReconstructionRegion.fitsInInspectionRegion(pad))
//      {
      JointTypeEnum jointTypeEnum = pad.getSubtype().getJointTypeEnum();

      String key = jointTypeEnum.getName();

      if ( InspectionFamily.isSpecialTwoPinDevice(inspectionFamily) == false)
      {
        // with user defined sliceheights, no region can have more than one subtype
        key += "_" + pad.getSubtype().getLongName();

        if (usePadRotationToBreakUpInspectionRegions(inspectionFamily))
          key += "_" + pad.getPinOrientationInDegreesAfterAllRotations();

        if (isLongPanel)
        {
          if(component.getBoard().getPanel().getPanelSettings().isPanelBasedAlignment())
          {
            if (rightSectionOfPanel.contains(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad)))
              key += "_" + PanelLocationInSystemEnum.RIGHT.getName();
            else
              key += "_" + PanelLocationInSystemEnum.LEFT.getName();
          }
          else
          {
            if(component.getBoard().getRightSectionOfLongBoard().contains(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad)))
              key += "_" + PanelLocationInSystemEnum.RIGHT.getName();
            else
              key += "_" + PanelLocationInSystemEnum.LEFT.getName();
          }
        }

        // finally, let's find a pad size that is within 20% of this pad size. we
        // want to separate pads that are more than 20% in any dimension
        double padSize = Math.max(pad.getWidthInNanoMeters(), pad.getLengthInNanoMeters());
        for (double size : padSizes)
        {
          double max = Math.max(padSize, size);
          double percent = 100.0 * Math.abs(size - padSize) / max;
          if (percent < 20)
          {
            // the two sizes are within 5% of each other, so use the one that is already in
            // the map
            padSize = size;
            break;
          }
        }

        int index = padSizes.indexOf(padSize);
        if (index == -1)
        {
          padSizes.add(padSize);
          index = padSizes.size() - 1;
        }

        key += "_" + index;
        
        //Khaw Chek Hau - XCR3601: Auto distribute the pressfit region by direction of artifact
        if (pad.getSubtype().getJointTypeEnum().equals(JointTypeEnum.PRESSFIT) && 
              pad.getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.ARTIFACT_BASED_REGION_SEPERATION_DIRECTION).equals("None") == false)
        {
          isPressfitUsingArtifactRegionSeperation = true;
          String padCoordinateIndex = null;
          
          if (pad.getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.ARTIFACT_BASED_REGION_SEPERATION_DIRECTION).equals("Horizontal"))
            padCoordinateIndex = "Y" + String.valueOf(pad.getCoordinateInNanoMeters().getY());
          else
            padCoordinateIndex = "X" + String.valueOf(pad.getCoordinateInNanoMeters().getX());
          
          key += "_" + padCoordinateIndex;
        }
      }

      List<Pad> pads = null;

      if (keyToPadListMap.containsKey(key))
      {
        pads = keyToPadListMap.get(key);
      }
      else
      {

        // for chips, we only have one list so make sure no list exists before creating a new one
        if (InspectionFamily.isSpecialTwoPinDevice(inspectionFamily))
          Assert.expect(keyToPadListMap.values().isEmpty());
        pads = new LinkedList<Pad>();
        keyToPadListMap.put(key, pads);
      }

      pads.add(pad);
//      }
//      else
//      {
//         pad.getPadSettings().setTestable(false);
//         if (ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad).getWidth() > ReconstructionRegion.getMaxInspectionRegionPadBoundsWidthInNanoMeters())
//           pad.getPadSettings().setUntestableReason(StringLocalizer.keyToString(new LocalizedString("PAD_UNTESTABLE_TOO_WIDE_FIT_RECONSTRUCTION_REGION_KEY",
//                                                                        new Object[]{MathUtil.convertNanoMetersToMils(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad).getWidth())})));
//         else if (ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad).getHeight() > ReconstructionRegion.getMaxInspectionRegionPadBoundsLengthInNanoMeters())
//           pad.getPadSettings().setUntestableReason(StringLocalizer.keyToString(new LocalizedString("PAD_UNTESTABLE_TOO_LONG_FIT_RECONSTRUCTION_REGION_KEY",
//                                                                        new Object[]{MathUtil.convertNanoMetersToMils(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad).getHeight())})));
//        
//        if (UnitTest.unitTesting() == false)
//        {
//          PanelRectangle padBounds = null;
//          if( InspectionFamily.isSpecialTwoPinDevice(pad.getSubtype().getInspectionFamilyEnum()) )
//          {
//            System.out.print("Component " + pad.getComponent().getReferenceDesignator() +
//                             " of land pattern " + pad.getComponent().getLandPattern().getName());
//            for (Pad compPad : component.getPads())
//            {
//              compPad.getPadTypeSettings().setTestable(false);
//              if (padBounds == null)
//                padBounds = ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(compPad);
//              else
//                padBounds.add(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(compPad));
//            }
//
//            if (padBounds.getWidth() > ReconstructionRegion.getMaxInspectionRegionPadBoundsWidthInNanoMeters())
//              System.out.println(" is too wide to fit in a reconstruction region (width: " +
//                                 MathUtil.convertNanoMetersToMils(padBounds.getWidth()) + ")");
//            else if (padBounds.getHeight() > ReconstructionRegion.getMaxInspectionRegionPadBoundsLengthInNanoMeters())
//              System.out.println(" is too long to fit in a reconstruction region (length: " +
//                                 MathUtil.convertNanoMetersToMils(padBounds.getHeight()) + ")");
//            else
//              Assert.expect(false);
//            break;
//          }
//          else
//          {
//            System.out.print("pad " + pad.getName() + " of component " + pad.getComponent().getReferenceDesignator() +
//                             " of land pattern " + pad.getComponent().getLandPattern().getName());
//
//            padBounds = ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad);
//            if (padBounds.getWidth() > ReconstructionRegion.getMaxInspectionRegionPadBoundsWidthInNanoMeters())
//              System.out.println(" is too wide to fit in a reconstruction region (width: " +
//                                 MathUtil.convertNanoMetersToMils(padBounds.getWidth()) + ")");
//            else if (padBounds.getHeight() > ReconstructionRegion.getMaxInspectionRegionPadBoundsLengthInNanoMeters())
//              System.out.println(" is too long to fit in a reconstruction region (length: " +
//                                 MathUtil.convertNanoMetersToMils(padBounds.getHeight()) + ")");
//            else
//              Assert.expect(false);
//          }
//        }
//      }
    }

    //Khaw Chek Hau - XCR3601: Auto distribute the pressfit region by direction of artifact
    if (isPressfitUsingArtifactRegionSeperation)
    {
      keyToPadListMap = separatePadsBasedOnInspectionRegionRulesForPressfit(component, keyToPadListMap);
    }
    
    return keyToPadListMap.values();
  }

  /**
   * Create inspection regions for the board
   * @author George A. David
   */
  private void createInspectionRegions(Board board)
  {
    Assert.expect(board != null);

    for (Component component : board.getComponents())
      createInspectionRegions(component);
  }

  /**
   * This method takes in a Panel and returns a List of InspectionRegion objects.
   *
   * @author George A. David
   */
  private void createInspectionRegions(Panel panel)
  {
    Assert.expect(panel != null);

    // iterate over each board create inspection regions for each board.
    for (Board board : panel.getBoards())
      createInspectionRegions(board);
  }

  /**
   * @author George A. David
   * @author Tan Hock Zoon
   */
  private void predeterminePadTestability()
  {
//    _padTypesThatExtendBeyondImageableRegion.clear();
    _padsThatExtendBeyondImageableRegion.clear();
    _componentTypesThatExtendBeyondImageableRegion.clear();
    
    boolean isEnlargeReconstructionRegion = _project.isEnlargeReconstructionRegion();

    Panel panel = _project.getPanel();

    Set<Pad> unsortedPads = panel.getPadsUnsorted();
    SurroundingPadUtil padUtil = new SurroundingPadUtil();
    padUtil.setUseBoundsForDeterminingVicinity(true);
    padUtil.setUseMaximumAreaNeededForInspection(false);
    padUtil.setPads(unsortedPads);

    // create the testable area
    PanelRectangle testableRect = null;

    //Siew Yeng - XCR-3174 - Assert when load a recipe with high mag component in a system without high mag
    //move this block of code from end of fuction to here to avoid skipping pad testability check.
    for(Pad pad : unsortedPads)
    {
      // if it's a through-hole pad, make sure the hole is within the pad
      if (pad.isThroughHolePad())
      {
        ThroughHoleLandPatternPad landPad = (ThroughHoleLandPatternPad)pad.getLandPatternPad();
        
        // if the hole for PTH is too small, set testable to false
        if (MathUtil.fuzzyContains(landPad.getShapeInNanoMeters(), landPad.getHoleShapeInNanoMeters()) == false)
        {
//          pad.getPadTypeSettings().setTestable(false);
//          pad.getPadType().getPadTypeSettings().setUntestableReason(StringLocalizer.keyToString(new LocalizedString("PAD_UNTESTABLE_HOLE_SHAPE_TOO_SMALL_KEY",
//                                                                    new Object[]{landPad.getHoleShapeInNanoMeters()})));
          pad.getPadSettings().setTestable(false);
          pad.getPadSettings().setUntestableReason(StringLocalizer.keyToString(new LocalizedString("PAD_UNTESTABLE_HOLE_SHAPE_TOO_SMALL_KEY",
                                                                    new Object[]{landPad.getHoleShapeInNanoMeters()})));
        }
      }
    }
    
    // Wei Chin added to filter variable Magnification before create inspection region
    for(Component comp : panel.getComponents())
    {
      for(Pad pad : comp.getPads())
      {
        boolean doesPadFit = ReconstructionRegion.fitsInInspectionRegion(pad, isEnlargeReconstructionRegion);
        //Siew Yeng - move the code from separatePadsBasedOnInspectionRegionRules()
        if(doesPadFit == false)
        {
          pad.getPadSettings().setTestable(false);
          if (ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad).getWidth() > ReconstructionRegion.getMaxInspectionRegionPadBoundsWidthInNanoMeters())
            pad.getPadSettings().setUntestableReason(StringLocalizer.keyToString(new LocalizedString("PAD_UNTESTABLE_TOO_WIDE_FIT_RECONSTRUCTION_REGION_KEY",
                                                                         new Object[]{MathUtil.convertNanoMetersToMils(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad).getWidth())})));
          else if (ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad).getHeight() > ReconstructionRegion.getMaxInspectionRegionPadBoundsLengthInNanoMeters())
            pad.getPadSettings().setUntestableReason(StringLocalizer.keyToString(new LocalizedString("PAD_UNTESTABLE_TOO_LONG_FIT_RECONSTRUCTION_REGION_KEY",
                                                                         new Object[]{MathUtil.convertNanoMetersToMils(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad).getHeight())})));
        }
      }
    }  
    
     //XCR1433 - To prevent v810 is crashed, make sure
    //          testsubprogram is valid 
    if(_testProgram.getAllTestSubPrograms().isEmpty() == true)
    {
      System.out.println("Test Sub Program is Empty ! ");
      return;
    }
    for(TestSubProgram subProgram : _testProgram.getAllTestSubPrograms())
    {
      if(testableRect == null)
        testableRect = new PanelRectangle(subProgram.getImageableRegionInNanoMeters());
      else
        testableRect.add(subProgram.getImageableRegionInNanoMeters());
    }

    // all the pads not in the testable area are untestable
    Set<Pad> pads = padUtil.getPadsNotContainedInRectangle(testableRect);
    for(Pad pad : pads)
    {
      _componentTypesThatExtendBeyondImageableRegion.add(pad.getComponent().getComponentType());
      
      //For pads that beyond imageable region, set testable to false
      if (ImageAnalysis.isSpecialTwoPinComponent(pad.getJointTypeEnum()))
      {
        for (Pad compPad : pad.getComponent().getPads())
        {
//          compPad.getPadTypeSettings().setTestable(false);
//          _padTypesThatExtendBeyondImageableRegion.add(compPad.getPadType());
//          compPad.getPadType().getPadTypeSettings().setUntestableReason(StringLocalizer.keyToString("PAD_UNTESTABLE_EXTEND_BEYOND_IMAGABLE_REGION_KEY"));
          compPad.getPadSettings().setTestable(false);
          _padsThatExtendBeyondImageableRegion.add(compPad);
          compPad.getPadSettings().setUntestableReason(StringLocalizer.keyToString("PAD_UNTESTABLE_EXTEND_BEYOND_IMAGABLE_REGION_KEY"));
        }
      }
      else
      {
//        _padTypesThatExtendBeyondImageableRegion.add(pad.getPadType());
//        pad.getPadTypeSettings().setTestable(false);
//        pad.getPadType().getPadTypeSettings().setUntestableReason(StringLocalizer.keyToString("PAD_UNTESTABLE_EXTEND_BEYOND_IMAGABLE_REGION_KEY"));
          _padsThatExtendBeyondImageableRegion.add(pad);
          pad.getPadSettings().setTestable(false);
          pad.getPadSettings().setUntestableReason(StringLocalizer.keyToString("PAD_UNTESTABLE_EXTEND_BEYOND_IMAGABLE_REGION_KEY"));
      }
    }
  }

  /**
   * @author George A. David
   */
  private boolean usePadRotationToBreakUpInspectionRegions(final InspectionFamilyEnum inspectionFamilyEnum)
  {
    Assert.expect(inspectionFamilyEnum != null);

    if (InspectionFamily.isSpecialTwoPinDevice(inspectionFamilyEnum))
      return false;
    else
      return true;
  }

  /**
   * @author George A. David
   */
  private int getZcurveWidthInNanoMeters(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);

    Pad pad = reconstructionRegion.getComponent().getPads().get(0);
    JointTypeEnum jointType = pad.getSubtype().getJointTypeEnum();

    int zCurveWidthInNanoMeters = MathUtil.convertMilsToNanoMetersInteger(4);

    if (jointType.equals(JointTypeEnum.COLLAPSABLE_BGA))
      zCurveWidthInNanoMeters = (int)Math.rint(0.075 * pad.getPitchInNanoMeters());
    else if (jointType.equals(JointTypeEnum.NON_COLLAPSABLE_BGA))
      zCurveWidthInNanoMeters = (int)Math.rint(0.125 * pad.getPitchInNanoMeters());
    else if (jointType.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
      zCurveWidthInNanoMeters = (int)Math.rint(0.075 * pad.getPitchInNanoMeters());
    else if (jointType.equals(JointTypeEnum.THROUGH_HOLE) ||
            jointType.equals(JointTypeEnum.OVAL_THROUGH_HOLE) || //Siew Yeng - XCR-3318 - Oval PTH
            jointType.equals(JointTypeEnum.PRESSFIT))
      zCurveWidthInNanoMeters = _project.getPanel().getThicknessInNanometers();

    return zCurveWidthInNanoMeters;
  }

  /**
   * @author George A. David
   */
  private void createSlicesForUnfocusedRegionsNeedingRetestIfNecessary(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);

    if(_project.getPanel().getPanelSettings().hasUnfocusedRegionNameToRetest(reconstructionRegion.getName()))
    {
      for (FocusGroup focusGroup : reconstructionRegion.getFocusGroups())
      {
        for(Slice slice : new ArrayList<Slice>(focusGroup.getSlices()))
        {
          List<SliceNameEnum> sliceNames = slice.getSliceName().getUnfocusedRegionSliceNamesForRetest();

          //ok, create 4 new slices to above and to below the current slice at 2 mil intervals
          int zOffset = _UNFOCUSED_REGION_SLICE_INTERVAL_IN_NANOS;
          Slice newSlice = createSlice(sliceNames.get(0),
                                       SliceTypeEnum.RECONSTRUCTION,
                                       slice.getFocusInstruction().getPercentValue(),
                                       zOffset,
                                       slice.getFocusInstruction().getZHeightInNanoMeters(),
                                       slice.getFocusInstruction().getFocusMethod(),
                                       false);
          focusGroup.addSlice(newSlice);

          zOffset += _UNFOCUSED_REGION_SLICE_INTERVAL_IN_NANOS;
          newSlice = createSlice(sliceNames.get(1),
                                 SliceTypeEnum.RECONSTRUCTION,
                                 slice.getFocusInstruction().getPercentValue(),
                                 zOffset,
                                 slice.getFocusInstruction().getZHeightInNanoMeters(),
                  slice.getFocusInstruction().getFocusMethod(),
                  false);
          focusGroup.addSlice(newSlice);

          zOffset = -_UNFOCUSED_REGION_SLICE_INTERVAL_IN_NANOS;
          newSlice = createSlice(sliceNames.get(2),
                                 SliceTypeEnum.RECONSTRUCTION,
                                 slice.getFocusInstruction().getPercentValue(),
                                 zOffset,
                                 slice.getFocusInstruction().getZHeightInNanoMeters(),
                  slice.getFocusInstruction().getFocusMethod(),
                  false);
          focusGroup.addSlice(newSlice);

          zOffset -= _UNFOCUSED_REGION_SLICE_INTERVAL_IN_NANOS;
          newSlice = createSlice(sliceNames.get(3),
                                 SliceTypeEnum.RECONSTRUCTION,
                                 slice.getFocusInstruction().getPercentValue(),
                                 zOffset,
                                 slice.getFocusInstruction().getZHeightInNanoMeters(),
                  slice.getFocusInstruction().getFocusMethod(),
                  false);
          focusGroup.addSlice(newSlice);
        }
      }
    }
  } 

  /**
   * added midboardOffsetInNanometer
   * added useDynamicRangeOptimization - Siew Yeng - XCR-2140
   * @author Wei Chin on behalf Lee Herng
   */
  private FocusGroup createFocusGroup(FocusProfileShapeEnum focusProfileShape,
                                      PanelRectangle focusRegionRectangleRelativeToPanelInNanoMeters,
                                      SliceNameEnum sliceName,
                                      SliceTypeEnum sliceType,
                                      int percentValue,
                                      int zOffsetInNanoMeters,
                                      int zHeightInNanoMeters,
                                      int zCurveWidthInNanoMeters,
                                      FocusMethodEnum focusMethod,
                                      int relativeZOffsetInNanoMeters,
                                      int midboardOffsetInNanometer,
                                      boolean isSliceUseToBuildGlobalSurfaceModel,
                                      int userDefinedWaveletLevel,
                                      boolean useDynamicRangeOptimization,
                                      int searchRangeLowLimitInNanometers,
                                      int searchRangeHighLimitInNanometers,
                                      int pspZOffsetInNanometers,
                                      ReconstructionRegion reconstructionRegion,
                                      boolean useMultipleStageSpeed)
  {
    FocusGroup focusGroup = new FocusGroup();
    FocusSearchParameters focusSearchParameters = new FocusSearchParameters();
    focusSearchParameters.setFocusProfileShape(focusProfileShape);
    FocusRegion focusRegion = new FocusRegion();
    focusRegion.setRectangleRelativeToPanelInNanoMeters(focusRegionRectangleRelativeToPanelInNanoMeters);
    focusSearchParameters.setFocusRegion(focusRegion);
    focusSearchParameters.setZCurveWidthInNanoMeters(zCurveWidthInNanoMeters);
    focusSearchParameters.setInitialWaveletLevel(userDefinedWaveletLevel);
    focusGroup.setRelativeZoffsetFromSurfaceInNanoMeters(relativeZOffsetInNanoMeters);
    focusGroup.setFocusSearchParameters(focusSearchParameters);
    Slice slice = createSlice(sliceName,
                              sliceType,
                              percentValue,
                              zOffsetInNanoMeters,
                              zHeightInNanoMeters,
                              focusMethod,
                              isSliceUseToBuildGlobalSurfaceModel,
                              useDynamicRangeOptimization,
                              useMultipleStageSpeed);
    focusGroup.addSlice(slice);
    focusGroup.setAutoFocusMidBoardOffsetInNanometers(midboardOffsetInNanometer);
    focusGroup.setSearchRangeLowLimitInNanometers(searchRangeLowLimitInNanometers);
    focusGroup.setSearchRangeHighLimitInNanometers(searchRangeHighLimitInNanometers);
    
    // XCR-3663 PSP Z-Offset
    if (reconstructionRegion.getComponent().isTopSide())
      pspZOffsetInNanometers = -pspZOffsetInNanometers;
    
    focusGroup.setPspZOffsetInNanometers(pspZOffsetInNanometers);

    final ComponentType componentType = reconstructionRegion.getComponent().getComponentType();

    POPLayerIdEnum popLayerId = POPLayerIdEnum.BASE;
    int totalPOPLayerNumber = _DEFAULT_TOTAL_POP_LAYER_NUMBER;
    int popZHeightInNanometers = _DEFAULT_POP_ZHEIGHT_IN_NANOMETERS;
    
    if (componentType.hasCompPackageOnPackage())
    {
      popLayerId = componentType.getPOPLayerId();
      popZHeightInNanometers = componentType.getPOPZHeightInNanometers();
      totalPOPLayerNumber = componentType.getCompPackageOnPackage().getTotalPOPLayerNumber();
      
      if (componentType.getPOPLayerId().equals(POPLayerIdEnum.BASE))
      {
        for (ComponentType compType : componentType.getCompPackageOnPackage().getComponentTypes())
        {
          if (compType.getPOPLayerId().equals(POPLayerIdEnum.ONE)) 
          {
            popZHeightInNanometers = compType.getPOPZHeightInNanometers();  
            break;
          }
        }
      } 
    } 
    
    focusGroup.setPOPLayerNumber(popLayerId);
    focusGroup.setPOPTotalLayerNumber(totalPOPLayerNumber);
    focusGroup.setPOPZHeightInNanometers(popZHeightInNanometers);
    
    return focusGroup;
  }

  /**
   * @author George A. David
   */
  private Slice createSlice(SliceNameEnum sliceName,
                            SliceTypeEnum sliceType,
                            int percentValue,
                            int zOffsetInNanoMeters,
                            int zHeightInNanoMeters,
                            FocusMethodEnum focusMethod,
                            boolean useToBuildGlobalSurfaceModel)

  {
    return createSlice(sliceName, 
                      sliceType, 
                      percentValue, 
                      zOffsetInNanoMeters, 
                      zHeightInNanoMeters, 
                      focusMethod, 
                      useToBuildGlobalSurfaceModel, 
                      false,
                      false);
  }
  
  /**
   * Added useDynamicRangeOptimization
   * @author Siew Yeng - XCR-2140
   */
  private Slice createSlice(SliceNameEnum sliceName,
                            SliceTypeEnum sliceType,
                            int percentValue,
                            int zOffsetInNanoMeters,
                            int zHeightInNanoMeters,
                            FocusMethodEnum focusMethod,
                            boolean useToBuildGlobalSurfaceModel,
                            boolean useDynamicRangeOptimization,
                            boolean useMultipleStageSpeed)

  {
    Slice slice = new Slice();
    slice.setSliceType(sliceType);
    slice.setSliceName(sliceName);
    FocusInstruction focusInstruction = new FocusInstruction();
    focusInstruction.setFocusMethod(focusMethod);
    focusInstruction.setPercentValue(percentValue);
    focusInstruction.setZOffsetInNanoMeters(zOffsetInNanoMeters);
    focusInstruction.setZHeightInNanoMeters(zHeightInNanoMeters);
    slice.setFocusInstruction(focusInstruction);
    
    if (useMultipleStageSpeed)
    {
      if (_config.getBooleanValue(SoftwareConfigEnum.USE_VELOCITY_MAPPING))
        slice.setReconstructionMethod(ReconstructionMethodEnum.BLEND);
      else
        slice.setReconstructionMethod(ReconstructionMethodEnum.STRETCH);
    }
    else if(useDynamicRangeOptimization) //Siew Yeng - XCR-2140
      slice.setReconstructionMethod(ReconstructionMethodEnum.STRETCH);
    else
      slice.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
    slice.setUseToBuildGlobalSurfaceModel(useToBuildGlobalSurfaceModel);

    return slice;
  }

  /**
   * @return a Slice using the specified focusInstruction and sliceNameEnum.
   * The slice will use ReconstructionMethod AVERAGE and SliceType will be set to RECONSTRUCTION
   *
   * @author Peter Esbensen refactored from George's code
   */
  private Slice createSlice(FocusInstruction focusInstruction,
                            SliceNameEnum sliceNameEnum,
                            boolean useToBuildGlobalSurfaceModel)
  {
    return createSlice(focusInstruction, sliceNameEnum, useToBuildGlobalSurfaceModel, false, false);
  }
  
  /**
   * @return a Slice using the specified focusInstruction and sliceNameEnum.
   * SliceType will be set to RECONSTRUCTION
   * If useDynamicRangeOptimization is true, this slice will use ReconstructionMethod STRETCH.
   *
   * @author Peter Esbensen refactored from George's code
   * @author Siew Yeng - XCR-2140 - DRO
   */
  private Slice createSlice(FocusInstruction focusInstruction,
                            SliceNameEnum sliceNameEnum,
                            boolean useToBuildGlobalSurfaceModel,
                            boolean useDynamicRangeOptimization,
                            boolean useMultipleStageSpeed)
  {
    Assert.expect(focusInstruction != null);
    Assert.expect(sliceNameEnum != null);

    Slice slice = new Slice();
    slice.setSliceName(sliceNameEnum);
    slice.setFocusInstruction(focusInstruction);

    if (useMultipleStageSpeed)
    {
      if (_config.getBooleanValue(SoftwareConfigEnum.USE_VELOCITY_MAPPING))
        slice.setReconstructionMethod(ReconstructionMethodEnum.BLEND);
      else
        slice.setReconstructionMethod(ReconstructionMethodEnum.STRETCH);
    }
    else if(useDynamicRangeOptimization)
      slice.setReconstructionMethod(ReconstructionMethodEnum.STRETCH);
    else
      slice.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
    slice.setSliceType(SliceTypeEnum.RECONSTRUCTION);
    slice.setUseToBuildGlobalSurfaceModel(useToBuildGlobalSurfaceModel);

    return slice;
  }
  
  /**
   * @author Chnee Khang Wah
   * 2012-02-21, 2.5D
   */
  private void createProjectionSliceForFocusGroup(FocusGroup focusGroup,
                                                  int percentValue,
                                                  int zOffsetInNanoMeters,
                                                  int zHeightInNanoMeters,
                                                  FocusMethodEnum focusMethod,
                                                  boolean useToBuildGlobalSurfaceModel)
  {
    // Only camera 1,2,5,6,8,9,11,13 will be send to generate and send to VVTS for release version.
    // To get projection image for all cameras, turn on the follwing flag to true.
    // By order of Seng Yew, open 2.5D to all camera. (2013-01-07)
      
    Slice slice = new Slice();
    
    if (true/*Config.isDeveloperDebugModeOn()*/)
    {
        slice = createSlice(SliceNameEnum.CAMERA_0, SliceTypeEnum.PROJECTION, percentValue,
                            zOffsetInNanoMeters, zHeightInNanoMeters, focusMethod, useToBuildGlobalSurfaceModel);
        focusGroup.addSlice(slice);
        
        slice = createSlice(SliceNameEnum.CAMERA_3, SliceTypeEnum.PROJECTION, percentValue,
                            zOffsetInNanoMeters, zHeightInNanoMeters, focusMethod, useToBuildGlobalSurfaceModel);
        focusGroup.addSlice(slice);
        
        slice = createSlice(SliceNameEnum.CAMERA_4, SliceTypeEnum.PROJECTION, percentValue,
                            zOffsetInNanoMeters, zHeightInNanoMeters, focusMethod, useToBuildGlobalSurfaceModel);
        focusGroup.addSlice(slice);
        
        slice = createSlice(SliceNameEnum.CAMERA_7, SliceTypeEnum.PROJECTION, percentValue,
                            zOffsetInNanoMeters, zHeightInNanoMeters, focusMethod, useToBuildGlobalSurfaceModel);
        focusGroup.addSlice(slice);
        
        slice = createSlice(SliceNameEnum.CAMERA_10, SliceTypeEnum.PROJECTION, percentValue,
                            zOffsetInNanoMeters, zHeightInNanoMeters, focusMethod, useToBuildGlobalSurfaceModel);
        focusGroup.addSlice(slice);
        
        slice = createSlice(SliceNameEnum.CAMERA_12, SliceTypeEnum.PROJECTION, percentValue,
                            zOffsetInNanoMeters, zHeightInNanoMeters, focusMethod, useToBuildGlobalSurfaceModel);
        focusGroup.addSlice(slice);
    }
    
    slice = createSlice(SliceNameEnum.CAMERA_1, SliceTypeEnum.PROJECTION, percentValue,
                        zOffsetInNanoMeters, zHeightInNanoMeters, focusMethod, useToBuildGlobalSurfaceModel);
    focusGroup.addSlice(slice);
    
    slice = createSlice(SliceNameEnum.CAMERA_2, SliceTypeEnum.PROJECTION, percentValue,
                        zOffsetInNanoMeters, zHeightInNanoMeters, focusMethod, useToBuildGlobalSurfaceModel);
    focusGroup.addSlice(slice);
    
    slice = createSlice(SliceNameEnum.CAMERA_5, SliceTypeEnum.PROJECTION, percentValue,
                        zOffsetInNanoMeters, zHeightInNanoMeters, focusMethod, useToBuildGlobalSurfaceModel);
    focusGroup.addSlice(slice);
    
    slice = createSlice(SliceNameEnum.CAMERA_6, SliceTypeEnum.PROJECTION, percentValue,
                        zOffsetInNanoMeters, zHeightInNanoMeters, focusMethod, useToBuildGlobalSurfaceModel);
    focusGroup.addSlice(slice);
    
    slice = createSlice(SliceNameEnum.CAMERA_8, SliceTypeEnum.PROJECTION, percentValue,
                        zOffsetInNanoMeters, zHeightInNanoMeters, focusMethod, useToBuildGlobalSurfaceModel);
    focusGroup.addSlice(slice);
    
    slice = createSlice(SliceNameEnum.CAMERA_9, SliceTypeEnum.PROJECTION, percentValue,
                        zOffsetInNanoMeters, zHeightInNanoMeters, focusMethod, useToBuildGlobalSurfaceModel);
    focusGroup.addSlice(slice);
    
    slice = createSlice(SliceNameEnum.CAMERA_11, SliceTypeEnum.PROJECTION, percentValue,
                        zOffsetInNanoMeters, zHeightInNanoMeters, focusMethod, useToBuildGlobalSurfaceModel);
    focusGroup.addSlice(slice);
    
    slice = createSlice(SliceNameEnum.CAMERA_13, SliceTypeEnum.PROJECTION, percentValue,
                        zOffsetInNanoMeters, zHeightInNanoMeters, focusMethod, useToBuildGlobalSurfaceModel);
    focusGroup.addSlice(slice);   
  }


  /**
   * @author George A. David
   */
  protected FocusGroup createFocusGroupForVerificationOrAlignmentRegion(PanelRectangle focusRegionRectangleInNanoMeters,
      ReconstructionRegion reconstructionRegion)
  {
    FocusGroup focusGroup = new FocusGroup();
    FocusSearchParameters focusSearchParameters = new FocusSearchParameters();
    FocusRegion focusRegion = new FocusRegion();
    focusRegion.setRectangleRelativeToPanelInNanoMeters(focusRegionRectangleInNanoMeters);
    focusSearchParameters.setFocusRegion(focusRegion);
    focusSearchParameters.setFocusProfileShape(FocusProfileShapeEnum.PEAK);
    focusGroup.setFocusSearchParameters(focusSearchParameters);
    // Assume users have followed the guidelines and chosen single-sided
    // alignment regions. This will tell autofocus to just use the largest
    // peak/edge rather than trying to pick the specified side of the board.
    if (reconstructionRegion.isAlignmentRegion())
      focusGroup.setSingleSidedRegionOfBoard(true);
    Slice slice = new Slice();
    slice.setSliceName(SliceNameEnum.PAD);
    FocusInstruction focusInstruction = new FocusInstruction();
    focusInstruction.setFocusMethod(FocusMethodEnum.SHARPEST);
    focusInstruction.setPercentValue(0);
    slice.setFocusInstruction(focusInstruction);
    slice.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
    slice.setAssociatedReconstructionRegion(reconstructionRegion);
    focusGroup.addSlice(slice);
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    //Add projection slide
    Slice slice_2D = new Slice();
    slice_2D.setSliceName(SliceNameEnum.CAMERA_0);
    slice_2D.setSliceType(SliceTypeEnum.PROJECTION);
    FocusInstruction focusInstruction_2D = new FocusInstruction();
    focusInstruction_2D.setFocusMethod(FocusMethodEnum.SHARPEST);
    focusInstruction_2D.setPercentValue(0);
    slice_2D.setFocusInstruction(focusInstruction_2D);
    slice_2D.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
    slice_2D.setAssociatedReconstructionRegion(reconstructionRegion);
    focusGroup.addSlice(slice_2D);

    return focusGroup;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  protected FocusGroup createFocusGroupFor2DAlignmentRegion(PanelRectangle focusRegionRectangleInNanoMeters,
      ReconstructionRegion reconstructionRegion)
  {
    FocusGroup focusGroup = new FocusGroup();
    FocusSearchParameters focusSearchParameters = new FocusSearchParameters();
    FocusRegion focusRegion = new FocusRegion();
    focusRegion.setRectangleRelativeToPanelInNanoMeters(focusRegionRectangleInNanoMeters);
    focusSearchParameters.setFocusRegion(focusRegion);
    focusSearchParameters.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
    focusGroup.setFocusSearchParameters(focusSearchParameters);
    // Assume users have followed the guidelines and chosen single-sided
    // alignment regions. This will tell autofocus to just use the largest
    // peak/edge rather than trying to pick the specified side of the board.
    if (reconstructionRegion.isAlignmentRegion())
      focusGroup.setSingleSidedRegionOfBoard(true);
    Slice slice = new Slice();
    slice.setSliceName(SliceNameEnum.PAD);
    FocusInstruction focusInstruction = new FocusInstruction();
    focusInstruction.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);
    focusInstruction.setPercentValue(0);
    slice.setFocusInstruction(focusInstruction);
    slice.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
    slice.setAssociatedReconstructionRegion(reconstructionRegion);
    focusGroup.addSlice(slice);
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    //Add projection slide
    Slice slice_2D = new Slice();
            
    slice_2D.setSliceName(SliceNameEnum.CAMERA_0);
    slice_2D.setSliceType(SliceTypeEnum.PROJECTION);
    FocusInstruction focusInstruction_2D = new FocusInstruction();
    focusInstruction_2D.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);
    focusInstruction_2D.setPercentValue(0);
    slice_2D.setFocusInstruction(focusInstruction_2D);
    slice_2D.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
    slice_2D.setAssociatedReconstructionRegion(reconstructionRegion);
    focusGroup.addSlice(slice_2D);
  
    return focusGroup;
  }
  
  /**
   * @author George A. David
   */
  private void markedMirroredComponentsAsCausingInterferencePatterns()
  {
    for(Set<Component> components : _landPatternRotationKeyToComponentsMap.values())
    {
      if(components.size() > 1 )
      {
        Set<Component> processedComponents = new HashSet<Component>();
        for(Component component : components)
        {
          processedComponents.add(component);
          for(Component otherComp : components)
          {
            if(processedComponents.contains(otherComp))
              continue;

            PanelCoordinate compCoord = component.getCoordinateRelativeToPanelInNanoMeters();
            PanelCoordinate otherCoord = otherComp.getCoordinateRelativeToPanelInNanoMeters();
            if(MathUtil.fuzzyEquals(compCoord.getX(), otherCoord.getX()) &&
               MathUtil.fuzzyEquals(compCoord.getY(), otherCoord.getY()))
            {
              // they are in the same exact spot, now lets check to make sure the pads match up.
              boolean doPadsMatch = true;
              Set<Pad> processedPads = new HashSet<Pad>();
              for(Pad pad : component.getPads())
              {
                processedPads.add(pad);
                for(Pad otherPad : otherComp.getPads())
                {
                  if(processedPads.contains(otherPad))
                    continue;

                  PanelCoordinate padCoord = pad.getCenterCoordinateRelativeToPanelInNanoMeters();
                  PanelCoordinate otherPadCoord = pad.getCenterCoordinateRelativeToPanelInNanoMeters();
                  if(MathUtil.fuzzyEquals(padCoord.getX(), otherPadCoord.getX()) == false ||
                     MathUtil.fuzzyEquals(padCoord.getY(), otherPadCoord.getY()) == false)
                  {
                    doPadsMatch = false;
                    break;
                  }
                  else // they match
                    processedPads.add(otherPad);
                }
              }

              if(doPadsMatch)
              {
                // these are mirrored components!
//                for(ReconstructionRegion region : _componentToReconstructionRegionsMap.get(component))
//                  region.setCausesInterferencePattern(true);
//                // let's clear out the regions so we don't process them again
//                _componentToReconstructionRegionsMap.get(component).clear();
//
//                for(ReconstructionRegion region : _componentToReconstructionRegionsMap.get(otherComp))
//                  region.setCausesInterferencePattern(true);
//                // let's clear out the regions so we don't process them again
//                _componentToReconstructionRegionsMap.get(otherComp).clear();
//
//                processedComponents.add(otherComp);
              }
            }
          }
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  private void setupFocusPriority(ReconstructionRegion region, boolean isSingleSided, boolean hasPotentialGridArrayArtifact)
  {
    if(region.getJointTypeEnum().equals(JointTypeEnum.RESISTOR) ||
       region.getJointTypeEnum().equals(JointTypeEnum.CAPACITOR) ||
       region.getJointTypeEnum().equals(JointTypeEnum.POLARIZED_CAP) ||
       region.getJointTypeEnum().equals(JointTypeEnum.GULLWING) ||
       region.getJointTypeEnum().equals(JointTypeEnum.JLEAD)
       // Wei Chin (Tall Cap)
//     || region.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR)
       )
    {
      if(isSingleSided)
        region.setFocusPriority(FocusPriorityEnum.FIRST);
      else if(hasPotentialGridArrayArtifact)
        region.setFocusPriority(FocusPriorityEnum.SIXTH);
      else
        region.setFocusPriority(FocusPriorityEnum.THIRD);
    }
    else if(region.getJointTypeEnum().equals(JointTypeEnum.NON_COLLAPSABLE_BGA) ||
            region.getJointTypeEnum().equals(JointTypeEnum.COLLAPSABLE_BGA) ||
            region.getJointTypeEnum().equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {
      if(isSingleSided)
        region.setFocusPriority(FocusPriorityEnum.SECOND);
      else
        region.setFocusPriority(FocusPriorityEnum.FOURTH);
    }
    else if(region.getJointTypeEnum().equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      if(isSingleSided)
        region.setFocusPriority(FocusPriorityEnum.THIRD);
      else
        region.setFocusPriority(FocusPriorityEnum.FIFTH);
    }
    else if(region.getJointTypeEnum().equals(JointTypeEnum.LEADLESS) ||
            region.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE) ||
            region.getJointTypeEnum().equals(JointTypeEnum.OVAL_THROUGH_HOLE) || //Siew Yeng - XCR-3318 - Oval PTH
            region.getJointTypeEnum().equals(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD))
      region.setFocusPriority(FocusPriorityEnum.FIFTH);
    else if(region.getJointTypeEnum().equals(JointTypeEnum.CGA) ||
            region.getJointTypeEnum().equals(JointTypeEnum.SINGLE_PAD) ||
            region.getJointTypeEnum().equals(JointTypeEnum.LGA) ||
            region.getJointTypeEnum().equals(JointTypeEnum.EXPOSED_PAD) ||
            region.getJointTypeEnum().equals(JointTypeEnum.SMALL_OUTLINE_LEADLESS))
      region.setFocusPriority(FocusPriorityEnum.SIXTH);
    else if(region.getJointTypeEnum().equals(JointTypeEnum.PRESSFIT) ||
            region.getJointTypeEnum().equals(JointTypeEnum.RF_SHIELD)||
            region.getJointTypeEnum().equals(JointTypeEnum.SURFACE_MOUNT_CONNECTOR))
      region.setFocusPriority(FocusPriorityEnum.SEVENTH);
    else
      Assert.expect(false);
  }

  /**
   * @author George A. David
   */
  private String getLandPatternKey(LandPattern landPattern, double degreesRotation)
  {
    // the program generation code will only run once on a landpattern, inspection family,
    // and rotation combination this is done in order to speed up program generation. The idea is
    // that the combination will always yield the result in the land pattern pads being
    // separated exactly the same and then the focus regions being created exactly the same.
    // So what we will do is to create them once based on the land pattern, then apply a matrix
    // to them to rotate them and locate them in the correct place for each component.
    // This is only true for components that are different by mulitples of 180. For example: 0 180 or
    // 12 192, etc...
    degreesRotation %= _DEGREES_ROTATION_NORMALIZATION_FACTOR;
    return landPattern.getName().toUpperCase() + "_"  + degreesRotation;
  }

  /**
   * @author George A. David
   */
  private Collection<Collection<Set<LandPatternPad>>> getSeparatedLandPatternPads(Component component)
  {
    String key = getLandPatternKey(component.getLandPattern(), component.getDegreesRotationAfterAllRotations());

    if (_landPatternKeyToPadListMap.containsKey(key))
    {
      List<List<Pad>> padList = _landPatternKeyToPadListMap.get(key);
      List<Pad> componentPads = component.getPads();
      for (List<Pad> pads :  padList)
      {
        if (componentPads.size() == pads.size())
        {
          boolean areSimilar = false;
          for (int i = 0; i < pads.size(); ++i)
          {
            Pad pad = pads.get(i);
            Pad componentPad = componentPads.get(i);
            if (pad.getLandPatternPad() == componentPad.getLandPatternPad() &&
               pad.getSubtype().getInspectionFamilyEnum().equals(componentPad.getSubtype().getInspectionFamilyEnum()) &&
               pad.getSubtype().equals(componentPad.getSubtype()))
            {
              areSimilar = true;
            }
            else
            {
              areSimilar = false;
              break;
            }
          }

          if (areSimilar)
            return _padsToSeparatedLandPatternPadsMap.get(pads);
        }
      }
    }

    return new LinkedList<Collection<Set<LandPatternPad>>>();
  }

  /**
   * @author George A. David
   */
  private List<java.awt.Shape> getFocusShapes(ReconstructionRegion inspectionRegion)
  {
    Assert.expect(inspectionRegion != null);
    Component component = inspectionRegion.getComponent();

    String key = getLandPatternKey(component.getLandPattern(), component.getDegreesRotationAfterAllRotations());
    if (_landPatternKeyToPadListMap.containsKey(key))
    {
      List<List<Pad>> padList = _landPatternKeyToPadListMap.get(key);
      List<Pad> componentPads = component.getPads();
      for (List<Pad> pads :  padList)
      {
        if (componentPads.size() == pads.size())
        {
          boolean areSimilar = false;
          for (int i = 0; i < pads.size(); ++i)
          {
            Pad pad = pads.get(i);
            Pad componentPad = componentPads.get(i);
            if (pad.getLandPatternPad() == componentPad.getLandPatternPad() &&
                pad.getSubtype().getInspectionFamilyEnum().equals(componentPad.getSubtype().getInspectionFamilyEnum()) &&
                pad.getSubtype().equals(componentPad.getSubtype()))
            {
              areSimilar = true;
            }
            else
            {
              areSimilar = false;
              break;
            }
          }

          if (areSimilar)
          {
            return _padsToFocusShapesMap.get(pads);
          }
        }
      }
    }
    return new LinkedList<java.awt.Shape>();
  }

  /**
   * @author George A. David
   */
  private void addSeparatedLandPatternPads(Component component, Collection<Collection<Set<LandPatternPad>>> landPatternPadsAndFocusRegions)
  {
    Assert.expect(component != null);
    Assert.expect(landPatternPadsAndFocusRegions != null);

    List<Pad> componentPads = component.getPads();
    _padsToSeparatedLandPatternPadsMap.put(componentPads, landPatternPadsAndFocusRegions);

    String key = getLandPatternKey(component.getLandPattern(), component.getDegreesRotationAfterAllRotations());

    if (_landPatternKeyToPadListMap.containsKey(key))
    {
      _landPatternKeyToPadListMap.get(key).add(componentPads);
    }
    else
    {
      List<List<Pad>> padLists = new LinkedList<List<Pad>>();
      padLists.add(componentPads);
      _landPatternKeyToPadListMap.put(key, padLists);
    }
  }

  /**
   * @author George A. David
   */
  private void addFocusShapes(ReconstructionRegion inspectionRegion, List<java.awt.Shape> landPatternPadsAndFocusRegions)
  {
    Assert.expect(inspectionRegion != null);
    Assert.expect(landPatternPadsAndFocusRegions != null);

    List<Pad> pads = new LinkedList<Pad>(inspectionRegion.getPads());
    _padsToFocusShapesMap.put(pads, landPatternPadsAndFocusRegions);

    Component component = inspectionRegion.getComponent();
    String key = getLandPatternKey(component.getLandPattern(), component.getDegreesRotationAfterAllRotations());

    if (_landPatternKeyToPadListMap.containsKey(key))     
    {
      _landPatternKeyToPadListMap.get(key).add(pads);
    }
    else
    {
      List<List<Pad>> padLists = new LinkedList<List<Pad>>();
      padLists.add(pads);
      _landPatternKeyToPadListMap.put(key, padLists);
    }
  }

  /**
   * Create inspection regions for the component
   * @author George A. David
   */
  private void createInspectionRegions(Component component)
  {
    Assert.expect(component != null);

    PanelSettings panelSettings = component.getBoard().getPanel().getPanelSettings();
    // ok, before we actually find a match, let's make sure the bounds of the component
    // does not straddle the long panel division line or is not fully contained in
    // the imageable area, otherwise then we cannot be guaranteed that
    // any previous divisions of this land pattern will work.
    Collection<Collection<Set<LandPatternPad>>> separatedLandPatternPads = null;
    boolean usesInconsistentPadSeparation = _componentTypesThatExtendBeyondImageableRegion.contains(component.getComponentType());
    if(usesInconsistentPadSeparation == false && panelSettings.isLongPanel())
    {
      Rectangle2D bounds = component.getShapeSurroundingPadsRelativeToPanelInNanoMeters().getBounds2D();
      if(panelSettings.isPanelBasedAlignment())
      {
        if (panelSettings.getRightSectionOfLongPanel().contains(bounds) == false && 
            panelSettings.getRightSectionOfLongPanel().intersects(bounds))
          usesInconsistentPadSeparation = true;
      }
      else
      {
        if (component.getBoard().getRightSectionOfLongBoard().contains(bounds) == false && 
            component.getBoard().getRightSectionOfLongBoard().intersects(bounds))
          usesInconsistentPadSeparation = true;
      }
    }

    if(usesInconsistentPadSeparation)
      separatedLandPatternPads = new LinkedList<Collection<Set<LandPatternPad>>>();
    else
      separatedLandPatternPads = getSeparatedLandPatternPads(component);

    Assert.expect(separatedLandPatternPads != null);

    if (separatedLandPatternPads.isEmpty())
    {
      Collection<List<Pad>> padLists = separatePadsBasedOnInspectionRegionRules(component);

      if(_printDebugInfo)
      {
        System.out.println("separated pad lists");
        Set<String> strings = new TreeSet<String>();
        for(List<Pad> pads : padLists)
        {
          Set<String> names = new TreeSet<String>();
          for(Pad pad : pads)
            names.add(pad.getName());
          String string = "";
          for(String name : names)
            string += name + " ";

          strings.add(string);
        }

        for(String string : strings)
          System.out.println(string);
      }

      // now create an inspection region for each pad list.
      for (List<Pad> pads : padLists)
        separatedLandPatternPads.add(getSeparatedLandPatternPads(pads));

      if(usesInconsistentPadSeparation == false)
        addSeparatedLandPatternPads(component, separatedLandPatternPads);
    }

    createInspectionRegions(component, separatedLandPatternPads);
    _printDebugInfo = false;
  }

  /**
   * @author George A. David
   */
  private Collection<Set<LandPatternPad>> getSeparatedLandPatternPads(List<Pad> pads)
  {
    Assert.expect(pads != null);

    List<LandPatternPad> landPatternPads = new LinkedList<LandPatternPad>();
    boolean areAllPadsTestable = true;
    for (Pad pad : pads)
    {
      landPatternPads.add(pad.getLandPatternPad());
      //areAllPadsTestable &= pad.getPadType().isTestable();
      areAllPadsTestable &= pad.isTestable();
    }

    InspectionFamilyEnum inspectionFamily = pads.get(0).getSubtype().getInspectionFamilyEnum();
    JointTypeEnum jointType = pads.get(0).getJointTypeEnum();
    double componentRotation = pads.get(0).getComponent().getDegreesRotationAfterAllRotations();
    boolean isHighMagnification = pads.get(0).getSubtype().getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH);
    Collection<Set<LandPatternPad>> separatedLandPatternPads = InspectionRegionGeneration.clusterLandPatternPads(landPatternPads, inspectionFamily, jointType, componentRotation, isHighMagnification);

    return separatedLandPatternPads;
  }

  /**
   * @author George A. David
   */
  public List<java.awt.Shape> createFocusShapes(Set<LandPatternPad> landPatternPads, InspectionFamilyEnum inspectionFamily, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(inspectionFamily != null);
    Assert.expect(jointTypeEnum != null);

    List<java.awt.Shape> focusShapes = new LinkedList<java.awt.Shape>();
    if (inspectionFamily.equals(InspectionFamilyEnum.CHIP))
      focusShapes.addAll(createFocusShapesForChips(landPatternPads));
    else if (inspectionFamily.equals(InspectionFamilyEnum.POLARIZED_CAP))
      focusShapes.addAll(createFocusShapesForPolarizedCap(landPatternPads));
    else if (inspectionFamily.equals(InspectionFamilyEnum.GULLWING))
      focusShapes.add(createFocusShapesForGullwingsOrConnector(landPatternPads));
    else if (inspectionFamily.equals(InspectionFamilyEnum.GRID_ARRAY))
      focusShapes.add(createFocusShapesForGridArraysOrThroughHoles(landPatternPads, _minPadAreaToTotalAreaRatioForGridArrayFocusRegions));
    else if (inspectionFamily.equals(InspectionFamilyEnum.THROUGHHOLE) ||
             inspectionFamily.equals(InspectionFamilyEnum.OVAL_THROUGHHOLE)) //Siew Yeng - XCR-3318 - Oval PTH
      focusShapes.add(createFocusShapesForGridArraysOrThroughHoles(landPatternPads, _minPadAreaToTotalAreaRatioForThrouhHoleFocusRegions));
    else if (inspectionFamily.equals(InspectionFamilyEnum.PRESSFIT))
      focusShapes.add(createFocusShapesForGridArraysOrThroughHoles(landPatternPads, _minPadAreaToTotalAreaRatioForThrouhHoleFocusRegions));
    else if (inspectionFamily.equals(InspectionFamilyEnum.ADVANCED_GULLWING))
      focusShapes.add(createFocusShapesForGullwingsOrConnector(landPatternPads));
    else if (inspectionFamily.equals(InspectionFamilyEnum.LARGE_PAD))
    {
      if (jointTypeEnum.equals(JointTypeEnum.LGA))        
        focusShapes.add(createFocusShapesForGridArraysOrThroughHoles(landPatternPads, _minPadAreaToTotalAreaRatioForGridArrayFocusRegions));
      else
        focusShapes.add(createFocusShapesForGullwingsOrConnector(landPatternPads));
    }
    else if (inspectionFamily.equals(InspectionFamilyEnum.CALIBRATION))
      focusShapes.add(createFocusShapesForCalibrationPads(landPatternPads));
    else if (inspectionFamily.equals(InspectionFamilyEnum.QUAD_FLAT_NO_LEAD))
      focusShapes.add(createFocusShapesForGullwingsOrConnector(landPatternPads));
    else if (inspectionFamily.equals(InspectionFamilyEnum.EXPOSED_PAD))
      focusShapes.add(createFocusShapesForGullwingsOrConnector(landPatternPads));
    else
      Assert.expect(false);

    return focusShapes;
  }

  /**
   * @author George A. David
   */
  public List<java.awt.Shape> createFocusShapes(Collection<Pad> pads, InspectionFamilyEnum inspectionFamily, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(pads != null);

    Set<LandPatternPad> landPatternPads = new HashSet<LandPatternPad>();
    for (Pad pad : pads)
    {
      landPatternPads.add(pad.getLandPatternPad());
    }

    return createFocusShapes(landPatternPads, inspectionFamily, jointTypeEnum);
  }

  /**
   * @author George A. David
   */
  private void addInspectionRegion(ReconstructionRegion inspectionRegion)
  {
    Assert.expect(inspectionRegion != null);
    Assert.expect(_testProgram != null);
    
    boolean isPanelBasedAlignment = _project.getPanel().getPanelSettings().isPanelBasedAlignment();
    
    SubtypeAdvanceSettings subtypeAdvanceSetting = inspectionRegion.getSubtypes().iterator().next().getSubtypeAdvanceSettings();
    // if the panel is divided, determine whether the regions are
    // on the left or right section of the panel, otherwise it's always on
    // the right.
    boolean gotIn = false;
    for(TestSubProgram subProgram : _testProgram.getAllTestSubProgramsIncludedNoInspectableTestSubPrograms())
    {
      if(subtypeAdvanceSetting.getMagnificationType().equals(subProgram.getMagnificationType()) &&
         subProgram.getImageableRegionInNanoMeters().contains(inspectionRegion.getBoundingRegionOfPadsInNanoMeters()))
      {
        if (isPanelBasedAlignment)
        {
          subProgram.addInspectionRegion(inspectionRegion);
          gotIn = true;
          break;
        }
        else
        {
          if (subProgram.getBoard().getName().equals(inspectionRegion.getComponent().getBoard().getName()))
          {
            subProgram.addInspectionRegion(inspectionRegion);
            gotIn = true;
            break;
          }
        }
      }
    }
//    System.out.println("region " + inspectionRegion.getComponent().getReferenceDesignator() + " has enter " + gotIn);
  }

  /**
   * @author George A. David
   */
  private void createInspectionRegions(Component component, Collection<Collection<Set<LandPatternPad>>> separatedLandPatternPads)
  {
    Assert.expect(component != null);
    Assert.expect(separatedLandPatternPads != null);

    List<Pad> componentPads = component.getPads();

    for (Collection<Set<LandPatternPad>> landPatternPadCollection : separatedLandPatternPads)
    {
      for (Set<LandPatternPad> landPatternPads : landPatternPadCollection)
      {
        List<Pad> pads = new LinkedList<Pad>();
        for (Pad pad : componentPads)
        {
          if (landPatternPads.contains(pad.getLandPatternPad()))
          {
            pads.add(pad);
            // ok, these pads are testable
//            pad.getPadTypeSettings().setTestable(true);
            pad.getPadSettings().setTestable(true);
          }
        }

        componentPads.removeAll(pads);

        if (pads.isEmpty())
          continue;

        ReconstructionRegion region = InspectionRegionGeneration.createInspectionRegion(pads);
        PanelRectangle regionRect = region.getRegionRectangleRelativeToPanelInNanoMeters();
        Assert.expect(regionRect.getWidth() <= ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters(), "Project: " + _project.getName() + " Region Too Wide: " + region.getComponent().getReferenceDesignator() + " " + MathUtil.convertNanoMetersToMils(regionRect.getWidth()));
        Assert.expect(regionRect.getHeight() <= ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters(), "Project: " + _project.getName() + " Region Too Long: " + region.getComponent().getReferenceDesignator() + " " + MathUtil.convertNanoMetersToMils(regionRect.getHeight()));
        addInspectionRegion(region);
      }
    }

    // any pads not used are untestable
    for(Pad pad : componentPads)
    {
//      if(pad.getPadTypeSettings().isTestable())
//      {
//        pad.getPadTypeSettings().setTestable(false);
////        if (UnitTest.unitTesting() == false)
////          System.out.println("Pad " + pad.getName() +
////                             " of Component " + pad.getComponent().getReferenceDesignator() +
////                             " of Land Pattern " + pad.getLandPatternPad().getLandPattern().getName() +
////                             " is untestable.");
//      }
      if (pad.getPadSettings().isTestable())
      {
        pad.getPadSettings().setTestable(false);
      }
    }
  }

  /**
   * @author George A. David
   * @authot Siew Yeng - remove 3rd and 4th slice
   */
  private List<java.awt.Shape> createFocusShapesForChips(Set<LandPatternPad> landPatternPads)
  {
    Assert.expect(landPatternPads != null);

    // for discreet components, we will create 4 focus groups with one slice each.
    // the first two slices are to test chips, first slice for clear chips, second slice for opaque chips
    // the last two slices are for Pcaps, pad slice and slug slice respectivel    // create the bounds of the pins
    Rectangle2D combinedRectRelativeToPanelInNanoMeters = null;
    for (LandPatternPad landPatternPad : landPatternPads)
    {
      if (combinedRectRelativeToPanelInNanoMeters == null)
        combinedRectRelativeToPanelInNanoMeters = landPatternPad.getShapeInNanoMeters().getBounds2D();
      else
        combinedRectRelativeToPanelInNanoMeters.add(landPatternPad.getShapeInNanoMeters().getBounds2D());
    }

    List<java.awt.Shape> focusRegions = new LinkedList<java.awt.Shape>();

    // first slice
    focusRegions.add(combinedRectRelativeToPanelInNanoMeters);

    // second slice
    focusRegions.add(combinedRectRelativeToPanelInNanoMeters);

    LandPatternPad landPatternPadOne = landPatternPads.iterator().next().getLandPattern().getLandPatternPadOne();
    // a sanity check
    Assert.expect(landPatternPads.contains(landPatternPadOne));

    java.awt.Shape padOneShape = landPatternPadOne.getShapeInNanoMeters();
    PanelRectangle padOneShapeBounds = new PanelRectangle(padOneShape);

    // first figure out the orientation of the component
    LandPatternPad landPatternPadTwo = null;
    for (LandPatternPad landPatternPad : landPatternPads)
    {
      if (landPatternPad != landPatternPadOne)
        landPatternPadTwo = landPatternPad;
    }
    Assert.expect(landPatternPadTwo != null);

    java.awt.Shape padTwoShape = landPatternPadTwo.getShapeInNanoMeters();
    PanelRectangle padTwoShapeBounds = new PanelRectangle(padTwoShape);

    // add 5 mils border to top, bottom, left and remove 12 mils from right.
    // find out where pad two is, above, left, right
    ComponentCoordinate padOneCoords = landPatternPadOne.getCenterCoordinateInNanoMeters();
    ComponentCoordinate padTwoCoords = landPatternPadTwo.getCenterCoordinateInNanoMeters();

    return focusRegions;
  }
  
  /**
   * @author George A. David
   */
  private List<java.awt.Shape> createFocusShapesForPolarizedCap(Set<LandPatternPad> landPatternPads)
  {
    Assert.expect(landPatternPads != null);

    // for discreet components, we will create 4 focus groups with one slice each.
    // the first two slices are to test chips, first slice for clear chips, second slice for opaque chips
    // the last two slices are for Pcaps, pad slice and slug slice respectivel    // create the bounds of the pins
    Rectangle2D combinedRectRelativeToPanelInNanoMeters = null;
    for (LandPatternPad landPatternPad : landPatternPads)
    {
      if (combinedRectRelativeToPanelInNanoMeters == null)
        combinedRectRelativeToPanelInNanoMeters = landPatternPad.getShapeInNanoMeters().getBounds2D();
      else
        combinedRectRelativeToPanelInNanoMeters.add(landPatternPad.getShapeInNanoMeters().getBounds2D());
    }

    // this is the focus region for the fourth slice
    Area slugArea = new Area((Rectangle2D)combinedRectRelativeToPanelInNanoMeters.clone());
    Rectangle2D slugRect = slugArea.getBounds2D();
    Rectangle2D focusRegionSlice4 = (Rectangle2D)combinedRectRelativeToPanelInNanoMeters.clone();

    List<java.awt.Shape> focusRegions = new LinkedList<java.awt.Shape>();

    // first slice
    focusRegions.add(combinedRectRelativeToPanelInNanoMeters);

    // second slice
    focusRegions.add(combinedRectRelativeToPanelInNanoMeters);

    LandPatternPad landPatternPadOne = landPatternPads.iterator().next().getLandPattern().getLandPatternPadOne();
    // a sanity check
    Assert.expect(landPatternPads.contains(landPatternPadOne));

    java.awt.Shape padOneShape = landPatternPadOne.getShapeInNanoMeters();
    PanelRectangle padOneShapeBounds = new PanelRectangle(padOneShape);

    // first figure out the orientation of the component
    LandPatternPad landPatternPadTwo = null;
    for (LandPatternPad landPatternPad : landPatternPads)
    {
      if (landPatternPad != landPatternPadOne)
        landPatternPadTwo = landPatternPad;
    }
    Assert.expect(landPatternPadTwo != null);

    java.awt.Shape padTwoShape = landPatternPadTwo.getShapeInNanoMeters();
    PanelRectangle padTwoShapeBounds = new PanelRectangle(padTwoShape);

    // add 5 mils border to top, bottom, left and remove 12 mils from right.
    // find out where pad two is, above, left, right
    ComponentCoordinate padOneCoords = landPatternPadOne.getCenterCoordinateInNanoMeters();
    ComponentCoordinate padTwoCoords = landPatternPadTwo.getCenterCoordinateInNanoMeters();

    // this is the focus region for the third slice
    Rectangle2D focusRegionSlice3 = padOneShape.getBounds2D();
    double slice3x = focusRegionSlice3.getMinX();
    double slice3y = focusRegionSlice3.getMinY();
    double slice3width = focusRegionSlice3.getWidth();
    double slice3length = focusRegionSlice3.getHeight();

    slugRect = slugArea.getBounds2D();
    slugArea.subtract(new Area(padTwoShape));
    slugRect = slugArea.getBounds2D();
    slugArea.subtract(new Area(padOneShape));
    slugRect = slugArea.getBounds2D();

    // in some corner cases, pin 1 and pin 2 are in the same exact
    // spot. in this case, the subtraction above will result in a bounds
    // x, y, width, and length at 0, 0, 0, and 0. In this case we will set
    // the focus region to be the pad shape.
    if(slugRect.getMinX() == 0 &&
       slugRect.getMinY() == 0 &&
       slugRect.getWidth() == 0 &&
       slugRect.getHeight() == 0)
      focusRegionSlice4 = padOneShape.getBounds2D();
    else
    {
      double widthPadding = slugRect.getWidth() * 0.10;
      double heightPadding = slugRect.getHeight() * 0.10;

      ComponentCoordinate resultCoords = padOneCoords.minus(padTwoCoords);
      double offset;
      if (Math.abs(resultCoords.getY()) > Math.abs(resultCoords.getX())) // the pads are aligned vertically
      {
        // For PCAPs only use the outer half of the focus region we'd normally choose
        // This is done since some PCAPs have a "stem" or "spindle" which extends from
        // the slug part way over pad 2. NOTE: this kludge seems to work ok for orthogonal
        // PCAPs, but probably will not work for arbitrary angle components.
        // TODO: fix this if we ever see arbitrary angle PCAPs
        if (resultCoords.getY() > 0)   // pad 1 above pad 2
          offset = 0.5 * slice3length;
        else
          offset = 0.0;
        focusRegionSlice3.setRect(slice3x,
                                  slice3y + offset,
                                  slice3width,
                                  0.5 * slice3length);

        focusRegionSlice4.setRect(slugRect.getMinX() - (0.3 * slugRect.getWidth()),
                                  slugRect.getMinY() + heightPadding,
                                  slugRect.getWidth() * 1.6,
                                  slugRect.getHeight() - (2 * heightPadding));
      }
      else // the pads are aligned horizontally, they could be at 45 degrees exactly, so it's a crap shoot which way is right, we choose horizontally.
      {
        if (resultCoords.getX() > 0)   // pad 1 right of pad 2
          offset = 0.5 * slice3width;
        else
          offset = 0.0;
        focusRegionSlice3.setRect(slice3x + offset,
                                  slice3y,
                                  slice3width / 2.0,
                                  slice3length);

        focusRegionSlice4.setRect(slugRect.getMinX() + widthPadding,
                                  slugRect.getMinY() - (0.3 * slugRect.getHeight()),
                                  slugRect.getWidth() - (2 * widthPadding),
                                  slugRect.getHeight() * 1.6);
      }
    }

    // third slice
    if(focusRegionSlice3.getWidth() < FocusRegion.getMinFocusRegionSizeInNanoMeters())
    {
      focusRegionSlice3.setRect(focusRegionSlice3.getCenterX() - FocusRegion.getMinFocusRegionSizeInNanoMeters(),
                                focusRegionSlice3.getMinY(),
                                FocusRegion.getMinFocusRegionSizeInNanoMeters(),
                                focusRegionSlice3.getHeight());
    }

    if(focusRegionSlice3.getHeight() < FocusRegion.getMinFocusRegionSizeInNanoMeters())
    {
      focusRegionSlice3.setRect(focusRegionSlice3.getMinX(),
                                focusRegionSlice3.getCenterY() - FocusRegion.getMinFocusRegionSizeInNanoMeters(),
                                focusRegionSlice3.getWidth(),
                                FocusRegion.getMinFocusRegionSizeInNanoMeters());
    }
    focusRegions.add(focusRegionSlice3);

    // fourth slice
    if(focusRegionSlice4.getWidth() < FocusRegion.getMinFocusRegionSizeInNanoMeters())
    {
      focusRegionSlice4.setRect(focusRegionSlice4.getCenterX() - FocusRegion.getMinFocusRegionSizeInNanoMeters(),
                                focusRegionSlice4.getMinY(),
                                FocusRegion.getMinFocusRegionSizeInNanoMeters(),
                                focusRegionSlice4.getHeight());
    }

    if(focusRegionSlice4.getHeight() < FocusRegion.getMinFocusRegionSizeInNanoMeters())
    {
      focusRegionSlice4.setRect(focusRegionSlice4.getMinX(),
                                focusRegionSlice4.getCenterY() - FocusRegion.getMinFocusRegionSizeInNanoMeters(),
                                focusRegionSlice4.getWidth(),
                                FocusRegion.getMinFocusRegionSizeInNanoMeters());
    }
    focusRegions.add(focusRegionSlice4);

    return focusRegions;
  }

  /**
   * @author George A. David
   */
  private Rectangle2D getCombinedRectangle(Collection<LandPatternPad> pads)
  {
    Assert.expect(pads != null);

    Rectangle2D combinedRect = null;
    for (LandPatternPad landPatternPad : pads)
    {
      if (combinedRect == null)
        combinedRect = ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundLandPatternPad(landPatternPad).getRectangle2D();
      else
        combinedRect.add(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundLandPatternPad(landPatternPad).getRectangle2D());
    }

    Assert.expect(combinedRect != null);
    return combinedRect;
  }

  /**
   * @author George A. David
   */
  private java.awt.Shape createFocusShapesForGullwingsOrConnector(Set<LandPatternPad> landPatternPadSet)
  {
    Assert.expect(landPatternPadSet != null);

    // gullwings have one focus group and one slice.
    // the focus region will consist of the 3 center most pads

    // find the pad type that is closest to the center of the bounds
    Rectangle2D combinedRect = getCombinedRectangle(landPatternPadSet);
    List<LandPatternPad> landPatternPads = new LinkedList<LandPatternPad>(landPatternPadSet);
    Collections.sort(landPatternPads, new LandPatternPadDistanceComparator(combinedRect.getCenterX(), combinedRect.getCenterY()));

    combinedRect = null;
    double areaOfPadsInMilsSquared = 0;
    int maxPadsInFocusRegion = Math.min(_MAX_GULLWING_PADS_IN_FOCUS_REGION, landPatternPadSet.size());
    for (int i = 0; i < maxPadsInFocusRegion; i++)
    {
      java.awt.Shape shape = landPatternPads.get(i).getShapeInNanoMeters();
      Rectangle2D shapeBounds = shape.getBounds2D();
      if (combinedRect == null)
        combinedRect = shape.getBounds2D();
      else
        combinedRect.add(shapeBounds);

      areaOfPadsInMilsSquared += MathUtil.convertNanoMetersToMils(shapeBounds.getWidth()) *
      MathUtil.convertNanoMetersToMils(shapeBounds.getHeight());
    }
    Assert.expect(combinedRect != null);

    // ok, check that the area of pins to total area ratio is acceptable
    double combinedRectAreaInMilsSquared = MathUtil.convertNanoMetersToMils(combinedRect.getWidth()) *
    MathUtil.convertNanoMetersToMils(combinedRect.getHeight());
    double pinsAreaToTotalAreaRatio = areaOfPadsInMilsSquared / combinedRectAreaInMilsSquared;
    if (pinsAreaToTotalAreaRatio < _minPadAreaToTotalAreaRatioForGullwingFocusRegions)
    {
      // ok, there is too much space, and not enough pads. start with the first pad
      // then keep adding pads closest to it until you pass the minimum ratio or you
      // get the maximum pads allowed
      Rectangle2D shapeBounds = landPatternPads.get(0).getShapeInNanoMeters().getBounds2D();
      Collections.sort(landPatternPads, new LandPatternPadDistanceComparator(shapeBounds.getCenterX(), shapeBounds.getCenterY()));
      combinedRect = shapeBounds;
      Rectangle2D combinedRectProposal = shapeBounds;
      areaOfPadsInMilsSquared = MathUtil.convertNanoMetersToMils(shapeBounds.getWidth()) *
      MathUtil.convertNanoMetersToMils(shapeBounds.getHeight());
      for (int i = 1; i < maxPadsInFocusRegion; ++i)
      {
        shapeBounds = landPatternPads.get(i).getShapeInNanoMeters().getBounds2D();
        areaOfPadsInMilsSquared += MathUtil.convertNanoMetersToMils(shapeBounds.getWidth()) *
        MathUtil.convertNanoMetersToMils(shapeBounds.getHeight());
        combinedRectProposal.add(shapeBounds);
        combinedRectAreaInMilsSquared = MathUtil.convertNanoMetersToMils(combinedRect.getWidth()) *
        MathUtil.convertNanoMetersToMils(combinedRect.getHeight());
        pinsAreaToTotalAreaRatio = areaOfPadsInMilsSquared / combinedRectAreaInMilsSquared;
        if (pinsAreaToTotalAreaRatio > _minPadAreaToTotalAreaRatioForGullwingFocusRegions)
          combinedRect.add(shapeBounds);
        else
          break;
      }
    }

    return combinedRect;
  }

  /**
   * @author George A. David
   */
  private java.awt.Shape createFocusShapesForCalibrationPads(Set<LandPatternPad> landPatternPadSet)
  {
    Assert.expect(landPatternPadSet != null);

    Rectangle2D combinedRect = getCombinedRectangle(landPatternPadSet);

    combinedRect.setRect(combinedRect.getCenterX() - FocusRegion.getMaxFocusRegionSizeInNanoMeters() / 2.0,
                         combinedRect.getCenterY() - FocusRegion.getMaxFocusRegionSizeInNanoMeters() / 2.0,
                         FocusRegion.getMaxFocusRegionSizeInNanoMeters(),
                         FocusRegion.getMaxFocusRegionSizeInNanoMeters());
    return combinedRect;
  }

  /**
   * @author George A. David
   */
  private java.awt.Shape createFocusShapesForGridArraysOrThroughHoles(Set<LandPatternPad> landPatternPadSet, double minimumPadAreaToTotalAreaRatio)
  {
    Assert.expect(landPatternPadSet != null);
    Assert.expect(minimumPadAreaToTotalAreaRatio > 0);

    // grid arrays have one focus group and 3 slices.
    // the focus region will start at the center pad and extend out 1.5 * pitch
    // find the center most pad
    Rectangle2D combinedRect = getCombinedRectangle(landPatternPadSet);
    List<LandPatternPad> landPatternPads = new LinkedList<LandPatternPad>(landPatternPadSet);
    Collections.sort(landPatternPads, new LandPatternPadDistanceComparator(combinedRect.getCenterX(), combinedRect.getCenterY()));

    LandPatternPad firstLandPatternPad = landPatternPads.get(0);
    Rectangle2D proposedFocusRegion = firstLandPatternPad.getShapeInNanoMeters().getBounds2D();
    int pitchInNanoMeters = firstLandPatternPad.getPitchInNanoMeters();
    if (pitchInNanoMeters == 0)
      pitchInNanoMeters = firstLandPatternPad.getLongestPadDimensionInNanoMeters();
    int borderInNanoMeters = (int)Math.rint(pitchInNanoMeters * _GRID_ARRAY_OR_THROUGH_HOLE_PITCH_MULTIPLIER);
    int halfPitchInNanoMeters = (int)Math.rint(pitchInNanoMeters / 2.0);
    proposedFocusRegion.setRect(proposedFocusRegion.getCenterX() - borderInNanoMeters,
                                proposedFocusRegion.getCenterY() - borderInNanoMeters,
                                2 * borderInNanoMeters,
                                2 * borderInNanoMeters);

    Pair<Double, Rectangle2D> padAreaInMilsSquaredAndBoundingRegion = getLandPatternPadAreaInMilsSquaredAndBoundingRegionForGridArraysOrThroughHoles(landPatternPads, proposedFocusRegion);
    double padAreaInMilsSquared = padAreaInMilsSquaredAndBoundingRegion.getFirst();
    proposedFocusRegion = padAreaInMilsSquaredAndBoundingRegion.getSecond();
    double totalAreaInMilsSquared = MathUtil.convertNanoMetersToMils(proposedFocusRegion.getWidth()) *
                                    MathUtil.convertNanoMetersToMils(proposedFocusRegion.getHeight());

    // ok, check that the area of pins to total area ratio is acceptable
    double padsAreaToTotalAreaRatio = padAreaInMilsSquared / totalAreaInMilsSquared;
    if (padsAreaToTotalAreaRatio < minimumPadAreaToTotalAreaRatio)
    {
      // ok, we have too much space, we'll need to do it the hard way! :(
      // we will create 16 different regions and choose the largest one that
      // meets the minimum ratio
      List<Rectangle2D> proposedFocusRegions = new LinkedList<Rectangle2D>();
      Rectangle2D centerPadBounds = landPatternPads.get(0).getShapeInNanoMeters().getBounds2D();

      // we have 9 regions to use and we can make 16 combinations
      //  *  *  *
      //  *  *  *
      //  *  *  *
      // both dimensions of the 9 regions are 1.5 * pitch in nanometers which I call borderInNanoMeters


      // 1) just the center
      //  *  *  *
      //  *  X  *
      //  *  *  *
      proposedFocusRegion = new Rectangle2D.Double(centerPadBounds.getCenterX() -
                                                   halfPitchInNanoMeters,
                                                   centerPadBounds.getCenterY() - halfPitchInNanoMeters,
                                                   pitchInNanoMeters,
                                                   pitchInNanoMeters);
      proposedFocusRegions.add(proposedFocusRegion);

      // 2) the center plus the left
      //  *  *  *
      //  X  X  *
      //  *  *  *
      proposedFocusRegion = new Rectangle2D.Double(centerPadBounds.getCenterX() -
                                                   borderInNanoMeters,
                                                   centerPadBounds.getCenterY() - halfPitchInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters,
                                                   pitchInNanoMeters);
      proposedFocusRegions.add(proposedFocusRegion);

      // 3) the center plus the right
      //  *  *  *
      //  *  X  X
      //  *  *  *
      proposedFocusRegion = new Rectangle2D.Double(centerPadBounds.getCenterX() -
                                                   halfPitchInNanoMeters,
                                                   centerPadBounds.getCenterY() - halfPitchInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters,
                                                   pitchInNanoMeters);
      proposedFocusRegions.add(proposedFocusRegion);

      // 4) the center plus the right and left
      //  *  *  *
      //  X  X  X
      //  *  *  *
      proposedFocusRegion = new Rectangle2D.Double(centerPadBounds.getCenterX() -
                                                   borderInNanoMeters,
                                                   centerPadBounds.getCenterY() - halfPitchInNanoMeters,
                                                   2 * borderInNanoMeters,
                                                   pitchInNanoMeters);
      proposedFocusRegions.add(proposedFocusRegion);

      // 5) the center plus upper
      //  *  X  *
      //  *  X  *
      //  *  *  *
      proposedFocusRegion = new Rectangle2D.Double(centerPadBounds.getCenterX() -
                                                   halfPitchInNanoMeters,
                                                   centerPadBounds.getCenterY() - halfPitchInNanoMeters,
                                                   pitchInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters);
      proposedFocusRegions.add(proposedFocusRegion);

      // 6) the center plus lower
      //  *  *  *
      //  *  X  *
      //  *  X  *
      proposedFocusRegion = new Rectangle2D.Double(centerPadBounds.getCenterX() -
                                                   halfPitchInNanoMeters,
                                                   centerPadBounds.getCenterY() - borderInNanoMeters,
                                                   pitchInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters);
      proposedFocusRegions.add(proposedFocusRegion);

      // 7) the center plus the upper and lower
      //  *  X  *
      //  *  X  *
      //  *  X  *
      proposedFocusRegion = new Rectangle2D.Double(centerPadBounds.getCenterX() -
                                                   halfPitchInNanoMeters,
                                                   centerPadBounds.getCenterY() - borderInNanoMeters,
                                                   pitchInNanoMeters,
                                                   2 * borderInNanoMeters);
      proposedFocusRegions.add(proposedFocusRegion);

      // 8) center , left , upper , upper left
      //  X  X  *
      //  X  X  *
      //  *  *  *
      proposedFocusRegion = new Rectangle2D.Double(centerPadBounds.getCenterX() -
                                                   borderInNanoMeters,
                                                   centerPadBounds.getCenterY() - halfPitchInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters);
      proposedFocusRegions.add(proposedFocusRegion);

      // 9) center , left , lower, lower left
      //  *  *  *
      //  X  X  *
      //  X  X  *
      proposedFocusRegion = new Rectangle2D.Double(centerPadBounds.getCenterX() -
                                                   borderInNanoMeters,
                                                   centerPadBounds.getCenterY() - borderInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters);
      proposedFocusRegions.add(proposedFocusRegion);

      // 10) center, left, upper, upper left, lower left, lower
      //  X  X  *
      //  X  X  *
      //  X  X  *
      proposedFocusRegion = new Rectangle2D.Double(centerPadBounds.getCenterX() -
                                                   borderInNanoMeters,
                                                   centerPadBounds.getCenterY() - borderInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters,
                                                   2 * borderInNanoMeters);
      proposedFocusRegions.add(proposedFocusRegion);

      // 11) center, right, upper, upper right
      //  *  X  X
      //  *  X  X
      //  *  *  *
      proposedFocusRegion = new Rectangle2D.Double(centerPadBounds.getCenterX() -
                                                   halfPitchInNanoMeters,
                                                   centerPadBounds.getCenterY() - halfPitchInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters);
      proposedFocusRegions.add(proposedFocusRegion);

      // 12) center, right, lower, lower right
      //  *  *  *
      //  *  X  X
      //  *  X  X
      proposedFocusRegion = new Rectangle2D.Double(centerPadBounds.getCenterX() -
                                                   halfPitchInNanoMeters,
                                                   centerPadBounds.getCenterY() - borderInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters);
      proposedFocusRegions.add(proposedFocusRegion);

      // 13) center, right, lower, lower right, upper right, uppper
      //  *  X  X
      //  *  X  X
      //  *  X  X
      proposedFocusRegion = new Rectangle2D.Double(centerPadBounds.getCenterX() -
                                                   halfPitchInNanoMeters,
                                                   centerPadBounds.getCenterY() - borderInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters,
                                                   2 * borderInNanoMeters);
      proposedFocusRegions.add(proposedFocusRegion);

      // 14) center, left, upper, upper left, upper right, right
      //  X  X  X
      //  X  X  X
      //  *  *  *
      proposedFocusRegion = new Rectangle2D.Double(centerPadBounds.getCenterX() -
                                                   borderInNanoMeters,
                                                   centerPadBounds.getCenterY() - halfPitchInNanoMeters,
                                                   2 * borderInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters);
      proposedFocusRegions.add(proposedFocusRegion);

      // 15) center, left, lower, lower left, lower right, right
      //  *  *  *
      //  X  X  X
      //  X  X  X
      proposedFocusRegion = new Rectangle2D.Double(centerPadBounds.getCenterX() -
                                                   borderInNanoMeters,
                                                   centerPadBounds.getCenterY() - borderInNanoMeters,
                                                   2 * borderInNanoMeters,
                                                   borderInNanoMeters + halfPitchInNanoMeters);
      proposedFocusRegions.add(proposedFocusRegion);

      // to be explicit, the final 16th combination is all the regions
      // center, left, right, upper, lower, upper right, upper left, lower right, lower left
      //  X  X  X
      //  X  X  X
      //  X  X  X
      // this has already been attempted and it didn't meet the min threshold
      // which is why we are in this if block, so no need to try it again since
      // we know it will not work! :)

      proposedFocusRegion = null;
      double largestPadAreaToTotalAreaRatio = 0.0;
      double largestAreaInMilsSquared = 0.0;
      boolean hasMinimumSpecBeenMet = false;
      for (Rectangle2D focusRegion : proposedFocusRegions)
      {
        padAreaInMilsSquaredAndBoundingRegion = getLandPatternPadAreaInMilsSquaredAndBoundingRegionForGridArraysOrThroughHoles(landPatternPads, focusRegion);
        padAreaInMilsSquared = padAreaInMilsSquaredAndBoundingRegion.getFirst();
        focusRegion = padAreaInMilsSquaredAndBoundingRegion.getSecond();

        totalAreaInMilsSquared = MathUtil.convertNanoMetersToMils(focusRegion.getWidth()) *
                                 MathUtil.convertNanoMetersToMils(focusRegion.getHeight());

        padsAreaToTotalAreaRatio = padAreaInMilsSquared / totalAreaInMilsSquared;
        if (padsAreaToTotalAreaRatio > minimumPadAreaToTotalAreaRatio)
          hasMinimumSpecBeenMet = true;

        if (hasMinimumSpecBeenMet)
        {
          if (totalAreaInMilsSquared > largestAreaInMilsSquared)
          {
            largestAreaInMilsSquared = totalAreaInMilsSquared;
            proposedFocusRegion = focusRegion;
          }
        }
        else
        {
          // if we haven't met the spec, then pick the area that has the largest ratio
          if (padsAreaToTotalAreaRatio > largestPadAreaToTotalAreaRatio)
          {
            largestPadAreaToTotalAreaRatio = padsAreaToTotalAreaRatio;
            proposedFocusRegion = focusRegion;
          }
        }
      }
      Assert.expect(proposedFocusRegion != null);
    }

    return proposedFocusRegion;
  }

  /**
   * @author George A. David
   */
  private void createFocusGroups()
  {
//    _rightSurfaceModelingReconstructionRegions.clear();
//    _leftSurfaceModelingReconstructionRegions.clear();
    _landPatternKeyToPadListMap.clear();
    _padsToFocusShapesMap.clear();
    
    // Chnee Khang Wah, 2012-02-02, 2.5D HIP development
    //_isGenerateMultiAngleImagesEnabled = RepairImagesConfigManager.getInstance().isGenerateMultiAngleImagesEnabled();  
    // Bee Hoon, Recipe Settings
    _isGenerateMultiAngleImagesEnabled = _project.isGenerateMultiAngleImage();
    _focusRegionComponentResetList.clear();
    
    for (TestSubProgram subProgram : _testProgram.getAllTestSubPrograms())
    {
      Collection<ReconstructionRegion> inspectionRegions = subProgram.getAllInspectionRegions();
      List<ReconstructionRegion> untestableRegions = new LinkedList<ReconstructionRegion>();

      for (ReconstructionRegion inspectionRegion : subProgram.getAllInspectionRegions())
      {
        inspectionRegion.clearFocusGroups();
        List<java.awt.Shape> focusShapes = null;

        if(subProgram.getImageableRegionInNanoMeters().containsShape(inspectionRegion.getComponent().getShapeRelativeToPanelInNanoMeters()) == false)
          _componentTypesThatExtendBeyondImageableRegion.add(inspectionRegion.getComponent().getComponentType());

        if(_componentTypesThatExtendBeyondImageableRegion.contains(inspectionRegion.getComponent().getComponentType()))
          focusShapes = new LinkedList<java.awt.Shape>();
        else
          focusShapes = getFocusShapes(inspectionRegion);
        if (focusShapes.isEmpty())
        {
          focusShapes = createFocusShapes(inspectionRegion.getPads(), inspectionRegion.getInspectionFamilyEnum(), inspectionRegion.getJointTypeEnum());
          // check to make sure the focus shapes are biggger than the minumum, if not, then
          // the pads in the inspection regions cannot be tested.
          boolean isTestable = true;
          for(java.awt.Shape shape : focusShapes)
          {
            Rectangle2D bounds = shape.getBounds2D();
            if(bounds.getWidth() < FocusRegion.getMinFocusRegionSizeInNanoMeters() ||
               bounds.getHeight() < FocusRegion.getMinFocusRegionSizeInNanoMeters())
            {
              String message = "The component " + inspectionRegion.getComponent().getReferenceDesignator() +
                               " of land pattern " + inspectionRegion.getComponent().getLandPattern().getName() +
                               " has an inspection region with a focus region that is too small (w: " + bounds.getWidth() + ", h: " + bounds.getHeight() + ")." +
                               " The minimum focus dimension is " + FocusRegion.getMinFocusRegionSizeInNanoMeters() + ". The following pads will be marked as untestable: ";
              for(Pad pad : inspectionRegion.getPads())
              {
//                pad.getPadTypeSettings().setTestable(false);
//                pad.getPadTypeSettings().setUntestableReason(StringLocalizer.keyToString(new LocalizedString("PAD_UNTESTABLE_FOCUS_REGION_TOO_SMALL_KEY",
//                                                             new Object[]{bounds.getWidth(), bounds.getHeight(), FocusRegion.getMinFocusRegionSizeInNanoMeters()})));
                pad.getPadSettings().setTestable(false);
                pad.getPadSettings().setUntestableReason(StringLocalizer.keyToString(new LocalizedString("PAD_UNTESTABLE_FOCUS_REGION_TOO_SMALL_KEY",
                                                             new Object[]{bounds.getWidth(), bounds.getHeight(), FocusRegion.getMinFocusRegionSizeInNanoMeters()})));
                message += pad.getName() + " ";
              }
//              if(UnitTest.unitTesting() == false)
//                System.out.println(message);
              isTestable = false;
              untestableRegions.add(inspectionRegion);
              break;
            }
          }
          if(isTestable == false)
            continue;

          if(_componentTypesThatExtendBeyondImageableRegion.contains(inspectionRegion.getComponent().getComponentType()) == false)
            addFocusShapes(inspectionRegion, focusShapes);
        }
        createFocusGroups(inspectionRegion, focusShapes);
        
        //Kee Chin Seong, Focus Region will be doing offset at here. as the focus region has been created.
        // now , check the focus region whether they need to be offset or not.
        if(inspectionRegion.getComponent().isUseCustomFocusRegion() == true)
        {
          if(_project.getComponentToFocusRegionMap().containsKey(inspectionRegion.getComponent()))
          {
            String regionKey = inspectionRegion.getBoundingRegionOfPadsInNanoMeters().getCenterX() + "_" +
                               inspectionRegion.getBoundingRegionOfPadsInNanoMeters().getCenterY() + "_" +
                               inspectionRegion.getBoundingRegionOfPadsInNanoMeters().getWidth() + "_" +
                               inspectionRegion.getBoundingRegionOfPadsInNanoMeters().getHeight();
            
             //bee-hoon.goh - check whether the region key exists.
             if (_project.getComponentToFocusRegionMap().get(inspectionRegion.getComponent()).containsKey(regionKey))
             {
               FocusRegion fcRegion = _project.getComponentToFocusRegionMap().get(inspectionRegion.getComponent()).get(regionKey).get(0);
               for(FocusGroup fg : inspectionRegion.getFocusGroups())
               {
                  fg.getFocusSearchParameters().getFocusRegion().setRectangleRelativeToPanelInNanoMeters(new PanelRectangle((int)fcRegion.getRectangleRelativeToPanelInNanometers().getRectangle2D().getX(),
                                                                                                                            (int)fcRegion.getRectangleRelativeToPanelInNanometers().getRectangle2D().getY(),
                                                                                                                            (int)fcRegion.getRectangleRelativeToPanelInNanometers().getRectangle2D().getWidth(),
                                                                                                                            (int)fcRegion.getRectangleRelativeToPanelInNanometers().getRectangle2D().getHeight()));
                }
              }
              else
              {
                if(_focusRegionComponentResetList.contains(inspectionRegion.getComponent().getReferenceDesignator()) == false) 
                  _focusRegionComponentResetList.add(inspectionRegion.getComponent().getReferenceDesignator());   
                // should prompt out message dialog in future (no message dialog due to gui layer cannot in business layer.)
                System.out.println("Custom Focus Region invalid for component: " + inspectionRegion.getComponent().getReferenceDesignator());
                _project.removeComponentFocusRegionInMap(inspectionRegion.getComponent());
                inspectionRegion.getComponent().setUseCustomFocusRegion(false);
              }
            }
          }
//        if (inspectionRegion.hasSurfaceModelingSlice())
//          if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
//            _rightSurfaceModelingReconstructionRegions.add(inspectionRegion);
//          else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
//            _leftSurfaceModelingReconstructionRegions.add(inspectionRegion);
//          else
//            Assert.expect(false);
      }

      if(untestableRegions.isEmpty() == false)
      {
        inspectionRegions.removeAll(untestableRegions);
        subProgram.setInspectionRegions(inspectionRegions);
      }
    }

    //ok, lets update the data in panel settings and remove any unfocused regions that do not exist.
    PanelSettings panelSettings = _project.getPanel().getPanelSettings();

    Set<String> unfocusedRegionNames = panelSettings.getUnfocusedRegionSlicesToBeFixed().keySet();
    if (unfocusedRegionNames.isEmpty() == false)
    {
      unfocusedRegionNames.removeAll(_testProgram.getInspectionRegionNames());
      for(String name : unfocusedRegionNames)
        panelSettings.removeUnfocusedRegionSliceToBeFixed(name);
    }

    unfocusedRegionNames = panelSettings.getUnfocusedRegionsNamesToRetest();
    if (unfocusedRegionNames.isEmpty() == false)
    {
      unfocusedRegionNames.removeAll(_testProgram.getInspectionRegionNames());
      for(String name : unfocusedRegionNames)
        panelSettings.removeUnfocusedRegionNameToRetest(name);
    }

    Map<String, Set<Pair<SliceNameEnum, Integer>>> regionSlicesToZoffset = panelSettings.getUnfocusedRegionSlicesToZoffsetInNanos();
    unfocusedRegionNames = regionSlicesToZoffset.keySet();
    if (unfocusedRegionNames.isEmpty() == false)
    {
      unfocusedRegionNames.removeAll(_testProgram.getInspectionRegionNames());
      for(String name : unfocusedRegionNames)
        panelSettings.removeUnfocusedRegionSlicesToZoffsetInNanos(name);
    }
  }

  /**
   * @author George A. David
   */
  private void setUpSurfaceModelingNeighbors()
  {
    for (TestSubProgram subProgram : _testProgram.getAllTestSubPrograms())
    {
      Collection<ReconstructionRegion> neighbors = null;
      Collection<ReconstructionRegion> inspectedNeighbors;
      Map<DoubleCoordinate, ReconstructionRegion>
          coordToReconstructionRegionMap = new HashMap<DoubleCoordinate, ReconstructionRegion>();
      Map<ReconstructionRegion, DoubleCoordinate>
          reconstructionRegionToCoordMap = new HashMap<ReconstructionRegion, DoubleCoordinate>();

      SurfaceModelingGenerationUtil modelingUtil = new SurfaceModelingGenerationUtil();

      if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
        modelingUtil.setReconstructionRegions(subProgram, new LinkedList<ReconstructionRegion>(subProgram.getSurfaceModelingReconstructionRegions()));
      else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
        modelingUtil.setReconstructionRegions(subProgram, new LinkedList<ReconstructionRegion>(subProgram.getSurfaceModelingReconstructionRegions()));
      else
        Assert.expect(false);

      for (ReconstructionRegion inspectionRegion : subProgram.getAllInspectionRegions())
      {
        Collection<ReconstructionRegion> sortedRegions = modelingUtil.getSurroundingSortedReconstructionRegions(inspectionRegion);
        /***** JMH: 7/31/06 try sending all neighbors down to IRPs **************
        if (sortedRegions.size() > 5)
        {
          List<DoubleCoordinate> surfaceModelingCoords = new LinkedList<DoubleCoordinate>();
          for (ReconstructionRegion surfaceModelingRegion : sortedRegions)
          {
            DoubleCoordinate coord = null;
            if (reconstructionRegionToCoordMap.containsKey(surfaceModelingRegion) == false)
            {
              PanelRectangle regionRect = surfaceModelingRegion.getSurfaceModelingFocusRegion().getRectangleRelativeToPanelInNanometers();
              coord = new DoubleCoordinate(regionRect.getCenterX(), regionRect.getCenterY());
              coordToReconstructionRegionMap.put(coord, surfaceModelingRegion);
              reconstructionRegionToCoordMap.put(surfaceModelingRegion, coord);
            }
            else
              coord = reconstructionRegionToCoordMap.get(surfaceModelingRegion);

            Assert.expect(coord != null);

            surfaceModelingCoords.add(coord);

          }

          List<DoubleCoordinate> convexHullCoords = null;
          if (MathUtil.isFuzzyColinear(surfaceModelingCoords))
          {
            // do no more
            // add all the groups
            convexHullCoords = surfaceModelingCoords;
          }
          else
          {
            PanelRectangle regionRect = inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
            DoubleCoordinate referenceCoord = new DoubleCoordinate(regionRect.getCenterX(), regionRect.getCenterY());
            convexHullCoords = MathUtil.getSmallestConvexHull(surfaceModelingCoords, referenceCoord);
          }

          Assert.expect(convexHullCoords != null);
          neighbors = new LinkedList<ReconstructionRegion>();
          for (DoubleCoordinate convexHullCoord : convexHullCoords)
          {
            ReconstructionRegion neighbor = coordToReconstructionRegionMap.get(convexHullCoord);
            neighbors.add(neighbor);
          }
        }
        else
        ****************************************************/
	
        // use them all
        neighbors = sortedRegions;
        inspectedNeighbors = getFilteredNeighborInspectionRegions(sortedRegions);
        
        Assert.expect(neighbors != null);
        Assert.expect(inspectedNeighbors != null);
        for (FocusGroup group : inspectionRegion.getFocusGroups())
        {
	    //XCR-2851 - Ee Jun Jiang - VirtualLiveMemoryLeak
	    group.clearNeighbors();
            // CR1085 fix by LeeHerng - If we turn on Auto Predictive Slice Height, we will add in the neighbors region regardless of No Load, No Test.
            if (inspectionRegion.useAutoPredictiveSliceHeight())
                group.addNeighbors(neighbors);
            else
                group.addNeighbors(inspectedNeighbors);
        }

        // CR1085 fix by LeeHerng - If we turn on Auto Predictive Slice Height, we need to set the parent reconstruction region to each neighbor regions.
        if (inspectionRegion.useAutoPredictiveSliceHeight())
        {
          for (ReconstructionRegion region : neighbors)
          {
            if (Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
            {
              System.out.println("generateTestProgramDebug - Neighbor region " + region.getRegionId() + " (parent region " + inspectionRegion.getRegionId() + ") has parent region set to use Auto Predictive Slice Height.");
            }
            region.setParentRegionUseAutoPredictiveSliceHeight(true);
          }
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  private void createFocusGroups(ReconstructionRegion reconstructionRegion, List<java.awt.Shape> focusRegions)
  {
    Assert.expect(reconstructionRegion != null);

    // ok, now transform all the panel rectangles to take into account the component's final location and rotation
    Component component = reconstructionRegion.getComponent();
    AffineTransform transform = new AffineTransform();
    component.preConcatenateShapeTransform(transform);

    boolean add6milBorder = false;
    InspectionFamilyEnum inspectionFamilyEnum = reconstructionRegion.getInspectionFamilyEnum();
    if (inspectionFamilyEnum.equals(InspectionFamilyEnum.CHIP) ||
        inspectionFamilyEnum.equals(InspectionFamilyEnum.POLARIZED_CAP) ||
        inspectionFamilyEnum.equals(InspectionFamilyEnum.GULLWING) ||
        inspectionFamilyEnum.equals(InspectionFamilyEnum.ADVANCED_GULLWING) ||
        inspectionFamilyEnum.equals(InspectionFamilyEnum.LARGE_PAD) ||
        inspectionFamilyEnum.equals(InspectionFamilyEnum.QUAD_FLAT_NO_LEAD) ||
        inspectionFamilyEnum.equals(InspectionFamilyEnum.EXPOSED_PAD) ||
        inspectionFamilyEnum.equals(InspectionFamilyEnum.CALIBRATION))
    {
      add6milBorder = true;
    }
    else if (inspectionFamilyEnum.equals(InspectionFamilyEnum.GRID_ARRAY) ||
             inspectionFamilyEnum.equals(InspectionFamilyEnum.THROUGHHOLE) ||
             inspectionFamilyEnum.equals(InspectionFamilyEnum.PRESSFIT) ||
             inspectionFamilyEnum.equals(InspectionFamilyEnum.OVAL_THROUGHHOLE))//Siew Yeng - XCR-3318 - Oval PTH
    {
      add6milBorder = false;
    }
    else
    {
      Assert.expect(false);
    }

    Rectangle2D maxRect = new Rectangle2D.Double(0, 0, 0, 0);
    List<java.awt.Shape> focusShapes = new LinkedList<java.awt.Shape>();
    for (java.awt.Shape focusRegion : focusRegions)
    {
      java.awt.Shape focusShape = transform.createTransformedShape(focusRegion);
      Rectangle2D bounds = focusShape.getBounds2D();
      if (add6milBorder)
      {
        // add a 6 mil border
        bounds.setRect(bounds.getMinX() - _MIN_FOCUS_REGION_BORDER_IN_NANOMETERS,
                       bounds.getMinY() - _MIN_FOCUS_REGION_BORDER_IN_NANOMETERS,
                       bounds.getWidth() + (2 * _MIN_FOCUS_REGION_BORDER_IN_NANOMETERS),
                       bounds.getHeight() + (2 * _MIN_FOCUS_REGION_BORDER_IN_NANOMETERS));
      }

      // calcuate the max area for focus regions
      maxRect.setRect(bounds.getCenterX() - FocusRegion.getMaxFocusRegionSizeInNanoMeters() / 2.0,
                      bounds.getCenterY() - FocusRegion.getMaxFocusRegionSizeInNanoMeters() / 2.0,
                      FocusRegion.getMaxFocusRegionSizeInNanoMeters(reconstructionRegion.getSubtypes().iterator().next().getSubtypeAdvanceSettings().getMagnificationType()),
                      FocusRegion.getMaxFocusRegionSizeInNanoMeters(reconstructionRegion.getSubtypes().iterator().next().getSubtypeAdvanceSettings().getMagnificationType()));
      
      Area maxArea = new Area(maxRect);
      // ok, in some cases, the max focus area calculated is larger than the inspection region.
      // trunctate it!
      Area inspectionArea = new Area(reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters().getRectangle2D());
      maxArea.intersect(inspectionArea);

      // ok, now truncate the focus area if it is bigger than the max area
      Area focusArea = new Area(bounds);
      focusArea.intersect(maxArea);
      bounds = focusArea.getBounds2D();
      double width = Math.min(bounds.getWidth(),FocusRegion.getMaxFocusRegionSizeInNanoMeters(reconstructionRegion.getSubtypes().iterator().next().getSubtypeAdvanceSettings().getMagnificationType()));
      double height = Math.min(bounds.getHeight(), FocusRegion.getMaxFocusRegionSizeInNanoMeters(reconstructionRegion.getSubtypes().iterator().next().getSubtypeAdvanceSettings().getMagnificationType()));
            
      bounds.setRect(bounds.getMinX(), bounds.getMinY(), width, height);
      Assert.expect(bounds.getWidth() >= FocusRegion.getMinFocusRegionSizeInNanoMeters());
      Assert.expect(bounds.getHeight() >= FocusRegion.getMinFocusRegionSizeInNanoMeters());
      focusShapes.add(bounds);
    }

    if (inspectionFamilyEnum.equals(InspectionFamilyEnum.CHIP))
      createFocusGroupsForChips(reconstructionRegion, focusShapes);
    else if (inspectionFamilyEnum.equals(InspectionFamilyEnum.POLARIZED_CAP))
      createFocusGroupsForPolarizedCap(reconstructionRegion, focusShapes);
    else if (inspectionFamilyEnum.equals(InspectionFamilyEnum.GULLWING))
      createFocusGroupsForGullwingsOrConnectors(reconstructionRegion, focusShapes);
    else if (inspectionFamilyEnum.equals(InspectionFamilyEnum.GRID_ARRAY))
      createFocusGroupsForGridArrays(reconstructionRegion, focusShapes);
    else if (inspectionFamilyEnum.equals(InspectionFamilyEnum.THROUGHHOLE) ||
             inspectionFamilyEnum.equals(InspectionFamilyEnum.OVAL_THROUGHHOLE)) //Siew Yeng - XCR-3318 - Oval PTH
      createFocusGroupsForThroughHoles(reconstructionRegion, focusShapes);
    else if (inspectionFamilyEnum.equals(InspectionFamilyEnum.PRESSFIT))
      createFocusGroupsForPressfits(reconstructionRegion, focusShapes);
    else if (inspectionFamilyEnum.equals(InspectionFamilyEnum.ADVANCED_GULLWING))
      createFocusGroupsForGullwingsOrConnectors(reconstructionRegion, focusShapes);
    else if (inspectionFamilyEnum.equals(InspectionFamilyEnum.LARGE_PAD))
      createFocusGroupsForGullwingsOrConnectors(reconstructionRegion, focusShapes);
    else if (inspectionFamilyEnum.equals(InspectionFamilyEnum.CALIBRATION))
      createFocusGroupsForGullwingsOrConnectors(reconstructionRegion, focusShapes);
    else if (inspectionFamilyEnum.equals(InspectionFamilyEnum.QUAD_FLAT_NO_LEAD))
      createFocusGroupsForGullwingsOrConnectors(reconstructionRegion, focusShapes);
    else if (inspectionFamilyEnum.equals(InspectionFamilyEnum.EXPOSED_PAD))
      createFocusGroupsForExposedPads(reconstructionRegion, focusShapes);
//      createFocusGroupsForGullwingsOrConnectors(reconstructionRegion, focusShapes);
    else
      Assert.expect(false);

    // now set any offsets the user may have chosen
    for(Slice slice : reconstructionRegion.getSlices())
    {
      if(_testProgram.isUnfocusedSliceUsingCustomFocus(reconstructionRegion, slice))
      {
        FocusInstruction instruction = slice.getFocusInstruction();
        slice.setIsUsingCustomFocus(true);
        slice.setDefaultFocusInstructionOffsetInNanoMeters(instruction.getZOffsetInNanoMeters());
        instruction.setZOffsetInNanoMeters(_testProgram.getUnfocusedSliceZheightOffsetInNanoMeters(reconstructionRegion, slice));
      }
    }
  }

  /**
   * @author John Heumann
   *
   * Compute  offset from the "surface" required for a particular slice.
   * Recall that the surface slice is the pin slice for PTH and the component / pad slice for everything else.
   * This routine returns the offset for any joint type / slice including user-defined offsets.
   * Results are absolute in the sense that larger positive numbers always mean higher (towards
   * the x-ray source), and high negative numbers mean lower, regardless of which side of the board the
   * component is on. This contrasts with UserDefinedSliceHeights where the offset sign is independent of board side.
   *
   * Use of this routine is NOT OPTIONAL. When new slices, algorithm families, or joint types are added,
   * this routine must be updated to return correct offsets for all cases.
   */
  public int getZOffset(
          JointTypeEnum jointTypeEnum,
          SliceNameEnum sliceNameEnum,
          Subtype subtype,
          boolean isTopSide,
          int panelThicknessInNanometers)
  {
    if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
      panelThicknessInNanometers = panelThicknessInNanometers - XrayTester.getSystemPanelThicknessOffset();//Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on";
    int offsetSign;
    // Default assignment... this works for most cases
    if (isTopSide == true)
    {
      offsetSign = 1;
    }
    else
    {
      offsetSign = -1;
    }
    // However there are a few exceptions...
    if (jointTypeEnum == JointTypeEnum.PRESSFIT || 
        ((jointTypeEnum == JointTypeEnum.THROUGH_HOLE || jointTypeEnum == JointTypeEnum.OVAL_THROUGH_HOLE) && //Siew Yeng - XCR-3318 - Oval PTH
          ((sliceNameEnum == SliceNameEnum.THROUGHHOLE_PROTRUSION))))
    {
      offsetSign = -offsetSign;
    }

    // Now get the offset magnitude
    double offset = 0;
    //System.out.println(sliceNameEnum);
    if (sliceNameEnum == SliceNameEnum.THROUGHHOLE_BARREL
            || sliceNameEnum == SliceNameEnum.THROUGHHOLE_BARREL_2
            || sliceNameEnum == SliceNameEnum.THROUGHHOLE_BARREL_3
            || sliceNameEnum == SliceNameEnum.THROUGHHOLE_BARREL_4
            || sliceNameEnum == SliceNameEnum.THROUGHHOLE_BARREL_5
            || sliceNameEnum == SliceNameEnum.THROUGHHOLE_BARREL_6
            || sliceNameEnum == SliceNameEnum.THROUGHHOLE_BARREL_7
            || sliceNameEnum == SliceNameEnum.THROUGHHOLE_BARREL_8
            || sliceNameEnum == SliceNameEnum.THROUGHHOLE_BARREL_9
            || sliceNameEnum == SliceNameEnum.THROUGHHOLE_BARREL_10
            || sliceNameEnum == SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE
            || sliceNameEnum == SliceNameEnum.THROUGHHOLE_PIN_SIDE)
    {
      if (UserDefinedSliceheights.isSliceWorkingUnitInThickness(subtype) == false)
      {
        // For these slices, user defined offets are specified as a percentage fill (i.e. % of panel thickness)
//        if (UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_2
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_3
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_4
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_5
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_6
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_7
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_8
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_9
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_10)
//        {
          offset = 0.01 * UserDefinedSliceheights.getHeightAsPercentOfBarrel(sliceNameEnum, subtype) * panelThicknessInNanometers;
          if (_printDebugInfo)
            System.out.println(subtype.getLongName() + "   " + 
                                   subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SLICEHEIGHT_WORKING_UNIT).toString().matches("Thickness") + "   " +
                                   UserDefinedSliceheights.getHeightAsPercentOfBarrel(sliceNameEnum, subtype) + "   " + offset + "  " +
                                   " percentage" +
                                   " with panel thickness " + panelThicknessInNanometers);
//        }
      }
      else
      {
        if (_printDebugInfo)
          System.out.println(UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum));
        
//        if (UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT_IN_THICKNESS
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT_IN_THICKNESS
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_2
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_3
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_4
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_5
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_6
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_7
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_8
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_9
//                || UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum) == AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_10)
//        {
          offset = UserDefinedSliceheights.getOffsetInNanometers(sliceNameEnum, subtype);
          if (_printDebugInfo)
            System.out.println(subtype.getLongName() + "   " + subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SLICEHEIGHT_WORKING_UNIT).toString().matches("Thickness") + "   " + offset + " mils");
//        }
      }
      //System.out.println(UserDefinedSliceheights.getHeightAsPercentOfBarrel(sliceNameEnum, subtype));
    }
    else if (((sliceNameEnum == SliceNameEnum.PAD || sliceNameEnum == SliceNameEnum.PCAP_SLUG)
            && (jointTypeEnum == JointTypeEnum.CAPACITOR || jointTypeEnum == jointTypeEnum.RESISTOR))
            || ((sliceNameEnum == SliceNameEnum.CLEAR_CHIP_PAD || sliceNameEnum == SliceNameEnum.OPAQUE_CHIP_PAD)
            && jointTypeEnum == JointTypeEnum.POLARIZED_CAP) // Wei Chin (Tall Cap)
            //             || (sliceNameEnum == SliceNameEnum.PCAP_SLUG || sliceNameEnum == SliceNameEnum.OPAQUE_CHIP_PAD || sliceNameEnum == SliceNameEnum.CLEAR_CHIP_PAD) &&
            //             (jointTypeEnum == JointTypeEnum.TALL_CAPACITOR)
            )
    {
      // CAPACITOR and RESISTOR don't have user defined offsets for PAD or PCAP_SLUG
      // Similarly, POLARIZED_CAP doesn't have user defined offsets for CLEAR_CHIP_PAD or OPAQUE_CHIP_PAD
      offset = 0.0;
    }
    else if ((jointTypeEnum == JointTypeEnum.SURFACE_MOUNT_CONNECTOR) && ((sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_2) || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_3)
            || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_4) || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_5) || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_6)
            || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_7) || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_8) || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_9)
            || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_10)))
    {     
      if ((sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_2) || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_3))
        offset = _SMT_CONNECTOR_ZOFFSET_FOUR_MILS_IN_NANOS; 
      else if ((sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_4) || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_5))
        offset = _SMT_CONNECTOR_ZOFFSET_EIGHT_MILS_IN_NANOS;
      else if ((sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_6) || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_7))
        offset = _SMT_CONNECTOR_ZOFFSET_TWELVE_MILS_IN_NANOS;
      else if ((sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_8) || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_9))
        offset = _SMT_CONNECTOR_ZOFFSET_SIXTEEN_MILS_IN_NANOS;
      else if ((sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_10))
        offset = _SMT_CONNECTOR_ZOFFSET_TWENTY_MILS_IN_NANOS;
      
      offsetSign = -1;
      
      if ((sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_3) || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_5)
           || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_7) || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_9)
           || (sliceNameEnum == SliceNameEnum.CONNECTOR_BARREL_10))
      
        offsetSign = 1;
    }
    else
    {
      // In most cases, the user defined offset is specified directly in nanometers
      offset = UserDefinedSliceheights.getOffsetInNanometers(sliceNameEnum, subtype);
 
      // However for throughhole INSERTION slices, the user-defined offset is relative to
      // the component side, but the surface slice for throughhole is the pin side
      if ((jointTypeEnum == JointTypeEnum.THROUGH_HOLE || jointTypeEnum == JointTypeEnum.OVAL_THROUGH_HOLE) && //Siew Yeng - XCR-3318 - Oval PTH
           sliceNameEnum == SliceNameEnum.THROUGHHOLE_INSERTION)
      {
        offset += panelThicknessInNanometers;
      }
    }

    return (int) Math.rint(offsetSign * offset);
  }

  /**
   * @author George A. David
   * @author Poh Kheng
   * @author Siew Yeng - remove 3rd slice: Pad and 4th slice: Pcap slug
   */
  private void createFocusGroupsForChips(ReconstructionRegion reconstructionRegion, List<java.awt.Shape> focusRegions)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(focusRegions != null);
    //Assert.expect(focusRegions.size() == 4);

    int zCurveWidthInNanoMeters = getZcurveWidthInNanoMeters(reconstructionRegion);
    Pad pad = reconstructionRegion.getComponent().getPads().get(0);
    int padSize = (pad.getWidthInNanoMeters() + pad.getLengthInNanoMeters()) / 2;
    int opaqueZCurveWidthInNanoMeters = (int) Math.rint(0.25 * padSize);
    int opaqueZOffsetInNanoMeters = (int) Math.rint(0.5 * padSize);
    if (opaqueZCurveWidthInNanoMeters < zCurveWidthInNanoMeters)
      opaqueZCurveWidthInNanoMeters = zCurveWidthInNanoMeters;

    final JointTypeEnum jointType = reconstructionRegion.getJointTypeEnum();
    final boolean isTopSide = reconstructionRegion.isTopSide();
    final int panelThicknessInNanometers = _project.getPanel().getThicknessInNanometers();
    final Subtype subtype = getSubtypeForUserDefinedSliceheights(reconstructionRegion);

    //Siew Yeng - XCR-2140 - DRO
    boolean useDynamicRangeOptimization = false;
    if(subtype.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel() != DynamicRangeOptimizationLevelEnum.ONE)
      useDynamicRangeOptimization = true;
    
    // Kok Chun, Tan - MVEDR
    boolean useMultipleStageSpeed = false;
    if (subtype.getSubtypeAdvanceSettings().getStageSpeedList().size() > 1)
      useMultipleStageSpeed = true;
    
    // Bee Hoon
    int midBoardZoffset = UserDefinedSliceheights.getAutoFocusMidBoardOffsetInNanometers(subtype) + 
            (int)reconstructionRegion.getBoard().getBoardSettings().getBoardZOffsetFromMeansOffsetInNanometers();
    int userDefinedInitialWaveletLevel = subtype.getUserDefinedInitialWaveletLevel();
    int searchRangeLowLimitInNanometers = UserDefinedSliceheights.getSearchRangeLowLimitInNanometers(subtype);
    int searchRangeHighLimitInNanometers = UserDefinedSliceheights.getSearchRangeHighLimitInNanometers(subtype);
    int pspZOffsetInNanometers = UserDefinedSliceheights.getPspZOffsetInNanometers(subtype);
    
    // Wei Chin (RFB)
    if(pad.getPadType().getPadTypeSettings().getGlobalSurfaceModel().equals(GlobalSurfaceModelEnum.USE_RFB))
    {
      // first slice
      // This focus group / slice is also used to discrimiate between clear and
      // opaque, so we have to use the opaque width and a fudged offset
      SliceNameEnum sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
      FocusGroup focusGroup;
      int referencesZHeight;
      if (isTopSide)
      {
        referencesZHeight = (int) getZTop();
      }
      else
      {
        referencesZHeight = (int) getZBottom();
      }

      Float referencesOffset = 0.0f;
      // Wei CHin
      Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE);
      if (value instanceof Float)
      {
        referencesOffset = (Float) value;
      }

//      referencesZHeight = (int) (referencesZHeight + MathUtil.convertMillimetersToNanometers(referencesOffset));
      referencesZHeight = (int) (referencesZHeight + MathUtil.convertMillimetersToNanometers(referencesOffset))
        + reconstructionRegion.getBoard().getBoardSettings().getBoardZOffsetFromMeansOffsetInNanometers();

      focusGroup = createFocusGroup(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE,
              new PanelRectangle(focusRegions.get(0)),
              sliceNameEnum,
              SliceTypeEnum.RECONSTRUCTION,
              0, // percent
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
              referencesZHeight,
              opaqueZCurveWidthInNanoMeters,
              FocusMethodEnum.USE_Z_HEIGHT,
              opaqueZOffsetInNanoMeters / 2,
              midBoardZoffset,
              true,
              userDefinedInitialWaveletLevel,
              useDynamicRangeOptimization,
              searchRangeLowLimitInNanometers,
              searchRangeHighLimitInNanometers,
              pspZOffsetInNanometers,
              reconstructionRegion,
              useMultipleStageSpeed);
      reconstructionRegion.addFocusGroup(focusGroup);

      // second slice
      sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
      focusGroup = createFocusGroup(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE,
              new PanelRectangle(focusRegions.get(1)),
              sliceNameEnum,
              SliceTypeEnum.RECONSTRUCTION,
              0, // percent
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
              referencesZHeight,
              opaqueZCurveWidthInNanoMeters,
              FocusMethodEnum.USE_Z_HEIGHT,
              opaqueZOffsetInNanoMeters,
              midBoardZoffset,
              true,
              userDefinedInitialWaveletLevel,
              useDynamicRangeOptimization,
              searchRangeLowLimitInNanometers,
              searchRangeHighLimitInNanometers,
              pspZOffsetInNanometers,
              reconstructionRegion,
              useMultipleStageSpeed);
      if ((jointType.equals(JointTypeEnum.CAPACITOR)
              || jointType.equals(JointTypeEnum.RESISTOR)))
      {
        Slice slice = createSlice(SliceNameEnum.SURFACE_MODELING,
                SliceTypeEnum.SURFACE,
                0,
                0,
                referencesZHeight,
                FocusMethodEnum.USE_Z_HEIGHT,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed); // Siew Yeng
        focusGroup.addSlice(slice);
      }
      reconstructionRegion.addFocusGroup(focusGroup);
    }
    else
    {      
      boolean useToBuildGlobalSurfaceModel = false;
      FocusGroup focusGroup;

      // first slice
      // This focus group / slice is also used to discrimiate between clear and
      // opaque, so we have to use the opaque width and a fudged offset
      SliceNameEnum sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
      if (jointType.equals(JointTypeEnum.RESISTOR))
      {
        useToBuildGlobalSurfaceModel = true;
      }
      else
      {
        useToBuildGlobalSurfaceModel = false;
      }
      focusGroup = createFocusGroup(FocusProfileShapeEnum.PEAK,
              new PanelRectangle(focusRegions.get(0)),
              sliceNameEnum,
              SliceTypeEnum.RECONSTRUCTION,
              0, // percent
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
              0,
              opaqueZCurveWidthInNanoMeters,
              FocusMethodEnum.SHARPEST,
              opaqueZOffsetInNanoMeters / 2,
              midBoardZoffset,
              useToBuildGlobalSurfaceModel,
              userDefinedInitialWaveletLevel,
              useDynamicRangeOptimization,
              searchRangeLowLimitInNanometers,
              searchRangeHighLimitInNanometers,
              pspZOffsetInNanometers,
              reconstructionRegion,
              useMultipleStageSpeed);

      if (jointType.equals(JointTypeEnum.RESISTOR))
      {
        Slice slice = createSlice(SliceNameEnum.SURFACE_MODELING,
                SliceTypeEnum.SURFACE,
                0,
                0,
                0,
                FocusMethodEnum.SHARPEST,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed); //Siew Yeng
        focusGroup.addSlice(slice);
      }

      reconstructionRegion.addFocusGroup(focusGroup);
     

      // second slice
      sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
      if (jointType.equals(JointTypeEnum.CAPACITOR))
      {
        useToBuildGlobalSurfaceModel = true;
      }
      else
      {
        useToBuildGlobalSurfaceModel = false;
      }
      focusGroup = createFocusGroup(FocusProfileShapeEnum.GAUSSIAN_EDGE,
              new PanelRectangle(focusRegions.get(1)),
              sliceNameEnum,
              SliceTypeEnum.RECONSTRUCTION,
              0, // percent
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
              0,
              opaqueZCurveWidthInNanoMeters,
              FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
              opaqueZOffsetInNanoMeters,
              midBoardZoffset,
              useToBuildGlobalSurfaceModel,
              userDefinedInitialWaveletLevel,
              useDynamicRangeOptimization,
              searchRangeLowLimitInNanometers,
              searchRangeHighLimitInNanometers,
              pspZOffsetInNanometers,
              reconstructionRegion,
              useMultipleStageSpeed);
      if (jointType.equals(JointTypeEnum.CAPACITOR))
      {
        Slice slice = createSlice(SliceNameEnum.SURFACE_MODELING,
                SliceTypeEnum.SURFACE,
                0,
                0,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed); //Siew Yeng
        focusGroup.addSlice(slice);
      }
      reconstructionRegion.addFocusGroup(focusGroup);
    }
  }
  
   private void createFocusGroupsForPolarizedCap(ReconstructionRegion reconstructionRegion, List<java.awt.Shape> focusRegions)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(focusRegions != null);
    //Assert.expect(focusRegions.size() == 4);

    int zCurveWidthInNanoMeters = getZcurveWidthInNanoMeters(reconstructionRegion);
    Pad pad = reconstructionRegion.getComponent().getPads().get(0);
    int padSize = (pad.getWidthInNanoMeters() + pad.getLengthInNanoMeters()) / 2;
    int opaqueZCurveWidthInNanoMeters = (int) Math.rint(0.25 * padSize);
    int opaqueZOffsetInNanoMeters = (int) Math.rint(0.5 * padSize);
    if (opaqueZCurveWidthInNanoMeters < zCurveWidthInNanoMeters)
      opaqueZCurveWidthInNanoMeters = zCurveWidthInNanoMeters;

    final JointTypeEnum jointType = reconstructionRegion.getJointTypeEnum();
    final boolean isTopSide = reconstructionRegion.isTopSide();
    final int panelThicknessInNanometers = _project.getPanel().getThicknessInNanometers();
    final Subtype subtype = getSubtypeForUserDefinedSliceheights(reconstructionRegion);

    //Siew Yeng - XCR-2140 - DRO
    boolean useDynamicRangeOptimization = false;
    if(subtype.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel() != DynamicRangeOptimizationLevelEnum.ONE)
      useDynamicRangeOptimization = true;
    
    //Kok Chun, Tan - MVEDR
    boolean useMultipleStageSpeed = false;
    if(subtype.getSubtypeAdvanceSettings().getStageSpeedList().size() > 1)
      useMultipleStageSpeed = true;
    
    // Bee Hoon
    int midBoardZoffset = UserDefinedSliceheights.getAutoFocusMidBoardOffsetInNanometers(subtype) + 
            (int)reconstructionRegion.getBoard().getBoardSettings().getBoardZOffsetFromMeansOffsetInNanometers();
    int userDefinedInitialWaveletLevel = subtype.getUserDefinedInitialWaveletLevel();
    int searchRangeLowLimitInNanometers = UserDefinedSliceheights.getSearchRangeLowLimitInNanometers(subtype);
    int searchRangeHighLimitInNanometers = UserDefinedSliceheights.getSearchRangeHighLimitInNanometers(subtype);
    int pspZOffsetInNanometers = UserDefinedSliceheights.getPspZOffsetInNanometers(subtype);
    
    // Wei Chin (RFB)
    if(pad.getPadType().getPadTypeSettings().getGlobalSurfaceModel().equals(GlobalSurfaceModelEnum.USE_RFB))
    {
      // first slice
      // This focus group / slice is also used to discrimiate between clear and
      // opaque, so we have to use the opaque width and a fudged offset
      SliceNameEnum sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
      FocusGroup focusGroup;
      int referencesZHeight;
      if (isTopSide)
      {
        referencesZHeight = (int) getZTop();
      }
      else
      {
        referencesZHeight = (int) getZBottom();
      }

      Float referencesOffset = 0.0f;
      // Wei CHin
      Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE);
      if (value instanceof Float)
      {
        referencesOffset = (Float) value;
      }

//      referencesZHeight = (int) (referencesZHeight + MathUtil.convertMillimetersToNanometers(referencesOffset));
      referencesZHeight = (int) (referencesZHeight + MathUtil.convertMillimetersToNanometers(referencesOffset))
        + reconstructionRegion.getBoard().getBoardSettings().getBoardZOffsetFromMeansOffsetInNanometers();

      focusGroup = createFocusGroup(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE,
              new PanelRectangle(focusRegions.get(0)),
              sliceNameEnum,
              SliceTypeEnum.RECONSTRUCTION,
              0, // percent
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
              referencesZHeight,
              opaqueZCurveWidthInNanoMeters,
              FocusMethodEnum.USE_Z_HEIGHT,
              opaqueZOffsetInNanoMeters / 2,
              midBoardZoffset,
              true,
              userDefinedInitialWaveletLevel,
              useDynamicRangeOptimization,
              searchRangeLowLimitInNanometers,
              searchRangeHighLimitInNanometers,
              pspZOffsetInNanometers,
              reconstructionRegion,
              useMultipleStageSpeed);
      reconstructionRegion.addFocusGroup(focusGroup);

      // second slice
      sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
      focusGroup = createFocusGroup(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE,
              new PanelRectangle(focusRegions.get(1)),
              sliceNameEnum,
              SliceTypeEnum.RECONSTRUCTION,
              0, // percent
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
              referencesZHeight,
              opaqueZCurveWidthInNanoMeters,
              FocusMethodEnum.USE_Z_HEIGHT,
              opaqueZOffsetInNanoMeters,
              midBoardZoffset,
              true,
              userDefinedInitialWaveletLevel,
              useDynamicRangeOptimization,
              searchRangeLowLimitInNanometers,
              searchRangeHighLimitInNanometers,
              pspZOffsetInNanometers,
              reconstructionRegion,
              useMultipleStageSpeed);
      if ((jointType.equals(JointTypeEnum.CAPACITOR)
              || jointType.equals(JointTypeEnum.RESISTOR)))
      {
        Slice slice = createSlice(SliceNameEnum.SURFACE_MODELING,
                SliceTypeEnum.SURFACE,
                0,
                0,
                referencesZHeight,
                FocusMethodEnum.USE_Z_HEIGHT,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed); // Siew Yeng
        focusGroup.addSlice(slice);
      }
      reconstructionRegion.addFocusGroup(focusGroup);


      // third slice
      // This focus group is used for both opaque and clear parts.
      // The peak is often pulled up on opaque parts, so we do a kludgey
      // hybrid, similar to that used for the first slice, in an attempt to
      // robustly include the peak
      sliceNameEnum = SliceNameEnum.PAD;
      focusGroup = createFocusGroup(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE,
              new PanelRectangle(focusRegions.get(2)),
              sliceNameEnum,
              SliceTypeEnum.RECONSTRUCTION,
              0, // percent
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
              referencesZHeight,
              opaqueZCurveWidthInNanoMeters,
              FocusMethodEnum.USE_Z_HEIGHT,
              opaqueZOffsetInNanoMeters / 2,
              midBoardZoffset,
              true,
              userDefinedInitialWaveletLevel,
              useDynamicRangeOptimization,
              searchRangeLowLimitInNanometers,
              searchRangeHighLimitInNanometers,
              pspZOffsetInNanometers,
              reconstructionRegion,
              useMultipleStageSpeed);
      if (jointType.equals(JointTypeEnum.POLARIZED_CAP))
      {
        Slice slice = createSlice(SliceNameEnum.SURFACE_MODELING,
                SliceTypeEnum.SURFACE,
                0,
                -2,
                referencesZHeight,
                FocusMethodEnum.USE_Z_HEIGHT,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed); // Siew Yeng
        focusGroup.addSlice(slice);
      }
      reconstructionRegion.addFocusGroup(focusGroup);

      // fourth slice
      sliceNameEnum = SliceNameEnum.PCAP_SLUG;
      focusGroup = createFocusGroup(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE,
              new PanelRectangle(focusRegions.get(3)),
              sliceNameEnum,
              SliceTypeEnum.RECONSTRUCTION,
              0, // percent
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
              referencesZHeight,
              opaqueZCurveWidthInNanoMeters,
              FocusMethodEnum.USE_Z_HEIGHT,
              opaqueZOffsetInNanoMeters,
              midBoardZoffset,
              false,
              userDefinedInitialWaveletLevel,
              useDynamicRangeOptimization,
              searchRangeLowLimitInNanometers,
              searchRangeHighLimitInNanometers,
              pspZOffsetInNanometers,
              reconstructionRegion,
              useMultipleStageSpeed);
      reconstructionRegion.addFocusGroup(focusGroup);
    }
    else
    {      
      boolean useToBuildGlobalSurfaceModel = false;
      FocusGroup focusGroup;

      // first slice
      // This focus group / slice is also used to discrimiate between clear and
      // opaque, so we have to use the opaque width and a fudged offset
      SliceNameEnum sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
      if (jointType.equals(JointTypeEnum.RESISTOR))
      {
        useToBuildGlobalSurfaceModel = true;
      }
      else
      {
        useToBuildGlobalSurfaceModel = false;
      }
      focusGroup = createFocusGroup(FocusProfileShapeEnum.PEAK,
              new PanelRectangle(focusRegions.get(0)),
              sliceNameEnum,
              SliceTypeEnum.RECONSTRUCTION,
              0, // percent
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
              0,
              opaqueZCurveWidthInNanoMeters,
              FocusMethodEnum.SHARPEST,
              opaqueZOffsetInNanoMeters / 2,
              midBoardZoffset,
              useToBuildGlobalSurfaceModel,
              userDefinedInitialWaveletLevel,
              useDynamicRangeOptimization,
              searchRangeLowLimitInNanometers,
              searchRangeHighLimitInNanometers,
              pspZOffsetInNanometers,
              reconstructionRegion,
              useMultipleStageSpeed);

      if (jointType.equals(JointTypeEnum.RESISTOR))
      {
        Slice slice = createSlice(SliceNameEnum.SURFACE_MODELING,
                SliceTypeEnum.SURFACE,
                0,
                0,
                0,
                FocusMethodEnum.SHARPEST,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed); //Siew Yeng
        focusGroup.addSlice(slice);
      }

      reconstructionRegion.addFocusGroup(focusGroup);
     

      // second slice
      sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
      if (jointType.equals(JointTypeEnum.CAPACITOR))
      {
        useToBuildGlobalSurfaceModel = true;
      }
      else
      {
        useToBuildGlobalSurfaceModel = false;
      }
      focusGroup = createFocusGroup(FocusProfileShapeEnum.GAUSSIAN_EDGE,
              new PanelRectangle(focusRegions.get(1)),
              sliceNameEnum,
              SliceTypeEnum.RECONSTRUCTION,
              0, // percent
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
              0,
              opaqueZCurveWidthInNanoMeters,
              FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
              opaqueZOffsetInNanoMeters,
              midBoardZoffset,
              useToBuildGlobalSurfaceModel,
              userDefinedInitialWaveletLevel,
              useDynamicRangeOptimization,
              searchRangeLowLimitInNanometers,
              searchRangeHighLimitInNanometers,
              pspZOffsetInNanometers,
              reconstructionRegion,
              useMultipleStageSpeed);
      if (jointType.equals(JointTypeEnum.CAPACITOR))
      {
        Slice slice = createSlice(SliceNameEnum.SURFACE_MODELING,
                SliceTypeEnum.SURFACE,
                0,
                0,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed); // Siew Yeng
        focusGroup.addSlice(slice);
      }
      reconstructionRegion.addFocusGroup(focusGroup);


      // third slice
      // This focus group is used for both opaque and clear parts.
      // The peak is often pulled up on opaque parts, so we do a kludgey
      // hybrid, similar to that used for the first slice, in an attempt to
      // robustly include the peak
      sliceNameEnum = SliceNameEnum.PAD;
      if (jointType.equals(JointTypeEnum.POLARIZED_CAP))
      {
        useToBuildGlobalSurfaceModel = true;
      }
      else
      {
        useToBuildGlobalSurfaceModel = false;
      }

      focusGroup = createFocusGroup(FocusProfileShapeEnum.PEAK,
              new PanelRectangle(focusRegions.get(2)),
              sliceNameEnum,
              SliceTypeEnum.RECONSTRUCTION,
              0, // percent
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
              0,
              opaqueZCurveWidthInNanoMeters,
              FocusMethodEnum.SHARPEST,
              opaqueZOffsetInNanoMeters / 2,
              midBoardZoffset,
              useToBuildGlobalSurfaceModel,
              userDefinedInitialWaveletLevel,
              useDynamicRangeOptimization,
              searchRangeLowLimitInNanometers,
              searchRangeHighLimitInNanometers,
              pspZOffsetInNanometers,
              reconstructionRegion,
              useMultipleStageSpeed);
      if (jointType.equals(JointTypeEnum.POLARIZED_CAP))
      {
        Slice slice = createSlice(SliceNameEnum.SURFACE_MODELING,
                SliceTypeEnum.SURFACE,
                0,
                -2,
                0,
                FocusMethodEnum.SHARPEST,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed); //Siew Yeng
        focusGroup.addSlice(slice);
      }
      reconstructionRegion.addFocusGroup(focusGroup);

      // fourth slice
      sliceNameEnum = SliceNameEnum.PCAP_SLUG;
      useToBuildGlobalSurfaceModel = false;
      focusGroup = createFocusGroup(FocusProfileShapeEnum.PEAK,
              new PanelRectangle(focusRegions.get(3)),
              sliceNameEnum,
              SliceTypeEnum.RECONSTRUCTION,
              0, // percent
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
              0,
              opaqueZCurveWidthInNanoMeters,
              FocusMethodEnum.SHARPEST,
              opaqueZOffsetInNanoMeters,
              midBoardZoffset,
              useToBuildGlobalSurfaceModel,
              userDefinedInitialWaveletLevel,
              useDynamicRangeOptimization,
              searchRangeLowLimitInNanometers,
              searchRangeHighLimitInNanometers,
              pspZOffsetInNanometers,
              reconstructionRegion,
              useMultipleStageSpeed);
      reconstructionRegion.addFocusGroup(focusGroup);
    }
  }

  /**
   * @author George A. David
   * @author Poh Kheng
   */
  private void createFocusGroupsForGullwingsOrConnectors(ReconstructionRegion reconstructionRegion, List<java.awt.Shape> focusRegions)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(focusRegions != null);
    Assert.expect(focusRegions.size() == 1);

    int zCurveWidthInNanoMeters = getZcurveWidthInNanoMeters(reconstructionRegion);

    final JointTypeEnum jointType = reconstructionRegion.getJointTypeEnum();
    final boolean isTopSide = reconstructionRegion.isTopSide();
    final int panelThicknessInNanometers = _project.getPanel().getThicknessInNanometers();
    final Subtype subtype = getSubtypeForUserDefinedSliceheights(reconstructionRegion);

     //Siew Yeng - XCR-2140 - DRO
    boolean useDynamicRangeOptimization = false;
    if(subtype.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel() != DynamicRangeOptimizationLevelEnum.ONE)
      useDynamicRangeOptimization = true;
    
    //Kok Chun, Tan - MVEDR
    boolean useMultipleStageSpeed = false;
    if(subtype.getSubtypeAdvanceSettings().getStageSpeedList().size() > 1)
      useMultipleStageSpeed = true;
    
    // Bee Hoon
    int midBoardZoffset = UserDefinedSliceheights.getAutoFocusMidBoardOffsetInNanometers(subtype) + 
            (int)reconstructionRegion.getBoard().getBoardSettings().getBoardZOffsetFromMeansOffsetInNanometers();
    int userDefinedInitialWaveletLevel = subtype.getUserDefinedInitialWaveletLevel();
    int searchRangeLowLimitInNanometers = UserDefinedSliceheights.getSearchRangeLowLimitInNanometers(subtype);
    int searchRangeHighLimitInNanometers = UserDefinedSliceheights.getSearchRangeHighLimitInNanometers(subtype);
    int pspZOffsetInNanometers = UserDefinedSliceheights.getPspZOffsetInNanometers(subtype);
    
    // Wei Chin (RFB)
    if (reconstructionRegion.getComponent().getPadOne().getPadType().getPadTypeSettings().getGlobalSurfaceModel().equals(GlobalSurfaceModelEnum.USE_RFB))
    {
      FocusGroup focusGroup;
      int referencesZHeight;
      if (isTopSide)
      {
        referencesZHeight = (int) getZTop();
      }
      else
      {
        referencesZHeight = (int) getZBottom();
      }

      Float referencesOffset = 0.0f;
      // Wei CHin
      Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE);
      if (value instanceof Float)
      {
        referencesOffset = (Float) value;
      }

//      referencesZHeight = (int) (referencesZHeight + MathUtil.convertMillimetersToNanometers(referencesOffset));
      referencesZHeight = (int) (referencesZHeight + MathUtil.convertMillimetersToNanometers(referencesOffset))
        + reconstructionRegion.getBoard().getBoardSettings().getBoardZOffsetFromMeansOffsetInNanometers();

      // define the pad slice
      SliceNameEnum sliceNameEnum = SliceNameEnum.PAD;
      focusGroup = createFocusGroup(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE,
              new PanelRectangle(focusRegions.get(0)),
              sliceNameEnum,
              SliceTypeEnum.RECONSTRUCTION,
              0, // percent
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
              referencesZHeight,
              zCurveWidthInNanoMeters,
              FocusMethodEnum.USE_Z_HEIGHT,
              0,
              midBoardZoffset,
              true,
              userDefinedInitialWaveletLevel,
              useDynamicRangeOptimization,
              searchRangeLowLimitInNanometers,
              searchRangeHighLimitInNanometers,
              pspZOffsetInNanometers,
              reconstructionRegion,
              useMultipleStageSpeed);

      if (SharedShortAlgorithm.allowHighShortDetection(subtype.getJointTypeEnum()))
      {
        // Add a High Short slice.  This was requested by Bosch.  The ideal solution would probably be a variable depth
        // of focus reconstruction, but until then, we need to provide this extra slice.
        sliceNameEnum = SliceNameEnum.HIGH_SHORT;
        Slice highShortSlice = createSlice(sliceNameEnum,
                SliceTypeEnum.RECONSTRUCTION,
                0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
                referencesZHeight,
                FocusMethodEnum.USE_Z_HEIGHT,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
        focusGroup.addSlice(highShortSlice);
      }


      // set the FocusSearchParameters depending on the joint type and orientation
      try
      {
        if (jointType.equals(JointTypeEnum.GULLWING)
                || jointType.equals(JointTypeEnum.JLEAD))
        {
          FocusSearchParameters fsp = focusGroup.getFocusSearchParameters();
          // All the pads in a gullwing or jlead insepaction region have the
          // same orientation, so just grab the first
          List<JointInspectionData> jointList =
                  reconstructionRegion.getJointInspectionDataList();
          int orientation = MathUtil.getDegreesWithin0To359(
                  MathUtil.convertDoubleToInt(
                  jointList.get(0).getPad().getPinOrientationInDegreesAfterAllRotations()));

          // For gullwings and Jleads oriented almost vertically or horizontally,
          // weight the "across" component of the wavelet gradient more heavily (75%)
          // to compute sharpness. Otherwise, "along" and "across" contribute equally.
          if ((orientation >= 0 && orientation < 30) || orientation > 330
                  || (orientation > 150 && orientation < 210))
          {
            fsp.setHorizontalSharpnessComponentPercentage(75);
          }
          else if ((orientation > 60 && orientation < 120)
                  || (orientation > 240 && orientation < 300))
          {
            fsp.setHorizontalSharpnessComponentPercentage(25);
          }
          // else {
          //   fsp.setHorizontalSharpnessComponentPercentage(50);  // Not needed - this is the default
          // }
          //

          // For gullwings and Jleads, a linear gradient of weights is applied
          // so that the toe contributes more than the heel to sharpnesss
          // NOTE: this idea did not improve performance, so it's not currently
          // implemented on the IRP side. (I.e. gradientWeightingMagnitudePercentage
          // is ignored). We've left the parameters in FocusSearchParameters
          // for possible future use.
          fsp.setGradientWeightingMagnitudePercentage(0);
          if (jointType.equals(JointTypeEnum.GULLWING))
          {
            fsp.setGradientWeightingOrientationInDegrees(orientation);
          }
          else
          { // JLEAD
            fsp.setGradientWeightingOrientationInDegrees(
                    MathUtil.getDegreesWithin0To359(orientation + 180));
          }
        }
      }
      catch (ValueOutOfRangeException e)
      {
        // do nothing
      }
      
      // Handle User-Defined-Slice
      // Kee Chin Seong - This is need LGA also
      if (jointType.equals(JointTypeEnum.SINGLE_PAD) ||
          jointType.equals(JointTypeEnum.LGA))
      {
        int numberOBarrelSlices = Algorithm.getNumberOfSlice(subtype);
        if (numberOBarrelSlices == 1)
        {
          // define the user-defined slice
          sliceNameEnum = SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1; 
          
          int zOffSet = getZOffset(jointType, SliceNameEnum.PAD, subtype, isTopSide, panelThicknessInNanometers) +
                        getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers);

          Slice slice = createSlice(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1,
            SliceTypeEnum.RECONSTRUCTION,
            -25,
            zOffSet,
            0,
            FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
            false,
            useDynamicRangeOptimization,
            useMultipleStageSpeed);// Siew Yeng
          focusGroup.addSlice(slice);
        }
        else if (numberOBarrelSlices == 2)
        {
          // define the user-defined slice
          sliceNameEnum = SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1; 
          
          int zOffSet = getZOffset(jointType, SliceNameEnum.PAD, subtype, isTopSide, panelThicknessInNanometers) +
                        getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers);

          Slice slice = createSlice(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1,
            SliceTypeEnum.RECONSTRUCTION,
            -25,
            zOffSet,
            0,
            FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
            false,
            useDynamicRangeOptimization,
            useMultipleStageSpeed);// Siew Yeng
          focusGroup.addSlice(slice);

          // define the user-defined slice
          sliceNameEnum = SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2;
          
          zOffSet = getZOffset(jointType, SliceNameEnum.PAD, subtype, isTopSide, panelThicknessInNanometers) +
                    getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers);
          
          slice = createSlice(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2,
            SliceTypeEnum.RECONSTRUCTION,
            -25,
            zOffSet,
            0,
            FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
            false,
            useDynamicRangeOptimization,
            useMultipleStageSpeed);// Siew Yeng
          focusGroup.addSlice(slice);
        }
      }

      if ((jointType.equals(JointTypeEnum.GULLWING)
              || jointType.equals(JointTypeEnum.JLEAD)
              || jointType.equals(JointTypeEnum.SURFACE_MOUNT_CONNECTOR))
              && reconstructionRegion.isInspected())
      {
        Slice slice = createSlice(SliceNameEnum.SURFACE_MODELING,
                SliceTypeEnum.SURFACE,
                0,
                -2,
                referencesZHeight,
                FocusMethodEnum.USE_Z_HEIGHT,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
        focusGroup.addSlice(slice);
        
        int numberOBarrelSlices = 0;       
        if (jointType.equals(JointTypeEnum.SURFACE_MOUNT_CONNECTOR))
          numberOBarrelSlices = Algorithm.getNumberOfSlice(subtype);
        if(numberOBarrelSlices >= 2)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_2,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) - _SMT_CONNECTOR_ZOFFSET_FOUR_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
        
        if(numberOBarrelSlices >= 3)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_3,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) + _SMT_CONNECTOR_ZOFFSET_FOUR_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
         
        if(numberOBarrelSlices >= 4)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_4,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) - _SMT_CONNECTOR_ZOFFSET_EIGHT_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        } 
        
        if(numberOBarrelSlices >= 5)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_5,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) + _SMT_CONNECTOR_ZOFFSET_EIGHT_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
        
        if(numberOBarrelSlices >= 6)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_6,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) - _SMT_CONNECTOR_ZOFFSET_TWELVE_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
        
        if(numberOBarrelSlices >= 7)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_7,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) + _SMT_CONNECTOR_ZOFFSET_TWELVE_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
        
        if(numberOBarrelSlices >= 8)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_8,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) - _SMT_CONNECTOR_ZOFFSET_SIXTEEN_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
        
        if(numberOBarrelSlices >= 9)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_9,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) + _SMT_CONNECTOR_ZOFFSET_SIXTEEN_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
        
        if(numberOBarrelSlices >= 10)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_10,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) - _SMT_CONNECTOR_ZOFFSET_TWENTY_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
      }
      reconstructionRegion.addFocusGroup(focusGroup);
    }
    // use normal auto focus
    else
    {
      // define the pad slice
      SliceNameEnum sliceNameEnum = SliceNameEnum.PAD;
      FocusGroup focusGroup;
      focusGroup = createFocusGroup(FocusProfileShapeEnum.GAUSSIAN,
              new PanelRectangle(focusRegions.get(0)),
              sliceNameEnum,
              SliceTypeEnum.RECONSTRUCTION,
              -25, // percent
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
              0,
              zCurveWidthInNanoMeters,
              FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
              0,
              midBoardZoffset,
              true,
              userDefinedInitialWaveletLevel,
              useDynamicRangeOptimization,
              searchRangeLowLimitInNanometers,
              searchRangeHighLimitInNanometers,
              pspZOffsetInNanometers,
              reconstructionRegion,
              useMultipleStageSpeed);

      if (SharedShortAlgorithm.allowHighShortDetection(subtype.getJointTypeEnum()))
      {
        // Add a High Short slice.  This was requested by Bosch.  The ideal solution would probably be a variable depth
        // of focus reconstruction, but until then, we need to provide this extra slice.
        sliceNameEnum = SliceNameEnum.HIGH_SHORT;
        Slice highShortSlice = createSlice(sliceNameEnum,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
        focusGroup.addSlice(highShortSlice);
      }


      // set the FocusSearchParameters depending on the joint type and orientation
      try
      {
        if (jointType.equals(JointTypeEnum.GULLWING)
                || jointType.equals(JointTypeEnum.JLEAD))
        {
          FocusSearchParameters fsp = focusGroup.getFocusSearchParameters();
          // All the pads in a gullwing or jlead insepaction region have the
          // same orientation, so just grab the first
          List<JointInspectionData> jointList =
                  reconstructionRegion.getJointInspectionDataList();
          int orientation = MathUtil.getDegreesWithin0To359(
                  MathUtil.convertDoubleToInt(
                  jointList.get(0).getPad().getPinOrientationInDegreesAfterAllRotations()));

          // For gullwings and Jleads oriented almost vertically or horizontally,
          // weight the "across" component of the wavelet gradient more heavily (75%)
          // to compute sharpness. Otherwise, "along" and "across" contribute equally.
          if ((orientation >= 0 && orientation < 30) || orientation > 330
                  || (orientation > 150 && orientation < 210))
          {
            fsp.setHorizontalSharpnessComponentPercentage(75);
          }
          else if ((orientation > 60 && orientation < 120)
                  || (orientation > 240 && orientation < 300))
          {
            fsp.setHorizontalSharpnessComponentPercentage(25);
          }
          // else {
          //   fsp.setHorizontalSharpnessComponentPercentage(50);  // Not needed - this is the default
          // }
          //

          // For gullwings and Jleads, a linear gradient of weights is applied
          // so that the toe contributes more than the heel to sharpnesss
          // NOTE: this idea did not improve performance, so it's not currently
          // implemented on the IRP side. (I.e. gradientWeightingMagnitudePercentage
          // is ignored). We've left the parameters in FocusSearchParameters
          // for possible future use.
          fsp.setGradientWeightingMagnitudePercentage(0);
          if (jointType.equals(JointTypeEnum.GULLWING))
          {
            fsp.setGradientWeightingOrientationInDegrees(orientation);
          }
          else
          { // JLEAD
            fsp.setGradientWeightingOrientationInDegrees(
                    MathUtil.getDegreesWithin0To359(orientation + 180));
          }
        }
      }
      catch (ValueOutOfRangeException e)
      {
        // do nothing
      }

      // Handle User-Defined-Slice
      if (jointType.equals(JointTypeEnum.SINGLE_PAD) ||
          jointType.equals(JointTypeEnum.LGA))
      {
        int numberOBarrelSlices = Algorithm.getNumberOfSlice(subtype);
        if (numberOBarrelSlices == 1)
        {
          // define the user-defined slice
          sliceNameEnum = SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1; 
          
          int zOffSet = getZOffset(jointType, SliceNameEnum.PAD, subtype, isTopSide, panelThicknessInNanometers) +
                        getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers);

          Slice slice = createSlice(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1,
            SliceTypeEnum.RECONSTRUCTION,
            -25,
            zOffSet,
            0,
            FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
            false,
            useDynamicRangeOptimization,
            useMultipleStageSpeed); // Siew Yeng
          focusGroup.addSlice(slice);
        }
        else if (numberOBarrelSlices == 2)
        {
          // define the user-defined slice
          sliceNameEnum = SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1; 
          
          int zOffSet = getZOffset(jointType, SliceNameEnum.PAD, subtype, isTopSide, panelThicknessInNanometers) +
                        getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers);

          Slice slice = createSlice(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1,
            SliceTypeEnum.RECONSTRUCTION,
            -25,
            zOffSet,
            0,
            FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
            false,
            useDynamicRangeOptimization,
            useMultipleStageSpeed); //Siew Yeng
          focusGroup.addSlice(slice);

          // define the user-defined slice
          sliceNameEnum = SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2;
          
          zOffSet = getZOffset(jointType, SliceNameEnum.PAD, subtype, isTopSide, panelThicknessInNanometers) +
                    getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers);
          
          slice = createSlice(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2,
            SliceTypeEnum.RECONSTRUCTION,
            -25,
            zOffSet,
            0,
            FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
            false,
            useDynamicRangeOptimization,
            useMultipleStageSpeed); //Siew Yeng
          focusGroup.addSlice(slice);
        }
      }
      
      if ((jointType.equals(JointTypeEnum.GULLWING)
              || jointType.equals(JointTypeEnum.JLEAD)
              || jointType.equals(JointTypeEnum.SURFACE_MOUNT_CONNECTOR))
              && reconstructionRegion.isInspected())
      {
        Slice slice = createSlice(SliceNameEnum.SURFACE_MODELING,
                SliceTypeEnum.SURFACE,
                0,
                -2,
                0,
                FocusMethodEnum.SHARPEST,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
        focusGroup.addSlice(slice);
        
        int numberOBarrelSlices = 0;       
        if (jointType.equals(JointTypeEnum.SURFACE_MOUNT_CONNECTOR))
          numberOBarrelSlices = Algorithm.getNumberOfSlice(subtype);
        if(numberOBarrelSlices >= 2)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_2,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) - _SMT_CONNECTOR_ZOFFSET_FOUR_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
        
        if(numberOBarrelSlices >= 3)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_3,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) + _SMT_CONNECTOR_ZOFFSET_FOUR_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
         
        if(numberOBarrelSlices >= 4)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_4,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) - _SMT_CONNECTOR_ZOFFSET_EIGHT_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        } 
        
        if(numberOBarrelSlices >= 5)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_5,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) + _SMT_CONNECTOR_ZOFFSET_EIGHT_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
        
        if(numberOBarrelSlices >= 6)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_6,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) - _SMT_CONNECTOR_ZOFFSET_TWELVE_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
        
        if(numberOBarrelSlices >= 7)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_7,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) + _SMT_CONNECTOR_ZOFFSET_TWELVE_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
        
        if(numberOBarrelSlices >= 8)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_8,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) - _SMT_CONNECTOR_ZOFFSET_SIXTEEN_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
        
        if(numberOBarrelSlices >= 9)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_9,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) + _SMT_CONNECTOR_ZOFFSET_SIXTEEN_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
        
        if(numberOBarrelSlices >= 10)
        {
          slice = createSlice(SliceNameEnum.CONNECTOR_BARREL_10,
                SliceTypeEnum.RECONSTRUCTION,
                -25,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) - _SMT_CONNECTOR_ZOFFSET_TWENTY_MILS_IN_NANOS,
                0,
                FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                false,
                useDynamicRangeOptimization,
                useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
      }
      reconstructionRegion.addFocusGroup(focusGroup);
    }
  }

  /**
   * @author Peter Esbensen refactored from George David
   */
  private int getRelativeZOffsetForGridArrays(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);

    Pad pad = reconstructionRegion.getComponent().getPads().get(0);
    int pitch = pad.getPitchInNanoMeters();
    JointTypeEnum jointType = reconstructionRegion.getJointTypeEnum();
    int relativeZOffsetInNanometers;
    if (jointType.equals(JointTypeEnum.NON_COLLAPSABLE_BGA)
            || jointType.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      relativeZOffsetInNanometers = (int) Math.rint(0.3 * pitch);
    }
    else if (jointType.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
      relativeZOffsetInNanometers = (int) Math.rint(0.18 * pitch);
    }
    else if (jointType.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {
      relativeZOffsetInNanometers = (int) Math.rint(0.18 * pitch);
    }
    else
    {
      relativeZOffsetInNanometers = 0;
    }
    return relativeZOffsetInNanometers;
  }

  /**
   * @author Peter Esbensen modified to handle user defined sliceheights
   * @author George A. David
   */
  private void createFocusGroupsForExposedPads(ReconstructionRegion reconstructionRegion, List<java.awt.Shape> focusRegions)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(focusRegions != null);
    Assert.expect(focusRegions.size() == 1);

    final JointTypeEnum jointType = reconstructionRegion.getJointTypeEnum();
    final boolean isTopSide = reconstructionRegion.isTopSide();
    final int panelThicknessInNanometers = _project.getPanel().getThicknessInNanometers();
    final Subtype subtype = getSubtypeForUserDefinedSliceheights(reconstructionRegion);

    int zCurveWidthInNanoMeters = getZcurveWidthInNanoMeters(reconstructionRegion);

    //Siew Yeng - XCR-2140 - DRO
    boolean useDynamicRangeOptimization = false;
    if(subtype.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel() != DynamicRangeOptimizationLevelEnum.ONE)
      useDynamicRangeOptimization = true;
    
    //Kok Chun, Tan - MVEDR
    boolean useMultipleStageSpeed = false;
    if(subtype.getSubtypeAdvanceSettings().getStageSpeedList().size() > 1)
      useMultipleStageSpeed = true;
    
    // Bee Hoon
    int midBoardZoffset = UserDefinedSliceheights.getAutoFocusMidBoardOffsetInNanometers(subtype) + 
            (int)reconstructionRegion.getBoard().getBoardSettings().getBoardZOffsetFromMeansOffsetInNanometers();
    int userDefinedInitialWaveletLevel = subtype.getUserDefinedInitialWaveletLevel();
    int searchRangeLowLimitInNanometers = UserDefinedSliceheights.getSearchRangeLowLimitInNanometers(subtype);
    int searchRangeHighLimitInNanometers = UserDefinedSliceheights.getSearchRangeHighLimitInNanometers(subtype);
    int pspZOffsetInNanometers = UserDefinedSliceheights.getPspZOffsetInNanometers(subtype);
    
    // Wei Chin (RFB)
    if (reconstructionRegion.getComponent().getPadOne().getPadType().getPadTypeSettings().getGlobalSurfaceModel().equals(GlobalSurfaceModelEnum.USE_RFB))
    {
      FocusGroup focusGroup;
      int referencesZHeight;
      if (isTopSide)
      {
        referencesZHeight = (int) getZTop();
      }
      else
      {
        referencesZHeight = (int) getZBottom();
      }

      Float referencesOffset = 0.0f;
      // Wei CHin
      Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE);
      if (value instanceof Float)
      {
        referencesOffset = (Float) value;
      }

//      referencesZHeight = (int) (referencesZHeight + MathUtil.convertMillimetersToNanometers(referencesOffset));
      referencesZHeight = (int) (referencesZHeight + MathUtil.convertMillimetersToNanometers(referencesOffset))
        + reconstructionRegion.getBoard().getBoardSettings().getBoardZOffsetFromMeansOffsetInNanometers();
      

      SliceNameEnum sliceNameEnum = SliceNameEnum.PAD;

      focusGroup = createFocusGroup(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE,
                                               new PanelRectangle(focusRegions.get(0)),
                                               sliceNameEnum,
                                               SliceTypeEnum.RECONSTRUCTION,
                                               0,
                                               getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
                                               referencesZHeight,
                                               zCurveWidthInNanoMeters,
                                               FocusMethodEnum.USE_Z_HEIGHT,
                                               0,
                                               0,
                                               true,
                                               userDefinedInitialWaveletLevel,
                                               useDynamicRangeOptimization,
                                               searchRangeLowLimitInNanometers,
                                               searchRangeHighLimitInNanometers,
                                               pspZOffsetInNanometers,
                                               reconstructionRegion,
                                               useMultipleStageSpeed);
      reconstructionRegion.addFocusGroup(focusGroup);
    }
    // use normal auto focus
    else
    {
      SliceNameEnum sliceNameEnum = SliceNameEnum.PAD;

      FocusGroup focusGroup = createFocusGroup(FocusProfileShapeEnum.NO_SEARCH_PREDICTED_SURFACE,
                                               new PanelRectangle(focusRegions.get(0)),
                                               sliceNameEnum,
                                               SliceTypeEnum.RECONSTRUCTION,
                                               0,
                                               getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
                                               0,
                                               zCurveWidthInNanoMeters,
                                               FocusMethodEnum.SHARPEST,
                                               0,
                                               midBoardZoffset,
                                               true,
                                               userDefinedInitialWaveletLevel,
                                               useDynamicRangeOptimization,
                                               searchRangeLowLimitInNanometers,
                                               searchRangeHighLimitInNanometers,
                                               pspZOffsetInNanometers,
                                               reconstructionRegion,
                                               useMultipleStageSpeed);
      reconstructionRegion.addFocusGroup(focusGroup);
    }
  }

  /**
   * @author George A. David
   * @author Poh Kheng
   */
  private void createFocusGroupsForGridArrays(ReconstructionRegion reconstructionRegion, List<java.awt.Shape> focusRegions)
  {
    // figure out what joint type this is and set the relative z offset accordingly
    int relativeZOffsetInNanometers = getRelativeZOffsetForGridArrays(reconstructionRegion);
    
    final JointTypeEnum jointType = reconstructionRegion.getJointTypeEnum();
    final boolean isTopSide = reconstructionRegion.isTopSide();
    final int panelThicknessInNanometers = _project.getPanel().getThicknessInNanometers();
    final Subtype subtype = getSubtypeForUserDefinedSliceheights(reconstructionRegion);

    //Siew Yeng - XCR-2140 - DRO
    boolean useDynamicRangeOptimization = false;
    if(subtype.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel() != DynamicRangeOptimizationLevelEnum.ONE)
      useDynamicRangeOptimization = true;
    
    //Kok Chun, Tan - MVEDR
    boolean useMultipleStageSpeed = false;
    if(subtype.getSubtypeAdvanceSettings().getStageSpeedList().size() > 1)
      useMultipleStageSpeed = true;
    
    // Bee Hoon
    int midBoardZoffset = UserDefinedSliceheights.getAutoFocusMidBoardOffsetInNanometers(subtype) + 
            (int)reconstructionRegion.getBoard().getBoardSettings().getBoardZOffsetFromMeansOffsetInNanometers();
    int userDefinedInitialWaveletLevel = subtype.getUserDefinedInitialWaveletLevel();
    int searchRangeLowLimitInNanometers = UserDefinedSliceheights.getSearchRangeLowLimitInNanometers(subtype);
    int searchRangeHighLimitInNanometers = UserDefinedSliceheights.getSearchRangeHighLimitInNanometers(subtype);
    int pspZOffsetInNanometers = UserDefinedSliceheights.getPspZOffsetInNanometers(subtype);
    
    // Wei Chin (RFB)
    if (reconstructionRegion.getComponent().getPadOne().getPadType().getPadTypeSettings().getGlobalSurfaceModel().equals(GlobalSurfaceModelEnum.USE_RFB))
    {
      FocusGroup focusGroup = createFocusGroupsForGridArraysOrThroughHoles(focusRegions, 
                                                                           0, 
                                                                           midBoardZoffset, 
                                                                           userDefinedInitialWaveletLevel, 
                                                                           searchRangeLowLimitInNanometers, 
                                                                           searchRangeHighLimitInNanometers, 
                                                                           pspZOffsetInNanometers, 
                                                                           reconstructionRegion);
      int referencesZHeight;
      if (isTopSide)
      {
        referencesZHeight = (int) getZTop();
      }
      else
      {
        referencesZHeight = (int) getZBottom();
      }

      Float referencesOffset = 0.0f;
      // Wei CHin
      Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE);
      if (value instanceof Float)
      {
        referencesOffset = (Float) value;
      }

//      referencesZHeight = (int) (referencesZHeight + MathUtil.convertMillimetersToNanometers(referencesOffset));
      referencesZHeight = (int) (referencesZHeight + MathUtil.convertMillimetersToNanometers(referencesOffset))
        + reconstructionRegion.getBoard().getBoardSettings().getBoardZOffsetFromMeansOffsetInNanometers();

      // set the focus profile shape depending on whether this is CGA or not
      FocusSearchParameters focusSearchParameters = focusGroup.getFocusSearchParameters();
      focusSearchParameters.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);

      // set the z curve width
      int zCurveWidthInNanoMeters = getZcurveWidthInNanoMeters(reconstructionRegion);
      focusSearchParameters.setZCurveWidthInNanoMeters(zCurveWidthInNanoMeters);

      // define the pad slice
      FocusInstruction focusInstruction = null;
      if(jointType.equals(JointTypeEnum.CGA))
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight, 0);
      else
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight, 0);
      Assert.expect(focusInstruction != null);

      SliceNameEnum sliceNameEnum = SliceNameEnum.PAD;
      focusInstruction.setZOffsetInNanoMeters( getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      Slice slice = createSlice(focusInstruction, sliceNameEnum, true, useDynamicRangeOptimization, useMultipleStageSpeed); // Siew Yeng
      focusGroup.addSlice(slice);

      if(jointType.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
      {
        sliceNameEnum = SliceNameEnum.BGA_LOWERPAD_ADDITIONAL;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
          getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed); //Siew Yeng
        focusGroup.addSlice(slice);
      }
      // define the midball slice
      sliceNameEnum = SliceNameEnum.MIDBALL;
      focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                                              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed); //Siew Yeng
      focusGroup.addSlice(slice);
      
      // Chnee Khang Wah, 2012-02-21, 2.5D
      // add a projection slice for MIDBALL slice
      try
      {
        //if (_isGenerateMultiAngleImagesEnabled && Config.isDeveloperDebugModeOn() && LicenseManager.getInstance().isDeveloperSystemEnabled())
        if (_isGenerateMultiAngleImagesEnabled)
        {
          createProjectionSliceForFocusGroup(focusGroup, 0,
                                             getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
                                             referencesZHeight, FocusMethodEnum.USE_Z_HEIGHT, false);
        }
      }
      catch(Exception e)
      {
        //do nothing
      }

      // define the package slice
      sliceNameEnum = SliceNameEnum.PACKAGE;
      focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                                              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed); // Siew Yeng
      focusGroup.addSlice(slice);

      if (jointType.equals(JointTypeEnum.CGA))
      {
        // define the high short slice
        sliceNameEnum = SliceNameEnum.HIGH_SHORT;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                                                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed); //Siew Yeng
        focusGroup.addSlice(slice);
      }
      
      // Handle User-Defined-Slice
      if (jointType.equals(JointTypeEnum.COLLAPSABLE_BGA) ||
          jointType.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) ||
          jointType.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) ||
          jointType.equals(JointTypeEnum.CGA))
      {
        int numberOBarrelSlices = Algorithm.getNumberOfSlice(subtype);
        int gridArrayUserDefinedSlice1EnumIndex = SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1.getId();
        // define the user-defined slice
        //Siew Yeng - XCR-3143 - fix recipe hang
        for (int enumIndexCounter = 0; enumIndexCounter < numberOBarrelSlices; enumIndexCounter++)
        {
          sliceNameEnum = SliceNameEnum.getEnumFromIndex(gridArrayUserDefinedSlice1EnumIndex + enumIndexCounter);

          int zOffSet = getZOffset(jointType, SliceNameEnum.MIDBALL, subtype, isTopSide, panelThicknessInNanometers)
            + getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers);

          focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight, zOffSet);
          slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
      }

      reconstructionRegion.addFocusGroup(focusGroup);

      // add the surface modeling slice
      if((jointType.equals(JointTypeEnum.COLLAPSABLE_BGA) ||
         jointType.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) ||
         jointType.equals(JointTypeEnum.NON_COLLAPSABLE_BGA)) &&
         reconstructionRegion.isInspected())
      {
        slice = createSlice(SliceNameEnum.SURFACE_MODELING,
                            SliceTypeEnum.SURFACE,
                            0,
                            0,
                            referencesZHeight,
                            FocusMethodEnum.USE_Z_HEIGHT,
                            false,
                            useDynamicRangeOptimization,
                            useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
    }
    else
    {
      FocusGroup focusGroup = createFocusGroupsForGridArraysOrThroughHoles(focusRegions, 
                                                                           relativeZOffsetInNanometers,
                                                                           midBoardZoffset, 
                                                                           userDefinedInitialWaveletLevel,
                                                                           searchRangeLowLimitInNanometers,
                                                                           searchRangeHighLimitInNanometers,
                                                                           pspZOffsetInNanometers,
                                                                           reconstructionRegion);

      //Swee Yee Wong - user defined midball to edge offset
      Float midballToEdgeOffset = 0.0f;
      int midballToEdgeOffsetInNanometer = 0;

      if(subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_DEFINE_OFFSET_FOR_PAD_AND_PACKAGE).toString().matches("On"))
      {
        Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_MIDBALL_TO_EDGE_SLICEHEIGHT);
        if (value instanceof Float)
        {
          midballToEdgeOffset = (Float) value;
        }

        // if this offset value is not learnt (equal to offset value, 0.0f), learn and get midball to edge offset value for this subtype
        if (midballToEdgeOffset == 0.0f)
        {
          Algorithm gridArrayMeasurementAlgorithm = InspectionFamily.getAlgorithm(InspectionFamilyEnum.GRID_ARRAY, AlgorithmEnum.MEASUREMENT);
          gridArrayMeasurementAlgorithm.learnAndUpdateAlgorithmSettingIfNecessaryDuringProgramGeneration(subtype, AlgorithmSettingEnum.USER_DEFINED_MIDBALL_TO_EDGE_SLICEHEIGHT);

          //after learnt, get this value again
          value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_MIDBALL_TO_EDGE_SLICEHEIGHT);
          if (value instanceof Float)
          {
            midballToEdgeOffset = (Float) value;
          }
        }
        midballToEdgeOffsetInNanometer = Math.round(MathUtil.convertMillimetersToNanometers(midballToEdgeOffset));
      }
	  
      // set the focus profile shape depending on whether this is CGA or not
      FocusSearchParameters focusSearchParameters = focusGroup.getFocusSearchParameters();
      if(jointType.equals(JointTypeEnum.CGA))
        focusSearchParameters.setFocusProfileShape(FocusProfileShapeEnum.EDGE);
      else
        focusSearchParameters.setFocusProfileShape(FocusProfileShapeEnum.GAUSSIAN);

      // set the z curve width
      int zCurveWidthInNanoMeters = getZcurveWidthInNanoMeters(reconstructionRegion);
      focusSearchParameters.setZCurveWidthInNanoMeters(zCurveWidthInNanoMeters);

      if(subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_DEFINE_OFFSET_FOR_PAD_AND_PACKAGE).toString().matches("On"))
      {
        // define the pad slice
        FocusInstruction focusInstruction = null;
        focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, 0, 0, 0);

        Assert.expect(focusInstruction != null);

        SliceNameEnum sliceNameEnum = SliceNameEnum.PAD;
        focusInstruction.setZOffsetInNanoMeters(
          getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) - midballToEdgeOffsetInNanometer);
        Slice slice = createSlice(focusInstruction, sliceNameEnum, true, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);

        if (jointType.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
        {
          sliceNameEnum = SliceNameEnum.BGA_LOWERPAD_ADDITIONAL;
          focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, 0, 0,
            getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) - midballToEdgeOffsetInNanometer);
          slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
      }
      else
      {
        // define the pad slice
        FocusInstruction focusInstruction = null;
        if (jointType.equals(JointTypeEnum.CGA))
        {
          focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, 0, 0, 0);
        }
        else
        {
          focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, -100, 0, 0);
        }
        Assert.expect(focusInstruction != null);

        SliceNameEnum sliceNameEnum = SliceNameEnum.PAD;
        focusInstruction.setZOffsetInNanoMeters(
          getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        Slice slice = createSlice(focusInstruction, sliceNameEnum, true, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);

        if (jointType.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
        {
          sliceNameEnum = SliceNameEnum.BGA_LOWERPAD_ADDITIONAL;
          focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, -100, 0,
            getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
          slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
      }
      // define the midball slice
      SliceNameEnum sliceNameEnum = SliceNameEnum.MIDBALL;
      FocusInstruction focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, 0, 0,
                                              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      Slice slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
      focusGroup.addSlice(slice);
      
      // Chnee Khang Wah, 2012-02-21, 2.5D
      // add a projection slice for MIDBALL slice
      try
      {
        //if (_isGenerateMultiAngleImagesEnabled && Config.isDeveloperDebugModeOn() && LicenseManager.getInstance().isDeveloperSystemEnabled())
        if (_isGenerateMultiAngleImagesEnabled)
        {
          createProjectionSliceForFocusGroup(focusGroup, 0,
                                             getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
                                             0, FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, false);
        }
      }
      catch(Exception e)
      {
        // do nothing
      }

      if(subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_DEFINE_OFFSET_FOR_PAD_AND_PACKAGE).toString().matches("On"))
      {
        // define the package slice
        sliceNameEnum = SliceNameEnum.PACKAGE;
        focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, 0, 0,
          getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) + midballToEdgeOffsetInNanometer);
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);

        if (jointType.equals(JointTypeEnum.CGA))
        {
          // define the high short slice
          sliceNameEnum = SliceNameEnum.HIGH_SHORT;
          focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, 0, 0,
            getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers) + midballToEdgeOffsetInNanometer);
          slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
      }
      else
      {
        // define the package slice
        sliceNameEnum = SliceNameEnum.PACKAGE;
        focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, 100, 0,
          getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);

        if (jointType.equals(JointTypeEnum.CGA))
        {
          // define the high short slice
          sliceNameEnum = SliceNameEnum.HIGH_SHORT;
          focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, 100, 0,
            getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
          slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
      }
      
      // Handle User-Defined-Slice
      if (jointType.equals(JointTypeEnum.COLLAPSABLE_BGA) ||
          jointType.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) ||
          jointType.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) ||
          jointType.equals(JointTypeEnum.CGA))
      {
        //Siew Yeng - XCR1745 - Semi-automated mode
        if(TestExecution.getInstance().isSemiAutomatedModeEnabled() && _project.isSemiAutomatedRecipe())
        {
          subtype.setTunedValue(AlgorithmSettingEnum.GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE, new String("2"));
        }
        
        int numberOBarrelSlices = Algorithm.getNumberOfSlice(subtype);
        if (numberOBarrelSlices == 1)
        {
          // define the user-defined slice
          sliceNameEnum = SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1;
          focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, 0, 0,
                                                  getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
          slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
          focusGroup.addSlice(slice);
        }
        else if (numberOBarrelSlices == 2)
        {
          //Siew Yeng - XCR1745 - Semi-automated mode
          if(TestExecution.getInstance().isSemiAutomatedModeEnabled() && _project.isSemiAutomatedRecipe())          
          {
            // define the user-defined slice
            sliceNameEnum = SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1;
            focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, -50, 0,
                                                    getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
            slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
            focusGroup.addSlice(slice);
            
            // define the user-defined slice
            sliceNameEnum = SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2;
            focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, 50, 0,
                                                    getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
            slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
            focusGroup.addSlice(slice);
          }
          else
          {
            // define the user-defined slice
            sliceNameEnum = SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1;
            focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, 0, 0,
                                                    getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
            slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
            focusGroup.addSlice(slice);
            
            // define the user-defined slice
            sliceNameEnum = SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2;
            focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, 0, 0,
                                                    getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
            slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
            focusGroup.addSlice(slice);
          }
        }
        else
        {
          int gridArrayUserDefinedSlice1EnumIndex = SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1.getId();
          // define the user-defined slice
          //Siew Yeng - XCR-3143 - fix recipe hang
          for (int enumIndexCounter = 0; enumIndexCounter < numberOBarrelSlices; enumIndexCounter++)
          {
            sliceNameEnum = SliceNameEnum.getEnumFromIndex(gridArrayUserDefinedSlice1EnumIndex + enumIndexCounter);

            focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE, 0, 0,
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
            slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
            focusGroup.addSlice(slice);
          }
        }
      }

      reconstructionRegion.addFocusGroup(focusGroup);

      // add the surface modeling slice
      if((jointType.equals(JointTypeEnum.COLLAPSABLE_BGA) ||
         jointType.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) ||
         jointType.equals(JointTypeEnum.NON_COLLAPSABLE_BGA)) &&
         reconstructionRegion.isInspected())
      {
        slice = createSlice(SliceNameEnum.SURFACE_MODELING,
                            SliceTypeEnum.SURFACE,
                            -100,
                            0,
                            0,
                            FocusMethodEnum.PERCENT_FROM_SHARPEST_TO_EDGE,
                            false,
                            useDynamicRangeOptimization,
                            useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
    }
  }

  /**
   * @author Peter Esbensen
   * @Edited By Kee Chin Seong
   *            - If there is Multiple Subtype with one region inside,
   *              remove it until left one subtype inside.
   */
  private Subtype getSubtypeForUserDefinedSliceheights(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    Collection<Subtype> subtypes = reconstructionRegion.getSubtypes();

    if(subtypes.size() > 1)
    {
      subtypes.iterator().next();
      Iterator T = subtypes.iterator();
      while(T.hasNext()) 
      {
        if(subtypes.size() == 1)
          break;
        T.next();
        T.remove();
      }
      for(Pad pad : reconstructionRegion.getComponent().getPads())
      {
        pad.getPadTypeSettings().setSubtype(subtypes.iterator().next());
      }
    }
    Assert.expect(subtypes.size() == 1, "cannot have multiple subtypes per region with user defined sliceheights");
    Subtype subtype = subtypes.iterator().next();
    return subtype;
  }

  /**
   * @author Peter Esbensen - modified to handle user defined sliceheights
   * @author George A. David
   */
  private void createFocusGroupsForThroughHoles(ReconstructionRegion reconstructionRegion, List < java.awt.Shape > focusRegions)
  {
    final JointTypeEnum jointType = reconstructionRegion.getJointTypeEnum();
    final boolean isTopSide = reconstructionRegion.isTopSide();
    final int panelThicknessInNanometers = _project.getPanel().getThicknessInNanometers();
    final Subtype subtype = getSubtypeForUserDefinedSliceheights(reconstructionRegion);
    
    //Siew Yeng - XCR-2140 - DRO
    boolean useDynamicRangeOptimization = false;
    if(subtype.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel() != DynamicRangeOptimizationLevelEnum.ONE)
      useDynamicRangeOptimization = true;
    
    //Kok Chun, Tan - MVEDR
    boolean useMultipleStageSpeed = false;
    if(subtype.getSubtypeAdvanceSettings().getStageSpeedList().size() > 1)
      useMultipleStageSpeed = true;
    
    // Bee Hoon
    int midBoardZoffset = UserDefinedSliceheights.getAutoFocusMidBoardOffsetInNanometers(subtype) + 
            (int)reconstructionRegion.getBoard().getBoardSettings().getBoardZOffsetFromMeansOffsetInNanometers();
    int userDefinedInitialWaveletLevel = subtype.getUserDefinedInitialWaveletLevel();
    int searchRangeLowLimitInNanometers = UserDefinedSliceheights.getSearchRangeLowLimitInNanometers(subtype);
    int searchRangeHighLimitInNanometers = UserDefinedSliceheights.getSearchRangeHighLimitInNanometers(subtype);
    int pspZOffsetInNanometers = UserDefinedSliceheights.getPspZOffsetInNanometers(subtype);
    
    FocusGroup focusGroup = createFocusGroupsForGridArraysOrThroughHoles(focusRegions, 
                                                                         0, 
                                                                         midBoardZoffset, 
                                                                         userDefinedInitialWaveletLevel, 
                                                                         searchRangeLowLimitInNanometers, 
                                                                         searchRangeHighLimitInNanometers,
                                                                         pspZOffsetInNanometers,
                                                                         reconstructionRegion);

    int zCurveWidthInNanoMeters = getZcurveWidthInNanoMeters(reconstructionRegion);
    FocusSearchParameters focusSearchParameters = focusGroup.getFocusSearchParameters();
    focusSearchParameters.setFocusProfileShape(FocusProfileShapeEnum.SQUARE_WAVE);
    focusSearchParameters.setZCurveWidthInNanoMeters(zCurveWidthInNanoMeters);


    // Wei Chin (RFB)
    if (reconstructionRegion.getComponent().getPadOne().getPadType().getPadTypeSettings().getGlobalSurfaceModel().equals(GlobalSurfaceModelEnum.USE_RFB))
    {
      int referencesZHeight;
      // exception in PTH (Pin Side is opposide)
      if (isTopSide)
      {
        referencesZHeight = (int) getZBottom();
      }
      else
      {
        referencesZHeight = (int) getZTop();
      }

      Float referencesOffset = 0.0f;
      // Wei CHin
      Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE);
      if (value instanceof Float)
      {
        referencesOffset = (Float) value;
      }

//      referencesZHeight = (int) (referencesZHeight + MathUtil.convertMillimetersToNanometers(referencesOffset));
      referencesZHeight = (int) (referencesZHeight + MathUtil.convertMillimetersToNanometers(referencesOffset))
        + reconstructionRegion.getBoard().getBoardSettings().getBoardZOffsetFromMeansOffsetInNanometers();

      FocusInstruction focusInstruction;
      // define the Component slice
      SliceNameEnum sliceNameEnum = SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE;
      focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      Slice slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
      focusGroup.addSlice(slice);

      // define the Protrusion slice
      sliceNameEnum = SliceNameEnum.THROUGHHOLE_PROTRUSION;
      focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
      focusGroup.addSlice(slice);

      // define the Insertion slice
      sliceNameEnum = SliceNameEnum.THROUGHHOLE_INSERTION;
      focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
      focusGroup.addSlice(slice);

      // define the Barrel slice
      sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL;
      focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
      focusGroup.addSlice(slice);
      
      // Chnee Khang Wah, 2012-12-17, 2.5D
      // add a projection slice for BARREL Slice 1
      try
      {
        //if (_isGenerateMultiAngleImagesEnabled && Config.isDeveloperDebugModeOn() && LicenseManager.isDeveloperSystemEnabled())
		if (_isGenerateMultiAngleImagesEnabled)	
        {
          createProjectionSliceForFocusGroup(focusGroup, 0,
                                             getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
                                             referencesZHeight, FocusMethodEnum.USE_Z_HEIGHT, false);
        }
      }
      catch(Exception e)
      {
        // do nothing
      }

      int numberOBarrelSlices = Algorithm.getNumberOfSlice(subtype);
      
      if(numberOBarrelSlices >= 2)
      {
         // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_2;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }

      if(numberOBarrelSlices >= 3)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_3;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
      if(numberOBarrelSlices >= 4)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_4;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }

      if(numberOBarrelSlices >= 5)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_5;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
      
      if(numberOBarrelSlices >= 6)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_6;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
      
      if(numberOBarrelSlices >= 7)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_7;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
      
      if(numberOBarrelSlices >= 8)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_8;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }

      if(numberOBarrelSlices >= 9)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_9;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
      
      if(numberOBarrelSlices >= 10)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_10;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
      
      // define the Pin slice
      sliceNameEnum = SliceNameEnum.THROUGHHOLE_PIN_SIDE;
      focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
      focusGroup.addSlice(slice);

      reconstructionRegion.addFocusGroup(focusGroup);
    }
    else
    {
      FocusInstruction focusInstruction;
      // define the Component slice
      SliceNameEnum sliceNameEnum = SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE;
      focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_BETWEEN_EDGES, 0, 0,
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      Slice slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
      focusGroup.addSlice(slice);

      // define the Protrusion slice
      sliceNameEnum = SliceNameEnum.THROUGHHOLE_PROTRUSION;
      focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_BETWEEN_EDGES, 0, 0,
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
      focusGroup.addSlice(slice);

      // define the Insertion slice
      sliceNameEnum = SliceNameEnum.THROUGHHOLE_INSERTION;
      focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_BETWEEN_EDGES, 0, 0,
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
      focusGroup.addSlice(slice);

      // define the Barrel slice
      sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL;
      focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_BETWEEN_EDGES, 0, 0,
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
      focusGroup.addSlice(slice);
      
      // Chnee Khang Wah, 2012-12-17, 2.5D
      // add a projection slice for BARREL Slice 1
      try
      {
        //if (_isGenerateMultiAngleImagesEnabled && Config.isDeveloperDebugModeOn() && LicenseManager.isDeveloperSystemEnabled())
		if (_isGenerateMultiAngleImagesEnabled)
        {
          createProjectionSliceForFocusGroup(focusGroup, 0,
                                             getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers),
                                             0, FocusMethodEnum.PERCENT_BETWEEN_EDGES, false);
        }
      }
      catch(Exception e)
      {
        // do nothing
      }

      int numberOBarrelSlices = Algorithm.getNumberOfSlice(subtype);
      
      if(numberOBarrelSlices >= 2)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_2;
        focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_BETWEEN_EDGES, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
      
      if(numberOBarrelSlices >= 3)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_3;
        focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_BETWEEN_EDGES, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
      
      if(numberOBarrelSlices >= 4)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_4;
        focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_BETWEEN_EDGES, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
      
      if(numberOBarrelSlices >= 5)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_5;
        focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_BETWEEN_EDGES, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
      
      if(numberOBarrelSlices >= 6)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_6;
        focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_BETWEEN_EDGES, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
      
      if(numberOBarrelSlices >= 7)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_7;
        focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_BETWEEN_EDGES, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
      
      if(numberOBarrelSlices >= 8)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_8;
        focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_BETWEEN_EDGES, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
      
      if(numberOBarrelSlices >= 9)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_9;
        focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_BETWEEN_EDGES, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
      
      if(numberOBarrelSlices >= 10)
      {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_10;
        focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_BETWEEN_EDGES, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
        focusGroup.addSlice(slice);
      }
      
      // define the Pin slice
      sliceNameEnum = SliceNameEnum.THROUGHHOLE_PIN_SIDE;
      focusInstruction = new FocusInstruction(FocusMethodEnum.PERCENT_BETWEEN_EDGES, 0, 0,
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);
      focusGroup.addSlice(slice);

      reconstructionRegion.addFocusGroup(focusGroup);
    }
  }

  /**
   *
   * @author John Heumann
   * @author Patrick Lacz
   */
  private void createFocusGroupsForPressfits(ReconstructionRegion reconstructionRegion, List < java.awt.Shape > focusRegions)
  {
    final JointTypeEnum jointType = reconstructionRegion.getJointTypeEnum();
    final boolean isTopSide = reconstructionRegion.isTopSide();
    final int panelThicknessInNanometers = _project.getPanel().getThicknessInNanometers();
    final Subtype subtype = getSubtypeForUserDefinedSliceheights(reconstructionRegion);
    
    //Siew Yeng - XCR-2140 - DRO
    boolean useDynamicRangeOptimization = false;
    if(subtype.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel() != DynamicRangeOptimizationLevelEnum.ONE)
      useDynamicRangeOptimization = true;
    
    //Kok Chun, Tan - MVEDR
    boolean useMultipleStageSpeed = false;
    if(subtype.getSubtypeAdvanceSettings().getStageSpeedList().size() > 1)
      useMultipleStageSpeed = true;

    // Bee Hoon
    int midBoardZoffset = UserDefinedSliceheights.getAutoFocusMidBoardOffsetInNanometers(subtype) + 
            (int)reconstructionRegion.getBoard().getBoardSettings().getBoardZOffsetFromMeansOffsetInNanometers();
    int userDefinedInitialWaveletLevel = subtype.getUserDefinedInitialWaveletLevel();
    int searchRangeLowLimitInNanometers = UserDefinedSliceheights.getSearchRangeLowLimitInNanometers(subtype);
    int searchRangeHighLimitInNanometers = UserDefinedSliceheights.getSearchRangeHighLimitInNanometers(subtype);
    int pspZOffsetInNanometers = UserDefinedSliceheights.getPspZOffsetInNanometers(subtype);
    
    FocusGroup focusGroup = createFocusGroupsForGridArraysOrThroughHoles(focusRegions, 
                                                                         0, 
                                                                         midBoardZoffset, 
                                                                         userDefinedInitialWaveletLevel, 
                                                                         searchRangeLowLimitInNanometers, 
                                                                         searchRangeHighLimitInNanometers,
                                                                         pspZOffsetInNanometers,
                                                                         reconstructionRegion);

    int zCurveWidthInNanoMeters = getZcurveWidthInNanoMeters(reconstructionRegion);
    FocusSearchParameters focusSearchParameters = focusGroup.getFocusSearchParameters();
    focusSearchParameters.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_PREDICTED_SURFACE);
    focusSearchParameters.setZCurveWidthInNanoMeters(zCurveWidthInNanoMeters);

    // Wei Chin (RFB)
    if (reconstructionRegion.getComponent().getPadOne().getPadType().getPadTypeSettings().getGlobalSurfaceModel().equals(GlobalSurfaceModelEnum.USE_RFB))
    {
      int referencesZHeight;
      if (isTopSide)
      {
        referencesZHeight = (int) getZTop();
      }
      else
      {
        referencesZHeight = (int) getZBottom();
      }

      Float referencesOffset = 0.0f;

      // Wei CHin
      Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE);
      if (value instanceof Float)
      {
        referencesOffset = (Float) value;
      }
//      referencesZHeight = (int) (referencesZHeight + MathUtil.convertMillimetersToNanometers(referencesOffset));
      referencesZHeight = (int) (referencesZHeight + MathUtil.convertMillimetersToNanometers(referencesOffset))
        + reconstructionRegion.getBoard().getBoardSettings().getBoardZOffsetFromMeansOffsetInNanometers();

      focusSearchParameters.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
      FocusInstruction focusInstruction;
      // set up the Component slice
      SliceNameEnum sliceNameEnum = SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE;
      focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                                              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      Slice slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed); // Siew Yeng
      focusGroup.addSlice(slice);

      // set up the Barrel slice
      sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL;
      focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed); // Siew Yeng
      focusGroup.addSlice(slice);

     int numberOfSlices = Algorithm.getNumberOfSlice(subtype);
     
     // define the Barrel slice = test
     if(numberOfSlices >= 2)
     {
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_2;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);
     }
     
     if(numberOfSlices >=3)
     {
       // define the Barrel slice = test
      sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_3;
      focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
      focusGroup.addSlice(slice);       
     }
      
     if(numberOfSlices >=4)
     {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_4;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);
     }
     
     if(numberOfSlices >=5)
     {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_5;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);
     }
     
     if(numberOfSlices >=6)
     {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_6;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);
     }
     
     if(numberOfSlices >=7)
     {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_7;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);
     }
     
     if(numberOfSlices >=8)
     {       
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_8;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);      
     }
     
     if(numberOfSlices >=9)
     {       
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_9;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);      
     }
     
     if(numberOfSlices >=10)
     {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_10;
        focusInstruction = new FocusInstruction(FocusMethodEnum.USE_Z_HEIGHT, 0, referencesZHeight,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);
     }
      // add the focus group to the reconstruction region
      reconstructionRegion.addFocusGroup(focusGroup);
    }
    else
    {
      int numberOfSlices = Algorithm.getNumberOfSlice(subtype);
      
      FocusInstruction focusInstruction;
      // set up the Component slice
      SliceNameEnum sliceNameEnum = SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE;
      focusInstruction = new FocusInstruction(FocusMethodEnum.SHARPEST, 0, 0,
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      Slice slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
      focusGroup.addSlice(slice);

      // set up the Barrel slice
      sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL;
      focusInstruction = new FocusInstruction(FocusMethodEnum.SHARPEST, 0, 0,
              getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
      slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
      focusGroup.addSlice(slice);

     if(numberOfSlices >=2)
     { 
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_2;
        focusInstruction = new FocusInstruction(FocusMethodEnum.SHARPEST, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);
     }
     
     if(numberOfSlices >=3)
     { 
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_3;
        focusInstruction = new FocusInstruction(FocusMethodEnum.SHARPEST, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);
     }
     
     if(numberOfSlices >=4)
     { 
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_4;
        focusInstruction = new FocusInstruction(FocusMethodEnum.SHARPEST, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);
     }
     
     if(numberOfSlices >=5)
     {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_5;
        focusInstruction = new FocusInstruction(FocusMethodEnum.SHARPEST, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);
     }
     
     if(numberOfSlices >=6)
     { 
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_6;
        focusInstruction = new FocusInstruction(FocusMethodEnum.SHARPEST, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);      
     }
     
     if(numberOfSlices >=7)
     {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_7;
        focusInstruction = new FocusInstruction(FocusMethodEnum.SHARPEST, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);
     }
     
     if(numberOfSlices >=8)
     { 
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_8;
        focusInstruction = new FocusInstruction(FocusMethodEnum.SHARPEST, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);
     }
     
     if(numberOfSlices >=9)
     {
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_9;
        focusInstruction = new FocusInstruction(FocusMethodEnum.SHARPEST, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);
     }
     
     if(numberOfSlices >=10)
     { 
        // define the Barrel slice = test
        sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL_10;
        focusInstruction = new FocusInstruction(FocusMethodEnum.SHARPEST, 0, 0,
                getZOffset(jointType, sliceNameEnum, subtype, isTopSide, panelThicknessInNanometers));
        slice = createSlice(focusInstruction, sliceNameEnum, false, useDynamicRangeOptimization, useMultipleStageSpeed);// Siew Yeng
        focusGroup.addSlice(slice);
     }
      // add the focus group to the reconstruction region
      reconstructionRegion.addFocusGroup(focusGroup);
    }
  }

  /**
   * @author Wei Chin on behalf of Lee Herng
   */
  private FocusGroup createFocusGroupsForGridArraysOrThroughHoles(
          List<java.awt.Shape> focusRegions, 
          int relativeZOffsetInNanoMeters, 
          int midboardOffsetInNanometer,
          int userDefinedWaveletLevel,
          int searchRangeLowLimitInNanometers,
          int searchRangeHighLimitInNanometers,
          int pspZOffsetInNanometers,
          ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(focusRegions != null);
    Assert.expect(focusRegions.size() == 1);

    FocusGroup focusGroup = new FocusGroup();
    FocusSearchParameters focusSearchParameters = new FocusSearchParameters();
    FocusRegion focusRegion = new FocusRegion();
    focusRegion.setRectangleRelativeToPanelInNanoMeters(new PanelRectangle(focusRegions.get(0)));
    focusSearchParameters.setFocusRegion(focusRegion);
    focusSearchParameters.setInitialWaveletLevel(userDefinedWaveletLevel);
    focusGroup.setFocusSearchParameters(focusSearchParameters);
    focusGroup.setRelativeZoffsetFromSurfaceInNanoMeters(relativeZOffsetInNanoMeters);
    focusGroup.setAutoFocusMidBoardOffsetInNanometers(midboardOffsetInNanometer);
    focusGroup.setSearchRangeLowLimitInNanometers(searchRangeLowLimitInNanometers);
    focusGroup.setSearchRangeHighLimitInNanometers(searchRangeHighLimitInNanometers);
    
    // XCR-3663 PSP Z-Offset
    if (reconstructionRegion.getComponent().isTopSide())
      pspZOffsetInNanometers = -pspZOffsetInNanometers;
    
    focusGroup.setPspZOffsetInNanometers(pspZOffsetInNanometers);
    
    final ComponentType componentType = reconstructionRegion.getComponent().getComponentType();

    POPLayerIdEnum popLayerId = POPLayerIdEnum.BASE;
    int totalPOPLayerNumber = _DEFAULT_TOTAL_POP_LAYER_NUMBER;
    int popZHeightInNanometers = _DEFAULT_POP_ZHEIGHT_IN_NANOMETERS;
    
    if (componentType.hasCompPackageOnPackage())
    {
      popLayerId = componentType.getPOPLayerId();
      popZHeightInNanometers = componentType.getPOPZHeightInNanometers();
      totalPOPLayerNumber = componentType.getCompPackageOnPackage().getTotalPOPLayerNumber();
      
      if (componentType.getPOPLayerId().equals(POPLayerIdEnum.BASE))
      {
        for (ComponentType compType : componentType.getCompPackageOnPackage().getComponentTypes())
        {
          if (compType.getPOPLayerId().equals(POPLayerIdEnum.ONE)) 
          {
            popZHeightInNanometers = compType.getPOPZHeightInNanometers();  
            break;
          }
        }
      } 
    } 
    
    focusGroup.setPOPLayerNumber(popLayerId);
    focusGroup.setPOPTotalLayerNumber(totalPOPLayerNumber);
    focusGroup.setPOPZHeightInNanometers(popZHeightInNanometers);
    
    return focusGroup;
  }

  /**
   * Checks if the pads for a grid array component passes the neighbor rule
   * The neighbor rule states that all pads of a reconstruction region
   * must have at least for neighbors. A neighbor is a pad that is not
   * too far from it. (the distance hasn' t yet been decided, but I will
   * use twice the pitch for now.
   * @author George A. David
   */
  private boolean passesGridArrayLandPatternPadsNeighborRule(Set<LandPatternPad> landPatternPads)
  {
    Assert.expect(landPatternPads != null);

    boolean passesRule = false;
    Map<LandPatternPad, Integer> padToNumNeighborsMap = new HashMap<LandPatternPad, Integer>();

    // if there aren't at least 4 pads, don' t even bother
    // checking, it WILL fail.
    if (landPatternPads.size() >= _MIN_NEIGHBORS_FOR_GRID_ARRAY_RECONSTRUCTION_REGION + 1)
    {
      List<LandPatternPad> neighborLandPatternPads = new LinkedList<LandPatternPad>(landPatternPads);
      // for each pad, find 3 close neighbors
      int numNeighbors = 0;
      for (LandPatternPad landPatternPad : landPatternPads)
      {
        int maxDistance = 3 * (landPatternPad.getInterPadDistanceInNanoMeters() + landPatternPad.getWidthInNanoMeters());
        ComponentCoordinate padCoord = landPatternPad.getCenterCoordinateInNanoMeters();
        numNeighbors = 0;
        if(padToNumNeighborsMap.containsKey(landPatternPad))
        {
          numNeighbors = padToNumNeighborsMap.get(landPatternPad);
        }
        if (numNeighbors <_MIN_NEIGHBORS_FOR_GRID_ARRAY_RECONSTRUCTION_REGION) // no need to continue checking
        {
          boolean isPadProcessed = true;
          for (LandPatternPad neighborLandPatternPad : neighborLandPatternPads)
          {
            if(isPadProcessed)
            {
              if(neighborLandPatternPad == landPatternPad)
                isPadProcessed = false;
            }
            else
            {
              ComponentCoordinate neighborCoord = neighborLandPatternPad.getCenterCoordinateInNanoMeters();
              double distance = MathUtil.calculateDistance(padCoord.getX(),
                  padCoord.getY(),
                  neighborCoord.getX(),
                  neighborCoord.getY());
              if (MathUtil.fuzzyLessThanOrEquals(distance, maxDistance))
              {
                ++numNeighbors;
                padToNumNeighborsMap.put(landPatternPad, numNeighbors);

                int neighborsNumNeighbors = 1;
                if (padToNumNeighborsMap.containsKey(neighborLandPatternPad))
                {
                  neighborsNumNeighbors += padToNumNeighborsMap.get(neighborLandPatternPad);
                }
                padToNumNeighborsMap.put(neighborLandPatternPad, neighborsNumNeighbors);

                if (numNeighbors == _MIN_NEIGHBORS_FOR_GRID_ARRAY_RECONSTRUCTION_REGION) // no need to continue checking
                  break;
              }
            }
          }
        }

        if (numNeighbors < _MIN_NEIGHBORS_FOR_GRID_ARRAY_RECONSTRUCTION_REGION)
          break;
      }

      if (numNeighbors == _MIN_NEIGHBORS_FOR_GRID_ARRAY_RECONSTRUCTION_REGION)
        passesRule = true;
    }

    return passesRule;
  }

  /**
   * @author George A. David
   */
  private double calculateLandPatternPadAreaToTotalAreaRatio(Set<LandPatternPad> landPatternPads)
  {
    Assert.expect(landPatternPads != null);

    double padAreaInMilsSquared = 0.0;
    Rectangle2D totalRect = null;
    for (LandPatternPad landPatternPad : landPatternPads)
    {
      Rectangle2D shapeBounds = landPatternPad.getShapeInNanoMeters().getBounds2D();
      // for circular pads, we use the "pitch bounds"
      if (landPatternPad.isPerfectCircle())
        shapeBounds.setRect(shapeBounds.getCenterX() - (landPatternPad.getPitchInNanoMeters() / 2.0),
                            shapeBounds.getCenterY() - (landPatternPad.getPitchInNanoMeters() / 2.0),
                            landPatternPad.getPitchInNanoMeters(),
                            landPatternPad.getPitchInNanoMeters());

      padAreaInMilsSquared += MathUtil.convertNanoMetersToMils(shapeBounds.getWidth()) *
      MathUtil.convertNanoMetersToMils(shapeBounds.getHeight());

      if (totalRect == null)
        totalRect = shapeBounds;
      else
        totalRect.add(shapeBounds);
    }

    double totalAreaInMilsSquared = MathUtil.convertNanoMetersToMils(totalRect.getWidth()) *
    MathUtil.convertNanoMetersToMils(totalRect.getHeight());

    return padAreaInMilsSquared / totalAreaInMilsSquared;
  }

  /**
   * @author George A. David
   */
  private Pair<Double, Rectangle2D> getLandPatternPadAreaInMilsSquaredAndBoundingRegionForGridArraysOrThroughHoles(List<LandPatternPad> landPatternPads, Rectangle2D region)
  {
    Assert.expect(landPatternPads != null);
    Assert.expect(region != null);

    double padAreaInMilsSquared = 0.0;
    Rectangle2D totalArea = null;
    Area regionArea = null;
    for (LandPatternPad landPatternPad : landPatternPads)
    {
      Rectangle2D shapeBounds = ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundLandPatternPad(landPatternPad).getRectangle2D();

      if (region.contains(shapeBounds))
      {
        // nothing to do!
      }
      else if (region.intersects(shapeBounds))
      {
        Area padArea = new Area(shapeBounds);
        if(regionArea == null)
          regionArea = new Area(region);
        padArea.intersect(regionArea);
        shapeBounds = padArea.getBounds2D();
      }
      else
        continue;

      if (totalArea == null)
        totalArea = shapeBounds;
      else
        totalArea.add(shapeBounds);

      padAreaInMilsSquared += MathUtil.convertNanoMetersToMils(shapeBounds.getWidth()) *
      MathUtil.convertNanoMetersToMils(shapeBounds.getHeight());
    }

    return new Pair<Double, Rectangle2D>(padAreaInMilsSquared, totalArea);
  }

  /**
   * @author George A. David
   */
  public static long getTimeToGenerateProgramInMillis()
  {
    Assert.expect(_generateProgramTimer != null);

    return _generateProgramTimer.getElapsedTimeInMillis();
  }

  /**
   * @author George A. David
   */
  private void createSurfaceModelingNeighbors()
  {
//    _rightSurfaceModelingReconstructionRegions.clear();
//    _leftSurfaceModelingReconstructionRegions.clear();
//    for (TestSubProgram subProgram: _testProgram.getAllTestSubPrograms())
//    {
//      if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
//        _rightSurfaceModelingReconstructionRegions.addAll(subProgram.getSurfaceModelingReconstructionRegions());
//      else if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
//        _leftSurfaceModelingReconstructionRegions.addAll(subProgram.getSurfaceModelingReconstructionRegions());
//      else
//        Assert.expect(false);
//    }

    setUpSurfaceModelingNeighbors();
  }

  /**
   * @author George A. David
   */
  private void setUpNeighbors()
  {
    createSurfaceModelingNeighbors();
    setUpVerificationRegionNeighbors();
  }

  /**
   * @author Matt Wharton
   */
  private void setUpSurroundingPadUtil()
  {
    TimerUtil timer = new TimerUtil();
    timer.reset();
    timer.start();

    SurroundingPadUtil surroundingPadUtil = new SurroundingPadUtil();
    surroundingPadUtil.setUseBoundsForDeterminingVicinity(true);
    surroundingPadUtil.setPads(_testProgram.getProject().getPanel().getPadsUnsorted());
    _testProgram.setSurroundingPadUtil(surroundingPadUtil);

//    if (UnitTest.unitTesting() == false)
//    {
//      System.out.println("[mdw] Time to calculate surrounding pad data: " + timer.getElapsedTimeInMillis());
//    }
  }

  /**
   * @author George A. David
   */
  public void intializeTestProgramAfterProjectLoad(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);
    initialize();
    try
    {
      _testProgram = testProgram;
      setUpNeighbors();
      setUpSurroundingPadUtil();
    }
    finally
    {
      reset();
    }
  }

  /**
   * @author George A. David
   */
  public void updateAdjustFocusSlices(TestProgram testProgram, boolean useAdjustFocusSlices)
  {
    Assert.expect(testProgram != null);

    if (useAdjustFocusSlices)
    {
      int minZheightForBottomSideNanoMeters = -StageBelts.getMaximumBeltTopDistanceBelowSystemCouponInNanoMeters() -
                                              XrayTester.getMaximumPanelDownWarpInNanoMeters() -
                                              XrayTester.getMaximumJointHeightInNanoMeters();
      int maxZheightForBottomSideNanoMeters = -StageBelts.getMinumumBeltTopDistanceBelowSystemCouponInNanoMeters() +
                                              XrayTester.getMaximumPanelUpWarpInNanoMeters();
      int minZheightForTopSideNanoMeters = (int)(-StageBelts.getMaximumBeltTopDistanceBelowSystemCouponInNanoMeters() -
                                           XrayTester.getMaximumPanelDownWarpInNanoMeters() -
                                           0.9 * testProgram.getProject().getPanel().getThicknessInNanometers());


      int maxZheightForTopSideNanoMeters = (int)(-StageBelts.getMinumumBeltTopDistanceBelowSystemCouponInNanoMeters() +
                                           XrayTester.getMaximumPanelUpWarpInNanoMeters() +
                                           (1.1) * testProgram.getProject().getPanel().getThicknessInNanometers());

      int startingRegionId = testProgram.getAllInspectionRegions().size() + testProgram.getVerificationRegions().size() + testProgram.getAlignmentRegions().size();
      testProgram.setSlicesForRetestingUnfocusedRegionsEnabled(false);

      // to adjust focus, we need to create about 100 slices in 2 mil increments,
      // however the IRPs cannot currently handle this, so we need to create new duplicate regions
      // with a max of 5 slices each.
      int numExtraRegions = 0;
      for (ReconstructionRegion region : testProgram.getUnfocusedReconstructionRegionsToBeFixed())
      {
        int startingZheightInNanos = minZheightForTopSideNanoMeters;
        int maxZheightInNanos = maxZheightForTopSideNanoMeters;
        if(region.isTopSide() == false)
        {
          startingZheightInNanos = minZheightForBottomSideNanoMeters;
          maxZheightInNanos = maxZheightForBottomSideNanoMeters;
        }

        int numExtraSlices = 0;
        testProgram.addFilter(region);

        for (FocusGroup group : region.getFocusGroups())
        {
          for (Slice slice : group.getSlices())
          {
            if (slice.isUsingCustomFocus())
            {
              // we want to reconstruct the original slice
              FocusInstruction instruction = slice.getFocusInstruction();
              instruction.setZOffsetInNanoMeters(slice.getDefaultFocusInstructionOffsetInNanoMeters());
            }
          }
        }
        int currentZheightInNanos = startingZheightInNanos;
        while(currentZheightInNanos <= maxZheightInNanos)
        {
          ++numExtraRegions;
          // create a new reconstruction region
          ReconstructionRegion newRegion = InspectionRegionGeneration.createInspectionRegion(region.getPads());
          newRegion.setRegionId(startingRegionId++);
          newRegion.setTestSubProgram(region.getTestSubProgram());
          newRegion.setReconstructionEngineId(region.getReconstructionEngineId());
          newRegion.setFocusPriority(region.getFocusPriority());

          for (FocusGroup group : region.getFocusGroups())
          {
            // create a new no search focus group
            FocusGroup newGroup = new FocusGroup();
            FocusSearchParameters searchParams = new FocusSearchParameters();
            searchParams.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
            searchParams.setFocusRegion(group.getFocusSearchParameters().getFocusRegion());
            searchParams.setZCurveWidthInNanoMeters(0);
            newGroup.setFocusSearchParameters(searchParams);
            newRegion.addFocusGroup(newGroup);

            ++numExtraSlices;
            // ok, now add the slices
            int indexOffset = numExtraSlices * 10;
            for (int j = 0; j < 5; j++)
            {
              Slice newSlice = new Slice();
              newSlice.setSliceName(SliceNameEnum.getAdditionalSliceName(j + indexOffset));
              newSlice.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
              FocusInstruction focusInstruction = new FocusInstruction();
              focusInstruction.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);
              focusInstruction.setPercentValue(0);
              focusInstruction.setZOffsetInNanoMeters(0);
              focusInstruction.setZHeightInNanoMeters(currentZheightInNanos);
              newSlice.setFocusInstruction(focusInstruction);
              newGroup.addSlice(newSlice);
              currentZheightInNanos += _UNFOCUSED_REGION_SLICE_INTERVAL_IN_NANOS;
            }
          }
          region.getTestSubProgram().addAdjustFocusInspectionRegion(newRegion);
          testProgram.addFilter(newRegion);
        }
      }
    }
    else
    {
      testProgram.setSlicesForRetestingUnfocusedRegionsEnabled(false);
      testProgram.clearReconstructionRegionFilters();
      testProgram.clearAdjustFocusInspectionRegions();
      for (ReconstructionRegion region : testProgram.getUnfocusedReconstructionRegionsWithCustomFocus())
      {
        for (FocusGroup group : region.getFocusGroups())
        {
          for (Slice slice : group.getSlices())
          {
            if(slice.isUsingCustomFocus())
            {
              // we want to reconstruct the original slice
              FocusInstruction instruction = slice.getFocusInstruction();
              int zOffset = testProgram.getUnfocusedSliceZheightOffsetInNanoMeters(region, slice);
              instruction.setZOffsetInNanoMeters(zOffset);
            }
          }
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  public void clearVerificationSurfaceEstimatorChanges(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);
    for(ReconstructionRegion region : testProgram.getVerificationRegions())
    {
      List<FocusGroup> groups = region.getFocusGroups();
      Assert.expect(groups.size() == 1);
      FocusGroup group = groups.get(0);
      // PEAK is used here and in createFocusGroupForVerificationOrAlignmentRegion.  These should
      // be combined so we don't have to remember to change one AND the other
      group.getFocusSearchParameters().setFocusProfileShape(FocusProfileShapeEnum.PEAK);
      List<Slice> slices = group.getSlices();

      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
//      Assert.expect(slices.size() == 1);
      FocusInstruction instruction = new FocusInstruction();
      for (Slice currentSlice : slices)
      {
        instruction = currentSlice.getFocusInstruction();
        instruction.setFocusMethod(FocusMethodEnum.SHARPEST);
        instruction.setZHeightInNanoMeters(0);
      }
    }
  }

  /**
   * @author John Heumann
   */
  private void setVerificationRegionSideUsage(boolean topSideExcluded[],
                                              boolean bottomSideExcluded[],
                                              int numVerificationRegionRows,
                                              int numVerificationRegionColumns,
                                              int numVerificationRegionsPerSide,
                                              Map<ReconstructionRegion, Integer> verificationRegionToZHeightMap,
                                              Set<ReconstructionRegion> verificationRegions,
                                              List<Component> components)
  {
    Assert.expect(numVerificationRegionsPerSide == numVerificationRegionRows * numVerificationRegionColumns);

    // Construct a set of top side verification regions ordered along colums and then rows
    List<ReconstructionRegion> topSideVerificationRegions = new ArrayList<ReconstructionRegion>();
    for (Map.Entry<ReconstructionRegion, Integer> entry : verificationRegionToZHeightMap.entrySet())
    {
      ReconstructionRegion verificationRegion = entry.getKey();
      if(verificationRegions.contains(verificationRegion))
      {
        if (verificationRegion.isTopSide())
          topSideVerificationRegions.add(verificationRegion);
      }
    }
    Assert.expect(topSideVerificationRegions.size() == numVerificationRegionsPerSide);

    for (int i = 0; i < numVerificationRegionsPerSide; ++i)
      topSideExcluded[i] = bottomSideExcluded[i] = true;

    PanelRectangle rect0 = topSideVerificationRegions.get(0).getRegionRectangleRelativeToPanelInNanoMeters();
    double x0 = rect0.getMinX();
    double y0 = rect0.getMinY();
    double dX = rect0.getWidth();
    double dY = rect0.getHeight();

    // Loop over all components, marking the verification regions they intersect
    for (int i = 0; i < components.size(); ++i)
    {
      Component component = components.get(i);
      PanelRectangle rect = new PanelRectangle(component.getShapeSurroundingPadsRelativeToPanelInNanoMeters());
      double xMin = rect.getMinX();
      double xMax = rect.getMaxX();
      double yMin = rect.getMinY();
      double yMax = rect.getMaxY();
      int c0 = (int) Math.floor((xMin - x0) / dX);
      int c1 = (int) Math.floor((xMax - x0) / dX);
      int r0 = (int) Math.floor((yMin - y0) / dY);
      int r1 = (int) Math.floor((yMax - y0) / dY);
      for (int row = r0; row <= r1; ++row)
      {
        int rowOffset = row * numVerificationRegionColumns;
        for (int col = c0; col <= c1; ++col)
        {
          int index = rowOffset + col;
          // RJG: Make sure the index within range of indicies, it could specify a component on a another board
          // (or section of the board in a long panel).
          if (index >= 0 && index < numVerificationRegionsPerSide)
          {
            if (component.isTopSide())
              topSideExcluded[index] = false;
            else
              bottomSideExcluded[index] = false;
          }
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  public void modifyTestProgramWithVerificationSurfaceEstimatorChanges( TestSubProgram subProgram,
                                                                        Map<ReconstructionRegion, Integer> reconstructionRegionToZheightInNanosMap)
  {
    Assert.expect(subProgram != null);
    Assert.expect(reconstructionRegionToZheightInNanosMap != null);

    Panel panel = subProgram.getTestProgram().getProject().getPanel();
    //int panelThicknessInNanos = panel.getThicknessInNanometers();
    int panelThicknessInNanos = (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
            ? panel.getThicknessInNanometers() - XrayTester.getSystemPanelThicknessOffset() : panel.getThicknessInNanometers();
//    for(TestSubProgram subProgram : testProgram.getAllTestSubPrograms())
    {
      Set<ReconstructionRegion> verificationRegions = new HashSet<ReconstructionRegion>(subProgram.getVerificationRegions());

      int numVerificationRegionsPerSide = verificationRegions.size() / 2;
      Assert.expect(numVerificationRegionsPerSide * 2 == verificationRegions.size()); // just a sanity check that the number was even
      Assert.expect(numVerificationRegionsPerSide * 2 <= reconstructionRegionToZheightInNanosMap.size()); // just a sanity check that the number was even
      double topSideZheights[] = new double[numVerificationRegionsPerSide];
      double bottomSideZheights[] = new double[numVerificationRegionsPerSide];
      int topCounter = 0;
      int bottomCounter = 0;
      for(Map.Entry<ReconstructionRegion, Integer> entry : reconstructionRegionToZheightInNanosMap.entrySet())
      {
        ReconstructionRegion verificationRegion = entry.getKey();
        if(verificationRegions.contains(verificationRegion))
        {
          if (verificationRegion.isTopSide())
            topSideZheights[topCounter++] = entry.getValue();
          else
            bottomSideZheights[bottomCounter++] = entry.getValue();
        }
      }

      Assert.expect(topCounter == (numVerificationRegionsPerSide));
      Assert.expect(bottomCounter == (numVerificationRegionsPerSide));

      // Figure out which verification regions contain top side components,
      // bottom side components, both, or neither
      boolean topSideExcluded[] = new boolean[numVerificationRegionsPerSide];
      boolean bottomSideExcluded[] = new boolean[numVerificationRegionsPerSide];
      setVerificationRegionSideUsage(topSideExcluded,
                                     bottomSideExcluded,
                                     subProgram.getNumVerificationRegionRows(),
                                     subProgram.getNumVerificationRegionColumns(),
                                     numVerificationRegionsPerSide,
                                     reconstructionRegionToZheightInNanosMap,
                                     verificationRegions,
                                     panel.getComponents());

      TimerUtil timer = new TimerUtil();
      timer.start();
      VerificationSurfaceEstimator surfaceEstimator = new VerificationSurfaceEstimator(subProgram.getNumVerificationRegionColumns(),
                                                                                       subProgram.getNumVerificationRegionRows(),
                                                                                       panelThicknessInNanos);
      surfaceEstimator.chooseVerificationHeight(topSideZheights, bottomSideZheights, topSideExcluded, bottomSideExcluded);
      timer.stop();
//      if(UnitTest.unitTesting() == false)
//        System.out.println("time to run verification surface estimator:" + timer.getElapsedTimeInMillis());

      // Set the reconstruction height to the predicted surface.
      // Bound them at max and min slice heights.
      int uncertaintyReferencePlaneToNominalPlaneInNanoMeters = SetHardwareConstants.getUncertaintyReferencePlaneToNominalPlaneInNanoMeters();

      double zHeightMax_NM = XrayTester.getMaxPanelSliceHeightFromNominalSliceInNanometers(subProgram.getTestProgram().getProject().getPanel().getThicknessInNanometers(), 0.0);

      zHeightMax_NM = zHeightMax_NM - uncertaintyReferencePlaneToNominalPlaneInNanoMeters;
      
      double zHeightMin_NM = XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers(subProgram.getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers())
                             + uncertaintyReferencePlaneToNominalPlaneInNanoMeters;
      topCounter = 0;
      bottomCounter = 0;
      for(Map.Entry<ReconstructionRegion, Integer> entry : reconstructionRegionToZheightInNanosMap.entrySet())
      {
        if(verificationRegions.contains(entry.getKey()))
        {
          List<FocusGroup> groups = entry.getKey().getFocusGroups();
          Assert.expect(groups.size() == 1);
          FocusGroup group = groups.get(0);
          group.getFocusSearchParameters().setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
          List<Slice> slices = group.getSlices();
          //Khaw Chek Hau - XCR2285: 2D Alignment on v810
          //Assert.expect(slices.size() == 1);
          FocusInstruction instruction = new FocusInstruction();
          SliceNameEnum slice = SliceNameEnum.PAD;
                  
          if (subProgram.getTestProgram().isManual2DAlignmentInProgress())
            slice = SliceNameEnum.CAMERA_0;
          
          for (Slice currentSlice : slices)  
          {
            if (currentSlice.getSliceName() == slice)
            {
              instruction = currentSlice.getFocusInstruction();
            }
          }

          instruction.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);

          if (entry.getKey().isTopSide())
          //  instruction.setZHeightInNanoMeters((int)topSideZheights[topCounter++]);
          {
            // Bound them at max and min slice heights.
            double zHeight = topSideZheights[topCounter++];
            zHeight = Math.max(zHeight, zHeightMin_NM);
            zHeight = Math.min(zHeight, zHeightMax_NM);
            instruction.setZHeightInNanoMeters((int)zHeight);
          }
          else
          //  instruction.setZHeightInNanoMeters((int)bottomSideZheights[bottomCounter++]);
          {
            // Bound them at max and min slice heights.
            double zHeight = bottomSideZheights[bottomCounter++];
            zHeight = Math.max(zHeight, zHeightMin_NM);
            zHeight = Math.min(zHeight, zHeightMax_NM);
            instruction.setZHeightInNanoMeters((int)zHeight);
          }
        }
      }
      Assert.expect(topCounter == (numVerificationRegionsPerSide));
      Assert.expect(bottomCounter == (numVerificationRegionsPerSide));
    }
  }

  /**
   * @author Sunit Bhalla
   * This inner class is used to sort the neighboring joints for open outlier.  The joints are sorted by distance.
   */
  class JointInspectionDataDistancePair implements Comparable<JointInspectionDataDistancePair>
  {
    private JointInspectionData _jointInspectionData;
    private double _distance;

    public JointInspectionDataDistancePair(JointInspectionData jointInspectionData, double distance)
    {
      Assert.expect(jointInspectionData != null);

      _jointInspectionData = jointInspectionData;
      _distance = distance;
    }

    public double getDistance()
    {
      return _distance;
    }

    public JointInspectionData getJointInspectionData()
    {
      Assert.expect(_jointInspectionData != null);
      return _jointInspectionData;
    }

    public int compareTo(JointInspectionDataDistancePair other)
    {
      Assert.expect(other != null);

      double aDistance = getDistance();
      double bDistance = other.getDistance();

      if (MathUtil.fuzzyEquals(aDistance, bDistance))
        return 0;
      if (aDistance < bDistance)
        return -1;
      else
        return 1;
    }
  }

  /**
   * @author George A. David
   */
  public boolean isCompatibleWithHardware(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    boolean isCompatible = false;
    TestProgramGenerationData currentData = createTestProgramGenerationData();
    TestProgramGenerationData programData = testProgram.getTestProgramGenerationData();
    if(currentData.getAlignmentFeatureLengthBufferInNanoMeters() == programData.getAlignmentFeatureLengthBufferInNanoMeters() &&
       currentData.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters() == programData.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters() &&
       currentData.getAlignmentUncertaintyInNanoMeters() == programData.getAlignmentUncertaintyInNanoMeters() &&
       currentData.getMaxAlignmentRegionLengthInNanoMeters() == programData.getMaxAlignmentRegionLengthInNanoMeters() &&
       currentData.getMaxAlignmentRegionWidthInNanoMeters() == programData.getMaxAlignmentRegionWidthInNanoMeters() &&
       currentData.getMaxFocusRegionSizeInNanoMeters() == programData.getMaxFocusRegionSizeInNanoMeters() &&
       currentData.getMaxImageableAreaLengthInNanoMeters() == programData.getMaxImageableAreaLengthInNanoMeters() &&
       currentData.getMaxInspectablePanelWidthInNanoMeters() == programData.getMaxInspectablePanelWidthInNanoMeters() &&
       currentData.getMaxInspectionRegionLengthInNanoMeters() == programData.getMaxInspectionRegionLengthInNanoMeters() &&
       currentData.getMaxInspectionRegionSizeDueToWarpInNanoMeters() == programData.getMaxInspectionRegionSizeDueToWarpInNanoMeters() &&
       currentData.getMaxInspectionRegionWidthInNanoMeters() == programData.getMaxInspectionRegionWidthInNanoMeters() &&
       currentData.getMaxVerificationRegionLengthInNanoMeters() == programData.getMaxVerificationRegionLengthInNanoMeters() &&
       currentData.getMaxVerificationRegionWidthInNanoMeters() == programData.getMaxVerificationRegionWidthInNanoMeters() &&
       currentData.getMinFocusRegionSizeInNanoMeters() == programData.getMinFocusRegionSizeInNanoMeters() &&
       currentData.getNumberOfAvailabeIrps() == programData.getNumberOfAvailabeIrps() &&
       currentData.isScanPathOptimization() == programData.isScanPathOptimization() &&
       currentData.isFocusOptimization() == programData.isFocusOptimization() && //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on";
       //currentData.isLargeImageView() == programData.isLargeImageView() &&
       //currentData.isGenerateMultiAngleImage() == programData.isGenerateMultiAngleImage() &&
       currentData.getSystemInspectionType() == programData.getSystemInspectionType() &&
       currentData.isPspEnable() == programData.isPspEnable() &&
       currentData.isSemiAutomatedModeEnabled() == programData.isSemiAutomatedModeEnabled() &&
       currentData.isVariableMagnificationEnabled() == programData.isVariableMagnificationEnabled() &&
       currentData.getNumberOfAvailablePhysicalIrps() == programData.getNumberOfAvailablePhysicalIrps() &&
       currentData.getLowMagnification().equalsIgnoreCase(programData.getLowMagnification()) &&
       currentData.getHighMagnification().equalsIgnoreCase(programData.getHighMagnification()))//punit.2889
    {
      isCompatible = true;
    }

    return isCompatible;
  }
  
  /**   
   * @author Cheah Lee Herng
   */
  public boolean isCompatibleWithAvailablePhysicalIrps(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);
    
    boolean isCompatible = false;
    TestProgramGenerationData currentData = createTestProgramGenerationData();
    TestProgramGenerationData programData = testProgram.getTestProgramGenerationData();
    
    if (currentData.getNumberOfAvailablePhysicalIrps() == programData.getNumberOfAvailablePhysicalIrps())
    {
      isCompatible = true;
    }
    return isCompatible;
  }
  
  /**
   * @author George A. David
   */
  public int getVersionNumber()
  {
    Assert.expect(_VERSION_NUMBER > 0);

    return _VERSION_NUMBER;
  }

  /**
   * @author George A. David
   */
  public int getRequiredDistanceFromInterferenceAffectedRegionInNanoMetersTypeFull()
  {
    return _config.getIntValue(SoftwareConfigEnum.REQUIRED_DISTANCE_FROM_INTERFERENCE_AFFECTED_REGION_IN_NANO_METERS_TYPE_FULL);
  }

  /**
   * @author George A. David
   */
  public int getRequiredDistanceFromInterferenceAffectedRegionInNanoMetersTypeHalf()
  {
    return _config.getIntValue(SoftwareConfigEnum.REQUIRED_DISTANCE_FROM_INTERFERENCE_AFFECTED_REGION_IN_NANO_METERS_TYPE_HALF);
  }

  /**
   * @author George A. David
   */
  public List<SignalCompensationEnum> getValidSignalCompensations(Collection<PadType> padTypes)
  {
    Assert.expect(padTypes != null);

    // start with the full range
    RangeUtil rangeUtil = null;

    for(PadType padType : padTypes)
    {
      // Chnee Khang Wah, 2013-04-23, new entropy based IC
      if(padType.getSubtype().getPanel().getProject().isGenerateByNewScanRoute())
      {
        break;
      }
      
      PadTypeSettings settings = padType.getPadTypeSettings();
      if(settings.isInterferenceCompensated() &&
         settings.hasCandidateStepRanges())
      {
        if (rangeUtil == null)
          rangeUtil = settings.getCandidateStepRanges();
        else
          rangeUtil.intersect(settings.getCandidateStepRanges());

        // CR fix by LeeHerng - I add the previous candidate step ranges into the calculation
        // to make sure it has the previous signal compensation.
        if (settings.hasPreviousCandidateStepRanges())
            rangeUtil.intersect(settings.getPreviousCandidateStepRanges());
      }
    }

    List<SignalCompensationEnum> validSignalCompensations = new LinkedList<SignalCompensationEnum>();


    List<SignalCompensationEnum> orderedSignalCompensations = SignalCompensationEnum.getOrderedSignalCompensations();
    if(rangeUtil == null)
    {
      // none of the pads are artifcat compensated, all signal compensations are valid
      validSignalCompensations.addAll(orderedSignalCompensations);
    }
    else
    {
      for (SignalCompensationEnum signalCompensation : orderedSignalCompensations)
      {
          // Modified by Seng Yew on 28-Sep-2011
          // signalCompensation.getStepSizeRange() is only used here. No other places as of the time of this modification.
          // getStepSizeRange() in SignalCompensationEnum is using hardcoded max scan step size values in systemConfigEnum
//        if (rangeUtil.intersects(signalCompensation.getStepSizeRange()))
        int maxAverageStepSizeInNanos = ImagingChainProgramGenerator.getMaxAverageStepSizeInNanometers();
        int maxStepSizeInNanos = ImagingChainProgramGenerator.calculateMaxStepSizeInNanometers();
    
        if (rangeUtil.intersects(getStepSizeRange(signalCompensation, maxStepSizeInNanos, maxAverageStepSizeInNanos)))
          validSignalCompensations.add(signalCompensation);
      }
    }
    
    // Clear memory
    if (rangeUtil != null)
    {
      rangeUtil.clear();
      rangeUtil = null;
    }
    
    return validSignalCompensations;
  }

  /**
   * @author George A. David
   */
  private Pair<Integer, Integer> getStepSizeRange(SignalCompensationEnum signalCompensation, int maxStepSizeInNanos, int maxAverageStepSizeInNanos)
  {
    Pair<Integer, Integer> pair = new Pair<Integer, Integer>();
    if (signalCompensation.equals(SignalCompensationEnum.DEFAULT_LOW))
    {
      // pair.setFirst(getMaxStepSizeInNanos(SignalCompensationEnum.MEDIUM, maxStepSizeInNanos, maxAverageStepSizeInNanos) + 1);
      // Khang Wah, 2013-05-21, New Scan Route
      pair.setFirst(getMaxStepSizeInNanos(SignalCompensationEnum.IL_1_5, maxStepSizeInNanos, maxAverageStepSizeInNanos) + 1);
      pair.setSecond(getMaxStepSizeInNanos(SignalCompensationEnum.DEFAULT_LOW, maxStepSizeInNanos, maxAverageStepSizeInNanos));
    }
    // Khang Wah, 2013-05-21, New Scan Route
    else if (signalCompensation.equals(SignalCompensationEnum.IL_1_5))
    {
      pair.setFirst(getMaxStepSizeInNanos(SignalCompensationEnum.MEDIUM, maxStepSizeInNanos, maxAverageStepSizeInNanos) + 1);
      pair.setSecond(getMaxStepSizeInNanos(SignalCompensationEnum.IL_1_5, maxStepSizeInNanos, maxAverageStepSizeInNanos));
    }
    else if (signalCompensation.equals(SignalCompensationEnum.MEDIUM))
    {
      pair.setFirst(getMaxStepSizeInNanos(SignalCompensationEnum.MEDIUM_HIGH, maxStepSizeInNanos, maxAverageStepSizeInNanos) + 1);
      pair.setSecond(getMaxStepSizeInNanos(SignalCompensationEnum.MEDIUM, maxStepSizeInNanos, maxAverageStepSizeInNanos));
    }
    else if (signalCompensation.equals(SignalCompensationEnum.MEDIUM_HIGH))
    {
      pair.setFirst(getMaxStepSizeInNanos(SignalCompensationEnum.HIGH, maxStepSizeInNanos, maxAverageStepSizeInNanos) + 1);
      pair.setSecond(getMaxStepSizeInNanos(SignalCompensationEnum.MEDIUM_HIGH, maxStepSizeInNanos, maxAverageStepSizeInNanos));
    }
    else if (signalCompensation.equals(SignalCompensationEnum.HIGH))
    {
      // pair.setFirst(getMaxStepSizeInNanos(SignalCompensationEnum.SUPER_HIGH, maxStepSizeInNanos, maxAverageStepSizeInNanos) + 1);
      // Khang Wah, 2013-05-21, New Scan Route
      pair.setFirst(getMaxStepSizeInNanos(SignalCompensationEnum.IL_5, maxStepSizeInNanos, maxAverageStepSizeInNanos) + 1);
      pair.setSecond(getMaxStepSizeInNanos(SignalCompensationEnum.HIGH, maxStepSizeInNanos, maxAverageStepSizeInNanos));
    }
    // Khang Wah, 2013-05-21, New Scan Route
    else if (signalCompensation.equals(SignalCompensationEnum.IL_5))
    {
      pair.setFirst(getMaxStepSizeInNanos(SignalCompensationEnum.IL_6, maxStepSizeInNanos, maxAverageStepSizeInNanos) + 1);
      pair.setSecond(getMaxStepSizeInNanos(SignalCompensationEnum.IL_5, maxStepSizeInNanos, maxAverageStepSizeInNanos));
    }
    // Khang Wah, 2013-05-21, New Scan Route
    else if (signalCompensation.equals(SignalCompensationEnum.IL_6))
    {
      pair.setFirst(getMaxStepSizeInNanos(SignalCompensationEnum.IL_7, maxStepSizeInNanos, maxAverageStepSizeInNanos) + 1);
      pair.setSecond(getMaxStepSizeInNanos(SignalCompensationEnum.IL_6, maxStepSizeInNanos, maxAverageStepSizeInNanos));
    }
    // Khang Wah, 2013-05-21, New Scan Route
    else if (signalCompensation.equals(SignalCompensationEnum.IL_7))
    {
      pair.setFirst(getMaxStepSizeInNanos(SignalCompensationEnum.SUPER_HIGH, maxStepSizeInNanos, maxAverageStepSizeInNanos) + 1);
      pair.setSecond(getMaxStepSizeInNanos(SignalCompensationEnum.IL_7, maxStepSizeInNanos, maxAverageStepSizeInNanos));
    }
    else if (signalCompensation.equals(SignalCompensationEnum.SUPER_HIGH))
    {
      pair.setFirst(1);
      pair.setSecond(getMaxStepSizeInNanos(SignalCompensationEnum.SUPER_HIGH, maxStepSizeInNanos, maxAverageStepSizeInNanos));
    }
    else
      Assert.expect(false);

    return pair;
  }

  /**
   * @return
   */
  public int getMaxPanelInspectableWidthInNanoMeters()
  {
      Assert.expect(_STANDARD_MAX_INSPECTABLE_PANEL_WIDTH_NANOS >= 0);
      Assert.expect(_THROUGHPUT_MAX_INSPECTABLE_PANEL_WIDTH_NANOS >= 0);

      int maxPanelInspectableWidthInNanometers = 0;
      SystemTypeEnum systemType = XrayTester.getSystemType();
      if (systemType.equals(SystemTypeEnum.STANDARD))
          maxPanelInspectableWidthInNanometers = _STANDARD_MAX_INSPECTABLE_PANEL_WIDTH_NANOS;
      else if (systemType.equals(SystemTypeEnum.THROUGHPUT))
          maxPanelInspectableWidthInNanometers = _THROUGHPUT_MAX_INSPECTABLE_PANEL_WIDTH_NANOS;
      else if(systemType.equals(SystemTypeEnum.XXL))
        maxPanelInspectableWidthInNanometers = _XXL_MAX_INSPECTABLE_PANEL_WIDTH_NANOS;
      else if(systemType.equals(SystemTypeEnum.S2EX))
        maxPanelInspectableWidthInNanometers = _S2EX_MAX_INSPECTABLE_PANEL_WIDTH_NANOS;
      else
        Assert.expect(false, "unknown system type: " + systemType.getName());

      return maxPanelInspectableWidthInNanometers;
  }

  /**
   * @author Wei Chin, Chong
   */
  public TestProgram determineNeighborsForAll(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);
    _testProgram = testProgram;
    determineOpenOutlierNeighbors();
    determineQuadFlatNoLeadNeighbors();
    _testProgram.setNeighboringHasAssigned(true);
    return _testProgram;
  }

  /**
   * @author Cheah, Lee Herng
   */
  public Collection<ReconstructionRegion> getFilteredNeighborInspectionRegions(Collection<ReconstructionRegion> inspectionRegions)
  {
    Assert.expect(inspectionRegions != null);

    Collection<ReconstructionRegion> inspectedNeighborInspectionRegions = new LinkedList<ReconstructionRegion>();
    for (ReconstructionRegion inspectionRegion : inspectionRegions)
    {
      if (inspectionRegion.isInspected())
      {
        inspectedNeighborInspectionRegions.add(inspectionRegion);
      }
    }

    return inspectedNeighborInspectionRegions;
  }

  /**
   * @author Cheah, Lee Herng
   */
  private void setupGlobalSurfaceModel()
  {
    for (TestSubProgram subProgram : _testProgram.getAllTestSubPrograms())
    {
      // Setup Global Surface Model Enum for Inspection Region
      for (ReconstructionRegion inspectionRegion : subProgram.getAllInspectionRegions())
      {
        ComponentType componentType = inspectionRegion.getComponent().getComponentType();
        inspectionRegion.setGlobalSurfaceModelEnum(componentType.getGlobalSurfaceModel());
        inspectionRegion.setUseToBuildGlobalSurfaceModel(componentType.useToBuildGlobalSurfaceModel());
      }
    }
  }

  /**
   * Please do not call it directly but through initializeAlignmentSubProgram()
   * @author George A. David
   * @author sham
   * @author Chin Seong, Kee
   */
  public void createAlignmentRegionsBaseOnUserGain()
  {
    _didAlignmentRegionsChange = false;

    PanelSettings panelSettings = _testProgram.getProject().getPanel().getPanelSettings();
    Panel panel = _testProgram.getProject().getPanel();

    Set<AlignmentGroup> groups = new HashSet<AlignmentGroup>();

    if (panelSettings.isPanelBasedAlignment())
    {
      groups.addAll(panelSettings.getAllAlignmentGroups());
      if(panelSettings.hasHighMagnificationComponent())
        groups.addAll(panelSettings.getAllAlignmentGroups());
    }
    else
    {
      for (Board board : panel.getBoards())
      {
        board.syncPadsToAlignmentGroupsIfNecessary();
        groups.addAll(board.getBoardSettings().getAllAlignmentGroups());
        if(panelSettings.hasHighMagnificationComponent())
          groups.addAll(board.getBoardSettings().getAllAlignmentGroups());
      }
    }
    
    List<TestSubProgram> customizeAlignmentTestSubPrograms = new ArrayList<TestSubProgram>();
    
    for (TestSubProgram subProgram : _testProgram.getAllInspectableTestSubPrograms())
    {
      if(subProgram.isLowMagnification() != XrayTester.isLowMagnification() )
      {
        ImageAcquisitionEngine.changeMagnification(subProgram.getMagnificationType());
        if(ImagingChainProgramGenerator.getEnableStepSizeOptimization())
          ImagingChainProgramGenerator.forceCalculateMaxStepSizeInNanometers(_project.getPanel().getThicknessInNanometers(), _project.getPanel().getMeansBoardZOffsetInNanometers());
      }
      
      if (subProgram.isSubProgramPerformAlignment())
      {
        if (panelSettings.isPanelBasedAlignment())
        {
          if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
          {
            if (panelSettings.isLongPanel())
            {
              prepareAlignmentSettingForTestSubProgram(subProgram, 
                                                       customizeAlignmentTestSubPrograms, 
                                                       panelSettings.getRightAlignmentGroupsForLongPanel(),
                                                       groups,
                                                       panelSettings.getRightSectionOfLongPanel());
            }
            else
            {
              prepareAlignmentSettingForTestSubProgram(subProgram, 
                                                       customizeAlignmentTestSubPrograms, 
                                                       panelSettings.getAlignmentGroupsForShortPanel(), 
                                                       groups, 
                                                       panelSettings.getRightSectionOfLongPanel());
            }
          }
          else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
          {
            prepareAlignmentSettingForTestSubProgram(subProgram, 
                                                     customizeAlignmentTestSubPrograms, 
                                                     panelSettings.getLeftAlignmentGroupsForLongPanel(), 
                                                     groups, 
                                                     panelSettings.getLeftSectionOfLongPanel());
          }
          else
          {
            Assert.expect(false);
          }
        }
        else
        {
          if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
          {
            if (subProgram.getBoard().isLongBoard())
            {
              prepareAlignmentSettingForTestSubProgram(subProgram, 
                                                       customizeAlignmentTestSubPrograms, 
                                                       subProgram.getBoard().getBoardSettings().getRightAlignmentGroupsForLongPanel(), 
                                                       groups, 
                                                       subProgram.getBoard().getRightSectionOfLongBoard());                   
            }
            else
            {
              prepareAlignmentSettingForTestSubProgram(subProgram, 
                                                       customizeAlignmentTestSubPrograms, 
                                                       subProgram.getBoard().getBoardSettings().getAlignmentGroupsForShortPanel(), 
                                                       groups, 
                                                       subProgram.getBoard().getRightSectionOfLongBoard());
            }
          }
          else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
          {
            prepareAlignmentSettingForTestSubProgram(subProgram, 
                                                     customizeAlignmentTestSubPrograms, 
                                                     subProgram.getBoard().getBoardSettings().getLeftAlignmentGroupsForLongPanel(), 
                                                     groups, 
                                                     subProgram.getBoard().getLeftSectionOfLongBoard());
          }
          else
          {
            Assert.expect(false);
          }
        }
      }
      else
      {
        //Khaw Chek Hau - XCR2285: 2D Alignment on v810
        subProgram.set2DAlignmentRegions(new ArrayList<ReconstructionRegion>());
        subProgram.set3DAlignmentRegions(new ArrayList<ReconstructionRegion>()); 
      }
    }

    // if there are any groups left, it means they are no longer valid
    for (AlignmentGroup group : groups)
    {
      // it's only invalid if it has pads, otherwise they haven't yet tried to set it up!
      if (group.isEmpty() == false)
      {
        //checked for the pad is inspected or not before added in invalidated group
        //as usual no load, no test will not count as invalidated group!
        boolean isSomePadsInspected = false;
        for(Pad pad : group.getPads())
        {
          if(pad.isInspected())
          {
            isSomePadsInspected = true;
            break;
          }
        }
        if(isSomePadsInspected)
          _testProgram.addInvalidatedAlignmentGroup(group);
      }
    }
    if(XrayTester.isLowMagnification() == false)
    {
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
      if(ImagingChainProgramGenerator.getEnableStepSizeOptimization())
        ImagingChainProgramGenerator.forceCalculateMaxStepSizeInNanometers(_project.getPanel().getThicknessInNanometers(), _project.getPanel().getMeansBoardZOffsetInNanometers());
    }
    customizeAlignmentTestSubPrograms.clear();
  }

  /**
   * this function must be called createAlignmentRegionsBaseOnUserGain
   * XCR-3283 Alignment Running Slow Speed Causing Long Inspection Time
   * @author Yong Sheng Chuan
   */
  private void setupProjectSurfaceMap()
  {
    PanelSettings panelSettings = _testProgram.getProject().getPanel().getPanelSettings();
    if (_enablePsp && _testProgram.getFilteredTestSubPrograms().isEmpty() == false)
    {
      if (panelSettings.isPanelBasedAlignment())
        _opticalProgramGeneration.setupPanelSurfaceMap(_project, _testProgram);
      else
        _opticalProgramGeneration.setupBoardSurfaceMap(_project, _testProgram);
    }
  }
  
 /**
  * Do not call it directly but through initializeAlignmentSubProgram()
 * @author sham
 */
  private void initializeSubProgramForAlignment()
  {
    Assert.expect(_testProgram.getAllTestSubPrograms().size() >= 0);
    
    // XCR by Lee Herng 15 May 2013 - Prior to setting up alignment groups, 
    // we need to check if there is any changes from
    clearAlignmentIfNecessary();
    
    PanelSettings panelSettings = _testProgram.getProject().getPanel().getPanelSettings();
    if (panelSettings.isPanelBasedAlignment())
    {
      if (panelSettings.isLongPanel())
      {
        int lowLeftPanelAlignmentSet = -1;
        int lowRightPanelAlignmentSet = -1;
        int highLeftPanelAlignmentSet = -1;
        int highRightPanelAlignmentSet = -1;
        for (TestSubProgram subProgram : _testProgram.getAllInspectableTestSubPrograms())
        {
          if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT) && 
              subProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
          {
            if (lowRightPanelAlignmentSet == -1)
            {
              subProgram.setIsSubProgramPerformAlignment(true);
              lowRightPanelAlignmentSet = subProgram.getId();
              subProgram.setSubProgramRefferenceIdForAlignment(lowRightPanelAlignmentSet);
              if (_enablePsp)
              {
                  // Mark the 1st TestSubProgram to perform surface map
                  subProgram.setIsSubProgramPerformSurfaceMap(true);
              }
              else
                  subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
            else
            {
              subProgram.setIsSubProgramPerformAlignment(false);
              subProgram.setSubProgramRefferenceIdForAlignment(lowRightPanelAlignmentSet);
              
              // We will not perform surface map for the rest of TestSubProgram contained 
              // within the same side of panel location.
              subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
          }
          else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT) && 
              subProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
          {
            if (lowLeftPanelAlignmentSet == -1)
            {
              subProgram.setIsSubProgramPerformAlignment(true);
              lowLeftPanelAlignmentSet = subProgram.getId();
              subProgram.setSubProgramRefferenceIdForAlignment(lowLeftPanelAlignmentSet);
              
              if (_enablePsp)
              {
                  // Mark the 1st TestSubProgram to perform surface map
                  subProgram.setIsSubProgramPerformSurfaceMap(true);
              }
              else
                  subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
            else
            {
              subProgram.setIsSubProgramPerformAlignment(false);
              subProgram.setSubProgramRefferenceIdForAlignment(lowLeftPanelAlignmentSet);
              
              // We will not perform surface map for the rest of TestSubProgram contained 
              // within the same side of panel location.
              subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
          }
          else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT) && 
              subProgram.getMagnificationType().equals(MagnificationTypeEnum.HIGH))
          {
            if (highRightPanelAlignmentSet == -1)
            {
              subProgram.setIsSubProgramPerformAlignment(true);
              highRightPanelAlignmentSet = subProgram.getId();
              subProgram.setSubProgramRefferenceIdForAlignment(highRightPanelAlignmentSet);

              if (_enablePsp)
              {
                  // Mark the 1st TestSubProgram to perform surface map
                  subProgram.setIsSubProgramPerformSurfaceMap(true);
              }
              else
                  subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
            else
            {
              subProgram.setIsSubProgramPerformAlignment(false);
              subProgram.setSubProgramRefferenceIdForAlignment(highRightPanelAlignmentSet);

              // We will not perform surface map for the rest of TestSubProgram contained 
              // within the same side of panel location.
              subProgram.setIsSubProgramPerformSurfaceMap(false); 
            }
          }
          else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT) && 
              subProgram.getMagnificationType().equals(MagnificationTypeEnum.HIGH))
          {
            if (highLeftPanelAlignmentSet == -1)
            {
              subProgram.setIsSubProgramPerformAlignment(true);
              highLeftPanelAlignmentSet = subProgram.getId();
              subProgram.setSubProgramRefferenceIdForAlignment(highLeftPanelAlignmentSet);
              
              if (_enablePsp)
              {
                  // Mark the 1st TestSubProgram to perform surface map
                  subProgram.setIsSubProgramPerformSurfaceMap(true);
              }
              else
                  subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
            else
            {
              subProgram.setIsSubProgramPerformAlignment(false);
              subProgram.setSubProgramRefferenceIdForAlignment(highLeftPanelAlignmentSet);

              // We will not perform surface map for the rest of TestSubProgram contained 
              // within the same side of panel location.
              subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
          }
        }
      }
      else
      {
        int lowRightPanelAlignmentSet = -1;
        int highRightPanelAlignmentSet = -1;
        for (TestSubProgram subProgram : _testProgram.getAllInspectableTestSubPrograms())
        {
          if( subProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
          {
            if (lowRightPanelAlignmentSet == -1)
            {
              subProgram.setIsSubProgramPerformAlignment(true);
              lowRightPanelAlignmentSet = subProgram.getId();
              subProgram.setSubProgramRefferenceIdForAlignment(lowRightPanelAlignmentSet);

              if (_enablePsp)
              {
                // Mark the 1st TestSubProgram to perform surface map
                subProgram.setIsSubProgramPerformSurfaceMap(true);
              }
              else
                subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
            else
            {
              subProgram.setIsSubProgramPerformAlignment(false);
              subProgram.setSubProgramRefferenceIdForAlignment(lowRightPanelAlignmentSet);
              subProgram.setIsSubProgramPerformSurfaceMap(false); 
            }
          }
          else if( subProgram.getMagnificationType().equals(MagnificationTypeEnum.HIGH))
          {
            if (highRightPanelAlignmentSet == -1)
            {
              subProgram.setIsSubProgramPerformAlignment(true);
              highRightPanelAlignmentSet = subProgram.getId();
              subProgram.setSubProgramRefferenceIdForAlignment(highRightPanelAlignmentSet);

              if (_enablePsp)
              {
                // Mark the 1st TestSubProgram to perform surface map
                subProgram.setIsSubProgramPerformSurfaceMap(true);
              }
              else
                subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
            else
            {
              subProgram.setIsSubProgramPerformAlignment(false);
              subProgram.setSubProgramRefferenceIdForAlignment(highRightPanelAlignmentSet);
              subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
          }
        }
      }
    }
    else
    {
      Board preBoard = null;
      int lowAlignmentProgramId = -1;
      int lowLeftAlignmentProgramId = -1;
      int lowRightAlignmentProgramId = -1;
      int highAlignmentProgramId = -1;
      int highLeftAlignmentProgramId = -1;
      int highRightAlignmentProgramId = -1;
      for (TestSubProgram subProgram : _testProgram.getAllInspectableTestSubPrograms())
      {
        if(subProgram.getBoard().equals(preBoard) == false)
        {
          lowAlignmentProgramId = -1;
          lowLeftAlignmentProgramId = -1;
          lowRightAlignmentProgramId = -1;
          highAlignmentProgramId = -1;
          highLeftAlignmentProgramId = -1;
          highRightAlignmentProgramId = -1;
        }
        preBoard = subProgram.getBoard();
        if(subProgram.getBoard().isLongBoard())
        {
          if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT) && 
             subProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
          {
            if(lowRightAlignmentProgramId == -1)
            {
              subProgram.setIsSubProgramPerformAlignment(true);
              lowRightAlignmentProgramId = subProgram.getId();
              subProgram.setSubProgramRefferenceIdForAlignment(lowRightAlignmentProgramId);
              if (_enablePsp)
              {
                // Mark the 1st TestSubProgram to perform surface map
                subProgram.setIsSubProgramPerformSurfaceMap(true);
              }
              else
                subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
            else
            {
              subProgram.setIsSubProgramPerformAlignment(false);
              subProgram.setSubProgramRefferenceIdForAlignment(lowRightAlignmentProgramId);
              subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
          }
          if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT) && 
             subProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
          {
            if(lowLeftAlignmentProgramId == -1)
            {
              subProgram.setIsSubProgramPerformAlignment(true);
              lowLeftAlignmentProgramId = subProgram.getId();
              subProgram.setSubProgramRefferenceIdForAlignment(lowLeftAlignmentProgramId);
              if (_enablePsp)
              {
                // Mark the 1st TestSubProgram to perform surface map
                subProgram.setIsSubProgramPerformSurfaceMap(true);
              }
              else
                subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
            else
            {
              subProgram.setIsSubProgramPerformAlignment(false);
              subProgram.setSubProgramRefferenceIdForAlignment(lowLeftAlignmentProgramId);
              subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
          }
          if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT) && 
             subProgram.getMagnificationType().equals(MagnificationTypeEnum.HIGH))
          {
            if(highRightAlignmentProgramId == -1)
            {
              subProgram.setIsSubProgramPerformAlignment(true);
              highRightAlignmentProgramId = subProgram.getId();
              subProgram.setSubProgramRefferenceIdForAlignment(highRightAlignmentProgramId);

              if (_enablePsp)
              {
                // Mark the 1st TestSubProgram to perform surface map
                subProgram.setIsSubProgramPerformSurfaceMap(true);
              }
              else
                subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
            else
            {
              subProgram.setIsSubProgramPerformAlignment(false);
              subProgram.setSubProgramRefferenceIdForAlignment(highRightAlignmentProgramId);
              subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
          }
          if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT) && 
             subProgram.getMagnificationType().equals(MagnificationTypeEnum.HIGH))
          {
            if(highLeftAlignmentProgramId == -1)
            {
              subProgram.setIsSubProgramPerformAlignment(true);
              highLeftAlignmentProgramId = subProgram.getId();
              subProgram.setSubProgramRefferenceIdForAlignment(highLeftAlignmentProgramId);

              if (_enablePsp)
              {
                // Mark the 1st TestSubProgram to perform surface map
                subProgram.setIsSubProgramPerformSurfaceMap(true);
              }
              else
                subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
            else
            {
              subProgram.setIsSubProgramPerformAlignment(false);
              subProgram.setSubProgramRefferenceIdForAlignment(highLeftAlignmentProgramId);
              subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
          }
        }
        else
        {
          if(subProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
          {
            if(lowAlignmentProgramId == -1)
            {
              subProgram.setIsSubProgramPerformAlignment(true);
              lowAlignmentProgramId = subProgram.getId();
              subProgram.setSubProgramRefferenceIdForAlignment(lowAlignmentProgramId);

              if (_enablePsp)
              {
                // Mark the 1st TestSubProgram to perform surface map
                subProgram.setIsSubProgramPerformSurfaceMap(true);
              }
              else
                subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
            else
            {
              subProgram.setIsSubProgramPerformAlignment(false);
              subProgram.setSubProgramRefferenceIdForAlignment(lowAlignmentProgramId);
              subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
          }
          if(subProgram.getMagnificationType().equals(MagnificationTypeEnum.HIGH))
          {
            if(highAlignmentProgramId == -1)
            {
              subProgram.setIsSubProgramPerformAlignment(true);
              highAlignmentProgramId = subProgram.getId();
              subProgram.setSubProgramRefferenceIdForAlignment(highAlignmentProgramId);

              if (_enablePsp)
              {
                // Mark the 1st TestSubProgram to perform surface map
                subProgram.setIsSubProgramPerformSurfaceMap(true);
              }
              else
                subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
            else
            {
              subProgram.setIsSubProgramPerformAlignment(false);
              subProgram.setSubProgramRefferenceIdForAlignment(highAlignmentProgramId);
              subProgram.setIsSubProgramPerformSurfaceMap(false);
            }
          }
        }
      }
    }
  }

  /**
   * @return
   * @author Wei Chin
   */
  private double getZBottom()
  {
    //calculate the different offset from zHeight from cal Mag
//    double zNominalRef = XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers() / MagnificationEnum.getNominalMagnificationAtReferencePlane();
//    double zRef = XrayCameraArray.getZHeightFromCameraArrayToXraySourceInNanometers() / MagnificationEnum.getMagnificationAtReferencePlane();
//    double zoffset = zRef - zNominalRef;
//    double zBottom = zoffset - StageBelts.getNominalBeltThicknessInNanometers();
//    System.out.println("zBottom : " + MathUtil.convertNanoMetersToMils(zBottom));
//    return zoffset;
    // follow virtual Live, easy for user to set
    return 0;
  }

  /**
   * @return
   * @author Wei Chin
   */
  private double getZTop()
  {
    Assert.expect(_project != null);
    
    int thicknessInNanoMeter = _project.getPanel().getThicknessInNanometers();

    double zTop = getZBottom() + thicknessInNanoMeter;

    if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
      zTop = zTop - XrayTester.getSystemPanelThicknessOffset();//Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on";

    return zTop;
  }
  
  
  /*
   * @author Kee Chin Seong
   */
  public void resetTestProgramUponGainChanged(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);


    _testProgram = testProgram;
    //XCR-2933, IRP hung if before run inspection user have ON OFF Xray and recipe is mix mag
    _testProgram.clearTestSubProgramFilters();
    _project = testProgram.getProject();
    boolean isLongPanel = _project.getPanel().getPanelSettings().isLongPanel();

    // no longer need this - Commented out by Kee chin Seong
    // THIS 1 IS NOT APPLICABLE.
    //_testProgram.addNotInspectableTestSubProgramFilters();

    if (Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
    {
      System.out.println("generateTestProgramDebug - save as minor");
      System.out.println("generateTestProgramDebug - test_program long panel:" + isLongPanel);
    }
    clearIsSubProgramPerformAlignmentSetting();
    clearIsSubProgramPerformSurfaceMapSetting();
    initializeAlignmentSubProgram();

    _imageAcquisitionEngine.defineProcessorStripsForInspectionRegions(_testProgram);
    fitToHardwareLimits();
    setupProjectSurfaceMap();
    createVerificationRegions(true);
    setupRegionIds();
    setUpSurfaceModelingNeighbors();
    _testProgram = determineNeighborsForAll(_testProgram);
    setupGlobalSurfaceModel();
    _testProgram.clearTotalReconstructionRegionToSubProgramMap();
    _testProgram.clearSliceIdToReconstructedSliceMap();
    _testProgram.clearTestSubProgramToUserGainEnumMap();
    _testProgram.createTotalReconstructionRegionToSubProgramMap();
    
    //Kok Chun, Tan
    if (_enablePsp && _testProgram.getFilteredTestSubPrograms().isEmpty() != true)
    {
      _opticalProgramGeneration.setupNearestOpticalCameraRectangle(_testProgram);
    }
  }

  /**
   * XCR-3393, Assert after select "Auto Select Alignment Group"
   * this is a package function that required to do it in one shot which will create alignment region for alignment subprogram
   * @author Yong Sheng Chuan
   */
  private void initializeAlignmentSubProgram()
  {
    _testProgram.removeUnusedTestSubPrograms();
    initializeSubProgramForAlignment();
    createAlignmentRegionsBaseOnUserGain();
    setupTestProgramAlignmentSetting();
  }
  
  /*
   * @author sham
   * Edited by - @author Kee Chin Seong
   */
  public void resetTestProgram(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);


    _testProgram = testProgram;
    //XCR-2933, IRP hung if before run inspection user have ON OFF Xray and recipe is mix mag
    _testProgram.clearTestSubProgramFilters();
    _project = testProgram.getProject();
     boolean isLongPanel = _project.getPanel().getPanelSettings().isLongPanel();
    
    // no longer need this - Commented out by Kee chin Seong
    // THIS 1 IS NOT APPLICABLE.
    //_testProgram.addNotInspectableTestSubProgramFilters();
    
    if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
    {
      System.out.println("generateTestProgramDebug - save as minor");
      System.out.println("generateTestProgramDebug - test_program long panel:"+isLongPanel);
    }
    clearIsSubProgramPerformAlignmentSetting();
    clearIsSubProgramPerformSurfaceMapSetting();
    initializeAlignmentSubProgram();
    setupProjectSurfaceMap();
    createVerificationRegions(true);
    setupRegionIds();
    setUpSurfaceModelingNeighbors();
    _testProgram = determineNeighborsForAll(_testProgram);
    setupGlobalSurfaceModel();
    _testProgram.clearTotalReconstructionRegionToSubProgramMap();
    _testProgram.clearSliceIdToReconstructedSliceMap();
    _testProgram.clearTestSubProgramToUserGainEnumMap();
    _testProgram.createTotalReconstructionRegionToSubProgramMap();
    
    //Kok Chun, Tan
    if (_enablePsp && _testProgram.getFilteredTestSubPrograms().isEmpty() != true)
    {
      _opticalProgramGeneration.setupNearestOpticalCameraRectangle(_testProgram);
    }
  }

  /**
   * @author sham
   */
  private void clearIsSubProgramPerformAlignmentSetting()
  {
    Assert.expect(_testProgram != null);

    for (TestSubProgram subProgram : _testProgram.getAllTestSubPrograms())
    {
      subProgram.setIsSubProgramPerformAlignment(false);
    }

  }
  
  /**
   * @param useRFP
   * @param rfpTopOffsetInMilimeters
   * @param rfpBottomOffsetInMilimeters
   * @param integrationLevel
   * @param userGain
   * @author Kee, Chin Seong - Fixed Alignment failed cause of grey level issues problem.
   * @author Chong, Wei Chin
   */
  public static void customizeVerificationRegions( TestProgram testProgram,
                                            boolean useRFP,
                                            int rfpTopOffsetInMilimeters,
                                            int rfpBottomOffsetInMilimeters,
                                            SignalCompensationEnum integrationLevel,
                                            UserGainEnum userGain,
                                            StageSpeedEnum stageSpeed)
  {
    Assert.expect(testProgram != null);   
    for (TestSubProgram subProgram : testProgram.getAllTestSubPrograms())
    {
      if( subProgram.isSubProgramPerformAlignment())
      {
        ImagingChainProgramGenerator.resetDefaultMeanStepSizeLimitInNanometers();
        for( ReconstructionRegion region : subProgram.getVerificationRegions() )
        {
          if(useRFP)
          {
            for(FocusGroup focusGroup : region.getFocusGroups())
            {
              FocusSearchParameters focusSearchParameters = focusGroup.getFocusSearchParameters();
              focusSearchParameters.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
              for(Slice slice : focusGroup.getSlices())
              {
                FocusInstruction focusInstruction = slice.getFocusInstruction();
                focusInstruction.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);
                if(region.isTopSide())
                  focusInstruction.setZHeightInNanoMeters(rfpTopOffsetInMilimeters);
                else
                  focusInstruction.setZHeightInNanoMeters(rfpBottomOffsetInMilimeters);
                focusInstruction.setZOffsetInNanoMeters(0);
              }
            }
          }
          List<StageSpeedEnum> virtualLiveSpeedList = new ArrayList<StageSpeedEnum>();
          virtualLiveSpeedList.add(stageSpeed);
          region.initializeVirtualLiveSetting(virtualLiveSpeedList, userGain, integrationLevel, DynamicRangeOptimizationLevelEnum.ONE);
        }
        ImagingChainProgramGenerator.setDefaultMeanStepSizeLimitInNanometers(subProgram.getVerificationRegions().iterator().next());   
      }
      subProgram.setVerificationUserGain(userGain);
      subProgram.setVerificationStageSpeed(stageSpeed);
    }
  }


  /**
   * @author Chong, Wei Chin
   */
  public static void resetCustomVerificationRegions(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);
    for (TestSubProgram subProgram : testProgram.getAllTestSubPrograms())
    {
      if( subProgram.isSubProgramPerformAlignment())
      {
        ImagingChainProgramGenerator.resetDefaultMeanStepSizeLimitInNanometers();
        for( ReconstructionRegion region : subProgram.getVerificationRegions() )
        {
          FocusGroup focusGroup = region.getFocusGroups().get(0);
          FocusSearchParameters focusSearchParameters = focusGroup.getFocusSearchParameters();
          focusSearchParameters.setFocusProfileShape(FocusProfileShapeEnum.PEAK);
          //Khaw Chek Hau - XCR2285: 2D Alignment on v810
          FocusInstruction focusInstruction = new FocusInstruction();
          for (Slice slice : focusGroup.getSlices())
          {
            focusInstruction = slice.getFocusInstruction();   
            focusInstruction.setFocusMethod(FocusMethodEnum.SHARPEST);
          }
          region.clearVirtualLiveSetting();  
          
          if (testProgram.isManual2DAlignmentInProgress())
          {
            List<StageSpeedEnum> virtualLiveSpeedList = new ArrayList<StageSpeedEnum>();
            virtualLiveSpeedList.add(StageSpeedEnum.ONE);
            region.initializeVirtualLiveSetting(virtualLiveSpeedList, UserGainEnum.ONE, SignalCompensationEnum.DEFAULT_LOW, DynamicRangeOptimizationLevelEnum.ONE);
          }
        }
        if (testProgram.isManual2DAlignmentInProgress())
          ImagingChainProgramGenerator.setDefaultMeanStepSizeLimitInNanometers(subProgram.getVerificationRegions().iterator().next()); 
        else
          ImagingChainProgramGenerator.resetDefaultMeanStepSizeLimitInNanometers();
      }
      subProgram.setVerificationUserGain(UserGainEnum.ONE);
      subProgram.setVerificationStageSpeed(StageSpeedEnum.ONE);
    }    
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void clearIsSubProgramPerformSurfaceMapSetting()
  {
    Assert.expect(_testProgram != null);

    for (TestSubProgram subProgram : _testProgram.getAllTestSubPrograms())
    {
        subProgram.setIsSubProgramPerformSurfaceMap(false);
        subProgram.clearOpticalRegions();
        subProgram.clearAlignmentOpticalRegions();
        subProgram.clearOpticalReferenceTestSubProgram();
        subProgram.clearOpticalRegionToComponents();
    } 
  }
  
  /**
   * @param testProgram
   * @param lengthAfterAllRotationsInNanoMeters
   * @param panelLocationInSystem
   * @param verificationRegionsBounds
   * @param realVerificationRegionsBounds
   * @param imageableRegion
   * @param userGain
   * @param magnificationTypeEnum
   * @return 
   * @author Wei Chin
   */
  public TestSubProgram createTestSubProgram(TestProgram testProgram, 
                                             int lengthAfterAllRotationsInNanoMeters,
                                             int alignmentMaxImageableAreaLengthInNanoMeters,
                                             PanelLocationInSystemEnum panelLocationInSystem,
                                             PanelRectangle inspectionSectionOfPanel,
                                             PanelRectangle alignmentSectionOfPanel,
                                             UserGainEnum userGain,
                                             StageSpeedEnum stageSpeed,
                                             MagnificationTypeEnum magnificationTypeEnum)
  {
    return createTestSubProgram(testProgram,
                                lengthAfterAllRotationsInNanoMeters,
                                alignmentMaxImageableAreaLengthInNanoMeters,
                                panelLocationInSystem,
                                inspectionSectionOfPanel,
                                alignmentSectionOfPanel,
                                userGain,
                                stageSpeed,
                                magnificationTypeEnum,
                                null);
  }
  
  /**
   * @param testProgram
   * @param lengthAfterAllRotationsInNanoMeters
   * @param panelLocationInSystem
   * @param verificationRegionsBounds
   * @param realVerificationRegionsBounds
   * @param imageableRegion
   * @param userGain
   * @param magnificationTypeEnum
   * @param board
   * @return 
   * @author Wei Chin
   */
  public TestSubProgram createTestSubProgram(TestProgram testProgram,
                                             int lengthAfterAllRotationsInNanoMeters,
                                             int alignmentMaxImageableAreaLengthInNanoMeters,
                                             PanelLocationInSystemEnum panelLocationInSystem,
                                             PanelRectangle inspectionSectionOfPanel,
                                             PanelRectangle alignmentSectionOfPanel,
                                             UserGainEnum userGain,
                                             StageSpeedEnum stageSpeed,
                                             MagnificationTypeEnum magnificationTypeEnum,
                                             Board board)
  {
    Assert.expect(testProgram != null);
    Assert.expect(panelLocationInSystem != null);
    Assert.expect(inspectionSectionOfPanel != null);
    Assert.expect(alignmentSectionOfPanel != null);
    Assert.expect(userGain != null);
    Assert.expect(stageSpeed != null);
    Assert.expect(magnificationTypeEnum != null);    
    
    
    PanelRectangle imageableRegion = new PanelRectangle(inspectionSectionOfPanel.getMinX() - Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters(),
                                                        inspectionSectionOfPanel.getMinY() - Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters(),
                                                        inspectionSectionOfPanel.getWidth() + (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters()),
                                                        inspectionSectionOfPanel.getHeight() + (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters()));

    // create the bounds of the verification regions
    PanelRectangle verificationRegionsBounds = new PanelRectangle(alignmentSectionOfPanel);
    PanelRectangle realVerificationRegionsBounds = new PanelRectangle(alignmentSectionOfPanel);

    truncateBoundsIfNecessary(verificationRegionsBounds);
    truncateBoundsIfNecessary(realVerificationRegionsBounds);
    truncateBoundsIfNecessary(imageableRegion);
    
    PanelRectangle alignmentImageableRegion = new PanelRectangle(alignmentSectionOfPanel.getMinX() - Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters(),
            alignmentSectionOfPanel.getMinY() - Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters(),
            alignmentSectionOfPanel.getWidth() + (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters()),
            alignmentMaxImageableAreaLengthInNanoMeters + (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters()));
    
    TestSubProgram subProgram = new TestSubProgram(testProgram);
    subProgram.setInspectableAreaLengthInNanoMeters(lengthAfterAllRotationsInNanoMeters);
    subProgram.setPanelLocationInSystem(panelLocationInSystem);
    subProgram.setVerificationRegionsBoundsInNanoMeters(verificationRegionsBounds);
    subProgram.setRealVerificationRegionsBoundsInNanoMeters(realVerificationRegionsBounds);
    subProgram.setImageableRegionInNanoMeters(imageableRegion);
    subProgram.setAlignmentImageableRegionInNanoMeters(alignmentImageableRegion);
    subProgram.setUserGain(userGain);
    subProgram.setStageSpeed(stageSpeed);
    subProgram.setMagnificationType(magnificationTypeEnum);
    if(board != null)
      subProgram.setBoard(board);
    return subProgram;
  }
  
  /**
   * @author We Chin
   */
  protected FocusGroup createFocusGroupForVerificationOrAlignmentRegionByRFP(PanelRectangle focusRegionRectangleInNanoMeters,
      ReconstructionRegion reconstructionRegion, int zHeightInNanometer)
  {
    FocusGroup focusGroup = new FocusGroup();
    FocusSearchParameters focusSearchParameters = new FocusSearchParameters();
    FocusRegion focusRegion = new FocusRegion();
    focusRegion.setRectangleRelativeToPanelInNanoMeters(focusRegionRectangleInNanoMeters);
    focusSearchParameters.setFocusRegion(focusRegion);
    focusSearchParameters.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
    focusGroup.setFocusSearchParameters(focusSearchParameters);
    // Assume users have followed the guidelines and chosen single-sided
    // alignment regions. This will tell autofocus to just use the largest
    // peak/edge rather than trying to pick the specified side of the board.
    if (reconstructionRegion.isAlignmentRegion())
      focusGroup.setSingleSidedRegionOfBoard(true);
    Slice slice = new Slice();
    slice.setSliceName(SliceNameEnum.PAD);
    FocusInstruction focusInstruction = new FocusInstruction();
    focusInstruction.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);
    focusInstruction.setZHeightInNanoMeters(zHeightInNanometer);
    slice.setFocusInstruction(focusInstruction);
    slice.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
    slice.setAssociatedReconstructionRegion(reconstructionRegion);
    focusGroup.addSlice(slice);

    return focusGroup;
  }

  /**
   * @author We Chin
   */
  protected void updateFocusGroupForVerificationOrAlignmentRegionByRFP(ReconstructionRegion reconstructionRegion, int zHeightInNanometer)
  {
    for(FocusGroup focusGroup : reconstructionRegion.getFocusGroups())
    {
      FocusSearchParameters focusSearchParameters = focusGroup.getFocusSearchParameters();
      focusSearchParameters.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
      for(Slice slice : focusGroup.getSlices())
      {
        FocusInstruction focusInstruction = slice.getFocusInstruction();
        focusInstruction.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);
//        System.out.println("WC --> z-height : " + zHeightInNanometer);
        focusInstruction.setZHeightInNanoMeters(zHeightInNanometer);
      }
    }
  }
  
  /**
   * @author We Chin
   */
  protected void updateFocusGroupForVerificationOrAlignmentRegionByAutofocus(ReconstructionRegion reconstructionRegion)
  {
    for(FocusGroup focusGroup : reconstructionRegion.getFocusGroups())
    {
      FocusSearchParameters focusSearchParameters = focusGroup.getFocusSearchParameters();
      focusSearchParameters.setFocusProfileShape(FocusProfileShapeEnum.PEAK);
      for(Slice slice : focusGroup.getSlices())
      {
        FocusInstruction focusInstruction = slice.getFocusInstruction();
        focusInstruction.setFocusMethod(FocusMethodEnum.SHARPEST);        
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void clearAlignmentIfNecessary()
  {
    Assert.expect(_testProgram != null);
    
    PanelSettings panelSettings = _project.getPanel().getPanelSettings();
    
    // We only clear the alignment transform if we detect there is a changes from long panel to short panel
    if (panelSettings.hasModifiedFromLongPanelToShortPanel())
    {
      _project.getPanel().getPanelSettings().clearAllAlignmentTransforms();
      if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
      {
        for(Board board: _project.getPanel().getBoards())
        {
          board.getBoardSettings().clearAllAlignmentTransforms();
        }
      }
    }
  }
  
  /*
   * bee-hoon.goh
   */
  public List<String> getFocusRegionComponentResetList()
  {
    return _focusRegionComponentResetList;
  }
  
  /*
   * bee-hoon.goh
   */
  public void clearFocusRegionComponentResetList()
  {
    if(_focusRegionComponentResetList != null)
      _focusRegionComponentResetList.clear();
  }
  
  /**
   * function to create alignment region for alignment testsubprogram
   * XCR-3283 Alignment Running Slow Speed Causing Long Inspection Time
   * @author sheng chuan
   */
  private void prepareAlignmentSettingForTestSubProgram(TestSubProgram subProgram, 
                                                        List<TestSubProgram> customizeAlignmentTestSubPrograms,
                                                        List<AlignmentGroup> alignmentGroups,
                                                        Set<AlignmentGroup> alignmentGroupSet,
                                                        PanelRectangle alignmentSection)
  {
    Assert.expect(subProgram != null);
    Assert.expect(customizeAlignmentTestSubPrograms != null);
    Assert.expect(alignmentGroups != null);
    Assert.expect(alignmentGroupSet != null);
    Assert.expect(alignmentSection != null);
    
    createAlignmentRegions(subProgram, alignmentGroups);
    alignmentGroupSet.removeAll(alignmentGroups);
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setupTestProgramAlignmentSetting()
  {
    List<Subtype> subtypseUsedForAlignment = new ArrayList<Subtype>();
    for (TestSubProgram subProgram : _testProgram.getAllTestSubPrograms())
    {
      if (subProgram.isSubProgramPerformAlignment())
      {
        for(AlignmentGroup ag : subProgram.getAlignmentGroups())
        {
          for(Pad pad : ag.getPads())
          {
            Subtype subtype = pad.getSubtype();
            if (subtypseUsedForAlignment.contains(subtype) == false)
              subtypseUsedForAlignment.add(subtype);
          }
        }
        
        if (subtypseUsedForAlignment.isEmpty() == false)
        {
          Collections.sort(subtypseUsedForAlignment, _sortAlignmentSubtypeComparator);
          Subtype referenceSubtype = subtypseUsedForAlignment.iterator().next();
          subProgram.setUserGain(referenceSubtype.getSubtypeAdvanceSettings().getUserGain());
          subProgram.setStageSpeed(referenceSubtype.getSubtypeAdvanceSettings().getLowestStageSpeedSetting());
        }
      }
    }
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void create2DAlignmentRegions(TestSubProgram subProgram, 
                                       Collection<AlignmentGroup> alignmentGroups)
  {  
    int maximumAlignmentRegionWidthInNanoMeters = ReconstructionRegion.getMaxAlignmentRegionWidthInNanoMeters();
    int maximumAlignmentRegionLengthInNanoMeters = ReconstructionRegion.getMaxAlignmentRegionLengthInNanoMeters();
   
    if(subProgram.isHighMagnification())
    {
      maximumAlignmentRegionWidthInNanoMeters = ReconstructionRegion.getMaxHighMagAlignmentRegionWidthInNanoMeters();
      maximumAlignmentRegionLengthInNanoMeters = ReconstructionRegion.getMaxHighMagAlignmentRegionLengthInNanoMeters();
    }    
    
    List<ReconstructionRegion> draggedRegions = new LinkedList<ReconstructionRegion>();
    
    for (AlignmentGroup alignmentGroup : alignmentGroups)
    {
      PanelRectangle draggedRec = new PanelRectangle(alignmentGroup.getDraggedExpectedMinX(), 
                                                     alignmentGroup.getDraggedExpectedMinY(), 
                                                     alignmentGroup.getDraggedWidth(), 
                                                     alignmentGroup.getDraggedHeight());
      
      ReconstructionRegion draggedRegion = subProgram.getAlignmentRegion(alignmentGroup);
      draggedRegion.clearFocusGroups();
      draggedRegion.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.ALIGNMENT);
      draggedRegion.setFocusPriority(FocusPriorityEnum.FIRST);      
      draggedRegion.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
      draggedRegion.setAlignmentGroup(alignmentGroup);
      draggedRegion.setRegionRectangleRelativeToPanelInNanoMeters(draggedRec);
      draggedRegion.setTopSide(alignmentGroup.isTopSide());

      PanelRectangle draggedRegionRect = draggedRegion.getRegionRectangleRelativeToPanelInNanoMeters();

      // ok, add the alignment uncertainty to each side, then truncate it
      // to make it fit it fit within the processor strip bounds.
      // then truncate it again if it's bigger than the max size.
      double xCoord = draggedRegionRect.getMinX() - Alignment.getAlignmentUncertaintyBorderInNanoMeters();
      double yCoord = draggedRegionRect.getMinY() - Alignment.getAlignmentUncertaintyBorderInNanoMeters();
      PanelRectangle enlargedRegionRect = new PanelRectangle((int) xCoord,
                                                             (int) yCoord,
                                                             draggedRegionRect.getWidth() + 2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters(),
                                                             draggedRegionRect.getHeight() +
                                                             2 * Alignment.getAlignmentUncertaintyBorderInNanoMeters());

      Area regionArea = new Area(enlargedRegionRect.getRectangle2D());
      xCoord = enlargedRegionRect.getCenterX() - maximumAlignmentRegionWidthInNanoMeters / 2.0;
      yCoord = enlargedRegionRect.getCenterY() - maximumAlignmentRegionLengthInNanoMeters / 2.0;
      Area maxArea = new Area(new Rectangle2D.Double(xCoord,
                                                     yCoord,
                                                     maximumAlignmentRegionWidthInNanoMeters,
                                                     maximumAlignmentRegionLengthInNanoMeters));
      regionArea.intersect(maxArea);
      draggedRegion.setRegionRectangleRelativeToPanelInNanoMeters(new PanelRectangle(regionArea));      
      draggedRegion.addFocusGroup(createFocusGroupFor2DAlignmentRegion(new PanelRectangle(draggedRegionRect), draggedRegion));
      draggedRegion.setAllowableAlignmentRegionInNanoMetes(draggedRegionRect);
      
      List<StageSpeedEnum> speedSetting = new ArrayList<StageSpeedEnum>();
      speedSetting.add(subProgram.getAlignmentStageSpeed());
      if (alignmentGroup.useCustomizeAlignmentSetting() && alignmentGroup.isEmpty() == false)
      {
        draggedRegion.initializeVirtualLiveSetting(speedSetting, 
                                                   subProgram.getAlignmentUserGain(),
                                                   alignmentGroup.getSignalCompensationEnum(), 
                                                   DynamicRangeOptimizationLevelEnum.ONE);
      }
      else
        draggedRegion.initializeVirtualLiveSetting(speedSetting, 
                                                   subProgram.getAlignmentUserGain(),
                                                   SignalCompensationEnum.DEFAULT_LOW, 
                                                   DynamicRangeOptimizationLevelEnum.ONE);
      
      draggedRegions.add(draggedRegion);
    }
      
    subProgram.set2DAlignmentRegions(draggedRegions);
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  private void create2DAlignmentRegionsIfAlignmentLearned(TestSubProgram subProgram, List<AlignmentGroup> alignmentGroups)
  {
    //Check for any 2D Alignment learned
    String alignment2DPath = Directory.getAlignment2DImagesDir(_project.getName());
    String alignmentFileName = null;
    boolean isAllAlignment2DLearned = true;
    List<String> alignmentFileNameList = new ArrayList<String>();
    
    if (FileUtilAxi.exists(alignment2DPath))
    {
      String magnification = null;

      if (subProgram.getMagnificationType() == MagnificationTypeEnum.LOW)
      {
        if (Config.getSystemCurrentLowMagnification().equals("M19"))
          magnification = "M19";   
        else if (Config.getSystemCurrentLowMagnification().equals("M23"))
          magnification = "M23"; 
      }
      else
      {
        magnification = "M11";  
      }

      for (AlignmentGroup alignmentGroup : alignmentGroups)
      {
        alignmentFileName = null;
        String fileInDirectory = null;
        java.io.File folderToScan = new java.io.File(alignment2DPath); 
        java.io.File[] listOfFiles = folderToScan.listFiles();
        
        for (java.io.File file : listOfFiles) 
        {
          if (file.isFile()) 
          {
            fileInDirectory = file.getName();
            Matcher matcher = _alignment2DImageFileNamePattern.matcher(fileInDirectory);
            matcher.reset(fileInDirectory); 
            
            if (matcher.find()) 
            {
              if (fileInDirectory.contains(magnification + "_G" + alignmentGroup.getName()))
              {
                alignmentFileName = fileInDirectory;
                break;
              }
            }
          }
        }
         
        if (alignmentFileName == null)
        {
          isAllAlignment2DLearned = false;  
          break;
		    } 
        else
          alignmentFileNameList.add(alignmentFileName);
      }
    }
    else
    {
      clearAlignment();
      return;
    }
    
    if (isAllAlignment2DLearned)
    {
      for (AlignmentGroup alignmentGroup : alignmentGroups)
      {
        for (String alignment2DFileName : alignmentFileNameList)
        {
          if (alignment2DFileName.contains("G" + alignmentGroup.getName()))  
          {
            String[] alignmentImageInfoArray = alignment2DFileName.split("G" + alignmentGroup.getName());
            String alignmentImageInfo = alignmentImageInfoArray[1]; 

            String[] strArray = null;      
            strArray = ((String)alignmentImageInfo).split("_");

            Assert.expect(strArray[1].matches("^[+-]?\\d+$") && strArray[2].matches("^[+-]?\\d+$") 
                          && strArray[3].matches("^[+-]?\\d+$") && strArray[4].matches("^[+-]?\\d+$"),
                          "The variable is not integer, the file name is: " + (String)alignment2DFileName);

            int draggedImageExpectedMinX = Integer.parseInt(strArray[1]);
            int draggedImageExpectedMinY = Integer.parseInt(strArray[2]);
            int draggedImageWidth = Integer.parseInt(strArray[3]);
            int draggedImageHeight = Integer.parseInt(strArray[4]);
            int draggedImageActualMinX = Integer.parseInt(strArray[5]);
            int draggedImageActualMinY = Integer.parseInt(strArray[6]);

            alignmentGroup.setDraggedExpectedMinX(draggedImageExpectedMinX);
            alignmentGroup.setDraggedExpectedMinY(draggedImageExpectedMinY);
            alignmentGroup.setDraggedWidth(draggedImageWidth);
            alignmentGroup.setDraggedHeight(draggedImageHeight);
            alignmentGroup.setDraggedActualMinX(draggedImageActualMinX);
            alignmentGroup.setDraggedActualMinY(draggedImageActualMinY);
          }
        }
      }
      create2DAlignmentRegions(subProgram, alignmentGroups);
    }
    else
    {
      clearAlignment();
    }
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  private void clearAlignment()
  {
    _project.getPanel().getPanelSettings().clearAllAlignmentTransforms();
    if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
    {
      for(Board board: _project.getPanel().getBoards())
      {
        board.getBoardSettings().clearAllAlignmentTransforms();
      }
    }
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  private Comparator<Subtype> _sortAlignmentSubtypeComparator = new Comparator<Subtype>()
  {
    public int compare(Subtype lhs, Subtype rhs)
    {
      SubtypeAdvanceSettings lhsSubtypeAdvanceSetting = lhs.getSubtypeAdvanceSettings();
      SubtypeAdvanceSettings rhsSubtypeAdvanceSetting = rhs.getSubtypeAdvanceSettings();
      if (lhsSubtypeAdvanceSetting.getUserGain().toDouble() == rhsSubtypeAdvanceSetting.getUserGain().toDouble())
      {
        if (lhsSubtypeAdvanceSetting.getLowestStageSpeedSetting().toDouble() == rhsSubtypeAdvanceSetting.getLowestStageSpeedSetting().toDouble())
          return 0;
        else if (lhsSubtypeAdvanceSetting.getLowestStageSpeedSetting().toDouble() > rhsSubtypeAdvanceSetting.getLowestStageSpeedSetting().toDouble())
          return 1;
        else 
          return -1;
      }
      else if (lhsSubtypeAdvanceSetting.getUserGain().toDouble() > rhsSubtypeAdvanceSetting.getUserGain().toDouble())
        return 1;
      else
        return -1;
    }
  };

  public static void reintializeDueToSystemTypeChange()
  {
    _programGeneration = null;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3601: Auto distribute the pressfit region by direction of artifact
   */
  private Map<String, List<Pad>> separatePadsBasedOnInspectionRegionRulesForPressfit(Component component, Map<String, List<Pad>> keyToPadListMap)
  {
    Boolean isLongPanel = component.getBoard().getPanel().getPanelSettings().isLongPanel();
    PanelRectangle rightSectionOfPanel = component.getBoard().getPanel().getPanelSettings().getRightSectionOfLongPanel();
    List<Double> padSizes = new ArrayList<Double>();
    Map<String, List<Pad>> pressfitRegionSeperationKeyToPadListMap = new HashMap<String, List<Pad>>();
    Collection<Set<LandPatternPad>> separatedLandPatternPads = null;
    List<Pad> padValue = new ArrayList<Pad>();

    for (Map.Entry<String, List<Pad>> entry : keyToPadListMap.entrySet())  
    {
      String key = entry.getKey();
      padValue = entry.getValue();
      boolean isAllPadListSinglePad = true;

      separatedLandPatternPads = getSeparatedLandPatternPads(padValue);

      for (Set<LandPatternPad> padSet : separatedLandPatternPads)
      {
        if (padSet.size() > 1)
        {
          isAllPadListSinglePad = false;
          break;
        }
      }

      if (isAllPadListSinglePad)
      {
        key = padValue.get(0).getSubtype().getJointTypeEnum().getName();

        // with user defined sliceheights, no region can have more than one subtype
        key += "_" + padValue.get(0).getSubtype().getLongName();

        if (usePadRotationToBreakUpInspectionRegions(padValue.get(0).getSubtype().getInspectionFamilyEnum()))
          key += "_" + padValue.get(0).getPinOrientationInDegreesAfterAllRotations();

        if (isLongPanel)
        {
          if(component.getBoard().getPanel().getPanelSettings().isPanelBasedAlignment())
          {
            if (rightSectionOfPanel.contains(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(padValue.get(0))))
              key += "_" + PanelLocationInSystemEnum.RIGHT.getName();
            else
              key += "_" + PanelLocationInSystemEnum.LEFT.getName();
          }
          else
          {
            if(component.getBoard().getRightSectionOfLongBoard().contains(ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(padValue.get(0))))
              key += "_" + PanelLocationInSystemEnum.RIGHT.getName();
            else
              key += "_" + PanelLocationInSystemEnum.LEFT.getName();
          }
        }

        // finally, let's find a pad size that is within 20% of this pad size. we
        // want to separate pads that are more than 20% in any dimension
        double padSize = Math.max(padValue.get(0).getWidthInNanoMeters(), padValue.get(0).getLengthInNanoMeters());
        for (double size : padSizes)
        {
          double max = Math.max(padSize, size);
          double percent = 100.0 * Math.abs(size - padSize) / max;
          if (percent < 20)
          {
            // the two sizes are within 5% of each other, so use the one that is already in
            // the map
            padSize = size;
            break;
          }
        }

        int index = padSizes.indexOf(padSize);
        if (index == -1)
        {
          padSizes.add(padSize);
          index = padSizes.size() - 1;
        }

        key += "_" + index;

        List<Pad> pads = null;

        if (pressfitRegionSeperationKeyToPadListMap.containsKey(key))
        {
          pads = pressfitRegionSeperationKeyToPadListMap.get(key);
        }
        else
        {
          pads = new LinkedList<Pad>();
          pressfitRegionSeperationKeyToPadListMap.put(key, pads);
        }

        pads.addAll(padValue);
      }
      else
      {
        pressfitRegionSeperationKeyToPadListMap.put(key, padValue);
      }
    }

    if (pressfitRegionSeperationKeyToPadListMap.isEmpty() == false)
    {
      keyToPadListMap.clear();
      keyToPadListMap = pressfitRegionSeperationKeyToPadListMap;
    }
    
    return keyToPadListMap;
  }
}
