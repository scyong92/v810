package com.axi.v810.business.testGen;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;
import com.axi.v810.hardware.*;

/**
 * A utility class to aid in finding the closest surface modeling slice
 * for each focus group. The surface modeling slices can be no further away than
 * an inch in x and an inch in y.
 *
 * @author George A. David
 */
class SurfaceModelingGenerationUtil
{
  private static final int _MAX_SURROUNDING_REGIONS = 16;
  private static final int _MAX_DISTANCE_IN_NANOMETERS = (int)MathUtil.convertMilsToNanoMeters(1500); //Siew Yeng - XCR-2460

  private Map<ImageReconstructionEngineEnum, Pair<List<Double>, Map<Double, List<ReconstructionRegion>>>> _xCoordToReconstructionRegionsMaps;
  private Map<ImageReconstructionEngineEnum, Pair<List<Double>, Map<Double, List<ReconstructionRegion>>>> _yCoordToReconstructionRegionsMaps;
  private Map<ReconstructionRegion, PanelRectangle> _reconstructionRegionToFocusRegionRectMap = new HashMap<ReconstructionRegion,PanelRectangle>();
  private boolean _printDebugInfo = false;

  /**
   * @author George A. David
   */
  void setReconstructionRegions(TestSubProgram subProgram, List<ReconstructionRegion> reconstructionRegions)
  {
    _reconstructionRegionToFocusRegionRectMap.clear();
    _xCoordToReconstructionRegionsMaps = createNewInternalMap(subProgram, reconstructionRegions, true);
    _yCoordToReconstructionRegionsMaps = createNewInternalMap(subProgram, reconstructionRegions, false);

//    System.out.println("============= x coords =============");
//    for(Map.Entry<ImageReconstructionEngineEnum, Pair<List<Double>, Map<Double, List<ReconstructionRegion>>>> entry : _xCoordToReconstructionRegionsMaps.entrySet())
//    {
//      System.out.println("Reconstruction Engine: " + entry.getKey());
//      if(entry.getValue() == null)
//        continue;
//
//      for(Map.Entry<Double, List<ReconstructionRegion>> mapEntry : entry.getValue().getSecond().entrySet())
//      {
//        System.out.println("Coord: " + mapEntry.getKey());
//        for(ReconstructionRegion region : mapEntry.getValue())
//          System.out.println(region.getRegionId());
//      }
//    }
//
//    System.out.println("============= y coords =============");
//    for(Map.Entry<ImageReconstructionEngineEnum, Pair<List<Double>, Map<Double, List<ReconstructionRegion>>>> entry : _yCoordToReconstructionRegionsMaps.entrySet())
//    {
//      System.out.println("Reconstruction Engine: " + entry.getKey());
//      if(entry.getValue() == null)
//        continue;
//
//      for(Map.Entry<Double, List<ReconstructionRegion>> mapEntry : entry.getValue().getSecond().entrySet())
//      {
//        System.out.println("Coord: " + mapEntry.getKey());
//        for(ReconstructionRegion region : mapEntry.getValue())
//          System.out.println(region.getRegionId());
//      }
//    }
  }

  /**
   * @author George A. David
   */
  private Map<ImageReconstructionEngineEnum, Pair<List<Double>, Map<Double, List<ReconstructionRegion>>>> createNewInternalMap(TestSubProgram subProgram,
                                                                                                                               List<ReconstructionRegion> reconstructionRegions,
                                                                                                                               boolean useXcoord)

  {
    Assert.expect(subProgram != null);
    Assert.expect(reconstructionRegions != null);

    Map<ImageReconstructionEngineEnum, Pair<List<Double>, Map<Double, List<ReconstructionRegion>>>> coordToReconstructionRegionsMaps = new TreeMap<ImageReconstructionEngineEnum, Pair<List<Double>,Map<Double,List<ReconstructionRegion>>>>();
    for (ProcessorStrip processorStrip : subProgram.getProcessorStripsForInspectionRegions())
      coordToReconstructionRegionsMaps.put(processorStrip.getReconstructionEngineId(), null);

    for(ReconstructionRegion region : reconstructionRegions)
      addReconstructionRegion(region, coordToReconstructionRegionsMaps, useXcoord);

    for(Pair<List<Double>, Map<Double, List<ReconstructionRegion>>> pair : coordToReconstructionRegionsMaps.values())
    {
      if(pair != null)
        pair.setFirst(new ArrayList<Double>(pair.getSecond().keySet()));
    }

    return coordToReconstructionRegionsMaps;
  }

  /**
   * @author George A. David
   */
  private void addReconstructionRegion(ReconstructionRegion reconstructionRegion,
                                       Map<ImageReconstructionEngineEnum, Pair<List<Double>, Map<Double, List<ReconstructionRegion>>>> coordToReconstructionRegionsMaps,
                                       boolean useXcoord)
  {
    Assert.expect(reconstructionRegion != null);

    PanelRectangle focusRegion = null;
    if(_reconstructionRegionToFocusRegionRectMap.containsKey(reconstructionRegion))
    {
      focusRegion = _reconstructionRegionToFocusRegionRectMap.get(reconstructionRegion);
    }
    else
    {
      focusRegion = reconstructionRegion.getSurfaceModelingFocusRegion().getRectangleRelativeToPanelInNanometers();
      PanelRectangle previous = _reconstructionRegionToFocusRegionRectMap.put(reconstructionRegion, focusRegion);
      Assert.expect(previous == null);
    }
    Double coord = null;
    if(useXcoord)
      coord = new Double(focusRegion.getCenterX());
    else
      coord = new Double(focusRegion.getCenterY());

    Pair<List<Double>, Map<Double, List<ReconstructionRegion>>> pair = coordToReconstructionRegionsMaps.get(reconstructionRegion.getReconstructionEngineId());
    if(pair == null)
    {
      pair = new Pair<List<Double>, Map<Double, List<ReconstructionRegion>>>();
      pair.setSecond(new TreeMap<Double, List<ReconstructionRegion>>());
      coordToReconstructionRegionsMaps.put(reconstructionRegion.getReconstructionEngineId(), pair);
    }

    Map<Double, List<ReconstructionRegion>> coordToReconstructionRegionsMap = pair.getSecond();

    if(coordToReconstructionRegionsMap.containsKey(coord) == false)
    {
      List<ReconstructionRegion> regions = new LinkedList<ReconstructionRegion>();
      regions.add(reconstructionRegion);
      coordToReconstructionRegionsMap.put(coord, regions);
    }
    else
      coordToReconstructionRegionsMap.get(coord).add(reconstructionRegion);
  }

  /**
   * @author George A. David
   */
  private Set<ReconstructionRegion> getReconstructionRegions(Map<Double, List<ReconstructionRegion>> coordToReconstructionRegionsMap,
      List<Double> coords,
      double coord,
      int maxDistance)

  {
    Assert.expect(coordToReconstructionRegionsMap != null);
    Assert.expect(coords != null);

    Set<ReconstructionRegion> regions = new HashSet<ReconstructionRegion>();

    // find the index of the smallest acceptable coordinate
    double smallestCoordinate = coord - maxDistance;
    int beginIndex = ListUtil.findIndexOfValueGreaterThanOrEqualTo(coords, smallestCoordinate);

    // find the index of the largest acceptable coordinate
    double largestCoordinate = coord + maxDistance;
    int endIndex = ListUtil.findIndexOfValueLessThanOrEqualTo(coords, largestCoordinate);

    if(_printDebugInfo)
      System.out.println("beginIndex: " + beginIndex + "  endIndex: " + endIndex);

    for (int i = beginIndex; i <= endIndex; ++i)
      regions.addAll(coordToReconstructionRegionsMap.get(coords.get(i)));

    return regions;

  }

  /**
   * @author George A. David
   */
  public Collection<ReconstructionRegion> getSurroundingSortedReconstructionRegions(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);

    _printDebugInfo = false;
    
    if(_printDebugInfo)
      System.out.println(reconstructionRegion.getName());

    PanelRectangle regionRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    Double xCoord = new Double(regionRect.getCenterX());
    Double yCoord = new Double(regionRect.getCenterY());

    ReconstructionRegionDistanceComparator comparator = new ReconstructionRegionDistanceComparator(xCoord, yCoord);
    comparator.setUseSurfaceModelingFocusRegionForDistanceComparison(true);
    Collection<ReconstructionRegion> surroundingSortedRegions = new TreeSet<ReconstructionRegion>(comparator);

    Pair<List<Double>, Map<Double, List<ReconstructionRegion>>> pair = _xCoordToReconstructionRegionsMaps.get(reconstructionRegion.getReconstructionEngineId());
    if(pair != null)
    {
      Map<Double, List<ReconstructionRegion>> xCoordToReconstructionRegionsMap = pair.getSecond();
      List<Double> xCoords = pair.getFirst();

      pair = _yCoordToReconstructionRegionsMaps.get(reconstructionRegion.getReconstructionEngineId());
      Assert.expect(pair != null);

      Map<Double, List<ReconstructionRegion>> yCoordToReconstructionRegionsMap = pair.getSecond();
      List<Double> yCoords = pair.getFirst();

      // for performance reasons, i will split the total acceptable region, currently 1 inch by 1 inch,
      // into 10 divisions. The first division will hold regions within 100 mils in x and y,
      // the second will hold regions within 200 mils in x and y, etc...
      // then at the end, we will only add regions until we hit the max regions we need, which is currently 8.
      // this will prevent the program from unecessarily sorting regions that will not be used.

      int numDivisions = 10;
      List<List<ReconstructionRegion>> candidateRegions = new ArrayList<List<ReconstructionRegion>>(numDivisions);
      for (int i = 0; i < numDivisions; ++i)
        candidateRegions.add(new LinkedList<ReconstructionRegion>());

      int maxDistance = _MAX_DISTANCE_IN_NANOMETERS / numDivisions;

      Set<ReconstructionRegion> regions = new HashSet<ReconstructionRegion>();
      for(int divisorIndex = 0; divisorIndex < numDivisions; ++divisorIndex)
      {
        if(_printDebugInfo)
        {
          System.out.println("===============================");
          System.out.println("divisorIndex:" + divisorIndex);
        }

        regions = getReconstructionRegions(xCoordToReconstructionRegionsMap,
                                           xCoords,
                                           xCoord,
                                           maxDistance * (divisorIndex + 1));
        regions.retainAll(getReconstructionRegions(yCoordToReconstructionRegionsMap,
                                                   yCoords,
                                                   yCoord,
                                                   maxDistance * (divisorIndex + 1)));

        //remove the reconstruction region in question
        regions.remove(reconstructionRegion);

        if(_printDebugInfo)
        {
          for(ReconstructionRegion region : regions)
            System.out.println(region.getRegionId() + " " + region.getName());
        }

        if(regions.size() >= _MAX_SURROUNDING_REGIONS)
          break;
      }

      surroundingSortedRegions.addAll(regions);
      if(_printDebugInfo)
      {
        System.out.println("============== sorted regions ==============");
        for(ReconstructionRegion region : surroundingSortedRegions)
          System.out.println(region.getRegionId() + " " + region.getName());
      }
      int numSortedRegions = surroundingSortedRegions.size();
      if (numSortedRegions > _MAX_SURROUNDING_REGIONS)
      {
        surroundingSortedRegions = new ArrayList<ReconstructionRegion>(surroundingSortedRegions).subList(0, _MAX_SURROUNDING_REGIONS);
      }
    }

    if(_printDebugInfo)
    {
      System.out.println("============== sorted regions ==============");
      for(ReconstructionRegion region : surroundingSortedRegions)
        System.out.println(region.getRegionId() + " " + region.getName());
    }

    _printDebugInfo = false;

    return surroundingSortedRegions;
  }
}
