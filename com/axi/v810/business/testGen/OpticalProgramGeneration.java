package com.axi.v810.business.testGen;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

/**
 * This class responsible for creating optical regions associated with TestProgram.
 * 
 * @author Cheah Lee Herng
 */
public class OpticalProgramGeneration 
{
  // private static member variable
  private static OpticalProgramGeneration _opticalProgramGeneration = null;
  
  /**
   * Constructor
   * 
   * @author Cheah Lee Herng
   */
  public static synchronized OpticalProgramGeneration getInstance()
  {
    if (_opticalProgramGeneration == null)
      _opticalProgramGeneration = new OpticalProgramGeneration();
    return _opticalProgramGeneration;
  }
  
  /**
   * This is the main function to handle panel-based optical regions.
   * 
   * @author Cheah Lee Herng
   */
  public void setupPanelSurfaceMap(Project project, TestProgram testProgram)
  {
    Assert.expect(project != null);
    Assert.expect(testProgram != null);
             
    Panel panel = project.getPanel();
    
    if (panel.hasPanelSurfaceMapSettings())
    {
      PanelSurfaceMapSettings panelSurfaceMapSettings = panel.getPanelSurfaceMapSettings();
      if (panelSurfaceMapSettings.hasOpticalRegion())
      {
        for (OpticalRegion panelOpticalRegion : panelSurfaceMapSettings.getOpticalRegions())
          addPanelOpticalRegion(testProgram, panelOpticalRegion);
      }
    }
    
    project.setIsSurfaceMapPointPopulated(true);
  }
  
  /**
   * This is the main function to handle board-based optical regions.
   * 
   * @author Cheah Lee Herng
   */
  public void setupBoardSurfaceMap(Project project, TestProgram testProgram)
  {
    Assert.expect(project != null);
    Assert.expect(testProgram != null);
      
    Panel panel = project.getPanel();

    List<Board> boards = panel.getBoards();
    for(Board board : boards)
    {
      if (board.hasBoardSurfaceMapSettings())
      {
        BoardSurfaceMapSettings boardSurfaceMapSettings = board.getBoardSurfaceMapSettings();
        if (boardSurfaceMapSettings.hasOpticalRegion())
        {
          for (OpticalRegion boardOpticalRegion : boardSurfaceMapSettings.getOpticalRegions())
          {
            addBoardOpticalRegion(testProgram, boardOpticalRegion, board);
          }
        }
      }
    }
    
    project.setIsSurfaceMapPointPopulated(true);
    
    if (boards != null)
      boards.clear();
  }
  
  /**
   * This function handles the core operation of adding OpticalRegions into correct TestSubPrograms.
   * It considers different component with different magnification (low and high).
   * 
   * This function is only for panel-based TestProgram.
   * 
   * @author Cheah Lee Herng
   * @author Kee chin Seong Edited - Must get non inspecatable testprogram cause after change subtype
   *                                 some Test subProgram might have filtered.
   */
  public void addPanelOpticalRegion(TestProgram testProgram, OpticalRegion opticalRegion, OpticalCameraRectangle opticalCameraRectangle)
  {
    Assert.expect(testProgram != null);
    Assert.expect(opticalRegion != null);
    Assert.expect(opticalCameraRectangle != null);

    // Setup map for corresponding TestSubProgram of each OpticalCameraRectangle
    Map<OpticalCameraRectangle, List<TestSubProgram>> opticalCameraRectangleToTestSubPrograms = new HashMap<OpticalCameraRectangle, List<TestSubProgram>>();
    for(TestSubProgram testSubProgram : testProgram.getAllTestSubProgramsIncludedNoInspectableTestSubPrograms())
    {
      PanelRectangle testSubProgramImageableRegion = testSubProgram.getImageableRegionInNanoMeters();
      if (opticalCameraRectangle.getRegion().intersects(testSubProgramImageableRegion))
      {
        if (opticalCameraRectangleToTestSubPrograms.containsKey(opticalCameraRectangle) == false)
        {
          opticalCameraRectangleToTestSubPrograms.put(opticalCameraRectangle, new ArrayList<TestSubProgram>());
        }
        opticalCameraRectangleToTestSubPrograms.get(opticalCameraRectangle).add(testSubProgram);
      }
    }

    // Start doing the assignment job
    assignPanelOpticalRegion(opticalRegion, opticalCameraRectangleToTestSubPrograms);
    
    // Clear everything
    if (opticalCameraRectangleToTestSubPrograms != null)
      opticalCameraRectangleToTestSubPrograms.clear();
  }
  
  /**
   * This function is similar to addPanelOpticalRegion(TestProgram testProgram, OpticalRegion opticalRegion, OpticalCameraRectangle opticalCameraRectangle)
   * but it was optimized to improve recipe loading speed.
   * 
   * This function is only for panel-based TestProgram.
   * 
   * @param testProgram Current loaded TestProgram
   * @param opticalRegion Current OpticalRegion that is attached to current TestProgram
   * 
   * @author Cheah Lee Herng
   */
  public void addPanelOpticalRegion(TestProgram testProgram, OpticalRegion opticalRegion)
  {
    Assert.expect(testProgram != null);
    Assert.expect(opticalRegion != null);
    
    // Setup map for corresponding TestSubProgram of each OpticalCameraRectangle
    Map<OpticalCameraRectangle, List<TestSubProgram>> opticalCameraRectangleToTestSubPrograms = new HashMap<OpticalCameraRectangle, List<TestSubProgram>>();
    for(OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getAllOpticalCameraRectangles())
    {
      for(TestSubProgram testSubProgram : testProgram.getAllTestSubProgramsIncludedNoInspectableTestSubPrograms())
      {
        PanelRectangle testSubProgramImageableRegion = testSubProgram.getImageableRegionInNanoMeters();
        if (opticalCameraRectangle.getRegion().intersects(testSubProgramImageableRegion))
        {
          if (opticalCameraRectangleToTestSubPrograms.containsKey(opticalCameraRectangle) == false)
          {
            opticalCameraRectangleToTestSubPrograms.put(opticalCameraRectangle, new ArrayList<TestSubProgram>());
          }
          opticalCameraRectangleToTestSubPrograms.get(opticalCameraRectangle).add(testSubProgram);
        }
      }
      
      // Sanity Check - At least one TestSubProgram must exist
      Assert.expect(opticalCameraRectangleToTestSubPrograms.get(opticalCameraRectangle).isEmpty() == false, 
                    "OpticalRectangle " + opticalCameraRectangle.getName() + " does not have any TestSubProgram");
    }
    
    // Start doing the assignment job
    assignPanelOpticalRegion(opticalRegion, opticalCameraRectangleToTestSubPrograms);
    
    // Clear everything
    if (opticalCameraRectangleToTestSubPrograms != null)
      opticalCameraRectangleToTestSubPrograms.clear();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void assignPanelOpticalRegion(OpticalRegion opticalRegion, 
                                        Map<OpticalCameraRectangle, List<TestSubProgram>> opticalCameraRectangleToTestSubPrograms)
  {
    Assert.expect(opticalRegion != null);
    Assert.expect(opticalCameraRectangleToTestSubPrograms != null);
    
    int lowMagSubProgramReferenceIdForOpticalRegion = -1;
    int highMagSubProgramReferenceIdForOpticalRegion = -1;
    for(Component component : opticalRegion.getComponents())
    {
      boolean isLowMag = false;
      boolean isHighMag = false;
      boolean mixedMag = false;
      
      // Determine if inspected component contains mix mag or single mag 
      // and if it is single mag, is it low mag or high mag
      MagnificationTypeEnum firstMagnificationTypeEnum = null;
      for(PadType padType : component.getComponentType().getPadTypes())
      {
        if (padType.isInspected())
        {
          if (firstMagnificationTypeEnum == null)
          {
            firstMagnificationTypeEnum = padType.getSubtype().getSubtypeAdvanceSettings().getMagnificationType();
          }
          else
          {
            if (padType.getSubtype().getSubtypeAdvanceSettings().getMagnificationType().equals(firstMagnificationTypeEnum) == false)
            {
              mixedMag = true;
              break;
            }
          }
        }
      }
      
      if (firstMagnificationTypeEnum == null)
        continue;
      else
      {
        if (mixedMag == false)
        {
          if (firstMagnificationTypeEnum.equals(MagnificationTypeEnum.LOW))
            isLowMag = true;
          else if (firstMagnificationTypeEnum.equals(MagnificationTypeEnum.HIGH))
            isHighMag = true;
          else
            Assert.expect(false, "Invalid magnification type.");
        }
      }
      
      boolean isDoneLowMag = false;
      boolean isDoneHighMag = false;
      for(Map.Entry<OpticalCameraRectangle, List<TestSubProgram>> entry : opticalCameraRectangleToTestSubPrograms.entrySet())
      {
        for(TestSubProgram processTestSubProgram : entry.getValue())
        {
          if (mixedMag)
          {
            if (isDoneLowMag && isDoneHighMag)
              break;

            if (isDoneLowMag == false && processTestSubProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
            {
              if (processTestSubProgram.isOpticalRegionAvailable(opticalRegion) == false)
              {
                if (processTestSubProgram.isSubProgramPerformSurfaceMap())
                {
                  processTestSubProgram.addOpticalRegion(opticalRegion);
                  lowMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();
                  isDoneLowMag = true;
                }
              }
              else
                lowMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();

              if (processTestSubProgram.isSubProgramPerformSurfaceMap() == false &&
                  processTestSubProgram.hasComponent(component) &&
                  processTestSubProgram.isOpticalRegionNameToComponentExists(opticalRegion.getName(), component) == false)
              {
                Assert.expect(lowMagSubProgramReferenceIdForOpticalRegion > 0);
                Assert.expect(processTestSubProgram.getId() > lowMagSubProgramReferenceIdForOpticalRegion);
                processTestSubProgram.addOpticalRegionNameToSubProgramReference(opticalRegion.getName(), lowMagSubProgramReferenceIdForOpticalRegion, component);
                isDoneLowMag = true;
              }
            }
            else if (isDoneHighMag == false && processTestSubProgram.getMagnificationType().equals(MagnificationTypeEnum.HIGH))
            {
              if (processTestSubProgram.isOpticalRegionAvailable(opticalRegion) == false)
              {
                if (processTestSubProgram.isSubProgramPerformSurfaceMap())
                {
                  processTestSubProgram.addOpticalRegion(opticalRegion);
                  highMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();
                  isDoneHighMag = true;
                }
              }
              else
                highMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();

              if (processTestSubProgram.isSubProgramPerformSurfaceMap() == false &&
                  processTestSubProgram.hasComponent(component) &&
                  processTestSubProgram.isOpticalRegionNameToComponentExists(opticalRegion.getName(), component) == false)
              {
                Assert.expect(highMagSubProgramReferenceIdForOpticalRegion > 0);
                Assert.expect(processTestSubProgram.getId() > highMagSubProgramReferenceIdForOpticalRegion);
                processTestSubProgram.addOpticalRegionNameToSubProgramReference(opticalRegion.getName(), highMagSubProgramReferenceIdForOpticalRegion, component);
                isDoneHighMag = true;
              }
            }
          }
          else if (isLowMag)
          {
            if (processTestSubProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
            {
              if (processTestSubProgram.isOpticalRegionAvailable(opticalRegion) == false)
              {
                if (processTestSubProgram.isSubProgramPerformSurfaceMap())
                {
                  processTestSubProgram.addOpticalRegion(opticalRegion);
                  lowMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();
                }
              }
              else
                lowMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();

              if (processTestSubProgram.isSubProgramPerformSurfaceMap() == false &&
                  processTestSubProgram.hasComponent(component) &&
                  processTestSubProgram.isOpticalRegionNameToComponentExists(opticalRegion.getName(), component) == false)
              {
                Assert.expect(lowMagSubProgramReferenceIdForOpticalRegion > 0);
                Assert.expect(processTestSubProgram.getId() > lowMagSubProgramReferenceIdForOpticalRegion);
                processTestSubProgram.addOpticalRegionNameToSubProgramReference(opticalRegion.getName(), lowMagSubProgramReferenceIdForOpticalRegion, component);
              }
            }
          }
          else if (isHighMag)
          {
            if (processTestSubProgram.getMagnificationType().equals(MagnificationTypeEnum.HIGH))
            {
              if (processTestSubProgram.isOpticalRegionAvailable(opticalRegion) == false)
              {
                if (processTestSubProgram.isSubProgramPerformSurfaceMap())
                {
                  processTestSubProgram.addOpticalRegion(opticalRegion);
                  highMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();
                }
              }
              else
                highMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();

              if (processTestSubProgram.isSubProgramPerformSurfaceMap() == false &&
                  processTestSubProgram.hasComponent(component) &&
                  processTestSubProgram.isOpticalRegionNameToComponentExists(opticalRegion.getName(), component) == false)
              {
                Assert.expect(highMagSubProgramReferenceIdForOpticalRegion > 0);
                Assert.expect(processTestSubProgram.getId() > highMagSubProgramReferenceIdForOpticalRegion);
                processTestSubProgram.addOpticalRegionNameToSubProgramReference(opticalRegion.getName(), highMagSubProgramReferenceIdForOpticalRegion, component);
              }
            }
          }
        } // End of for-loop TestSubProgram 
      } // End of for-loop Map OpticalCameraRectangle - List TestSubPrograms
    } // End of for-loop Component
  }
  
  /**
   * This function handles the core operation of adding OpticalRegions into correct TestSubPrograms.
   * It considers different component with different magnification (low and high).
   * 
   * This function is only for board-based TestProgram.
   * 
   * @author Cheah Lee Herng
   * @author Kee chin Seong Edited - Must get non inspecatable testprogram cause after change subtype
   *                                 some Test subProgram might have filtered.
   */
  public void addBoardOpticalRegion(TestProgram testProgram, OpticalRegion opticalRegion, Board board)
  {
    Assert.expect(testProgram != null);
    Assert.expect(opticalRegion != null);
    Assert.expect(board != null);

    // Get a list of TestSubProgram which intersects with the rectangle
    List<TestSubProgram> testSubPrograms = new ArrayList<TestSubProgram>();
    List<TestSubProgram> boardTestSubPrograms = testProgram.getTestSubPrograms(board);

    for(OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getOpticalCameraRectangles(board))
    {
      for(TestSubProgram testSubProgram : boardTestSubPrograms)
      {
        PanelRectangle testSubProgramImageableRegion = testSubProgram.getImageableRegionInNanoMeters();

        if (opticalCameraRectangle.getRegion().intersects(testSubProgramImageableRegion))
        {
          if (testSubPrograms.contains(testSubProgram) == false)
          {
            testSubPrograms.add(testSubProgram);
          }
        }
      }
    }
    Assert.expect(testSubPrograms.size() > 0);    // At least one TestSubProgram must exists
    
    int lowMagSubProgramReferenceIdForOpticalRegion = -1;
    int highMagSubProgramReferenceIdForOpticalRegion = -1;
    for(Component component : opticalRegion.getComponents(board))
    {
      boolean isLowMag = false;
      boolean isHighMag = false;
      boolean mixedMag = false;
      
      // Determine if inspected component contains mix mag or single mag 
      // and if it is single mag, is it low mag or high mag
      MagnificationTypeEnum firstMagnificationTypeEnum = null;
      for(PadType padType : component.getComponentType().getPadTypes())
      {
        if (padType.isInspected())
        {
          if (firstMagnificationTypeEnum == null)
          {
            firstMagnificationTypeEnum = padType.getSubtype().getSubtypeAdvanceSettings().getMagnificationType();
          }
          else
          {
            if (padType.getSubtype().getSubtypeAdvanceSettings().getMagnificationType().equals(firstMagnificationTypeEnum) == false)
            {
              mixedMag = true;
              break;
            }
          }
        }
      }
      
      if (firstMagnificationTypeEnum == null)
        continue;
      else
      {
        if (mixedMag == false)
        {
          if (firstMagnificationTypeEnum.equals(MagnificationTypeEnum.LOW))
            isLowMag = true;
          else if (firstMagnificationTypeEnum.equals(MagnificationTypeEnum.HIGH))
            isHighMag = true;
          else
            Assert.expect(false, "Invalid magnification type.");
        }
      }
      
      boolean isDoneLowMag = false;
      boolean isDoneHighMag = false;
      for(TestSubProgram processTestSubProgram : testSubPrograms)
      {
        if (mixedMag)
        {
          if (isDoneLowMag && isDoneHighMag)
            break;

          if (isDoneLowMag == false && processTestSubProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
          {
            if (processTestSubProgram.isOpticalRegionAvailable(opticalRegion) == false)
            {
              if (processTestSubProgram.isSubProgramPerformSurfaceMap())
              {
                processTestSubProgram.addOpticalRegion(opticalRegion);
                lowMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();
                isDoneLowMag = true;
              }
            }
            else
              lowMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();

            if (processTestSubProgram.isSubProgramPerformSurfaceMap() == false &&
                processTestSubProgram.hasComponent(component) &&
                processTestSubProgram.isOpticalRegionNameToComponentExists(opticalRegion.getName(), component) == false)
            {
              Assert.expect(lowMagSubProgramReferenceIdForOpticalRegion > 0);
              Assert.expect(processTestSubProgram.getId() > lowMagSubProgramReferenceIdForOpticalRegion);
              processTestSubProgram.addOpticalRegionNameToSubProgramReference(opticalRegion.getName(), lowMagSubProgramReferenceIdForOpticalRegion, component);
              isDoneLowMag = true;
            }
          }
          else if (isDoneHighMag == false && processTestSubProgram.getMagnificationType().equals(MagnificationTypeEnum.HIGH))
          {
            if (processTestSubProgram.isOpticalRegionAvailable(opticalRegion) == false)
            {
              if (processTestSubProgram.isSubProgramPerformSurfaceMap())
              {
                processTestSubProgram.addOpticalRegion(opticalRegion);
                highMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();
                isDoneHighMag = true;
              }
            }
            else
              highMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();

            if (processTestSubProgram.isSubProgramPerformSurfaceMap() == false &&
                processTestSubProgram.hasComponent(component) &&
                processTestSubProgram.isOpticalRegionNameToComponentExists(opticalRegion.getName(), component) == false)
            {
              Assert.expect(highMagSubProgramReferenceIdForOpticalRegion > 0);
              Assert.expect(processTestSubProgram.getId() > highMagSubProgramReferenceIdForOpticalRegion);
              processTestSubProgram.addOpticalRegionNameToSubProgramReference(opticalRegion.getName(), highMagSubProgramReferenceIdForOpticalRegion, component);
              isDoneHighMag = true;
            }
          }
        }
        else if (isLowMag)
        {
          if (processTestSubProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
          {
            if (processTestSubProgram.isOpticalRegionAvailable(opticalRegion) == false)
            {
              if (processTestSubProgram.isSubProgramPerformSurfaceMap())
              {
                processTestSubProgram.addOpticalRegion(opticalRegion);
                lowMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();
                break;
              }
            }
            else
              lowMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();

            if (processTestSubProgram.isSubProgramPerformSurfaceMap() == false &&
                processTestSubProgram.hasComponent(component) &&
                processTestSubProgram.isOpticalRegionNameToComponentExists(opticalRegion.getName(), component) == false)
            {
              Assert.expect(lowMagSubProgramReferenceIdForOpticalRegion > 0);
              Assert.expect(processTestSubProgram.getId() > lowMagSubProgramReferenceIdForOpticalRegion);
              processTestSubProgram.addOpticalRegionNameToSubProgramReference(opticalRegion.getName(), lowMagSubProgramReferenceIdForOpticalRegion, component);
              break;
            }
          }
        }
        else if (isHighMag)
        {
          if (processTestSubProgram.getMagnificationType().equals(MagnificationTypeEnum.HIGH))
          {
            if (processTestSubProgram.isOpticalRegionAvailable(opticalRegion) == false)
            {
              if (processTestSubProgram.isSubProgramPerformSurfaceMap())
              {
                processTestSubProgram.addOpticalRegion(opticalRegion);
                highMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();
                break;
              }
            }
            else
              highMagSubProgramReferenceIdForOpticalRegion = processTestSubProgram.getId();

            if (processTestSubProgram.isSubProgramPerformSurfaceMap() == false &&
                processTestSubProgram.hasComponent(component) &&
                processTestSubProgram.isOpticalRegionNameToComponentExists(opticalRegion.getName(), component) == false)
            {
              Assert.expect(highMagSubProgramReferenceIdForOpticalRegion > 0);
              Assert.expect(processTestSubProgram.getId() > highMagSubProgramReferenceIdForOpticalRegion);
              processTestSubProgram.addOpticalRegionNameToSubProgramReference(opticalRegion.getName(), highMagSubProgramReferenceIdForOpticalRegion, component);
              break;
            }
          }
        }
      } // End of for-loop TestSubProgram
    } // End of for-loop Component
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setupPanelAlignmentSurfaceMap(Project project, TestProgram testProgram)
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();

    if (panel.hasPanelAlignmentSurfaceMapSettings())
    {
      PanelAlignmentSurfaceMapSettings panelAlignmentSurfaceMapSettings = panel.getPanelAlignmentSurfaceMapSettings();
      for (OpticalRegion panelAlignmentOpticalRegion : panelAlignmentSurfaceMapSettings.getOpticalRegions())
      {
        if (panelAlignmentOpticalRegion.isSetToUsePspForAlignment())
        {
          for (OpticalCameraRectangle alignmentOpticalCameraRectangle : panelAlignmentOpticalRegion.getAllOpticalCameraRectangles())
          {
            for (TestSubProgram testSubProgram : testProgram.getAllInspectableTestSubPrograms())
            {
              PanelRectangle testSubProgramImageableRegion = testSubProgram.getImageableRegionInNanoMeters();

              if (alignmentOpticalCameraRectangle.getRegion().intersects(testSubProgramImageableRegion)
                      && testSubProgram.isAlignmentOpticalRegionAvailable(panelAlignmentOpticalRegion) == false
                      && testSubProgram.isSubProgramPerformAlignment())
              {
                testSubProgram.addAlignmentOpticalRegion(panelAlignmentOpticalRegion);
              }
            }
          }
        }
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setupBoardAlignmentSurfaceMap(Project project, TestProgram testProgram)
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();

    List<Board> boards = panel.getBoards();
    for (Board board : boards)
    {
      BoardAlignmentSurfaceMapSettings boardAlignmentSurfaceMapSettings = board.getBoardAlignmentSurfaceMapSettings();
      for (OpticalRegion boardAlignmentOpticalRegion : boardAlignmentSurfaceMapSettings.getOpticalRegions())
      {
        if (boardAlignmentOpticalRegion.isSetToUsePspForAlignment())
        {
          for (OpticalCameraRectangle alignmentOpticalCameraRectangle : boardAlignmentOpticalRegion.getOpticalCameraRectangles(board))
          {
            for (TestSubProgram testSubProgram : testProgram.getTestSubPrograms(board))
            {
              PanelRectangle testSubProgramImageableRegion = testSubProgram.getImageableRegionInNanoMeters();

              if (alignmentOpticalCameraRectangle.getRegion().intersects(testSubProgramImageableRegion)
                      && testSubProgram.isAlignmentOpticalRegionAvailable(boardAlignmentOpticalRegion) == false
                      && testSubProgram.isSubProgramPerformAlignment())
              {
                testSubProgram.addAlignmentOpticalRegion(boardAlignmentOpticalRegion);
              }
            }
          }
        }
      }
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setupNearestOpticalCameraRectangle(TestProgram testProgram)
  {
    SurfaceMapping.getInstance().allocateOpticalCameraRectangleToComponents(testProgram);
  }
}
