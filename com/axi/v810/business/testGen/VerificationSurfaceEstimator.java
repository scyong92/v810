package com.axi.v810.business.testGen;

import Jama.Matrix;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.lang.Exception;
import java.util.Arrays;
import com.axi.util.Assert;
import com.axi.util.MathUtil;
import com.axi.util.MathUtilEnum;
import com.axi.util.QuadraticSurfaceEstimator;
import com.axi.util.QuadraticSurfaceEstimatorTypeEnum;
import com.axi.util.UnitTest;
import com.axi.v810.datastore.config.Config;
import com.axi.v810.datastore.config.HardwareConfigEnum;
import com.axi.v810.datastore.config.SoftwareConfigEnum;
import com.axi.v810.hardware.XrayTester;
import java.io.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.communication.SetHardwareConstants;
import com.axi.v810.hardware.*;

/**
 * <p>
 * Title: VerificationSurfaceEstimator
 * </p>
 *
 * <p>
 * Description: Estimate the verifcation image height for the panel using
 * estimates of the panel surface from the initial verification pass.
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2006
 * </p>
 *
 * <p>
 * Company: Agilent Technogies
 * </p>
 *
 * @author Rick Gaudette
 * @version 1.0
 */
public class VerificationSurfaceEstimator
{
  public final static int NORMAL                          =  0;       // Complete surface estimate done.
  public final static int TWO_PASS_DISABLED_NC            = -1;       // NO changes have been made to zTop & zBottom values.
  public final static int ABNORMAL                        =  1;       // Should never occur, it is unknown if zTop or zBottom values have changed.
  public final static int TOO_FEW_CLOSE_NEIGHBORS         =  2;       // At least one some elements of zTop & zBottom have changed.
  public final static int TOO_FEW_CLOSE_NEIGHBORS_NC      = -2;       // NO changes have been made to zTop & zBottom values.
  //public final static int CANNOT_COMPUTE_TOP_MEDIAN       =  3;       // At least one some elements of zTop & zBottom have changed.
  //public final static int CANNOT_COMPUTE_TOP_MEDIAN_NC    = -3;       // NO changes have been made to zTop & zBottom values.
  //public final static int CANNOT_COMPUTE_BOTTOM_MEDIAN    =  4;       // At least one some elements of zTop & zBottom have changed.
  //public final static int CANNOT_COMPUTE_BOTTOM_MEDIAN_NC = -4;       // NO changes have been made to zTop & zBottom values.
  public final static int TOO_FEW_VALID_POINTS            =  5;       // At least one some elements of zTop & zBottom have changed.
  public final static int TOO_FEW_VALID_POINTS_NC         = -5;       // NO changes have been made to zTop & zBottom values.
  public final static int ERROR_SOLVING_CULLED_MATRIX     =  6;       // At least one some elements of zTop & zBottom have changed.
  public final static int ERROR_SOLVING_CULLED_MATRIX_NC  = -6;       // NO changes have been made to zTop & zBottom values.
  public final static int ERROR_SOLVING_MODEL_MATRIX      =  7;       // At least one some elements of zTop & zBottom have changed.
  public final static int ERROR_SOLVING_MODEL_MATRIX_NC   = -7;       // NO changes have been made to zTop & zBottom values.

  public final static int NEIGHBOR_MAXIMUM_DELTA_Z_IN_NANOMETERS = 127000;
  public final static double NEIGHBOR_MIN_NEAR_RATIO = 5.0 / 17.0;
  public final static double NEIGHBOR_MAXIMUM_THICKNESS_CHANGE_PERCENTANGE = 10.0;
  public final static int POINT_CAPTURE_MAXIMUM_DELTA_Z_IN_NANOMETERS = 762000;
  public final static double  PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE = 15.0;
  public final static int MINIMUM_POINTS_TO_SOLVE_SURFACE = 6;

  private static Config _config;
  private int _width;
  private int _height;
  private double _nominalPanelThicknessInNM;
  private boolean _debug;
  private boolean _detailed_debug;
  private PrintWriter _dw;

  /**
   * Constructor.
   * @param width is the number of verification regions in the width dimension (the axis perendicular to the rails).
   * @param height is the number of verification regions in the height dimension (the axis parallel to the rails).
   * @param nominalPanelThicknessInNM is the nominal panel thickness in nanometers.
   * @author Rick Gaudette
   */
  public VerificationSurfaceEstimator(int width, int height, double nominalPanelThicknessInNM)
  {
    init(width, height, nominalPanelThicknessInNM, Config.isDeveloperDebugModeOn());
  }

  /**
   * @author George A. David
   */
  public static boolean isTwoPassVerificationSurfaceModelingEnabled()
  {
    if (_config == null)
      _config = Config.getInstance();

    return _config.getBooleanValue(SoftwareConfigEnum.TWO_PASS_VERIFICATION_SURFACE_MODELING_ENABLED);
  }

  /**
   * Given the initial estimates of the top and bottom verification slice
   * heights a quadratic surface model is estimated.  The top and bottom arrays
   * are modified with the estimated surface heights.
   *
   * @param zTop is the initial estimate of top verification surface z heights.
   * The size of this array must be (width * height) elements. It must also
   * index quicker through width.  The top surface estimate is returned in this
   * array.
   *
   * @param zBottom is the initial estimate of bottom verification surface z
   * heights.  The size of this array must be (width * height) elements.  It
   * must also index quicker through width.  The bottom surface estimate
   * is returned in this array.
   *
   * @param topExcluded (input) defines regions with no top side parts
   *
   * @param bottomExcluded (input) sdefines regions with no bottom side parts
   *
   * @return a status code that indicate early exits from the surface estimator
   * algorithm.  A status code of zero indicates a normal return.  A positive
   * status code indicates an early exit from the estimator's algorithm with
   * at least one value change in zTop or zBottom.  A negative status code
   * indicates an early exit from the estimator's algorithm with NO changes
   * to any value in zTop and zBottom.
   *
   * @author Rick Gaudette
   */
  public int chooseVerificationHeight(double[] zTop, double[] zBottom, boolean topExcluded[], boolean bottomExcluded[])
  {
    int returnCode = ABNORMAL;
    Assert.expect(zTop != null);
    Assert.expect(zBottom != null);
    Assert.expect(zTop != zBottom);
    final int nElements = _width * _height;
    Assert.expect(zTop.length    == nElements);
    Assert.expect(zBottom.length == nElements);
    Assert.expect(topExcluded.length    == nElements);
    Assert.expect(bottomExcluded.length == nElements);

    if (_detailed_debug)
    {
      PrintWriter pw = null;
      try
      {
        pw = new PrintWriter("C:/temp/verificationRegionUsage.txt");
        int n = 0;
        for (int i = 0; i < _height; i++)
        {
          for (int j = 0; j < _width; j++)
          {
            if (topExcluded[n] || bottomExcluded[n])
            {
              if (topExcluded[n] && bottomExcluded[n])
                pw.printf("- ");
              else if (topExcluded[n])
                pw.print("B ");
              else // bottom excluded
                pw.print("T ");
            }
            else
              pw.print("+ ");
            n++;
          }
          pw.println("");
        }
        pw.close();
      }
      catch (Exception e)
      {
        pw = null;
      }
    }

    if (! isTwoPassVerificationSurfaceModelingEnabled())
    {
      returnCode = TWO_PASS_DISABLED_NC;
      return returnCode;
    }

    try
    {
      if (_debug || _detailed_debug)
      {
        try
        {
          String dumpFileName = "C:/temp/dump_chooseVerificationHeight.txt";

          if (UnitTest.unitTesting())
            _dw = new PrintWriter(new FileOutputStream(dumpFileName, true));  // Append dumpped information.
          else
            _dw = new PrintWriter(dumpFileName);                              // Overwrite dumpped information.
        }
        catch (Exception e)
        {
          _dw = null;
        }
      }

      if ((_debug || _detailed_debug) && (_dw != null))
      {
        _dw.printf("TWO_PASS_VERIFICATION_SURFACE_MODELING_ENABLED:           %b%n",
                   _config.getBooleanValue(SoftwareConfigEnum.TWO_PASS_VERIFICATION_SURFACE_MODELING_ENABLED));
        _dw.printf("TWO_PASS_VERIFICATION_SURFACE_MODELING_DEBUG_ENABLED:     %b%n",
                   _config.getBooleanValue(SoftwareConfigEnum.TWO_PASS_VERIFICATION_SURFACE_MODELING_DEBUG_ENABLED));

        PrintWriter pw = null;
        try
        {
          pw = new PrintWriter("C:/temp/zTop_initial.txt");
          dumpArrayConvertNanometersToMils(pw, zTop, "% zTop (in mils) (initial)");
          pw.close();
          pw = new PrintWriter("C:/temp/zBottom_initial.txt");
          dumpArrayConvertNanometersToMils(pw, zBottom, "% zTop (in mils) (initiali)");
          pw.close();
        }
        catch (FileNotFoundException ex)
        {
          pw = null;
        }

      }

      // Swap any pairs where the top < bottom.
      final int nSwaps = swapInvertedPairs(zTop, zBottom);

      if (_detailed_debug)
      {
        PrintWriter pw = null;
        try
        {
          pw = new PrintWriter("C:/temp/zTop_swapped.txt");
          dumpArrayConvertNanometersToMils(pw, zTop, "% zTop (in mils) (after swapping)");
          pw.close();
          pw = new PrintWriter("C:/temp/zBottom_swapped.txt");
          dumpArrayConvertNanometersToMils(pw, zBottom, "% zTop (in mils) (after swapping)");
          pw.close();
        }
        catch (FileNotFoundException ex)
        {
          pw = null;
        }

      }

      // Count the close neighbors for each point.
      boolean[] validTop           = new boolean[nElements];
      boolean[] validTopSwapped    = new boolean[nElements];
      boolean[] validBottom        = new boolean[nElements];
      boolean[] validBottomSwapped = new boolean[nElements];

      // Find set of points that appear to represent surface (top/bottom) because they are close (in z) to their neighbor points.
      findCloseNeighbors(zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, topExcluded, bottomExcluded);
      final int countTop           = countTrue(validTop);
      final int countTopSwapped    = countTrue(validTopSwapped);
      final int countBottom        = countTrue(validBottom);
      final int countBottomSwapped = countTrue(validBottomSwapped);

      if (_detailed_debug && (_dw != null))
      {
        _dw.printf("countTrue(validTop): %d%n", countTop);
        _dw.printf("countTrue(validTopSwapped): %d%n", countTopSwapped);
        _dw.printf("countTrue(validBottom): %d%n", countBottom);
        _dw.printf("countTrue(validBottomSwapped): %d%n", countBottomSwapped);

        PrintWriter pw = null;
        try
        {
          pw = new PrintWriter("C:/temp/validTop_closeNeighbors.txt");
          dumpArray(pw, validTop, "% validTop (after findCloseNeighbors)");
          pw.close();
          pw = new PrintWriter("C:/temp/validTopSwapped_closeNeighbors.txt");
          dumpArray(pw, validTopSwapped, "% validTopSwapped (after findCloseNeighbors)");
          pw.close();
          pw = new PrintWriter("C:/temp/validBottom_closeNeighbors.txt");
          dumpArray(pw, validBottom, "% validBottom (after findCloseNeighbors)");
          pw.close();
          pw = new PrintWriter("C:/temp/validBottomSwapped_closeNeighbors.txt");
          dumpArray(pw, validBottomSwapped, "% validBottomSwappeed (after findCloseNeighbors)");
          pw.close();
        }
        catch (FileNotFoundException ex)
        {
          pw = null;
        }
      }

      int nTotalTop = countTop + countBottomSwapped;
      int nTotalBottom = countBottom + countTopSwapped;

      if ((nTotalTop + nTotalBottom) < MINIMUM_POINTS_TO_SOLVE_SURFACE)
      {
        returnCode = (nSwaps > 0) ? TOO_FEW_CLOSE_NEIGHBORS : TOO_FEW_CLOSE_NEIGHBORS_NC;
        return returnCode;
      }

      //if (nTotalTop < 1)
      //{
      //  returnCode = (nSwaps > 0) ? CANNOT_COMPUTE_TOP_MEDIAN : CANNOT_COMPUTE_TOP_MEDIAN_NC;
      //  return returnCode;
      //}

      //if (nTotalBottom < 1)
      //{
      //  returnCode = (nSwaps > 0) ? CANNOT_COMPUTE_BOTTOM_MEDIAN : CANNOT_COMPUTE_BOTTOM_MEDIAN_NC;
      //  return returnCode;
      //}

      double topMedian, bottomMedian;
      // Compute the median value of the valid top and bottom points (include swapped points).
      // If we have only top or bottom side points, fudge the other side so we can continue.
      if (nTotalBottom == 0)
      {
        topMedian = combinedValidMedian(zTop, validTop, zBottom, validBottomSwapped);
        bottomMedian = topMedian - _nominalPanelThicknessInNM;
      }
      else if (nTotalTop == 0)
      {
        bottomMedian = combinedValidMedian(zBottom, validBottom, zTop, validTopSwapped);
        topMedian = bottomMedian + _nominalPanelThicknessInNM;
      }
      else
      {
        topMedian = combinedValidMedian(zTop, validTop, zBottom, validBottomSwapped);
        bottomMedian = combinedValidMedian(zBottom, validBottom, zTop, validTopSwapped);
      }

      if (_debug && (_dw != null))
      {
        _dw.printf("topMedian:    %+22.15e%n", MathUtil.convertUnits(topMedian,    MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
        _dw.printf("bottomMedian: %+22.15e%n", MathUtil.convertUnits(bottomMedian, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
      }

      // Compute the acceptible range for top side and bottom point.s
      final double topLowerLimit    = topMedian    - XrayTester.getMaximumPanelDownWarpInNanoMeters();
      final double topUpperLimit    = topMedian    + XrayTester.getMaximumPanelUpWarpInNanoMeters()   + XrayTester.getMaximumJointHeightInNanoMeters();
      final double bottomLowerLimit = bottomMedian - XrayTester.getMaximumPanelDownWarpInNanoMeters() - XrayTester.getMaximumJointHeightInNanoMeters();
      final double bottomUpperLimit = bottomMedian + XrayTester.getMaximumPanelUpWarpInNanoMeters();

      if (_debug && (_dw != null))
      {
        _dw.printf("topLowerLimit:    %+22.15e%n", MathUtil.convertUnits(topLowerLimit,    MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
        _dw.printf("topUpperLimit:    %+22.15e%n", MathUtil.convertUnits(topUpperLimit,    MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
        _dw.printf("bottomLowerLimit: %+22.15e%n", MathUtil.convertUnits(bottomLowerLimit, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
        _dw.printf("bottomUpperLimit: %+22.15e%n", MathUtil.convertUnits(bottomUpperLimit, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
      }

      // Cull any outliers that are outside of the acceptible range.
      final int nTop           = cullOutliers(zTop,    validTop,           topLowerLimit,    topUpperLimit);
      final int nTopSwapped    = cullOutliers(zTop,    validTopSwapped,    bottomLowerLimit, bottomUpperLimit);
      final int nBottom        = cullOutliers(zBottom, validBottom,        bottomLowerLimit, bottomUpperLimit);
      final int nBottomSwapped = cullOutliers(zBottom, validBottomSwapped, topLowerLimit,    topUpperLimit);
      nTotalTop = nTop + nBottomSwapped;
      nTotalBottom = nBottom + nTopSwapped;

      if (_debug && (_dw != null))
      {
        _dw.printf("countTrue(validTop): %d%n", nTop);
        _dw.printf("countTrue(validTopSwapped): %d%n", nTopSwapped);
        _dw.printf("countTrue(validBottom): %d%n", nBottom);
        _dw.printf("countTrue(validBottomSwapped): %d%n", nBottomSwapped);

        if (_detailed_debug)
        {
          PrintWriter pw = null;
          try
          {
            pw = new PrintWriter("C:/temp/validTop_culled.txt");
            dumpArray(pw, validTop, "% validTop (after cullOutliers)");
            pw.close();
            pw = new PrintWriter("C:/temp/validTopSwapped_culled.txt");
            dumpArray(pw, validTopSwapped, "% validTopSwapped (after cullOutliers)");
            pw.close();
            pw = new PrintWriter("C:/temp/validBottom_culled.txt");
            dumpArray(pw, validBottom, "% validBottom (after cullOutliers)");
            pw.close();
            pw = new PrintWriter("C:/temp/validBottomSwapped_culled.txt");
            dumpArray(pw, validBottomSwapped, "% validBottomSwappeed (after cullOutliers)");
            pw.close();

          }
          catch (FileNotFoundException ex)
          {
            pw = null;
          }
        }
      }

      // Estimate the acutal board thickness if we have both top and bottom side samples.
      double refinedThickness;
      if (nTotalTop > 0 && nTotalBottom > 0)
        refinedThickness = refineBoardThickness(zTop, validTop, zBottom, validBottom, topMedian, bottomMedian);
      else
        refinedThickness = _nominalPanelThicknessInNM;

      if (_debug && (_dw != null))
        _dw.printf("refinedThickness (in mils): %+22.15e%n", MathUtil.convertUnits(refinedThickness, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));

      // Apply corrections to board thickness.
      final double halfThickness = refinedThickness / 2.0;

      if (_debug && (_dw != null))
      {
        _dw.printf("halfThickness (in mils):             %+22.15e%n", MathUtil.convertUnits(halfThickness,    MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));
      }

      // Construct the 2nd order width, 1st order height least squares estimate model.
      final int nValidPoints = nTop + nBottom + nTopSwapped + nBottomSwapped;

      if (_detailed_debug && (_dw != null))
        _dw.printf("nValidPoints:     %d%n", nValidPoints);

      if (nValidPoints < MINIMUM_POINTS_TO_SOLVE_SURFACE)
      {
        returnCode = (nSwaps > 0) ? TOO_FEW_VALID_POINTS : TOO_FEW_VALID_POINTS_NC;
        return returnCode;
      }

      double x[] = new double[nValidPoints];
      double y[] = new double[nValidPoints];
      double z[] = new double[nValidPoints];
      initializeVectors(x, y, z, zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, halfThickness);
      QuadraticSurfaceEstimator quadraticSurfaceEstimator;
      try {
        quadraticSurfaceEstimator =
          new QuadraticSurfaceEstimator(x, y, z, QuadraticSurfaceEstimatorTypeEnum.QUADRATIC);
      }
      catch (Exception e)
      {
        returnCode = (nSwaps > 0) ? ERROR_SOLVING_CULLED_MATRIX : ERROR_SOLVING_CULLED_MATRIX_NC;
        return returnCode;
      }

      double xAll[] = new double[nElements];
      double yAll[] = new double[nElements];
      double zAll[] = new double[nElements];
      for (int i = 0; i < nElements; ++i)
      {
        final int iWidth = i % _width;
        final int iHeight = i / _width;
        xAll[i] = iWidth;
        yAll[i] = iHeight;
      }
      zAll = quadraticSurfaceEstimator.getEstimatedHeights(xAll, yAll);

      if (_detailed_debug && (_dw != null))
      {
        PrintWriter pw = null;
        try
        {
          pw = new PrintWriter("C:/temp/zPredict.txt");
          dumpArrayConvertNanometersToMils(pw, zAll, "% zPredict");
          pw.close();
        }
        catch (FileNotFoundException ex)
        {
          pw = null;
        }
      }

      // Find the points that are near the model.
      final double pointCaptureMax = Math.min(POINT_CAPTURE_MAXIMUM_DELTA_Z_IN_NANOMETERS, refinedThickness / 2);

      int nModelPoints = 0;
      for (int i = 0; i < nElements; ++i)
      {
        validTop[i]           = (Math.abs(zAll[i] + halfThickness - zTop[i])    < pointCaptureMax) && (!topExcluded[i]);
        validTopSwapped[i]    = (Math.abs(zAll[i] - halfThickness - zTop[i])    < pointCaptureMax) && (!bottomExcluded[i]);
        validBottom[i]        = (Math.abs(zAll[i] - halfThickness - zBottom[i]) < pointCaptureMax) && (!bottomExcluded[i]);
        validBottomSwapped[i] = (Math.abs(zAll[i] + halfThickness - zBottom[i]) < pointCaptureMax) && (!topExcluded[i]);

        if (validTop[i])            ++nModelPoints;
        if (validBottom[i])         ++nModelPoints;
        if (validTopSwapped[i])     ++nModelPoints;
        if (validBottomSwapped[i])  ++nModelPoints;
      }

      if (_debug && (_dw != null))
      {
        if (_detailed_debug)
        {
          PrintWriter pw = null;
          try
          {
            pw = new PrintWriter("C:/temp/validTop_pointCapture.txt");
            dumpArray(pw, validTop, "% validTop (after pointCapture)");
            pw.close();
            pw = new PrintWriter("C:/temp/validTopSwapped_pointCapture.txt");
            dumpArray(pw, validTopSwapped, "% validTopSwapped (after pointCapture)");
            pw.close();
            pw = new PrintWriter("C:/temp/validBottom_pointCapture.txt");
            dumpArray(pw, validBottom, "% validBottom (after pointCapture)");
            pw.close();
            pw = new PrintWriter("C:/temp/validBottomSwapped_pointCapture.txt");
            dumpArray(pw, validBottomSwapped, "% validBottomSwappeed (after pointCapture)");
            pw.close();
          }
          catch (FileNotFoundException ex)
          {
            pw = null;
          }

        }
        _dw.printf("nModelPoints: %d%n", nModelPoints);
      }
      
      // XCR1550 (Lee Herng on 23-Dec-2012) - Make sure we only have valid points
      // before proceed to calling quadraticSurfaceEstimator
      if (nModelPoints < MINIMUM_POINTS_TO_SOLVE_SURFACE)
      {
        returnCode = (nSwaps > 0) ? TOO_FEW_VALID_POINTS : TOO_FEW_VALID_POINTS_NC;
        return returnCode;
      }

      // Construct a new estimate using only points near the predicted surface
      x = new double[nModelPoints];
      y = new double[nModelPoints];
      z = new double[nModelPoints];
      initializeVectors(x, y, z, zTop, validTop, validTopSwapped, zBottom, validBottom, validBottomSwapped, halfThickness);
      try
      {
        quadraticSurfaceEstimator =
          new QuadraticSurfaceEstimator(x, y, z, QuadraticSurfaceEstimatorTypeEnum.QUADRATIC);
      }
      catch (Exception e)
      {
        returnCode = (nSwaps > 0) ? ERROR_SOLVING_MODEL_MATRIX : ERROR_SOLVING_MODEL_MATRIX_NC;
        return returnCode;
      }

      // Using the model specify the z height for each verifcation region.
      zAll = quadraticSurfaceEstimator.getEstimatedHeights(xAll, yAll);

      if (_detailed_debug && (_dw != null))
      {
        PrintWriter pw = null;
        try
        {
          pw = new PrintWriter("C:/temp/zPredict_corrected.txt");
          dumpArrayConvertNanometersToMils(pw, zAll, "% zPredict (in mils) (after pointCapture & correction)");
          pw.close();
        }
        catch (FileNotFoundException ex)
        {
          pw = null;
        }
      }

      // Set the reconstruction height to the predicted surface.
      // Bound them at max and min slice heights.
      int uncertaintyReferencePlaneToNominalPlaneInNanoMeters = SetHardwareConstants.getUncertaintyReferencePlaneToNominalPlaneInNanoMeters();
      double zHeightMax_NM = XrayTester.getMaximumSliceHeightFromNominalSliceInNanometers()
                             - uncertaintyReferencePlaneToNominalPlaneInNanoMeters;
      double zHeightMin_NM = XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers()
                             + uncertaintyReferencePlaneToNominalPlaneInNanoMeters;

      for (int i = 0; i < nElements; ++i)
      {
        zBottom[i] = zAll[i] - halfThickness;
        zBottom[i] = Math.max(zBottom[i], zHeightMin_NM);
        zBottom[i] = Math.min(zBottom[i], zHeightMax_NM);

        zTop[i] = zAll[i] + halfThickness;
        zTop[i] = Math.max(zTop[i], zHeightMin_NM);
        zTop[i] = Math.min(zTop[i], zHeightMax_NM);
      }

      returnCode = NORMAL;
      return returnCode;
    }
    finally
    {
      if (_debug && (_dw != null))
      {
        if (_detailed_debug)
        {
          PrintWriter pw = null;
          try
          {
            pw = new PrintWriter("C:/temp/zTop_final.txt");
            dumpArrayConvertNanometersToMils(pw, zTop, "% zTop (in mils) (final)");
            pw.close();
            pw = new PrintWriter("C:/temp/zBottom_final.txt");
            dumpArrayConvertNanometersToMils(pw, zBottom, "% zBottom (in mils) (final)");
            pw.close();
          }
          catch (FileNotFoundException ex)
          {
            pw = null;
          }
        }

        _dw.printf("returnCode: %d%n", returnCode);
      }

      if (_dw != null)
        _dw.close();
    }
  }

  /**
   * Debug/unit test Constructor.
   * @param width is the number of verification region in the width dimension (the axis perendicular to the rails).
   * @param height is the number of verification region in the height dimension (the axis parallel to the rails).
   * @param nominalPanelThicknessInNM is the nominal panel thickness in nanometers.
   * @param debug a flag which if true causes debug output to be spewed.
   * @author Bob Balliew
   */
  VerificationSurfaceEstimator(int width,
                               int height,
                               double nominalPanelThicknessInNM,
                               boolean debug)
  {
    init(width, height, nominalPanelThicknessInNM, debug || Config.isDeveloperDebugModeOn());
  }

  /**
   * Initialize instance variables.
   * @param width is the number of verification region in the width dimension (the axis perendicular to the rails).
   * @param height is the number of verification region in the height dimension (the axis parallel to the rails).
   * @param nominalPanelThicknessInNM is the nominal panel thickness in nanometers.
   * @param debug a flag which if true causes debug output to be spewed.
   * @author Bob Balliew
   */
  private void init(int width, int height, double nominalPanelThicknessInNM, boolean debug)
  {
    _config = Config.getInstance();
    Assert.expect(_config != null);
    Assert.expect(width > 0);
    Assert.expect(height > 0);
    Assert.expect(nominalPanelThicknessInNM >= (double)_config.getIntValue(HardwareConfigEnum.PANEL_HANDLER_MINIMUM_PANEL_THICKNESS_IN_NANOMETERS));
    Assert.expect(nominalPanelThicknessInNM <= (double)_config.getIntValue(HardwareConfigEnum.PANEL_HANDLER_MAXIMUM_PANEL_THICKNESS_IN_NANOMETERS));

    _width = width;
    _height = height;
    _nominalPanelThicknessInNM = nominalPanelThicknessInNM;
    _debug = debug;
    _detailed_debug = _config.getBooleanValue(SoftwareConfigEnum.TWO_PASS_VERIFICATION_SURFACE_MODELING_DEBUG_ENABLED);
    _dw = null;
  }

  /**
   * Catagorize each top side estimate as: valid top side point, valid top swapped
   * (top estimate represents a valid bottom side estimate), or neither.
   * Catagorize each bottom side estimate as: valid bottom side point, valid bottom
   * swapped (bottom estimate represents a valid top side estimate), or neither.
   * Same and opposite side thresholds are used to perform the catagorizations.
   * @param zTop is an array of top side estimates.
   * @param validTop is an array of flags that are returned, where true indicates
   * that the corresponding element of zTop is a valid top side estimate.
   * @param validTopSwapped is an array of flags that are returned, where true indicates
   * that the corresponding element of zTop is a valid bottom side estimate.
   * @param zBottom is an array of bottom side estimates.
   * @param validBottom is an array of flags that are returned, where true indicates
   * that the corresponding element of zBottom is a valid bottom side estimate.
   * @param validBottomSwapped is an array of flags that are returned, where true indicates
   * that the corresponding element of zBottom is a valid top side estimate.
   * @param topExcluded is an array of flags indicating that this area should not
   * be used as a top side estimate
   * @param bottomExcluded is an array of flags indicating that this area should not
   * be used as a bottom side estimate
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  void findCloseNeighbors(double[] zTop,
                          boolean[] validTop,
                          boolean[] validTopSwapped,
                          double[] zBottom,
                          boolean[] validBottom,
                          boolean[] validBottomSwapped,
                          boolean[] topExcluded,
                          boolean[] bottomExcluded)
  {
    Assert.expect(zTop               != null);
    Assert.expect(validTop           != null);
    Assert.expect(validTopSwapped    != null);
    Assert.expect(zBottom            != null);
    Assert.expect(validBottom        != null);
    Assert.expect(validBottomSwapped != null);
    Assert.expect(zTop            != zBottom);
    Assert.expect(validTop        != validTopSwapped);
    Assert.expect(validTop        != validBottom);
    Assert.expect(validTop        != validBottomSwapped);
    Assert.expect(validTopSwapped != validBottom);
    Assert.expect(validTopSwapped != validBottomSwapped);
    Assert.expect(validBottom     != validBottomSwapped);
    final int nElements = _height * _width;
    Assert.expect(zTop.length               == nElements);
    Assert.expect(validTop.length           == nElements);
    Assert.expect(validTopSwapped.length    == nElements);
    Assert.expect(zBottom.length            == nElements);
    Assert.expect(validBottom.length        == nElements);
    Assert.expect(validBottomSwapped.length == nElements);
    Assert.expect(topExcluded.length        == nElements);
    Assert.expect(bottomExcluded.length     == nElements);

    // Neighbor thresholds
    final double sameSideThresh     = 2.0 * NEIGHBOR_MAXIMUM_DELTA_Z_IN_NANOMETERS;
    final double oppositeSideThresh = sameSideThresh + ((NEIGHBOR_MAXIMUM_THICKNESS_CHANGE_PERCENTANGE / 100.0) * _nominalPanelThicknessInNM);

    for (int iHeight = 0; iHeight < _height; ++iHeight)
    {
      for (int iWidth = 0; iWidth < _width; ++iWidth)
      {
        int nNeighbors     = 0;
        int nBottom        = 0;
        int nTop           = 0;
        int nBottomSwapped = 0;
        int nTopSwapped    = 0;
        final int idxPoint = iHeight * _width + iWidth;
        Assert.expect(idxPoint >= 0);
        Assert.expect(idxPoint < nElements);
        Assert.expect(zTop[idxPoint] >= zBottom[idxPoint]);

        if (!topExcluded[idxPoint] || !bottomExcluded[idxPoint])
        {
          for (int tHeight = iHeight - 1; tHeight <= iHeight + 1; ++tHeight)
          {
            if ((tHeight < 0) || (tHeight >= _height))
              continue; // Skip points that do not exist in height direction.

            for (int tWidth = iWidth - 1; tWidth <= iWidth + 1; ++tWidth)
            {
              if ((tWidth < 0) || (tWidth >= _width))
                continue; // Skip points that do not exist in width direction.

              final int idxTest = tHeight * _width + tWidth;
              Assert.expect(idxTest >= 0);
              Assert.expect(idxTest < nElements);

              if (!topExcluded[idxTest])
                nNeighbors += 1;
              if (!bottomExcluded[idxTest])
                nNeighbors += 1;

              // Check bottom side point
              if (!bottomExcluded[idxPoint])
              {
                // Bottom side neighbor
                if (!bottomExcluded[idxTest])
                {
                  if (Math.abs(zBottom[idxPoint] - zBottom[idxTest]) < sameSideThresh)
                    ++nBottom;
                  if (Math.abs(zTop[idxPoint] - zBottom[idxTest]) < sameSideThresh)
                    ++nTopSwapped;
                }

                // Top side neighbor
                if (!topExcluded[idxTest])
                {
                  final double testTopMinusThickness
                      = zTop[idxTest] - _nominalPanelThicknessInNM;
                  if (Math.abs(zBottom[idxPoint] - testTopMinusThickness) < oppositeSideThresh)
                    ++nBottom;
                  if (Math.abs(zTop[idxPoint] - testTopMinusThickness) < oppositeSideThresh)
                    ++nTopSwapped;
                }
              }

              // Check top side points
              if (!topExcluded[idxPoint])
              {
                // Bottom side neighbor
                if (!bottomExcluded[idxTest])
                {
                  final double testBottomPlusThickness
                      = zBottom[idxTest] + _nominalPanelThicknessInNM;
                  if (Math.abs(zTop[idxPoint] - testBottomPlusThickness) < oppositeSideThresh)
                    ++nTop;
                  if (Math.abs(zBottom[idxPoint] - testBottomPlusThickness) < oppositeSideThresh)
                    ++nBottomSwapped;
                }

                // Top side neighbor
                if (!topExcluded[idxTest])
                {
                  if (Math.abs(zTop[idxPoint] - zTop[idxTest]) < sameSideThresh)
                    ++nTop;
                  if (Math.abs(zBottom[idxPoint] - zTop[idxTest]) < sameSideThresh)
                  ++nBottomSwapped;
                }
              }
            }
          }

          // Calculate the ratio of neighbors that are within range compared to the total number of neighbors.
          // Subtract 1 from top and bottom because a point is always near itself.
          final double totalNeighbors = nNeighbors;

          if (!topExcluded[idxPoint])
          {
            validTop[idxPoint] = ((double)(nTop - 1) / totalNeighbors) > NEIGHBOR_MIN_NEAR_RATIO;
            validBottomSwapped[idxPoint] = ((double)nBottomSwapped / totalNeighbors) > NEIGHBOR_MIN_NEAR_RATIO;
          }
          if (!bottomExcluded[idxPoint])
          {
            validBottom[idxPoint] = ((double)(nBottom - 1) / totalNeighbors) > NEIGHBOR_MIN_NEAR_RATIO;
            validTopSwapped[idxPoint] = ((double)nTopSwapped / totalNeighbors) > NEIGHBOR_MIN_NEAR_RATIO;
          }
        }
      }
    }
  }

  /**
   * Attempt to refine the board thickness estimate using either the local or global data.
   * @param zTop is an array of top side estimates.
   * @param validTop are flags that indicate if each zTop element is a valid top side estimate.
   * @param zBottom is an array of bottom side estimates.
   * @param validBottom are flags that indicate if each zBottom element is a valid bottom side estimate.
   * @author Rick Gaudette
   */
  double refineBoardThickness(double[] zTop,
                              boolean[] validTop,
                              double[] zBottom,
                              boolean[] validBottom,
                              double topMedian,
                              double bottomMedian)
  {
    Assert.expect(zTop        != null);
    Assert.expect(validTop    != null);
    Assert.expect(zBottom     != null);
    Assert.expect(validBottom != null);
    final int nElements = _height * _width;
    Assert.expect(zTop     != zBottom);
    Assert.expect(validTop != validBottom);
    Assert.expect(zTop.length        == nElements);
    Assert.expect(validTop.length    == nElements);
    Assert.expect(zBottom.length     == nElements);
    Assert.expect(validBottom.length == nElements);

    // Compute a local estimate from valid top and bottom pairs.
    int nPairs = 0;
    double thickness = 0.0;

    for (int i = 0; i < nElements; ++i)
    {
      if (validTop[i] && validBottom[i])
      {
        thickness += (zTop[i] - zBottom[i]);
        ++nPairs;
      }
    }

    if (nPairs > 0)
      thickness /= nPairs;

    final double localAverageDelta = Math.abs(thickness - _nominalPanelThicknessInNM) / _nominalPanelThicknessInNM;
    final double maxDelta = PANEL_MAXIMUM_THICKNESS_CHANGE_PERCENTAGE / 100.0;

    if (localAverageDelta < maxDelta)
      return thickness;                 // Return the local estimate if it is within the specified range of the nominal.

    // Compute a global estimate from the medians.
    thickness = topMedian - bottomMedian;
    final double medianDelta = Math.abs(thickness - _nominalPanelThicknessInNM) / _nominalPanelThicknessInNM;

    if (medianDelta < maxDelta)
      return thickness;                 // Return the global estimate if it is within the specified range of the nominal.

    return _nominalPanelThicknessInNM;  // Neither estimate was in range return the nominal
  }

  /**
   * Construct the x, y, z vectors to use in fitting a quadratic surface model
   *
   * @author Rick Gaudette
   * @author John Heumann
   */
  void initializeVectors(double x[],
                         double y[],
                         double z[],
                         double zTop[],
                         boolean[] validTop,
                         boolean[] validTopSwapped,
                         double[] zBottom,
                         boolean[] validBottom,
                         boolean[] validBottomSwapped,
                         double halfThickness)
  {
    Assert.expect(x                  != null);
    Assert.expect(y                  != null);
    Assert.expect(z                  != null);
    Assert.expect(zTop               != null);
    Assert.expect(validTop           != null);
    Assert.expect(validTopSwapped    != null);
    Assert.expect(zBottom            != null);
    Assert.expect(validBottom        != null);
    Assert.expect(validBottomSwapped != null);
    Assert.expect(zTop            != zBottom);
    Assert.expect(validTop        != validTopSwapped);
    Assert.expect(validTop        != validBottom);
    Assert.expect(validTop        != validBottomSwapped);
    Assert.expect(validTopSwapped != validBottom);
    Assert.expect(validTopSwapped != validBottomSwapped);
    Assert.expect(validBottom     != validBottomSwapped);
    final int nElements = _height * _width;
    Assert.expect(zTop.length               == nElements);
    Assert.expect(validTop.length           == nElements);
    Assert.expect(validTopSwapped.length    == nElements);
    Assert.expect(zBottom.length            == nElements);
    Assert.expect(validBottom.length        == nElements);
    Assert.expect(validBottomSwapped.length == nElements);
    final int nValidPoints = countTrue(validTop) + countTrue(validTopSwapped) + countTrue(validBottom) + countTrue(validBottomSwapped);
    Assert.expect(x.length                  == nValidPoints);
    Assert.expect(y.length                  == nValidPoints);
    Assert.expect(z.length                  == nValidPoints);

    int iRow = 0;

    for (int i = 0; i < nElements; ++i)
    {
      final int iWidth = i % _width;
      final int iHeight = i / _width;

      if (validTop[i])
      {
        x[iRow] = iWidth;
        y[iRow] = iHeight;
        z[iRow++] = zTop[i] - halfThickness;
      }

      if (validBottom[i])
      {
        x[iRow] = iWidth;
        y[iRow] = iHeight;
        z[iRow++] = zBottom[i] + halfThickness;
      }

      if (validTopSwapped[i])
      {
        x[iRow] = iWidth;
        y[iRow] = iHeight;
        z[iRow++] = zTop[i] + halfThickness;
      }

      if (validBottomSwapped[i])
      {
        x[iRow] = iWidth;
        y[iRow] = iHeight;
        z[iRow++] = zBottom[i] - halfThickness;
      }
    }

    Assert.expect(iRow == nValidPoints);
  }

  /**
   * Compute the median of the set created by combining the valid elements from
   * two arrays.
   * @param array1 is the first array of values.
   * @param validArray1 is an array of flags indicating which elements of the first array contain valid data.
   * @param array2 is the second array of values.
   * @param validArray2 is an array of flags indicating which elements of the second array contain valid data.
   * @return the median value of the valid elements of both arrays.
   * @author Rick Gaudette
   */
  static double combinedValidMedian(double[] array1, boolean[] validArray1, double[] array2, boolean[] validArray2)
  {
    Assert.expect(array1      != null);
    Assert.expect(validArray1 != null);
    Assert.expect(array2      != null);
    Assert.expect(validArray2 != null);
    Assert.expect(array1      != array2);
    Assert.expect(validArray1 != validArray2);
    // Check references point to identically sized array per pair (this routine will function correctly if array1.length != array2.length).
    Assert.expect(array1.length == validArray1.length);
    Assert.expect(array2.length == validArray2.length);

    // Create the combined set of points
    final int nTotal = countTrue(validArray1) + countTrue(validArray2);
    Assert.expect(nTotal > 0, "No data points for median");
    double[] combined = new double[nTotal];
    int ic = 0;

    for (int i = 0; i < validArray1.length; ++i)
    {
      if (validArray1[i])
      {
        combined[ic++] = array1[i];
      }
    }

    for (int i = 0; i < validArray2.length; ++i)
    {
      if (validArray2[i])
      {
        combined[ic++] = array2[i];
      }
    }

    Assert.expect(nTotal == ic);

    // Compute the the median of the combined sets.  Return average of center pair if number of points is even.
    Arrays.sort(combined);
    return (combined[nTotal / 2] + combined[(nTotal - 1) / 2]) / 2.0;
  }

  /**
   * Cull elements outside of the specified range by modifing the valid array
   *
   * @param array is the values to be evaluated
   * @param valid are flags to indicate which values are to be evaluated by this routine.
   * For each element of the array which is culled its flag is modified to be false.
   * @param min is the minimum acceptible value
   * @param max is the maximum acceptible value
   * @return the number of elements in the array which are valid after culling outliers.
   * @author Rick Gaudette
   */
  static int cullOutliers(double[] array, boolean[] valid, double min, double max)
  {
    Assert.expect(array != null);
    Assert.expect(valid != null);
    Assert.expect(array.length == valid.length);
    int nValid = 0;

    for (int i = 0; i < valid.length; ++i)
    {
      valid[i] = valid[i] && (array[i] >= min) && (array[i] <= max);

      if (valid[i])  ++nValid;
    }

    return nValid;
  }

  /**
   * Swap any pairs of height estimates that indicate the bottom is above the
   * top of the board.
   * @param zTop is an array of top side estimates.
   * @param zBottom is an array of bottom side estimates.
   * @author Rick Gaudette
   * @author Bob Balliew
   */
  static int swapInvertedPairs(double[] zTop, double[] zBottom)
  {

    Assert.expect(zTop != null);
    Assert.expect(zBottom != null);
    Assert.expect(zTop != zBottom);
    Assert.expect(zTop.length == zBottom.length);
    final int nElements = zTop.length;
    int nSwaps = 0;

    for (int i = 0; i < nElements; ++i)
    {
      if (zTop[i] < zBottom[i])
      {
        final double temp = zTop[i];
        zTop[i] = zBottom[i];
        zBottom[i] = temp;
        ++nSwaps;
      }
    }

    return nSwaps;
  }

  /**
   * Count the number of true values in a boolean array.
   * @param array an array of flags.
   * @return the number of elements in the array that are true.
   * @author Rick Gaudette
   */
  static int countTrue(boolean[] array)
  {
    Assert.expect(array != null);
    int nTrue = 0;

    for (boolean flag : array)
    {
      if (flag)
      {
        ++nTrue;
      }
    }

    return nTrue;
  }

  /**
   * @param size the number of blanks in the string returned.
   * @return a string containing the specified number of blanks (spaces).
   * @author Bob Balliew
   */
  private static String makeBlanks(int size)
  {
    if (size <= 0)  return "";

    StringBuffer buf = new StringBuffer(size);

    for (int i = 0; i < size; ++i)
      buf.append(' ');

    return buf.toString();
  }

  /**
   * Print out the matrix (in mils).
   * @param pw is the stream to print on.
   * @param m is the matrix to print.
   * @param name is the name to print as a title.
   */
  static void printMatrixInMils(PrintWriter pw, Matrix m, String name)
  {
    // Check for no null references.
    Assert.expect(pw   != null);
    Assert.expect(m    != null);
    Assert.expect(name != null);
    pw.printf("%s%n", name);
    final int rowIndexWidth = (int)Math.log10(Math.max(m.getRowDimension() - 1, 1)) + 1;
    final int colIndexWidth = (int)Math.log10(Math.max(m.getColumnDimension() - 1, 1)) + 1;
    final int dataPrecision = 10;
    final int rowWidth      = 1 + rowIndexWidth + 1;                     // '[', row index, ']'.
    final int colWidth      = 1 + 1 + colIndexWidth + 1;                 // blank space, '[', col index, ']'.
    final int dataWidth     = 1 + 1 + 1 + 1 + dataPrecision + 1 + 1 + 2; // blank space, sign, digit, decimal point, decimal to precision digits, 'e', sign , exponent.
    final int dataColWidth  = Math.max(colWidth, dataWidth);
    final int dataPadWidth  = dataColWidth - colWidth;
    final String padFmt     = makeBlanks(rowWidth);
    final String colFmt     = makeBlanks(dataPadWidth) + " [%" + colIndexWidth + "d]";
    final String rowFmt     = "[%" + rowIndexWidth + "d]";
    final String dataFmt    = "%" + dataColWidth + "." + dataPrecision + "e";

    pw.printf(padFmt);

    for (int j = 0; j < m.getColumnDimension(); ++j)
      pw.printf(colFmt, j);

    pw.printf("%n");

    for (int i = 0; i < m.getRowDimension(); ++i)
    {
      pw.printf(rowFmt, i);

      for (int j = 0; j < m.getColumnDimension(); ++j)
        pw.printf(dataFmt, MathUtil.convertUnits(m.get(i, j), MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));

      pw.printf("%n");
    }

    pw.printf("%n");
  }

  /**
   * Print out the array.
   * @param pw is the stream to print on.
   * @param array is the array to print.
   * @param name is the name to print as a title.
   */
  static void printArray(PrintWriter pw, double[] array, String name)
  {
    Assert.expect(pw    != null);
    Assert.expect(array != null);
    Assert.expect(name  != null);
    final int rowIndexWidth = (int)Math.log10(Math.max(array.length - 1, 1)) + 1;
    final int dataPrecision = 10;
    final int dataWidth     = 1 + 1 + 1 + 1 + dataPrecision + 1 + 1 + 2; // blank space, sign, digit, decimal point, decimal to precision digits, 'e', sign , exponent.
    final String rowFmt     = "[%" + rowIndexWidth + "d]";
    final String dataFmt    = "%" + dataWidth + "." + dataPrecision + "e";
    pw.printf("%s%n", name);

    for (int i = 0; i < array.length; ++i)
      pw.printf(rowFmt + dataFmt + " (" + dataFmt + " in mils )%n", i, array[i], MathUtil.convertUnits(array[i], MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));

    pw.printf("%n");
  }

  /**
   * Dump out the the Matrix in TAB delimited columns (easy for Excel to read).
   * @param pw is the stream to dump to.
   * @param matrix is the Matrix to dump.
   * @author Bob Balliew
   */
  private void dumpMatrix(PrintWriter pw, Matrix matrix, String name)
  {
    Assert.expect(pw != null);
    Assert.expect(matrix != null);
    Assert.expect(name != null);
    pw.printf("%s%n", name);

    for (int i = 0; i < matrix.getRowDimension(); ++i)
    {
      for (int j = 0; j < matrix.getColumnDimension(); ++j)
      {
        pw.printf("%+22.15e", matrix.get(i, j));

        if ((j + 1) == matrix.getColumnDimension())
          pw.printf("%n");
        else
          pw.printf("\t");
      }
    }
  }

  /**
   * Dump out the the Matrix in TAB delimited columns (easy for Excel to read).
   * @param pw is the stream to dump to.
   * @param matrix is the Matrix to dump.
   * @author Bob Balliew
   */
  private void dumpMatrixConvertNanometersToMils(PrintWriter pw, Matrix matrix, String name)
  {
    Assert.expect(pw != null);
    Assert.expect(matrix != null);
    Assert.expect(name != null);
    pw.printf("%s%n", name);

    for (int i = 0; i < matrix.getRowDimension(); ++i)
      for (int j = 0; j < matrix.getColumnDimension(); ++j)
      {
        pw.printf("%+22.15e", MathUtil.convertUnits(matrix.get(i, j), MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));

        if ((j + 1) == matrix.getColumnDimension())
          pw.printf("%n");
        else
          pw.printf("\t");
      }
  }

  /**
   * Dump out the the array in TAB delimited columns (easy for Excel to read).
   * @param pw is the stream to dump to.
   * @param array is the array to dump.
   * @author Bob Balliew
   */
  private void dumpArrayConvertNanometersToMils(PrintWriter pw, double[] array, String name)
  {
    Assert.expect(pw != null);
    Assert.expect(array != null);
    Assert.expect(name != null);
    Assert.expect(_width > 0);
    Assert.expect((array.length % _width) == 0);
    pw.printf("%s%n", name);

    for (int i = 0; i < array.length; ++i)
    {
      pw.printf("%+22.15e", MathUtil.convertUnits(array[i], MathUtilEnum.NANOMETERS, MathUtilEnum.MILS));

      if (((i + 1) % _width) == 0)
        pw.printf("%n");
      else
        pw.printf("\t");
    }
  }

  /**
   * Dump out the the array in TAB delimited columns (easy for Excel to read).
   * @param pw is the stream to dump to.
   * @param array is the array to dump.
   * @author Bob Balliew
   */
  private void dumpArray(PrintWriter pw, boolean[] array, String name)
  {
    Assert.expect(pw != null);
    Assert.expect(array != null);
    Assert.expect(name != null);
    Assert.expect(_width > 0);
    Assert.expect((array.length % _width) == 0);
    pw.printf("%s%n", name);

    for (int i = 0; i < array.length; ++i)
    {
      pw.printf("%b", array[i]);

      if (((i + 1) % _width) == 0)
        pw.printf("%n");
      else
        pw.printf("\t");
    }
  }
}
