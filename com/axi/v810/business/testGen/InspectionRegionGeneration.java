package com.axi.v810.business.testGen;

import java.awt.geom.*;
import java.util.*;
import java.util.zip.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author wei-chin.chong
 */
public class InspectionRegionGeneration
{
  private static final int _DEGREES_ROTATION_NORMALIZATION_FACTOR = 180;
  private static final double _MIN_PAD_AREA_TO_TOTAL_AREA_RATIO_RECONSTRUCTION_REGIONS = 0.5;

  private static final boolean _TEST_IRP_BOUNDARIES = false;
  static private boolean _printDebugInfo = false;

  // this comparator first sorts in y, then in x.
  static private Comparator<PanelRectangle> _panelRectangleComparator = new Comparator<PanelRectangle>()
  {
    /**
     * @author George A. David
     */
    public int compare(PanelRectangle lhs, PanelRectangle rhs)
    {
      if(MathUtil.fuzzyEquals(lhs.getMinY(), rhs.getMinY(), MathUtil.NANOMETERS_PER_MIL))
      {
        return (int)(lhs.getMinX() - rhs.getMinX());
      }
      else
        return (int)(lhs.getMinY() - rhs.getMinY());
    }
  };

  // this comparator first sorts in y, then in x.
  static private Comparator<Point2D> _pointComparator = new Comparator<Point2D>()
  {
    /**
     * @author George A. David
     */
    public int compare(Point2D lhs, Point2D rhs)
    {
      if(lhs == rhs)
        return 0;
      
      if(MathUtil.fuzzyEquals(lhs.getY(), rhs.getY(), MathUtil.NANOMETERS_PER_MIL))
      {
        return (int)(lhs.getX() - rhs.getX());
      }
      else
        return (int)(lhs.getY() - rhs.getY());
    }
  };

  /**
   * This function uses a modified version of the k-means algorithm to group the land pattern pads into
   * inspection regions. This ensures that the pads are distributed as evenly as possible.
   * The k-means algorithm
   *    Here is an example of how it works. We start with a set of points that we want to distribute evenly:
   *
   *    x   x   x   x   x   x
   *
   *    x   x   x   x   x   x
   *
   *    x   x   x   x   x   x
   *
   *    x   x   x   x   x   x
   *
   *    x   x   x   x   x   x
   *
   *    x   x   x   x   x   x
   *
   *    We add seeds to it.
   *
   *    x   x   x   x   x   x
   *        1           2
   *    x   x   x   x   x   x
   *
   *    x   x   x   x   x   x
   *        3           4
   *    x   x   x   x   x   x
   *
   *    x   x   x   x   x   x
   *        5           6
   *    x   x   x   x   x   x
   *
   * For each point, we group it to the closest seed. Now we have a set of groups.
   * We find the center of each group and use those as seeds and run the experiment
   * again. We modify the seeds again based on the new centers of the new groups
   * and run the experiment again. Eventually we will get to a state where the
   * centers do not change. Then the algorithm is complete.
   *
   * For our case, we add an extra step. Prior to re-centering the seed points,
   * we determine if the clusters are valid. The clusters are valid if their
   * bounds do not exceed the maximum recommended width and height of an inspection
   * regions which is currently 400 x 400 mils. If the cluster is invalid, we
   * create new seed points for that cluster. If it's too wide, we split the region
   * in two along the width and create two new points at the centers of the new
   * regions. If it is too tall, we split the region in two along the height and
   * create two new points at the centers of the new regions. If it is too tall
   * and two wide, we split the region into 4, we divide in have by the width and
   * by the height. Then we create 4 new centers for the new regions. Finally we
   * run the experiment again.
   *
   * In some cases, we could keep iterating such that we keep hitting the same
   * states over and over again. In this case, we note each state prior to
   * re-centering the seeds. That way if we come across a state we already processed,
   * we know that we are in a loop that will never finish. So we use the last
   * state calculated.
   *
   * We must be care to make sure this function is deterministic. Given the same
   * set of data, it must ALWAYS calculate the same ending state. This is vital
   * for x6000. If the results are inconsistent, the inspection results the
   * customer sees will be inconsistent. This would be unacceptable.
   *
   * @author George A. David
   */
  static public Collection<Set<LandPatternPad>> clusterLandPatternPads(List<LandPatternPad> landPatternPads,
          InspectionFamilyEnum inspectionFamilyEnum, JointTypeEnum jointTypeEnum, double componentRotation, boolean highMagnification)
  {
    Assert.expect(landPatternPads != null);
    Assert.expect(inspectionFamilyEnum != null);
    Assert.expect(jointTypeEnum != null);

    Map<PanelRectangle, Set<LandPatternPad>> finalRegionToLandPatternPadsMap = new TreeMap<PanelRectangle, Set<LandPatternPad>>(_panelRectangleComparator);
    LandPatternPad firstLandPatternPad = landPatternPads.get(0);
    
    int maximumInspectionRegionSize = ReconstructionRegion.getMaxInspectionRegionSizeInNanoMeters();
    
    if(highMagnification)
      maximumInspectionRegionSize = ReconstructionRegion.getMaxHighMagInspectionRegionSizeInNanoMeters();

    if (Project.getCurrentlyLoadedProject().isBGAInspectionRegionSetTo1X1())
    {
      // RJG limit the inspection region size to a small number of joints if this is BGA
      if (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
      {
        LandPatternPad lpp = landPatternPads.get(0);

        float pitch_scale = 2.5F;
        maximumInspectionRegionSize = (int) pitch_scale * lpp.getPitchInNanoMeters();
//        System.out.printf("BGA max inspection region size: %f, pitch scale: %f\n",
//          maximumInspectionRegionSize / 1000.0F, pitch_scale);
      }
    }
    
    // normalize the rotation to within 180 degrees
    componentRotation %= _DEGREES_ROTATION_NORMALIZATION_FACTOR;

    AffineTransform rotationTransform = AffineTransform.getRotateInstance(Math.toRadians(componentRotation));


    if (InspectionFamily.isSpecialTwoPinDevice(inspectionFamilyEnum))
    {
      // chips are not to be spit up and should only have two pads
      Assert.expect(landPatternPads.size() == 2);
      PanelRectangle rect = new PanelRectangle(rotationTransform.createTransformedShape(firstLandPatternPad.getShapeInNanoMeters()));
      rect.add(new PanelRectangle(rotationTransform.createTransformedShape(landPatternPads.get(1).getShapeInNanoMeters())));
      // make sure it's not too small, this is limited by the min focus region allowd.
      if(rect.getWidth() >= FocusRegion.getMinFocusRegionSizeInNanoMeters() &&
         rect.getHeight() >= FocusRegion.getMinFocusRegionSizeInNanoMeters())
        finalRegionToLandPatternPadsMap.put(rect,
                                       new HashSet<LandPatternPad>(landPatternPads));
    }
    else
    {
      Map<LandPatternPad, Pair<PanelRectangle, Point2D>>
          padToShapeAndCoordPairMap = new HashMap<LandPatternPad, Pair<PanelRectangle, Point2D>>();

      Set<LandPatternPad> contigiousPadSet = new HashSet<LandPatternPad>(landPatternPads);
//      for (Set<LandPatternPad> contigiousPadSet : separatePadsIntoContiguousAreas(landPatternPads))
      {
        Map<PanelRectangle, Set<LandPatternPad>> regionToLandPatternPadsMap = new TreeMap<PanelRectangle, Set<LandPatternPad>>(_panelRectangleComparator);
        // create a region that encompasses all the pads, and while you're at
        // it, count the number of horizontal and vertical rows
        PanelRectangle combinedRect = null;
        for (LandPatternPad landPatternPad : contigiousPadSet)
        {
          java.awt.Shape padShape = rotationTransform.createTransformedShape(landPatternPad.getShapeInNanoMeters());
          PanelRectangle padRect = ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundRegion(new PanelRectangle(padShape),
              landPatternPad.getAlgorithmLimitedInterPadDistanceInNanoMeters());
          Point2D coord = new Point2D.Double(padRect.getCenterX(), padRect.getCenterY());
          Pair<PanelRectangle, Point2D> pair = new Pair<PanelRectangle, Point2D>(padRect, coord);
          padToShapeAndCoordPairMap.put(landPatternPad, pair);

          if (combinedRect == null)
            combinedRect = new PanelRectangle(padRect);
          else
            combinedRect.add(padRect);
        }

        List<LandPatternPad> contigiousPads = new LinkedList<LandPatternPad>(contigiousPadSet);
        Collections.sort(contigiousPads,
                         new LandPatternPadDistanceComparator(combinedRect.getMinX(), combinedRect.getMinY(), componentRotation));
//      if(_printDebugInfo)
//      {
//        System.out.println("After sorting land pattern pads");
//        for(LandPatternPad pad : contigiousPads)
//          System.out.print(pad.getName() + "  ");
//        System.out.println();
//      }

        // ok, now divide up into equal regions not larger than the maximum allowed.
        int numColumns = (int)Math.ceil(combinedRect.getWidth() /
                                        (double)maximumInspectionRegionSize);
        int numRows = (int)Math.ceil(combinedRect.getHeight() /
                                     (double)maximumInspectionRegionSize);

        // determine the width and length of each region.
        double regionWidth = combinedRect.getWidth() / (double)numColumns;
        double regionLength = combinedRect.getHeight() / (double)numRows;

        // ok, now we have a grid, create seed points for clustering int he center of each region.
        List<Point2D> seeds = new LinkedList<Point2D>();
        for (int i = 0; i < numRows; ++i)
        {
          double y = combinedRect.getMinY() + regionLength * (i + 0.5);
          for (int j = 0; j < numColumns; ++j)
          {
            double x = combinedRect.getMinX() + regionWidth * (j + 0.5);
            Point2D point = new Point2D.Double(x, y);
            seeds.add(point);
          }
        }

        Set<Long> previousValidClusters = new TreeSet<Long>();
        // now let's cluster the pads to their closest seed. if there is tie, choose the left seed.
        List<LandPatternPad> untestablePads = new LinkedList<LandPatternPad>();
        CRC32 checksumUtil = new CRC32();
        Set<Long> previousStates  = new HashSet<Long>();
        boolean done = false;
        boolean repeatSeed = false; // value will be true when there is a repeat of same set of seeds which will cause it cannot come out from the loop.
        // CR1059 - Software hanged when import certain NDF files
        // This variable is added to prevent infinite looping in the while loop.
        int repeatSameSeedChecksumCount = 0;

        while (done == false)
        {
          // this will reset the repeatSeed to false after the seeds is divided to 3 regions
          if(repeatSeed == true && (numColumns == 3 || numRows == 3))
          {
            repeatSeed = false;
          }

          Map<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>> seedToPadsMap = clusterPads(contigiousPads, padToShapeAndCoordPairMap, seeds);

          // remove unused seeds
          seeds.retainAll(seedToPadsMap.keySet());

          CRC32 seedsChecksum = new CRC32();

          List<Point2D> checkSumsSeeds = seeds;
          Collections.sort(checkSumsSeeds, _pointComparator);

          String seedStringList = "";
          // do cluster work and validation
          for (Point2D point : checkSumsSeeds)
          {
            String seedString = point.getX() + "_" + point.getY();

            if(seedStringList.indexOf(seedString) == -1)
            {
              seedStringList = seedStringList + seedString;
              seedsChecksum.update(seedString.getBytes());
            }
          }

          // this will be the unique id.
          Long checksum = seedsChecksum.getValue();
          if (previousStates.contains(checksum))
          {
            repeatSeed = true;
            ++repeatSameSeedChecksumCount;
          }
          else
          {
            repeatSameSeedChecksumCount = 0; // reset if it's not continuous. Make sure it's continuously repeat.
            previousStates.add(checksum);
          }

          // ok, now lets validate the clusters, their combined space must
          // not be bigger than 400 mils by 400 mils. if one is, split the region
          // in half, create new seeds in the center and re-cluster.
          boolean clustersWereInvalidated = false;

          for (Map.Entry<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>> mapEntry : seedToPadsMap.entrySet())
          {
            PanelRectangle entryRect = mapEntry.getValue().getSecond();
            if (isClusterValid(mapEntry.getValue().getFirst(), entryRect, inspectionFamilyEnum, jointTypeEnum, maximumInspectionRegionSize) == false)
            {
              // ok, we need to check the case where a single pad is bigger than the desired 400 mils size.
              // if this is so, then our only choice is to see if it fits within the maximum inspection region
              // which is 1000 x 550 mils. If not, then it is untestable. if we don't do this, we will get
              // caught up in an infinite loop.
              if (mapEntry.getValue().getFirst().size() == 1)
              {
                if (highMagnification == false && (
                    entryRect.getWidth() <= ReconstructionRegion.getMaxInspectionRegionPadBoundsWidthInNanoMeters() ||
                    entryRect.getHeight() <= ReconstructionRegion.getMaxInspectionRegionPadBoundsLengthInNanoMeters()) )
                {
                  continue;
                }
                else if(highMagnification && (
                    entryRect.getWidth() <= ReconstructionRegion.getMaxHighMagInspectionRegionPadBoundsWidthInNanoMeters() ||
                    entryRect.getHeight() <= ReconstructionRegion.getMaxHighMagInspectionRegionPadBoundsLengthInNanoMeters()))
                {
                  continue;
                }
                else // the pad is untestable
                {
                  untestablePads.add(mapEntry.getValue().getFirst().iterator().next());
//                  if (UnitTest.unitTesting() == false)
//                  {
//                    LandPatternPad pad = mapEntry.getValue().getFirst().iterator().next();
//                    System.out.print("land pattern pad " + pad.getName() + " of land pattern " + pad.getLandPattern().getName());
//
//                    if (entryRect.getWidth() > ReconstructionRegion.getMaxInspectionRegionPadBoundsWidthInNanoMeters())
//                      System.out.println(" is too wide to fit in a reconstruction region (width: " +
//                                         MathUtil.convertNanoMetersToMils(entryRect.getWidth()) + ")");
//                    else if (entryRect.getHeight() > ReconstructionRegion.getMaxInspectionRegionPadBoundsLengthInNanoMeters())
//                      System.out.println(" is too long to fit in a reconstruction region (length: " +
//                                         MathUtil.convertNanoMetersToMils(entryRect.getHeight()) + ")");
//                    else
//                      Assert.expect(false);
//                  }
                }
              }
              else if (repeatSameSeedChecksumCount > 5)
              {
                if (entryRect.getWidth() <= maximumInspectionRegionSize &&
                    entryRect.getHeight() <= maximumInspectionRegionSize)
                {
                  continue; //Let it be even though it does not fulfill the minimum 50% pad to total area ratio.
                }
                else
                {
                  // Make all untestable if they are not within maximum inspection region size.
                  untestablePads.addAll(mapEntry.getValue().getFirst());
                }
              }

              clustersWereInvalidated = true;

              numRows = 1;
              numColumns = 1;

              if (entryRect.getWidth() > maximumInspectionRegionSize)
              {
                if(repeatSeed == true)
                {
                  numColumns = 3;
                }
                else
                {
                  numColumns = 2;
                }
              }

              if (entryRect.getHeight() > maximumInspectionRegionSize)
              {
                if(repeatSeed == true)
                {
                  numRows = 3;
                }
                else
                {
                  numRows = 2;
                }
              }

              if (numRows == 1 && numColumns == 1)
              {
                // this means it failed because of either a neighbor rule or
                // pad are rule, so let's split it up into 4 regions.
                if(repeatSeed == true)
                {
                  numColumns = 3;
                  numRows = 3;
                }
                else
                {
                  numColumns = 2;
                  numRows = 2;
                }
              }

              Assert.expect(numColumns >= 2 || numRows >= 2);

              // determine the width and length of each region.
              regionWidth = entryRect.getWidth() / (double)numColumns;
              regionLength = entryRect.getHeight() / (double)numRows;

              List<Point2D> newSeeds = new LinkedList<Point2D>();
              for (int i = 0; i < numRows; ++i)
              {
                double y = entryRect.getMinY() + regionLength * (i + 0.5);
                for (int j = 0; j < numColumns; ++j)
                {
                  double x = entryRect.getMinX() + regionWidth * (j + 0.5);
                  Point2D point = new Point2D.Double(x, y);
                  newSeeds.add(point);
                }
              }

              seeds.remove(mapEntry.getKey());
              // There are cases where the splited seeds are the same location as the seed from neighbouring pads, due to repeating patterns and conincidence.
              // To prevent duplicates, add when it's not already exist.
              // Variable "seeds" is a List, so it can contain duplicates.
//              seeds.addAll(newSeeds);
              for (Point2D newSeed : newSeeds)
              {
                if (seeds.contains(newSeed) == false)
                  seeds.add(newSeed);
              }
            }
          }

          boolean clustersWereEliminated = false;
          boolean clusterCentersWereModified = false;

          if (clustersWereInvalidated == false)
          {
            // create a unique string for this cluster set and save it's result
            String clusterString = "";
            Set<String> clusterPadNames = new TreeSet<String>();
            checksumUtil.reset();
            for (Map.Entry<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>> mapEntry : seedToPadsMap.entrySet())
            {
              Point2D point = mapEntry.getKey();
              PanelRectangle regionRect = mapEntry.getValue().getSecond();
              clusterString += "_" + point.getX() +
                  "_" + point.getY() +
                  "_" + regionRect.getMinX() +
                  "_" + regionRect.getMinY() +
                  "_" + regionRect.getWidth() +
                  "_" + regionRect.getHeight();
              checksumUtil.update(clusterString.getBytes());

              if (_printDebugInfo)
              {
                Set<String> names = new TreeSet<String>();
                for (LandPatternPad pad : mapEntry.getValue().getFirst())
                  names.add(pad.getName());

                String clusterName = "";
                for (String name : names)
                  clusterName += name + " ";

                PanelRectangle rect = mapEntry.getValue().getSecond();
                clusterName += rect.getCenterX() + " " + rect.getCenterY();

                clusterPadNames.add(clusterName);
              }
            }
            if (_printDebugInfo)
            {
              System.out.println("Cluster Strings " + previousValidClusters.size() + ":");
              for (String name : clusterPadNames)
                System.out.println(name);
            }
            if (previousValidClusters.contains(checksumUtil.getValue()))
              // we are going in circles, let's just stop it here.
              done = true;
            else
            {
              previousValidClusters.add(checksumUtil.getValue());
              regionToLandPatternPadsMap.clear();
              for (Map.Entry<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>> mapEntry : seedToPadsMap.entrySet())
              {
                Pair<Set<LandPatternPad>, PanelRectangle> pair = mapEntry.getValue();
                regionToLandPatternPadsMap.put(pair.getSecond(), pair.getFirst());
              }

              // ok, by this point, all of the clusters have been validated.
              // now let's recenter the points and try again.
              for (Map.Entry<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>> mapEntry : seedToPadsMap.entrySet())
              {
                Point2D point = mapEntry.getKey();
                PanelRectangle rect = mapEntry.getValue().getSecond();
                int centerX = rect.getCenterX();
                int centerY = rect.getCenterY();
                if (point.getX() != centerX ||
                    point.getY() != centerY)
                {
                  clusterCentersWereModified = true;
                  point.setLocation(centerX, centerY);
                }
              }

              if (clusterCentersWereModified == false)
              {
                // now let's try to eliminate clusters.
                Set<Point2D> processedClusters = new HashSet<Point2D>();
                while (processedClusters.size() < seedToPadsMap.size())
                {
                  for (Map.Entry<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>> mapEntry : seedToPadsMap.entrySet())
                  {
                    Point2D point = mapEntry.getKey();
                    if (processedClusters.contains(point))
                      continue;
                    if (eliminateClusterIfPossible(mapEntry, seedToPadsMap, padToShapeAndCoordPairMap, inspectionFamilyEnum, jointTypeEnum, maximumInspectionRegionSize))
                    {
                      clustersWereEliminated = true;
                      seeds.remove(point);
                      break;
                    }
                    processedClusters.add(point);
                  }
                }
              }
            }
          }

          if(previousValidClusters.size() >= 200)
            // we've hit our limit, let's use what we have, otherwise we could go on forever
            done = true;

          if (done != true)
            done = (clustersWereEliminated == false) &&
                   (clustersWereInvalidated == false) &&
                   (clusterCentersWereModified == false);
          contigiousPads.removeAll(untestablePads);
          untestablePads.clear();
        }

//        System.out.println("Number of iterations to cluster pads for LandPattern " + landPatternPads.get(0).getLandPattern().getName() + ": " + previousValidClusters.size());

        // ok, let's check for any regions that do not meet the area ratio guidelines
        // and let's split them up.
        splitClustersThatDontMeetAreaRatioSpecs(regionToLandPatternPadsMap, padToShapeAndCoordPairMap, inspectionFamilyEnum, jointTypeEnum, maximumInspectionRegionSize);
        if (_printDebugInfo)
        {
          System.out.println("Final Split Clusters:");
          Set<String> clusterNames = new TreeSet<String>();
          for (Set<LandPatternPad> pads : regionToLandPatternPadsMap.values())
          {
            Set<String> padNames = new TreeSet<String>();

            for (LandPatternPad pad : pads)
              padNames.add(pad.getName());

            String clusterName = "";
            for (String padName : padNames)
              clusterName += padName + " ";

            clusterNames.add(clusterName);
          }

          for (String name : clusterNames)
            System.out.println(name);

        }

        // ok, as a last final sanity check, let's combine any regions we can.
        combineClusters(regionToLandPatternPadsMap, inspectionFamilyEnum, jointTypeEnum, maximumInspectionRegionSize);

        // finally, if there are any regions less then the min focus region, then
        // those pads are untestable.
        for (PanelRectangle rect : new HashSet<PanelRectangle>(regionToLandPatternPadsMap.keySet()))
        {
          // make sure it's not too small, this is limited by the min focus region allowd.
          if (rect.getWidth() < FocusRegion.getMinFocusRegionSizeInNanoMeters() &&
              rect.getHeight() < FocusRegion.getMinFocusRegionSizeInNanoMeters())
            regionToLandPatternPadsMap.remove(rect);
        }

        finalRegionToLandPatternPadsMap.putAll(regionToLandPatternPadsMap);
      }
    }

    if(_printDebugInfo)
    {
      System.out.println("Final Clusters:");
      Set<String> clusterNames = new TreeSet<String>();
      for(Set<LandPatternPad> pads : finalRegionToLandPatternPadsMap.values())
      {
        Set<String> padNames = new TreeSet<String>();

        for(LandPatternPad pad : pads)
          padNames.add(pad.getName());

        String clusterName = "";
        for(String padName : padNames)
          clusterName += padName + " ";

        clusterNames.add(clusterName);
      }

      for(String name : clusterNames)
        System.out.println(name);

    }
    return finalRegionToLandPatternPadsMap.values();
  }

  /**
   * @author George A. David
   */
  static private void splitClustersThatDontMeetAreaRatioSpecs(Map<PanelRectangle, Set<LandPatternPad>> regionToLandPatternPadsMap,
                                                       Map<LandPatternPad, Pair<PanelRectangle, Point2D>>  padToShapeAndCoordPairMap,
                                                       InspectionFamilyEnum inspectionFamilyEnum,
													   JointTypeEnum jointTypeEnum,
                                                       int maximumInspectionRegionSizeInNanometers)
  {
    Assert.expect(regionToLandPatternPadsMap != null);
    Assert.expect(jointTypeEnum != null);

    List<Map.Entry<PanelRectangle, Set<LandPatternPad>>> invalidatedEntries = new LinkedList<Map.Entry<PanelRectangle, Set<LandPatternPad>>>();
    for(Map.Entry<PanelRectangle, Set<LandPatternPad>> mapEntry : regionToLandPatternPadsMap.entrySet())
    {
      if(doPadsMeetAreaRatioSpecs(mapEntry.getValue(), inspectionFamilyEnum, jointTypeEnum) == false)
        invalidatedEntries.add(mapEntry);
    }

    for(Map.Entry<PanelRectangle, Set<LandPatternPad>> entry : invalidatedEntries)
    {
      // determine the width and length of each region.
      double regionWidth = entry.getKey().getWidth() / 4.0;
      double regionLength = entry.getKey().getHeight() / 4.0;

      List<Point2D> newSeeds = new LinkedList<Point2D>();
      for (int i = 0; i < 4; ++i)
      {
        double y = entry.getKey().getMinY() + regionLength * (i + 0.5);
        for (int j = 0; j < 4; ++j)
        {
          double x = entry.getKey().getMinX() + regionWidth * (j + 0.5);
          Point2D point = new Point2D.Double(x, y);
          newSeeds.add(point);
        }
      }

      Map<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>> seedToPadsMap = clusterPads(entry.getValue(), padToShapeAndCoordPairMap, newSeeds);
      Map<PanelRectangle, Set<LandPatternPad>> newRegionToLandPatternPadsMap = new HashMap<PanelRectangle, Set<LandPatternPad>>();
      for(Map.Entry<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>> newEntry : seedToPadsMap.entrySet())
      {
        newRegionToLandPatternPadsMap.put(newEntry.getValue().getSecond(), newEntry.getValue().getFirst());
      }

      combineClusters(newRegionToLandPatternPadsMap, inspectionFamilyEnum, jointTypeEnum, maximumInspectionRegionSizeInNanometers);

      regionToLandPatternPadsMap.remove(entry.getKey());
      regionToLandPatternPadsMap.putAll(newRegionToLandPatternPadsMap);
    }
  }

 /**
   * @author George A. David
   */
  static private Map<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>> clusterPads
      (Collection<LandPatternPad> landPatternPads,
      Map<LandPatternPad, Pair<PanelRectangle, Point2D>>  padToShapeAndCoordPairMap,
      List<Point2D> seeds)
  {
    Map<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>>
        seedToPadsMap = new TreeMap<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>>(_pointComparator);

    for (LandPatternPad pad : landPatternPads)
    {
      Point2D coord = padToShapeAndCoordPairMap.get(pad).getSecond();
      Point2D closestPoint = null;
      double closestDistanceSquared = -1;
      for (Point2D point : seeds)
      {
        double distanceSquared = MathUtil.calculateDistanceSquared(coord.getX(), coord.getY(), point.getX(),
            point.getY());
        if (closestPoint == null)
        {
          closestPoint = point;
          closestDistanceSquared = distanceSquared;
        }
        else
        {
          if (distanceSquared < closestDistanceSquared)
          {
            closestPoint = point;
            closestDistanceSquared = distanceSquared;
          }
          else if (distanceSquared == closestDistanceSquared)
          {
            // first choose the one that is less in y, then less in x
            if (closestPoint.getY() == point.getY())
            {
              if (closestPoint.getX() > point.getX())
              {
                closestPoint = point;
                closestDistanceSquared = distanceSquared;
              }
            }
            else if (closestPoint.getY() > point.getY())
            {
              closestPoint = point;
              closestDistanceSquared = distanceSquared;
            }
          }
        }
      }

      if (seedToPadsMap.containsKey(closestPoint))
      {
        Pair<Set<LandPatternPad>, PanelRectangle> pair = seedToPadsMap.get(closestPoint);
        pair.getFirst().add(pad);
        pair.getSecond().add(padToShapeAndCoordPairMap.get(pad).getFirst());
      }
      else
      {
        Set<LandPatternPad> closestPads = new HashSet<LandPatternPad>();
        closestPads.add(pad);
        Pair<Set<LandPatternPad>, PanelRectangle> pair = new Pair<Set<LandPatternPad>, PanelRectangle>();
        pair.setFirst(closestPads);
        pair.setSecond(new PanelRectangle(padToShapeAndCoordPairMap.get(pad).getFirst()));

        seedToPadsMap.put(closestPoint, pair);
      }
    }

    return seedToPadsMap;
  }

  /**
   * @author George A. David
   */
  static private double calculateLandPatternPadAreaToTotalAreaRatio(Set<LandPatternPad> landPatternPads)
  {
    Assert.expect(landPatternPads != null);

    double padAreaInMilsSquared = 0.0;
    Rectangle2D totalRect = null;
    for (LandPatternPad landPatternPad : landPatternPads)
    {
      Rectangle2D shapeBounds = landPatternPad.getShapeInNanoMeters().getBounds2D();
      // for circular pads, we use the "pitch bounds"
      if (landPatternPad.isPerfectCircle())
        shapeBounds.setRect(shapeBounds.getCenterX() - (landPatternPad.getPitchInNanoMeters() / 2.0),
                            shapeBounds.getCenterY() - (landPatternPad.getPitchInNanoMeters() / 2.0),
                            landPatternPad.getPitchInNanoMeters(),
                            landPatternPad.getPitchInNanoMeters());

      padAreaInMilsSquared += MathUtil.convertNanoMetersToMils(shapeBounds.getWidth()) *
      MathUtil.convertNanoMetersToMils(shapeBounds.getHeight());

      if (totalRect == null)
        totalRect = shapeBounds;
      else
        totalRect.add(shapeBounds);
    }

    double totalAreaInMilsSquared = MathUtil.convertNanoMetersToMils(totalRect.getWidth()) *
    MathUtil.convertNanoMetersToMils(totalRect.getHeight());

    return padAreaInMilsSquared / totalAreaInMilsSquared;
  }

  /**
   * @author George A. David
   */
  static private boolean doPadsMeetAreaRatioSpecs(Set<LandPatternPad> pads, 
                                                  InspectionFamilyEnum inspectionFamilyEnum,
                                                  JointTypeEnum jointTypeEnum)
  {
    Assert.expect(pads != null);
    Assert.expect(inspectionFamilyEnum != null);
    Assert.expect(jointTypeEnum != null);

    boolean passesMinimumSpecs = false;
    if(inspectionFamilyEnum.equals(InspectionFamilyEnum.GRID_ARRAY) ||
      (inspectionFamilyEnum.equals(InspectionFamilyEnum.LARGE_PAD) && jointTypeEnum.equals(JointTypeEnum.LGA)))
      passesMinimumSpecs = true;
    else if(pads.iterator().next().getLandPattern().getPanel().getProject().getScanPathMethod().getId() >= ScanPathMethodEnum.METHOD_THROUGHOLE_PRESSFIT_SKIP_AREA_RATIO_CHECK.getId() &&
           (inspectionFamilyEnum.equals(InspectionFamilyEnum.THROUGHHOLE) || 
            inspectionFamilyEnum.equals(InspectionFamilyEnum.PRESSFIT) ||
            inspectionFamilyEnum.equals(InspectionFamilyEnum.OVAL_THROUGHHOLE))) //Siew Yeng - XCR-3318 - Oval PTH
    {
      //XCR-2760 - Siew Yeng - Scan path method 6
      //divide region equally (like GridArray) - this can help to speed up project loading
      passesMinimumSpecs = true;
    }
    else
    {
      double padAreaToTotalAreaRatio = calculateLandPatternPadAreaToTotalAreaRatio(pads);
      // check if the ratio is bigger than the minimum spec, and also just
      // for a sanity check, if the region has less than 4 pads, let's try again
      if (padAreaToTotalAreaRatio >= _MIN_PAD_AREA_TO_TOTAL_AREA_RATIO_RECONSTRUCTION_REGIONS) // && regionLandPatternPads.size() > 2)
      {
        // ok, if this is a grid array, we need to make one last check before we accept.
        // we need to check the neighbor rule.
//      if (inspectionFamilyEnum.equals(InspectionFamilyEnum.GRID_ARRAY))
//      {
//        if (passesGridArrayLandPatternPadsNeighborRule(pads))
//          passesMinimumSpecs = true;
//        else
//          passesMinimumSpecs = false;
//      }
//      else
        passesMinimumSpecs = true;
      }
      else
        passesMinimumSpecs = false;
    }
    return passesMinimumSpecs;
  }

  /**
   * @author George A. David
   */
  static private boolean isClusterValid(Set<LandPatternPad> pads, PanelRectangle region, 
                                        InspectionFamilyEnum inspectionFamilyEnum,
										JointTypeEnum jointTypeEnum,
                                        int maxInspectionRegionSizeInNanoMeters)
  {
    Assert.expect(pads != null);
    Assert.expect(region != null);
    if (region.getWidth() <= maxInspectionRegionSizeInNanoMeters &&
        region.getHeight() <= maxInspectionRegionSizeInNanoMeters &&
        doPadsMeetAreaRatioSpecs(pads, inspectionFamilyEnum, jointTypeEnum))
      return true;

    return false;
  }

  /**
   * @author George A. David
   */
  static private boolean eliminateClusterIfPossible(Map.Entry<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>> clusterToEliminate,
                                                    Map<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>> seedToPadsMap,
                                                    Map<LandPatternPad, Pair<PanelRectangle, Point2D>> padToShapeAndCoordPairMap,
                                                    InspectionFamilyEnum inspectionFamilyEnum,
                                                    JointTypeEnum jointTypeEnum,
													int maxInspectionRegionSizeInNanoMeters)
  {
    Map<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>> seedToNewPadsMap = new HashMap<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>>();
    List<LandPatternPad> pads = new LinkedList<LandPatternPad>(clusterToEliminate.getValue().getFirst());
    for(Map.Entry<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>> mapEntry : seedToPadsMap.entrySet())
    {
      if(mapEntry.getKey() == clusterToEliminate.getKey())
        continue;

      List<LandPatternPad> usedPads = new LinkedList<LandPatternPad>();
      for(LandPatternPad pad : pads)
      {
        PanelRectangle newRect = new PanelRectangle(padToShapeAndCoordPairMap.get(pad).getFirst());
        if(seedToNewPadsMap.containsKey(mapEntry.getKey()))
          newRect.add(seedToNewPadsMap.get(mapEntry.getKey()).getSecond());
        else
          newRect.add(mapEntry.getValue().getSecond());

        if(newRect.getWidth() <= maxInspectionRegionSizeInNanoMeters &&
           newRect.getHeight() <= maxInspectionRegionSizeInNanoMeters)
        {
          if(seedToNewPadsMap.containsKey(mapEntry.getKey()))
          {
            seedToNewPadsMap.get(mapEntry.getKey()).getFirst().add(pad);
            seedToNewPadsMap.get(mapEntry.getKey()).setSecond(newRect);
          }
          else
          {
            Pair<Set<LandPatternPad>, PanelRectangle> pair = new Pair<Set<LandPatternPad>, PanelRectangle>();
            Set<LandPatternPad> newPads = new HashSet<LandPatternPad>();
            newPads.add(pad);
            pair.setFirst(newPads);
            pair.setSecond(newRect);
            seedToNewPadsMap.put(mapEntry.getKey(), pair);
          }

          usedPads.add(pad);
        }
      }

      pads.removeAll(usedPads);
      if(pads.isEmpty())
      {
        // ok, let' s validate the new regions
        boolean areRegionsValid = true;
        for(Map.Entry<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>> newEntry : seedToNewPadsMap.entrySet())
        {
          Pair<Set<LandPatternPad>, PanelRectangle> pair = seedToPadsMap.get(newEntry.getKey());
          Assert.expect(pair != null);
          newEntry.getValue().getFirst().addAll(pair.getFirst());
          if(isClusterValid(newEntry.getValue().getFirst(), newEntry.getValue().getSecond(), inspectionFamilyEnum, jointTypeEnum, maxInspectionRegionSizeInNanoMeters) == false)
          {
            areRegionsValid = false;
            break;
          }
          newEntry.getValue().getFirst().removeAll(pair.getFirst());
        }

        if(areRegionsValid)
        {
          for (Map.Entry<Point2D, Pair<Set<LandPatternPad>, PanelRectangle>> newEntry : seedToNewPadsMap.entrySet())
          {
            Pair<Set<LandPatternPad>, PanelRectangle> pair = seedToPadsMap.get(newEntry.getKey());
            Assert.expect(pair != null);
            pair.getFirst().addAll(newEntry.getValue().getFirst());
            pair.setSecond(newEntry.getValue().getSecond());
          }
          seedToPadsMap.remove(clusterToEliminate.getKey());

          return true;
        }
      }
    }

    return false;
  }

  /**
   * @author George A. David
   */
  static private void combineClusters(Map<PanelRectangle, Set<LandPatternPad>> regionToLandPatternPadsMap, 
                                      InspectionFamilyEnum inspectionFamilyEnum,
                                      JointTypeEnum jointTypeEnum,
									  int maxInspectionRegionSizeInNanoMeters)
  {
    Assert.expect(inspectionFamilyEnum != null);
    Assert.expect(jointTypeEnum != null);
      
    // ok, as a last final sanity check, let's combine any regions we can.
    Set<PanelRectangle> processedRegions = new HashSet<PanelRectangle>();
    while(processedRegions.size() < regionToLandPatternPadsMap.size())
    {
      for (Map.Entry<PanelRectangle, Set<LandPatternPad>> mapEntry : regionToLandPatternPadsMap.entrySet())
      {
        if (processedRegions.contains(mapEntry.getKey()))
          continue;
        processedRegions.add(mapEntry.getKey());

        PanelRectangle mapEntryRect = new PanelRectangle(mapEntry.getKey());
        List<PanelRectangle> regionsToRemove = new LinkedList<PanelRectangle>();

        for (Map.Entry<PanelRectangle, Set<LandPatternPad>> entry : regionToLandPatternPadsMap.entrySet())
        {
          if (processedRegions.contains(entry.getKey()))
            continue;

          PanelRectangle newRect = new PanelRectangle(entry.getKey());
          newRect.add(mapEntryRect);
          if (newRect.getWidth() <= maxInspectionRegionSizeInNanoMeters &&
              newRect.getHeight() <= maxInspectionRegionSizeInNanoMeters)
          {
            mapEntry.getValue().addAll(entry.getValue());
            if(doPadsMeetAreaRatioSpecs(mapEntry.getValue(), inspectionFamilyEnum, jointTypeEnum))
            {
              mapEntry.getKey().add(entry.getKey());
              regionsToRemove.add(entry.getKey());
              mapEntryRect = newRect;
            }
            else
              mapEntry.getValue().removeAll(entry.getValue());
          }
        }

        if(regionsToRemove.isEmpty() == false)
        {
          for(PanelRectangle region : regionsToRemove)
            regionToLandPatternPadsMap.remove(region);
          break;
        }
      }
    }
  }

  /**
   * @author Yong Sheng Chuan
   */
  static public ReconstructionRegion createInspectionRegion(Collection<Pad> pads)
  {
    return createInspectionRegion(pads, false);
  }
  
  /**
   * @author George A. David
   */
  static public ReconstructionRegion createInspectionRegion(Collection<Pad> pads, boolean isVirtualLiveRegion)
  {
    Assert.expect(pads != null);

    ReconstructionRegion region = new ReconstructionRegion();
    region.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.INSPECTION);
    region.setTopSide(pads.iterator().next().isTopSide());
    // Added by Seng Yew on 22-Apr-2011
    // IMPORTANT NOTE : Have to make sure only one subtype in one reconstruction region.
    // The way to get subtype must consistent with setAllInspectionRegionsWithImageNormalizationBySubtype() in TestProgram.java
//    if (pads.iterator().next().getSubtype().doesAlgorithmSettingExist(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM) &&
//        pads.iterator().next().getSubtype().doesAlgorithmSettingExist(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM))
//    {
//      float minVal = (Float)pads.iterator().next().getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM);
//      float maxVal = (Float)pads.iterator().next().getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM);
//      region.setMinimumGraylevel(minVal);
//      region.setMaximumGraylevel(maxVal);
//    }

    boolean printDebug = false;
    for (Pad pad : pads)
    {
      JointInspectionData jointInspectionData = new JointInspectionData(pad);
      region.addJointInspectionData(jointInspectionData);
      Assert.expect(pad.isTopSide() == region.isTopSide());
    }

    // now add the automatic alignment uncertainty border
    PanelRectangle regionRect = region.getRegionRectangleRelativeToPanelInNanoMeters();
    if(_TEST_IRP_BOUNDARIES)
    {
      regionRect.setRect(regionRect.getCenterX() - ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters()/2.0,
                         regionRect.getCenterY() - ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters()/2.0,
                         ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters(),
                         ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters());

    }
    else
    {
      // By Lee Herng (1 April 2011) - To allow user to have the control whether  they want to view larger view of the image.
      // This will also prevent the image set incompatibility issues when changing from large view image to normal view.
      //if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_LARGE_IMAGE_VIEW) == true 
      if(region.getBoard().getPanel().getProject().isEnableLargeImageView() && isVirtualLiveRegion == false )
      {
        int maximumReconstructionRegionLengthInNanometers = MathUtil.convertMilsToNanoMetersInteger(400);
        int maximumReconstructionRegionWidthInNanometers = MathUtil.convertMilsToNanoMetersInteger(400);

        if(pads.iterator().next().getSubtype().getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
        {
          maximumReconstructionRegionLengthInNanometers = MathUtil.convertMilsToNanoMetersInteger(200);
          maximumReconstructionRegionWidthInNanometers = MathUtil.convertMilsToNanoMetersInteger(200);
        }
        
        if (regionRect.getWidth() > maximumReconstructionRegionWidthInNanometers)
        {
          maximumReconstructionRegionWidthInNanometers = regionRect.getWidth();
        }
        if (regionRect.getHeight() > maximumReconstructionRegionLengthInNanometers)
        {
          maximumReconstructionRegionLengthInNanometers = regionRect.getHeight();
        }

        int offsetLengthInNanometers = maximumReconstructionRegionLengthInNanometers - regionRect.getHeight();
        int offsetWidthInNanometers = maximumReconstructionRegionWidthInNanometers - regionRect.getWidth();

        // extra checking for maximum region
        if (maximumReconstructionRegionLengthInNanometers + (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters())
          > ReconstructionRegion.getMaxInspectionRegionLengthInNanoMeters())
        {
          maximumReconstructionRegionLengthInNanometers -= 2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters();
        }
        if (maximumReconstructionRegionWidthInNanometers + (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters())
          > ReconstructionRegion.getMaxInspectionRegionWidthInNanoMeters())
        {
          maximumReconstructionRegionWidthInNanometers -= 2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters();
        }

        // do some checking before enlarge the view
        int minX = regionRect.getMinX() - (int) (offsetWidthInNanometers / 2);
        int minY = regionRect.getMinY() - (int) (offsetLengthInNanometers / 2);

        if (minX < 0)
        {
          minX = 0;
        }
        if (minY < 0)
        {
          minY = 0;
        }

        regionRect.setRect(
            minX - Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters(),
            minY - Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters(),
            maximumReconstructionRegionWidthInNanometers + (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters()),
            maximumReconstructionRegionLengthInNanometers + (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters()));
      }
      else
      {
          regionRect.setRect(regionRect.getMinX() - Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters(),
                             regionRect.getMinY() - Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters(),
                             regionRect.getWidth() + (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters()),
                             regionRect.getHeight() + (2 * Alignment.getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters()));
      }
    }
    region.setRegionRectangleRelativeToPanelInNanoMeters(regionRect);

    return region;
  }
}
