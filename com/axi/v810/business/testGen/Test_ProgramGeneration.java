package com.axi.v810.business.testGen;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import com.axi.v810.business.imageAcquisition.ImagingChainProgramGenerator;

/**
 * @author George A. David
 */
class Test_ProgramGeneration extends UnitTest
{
  private boolean _creatingInputFile = false;
  private int _lineNumber = 0;
  private BufferedReader _is;
  private PrintWriter _os;
  private Config _config = Config.getInstance();
  private long _numCharactersToSkipFirstTest = -1;
  private String _projectName;

  /**
   * @author George A. David
   */
  private static Comparator<TestSubProgram> _subProgramComparator = new Comparator<TestSubProgram>()
  {
    public int compare(TestSubProgram lhs, TestSubProgram rhs)
    {
      return lhs.getPanelLocationInSystem().getName().compareTo(rhs.getPanelLocationInSystem().getName());
    }
  };

  /**
   * @author George A. David
   */
  private static Comparator<ReconstructionRegion> _reconstructionRegionComparator = new Comparator<ReconstructionRegion>()
  {
    public int compare(ReconstructionRegion lhs, ReconstructionRegion rhs)
    {
      return lhs.getName().compareTo(rhs.getName());
    }
  };

  /**
   * @author George A. David
   */
  private static Comparator<FocusGroup> _focusGroupComparator = new Comparator<FocusGroup>()
  {
    public int compare(FocusGroup lhs, FocusGroup rhs)
    {
      Assert.expect(lhs != null);
      Assert.expect(rhs != null);

      // they're the same exact object!!!
      if (lhs == rhs)
        return 0;

      PanelRectangle lhsFocusGroupRect = lhs.getFocusSearchParameters().getFocusRegion().
                                         getRectangleRelativeToPanelInNanometers();
      PanelRectangle rhsFocusGroupRect = rhs.getFocusSearchParameters().getFocusRegion().
                                         getRectangleRelativeToPanelInNanometers();

      double firstDistanceSquared = MathUtil.calculateDistanceSquared(lhsFocusGroupRect.getMinX(),
          lhsFocusGroupRect.getMinY(), 0, 0);
      double secondDistanceSquared = MathUtil.calculateDistanceSquared(rhsFocusGroupRect.getMinX(),
          rhsFocusGroupRect.getMinY(), 0, 0);

      if (firstDistanceSquared > secondDistanceSquared)
        return 1;
      else if (firstDistanceSquared < secondDistanceSquared)
        return -1;
      else
        return 0;
    }
  };

  /**
   * @author George A. David
   * @author Patrick Lacz
   */
  private static Comparator<Slice> _sliceComparator = new Comparator<Slice>()
  {
    public int compare(Slice lhs, Slice rhs)
    {
      Assert.expect(lhs != null);
      Assert.expect(rhs != null);

      // they're the same exact object!!!
      if (lhs == rhs)
        return 0;

      int lhsSliceNameIndex = lhs.getSliceName().getId();
      int rhsSliceNameIndex = rhs.getSliceName().getId();
      return lhsSliceNameIndex - rhsSliceNameIndex;
    }
  };

  /**
   * @author Michael Tutkowski
   */
  public static void main(String[] args)
  {
//    debugTest();
//    for(int i = 0; i < 10; ++i)
//    {
//      System.out.println("start test " + i);
      UnitTest.execute(new Test_ProgramGeneration());
//      System.out.println("end test " + i);
//    }
  }

  /**
   * @author George A. David
   */
  private static void debugTest()
  {
    try
    {
//    String projectName = "0027parts";
//    String projectName = "hp5dx_1024";
//    String projectName = "nepcon_wide_ew";
//    String projectName = "242_hetero";
//    String projectName = "families_all_rlv";
//    String projectName = "moto_speed";
//    String projectName = "41k_joints";
//    String projectName = "73-9375-03_modified";
//    String projectName = "84k_joints_explode";
//    String projectName = "moto_101k_joints_explode";
//    String projectName = "sledge";
//    String projectName = "cygnusAllFamilies";
//    String projectName = "malabar_120k_gad";
//    String projectName = "73-9375-03_modified";
      String projectName = "73-3764_ASAP_83";

//
      System.out.println("loading project");
      Project project = loadProject(projectName, false);
      System.out.println("project loaded");

//    try
//    {
//      System.in.read();
//    }
//    catch (IOException ex2)
//    {
//      Assert.expect(false);
//    }
      System.out.println("generating program");
      ProgramGeneration.getInstance().generateTestProgram(project);
      System.out.println("time to generate program: " + ProgramGeneration.getTimeToGenerateProgramInMillis());
      System.out.println("program generated");

//    System.out.println(project.getName() + "," + project.getPanel().getNumComponents() + "," + project.getPanel().getNumJoints());
//    ProgramGeneration programGen = ProgramGeneration.getInstance();
//    for(int i = 0; i < 10; i++)
//    {
//      programGen.generateTestProgram(project);
//      System.out.println("," + ProgramGeneration.getTimeToGenerateProgramInMillis());
//    }
//    System.out.println("Number of inspection regions: " + testProgram.getInspectionRegions().size());
//    System.out.println("Number of inspeciton images: " + testProgram.getNumberOfInspectionImages());
//    System.out.println("Number of Slices, Width Nanometers, Length Nanometers");
//    for(ReconstructionRegion inspectionRegion : testProgram.getInspectionRegions())
//    {
//      PanelRectangle rect = inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
//      System.out.println(inspectionRegion.getSlices().size() + "," + rect.getWidth() + "," +  rect.getHeight());
//    }
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author George A. David
   */
  private static Project loadProject(String projectName, boolean alwaysImport)
  {
//    System.out.println(FileName.getLegacyPanelNdfFullPath(projectName));
    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    Project project = null;
    try
    {
      if(alwaysImport || Project.doesProjectExistLocally(projectName) == false)
      {
        project = Project.importProjectFromNdfs(projectName, warnings);
      }
      else
      {
        BooleanRef abortedDuringLoad = new BooleanRef();
        project = Project.load(projectName, abortedDuringLoad);
        Assert.expect(abortedDuringLoad.getValue() == false);
      }
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
      Assert.expect(false);
    }

    return project;
  }

  /**
   * Test that hardware.calib values has not changed.
   * Some changes to this file may cause this test to fail
   * @author George A. David
   */
  public void testHardwareCalib()
  {
    try
    {
      Config.getInstance().loadIfNecessary();
    }
    catch (DatastoreException ex)
    {
      ex.printStackTrace();
      Assert.expect(false);
    }
    for(ConfigEnum configEnum : HardwareConfigEnum.getAllConfigEnums())
    {
      System.out.println(Config.getInstance().getValue(configEnum));
    }
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      _is = is;
      _os = os;

      String inputPath = getTestInputFileDir() + File.separator + "Test_ProgramGeneration_1.input";
      File file = new File(inputPath);
      long numBytes = file.length();
      int numCharsInFile = (int)(numBytes / JavaTypeSizeEnum.CHAR.getSizeInBytes());

      _config.loadIfNecessary();
      // this will cause this test to fail.
      double value = _config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL);
      Assert.expect(value == 5.26315789473684,
                    "The value of " + SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL.getKey() + " in the file " + FileName.getHardwareCalibFullPath() + " has changed from the expected value of 6.25 to " +
                    value + ". This will cause this test to fail and so there's no point in running this test.");


      if (System.getProperty("RUNNING_IN_IDE") != null)
      {
        // we are running in the IDE and this test case will not run without redirecting the input stream
        _is = new LineNumberReader(new FileReader(inputPath));
      }

      if (_creatingInputFile)
        _os = new PrintWriter(new File(inputPath));

      ImagingChainProgramGenerator.disableStepSizeOptimization(true);
//      com.axi.v810.business.imageAnalysis.InspectionFamily.printInspectionFamilyData();
      testAllJointTypes();
      testAllJointTypes2();
      testGeneratingProgram();
      testReloadingAndGeneratingProgram(numCharsInFile);
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      _os.flush();
    }
  }

  /**
   * @author George A. David
   */
  private void testAllJointTypes()
  {
    // test that all joint types are handled
    String projectName = "0027parts";
    Project project = loadProject(projectName, true);
    CompPackage compPackage = project.getPanel().getComponent("1", "c1").getCompPackage();
    for (JointTypeEnum jointType : JointTypeEnum.getAllJointTypeEnums())
    {
      compPackage.setJointTypeEnum(jointType);
      project.getTestProgram();
    }
  }

  /**
   * @author George A. David
   */
  private void testAllJointTypes2()
  {
    String projectName = "ALL_JOINT_TYPES";
    Project project = loadProject(projectName, false);
    Set<JointTypeEnum> allJointTypeEnums = new HashSet<JointTypeEnum>(JointTypeEnum.getAllJointTypeEnums());
    Set<JointTypeEnum> jointTypeEnums = new HashSet<JointTypeEnum>(project.getPanel().getJointTypeEnums());
//    Map<InspectionFamilyEnum, List<JointTypeEnum>> map = new HashMap<InspectionFamilyEnum, List<JointTypeEnum>>();
//    for(JointTypeEnum jointType : allJointTypeEnums)
//    {
//      InspectionFamilyEnum familyEnum = InspectionFamily.getInspectionFamilyEnum(jointType);
//      if(map.containsKey(familyEnum) == false)
//        map.put(familyEnum, new LinkedList<JointTypeEnum>());
//      map.get(familyEnum).add(jointType);
//    }
//
//    for(Map.Entry<InspectionFamilyEnum, List<JointTypeEnum>> entry : map.entrySet())
//    {
//      System.out.println("****** " + entry.getKey().getName() + " ******");
//      for(JointTypeEnum jointType : entry.getValue())
//      {
//        System.out.println(jointType.getName());
//      }
//    }
//    Assert.expect(false);
    allJointTypeEnums.removeAll(jointTypeEnums);
    Expect.expect(allJointTypeEnums.isEmpty(), "New joint types have been added and are not being tested by the Program Generation regression test.\n" +
                                               "Please add them the project " + projectName + " in the unit test directory so that we may properly test it.\n" +
                                               "Here are the list of new joint types found: " + allJointTypeEnums);

    // now set all pads to artifact compensated if possible
    for(PadType padType : project.getPanel().getPadTypesUnsorted())
    {
      PadTypeSettings settings = padType.getPadTypeSettings();
      if(settings.getEffectiveArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false)
        settings.setArtifactCompensationState(ArtifactCompensationStateEnum.COMPENSATED);
    }

    checkResults(projectName, project.getTestProgram());
    // now rotate the panel by 90 degrees, and try again.
    PanelSettings panelSettings = project.getPanel().getPanelSettings();
    panelSettings.setDegreesRotationRelativeToCad(panelSettings.getDegreesRotationRelativeToCad() + 90);
    checkResults(projectName, project.getTestProgram());
  }

  /**
   * @author George A. David
   */
  private void testGeneratingProgram() throws Exception
  {
    String projectName = "41k_joints";
//    projectName = "moto_speed";
//    projectName = "nepcon_wide_ew";
//    projectName = "families_all_rlv";
//    projectName = "jtr_sledge_div";

    Project project = loadProject(projectName, true);
    Panel panel = project.getPanel();
    // add alignment points
    List<AlignmentGroup> groups = panel.getPanelSettings().getLeftAlignmentGroupsForLongPanel();
    AlignmentGroup group = groups.get(0);
    for (Pad pad : panel.getComponent("1", "r18251").getPads())
      group.addPad(pad);

    group = groups.get(1);
    for (Pad pad : panel.getComponent("1", "r19330").getPads())
      group.addPad(pad);
    for (Pad pad : panel.getComponent("1", "c25964").getPads())
      group.addPad(pad);
    for (Pad pad : panel.getComponent("1", "r19348").getPads())
      group.addPad(pad);

    group = groups.get(2);
    for (Pad pad : panel.getComponent("1", "c22668").getPads())
      group.addPad(pad);
    for (Pad pad : panel.getComponent("1", "c22667").getPads())
      group.addPad(pad);
    for (Pad pad : panel.getComponent("1", "c22591").getPads())
      group.addPad(pad);

    groups = panel.getPanelSettings().getRightAlignmentGroupsForLongPanel();

    group = groups.get(0);
    for (Pad pad : panel.getComponent("1", "r19410").getPads())
      group.addPad(pad);
    for (Pad pad : panel.getComponent("1", "c26299").getPads())
      group.addPad(pad);

    group = groups.get(1);
    group.addPad(panel.getPad("1", "u19703", "22"));
    group.addPad(panel.getPad("1", "j15301", "2"));

    group = groups.get(2);
    for (Pad pad : panel.getComponent("1", "c25970").getPads())
      group.addPad(pad);
    for (Pad pad : panel.getComponent("1", "r18611").getPads())
      group.addPad(pad);

    // now set all pads to artifact compensated if possible
    for(PadType padType : project.getPanel().getPadTypesUnsorted())
    {
      PadTypeSettings settings = padType.getPadTypeSettings();
      if(settings.getEffectiveArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false)
        settings.setArtifactCompensationState(ArtifactCompensationStateEnum.COMPENSATED);
    }

    TestProgram testProgram = project.getTestProgram();
//    System.out.println("time to generate program: " + timer.getElapsedTimeInMillis());
    String logFile = getTestDataDir() + File.separator + "projectGenerationTiming.log";

    boolean append = true;
    if (FileUtil.exists(logFile))
      append = true;
    FileWriterUtil writer = new FileWriterUtil(logFile, append);
    writer.open();
    writer.writeln(Directory.getDirName(System.currentTimeMillis()) + " " +
                   ProgramGeneration.getTimeToGenerateProgramInMillis());
    writer.close();

//    Expect.expect(ProgramGeneration.getTimeToGenerateProgramInMillis() < 30000,
//                  "time to generate program: " + ProgramGeneration.getTimeToGenerateProgramInMillis());

    project.save();
    BooleanRef abortedDuringLoad = new BooleanRef();
    project = Project.load(project.getName(), abortedDuringLoad);
    Assert.expect(abortedDuringLoad.getValue() == false);
    testProgram = project.getTestProgram();

    checkResults(projectName, testProgram);
  }

  /**
   * @author George A. David
   */
  private void testReloadingAndGeneratingProgram(int numCharsInFile) throws Exception
  {
    String projectName = "Cygnus_asap_83";
    _is.mark(numCharsInFile);

    Project project = null;
    for (int i = 0; i < 5; i++)
    {
      if (i == 0)
        project = loadProject(projectName, true);
      else
      {
        project = loadProject(projectName, false);

        if (i % 3 == 0)
        {
          project.save();
          project = loadProject(projectName, false);
        }
      }

      // now set all pads to artifact compensated if possible
      for(PadType padType : project.getPanel().getPadTypesUnsorted())
      {
        PadTypeSettings settings = padType.getPadTypeSettings();
        if(settings.getEffectiveArtifactCompensationState().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false)
          settings.setArtifactCompensationState(ArtifactCompensationStateEnum.COMPENSATED);
      }

      checkResults(project.getName(), project.getTestProgram());
      if (_creatingInputFile)
        break;
      _is.reset();
    }

  }

  /**
   * @author George A. David
   */
  private void checkResults(String projectName, TestProgram testProgram)
  {
    Assert.expect(projectName != null);
    Assert.expect(testProgram != null);

    _projectName = projectName;
    _numCharactersToSkipFirstTest = 0;

    checkResults("TestProgram data for " + projectName);
    checkResults("Number of SubPrograms: " + testProgram.getAllTestSubPrograms().size());
    checkResults("Number of Verification Regions: " + testProgram.getVerificationRegions().size());
    checkResults("Number of Inspection Regions: " + testProgram.getAllInspectionRegions().size());
    checkResults("Number or alignment regions: " + testProgram.getAlignmentRegions().size());

    checkResults("Checking each TestSubProgram");

    List<TestSubProgram> subPrograms = testProgram.getAllTestSubPrograms();
    Collections.sort(subPrograms, _subProgramComparator);
    for(TestSubProgram subProgram : subPrograms)
    {
      List<ReconstructionRegion> verificationRegions = new LinkedList<ReconstructionRegion>(subProgram.getVerificationRegions());
      Collections.sort(verificationRegions, _reconstructionRegionComparator);
      checkResults("Number of Verification Regions: " + verificationRegions.size());
      printReconstructionRegionDetails(verificationRegions);

      List<ReconstructionRegion> inspectionRegions = new LinkedList<ReconstructionRegion>(subProgram.getAllInspectionRegions());
      Collections.sort(inspectionRegions, _reconstructionRegionComparator);
      checkResults("Number of Inspection Regions: " + inspectionRegions.size());
      printReconstructionRegionDetails(inspectionRegions);

      List<ReconstructionRegion> alignmentRegions = new LinkedList<ReconstructionRegion>(subProgram.getAlignmentRegions());
      Collections.sort(alignmentRegions, _reconstructionRegionComparator);
      checkResults("Number or alignment regions: " + alignmentRegions.size());
      printReconstructionRegionDetails(alignmentRegions);
    }
  }

  /**
   * @author George A. David
   */
  private void printReconstructionRegionDetails(List<ReconstructionRegion> reconstructionRegions)
  {
    Assert.expect(reconstructionRegions != null);

    for(ReconstructionRegion region : reconstructionRegions)
    {
      checkResults("Region Name: " + region.getName());
      checkResults("Region ID: " + region.getRegionId());
      PanelRectangle rect = region.getRegionRectangleRelativeToPanelInNanoMeters();
      checkResults("Region Rectangle: " + rect.getMinX() + " " + rect.getMinY() + " " + rect.getWidth() + " " + rect.getHeight());
      String isTop = "Is top side: ";
      if(region.isTopSide())
        isTop += "yes";
      else
        isTop += "no";
      checkResults(isTop);
      checkResults("Reconstruction Engine ID: " + region.getReconstructionEngineId().toString());

      if(region.isAlignmentRegion())
      {
        if(region.hasJoints())
        {
          List<Pad> pads = new LinkedList<Pad>(region.getPads());
          Collections.sort(pads, new PadComparator());
          Set<String> compNames = new TreeSet<String>();
          String padsString = "Pads: ";
          for(Pad pad : pads)
          {
            compNames.add(pad.getComponent().getReferenceDesignator());
            padsString += pad.getName() + " ";
          }

          String compString = "Components: ";
          for(String compName : compNames)
          {
            compString += compName + " ";
          }

          checkResults("Number of Components: " + compNames.size());
          checkResults(compString);
          checkResults("Number of Pads: " + pads.size());
          checkResults(padsString);
        }

      }
      else if(region.hasComponent())
      {
        checkResults("Component: " + region.getComponent().getReferenceDesignator());
        checkResults("Number of Pads: " + region.getPads().size());
        String padsString = "Pads: ";
        List<Pad> pads = new LinkedList<Pad>(region.getPads());
        Collections.sort(pads, new PadComparator());
        for (Pad pad : pads)
        {
          padsString += pad.getName() + " ";
        }
        checkResults(padsString);
      }

      checkResults("Number of Fiducials: " + region.getFiducialInspectionDataList().size());
      String fiducialsString = "Fiducials: ";
      for(FiducialInspectionData jointData : region.getFiducialInspectionDataList())
      {
        fiducialsString += jointData.getFiducial().getName() + " ";
      }
      checkResults(fiducialsString);

      checkResults("Number of Slices: " + region.getNumberOfSlices());

      List<FocusGroup> focusGroups = region.getFocusGroups();
      checkResults("Number of Focus Groups: " + focusGroups.size());
      Collections.sort(focusGroups, _focusGroupComparator);
      for(FocusGroup focusGroup : focusGroups)
      {
//        checkResults("Focus Group ID: " + focusGroup.getId());
        List<Slice> slices = focusGroup.getSlices();
        checkResults("Number of Slices: " + focusGroup.getNumberOfSlices());
        Collections.sort(slices, _sliceComparator);
        for(Slice slice : focusGroup.getSlices())
        {
          checkResults("Slice Name: " + slice.getSliceName().getName());
          //          println("Reconstruction z height: " + slice.getReconstructionZheightInNanoMeters());
          FocusInstruction instruction = slice.getFocusInstruction();
          checkResults("Focus Method: " + instruction.getFocusMethod().getDescription());
          checkResults("Focus Percent: " + instruction.getPercentValue());
          checkResults("Focus z height: " + instruction.getZHeightInNanoMeters());
          checkResults("Focus z offset: " + instruction.getZOffsetInNanoMeters());
        }
        FocusSearchParameters searchParams = focusGroup.getFocusSearchParameters();
        checkResults("Horizontal sharpness component: " + searchParams.getHorizontalSharpnessComponentPercentage() + "%");
        checkResults("Positional weighting: " + searchParams.getGradientWeightingMagnitudePercentage() + "%");
        checkResults("Positional weighting orientation: " + searchParams.getGradientWeightingOrientationInDegrees() + " degrees");
        FocusRegion focusRegion = searchParams.getFocusRegion();
        PanelRectangle focusRect = focusRegion.getRectangleRelativeToPanelInNanometers();
        checkResults("Focus Region: " + focusRect.getMinX() + " " + focusRect.getMinY() + " " + focusRect.getWidth() + " " + focusRect.getHeight());
        checkResults("Focus Profile Shape: " + searchParams.getFocusProfileShape().getDescription());
        checkResults("Z curve width: " + searchParams.getZCurveWidthInNanoMeters());
        if(region.isAlignmentRegion() == false)
        {
          List<ReconstructionRegion> neighbors = focusGroup.getNeighbors();
          checkResults("Number of Neighbors: " + neighbors.size());
          Collections.sort(neighbors, _reconstructionRegionComparator);
          for (ReconstructionRegion neighbor : neighbors)
            checkResults("Neighbor Name: " + neighbor.getName());
        }
        //        println("Relative Z Offset: " + focusGroup.getRelativeZoffsetFromSurfaceInNanoMeters());
      }

      checkResults("Is affected by interference pattern: " + region.isAffectedByInterferencePattern());
      if(region.isAffectedByInterferencePattern())
      {
        String rangesString = "Candidate Ranges: ";
        for(Pair<Integer, Integer> pair: region.getCandidateStepRanges().getRanges())
        {
          rangesString += pair.getFirst() + "-" + pair.getSecond() + " ";
        }
        checkResults(rangesString);
      }
    }
  }

  /**
   * @author George A. David
   */
  private void checkResults(String message)
  {
    try
    {
      if (_creatingInputFile)
        _os.println(message);
      else
      {
        ++_lineNumber;
        String expectedMessage = _is.readLine();
        _numCharactersToSkipFirstTest += expectedMessage.length();
        boolean resultsMatch = message.equals(expectedMessage);
        if(resultsMatch == false)
        {
          _os.println();
          _os.println("WARNING: A change has been made that invalidates the test program.\n" +
                      "This change may also invalidate image sets, some tuning, and some learning.\n" +
                      "Please make sure you really need this change prior to modifying this regression test to pass.\n" +
                      "Forcing the customer to re-tune their projects is a decision that should not be taken lightly.");
          _os.println("Project: " + _projectName);
          _os.println("Line: " + _lineNumber);
          _os.println("Expected: " + expectedMessage);
          _os.println("Actual: " + message);
          Assert.expect(false);
        }
        Expect.expect(resultsMatch);
      }
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }
}
