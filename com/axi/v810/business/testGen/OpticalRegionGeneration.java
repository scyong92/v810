package com.axi.v810.business.testGen;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.hardware.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalRegionGeneration 
{
  public final static int OPTICAL_CAMERA_VIEW_FACTOR_1 = 16;
  public final static int OPTICAL_CAMERA_VIEW_FACTOR_2 = 14;
  
  /**
   * @author Cheah Lee Herng
   * @author Ying-Huan.Chu
   */
  public static List<OpticalCameraRectangle> generateCameraFovPanelRectangles(java.awt.Rectangle panelRectangleInNanometer, double opticalRectangleSpacingInNanometer)
  {
    Assert.expect(panelRectangleInNanometer != null);
    
    int opticalRegionMinX = (int) panelRectangleInNanometer.getMinX();
    int opticalRegionMinY = (int) panelRectangleInNanometer.getMinY();
    
    int opticalRegionMaxX = (int) panelRectangleInNanometer.getMaxX();
    int opticalRegionMaxY = (int) panelRectangleInNanometer.getMaxY();
    
    int opticalCameraRectangleWidthInNanometer = PspHardwareEngine.getInstance().getActualImageWidthInNanometer();
    int opticalCameraRectangleHeightInNanometer = PspHardwareEngine.getInstance().getActualImageHeightInNanometer();
    
    // We will generate OpticalCameraRectangles from the top left corner of the OpticalRegion.
    // Thus we will set our currentX to the minimum X of OpticalRegion and our currentY to the maximum Y of OpticalRegion.
    int currentX = opticalRegionMinX;
    int currentY = opticalRegionMaxY;
    
    List<OpticalCameraRectangle> opticalCameraRectangleList = new ArrayList<>();
    boolean continueProcessRow = true;
    boolean continueProcessColumn = true;
    
    currentY -= opticalCameraRectangleHeightInNanometer; // we have to minus the height of OpticalCameraRectangle because OpticalCameraRectangle is recorded with bottomLeftXY.
    
    while (continueProcessRow)
    {
      if (currentY >= opticalRegionMinY)
      {
        continueProcessColumn = true;
        while (continueProcessColumn)
        {
          if ((currentX + opticalCameraRectangleWidthInNanometer) <= opticalRegionMaxX)
          {
            OpticalCameraRectangle opticalCameraRectangle = new OpticalCameraRectangle(currentX, 
                                                                                      currentY, 
                                                                                      opticalCameraRectangleWidthInNanometer, 
                                                                                      opticalCameraRectangleHeightInNanometer);
            opticalCameraRectangleList.add(opticalCameraRectangle);
            currentX += opticalCameraRectangleWidthInNanometer + opticalRectangleSpacingInNanometer;
          }
          else
          {
            // Uncomment this part of the code to allow the system to auto-adjust OpticalCameraRectangles to fit in OpticalRegion.
            currentX = opticalRegionMaxX - opticalCameraRectangleWidthInNanometer;
            OpticalCameraRectangle opticalCameraRectangle = new OpticalCameraRectangle(currentX, 
                                                                                      currentY, 
                                                                                      opticalCameraRectangleWidthInNanometer, 
                                                                                      opticalCameraRectangleHeightInNanometer);
            opticalCameraRectangleList.add(opticalCameraRectangle);
            currentX = opticalRegionMinX;
            currentY -= opticalCameraRectangleHeightInNanometer + opticalRectangleSpacingInNanometer;
            continueProcessColumn = false;
          }
        }
      }
      else
      {
        currentY = opticalRegionMinY;
        continueProcessColumn = true;
        while (continueProcessColumn)
        {
          if ((currentX + opticalCameraRectangleWidthInNanometer) <= opticalRegionMaxX)
          {
            OpticalCameraRectangle opticalCameraRectangle = new OpticalCameraRectangle(currentX, 
                                                                                      currentY, 
                                                                                      opticalCameraRectangleWidthInNanometer, 
                                                                                      opticalCameraRectangleHeightInNanometer);
            opticalCameraRectangleList.add(opticalCameraRectangle);
            currentX += opticalCameraRectangleWidthInNanometer + opticalRectangleSpacingInNanometer;
          }
          else
          {
            // Uncomment this part of the code to allow the system to auto-adjust OpticalCameraRectangles to fit in OpticalRegion.
            currentX = opticalRegionMaxX - opticalCameraRectangleWidthInNanometer;
            OpticalCameraRectangle opticalCameraRectangle = new OpticalCameraRectangle(currentX, 
                                                                                      currentY, 
                                                                                      opticalCameraRectangleWidthInNanometer, 
                                                                                      opticalCameraRectangleHeightInNanometer);
            opticalCameraRectangleList.add(opticalCameraRectangle);
            currentX = opticalRegionMinX;
            currentY += opticalCameraRectangleHeightInNanometer + opticalRectangleSpacingInNanometer;
            continueProcessColumn = false;
          }
        }
        continueProcessRow = false;
      }
    }
    Collections.sort(opticalCameraRectangleList, new com.axi.v810.gui.testDev.OpticalCameraRectangleComparator(false, com.axi.v810.gui.testDev.OpticalCameraRectangleComparatorEnum.COORDINATE));
    return opticalCameraRectangleList;
  }
  
  /**
   * @author Jack Hwee
   */
  public static int getCameraViewFactor()
  {
    if (PspEngine.isVersion1())
    {
      return OPTICAL_CAMERA_VIEW_FACTOR_1;
    }
    else
    {
      return OPTICAL_CAMERA_VIEW_FACTOR_2;
    }
  }
}
