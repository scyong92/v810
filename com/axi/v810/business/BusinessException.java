package com.axi.v810.business;

import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * This is the base class that all business layer exceptions should extend
 */
public abstract class BusinessException extends XrayTesterException
{
  /**
   * Create this exception with a localized string.
   * @author Bill Darbie
   */
  public BusinessException(LocalizedString localizedString)
  {
    super(localizedString);
  }

}
