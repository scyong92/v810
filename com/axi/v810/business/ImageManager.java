package com.axi.v810.business;

import java.awt.image.*;
import java.io.*;
import java.util.*;
import java.util.zip.*;
import javax.imageio.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * X-ray Image Load and Save
 * @author Kay Lannen
 */
public class ImageManager
{

  private static ImageManager _instance;
  private static boolean _ignoreCompatibilityCheck = false;

  private ImageManagerObservable _imageManagerObservable = ImageManagerObservable.getInstance();

  // bee-hoon.goh
  private ProjectErrorLogUtil _projectErrorLogUtil = ProjectErrorLogUtil.getInstance();
  
  // bee-hoon.goh - Virtual live verification images
  private final String _verificationImageBothSide = "verificationImage_both.jpg";
  private final String _verificationImage = "verificationImage_";
  private XrayTester _xRayTester = XrayTester.getInstance();
  private String _prevhighMagnification = null;
  private String _prevlowMagnification = null;
  static final float _IDENTICAL_RESIZE_FACTOR = 1f;
  
  /**
   * @author George A. David
   */
  private ImageManager()
  {
    // do nothing
  }

  /**
   * @author George A. David
   */
  public static synchronized ImageManager getInstance()
  {
    if (_instance == null)
    {
      _instance = new ImageManager();
    }

    return _instance;
  }

  /**
   * @author Matt Wharton
   */
  public int getNumberOfInspectionImagesToSave(ImageSetData imageSetData, TestProgram testProgram)
  {
    Assert.expect(imageSetData != null);
    Assert.expect(testProgram != null);

    // Start with the base number of inspection images.
    int numberOfInspectionImagesToSave = testProgram.getNumberOfInspectionImages();

    // If we're collecting lightest images, double our count.
    if (imageSetData.collectLightestImages())
    {
      numberOfInspectionImagesToSave *= 2;
    }

    // If we're collecting focus confirmation images, we need to update our count accordingly.
    if (imageSetData.collectFocusConfirmationImages())
    {
      Collection<Integer> candidateFocusConfirmationOffsets = FocusConfirmationEngine.getCandidateFocusConfirmationOffsets();
      int numberOfFocusConfirmationOffsets = candidateFocusConfirmationOffsets.size();

      int totalNumberOfFocusConfirmationImages = 0;
      for (ReconstructionRegion reconstructionRegion : testProgram.getFilteredInspectionRegions())
      {
        JointTypeEnum jointTypeEnum = reconstructionRegion.getJointTypeEnum();
        if (jointTypeEnum.isAvailableForRetestFailingPins())
        {
          int numberOfReconstructedSlices = reconstructionRegion.getNumberOfSlicesToBeReconstructed();
          totalNumberOfFocusConfirmationImages += (numberOfReconstructedSlices * numberOfFocusConfirmationOffsets);
        }
      }

      numberOfInspectionImagesToSave += totalNumberOfFocusConfirmationImages;
    }

    return numberOfInspectionImagesToSave;
  }

  /**
   * Saves an inspection image to disk.
   * @author Kay Lannen
   */
  public void saveInspectionImageToBeDeprecated(ImageSetData imageSetData,
    java.awt.image.BufferedImage bufferedImage,
    String reconstructionRegionName,
    int sliceId,
    PanelRectangle panelRectangle) throws DatastoreException
  {
    String imagesDir = imageSetData.getDir();
    
    String fileName = FileName.getInspectionImageName(reconstructionRegionName, sliceId);
    XrayImageIoUtil.savePngImage(bufferedImage, imagesDir + File.separator + fileName);
    _imageManagerObservable.stateChanged(ImageManagerEventEnum.INSPECTION_IMAGE_SAVED);
  }

  /**
   * Saves an inspection image to disk.
   * @author George A. David
   */
  public void saveDebugInspectionImage(ImageSetData imageSetData,
    java.awt.image.BufferedImage bufferedImage,
    String reconstructionRegionName,
    int sliceId,
    int offsetInMils,
    PanelRectangle panelRectangle) throws DatastoreException
  {
    Assert.expect(panelRectangle != null);

    String imagesDir = Directory.getXrayInspectionImagesDir(imageSetData.getProjectName(), imageSetData.getImageSetName());

    String fileName = reconstructionRegionName + "_" + sliceId + "_" + offsetInMils + ".png";

    XrayImageIoUtil.savePngImage(bufferedImage, imagesDir + File.separator + fileName);
    _imageManagerObservable.stateChanged(ImageManagerEventEnum.INSPECTION_IMAGE_SAVED);
  }

  /**
   * Saves all inspection images for a reconstruction region to disk.
   * @author Kay Lannen
   */
  public void saveInspectionImages(ImageSetData imageSetData, ReconstructedImages reconImages, String projectName) throws DatastoreException
  {
    Assert.expect(reconImages != null);
    ReconstructionRegion reconstructionRegion = reconImages.getReconstructionRegion();
    String regionName = reconstructionRegion.getName();
    PanelRectangle panelRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();

    // XCR-2953 Unable to Display Good Image in VVTS - Chin Seong
    String variationName = reconstructionRegion.getComponent().getBoard().getPanel().getProject().getSelectedVariationName();
    String goodImageProjectName = "";
    //XCR-3450, Always show variation name when generating good images
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.TEST_EXEC_ALWAYS_DISPLAY_VARIATION_NAME))
    {
      if (variationName.equals(VariationSettingManager.ALL_LOADED) == false)
        goodImageProjectName = projectName + "@" + variationName;
      else
        goodImageProjectName = projectName;
    }
    else
      goodImageProjectName = projectName;
    
    for (ReconstructedSlice reconstructedSlice : reconImages.getReconstructedSlices())
    {
      SliceNameEnum sliceName = reconstructedSlice.getSliceNameEnum();

      //Siew Yeng - XCR-2387 - do not save diagnostic slice image
      //Siew Yeng - XCR-2683 - do not save enhanced slice image
      if (sliceName.isAdditionalSlice() == false && 
          EnumToUniqueIDLookup.getInstance().isDiagnosticSlice(sliceName) == false && 
          EnumToUniqueIDLookup.getInstance().isEnhancedImageSlice(sliceName) == false)
      {
        saveInspectionImage(reconstructedSlice, reconstructionRegion, imageSetData.getDir());
        if (imageSetData.isSaveGoodImages())
        {
          Assert.expect(projectName != null);
          saveGoodImage(reconstructedSlice, reconstructionRegion, goodImageProjectName);
        }
      }
//      else
//      {
//        Slice debugSlice = reconstructionRegion.getSlice(sliceName);
//        Slice origSlice = debugSlice.getOriginalSliceForDebugSlice();
//        java.awt.image.BufferedImage buffImg = reconstructedSlice.getImage().getBufferedImage();
//        int offsetInMils = (int)MathUtil.convertNanoMetersToMils(debugSlice.getFocusInstruction().getZOffsetInNanoMeters() - origSlice.getFocusInstruction().getZOffsetInNanoMeters());
//        saveDebugInspectionImage(imageSetData, buffImg, regionName, origSlice.getSliceName().getId(), offsetInMils, panelRect);
//      }
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void saveProductionTuneDefectImages(ReconstructedImages reconImages,
                                             String userDefinedPath,
                                             String imageSetName) throws DatastoreException
  {
    Assert.expect(reconImages != null);
    Assert.expect(userDefinedPath != null);
    Assert.expect(imageSetName != null);
    
    ReconstructionRegion reconstructionRegion = reconImages.getReconstructionRegion();
    for (ReconstructedSlice reconstructedSlice : reconImages.getReconstructedSlices())
    {
      SliceNameEnum sliceName = reconstructedSlice.getSliceNameEnum();
      
      if (sliceName.isAdditionalSlice() == false)
      {
        String imageFullPath = FileName.getInspectionImageName(userDefinedPath, 
                                                               reconstructionRegion.getTestSubProgram().getTestProgram().getProject().getName(), 
                                                               imageSetName, 
                                                               reconstructionRegion.getName(), 
                                                               sliceName.getId());
        ImageEnhancement.autoEnhanceContrastInPlace(reconstructedSlice.getImage());
        XrayImageIoUtil.savePngImage(reconstructedSlice.getImage().getBufferedImage(), imageFullPath);
      }
    }
  }

   /**
   * @author khang-shian.sham
   */
  public void saveGoodImage(ReconstructedSlice reconstructedSlice,
    ReconstructionRegion reconstructionRegion, String projectName) throws DatastoreException
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(projectName != null);

    int sliceNameId = reconstructedSlice.getSliceNameEnum().getId();
    PanelRectangle panelRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    String goodImagePathAndName = FileName.getGoodImagePathAndName(reconstructionRegion.isTopSide(),
      panelRect.getMinX(), panelRect.getMinY(), sliceNameId, projectName);
    ImageEnhancement.autoEnhanceContrastInPlace(reconstructedSlice.getImage());
    XrayImageIoUtil.saveJpegImage(reconstructedSlice.getImage().getBufferedImage(), goodImagePathAndName); //Chin-seong, Kee - Auto enhance the image to better
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   */
  public void saveLightestInspectionImages(ImageSetData imageSetData, ReconstructedImages reconImages) throws DatastoreException
  {
    Assert.expect(imageSetData != null);
    Assert.expect(reconImages != null);

    ReconstructionRegion reconstructionRegion = reconImages.getReconstructionRegion();
    String imageSetDir = imageSetData.getDir();

    for (ReconstructedSlice reconstructedSlice : reconImages.getReconstructedSlices())
    {
      saveLightestInspectionImage(reconstructedSlice, reconstructionRegion, imageSetDir);
    }
  }

  /**
   * @author Matt Wharton
   */
  public void saveLightestInspectionImage(ReconstructedSlice reconstructedSlice,
    ReconstructionRegion reconstructionRegion,
    String imagesDir) throws DatastoreException
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(imagesDir != null);

    String reconstructionRegionName = reconstructionRegion.getName();
    int sliceNameId = reconstructedSlice.getSliceNameEnum().getId();
    String fileName = FileName.getLightestInspectionImageName(reconstructionRegionName, sliceNameId);

    XrayImageIoUtil.savePngImage(reconstructedSlice.getImage(), imagesDir + File.separator + fileName);
    _imageManagerObservable.stateChanged(ImageManagerEventEnum.INSPECTION_IMAGE_SAVED);
  }

  /**
   * @author Matt Wharton
   */
  public void saveFocusConfirmationInspectionImages(ImageSetData imageSetData,
    ReconstructedImages reconstructedImages,
    int sliceOffset) throws DatastoreException
  {
    Assert.expect(imageSetData != null);
    Assert.expect(reconstructedImages != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    String imagesDir = imageSetData.getDir();

    for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
    {
      saveFocusConfirmationInspectionImage(reconstructedSlice, reconstructionRegion, sliceOffset, imagesDir);
    }
  }

  /**
   * @author Matt Wharton
   */
  public void saveFocusConfirmationInspectionImage(ReconstructedSlice reconstructedSlice,
    ReconstructionRegion reconstructionRegion,
    int sliceOffset,
    String imagesDir) throws DatastoreException
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(imagesDir != null);

    String reconstructionRegionName = reconstructionRegion.getName();
    int sliceNameId = reconstructedSlice.getSliceNameEnum().getId();
    String fileName = FileName.getFocusConfirmationInspectionImageName(reconstructionRegionName, sliceNameId, sliceOffset);

    XrayImageIoUtil.savePngImage(reconstructedSlice.getImage(), imagesDir + File.separator + fileName);
    _imageManagerObservable.stateChanged(ImageManagerEventEnum.INSPECTION_IMAGE_SAVED);
  }

  /**
   * @author Patrick Lacz
   */
  public void saveInspectionImage(ReconstructedSlice reconstructedSlice,
    ReconstructionRegion reconstructionRegion,
    String imagesDir) throws DatastoreException
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(imagesDir != null);

    String reconsturctionRegionName = reconstructionRegion.getName();
    int sliceNameId = reconstructedSlice.getSliceNameEnum().getId();
    String fileName = FileName.getInspectionImageName(reconsturctionRegionName, sliceNameId);

    if (Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType() )
      XrayImageIoUtil.saveTiffImage(reconstructedSlice.getImage(), imagesDir + File.separator + fileName);
    else
      XrayImageIoUtil.savePngImage(reconstructedSlice.getImage(), imagesDir + File.separator + fileName);
    _imageManagerObservable.stateChanged(ImageManagerEventEnum.INSPECTION_IMAGE_SAVED);
  }

  /**
   * @author George A. David
   */
  public void saveAdjustImages(ReconstructedImages reconImages) throws DatastoreException
  {
    Assert.expect(reconImages != null);
    ReconstructionRegion reconstructionRegion = reconImages.getReconstructionRegion();
    String imagesDir = Directory.getAdjustFocusImagesDir(reconstructionRegion.getTestSubProgram().getTestProgram().getProject().getName());
    for (ReconstructedSlice reconstructedSlice : reconImages.getReconstructedSlices())
    {
      saveAdjustFocusImage(reconstructedSlice, reconstructionRegion, imagesDir);
    }
  }

  /**
   * @author George A. David
   */
  public String generateAdjustFocusFileName(Slice adjustFocusSlice, ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(adjustFocusSlice != null);
    Assert.expect(adjustFocusSlice.isAdditionalSlice());
    Assert.expect(reconstructionRegion != null);

//    Slice origSlice = adjustFocusSlice.getOriginalSliceForAdditionalSlice();
    int offsetInMils = (int) MathUtil.convertNanoMetersToMils(adjustFocusSlice.getFocusInstruction().getZHeightInNanoMeters());
    return FileName.getAdjustFocusImageName(reconstructionRegion.getName(), offsetInMils);

  }

  /**
   * @author Patrick Lacz
   */
  public void saveAdjustFocusImage(ReconstructedSlice reconstructedSlice,
    ReconstructionRegion reconstructionRegion,
    String imagesDir) throws DatastoreException
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(imagesDir != null);

    String reconsturctionRegionName = reconstructionRegion.getName();
    SliceNameEnum sliceName = reconstructedSlice.getSliceNameEnum();
    int sliceNameId = sliceName.getId();

    String fileName = null;
    if (reconstructedSlice.getSliceNameEnum().isAdditionalSlice() == false)
    {
      fileName = FileName.getInspectionImageName(reconsturctionRegionName, sliceNameId);
    }
    else
    {
      fileName = generateAdjustFocusFileName(reconstructionRegion.getSlice(sliceName), reconstructionRegion);
    }

    if (Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType() )
      XrayImageIoUtil.saveTiffImage(reconstructedSlice.getImage(), imagesDir + File.separator + fileName);
    else
      XrayImageIoUtil.savePngImage(reconstructedSlice.getImage(), imagesDir + File.separator + fileName);
    
    _imageManagerObservable.stateChanged(ImageManagerEventEnum.ADJUST_FOCUS_IMAGE_SAVED);
  }

  /**
   * @author George A. David
   */
  public void saveInspectionImagesForOnlineTestDevelopment(ReconstructedImages reconstructedImages) throws DatastoreException
  {
    String dir = Directory.getOnlineXrayImagesDir();
    FileUtilAxi.mkdirs(dir);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
    {
      saveInspectionImage(reconstructedSlice, reconstructionRegion, dir);
    }
  }

  /**
   * Saves all virtual live images for a reconstruction region to disk.
   * @author Scott Richardson
   */
  public void saveVirtualLiveImages(String projectName, ReconstructedImages reconstructedImages, boolean enhanceImageContrast) throws DatastoreException
  {
    Assert.expect(projectName != null);
    Assert.expect(reconstructedImages != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

    for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
    {
      Image orthogonalImage = reconstructedSlice.getOrthogonalImage();
      if (enhanceImageContrast)
      {
        ImageEnhancement.autoEnhanceContrastInPlace(orthogonalImage);
      }
      BufferedImage bufferedImage = orthogonalImage.getBufferedImage();
      for (Slice slice : reconstructionRegion.getFocusGroups().get(0).getSlices())
      {
        if (slice.getFocusInstruction().getZHeightInNanoMeters() == reconstructedSlice.getHeightInNanometers())
        {
          saveVirtualLiveImage(projectName, reconstructionRegion, slice, bufferedImage);
          break;
        }
      }
    }
  }


  /**
   * @author Scott Richardson
   */
  public void saveVirtualLiveImage(String projectName, ReconstructionRegion reconstructionRegion,
                                   Slice slice, java.awt.image.BufferedImage bufferedImage) throws DatastoreException
  {
    String dirName = Directory.getXrayVirtualLiveImagesDir(projectName) + File.separator + VirtualLiveImageGeneration.getInstance().getImageSetName();

    // slice name indexed by <top>_<xCoordinate>_<yCoordinate>_<width>_<height>_<sliceHeight>
    String name = reconstructionRegion.getName() + "_" + slice.getFocusInstruction().getZHeightInNanoMeters();
    String fileName = FileName.getVerificationImageName(name);

    XrayImageIoUtil.saveJpegImage(bufferedImage, dirName + File.separator + fileName);
  }

  /**
   * @author sham
   */
  public void saveVirtualLiveImage(String projectName, java.awt.image.BufferedImage bufferedImage, int zHeightInNanometers, String imageSetDir) throws DatastoreException
  {
    String dirName = "";
    if(imageSetDir == null || imageSetDir == "")
    {
      dirName= Directory.getXrayVirtualLiveImagesDir(projectName) + File.separator + VirtualLiveImageGeneration.getInstance().getImageSetName();
    }
    else
    {
      dirName = imageSetDir;
    }
    String fileName="";
    if (Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType())
    {
      fileName += FileName.getVirtualLiveImageNamePrefix() + "_" + String.valueOf(zHeightInNanometers) + FileName.getVirtualLiveTiffImageExtension();
      XrayImageIoUtil.saveTiffImage(bufferedImage, dirName + File.separator + fileName);
    }
    else
    {
      fileName += FileName.getVirtualLiveImageNamePrefix() + "_" + String.valueOf(zHeightInNanometers) + FileName.getVirtualLiveImageExtension();      
      XrayImageIoUtil.savePngImage(bufferedImage, dirName + File.separator + fileName);
    }
    bufferedImage=null;
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   * @author Ronald Lim
   * @author Khang Wah, Chnee
   * - remove 2.5d images from any contrast enhancement activity
   * @author Lim, Lay Ngor - revert the image back to non-resize so that VVTS able 
   * to display correct locator for diagnostic & enhanced image.
   */
  public void saveRepairImages(ReconstructedImages reconstructedImages,
    int sliceOffset,
    String projectName,
    long timeStampInMillis) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(projectName != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    PanelRectangle panelRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    String resultImagesDirectory = Directory.getInspectionResultsDir(projectName, timeStampInMillis);

    Map<SliceNameEnum, Image> correctedImages = new TreeMap<SliceNameEnum, Image>();
    for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
    {
      correctedImages.put(reconstructedSlice.getSliceNameEnum(), new Image(reconstructedSlice.getImage()));
    }

    ImageEnhancement.autoEnhanceContrastInPlace(excludeUnnecessaryImageFromEnhancement(correctedImages).values());
       
    // XCR-3546 Unable to save 2.5D image When Turn on Save Enhance Image
    for(Map.Entry<SliceNameEnum, Image> entry : correctedImages.entrySet())
    {
      SliceNameEnum sliceNameEnum = entry.getKey();
      int sliceNumber = sliceNameEnum.getId();
      
      Image image = correctedImages.get(sliceNameEnum);
      
      if (EnumToUniqueIDLookup.getInstance().isDiagnosticSlice(sliceNameEnum) || 
          EnumToUniqueIDLookup.getInstance().isEnhancedImageSlice(sliceNameEnum))
      {
        // XCR-3570 REFERENCE COUNT WARNING is added when save enhance image is enable
        image.decrementReferenceCount();
        continue;
      }

      saveRepairImage(image,
        resultImagesDirectory,
        reconstructionRegion.isTopSide(),
        panelRect.getMinX(),
        panelRect.getMinY(),
        panelRect.getWidth(),
        panelRect.getHeight(),
        sliceNumber,
        sliceOffset);

      //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
      SliceNameEnum diagnosticSliceName = EnumToUniqueIDLookup.getInstance().getDiagnosticSliceNameEnum(sliceNameEnum);
      if(diagnosticSliceName != null && reconstructedImages.hasReconstructedSlice(diagnosticSliceName))
      {
        Image diagnosticImage = new Image(reconstructedImages.getReconstructedSlice(diagnosticSliceName).getImage());
        
        //Lim, Lay Ngor - remove the resize in diagnostic image(after enhanced) to original image size for VVTS use.
        final float scaleX = (float)image.getWidth() / (float)diagnosticImage.getWidth();
        final float scaleY = (float)image.getHeight()/ (float)diagnosticImage.getHeight();
        if(scaleX != _IDENTICAL_RESIZE_FACTOR || scaleY != _IDENTICAL_RESIZE_FACTOR)
        {
          Image diagnosticImageWithoutResize = Image.createContiguousFloatImage(image.getWidth(), image.getHeight());
          ImageEnhancer.resizeLinear32bitsImage(diagnosticImageWithoutResize, diagnosticImage, scaleX, scaleY);
          diagnosticImage.decrementReferenceCount();
          diagnosticImage = diagnosticImageWithoutResize;
        }
        
        // Diagnostic Slice with void image
        java.awt.image.BufferedImage diagnosticBufferedImage = com.axi.guiUtil.BufferedImageUtil.overlayImages(image.getBufferedImage(),
                                                              diagnosticImage.getAlphaBufferedImage(com.axi.guiUtil.LayerColorEnum.VOIDING_PIXELS.getColor()), 
                                                              com.axi.guiUtil.LayerColorEnum.VOIDING_PIXELS.getColor(), 
                                                              java.awt.image.BufferedImage.TYPE_INT_RGB);

        //Siew Yeng - XCR-2387
        // Diagnostic Slice with void + largest void image
        float thresholdForGreyColor = (float)com.axi.guiUtil.LayerColorEnum.DIAGNOSTIC_LARGEST_VOIDING_PIXELS.getColor().getRed();
        
        if(Threshold.countPixelsInRange(diagnosticImage, RegionOfInterest.createRegionFromImage(diagnosticImage), thresholdForGreyColor - 1.f, thresholdForGreyColor + 1.f) > 0)
        {
          Threshold.threshold(diagnosticImage, thresholdForGreyColor - 1.f, 0.f, thresholdForGreyColor + 1.f, 0.f);

          diagnosticBufferedImage = com.axi.guiUtil.BufferedImageUtil.overlayImages(diagnosticBufferedImage,
                                  diagnosticImage.getAlphaBufferedImage(com.axi.guiUtil.LayerColorEnum.LARGEST_VOIDING_PIXELS.getColor()), 
                                  com.axi.guiUtil.LayerColorEnum.LARGEST_VOIDING_PIXELS.getColor(), 
                                  java.awt.image.BufferedImage.TYPE_INT_RGB);  
        }

        diagnosticImage.decrementReferenceCount();
        
        Assert.expect(diagnosticBufferedImage != null);
        saveRepairImage(diagnosticBufferedImage,
          resultImagesDirectory,
          reconstructionRegion.isTopSide(),
          panelRect.getMinX(),
          panelRect.getMinY(),
          panelRect.getWidth(),
          panelRect.getHeight(),
          diagnosticSliceName.getId(),
          sliceOffset);

        diagnosticBufferedImage.flush();
      }
     
      //Siew Yeng - XCR-2683 - add enhanced image
      SliceNameEnum enhancedImageSliceName = EnumToUniqueIDLookup.getInstance().getEnhancedImageSliceNameEnum(sliceNameEnum);
      
      if(enhancedImageSliceName != null && reconstructedImages.hasReconstructedSlice(enhancedImageSliceName))
      {
        //Lim, Lay Ngor - remove the resize in diagnostic image(after enhanced) to original image size for VVTS use.
        Image enhancedImage = new Image(reconstructedImages.getReconstructedSlice(enhancedImageSliceName).getOrthogonalImage());
        final float scaleX = (float)image.getWidth() / (float)enhancedImage.getWidth();
        final float scaleY = (float)image.getHeight()/ (float)enhancedImage.getHeight();
        if(scaleX != _IDENTICAL_RESIZE_FACTOR || scaleY != _IDENTICAL_RESIZE_FACTOR)
        {
          Image enhanceImageWithoutResize = Image.createContiguousFloatImage(image.getWidth(), image.getHeight());
          ImageEnhancer.resizeLinear32bitsImage(enhanceImageWithoutResize, enhancedImage, scaleX, scaleY);
          enhancedImage.decrementReferenceCount();
          enhancedImage = enhanceImageWithoutResize;
        }
        
        saveRepairImage(enhancedImage,
          resultImagesDirectory,
          reconstructionRegion.isTopSide(),
          panelRect.getMinX(),
          panelRect.getMinY(),
          panelRect.getWidth(),
          panelRect.getHeight(),
          enhancedImageSliceName.getId(),
          sliceOffset);
        
        enhancedImage.decrementReferenceCount();
      }
      image.decrementReferenceCount();  // get rid of the memory ASAP
    }
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   * @author John Heumann
   * @author Ronald Lim
   * @author Khang Wah, Chnee
   * - remove 2.5d images from any contrast enhancement activity
   */
  public void saveRepairImages(ReconstructedImages reconstructedImages,
    int sliceOffset,
    Set<SliceNameEnum> slicesToExclude,
    String projectName,
    long timeStampInMillis) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(projectName != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    PanelRectangle panelRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    String resultImagesDirectory = Directory.getInspectionResultsDir(projectName, timeStampInMillis);

    Map<SliceNameEnum, Image> correctedImages = new TreeMap<SliceNameEnum, Image>();
    for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
    {
      SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();
      if (slicesToExclude.contains(sliceNameEnum) == false)
      {
        correctedImages.put(reconstructedSlice.getSliceNameEnum(), new Image(reconstructedSlice.getImage()));
      }
    }

    ImageEnhancement.autoEnhanceContrastInPlace(excludeUnnecessaryImageFromEnhancement(correctedImages).values());

    for(Map.Entry<SliceNameEnum, Image> entry : correctedImages.entrySet())
    {
      SliceNameEnum sliceNameEnum = entry.getKey();
      Image image = correctedImages.get(sliceNameEnum);
      
      if (EnumToUniqueIDLookup.getInstance().isDiagnosticSlice(sliceNameEnum) || 
          EnumToUniqueIDLookup.getInstance().isEnhancedImageSlice(sliceNameEnum))
      {
        // XCR-3570 REFERENCE COUNT WARNING is added when save enhance image is enable
        image.decrementReferenceCount();
        continue;
      }
      
      if (slicesToExclude.contains(sliceNameEnum) == false)
      {
        int sliceNumber = sliceNameEnum.getId();

        saveRepairImage(image,
          resultImagesDirectory,
          reconstructionRegion.isTopSide(),
          panelRect.getMinX(),
          panelRect.getMinY(),
          panelRect.getWidth(),
          panelRect.getHeight(),
          sliceNumber,
          sliceOffset);
        
        //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
        SliceNameEnum diagnosticSliceName = EnumToUniqueIDLookup.getInstance().getDiagnosticSliceNameEnum(sliceNameEnum);
        if(diagnosticSliceName != null && reconstructedImages.hasReconstructedSlice(diagnosticSliceName))
        {
          Image diagnosticImage = reconstructedImages.getReconstructedSlice(diagnosticSliceName).getImage();

          // Diagnostic Slice with void image
          java.awt.image.BufferedImage diagnosticBufferedImage = com.axi.guiUtil.BufferedImageUtil.overlayImages(image.getBufferedImage(),
                                                                diagnosticImage.getAlphaBufferedImage(com.axi.guiUtil.LayerColorEnum.VOIDING_PIXELS.getColor()), 
                                                                com.axi.guiUtil.LayerColorEnum.VOIDING_PIXELS.getColor(), 
                                                                java.awt.image.BufferedImage.TYPE_INT_RGB);

          //Siew Yeng - XCR-2387
          // Diagnostic Slice with void + largest void image
          float thresholdForGreyColor = (float)com.axi.guiUtil.LayerColorEnum.DIAGNOSTIC_LARGEST_VOIDING_PIXELS.getColor().getRed();

          if(Threshold.countPixelsInRange(diagnosticImage, RegionOfInterest.createRegionFromImage(diagnosticImage), thresholdForGreyColor - 1.f, thresholdForGreyColor + 1.f) > 0)
          {
            Threshold.threshold(diagnosticImage, thresholdForGreyColor - 1.f, 0.f, thresholdForGreyColor + 1.f, 0.f);

            diagnosticBufferedImage = com.axi.guiUtil.BufferedImageUtil.overlayImages(diagnosticBufferedImage,
                                    diagnosticImage.getAlphaBufferedImage(com.axi.guiUtil.LayerColorEnum.LARGEST_VOIDING_PIXELS.getColor()), 
                                    com.axi.guiUtil.LayerColorEnum.LARGEST_VOIDING_PIXELS.getColor(), 
                                    java.awt.image.BufferedImage.TYPE_INT_RGB);  
          }

          saveRepairImage(diagnosticBufferedImage,
            resultImagesDirectory,
            reconstructionRegion.isTopSide(),
            panelRect.getMinX(),
            panelRect.getMinY(),
            panelRect.getWidth(),
            panelRect.getHeight(),
            diagnosticSliceName.getId(),
            sliceOffset);

          diagnosticBufferedImage.flush();
        }
        
        //Siew Yeng - XCR-2683 - add enhanced image
        SliceNameEnum enhancedImageSliceName = EnumToUniqueIDLookup.getInstance().getEnhancedImageSliceNameEnum(sliceNameEnum);

        if(enhancedImageSliceName != null && reconstructedImages.hasReconstructedSlice(enhancedImageSliceName))
        {
          saveRepairImage(reconstructedImages.getReconstructedSlice(enhancedImageSliceName).getOrthogonalImage(),
            resultImagesDirectory,
            reconstructionRegion.isTopSide(),
            panelRect.getMinX(),
            panelRect.getMinY(),
            panelRect.getWidth(),
            panelRect.getHeight(),
            enhancedImageSliceName.getId(),
            sliceOffset);
        }
      }
      image.decrementReferenceCount();  // get rid of the memory ASAP
    }
  }

  /**
   * @author Matt Wharton
   */
  public void saveRepairImages(ReconstructedImages reconstructedImages,
    String projectName,
    long timeStampInMillis) throws DatastoreException
  {
    saveRepairImages(reconstructedImages, 0, projectName, timeStampInMillis);
  }

  /**
   * @author Matt Wharton
   */
  public List<String> getResultImagePaths(ReconstructionRegion reconstructionRegion,
    String projectName,
    long timeStampInMillis)
  {
    return getResultImagePaths(reconstructionRegion, 0, projectName, timeStampInMillis);
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   * @author Kee Chin Seong - Adding the Width Height to make the image name unique
   */
  public List<String> getResultImagePaths(ReconstructionRegion reconstructionRegion,
    int focusOffsetInNanometers,
    String projectName,
    long timeStampInMillis)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(projectName != null);

    List<String> resultImagePaths = new LinkedList<String>();

    PanelRectangle panelRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    String resultImagesDirectory = Directory.getInspectionResultsDir(projectName, timeStampInMillis);

    for (SliceNameEnum sliceNameEnum : reconstructionRegion.getInspectedSliceNames())
    {
      int sliceNumber = sliceNameEnum.getId();

      resultImagePaths.add(resultImagesDirectory + File.separator
        + FileName.getResultImageName(reconstructionRegion.isTopSide(),
        panelRect.getMinX(),
        panelRect.getMinY(),
        panelRect.getWidth(),
        panelRect.getHeight(),
        sliceNumber,
        focusOffsetInNanometers));
    }


    return resultImagePaths;
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   * @author John Heumann
   * @author Kee Chin Seong - Adding the Width Height to make the image name unique
   */
  public List<String> getResultImagePaths(ReconstructionRegion reconstructionRegion,
    int focusOffsetInNanometers,
    Set<SliceNameEnum> slicesToExclude,
    String projectName,
    long timeStampInMillis)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(projectName != null);

    List<String> resultImagePaths = new LinkedList<String>();

    PanelRectangle panelRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    String resultImagesDirectory = Directory.getInspectionResultsDir(projectName, timeStampInMillis);

    for (SliceNameEnum sliceNameEnum : reconstructionRegion.getInspectedSliceNames())
    {
      if (slicesToExclude.contains(sliceNameEnum) == false)
      {
        int sliceNumber = sliceNameEnum.getId();

        resultImagePaths.add(resultImagesDirectory + File.separator
          + FileName.getResultImageName(reconstructionRegion.isTopSide(),
          panelRect.getMinX(),
          panelRect.getMinY(),
          panelRect.getWidth(),
          panelRect.getHeight(),
          sliceNumber,
          focusOffsetInNanometers));
      }
    }


    return resultImagePaths;
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   * @author Kee Chin Seong - Adding the Width Height to make the image name unique
   */
  private void saveRepairImage(java.awt.image.BufferedImage bufferedImage,
    String resultImagesDirectory,
    boolean topSide,
    int xCoordinate,
    int yCoordinate,
    int width, 
    int height,
    int sliceNumber,
    int sliceOffset)
    throws DatastoreException
  {
    String fileName = FileName.getResultImageName(topSide, xCoordinate, yCoordinate, width, height, sliceNumber, sliceOffset);
    
    // XCR-2465 Ability to transfer PNG image to VVTS - Cheah Lee Herng
    if (Config.getInstance().getIntValue(SoftwareConfigEnum.REPAIR_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getPngImageType())
      XrayImageIoUtil.savePngImage(bufferedImage, resultImagesDirectory + File.separator + fileName);
    else
      XrayImageIoUtil.saveJpegImage(bufferedImage, resultImagesDirectory + File.separator + fileName);
  }

  /**
   * @author Ronald Lim
   * @author Kee Chin Seong - Adding the Width Height to make the image name unique
   */
  private void saveRepairImage(Image image,
    String resultImagesDirectory,
    boolean topSide,
    int xCoordinate,
    int yCoordinate,
    int width,
    int height,
    int sliceNumber,
    int sliceOffset)
    throws DatastoreException
  {
    String fileName = FileName.getResultImageName(topSide, xCoordinate, yCoordinate, width, height, sliceNumber, sliceOffset);
    
    // XCR-2465 Ability to transfer PNG image to VVTS - Cheah Lee Herng
    if (Config.getInstance().getIntValue(SoftwareConfigEnum.REPAIR_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getPngImageType())
      XrayImageIoUtil.savePngImage(image, resultImagesDirectory + File.separator + fileName);
    else
      XrayImageIoUtil.saveJpegImage(image, resultImagesDirectory + File.separator + fileName);
  }

  /**
   * @author George A. David
   */
  private ReconstructedImages loadInspectionImages(String inspectionImagesDir,
    ReconstructionRegion reconstructionRegion,
    Collection<Slice> slices) throws DatastoreException
  {
    Assert.expect(inspectionImagesDir != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(slices != null);

    ReconstructedImages reconImages = new ReconstructedImages(reconstructionRegion);

    for (Slice slice : slices)
    {
      SliceNameEnum sliceName = slice.getSliceName();
      String tiffFilename = FileName.getTiffInspectionImageName(reconstructionRegion.getName(), sliceName.getId());
      String pngFilename = FileName.getPngInspectionImageName(reconstructionRegion.getName(), sliceName.getId());

      Image image = null;

      if(FileUtil.exists(inspectionImagesDir + File.separator + tiffFilename))
        image = XrayImageIoUtil.loadTiffImage(inspectionImagesDir + File.separator + tiffFilename);
      else if(FileUtil.exists(inspectionImagesDir + File.separator + pngFilename))
        image = XrayImageIoUtil.loadPngImage(inspectionImagesDir + File.separator + pngFilename);
      else
      {
        DatastoreException dex = new FileNotFoundDatastoreException(inspectionImagesDir);
        throw dex;
      }
      
      reconImages.addImage(image, sliceName);
 
      image.decrementReferenceCount();
    }    
    Assert.expect(reconImages.getNumberOfSlices() > 0);
    return reconImages;
  }

  /**
   * Loads all inspection images for a reconstruction region from disk.
   * @author Kay Lannen
   * @author Matt Wharton
   */
  public ReconstructedImages loadInspectionImages(ImageSetData imageSetData,
    ReconstructionRegion reconstructionRegion) throws DatastoreException
  {
    Assert.expect(imageSetData != null);
    String inspectionImagesDir = null;
    if (imageSetData.isOnlineTestDevelopment())
    {
      inspectionImagesDir = Directory.getOnlineXrayImagesDir(imageSetData.getImageSetName());
    }
    else
    {
      inspectionImagesDir = Directory.getXrayInspectionImagesDir(imageSetData.getProjectName(), imageSetData.getImageSetName());
    }

    return loadInspectionImages(inspectionImagesDir, reconstructionRegion, imageSetData.isGenerateMultiAngleImagesEnabled());
  }

  /**
   * @author George A. David
   */
  public ReconstructedImages loadInspectionImages(String imagesDir,
    ReconstructionRegion reconstructionRegion,
    boolean hasMultiAngleImages) throws DatastoreException
  {
    Assert.expect(imagesDir != null);
    Assert.expect(reconstructionRegion != null);

    Assert.expect(reconstructionRegion.getNumberOfSlices() > 0);
    Assert.expect(reconstructionRegion.getInspectedSlices().isEmpty() == false);

    List<Slice> slices = new LinkedList<Slice>();
    for (Slice slice : reconstructionRegion.getReconstructionSlices())
    {
      // 2012-02-28 by Chnee Khang Wah : 2.5D
      // 2012-11-14 by Ying-Huan.Chu   : Removed 'slice.getSliceType().equals(SliceTypeEnum.PROJECTION)'
      // 2013-03-07 by Cheah Lee Herng : Handle 2.5D projection images looping (XCR1594)
      if (slice.getSliceType().equals(SliceTypeEnum.SURFACE) ||
          (hasMultiAngleImages == false && slice.getSliceType().equals(SliceTypeEnum.PROJECTION))) 
      {
        continue; // this slice is only used in the IRPs, no images are ever saved
      }
      slices.add(slice);
    }

    return loadInspectionImages(imagesDir, reconstructionRegion, slices);
  }

  /**
   * Loads only the images that are actually to be inspected. For instance,
   * for Throughhole, this method returns only 3 slices while loadInspectionImages returns 21.
   * Copied from loadInspectionImages
   * @author Patrick Lacz
   */
  public ReconstructedImages loadOnlyInspectedImages(ImageSetData imageSetData,
    ReconstructionRegion reconstructionRegion)
    throws DatastoreException
  {
    Assert.expect(imageSetData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(reconstructionRegion.getNumberOfSlices() > 0);
    Assert.expect(reconstructionRegion.getInspectedSlices().isEmpty() == false);

    String inspectionImagesDir = null;
    if (imageSetData.isOnlineTestDevelopment())
    {
      inspectionImagesDir = Directory.getOnlineXrayImagesDir(imageSetData.getImageSetName());
    }
    else
    {
      inspectionImagesDir = Directory.getXrayInspectionImagesDir(imageSetData.getProjectName(), imageSetData.getImageSetName());
    }
    return loadInspectionImages(inspectionImagesDir, reconstructionRegion, reconstructionRegion.getInspectedSlices());
  }

  /**
   * Loads the 'lightest' images for the specified region from the specified image set.
   *
   * @author Matt Wharton
   */
  public ReconstructedImages loadLightestInspectionImages(ImageSetData imageSetData,
    ReconstructionRegion reconstructionRegion) throws DatastoreException
  {
    Assert.expect(imageSetData != null);
    Assert.expect(reconstructionRegion != null);

    ReconstructedImages lightestImages = new ReconstructedImages(reconstructionRegion);

    String imageSetDir = Directory.getXrayInspectionImagesDir(imageSetData.getProjectName(), imageSetData.getImageSetName());
    for (Slice slice : reconstructionRegion.getSlices())
    {
      if (slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION))
      {
        SliceNameEnum sliceName = slice.getSliceName();
        String lightestImageFileName = FileName.getLightestInspectionImageName(reconstructionRegion.getName(), sliceName.getId());

        Image image = XrayImageIoUtil.loadPngImage(imageSetDir + File.separator + lightestImageFileName);
        lightestImages.addImage(image, sliceName);
        image.decrementReferenceCount();
      }
    }

    return lightestImages;
  }

  /**
   * Loads the focus confirmation images for the specified region at the specified offset from the specified image set.
   *
   * @author Matt Wharton
   */
  public ReconstructedImages loadFocusConfirmationInspectionImages(ImageSetData imageSetData,
    ReconstructionRegion reconstructionRegion,
    int sliceOffsetInNanometers) throws DatastoreException
  {
    Assert.expect(imageSetData != null);
    Assert.expect(reconstructionRegion != null);

    ReconstructedImages focusConfirmationImages = new ReconstructedImages(reconstructionRegion);

    String imageSetDir = Directory.getXrayInspectionImagesDir(imageSetData.getProjectName(), imageSetData.getImageSetName());
    for (Slice slice : reconstructionRegion.getSlices())
    {
      if (slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION))
      {
        SliceNameEnum sliceName = slice.getSliceName();
        String focusConfirmationImageFileName = FileName.getFocusConfirmationInspectionImageName(reconstructionRegion.getName(),
          sliceName.getId(),
          sliceOffsetInNanometers);

        Image image = XrayImageIoUtil.loadPngImage(imageSetDir + File.separator + focusConfirmationImageFileName);
        focusConfirmationImages.addImage(image, sliceName);
        image.decrementReferenceCount();
      }
    }

    return focusConfirmationImages;
  }

  /**
   * Saves a panel tile image to disk.
   * The image will be saved to the online Xray images location.
   * @author Kay Lannen
   */
  public void saveVerificationImage(String projectName,
    ReconstructionRegion verificationRegion,
    java.awt.image.BufferedImage bufferedImage) throws DatastoreException
  {
    Assert.expect(verificationRegion != null);

    String dirName = Directory.getXrayVerificationImagesDir(projectName);
    String fileName = FileName.getVerificationImageName(verificationRegion.getName());
    XrayImageIoUtil.saveJpegImage(bufferedImage, dirName + File.separator + fileName);
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void save2DVerificationImage(String projectName,
    ReconstructionRegion verificationRegion,
    java.awt.image.BufferedImage bufferedImage) throws DatastoreException
  {
    Assert.expect(verificationRegion != null);
    
    String dirName = Directory.getXray2DVerificationImagesDir(projectName);
    String fileName = FileName.getVerificationImageName(verificationRegion.getName());
    XrayImageIoUtil.saveJpegImage(bufferedImage, dirName + File.separator + fileName);
  }

  /**
   * @author George A. David
   */
  public void saveVerificationImages(String projectName, ReconstructedImages reconImages) throws DatastoreException
  {
    Assert.expect(reconImages != null);
    ReconstructionRegion reconstructionRegion = reconImages.getReconstructionRegion();

    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    SliceNameEnum sliceName = SliceNameEnum.PAD;
    
    if (reconstructionRegion.getTestSubProgram().getTestProgram().isManual2DAlignmentInProgress()
          && _xRayTester.isSimulationModeOn() == false)
    {
      sliceName = SliceNameEnum.CAMERA_0;
    }
    
    for (ReconstructedSlice reconstructedSlice : reconImages.getReconstructedSlices())
    {
      if (reconstructedSlice.getSliceNameEnum() != sliceName)
        continue;
      
      java.awt.image.BufferedImage buffImg = reconstructedSlice.getOrthogonalImage().getBufferedImage();
      saveVerificationImage(projectName, reconstructionRegion, buffImg);
    }
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void save2DVerificationImages(String projectName, ReconstructedImages reconImages) throws DatastoreException
  {
    Assert.expect(reconImages != null);
    ReconstructionRegion reconstructionRegion = reconImages.getReconstructionRegion();
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    SliceNameEnum sliceName = SliceNameEnum.PAD;
    
    if (reconstructionRegion.getTestSubProgram().getTestProgram().isManual2DAlignmentInProgress()
          && _xRayTester.isSimulationModeOn() == false)
    {
      sliceName = SliceNameEnum.CAMERA_0;
    }
    
    for (ReconstructedSlice reconstructedSlice : reconImages.getReconstructedSlices())
    {
      if (reconstructedSlice.getSliceNameEnum() != sliceName)
        continue;
      
      java.awt.image.BufferedImage buffImg = reconstructedSlice.getOrthogonalImage().getBufferedImage();
      save2DVerificationImage(projectName, reconstructionRegion, buffImg);
    }
  }

  /**
   * Load a panel tile image from disk.
   * @author Kay Lannen
   */
  public java.awt.image.BufferedImage loadVerificationImage(String projectName, ReconstructionRegion reconstructionRegion) throws DatastoreException
  {
    String dirName = Directory.getXrayVerificationImagesDir(projectName);    
    String fileName = FileName.getVerificationImageName(reconstructionRegion.getName());

    return XrayImageIoUtil.loadJpegBufferedImage(dirName + File.separator + fileName);
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public java.awt.image.BufferedImage load2DVerificationImage(String projectName, ReconstructionRegion reconstructionRegion) throws DatastoreException
  {
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    String dirName = Directory.getXray2DVerificationImagesDir(projectName);      
    String fileName = FileName.getVerificationImageName(reconstructionRegion.getName());

    return XrayImageIoUtil.loadJpegBufferedImage(dirName + File.separator + fileName);
  }

  /**
   * @author Scott Richardson
   */
  public java.awt.image.BufferedImage loadVirtualLiveImage(String projectName, String imageName) throws DatastoreException
  {
    String dirName = Directory.getXrayVirtualLiveImagesDir(projectName);

    return XrayImageIoUtil.loadJpegBufferedImage(dirName + File.separator + imageName);
  }

  /**
   * @author George A. David
   * @author Jack Hwee XCR1164 - fix image set comparability
   * added a new parameter by sheng chuan to determine need to update the compatibility or not
   */
  public List<ImageSetData> getImageSetData(String projectName, boolean updateImageSetCompatibility) throws DatastoreException
  {
    Assert.expect(projectName != null);

    List<ImageSetData> imageSetDataList = new LinkedList<ImageSetData>();
    String inspectionImagesDir = Directory.getXrayInspectionImagesDir(projectName);
    ImageSetInfoReader reader = new ImageSetInfoReader();


    if (FileUtil.exists(inspectionImagesDir) == false)
    {
      return imageSetDataList;
    }

    Collection<String> filePaths = FileUtil.listDirectories(inspectionImagesDir);

    if (filePaths.isEmpty())
    {
      return imageSetDataList;
    }

    if(isIgnoreCompatibilityCheck())
    { 
      for (String filePath : filePaths)
      {
        File file = new File(filePath);
        if (file.isDirectory())
        {
          String imageSetInfoPath = filePath + File.separator + FileName.getImageSetInfoFileName();
          if (FileUtilAxi.exists(imageSetInfoPath))
          {
            reader.parseFile(imageSetInfoPath);
            ImageSetData imageSetData = reader.getImageSetData();
            if (imageSetData.getProjectName().equalsIgnoreCase(projectName))
            {
              //Yi Fong
              // XCR 3322 subtask: XCR 3702 -  Software crash when run test with different joint types after recipe conversion	  
              // to write in the project magnification info into those image set info without the magnification info
              if (imageSetData.hasHighMagnification() == false || imageSetData.hasLowMagnification() == false)
              {
                ImageSetInfoWriter writer = new ImageSetInfoWriter();
                imageSetData.setHighMagnification(getInstance().getPrevProjectHighMagnification());
                writer.write(imageSetData);
                imageSetData.setLowMagnification(getInstance().getPrevProjectLowMagnification());
                writer.write(imageSetData);
              }
              if(updateImageSetCompatibility)
              { 
                imageSetData.setPercentOfCompatibleImages(calculateImageDataCompatibility(imageSetData,projectName));
              }  
              imageSetDataList.add(imageSetData);
              //XCR 3322: Software crash when run test with different joint types after recipe conversion 
              //to compare the magnification between the image set and the system
              //if is not equal then the image set will be removed from the imageSetDataList
              if (imageSetData.hasHighMagnification() || imageSetData.hasLowMagnification())
              {
                if (imageSetData.getLowMagnification().equals(Config.getSystemCurrentLowMagnification()) == false ||
                    imageSetData.getHighMagnification().equals(Config.getSystemCurrentHighMagnification()) == false)
                {
                  imageSetDataList.remove(imageSetData); 
                }
              }
            }
          }          
        }
      }
    }
    else
    {
      // XCR1566 Lee Herng 30 Jan 2013 - Getting previous checksum cause the select image set operation to be very slow.
      // To speed up, we do not support getting previous checksum anymore from 5.3 version.

      for (String filePath : filePaths)
      {
        File file = new File(filePath);
        if (file.isDirectory())
        {
          String imageSetInfoPath = filePath + File.separator + FileName.getImageSetInfoFileName();          
          if (FileUtilAxi.exists(imageSetInfoPath))
          {
            reader.parseFile(imageSetInfoPath);
            ImageSetData imageSetData = reader.getImageSetData();
            if (imageSetData.getProjectName().equalsIgnoreCase(projectName))
            {
              //Yi Fong
              // XCR 3322 subtask: XCR 3702 -  Software crash when run test with different joint types after recipe conversion	  
              // to write in the project magnification info into those image set info without the magnification info
              if (imageSetData.hasHighMagnification() == false || imageSetData.hasLowMagnification() == false)
              {
                ImageSetInfoWriter writer = new ImageSetInfoWriter();
                imageSetData.setHighMagnification(getInstance().getPrevProjectHighMagnification());
                writer.write(imageSetData);
                imageSetData.setLowMagnification(getInstance().getPrevProjectLowMagnification());
                writer.write(imageSetData);
              }
              if (updateImageSetCompatibility)
              {
                imageSetData.setPercentOfCompatibleImages(calculateImageDataCompatibility(imageSetData, projectName));
              }
              imageSetDataList.add(imageSetData);
              //XCR 3322: Software crash when run test with different joint types after recipe conversion 
              // to compare the magnification between the image set and the system
              //if is not equal then the image set will be removed from the imageSetDataList
              if (imageSetData.hasHighMagnification() || imageSetData.hasLowMagnification())
              {
                if (imageSetData.getLowMagnification().equals(Config.getSystemCurrentLowMagnification()) == false ||
                    imageSetData.getHighMagnification().equals(Config.getSystemCurrentHighMagnification()) == false)
                {
                  imageSetDataList.remove(imageSetData);
                }
              }             
            }
          }
        }
      }
    }
    return imageSetDataList;
  }

  /**
   * @param imageSetData
   * @param projectName
   * @return
   * @throws DatastoreException 
   * @author sheng chuan
   */
  public double calculateImageDataCompatibility(ImageSetData imageSetData,String projectName) throws DatastoreException
  {
    String inspectionImagesDir = Directory.getXrayInspectionImagesDir(projectName);

    Collection<String> filePaths = FileUtil.listDirectories(inspectionImagesDir);

    if (isIgnoreCompatibilityCheck())
    {
      for (String filePath : filePaths)
      {
        File file = new File(filePath);
        //XCR-3133, Ignore Image set compatibility check cause a compatible image set become not compatible
        if (imageSetData.getImageSetName().equalsIgnoreCase(file.getName()) && file.isDirectory())
        {
          Collection<String> projectImageFiles = getInspectionImageFileNames(projectName);

          String imageSetInfoPath = filePath + File.separator + FileName.getImageSetInfoFileName();
          String InspectionImageInfoPath = filePath + File.separator + FileName.getInspectionImageInfoProgramFileName();

          Collection<String> allImageFiles = FileUtil.listAllFileNamesWithoutExtensionInDirectory(filePath);
          allImageFiles.remove(FileUtil.getNameWithoutExtension(imageSetInfoPath));
          allImageFiles.remove(FileUtil.getNameWithoutExtension(InspectionImageInfoPath));
          int totalImages = allImageFiles.size();
          allImageFiles.removeAll(projectImageFiles);
          int totalCompatibleImages = totalImages - allImageFiles.size();
          if (totalImages == 0)  // avoid the divide by zero issue
          {
            return 0;
          }
          else
          {
            return ((double) totalCompatibleImages / (double) totalImages * 100.0);
          }
        }
      }
    }
    else
    {
      Set<String> usedInspectionImageInfos = getInspectionImageInfos(projectName);
      Set<String> imageSetInspectionImageInfos = getInspectionImageInfo(imageSetData);

      int totalImages = imageSetInspectionImageInfos.size();

      imageSetInspectionImageInfos.removeAll(usedInspectionImageInfos);

      int totalCompatibleImages = totalImages - imageSetInspectionImageInfos.size();

      if (totalImages == 0)  // avoid the divide by zero issue
      {
        return 0.0;
      }
      else
      {
        return ((double) totalCompatibleImages / (double) totalImages * 100.0);
      }
    }
    
    return 0.0;
  }
  
  /**
   * @author George A. David
   */
  public List<ImageSetData> getImageSetData(Project project, boolean checkImageSetCompatibility) throws DatastoreException
  {
    return getImageSetData(project.getName(), checkImageSetCompatibility);
  }

  /**
   * @author George A. David
   */
  public List<ImageSetData> getCompatibleImageSetData(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    return getCompatibleImageSetData(project.getName());
  }

  /**
   * @author George A. David
   */
  public List<ImageSetData> getCompatibleImageSetData(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    List<ImageSetData> compatibleImageSetDataList = new LinkedList<ImageSetData>();
    for (ImageSetData imageSetData : getImageSetData(projectName,true))
    {
      if (imageSetData.getPercentOfCompatibleImages() > 0)
      {
        compatibleImageSetDataList.add(imageSetData);
      }
    }

    return compatibleImageSetDataList;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void deleteOnlineCachedImages() throws DatastoreException
  {
    FileUtilAxi.deleteDirectoryContents(Directory.getOnlineXrayVerificationImagesDir());
    _imageManagerObservable.stateChanged(ImageManagerEventEnum.VERIFICATION_IMAGE_DELETED);
  }

  /**
   * @author George A. David
   */
  public void deleteImages(ImageSetData imageSetData) throws DatastoreException
  {
    Assert.expect(imageSetData != null);

    String dir = Directory.getXrayInspectionImagesDir(imageSetData.getProjectName(), imageSetData.getImageSetName());
    if (FileUtilAxi.exists(dir))
    {
      FileUtilAxi.delete(dir);
    }
  }

  /**
   * @author Peter Esbensen
   */
  public boolean reconstructedImagesFilesExist(ImageSetData imageSetData, ReconstructionRegion reconstructionRegion)
  {
    String inspectionImagesDir = Directory.getXrayInspectionImagesDir(imageSetData.getProjectName(), imageSetData.getImageSetName());

    boolean exists = true;
    for (Slice slice : reconstructionRegion.getReconstructionSlices())
    {
      SliceNameEnum sliceName = slice.getSliceName();
      String filename = FileName.getInspectionImageName(reconstructionRegion.getName(), sliceName.getId());

      if (FileUtil.exists(inspectionImagesDir + File.separator + filename) == false)
      {
        exists = false;
        break;
      }
    }
    return exists;
  }

  /**
   * @author Patrick Lacz
   * @author Aimee Ong
   * @author Wei Chin
   */
  public Set<ReconstructionRegion> whichReconstructionRegionsHaveCompatibleImages(ImageSetData imageSetData, Collection<ReconstructionRegion> regions) throws DatastoreException
  {
    Assert.expect(imageSetData != null);
    Assert.expect(regions != null);
    Set<ReconstructionRegion> recoRegions = new HashSet<ReconstructionRegion>();

    if (regions.isEmpty() == false)
    {
      Project project = regions.iterator().next().getTestSubProgram().getTestProgram().getProject();

      if(isIgnoreCompatibilityCheck())
      {
        for (ReconstructionRegion region : regions)
        {
          if (region.getNumberOfSlices() == 0)
          {
            continue;
          }

          String inspectionImagesDir = Directory.getXrayInspectionImagesDir(imageSetData.getProjectName(), imageSetData.getImageSetName());
          if (imageSetData.isOnlineTestDevelopment())
          {
            inspectionImagesDir = Directory.getOnlineXrayImagesDir(imageSetData.getImageSetName());
          }
          else
          {
            inspectionImagesDir = Directory.getXrayInspectionImagesDir(imageSetData.getProjectName(), imageSetData.getImageSetName());
          }

          if (FileUtilAxi.exists(inspectionImagesDir) == false)
          {
            continue;
          }

          for (Slice slice : region.getReconstructionSlices())
          {
            // we always ignore the surface modeling slice, only the IRPs use this
            if (slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION))
            {
              SliceNameEnum sliceName = slice.getSliceName();
              String tiffFilename = FileName.getTiffInspectionImageName(region.getName(), sliceName.getId());
              String pngFilename = FileName.getPngInspectionImageName(region.getName(), sliceName.getId());

              if (FileUtil.exists(inspectionImagesDir + File.separator + tiffFilename))
              {
                recoRegions.add(region);
              }
              else if(FileUtil.exists(inspectionImagesDir + File.separator + pngFilename))
              {
                recoRegions.add(region);
              }
            }
          }
        }
      }
      else
      {
        Set<String> imageInfos = getInspectionImageInfo(imageSetData);
        int panelThicknessInNanos = project.getPanel().getThicknessInNanometers();

        //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on";
        if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
            panelThicknessInNanos = panelThicknessInNanos - XrayTester.getSystemPanelThicknessOffset();

        boolean haveAllImagesForThisRegion = true;

        for (ReconstructionRegion region : regions)
        {
          // Verify that the region actually has slices. This should not occur in the production code, however it
          // may occur during development due to splitting of regions for IRPs, etc.
          // @author Patrick Lacz
          if (region.getNumberOfSlices() == 0)
          {
            continue;
          }

          String inspectionImagesDir = Directory.getXrayInspectionImagesDir(imageSetData.getProjectName(), imageSetData.getImageSetName());
          if (imageSetData.isOnlineTestDevelopment())
          {
            inspectionImagesDir = Directory.getOnlineXrayImagesDir(imageSetData.getImageSetName());
          }
          else
          {
            inspectionImagesDir = Directory.getXrayInspectionImagesDir(imageSetData.getProjectName(), imageSetData.getImageSetName());
          }

          if (FileUtilAxi.exists(inspectionImagesDir) == false)
          {
            continue;
          }
        
          // XCR1677 - When fine tuning subype, need to regenerate test program everytime slice setup is changed.
          // The subtype collection will be reset everytime save, slow load, and test program generated.
          ProjectState projectState = project.getProjectState();       

          Collection<Subtype> subtypesWithInvalidatedSliceHeight = projectState.getInvalidSubtypes();
        
          if (subtypesWithInvalidatedSliceHeight.size() > 0)
          {
            for (Subtype subtype : region.getSubtypes())
            {
              if (subtypesWithInvalidatedSliceHeight.contains(subtype))
                return recoRegions;
            }
          }
    
          haveAllImagesForThisRegion = true;        
          for (Slice slice : region.getReconstructionSlices())
          {
            // we always ignore the surface modeling slice, only the IRPs use this
            if (slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION))
            {
              SliceNameEnum sliceName = slice.getSliceName();
              String filename = FileName.getInspectionImageName(region.getName(), sliceName.getId());

              if (FileUtil.exists(inspectionImagesDir + File.separator + filename) == false)
              {
                haveAllImagesForThisRegion = false;
                break;
              }
            }
          }

          if (haveAllImagesForThisRegion)
          {
            // XCR1566 Lee Herng 30 Jan 2013 - Remove previous checksum checking here as we do not want to slow down 
            // the overall compatibility check.
            if (imageInfos.containsAll(getChecksumValues(region, panelThicknessInNanos, true)))
            {
              recoRegions.add(region);
            }
          }
        }
      }
    }
    return recoRegions;
  }

  /**
   * @author Matt Wharton
   */
  public boolean haveLightestInspectionImagesForReconstructionRegion(ImageSetData imageSetData,
    ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(imageSetData != null);
    Assert.expect(reconstructionRegion != null);

    // The lightest images will always be in a specific image set directory.
    String imageSetDirectory = Directory.getXrayInspectionImagesDir(imageSetData.getProjectName(),
      imageSetData.getImageSetName());

    if (FileUtilAxi.exists(imageSetDirectory) == false)
    {
      return false;
    }

    boolean haveAllLightestImagesForRegion = true;
    for (Slice slice : reconstructionRegion.getReconstructionSlices())
    {
      if (slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION))
      {
        SliceNameEnum sliceName = slice.getSliceName();
        String lightestImageFileName = FileName.getLightestInspectionImageName(reconstructionRegion.getName(),
          sliceName.getId());

        if (FileUtilAxi.exists(imageSetDirectory + File.separator + lightestImageFileName) == false)
        {
          haveAllLightestImagesForRegion = false;
          break;
        }
      }
    }

    return haveAllLightestImagesForRegion;
  }

  /**
   * @author Matt Wharton
   */
  public boolean haveFocusConfirmationInspectionImagesForRegion(ImageSetData imageSetData,
    ReconstructionRegion reconstructionRegion,
    int sliceOffsetInNanometers)
  {
    Assert.expect(imageSetData != null);
    Assert.expect(reconstructionRegion != null);

    // The focus confirmation images will always be in a specific image set directory.
    String imageSetDirectory = Directory.getXrayInspectionImagesDir(imageSetData.getProjectName(),
      imageSetData.getImageSetName());

    if (FileUtilAxi.exists(imageSetDirectory) == false)
    {
      return false;
    }

    boolean haveAllFocusConfirmationImagesForRegionAtOffset = true;
    for (Slice slice : reconstructionRegion.getReconstructionSlices())
    {
      if (slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION))
      {
        SliceNameEnum sliceName = slice.getSliceName();
        String focusConfirmationImageFileName = FileName.getFocusConfirmationInspectionImageName(reconstructionRegion.getName(),
          sliceName.getId(),
          sliceOffsetInNanometers);

        if (FileUtilAxi.exists(imageSetDirectory + File.separator + focusConfirmationImageFileName) == false)
        {
          haveAllFocusConfirmationImagesForRegionAtOffset = false;
          break;
        }
      }
    }

    return haveAllFocusConfirmationImagesForRegionAtOffset;
  }

  /**
   * @author George A. David
   */
  private Set<String> getInspectionImageInfos(String projectName) throws DatastoreException
  {
    Set<String> imageInfos = null;
    if (Project.isCurrentProjectLoaded())
    {
      Project project = Project.getCurrentlyLoadedProject();
      if (project.getName().equalsIgnoreCase(projectName))
      {
        imageInfos = getInspectionImageInfos(project.getTestProgram());
      }
      else
      {
        imageInfos = readInspectionImageInfo(projectName);
      }
    }
    else
    {
      imageInfos = readInspectionImageInfo(projectName);
    }
    Assert.expect(imageInfos != null);

    return imageInfos;
  }

   /**
   * @author Jack Hwee
   * XCR1164 - fix image set comparability
   */
  private Set<String> getPreviousInspectionImageInfos(String projectName) throws DatastoreException
  {
    Set<String> imageInfos = null;
    if (Project.isCurrentProjectLoaded())
    {
      Project project = Project.getCurrentlyLoadedProject();
      if (project.getName().equalsIgnoreCase(projectName))
      {
        imageInfos = getPreviousInspectionImageInfos(project.getTestProgram());
      }
      else
      {
        imageInfos = readInspectionImageInfo(projectName);
      }
    }
    else
    {
      imageInfos = readInspectionImageInfo(projectName);
    }
    Assert.expect(imageInfos != null);

    return imageInfos;
  }

  /**
   * @author George A. David
   * @author Aimee Ong
   */
  public Set<String> getInspectionImageInfos(TestProgram testProgram, Collection<ReconstructionRegion> inspectionRegions, boolean useOnlyCreatedSlices)
  {
    Assert.expect(testProgram != null);
    Assert.expect(inspectionRegions != null);

    Set<String> imageInfos = new HashSet<String>();
    int panelThicknessInNanos = testProgram.getProject().getPanel().getThicknessInNanometers();

    //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on";
    if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
      panelThicknessInNanos = panelThicknessInNanos - XrayTester.getSystemPanelThicknessOffset();
    
    for (ReconstructionRegion inspectionRegion : inspectionRegions)
    { 
      imageInfos.addAll(getChecksumValues(inspectionRegion, panelThicknessInNanos, useOnlyCreatedSlices));
    }

    return imageInfos;
  }

   /**
   * @author Jack Hwee XCR1164 - fix image set comparability
   */
  public Set<String> getPreviousInspectionImageInfos(TestProgram testProgram, Collection<ReconstructionRegion> inspectionRegions, boolean useOnlyCreatedSlices)
  {
    Assert.expect(testProgram != null);
    Assert.expect(inspectionRegions != null);

    Set<String> imageInfos = new HashSet<String>();
    int panelThicknessInNanos = testProgram.getProject().getPanel().getThicknessInNanometers();

    //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on";
    if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
      panelThicknessInNanos = panelThicknessInNanos - XrayTester.getSystemPanelThicknessOffset();

    for (ReconstructionRegion inspectionRegion : inspectionRegions)
    {
    imageInfos.addAll(getPreviousChecksumValues(inspectionRegion, panelThicknessInNanos, useOnlyCreatedSlices));  //Jack Hwee XCR1164 - fix image set comparability

    }

    return imageInfos;
  }

  /**
   * @author George A. David
   */
  public Set<String> getInspectionImageInfos(TestProgram testProgram)
  {
    return getInspectionImageInfos(testProgram, testProgram.getFilteredInspectionRegions(), true);
  }

  /**
   * @author Jack Hwee
   * XCR1164 - fix image set comparability
   */
  public Set<String> getPreviousInspectionImageInfos(TestProgram testProgram)
  {
    return getPreviousInspectionImageInfos(testProgram, testProgram.getFilteredInspectionRegions(), true);
  }

  /**
   * @author George A. David
   */
  public ReconstructedImages loadAdjustFocusImages(ReconstructionRegion reconstructionRegion) throws DatastoreException
  {
    Assert.expect(reconstructionRegion != null);

    Assert.expect(reconstructionRegion.getNumberOfSlices() > 0);
    Assert.expect(reconstructionRegion.getInspectedSlices().isEmpty() == false);

    List<Slice> slices = new LinkedList<Slice>();
    for (Slice slice : reconstructionRegion.getReconstructionSlices())
    {
      if (slice.createSlice() && slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION))
      {
        slices.add(slice);
      }
    }

    for (ReconstructionRegion adjustFocusRegion : reconstructionRegion.getTestSubProgram().getAdjustFocusRegions(reconstructionRegion.getName()))
    {
      for (Slice slice : adjustFocusRegion.getReconstructionSlices())
      {
        if (slice.getSliceType().equals(SliceTypeEnum.SURFACE) || slice.getSliceType().equals(SliceTypeEnum.PROJECTION)) // Chnee Khang Wah, 2012-02-28, 2.5D
        {
          continue; // this slice is only used in the IRPs, no images are ever saved
        }
        slices.add(slice);
      }
    }

    ReconstructedImages reconImages = new ReconstructedImages(reconstructionRegion);
    String imagesDir = Directory.getAdjustFocusImagesDir(reconstructionRegion.getTestSubProgram().getTestProgram().getProject().getName());

    for (Slice slice : slices)
    {
      SliceNameEnum sliceName = slice.getSliceName();
      String filename = null;

      if (slice.isAdditionalSlice())
      {
        filename = generateAdjustFocusFileName(slice, reconstructionRegion);
      }
      else
      {
        filename = FileName.getInspectionImageName(reconstructionRegion.getName(), sliceName.getId());
      }

      Image image = XrayImageIoUtil.loadPngImage(imagesDir + File.separator + filename);
      reconImages.addImage(image, sliceName);
      image.decrementReferenceCount();
    }

    Assert.expect(reconImages.getNumberOfSlices() > 0);
    return reconImages;
  }

  /**
   * @author George A. David
   */
  public Set<String> getAdjustFocusImageNames(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    Set<String> imageNames = new HashSet<String>();

    for (ReconstructionRegion inspectionRegion : testProgram.getFilteredInspectionRegions())
    {
      for (Slice slice : inspectionRegion.getInspectedSlices())
      {
        SliceNameEnum sliceName = slice.getSliceName();
        String filename = null;
        if (slice.isAdditionalSlice())
        {
          filename = generateAdjustFocusFileName(slice, inspectionRegion);
        }
        else
        {
          filename = FileName.getInspectionImageName(inspectionRegion.getName(), sliceName.getId());
        }

        imageNames.add(filename);
      }
    }

    return imageNames;
  }

  /**
   * @author George A. David
   */
  public Set<String> getVerificationImageNames(String projectName, boolean isNeedCheckLatestProgram) throws DatastoreException
  {
    Set<String> imageNames = null;
    if (Project.isCurrentProjectLoaded())
    {
      Project project = Project.getCurrentlyLoadedProject();
      if (project.getName().equalsIgnoreCase(projectName))
      {
        boolean checkLatestProgram = project.isCheckLatestTestProgram();
        project.setCheckLatestTestProgram(isNeedCheckLatestProgram);
         
        imageNames = getVerificationImageNames(project.getTestProgram());
        
        project.setCheckLatestTestProgram(checkLatestProgram);
      }
      else
      {
        imageNames = readVerificationImageNames(projectName);
      }
    }
    else
    {
      imageNames = readVerificationImageNames(projectName);
    }
    Assert.expect(imageNames != null);

    return imageNames;
  }

  /**
   * @author George A. David
   */
  public Set<String> getVerificationImageNames(TestSubProgram subProgram)
  {
    Assert.expect(subProgram != null);

    Set<String> imageNames = new HashSet<String>();

    for (ReconstructionRegion inspectionRegion : subProgram.getVerificationRegions())
    {
      String filename = FileName.getVerificationImageName(inspectionRegion.getName());
      imageNames.add(filename);
    }
    return imageNames;
  }

  /**
   * @author George A. David
   */
  public Set<String> getVerificationImageNames(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    Set<String> imageNames = new HashSet<String>();

    for (ReconstructionRegion inspectionRegion : testProgram.getVerificationRegions())
    {
      String filename = FileName.getVerificationImageName(inspectionRegion.getName());
      imageNames.add(filename);
    }
    return imageNames;
  }

  /**
   * @author George A. David
   */
  private void saveVerificationImageNames(TestProgram testProgram, String verificationImageNamesProgramFullPath) throws DatastoreException
  {
    saveImageInfo(getVerificationImageNames(testProgram), verificationImageNamesProgramFullPath, false);
  }

  /**
   * @author George A. David
   */
  private void saveVerificationImageNames(TestSubProgram subProgram, String verificationImageNamesProgramFullPath) throws DatastoreException
  {
    saveImageInfo(getVerificationImageNames(subProgram), verificationImageNamesProgramFullPath, true);
  }

  /**
   * @author George A. David
   */
  public void saveImageInfo(Set<String> imageInfos, String imageNamesProgramFullPath, boolean append) throws DatastoreException
  {
    Assert.expect(imageInfos != null);

    FileWriterUtilAxi writer = new FileWriterUtilAxi(imageNamesProgramFullPath, append);
    try
    {
      writer.open();
      for (String imageInfo : imageInfos)
      {
        writer.writeln(imageInfo);
      }
    } finally
    {
      writer.close();
    }
  }

  /**
   * @author George A. David
   */
  public void saveProductionTuneDefectImageInfo(ImageSetData imageSetData, TestProgram testProgram, Collection<ReconstructionRegion> inspectionRegions) throws DatastoreException
  {
    Assert.expect(imageSetData != null);

    String inspectionImageInfoProgramFullPath;
    if (imageSetData.isOnlineTestDevelopment())
    {
      inspectionImageInfoProgramFullPath = FileName.getInspectionImageInfoProgramFullPathForOnlineTestDevelopment(imageSetData.getImageSetName());
    }
    else
    {
      inspectionImageInfoProgramFullPath = FileName.getInspectionImageInfoProgramFullPath(imageSetData.getProjectName(), imageSetData.getImageSetName());
    }

    saveImageInfo(getInspectionImageInfos(testProgram, inspectionRegions, false), inspectionImageInfoProgramFullPath, false);
  }

  /**
   * @author George A. David
   */
  public void saveInspectionImageInfo(ImageSetData imageSetData, TestProgram testProgram) throws DatastoreException
  {
    Assert.expect(imageSetData != null);

    String inspectionImageInfoProgramFullPath;
    if (imageSetData.isOnlineTestDevelopment())
    {
      inspectionImageInfoProgramFullPath = FileName.getInspectionImageInfoProgramFullPathForOnlineTestDevelopment(imageSetData.getImageSetName());
    }
    else
    {
      inspectionImageInfoProgramFullPath = FileName.getInspectionImageInfoProgramFullPath(imageSetData.getProjectName(), imageSetData.getImageSetName());
    }

    saveImageInfo(getInspectionImageInfos(testProgram), inspectionImageInfoProgramFullPath, false);
  }

  /**
   * @author George A. David
   */
  public void saveAdjustFocusImageNames(TestProgram testProgram) throws DatastoreException
  {
    Assert.expect(testProgram != null);

    String adjustFocusImageNamesProgramFullPath = FileName.getAdjustImageNamesProgramFullPath(testProgram.getProject().getName());

    Set<String> imageNames = getAdjustFocusImageNames(testProgram);

    saveImageInfo(imageNames, adjustFocusImageNamesProgramFullPath, false);
  }

  /**
   * @author George A. David
   */
  public void saveVerificationImageNames(TestProgram testProgram) throws DatastoreException
  {
    Assert.expect(testProgram != null);

    saveVerificationImageNames(testProgram, FileName.getVerificationImageNamesProgramFullPath(testProgram.getProject().getName()));
  }

  /**
   * @author George A. David
   */
  public void saveVerificationImageNames(TestSubProgram subProgram) throws DatastoreException
  {
    saveVerificationImageNames(subProgram, FileName.getVerificationImageNamesProgramFullPath(subProgram.getTestProgram().getProject().getName()));
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void save2DVerificationImageNames(TestProgram testProgram) throws DatastoreException
  {
    Assert.expect(testProgram != null);

    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    saveVerificationImageNames(testProgram, FileName.get2DVerificationImageNamesProgramFullPath(testProgram.getProject().getName()));
  }

  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void save2DVerificationImageNames(TestSubProgram subProgram) throws DatastoreException
  {
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    saveVerificationImageNames(subProgram, FileName.get2DVerificationImageNamesProgramFullPath(subProgram.getTestProgram().getProject().getName()));
  }

  /**
   * @author Scott Richardson
   */
  public Set<String> getVirtualLiveImageNames(TestProgram testProgram)
  {
    Set<String> imageNames = new HashSet<String>();

    for (ReconstructionRegion reconstructionRegion : testProgram.getVerificationRegions())
    {
      for (Slice slice : reconstructionRegion.getReconstructionSlices())
      {
        // A slice name indexed by <top>_<xCoordinate>_<yCoordinate>_<width>_<height>_<sliceHeight>
        String name = reconstructionRegion.getName() + "_" + slice.getFocusInstruction().getZHeightInNanoMeters();
        String filename = FileName.getVerificationImageName(name);

        imageNames.add(filename);
      }
    }
    return imageNames;
  }

  /**
   * @author George A. David
   */
  public Set<String> getInspectionImageInfo(ImageSetData imageSetData) throws DatastoreException
  {
    Assert.expect(imageSetData != null);

    if (imageSetData.isOnlineTestDevelopment())
    {
      return readImageNames(FileName.getInspectionImageInfoProgramFullPathForOnlineTestDevelopment(imageSetData.getImageSetName()));
    }
    else
    {
      return readImageNames(FileName.getInspectionImageInfoProgramFullPath(imageSetData.getProjectName(),
        imageSetData.getImageSetName()));
    }
  }

   /**
   * @author Jack Hwee
   * XCR1164 - fix image set comparability
   */
  public Set<String> getPreviousInspectionImageInfo(ImageSetData imageSetData) throws DatastoreException
  {
    Assert.expect(imageSetData != null);

    if (imageSetData.isOnlineTestDevelopment())
    {
      return readImageNames(FileName.getInspectionImageInfoProgramFullPathForOnlineTestDevelopment(imageSetData.getImageSetName()));
    }
    else
    {
      return readImageNames(FileName.getInspectionImageInfoProgramFullPath(imageSetData.getProjectName(),
        imageSetData.getImageSetName()));
    }
  }

  /**
   * @author George A. David
   */
  private Set<String> readInspectionImageInfo(String projectName) throws DatastoreException
  {
    String imageNamesPath = FileName.getInspectionImageInfoProgramFullPath(projectName);
    if (FileUtilAxi.exists(imageNamesPath))
    {
      return readImageNames(imageNamesPath);
    }
    else
    {
      return new HashSet<String>();
    }
  }

  /**
   * @author George A. David
   */
  private Set<String> readVerificationImageNames(String projectName) throws DatastoreException
  {
    String imageNamesPath = FileName.getVerificationImageNamesProgramFullPathInProjectDir(projectName);
    if (FileUtilAxi.exists(imageNamesPath))
    {
      return readImageNames(imageNamesPath);
    }
    else
    {
      return new HashSet<String>();
    }
  }

  /**
   * @author George A. David
   */
  private Set<String> readAdjustFocusImageNames(String projectName) throws DatastoreException
  {
    String imageNamesPath = FileName.getAdjustImageNamesProgramFullPath(projectName);
    if (FileUtilAxi.exists(imageNamesPath))
    {
      return readImageNames(imageNamesPath);
    }
    else
    {
      return new HashSet<String>();
    }
  }

  /**
   * @author George A. David
   */
  private Set<String> readImageNames(String inspectionImageNamesProgramFullPath) throws DatastoreException
  {
    Set<String> imageNames = new HashSet<String>();
    FileReaderUtilAxi reader = new FileReaderUtilAxi();
    try
    {
      reader.open(inspectionImageNamesProgramFullPath);

      while (reader.hasNextLine())
      {
        imageNames.add(reader.readNextLine());
      }
    } finally
    {
      reader.close();
    }

    return imageNames;
  }

  /**
   * @author George A. David
   */
  public void saveInspectionImageInfo(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    saveImageInfo(getInspectionImageInfos(project.getTestProgram()),
      FileName.getInspectionImageInfoProgramFullPath(project.getName()),
      false);
  }

  /**
   * @author George A. David
   */
  public void saveImageInfoToProjectDirectory(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(project.isTestProgramValidIgnoringAlignmentRegions());

    TestProgram testProgram = project.getTestProgramForAlignmentRegionsGeneration();
    testProgram.clearFilters();
    saveImageInfo(getInspectionImageInfos(testProgram),
      FileName.getInspectionImageInfoProgramFullPath(project.getName()),
      false);
    saveImageInfo(getVerificationImageNames(testProgram),
      FileName.getVerificationImageNamesProgramFullPathInProjectDir(project.getName()),
      false);
  }

  /**
   * @author George A. David
   */
  public boolean isCompatible(Project project, ImageSetData imageSetData) throws DatastoreException
  {
    Set<String> imageSetInspectionImageInfos = getInspectionImageInfo(imageSetData);
    imageSetInspectionImageInfos.removeAll(getInspectionImageInfos(project.getTestProgram()));

    //Jack Hwee XCR1164 - fix image set comparability
    imageSetInspectionImageInfos.removeAll(getPreviousInspectionImageInfos(project.getTestProgram()));

    return imageSetInspectionImageInfos.isEmpty();
  }

  /**
   * @author Scott Richardson
   */
  public boolean hasVirtualLiveImage(String projectName, Slice slice)
  {
    String dirName = Directory.getXrayVirtualLiveImagesDir(projectName);

    // slice name indexed by <top>_<xCoordinate>_<yCoordinate>_<width>_<height>_<sliceHeight>
    String name = slice.getAssociatedReconstructionRegion().getName() + "_" + slice.getFocusInstruction().getZHeightInNanoMeters();
    String fileName = FileName.getVerificationImageName(name);

    if (FileUtilAxi.exists(dirName + File.separator + fileName))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * @author George A. David
   */
  public boolean hasValidVerificationImages(Project project, boolean isNeedCheckLatestProgram)
  {
    Assert.expect(project != null);

    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    return hasValidVerificationImages(project.getName(), isNeedCheckLatestProgram, project.getTestProgram().isManual2DAlignmentInProgress());
  }

  /**
   * @author George A. David
   */
  public boolean hasValidVerificationImages(TestSubProgram subProgram)
  {
    boolean hasValidVerificationImages = false;
    try
    {
      String projectName = subProgram.getTestProgram().getProject().getName();
      // if the all the images on disk  are
      // used with the current test program, then we do not need new
      // verification images
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      
      String verificationImageNamesProgramFullPath = null;
      
      if (subProgram.getTestProgram().isManual2DAlignmentInProgress())
        verificationImageNamesProgramFullPath = FileName.get2DVerificationImageNamesProgramFullPath(projectName);
      else
        verificationImageNamesProgramFullPath = FileName.getVerificationImageNamesProgramFullPath(projectName);
      
      if (FileUtilAxi.exists(verificationImageNamesProgramFullPath))
      {
        Set<String> usedInspectionImageNames = getVerificationImageNames(subProgram);
        Set<String> verificationImageNamesOnDisk = readImageNames(verificationImageNamesProgramFullPath);
        usedInspectionImageNames.removeAll(verificationImageNamesOnDisk);
        if (usedInspectionImageNames.isEmpty())
        {
          hasValidVerificationImages = true;
        }
      }
    }
    catch (DatastoreException dex)
    {
      // do nothing
    }

    return hasValidVerificationImages;
  }

  /**
   * @author George A. David
   */
  public boolean hasValidVerificationImages(String projectName, boolean isNeedCheckLatestProgram, boolean isUsing2DAlignment)
  {
    Assert.expect(projectName != null);

    boolean hasValidVerificationImages = false;
    try
    {
      // if the all the images on disk  are
      // used with the current test program, then we do not need new
      // verification images
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      String verificationImageNamesProgramFullPath = null;
      
      if (isUsing2DAlignment)
        verificationImageNamesProgramFullPath = FileName.get2DVerificationImageNamesProgramFullPath(projectName);
      else
        verificationImageNamesProgramFullPath = FileName.getVerificationImageNamesProgramFullPath(projectName);

      if (FileUtilAxi.exists(verificationImageNamesProgramFullPath))
      {
        Set<String> usedInspectionImageNames = getVerificationImageNames(projectName, isNeedCheckLatestProgram);
        Set<String> verificationImageNamesOnDisk = readImageNames(verificationImageNamesProgramFullPath);
        
//        verificationImageNamesOnDisk.removeAll(usedInspectionImageNames);
//        if (verificationImageNamesOnDisk.isEmpty())
//        {
//          hasValidVerificationImages = true;
//        }
        // Siew Yeng - XCR1637 
        // There is the case that verificationImagesOnDisk will contain images other than usedInspectionImageNames, 
        // if this happened, hasValidVerificationImages will always return false. To handle this condition, 
        // check if verificationImageNamesOnDisk contains all the usedInspectionImageNames images.
        if(verificationImageNamesOnDisk.containsAll(usedInspectionImageNames))
        {
          hasValidVerificationImages = true;
        }
      }
    }
    catch (DatastoreException dex)
    {
      // do nothing
    }

    return hasValidVerificationImages;

  }

  /**
   * @author George A. David
   */
  public boolean doAdjustFocusImagesExist(String projectName)
  {
    if (FileUtil.exists(Directory.getAdjustFocusImagesDir(projectName)))
    {
      File file = new File(Directory.getAdjustFocusImagesDir(projectName));
      String[] files = file.list();
      if (files != null && files.length > 0)
      {
        return true;
      }
    }

    return false;
  }

  /**
   * @author George A. David
   */
  public boolean doesCompatibleAdjustFocusImagesExist(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (doAdjustFocusImagesExist(project.getName()) == false)
    {
      return false;
    }

    Set<String> currentImageNames = getAdjustFocusImageNames(project.getTestProgram());
    currentImageNames.removeAll(readAdjustFocusImageNames(project.getName()));

    return currentImageNames.isEmpty();
  }

  /**
   * @author Aimee Ong
   * @author Ronald Lim
   * @author Lim, Seng Yew
   */
  public Set<String> getChecksumValues(ReconstructionRegion region, int panelThicknessInNanos, boolean useOnlyCreatedSlices)
  {
    Assert.expect(region != null);
    Set<String> imageInfos = new HashSet<String>();
    String predictiveSliceHeightInfo = null;
    boolean predictiveSliceHeightOn = false;
    String userGainInfo = null;
    String stageSpeedInfo = null;
    boolean isAllComponentSameUserGain = false;
    boolean isAllComponentSameStageSpeed = false;
    String lowMagnificationInfo = null;

    SystemTypeEnum systemType = XrayTester.getSystemType();
    boolean includeSystemTypeInfo = false;

    if (systemType.equals(SystemTypeEnum.STANDARD) == false)
    {
      includeSystemTypeInfo = true;
    }

    // do NOT use Adler32 here since Adler32 is not very good with small strings.
    // we actually would see cases where all images should be invalid, but using
    // adler32 we would get something like 0.28% still compatible.
    // CRC32 does not have this problem
    CRC32 checksumUtil = new CRC32();

    for (Subtype subtype : region.getSubtypes())
    {
      //Ngie Xing, image set compatibility for different low magnification
      if (XrayTester.isS2EXEnabled())
      {
        if (subtype.getSubtypeAdvanceSettings().getMagnificationType() == MagnificationTypeEnum.LOW)
        {
          lowMagnificationInfo = Config.getSystemCurrentLowMagnification();
        }
      }
      
      predictiveSliceHeightOn = subtype.usePredictiveSliceHeight();
      if (predictiveSliceHeightOn == false)
      {
        break;
      }
      String predictiveSliceHeight = String.valueOf(subtype.usePredictiveSliceHeight());
      predictiveSliceHeightInfo = predictiveSliceHeightInfo + " " + predictiveSliceHeight;
    }

    isAllComponentSameUserGain = region.getBoard().getPanel().getPanelSettings().isAllComponentSameUserGain();
    if(isAllComponentSameUserGain == false)
    {
      userGainInfo = region.getComponent().getComponentType().getUserGain().toString();
    }
    
    isAllComponentSameStageSpeed = region.getBoard().getPanel().getPanelSettings().isAllComponentSameStageSpeed();
    if(isAllComponentSameStageSpeed == false && region.getSubtypes().iterator().next().getSubtypeAdvanceSettings().getStageSpeedList().isEmpty() == false)
    {
      if (region.getSubtypes().iterator().next().getSubtypeAdvanceSettings().getStageSpeedList().size() > 1)
        stageSpeedInfo = region.getSubtypes().iterator().next().getSubtypeAdvanceSettings().getStageSpeedList().toString();
      else
        stageSpeedInfo = region.getSubtypes().iterator().next().getSubtypeAdvanceSettings().getStageSpeedList().iterator().next().toString();
    }

    for (FocusGroup focusGroup : region.getFocusGroups())
    {
      FocusSearchParameters searchParams = focusGroup.getFocusSearchParameters();
      FocusRegion focusRegion = searchParams.getFocusRegion();
      PanelRectangle rect = focusRegion.getRectangleRelativeToPanelInNanometers();

         String focusGroupInfo = focusGroup.getRelativeZoffsetFromSurfaceInNanoMeters() + " "
        + focusGroup.isSingleSidedRegionOfBoard() + " "
        + searchParams.getFocusProfileShape().getDescription() + " "
        + searchParams.getGradientWeightingMagnitudePercentage() + " "
        // Change orientation should not invalidate image set.
        // HorizontalSharpnessComponentPercentage depends on orientation angle value, have to disable together.
        // Just by changing oerientation, time is wasted to regenerate image sets for learning.
        // Modified by Seng Yew on 15-Dec-2010 during Flex Doumen eval
//        + searchParams.getGradientWeightingOrientationInDegrees() + " "
//        + searchParams.getHorizontalSharpnessComponentPercentage() + " "
        + searchParams.getZCurveWidthInNanoMeters() + " "
        + rect.getMinX() + " " + rect.getMinY() + " "
        + rect.getWidth() + " " + rect.getHeight();
 
      // we always ignore the surface modeling slice, only the IRPs use this
      for (Slice slice : focusGroup.getSlices())
      {
        if (useOnlyCreatedSlices = slice.createSlice() == false)
        {
          continue;
        }

        if (slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION))
        {
          FocusInstruction instruction = slice.getFocusInstruction();
          SliceNameEnum sliceName = slice.getSliceName();
          String filename = null;
          
          if(Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType())
            filename = region.getName()+ "_" + sliceName.getId();
          else
            filename = FileName.getInspectionImageName(region.getName(), sliceName.getId());

          String info = filename + " " + panelThicknessInNanos + " "
            + instruction.getFocusMethod().getDescription() + " "
            + instruction.getPercentValue() + " "
            + instruction.getZHeightInNanoMeters() + " "
            + instruction.getZOffsetInNanoMeters() + " "
            + focusGroupInfo;

          if (predictiveSliceHeightOn)
          {
            info += " " + predictiveSliceHeightInfo;
          }
          if(isAllComponentSameUserGain == false)
          {
            info += " " + userGainInfo;
          }
          //Ngie Xing
          if (XrayTester.isS2EXEnabled())
          {
            if (lowMagnificationInfo != null)
              info += " " + lowMagnificationInfo;
          }
          if(isAllComponentSameStageSpeed == false)
          {
            info += " " + stageSpeedInfo;
          }
          checksumUtil.reset();
          checksumUtil.update(info.getBytes());
          if (includeSystemTypeInfo)
          {
            checksumUtil.update(systemType.getName().getBytes());
          }
          imageInfos.add(String.valueOf(checksumUtil.getValue()));
        }
      }
    }

    return imageInfos;
  }

   /**
   * @author Jack Hwee XCR1164 - fix image set comparability
   */
  public Set<String> getPreviousChecksumValues(ReconstructionRegion region, int panelThicknessInNanos, boolean useOnlyCreatedSlices)
  {
    Assert.expect(region != null);
    Set<String> imageInfos = new HashSet<String>();
    String predictiveSliceHeightInfo = null;
    boolean predictiveSliceHeightOn = false;

    SystemTypeEnum systemType = XrayTester.getSystemType();
    boolean includeSystemTypeInfo = false;

    if (systemType.equals(SystemTypeEnum.STANDARD) == false)
    {
      includeSystemTypeInfo = true;
    }

    // do NOT use Adler32 here since Adler32 is not very good with small strings.
    // we actually would see cases where all images should be invalid, but using
    // adler32 we would get something like 0.28% still compatible.
    // CRC32 does not have this problem
    CRC32 checksumUtil = new CRC32();

    for (Subtype subtype : region.getSubtypes())
    {
      predictiveSliceHeightOn = subtype.usePredictiveSliceHeight();
      if (predictiveSliceHeightOn == false)
      {
        break;
      }
      String predictiveSliceHeight = String.valueOf(subtype.usePredictiveSliceHeight());
      predictiveSliceHeightInfo = predictiveSliceHeightInfo + " " + predictiveSliceHeight;
    }

    for (FocusGroup focusGroup : region.getFocusGroups())
    {
      FocusSearchParameters searchParams = focusGroup.getFocusSearchParameters();
      FocusRegion focusRegion = searchParams.getFocusRegion();
      PanelRectangle rect = focusRegion.getRectangleRelativeToPanelInNanometers();

         String focusGroupInfo = focusGroup.getRelativeZoffsetFromSurfaceInNanoMeters() + " "
        + focusGroup.isSingleSidedRegionOfBoard() + " "
        + searchParams.getFocusProfileShape().getDescription() + " "
        + searchParams.getGradientWeightingMagnitudePercentage() + " "
        // Change orientation should not invalidate image set.
        // HorizontalSharpnessComponentPercentage depends on orientation angle value, have to disable together.
        // Just by changing oerientation, time is wasted to regenerate image sets for learning.
        // Modified by Seng Yew on 15-Dec-2010 during Flex Doumen eval
        + searchParams.getGradientWeightingOrientationInDegrees() + " "
        + searchParams.getHorizontalSharpnessComponentPercentage() + " "
        + searchParams.getZCurveWidthInNanoMeters() + " "
        + rect.getMinX() + " " + rect.getMinY() + " "
        + rect.getWidth() + " " + rect.getHeight();

      // we always ignore the surface modeling slice, only the IRPs use this
      for (Slice slice : focusGroup.getSlices())
      {
        if (useOnlyCreatedSlices = slice.createSlice() == false)
        {
          continue;
        }

        if (slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION))
        {
          FocusInstruction instruction = slice.getFocusInstruction();
          SliceNameEnum sliceName = slice.getSliceName();
          String filename = FileName.getInspectionImageName(region.getName(), sliceName.getId());
          String info = filename + " " + panelThicknessInNanos + " "
            + instruction.getFocusMethod().getDescription() + " "
            + instruction.getPercentValue() + " "
            + instruction.getZHeightInNanoMeters() + " "
            + instruction.getZOffsetInNanoMeters() + " "
            + focusGroupInfo;

          if (predictiveSliceHeightOn)
          {
            info += " " + predictiveSliceHeightInfo;
          }
          checksumUtil.reset();
          checksumUtil.update(info.getBytes());
          if (includeSystemTypeInfo)
          {
            checksumUtil.update(systemType.getName().getBytes());
          }
          imageInfos.add(String.valueOf(checksumUtil.getValue()));
        }
      }
    }

    return imageInfos;
  }

  /**
   * @author Chong, Wei Chin
   */
  private Collection<String> getInspectionImageFileNames(String projectName) throws DatastoreException
  {
    Collection <String> imageFileNames = null;
    if (Project.isCurrentProjectLoaded())
    {
      Project project = Project.getCurrentlyLoadedProject();
      if (project.getName().equalsIgnoreCase(projectName))
      {
        imageFileNames = getInspectionImageFileNames(project.getTestProgram().getAllInspectionRegions());
      }
      else
      {
        imageFileNames = readInspectionImageFileNames(projectName);
      }
    }
    else
    {
      imageFileNames = readInspectionImageFileNames(projectName);
    }
    Assert.expect(imageFileNames != null);

    return imageFileNames;
  }

  /**
   * @author Chog, Wei Chin
   */
  public Set<String> getInspectionImageFileNames(Collection<ReconstructionRegion> inspectionRegions)
  {
    Assert.expect(inspectionRegions != null);

    Set<String> imageFileNames = new HashSet<String>();

    for (ReconstructionRegion inspectionRegion : inspectionRegions)
    {
//      reconstructionRegionName + "_" + sliceId
      for(Slice slice : inspectionRegion.getSlices())
        imageFileNames.add(inspectionRegion.getName() + "_" + slice.getSliceName().getId());
    }
    return imageFileNames;
  }

  /**
   * @author Chong Wei Chin
   */
  private Collection <String> readInspectionImageFileNames(String projectName) throws DatastoreException
  {
    String imageNamesPath = FileName.getInspectionImageInfoProgramFullPath(projectName);
    if (FileUtilAxi.exists(imageNamesPath))
    {
      return FileUtil.listAllFileNamesWithoutExtensionInDirectory(imageNamesPath);
    }
    else
    {
      return new HashSet<String>();
    }
  }

  /**
   * @return the _ignoreCompatibilityCheck
   * @author Wei Chin
   */
  public static boolean isIgnoreCompatibilityCheck()
  {
    return _ignoreCompatibilityCheck;
  }

  /**
   * @param aIgnoreCompatibilityCheck the _ignoreCompatibilityCheck to set
   * @author Wei Chin
   */
  public static void setIgnoreCompatibilityCheck(boolean aIgnoreCompatibilityCheck)
  {
    _ignoreCompatibilityCheck = aIgnoreCompatibilityCheck;
  }

  /**
   * @authror Siew Yeng
   */
  public BufferedImage stitchOfflineComponentImage(Component component, 
          SliceNameEnum sliceNameEnum,
          List<ReconstructionRegion> reconstructionRegionsList, 
          ImageSetData imageSetData,
          boolean enhanceImage)
  { 
    BufferedImage componentImage = null;
    java.awt.Graphics2D componentGraphic;
    
    int originX = Integer.MAX_VALUE;
    int originY = Integer.MIN_VALUE; 
    int minY = Integer.MAX_VALUE;
    int maxX = Integer.MIN_VALUE; 
    
    int inspectionImageXInMils;
    int inspectionImageYInMils;
    int inspectionImageXInNanoMeters;
    int inspectionImageYInNanoMeters;
    int inspectionImageXInPixels;
    int inspectionImageYInPixels;
    int imageWidthInMils;
    int imageHeightInMils;
    int imageWidthInNanoMeters;
    int imageHeightInNanoMeters;
    int imageWidthInPixels;
    int imageHeightInPixels;   

    int newOriginX;
    int newOriginY;
    int width;
    int height;
    
    int sliceIndex;
    
    boolean isImageGeneratedWithHighMag = false;
    
    String inspectionImageFileName;
 
    // find the origin for stitching image
    for (ReconstructionRegion region : reconstructionRegionsList)
    {
      String splitName[] = region.getName().split("_");
      // check if image is high mag
      if (splitName[1].equalsIgnoreCase("high"))
      {
        isImageGeneratedWithHighMag = true;
        newOriginX = Integer.parseInt(splitName[2]);
        newOriginY = Integer.parseInt(splitName[3]);
        width = Integer.parseInt(splitName[4]);
        height = Integer.parseInt(splitName[5]);
      }
      else
      {
        isImageGeneratedWithHighMag = false;
        newOriginX = Integer.parseInt(splitName[1]);
        newOriginY = Integer.parseInt(splitName[2]);
        width = Integer.parseInt(splitName[3]);
        height = Integer.parseInt(splitName[4]);
      }

      // find minimum X coordinate
      if (newOriginX < originX)
        originX = newOriginX;

      //find maximum Y coordinate
      if (newOriginY + height > originY)
        originY = newOriginY + height;

      // find minimum Y coordinate
      if (newOriginY < minY)
        minY = newOriginY;

      //find maximum X coordinate
      if (newOriginX + width > maxX)
        maxX = newOriginX + width;
    }      

    // Get image size in pixels
    imageWidthInMils = maxX - originX;             
    imageWidthInNanoMeters = (int) MathUtil.convertMilsToNanoMeters(imageWidthInMils);       
    imageHeightInMils = originY - minY; 
    imageHeightInNanoMeters = (int) MathUtil.convertMilsToNanoMeters(imageHeightInMils);

    if (isImageGeneratedWithHighMag)
    {
      imageWidthInPixels = (int)Math.ceil(imageWidthInNanoMeters * 1f / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel());
      imageHeightInPixels = (int)Math.ceil(imageHeightInNanoMeters * 1f / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel());   
    }
    else
    {
      imageWidthInPixels = (int)Math.ceil(imageWidthInNanoMeters * 1f / MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
      imageHeightInPixels = (int)Math.ceil(imageHeightInNanoMeters * 1f / MagnificationEnum.NOMINAL.getNanoMetersPerPixel());   
    }

    // Stitch the images by looking thru file names
    componentImage = new BufferedImage(imageWidthInPixels, imageHeightInPixels, BufferedImage.TYPE_BYTE_GRAY);
    componentGraphic = componentImage.createGraphics();

    sliceIndex = sliceNameEnum.getId(); 

    for(ReconstructionRegion reconstructionRegion : reconstructionRegionsList)
    {
      inspectionImageFileName = reconstructionRegion.getName() + "_" + sliceIndex;                                 
      
      ReconstructedImages reconstructedImages = null;
      try
      {
        reconstructedImages = loadInspectionImages(imageSetData, reconstructionRegion);
        if (enhanceImage)
          reconstructedImages.ensureInspectedSlicesAreReadyForInspection();
      }
      catch (DatastoreException de)
      {
        try 
        {               
          _projectErrorLogUtil.log(imageSetData.getProjectName(), "Cannot find image file for component stitching: Component: ", component.getReferenceDesignator() + "  Slice: " + sliceIndex);
        } 
        catch (XrayTesterException ex) 
        {
          System.out.println("Cannot save log in project error log.");
        } 
      }
      
      
      String splitInspectionImageName[] = inspectionImageFileName.split("_");

      if(isImageGeneratedWithHighMag)
      {
        inspectionImageXInMils = originX - Integer.parseInt(splitInspectionImageName[2]);
        inspectionImageYInMils = originY - (Integer.parseInt(splitInspectionImageName[3]) + Integer.parseInt(splitInspectionImageName[5]));

        // Change to positive value if the X or Y is negative value
        if(inspectionImageXInMils < 0)
          inspectionImageXInMils = -(inspectionImageXInMils);
        if(inspectionImageYInMils < 0)
          inspectionImageYInMils = -(inspectionImageYInMils);

        // Convert x and y from mils back to pixel unit
        inspectionImageXInNanoMeters = (int) MathUtil.convertMilsToNanoMeters(inspectionImageXInMils);
        inspectionImageYInNanoMeters = (int) MathUtil.convertMilsToNanoMeters(inspectionImageYInMils);

        inspectionImageXInPixels = (int)Math.ceil(inspectionImageXInNanoMeters * 1f / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel());
        inspectionImageYInPixels = (int)Math.ceil(inspectionImageYInNanoMeters * 1f / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel());
      }
      else
      {
        inspectionImageXInMils = originX - Integer.parseInt(splitInspectionImageName[1]);
        inspectionImageYInMils = originY - (Integer.parseInt(splitInspectionImageName[2]) + Integer.parseInt(splitInspectionImageName[4]));

        // Change to positive value if the X or Y is negative value
        if(inspectionImageXInMils < 0)
          inspectionImageXInMils = -(inspectionImageXInMils);
        if(inspectionImageYInMils < 0)
          inspectionImageYInMils = -(inspectionImageYInMils);

        // Convert x and y from mils back to pixel unit
        inspectionImageXInNanoMeters = (int) MathUtil.convertMilsToNanoMeters(inspectionImageXInMils);
        inspectionImageYInNanoMeters = (int) MathUtil.convertMilsToNanoMeters(inspectionImageYInMils);

        inspectionImageXInPixels = (int)Math.ceil(inspectionImageXInNanoMeters * 1f / MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
        inspectionImageYInPixels = (int)Math.ceil(inspectionImageYInNanoMeters * 1f / MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
      }
    
      if(reconstructedImages != null)
      {
        if(reconstructedImages.hasReconstructedSlice(sliceNameEnum))
        {
          Image enhancedImage = reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage();
          ImageEnhancement.autoEnhanceContrastInPlace(enhancedImage);
          componentGraphic.drawImage(enhancedImage.getBufferedImage(), inspectionImageXInPixels, inspectionImageYInPixels, null); 
        }

        reconstructedImages.decrementReferenceCount();
      }
    }
    return componentImage;
  }
  
  /**
   * Wei Chin comment: 
   * TODO: This function should move to GUI layer --> it can be a generic function 
   * Stitch whole board verification images for Virtual Live
   * @author bee-hoon.goh
   * Edited by Kee Chin Seong - If the folder contains the VerificationImages_top / bottom / both
   *                            ignore those files and continue stitching
   */
  public BufferedImage stitchVerificationImage(String projectName, boolean isCadCreatorEnv)
  { 
    BufferedImage panelImage = null;
    java.awt.Graphics2D panelGraphic = null;
    BufferedImage resizePanelImage = null;
    java.awt.Graphics2D resizePanelGraphic = null;
    BufferedImage topResizePanelImage = null;
    BufferedImage bottomResizePanelImage = null;
    
    int originX = Integer.MAX_VALUE;
    int originY = Integer.MIN_VALUE; 
    int minY = Integer.MAX_VALUE;
    int maxX = Integer.MIN_VALUE; 
    
    int verificationImageXInMils;
    int verificationImageYInMils;
    int verificationImageXInNanoMeters;
    int verificationImageYInNanoMeters;
    int verificationImageXInPixels = 0;
    int verificationImageYInPixels = 0;
    int imageWidthInMils;
    int imageHeightInMils;
    int imageWidthInNanoMeters;
    int imageHeightInNanoMeters;
    int imageWidthInPixels = 0;
    int imageHeightInPixels = 0;   
    int resizeImageWidthInPixels = 0;
    int resizeImageHeightInPixels = 0;

    int newOriginX;
    int newOriginY;
    int width;
    int height;
    
    List<String> boardSideList = new ArrayList<String>();
    boardSideList.add("bottom");
    boardSideList.add("top");
    
    String virtualLiveVerificationImagesDir = "";
    if(isCadCreatorEnv == false)
      virtualLiveVerificationImagesDir = Directory.getVirtualLiveVerificationImagesDir(projectName);
    else
      virtualLiveVerificationImagesDir = Directory.getXrayVerificationImagesDirectory();
    
    String verificationImagesDir = Directory.getXrayVerificationImagesDir(projectName);
    File verificationImagesFolder = new File(verificationImagesDir);
    if (!verificationImagesFolder.exists()) 
      verificationImagesFolder.mkdir(); 
    File[] listOfVerificationImageFiles = verificationImagesFolder.listFiles();

    if(listOfVerificationImageFiles == null)
      return panelImage;
    
    for (File file : listOfVerificationImageFiles) 
    { 
      if (file.isFile()) 
      {
        int pos = file.getName().lastIndexOf(".");
        String fileNameWithoutExt[] = file.getName().substring(0, pos).split("_");
      
        if (file.getName().substring(pos + 1).equalsIgnoreCase("jpg") == false)
          continue;
        else if(file.getName().startsWith("verificationImage_") == true)
          continue;
        
        // sample image name: right_bottom_-50_-50_262_307.jpg
        newOriginX = Integer.parseInt(fileNameWithoutExt[2]);
        newOriginY = Integer.parseInt(fileNameWithoutExt[3]);
        width = Integer.parseInt(fileNameWithoutExt[4]);
        height = Integer.parseInt(fileNameWithoutExt[5]);
        
        // find minimum X coordinate
        if (newOriginX < originX)
          originX = newOriginX;

        //find maximum Y coordinate
        if (newOriginY + height > originY)
          originY = newOriginY + height;

        // find minimum Y coordinate
        if (newOriginY < minY)
          minY = newOriginY;

        //find maximum X coordinate
        if (newOriginX + width > maxX)
          maxX = newOriginX + width;
      }      

      // Get image size in pixels
      imageWidthInMils = maxX - originX;             
      imageWidthInNanoMeters = (int) MathUtil.convertMilsToNanoMeters(imageWidthInMils);       
      imageHeightInMils = originY - minY; 
      imageHeightInNanoMeters = (int) MathUtil.convertMilsToNanoMeters(imageHeightInMils);

      imageWidthInPixels = (int)Math.ceil(imageWidthInNanoMeters * 1f / MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
      imageHeightInPixels = (int)Math.ceil(imageHeightInNanoMeters * 1f / MagnificationEnum.NOMINAL.getNanoMetersPerPixel()); 
    }
    
    panelImage = new BufferedImage(imageWidthInPixels, imageHeightInPixels, BufferedImage.TYPE_BYTE_GRAY);
    panelGraphic = panelImage.createGraphics();

    int index = 0;
    for(String boardSide : boardSideList)
    {
      for (File file : listOfVerificationImageFiles) 
      {
        BufferedImage[] image = new BufferedImage[verificationImagesFolder.listFiles().length];

        if (file.isFile()) 
        {
          int spos = file.getName().lastIndexOf(".");
          String sfileNameWithoutExt[] = file.getName().substring(0, spos).split("_");

          if (file.getName().substring(spos + 1).equalsIgnoreCase("jpg") == false)
            continue;
          else if(file.getName().startsWith("verificationImage_") == true)
            continue;

            if(boardSide.equalsIgnoreCase(sfileNameWithoutExt[1]))
            {
              verificationImageXInMils = originX - Integer.parseInt(sfileNameWithoutExt[2]);
              verificationImageYInMils = originY - (Integer.parseInt(sfileNameWithoutExt[3]) + Integer.parseInt(sfileNameWithoutExt[5]));

              // Change to positive value if the X or Y is negative value
              if(verificationImageXInMils < 0)
                verificationImageXInMils = -(verificationImageXInMils);
              if(verificationImageYInMils < 0)
                verificationImageYInMils = -(verificationImageYInMils);

              // Convert x and y from mils back to pixel unit
              verificationImageXInNanoMeters = (int) MathUtil.convertMilsToNanoMeters(verificationImageXInMils);
              verificationImageYInNanoMeters = (int) MathUtil.convertMilsToNanoMeters(verificationImageYInMils);

              verificationImageXInPixels = (int)Math.ceil(verificationImageXInNanoMeters * 1f / MagnificationEnum.NOMINAL.getNanoMetersPerPixel());
              verificationImageYInPixels = (int)Math.ceil(verificationImageYInNanoMeters * 1f / MagnificationEnum.NOMINAL.getNanoMetersPerPixel());

              try 
              {
                image[index] = ImageIO.read(file);
              } 
              catch (IOException ex) 
              {
                System.out.println("Error Reading File: " + ex);
              }
            }
            
            panelGraphic.drawImage(image[index], verificationImageXInPixels, verificationImageYInPixels, null);                 
        }
        index++;
      }
      
      // Resize the image to around 1500 pixels    
      int scale = imageWidthInPixels / 1500;
      resizeImageWidthInPixels = imageWidthInPixels / scale;
      resizeImageHeightInPixels = imageHeightInPixels / scale;
      if (imageWidthInPixels > 1500 || imageHeightInPixels > 1500)  
      {
        resizePanelImage = new BufferedImage(resizeImageWidthInPixels, resizeImageHeightInPixels, BufferedImage.TYPE_BYTE_GRAY);
        resizePanelGraphic = resizePanelImage.createGraphics();
        resizePanelGraphic.drawImage(panelImage, 0, 0, resizeImageWidthInPixels, resizeImageHeightInPixels, null);
        resizePanelGraphic.setRenderingHint(java.awt.RenderingHints.KEY_INTERPOLATION, java.awt.RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        resizePanelGraphic.setRenderingHint(java.awt.RenderingHints.KEY_RENDERING, java.awt.RenderingHints.VALUE_RENDER_QUALITY);
      }
      
      // Testing save top and bottom images
      File virtualLiveVerificationImagesFolder = new File(virtualLiveVerificationImagesDir);
      if (!virtualLiveVerificationImagesFolder.exists()) 
        virtualLiveVerificationImagesFolder.mkdir(); 
      
      File outputfile;
      if(isCadCreatorEnv == false)
         outputfile = new File(virtualLiveVerificationImagesDir + File.separator + _verificationImage + boardSide + ".jpg");
      else
         outputfile = new File(verificationImagesDir + File.separator + _verificationImage + boardSide + ".jpg"); 
      try 
      {
        ImageIO.write(resizePanelImage, "jpg", outputfile);
      } 
      catch (IOException ex) 
      {
        System.out.println("Unable to write verificationImage_" + boardSide + ".jpg : " + ex );
      }
    
      // Identify Top side image and Bottom side image
      if(boardSide.equalsIgnoreCase("top"))
        topResizePanelImage = resizePanelImage;
      else if(boardSide.equalsIgnoreCase("bottom"))
        bottomResizePanelImage = resizePanelImage;

      // Reset the index
      index = 0;
    }// End of the for loop (board side)
     
    // Testing save blend image
    BufferedImage blendPanelImage = Arithmetic.blend(topResizePanelImage, bottomResizePanelImage, 0.5);
    File outputfile;
    if(isCadCreatorEnv == false)
        outputfile = new File(virtualLiveVerificationImagesDir + File.separator + _verificationImageBothSide);
    else
        outputfile = new File(verificationImagesDir +  File.separator + _verificationImageBothSide);
    try 
    {
      ImageIO.write(blendPanelImage, "jpg", outputfile);
    } 
    catch (IOException ex) 
    {
      System.out.println("Unable to write verification_both.jpg : " + ex);
    }
    
    return panelImage;
  }
  /**
   * @author Chnee Khang Wah
   * @comment we want to exclude some images such as diagnostic and 2.5 from image enhancement.
   * @param rawImages
   * @return 
   */
  public Map<SliceNameEnum, Image> excludeUnnecessaryImageFromEnhancement(Map<SliceNameEnum, Image> rawImages)
  {
    Assert.expect(rawImages != null);
    
    Map<SliceNameEnum, Image> filteredImages = new HashMap<SliceNameEnum, Image>();
    
    for (Map.Entry<SliceNameEnum, Image> entry : rawImages.entrySet())
    {
      if(entry.getKey().isMultiAngleSlice() || // 2.5d images
         EnumToUniqueIDLookup.getInstance().isDiagnosticSlice(entry.getKey()) || // Diagnostic Slice
         EnumToUniqueIDLookup.getInstance().isEnhancedImageSlice(entry.getKey()))   
      {
        // do nothing
      }
      else
      {
        filteredImages.put(entry.getKey(), entry.getValue());
      }
    }
    Assert.expect(filteredImages.isEmpty()==false);
    
    return filteredImages;
  }
  
  /**
   * @author Ooi Yi Fong
   */
  public void setPrevProjectHighMagnification(String highMagnification)
  {
    Assert.expect(highMagnification != null);
    _prevhighMagnification = highMagnification;
  }

  /**
   * @author Ooi Yi Fong
   */
  public String getPrevProjectHighMagnification()
  {
    return _prevhighMagnification;
  }

  /**
   * @author Ooi Yi Fong
   */
  public void setPrevProjectLowMagnification(String lowMagnification)
  {
    Assert.expect(lowMagnification != null);
    _prevlowMagnification = lowMagnification;
  }

  /**
   * @author Ooi Yi Fong
   */
  public String getPrevProjectLowMagnification()
  {
    return _prevlowMagnification;
  }
}
