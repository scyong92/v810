package com.axi.v810.business;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.util.*;

/**
 * This class supplies the following functionality
 * to use on the project database.
 * <ul>
 *   <li>add a project to the database
 *   <li>determine if a project exists in the database
 *   <li>get the database version number of a project in the database
 *   <li>get a project from the database
 *   <li>delete a project
 *</ul>
 * @author George A. David
 */
public class ProjectDatabase
{
  public static ProjectSummaryReader _projectSummaryReader;
  public static Config _config = Config.getInstance();
  
  private static boolean _continueWithoutDatabase = false;

  /**
   * @author George A. David
   */
  public static void addProject(String projectName) throws DatastoreException
  {
    addProject(projectName, "");
  }
  /**
   * add a project to the database
   *
   * @param projectName the name of the projec to add to the database.
   * @author George A. David
   */
  public static void addProject(String projectName, String databaseComment) throws DatastoreException
  {
    Assert.expect(projectName != null);

    int databaseVersion = getNewVersionNumber(projectName);

    createLocalTemporaryDatabaseFile(projectName);

    checkDiskSpaceInDatabase(projectName);

    transferToDatabase(projectName, databaseVersion);

    saveDatabaseComment(projectName, databaseComment, databaseVersion);

    deleteLocalTemporaryDatabaseFile(projectName);
  }

  /**
   * @author George A. David
   */
  private static void saveDatabaseComment(String projectName, String databaseComment, int databaseVersion) throws DatastoreException
  {
    FileWriterUtilAxi writer = new FileWriterUtilAxi(FileName.getProjectDatabaseCommentFileFullPath(projectName, databaseVersion), false);
    writer.open();
    writer.writeln(databaseComment);
    writer.close();
  }

  /**
   * @author George A. David
   */
  private static String readDatabaseComment(String projectName, int databaseVersion) throws DatastoreException
  {
    return FileReaderUtilAxi.readFileContents(FileName.getProjectDatabaseCommentFileFullPath(projectName, databaseVersion));
  }

  /**
   * Check if the project exists in the database
   *
   * @param projectName the name of the project to check if exists
   * @author George A. David
   */
  public static boolean doesProjectExist(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    boolean fileExists = false;

    String databasePath = Directory.getProjectDatabaseDir();
    File file = new File(databasePath);
    if(file.exists()== false)
    {
      if ( _continueWithoutDatabase == false)
      {
        FileUtilAxi.createDirectory(databasePath);
      }
      else
        return fileExists;
    }
    String[] fileNames = file.list();

    Pattern pattern = createFileMatchingPattern(projectName);

    // now that we have our regular expression set up, let's see if any of the files match this name.
    for(int i = 0; i < fileNames.length; ++i)
    {
      Matcher matcher = pattern.matcher(fileNames[i]);
      if(matcher.matches())
      {
        fileExists = true;
        break;
      }

    }

    return fileExists;
  }

  /**
   * Get the new database version number
   * @param projectName the name of the project
   * @author George A. David
   */
  public static int getNewVersionNumber(String projectName) throws DatastoreException
  {
    int currentVersionNumber = 0;
    if(doesProjectExist(projectName))
      currentVersionNumber = getLatestDatabaseVersion(projectName);

    return currentVersionNumber + 1;
  }

  /**
   * get the database version number of the project in the database
   * @param projectName the name of the project to find
   * @author George A. David
   */
  public static int getLatestDatabaseVersion(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);
    Assert.expect(projectName != null);

    int latestVersion = -1;

    String databasePath = Directory.getProjectDatabaseDir();
    File file = new File(databasePath);
    if(file.exists()== false)
      FileUtilAxi.createDirectory(databasePath);
    String[] fileNames = file.list();

    Pattern pattern = createFileMatchingPattern(projectName);

    // now that we have our regular expression set up, let's see if any of the files match this name.
    for(int i = 0; i < fileNames.length; ++i)
    {
      Matcher matcher = pattern.matcher(fileNames[i]);
      if(matcher.matches())
      {
        try
        {
          int currentVersion = StringUtil.convertStringToInt(matcher.group(1));
          latestVersion = Math.max(latestVersion, currentVersion);
        }
        catch (BadFormatException ex)
        {
          // this should not happen
          Assert.logException(ex);
          Assert.expect(false);
        }
      }
    }

    Assert.expect(latestVersion > 0);

    return latestVersion;
  }

  /**
   * gets the latest version
   * @author George A. David
   */
  public static void getProject(String projectName) throws DatastoreException
  {
    getProject(projectName, getLatestDatabaseVersion(projectName));
  }

  /**
   * Retrieve the project from the database
   *
   * @param projectName the name of the project to retrieve
   * @author George A. David
   */
  public static void getProject(String projectName, int versionNumber) throws DatastoreException
  {
    Assert.expect(projectName != null);

    boolean projectExistsLocally = Project.doesProjectExistLocally(projectName);

    checkLocalHardDriveSpace(projectName, versionNumber);

    copyDatabaseFileToLocalTempDatabaseFile(projectName, versionNumber);

    if(projectExistsLocally)
      backupLocalProject(projectName);

    unzipLocalTemporaryDatabaseFile(projectName);

    if(projectExistsLocally)
      deleteLocalBackup(projectName);
  }

  /**
   * delete a project from the database
   * @param projectName the name of the project to delete
   * @author George A. David
   */
  public static void deleteProject(String projectName, int databaseVersion) throws DatastoreException
  {
    Assert.expect(projectName != null);

    // get the path to the database file
    String databaseFilePath = FileName.getProjectDatabaseFileFullPath(projectName, databaseVersion);

    FileUtilAxi.delete(databaseFilePath);
    FileUtilAxi.delete(FileName.getProjectDatabaseSummaryFileFullPath(projectName, databaseVersion));
    FileUtilAxi.delete(FileName.getProjectDatabaseCommentFileFullPath(projectName, databaseVersion));
  }

  /**
   * @author Cheah Lee Herng 
   */
  public static void deleteProject(String projectName, int databaseVersion, int projectSummaryVersion) throws DatastoreException
  {
    Assert.expect(projectName != null);

    // get the path to the database file
    String databaseFilePath = FileName.getProjectDatabaseFileFullPath(projectName, databaseVersion);

    if (FileUtilAxi.exists(databaseFilePath)) 
      FileUtilAxi.delete(databaseFilePath);
    if (FileUtilAxi.exists(FileName.getProjectDatabaseSummaryFileFullPath(projectName, databaseVersion, projectSummaryVersion)))
      FileUtilAxi.delete(FileName.getProjectDatabaseSummaryFileFullPath(projectName, databaseVersion, projectSummaryVersion));
    if (FileUtilAxi.exists(FileName.getProjectDatabaseCommentFileFullPath(projectName, databaseVersion)))
      FileUtilAxi.delete(FileName.getProjectDatabaseCommentFileFullPath(projectName, databaseVersion));
  }

  /**
   * create a local database file that will be used to copy over to the database
   * @param projectName the name of the project being added to the database
   * @author George A. David
   */
  private static void createLocalTemporaryDatabaseFile(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    String localTemporaryDatabaseFileName = Directory.getTempDir() + File.separator +
                                            projectName + FileName.getZipFileExtension();
    ProjectArchiver archiver = new ProjectArchiver();
    archiver.setProjectName(projectName);
    archiver.setDestinationFilename(localTemporaryDatabaseFileName);
    archiver.setIncludeAlgorithmLearning(archiver.doesAlgorithmLearningExist());
    archiver.archive();
  }

  /**
   * check if we have enough disk space in the database to add the project
   * @param projectName the name of the project being added to the database
   * @author George A. David
   */
  private static void checkDiskSpaceInDatabase(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    String localTemporaryDatabaseFileName = Directory.getTempDir() + File.separator +
                                              projectName + FileName.getZipFileExtension();

    File temporaryFile = new File(localTemporaryDatabaseFileName);
    Assert.expect(temporaryFile.exists());

    // get the size of the file
    long sizeInBytes = temporaryFile.length();

    // get the free disk spae available
    String databasePath = Directory.getProjectDatabaseDir();
     if(FileUtilAxi.exists(databasePath) == false)
      FileUtilAxi.createDirectory(databasePath);
    long freeDiskSpaceInBytes = FileUtil.getAvailableDiskSpaceInBytes(databasePath);

    // if not enough space, throw an exception
    if(freeDiskSpaceInBytes < sizeInBytes)
    {
      temporaryFile.delete();
      throw new InsufficientSpaceInProjectDatabaseDatastoreException(databasePath,
                                                                     projectName);
    }
  }

  /**
   * copy the local database file to the database
   *
   * @param projectName the name of the project being added to the database
   * @author George A. David
   */
  private static void transferToDatabase(String projectName, int versionNumber) throws DatastoreException
  {
    Assert.expect(projectName != null);

    String newDatabaseFilePath = FileName.getProjectDatabaseFileFullPath(projectName, versionNumber);
    String localTemporaryDatabaseFileName = Directory.getTempDir() + File.separator +
                                              projectName + FileName.getZipFileExtension();

    try
    {
      // copy the temporary file to the database.
      FileUtilAxi.copy(localTemporaryDatabaseFileName, newDatabaseFilePath);
      String fileName = null;
      int version = FileName.getProjectSummaryLatestFileVersion();
      while (version > 0)
      {
         fileName = FileName.getProjectSummaryFullPath(projectName, version);
         if (FileUtilAxi.exists(fileName))
           break;
         --version;
      }
      Assert.expect(fileName != null);

      // copy the summary file
      FileUtilAxi.copy(fileName, FileName.getProjectDatabaseSummaryFileFullPath(projectName, versionNumber, version));
    }
    catch (DatastoreException dex)
    {
      // the copy failed
      // delete the local temporary database file
      FileUtilAxi.delete(localTemporaryDatabaseFileName);

      // delete the new file in the database
      FileUtilAxi.delete(newDatabaseFilePath);

      throw dex;
    }
  }

  /**
   * delete the local database file
   * @param the project being addded to the database
   * @author George A. David
   */
  private static void deleteLocalTemporaryDatabaseFile(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    String localTempDatabaseFileName = Directory.getTempDir() + File.separator +
                                         projectName + FileName.getZipFileExtension();

    // delete the file
    FileUtilAxi.delete(localTempDatabaseFileName);
  }

  /**
   * check if we have enough local hard drive space to download a project from the database
   * @param projectName the name of the project being downloaded
   * @author George A. David
   */
  private static void checkLocalHardDriveSpace(String projectName, int versionNumber) throws DatastoreException
  {
    Assert.expect(projectName != null);

    String projectDir = Directory.getProjectDir(projectName);
    String databaseFileName = FileName.getProjectDatabaseFileFullPath(projectName, versionNumber);

    long uncompressedZipFileSizeInBytes = uncompressedZipFileSizeInBytes = FileUtilAxi.getUncompressedZipFileSizeInBytes(databaseFileName);

    // the the local free disk space
    long freeSpaceInBytes = FileUtil.getAvailableDiskSpaceInBytes(Directory.getProjectsDir());

    // throw an exception if there is not enough space
    if(freeSpaceInBytes < uncompressedZipFileSizeInBytes)
    {
      throw new InsufficientSpaceToGetProjectDatastoreException(projectDir,
                                                                projectName,
                                                                Directory.getProjectDatabaseDir());
    }
  }

  /**
   * backup the local project data
   * @param projectName the name of the project to backup
   * @author George A. David
   */
  private static void backupLocalProject(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    String projectDir = Directory.getProjectDir(projectName);
    String backupProjectDir = Directory.getBackupProjectDir(projectName);

    // delete any previous backups
    if(FileUtil.exists(backupProjectDir))
      FileUtilAxi.deleteDirectoryContents(backupProjectDir);
    else
      FileUtilAxi.createDirectory(backupProjectDir);


    // do not backup the images, these will always remain
    // intact no matter what version of the project exists.
    // the user may delete these images using the TDT
    List<String> dontMoveFiles = new ArrayList<String>();
    dontMoveFiles.add(backupProjectDir);
    dontMoveFiles.add(Directory.getXrayImagesDir(projectName));

    // backup the directory
    FileUtilAxi.moveDirectoryContents(projectDir, backupProjectDir, dontMoveFiles);
  }

  /**
   * @author George A. David
   */
  private static void deleteLocalBackup(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    String backupProjectDir = Directory.getBackupProjectDir(projectName);

    FileUtilAxi.deleteDirectoryContents(backupProjectDir);
  }

  /**
   * copy over the database file to a local temporary database file
   * @param projectName the project we want to download from the database
   * @author George A. David
   */
  private static void copyDatabaseFileToLocalTempDatabaseFile(String projectName, int versionNumber) throws DatastoreException
  {
    Assert.expect(projectName != null);

    String localTemporaryDatabaseFileName = projectName + FileName.getZipFileExtension();
    String databaseFileName = FileName.getProjectDatabaseFileFullPath(projectName, versionNumber);

    // copy the database file to a local temporary file to the database.
    try
    {
      FileUtilAxi.copy(databaseFileName, localTemporaryDatabaseFileName);
    }
    catch (DatastoreException dex)
    {
      // the copy failed
      // delete the local temporary database file
      FileUtilAxi.delete(localTemporaryDatabaseFileName);

      throw dex;
    }
  }

  /**
   * unzip the local database file
   * @param projectName the project downloaded from the database
   * @author George A. David
   */
  private static void unzipLocalTemporaryDatabaseFile(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    boolean projectExistsLocally = Project.doesProjectExistLocally(projectName);
    String projectDir = Directory.getProjectDir(projectName);
    String backupProjectDir = FileUtil.changeExtensionToBackup(projectDir);
    String localTemporaryDatabaseFileName = projectName + FileName.getZipFileExtension();

    try
    {
      FileUtilAxi.unZip(localTemporaryDatabaseFileName, projectDir, false);
    }
    catch(DatastoreException exception)
    {
      // if the project previously existed, try restoring it.
      if(projectExistsLocally)
        FileUtilAxi.moveDirectoryContents(backupProjectDir, projectDir);

      throw exception;
    }
  }

  /**
   * Using the project name passed in, it creates a pattern to
   * match the following string projectName.version#.zip.
   * @param projectName the name of the project
   * @author George A. David
   *
   * modified by Wei Chin, Chong (08 July 2008) - handling the parenthesis
   */
  private static Pattern createFileMatchingPattern(String projectName)
  {
    Assert.expect(projectName != null);

    // the period, the question mark and the parenthesis are special characters in regular expresions.
    // these are valid characters for a project name. we need to add escape characters
    // in front of them to create a reqular expression out of them. the regular expresion
    // we have setup will match the following string "projectName.version#.zip"

    String regEx = projectName.replaceAll("(\\.|\\?|\\(|\\))", "\\\\$1");

    regEx += "\\.(\\d+)\\.zip";

    return Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
  }

  /**
   * @author George A. David
   */
  private static Pattern createFileMatchingPattern()
  {
    String regEx = "(.+)\\.(\\d+)\\.zip";

    return Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
  }

  /**
   * Get the existing project database file name
   * @param projectName the name of the project
   * @author George A. David
   */
  private static String getExistingDatabaseFileName(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    String fileName = null;

    String databasePath = Directory.getProjectDatabaseDir();
    File file = new File(databasePath);
    if(file.exists()== false)
      FileUtilAxi.createDirectory(databasePath);
    File[] files = file.listFiles();

    Pattern pattern = createFileMatchingPattern(projectName);

    // now that we have our regular expression set up, let's see if any of the files match this name.
    for(int i = 0; i < files.length; ++i)
    {
      Matcher matcher = pattern.matcher(files[i].getName());
      if(matcher.matches())
      {
        fileName = files[i].getPath();
        break;
      }
    }

    Assert.expect(fileName != null);
    return fileName;
  }

  /**
   * Determine if a newer project exists in the database
   * @param projectName the name of the project
   * @author George A. David
   */
  public static boolean doesNewerProjectExist(String projectName) throws XrayTesterException
  {
    Assert.expect(projectName != null);

    boolean newerExists = false;
    if(doesProjectExist(projectName))
    {
      if (Project.doesProjectExistLocally(projectName) == false)
        return true;

      if(_projectSummaryReader == null)
        _projectSummaryReader = ProjectSummaryReader.getInstance();
      int databaseVersion = getLatestDatabaseVersion(projectName);
      ProjectSummary databaseSummary = _projectSummaryReader.read(projectName, databaseVersion);
      ProjectSummary localSummary = _projectSummaryReader.read(projectName);

      newerExists = databaseSummary.getVersion() > localSummary.getVersion();
      if(databaseSummary.getVersion() == localSummary.getVersion())
      {
        // if the checksusm don't match, we cannot tell if a newer
        // one exists, throw an exception.
        if(databaseSummary.getCadXmlCheckSum() != localSummary.getCadXmlCheckSum() ||
           databaseSummary.getSettingsXmlCheckSum() != localSummary.getSettingsXmlCheckSum())
        {
          throw new ProjectDatabaseVersionConflictBusinessException(projectName);
        }
      }
    }

    return newerExists;
  }

  /**
   * @author George A. David
   */
  public static List<ProjectSummary> getProjectSummaries() throws DatastoreException
  {
    String databasePath = Directory.getProjectDatabaseDir();
    File file = new File(databasePath);
    if(file.exists()== false)
      FileUtilAxi.createDirectory(databasePath);
    File[] files = file.listFiles();

    Pattern pattern = createFileMatchingPattern();

    List<ProjectSummary> projectSummaries = new LinkedList<ProjectSummary>();
    // now that we have our regular expression set up, let's see if any of the files match this name.
    for(int i = 0; i < files.length; ++i)
    {
      Matcher matcher = pattern.matcher(files[i].getName());
      if(matcher.matches())
      {
        String projectName = matcher.group(1);
        int databaseVersion = 0;
        try
        {
          databaseVersion = StringUtil.convertStringToInt(matcher.group(2));
        }
        catch (BadFormatException ex)
        {
          Assert.expect(false);
        }

        if(_projectSummaryReader == null)
          _projectSummaryReader = ProjectSummaryReader.getInstance();
        ProjectSummary summary = _projectSummaryReader.read(projectName, databaseVersion);
        summary.setDatabaseComment(readDatabaseComment(projectName, databaseVersion));
        projectSummaries.add(summary);
      }
    }

    return projectSummaries;
  }

  /**
   * @author George A. David
   */
  public static boolean isAutoUpdateLocalProjectEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.AUTOMATICALLY_PULL_LATEST_PROJECT_VERSION_ON_LOAD);
  }

  /**
   * @author George A. David
   */
  public static void setAutoUpdateLocalProject(boolean autoUpdateEnabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.AUTOMATICALLY_PULL_LATEST_PROJECT_VERSION_ON_LOAD, autoUpdateEnabled);
  }
  
  /**
   * XCR-3364
   * default value is false; if user want to continue testing even database directory is invalid, set it true;
   * @author weng-jian.eoh
   * @param continueWithoutDatabase 
   */
  public static void setContinueWithoutDatabase(boolean continueWithoutDatabase)
  {
    _continueWithoutDatabase = continueWithoutDatabase;
  }
  /**
   * XCR-3364
   * @author weng-jian.eoh
   * 
   * if true,continue testing with current recipe without pull from database; 
   * if database directory is valid, change back to false. Continue pull recipe from database
   * @return 
   */
  public static boolean isContinueWithoutDatabase()
  {    
    if (isDatabasePathValid() == false && _continueWithoutDatabase == true)
    {
      return true;
    }
    return false;
  }
  
  /**
   * XCR-3364
   * @author weng-jian.eoh
   * check path valid or not;
   * @return 
   */
  private static boolean isDatabasePathValid()
  {
    String databasePath = Directory.getProjectDatabaseDir();
    if(FileUtil.exists(databasePath))
    {
      return true;
    }
    return false;
  }
}
