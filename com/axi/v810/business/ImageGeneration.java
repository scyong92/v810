package com.axi.v810.business;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.internalTools.*;
import com.axi.v810.util.*;

/**
 * This class will be responsible for recieving and saving images for offline testing.
 *
 * @author George A. David
 */
public class ImageGeneration implements Observer
{
  private static ImageGeneration _instance;
  private ImageManager _imageManager = ImageManager.getInstance();
  private ImageAcquisitionEngine _imageAcquisitionEngine;
  private OpticalImageAcquisitionEngine _opticalImageAcquisitionEngine;
  private ImageSetData _imageSetData;
  private XrayTesterException _exception;
  private XrayTester _xRayTester = XrayTester.getInstance();
  private WorkerThread _acquireIncomingImagesThread  = new WorkerThread("image set generation");
  private WorkerThread _opticalImageAcquisitionWorkerThread = new WorkerThread("optical image acquisition thread");
  private WorkerThread _alignmentOpticalImageAcquisitionWorkerThread = new WorkerThread("alignment optical image acquisition thread");
  private ImageSetGenerator _imageSetGenerator = ImageSetGenerator.getInstance();
  private String _projectName;
  private boolean _aborted = false;
  private boolean _isGeneratingImages = false;
  private BooleanLock _waitForImagesToBeSavedLock = new BooleanLock();
  private Alignment _alignment;
  private TimerUtil _timer = new TimerUtil();
  private Map<Integer, FocusResult> _focusGroupIdToFocusResultMap = new HashMap<Integer, FocusResult>();
  private ProgramGeneration _programGeneration = ProgramGeneration.getInstance();
  private ImageGenerationObservable _imageGenerationObservable = ImageGenerationObservable.getInstance();
  
  private BooleanLock _acquiringOpticalImages = new BooleanLock(false);
  private BooleanLock _acquiringAlignmentOpticalImages = new BooleanLock(false);
  private BooleanLock _abortOpticalImageAcquisitionFlag = new BooleanLock(false);
  private BooleanLock _abortAlignmentOpticalImageAcquisitionFlag = new BooleanLock(false);

  private Comparator<ReconstructionRegion> _regionLocationComparator = new Comparator<ReconstructionRegion>()
  {
    public int compare(ReconstructionRegion lhs, ReconstructionRegion rhs)
    {
      PanelCoordinate lhsCoord= lhs.getCenterCoordinateRelativeToPanelInNanoMeters();
      PanelCoordinate rhsCoord= rhs.getCenterCoordinateRelativeToPanelInNanoMeters();

      // first check y, then x
      if(lhsCoord.getY() < rhsCoord.getY())
        return -1;
      else if(lhsCoord.getY() > rhsCoord.getY())
        return 1;
      else
      {
        if(lhsCoord.getX() < rhsCoord.getX())
          return -1;
        else if(lhsCoord.getX() > rhsCoord.getX())
          return 1;
        else // check top or bottom, top goes first
          if(lhs.isTopSide())
            return -1;
          else
            return 1;
      }
    }
  };
  private Map<ReconstructionRegion, Integer> _reconstructionRegionToZheightInNanosMap = new TreeMap<ReconstructionRegion, Integer>(_regionLocationComparator);
//  private FocusResultsObservable _focusResultObservable = FocusResultsObservable.getInstance();

  /**
   * @author George A. David
   */
  public static synchronized ImageGeneration getInstance()
  {
    if(_instance == null)
      _instance = new ImageGeneration();

    return _instance;
  }

  /**
   * @author George A. David
   */
  private ImageGeneration()
  {
    _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
    _opticalImageAcquisitionEngine = OpticalImageAcquisitionEngine.getInstance();
  }

  /**
   * @author George A. David
   * @author Aimee Ong
   */
  public void acquireOfflineImagesIfNecessary(final TestProgram testProgram,
                                              TestExecutionTimer testExecutionTimer,
                                              ImageSetData imageSetData) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(imageSetData != null);

    List<ReconstructionRegion> regionsNeedingImages = new LinkedList<ReconstructionRegion>();
    List<ReconstructionRegion> inspectionReconstructionRegions = testProgram.getFilteredInspectionRegions();

    // let's determine what regions need images.
    Set<ReconstructionRegion> recoRegionsWithCompatibleImages = _imageManager.whichReconstructionRegionsHaveCompatibleImages(imageSetData, inspectionReconstructionRegions);
    inspectionReconstructionRegions.removeAll(recoRegionsWithCompatibleImages);
    regionsNeedingImages.addAll(inspectionReconstructionRegions);

    if (regionsNeedingImages.isEmpty() == false)
    {
      // ok, we need to capture some images.
      testProgram.addFilters(regionsNeedingImages);
      acquireOfflineImages(testProgram, testExecutionTimer, imageSetData);
      testProgram.clearReconstructionRegionFilters();
    }
  }

  /**
   * @author George A. David
   */
  public void acquireOfflineImages(final TestProgram testProgram,
                                   TestExecutionTimer testExecutionTimer,
                                   ImageSetData imageSetData) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(imageSetData != null);

    _imageGenerationObservable.stateChanged(new ImageGenerationEvent(ImageGenerationEventEnum.BEGIN_ACQUIRE_OFFLINE_IMAGES));

    _timer.reset();
    try
    {
      _imageSetData = imageSetData;
      _exception = null;
      _aborted = false;
      _isGeneratingImages = true;

      initializeOfflineImageDirectory(testProgram);

      if(_aborted)
        return;

      // we don't want to generate images for the extra slices used for retesting unfocused regions
      testProgram.setSlicesForRetestingUnfocusedRegionsEnabled(false);

      if(_aborted)
        return;
   
     // XCR1642 - To filter the testSubProgram pass into acquireAlignmentAndOfflineImages() during customize fine tuning image collection, which cause hang.
      if(testProgram.hasVariableMagnification())
      {
        for(MagnificationTypeEnum magnificationType : MagnificationTypeEnum.getMagnificationToEnumMap().values())
        {
          if (_aborted)
            return;

          final TestProgram cleanTestProgram = testProgram;
          cleanTestProgram.clearTestSubProgramFilters();
          //XCR-3467, System Crash When Collect Mixed Mag Fine Tuning Images
          boolean hasSubProgramConfigured = setupFilteredTestProgramForFineTuningImage(imageSetData, cleanTestProgram, magnificationType);
          if(cleanTestProgram.getFilteredInspectionRegionsFromFilterTestSubProgram().size() > 0 && hasSubProgramConfigured)
            acquireAlignmentAndOfflineImages(cleanTestProgram, testExecutionTimer);
        }
      }
      else
      {
        //XCR-3467, System Crash When Collect Mixed Mag Fine Tuning Images
        if (setupFilteredTestProgramForFineTuningImage(imageSetData, testProgram))
          acquireAlignmentAndOfflineImages(testProgram, testExecutionTimer);
      }    
    }
    catch (XrayTesterException ex)
    {
      abort(ex);
    }
    catch (Throwable throwable)
    {
      Assert.logException(throwable);
    }
    finally
    {
      _isGeneratingImages = false;

      if (_aborted)
      {
        if ((_xRayTester.isSimulationModeOn() || _xRayTester.isHardwareAvailable() == false)
          && _imageAcquisitionEngine.isOfflineReconstructionEnabled() == false)
          _imageSetGenerator.waitForImageGenerationToStop();

        try
        {
          _imageManager.deleteImages(_imageSetData);
        }
        catch (XrayTesterException ex)
        {
          if (_exception != null)
            ex.printStackTrace();
          else
            _exception = ex; // will be thrown at the end of this finally block
        }
      }

      _imageSetData = null;
      testProgram.setSlicesForRetestingUnfocusedRegionsEnabled(true);
      testProgram.clearTestSubProgramFilters();
      _imageGenerationObservable.stateChanged(new ImageGenerationEvent(ImageGenerationEventEnum.END_ACQUIRE_OFFLINE_IMAGES));
      if (_exception != null)
        throw _exception;
    }
  }

/**
   * @author George A. David
   * @author John Heumann
 */
public void initializeOfflineImageDirectory(TestProgram testProgram, ImageSetData imageSetData) throws DatastoreException
{
  // create the image directory
  imageSetData.setPercentOfCompatibleImages(100.0);
  if (imageSetData.isOnlineTestDevelopment() == false)
  {
    long currentTime = System.currentTimeMillis();
    imageSetData.setImageSetName(Directory.getDirName(currentTime));
    imageSetData.setDateInMils(currentTime);
    FileUtilAxi.createDirectory(imageSetData.getDir());
    ImageSetInfoWriter writer = new ImageSetInfoWriter();
    writer.write(imageSetData);
    _imageManager.saveInspectionImageInfo(imageSetData, testProgram);
  }
  else
    FileUtilAxi.createDirectory(imageSetData.getDir());
}


  /**
   * @author George A. David
   * @author John Heumann
   */
  private void initializeOfflineImageDirectory(TestProgram testProgram) throws DatastoreException
  {
    initializeOfflineImageDirectory(testProgram, _imageSetData);
  }

  /**
   * @author George A. David
   */
  private void acquireAlignmentAndOfflineImages(TestProgram testProgram, TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    _imageAcquisitionEngine.initialize();
    _waitForImagesToBeSavedLock.setValue(false);
    
    // Reset parameter
    _abortAlignmentOpticalImageAcquisitionFlag.setValue(false);
    _abortOpticalImageAcquisitionFlag.setValue(false);
    
    // Commented by Lee Herng, 29 Aug 2014
    // This is because PSP alignment is still un-stable.
    /**
    // First and foremost, we run Alignment SurfaceMap and SurfaceMap, if any.
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP) &&
        testProgram.getAllInspectableAlignmentSurfaceMapTestSubPrograms().size() > 0)
    {
        // Perform Surface Map for Alignment usage
        performAlignmentSurfaceMap(testProgram, testExecutionTimer);
    }
    * **/
    
    if (_abortOpticalImageAcquisitionFlag.isFalse() && _abortAlignmentOpticalImageAcquisitionFlag.isFalse())
    {        
        handleIncommingAlignmentAndOfflineImages(testProgram);

        // initialize alignment
        if (_alignment == null)
          _alignment = Alignment.getInstance();

        _alignment.initializeAlignmentProcess(testProgram);

        if(_aborted)
          return;

        // begin acquiring images.
        _imageAcquisitionEngine.acquireImageSetImages(testProgram, false, testExecutionTimer);

        if(_aborted)
          return;

        try
        {
          _waitForImagesToBeSavedLock.waitUntilTrue();
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
          Assert.expect(false);
        }
    }
  }

  /**
   * @author George A. David
   */
  private void handleIncommingAlignmentAndOfflineImages(final TestProgram testProgram)
  {
    // start the thread to save images when available
    _acquireIncomingImagesThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          for (TestSubProgram subProgram : testProgram.getFilteredTestSubPrograms())
          {
            int numRegions = subProgram.getFilteredInspectionRegions().size();
            if (numRegions <= 0 && subProgram.isSubProgramPerformAlignment() == false)
            {
              continue;
            }

            handleIncomingAlignmentImages(subProgram, true);

            if (_aborted)
              return;

            // then we get the inspection images
            for (int i = 0; i < numRegions; ++i)
            {
              BooleanRef abortedDuringGet = new BooleanRef(false);
              ReconstructedImages reconstructedImages = _imageAcquisitionEngine.getReconstructedImages(
                  abortedDuringGet);

              if (abortedDuringGet.getValue())
              {
                _aborted = true;
                return;
              }

              ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

              //add project name parameter in saveInspectionImages function call 
              saveInspectionImages(reconstructedImages,testProgram.getProject().getName());
              reconstructedImages.decrementReferenceCount();

              if(_aborted)
                return;

              if (_imageSetData.collectLightestImages())
              {
                BooleanRef lightestImagesAvailable = new BooleanRef(true);
                ReconstructedImages lightestImages =
                    _imageAcquisitionEngine.reacquireLightestImagesAndWait(reconstructionRegion, lightestImagesAvailable);
                if (lightestImagesAvailable.getValue() == true)
                {
                  saveLightestInspectionImages(lightestImages);
                  lightestImages.decrementReferenceCount();
                }
              }

              if(_aborted)
                return;

              if (_imageSetData.collectFocusConfirmationImages())
              {
                JointTypeEnum jointTypeEnum = reconstructionRegion.getJointTypeEnum();
                if (jointTypeEnum.isAvailableForRetestFailingPins())
                {
                  Collection<Integer> candidateFocusConfirmationOffsets = FocusConfirmationEngine.getCandidateFocusConfirmationOffsets();
                  for (int candidateFocusConfirmationOffset : candidateFocusConfirmationOffsets)
                  {
                    BooleanRef candidateFocusConfirmationImagesAvailable = new BooleanRef(true);
                    ReconstructedImages focusConfirmationImages =
                        _imageAcquisitionEngine.reacquireImagesAndWait(reconstructionRegion,
                                                                       candidateFocusConfirmationOffset,
                                                                       candidateFocusConfirmationImagesAvailable);
                    if (candidateFocusConfirmationImagesAvailable.getValue() == true)
                    {
                      saveFocusConfirmationImages(focusConfirmationImages, candidateFocusConfirmationOffset);
                      focusConfirmationImages.decrementReferenceCount();
                    }
                  }
                }
              }

              if (_imageAcquisitionEngine.isAbortInProgress() == false)
                _imageAcquisitionEngine.reconstructionRegionComplete(reconstructionRegion, false);
              _imageAcquisitionEngine.freeReconstructedImages(reconstructionRegion);
            }
          }

          // ok we are done, but the system always scans the whole entire panel.
          // we need to tell it to stop now in case we are doing a partial test.
          _imageAcquisitionEngine.abort();
        }
        catch (XrayTesterException xtex)
        {
          abort(xtex);
        }
        catch (Throwable throwable)
        {
          Assert.logException(throwable);
        }
        finally
        {
          _waitForImagesToBeSavedLock.setValue(true);
        }
      }
    });
  }

  /**
   * @author George A. David
   */
  public void acquireAdjustFocusImages(final TestProgram testProgram,
                                       TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);

    ImageGenerationEvent imageGenerationEvent = new ImageGenerationEvent(ImageGenerationEventEnum.BEGIN_ACQUIRE_FOCUS_SET_IMAGES);
    _imageGenerationObservable.stateChanged(imageGenerationEvent);

    _timer.reset();
    String imagesDir = Directory.getAdjustFocusImagesDir(testProgram.getProject().getName());
    try
    {
      _exception = null;
      _aborted = false;
      _isGeneratingImages = true;
      _focusGroupIdToFocusResultMap.clear();

      // create the image directory
      if(FileUtilAxi.exists(imagesDir))
        FileUtilAxi.deleteDirectoryContents(imagesDir);
      else
        FileUtilAxi.createDirectory(imagesDir);

//      testProgram.updateWithAdjustFocusSlices(true);
      if ((_xRayTester.isSimulationModeOn() || _xRayTester.isHardwareAvailable() == false)
          && _imageAcquisitionEngine.isOfflineReconstructionEnabled() == false)
        _imageSetGenerator.generateAdjustFocusImages(testProgram.getProject());
      else
      {
        _imageAcquisitionEngine.initialize();
        _waitForImagesToBeSavedLock.setValue(false);
        // start the thread to save images when available
        _acquireIncomingImagesThread.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              for (TestSubProgram subProgram : testProgram.getAllTestSubPrograms())
              {
                handleIncomingAlignmentImages(subProgram, true);

                if (_aborted)
                  return;

                // then we get the inspection images
                int numRegions = subProgram.getFilteredInspectionRegions().size();
                for (int i = 0; i < numRegions; ++i)
                {
                  BooleanRef abortedDuringGet = new BooleanRef(false);
                  ReconstructedImages reconstructedImages = _imageAcquisitionEngine.getReconstructedImages(
                      abortedDuringGet);

                  if (abortedDuringGet.getValue())
                  {
                    _aborted = true;
                    return;
                  }

                  ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

                  handleAdjustFocusImages(reconstructedImages);
                  reconstructedImages.decrementReferenceCount();


                  if (_imageAcquisitionEngine.isAbortInProgress() == false)
                    _imageAcquisitionEngine.reconstructionRegionComplete(reconstructionRegion, false);
                  _imageAcquisitionEngine.freeReconstructedImages(reconstructionRegion);
                }
                // ok we are done, but the system always scans the whole entire panel.
                // we need to tell it to stop now.
                _imageAcquisitionEngine.abort();
              }
            }
            catch (XrayTesterException xtex)
            {
              abort(xtex);
            }
            catch (Throwable throwable)
            {
              Assert.logException(throwable);
            }
            finally
            {
              _waitForImagesToBeSavedLock.setValue(true);
            }
          }
        });

        // initialize alignment
        if (_alignment == null)
          _alignment = Alignment.getInstance();

        _alignment.initializeAlignmentProcess(testProgram);

        if(_aborted)
          return;

        // begin acquiring images.
        _imageAcquisitionEngine.acquireImageSetImages(testProgram, false, testExecutionTimer);

        if(_aborted)
          return;

        try
        {
          _waitForImagesToBeSavedLock.waitUntilTrue();
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
          Assert.expect(false);
        }
      }

      if(_aborted)
        return;

      _imageManager.saveAdjustFocusImageNames(testProgram);
    }
    catch (XrayTesterException ex)
    {
      abort(ex);
    }
    catch (Throwable throwable)
    {
      Assert.logException(throwable);
    }
    finally
    {
      _isGeneratingImages = false;
      if(_aborted)
      {
        if ((_xRayTester.isSimulationModeOn() || _xRayTester.isHardwareAvailable() == false)
        && _imageAcquisitionEngine.isOfflineReconstructionEnabled() == false)
          _imageSetGenerator.waitForImageGenerationToStop();

        FileUtilAxi.deleteDirectoryContents(imagesDir);
      }
      _imageSetData = null;
      _focusGroupIdToFocusResultMap.clear();
      _imageGenerationObservable.stateChanged(new ImageGenerationEvent(ImageGenerationEventEnum.END_ACQUIRE_FOCUS_SET_IMAGES));
      if (_exception != null)
        throw _exception;
    }
  }

  /**
   * @author George A. David
   * @author khang-shian.sham
   */
  private synchronized void saveInspectionImages(ReconstructedImages reconstructedImages, String projectName)
  {
    Assert.expect(projectName != null);

    if(_exception != null || _aborted)
      return;

    try
    {
      _timer.start();
       //add project name parameter in saveInspectionImages function call
      _imageManager.saveInspectionImages(_imageSetData, reconstructedImages,projectName);
    }
    catch (XrayTesterException ex)
    {
      abort(ex);
    }
    finally
    {
      _timer.stop();
    }
  }

  /**
   * @author George A. David
   */
  private synchronized void saveLightestInspectionImages(ReconstructedImages reconstructedImages)
  {
    if(_exception != null || _aborted)
      return;

    try
    {
      _timer.start();
      _imageManager.saveLightestInspectionImages(_imageSetData, reconstructedImages);
    }
    catch (XrayTesterException ex)
    {
      abort(ex);
    }
    finally
    {
      _timer.stop();
    }
  }

  /**
   * @author Matt Wharton
   */
  private synchronized void saveFocusConfirmationImages(ReconstructedImages reconstructedImages, int sliceOffset)
  {
    if ((_exception != null) || _aborted)
    {
      return;
    }

    try
    {
      _timer.start();
      _imageManager.saveFocusConfirmationInspectionImages(_imageSetData, reconstructedImages, sliceOffset);
    }
    catch (XrayTesterException xtex)
    {
      abort(xtex);
    }
    finally
    {
      _timer.stop();
    }
  }

  /**
   * @author George A. David
   */
  private synchronized void handleAdjustFocusImages(ReconstructedImages reconstructedImages)
  {
    if(_exception != null || _aborted)
      return;

    try
    {
      _timer.start();
      _imageManager.saveAdjustImages(reconstructedImages);
    }
    catch (XrayTesterException ex)
    {
      abort(ex);
    }
    finally
    {
      _timer.stop();
    }
  }

  /**
   * @author George A. David
   */
  private synchronized void saveVerificationImages(ReconstructedImages reconstructedImages)
  {
    if(_exception != null || _aborted)
      return;

    try
    {
      _timer.start();
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      if (reconstructedImages.getReconstructionRegion().getTestSubProgram().getTestProgram().isManual2DAlignmentInProgress())
        _imageManager.save2DVerificationImages(_projectName, reconstructedImages);
      else
        _imageManager.saveVerificationImages(_projectName, reconstructedImages);
    }
    catch (XrayTesterException ex)
    {
      abort(ex);
    }
    finally
    {
      _timer.stop();
    }
  }

  /**
   * Verification Image generation is a two-step process to get a better handle on
   * how to properly focus the images. The first step is to acquire all the images and
   * save their focus zheights. Then we send these z heights to the VerificaitonSurfaceEstimator
   * which will return a modified set of those z heights.
   * @author George A. David
   */
  public void alignAndAcquireVerificationImages(TestProgram testProgram, TestExecutionTimer testExecutionTimer, boolean alignBeforeAcquiringImages) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    try
    {
      _isGeneratingImages = true;
      ImageGenerationEvent imageGenerationEvent = new ImageGenerationEvent(ImageGenerationEventEnum.BEGIN_ACQUIRE_VERIFICATION_IMAGES);

      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      if (VerificationSurfaceEstimator.isTwoPassVerificationSurfaceModelingEnabled())
        imageGenerationEvent.setNumEventsToExpect(2 * testProgram.getVerificationRegions().size());
      else
        imageGenerationEvent.setNumEventsToExpect(testProgram.getVerificationRegions().size());

      _imageGenerationObservable.stateChanged(imageGenerationEvent);

      for(TestSubProgram subProgram : testProgram.getAllTestSubPrograms())
      {
       if(subProgram.getVerificationRegions().size() > 0)
        {
          boolean wasAligned = false;
          //Khaw Chek Hau - XCR2285: 2D Alignment on v810
          if (VerificationSurfaceEstimator.isTwoPassVerificationSurfaceModelingEnabled())
          {
            _reconstructionRegionToZheightInNanosMap.clear();
            if (testProgram.isManual2DAlignmentInProgress() == false)
              acquireVerificationImages(subProgram, testExecutionTimer, true, alignBeforeAcquiringImages);
            else
              acquire2DVerificationImages(subProgram, testExecutionTimer, true, alignBeforeAcquiringImages);
              
            if (_aborted)
              return;

            _imageGenerationObservable.stateChanged(new ImageGenerationEvent(ImageGenerationEventEnum.CALCULATE_VERIFICATION_SURFACE_SURFACE_MODEL));
            _programGeneration.modifyTestProgramWithVerificationSurfaceEstimatorChanges(subProgram, _reconstructionRegionToZheightInNanosMap);

            if (_aborted)
              return;
            if(alignBeforeAcquiringImages)
              wasAligned = true;
          }

          boolean is2DAlignmentInProgress = subProgram.getTestProgram().isManual2DAlignmentInProgress();
          
        // if we did two stage, we don't need to align again
          if(wasAligned && alignBeforeAcquiringImages)
          {
            if (is2DAlignmentInProgress)
              acquire2DVerificationImages(subProgram, testExecutionTimer, false, false);
            else
              acquireVerificationImages(subProgram, testExecutionTimer, false, false);
          }
          else
          {
            if (is2DAlignmentInProgress)
              acquire2DVerificationImages(subProgram, testExecutionTimer, false, alignBeforeAcquiringImages);
            else
              acquireVerificationImages(subProgram, testExecutionTimer, false, alignBeforeAcquiringImages);
          }
          
          // Kok Chun, Tan - XCR-3108 - check abort flag here to make sure not abort is perform before run next test subprogram.
          if (_aborted)
              return;
        }
      }
    }
    finally
    {
      _isGeneratingImages = false;
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      if (VerificationSurfaceEstimator.isTwoPassVerificationSurfaceModelingEnabled())
        _programGeneration.clearVerificationSurfaceEstimatorChanges(testProgram);
      _reconstructionRegionToZheightInNanosMap.clear();
      _imageGenerationObservable.stateChanged(new ImageGenerationEvent(ImageGenerationEventEnum.END_ACQUIRE_VERIFICATION_IMAGES));
    }
  }

  /**
   * Verification Image generation is a two-step process to get a better handle on
   * how to properly focus the images. The first step is to acquire all the images and
   * save their focus zheights. Then we send these z heights to the VerificaitonSurfaceEstimator
   * which will return a modified set of those z heights.
   * @author George A. David
   */
  public void acquireAllVerificationImages(TestSubProgram subProgram, TestExecutionTimer testExecutionTimer, boolean alignBeforeAcquiringImages, boolean useRFP) throws XrayTesterException
  {
    Assert.expect(subProgram != null);
    try
    {
      _isGeneratingImages = true;
      ImageGenerationEvent imageGenerationEvent = new ImageGenerationEvent(ImageGenerationEventEnum.BEGIN_ACQUIRE_VERIFICATION_IMAGES);

      {
        //Khaw Chek Hau - XCR2285: 2D Alignment on v810
        if (VerificationSurfaceEstimator.isTwoPassVerificationSurfaceModelingEnabled() && useRFP == false)
        {
          imageGenerationEvent.setNumEventsToExpect(2 * subProgram.getVerificationRegions().size());

          _imageGenerationObservable.stateChanged(imageGenerationEvent);
          _reconstructionRegionToZheightInNanosMap.clear();
          
          if (subProgram.getTestProgram().isManual2DAlignmentInProgress() == false)
            acquireVerificationImages(subProgram, testExecutionTimer, true, alignBeforeAcquiringImages);
          else
            acquire2DVerificationImages(subProgram, testExecutionTimer, true, alignBeforeAcquiringImages);
          
          if (_aborted)
            return;

          _imageGenerationObservable.stateChanged(new ImageGenerationEvent(ImageGenerationEventEnum.CALCULATE_VERIFICATION_SURFACE_SURFACE_MODEL));
          _programGeneration.modifyTestProgramWithVerificationSurfaceEstimatorChanges(subProgram, _reconstructionRegionToZheightInNanosMap);

          if (_aborted)
            return;
        }
        else
        {
          imageGenerationEvent.setNumEventsToExpect(subProgram.getVerificationRegions().size());
          _imageGenerationObservable.stateChanged(imageGenerationEvent);
        }

        if (subProgram.getTestProgram().isManual2DAlignmentInProgress())
          acquire2DVerificationImages(subProgram, testExecutionTimer, false, alignBeforeAcquiringImages);
        else
          acquireVerificationImages(subProgram, testExecutionTimer, false, alignBeforeAcquiringImages);
      }
    }
    finally
    {
      _isGeneratingImages = false;
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      if (VerificationSurfaceEstimator.isTwoPassVerificationSurfaceModelingEnabled() && useRFP == false)
        _programGeneration.clearVerificationSurfaceEstimatorChanges(subProgram.getTestProgram());
      _reconstructionRegionToZheightInNanosMap.clear();
      _imageGenerationObservable.stateChanged(new ImageGenerationEvent(ImageGenerationEventEnum.END_ACQUIRE_VERIFICATION_IMAGES));
    }
  }

  /**
   * @author George A. David
   */
  private void acquireVerificationImages(TestProgram testProgram, TestExecutionTimer testExecutionTimer, boolean isEstimatingSurface, boolean alignBeforeAcquiringImages) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);

    _timer.reset();
    try
    {
      _exception = null;
      _aborted = false;
      _projectName = testProgram.getProject().getName();
      String imagesDir = Directory.getXrayVerificationImagesDir(_projectName);
      if(FileUtilAxi.exists(imagesDir))
        FileUtilAxi.deleteDirectoryContents(imagesDir);
      else
        FileUtilAxi.createDirectory(imagesDir);

      for(final TestSubProgram subProgram : testProgram.getAllTestSubPrograms())
      {
        if (alignBeforeAcquiringImages)
          acquireAndProcessAlignmentImages(subProgram, testExecutionTimer, false);

        acquireAndProcessVerificationImages(subProgram, testExecutionTimer, isEstimatingSurface);

        if (_aborted)
          return;
      }

      if (isEstimatingSurface == false)
        _imageManager.saveVerificationImageNames(testProgram);
    }
    catch (XrayTesterException ex)
    {
      abort(ex);
    }
    catch (Throwable throwable)
    {
      Assert.logException(throwable);
    }
    finally
    {
      if (_exception != null)
        throw _exception;
    }
  }

  /**
   * @author George A. David
   */
  private void acquireVerificationImages(TestSubProgram subProgram, TestExecutionTimer testExecutionTimer, boolean isEstimatingSurface, boolean alignBeforeAcquiringImages) throws XrayTesterException
  {
    Assert.expect(subProgram != null);
    Assert.expect(testExecutionTimer != null);

    _timer.reset();
    TestProgram testProgram = subProgram.getTestProgram();
    try
    {
      _exception = null;
      _aborted = false;
      _projectName = testProgram.getProject().getName();
      String imagesDir = Directory.getXrayVerificationImagesDir(_projectName);
      if(FileUtilAxi.exists(imagesDir) == false)
        FileUtilAxi.createDirectory(imagesDir);

      if (alignBeforeAcquiringImages)
        acquireAndProcessAlignmentImages(subProgram, testExecutionTimer, false);

      acquireAndProcessVerificationImages(subProgram, testExecutionTimer, isEstimatingSurface);

      if (_aborted)
        return;

      if (isEstimatingSurface == false)
        _imageManager.saveVerificationImageNames(subProgram);
    }
    catch (XrayTesterException ex)
    {
      abort(ex);
    }
    catch (Throwable throwable)
    {
      Assert.logException(throwable);
    }
    finally
    {
      if (_exception != null)
        throw _exception;
    }
  }

  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  private void acquire2DVerificationImages(TestSubProgram subProgram, TestExecutionTimer testExecutionTimer, boolean isEstimatingSurface, boolean alignBeforeAcquiringImages) throws XrayTesterException
  {
    Assert.expect(subProgram != null);
    Assert.expect(testExecutionTimer != null);

    _timer.reset();
    TestProgram testProgram = subProgram.getTestProgram();
    try
    {
      _exception = null;
      _aborted = false;
      _projectName = testProgram.getProject().getName();

      //Khaw Chek Hau - XCR2285: 2D Alignment on v810      
      String imagesDir = Directory.getXray2DVerificationImagesDir(_projectName);
        
      if(FileUtilAxi.exists(imagesDir) == false)
        FileUtilAxi.createDirectory(imagesDir);

      if (alignBeforeAcquiringImages)
        acquireAndProcessAlignmentImages(subProgram, testExecutionTimer, false);

      acquireAndProcessVerificationImages(subProgram, testExecutionTimer, isEstimatingSurface);

      if (_aborted)
        return;

      if (isEstimatingSurface == false)
        _imageManager.save2DVerificationImageNames(subProgram);
    }
    catch (XrayTesterException ex)
    {
      abort(ex);
    }
    catch (Throwable throwable)
    {
      Assert.logException(throwable);
    }
    finally
    {
      if (_exception != null)
        throw _exception;
    }
  }
  
  /**
   * @author George A. David
   */
  private void acquireAndProcessVerificationImages(TestSubProgram subProgram, TestExecutionTimer testExecutionTimer, boolean isEstimatingSurface) throws XrayTesterException
  {
    Assert.expect(subProgram != null);

    _waitForImagesToBeSavedLock.setValue(false);
    _imageAcquisitionEngine.initialize();

    if (_aborted)
      return;

    handleIncomingVerificationImages(subProgram, isEstimatingSurface);

    if (_aborted)
      return;

    _imageAcquisitionEngine.acquireVerificationImages(subProgram, testExecutionTimer);


    if (_aborted)
      return;

    try
    {
      _waitForImagesToBeSavedLock.waitUntilTrue();
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }

  }

  /**
   * @author George A. David
   */
  private void acquireAlignmentImages(TestProgram testProgram, TestExecutionTimer testExecutionTimer, boolean sendAlignmentInfoToIrps) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Collection<TestSubProgram> subPrograms =  testProgram.getAllTestSubPrograms();
    for(TestSubProgram subProgram : subPrograms)
      acquireAndProcessAlignmentImages(subProgram, testExecutionTimer, sendAlignmentInfoToIrps);
  }

  /**
   * @author George A. David
   */
  private void acquireAndProcessAlignmentImages(TestSubProgram subProgram, TestExecutionTimer testExecutionTimer, boolean sendAlignmentInfoToIrps) throws XrayTesterException
  {
    Assert.expect(subProgram != null);

    BooleanLock waitForAlignmentToCompleteLock = new BooleanLock(false);

    _imageAcquisitionEngine.initialize();
    processIncomingAlignmentImages(subProgram, waitForAlignmentToCompleteLock, sendAlignmentInfoToIrps);

    if (_aborted)
      return;

    // initialize alignment
    if (_alignment == null)
      _alignment = Alignment.getInstance();

    _alignment.initializeAlignmentProcess(subProgram);

    if (_aborted)
      return;

    _imageAcquisitionEngine.acquireAlignmentImages(subProgram, testExecutionTimer);

    if (_aborted)
      return;

    try
    {
      waitForAlignmentToCompleteLock.waitUntilTrue();
    }
    catch (InterruptedException ex1)
    {
      // do nothing
    }
  }

  /**
   * imageset image will come here also
   * @author George A. David
   */
  private void handleIncomingAlignmentImages(TestSubProgram subProgram, boolean sendAlignmentInfoToIrps) throws XrayTesterException
  {
    int numRegions = subProgram.getAlignmentRegions().size();
    // XCR1168: align on image only when user gain is one,
    // the rest use apply alignment result.
    if (subProgram.isSubProgramPerformAlignment())
    {
      for (int i = 0; i < numRegions; ++i)
      {
        BooleanRef abortedDuringGet = new BooleanRef(false);
        ReconstructedImages reconstructedImages = _imageAcquisitionEngine.getReconstructedImages(abortedDuringGet);

        if (abortedDuringGet.getValue())
        {
          _aborted = true;
          return;
        }

        // CR1046 fix by LeeHerng - De-reference reconstructed image
        try
        {
          boolean isUsing2DAlignmentMethod = subProgram.getTestProgram().getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod();
          
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP) == false)
          {
            //Khaw Chek Hau - XCR2285: 2D Alignment on v810
            if (isUsing2DAlignmentMethod)
              _alignment.alignOn2DImage(reconstructedImages, sendAlignmentInfoToIrps);
            else
              _alignment.alignOnImage(reconstructedImages, sendAlignmentInfoToIrps);
          }  
          else
          {
            if (subProgram.hasEnabledOpticalCameraRectangles())
            {
              //Khaw Chek Hau - XCR2285: 2D Alignment on v810
              if (isUsing2DAlignmentMethod)
                _alignment.alignOn2DImage(reconstructedImages, false);
              else
                _alignment.alignOnImage(reconstructedImages, false);
            }
            else
            {
              if (isUsing2DAlignmentMethod)
                _alignment.alignOn2DImage(reconstructedImages, sendAlignmentInfoToIrps);
              else
                _alignment.alignOnImage(reconstructedImages, sendAlignmentInfoToIrps);
            }
          }
        }
        catch (XrayTesterException ex)
        {
          throw ex;
        }
        finally
        {
          if (abortedDuringGet.getValue() == false)
          {
            reconstructedImages.decrementReferenceCount();
          }
        }
      }
    }
    else
    {
      _imageAcquisitionEngine.applyAlignmentResult(subProgram.getTestProgram().getTestSubProgramById(subProgram.getSubProgramRefferenceIdForAlignment()).getAggregateRuntimeAlignmentTransform(), subProgram.getId());
    }
  }

  /**
   * @author George A. David
   */
  private void processIncomingAlignmentImages(final TestSubProgram subProgram, final BooleanLock waitForAlignmentToCompleteLock, final boolean sendAlignmentInfoToIrps)
  {
    Assert.expect(subProgram != null);
    _acquireIncomingImagesThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          handleIncomingAlignmentImages(subProgram, sendAlignmentInfoToIrps);
        }
        catch (XrayTesterException ex)
        {
          abort(ex);
        }
        catch (Throwable ex)
        {
          Assert.logException(ex);
        }
        finally
        {
          waitForAlignmentToCompleteLock.setValue(true);
        }
      }
    });
  }

  /**
   * @author George A. David
   * @author Tan Hock Zoon
   */
  private void handleIncomingVerificationImages(final TestSubProgram subProgram, final boolean isEstimatingSurface)
  {
    Assert.expect(subProgram != null);

    // start the thread to save images when available
    _acquireIncomingImagesThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          int numRegions = subProgram.getVerificationRegions().size();
          for (int i = 0; i < numRegions; ++i)
          {
            BooleanRef abortedDuringGet = new BooleanRef(false);
            ReconstructedImages reconstructedImages = _imageAcquisitionEngine.getReconstructedImages(
                abortedDuringGet);

            if (abortedDuringGet.getValue())
            {
              _aborted = true;
              return;
            }

            ReconstructionRegion region = reconstructedImages.getReconstructionRegion();
            
            //Khaw Chek Hau - XCR2285: 2D Alignment on v810
            SliceNameEnum slice = SliceNameEnum.PAD;
            if (subProgram.getTestProgram().isManual2DAlignmentInProgress() && _xRayTester.isSimulationModeOn() == false)
              slice = SliceNameEnum.CAMERA_0;
            
            if (isEstimatingSurface)
            {
              //Assert.expect(reconstructedImages.getReconstructedSlices().size() == 1); 
              Object previous = _reconstructionRegionToZheightInNanosMap.put(region, reconstructedImages.getReconstructedSlice(slice).getHeightInNanometers());   
              Assert.expect(previous == null);
            }
            else
            {
              for (ReconstructedSlice reconstructedSlice: reconstructedImages.getReconstructedSlices())
              {
                if (reconstructedSlice.getSliceNameEnum().equals(slice))
                {
                  saveVerificationImages(reconstructedImages);
                }
              }
            }
            reconstructedImages.decrementReferenceCount();

            if (_aborted == false)
              _imageGenerationObservable.stateChanged(new ImageGenerationEvent(ImageGenerationEventEnum.VERIFICATION_IMAGE_ACQUIRED));

            if (_imageAcquisitionEngine.isAbortInProgress() == false)
              _imageAcquisitionEngine.reconstructionRegionComplete(region, false);
            _imageAcquisitionEngine.freeReconstructedImages(region);
          }
        }
        catch (XrayTesterException ex)
        {
          abort(ex);
        }
        catch (Throwable ex)
        {
          Assert.logException(ex);
        }
        finally
        {
          _waitForImagesToBeSavedLock.setValue(true);
        }
      }
    });
  }

  /**
   * @author George A. David
   */
  private void abort(XrayTesterException xex)
  {
    if(_exception != null)
      return;

    _exception = xex;
    try
    {
      abort();
    }
    catch (XrayTesterException ex)
    {
      // we already have an exception, let's
      // just print this out.
      ex.printStackTrace();
    }
  }

  /**
   * @author George A. David
   */
  public void abort() throws XrayTesterException
  {
    Assert.expect(_imageAcquisitionEngine != null);

    if(_isGeneratingImages)
    {
      _aborted = true;

      try
      {
        _opticalImageAcquisitionEngine.abort();  
        _imageAcquisitionEngine.abort();
        _imageSetGenerator.abort();
      }
      catch (XrayTesterException ex)
      {
        if(_exception == null)
        {
          _exception = ex;
          throw ex;
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  public synchronized void update(Observable observable, Object argument)
  {
    // not needed for 1.0
//    if(observable instanceof FocusResultsObservable)
//    {
//
//    }
//    else
      Assert.expect(false, "did not recognize obsevable: " + observable.getClass());
  }
  
//  /**
//   * @author Cheah Lee Herng
//   */
//  private void performSurfaceMap(TestProgram testProgram, TestExecutionTimer testExecutionTimer) throws XrayTesterException
//  {
//      Assert.expect(testProgram != null);
//      Assert.expect(testExecutionTimer != null);
//      
//      final TestProgram finalTestProgram = testProgram;
//      final TestExecutionTimer finalTestExecutionTimer = testExecutionTimer;
//      
//      _acquiringOpticalImages.setValue(true);
//      _opticalImageAcquisitionWorkerThread.invokeLater(new Runnable()
//      {
//        public void run()
//        {
//            try
//            {               
//                _opticalImageAcquisitionEngine.acquireProductionOpticalImages(finalTestProgram, false, finalTestExecutionTimer);
//            }
//            catch (XrayTesterException th)
//            { 
//                _abortOpticalImageAcquisitionFlag.setValue(true);
//                abort(th);
//            }
//            finally
//            {
//              if (_opticalImageAcquisitionEngine.isUserAborted())
//                  _abortOpticalImageAcquisitionFlag.setValue(true);
//                
//              _acquiringOpticalImages.setValue(false);
//            }
//        }
//      });
//      
//      try
//      {          
//          _acquiringOpticalImages.waitUntilFalse();
//      }
//      catch(InterruptedException ex)
//      {
//          // Do nothing
//      }
//  }
    /**
   * @author Jack Hwee
   * To filter the testSubProgram pass into acquireAlignmentAndOfflineImages() during customize fine tuning image collection, which cause hang.
   */
  private boolean setupFilteredTestProgramForFineTuningImage(ImageSetData imageSetData, TestProgram testProgram)
  {
    MagnificationTypeEnum magnificationTypeEnum = testProgram.getAllInspectableTestSubPrograms().get(0).getMagnificationType();
    return setupFilteredTestProgramForFineTuningImage(imageSetData, testProgram, magnificationTypeEnum);    
  }  
  
   /**
   * @author Jack Hwee
   * To filter the testSubProgram pass into acquireAlignmentAndOfflineImages() during customize fine tuning image collection, which cause hang.
   */
  private boolean setupFilteredTestProgramForFineTuningImage(ImageSetData imageSetData, TestProgram testProgram, MagnificationTypeEnum magnificationTypeEnum)
  {
    Assert.expect(imageSetData != null);
    Assert.expect(testProgram != null);
 
    Board board = testProgram.getProject().getPanel().getBoards().get(0);
   
    // to handle board based alignment
    if (imageSetData.hasBoardInstance())
      board = imageSetData.getBoardInstanceToAcquireImagesAgainst();
    else if (imageSetData.getImageSetTypeEnum().equals(ImageSetTypeEnum.BOARD))
      board = imageSetData.getBoardToAcquireImagesFor();
    
    boolean isPanelBasedAlignment =  testProgram.getProject().getPanel().getPanelSettings().isPanelBasedAlignment();
    boolean isBoardBasedAlignmentWithOneBoardInstance = (imageSetData.collectImagesOnAllBoardInstances() == false && isPanelBasedAlignment == false && imageSetData.collectImagesOnEntirePanel() == false);
    
    List<TestSubProgram> finalTestSubPrograms = new ArrayList<TestSubProgram>();
    List<TestSubProgram> alignmentTestSubPrograms = new ArrayList<TestSubProgram>();
    List<TestSubProgram> inspectionTestSubPrograms = new ArrayList<TestSubProgram>();
    
    if (isBoardBasedAlignmentWithOneBoardInstance)
    {
      for(TestSubProgram subProgram : testProgram.getAllInspectableTestSubPrograms())
      {
        if(subProgram.isSubProgramPerformAlignment() && 
           subProgram.getMagnificationType().equals(magnificationTypeEnum) && 
           subProgram.getBoard().equals(board) &&
           alignmentTestSubPrograms.contains(subProgram) == false)
        {
          alignmentTestSubPrograms.add(subProgram);
        }
      }
    }
    else
    {
      for(TestSubProgram subProgram : testProgram.getAllInspectableTestSubPrograms())
      {
        if(subProgram.isSubProgramPerformAlignment() && 
           subProgram.getMagnificationType().equals(magnificationTypeEnum) &&
           alignmentTestSubPrograms.contains(subProgram) == false)
        {
          alignmentTestSubPrograms.add(subProgram);
        }
      }
    }
    
    if ((imageSetData.getImageSetTypeEnum().equals(ImageSetTypeEnum.SUBTYPE)))
    {
      Subtype selectedSubtype = imageSetData.getSubtypeToAcquireImagesFor();
      JointTypeEnum jointTypeEnum = selectedSubtype.getJointTypeEnum();
      
      if (isBoardBasedAlignmentWithOneBoardInstance)
      {     
        for(TestSubProgram subProgram : testProgram.getAllInspectableTestSubPrograms())
        {
          if(subProgram.getMagnificationType().equals(magnificationTypeEnum) && subProgram.hasSubtype(selectedSubtype) 
               && selectedSubtype.getJointTypeEnum().equals(jointTypeEnum)
               && subProgram.getBoard().equals(board)
               && inspectionTestSubPrograms.contains(subProgram) == false)
          {
            inspectionTestSubPrograms.add(subProgram);
            testProgram.addFilter(selectedSubtype);
          }
        }
      }
      else
      {
        for(TestSubProgram subProgram : testProgram.getAllInspectableTestSubPrograms())
        {
          if(subProgram.getMagnificationType().equals(magnificationTypeEnum) && subProgram.hasSubtype(selectedSubtype) 
               && selectedSubtype.getJointTypeEnum().equals(jointTypeEnum)
               && inspectionTestSubPrograms.contains(subProgram) == false)
          {
            inspectionTestSubPrograms.add(subProgram);
            testProgram.addFilter(selectedSubtype);
          }
        } 
      }
    }
    else if ((imageSetData.getImageSetTypeEnum().equals(ImageSetTypeEnum.JOINT_TYPE)))
    {
      JointTypeEnum jointTypeEnum = imageSetData.getJointTypeEnumToAcquireImagesFor();
      List<Subtype> subtypes = imageSetData.getBoardInstanceToAcquireImagesAgainst().getInspectedSubtypes(jointTypeEnum);
     
      if (isBoardBasedAlignmentWithOneBoardInstance)
      {
        for(TestSubProgram subProgram : testProgram.getAllInspectableTestSubPrograms())
        {
          for (Subtype subtype : subtypes)
          {
            if(subProgram.getMagnificationType().equals(magnificationTypeEnum) && 
               subProgram.hasSubtype(subtype) && 
               subProgram.getBoard().equals(board) &&
               inspectionTestSubPrograms.contains(subProgram) == false)
            {
              inspectionTestSubPrograms.add(subProgram);
              testProgram.addFilter(jointTypeEnum);
            }
          }
        }
      }
      else
      {
        for(TestSubProgram subProgram : testProgram.getAllInspectableTestSubPrograms())
        {
          for (Subtype subtype : subtypes)
          {
            if(subProgram.getMagnificationType().equals(magnificationTypeEnum) && 
               subProgram.hasSubtype(subtype) &&
               inspectionTestSubPrograms.contains(subProgram) == false)
            {
              inspectionTestSubPrograms.add(subProgram);
              testProgram.addFilter(jointTypeEnum);
            }
          }
        } 
      }
    }
    else if ((imageSetData.getImageSetTypeEnum().equals(ImageSetTypeEnum.COMPONENT)))
    {
      ComponentType componentType = imageSetData.getComponentTypeToAcquireImagesFor();
      List<Component> components = componentType.getComponents();
     
      if (isBoardBasedAlignmentWithOneBoardInstance)
      {
        for(TestSubProgram subProgram : testProgram.getAllInspectableTestSubPrograms())
        {
          for(Component component : components)
          {
            if(subProgram.getMagnificationType().equals(magnificationTypeEnum) && 
               subProgram.hasComponent(component) && subProgram.getBoard().equals(board) &&
               inspectionTestSubPrograms.contains(subProgram) == false)
            {
              inspectionTestSubPrograms.add(subProgram);
              testProgram.addFilter(component);
            }
          }
        }
      }
      else
      {
        for(TestSubProgram subProgram : testProgram.getAllInspectableTestSubPrograms())
        {
          for(Component component : components)
          {
            if(subProgram.getMagnificationType().equals(magnificationTypeEnum) && 
               subProgram.hasComponent(component) &&
               inspectionTestSubPrograms.contains(subProgram) == false)
            {
              inspectionTestSubPrograms.add(subProgram);
              testProgram.addFilter(component);
            }
          }
        } 
      }
    }
    else if ((imageSetData.getImageSetTypeEnum().equals(ImageSetTypeEnum.PIN)))
    {
      PadType padType = imageSetData.getPadTypeToAcquireImagesFor();
     
      if (isBoardBasedAlignmentWithOneBoardInstance)
      {
        for(TestSubProgram subProgram : testProgram.getAllInspectableTestSubPrograms())
        {
          for(Pad pad : padType.getPads())
          {
            if(subProgram.getMagnificationType().equals(magnificationTypeEnum) && 
               subProgram.getBoard().equals(board) &&
               subProgram.hasInspectionPad(pad) &&
               inspectionTestSubPrograms.contains(subProgram) == false)
            {
              inspectionTestSubPrograms.add(subProgram);
              testProgram.addFilter(pad);
            }
          }
        }
      }
      else
      {
        for(TestSubProgram subProgram : testProgram.getAllInspectableTestSubPrograms())
        {
          for(Pad pad : padType.getPads())
          {
            if(subProgram.getMagnificationType().equals(magnificationTypeEnum) &&                
               subProgram.hasInspectionPad(pad) &&
               inspectionTestSubPrograms.contains(subProgram) == false)
            {
              inspectionTestSubPrograms.add(subProgram);
              testProgram.addFilter(pad);
            }
          }
        }  
      }
    }
    else if ((imageSetData.getImageSetTypeEnum().equals(ImageSetTypeEnum.PANEL)) || 
             (imageSetData.getImageSetTypeEnum().equals(ImageSetTypeEnum.BOARD)))
    {
      if(testProgram.getProject().getPanel().getPanelSettings().isPanelBasedAlignment()== false &&
         imageSetData.getImageSetTypeEnum().equals(ImageSetTypeEnum.BOARD))
      {
        for(TestSubProgram subProgram : testProgram.getAllInspectableTestSubPrograms())
        {
          if(subProgram.getBoard().equals(board) && 
             subProgram.getMagnificationType().equals(magnificationTypeEnum) &&
             inspectionTestSubPrograms.contains(subProgram) == false)
          {
            inspectionTestSubPrograms.add(subProgram);
            testProgram.addFilter(board);
          }
        }
      }
      else
      {
        for(TestSubProgram subProgram : testProgram.getAllInspectableTestSubPrograms())
        {
          if(subProgram.getMagnificationType().equals(magnificationTypeEnum) &&
             inspectionTestSubPrograms.contains(subProgram) == false)
          {
            inspectionTestSubPrograms.add(subProgram);
          }
        }
      }
    }
    
    // Sync TestSubProgram
    boolean containAlignmentTestSubProgram = false;
    boolean isLongProgram = testProgram.isLongProgram();
    {
      if (isPanelBasedAlignment)
      {
        if (isLongProgram)
        {
          boolean isRightPanelContainAlignment  = false;
          boolean isLeftPanelContainAlignment   = false;

          boolean hasRightTestSubProgram = false;
          boolean hasLeftTestSubProgram  = false;
          
          for(TestSubProgram testSubProgram : inspectionTestSubPrograms)
          {
            if (testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
              hasRightTestSubProgram = true;
            else if (testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
              hasLeftTestSubProgram = true;
            else
              Assert.expect(false, "Invalid PanelLocationInSystemEnum");
          }
          
          for(TestSubProgram testSubProgram : inspectionTestSubPrograms)
          {
            if (testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT) && 
                testSubProgram.isSubProgramPerformAlignment() &&
                testSubProgram.getMagnificationType().equals(magnificationTypeEnum) &&
                isRightPanelContainAlignment == false)
            {
              isRightPanelContainAlignment = true;
            }

            if (testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT) && 
                testSubProgram.isSubProgramPerformAlignment() &&
                testSubProgram.getMagnificationType().equals(magnificationTypeEnum) &&
                isLeftPanelContainAlignment == false)
            {
              isLeftPanelContainAlignment = true;
            }
            
            if (hasRightTestSubProgram && hasLeftTestSubProgram)
            {
              if (isRightPanelContainAlignment && isLeftPanelContainAlignment)
              {
                containAlignmentTestSubProgram = true;
                break;
              }
            }
            else if (hasRightTestSubProgram && isRightPanelContainAlignment)
            {
              containAlignmentTestSubProgram = true;
              break;
            }
            else if (hasLeftTestSubProgram && isLeftPanelContainAlignment)
            {
              containAlignmentTestSubProgram = true;
              break;
            }
          }
        }
        else
        {
          for(TestSubProgram testSubProgram : inspectionTestSubPrograms)
          {
            if (testSubProgram.isSubProgramPerformAlignment() && 
                testSubProgram.getMagnificationType().equals(magnificationTypeEnum))
            {
              containAlignmentTestSubProgram = true;
              break;
            }
          }
        }
      }
      else
      {
        List<Board> processBoards = new ArrayList<Board>();
        if (isBoardBasedAlignmentWithOneBoardInstance)
          processBoards.add(board);
        else
          processBoards.addAll(testProgram.getProject().getPanel().getBoards());
        
        boolean allBoardContainAlighmentTestSubProgram = true;
        for(Board currentBoard : processBoards)
        {
          boolean boardContainAlignmentTestSubProgram = false;
          if (isLongProgram)
          {
            boolean isRightPanelContainAlignment  = false;
            boolean isLeftPanelContainAligment    = false;

            boolean hasRightTestSubProgram = false;
            boolean hasLeftTestSubProgram  = false;

            for(TestSubProgram testSubProgram : inspectionTestSubPrograms)
            {
              // XCR-3881 System Crash When Collect Fine Tuning Image on Multi-Long Board
              if (hasRightTestSubProgram && hasLeftTestSubProgram)
                break;
              
              if (testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT) &&
                  testSubProgram.getBoard().equals(currentBoard))
              {
                hasRightTestSubProgram = true;
              }
              else if (testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT) &&
                       testSubProgram.getBoard().equals(currentBoard))
              {
                hasLeftTestSubProgram = true;
              }
            }
            
            for(TestSubProgram testSubProgram : inspectionTestSubPrograms)
            {
              if (testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT) && 
                  testSubProgram.isSubProgramPerformAlignment() &&
                  testSubProgram.getMagnificationType().equals(magnificationTypeEnum) &&
                  testSubProgram.getBoard().equals(currentBoard))
              {
                isRightPanelContainAlignment = true;
              }

              if (testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT) && 
                  testSubProgram.isSubProgramPerformAlignment() &&
                  testSubProgram.getMagnificationType().equals(magnificationTypeEnum) &&
                  testSubProgram.getBoard().equals(currentBoard))
              {
                isLeftPanelContainAligment = true;
              }
              
              if (hasRightTestSubProgram && hasLeftTestSubProgram)
              {
                if (isRightPanelContainAlignment && isLeftPanelContainAligment)
                {
                  boardContainAlignmentTestSubProgram = true;
                  break;
                }
              }
              else if (hasRightTestSubProgram && isRightPanelContainAlignment)
              {
                boardContainAlignmentTestSubProgram = true;
                break;
              }
              else if (hasLeftTestSubProgram && isLeftPanelContainAligment)
              {
                boardContainAlignmentTestSubProgram = true;
                break;
              }
            }
          }
          else
          {
            for(TestSubProgram testSubProgram : inspectionTestSubPrograms)
            {
              if (testSubProgram.isSubProgramPerformAlignment() && 
                  testSubProgram.getMagnificationType().equals(magnificationTypeEnum) &&
                  testSubProgram.getBoard().equals(currentBoard))
              {
                boardContainAlignmentTestSubProgram = true;
                break;
              }
            }
          }
          
          allBoardContainAlighmentTestSubProgram &= boardContainAlignmentTestSubProgram;
          if (allBoardContainAlighmentTestSubProgram == false)
            break;
        }
        containAlignmentTestSubProgram = allBoardContainAlighmentTestSubProgram;
      }
    }
    
    if (containAlignmentTestSubProgram == false)
    {
      for(TestSubProgram alignmentTestSubProgram : alignmentTestSubPrograms)
      {
        for(TestSubProgram inspectionTestSubProgram : inspectionTestSubPrograms)
        {
          if (inspectionTestSubProgram.getImageableRegionInNanoMeters().intersects(alignmentTestSubProgram.getImageableRegionInNanoMeters()) &&
              inspectionTestSubProgram.getPanelLocationInSystem().equals(alignmentTestSubProgram.getPanelLocationInSystem()) &&
              inspectionTestSubProgram.getMagnificationType().equals(alignmentTestSubProgram.getMagnificationType()) &&
              finalTestSubPrograms.contains(alignmentTestSubProgram) == false)
          {
            finalTestSubPrograms.add(alignmentTestSubProgram);
          }
        }
      }
    }
    //XCR-3467, System Crash When Collect Mixed Mag Fine Tuning Images
    boolean hasSubProgramToGenerate = inspectionTestSubPrograms.isEmpty() == false;
    finalTestSubPrograms.addAll(inspectionTestSubPrograms);
    for(TestSubProgram testSubProgram : finalTestSubPrograms)
    {
      testProgram.addFilter(testSubProgram);
    }
    
    // Clean up
    finalTestSubPrograms.clear();
    alignmentTestSubPrograms.clear();
    inspectionTestSubPrograms.clear();
    
    return hasSubProgramToGenerate;
  }
  
   /**
   * @author Cheah Lee Herng
   */
  private void performAlignmentSurfaceMap(TestProgram testProgram, TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
      Assert.expect(testProgram != null);
      Assert.expect(testExecutionTimer != null);
      
      final TestProgram finalTestProgram = testProgram;
      final TestExecutionTimer finalTestExecutionTimer = testExecutionTimer;
      _acquiringAlignmentOpticalImages.setValue(true);
      _alignmentOpticalImageAcquisitionWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            _opticalImageAcquisitionEngine.acquireAlignmentOpticalImages(finalTestProgram, false, finalTestExecutionTimer);
          }
          catch (XrayTesterException th)
          {
            _abortAlignmentOpticalImageAcquisitionFlag.setValue(true);
            abort(th);
          }
          finally
          {
            if (_opticalImageAcquisitionEngine.isUserAborted())
            {
              _abortAlignmentOpticalImageAcquisitionFlag.setValue(true);
            }

            _acquiringAlignmentOpticalImages.setValue(false);
          }
        }
      });
      
      try
      {          
          _acquiringAlignmentOpticalImages.waitUntilFalse();
      }
      catch(InterruptedException ex)
      {
          // Do nothing
      }
  }
}
