package com.axi.v810.business;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * This excception is thrown if there is no surface data for a panel or board
 *
 * @author Andy Mechtenberg
 */
public class NoSurfaceMapBusinessException extends BusinessException
{
  /**
   * @author Bill Darbie
   */
  public NoSurfaceMapBusinessException()
  {
    super(new LocalizedString("EX_NO_SURFACE_MAP_KEY", null));
  }
}
