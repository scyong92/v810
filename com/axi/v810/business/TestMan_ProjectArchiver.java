package com.axi.v810.business;

import java.io.*;
import java.util.*;

import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.util.*;

/**
 * debugging aid
 *
 * @author Greg Loring
 */
public class TestMan_ProjectArchiver
{
  // in/out for the "console"
  private BufferedReader _in = new BufferedReader(new InputStreamReader(System.in));
  private PrintStream _out = System.out;

  private String _lastDestinationFilename = null;

  /**
   * console app for rapid debugging
   *
   * @author Greg Loring
   */
  public static void main(String[] args) throws Exception
  {
    (new TestMan_ProjectArchiver()).run();
  }

  /**
   * @author Greg Loring
   */
  private TestMan_ProjectArchiver()
  {
  }

  /**
   * run console app for rapid debugging.  this should also serve as an example of how
   *  any user interface would interact with a ProjectArchiver object.
   *
   * @author Greg Loring
   */
  private void run() throws Exception
  {
    do
    {
      ProjectArchiver archiver = new ProjectArchiver();

      archiver.setProjectName(select("project", FileName.getProjectNames()));
//      printFileAndDirectoryNames(archiver);

      if (archiver.doesAlgorithmLearningExist())
      {
        archiver.setIncludeAlgorithmLearning(readYesNo("include algorithm learning files"));
//        printFileAndDirectoryNames(archiver);
      }
      else
      {
        _out.println("no algorithm learning files found");
      }

      if (archiver.doVerificationImagesExist())
      {
        archiver.setIncludeVerificationImages(readYesNo("include verification images"));
//        printFileAndDirectoryNames(archiver);
      }
      else
      {
        _out.println("no verification images found");
      }

      // inspection images (optionally limited by component or joint)
      BooleanRef abortedDuringLoad = new BooleanRef();
      Project project = Project.load(archiver.getProjectName(), abortedDuringLoad);
      Assert.expect(abortedDuringLoad.getValue() == false);
      Panel panel = project.getPanel();
      Component component = select("component", prependNull(panel.getComponents()));
      if (component != null)
      {
        Pad jointPad = select("joint", prependNull(component.getPads()));
        if (jointPad != null)
        {
          archiver.setJointPad(jointPad);
        }
        else
        {
          archiver.setComponent(component);
        }
      }
      List<ImageSetData> imageSets = archiver.getAllowableInspectionImageSets();
      if (imageSets.size() > 0)
      {
        for (ImageSetData imageSet : imageSets)
        {
          String name = imageSet.getImageSetName();
          String descr = imageSet.getSystemDescription();
          if (readYesNo("include " + name + '(' + descr + ')'))
          {
            archiver.addInspectionImageSet(imageSet);
          }
        }
      }
      else
      {
        _out.println("no verification images found");
      }
      printFileAndDirectoryNames(archiver);

      archiver.setDestinationFilename(readDestinationFilename());

      // don't do this, 'cuz its just looks like it would be too much trouble to clean up
//      archiver.setAddToProjectDatabase(readYesNo("add to ProjectDatabase"));

      _out.print("archiving...");
      archiver.archive();
      _out.println("DONE");

      _lastDestinationFilename = archiver.getDestinationFilename();
    }
    while (readYesNo("another"));
  }

  /**
   * @author Greg Loring
   */
  private void printFileAndDirectoryNames(ProjectArchiver archiver) throws DatastoreException
  {
    _out.println("files and directories to archive:");
    for (String fileOrDirectoryName : archiver.getFileAndDirectoryNames())
    {
      _out.print("  ");
      _out.println(fileOrDirectoryName);
    }

  }

  /**
   * simulate a javax.swing.JFileChooser
   *
   * @author Greg Loring
   */
  private String readDestinationFilename() throws IOException
  {
    while (true)
    {
      _out.print("destination file");
      if (_lastDestinationFilename != null)
        _out.print(" [" + _lastDestinationFilename + "]");
      _out.print("? ");

      String line = readTrimmedLine();
      if (line.equals(""))
        return _lastDestinationFilename;

      File file = new File(line);
      if (file.exists())
      {
        if (file.isDirectory())
        {
          _out.println(line + " is a directory");
          continue;
        }
        if (readYesNo("overwrite " + line))
          return line;
        else
          continue;
      }
      File directory = file.getParentFile();
      if ((directory == null) || (directory.exists() && directory.isDirectory()))
      {
        if (readYesNo("write " + line))
          return line;
        else
          continue;
      }
      _out.println(line + " doesn't exist and " + directory.getPath() + " is not an existing directory");
    }
  }

  /**
   * simulate a javax.swing.JCheckBox
   *
   * @author Greg Loring
   */
  private boolean readYesNo(String prompt) throws IOException
  {
    while (true)
    {
      _out.print(prompt + " [y/N]? ");
      String line = readTrimmedLine().toLowerCase();
      if (line.equals("y") || line.equals("yes"))
      {
        return true;
      }
      if (line.equals("") || line.equals("n") || line.equals("no"))
      {
        return false;
      }
    }
  }

  /**
   * @author Greg Loring
   */
  private <T> List<T> prependNull(List<T> options)
  {
    List<T> results = new ArrayList<T>(1 + options.size());
    results.add(null);
    results.addAll(options);
    return results;
  }

  /**
   * simulate a javax.swing.JComboBox (or HTML <select>)
   *
   * @author Greg Loring
   */
  private <T> T select(String prompt, List<T> options) throws IOException
  {
    int i = 0;
    for (T option : options)
    {
      _out.println(i++ + ": " + ((option == null) ? "<NONE>" : option));
    }
    i = readUnsignedLessThan("which " + prompt, options.size());
    return options.get(i);
  }

  /**
   * @author Greg Loring
   */
  private int readUnsignedLessThan(String prompt, int end) throws IOException
  {
    while (true)
    {
      _out.print(prompt + " [#]? ");
      String line = readTrimmedLine();
      try
      {
        int result = Integer.parseInt(line);
        if ((result >= 0) && (result < end))
        {
          return result;
        }
      }
      catch (NumberFormatException nfx)
      {
        // just ask again
      }
    }
  }

  /**
   * @author Greg Loring
   */
  private String readTrimmedLine() throws IOException
  {
    String line = _in.readLine();
    if (line == null)
    {
      System.err.println("unexpected EOF");
      System.exit(-1);
    }
    line = line.trim();
    return line;
  }

}
