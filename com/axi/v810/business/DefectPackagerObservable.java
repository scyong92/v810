package com.axi.v810.business;

import java.util.*;

import com.axi.util.*;
/**
 *
 * @author chin-seong.kee
 */
public class DefectPackagerObservable extends Observable
{
  private static DefectPackagerObservable _instance;

  /**
   * @author George A. David
   */
  private DefectPackagerObservable()
  {
    // do nothing
  }

  /**
   * @author George A. David
   */
  public static synchronized DefectPackagerObservable getInstance()
  {
    if (_instance == null)
      _instance = new DefectPackagerObservable();

    return _instance;
  }


  /**
   * @author George A. David
   */
  public void stateChanged(DefectPackagerTaskEventEnum defectPackagerEventEnum)
  {
    Assert.expect(defectPackagerEventEnum != null);

    setChanged();
    notifyObservers(defectPackagerEventEnum);
  }
}