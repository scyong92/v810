package com.axi.v810.business.userAccounts;

/**
 * This class defines events used created by user account activities
 * @author George Booth
 */
public class UserAccountsEventEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static UserAccountsEventEnum USER_ACCOUNT_ADDED = new UserAccountsEventEnum(++_index);
  public static UserAccountsEventEnum USER_ACCOUNT_UPDATED = new UserAccountsEventEnum(++_index);
  public static UserAccountsEventEnum USER_ACCOUNT_DELETED = new UserAccountsEventEnum(++_index);

  /**
   * @author George Booth
   */
  private UserAccountsEventEnum(int id)
  {
    super(id);
  }
}
