package com.axi.v810.business.userAccounts;

import com.axi.util.*;
import java.io.Serializable;

/**
 *
 * <p>Title: UserAccountInfo</p>
 *
 * <p>Description: One record of user login account information.</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class UserAccountInfo implements Serializable
{
  private String _accountName;
  private String _fullName;
  private String _password;
  private UserTypeEnum _accountType;
  // an Agilent account not visible to the user
  private boolean _hiddenAccount = false;
  // an Agilent account where the user can only change the password
  private boolean _internalAccount = false;

  /**
   * Empty constructor
   * @author George Booth
   */
  public UserAccountInfo()
  {
    // do nothing
  }

  /**
   * constructor for a normal customer account
   * @author George Booth
   */
  public UserAccountInfo(String accountName, String fullName,
                         String password, UserTypeEnum accountType)
  {
    setAccountName(accountName);
    setFullName(fullName);
    setPassword(password);
    setAccountType(accountType);
    setHiddenAccount(false);
    setInternalAccount(false);
  }

  /**
   * constructor for an internal Agilent account
   * @author George Booth
   */
  public UserAccountInfo(String accountName, String fullName,
                         String password, UserTypeEnum accountType, boolean hidden, boolean internal)
  {
    setAccountName(accountName);
    setFullName(fullName);
    setPassword(password);
    setAccountType(accountType);
    setHiddenAccount(hidden);
    setInternalAccount(internal);
  }

  /**
   * copy constructor
   * @author George Booth
   */
  public UserAccountInfo(UserAccountInfo account)
  {
    setAccountName(account.getAccountName());
    setFullName(account.getFullName());
    setPassword(account.getPassword());
    setAccountType(account.getAccountType());
    setHiddenAccount(account.isHiddenAccount());
    setInternalAccount(account.isInternalAccount());
  }

  /**
   * @author George Booth
   */
  public void updateAccountInfo(UserAccountInfo newAccountInfo)
  {
    setAccountName(newAccountInfo.getAccountName());
    setFullName(newAccountInfo.getFullName());
    setPassword(newAccountInfo.getPassword());
    setAccountType(newAccountInfo.getAccountType());
  }

  /**
   * @author George Booth
   */
  public boolean isValidAccount()
  {
    return _accountName != null && _accountName.length() > 0 &&
        _fullName != null && _fullName.length() > 0 &&
        _password != null && _accountType != null;
  }

  /**
   * @author George Booth
   */
  public void setAccountName(String accountName)
  {
    Assert.expect(accountName != null);

    _accountName = accountName;
  }

  /**
   * @author George Booth
   */
  public String getAccountName()
  {
    Assert.expect(_accountName != null);

    return _accountName;
  }

  /**
   * @author George Booth
   */
  public void setFullName(String fullName)
  {
    Assert.expect(fullName != null);

    _fullName = fullName;
  }

  /**
   * @author George Booth
   */
  public String getFullName()
  {
    Assert.expect(_fullName != null);

    return _fullName;
  }

  /**
   * @author George Booth
   */
  public void setPassword(String password)
  {
    Assert.expect(password != null);

    _password = password;
  }

  /**
   * @author George Booth
   */
  public String getPassword()
  {
    Assert.expect(_password != null);

    return _password;
  }

  /**
   * @author George Booth
   */
  public void setAccountType(UserTypeEnum accountType)
  {
    Assert.expect(accountType != null);

    _accountType = accountType;
  }

  /**
   * @author George Booth
   */
  public UserTypeEnum getAccountType()
  {
    Assert.expect(_accountType != null);

    return _accountType;
  }

  /**
   * @author George Booth
   */
  public void setHiddenAccount(boolean hidden)
  {
    _hiddenAccount = hidden;
  }

  /**
   * @author George Booth
   */
  public boolean isHiddenAccount()
  {
    return _hiddenAccount;
  }

  /**
   * @author George Booth
   */
  public void setInternalAccount(boolean internal)
  {
    _internalAccount = internal;
  }

  /**
   * @author George Booth
   */
  public boolean isInternalAccount()
  {
    return _internalAccount;
  }

  public String toString()
  {
    return _accountName + ", " + _fullName + ", " + getPassword() + ", " +
           _accountType.getName() + ", " + _hiddenAccount + ", " + _internalAccount;
  }
}
