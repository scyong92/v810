package com.axi.v810.business.userAccounts;

import java.util.*;

import com.axi.util.*;

/**
 * This class is used to notify any Observers that the user accounts have been changed.
 *
 * @author George Booth
 */
public class UserAccountsObservable extends Observable
{
  private static UserAccountsObservable _instance = null;

  /**
   * @author George Booth
   */
  private UserAccountsObservable()
  {
    // do nothing
  }

  /**
   * @author George Booth
   */
  public static synchronized UserAccountsObservable getInstance()
  {
    if (_instance == null)
      _instance = new UserAccountsObservable();

    return _instance;
  }

  /**
   * notify all observers that user accounts have been changed
   * @author George Booth
   */
  public void stateChanged(Object source, UserAccountsEventEnum userAccountEventEnum)
  {
    Assert.expect(source != null);
    Assert.expect(userAccountEventEnum != null);

    setChanged();
    notifyObservers(new UserAccountsEvent(source, userAccountEventEnum));
  }
}

