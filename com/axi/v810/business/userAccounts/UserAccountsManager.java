package com.axi.v810.business.userAccounts;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.userAccounts.*;

/**
 *
 * <p>Title: UserAccountsManager</p>
 *
 * <p>Description: Controls the information used to access the x6000 application.
 * The user accounts are used by the logon manager to grant login permission.</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class UserAccountsManager
{
  private static UserAccountsManager _instance = null;
  private java.util.ArrayList<UserAccountInfo> _userAccounts = null;
  private String _DEFAULT_PASSWORD = "nebulae";
  private static String _currentUserLoginName = "login";
  private static String _currentUserFullName = "login";
  private static UserTypeEnum _currentUserType = UserTypeEnum.LOGIN;

  private UserAccountsObservable _userAccountsObservable = UserAccountsObservable.getInstance();

  // rev level
  public static final int PREVIOUS_VERSION = 0;
  public static final int CURRENT_VERSION = 1;


  /**
   * This method gets the one and only instance of UserAccountsManager object
   *
   * @author George Booth
   */
  public synchronized static UserAccountsManager getInstance()
  {
    if (_instance == null)
      _instance = new UserAccountsManager();

    return _instance;
  }

  /**
   * @author George Booth
   */
  private UserAccountsManager()
  {
  }

  /**
   * @author George Booth
   */
  public void readUserAccounts() throws BusinessException
  {
    try
    {
      UserAccountsReader reader = new UserAccountsReader();
      _userAccounts = reader.readUserAccountsFile();
    }
    catch(BusinessException be)
    {
      // problem reading the file - set up default accounts
      createDefaultUserAccounts();
      throw be;
    }

    // if no accounts were read, set up default accounts
    if (_userAccounts.size() == 0)
    {
      createDefaultUserAccounts();
    }

//    System.out.println("\nAccounts");
//    for (UserAccountInfo userAccount : _userAccounts)
//    {
//      System.out.println(userAccount.toString());
//    }
  }

  /**
   * @author George Booth
   */
  void createDefaultUserAccounts()
  {
    _userAccounts = new java.util.ArrayList<UserAccountInfo>();
    // hidden internal account
    _userAccounts.add(new UserAccountInfo(UserTypeEnum.SERVICE_EXPERT.getName(),
                                          "v810 default service expert account",
                                          "service_expert",
                                          UserTypeEnum.SERVICE_EXPERT, true, true));
    // default accounts
    _userAccounts.add(new UserAccountInfo(UserTypeEnum.ADMINISTRATOR.getName(),
                                          "v810 default administrator account",
                                          _DEFAULT_PASSWORD,
                                          UserTypeEnum.ADMINISTRATOR, false, true));
    _userAccounts.add(new UserAccountInfo(UserTypeEnum.DEVELOPER.getName(),
                                          "v810 default developer account",
                                          _DEFAULT_PASSWORD,
                                          UserTypeEnum.DEVELOPER, false, true));
    _userAccounts.add(new UserAccountInfo(UserTypeEnum.OPERATOR.getName(),
                                          "v810 default operator account",
                                          _DEFAULT_PASSWORD,
                                          UserTypeEnum.OPERATOR, false, true));
    _userAccounts.add(new UserAccountInfo(UserTypeEnum.SERVICE.getName(),
                                          "v810 default service account",
                                          _DEFAULT_PASSWORD,
                                          UserTypeEnum.SERVICE, false, true));
    // sample account
    _userAccounts.add(new UserAccountInfo("sample user",
                                          "sample user account",
                                          _DEFAULT_PASSWORD,
                                          UserTypeEnum.OPERATOR));
  }

  /**
   * Return all user accounts except those hidden from users.
   * A copy constructor is used so that changes can be made externally without
   * affecting the local data.  Use updateAllAccounts() then writeAccounts() to
   * save external changes.
   * @author George Booth
   */
  public java.util.ArrayList<UserAccountInfo> getAccounts()
  {
    Assert.expect(_userAccounts != null);

    java.util.ArrayList<UserAccountInfo> userAccounts = new java.util.ArrayList<UserAccountInfo>();
    for (UserAccountInfo userAccount : _userAccounts)
    {
      if (userAccount.isHiddenAccount() == false)
      {
        userAccounts.add(new UserAccountInfo(userAccount));
      }
    }
    return userAccounts;
  }

  /**
   * @author George Booth
   */
  public boolean doesUserExist(String userName)
  {
    Assert.expect(_userAccounts != null);

    return getAccountIndex(userName) > -1;
  }

  /**
   * @author George Booth
   */
  int getAccountIndex(String userName)
  {
    Assert.expect(_userAccounts != null);

    int index = -1;
    for (UserAccountInfo userAccount : _userAccounts)
    {
      index++;
      if (userAccount.getAccountName().equalsIgnoreCase(userName))
      {
        return index;
      }
    }
    return -1;
  }

  /**
   * @author George Booth
   */
  public boolean isValidUserAccount(String userName, String password)
  {
    Assert.expect(_userAccounts != null);

    if (doesUserExist(userName))
    {
      for (UserAccountInfo userAccount : _userAccounts)
      {
        if (userAccount.getAccountName().equalsIgnoreCase(userName))
        {
          return userAccount.getPassword().equals(password);
        }
      }
    }
    return false;
  }

  /**
   * @author George Booth
   */
  public UserTypeEnum getUserTypeFromUserName(String userName)
  {
    Assert.expect(_userAccounts != null);

    if (doesUserExist(userName))
    {
      for (UserAccountInfo userAccount : _userAccounts)
      {
        if (userAccount.getAccountName().equalsIgnoreCase(userName))
          return userAccount.getAccountType();
      }
    }
    Assert.expect(false, "user account not found");
    return null;
  }

  /**
   * @author George Booth
   */
  public void clearCurrentUser()
  {
    _currentUserLoginName = "login";
    _currentUserFullName = "login";
    _currentUserType = UserTypeEnum.LOGIN;
  }

  /**
   * @author George Booth
   */
  public void setCurrentUser(String loginName)
  {
    Assert.expect(_userAccounts != null);
    Assert.expect(doesUserExist(loginName));

    for (UserAccountInfo userAccount : _userAccounts)
    {
      if (userAccount.getAccountName().equalsIgnoreCase(loginName))
      {
        _currentUserLoginName = userAccount.getAccountName();
        _currentUserFullName = userAccount.getFullName();
        _currentUserType = userAccount.getAccountType();
      }
    }
  }

  /**
   * @author George Booth
   */
  public static String getCurrentUserLoginName()
  {
    return _currentUserLoginName;
  }

  /**
   * @author George Booth
   */
  public static UserTypeEnum getCurrentUserType()
  {
    return _currentUserType;
  }

  /**
   * @author George Booth
   */
  public void addAccount(UserAccountInfo userAccount) throws DatastoreException
  {
    Assert.expect(userAccount != null);
    Assert.expect(_userAccounts != null);
    Assert.expect(doesUserExist(userAccount.getAccountName()) == false);

    _userAccounts.add(userAccount);
    writeUserAccounts();
    _userAccountsObservable.stateChanged(userAccount, UserAccountsEventEnum.USER_ACCOUNT_ADDED);
  }

  /**
   * @author George Booth
   */
  public void updateAccount(UserAccountInfo oldAccount, UserAccountInfo updatedAccount)  throws DatastoreException
  {
    Assert.expect(oldAccount != null);
    Assert.expect(_userAccounts != null);
    Assert.expect(doesUserExist(oldAccount.getAccountName()));

    int index = getAccountIndex(oldAccount.getAccountName());
    _userAccounts.get(index).updateAccountInfo(updatedAccount);
    writeUserAccounts();
    List<UserAccountInfo> userAccountsList = new ArrayList<UserAccountInfo>();
    userAccountsList.add(oldAccount);
    userAccountsList.add(updatedAccount);
    _userAccountsObservable.stateChanged(userAccountsList, UserAccountsEventEnum.USER_ACCOUNT_UPDATED);
  }

  /**
   * @author George Booth
   */
  public void deleteAccount(UserAccountInfo userAccount) throws DatastoreException
  {
    Assert.expect(userAccount != null);
    Assert.expect(_userAccounts != null);
    Assert.expect(doesUserExist(userAccount.getAccountName()));

    int index = getAccountIndex(userAccount.getAccountName());
    _userAccounts.remove(index);
    writeUserAccounts();
    _userAccountsObservable.stateChanged(userAccount, UserAccountsEventEnum.USER_ACCOUNT_DELETED);
  }

  /**
   * @author George Booth
   */
  public void writeUserAccounts() throws DatastoreException
  {
    Assert.expect(_userAccounts != null);

    UserAccountsWriter writer = new UserAccountsWriter();
    writer.writeUserAccountsFile(_userAccounts);
  }

}
