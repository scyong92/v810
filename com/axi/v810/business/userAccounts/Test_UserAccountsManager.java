package com.axi.v810.business.userAccounts;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.FileName;
import com.axi.v810.datastore.Directory;
import com.axi.v810.datastore.DatastoreException;

/**
 * This class provides a basic test harness for User Accounts on the X6000.
 * @author George Booth
 */
public class Test_UserAccountsManager extends UnitTest
{
  private UserAccountsManager _userAccountsManager = null;
  private java.util.ArrayList<UserAccountInfo> _userAccounts = null;

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @author George Booth
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    // make sure the accounts directory and file don't exist
    String accountsFilePath = FileName.getUserAccountsFullPath();
    if (FileUtilAxi.exists(accountsFilePath))
    {
      try
      {
        FileUtilAxi.delete(accountsFilePath);
      }
      catch (DatastoreException de)
      {
        System.out.println("delete account file DatastoreException = " + de.toString());
      }
    }
    String accountsDir = Directory.getUserAccountsDir();
    if (FileUtilAxi.exists(accountsDir))
    {
      try
      {
        FileUtilAxi.delete(accountsDir);
      }
      catch (DatastoreException de)
      {
        System.out.println("delete accountn dir DatastoreException = " + de.toString());
      }
    }

    //Trivial test to ensure that user accounts can be obtained
    try
    {
      _userAccountsManager = UserAccountsManager.getInstance();
      _userAccountsManager.readUserAccounts();
    }
    catch(Exception ex)
    {
      ex.printStackTrace(os);
    }
    // good stuff
    testCase1();
    // bad stuff
    testCase2();
  }

  /**
   * Test valid usage
   *
   * @author George Booth
   */
  void testCase1()
  {
    // list default accounts
    System.out.println("--- Default accounts");
    _userAccounts = _userAccountsManager.getAccounts();
    for (UserAccountInfo userAccount : _userAccounts)
    {
      System.out.println(userAccount.toString());
    }
    System.out.println("--- Adding accounts");
    // add some new accounts
    UserAccountInfo account1 = new UserAccountInfo("1", "First", "1", UserTypeEnum.ADMINISTRATOR);
    UserAccountInfo account2 = new UserAccountInfo("2", "Second", "22", UserTypeEnum.DEVELOPER);
    UserAccountInfo account3 = new UserAccountInfo("3", "Third", "3333", UserTypeEnum.OPERATOR);
    UserAccountInfo account4 = new UserAccountInfo("4", "Fourth", "4444", UserTypeEnum.SERVICE);
    try
    {
      _userAccountsManager.addAccount(account1);
      _userAccountsManager.addAccount(account2);
      _userAccountsManager.addAccount(account3);
      _userAccountsManager.addAccount(account4);
    }
    catch(XrayTesterException xte)
    {
      System.out.println("addAccount XrayTesterException = " + xte.toString());
    }

    // update some accounts
    System.out.println("--- Updating accounts");
    UserAccountInfo newAccount2 = new UserAccountInfo("22", "Second", "22", UserTypeEnum.DEVELOPER);
    UserAccountInfo newAccount3 = new UserAccountInfo("3", "Third", "3333", UserTypeEnum.DEVELOPER);
    try
    {
      _userAccountsManager.updateAccount(account2, newAccount2);
      _userAccountsManager.updateAccount(account3, newAccount3);
    }
    catch(XrayTesterException xte)
    {
      System.out.println("updateAccount XrayTesterException = " + xte.toString());
    }

    // delete some accounts
    System.out.println("--- Deleting accounts");
    UserAccountInfo sampleAccount = _userAccounts.get(4);
    try
    {
      _userAccountsManager.deleteAccount(sampleAccount);
      _userAccountsManager.deleteAccount(account1);
    }
    catch(XrayTesterException xte)
    {
      System.out.println("deleteAccount XrayTesterException = " + xte.toString());
    }
    // print the updates
    _userAccounts = _userAccountsManager.getAccounts();
    for (UserAccountInfo userAccount : _userAccounts)
    {
      System.out.println(userAccount.toString());
    }
  }

  /**
   * @author George Booth
   */
  void testCase2()
  {
    // create invalid accounts
    try
    {
      UserAccountInfo bogus = new UserAccountInfo(null, null, null, null, false, false);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    try
    {
      UserAccountInfo bogus = new UserAccountInfo("junk", null, null, null, false, false);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    try
    {
      UserAccountInfo bogus = new UserAccountInfo("junk", "junk", null, null, false, false);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    try
    {
      UserAccountInfo bogus = new UserAccountInfo("junk", "junk", "junk", null, false, false);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    // find a user
    boolean found = _userAccountsManager.doesUserExist("service");
    Assert.expect(found == true);

    // don't find a non-existent user
    found = _userAccountsManager.doesUserExist("mike jackson");
    Assert.expect(found == false);

    // validate a good account
    boolean valid = _userAccountsManager.isValidUserAccount("service_expert", "service_expert");
    Assert.expect(valid == true);

    // don't validate a non-existent account
    valid = _userAccountsManager.isValidUserAccount("serviceExpert", "service_expert");
    Assert.expect(valid == false);

    // don't validate a bad password
    valid = _userAccountsManager.isValidUserAccount("service_expert", "serviceExpert");
    Assert.expect(valid == false);

    // get a valid userType
    UserTypeEnum userTypeEnum = _userAccountsManager.getUserTypeFromUserName("administrator");
    Assert.expect(userTypeEnum.equals(UserTypeEnum.ADMINISTRATOR));

    // get a invalid userType
    try
    {
      userTypeEnum = _userAccountsManager.getUserTypeFromUserName("administrators");
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

  }

  public static void main(String[] args)
  {
    UnitTest.execute(new Test_UserAccountsManager());
  }
}
