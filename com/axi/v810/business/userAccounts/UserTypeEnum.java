package com.axi.v810.business.userAccounts;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author George Booth
 */
public class UserTypeEnum extends com.axi.util.Enum implements Serializable
{
  private static Map<String, UserTypeEnum> _nameToUserTypeEnumMap = new TreeMap<String, UserTypeEnum>(new AlphaNumericComparator());

  private static int _index = -1;

  public final static UserTypeEnum ADMINISTRATOR = new UserTypeEnum(++_index, "administrator");
  public final static UserTypeEnum DEVELOPER = new UserTypeEnum(++_index, "developer");
  public final static UserTypeEnum OPERATOR = new UserTypeEnum(++_index, "operator");
  public final static UserTypeEnum SERVICE_EXPERT = new UserTypeEnum(++_index, "service_expert");
  public final static UserTypeEnum SERVICE = new UserTypeEnum(++_index, "service");
  public final static UserTypeEnum LOGIN = new UserTypeEnum(++_index, "login");

  private String _name;

  /**
   * @author George Booth
   */
  private UserTypeEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);
   _name = name.intern();
   _nameToUserTypeEnumMap.put(_name, this);
  }

  /**
   * @author George Booth
   */
  public String getName()
  {
    return _name;
  }

  /**
   * @author George Booth
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author George Booth
   */
  public static UserTypeEnum getUserTypeEnum(String name)
  {
    UserTypeEnum userType = _nameToUserTypeEnumMap.get(name);
    Assert.expect(userType != null);
    return userType;
  }

  /**
   * @author George Booth
   */
  public static boolean doesUserTypeExist(String name)
  {
    return _nameToUserTypeEnumMap.containsKey(name);
  }

  /**
   * return userTypeEnums that customer can use for logins
   * "service_expert" and "login" for internal use only
   * @author George Booth
   */
  public static List<UserTypeEnum> getCustomerUserTypeEnums()
  {
    ArrayList<UserTypeEnum> customerUserTypeEnums = new ArrayList<UserTypeEnum>();
    customerUserTypeEnums.add(UserTypeEnum.getUserTypeEnum("administrator"));
    customerUserTypeEnums.add(UserTypeEnum.getUserTypeEnum("developer"));
    customerUserTypeEnums.add(UserTypeEnum.getUserTypeEnum("operator"));
    customerUserTypeEnums.add(UserTypeEnum.getUserTypeEnum("service"));
    return customerUserTypeEnums;
  }

  /**
   * return all userTypeEnums
   * @author George Booth
   */
  public static List<UserTypeEnum> getAllUserTypeEnums()
  {
    return new ArrayList<UserTypeEnum>(_nameToUserTypeEnumMap.values());
  }
}
