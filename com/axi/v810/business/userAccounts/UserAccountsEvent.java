package com.axi.v810.business.userAccounts;

import com.axi.util.*;

/**
 * All user accounts events should notify their observers with this class
 * @author George Booth
 */
public class UserAccountsEvent
{
  private Object _source;
  private UserAccountsEventEnum _userAccountsEventEnum;

  /**
   * @author George Booth
   */
  public UserAccountsEvent(Object source, UserAccountsEventEnum userAccountsEventEnum)
  {
    Assert.expect(userAccountsEventEnum != null);

    _source = source;
    _userAccountsEventEnum = userAccountsEventEnum;
  }

  /**
   * @author George Booth
   */
  public Object getSource()
  {
    return _source;
  }

  /**
   * @author George Booth
   */
  public UserAccountsEventEnum getUserAccountsEventEnum()
  {
    Assert.expect(_userAccountsEventEnum != null);

    return _userAccountsEventEnum;
  }
}
