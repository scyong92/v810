package com.axi.v810.business;

import com.axi.util.*;

/**
 * Thrown when we cannot resolve whether or not the local project
 * is newer than the database project. This happens when the version
 * numbers match up, but the cad/settings do not.
 *
 * @author George David
 */
class ProjectDatabaseVersionConflictBusinessException extends BusinessException
{
  /**
   * @author George David
   */
  ProjectDatabaseVersionConflictBusinessException(String projectName)
  {
    super(new LocalizedString("BS_ERROR_PROJECT_DATABASE_VERSION_CONFLICT_KEY",
                              new Object[]{projectName}));
    Assert.expect(projectName != null);
  }
}
