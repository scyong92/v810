package com.axi.v810.business;

import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import java.io.*;

/**
 * This class keeps track of the system status.
 * @author swee-yee.wong
 */
public class SystemStatusPersistance implements Serializable
{
  private String _lowMag;
  private String _highMag;
  
  /**
   * @author swee-yee.wong
   */
  public SystemStatusPersistance()
  {
    _lowMag = Config.getInstance().getStringValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION);
    _highMag = Config.getInstance().getStringValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_HIGH_MAGNIFICATION);
  }
  
  /**
   * @author swee-yee.wong
   */
  public boolean isSystemMagnificationReconfigured()
  {
    return (_lowMag.equalsIgnoreCase(Config.getSystemCurrentLowMagnification()) == false) ||
           (_highMag.equalsIgnoreCase(Config.getSystemCurrentHighMagnification()) == false);
  }
  
  /**
   * @author swee-yee.wong
   */
  public String getPrevSystemLowMag()
  {
    return _lowMag;
  }

  /**
   * @author swee-yee.wong
   */
  public String getPrevSystemHighMag()
  {
    return _highMag;
  }

  /**
   * @author swee-yee.wong
   */
  public boolean equals(Object obj)
  {
    boolean isEqual = false;

    if (super.equals(obj))
      isEqual = true;
    else
    {
      if (obj instanceof SystemStatusPersistance)
      {
        SystemStatusPersistance compareTo = (SystemStatusPersistance) obj;

        if (_lowMag.compareToIgnoreCase(compareTo._lowMag) == 0 && 
            _highMag.compareToIgnoreCase(compareTo._highMag) == 0)
          isEqual = true;
      }
    }

    return isEqual;
  }

  /**
   * XCR-3157 IRP error message is prompted if run alignment/fine tuning without run full cdna and without unload the board 1st
   * Make information persistent in case of system power failure.
   * @throws DatastoreException
   * @author Swee Yee Wong
   */
  public void savePersistentSystemInformation() throws DatastoreException
  {
    _lowMag = Config.getInstance().getStringValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION);
    _highMag = Config.getInstance().getStringValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_HIGH_MAGNIFICATION);
    FileUtilAxi.saveObjectToSerializedFile(this, FileName.getSystemStatusConfigFullPath());
  }

  /**
   * XCR-3157 IRP error message is prompted if run alignment/fine tuning without run full cdna and without unload the board 1st
   * Retrieve persistent data so we know the state System is in.
   * @author Swee Yee Wong
   */
  public SystemStatusPersistance getPersistentSystemInformation()
  {
    SystemStatusPersistance persistance = null;
    try
    {
      if(FileUtilAxi.exists(FileName.getSystemStatusConfigFullPath()))
      {
        persistance = (SystemStatusPersistance) FileUtilAxi.loadObjectFromSerializedFile(
                FileName.getSystemStatusConfigFullPath());
      }
      else
      {
        persistance = new SystemStatusPersistance();
      }
    }
    catch (DatastoreException de)
    {
      persistance = new SystemStatusPersistance();
    }
    return persistance;
  }
}

