package com.axi.v810.business.monitorGamma;

import java.io.*;

import com.axi.util.Assert;
import com.axi.v810.datastore.*;

/**
 * This is the business layer class of the monitor gamma utility.
 * It allows access to the gamma value stored in the file system.
 * The getValue() will return a default gamma value if it encounters error.
 *
 * @author Vincent Wong
 */
public class MonitorGamma
{
  private final static double _MAXGAMMA = 3.0;
  private final static double _MINGAMMA = 1.0;
  private final static double _DEFAULTGAMMA = 2.2;

  private GammaSetting _gammaSetting;

  /**
   *  Open connection to the file system
   *
   *  @param filename is the full pathname of the storage file.
   *  @author Vincent Wong
   */
  public MonitorGamma( String filename )
  {
    //----------------------------------------
    // Normally, an RMI call is used when creating a new instance. 
    // This is a special case. The data is stored locally.
    //
    _gammaSetting = new GammaSetting( filename );
  }

  /**
   *  Save the gamma value to the system.
   *  The input value must be within the range [_MINGAMMA, _MAXGAMMA].
   *  The method throws DatastoreException if the value cannot be written
   *  to the file system.
   *
   *  @param gammaValue the gamma value to save.
   *  @author Vincent Wong
   */
  public void setValue( double gammaValue ) throws DatastoreException
  {
    if (gammaValue < _MINGAMMA)
      gammaValue = _MINGAMMA;

    if (gammaValue > _MAXGAMMA)
      gammaValue = _MAXGAMMA;

    _gammaSetting.setValue( gammaValue );
  }

  /**
   *  Retrieve the gamma value from the system.
   *  The returned value is guaranteed to be in the range [_MINGAMMA, _MAXGAMMA].
   *
   *  @return A valid gamma value.
   *  @author Vincent Wong
   */
  public double getValue() throws DatastoreException
  {
    double gammaValue = _DEFAULTGAMMA;

    boolean exception = false;

    try
    {
      gammaValue = _gammaSetting.getValue();
    }
    catch ( FileCorruptDatastoreException e ) // unable to return a valid value
    {
      exception = true;
    }
    catch ( FileNotFoundDatastoreException e ) // unable to return a valid value
    {
      exception = true;
    }
    finally
    {
      if ( exception )
      {
        setValue( _DEFAULTGAMMA ); // attempt to fix the problem
        gammaValue = _DEFAULTGAMMA;
      }
    }

    if ( gammaValue < _MINGAMMA || gammaValue > _MAXGAMMA )
    {
      setValue( _DEFAULTGAMMA ); // attempt to fix the problem
      gammaValue = _DEFAULTGAMMA; // return a default value if out of range
    }

    return gammaValue;
  }

  /**
   *  Make the configuration file on disk read-only.
   *
   *  @return True if the operation is successful and false otherwise.
   *  @author Vincent Wong
   */
  public boolean setReadOnly()
  {
    return _gammaSetting.setReadOnly();
  }

  /**
   *  Get the maximum allowable gamma value.
   *
   *  @return The maximum allowable value.
   *  @author Vincent Wong
   */
  public static double getMaxGamma()
  {
    return _MAXGAMMA;
  }

  /**
   *  Get the minimum allowable gamma value.
   *
   *  @return The minimum allowable value.
   *  @author Vincent Wong
   */
  public static double getMinGamma()
  {
    return _MINGAMMA;
  }

  /**
   *  Get the system default gamma value.
   *
   *  @return The default value.
   *  @author Vincent Wong
   */
  public static double getDefaultGamma()
  {
    return _DEFAULTGAMMA;
  }
}
