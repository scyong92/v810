package com.axi.v810.business.monitorGamma;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Vincent Wong
 */
public class Test_MonitorGamma extends UnitTest
{
  private String _fullpath = "display.cfg"; 

  //----------------------------------------
  // Main.
  //
  public static void main( String[] args )
  {
    UnitTest.execute( new Test_MonitorGamma() );
  }

  //----------------------------------------
  // Test procedure.
  //
  public void test( BufferedReader is, PrintWriter os )
  {
    double maxGamma = MonitorGamma.getMaxGamma();
    double minGamma = MonitorGamma.getMinGamma();
    double defaultGamma = MonitorGamma.getDefaultGamma();
    
    Expect.expect( maxGamma > 1.0, "Unexpected maximum gamma value." );
    Expect.expect( maxGamma < 4.0, "Unexpected maximum gamma value." );
    Expect.expect( minGamma >= 1.0, "Unexpected minimum gamma value." );
    Expect.expect( minGamma < maxGamma, "Unexpected minimum gamma value." );
    Expect.expect( defaultGamma <= maxGamma,
                   "Unexpected default gamma value." );
    Expect.expect( defaultGamma >= minGamma,
                   "Unexpected default gamma value." );

    //----------------------------------------
    // Remove the config file if it is already there.
    //
    File configFile = new File( _fullpath );

    if ( configFile.exists() )
      configFile.delete(); // remove the config file

    MonitorGamma monitorGamma = new MonitorGamma( _fullpath );
    double gamma = 0.0;

    //----------------------------------------
    // The config is not present, the default value should be returned and
    // the config file should be created.
    //
    try
    {
      gamma = monitorGamma.getValue();
      Expect.expect( gamma == defaultGamma, "Default value is expected." ); 
      Expect.expect( configFile.exists(), "Config file should be created." ); 

      if ( configFile.exists() )
        configFile.delete(); // remove the config file
    }
    catch ( DatastoreException e )
    {
      Expect.expect( false,
      "DatastoreException not expected in case of config file not present." );
    }


    //----------------------------------------
    // The config is not present, setValue() should create a new config file.
    //
    try
    {
      monitorGamma.setValue( (minGamma + maxGamma)/2. );
      Expect.expect( configFile.exists(), "Config file should be created." ); 
    }
    catch ( DatastoreException e )
    {
      Expect.expect( false,
      "DatastoreException not expected in case of config file not present." );
    }


    //----------------------------------------
    // The config file is present, getValue() should return the stored value.
    //
    try
    {
      gamma = monitorGamma.getValue();
      Expect.expect( gamma == (minGamma + maxGamma)/2.,
                     "Unexpected gamma value returned." ); 
    }
    catch ( DatastoreException e )
    {
      Expect.expect( false,
        "DatastoreException not expected when calling setValue()." );
    }


/*
    //----------------------------------------
    // Call setValue() with an out-of-range value should give an assert error.
    //
    try
    {
      monitorGamma.setValue( maxGamma + 1 ); // assert error expected
    }
    catch ( DatastoreException e )
    {
      Expect.expect( false,
        "DatastoreException not expected if value is out of range." );
    }
*/


    //----------------------------------------
    // If the config file is read-only, there should be a DatastoreException.
    //
    try
    {
      monitorGamma.setValue( maxGamma );
      gamma = monitorGamma.getValue();
      Expect.expect( gamma == maxGamma, "Unexpected gamma value returned." );

      configFile.setReadOnly();
      monitorGamma.setValue( minGamma );
    }
    catch ( DatastoreException e )
    {
      Expect.expect( true );
    }


    //----------------------------------------
    // Try to read some invalid contents from an arbitrary file.
    // getValue() should return the default gamma value and then put
    // the value in the file.
    //
    String tempFileName = "tempfile.txt";

    try
    {
      FileWriter fileWriter = new FileWriter( tempFileName );
      fileWriter.write( "a" ); // write a non-numeric character in it
      fileWriter.close();
    }
    catch ( IOException e )
    {
      Expect.expect( false, "Unable to create temp file for testing." ); 
    }

    MonitorGamma monitorGamma2 = new MonitorGamma( tempFileName );

    try
    {
      gamma = monitorGamma2.getValue();
      Expect.expect( gamma == defaultGamma, "Default value is expected." ); 
      File tempFile = new File( tempFileName );
      tempFile.delete();
    }
    catch ( DatastoreException e )
    {
      Expect.expect( false, "DatastoreExceptions not expected." );
    }

    //----------------------------------------
    // Create an empty config file. getValue() should return the default gamma
    // value and then put the value in the file.
    //
    File tempFile = null;

    try
    {
      tempFile = File.createTempFile( "temp", "cfg" ); // empty file
    }
    catch ( IOException e )
    {
      Expect.expect( false, "Unable to create temp file for testing." ); 
    }

    MonitorGamma monitorGamma3 = new MonitorGamma( tempFile.getAbsolutePath() );

    try
    {
      gamma = monitorGamma3.getValue();
      Expect.expect( gamma == defaultGamma, "Default value is expected." ); 
      gamma = monitorGamma3.getValue();
      Expect.expect( gamma == defaultGamma, "Default value is expected." ); 
      monitorGamma3.setValue( maxGamma );
      gamma = monitorGamma3.getValue();
      Expect.expect( gamma == maxGamma, "Maximum value is expected." ); 
    }
    catch ( DatastoreException e )
    {
      Expect.expect( false, "DatastoreExceptions not expected." );
    }

    if ( tempFile != null )
      tempFile.delete();
   

    //----------------------------------------
    // Clean up after the test.
    //
    if ( configFile.exists() )
      configFile.delete(); // remove the config file
  }
}
