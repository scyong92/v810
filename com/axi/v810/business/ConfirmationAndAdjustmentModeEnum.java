package com.axi.v810.business;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 *
 * @author siew-yeng.phang
 */
public class ConfirmationAndAdjustmentModeEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;
  private static HashMap<String, ConfirmationAndAdjustmentModeEnum> _stringToEnumMap;
  private String _name;

  /**
   * @author Phang Siew Yeng
   */
  static
  {
    _stringToEnumMap = new HashMap<String, ConfirmationAndAdjustmentModeEnum>();
  }

  public static final ConfirmationAndAdjustmentModeEnum STANDARD_MODE = new ConfirmationAndAdjustmentModeEnum(
      ++_index, "standard");

  public static final ConfirmationAndAdjustmentModeEnum PASSIVE_MODE = new ConfirmationAndAdjustmentModeEnum(
      ++_index, "passive");
  
  /**
   * @author Phang Siew Yeng
   */
  private ConfirmationAndAdjustmentModeEnum(int id, String confirmationAndAdjustmentModeString)
  {
    super(id);

    Assert.expect(confirmationAndAdjustmentModeString != null);
    _name = confirmationAndAdjustmentModeString;

    Assert.expect(_stringToEnumMap.put(confirmationAndAdjustmentModeString, this) == null);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  static ConfirmationAndAdjustmentModeEnum getEnum(String keyString)
  {
    Assert.expect(keyString != null);

    ConfirmationAndAdjustmentModeEnum modeEnum = _stringToEnumMap.get(keyString);

    Assert.expect(modeEnum != null);
    return modeEnum;
  }

  /**
   * @author Phang Siew Yeng
   */
  public static List<ConfirmationAndAdjustmentModeEnum> getAllValues()
  {
    List<ConfirmationAndAdjustmentModeEnum> allValues = new ArrayList<ConfirmationAndAdjustmentModeEnum>();
    allValues.add(ConfirmationAndAdjustmentModeEnum.STANDARD_MODE);
    allValues.add(ConfirmationAndAdjustmentModeEnum.PASSIVE_MODE);
    return allValues;
  }

  /**
   * @author Phang Siew Yeng
   */
  public String toString()
  {
    return _name;
  }
}
