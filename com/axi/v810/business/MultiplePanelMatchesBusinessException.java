package com.axi.v810.business;

import com.axi.util.*;

/**
 * This exception is thrown when more than one panel program matches the panel model
 * received from Board Tracking (CAM).
 *
 * @author Jeff Ryer
 */
class MultiplePanelMatchesBusinessException extends BusinessException
{
  /**
   * @author Jeff Ryer
   */
  MultiplePanelMatchesBusinessException(String firstPanelName,
                                         String secondPanelName)
  {
    super(new LocalizedString("BS_ERROR_MULTIPLE_PANEL_MATCHES_KEY",
                              new Object[]{firstPanelName,
                                           secondPanelName}));
    Assert.expect(firstPanelName != null);
    Assert.expect(secondPanelName != null);
  }
}
