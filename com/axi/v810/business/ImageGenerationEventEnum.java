package com.axi.v810.business;

import com.axi.util.*;


/**
 * @author George A. David
 */
public class ImageGenerationEventEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static ImageGenerationEventEnum BEGIN_ACQUIRE_VERIFICATION_IMAGES = new ImageGenerationEventEnum(++_index);
  public static ImageGenerationEventEnum END_ACQUIRE_VERIFICATION_IMAGES = new ImageGenerationEventEnum(++_index);
  public static ImageGenerationEventEnum VERIFICATION_IMAGE_ACQUIRED = new ImageGenerationEventEnum(++_index);
  public static ImageGenerationEventEnum CALCULATE_VERIFICATION_SURFACE_SURFACE_MODEL = new ImageGenerationEventEnum(++_index);
  public static ImageGenerationEventEnum BEGIN_ACQUIRE_FOCUS_SET_IMAGES = new ImageGenerationEventEnum(++_index);
  public static ImageGenerationEventEnum END_ACQUIRE_FOCUS_SET_IMAGES = new ImageGenerationEventEnum(++_index);
  public static ImageGenerationEventEnum BEGIN_ACQUIRE_OFFLINE_IMAGES = new ImageGenerationEventEnum(++_index);
  public static ImageGenerationEventEnum END_ACQUIRE_OFFLINE_IMAGES = new ImageGenerationEventEnum(++_index);
  /**
   * @author George A. David
   */
  private ImageGenerationEventEnum(int id)
  {
    super(id);
  }
}
