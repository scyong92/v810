package com.axi.v810.business;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class PanelCadSizeDoesNotMatchPanelSizeInTesterBusinessException extends BusinessException
{
  /**
   * @author Bill Darbie
   */
  public PanelCadSizeDoesNotMatchPanelSizeInTesterBusinessException(int cadWidthInNanoMeters,
                                                                    int cadLengthInNanoMeters,
                                                                    int panelUnderTestWidthInNanoMeters,
                                                                    int panelUnderTestLengthInNanoMeters)
  {
    super(new LocalizedString("BUS_PANEL_CAD_SIZE_DOES_NOT_MATCH_PANEL_SIZE_IN_TESTER_EXCEPTION_KEY",
                              new Object[]{cadWidthInNanoMeters,
                                           cadLengthInNanoMeters,
                                           panelUnderTestWidthInNanoMeters,
                                           panelUnderTestLengthInNanoMeters}));
  }
}
