package com.axi.v810.business;

import java.util.*;

import com.axi.util.*;

/**
 * Allows you to observe events that come from the image generation
 * @author George A. David
 */
public class ImageGenerationObservable extends Observable
{
  private static ImageGenerationObservable _instance;

  /**
   * @author George A. David
   */
  private ImageGenerationObservable()
  {
    // do nothing
  }

  /**
   * @author George A. David
   */
  public static synchronized ImageGenerationObservable getInstance()
  {
    if (_instance == null)
      _instance = new ImageGenerationObservable();

    return _instance;
  }


  /**
   * @author George A. David
   */
  public void stateChanged(ImageGenerationEvent imageGenerationEvent)
  {
    Assert.expect(imageGenerationEvent != null);

    setChanged();
    notifyObservers(imageGenerationEvent);
  }
}


