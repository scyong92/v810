package com.axi.v810.business;

import com.axi.v810.datastore.config.Config;
import com.axi.util.*;
import com.axi.v810.datastore.config.SoftwareConfigEnum;
import com.axi.v810.datastore.*;

public class CustomerInfo
{
  private static CustomerInfo _instance;
  private Config _config;

  /**
   * @author Andy Mechtenberg
   */
  public static synchronized CustomerInfo getInstance()
  {
    if (_instance == null)
    {
      _instance = new CustomerInfo();
    }

    return _instance;
  }

  /**
   * @author Andy Mechtenberg
   */
  private CustomerInfo()
  {
    _config = Config.getInstance();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getCompanyName()
  {
    return _config.getStringValue(SoftwareConfigEnum.CUSTOMER_COMPANY_NAME);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setCompanyName(String companyName) throws DatastoreException
  {
    Assert.expect(companyName != null);
    _config.setValue(SoftwareConfigEnum.CUSTOMER_COMPANY_NAME, companyName);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getCustomerName()
  {
    return _config.getStringValue(SoftwareConfigEnum.CUSTOMER_NAME);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setCustomerName(String customerName) throws DatastoreException
  {
    Assert.expect(customerName != null);
    _config.setValue(SoftwareConfigEnum.CUSTOMER_NAME, customerName);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getEmailAddress()
  {
    return _config.getStringValue(SoftwareConfigEnum.CUSTOMER_EMAIL_ADDRESS);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setEmailAddress(String emailAddress) throws DatastoreException
  {
    Assert.expect(emailAddress != null);
    _config.setValue(SoftwareConfigEnum.CUSTOMER_EMAIL_ADDRESS, emailAddress);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getPhoneNumber()
  {
    return _config.getStringValue(SoftwareConfigEnum.CUSTOMER_PHONE_NUMBER);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setPhoneNumber(String phoneNumber) throws DatastoreException
  {
    Assert.expect(phoneNumber != null);
    _config.setValue(SoftwareConfigEnum.CUSTOMER_PHONE_NUMBER, phoneNumber);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getMailServer()
  {
    return _config.getStringValue(SoftwareConfigEnum.SMTP_SERVER);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setMailServer(String mailServer) throws DatastoreException
  {
    Assert.expect(mailServer != null);
    _config.setValue(SoftwareConfigEnum.SMTP_SERVER, mailServer);
  }

}
