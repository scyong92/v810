package com.axi.v810.business;

import com.axi.util.*;


/**
 * @author George A. David
 */
public class ImageGenerationEvent
{
  private ImageGenerationEventEnum _imageGenerationEventEnum;
  // this is used for the begin cases. It tells the observers how many events to expect.
  int _numEventsToExpect = -1;

  /**
   * @author George A. David
   */
  public ImageGenerationEvent(ImageGenerationEventEnum imageGenerationEventEnum)
  {
    Assert.expect(imageGenerationEventEnum != null);

    _imageGenerationEventEnum = imageGenerationEventEnum;
  }

  /**
   * @author George A. David
   */
  public void setImageGenerationEventEnum(ImageGenerationEventEnum imageGenerationEventEnum)
  {
    Assert.expect(imageGenerationEventEnum != null);

    _imageGenerationEventEnum = imageGenerationEventEnum;
  }

  /**
   * @author George A. David
   */
  public ImageGenerationEventEnum getImageGenerationEventEnum()
  {
    Assert.expect(_imageGenerationEventEnum != null);

    return _imageGenerationEventEnum;
  }

  /**
   * @author George A. David
   */
  public void setNumEventsToExpect(int numEvents)
  {
    Assert.expect(_imageGenerationEventEnum.equals(ImageGenerationEventEnum.BEGIN_ACQUIRE_VERIFICATION_IMAGES));
    Assert.expect(numEvents > 0);

    _numEventsToExpect = numEvents;
  }

  /**
   * @author George A. David
   */
  public int getNumEventsToExpect()
  {
    Assert.expect(_imageGenerationEventEnum.equals(ImageGenerationEventEnum.BEGIN_ACQUIRE_VERIFICATION_IMAGES));
    Assert.expect(_numEventsToExpect > 0);


    return _numEventsToExpect;
  }
}