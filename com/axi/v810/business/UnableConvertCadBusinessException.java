/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.business;

import com.axi.util.*;

/**
 *
 * @author weng-jian.eoh
 */
public class UnableConvertCadBusinessException extends BusinessException
{
  public UnableConvertCadBusinessException(String filePahth,String message)
  {
    super(new LocalizedString("MM_GUI_UNABLE_CONVERT_CAD_MESSAGE_KEY", new Object[]
      {
        filePahth,
        message
      }));
  }
}
