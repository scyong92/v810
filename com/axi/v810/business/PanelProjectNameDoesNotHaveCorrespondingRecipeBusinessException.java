/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.business;

import com.axi.util.*;

/**
 *
 * @author sheng-chuan.yong
 */
public class PanelProjectNameDoesNotHaveCorrespondingRecipeBusinessException extends BusinessException
{
  public PanelProjectNameDoesNotHaveCorrespondingRecipeBusinessException(String projectName)
  {
    super(new LocalizedString("BUS_PANEL_PROJECT_NAME_DOES_NOT_HAVE_CORRESPONDING_RECIPE_EXCEPTION_KEY",
                          new Object[]{projectName}));
  }
}
