package com.axi.v810.business.cadcreator;

import com.axi.util.Assert;

import java.util.*;

/**
 *
 * @author chin-seong.kee
 */
public class LandPatternDraggedShapeSettings extends Observable
{
  private int _enabled = -1;
  private java.awt.Shape _croppedAreaShape;
  private java.awt.Shape _draggedShape;
  private CadCreatorROI _cadCreatorROI = null;
  private static LandPatternDraggedShapeSettings _landPatternSelectedAreaSettings = null;
   
  LandPatternDraggedShapeSettings()
  { 
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setLandPatternSelectedArea(java.awt.Shape shape)
  {
    Assert.expect(shape != null);

    disable();
    
    java.awt.Shape newShape = null;
    if(shape != null)
      newShape = changeShapeFitToPanelSizeIfNeeded(shape); 
    
    getDraggedROI().setROI((int) newShape.getBounds2D().getCenterX(), 
                          (int) newShape.getBounds2D().getCenterY(), 
                          (int) newShape.getBounds2D().getWidth(), 
                          (int) newShape.getBounds2D().getHeight(),
                          (double) newShape.getBounds2D().getMinX(),
                          (double) newShape.getBounds2D().getMinY());
    
    setDraggedShape(newShape);
     
    enable();
    setObservable();
  }
  
    /**
   * @author Kee Chin Seong
   */
  public void enable()
  {
    --_enabled;
    Assert.expect(_enabled >= 0);
  }

  /**
   * @author Kee Chin Seong
   */
  public void disable()
  {
    if (_enabled == -1)
      _enabled = 0;
    ++_enabled;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setCroppedShape(java.awt.Shape shape)
  {
    Assert.expect(shape != null);

    _croppedAreaShape = shape;
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void setDraggedShape(java.awt.Shape shape)
  {
    Assert.expect(shape != null);

    _draggedShape = shape;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public java.awt.Shape getDraggedShape()
  {
    Assert.expect(_draggedShape != null);

    return _draggedShape;
  }
  
   /**
   * author Kee Chin Seong
   */
  public void setObservable()
  {
    java.awt.Shape shape = getShapeFitWithinPanel();
    if(isEnabled() && shape != null)
    {
      setChanged();
      notifyObservers(shape);
    }
  }
  
  public static LandPatternDraggedShapeSettings getInstance()
  {
    if (_landPatternSelectedAreaSettings == null)
    {
      _landPatternSelectedAreaSettings = new LandPatternDraggedShapeSettings();
    }
    return _landPatternSelectedAreaSettings;
  }
  
  
  /**
   * @author Kee Chin Seong
   */
  public CadCreatorROI getDraggedROI()
  {
    return _cadCreatorROI;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public java.awt.Shape getShapeFitWithinPanel()
  {
    java.awt.Shape shape = null;

    shape = getDraggedROI().getShape();
    
    if(shape != null)
      shape = changeShapeFitToPanelSizeIfNeeded(shape);
    
    return shape;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public java.awt.Shape getPanelShape()
  {
    Assert.expect(_croppedAreaShape!=null);
    
    return new java.awt.geom.Rectangle2D.Double(0, 0, (_croppedAreaShape.getBounds2D().getWidth() * 25400), (_croppedAreaShape.getBounds2D().getHeight() * 25400));
  }
  
  /*
   * @author Kee Chin Seong
   */
  private java.awt.Shape changeShapeFitToPanelSizeIfNeeded(java.awt.Shape shape)
  {
    Assert.expect(shape != null);
 
    // reset invalid Min X and Max X or Min Y and Max Y
    if ( (shape.getBounds2D().getMinX() < 0) || 
         (shape.getBounds2D().getMinY() < 0) || 
         (shape.getBounds2D().getMaxX() < 0) || 
         (shape.getBounds2D().getMaxY() < 0) )
    {
      if(shape instanceof java.awt.geom.Rectangle2D)
         shape = new java.awt.geom.Rectangle2D.Double(0,0,2540000,2540000);
      else
         shape = new java.awt.geom.Ellipse2D.Double(0,0,2540000,2540000);
      
      return shape;
    }
    
    // Trim if X is negative 
    if (shape.getBounds2D().getMinX() < 0)
    {
       if(shape instanceof java.awt.geom.Rectangle2D)
          shape = new java.awt.geom.Rectangle2D.Double(0, shape.getBounds2D().getMinY(), shape.getBounds2D().getWidth() - (0 - shape.getBounds2D().getMinX()), shape.getBounds2D().getHeight());
       else
          shape = new java.awt.geom.Ellipse2D.Double(0, shape.getBounds2D().getMinY(), shape.getBounds2D().getWidth() - (0 - shape.getBounds2D().getMinX()), shape.getBounds2D().getHeight());
    }
    // Trim if Y is negative
    if (shape.getBounds2D().getMinY() < 0)
    {
       if(shape instanceof java.awt.geom.Rectangle2D)
           shape = new java.awt.geom.Rectangle2D.Double(shape.getBounds2D().getMinX(), 0, shape.getBounds2D().getWidth(), shape.getBounds2D().getHeight() - (0 - shape.getBounds2D().getMinY())); 
       else
           shape = new java.awt.geom.Ellipse2D.Double(shape.getBounds2D().getMinX(), 0, shape.getBounds2D().getWidth(), shape.getBounds2D().getHeight() - (0 - shape.getBounds2D().getMinY())); 
    }
    // Trim when X is greater than panel maximum width
    if (shape.getBounds2D().getMaxX() > getPanelShape().getBounds2D().getMaxX())
    {
       if(shape instanceof java.awt.geom.Rectangle2D)
          shape = new java.awt.geom.Rectangle2D.Double(shape.getBounds2D().getMinX(),
                        shape.getBounds2D().getMinY(),
                        shape.getBounds2D().getWidth() - (shape.getBounds2D().getMaxX() - getPanelShape().getBounds2D().getMaxX()),
                        shape.getBounds2D().getHeight());
       else
          shape = new java.awt.geom.Ellipse2D.Double(shape.getBounds2D().getMinX(),
                        shape.getBounds2D().getMinY(),
                        shape.getBounds2D().getWidth() - (shape.getBounds2D().getMaxX() - getPanelShape().getBounds2D().getMaxX()),
                        shape.getBounds2D().getHeight());
    }
    // Trim when Y is greater than panel maximum height
    if (shape.getBounds2D().getMaxY() > getPanelShape().getBounds2D().getMaxY())
    {
       if(shape instanceof java.awt.geom.Rectangle2D)
          shape = new java.awt.geom.Rectangle2D.Double(shape.getBounds2D().getMinX(),
                    shape.getBounds2D().getMinY(),
                    shape.getBounds2D().getWidth(),
                    shape.getBounds2D().getHeight() - (shape.getBounds2D().getMaxY() - getPanelShape().getBounds2D().getMaxY()));
       if(shape instanceof java.awt.geom.Rectangle2D)
          shape = new java.awt.geom.Ellipse2D.Double(shape.getBounds2D().getMinX(),
                    shape.getBounds2D().getMinY(),
                    shape.getBounds2D().getWidth(),
                    shape.getBounds2D().getHeight() - (shape.getBounds2D().getMaxY() - getPanelShape().getBounds2D().getMaxY()));
    }
    
    if(_cadCreatorROI == null)
    {
        if(shape instanceof java.awt.geom.Rectangle2D)
           _cadCreatorROI = new CadCreatorROI(0,0,0,0,0,0, false);
        else
           _cadCreatorROI = new CadCreatorROI(0,0,0,0,0,0, true);
    }
    else
    {
        if(shape instanceof java.awt.geom.Rectangle2D)
           _cadCreatorROI.setIsCircleShape(false);
        else
           _cadCreatorROI.setIsCircleShape(true);
    }
    setEnlargeUncertaintyInNanoMeter(0);

    return shape;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setEnlargeUncertaintyInNanoMeter(int enlargeUncertaintyInNanoMeter)
  {
    Assert.expect(enlargeUncertaintyInNanoMeter >= 0);

    getDraggedROI().setEnlargeUncertaintyInNanoMeter (enlargeUncertaintyInNanoMeter);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setObservable(java.awt.Shape shape)
  {
    Assert.expect(shape != null);
    
    if(isEnabled())
    {
      setChanged();
      notifyObservers(shape);
    }
  }
  
    /**
   * @author Kee Chin Seong
   */
  public boolean isEnabled()
  {
    if (_enabled == 0)
      return true;

    return false;
  }

}
