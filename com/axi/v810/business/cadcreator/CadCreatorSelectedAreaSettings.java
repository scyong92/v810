package com.axi.v810.business.cadcreator;

import com.axi.util.Assert;
import java.awt.geom.Rectangle2D;
import java.util.Observable;

/**
 *
 * @author chin-seong.kee
 */
public class CadCreatorSelectedAreaSettings extends Observable
{
  private int _enabled = -1;
  private java.awt.Shape _panelShape;
  private java.awt.Shape _drawShape;
   
  private CadCreatorROI _cadCreatorROI = new CadCreatorROI(0,0,0,0,0,0);
  private static CadCreatorSelectedAreaSettings _cadCreatorSelectedAreaSettings = null;
   
  CadCreatorSelectedAreaSettings()
  { 
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setCadCreatorSelectedAreaData(java.awt.Shape shape)
  {
    Assert.expect(shape != null);

    disable();
    
    java.awt.Shape newShape = null;
    if(shape != null)
      newShape = changeShapeFitToPanelSizeIfNeeded(shape);
    
    getCadCreatorROI().setROI((int) newShape.getBounds2D().getCenterX(), 
                          (int) newShape.getBounds2D().getCenterY(), 
                          (int) newShape.getBounds2D().getWidth(), 
                          (int) newShape.getBounds2D().getHeight(),
                          (double) newShape.getBounds2D().getMinX(),
                          (double) newShape.getBounds2D().getMinY());
    
    setDrawShape(newShape);
    
    enable();
    setObservable();
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void setDrawShape(java.awt.Shape drawShape)
  {
    _drawShape = drawShape;
  }
  
  /*
   * @return
   * @author Kee chin Seong
   */
  public java.awt.Shape getDrawShape()
  {
    Assert.expect(_drawShape != null);
    return _drawShape;  
  }
  
    /**
   * @author Kee Chin Seong
   */
  public void enable()
  {
    --_enabled;
    Assert.expect(_enabled >= 0);
  }

  /**
   * @author Kee Chin Seong
   */
  public void disable()
  {
    if (_enabled == -1)
      _enabled = 0;
    ++_enabled;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setShape(java.awt.Shape shape)
  {
    Assert.expect(shape != null);

    _panelShape = shape;
  }
  
   /**
   * author Kee Chin Seong
   */
  public void setObservable()
  {
    java.awt.Shape shape = getShapeFitWithinPanel();
    if(isEnabled() && shape != null)
    {
      setChanged();
      notifyObservers(shape);
    }
  }
  
  public static CadCreatorSelectedAreaSettings getInstance()
  {
    if (_cadCreatorSelectedAreaSettings == null)
    {
      _cadCreatorSelectedAreaSettings = new CadCreatorSelectedAreaSettings();
    }
    return _cadCreatorSelectedAreaSettings;
  }
  
  public CadCreatorROI getCadCreatorROI()
  {
    return _cadCreatorROI;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public java.awt.Shape getShapeFitWithinPanel()
  {
    java.awt.Shape shape = null;

    shape = getCadCreatorROI().getShape();
    
    if(shape != null)
      shape = changeShapeFitToPanelSizeIfNeeded(shape);
    
    return shape;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public java.awt.Shape getPanelShape()
  {
    Assert.expect(_panelShape!=null);
    
    return _panelShape;
  }
  
  /*
   * @author Kee Chin Seong
   */
  private java.awt.Shape changeShapeFitToPanelSizeIfNeeded(java.awt.Shape shape)
  {
    Assert.expect(shape != null);
    // reset invalid Min X and Max X or Min Y and Max Y
    if ( (shape.getBounds2D().getMinX() > getPanelShape().getBounds2D().getMaxX()) || 
         (shape.getBounds2D().getMinY() > getPanelShape().getBounds2D().getMaxY()) || 
         (shape.getBounds2D().getMaxX() < 0) || 
         (shape.getBounds2D().getMaxY() < 0) )
    {
      shape = new java.awt.Rectangle.Double(0,0,2540000,2540000);
      return shape;
    }
    
    // Trim if X is negative 
    if (shape.getBounds2D().getMinX() < 0)
    {
//      shape.getBounds2D().setRect(0, shape.getBounds2D().getMinY(), shape.getBounds2D().getWidth() - (0 - shape.getBounds2D().getMinX()), shape.getBounds2D().getHeight());
      shape = new java.awt.Rectangle.Double(0, shape.getBounds2D().getMinY(), shape.getBounds2D().getWidth() - (0 - shape.getBounds2D().getMinX()), shape.getBounds2D().getHeight());
    }
    // Trim if Y is negative
    if (shape.getBounds2D().getMinY() < 0)
    {
//      shape.getBounds2D().setRect(shape.getBounds2D().getMinX(), 0, shape.getBounds2D().getWidth(), shape.getBounds2D().getHeight() - (0 - shape.getBounds2D().getMinY()));
      shape = new java.awt.Rectangle.Double(shape.getBounds2D().getMinX(), 0, shape.getBounds2D().getWidth(), shape.getBounds2D().getHeight() - (0 - shape.getBounds2D().getMinY())); 
    }
    // Trim when X is greater than panel maximum width
    if (shape.getBounds2D().getMaxX() > getPanelShape().getBounds2D().getMaxX())
    {
      shape = new java.awt.Rectangle.Double(shape.getBounds2D().getMinX(),
        shape.getBounds2D().getMinY(),
        shape.getBounds2D().getWidth() - (shape.getBounds2D().getMaxX() - getPanelShape().getBounds2D().getMaxX()),
        shape.getBounds2D().getHeight());
    }
    // Trim when Y is greater than panel maximum height
    if (shape.getBounds2D().getMaxY() > getPanelShape().getBounds2D().getMaxY())
    {
      shape = new java.awt.Rectangle.Double(shape.getBounds2D().getMinX(),
        shape.getBounds2D().getMinY(),
        shape.getBounds2D().getWidth(),
        shape.getBounds2D().getHeight() - (shape.getBounds2D().getMaxY() - getPanelShape().getBounds2D().getMaxY()));
    }
    setEnlargeUncertaintyInNanoMeter(0);

    return shape;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setEnlargeUncertaintyInNanoMeter(int enlargeUncertaintyInNanoMeter)
  {
    Assert.expect(enlargeUncertaintyInNanoMeter >= 0);

    getCadCreatorROI().setEnlargeUncertaintyInNanoMeter (enlargeUncertaintyInNanoMeter);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setObservable(java.awt.Shape shape)
  {
    Assert.expect(shape != null);
    
    if(isEnabled())
    {
      setChanged();
      notifyObservers(shape);
    }
  }
  
    /**
   * @author Kee Chin Seong
   */
  public boolean isEnabled()
  {
    if (_enabled == 0)
      return true;

    return false;
  }

}
