package com.axi.v810.business.cadcreator;

import java.util.*;

/**
 *
 * @author wei-chin.chong
 */
public class CadCreatorROI extends Observable
{
  private int _centerXInNanoMeter = 0;
  private int _centerYInNanoMeter = 0;
  private int _widthInNanoMeter = 0;
  private int _heightInNanoMeter = 0;
  private int _enlargeUncertaintyInNanoMeter = 0;
  
  private double _minXInNanoMeter = 0;
  private double _minYInNanoMeter = 0;
  private boolean _isCircle = false;

  /**
   * @param centerXInNanoMeter
   * @param centerYInNanoMeter
   * @param widthInNanoMeter
   * @param heightInNanoMeter 
   * @author chin-seong.kee
   */
  CadCreatorROI(int centerXInNanoMeter, int centerYInNanoMeter,  int widthInNanoMeter, 
                int heightInNanoMeter, double minXInNanoMeter, double minYInNanoMeter)
  {
    _centerXInNanoMeter = centerXInNanoMeter;
    _centerYInNanoMeter = centerYInNanoMeter;
    _widthInNanoMeter = widthInNanoMeter;
    _heightInNanoMeter = heightInNanoMeter;
    _minXInNanoMeter = minXInNanoMeter;
    _minXInNanoMeter = minYInNanoMeter;
  }
  
  /*
   * @Author Kee Chin Seong
   */
  CadCreatorROI(int centerXInNanoMeter, int centerYInNanoMeter,  int widthInNanoMeter, 
                int heightInNanoMeter, double minXInNanoMeter, double minYInNanoMeter, boolean isCircle)
  {
    _centerXInNanoMeter = centerXInNanoMeter;
    _centerYInNanoMeter = centerYInNanoMeter;
    _widthInNanoMeter = widthInNanoMeter;
    _heightInNanoMeter = heightInNanoMeter;
    _minXInNanoMeter = minXInNanoMeter;
    _minXInNanoMeter = minYInNanoMeter;
    _isCircle = isCircle;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setIsCircleShape(boolean isCircleShape)
  {
    _isCircle = isCircleShape;
  }
  
  /*
   * @Author Kee Chin Seong
   */
  public double getMinXInNanotMeter()
  {
    return _minXInNanoMeter;
  }
  
  /*
   * @Author Kee Chin Seong
   */
  public double getMinYInNanotMeter()
  {
    return _minYInNanoMeter;
  }
  
  /**
   * @return the _centerXInNanoMeter
   * @author chin-seong.kee
   */
  public int getCenterXInNanoMeter()
  {
    return _centerXInNanoMeter;
  }

  /**
   * @param centerXInNanoMeter the _centerXInNanoMeter to set
   * @author chin-seong.kee
   */
  public void setCenterXInNanoMeter(int centerXInNanoMeter)
  {
    _centerXInNanoMeter = centerXInNanoMeter;
  }

  /**
   * @return the _centerYInNanoMeter
   * @author chin-seong.kee
   */
  public int getCenterYInNanoMeter()
  {
    return _centerYInNanoMeter;
  }

  /**
   * @param centerYInNanoMeter the _centerYInNanoMeter to set
   * @author chin-seong.kee
   */
  public void setCenterYInNanoMeter(int centerYInNanoMeter)
  {
    _centerYInNanoMeter = centerYInNanoMeter;
  }

  /**
   * @return the _widthInNanoMeter
   * @author chin-seong.kee
   */
  public int getWidthInNanoMeter()
  {
    return _widthInNanoMeter;
  }

  /**
   * @param widthInNanoMeter the _widthInNanoMeter to set
   * @author chin-seong.kee
   */
  public void setWidthInNanoMeter(int widthInNanoMeter)
  {
    _widthInNanoMeter = widthInNanoMeter;
  }

  /**
   * @return the _heightInNanoMeter
   * @author chin-seong.kee
   */
  public int getHeightInNanoMeter()
  {
    return _heightInNanoMeter;
  }

  /**
   * @param heightInNanoMeter the _heightInNanoMeter to set
   * @author chin-seong.kee
   */
  public void setHeightInNanoMeter(int heightInNanoMeter)
  {
    _heightInNanoMeter = heightInNanoMeter;
  }
  
  /**
   * @param centerXInNanoMeter
   * @param centerYInNanoMeter
   * @param widthInNanoMeter
   * @param heightInNanoMeter 
   * @author chin-seong.kee
   */
  public void setROI(int centerXInNanoMeter, int centerYInNanoMeter,  int widthInNanoMeter, 
                     int heightInNanoMeter, double minXInNanoMeter, double minYInNanoMeter)
  {
    _centerXInNanoMeter = centerXInNanoMeter;
    _centerYInNanoMeter = centerYInNanoMeter;
    _widthInNanoMeter = widthInNanoMeter;
    _heightInNanoMeter = heightInNanoMeter;
    _minXInNanoMeter = minXInNanoMeter;
    _minXInNanoMeter = minYInNanoMeter;
  }
  
  /**
   * @return 
   * @author chin-seong.kee
   */
  public boolean isValidShape()
  {
    return _widthInNanoMeter > 0 && _heightInNanoMeter > 0;
  }
  
  /**
   * @return 
   * @author chin-seong.kee
   */
  public java.awt.Shape getShape()
  {
    //if(isValidShape() == false)
    //  return null;
    java.awt.Shape shape = null;
    if(_isCircle == false)
    {
        shape = new java.awt.Rectangle.Double(getCenterXInNanoMeter() - getWidthInNanoMeter() / 2 - getEnlargeUncertaintyInNanoMeter(),
                  getCenterYInNanoMeter() - getHeightInNanoMeter() / 2 - getEnlargeUncertaintyInNanoMeter(),
                  getWidthInNanoMeter() + 2 * getEnlargeUncertaintyInNanoMeter(),
                  getHeightInNanoMeter() + 2 * getEnlargeUncertaintyInNanoMeter());
    }
    else
    {
        shape = new java.awt.geom.Ellipse2D.Double(getCenterXInNanoMeter() - getWidthInNanoMeter() / 2 - getEnlargeUncertaintyInNanoMeter(),
                  getCenterYInNanoMeter() - getHeightInNanoMeter() / 2 - getEnlargeUncertaintyInNanoMeter(),
                  getWidthInNanoMeter() + 2 * getEnlargeUncertaintyInNanoMeter(),
                  getHeightInNanoMeter() + 2 * getEnlargeUncertaintyInNanoMeter());
    }
    return shape;
  }

  /**
   * @return the _enlargeUncertaintyInNanoMeter
   * @author chin-seong.kee
   */
  public int getEnlargeUncertaintyInNanoMeter()
  {
    return _enlargeUncertaintyInNanoMeter;
  }

  /**
   * @param enlargeUncertaintyInNanoMeter the _enlargeUncertaintyInNanoMeter to set
   * @author chin-seong.kee
   */
  public void setEnlargeUncertaintyInNanoMeter(int enlargeUncertaintyInNanoMeter)
  {
    _enlargeUncertaintyInNanoMeter = enlargeUncertaintyInNanoMeter;
  }
}
