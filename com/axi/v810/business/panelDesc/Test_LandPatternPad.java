package com.axi.v810.business.panelDesc;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class Test_LandPatternPad extends UnitTest
{
  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_LandPatternPad());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    basicTests();
    pinOrientationEnumTests();
  }

  /**
   * @author Bill Darbie
   */
  private void basicTests()
  {
    TestLandPatternPad testLandPatternPad = new TestLandPatternPad();

    // get and set CoordinateInNanometers
    try
    {
      testLandPatternPad.getCoordinateInNanoMeters();
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }

    try
    {
      testLandPatternPad.setCoordinateInNanoMeters(null);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }

    ComponentCoordinate padCoord = new ComponentCoordinate(4,6);
    testLandPatternPad.setLandPattern(new LandPattern());
    testLandPatternPad.setCoordinateInNanoMeters(padCoord);
    Expect.expect(testLandPatternPad.getCoordinateInNanoMeters().equals(padCoord));

    // get and set WidthInNanometers
    try
    {
      testLandPatternPad.getWidthInNanoMeters();
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }

    try
    {
      testLandPatternPad.setWidthInNanoMeters(0);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }

    int padWidth = 5;
    testLandPatternPad.setWidthInNanoMeters(padWidth);
    Expect.expect(testLandPatternPad.getWidthInNanoMeters() == padWidth);

    // get and set LengthInNanometers
    try
    {
      testLandPatternPad.getLengthInNanoMeters();
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }

    try
    {
      testLandPatternPad.setLengthInNanoMeters(0);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }

    int padLength = 5;
    testLandPatternPad.setLengthInNanoMeters(padLength);
    Expect.expect(testLandPatternPad.getLengthInNanoMeters() == padLength);

    // get and set degreesRotation
    try
    {
      testLandPatternPad.getDegreesRotation();
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }

    int padRotation = 50;
    testLandPatternPad.setDegreesRotation(padRotation);
    Expect.expect(testLandPatternPad.getDegreesRotation() == padRotation);

    // get and set padShape
    try
    {
      testLandPatternPad.getShapeEnum();
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }

    try
    {
      testLandPatternPad.setShapeEnum(null);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }

    ShapeEnum padShape = ShapeEnum.RECTANGLE;
    testLandPatternPad.setShapeEnum(padShape);
    Expect.expect(testLandPatternPad.getShapeEnum().equals(padShape));

    // check createDuplicate
    Panel panel = new Panel();
    LandPattern landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("landPatternName1");

    LandPatternPad landPatternPad = new SurfaceMountLandPatternPad();
    landPatternPad.setLandPattern(landPattern);
    landPatternPad.setCoordinateInNanoMeters(new ComponentCoordinate(10,20));
    landPatternPad.setDegreesRotation(0);
    landPatternPad.setWidthInNanoMeters(10);
    landPatternPad.setLengthInNanoMeters(20);
    landPatternPad.setShapeEnum(ShapeEnum.CIRCLE);
    landPatternPad.setName("1");

    LandPattern landPattern2 = new LandPattern();
    landPattern2.setPanel(panel);
    landPattern2.setName("landPatternName2");
    LandPatternPad landPatternPadDup = landPatternPad.createDuplicate(landPattern2);
    Assert.expect(landPatternPadDup.getLandPattern() == landPattern2);
    Assert.expect(landPatternPadDup.getCoordinateInNanoMeters().equals(landPatternPad.getCoordinateInNanoMeters()));
    Assert.expect(landPatternPadDup.getDegreesRotation() == landPatternPad.getDegreesRotation());
    Assert.expect(landPatternPadDup.getInterPadDistanceInNanoMeters() == landPatternPad.getInterPadDistanceInNanoMeters());
    Assert.expect(landPatternPadDup.getLengthInNanoMeters() == landPatternPad.getLengthInNanoMeters());
    Assert.expect(landPatternPadDup.getWidthInNanoMeters() == landPatternPad.getWidthInNanoMeters());
    Assert.expect(landPatternPadDup.getName().equals(landPatternPad.getName()));
    Assert.expect(landPatternPadDup.isPadOne() == landPatternPad.isPadOne());
    Assert.expect(landPatternPadDup.isSurfaceMountPad());
  }

  /**
   * @author Bill Darbie
   */
  private void pinOrientationEnumTests()
  {
    LandPatternPad landPatternPad = new SurfaceMountLandPatternPad();
    LandPattern landPattern = new LandPattern();
    landPatternPad.setLandPattern(landPattern);

    // test PinOrientationEnum and PinOrientationAfterRotationEnum
    landPatternPad.setDegreesRotation(0);
    landPatternPad.setPinOrientationEnum(PinOrientationEnum.NOT_APPLICABLE);
    Expect.expect(landPatternPad.getPinOrientationEnum().equals(PinOrientationEnum.NOT_APPLICABLE));
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.NOT_APPLICABLE));

    // N, S, E, W cases
    landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN);
    Expect.expect(landPatternPad.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(0);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(90);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(180);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(270);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN));

    landPatternPad.setDegreesRotation(0);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN));
    landPatternPad.setDegreesRotation(90);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN));
    landPatternPad.setDegreesRotation(180);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN));
    landPatternPad.setDegreesRotation(270);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN));

    landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN);
    Expect.expect(landPatternPad.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(0);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(90);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(180);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(270);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN));

    landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN);
    Expect.expect(landPatternPad.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(0);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(90);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(180);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(270);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN));

    landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN);
    Expect.expect(landPatternPad.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(0);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(90);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(180);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(270);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN));


    // NE, NW, SE, SW
    landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN);
    Expect.expect(landPatternPad.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(10);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_WEST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(100);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_WEST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(190);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_EAST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(280);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_EAST_OF_PIN));

    landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN);
    Expect.expect(landPatternPad.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(10);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_EAST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(100);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_EAST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(190);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_WEST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(280);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_WEST_OF_PIN));


    landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN);
    Expect.expect(landPatternPad.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(10);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_EAST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(100);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_WEST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(190);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_WEST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(280);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_EAST_OF_PIN));

    landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN);
    Expect.expect(landPatternPad.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(10);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_WEST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(100);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_EAST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(190);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_EAST_OF_PIN));
    landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(280);
    Expect.expect(landPatternPad.getPinOrientationAfterRotationEnum().equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_WEST_OF_PIN));
  }

  /**
   * @author Andy Mechtenberg
   */
  private class TestLandPatternPad extends LandPatternPad
  {
    // because LandPatternPad is abstract, I'll extend it here so I can create an instance to test.
    public boolean isThroughHolePad()
    {
      return true;
    }

    // because LandPatternPad is abstract, I'll extend it here so I can create an instance to test.
    public boolean isSurfaceMountPad()
    {
      return false;
    }
  }
}


