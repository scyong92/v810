package com.axi.v810.business.panelDesc;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class Test_Board extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_Board());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    // test setName() and getName()
    try
    {
      Board board = new Board();
      Panel panel = new Panel();
      panel.disableChecksForNdfParsers();
      BoardType boardType = new BoardType();
      boardType.setPanel(panel);
      board.setBoardType(boardType);
      try
      {
        board.setName(null);
        Expect.expect(false);
      }
      catch (AssertException e)
      {
        // do nothing
      }
      try
      {
        board.getName();
        Expect.expect(false);
      }
      catch (AssertException e)
      {
        // do nothing
      }
      String name = "BoardName";
      board.setName(name);
      Expect.expect(board.getName().equals(name));

// wpd remove virgo
//    // test setVersion() and getVersion()
//    try
//    {
//      board.setVersion(null);
//      Expect.expect(false);
//    }
//    catch(AssertException e)
//    {
//      // do nothing
//    }
//    try
//    {
//      board.getVersion();
//      Expect.expect(false);
//    }
//    catch(AssertException e)
//    {
//      // do nothing
//    }
//    String version = "Version";
//    board.setVersion(version);
//    Expect.expect(board.getVersion().equals(version));

      // test setTopSideBoard() and topSideBoardExists() and getTopSideBoard()
      try
      {
        board.setSideBoard1(null);
        Expect.expect(false);
      }
      catch (AssertException e)
      {
        // do nothing
      }
      SideBoard topSideBoard = new SideBoard();
      board.setSideBoard1(topSideBoard);
      Expect.expect(board.sideBoard1Exists());
      Expect.expect(board.getSideBoard1() == topSideBoard);

      // test setBottomSideBoard() and bottomSideBoardExists() and getBottomSideBoard()
      try
      {
        board.setSideBoard2(null);
        Expect.expect(false);
      }
      catch (AssertException e)
      {
        // do nothing
      }
      Expect.expect(board.sideBoard2Exists() == false);
      SideBoard bottomSideBoard = new SideBoard();
      board.setSideBoard2(bottomSideBoard);
      Expect.expect(board.sideBoard2Exists());
      Expect.expect(board.getSideBoard2() == bottomSideBoard);

      // test setBoardSettings() and getBoardSettings()
      try
      {
        board.setBoardSettings(null);
        Expect.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing
      }
      try
      {
        board.getBoardSettings();
        Expect.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing
      }
      BoardSettings boardSettings = new BoardSettings(board);
      board.setBoardSettings(boardSettings);
      Expect.expect(board.getBoardSettings() == boardSettings);

// wpd virgo
//    // test setWidthInNanoMeters and getWidthInNanoMeters
//    try
//    {
//      board.getWidthInNanoMeters();
//      Expect.expect(false);
//    }
//    catch(AssertException e)
//    {
//      // do nothing
//    }
//    try
//    {
//      board.setWidthInNanoMeters(0);
//      Expect.expect(false);
//    }
//    catch(AssertException e)
//    {
//      // do nothing
//    }
//    int width = 50;
//    board.setWidthInNanoMeters(width);
//    Expect.expect(board.getWidthInNanoMeters() == width);

// wpd virgo
//    // test setLengthInNanoMeters and getLengthInNanoMeters
//    try
//    {
//      board.getLengthInNanoMeters();
//      Expect.expect(false);
//    }
//    catch(AssertException e)
//    {
//      // do nothing
//    }
//    try
//    {
//      board.setLengthInNanoMeters(0);
//      Expect.expect(false);
//    }
//    catch(AssertException e)
//    {
//      // do nothing
//    }
//    int length = 50;
//    board.setLengthInNanoMeters(length);
//    Expect.expect(board.getLengthInNanoMeters() == length);

      // test setDegreesRotationRelativeToPanel and getDegreesRotationRelativeToPanel
      boardType.setWidthInNanoMeters(1);
      boardType.setLengthInNanoMeters(1);
      int rotation = 180;
      panel.setWidthInNanoMeters(10);
      panel.setLengthInNanoMeters(10);
      PanelSettings panelSettings = new PanelSettings();
      panelSettings.setPanel(panel);
      panelSettings.setDegreesRotationRelativeToCad(0);
      panel.setPanelSettings(panelSettings);
      panel.setPanelSettings(panelSettings);
      board.setDegreesRotationRelativeToPanelFromNdfReaders(rotation);

      // test setCoordinateInNanoMeters and getCoordinateInNanoMeters
      try
      {
        board.getCoordinateInNanoMeters();
        Expect.expect(false);
      }
      catch (AssertException e)
      {
        // do nothing
      }
      try
      {
        board.setCoordinateInNanoMeters(null);
        Expect.expect(false);
      }
      catch (AssertException e)
      {
        // do nothing
      }
      PanelCoordinate coordinate = new PanelCoordinate(40, 32);
      board.setCoordinateInNanoMeters(coordinate);
      Expect.expect(board.getCoordinateInNanoMeters().equals(coordinate));


      /** @todo  test setLegacyComponentIds(String panelName) */
      // this one cannot be tested easily here -- should be tested in Test_Panel

      // test getComponents()
      List<Component> list = board.getComponents();
      Expect.expect(list.size() == 0);

      Component c1 = new Component();
      ComponentType ct1 = new ComponentType();
      ct1.setPanel(panel);
      c1.setComponentType(ct1);
      ct1.setReferenceDesignator("c1");
      topSideBoard.addComponent(c1);
      board = new Board();
      boardSettings = new BoardSettings();
      boardSettings.setInspected(false);
      board.setBoardSettings(boardSettings);
      board.setSideBoard1(topSideBoard);
      list = board.getComponents();
      Expect.expect(list.size() == 1);

      Component c2 = new Component();
      ComponentType ct2 = new ComponentType();
      ct2.setPanel(panel);
      ct2.setReferenceDesignator("c2");
      c2.setComponentType(ct2);
      topSideBoard.addComponent(c2);
      list = board.getComponents();
      Expect.expect(list.size() == 2);

      Component c3 = new Component();
      ComponentType ct3 = new ComponentType();
      ct3.setPanel(panel);
      ct3.setReferenceDesignator("c3");
      c3.setComponentType(ct3);
      bottomSideBoard.addComponent(c3);
      board.setSideBoard2(bottomSideBoard);
      list = board.getComponents();
      Expect.expect(list.size() == 3);

      // test getInspectedComponents()
      // to test this method properly it is necessary to set up a lot of things
      ComponentTypeSettings cts1 = new ComponentTypeSettings();
      cts1.setLoaded(true);
      c1.getComponentType().setComponentTypeSettings(cts1);
      cts1.setInspected(false);

      ComponentTypeSettings cts2 = new ComponentTypeSettings();
      cts2.setLoaded(true);
      c2.getComponentType().setComponentTypeSettings(cts2);
      cts2.setInspected(false);

      LandPattern landPattern = new LandPattern();
      landPattern.setPanel(panel);
      LandPatternPad landPatternPad = new SurfaceMountLandPatternPad();
      landPatternPad.setLandPattern(landPattern);
      landPatternPad.setName("1");
      Pad pad = new Pad();
      PadType padType = new PadType();
      padType.setLandPatternPad(landPatternPad);
      PackagePin packagePin = new PackagePin();
      padType.setPackagePin(packagePin);
      pad.setPadType(padType);
      PadTypeSettings padTypeSettings = new PadTypeSettings();
      padTypeSettings.setPadType(padType);
      pad.getPadSettings().setTestable(true);
 //     padTypeSettings.setTestable(true);
      padTypeSettings.setInspected(true);
      padType.setPadTypeSettings(padTypeSettings);
      c2.addPad(pad);
      ComponentTypeSettings cts3 = new ComponentTypeSettings();
      cts3.setLoaded(true);
      c3.getComponentType().setComponentTypeSettings(cts3);
      cts3.setInspected(false);

      // OK - now check that there are no inspected components
      List<Component> inspectedComponents = board.getInspectedComponents();
      Expect.expect(inspectedComponents.size() == 0);
      // set c2 to be inspected but don't set any of its pins to use any algorithms
      // and confirm that no components are tested still
      /** @todo wpd */
//    cts2.setInspected(true);
//    inspectedComponents = board.getInspectedComponents();
//    Expect.expect(inspectedComponents.size() == 1);
//    Expect.expect(inspectedComponents.get(0) == c2);
//    // now change c2 to be NOT_TESTED
//    cts2.setInspected(false);
//    inspectedComponents = board.getInspectedComponents();
//    Expect.expect(inspectedComponents.size() == 0);
//    // now try a partial inspection setting
//    cts2.setInspected(true);
//    inspectedComponents = board.getInspectedComponents();
//    Expect.expect(inspectedComponents.size() == 1);
//    Expect.expect(inspectedComponents.get(0) == c2);

      // test getUninspectedComponents
      // use the previous setup to test this
      // first, all three are marked NO_INSPECTION
      cts2.setInspected(false);
      List<Component> untestedComponents = board.getUninspectedComponents();
      Expect.expect(untestedComponents.size() == 3);

      // c1 and c3 have no inpsected pins, so setting them to inpsect doesn't make it so
      cts1.setInspected(true);
      cts3.setInspected(true);
      untestedComponents = board.getUninspectedComponents();
      Expect.expect(untestedComponents.size() == 3);

      // c2 is now inspected, and it has a valid pin algorithm family
      cts2.setInspected(true);
//    untestedComponents = board.getUninspectedComponents();
//    Expect.expect(untestedComponents.size() == 2);

      // test getComponent(int legacyId) here is trivial (not-set) testing
//    try
//    {
//      board.getComponent(-1);
//      Expect.expect(false);
//    }
//    catch(AssertException ae)
//    {
//      // do nothing
//    }
//    try
//    {
//      board.getComponent(10);
//      Expect.expect(false);
//    }
//    catch(AssertException ae)
//    {
//      // do nothing
//    }

      /** @todo test getPin(int legacySortedJointNumber) needs to be tested in Test_Panel */

// wpd virgo
//    // test getDaughterBoards()
//    List boards = board.getDaughterBoards();
//    Expect.expect(boards.size() == 0);
//    board.setTopSideBoard(topSideBoard);
//    List daughterBoards = board.getDaughterBoards();
//    Expect.expect(daughterBoards.size() == 0);
//    Board daughterBoard = new Board();
//    topSideBoard.addDaughterBoard(daughterBoard);
//    daughterBoards = board.getDaughterBoards();
//    Expect.expect(daughterBoards.size() == 1);
//    board.setBottomSideBoard(bottomSideBoard);
//    daughterBoards = board.getDaughterBoards();
//    Expect.expect(daughterBoards.size() == 1);
//    bottomSideBoard.addDaughterBoard(daughterBoard);
//    daughterBoards = board.getDaughterBoards();
//    Expect.expect(daughterBoards.size() == 2);

      // test getCustomAlgorithmFamilies()

      board = new Board();
      List<Subtype> algFamilies = board.getSubtypes();
      Expect.expect(algFamilies.size() == 0);
      // now add an algorithm family - this requires a little work
      Component component = new Component();
      ComponentType componentType = new ComponentType();
      componentType.setPanel(panel);
      component.setComponentType(componentType);
      componentType.setReferenceDesignator("c1");
      pad = new Pad();
      padType = new PadType();
      padType.setLandPatternPad(landPatternPad);
      pad.setPadType(padType);
      component.addPad(pad);
      padTypeSettings = new PadTypeSettings();
//      padTypeSettings.setTestable(true);
      pad.getPadSettings().setTestable(true);
      padTypeSettings.setInspected(true);
      padType.setPadTypeSettings(padTypeSettings);
      padType.setPackagePin(packagePin);
      SideBoard sideBoard = new SideBoard();

      // test legacyReadInViewList(Panel panel) is be in Test_Panel */
     // this one cannot be tested easily here

     // test legacyReadInSurfaceMapRtfData(Panel panel) is in Test_Panel */
    // this one cannot be tested easily here

    // test getNumJoints()
    board = new Board();
      Expect.expect(board.getNumJoints() == 0);
      component = new Component();
      component.setComponentType(componentType);
      componentType.setReferenceDesignator("c1");
      ComponentTypeSettings componentTypeSettings = new ComponentTypeSettings();
      componentTypeSettings.setLoaded(true);
      componentType.setComponentTypeSettings(componentTypeSettings);
      pad = new Pad();
      padType = new PadType();
      padType.setLandPatternPad(landPatternPad);
      pad.setPadType(padType);
      component.addPad(pad);
      padTypeSettings = new PadTypeSettings();
      padType.setPadTypeSettings(padTypeSettings);
      sideBoard = new SideBoard();
      sideBoard.addComponent(component);
      board.setSideBoard1(sideBoard);
      Expect.expect(board.getNumJoints() == 1);

      // test getNumTestedJoints()
      // the component must be set to be inspected
      /** @todo wpd */
//    ComponentTypeSettings cts = new ComponentTypeSettings();
//    cts.setLoaded(true);
//    cts.setInspected(true);
//    component.getComponentType().setComponentTypeSettings(cts);
//    padTypeSettings.setInspected(false);
//    padType.setPackagePin(packagePin);
//    Expect.expect(board.getNumTestedJoints() == 0);
//    padTypeSettings.setInspected(true);
//    Expect.expect(board.getNumTestedJoints() == 1);
//    cts.setInspected(false);
//    Expect.expect(board.getNumTestedJoints() == 0);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
