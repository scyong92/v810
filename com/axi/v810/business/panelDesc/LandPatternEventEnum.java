package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class LandPatternEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static LandPatternEventEnum CREATE_OR_DESTROY = new LandPatternEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static LandPatternEventEnum ADD_OR_REMOVE = new LandPatternEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static LandPatternEventEnum PANEL = new LandPatternEventEnum(++_index);
  public static LandPatternEventEnum NAME = new LandPatternEventEnum(++_index);
  public static LandPatternEventEnum WIDTH = new LandPatternEventEnum(++_index);
  public static LandPatternEventEnum LENGTH = new LandPatternEventEnum(++_index);
  public static LandPatternEventEnum LOWER_LEFT_COORDINATE = new LandPatternEventEnum(++_index);
  public static LandPatternEventEnum ADD_OR_REMOVE_LAND_PATTERN_PAD = new LandPatternEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static LandPatternEventEnum ADD_OR_REMOVE_COMPONENT_TYPE = new LandPatternEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static LandPatternEventEnum NUM_ROWS = new LandPatternEventEnum(++_index);
  public static LandPatternEventEnum NUM_COLS = new LandPatternEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private LandPatternEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private LandPatternEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
