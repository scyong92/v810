package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class CompPackageEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static CompPackageEventEnum CREATE_OR_DESTROY = new CompPackageEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static CompPackageEventEnum PANEL = new CompPackageEventEnum(++_index);
  public static CompPackageEventEnum LONG_NAME = new CompPackageEventEnum(++_index);
  public static CompPackageEventEnum SHORT_NAME = new CompPackageEventEnum(++_index);
  public static CompPackageEventEnum ADD_OR_REMOVE_COMPONENT_TYPE = new CompPackageEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static CompPackageEventEnum ADD_OR_REMOVE_PACKAGE_PIN = new CompPackageEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static CompPackageEventEnum LAND_PATTERN = new CompPackageEventEnum(++_index);
  public static CompPackageEventEnum JOINT_TYPE_ENUM = new CompPackageEventEnum(++_index);
  public static CompPackageEventEnum EXPORT_COMPLETE_ENUM = new CompPackageEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private CompPackageEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private CompPackageEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
