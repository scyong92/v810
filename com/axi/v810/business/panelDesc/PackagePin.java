package com.axi.v810.business.panelDesc;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;

/**
 * A PackagePin object contains the Pin information that is the same
 * for a CompPackage type.
 * For example, U1 and U2 may both use the same Package.  This means
 * that Pin 1 for U1 and U2 would share some common data.  The
 * data that would be the same no matter which physical instance of the Pin
 * you are dealing with is stored in this class.  The data that changes
 * with U1 and U2 for Pin 1 is in the Pin class.  This allows a
 * more efficient use of memory.
 *
 * @author Bill Darbie
 */
public class PackagePin implements Serializable
{
  // created by ComponentType, CompPackageReader

  private CompPackage _compPackage; // PanelDescBus, CompPackageReader
  // each CompPackage is associated with ONLY one LandPattern
  // each PackagePin is associated with ONLY one LandPatternPad
  private LandPatternPad _landPatternPad; // PanelDescBus, CompPackageReader
  // pad orientation is the edge of the pad (at 0 degrees rotation)
  // that is closest to where the pin
  // begins exiting the component it is attached to
  private PinOrientationEnum _pinOrientationEnum;  // calculated from LandPatternPadLocations or set by user // CompPackageReader
  private JointTypeEnum _jointTypeEnum;
  private int _jointHeightInNanoMeters = -1;
  private int _customJointHeightInNanoMeters = 0;

  private static transient ProjectObservable _projectObservable;

  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  PackagePin(PackagePin rhs)
  {
    _projectObservable.setEnabled(false);
    try
    {
      // shallow copy CompPackage
      _compPackage = rhs._compPackage;
      // shallow copy the enum since it is not mutable
      _pinOrientationEnum = rhs._pinOrientationEnum;
      _jointTypeEnum = rhs._jointTypeEnum;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, this, PackagePinEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public PackagePin()
  {
    _projectObservable.stateChanged(this, null, this, PackagePinEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      getCompPackage().removePackagePin(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, PackagePinEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void setCompPackage(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);

    if (compPackage == _compPackage)
      return;

    CompPackage oldValue = _compPackage;
    _projectObservable.setEnabled(false);
    try
    {
      _compPackage = compPackage;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, compPackage, PackagePinEventEnum.COMP_PACKAGE);
  }

  /**
   * @author Bill Darbie
   */
  public CompPackage getCompPackage()
  {
    Assert.expect(_compPackage != null);

    return _compPackage;
  }

  /**
   * @author Bill Darbie
   */
  public void setLandPatternPad(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    if (landPatternPad == _landPatternPad)
      return;

    LandPatternPad oldValue = _landPatternPad;
    _projectObservable.setEnabled(false);
    try
    {
      _landPatternPad = landPatternPad;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, landPatternPad, PackagePinEventEnum.LAND_PATTERN_PAD);
  }

  /**
   * @author Bill Darbie
   */
  public LandPatternPad getLandPatternPad()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad;
  }

  /**
   * Set the edge of the pad (at 0 degrees rotation) that is closest
   * to the location where the pin is attached to the component.
   * Call this method to override the default orientation that was
   * determined with CAD data (and stored in the LandPatternPad class).
   * @author Bill Darbie
   */
  public void setPinOrientationEnum(PinOrientationEnum pinOrientationEnum)
  {
    Assert.expect(pinOrientationEnum != null);

    if (pinOrientationEnum.equals(_pinOrientationEnum))
      return;

    PinOrientationEnum oldValue = _pinOrientationEnum;
    _projectObservable.setEnabled(false);
    try
    {
      _pinOrientationEnum = pinOrientationEnum;
      
      // [XCR1603] - Commented by Ying-Huan.Chu
      // Changing a pin's orientation does not change its LandPatternPad's shape, thus we don't have to invalidate its shape.
      // Changed by Kee Chin Seong - Instead of invalidate SHAPE, we invalidate ORITENTATION since
      // ORientation is needed to invalidate instead of SHAPE.
      if (_landPatternPad != null)
        _landPatternPad.invalidateOrientation();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, pinOrientationEnum, PackagePinEventEnum.PAD_ORIENTATION_ENUM);
  }

  /**
   * @author Bill Darbie
   */
  void clearPinOrientationEnum()
  {
    _pinOrientationEnum = null;
  }

  /**
   * @return the edge of the pad (at 0 degrees rotation) that is closest
   * to the location where the pin is attached to the component.
   * @author Bill Darbie
   */
  public PinOrientationEnum getPinOrientationEnum()
  {
    // The PackagePin will have _pinOrientationEnum set ONLY if its value has been
    // overridden from the orientation calculated from the LandPatternPad.  If it
    // has not been set, then find the setting from the LandPatternPad class and
    // return that
    PinOrientationEnum pinOrientationEnum = null;

    if (_pinOrientationEnum != null)
    {
      // return the value that was set in this class
      pinOrientationEnum = _pinOrientationEnum;
    }
    else
    {
      Assert.expect(_landPatternPad != null);
      pinOrientationEnum = _landPatternPad.getPinOrientationEnum();
    }

    Assert.expect(pinOrientationEnum != null);
    return pinOrientationEnum;
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasPinOrientationEnumBeenOverridden()
  {
    if (_pinOrientationEnum == null)
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  public void setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum pinOrientationAfterRotationEnum)
  {
    Assert.expect(pinOrientationAfterRotationEnum != null);

    Assert.expect(pinOrientationAfterRotationEnum.equals(PinOrientationAfterRotationEnum.NOT_APPLICABLE) == false);

    double degrees = getLandPatternPad().getDegreesRotation();
    PinOrientationEnum pinOrientationEnum = pinOrientationAfterRotationEnum.getPinOrientationEnum(degrees);

    setPinOrientationEnum(pinOrientationEnum);
  }

  /**
   * @author Bill Darbie
   */
  public PinOrientationAfterRotationEnum getPinOrientationAfterRotationEnum()
  {
    // The PackagePin will have _pinOrientationEnum set ONLY if its value has been
    // overridden from the orientation calculated from the LandPatternPad.  If it
    // has not been set, then find the setting from the LandPatternPad class and
    // return that
    PinOrientationEnum pinOrientationEnum = getPinOrientationEnum();
    double degrees = getLandPatternPad().getDegreesRotation();

    return PinOrientationAfterRotationEnum.getPinOrientationAfterRotationEnum(pinOrientationEnum, degrees);
  }

  /**
   * @author Bill Darbie
   */
  public void setJointTypeEnum(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    if (jointTypeEnum.equals(_jointTypeEnum))
      return;

    JointTypeEnum oldValue = _jointTypeEnum;
    _projectObservable.setEnabled(false);
    try
    {
      _jointTypeEnum = jointTypeEnum;

      if (_compPackage != null)
      {
        _compPackage.invalidateUsesOneJointTypeEnum();
        for (ComponentType componentType : _compPackage.getComponentTypes())
          componentType.invalidateShape();
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, jointTypeEnum, PackagePinEventEnum.JOINT_TYPE_ENUM);
  }

  /**
   * @author Bill Darbie
   */
  boolean hasJointTypeEnumAssigned()
  {
    if (_jointTypeEnum == null)
      return false;

    return true;
  }

  /**
   * @return JointTypeEnum for this joint
   * @author Andy Mechtenberg
   */
  public JointTypeEnum getJointTypeEnum()
  {
    Assert.expect(_jointTypeEnum != null);
    return _jointTypeEnum;
  }

  /**
   * @author Bill Darbie
   */
  public String getName()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.getName();
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isPadOne()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.isPadOne();
  }

  /**
   * @author Bill Darbie
   */
  public int getPitchInNanoMeters()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.getPitchInNanoMeters();
  }

  /**
   * InterPadDistance is the distance from one edge of a Pad to the edge of its nearest Pad.
   *
   * @author Bill Darbie
   */
  public int getInterPadDistanceInNanoMeters()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.getInterPadDistanceInNanoMeters();
  }

  /**
   * @return PackagePin shape not related to any component, board or panel setting.
   * The returned shape is the final, viewable shape.
   * @author Andy Mechtenberg
   */
  public java.awt.Shape getShapeInNanoMeters()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.getShapeInNanoMeters();
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasPadType()
  {
    Assert.expect(_compPackage != null);

    boolean hasPadType = false;
    String padName = getName();
    for (ComponentType componentType : _compPackage.getComponentTypes())
    {
      if (componentType.hasPadType(padName))
      {
        hasPadType = true;
        break;
      }
    }

    return hasPadType;
  }

  /**
   * Retrives the applicable Subtype for this PackagePin.
   * Update in December 2007 -- commented out.  This method makes little sense
   * as a package pin may have more than one subtype associated with it.  This
   * particular implementation finds just the first one, which is misleading.
   * No one should be using this.
   *
   * @author Matt Wharton
   */
//  public Subtype getSubtype()
//  {
//    PadType padType = getPadType();
//    return padType.getSubtype();
//  }

  /**
   * Retrieves the applicable ComponentType for this PackagePin.
   *
   * @author Matt Wharton
   */
  public ComponentType getComponentType()
  {
    Assert.expect(_compPackage != null);

    ComponentType componentType = null;
    for (ComponentType candidateComponentType : _compPackage.getComponentTypes())
    {
      if (_compPackage == candidateComponentType.getCompPackage())
      {
        componentType = candidateComponentType;
        break;
      }
    }

    Assert.expect(componentType != null);
    return componentType;
  }

  /**
   * @author George A. David
   */
  void invalidateJointHeight()
  {
    _jointHeightInNanoMeters = -1;
  }

  /**
   * @author George A. David
   * @editedby Kee Chin Seong
   */
  public int getJointHeightInNanoMeters()
  {
    int jointHeight = -1;
    if(_customJointHeightInNanoMeters > 0)
    {
      jointHeight = _customJointHeightInNanoMeters;
    }
    else
    {
      if(_jointHeightInNanoMeters < 0)
      {
        if (_jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
        {
          double jointHeightInMils = MathUtil.convertNanoMetersToMils(2.0 * _landPatternPad.getWidthInNanoMeters() / 3.0);

          // now round it to the nearest mil
          jointHeightInMils = Math.round(jointHeightInMils);
          _jointHeightInNanoMeters = MathUtil.convertMilsToNanoMetersInteger(jointHeightInMils);
        }
        else if (_jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA))
          _jointHeightInNanoMeters = _landPatternPad.getWidthInNanoMeters();
        else if (_jointTypeEnum.equals(JointTypeEnum.CGA))
          _jointHeightInNanoMeters = 2 * _landPatternPad.getWidthInNanoMeters();
        else if (_jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
          _jointHeightInNanoMeters = (int)Math.round(2.0 * _landPatternPad.getWidthInNanoMeters() / 3.0);
        else if(_landPatternPad.isSurfaceMountPad())
          _jointHeightInNanoMeters = MathUtil.convertMilsToNanoMetersInteger(3);
        else if(_landPatternPad.isThroughHolePad())
          _jointHeightInNanoMeters = Project.getCurrentlyLoadedProject().getPanel().getThicknessInNanometers();
        else
          Assert.expect(false);
      }
      
      //Kee Chin Seong - To ensure that the jointHeight is not equal or less than 0, default must be 1 !
      if(_jointHeightInNanoMeters <= 0)
        _jointHeightInNanoMeters = 1;

      jointHeight = _jointHeightInNanoMeters;
    }

    Assert.expect(jointHeight > 0);

    return jointHeight;
  }

  /**
   * @author George A. David
   * @author Laura Cormos
   */
  public void setCustomJointHeightInNanoMeters(int customJointHeightInNanoMeters)
  {
    Assert.expect(customJointHeightInNanoMeters >= 0);

    if(_customJointHeightInNanoMeters == customJointHeightInNanoMeters)
      return;

    int oldJointHeight = _customJointHeightInNanoMeters;

    _projectObservable.setEnabled(false);
    try
    {
      _customJointHeightInNanoMeters = customJointHeightInNanoMeters;
      if (_compPackage != null)
      {
        _compPackage.invalidateUsesOneJointHeight();
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldJointHeight, _customJointHeightInNanoMeters, PackagePinEventEnum.CUSTOM_JOINT_HEIGHT);
  }

  /**
   * @author George A. David
   */
  public int getCustomJointHeightInNanoMeters()
  {
    Assert.expect(_customJointHeightInNanoMeters >= 0);
    return _customJointHeightInNanoMeters;
  }
}
