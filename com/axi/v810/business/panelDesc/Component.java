package com.axi.v810.business.panelDesc;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

//ShengChuan - Clear Tombstone
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.algorithmLearning.*;

/**
 * This class describes any component on a board.  For each physical
 * Component on a SideBoard, there is an instance of this class.
 *
 * @author Bill Darbie
 */
public class Component implements Serializable
{
  // created by ComponentNdfReader, PanelReader
  private ComponentType _componentType;
  private transient String _fivedxPackageName = null;

  private Map<String, Pad> _padNameToPadMap = new TreeMap<String, Pad>(new AlphaNumericComparator()); // BoardNdfReader, PanelReader

  private SideBoard _sideBoard; // BoardNdfReader, PanelReader

  private transient java.awt.Shape _shape;
  private transient java.awt.Shape _shapeBoundedByPads;
  private transient java.awt.Shape _shapeSurroundingPads;
  // this is the actual area taken up by the pads which is not to be confused
  // by the bounds of the pads. The area will know where the empty space is.
  // This is what I need. George A. David
  private transient java.awt.geom.Area _padArea;
  private transient Point2D _rotationPoint;
  private transient DoubleRef _degrees;
  private static transient ProjectObservable _projectObservable;
  private static transient Alignment _alignment;
  
  //Kee Chin Seong - By Default, this component no need to be bypassed.
  private boolean _isUseCustomFocusRegion = false;
  
  // bee-hoon.goh, Image Stitching
  private transient List <ReconstructedImages> _reconstructedImagesList;
  private transient List<ReconstructionRegion> _reconstructionRegionsList;
  private transient List<String> _reconstructionRegionsNameList;
  private transient int _numberOfReconstructionRegion = 0;
  
  private transient AlgorithmExpectedImageTemplateLearningReaderWriter _expectedImageTemplateLearning;//ShengChuan - Clear Tombstone
  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
    _alignment = Alignment.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public Component(Component rhs)
  {
    Assert.expect(rhs != null);

    _projectObservable.setEnabled(false);
    try
    {
      // shallow copy ComponentType
      _componentType = rhs._componentType;
      _componentType.addComponent(this);
      // deep copy Pads
      if (rhs._padNameToPadMap != null)
      {
        for (Pad rhsPad : rhs._padNameToPadMap.values())
        {
          Pad newPad = new Pad(rhsPad);
          addPad(newPad);
        }
      }

      // shallow copy SideBoard
      _sideBoard = rhs._sideBoard;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, this, ComponentEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public Component()
  {
    _projectObservable.stateChanged(this, null, this, ComponentEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      getSideBoard().removeComponent(this);
      getComponentType().removeComponent(this);
      for (Pad pad : getPads())
        pad.destroy();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, ComponentEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void setComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);

    if (componentType == _componentType)
      return;

    ComponentType oldValue = _componentType;
    _projectObservable.setEnabled(false);
    try
    {
      _componentType = componentType;
      _componentType.addComponent(this);
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, componentType, ComponentEventEnum.COMPONENT_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public ComponentType getComponentType()
  {
    Assert.expect(_componentType != null);

    return _componentType;
  }

  /**
   * @author Bill Darbie
   */
  public ComponentTypeSettings getComponentTypeSettings()
  {
    Assert.expect(_componentType != null);

    return _componentType.getComponentTypeSettings();
  }

  /**
   * @author Bill Darbie
   */
  public String getReferenceDesignator()
  {
    Assert.expect(_componentType != null);

    return _componentType.getReferenceDesignator();
  }

  /**
   * Return the name of the component with the board name prepended to it.
   * @author Peter Esbensen
   */
  public String getBoardNameAndReferenceDesignator()
  {
    Assert.expect(_componentType != null);
    return getBoard().getName() + "_" + _componentType.getReferenceDesignator();
  }

  /**
   * @author Bill Darbie
   */
  public Board getBoard()
  {
    return getSideBoard().getBoard();
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getReferenceDesignator();
  }

  /**
   * @author Bill Darbie
   */
  public PadTypeEnum getPadTypeEnum()
  {
    Assert.expect(_componentType != null);

    return _componentType.getPadTypeEnum();
  }

  /**
   * @author Bill Darbie
   */
  /**
   * @author Bill Darbie
   */
  public boolean isOnSideBoard1()
  {
    if (getSideBoard().isSideBoard1())
      return true;
    return false;
  }

  /**
   * @author George A. David
   */
  public void setSideBoard(SideBoard sideBoard)
  {
    Assert.expect(sideBoard != null);

    if (sideBoard == _sideBoard)
      return;

    SideBoard oldValue = _sideBoard;
    _projectObservable.setEnabled(false);
    try
    {
      _sideBoard = sideBoard;
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, sideBoard, ComponentEventEnum.SIDE_BOARD);
  }

  /**
   * @author George A. David
   */
  public SideBoard getSideBoard()
  {
    Assert.expect(_sideBoard != null);

    return _sideBoard;
  }

  /**
   * @author Bill Darbie
   */
  void changeSide()
  {
    SideBoard sideBoard = null;
    SideBoard newSideBoard = null;

    _projectObservable.setEnabled(false);
    try
    {
      sideBoard = getSideBoard();
      Board board = sideBoard.getBoard();
      Assert.expect(board.sideBoard1Exists());
      Assert.expect(board.sideBoard2Exists());

      SideBoard sideBoard1 = board.getSideBoard1();
      SideBoard sideBoard2 = board.getSideBoard2();
      if (sideBoard == sideBoard1)
      {
        newSideBoard = sideBoard2;
      }
      else if (sideBoard == sideBoard2)
      {
        newSideBoard = sideBoard1;
      }
      else
        Assert.expect(false);
      _sideBoard.removeComponent(this);
      setSideBoard(newSideBoard);
      newSideBoard.addComponent(this);
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, sideBoard, newSideBoard, ComponentEventEnum.CHANGE_SIDE);
  }

  /**
   * @return the pin specified as pin on for this component.
   * @author Erica Wheatcroft
   */
  public Pad getPadOne()
  {
    Assert.expect(_padNameToPadMap != null);

    Pad padOne = null;
    for (Pad pad : _padNameToPadMap.values())
    {
      if (pad.isPadOne())
      {
        padOne = pad;
        break;
      }
    }

    return padOne;
  }
  
  /**
   * get first pad by joint type enum
   * //XCR-3455, Wrong image display if pad 1 is different joint type
   * @author Yong Sheng Chuan
   */
  public Pad getPadOne(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(_padNameToPadMap != null);
    Assert.expect(jointTypeEnum != null);
    
    Pad padOne = null;
    for (Pad pad : _padNameToPadMap.values())
    {
      if (pad.isPadOne() && pad.getJointTypeEnum().equals(jointTypeEnum))
      {
        padOne = pad;
        break;
      }
    }

    if (padOne == null)
    {
      for (Pad pad : _padNameToPadMap.values())
      {
        if (pad.getJointTypeEnum().equals(jointTypeEnum))
        {
          padOne = pad;
          break;
        }
      }
    }
    return padOne;
  }

  /**
   * @author Keith Lee
   */
  public void addPad(Pad pad)
  {
    Assert.expect(pad != null);
    Assert.expect(_padNameToPadMap != null);

    _projectObservable.setEnabled(false);
    try
    {
      Pad prev = _padNameToPadMap.put(pad.getName(), pad);
      Assert.expect(prev == null);
      pad.setComponent(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, pad, ComponentEventEnum.ADD_OR_REMOVE_PAD);
  }

  /**
   * @author Bill Darbie
   */
  public void removePad(Pad pad)
  {
    Assert.expect(pad != null);

    _projectObservable.setEnabled(false);
    try
    {
      Pad prev = _padNameToPadMap.remove(pad.getName());
      Assert.expect(prev != null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, pad, null, ComponentEventEnum.ADD_OR_REMOVE_PAD);
  }

  /**
   * Create new Pads for this Component using pre-existing PadTypes.
   * @author Bill Darbie
   */
  void createPads()
  {
    List<Pad> pads = new ArrayList<Pad>();
    for (PadType padType : getComponentType().getPadTypes())
    {
      Pad pad = new Pad();
      pad.setNodeName("");
      PadSettings padSettings = new PadSettings();
      pad.setPadSettings(padSettings);
      padSettings.setPad(pad);
      //Kok Chun, Tan - XCR-2087
//      padSettings.setTestable(padType.isInspected());
      padSettings.setTestable(true);
      pad.setComponent(this);
      pads.add(pad);
      pad.setPadType(padType);
      addPad(pad);
      padType.addPad(pad);
    }
  }

  /**
   * @author Keith Lee
   */
  public List<Pad> getPads()
  {
    Assert.expect(_padNameToPadMap != null);

    return new ArrayList<Pad>(_padNameToPadMap.values());
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasPad(String padName)
  {
    Assert.expect(padName != null);
    Assert.expect(_padNameToPadMap != null);

    boolean padExists = false;
    for (Pad pad : _padNameToPadMap.values())
    {
      if (pad.getName().equalsIgnoreCase(padName))
      {
        padExists = true;
        break;
      }
    }

    return padExists;
  }

  /**
   * @author Bill Darbie
   */
  public Pad getPad(String padName)
  {
    Assert.expect(padName != null);
    Assert.expect(_padNameToPadMap != null);

    Pad matchedPad = null;
    for (Pad pad : _padNameToPadMap.values())
    {
      if (pad.getName().equalsIgnoreCase(padName))
      {
        matchedPad = pad;
        break;
      }
    }

    Assert.expect(matchedPad != null);
    return matchedPad;
  }

  /**
    * @author Bill Darbie
    */
   void setNewPadName(String oldName, String newName)
   {
     Assert.expect(oldName != null);
     Assert.expect(newName != null);

     Pad pad = _padNameToPadMap.get(oldName);

     Pad prev = null;
     if (oldName != null)
     {
       prev = _padNameToPadMap.remove(oldName);
       Assert.expect(prev != null);
     }

     prev = _padNameToPadMap.put(newName, pad);
     Assert.expect(prev == null);
   }

  /**
   * @author Bill Darbie
   */
  public CompPackage getCompPackage()
  {
    Assert.expect(_componentType != null);

    return _componentType.getCompPackage();
  }

  /**
   * @author Bill Darbie
   */
  public LandPattern getLandPattern()
  {
    Assert.expect(_componentType != null);

    return _componentType.getLandPattern();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isLoaded()
  {
    return getComponentTypeSettings().isLoaded();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isInspected()
  {
    boolean inspected = false;
    if (getBoard().isInspected())
      inspected = getComponentType().isInspected();

    return inspected;
  }

  /**
   * @return true if this Component is facing the x-ray soure.
   * @author Andy Mechtenberg
   */
  public boolean isTopSide()
  {
    return getSideBoard().isTopSide();
  }

  /**
   * @return true if this Component is NOT facing the x-ray source.
   * @author Bill Darbie
   */
  public boolean isBottomSide()
  {
    return !isTopSide();
  }

  /**
   * This coordinate is not necessarily the center of the component, but is the
   * point from which the pad/pin coordinates are referenced.
   * @return the coordinate of this Component relative to its Boards bottom left corner.
   * @author Bill Darbie
   */
  public BoardCoordinate getCoordinateInNanoMeters()
  {
    Assert.expect(_componentType != null);

    return _componentType.getCoordinateInNanoMeters();
  }

  /**
   * @author Bill Darbie
   */
  public PanelCoordinate getCoordinateRelativeToPanelInNanoMeters()
  {
    createShapesRelativeToPanelIfNecessary();
    int x = MathUtil.roundDoubleToInt(_rotationPoint.getX());
    int y = MathUtil.roundDoubleToInt(_rotationPoint.getY());

    return new PanelCoordinate(x, y);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public PanelCoordinate getCoordinateRelativeToCadOriginInNanoMeters()
  {
    createShapesRelativeToCadOriginIfNecessary();
    
    int x = MathUtil.roundDoubleToInt(_rotationPoint.getX());
    int y = MathUtil.roundDoubleToInt(_rotationPoint.getY());

    return new PanelCoordinate(x, y);
  }
  
  /**
   * @return the lower left coordinate of this Component relative to its Panels bottom left corner
   * after all rotations have been applied.
   * @author Bill Darbie
   */
  public PanelCoordinate getLowerLeftCoordinateRelativeToPanelInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    java.awt.geom.Rectangle2D bounds = shape.getBounds2D();

    int x = MathUtil.roundDoubleToInt(bounds.getMinX());
    int y = MathUtil.roundDoubleToInt(bounds.getMinY());

    return new PanelCoordinate(x, y);
  }

  /**
   * @return the center coordinate of this Component relative to its Panels bottom left corner
   * after all rotations have been applied.
   * @author Bill Darbie
   */
  public PanelCoordinate getCenterCoordinateRelativeToPanelInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    java.awt.geom.Rectangle2D bounds = shape.getBounds2D();

    return new PanelCoordinate(MathUtil.roundDoubleToInt(bounds.getCenterX()), MathUtil.roundDoubleToInt(bounds.getCenterY()));
  }

  /**
   * @return the x axis width of the Component BEFORE any rotation has been applied.
   * @author Bill Darbie
   */
  public int getWidthInNanoMeters()
  {
    Assert.expect(_componentType != null);

    return _componentType.getWidthInNanoMeters();
  }

  /**
   * @return the x axis width of the Component AFTER the Board and Panel have been rotated.
   * @author Bill Darbie
   */
  public int getWidthAfterAllRotationsInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    java.awt.Rectangle bounds = shape.getBounds();

    return bounds.width;
  }

  /**
   * @return the y axis length of the Component BEFORE any rotation has been applied.
   * @author Bill Darbie
   */
  public int getLengthInNanoMeters()
  {
    Assert.expect(_componentType != null);

    return _componentType.getLengthInNanoMeters();
  }

  /**
   * @return the y axis length of the Component AFTER the Board and Panel have been rotated.
   * @author Bill Darbie
   */
  public int getLengthAfterAllRotationsInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    java.awt.Rectangle bounds = shape.getBounds();

    return bounds.height;
  }

  /**
   * @author Bill Darbie
   */
  public double getDegreesRotationRelativeToBoard()
  {
    Assert.expect(_componentType != null);

    return _componentType.getDegreesRotationRelativeToBoard();
  }

  /**
   * @return the degrees rotation after all rotations from this Component, its Board, and Panel have been
   * applied.
   *
   * @author Bill Darbie
   */
  public double getDegreesRotationAfterAllRotations()
  {
    if (_degrees == null)
    {
      double degrees = getDegreesRotationRelativeToBoard();
      java.awt.geom.AffineTransform transform = java.awt.geom.AffineTransform.getRotateInstance(Math.toRadians(degrees));

      // apply the transform of the component.
      getBoard().preConcatenateShapeTransform(transform, false, true, true);

      Point2D unitVector = new Point2D.Double(1, 0);
      Point2D transformedPoint = transform.transform(unitVector, null);

      double x1 = unitVector.getX();
      double y1 = unitVector.getY();
      double x2 = transformedPoint.getX();
      double y2 = transformedPoint.getY();

      // get the angle between x1,y1 and x2,y2
      degrees = Math.toDegrees(Math.acos(x1 * x2 + y1 * y2));

      // if y2 is positive then the degrees should be from 0 to 180, so do nothing
      // if y2 is negative then the degrees should be from 180 to 360
      // since the acos returns degrees from 0 to 180 only, we must compenstate here
      if (y2 < 0)
        degrees = -degrees;

      degrees = MathUtil.getDegreesWithin0To359(degrees);
      _degrees = new DoubleRef(degrees);
    }

    Assert.expect(_degrees !=  null);
    return _degrees.getValue();
  }

  /**
   * @author Bill Darbie
   */
  public List<JointTypeEnum> getJointTypeEnums()
  {
    Assert.expect(_componentType != null);
    return _componentType.getJointTypeEnums();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isComponentWithinBoardOutline()
  {
    return _componentType.isComponentTypeWithinBoardTypeOutline();
  }

  /**
   * @return false if the process of moving this component by the offset passed in will cause the component to
   * move off the board.
   * @author Bill Darbie
   */
  public boolean willComponentBeInsideBoardOutlineAfterOffset(int xOffsetAfterAllFlipsAndRotationsInNanoMeters, int yOffsetAfterAllFlipsAndRotationsInNanometers)
  {
    // we will temporarily move the component to see if it falls outside the board outline.
    // suppress sending any ProjectStateEvents that might occur because of this since we will
    // immediatly return the component back to its original position
    getBoard().getPanel().getProject().getProjectState().disableProjectStateEvents();
    boolean compWithinBoard = false;
    try
    {
      moveByOffSetInNanoMeters(xOffsetAfterAllFlipsAndRotationsInNanoMeters,
                               yOffsetAfterAllFlipsAndRotationsInNanometers);
      compWithinBoard = isComponentWithinBoardOutline();
      moveByOffSetInNanoMeters( -xOffsetAfterAllFlipsAndRotationsInNanoMeters,
                               -yOffsetAfterAllFlipsAndRotationsInNanometers);
    }
    finally
    {
      getBoard().getPanel().getProject().getProjectState().enableProjectStateEvents();
    }

    return compWithinBoard;
  }

  /**
   * Move this Components ComponentType by the offset passed in.  The offset passed in is an offset of the
   * Component as views from x-rays after all rotations and flips have occured.
   *
   * @author Bill Darbie
   */
  public void moveByOffSetInNanoMeters(int xOffsetAfterAllFlipsAndRotationsInNanoMeters, int yOffsetAfterAllFlipsAndRotationsInNanometers)
  {
    // create the correct transform
    AffineTransform transform = AffineTransform.getTranslateInstance(0, 0);
    preConcatenateShapeTransform(transform, false, false, true);

    // use the inverse of that transform to figure out the correct offset
    Point2D sourcePoint = new Point2D.Double(xOffsetAfterAllFlipsAndRotationsInNanoMeters, yOffsetAfterAllFlipsAndRotationsInNanometers);
    Point2D destPoint = new Point2D.Double(0, 0);
    try
    {
      transform.inverseTransform(sourcePoint, destPoint);
    }
    catch (NoninvertibleTransformException nte)
    {
      Assert.expect(false);
    }

    int xOffset = MathUtil.roundDoubleToInt(destPoint.getX());
    int yOffset = MathUtil.roundDoubleToInt(destPoint.getY());

    // apply the offset to the ComponentType
    ComponentType componentType = getComponentType();
    BoardCoordinate boardCoord = componentType.getCoordinateInNanoMeters();
    boardCoord.setX(boardCoord.getX() + xOffset);
    boardCoord.setY(boardCoord.getY() + yOffset);
    componentType.setCoordinateInNanoMeters(boardCoord);
  }

  /**
   * @author Bill Darbie
   */
  void invalidateShape()
  {
    _shape = null;
    _shapeBoundedByPads = null;
    _shapeSurroundingPads = null;
    _padArea = null;
    _degrees = null;
    for (Pad pad : getPads())
      pad.invalidateShape();
  }

  /**
   * @author Bill Darbie
   */
  void invalidateShapeForPadChangeOnly()
  {
    _shape = null;
    _shapeBoundedByPads = null;
    _shapeSurroundingPads = null;
    _padArea = null;
    _degrees = null;
  }

  /**
   * @return the component's shape, after rotating the component and shifting it relative to its boards lower
   * left corner.
   * @author Bill Darbie
   */
  public java.awt.Shape getShapeSurroundingPadsInNanoMeters()
  {
    Assert.expect(_componentType != null);

    return _componentType.getShapeSurroundingPadsInNanoMeters();
  }

  /**
   * @return the component's shape, after rotating the component and shifting it relative to its boards lower
   * left corner.
   * @author Matt Wharton
   */
  public java.awt.Shape getShapeBoundedByPadsInNanoMeters()
  {
    Assert.expect(_componentType != null);

    return _componentType.getShapeBoundedByPadsInNanoMeters();
  }

  /**
   * @return the component's shape, after rotating the component and shifting it relative to its boards lower
   * left corner.
   * @author Andy Mechtenberg
   */
  public java.awt.Shape getShapeInNanoMeters()
  {
    Assert.expect(_componentType != null);

    return _componentType.getShapeInNanoMeters();
  }

  /**
   * @author Bill Darbie
   */
  public void createShapesRelativeToPanelIfNecessary()
  {
    createShapesRelativeToPanelOrCadIfNecessary(true);
  }
  
  /**
   * @author Bill Darbie
   */
  public void createShapesRelativeToPanelOrCadIfNecessary(boolean relativeToPanel)
  {
    Assert.expect(_componentType != null);

    if (_shape == null ||
        _shapeSurroundingPads == null ||
        _padArea == null ||
        _shapeBoundedByPads == null)
    {
      // build up the proper transform
      AffineTransform transform = AffineTransform.getTranslateInstance(0, 0);

      Assert.expect(_sideBoard != null);
      
      //Siew Yeng - relative to panel
      if(relativeToPanel)
        getBoard().preConcatenateShapeTransform(transform);
      else //relative to CAD origin
        getBoard().preConcatenateShapeTransform(transform, false);

      // create the initial shape
      // make sure that the ComponentType call here does not apply negative rotation for side2 ComponentTypes
      // since the left-to-right flip code above already does that
      _shape = _componentType.getShapeSurroundingPadsInNanoMeters();
      _shapeBoundedByPads = _componentType.getShapeBoundedByPadsInNanoMeters();
      _shapeSurroundingPads = _componentType.getShapeSurroundingPadsInNanoMeters();
      _padArea = _componentType.getPadAreaInNanoMeters();
      BoardCoordinate boardCoord = getCoordinateInNanoMeters();
      Point2D rotationPoint = new Point2D.Double(boardCoord.getX(), boardCoord.getY());

      // apply the transform to it
      _shape = transform.createTransformedShape(_shape);
      _shapeBoundedByPads = transform.createTransformedShape(_shapeBoundedByPads);
      _shapeSurroundingPads = transform.createTransformedShape(_shapeSurroundingPads);
      _rotationPoint = transform.transform(rotationPoint, _rotationPoint);
      _padArea = new Area(transform.createTransformedShape(_padArea));
    }
  }

  /**
   * @author Phang Siew Yeng
   */
  public void createShapesRelativeToCadOriginIfNecessary()
  {
    //invalidate shape relative to panel
    invalidateShape();
    
    createShapesRelativeToPanelOrCadIfNecessary(false);
    
    //invalidate shape relative to cad origin
    invalidateShape();
  }

  /**
   * Return the shape after relative to the Panels lower left corner as seen by x-rays.
   * @return the component's shape, after rotating the component, and then applying the board and panel rotations
   * @author Andy Mechtenberg
   */
  public java.awt.Shape getShapeRelativeToPanelInNanoMeters()
  {
    createShapesRelativeToPanelIfNecessary();
    Assert.expect(_shape != null);
    return _shape;
  }

  /**
   * @author Matt Wharton
   */
  public java.awt.Shape getShapeBoundedByPadsRelativeToPanelInNanoMeters()
  {
    createShapesRelativeToPanelIfNecessary();
    Assert.expect(_shapeBoundedByPads != null);
    return _shapeBoundedByPads;
  }

  /**
   * @author George A. David
   */
  public java.awt.Shape getShapeSurroundingPadsRelativeToPanelInNanoMeters()
  {
    createShapesRelativeToPanelIfNecessary();
    Assert.expect(_shapeSurroundingPads != null);
    return _shapeSurroundingPads;
  }

  /**
   * this is the actual area taken up by the pads which is not to be confused
   * by the bounds of the pads. The area will know where the empty space is.
   * This is what I need. George A. David
   * @author George A. David
   */
  public java.awt.geom.Area getPadAreaRelativeToPanelInNanoMeters()
  {
    createShapesRelativeToPanelIfNecessary();
    return _padArea;
  }


  /**
   * Returns a shape relative to the Panel's lower left, but taking
   * into account the alignment matrix.
   * @author George A. David
   * @author Wei Chin, Chong
   */
  public java.awt.Shape getAlignedShapeRelativeToPanelInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    Panel panel = getBoard().getPanel();

    if(panel.getPanelSettings().isPanelBasedAlignment())
    {
      if (panel.getPanelSettings().getRightSectionOfLongPanel().containsShape(shape))
        shape = _alignment.getCorrectedPositionInPanelCoordinates(shape, panel.getProject().getTestProgram().getTestSubProgram(PanelLocationInSystemEnum.RIGHT));
      else
        shape = _alignment.getCorrectedPositionInPanelCoordinates(shape, panel.getProject().getTestProgram().getTestSubProgram(PanelLocationInSystemEnum.LEFT));
    }
    else
    {
      if(getBoard().getBoardSettings().isLongBoard())
      {
        if (getBoard().getRightSectionOfLongBoard().containsShape(shape))
          shape = _alignment.getCorrectedPositionInPanelCoordinates(shape, panel.getProject().getTestProgram().getTestSubProgram(getBoard(), PanelLocationInSystemEnum.RIGHT));
        else
          shape = _alignment.getCorrectedPositionInPanelCoordinates(shape, panel.getProject().getTestProgram().getTestSubProgram(getBoard(), PanelLocationInSystemEnum.LEFT));
      }
      else
        shape = _alignment.getCorrectedPositionInPanelCoordinates(shape, panel.getProject().getTestProgram().getTestSubProgram(getBoard()));
    }

    return shape;
  }

  /**
   * Take the transform passed in and modify it to take into account this Components rotation and where
   * it is relative to the Boards bottom left corner as seen by x-rays.
   *
   * @author Bill Darbie
   */
  public void preConcatenateShapeTransform(AffineTransform transform)
  {
    Assert.expect(transform != null);

    preConcatenateShapeTransform(transform, true, true, false);
  }

  /**
   * Take the transform passed in and modify it to take into account this Components rotation and where
   * it is relative to the Boards bottom left corner as seen by x-rays.
   *
   * @author Bill Darbie
   */
  void preConcatenateShapeTransform(AffineTransform transform,
                                    boolean applyComponentAndBoardOriginTranslation,
                                    boolean applyComponentRotation,
                                    boolean ignoreBoardSide)
  {
    Assert.expect(transform != null);

    _componentType.preConcatenateShapeTransform(transform, applyComponentAndBoardOriginTranslation, applyComponentRotation, ignoreBoardSide);
    getSideBoard().getBoard().preConcatenateShapeTransform(transform, applyComponentAndBoardOriginTranslation, true, true);
  }
  
  /**
   * @return the _fivedxPackageName
   * @author Jack Hwee
   */
  public String getFivedxPackageName()
  {
    return _fivedxPackageName;
  }

  /**
   * @param fivedxPackageName the _fivedxPackageName to set
   * @author Jack Hwee
   */
  public void setFivedxPackageName(String fivedxPackageName)
  {
    Assert.expect(fivedxPackageName != null);
    this._fivedxPackageName = fivedxPackageName;
  }

   /**
   * @author Jack Hwee
   */
  public boolean has5dxPackageName()
  {
   return _fivedxPackageName!= null;
  }

  /**
   * Get the region of interest for the component relative to the rotated
   * panel in pixels.
   *
   * @author Sham
   */
  public RegionOfInterest getComponentShapeRelativeToPanelInPixels()
  {
    RegionOfInterest componentShapeRelativeToPanelInPixels = null;

    // initialize region to be the same as pad one, that way we have the right orientation
    componentShapeRelativeToPanelInPixels = new RegionOfInterest(getPadOne().getPadShapeRelativeToPanelInPixels());

    for (Pad pad : getPads())
    {
      componentShapeRelativeToPanelInPixels.add(pad.getPadShapeRelativeToPanelInPixels());
    }

    return componentShapeRelativeToPanelInPixels;
  }
  
  /**
   * @author sham
   */
  public boolean isComponentUseSingleMagnification()
  {
    //Siew Yeng - XCR-3851
    List<PadType> inspectedPadTypes = getComponentType().getInspectedPadTypes();
    
    //no test component is consider as single mag
    if(inspectedPadTypes.isEmpty())
      return true;
    
    MagnificationTypeEnum magnificationTypeEnum = inspectedPadTypes.get(0).getSubtype().getSubtypeAdvanceSettings().getMagnificationType();  
    for(PadType padType : inspectedPadTypes)
    {
      if(padType.getSubtype().getSubtypeAdvanceSettings().getMagnificationType().equals(magnificationTypeEnum) == false)
      {
        return false;
      }
    }
    
    return true;
  }
  
  /**
   * This function returns the magnification associated with ComponentType.
   * To use this function, we need to make sure it does not contain mixed magnification.
   * 
   * @author Cheah Lee Herng 
   */
  public boolean isHighMagnification()
  {
    Assert.expect(isComponentUseSingleMagnification());
    
    MagnificationTypeEnum magnificationTypeEnum = getComponentType().getPadTypes().get(0).getSubtype().getSubtypeAdvanceSettings().getMagnificationType();
    if (magnificationTypeEnum.equals(MagnificationTypeEnum.HIGH))
      return true;
    else
      return false;
  }
  
  /**
   * This function returns the magnification associated with ComponentType.
   * To use this function, we need to make sure it does not contain mixed magnification.
   * 
   * @author Cheah Lee Herng 
   */
  public boolean isLowMagnification()
  {
    Assert.expect(isComponentUseSingleMagnification());
    
    MagnificationTypeEnum magnificationTypeEnum = getComponentType().getPadTypes().get(0).getSubtype().getSubtypeAdvanceSettings().getMagnificationType();
    if (magnificationTypeEnum.equals(MagnificationTypeEnum.LOW))
      return true;
    else
      return false;
  }
  
  /*
   * @Author Kee Chin Seong
   */
  public void setUseCustomFocusRegion(boolean isNeedByPassedDueToFocusRegionOffset)
  {
    _isUseCustomFocusRegion = isNeedByPassedDueToFocusRegionOffset;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isUseCustomFocusRegion()
  {
    return _isUseCustomFocusRegion;
  }
  
  /**
   * Get the number of reconstruction region name of a component
   * @author Bee Hoon
   */
  public List<String> getReconstructionRegionNameList()
  {
    JointInspectionData jointInspectionData;
    ReconstructionRegion jointInspectionRegion;
    String imageName;

    if(_reconstructionRegionsNameList == null)
      _reconstructionRegionsNameList = new ArrayList<String>();
    
    if(_reconstructionRegionsNameList.isEmpty() == false)
      _reconstructionRegionsNameList.clear();
    
    for (Pad pad : this.getPads())
    {
      if(pad.isInspected() == false)
        continue;
      
      jointInspectionData = this.getBoard().getPanel().getProject().getTestProgram().getJointInspectionData(pad);
      jointInspectionRegion = jointInspectionData.getInspectionRegion();

      imageName = jointInspectionRegion.getName();   
      if(_reconstructionRegionsNameList.contains(imageName) == false) 
        _reconstructionRegionsNameList.add(imageName);
    }
    
    return _reconstructionRegionsNameList;
  }
  
  /**
   * Get reconstruction region list of this component
   * @author Siew Yeng
   */
  public List<ReconstructionRegion> getReconstructionRegionList(TestProgram testProgram)
  {
    JointInspectionData jointInspectionData;
    ReconstructionRegion jointInspectionRegion;

    if(_reconstructionRegionsList == null)
      _reconstructionRegionsList = new ArrayList<ReconstructionRegion>();
    
    if(_reconstructionRegionsList.isEmpty() == false)
      _reconstructionRegionsList.clear();

    for (Pad pad : this.getPads())
    {
      if(pad.isInspected() == false)
        continue;
      
      jointInspectionData = testProgram.getJointInspectionData(pad);
      jointInspectionRegion = jointInspectionData.getInspectionRegion();
  
      if(_reconstructionRegionsList.contains(jointInspectionRegion) == false) 
        _reconstructionRegionsList.add(jointInspectionRegion);
    }
    
    return _reconstructionRegionsList;
  }

  /**
   * Clear Tombstone
   * @return true if there is any template learned.
   * //XCR-3202, Assert when perform template match using resize image
   * @author sheng chuan
   */
  public boolean hasExpectedImageTemplateLearning(Pad pad,Board board, SliceNameEnum sliceNameEnum, Image image) throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(pad != null);

    getAlgorithmTemplateLearningReference();
    boolean hasTemplateLearning = _expectedImageTemplateLearning.doesExpectedImageLearningExist(pad,board, sliceNameEnum);

    //XCR-3202, Assert when perform template match using resize image
    if(hasTemplateLearning)
    {
      Image learnedImage = getExpectedImageLearning(pad,board, sliceNameEnum).getLearnedExpectedImage();
      if(learnedImage.getHeight() != image.getHeight() || learnedImage.getWidth() != image.getWidth())
      {
        hasTemplateLearning = false;
      }
      learnedImage.decrementReferenceCount();
    }
    
    return hasTemplateLearning;
  }
  
  /**
   * Clear Tombstone
   * @return true if there is any template learned.
   *
   * @author sheng chuan
   */
  public boolean hasExpectedImageTemplateLearning(Pad pad) throws DatastoreException
  {
    Assert.expect(pad != null);

    getAlgorithmTemplateLearningReference();
    boolean hasShortProfileLearning = _expectedImageTemplateLearning.doesExpectedImageLearningExist(pad);

    return hasShortProfileLearning;
  }

  /**
   * Clear Tombstone
   * @author sheng chuan
   */
  private void getAlgorithmTemplateLearningReference()
  {
    if (_expectedImageTemplateLearning == null)
      _expectedImageTemplateLearning = this.getBoard().getPanel().getProject().getAlgorithmExpectedImageTemplateLearningReaderWriter();
  }

  /*
   * Clear Tombstone
   * @author sheng chuan
   */
  public ExpectedImageTemplateLearning getExpectedImageLearning(Pad pad, Board board, SliceNameEnum sliceNameEnum)
      throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(pad != null);

    getAlgorithmTemplateLearningReference();
    ExpectedImageTemplateLearning templateLearning = _expectedImageTemplateLearning.readExpectedImageLearning(pad, board, sliceNameEnum);
    Assert.expect(templateLearning != null);

    return templateLearning;
  }
  
  /**
   * Clear Tombstone - Deletes the learned expected image data for the associated Pad.
   *
   * @author sheng chuan
   */
  public void deleteExpectedImageLearning(Pad pad) throws DatastoreException
  {
    Assert.expect(pad != null);

    // NOTE: we do not need to do a _projectObservable.stateChanged() here because
    //       the event it getting tracked at a higher level
    getAlgorithmTemplateLearningReference();
    _expectedImageTemplateLearning.deleteExpectedImageLearning(pad);
  }
  
  /**
   * Clear Tombstone
   * author sheng chuan
   */
  public void setExpectedImageLearning(ExpectedImageTemplateLearning templateLearningData) throws DatastoreException
  {
    Assert.expect(templateLearningData != null);

    // NOTE: we do not need to do a _projectObservable.stateChanged() here because
    //       the event it getting tracked at a higher level

    getAlgorithmTemplateLearningReference();
    _expectedImageTemplateLearning.writeExpectedImageTemplateLearning(templateLearningData);
  }
}
