package com.axi.v810.business.panelDesc;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * Tests the Panel class
 * @author George A. David
 */
public class Test_Panel extends UnitTest
{
  private static final int _ZERO = 0;
  private static final int _NON_ZERO = 70;
  private static final int _NINETY = 90;
  private static final int _ONE_EIGHTY = 180;
  private static final int _TWO_SEVENTY = 270;
  private static final int _NEGATIVE_NUMBER = -1;
  private static final String _TEST = "Test";

  private static Panel _panel = null;

  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Panel());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      testBasicMethods();

      testCreateDestroyMethods();

      // test convertCoordinateRelativeToPanelToMachineCoordinte
      testMachineCoordinateConversion();
      checkShapesForFamiliesAllRlv();
      checkBoardOutline();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * This tests setting and getting on the basic Panel methods.
   * It will also load a panel (twice) and do a quick check that it's populated correctly.
   * @author Andy Mechtenberg
   */
  private void testBasicMethods() throws Exception
  {
    _panel = new Panel();

    //test getCadVersion assert
    try
    {
      _panel.getCadVersion();
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }

    // test setCadVersion assert
    try
    {
      _panel.setCadVersion(null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    String cadVersion = "cad version";
    _panel.setCadVersion(cadVersion);
    Expect.expect(cadVersion.equals(_panel.getCadVersion()));

    //test getPanelSettings assert
    try
    {
      _panel.getPanelSettings();
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }

    // test setPanelSettings assert
    try
    {
      _panel.setPanelSettings(null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    PanelSettings panelSettings = new PanelSettings();
    _panel.setPanelSettings(panelSettings);
    Expect.expect(panelSettings == _panel.getPanelSettings());

    // test getWidthInNanoMeters assert
    try
    {
      _panel.getWidthInNanoMeters();
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }

    // test setWidthInNanoMeters assert
    try
    {
      _panel.setWidthInNanoMeters(-1);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    _panel.disableChecksForNdfParsers();
    int width = 100;
    _panel.setWidthInNanoMeters(width);
    Expect.expect(width == _panel.getWidthInNanoMeters());

    // test getLengthInNanoMeters assert
    try
    {
      _panel.getLengthInNanoMeters();
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }

    // test setLengthInNanoMeters assert
    try
    {
      _panel.setLengthInNanoMeters(-1);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    int length = 50;
    _panel.setLengthInNanoMeters(length);
    Expect.expect(length == _panel.getLengthInNanoMeters());

    // test getDegreesRotationRelativeToCad assert
    try
    {
      _panel.getDegreesRotationRelativeToCad();
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }

    // test getNumBoards
    Expect.expect(_panel.getNumBoards() == 0);
    _panel.enableChecksAfterNdfParsersAreComplete();
  }

  /**
   * @author Bill Darbie
   */
  void testCreateDestroyMethods()
  {
    Project project = new Project(false);

    try
    {
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();
      project = Project.importProjectFromNdfs("smallPanel", warnings);
      Panel panel = project.getPanel();
      List<Board> boards = panel.getBoards();
      int numBoards = boards.size();
      BoardType boardType = panel.getBoardTypes().get(0);

      // lets try creating a new board
      Board board = panel.createBoard(boardType, new PanelCoordinate(0, 0), 0, false);
      Assert.expect(panel.getBoards().size() == numBoards + 1);
      // now destroy the board
      board.destroy();
      Assert.expect(panel.getBoards().size() == numBoards);

      // lets try creating a new board, then removing and adding it back in
      board = panel.createBoard(boardType, new PanelCoordinate(0, 0), 0, false);
      Assert.expect(panel.getBoards().size() == numBoards + 1);
      // now destroy the board
      board.remove();
      Assert.expect(panel.getBoards().size() == numBoards);
      board.add();
      Assert.expect(panel.getBoards().size() == numBoards + 1);

      // now create a subtype
      int numSubtypes = panel.getSubtypes().size();
      LandPattern landPattern = panel.getLandPatterns().get(0);
      Subtype subtype = panel.createSubtype("newSubtype", landPattern, JointTypeEnum.COLLAPSABLE_BGA);
      Assert.expect(panel.getSubtypes().size() == numSubtypes + 1);
      // now destroy the subtype
      subtype.destroy();
      Assert.expect(panel.getSubtypes().size() == numSubtypes);

      // create a LandPattern
      int numLandPatterns = panel.getAllLandPatterns().size();
      landPattern = panel.createLandPattern("newLandPattern");
      landPattern.createSurfaceMountLandPatternPad(new ComponentCoordinate(100, 100), 0, 20, 20, ShapeEnum.CIRCLE);

      Assert.expect(panel.getAllLandPatterns().size() == numLandPatterns + 1);
      // destroy the LandPattern
      landPattern.destroy();
      Assert.expect(panel.getAllLandPatterns().size() == numLandPatterns);

      landPattern = panel.createLandPattern("newLandPattern");
      landPattern.createSurfaceMountLandPatternPad(new ComponentCoordinate(100, 100), 0, 20, 20, ShapeEnum.CIRCLE);

      Assert.expect(panel.getAllLandPatterns().size() == numLandPatterns + 1);
      // remove and add back the LandPattern
      landPattern.remove();
      Assert.expect(panel.getAllLandPatterns().size() == numLandPatterns);
      landPattern.add();
      Assert.expect(panel.getAllLandPatterns().size() == numLandPatterns + 1);


      // create a Fiducial
      int numFiducials = panel.getFiducials().size();
      Fiducial fiducial = panel.createFiducial("newFiducial", true);
      Assert.expect(panel.getFiducials().size() == numFiducials + 1);
      // destroy Fiducial
      fiducial.destroy();
      Assert.expect(panel.getFiducials().size() == numFiducials);

      // create a new one, then remove and add it back
      fiducial = panel.createFiducial("newFiducial", true);
      Assert.expect(panel.getFiducials().size() == numFiducials + 1);
      // remove Fiducial
      fiducial.remove();
      Assert.expect(panel.getFiducials().size() == numFiducials);
      fiducial.add();
      Assert.expect(panel.getFiducials().size() == numFiducials + 1);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author George A. David
   */
  private void deleteProjectlFileIfExists(String projectName)
  {
    //delete the panel file if it exists, we first want to test loading ndf files.
    String projectFullPath = FileName.getProjectSerializedFullPath(projectName);
    if (FileUtil.exists(projectFullPath))
    {
      try
      {
        FileUtilAxi.delete(FileName.getProjectSerializedFullPath(projectName));
      }
      catch(DatastoreException dex)
      {
        Expect.expect(false);

      }
    }
  }

  /**
   * @author George A. David
   */
  private void testMachineCoordinateConversion()
  {
    /** @todo wpd - G$ */
//wpd - put this back in once we have the code for alignment
//    // Test_Panel_3 uses panel based alignment
//    panelName = "Test_Panel_3";
//    panelFullPath = FileName.getPanelFullPath(panelName);
//    try
//    {
//      FileName.deleteFile(panelFullPath);
//    }
//    catch(DatastoreException dex)
//    {
//      Expect.expect(false);
//      dex.printStackTrace();
//    }
//    _panel = loadPanel(panelName, new ArrayList());
//    try
//    {
//      _panel.getPanelSettings().readLegacyAlignmentData(_panel);
//    }
//    catch(DatastoreException dex)
//    {
//      Expect.expect(false);
//      dex.printStackTrace();
//    }
//    board = (Board)_panel.getBoards().get(0);
//    coordinate = _panel.convertCoordinateRelativeToPanelToMachineCoordinate(new PanelCoordinate(125, 30), board.getTopSideBoard());
//    Expect.expect(coordinate.getX() == 221614459, "coordinate.getX() == " + coordinate.getX());
//    Expect.expect(coordinate.getY() == 232456310, "coordinate.getY() == " + coordinate.getY());
  }

  /**
   * @author George A. David
   */
  public void deleteDirectoryIncludingContents(String directoryName)
  {
    Assert.expect(directoryName != null);

    File directory = new File(directoryName);
    Assert.expect(directory.exists());
    if(directory.isDirectory())
    {
      File[] files = directory.listFiles();
      for(int i = 0; i < files.length; ++i)
      {
        if(files[i].isDirectory())
        {
          deleteDirectoryIncludingContents(files[i].getAbsolutePath());
        }
        else
        {
          files[i].delete();
        }
      }
    }

    directory.delete();
  }

  /**
   * @author George A. David
   */
  private boolean doesCustomAlgorithmFamilyExist(Panel panel, String customAlgorithmFamilyName)
  {
    Assert.expect(panel != null);
    Assert.expect(customAlgorithmFamilyName != null);

    boolean customAlgorithmFamilyExists = false;
    List<Subtype> subtypes = panel.getSubtypes();
    for (Subtype subtype : subtypes)
    {
      if(subtype.getShortName().equalsIgnoreCase(customAlgorithmFamilyName))
      {
        customAlgorithmFamilyExists = true;
        break;
      }
    }

    return customAlgorithmFamilyExists;
  }

  /**
   * @author George A. David
   */
  private boolean doesComponentExist(Panel panel, String componentName)
  {
    Assert.expect(panel != null);
    Assert.expect(componentName != null);

    boolean componentExists = false;
    List<Component> components = panel.getComponents();
    for (Component component : components)
    {
      if(component.getReferenceDesignator().equalsIgnoreCase(componentName))
      {
        componentExists = true;
        break;
      }
    }

    return componentExists;
  }

  /**
   * Compares a shape with the double data that it should contain
   * A rectangle, represented by a general path, takes the form of a SEG_MOVETO point, followed by
   * 4 SEG_LINETO points for a total of 2 + (4*2) = 10 points.
   * @author Andy Mechtenberg
   */
  private void compareRectangleShape(java.awt.Shape shape, double[] expectedCoords, boolean print)
  {
    double[] coords = new double[6];
    java.awt.geom.AffineTransform identityTransform = new java.awt.geom.AffineTransform();
    java.awt.geom.PathIterator pathIterator = shape.getPathIterator(identityTransform);

    int type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_MOVETO)
    {
      if (print)
        System.out.println(coords[0] + " " + coords[1]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[0]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[1]));
      }
    }
    else
      Expect.expect(false);

    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_LINETO)
    {
      if (print)
        System.out.println(coords[0] + " " + coords[1]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[2]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[3]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_LINETO)
    {
      if (print)
        System.out.println(coords[0] + " " + coords[1]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[4]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[5]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_LINETO)
    {
      if (print)
        System.out.println(coords[0] + " " + coords[1]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[6]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[7]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_LINETO)
    {
      if (print)
        System.out.println(coords[0] + " " + coords[1]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[8]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[9]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_CLOSE) // we're done
    {
      pathIterator.next();
    }
    else
      Expect.expect(false);
  }

  /**
   * Compares a shape with the double data that it should contain.
   * A circular shape, represented as a general path takes the form of a MOVE_TO point, followed
   * by 4 SEG_CUBICTO points, for a total of 2 + (4*6) = 26 points.
   * @author Andy Mechtenberg
   */
  private void compareCircularShape(java.awt.Shape shape, double[] expectedCoords, boolean printOnly)
  {
    double[] coords = new double[6];
    java.awt.geom.AffineTransform identityTransform = new java.awt.geom.AffineTransform();
    java.awt.geom.PathIterator pathIterator = shape.getPathIterator(identityTransform);

    int type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_MOVETO)
    {
      if (printOnly)
        System.out.println(coords[0] + " " + coords[1]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[0]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[1]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_CUBICTO)  // CUBICTO holds 6 doubles
    {
      if (printOnly)
        System.out.println(coords[0] + " " + coords[1] + " " + coords[2] + " " + coords[3] + " " + coords[4] + " " + coords[5]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[2]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[3]));
        Expect.expect(MathUtil.fuzzyEquals(coords[2], expectedCoords[4]));
        Expect.expect(MathUtil.fuzzyEquals(coords[3], expectedCoords[5]));
        Expect.expect(MathUtil.fuzzyEquals(coords[4], expectedCoords[6]));
        Expect.expect(MathUtil.fuzzyEquals(coords[5], expectedCoords[7]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_CUBICTO)
    {
      if (printOnly)
        System.out.println(coords[0] + " " + coords[1] + " " + coords[2] + " " + coords[3] + " " + coords[4] + " " + coords[5]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[8]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[9]));
        Expect.expect(MathUtil.fuzzyEquals(coords[2], expectedCoords[10]));
        Expect.expect(MathUtil.fuzzyEquals(coords[3], expectedCoords[11]));
        Expect.expect(MathUtil.fuzzyEquals(coords[4], expectedCoords[12]));
        Expect.expect(MathUtil.fuzzyEquals(coords[5], expectedCoords[13]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_CUBICTO)
    {
      if (printOnly)
        System.out.println(coords[0] + " " + coords[1] + " " + coords[2] + " " + coords[3] + " " + coords[4] + " " + coords[5]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[14]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[15]));
        Expect.expect(MathUtil.fuzzyEquals(coords[2], expectedCoords[16]));
        Expect.expect(MathUtil.fuzzyEquals(coords[3], expectedCoords[17]));
        Expect.expect(MathUtil.fuzzyEquals(coords[4], expectedCoords[18]));
        Expect.expect(MathUtil.fuzzyEquals(coords[5], expectedCoords[19]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_CUBICTO)
    {
      if (printOnly)
        System.out.println(coords[0] + " " + coords[1] + " " + coords[2] + " " + coords[3] + " " + coords[4] + " " + coords[5]);
      else
      {
        Expect.expect(MathUtil.fuzzyEquals(coords[0], expectedCoords[20]));
        Expect.expect(MathUtil.fuzzyEquals(coords[1], expectedCoords[21]));
        Expect.expect(MathUtil.fuzzyEquals(coords[2], expectedCoords[22]));
        Expect.expect(MathUtil.fuzzyEquals(coords[3], expectedCoords[23]));
        Expect.expect(MathUtil.fuzzyEquals(coords[4], expectedCoords[24]));
        Expect.expect(MathUtil.fuzzyEquals(coords[5], expectedCoords[25]));
      }
    }
    else
      Expect.expect(false);
    pathIterator.next();
    type = pathIterator.currentSegment(coords);
    if (type == java.awt.geom.PathIterator.SEG_CLOSE) // we're done
    {
      pathIterator.next();
    }
    else
      Expect.expect(false);
  }


  /**
   * check that all getShape() methods work for the panel families_all_rlv
   * This method will load that panel, and check the panel, top board, bottom board
   * component R239 pin 1 on top side and XU6 pin 20 on the bottom side
   * @author Andy Mechtenberg
   */
  private void checkShapesForFamiliesAllRlv()
  {
    // switch to panel families_all_rlv for shape testing.  This is a better panel than nepcon_wide_ew
    // because it had different panel, top board and bottom board rotations
    _panel = null;
    String panelName = "families_all_rlv";
    File file = new File(FileName.getProjectSerializedFullPath(panelName));
    if(file.exists())
      file.delete();
    file = new File(FileName.getProjectVersionFullName(panelName));
    if(file.exists())
      file.delete();

    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    Project project = null;
    try
    {
      project = Project.importProjectFromNdfs(panelName, warnings);
      _panel = project.getPanel();
    }
    catch(XrayTesterException e)
    {
      e.printStackTrace();
    }
    Expect.expect(_panel != null);

    // first, compare panel shape -- a rectangle
    java.awt.Shape panelShape = _panel.getShapeInNanoMeters();
    double[] expectedCoords = new double[26];
    int i = 0;
    expectedCoords[i++] = 1.4922499999999997E8;
    expectedCoords[i++] = 0.0;
    expectedCoords[i++] = 1.4922499999999997E8;
    expectedCoords[i++] = 2.8956E8;
    expectedCoords[i++] = 8.0;
    expectedCoords[i++] = 2.8956E8;
    expectedCoords[i++] = 8.0;
    expectedCoords[i++] = 0.0;
    expectedCoords[i++] = 1.4922499999999997E8;
    expectedCoords[i++] = 0.0;

    compareRectangleShape(panelShape, expectedCoords, false);

    // next, do top board side:
    List<Board> boards = _panel.getBoards();
    Expect.expect(boards.size() == 1);
    Board board = (Board)boards.get(0);
    java.awt.Shape topBoardShape = board.getShapeRelativeToPanelInNanoMeters();
    i = 0;
    expectedCoords[i++] = 0.0;
    expectedCoords[i++] = 0.0;
    expectedCoords[i++] = 1.49224992E8;
    expectedCoords[i++] = 0.0;
    expectedCoords[i++] = 1.49224992E8;
    expectedCoords[i++] = 2.8956E8;
    expectedCoords[i++] = 0.0;
    expectedCoords[i++] = 2.8956E8;
    expectedCoords[i++] = 0.0;
    expectedCoords[i++] = 0.0;


    expectedCoords[i++] = 1.49224992E8;
    compareRectangleShape(topBoardShape, expectedCoords, false);

    // now, let's do a component on the top, then one on the bottom
    Component topSideComponent = board.getSideBoard1().getComponent("R239");  // rotated 270
    Expect.expect(topSideComponent != null);
    java.awt.Shape topComponentShape = topSideComponent.getShapeRelativeToPanelInNanoMeters();
    i = 0;
    expectedCoords[i++] = 9.02335047E7;
    expectedCoords[i++] = 5.93598E7;
    expectedCoords[i++] = 9.02335047E7;
    expectedCoords[i++] = 5.69722E7;
    expectedCoords[i++] = 9.1630496E7;
    expectedCoords[i++] = 5.69722E7;
    expectedCoords[i++] = 9.1630496E7;
    expectedCoords[i++] = 5.93598E7;
    expectedCoords[i++] = 9.02335047E7;
    expectedCoords[i++] = 5.93598E7;

    compareRectangleShape(topComponentShape, expectedCoords, false);

    // do the pin and pad for pin 1 on this device
    Pad padOne = topSideComponent.getPad("1");
    java.awt.Shape topPadOneShape = padOne.getShapeRelativeToPanelInNanoMeters();
    i = 0;
    expectedCoords[i++] = 9.0233504E7;
    expectedCoords[i++] = 5.77342E7;
    expectedCoords[i++] = 9.0233504E7;
    expectedCoords[i++] = 5.69722E7;
    expectedCoords[i++] = 9.1630496E7;
    expectedCoords[i++] = 5.69722E7;
    expectedCoords[i++] = 9.1630496E7;
    expectedCoords[i++] = 5.77342E7;
    expectedCoords[i++] = 9.0233504E7;
    expectedCoords[i++] = 5.77342E7;

    compareRectangleShape(topPadOneShape, expectedCoords, false);

    // rotated 90 about a non-corner point - shifty!
    Component bottomSideComponent = board.getSideBoard2().getComponent("XU6");
    Expect.expect(bottomSideComponent != null);
    java.awt.Shape bottomComponentShape = bottomSideComponent.getShapeRelativeToPanelInNanoMeters();
    i = 0;
    expectedCoords[i++] = 9.11606E7;
    expectedCoords[i++] = 1.75539392E8;
    expectedCoords[i++] = 1.37439392E8;
    expectedCoords[i++] = 1.75539392E8;
    expectedCoords[i++] = 1.37439392E8;
    expectedCoords[i++] = 1.29260592E8;
    expectedCoords[i++] = 9.11606E7;
    expectedCoords[i++] = 1.29260592E8;
    expectedCoords[i++] = 9.11606E7;
    expectedCoords[i++] = 1.75539392E8;

    compareRectangleShape(bottomComponentShape, expectedCoords, false);

    // do pin and pad 20 of XU6 -- these are circular pins and pads, they have 26 expected points
    Pad pad20 = bottomSideComponent.getPad("20");
    java.awt.Shape pad20Shape = pad20.getShapeRelativeToPanelInNanoMeters();

    i = 0;
    expectedCoords[i++] = 9.67994E7;
    expectedCoords[i++] = 1.7018E8;
    expectedCoords[i++] = 9.67994E7;
    expectedCoords[i++] = 1.70025696E8 ;
    expectedCoords[i++] = 9.6674312E7;
    expectedCoords[i++] = 1.69900608E8;
    expectedCoords[i++] = 9.652E7;
    expectedCoords[i++] = 1.69900608E8;
    expectedCoords[i++] = 9.6365688E7;
    expectedCoords[i++] = 1.69900608E8;
    expectedCoords[i++] = 9.62406E7;
    expectedCoords[i++] = 1.70025696E8;
    expectedCoords[i++] = 9.62406E7;
    expectedCoords[i++] = 1.7018E8;
    expectedCoords[i++] = 9.62406E7;
    expectedCoords[i++] = 1.70334304E8;
    expectedCoords[i++] = 9.6365688E7;
    expectedCoords[i++] = 1.70459392E8;
    expectedCoords[i++] = 9.652E7;
    expectedCoords[i++] = 1.70459392E8;
    expectedCoords[i++] = 9.6674312E7;
    expectedCoords[i++] = 1.70459392E8 ;
    expectedCoords[i++] = 9.67994E7;
    expectedCoords[i++] = 1.70334304E8 ;
    expectedCoords[i++] = 9.67994E7;
    expectedCoords[i++] = 1.7018E8;

    compareCircularShape(pad20Shape, expectedCoords, false);
  }

  /**
   * @author Bill Darbie
   */
  private void checkBoardOutline()
  {
    String panelName = "nepcon_wide_ew";
    File file = new File(FileName.getProjectSerializedFullPath(panelName));
    if(file.exists())
      file.delete();
    file = new File(FileName.getProjectVersionFullName(panelName));
    if(file.exists())
      file.delete();

    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    Project project = null;
    try
    {
      project = Project.importProjectFromNdfs(panelName, warnings);
      _panel = project.getPanel();
    }
    catch(XrayTesterException e)
    {
      e.printStackTrace();
    }

    List<Board> boards = _panel.getBoardsOutsidePanel();
    Expect.expect(boards.isEmpty());
  }
}
