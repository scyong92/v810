package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class BoardTypeEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static BoardTypeEventEnum CREATE_OR_DESTROY = new BoardTypeEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static BoardTypeEventEnum PANEL = new BoardTypeEventEnum(++_index);
  public static BoardTypeEventEnum NAME = new BoardTypeEventEnum(++_index);
  public static BoardTypeEventEnum WIDTH = new BoardTypeEventEnum(++_index);
  public static BoardTypeEventEnum LENGTH = new BoardTypeEventEnum(++_index);
  public static BoardTypeEventEnum ADD_OR_REMOVE_BOARD = new BoardTypeEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static BoardTypeEventEnum SIDE_BOARD_TYPE_1 = new BoardTypeEventEnum(++_index);
  public static BoardTypeEventEnum SIDE_BOARD_TYPE_2 = new BoardTypeEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private BoardTypeEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private BoardTypeEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
