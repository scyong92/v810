package com.axi.v810.business.panelDesc;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * There is one PadType instance for each physical Pad on the same BoardType.
 * @author Bill Darbie
 */
public class PadType implements Serializable
{
  // created by BoardNdfReader, CompPackageReader

  private LandPatternPad _landPatternPad; // BoardNdfReader, CompPackageReader
  private PackagePin _packagePin; // ComponentNdfReader, CompPackageReader
  private PadTypeSettings _padTypeSettings; // BoardNdfReader, BoardTypeSettingsReader
  private ComponentType _componentType; // BoardNdfReader, CompPackageReader
  private Set<Pad> _padSet = new HashSet<Pad>(); // BoardNdfReader, PanelReader
  private transient Set<ComponentType> _componentTypesThatCauseInterferencePatterns;
  private Set<Pair<String, String>> _boardTypeNameToComponentNameSet;

  private static transient ProjectObservable _projectObservable;

  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public PadType()
  {
    _projectObservable.stateChanged(this, null, this, PadTypeEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      getComponentType().removePadType(this);

      _padTypeSettings.destroy();
      _padTypeSettings = null;

      for (Pad pad : getPads())
        pad.destroy();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, PadTypeEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public String getName()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.getName();
  }

  /**
   * @return the name of the pad including the component name, for example: "U1-1"
   * @author Bill Darbie
   */
  public String getComponentAndPadName()
  {
    Assert.expect(_componentType != null);

    String refDes = _componentType.getReferenceDesignator();
    String fullName = refDes + "-" + getName();

    return fullName;
  }

  /**
   * @author Bill Darbie
   */
  public String getBoardAndComponentAndPadName()
  {
    String boardTypeName = _componentType.getSideBoardType().getBoardType().getName();
    return boardTypeName + " " + getComponentAndPadName();
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author Bill Darbie
   */
  public void setComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);

    if (componentType == _componentType)
      return;

    ComponentType oldValue = _componentType;
    _projectObservable.setEnabled(false);
    try
    {
      _componentType = componentType;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, componentType, PadTypeEventEnum.COMPONENT_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public ComponentType getComponentType()
  {
    Assert.expect(_componentType != null);

    return _componentType;
  }

  /**
   * @author Bill Darbie
   */
  public void addPad(Pad pad)
  {
    Assert.expect(pad != null);
    Assert.expect(_padSet != null);

    _projectObservable.setEnabled(false);
    try
    {
      boolean added = _padSet.add(pad);
      Assert.expect(added);
      pad.setPadType(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, pad, PadTypeEventEnum.ADD_OR_REMOVE_PAD);
  }

  /**
   * @author Bill Darbie
   */
  void removePad(Pad pad)
  {
    Assert.expect(pad != null);

    _projectObservable.setEnabled(false);
    try
    {
      boolean removed = _padSet.remove(pad);
      Assert.expect(removed);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, pad, null, PadTypeEventEnum.ADD_OR_REMOVE_PAD);
  }

  /**
   * @author Bill Darbie
   */
  public List<Pad> getPads()
  {
    Assert.expect(_padSet != null);

    return new ArrayList<Pad>(_padSet);
  }

  /**
   * @author Keith Lee
   */
  public void setLandPatternPad(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    if (landPatternPad == _landPatternPad)
      return;

    LandPatternPad oldValue = _landPatternPad;
    _projectObservable.setEnabled(false);
    try
    {
      invalidateShape();
      _landPatternPad = landPatternPad;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, landPatternPad, PadTypeEventEnum.LAND_PATTERN_PAD);
  }

  /**
   * @author Keith Lee
   */
  public LandPatternPad getLandPatternPad()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad;
  }

  /**
   * @author Bill Darbie
   */
  public ThroughHoleLandPatternPad getThroughHoleLandPatternPad()
  {
    Assert.expect(isThroughHolePad());

    return (ThroughHoleLandPatternPad)_landPatternPad;
  }

  /**
   * @author Bill Darbie
   */
  public SurfaceMountLandPatternPad getSurfaceMountLandPatternPad()
  {
    Assert.expect(isSurfaceMountPad());

    return (SurfaceMountLandPatternPad)_landPatternPad;
  }

  /**
   * @author Bill Darbie
   */
  public int getPitchInNanoMeters()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.getPitchInNanoMeters();
  }

  /**
   * @author Bill Darbie
   */
  public void setPadTypeSettings(PadTypeSettings padTypeSettings)
  {
    Assert.expect(padTypeSettings != null);

    if (padTypeSettings == _padTypeSettings)
      return;

    PadTypeSettings oldValue = _padTypeSettings;
    _projectObservable.setEnabled(false);
    try
    {
      _padTypeSettings = padTypeSettings;
      _padTypeSettings.setPadType(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, padTypeSettings, PadTypeEventEnum.PAD_TYPE_SETTINGS);
  }

  /**
   * @author Bill Darbie
   */
  public PadTypeSettings getPadTypeSettings()
  {
    Assert.expect(_padTypeSettings != null);

    return _padTypeSettings;
  }

  /**
   * Get degrees rotation of this Pad.  The rotation of a Pad describes
   * the rotation around the Pad's Center
   * @author Bill Darbie
   */
  public double getDegreesRotation()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.getDegreesRotation();
  }

  /**
   * @author Bill Darbie
   */
  public void setPackagePin(PackagePin packagePin)
  {
    Assert.expect(packagePin != null);

    if (packagePin == _packagePin)
      return;

    PackagePin oldValue = _packagePin;
    _projectObservable.setEnabled(false);
    try
    {
      _packagePin = packagePin;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, packagePin, PadTypeEventEnum.PACKAGE_PIN);
  }

  /**
   * @author Bill Darbie
   */
  public PackagePin getPackagePin()
  {
    Assert.expect(_packagePin != null);

    return _packagePin;
  }

  /**
   * @author George A. David
   */
  public JointTypeEnum getJointTypeEnum()
  {
    Assert.expect(_packagePin != null);

    return _packagePin.getJointTypeEnum();
  }

  /**
   * @author Keith Lee
   */
  public ShapeEnum getShapeEnum()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.getShapeEnum();
  }

  /**
   * @return the coordinate of this Pad relative to the reference
   * point of the Component it gets associated with.
   * The reference point is not necessarily the center of the component, but is the
   * point from which the pad/pin coordinates are referenced.
   * @author Keith Lee
   */
  public ComponentCoordinate getCoordinateInNanoMeters()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.getCoordinateInNanoMeters();
  }

  /**
   * @author George A. David
   */
  public ComponentCoordinate getCenterCoordinateInNanoMeters()
  {
    java.awt.Shape shape = getShapeInNanoMeters();
    Rectangle2D bounds = shape.getBounds2D();

    return new ComponentCoordinate((int)Math.round(bounds.getCenterX()), (int)Math.round(bounds.getCenterY()));
  }

  /**
   * @author Keith Lee
   */
  public int getWidthInNanoMeters()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.getWidthInNanoMeters();
  }

  /**
   * @author Keith Lee
   */
  public int getLengthInNanoMeters()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.getLengthInNanoMeters();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isThroughHolePad()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.isThroughHolePad();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isSurfaceMountPad()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.isSurfaceMountPad();
  }

  /**
   * @return true if this pin is pin one and false otherwise
   * @author Bill Darbie
   */
  public boolean isPadOne()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.isPadOne();
  }

  /**
   * A pad is considered inspected if it is loaded, is testable, and is set to be inspected.
   * @author Bill Darbie
   */
  public boolean isInspected()
  {
    boolean isInspected = false;

    Assert.expect(_padTypeSettings != null);
    if (_padTypeSettings.isInspected())
      isInspected = true;

    return isInspected;
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasSubtype()
  {
    Assert.expect(_padTypeSettings != null);

    return _padTypeSettings.hasSubtype();
  }

  /**
   * @author Bill Darbie
   */
  public Subtype getSubtype()
  {
    Assert.expect(_padTypeSettings != null);

    return _padTypeSettings.getSubtype();
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean doesSubtypeHasIncompatibleInspectionFamily()
  {
    Assert.expect(_padTypeSettings != null);

    return _padTypeSettings.doesSubtypeHasIncompatibleInspectionFamily();  
  }

  /**
   * @author Bill Darbie
   */
  public PinOrientationEnum getPinOrientationEnum()
  {
    Assert.expect(_packagePin != null);

    return _packagePin.getPinOrientationEnum();
  }

  /**
   * InterPadDistance is the distance from one edge of a Pad to the edge of its nearest Pad.
   *
   * @author Bill Darbie
   */
  public int getInterPadDistanceInNanoMeters()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.getInterPadDistanceInNanoMeters();
  }

  /**
   * @author George A. David
   */
  public int getAlgorithmLimitedInterPadDistanceInNanometers()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.getAlgorithmLimitedInterPadDistanceInNanoMeters();
  }

  /**
   * @author Bill Darbie
   */
  void invalidateShape()
  {
    if (_landPatternPad != null)
      _landPatternPad.invalidateShape();
  }

  /**
   * @return the Shape not related to any component, board or panel setting.
   * @author Andy Mechtenberg
   */
  public java.awt.Shape getShapeInNanoMeters()
  {
    Assert.expect(_landPatternPad != null);

    return _landPatternPad.getShapeInNanoMeters();
  }

  /**
   * @author George A. David
   */
  public void clearComponentTypesThatCauseInterferencePatterns()
  {
    if (_componentTypesThatCauseInterferencePatterns != null)
    {
      // RMS: Clean the content of our map to make GC more effcient.
      _componentTypesThatCauseInterferencePatterns.clear();
      _componentTypesThatCauseInterferencePatterns = null;
      _boardTypeNameToComponentNameSet = null;
    }
  }

  /**
   * @author George A. David
   */
  public void addComponentTypeThatCausesInteferencePattern(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    initComponentTypeThatCausesInteferencePatternIfNecessary();

    _boardTypeNameToComponentNameSet.add(new Pair<String, String>(componentType.getSideBoardType().getBoardType().getName(), componentType.getReferenceDesignator()));

    _componentTypesThatCauseInterferencePatterns.add(componentType);

  }

  /**
  * @author Yee Seong
  */
  public void deleteComponentTypeThatCausesInterferencePatterns(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    initComponentTypeThatCausesInteferencePatternIfNecessary();

    for (Pair<String, String> boardTypeNameToComponentName : _boardTypeNameToComponentNameSet)
    {
      if(boardTypeNameToComponentName.getSecond().equals(componentType.getReferenceDesignator()) &&
         boardTypeNameToComponentName.getFirst().equals(componentType.getSideBoardType().getBoardType().getName()));
      {
        _boardTypeNameToComponentNameSet.remove(boardTypeNameToComponentName);
        break;
      }
    }

    _componentTypesThatCauseInterferencePatterns.remove(componentType);

  }


  /**
   * @author George A. David
   */
  public boolean hasComponentTypesThatCauseInterferencePatterns()
  {
    boolean hasComponent = false;
    if(_componentTypesThatCauseInterferencePatterns != null && _componentTypesThatCauseInterferencePatterns.isEmpty() == false)
      hasComponent = true;

    if(_boardTypeNameToComponentNameSet != null && _boardTypeNameToComponentNameSet.isEmpty() == false)
      hasComponent = true;

    return hasComponent;
  }

  /**
   * @author George A. David
   */
  public List<ComponentType> getComponentTypesThatCauseInterferencePatterns()
  {
    initComponentTypeThatCausesInteferencePatternIfNecessary();

    return new ArrayList<ComponentType>(_componentTypesThatCauseInterferencePatterns);
  }

  /**
   * @author Poh Kheng
   */
  private void initComponentTypeThatCausesInteferencePatternIfNecessary()
  {
    Panel panel = _componentType.getSideBoardType().getBoardType().getPanel();
    if(_componentTypesThatCauseInterferencePatterns == null && _boardTypeNameToComponentNameSet != null)
    {
      _componentTypesThatCauseInterferencePatterns = new HashSet<ComponentType>();
      for (Pair<String, String> boardTypeNameToComponentName : _boardTypeNameToComponentNameSet)
       {

         ComponentType componentType = panel.getComponentType(boardTypeNameToComponentName.getFirst(), boardTypeNameToComponentName.getSecond());
         _componentTypesThatCauseInterferencePatterns.add(componentType);
       }
    }
    if(_componentTypesThatCauseInterferencePatterns == null)
      _componentTypesThatCauseInterferencePatterns = new HashSet<ComponentType>();

    if(_boardTypeNameToComponentNameSet == null)
      _boardTypeNameToComponentNameSet = new HashSet<Pair<String, String>>();

  }

  /**
   * @author Poh Kheng
   */
  public double getAreaInNanoMeters()
  {
    Assert.expect(_landPatternPad != null);
    double shapeArea = 0;

    if(_landPatternPad.getShapeEnum().equals(ShapeEnum.RECTANGLE))
    {
      shapeArea = MathUtil.sumSquareRootRectangleArea(_landPatternPad.getWidthInNanoMeters(), _landPatternPad.getLengthInNanoMeters());
    }
    else if(_landPatternPad.getShapeEnum().equals(ShapeEnum.CIRCLE))
    {
      shapeArea = MathUtil.sumSquareRootAbroundArea(_landPatternPad.getWidthInNanoMeters(), _landPatternPad.getLengthInNanoMeters());
    }

    return shapeArea;
  }

}
