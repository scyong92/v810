package com.axi.v810.business.panelDesc;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * Both footprint and land pattern are represented by this class.  There
 * is one instance of this class for each unique Land Pattern on the
 * Panel.  This means more than one Component can share the same LandPattern.
 * Components on different Boards can share the same LandPattern.
 *
 * @author Bill Darbie
 */
public class LandPattern implements Serializable
{
  // created by SurfaceMountLandPatternNdfReader/ThroughHoleLandPatternNdfReader, LandPatternReader
  private Panel _panel; // SurfaceMountLandPatternNdfReader/ThroughHoleLandPatternNdfReader, LandPatternReader
  private String _name; // SurfaceMountLandPatternNdfReader/ThroughHoleLandPatternNdfReader, LandPatternReader
  private Map<String, LandPatternPad> _landPatternPadNameToLandPatternPadMap = new TreeMap<String, LandPatternPad>(new AlphaNumericComparator()); // SurfaceMountLandPatternNdfReader/ThroughHoleLandPatternNdfReader, LandPatternReader
  
  // do not sort by name for _componentTypeSet since the same name can occur more than once!
  private Set<ComponentType> _componentTypeSet = new HashSet<ComponentType>(); // BoardNdfReader, BoardTypeReader
  private int _widthInNanoMeters = - 1; // calculated from LandPatternPads
  private int _lengthInNanoMeters = -1; // calculated from LandPatternPads
  private BoardCoordinate _lowerLeftCoordinateInNanoMeters; // calculated from LandPatternPads
  private boolean _sizeValid = false;
  private boolean _pitchAndInterPadDistanceIsValid = false;

  private static transient ProjectObservable _projectObservable;

  private transient boolean _swapInProgress = false;
  private transient boolean _createLandPatternPadInProgress = false;

  private boolean _numRowsAndColsAndPadOneCenterValid = false;
  private int _numRows;
  private int _numCols;
  private LandPatternPad _padOne;
  private LandPatternPad _userAssignedPadOne;
  private boolean _isPadOneInCenterOfRowOrColumn = false;
  
  private static transient PadNameComparator _padNameComparator;
  private static transient AlphaNumericComparator _alphaNumericComparator;
  
  private static final int _defaultHoleDiameter = MathUtil.convertMilsToNanoMetersInteger(10);

  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
    _padNameComparator = new PadNameComparator();
    _alphaNumericComparator = new AlphaNumericComparator();
  }

  /**
   * @author Bill Darbie
   */
  public LandPattern()
  {
    _projectObservable.stateChanged(this, null, this, LandPatternEventEnum.CREATE_OR_DESTROY);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public LandPattern(String name)
  {
    Assert.expect(name != null);
    
    _name = name;
    _projectObservable.stateChanged(this, null, this, LandPatternEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      Assert.expect(_panel != null);
      _panel.removeLandPattern(this);
      for (LandPatternPad landPatternPad : getLandPatternPads())
        landPatternPad.destroy();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, LandPatternEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void remove()
  {
    _projectObservable.setEnabled(false);
    try
    {
      Assert.expect(_panel != null);
      _panel.removeLandPattern(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, LandPatternEventEnum.ADD_OR_REMOVE);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void add()
  {
    _projectObservable.setEnabled(false);
    try
    {
      Assert.expect(_panel != null);
      _panel.addLandPattern(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, this, LandPatternEventEnum.ADD_OR_REMOVE);
  }

  /**
   * @author Bill Darbie
   */
  public void setPanel(Panel panel)
  {
    Assert.expect(panel != null);

    Panel oldValue = _panel;
    _projectObservable.setEnabled(false);
    try
    {
      _panel = panel;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, panel, LandPatternEventEnum.PANEL);

  }

  /**
   * @author Bill Darbie
   */
  public void checkLandPatternPadName(String name) throws BusinessException
  {
    if (isLandPatternPadNameValid(name) == false)
      throw new InvalidNameBusinessException("land pattern pad", name, getLandPatternPadNameIllegalChars());

    if (isLandPatternPadNameDuplicate(name))
      throw new DuplicateNameBusinessException("land pattern pad", name);
  }

  /**
   * @author Bill Darbie
   */
  public String getLandPatternPadNameIllegalChars()
  {
    return " ";
  }

  /**
   * @author Bill Darbie
   */
  public boolean isLandPatternPadNameValid(String name)
  {
    Assert.expect(name != null);
    if (name.contains(" "))
      return false;
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isLandPatternPadNameDuplicate(String name)
  {
    Assert.expect(name != null);
    Assert.expect(_panel != null);

    if ((_panel.areChecksDisabledForNdfparsers() == false) && (_landPatternPadNameToLandPatternPadMap.containsKey(name)))
      return true;
    return false;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isLandPatternPadNameDuplicateDuringPinsNdfParsing(String name)
  {
    Assert.expect(name != null);
    Assert.expect(_panel != null);

    if (_landPatternPadNameToLandPatternPadMap.containsKey(name))
      return true;
    return false;
  }


  /**
   * @author Bill Darbie
   */
  void setNewLandPatternPadName(LandPatternPad landPatternPad, String oldName, String newName)
  {
    Assert.expect(landPatternPad != null);
    Assert.expect(newName != null);

    if ((_swapInProgress == false) && (_createLandPatternPadInProgress == false))
    {
      for (ComponentType componentType : getComponentTypesUnsorted())
        componentType.setNewPadTypeName(landPatternPad, oldName, newName);
    }

    Assert.expect(_panel != null);
    boolean disableCheck = _panel.areChecksDisabledForNdfparsers();

    LandPatternPad prev = null;
    if (oldName != null)
    {
      prev = _landPatternPadNameToLandPatternPadMap.remove(oldName);
      if (disableCheck == false)
        Assert.expect(prev != null);
    }
    prev = _landPatternPadNameToLandPatternPadMap.put(newName, landPatternPad);
    if (disableCheck == false)
      Assert.expect(prev == null);

    _padOne = null;
    //XCR-3455, Wrong image display if pad 1 is different joint type
    assignPadOneIfNecessary();
    if (oldName == null)
      _projectObservable.stateChanged(this, null, landPatternPad, LandPatternEventEnum.ADD_OR_REMOVE_LAND_PATTERN_PAD);

  }

  /**
   * @author Bill Darbie
   */
  public void setName(String name)
  {
    Assert.expect(name != null);
    Assert.expect(_panel != null);
    Assert.expect(_panel.isLandPatternNameValid(name));
    //Assert.expect(_panel.isLandPatternNameDuplicate(name) == false);

    if (name.equals(_name))
      return;

    String oldValue = _name;
    _projectObservable.setEnabled(false);
    try
    {
      _panel.setNewLandPatternName(this, _name, name);
      _name = name.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, name, LandPatternEventEnum.NAME);
  }

  /**
   * @author Bill Darbie
   */
  public String getName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author Laura Cormos
   */
  public Panel getPanel()
  {
    Assert.expect(_panel != null);

    return _panel;
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author Bill Darbie
   * @author Ying-Huan.Chu
   */
  public void addLandPatternPad(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);
    _projectObservable.setEnabled(false);
     try
     {
       _sizeValid = false;
       _pitchAndInterPadDistanceIsValid = false;
       _padOne = null;

       LandPatternPad prev = _landPatternPadNameToLandPatternPadMap.put(landPatternPad.getName(), landPatternPad);
       // to handle undo case - if previously deleted pad is a padOne, 
       // invalidate current padOne and restore the previously deleted pad as padOne.
       if (landPatternPad.isPadOne())
       {
         getLandPatternPadOne().setPadOne(false);
         _padOne = landPatternPad;
       }
//       Assert.expect(prev == null);
     }
     finally
     {
       _projectObservable.setEnabled(true);
     }
     _projectObservable.stateChanged(this, null, landPatternPad, LandPatternEventEnum.ADD_OR_REMOVE_LAND_PATTERN_PAD);
  }

  /**
   * @author Bill Darbie
   */
  public void removeLandPatternPad(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    _projectObservable.setEnabled(false);
    try
    {
      // if a new LandPatternPad is removed, then the calculated width, length, and coordinates are wrong
      _sizeValid = false;
      _pitchAndInterPadDistanceIsValid = false;
      _padOne = null;
      LandPatternPad prev = _landPatternPadNameToLandPatternPadMap.remove(landPatternPad.getName());
      Assert.expect(prev != null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, landPatternPad, null, LandPatternEventEnum.ADD_OR_REMOVE_LAND_PATTERN_PAD);
  }

  /**
   * @author Bill Darbie
   */
  public LandPatternPad createSurfaceMountLandPatternPad(ComponentCoordinate componentCoordinate,
                                                         double rotation,
                                                         int lengthInNanoMeters,
                                                         int widthInNanoMeters,
                                                         ShapeEnum shapeEnum)
  {
    LandPatternPad landPatternPad = null;
    try
    {
      _createLandPatternPadInProgress = true;
      landPatternPad = createLandPatternPad(true,
                                            componentCoordinate,
                                            rotation,
                                            lengthInNanoMeters,
                                            widthInNanoMeters,
                                            shapeEnum);
    }
    finally
    {
      _createLandPatternPadInProgress = false;
    }

    Assert.expect(landPatternPad != null);
    return landPatternPad;
  }

  /**
   * @author Bill Darbie
   */
  public LandPatternPad createThroughHoleLandPatternPad(ComponentCoordinate componentCoordinate,
                                                        double rotation,
                                                        int lengthInNanoMeters,
                                                        int widthInNanoMeters,
                                                        ShapeEnum shapeEnum)
  {
    LandPatternPad landPatternPad = null;
    try
    {
      _createLandPatternPadInProgress = true;
      landPatternPad = createLandPatternPad(false,
                                            componentCoordinate,
                                            rotation,
                                            lengthInNanoMeters,
                                            widthInNanoMeters,
                                            shapeEnum);
    }
    finally
    {
      _createLandPatternPadInProgress = false;
    }
    Assert.expect(landPatternPad != null);
    return landPatternPad;
  }

  /**
   * @author Bill Darbie
   */
  public LandPattern createDuplicate(String landPatternName)
  {
    Assert.expect(landPatternName != null);

    LandPattern landPattern = null;
    _projectObservable.setEnabled(false);
    try
    {
      landPattern = new LandPattern();
      landPattern.setPanel(_panel);
      for (LandPatternPad landPatternPadToDuplicate : getLandPatternPads())
      {
        landPatternPadToDuplicate.createDuplicate(landPattern);
      }
      landPattern.setName(landPatternName);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(landPattern, null, landPattern, LandPatternEventEnum.CREATE_OR_DESTROY);

    Assert.expect(landPattern != null);
    return landPattern;
  }

  /**
   * @author Bill Darbie
   */
  public List<CompPackage> getCompPackages()
  {
    Set<CompPackage> compPackageSet = new TreeSet<CompPackage>(_alphaNumericComparator);
    for (ComponentType componentType : getComponentTypesUnsorted())
      compPackageSet.add(componentType.getCompPackage());

    return new ArrayList<CompPackage>(compPackageSet);
  }

  /**
   * @author Bill Darbie
   */
  private LandPatternPad createLandPatternPad(boolean surfaceMount,
                                              ComponentCoordinate componentCoordinate,
                                              double rotation,
                                              int lengthInNanoMeters,
                                              int widthInNanoMeters,
                                              ShapeEnum shapeEnum)
  {
    LandPatternPad landPatternPad = null;

    _projectObservable.setEnabled(false);
    try
    {
      _sizeValid = false;
      _pitchAndInterPadDistanceIsValid = false;
      _padOne = null;

      if (surfaceMount)
        landPatternPad = new SurfaceMountLandPatternPad();
      else
      {
        ThroughHoleLandPatternPad thLandPatternPad = new ThroughHoleLandPatternPad();
        //Ngie xing, XCR-2112, Add a component with New Through Hole Pad Array (PTH) could not reflect Testable at modify subtypes
        thLandPatternPad.setHoleCoordinateInNanoMeters(componentCoordinate);
        
        //XCR-2585 - 24/3/2015
        //make sure pad will never bigger than hole
        //Siew Yeng - XCR-3318 - Oval PTH
        if (lengthInNanoMeters < _defaultHoleDiameter)
        {
          thLandPatternPad.setHoleLengthInNanoMeters(lengthInNanoMeters);
        }
        else
        {
//          thLandPatternPad.setHoleDiameterInNanoMeters(_defaultHoleDiameter);
          thLandPatternPad.setHoleLengthInNanoMeters(_defaultHoleDiameter);
          
        }
        
        if (widthInNanoMeters < _defaultHoleDiameter)
        {
          thLandPatternPad.setHoleWidthInNanoMeters(widthInNanoMeters);
        }
        else
        {
//          thLandPatternPad.setHoleDiameterInNanoMeters(_defaultHoleDiameter);
          thLandPatternPad.setHoleWidthInNanoMeters(_defaultHoleDiameter);          
        }
        landPatternPad = thLandPatternPad;
      }

      landPatternPad.setLandPattern(this);
      landPatternPad.setCoordinateInNanoMeters(componentCoordinate);
      landPatternPad.setDegreesRotationWithoutAffectingPinOrientation(rotation);
      landPatternPad.setLandPattern(this);
      landPatternPad.setLengthInNanoMeters(lengthInNanoMeters);
      landPatternPad.setWidthInNanoMeters(widthInNanoMeters);
      landPatternPad.setShapeEnum(shapeEnum);

      // find a unique name for the new Pad
      Set<String> nameSet = new HashSet<String>();
      List<LandPatternPad> landPatternPads = getLandPatternPads();
      for (LandPatternPad existingLandPatternPad : landPatternPads)
      {
        String name = existingLandPatternPad.getName();
        boolean added = nameSet.add(name);
        Assert.expect(added);
      }
      int nameInt = landPatternPads.size() + 1;
      String newName = Integer.toString(nameInt);
      while (nameSet.contains(newName))
      {
        ++nameInt;
        newName = Integer.toString(nameInt);
      }

      landPatternPad.setName(newName);

      // create Pad and PadType, and PackagePin objects
      Set<CompPackage> origCompPackageSet = new HashSet<CompPackage>();
      Map<CompPackage, PackagePin> compPackageToPackagePinMap = new HashMap<CompPackage, PackagePin>();
      for (ComponentType componentType : getComponentTypesUnsorted())
      {
        CompPackage compPackage = componentType.getCompPackage();
        origCompPackageSet.add(compPackage);

        PackagePin packagePin = null;
        JointTypeEnum jointTypeEnum = null;
        if (compPackageToPackagePinMap.containsKey(compPackage) == false)
        {
          packagePin = new PackagePin();
          packagePin.setCompPackage(compPackage);
          packagePin.setLandPatternPad(landPatternPad);
          //Chin seong, Kee - Set the joing type enum here or will cause assert because null will be return when saving project
          // if the subtype is not set !
          if (componentType.getCompPackage().usesOneJointTypeEnum())
          {
             jointTypeEnum = componentType.getCompPackage().getJointTypeEnum(); 
          }
          else
          {
              // Handle multiple joint type in package
              List<PackagePin> packagePins = componentType.getCompPackage().getPackagePins();              
              for(PackagePin currentPackagePin : packagePins)
              {
                  Rectangle2D packagePinRectangle = currentPackagePin.getShapeInNanoMeters().getBounds2D();
                  if (packagePinRectangle.getCenterX() == landPatternPad.getCenterCoordinateInNanoMeters().getX() &&
                      packagePinRectangle.getCenterY() == landPatternPad.getCenterCoordinateInNanoMeters().getY() &&
                      packagePinRectangle.getWidth() == landPatternPad.getWidthInNanoMeters() &&
                      packagePinRectangle.getHeight() == landPatternPad.getLengthInNanoMeters())
                  {
                      jointTypeEnum = currentPackagePin.getJointTypeEnum();
                      break;
                  }
              }
              //added by Jack Hwee - make sure at least one JointTypeEnum will be set
              if (jointTypeEnum == null)
                jointTypeEnum = packagePins.get(0).getJointTypeEnum();
              Assert.expect(jointTypeEnum != null);
          }
          packagePin.setJointTypeEnum(jointTypeEnum);
          compPackage.addPackagePin(packagePin);
          compPackageToPackagePinMap.put(compPackage, packagePin);
        }
        else
        {
          packagePin = compPackageToPackagePinMap.get(compPackage);
          Assert.expect(packagePin != null);
          jointTypeEnum = packagePin.getJointTypeEnum();
        }

        Subtype subtype = _panel.getSubtypeCreatingIfNecessary(componentType.getLandPattern(), jointTypeEnum);

        PadType padType = new PadType();
        PadTypeSettings padTypeSettings = new PadTypeSettings();
        padTypeSettings.setInspected(true);
        padTypeSettings.setPadType(padType);

        padType.setComponentType(componentType);
        padType.setLandPatternPad(landPatternPad);
        padType.setPackagePin(packagePin);
        padType.setPadTypeSettings(padTypeSettings);
        // landPatternPad must have a name set for this call to work
        componentType.addPadType(padType);

        //Chin Seong, Kee - Have to wait all pad type settings been created, set then ony can set subtype
        padTypeSettings.setSubtype(subtype);

        for (Component component : componentType.getComponents())
        {
          Pad pad = new Pad();
          pad.setNodeName("");

          PadSettings padSettings = new PadSettings();
          padSettings.setTestable(true);
          pad.setPadSettings(padSettings);
          padSettings.setPad(pad);
          padType.addPad(pad);
          pad.setPadType(padType);
          pad.setComponent(component);

          // landPatternPad must have a name set for this call to work
          component.addPad(pad);
        }

        //Chin Seong, Kee - commented as it will overwrite previous set joint type etc
        //componentType.assignOrModifyCompPackageAndSubtypeAndJointTypeEnums();
      }

      // now check for any unused CompPackages and remove them
      Set<CompPackage> newCompPackageSet = new HashSet<CompPackage>(getCompPackages());
      for (ComponentType componentType : getComponentTypesUnsorted())
      {
        newCompPackageSet.add(componentType.getCompPackage());
      }

      // remove all of the new CompPackages from the original set
      origCompPackageSet.removeAll(newCompPackageSet);
      for (CompPackage unusedCompPackage : origCompPackageSet)
      {
        if (_panel.doesCompPackageExist(unusedCompPackage))
          unusedCompPackage.destroy();
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(landPatternPad, null, landPatternPad, LandPatternPadEventEnum.CREATE_OR_DESTROY);

    Assert.expect(landPatternPad != null);
    return landPatternPad;
  }

  /**
   * @return a List containing LandPatternPads for this LandPattern
   * @author Keith Lee
   */
  public List<LandPatternPad> getLandPatternPads()
  {
    Assert.expect(_landPatternPadNameToLandPatternPadMap != null);

    List<LandPatternPad> landPatternPads = new ArrayList<LandPatternPad>(_landPatternPadNameToLandPatternPadMap.values());
    
    if(landPatternPads.size() == 2)
      Collections.sort(landPatternPads, _padNameComparator);
    
    return landPatternPads;
  }

  /**
   * @author Laura Cormos
   */
  public LandPatternPad getLandPatternPad(String landPatternPadName)
  {
    Assert.expect(landPatternPadName != null);

    LandPatternPad landPatternPad = _landPatternPadNameToLandPatternPadMap.get(landPatternPadName);
    Assert.expect(landPatternPad != null);
    return landPatternPad;
  }

  /**
   * @author Bill Darbie
   */
  public void addComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    Assert.expect(_componentTypeSet != null);

    _projectObservable.setEnabled(false);
    try
    {
      boolean added = _componentTypeSet.add(componentType);
      Assert.expect(added);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, componentType, LandPatternEventEnum.ADD_OR_REMOVE_COMPONENT_TYPE);
  }

  /**
   * @return a List of all ComponentTypes that use this LandPattern.
   * @author Bill Darbie
   */
  public List<ComponentType> getComponentTypes()
  {
    Assert.expect(_componentTypeSet != null);

    List<ComponentType> componentTypes = new ArrayList<ComponentType>(_componentTypeSet);
    Collections.sort(componentTypes, _alphaNumericComparator);
    return componentTypes;
  }

  /**
   * @author Bill Darbie
   */
  public List<ComponentType> getComponentTypesUnsorted()
  {
    List<ComponentType> componentTypes = new ArrayList<ComponentType>(_componentTypeSet);
    return componentTypes;
  }

  /**
   * @author Bill Darbie
   */
  public void removeComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);

    _projectObservable.setEnabled(false);
    try
    {
      boolean removed = _componentTypeSet.remove(componentType);
      Assert.expect(removed);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, componentType, null, LandPatternEventEnum.ADD_OR_REMOVE_COMPONENT_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isInUse()
  {
    if (_componentTypeSet.isEmpty() == false)
      return true;
    return false;
  }

  /**
   * @author Bill Darbie
   */
  private void setLowerLeftCoordinteInNanoMeters(BoardCoordinate lowerLeftCoordinateInNanoMeters)
  {
    Assert.expect(lowerLeftCoordinateInNanoMeters != null);
    BoardCoordinate oldValue = null;
    if (_lowerLeftCoordinateInNanoMeters != null)
      oldValue = new BoardCoordinate(_lowerLeftCoordinateInNanoMeters);
    _projectObservable.setEnabled(false);
    try
    {
      _lowerLeftCoordinateInNanoMeters = lowerLeftCoordinateInNanoMeters;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, lowerLeftCoordinateInNanoMeters, LandPatternEventEnum.LOWER_LEFT_COORDINATE);
  }

  /**
   * @return the lower left coordinate for this LandPattern
   * @author Bill Darbie
   */
  BoardCoordinate getLowerLeftCoordinateInNanoMeters()
  {
    calculateWidthLengthAndLowerLeftCoordinateIfNecessary();

    Assert.expect(_lowerLeftCoordinateInNanoMeters != null);
    return new BoardCoordinate(_lowerLeftCoordinateInNanoMeters);
  }

  /**
   * @author Bill Darbie
   */
  private void setWidthInNanoMeters(int widthInNanoMeters)
  {
    Assert.expect(widthInNanoMeters > 0);
    int oldValue = _widthInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _widthInNanoMeters = widthInNanoMeters;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, widthInNanoMeters, LandPatternEventEnum.WIDTH);
  }

  /**
   * Width is calculated based on LandPatternPads
   * @return the x axis width of the LandPattern BEFORE any rotation has been applied.
   * @author Keith Lee
   */
  public int getWidthInNanoMeters()
  {
    calculateWidthLengthAndLowerLeftCoordinateIfNecessary();

    Assert.expect(_widthInNanoMeters != -1);
    return _widthInNanoMeters;
  }

  /**
   * @author Bill Darbie
   */
  private void setLengthInNanoMeters(int lengthInNanoMeters)
  {
    Assert.expect(lengthInNanoMeters > 0);
    int oldValue = _lengthInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _lengthInNanoMeters = lengthInNanoMeters;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, lengthInNanoMeters, LandPatternEventEnum.WIDTH);
  }

  /**
   * Length is calculated based on LandPatternPads
   * @return the y axis length of the LandPattern BEFORE any rotation has been applied.
   * @author Keith Lee
   */
  public int getLengthInNanoMeters()
  {
    calculateWidthLengthAndLowerLeftCoordinateIfNecessary();

    Assert.expect(_lengthInNanoMeters != -1);
    return _lengthInNanoMeters;
  }

  /**
   * @author Bill Darbie
   */
  public PadTypeEnum getPadTypeEnum()
  {
    PadTypeEnum padTypeEnum = null;

    for (LandPatternPad landPatternPad : _landPatternPadNameToLandPatternPadMap.values())
    {
      boolean isSurfaceMount = landPatternPad.isSurfaceMountPad();
      if (isSurfaceMount)
      {
        if (padTypeEnum == null)
          padTypeEnum = PadTypeEnum.SURFACE_MOUNT;
        else if (padTypeEnum.equals(PadTypeEnum.SURFACE_MOUNT) == false)
        {
          padTypeEnum = PadTypeEnum.SURFACE_MOUNT_AND_THROUGH_HOLE;
          break;
        }
      }
      else
      {
        if (padTypeEnum == null)
          padTypeEnum = PadTypeEnum.THROUGH_HOLE;
        else if (padTypeEnum.equals(PadTypeEnum.THROUGH_HOLE) == false)
        {
          padTypeEnum = PadTypeEnum.SURFACE_MOUNT_AND_THROUGH_HOLE;
          break;
        }
      }
    }

    Assert.expect(padTypeEnum != null);
    return padTypeEnum;
  }

  /**
   * @author Bill Darbie
   */
  public void assignAll()
  {
    assignPinOrientations();
    assignPitchAndInterPadDistanceToLandPatternPadsIfNecessary();
    assignPadOneIfNecessary();
    calculateWidthLengthAndLowerLeftCoordinateIfNecessary();
    calcNumRowsAndNumColumnsAndPadOneInCenter();
  }

  /**
   * This function calculates the orientation of pads that have no value already.
   * The general idea is to have pin orientation pointing towards the center of
   * the device.  The CompPackage class makes corrections to this assignment based
   * on the JointTypeEnum of the PackagePins.
   *
   * @author Bill Darbie
   */
  void assignPinOrientations()
  {
    Assert.expect(_landPatternPadNameToLandPatternPadMap != null);
    Assert.expect(_landPatternPadNameToLandPatternPadMap.isEmpty() == false);

    // any round or through hole pads should be set to PinOrientationEnum.NOT_APPLICABLE
    List<LandPatternPad> pads = setThroughHolePadsToNoOrientation();

    if (pads.isEmpty())
      return;

    ComponentCoordinate minPadCoordinate = new ComponentCoordinate();
    ComponentCoordinate maxPadCoordinate = new ComponentCoordinate();
    getMinAndMaxPadCoordinateForAllLandPatternPads(pads, minPadCoordinate, maxPadCoordinate);

    int maxX = maxPadCoordinate.getX();
    int maxY = maxPadCoordinate.getY();
    int minX = minPadCoordinate.getX();
    int minY = minPadCoordinate.getY();

    IntegerRef numPadsOnMinX = new IntegerRef(0);
    IntegerRef numPadsOnMaxX = new IntegerRef(0);
    IntegerRef numPadsOnMinY = new IntegerRef(0);
    IntegerRef numPadsOnMaxY = new IntegerRef(0);

    countPadsOnCoordinates(pads,
                           minX, maxX, minY, maxY,
                           numPadsOnMinX, numPadsOnMaxX, numPadsOnMinY, numPadsOnMaxY);

    int numberOfPadsOnMinX = numPadsOnMinX.getValue();
    int numberOfPadsOnMaxX = numPadsOnMaxX.getValue();
    int numberOfPadsOnMinY = numPadsOnMinY.getValue();
    int numberOfPadsOnMaxY = numPadsOnMaxY.getValue();

    int numberOfPadsOnCorner = getNumberOfPadsOnCorner(pads, minPadCoordinate, maxPadCoordinate);
    int numberOfPadsTotal = pads.size();

    int numberOfPadsOnMinAndMaxX = numberOfPadsOnMinX + numberOfPadsOnMaxX;
    int numberOfPadsOnMinAndMaxY = numberOfPadsOnMinY + numberOfPadsOnMaxY;

    if (numberOfPadsTotal == 1)
    {
      // one-Pad package
      calculatePinOrientationOfLandPatternWithOnePad(pads);
    }
    else if (numberOfPadsTotal == 2)
    {
      // two-Pad package
      calculatePinOrientationOfLandPatternWithTwoPads(pads, minPadCoordinate, maxPadCoordinate);
    }
    else if (minY == maxY)
    {
      // we have this:
      // |||||||

      // a single row of Pads package
      LandPatternPad landPatternPad = pads.get(0);
      if (minY <= 0)
      {
        calculateOrientationsOfRemainingPadsAlignedHorizontally(pads,
                                                                landPatternPad,
                                                                PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN);
      }
      else
      {
        calculateOrientationsOfRemainingPadsAlignedHorizontally(pads,
                                                                landPatternPad,
                                                                PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN);
      }
    }
    else if (minX == maxX)
    {
      // we have this:
      // -
      // -
      // -

      // a single column of Pads package
      LandPatternPad landPatternPad = pads.get(0);

      if (minX <= 0)
      {
        calculateOrientationsOfRemainingPadsAlignedVertically(pads,
                                                              landPatternPad,
                                                              PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN);
      }
      else
      {
        calculateOrientationsOfRemainingPadsAlignedVertically(pads,
                                                              landPatternPad,
                                                              PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN);
      }
    }
    else if (numberOfPadsOnMinAndMaxX == numberOfPadsOnMinAndMaxY)
    {
      // some kind of rectangle
      int width = Math.abs(maxX - minX);
      int length = Math.abs(maxY - minY);

      if (numberOfPadsTotal == (numberOfPadsOnMinAndMaxX + numberOfPadsOnMinAndMaxY))
      {
        if (width < length)
        {
          // we have this kind of case:
          //    ||
          //  -    -
          //  -    -
          //  -    -
          //  -    -
          //    ||

          // rectangle with larger length than width
          calculatePinOrientationOfPadsAlignedVertically(pads, minPadCoordinate, maxPadCoordinate);
        }
        else if (width > length)
        {
          // we have this kind of case:
          //    |||||
          //  -       -
          //  -       -
          //    |||||

          // rectangle with larger width than length
          calculatePinOrientationOfPadsAlignedHorizontally(pads, minPadCoordinate, maxPadCoordinate);
        }
        else
        {
          // square
          if ((numberOfPadsOnMinX == numberOfPadsOnMaxX) &&
              (numberOfPadsOnMinX == numberOfPadsOnMinY) &&
              (numberOfPadsOnMinY == numberOfPadsOnMaxY))
          {
            // we have this kind of case:
            //    ||
            //  -    -
            //  -    -
            //    ||

            // number of pads on min and max horizontal rows match
            // number of pads on min and max vertical rows match
            calculatePinOrientationOfPadsAlignedVertically(pads, minPadCoordinate, maxPadCoordinate);
          }
          else
          {
            if ((numberOfPadsOnMinX != numberOfPadsOnMaxX) && (numberOfPadsOnMinY == numberOfPadsOnMaxY))
            {
              // we have this kind of case:
              //    ||
              //  -    -
              //  -
              //    ||


              // number of pads on min and max horizontal rows do NOT match and
              // number of pads on min and max vertical rows do match
              calculatePinOrientationOfPadsAlignedVertically(pads, minPadCoordinate, maxPadCoordinate);
            }
            else if ((numberOfPadsOnMinY != numberOfPadsOnMaxY) && (numberOfPadsOnMinX == numberOfPadsOnMaxX))
            {
              // we have this kind of case:
              //    ||
              //  -    -
              //  -    -
              //    |

              // number of pads on min and max horizontal rows do match
              // number of pads on min and max vertical rows do NOT match
              calculatePinOrientationOfPadsAlignedHorizontally(pads, minPadCoordinate, maxPadCoordinate);
            }
            else
            {
              // number of pads on min and max horizontal rows do NOT match
              // number of pads on min and max vertical rows do NOT match
              if ((numberOfPadsOnMinX > numberOfPadsOnMaxX && numberOfPadsOnMaxY > numberOfPadsOnMinY) ||
                  (numberOfPadsOnMinX < numberOfPadsOnMaxX && numberOfPadsOnMaxY < numberOfPadsOnMinY))
              {
                // we have either of these cases:
                //    ||
                //  -    -
                //  -
                //    |
                //
                //     |
                //       -
                //  -    -
                //    ||

                calculatePinOrientationOfPadsAlignedVertically(pads, minPadCoordinate, maxPadCoordinate);
              }
              else
              {
                // not sure what case this is
                calculatePinOrientationOfPadsAlignedHorizontally(pads, minPadCoordinate, maxPadCoordinate);
              }
            }
          }
        }
      }
      else if (numberOfPadsOnCorner == numberOfPadsTotal)
      {
        if (width < length)
        {
          // this case:
          //   ||
          //
          //
          //   ||

          // rectangle with larger length than width
          calculatePinOrientationOfPadsAlignedHorizontally(pads, minPadCoordinate, maxPadCoordinate);
        }
        else
        {
          // this case:
          // -   -
          // -   -

          // rectangle with larger width than length
          calculatePinOrientationOfPadsAlignedVertically(pads, minPadCoordinate, maxPadCoordinate);
        }
      }
    }
    else if (numberOfPadsOnMinAndMaxX > numberOfPadsOnMinAndMaxY)
    {
      // we have this case:
      //   ||||||||
      //
      //   ||||||||
      calculatePinOrientationOfPadsAlignedVertically(pads, minPadCoordinate, maxPadCoordinate);
    }
    else if (numberOfPadsOnMinAndMaxX < numberOfPadsOnMinAndMaxY)
    {
      // we have this case:
      //   -  -
      //   -  -
      //   -  -
      //   -  -
      calculatePinOrientationOfPadsAlignedHorizontally(pads, minPadCoordinate, maxPadCoordinate);
    }

    DoubleCoordinate focalPoint1 = new DoubleCoordinate();
    DoubleCoordinate focalPoint2 = new DoubleCoordinate();
    setFocalPointsOfLandPattern(focalPoint1, focalPoint2, minPadCoordinate, maxPadCoordinate);

    calculateAllUnassignedPinOrientations(pads, focalPoint1, focalPoint2);
    finalCheckForUnassignedPinOrientations();

    for (CompPackage compPackage : getCompPackages())
      compPackage.assignPinOrientationCorrectionsWithoutOverridingPreviousUserSettings();
  }

  /**
   * @return the List of LandPatternPads that are still not assigned.
   * @author Bill Darbie
   */
  private List<LandPatternPad> setThroughHolePadsToNoOrientation()
  {
    List<LandPatternPad> unassignedPads = new ArrayList<LandPatternPad>();
    for (LandPatternPad landPatternPad : _landPatternPadNameToLandPatternPadMap.values())
    {
      if (landPatternPad.isThroughHolePad())
        landPatternPad.setPinOrientationEnum(PinOrientationEnum.NOT_APPLICABLE);
      else
        unassignedPads.add(landPatternPad);
    }

    return unassignedPads;
  }

  /**
   * @author Eugene Kim-Leighton
   */
  private void finalCheckForUnassignedPinOrientations()
  {
    for (LandPatternPad landPatternPad : _landPatternPadNameToLandPatternPadMap.values())
    {
      Assert.expect(landPatternPad.isPinOrientationEnumAssigned(), "Pad orientation not set for package: " + getName());
    }
  }

  /**
   * @author Eugene Kim-Leighton
   */
  private void calculateAllUnassignedPinOrientations(List<LandPatternPad> landPatternPads,
                                                     DoubleCoordinate focalPoint1,
                                                     DoubleCoordinate focalPoint2)
  {
    Assert.expect(landPatternPads != null);
    Assert.expect(focalPoint1 != null);
    Assert.expect(focalPoint2 != null);

    boolean verticalFocalPoints = false;
    if (focalPoint1.getX() == focalPoint2.getX())
      verticalFocalPoints = true;

    for (LandPatternPad landPatternPad : landPatternPads)
    {
      ComponentCoordinate padCoordinate = landPatternPad.getCoordinateInNanoMeters();

      if (verticalFocalPoints)
      {
        if (padCoordinate.getY() < focalPoint1.getY())
          calculatePinOrientationWithPadCoordinate(landPatternPad, focalPoint1);
        else if (padCoordinate.getY() > focalPoint2.getY())
          calculatePinOrientationWithPadCoordinate(landPatternPad, focalPoint2);
        else
        {
          DoubleCoordinate orthogonal = new DoubleCoordinate();
          orthogonal.setX(focalPoint1.getX());
          orthogonal.setY(padCoordinate.getY());
          calculatePinOrientationWithPadCoordinate(landPatternPad, orthogonal);
        }
      }
      else
      {
        if (padCoordinate.getX() < focalPoint1.getX())
          calculatePinOrientationWithPadCoordinate(landPatternPad, focalPoint1);
        else if (padCoordinate.getX() > focalPoint2.getX())
          calculatePinOrientationWithPadCoordinate(landPatternPad, focalPoint2);
        else
        {
          DoubleCoordinate orthogonal = new DoubleCoordinate();
          orthogonal.setY(focalPoint1.getY());
          orthogonal.setX(padCoordinate.getX());
          calculatePinOrientationWithPadCoordinate(landPatternPad, orthogonal);
        }
      }
    }
  }

  /**
   * @author Eugene Kim-Leighton
   */
  private void calculatePinOrientationWithPadCoordinate(DoubleCoordinate focalPoint1, DoubleCoordinate focalPoint2, PanelCoordinate minCoordinate, PanelCoordinate maxCoordinate)
  {
    Assert.expect(focalPoint1 != null);
    Assert.expect(focalPoint2 != null);
    Assert.expect(minCoordinate != null);
    Assert.expect(maxCoordinate != null);

    double offsetX = (maxCoordinate.getX() - minCoordinate.getX()) / 2.0;
    double offsetY = (maxCoordinate.getY() - minCoordinate.getY()) / 2.0;

    if (offsetX < offsetY)
    {
      focalPoint1.setX(minCoordinate.getX() + offsetX);
      focalPoint2.setX(minCoordinate.getX() + offsetX);
      focalPoint1.setY(minCoordinate.getY() + offsetX);
      focalPoint2.setY(maxCoordinate.getY() - offsetX);
    }
    else
    {
      focalPoint1.setX(minCoordinate.getX() + offsetY);
      focalPoint2.setX(maxCoordinate.getX() - offsetY);
      focalPoint1.setY(minCoordinate.getY() + offsetY);
      focalPoint2.setY(minCoordinate.getY() + offsetY);
    }
  }

  /**
   * @author Eugene Kim-Leighton
   */
  private void calculatePinOrientationOfPadsAlignedHorizontally(List<LandPatternPad> landPatternPads,
                                                                ComponentCoordinate minCoordinate,
                                                                ComponentCoordinate maxCoordinate)
  {
    Assert.expect(landPatternPads != null);
    Assert.expect(minCoordinate != null);
    Assert.expect(maxCoordinate != null);

    LandPatternPad minPad = null;
    LandPatternPad maxPad = null;

    for (LandPatternPad landPatternPad : landPatternPads)
    {
      ComponentCoordinate padCoordinate = landPatternPad.getCoordinateInNanoMeters();

      if (landPatternPad.isPinOrientationEnumAssigned()== false)
      {
        if (padCoordinate.getY() == minCoordinate.getY())
        {
          landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN);
          minPad = landPatternPad;
        }
        else if (padCoordinate.getY() == maxCoordinate.getY())
        {
          landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN);
          maxPad = landPatternPad;
        }
        else
        {
          // do nothing
        }
      }
    }
    if (minPad != null)
    {
      calculateOrientationsOfRemainingPadsAlignedHorizontally(landPatternPads,
                                                              minPad,
                                                              PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN);
    }

    if (maxPad != null)
    {
      calculateOrientationsOfRemainingPadsAlignedHorizontally(landPatternPads,
                                                              maxPad,
                                                              PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN);
    }
  }

  /**
   * @author Eugene Kim-Leighton
   */
  private void calculatePinOrientationOfPadsAlignedVertically(List<LandPatternPad> landPatternPads,
                                                              ComponentCoordinate minCoordinate,
                                                              ComponentCoordinate maxCoordinate)
  {
    Assert.expect(landPatternPads != null);
    Assert.expect(minCoordinate != null);
    Assert.expect(maxCoordinate != null);

    LandPatternPad minPad = null;
    LandPatternPad maxPad = null;

    for (LandPatternPad landPatternPad : landPatternPads)
    {
      ComponentCoordinate padCoordinate = landPatternPad.getCoordinateInNanoMeters();

      if (landPatternPad.isPinOrientationEnumAssigned() == false)
      {
        if (padCoordinate.getX() == minCoordinate.getX())
        {
          landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN);
          minPad = landPatternPad;
        }
        else if (padCoordinate.getX() == maxCoordinate.getX())
        {
          landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN);
          maxPad = landPatternPad;
        }
        else
        {
          // do nothing
        }
      }
    }
    if (minPad != null)
    {
      calculateOrientationsOfRemainingPadsAlignedVertically(landPatternPads, minPad, PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN);
    }

    if (maxPad != null)
    {
      calculateOrientationsOfRemainingPadsAlignedVertically(landPatternPads, maxPad, PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN);
    }
  }

  /**
   * @author Eugene Kim-Leighton
   */
  private void calculateOrientationsOfRemainingPadsAlignedHorizontally(List<LandPatternPad> landPatternPads,
                                                                       LandPatternPad referencePad,
                                                                       PinOrientationEnum pinOrientationEnum)
  {
    Assert.expect(landPatternPads != null);
    Assert.expect(referencePad != null);
    Assert.expect(pinOrientationEnum != null);
    Assert.expect(pinOrientationEnum != PinOrientationEnum.NOT_APPLICABLE);

    boolean landPatternPadWidthIsGreaterThanOrEqualToPadLength = isPadWidthGreaterThanOrEqualToPadLength(referencePad);
    double halfLength = referencePad.getLengthInNanoMeters() / 2.0;
    double lowLimit = (referencePad.getCoordinateInNanoMeters()).getY() - halfLength;
    double highLimit = (referencePad.getCoordinateInNanoMeters()).getY() + halfLength;

    for (LandPatternPad landPatternPad : landPatternPads)
    {
      ComponentCoordinate padCoordinate = landPatternPad.getCoordinateInNanoMeters();

      if ((landPatternPad.isPinOrientationEnumAssigned() == false) &&
          (padCoordinate.getY() >= lowLimit) &&
          (padCoordinate.getY() <= highLimit) &&
          (landPatternPadWidthIsGreaterThanOrEqualToPadLength == isPadWidthGreaterThanOrEqualToPadLength(landPatternPad)))
      {
        landPatternPad.setPinOrientationEnum(pinOrientationEnum);
      }
    }
  }

  /**
   * @author Eugene Kim-Leighton
   */
  private void calculateOrientationsOfRemainingPadsAlignedVertically(List<LandPatternPad> landPatternPads,
                                                                     LandPatternPad referencePad,
                                                                     PinOrientationEnum pinOrientationEnum)
  {
    Assert.expect(landPatternPads != null);
    Assert.expect(referencePad != null);
    Assert.expect(pinOrientationEnum != null);
    Assert.expect(pinOrientationEnum != PinOrientationEnum.NOT_APPLICABLE);

    boolean landPatternPadDimensionRatioGreaterThanOrEqualToOne = isPadWidthGreaterThanOrEqualToPadLength(referencePad);
    double halfWidth = referencePad.getWidthInNanoMeters() / 2;
    double lowLimit = (referencePad.getCoordinateInNanoMeters()).getX() - halfWidth;
    double highLimit = (referencePad.getCoordinateInNanoMeters()).getX() + halfWidth;

    for (LandPatternPad landPatternPad : landPatternPads)
    {
      ComponentCoordinate padCoordinate = landPatternPad.getCoordinateInNanoMeters();

      if ((landPatternPad.isPinOrientationEnumAssigned() == false) &&
          (padCoordinate.getX() >= lowLimit) &&
          (padCoordinate.getX() <= highLimit) &&
          (landPatternPadDimensionRatioGreaterThanOrEqualToOne == isPadWidthGreaterThanOrEqualToPadLength(landPatternPad)))
      {
        landPatternPad.setPinOrientationEnum(pinOrientationEnum);
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private boolean isPadWidthGreaterThanOrEqualToPadLength(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    if (landPatternPad.getWidthInNanoMeters() >= landPatternPad.getLengthInNanoMeters())
      return true;

    return false;
  }

  /**
   * @author Eugene Kim-Leighton
   */
  private void calculatePinOrientationOfLandPatternWithOnePad(List<LandPatternPad> landPatternPads)
  {
    Assert.expect(landPatternPads != null);
    Assert.expect(landPatternPads.size() == 1);

    LandPatternPad landPatternPad = (LandPatternPad)landPatternPads.get(0);

    if (landPatternPad.isPinOrientationEnumAssigned() == false)
    {
      ComponentCoordinate padCoordinate = landPatternPad.getCoordinateInNanoMeters();
      if (padCoordinate.getX() > 0)
      {
        // single Pad with an x > 0
        landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN);
      }
      else if (padCoordinate.getX() == 0)
      {
        if (padCoordinate.getY() > 0)
        {
          // single Pad with x = 0 , y > 0
          landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN);
        }
        else
        {
          // single Pad with x = 0 , y <= 0
          landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN);
        }
      }
      else
      {
        // single Pad with a negative X
        landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN);
      }
    }
  }

  /**
   * This function calculates and assigns a Pads orientation for the case where the LandPattern only
   * has two pads.
   * @author Eugene Kim-Leighton
   */
  private void calculatePinOrientationOfLandPatternWithTwoPads(List<LandPatternPad> landPatternPads,
                                                               ComponentCoordinate minCoordinate,
                                                               ComponentCoordinate maxCoordinate)
  {
    Assert.expect(landPatternPads != null);
    Assert.expect(minCoordinate != null);
    Assert.expect(maxCoordinate != null);

    DoubleCoordinate center = new DoubleCoordinate();
    center.setX((minCoordinate.getX() + maxCoordinate.getX()) / 2.0);
    center.setY((minCoordinate.getY() + maxCoordinate.getY()) / 2.0);

    for (LandPatternPad landPatternPad : landPatternPads)
    {
      if (landPatternPad.isPinOrientationEnumAssigned() == false)
      {
        calculatePinOrientationWithPadCoordinate(landPatternPad, center);
      }
    }
  }

  /**
   * @author Eugene Kim-Leighton
   */
  private void calculatePinOrientationWithPadCoordinate(LandPatternPad landPatternPad, DoubleCoordinate referencePoint)
  {
    Assert.expect(landPatternPad != null);
    Assert.expect(referencePoint != null);

    if (landPatternPad.isPinOrientationEnumAssigned() == false)
    {
      ComponentCoordinate padCoordinate = landPatternPad.getCoordinateInNanoMeters();

      double deltaX = padCoordinate.getX() - referencePoint.getX();
      double deltaY = padCoordinate.getY() - referencePoint.getY();

      if (Math.abs(deltaX) > Math.abs(deltaY))
      {
        // deltaX has the largest magnitude, so use it to determine orientation
        if (deltaX < 0)
          landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN);
        else
          landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN);
      }
      else
      {
        // deltaY has the largest magnitude, so use it to determine orientation
        if (deltaY < 0)
          landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN);
        else
          landPatternPad.setPinOrientationEnum(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN);
      }
    }
  }

  /**
   * @author Eugene Kim-Leighton
   */
  private int getNumberOfPadsOnCorner(List<LandPatternPad> landPatternPads,
                                      ComponentCoordinate minPadCoordinate,
                                      ComponentCoordinate maxPadCoordinate)
  {
    Assert.expect(landPatternPads != null);
    Assert.expect(minPadCoordinate != null);
    Assert.expect(maxPadCoordinate != null);

    int minX = minPadCoordinate.getX();
    int maxX = maxPadCoordinate.getX();
    int minY = minPadCoordinate.getY();
    int maxY = maxPadCoordinate.getY();

    int numPadsOnCorner = 0;
    for (LandPatternPad landPatternPad : landPatternPads)
    {
      ComponentCoordinate padCoordinate = landPatternPad.getCoordinateInNanoMeters();
      int x = padCoordinate.getX();
      int y = padCoordinate.getY();
      if ((x == maxX && y == maxY) ||
          (x == maxX && y == minY) ||
          (x == minX && y == minY) ||
          (x == minX && y == maxY))
      {
        ++numPadsOnCorner;
      }
    }

    return numPadsOnCorner;
  }

  /**
   * This function counts the number of pads that have the same coordinate
   * as minimum or maximum coordinate
   * @author Eugene Kim-Leighton
   */
  private void countPadsOnCoordinates(List<LandPatternPad> landPatternPads,
                                      int minX,
                                      int maxX,
                                      int minY,
                                      int maxY,
                                      IntegerRef numPadsOnMinX,
                                      IntegerRef numPadsOnMaxX,
                                      IntegerRef numPadsOnMinY,
                                      IntegerRef numPadsOnMaxY)
  {
    Assert.expect(landPatternPads != null);

    int padsOnMinX = 0;
    int padsOnMaxX = 0;
    int padsOnMinY = 0;
    int padsOnMaxY = 0;
    for (LandPatternPad landPatternPad : landPatternPads)
    {
      ComponentCoordinate padCoordinate = landPatternPad.getCoordinateInNanoMeters();
      int x = padCoordinate.getX();
      int y = padCoordinate.getY();
      if (x == minX)
        ++padsOnMinX;
      if (x == maxX)
        ++padsOnMaxX;
      if (y == minY)
        ++padsOnMinY;
      if (y == maxY)
        ++padsOnMaxY;
    }
    numPadsOnMinX.setValue(padsOnMinX);
    numPadsOnMaxX.setValue(padsOnMaxX);
    numPadsOnMinY.setValue(padsOnMinY);
    numPadsOnMaxY.setValue(padsOnMaxY);
  }

  /**
   * Calculate the coordinate of the smallest x and smallest y coordinates of all the pads
   * associated with the CompPackage.  Note that the x can come from one pad
   * and the y from another.  Do the same thing for the largest x and y coordinate
   * @author Eugene Kim-Leighton
   * @author Bill Darbie
   */
  private void getMinAndMaxPadCoordinateForAllLandPatternPads(List<LandPatternPad> landPatternPads,
                                                              ComponentCoordinate minPadCoordinate,
                                                              ComponentCoordinate maxPadCoordinate)
  {
    Assert.expect(landPatternPads != null);
    Assert.expect(minPadCoordinate != null);
    Assert.expect(maxPadCoordinate != null);

    Assert.expect(landPatternPads.isEmpty() == false);

    boolean first = true;
    for (LandPatternPad landPatternPad : landPatternPads)
    {
      ComponentCoordinate padCoordinate = landPatternPad.getCoordinateInNanoMeters();
      if (first == true)
      {
        minPadCoordinate.setX(padCoordinate.getX());
        minPadCoordinate.setY(padCoordinate.getY());
        maxPadCoordinate.setX(padCoordinate.getX());
        maxPadCoordinate.setY(padCoordinate.getY());
        first = false;
      }
      else
      {
        if (padCoordinate.getX() < minPadCoordinate.getX())
          minPadCoordinate.setX(padCoordinate.getX());
        if (padCoordinate.getY() < minPadCoordinate.getY())
          minPadCoordinate.setY(padCoordinate.getY());
        if (padCoordinate.getX() > maxPadCoordinate.getX())
          maxPadCoordinate.setX(padCoordinate.getX());
        if (padCoordinate.getY() > maxPadCoordinate.getY())
          maxPadCoordinate.setY(padCoordinate.getY());
      }
    }
  }

  /**
   * @author Poh Kheng
   */
  public void getMinAndMaxPadBoundCoordinateForAllLandPatternPads(ComponentCoordinate minPadCoordinate,
                                                             ComponentCoordinate maxPadCoordinate)
  {
    Assert.expect(minPadCoordinate != null);
    Assert.expect(maxPadCoordinate != null);
    boolean first = true;
    for (LandPatternPad landPatternPad : getLandPatternPads())
    {
      ComponentCoordinate padMinCoordinate = landPatternPad.getMinPadCoordinateInNanoMeters();
      ComponentCoordinate padMaxCoordinate = landPatternPad.getMaxPadCoordinateInNanoMeters();
      if (first == true)
      {
        minPadCoordinate.setX(padMinCoordinate.getX());
        minPadCoordinate.setY(padMinCoordinate.getY());
        maxPadCoordinate.setX(padMaxCoordinate.getX());
        maxPadCoordinate.setY(padMaxCoordinate.getY());
        first = false;
      }
      else
      {
        if (padMinCoordinate.getX() < minPadCoordinate.getX())
          minPadCoordinate.setX(padMinCoordinate.getX());
        if (padMinCoordinate.getY() < minPadCoordinate.getY())
          minPadCoordinate.setY(padMinCoordinate.getY());
        if (padMaxCoordinate.getX() > maxPadCoordinate.getX())
          maxPadCoordinate.setX(padMaxCoordinate.getX());
        if (padMaxCoordinate.getY() > maxPadCoordinate.getY())
          maxPadCoordinate.setY(padMaxCoordinate.getY());
      }
    }
  }


  /**
   * @author Eugene Kim-Leighton
   */
  private void setFocalPointsOfLandPattern(DoubleCoordinate focalPoint1,
                                           DoubleCoordinate focalPoint2,
                                           ComponentCoordinate minCoordinate,
                                           ComponentCoordinate maxCoordinate)
  {
    Assert.expect(focalPoint1 != null);
    Assert.expect(focalPoint2 != null);
    Assert.expect(minCoordinate != null);
    Assert.expect(maxCoordinate != null);

    double offsetX = (maxCoordinate.getX() - minCoordinate.getX()) / 2.0;
    double offsetY = (maxCoordinate.getY() - minCoordinate.getY()) / 2.0;

    if (offsetX < offsetY)
    {
      focalPoint1.setX(minCoordinate.getX() + offsetX);
      focalPoint2.setX(minCoordinate.getX() + offsetX);
      focalPoint1.setY(minCoordinate.getY() + offsetX);
      focalPoint2.setY(maxCoordinate.getY() - offsetX);
    }
    else
    {
      focalPoint1.setX(minCoordinate.getX() + offsetY);
      focalPoint2.setX(maxCoordinate.getX() - offsetY);
      focalPoint1.setY(minCoordinate.getY() + offsetY);
      focalPoint2.setY(minCoordinate.getY() + offsetY);
    }
  }

  /**
   * Assign one pad to be pad one.  This method should only be called after all
   * pads have been added to the component (both top and bottom side pads)
   *
   * @author Bill Darbie
   */
  public void assignPadOneIfNecessary()
  {
    //XCR-3447, Assert when undo changes on sm/th column in edit landpattern if land pattern only contain 1 pad
    if (_padOne == null && _landPatternPadNameToLandPatternPadMap.isEmpty() == false)
    {
      // the _landPatternPadSet is already sorted by name
      if (_userAssignedPadOne == null)
      {
        int i = 0;
        _projectObservable.setEnabled(false);
        try
        {
          for (LandPatternPad landPatternPad : _landPatternPadNameToLandPatternPadMap.values())
          {
            ++i;
            if (i == 1)
            {
              landPatternPad.setPadOneFromAutoAssign(true);
              _padOne = landPatternPad;
            }
            else
              landPatternPad.setPadOneFromAutoAssign(false);
          }
        }
        finally
        {
          _projectObservable.setEnabled(true);
        }
        _projectObservable.stateChanged(_padOne, null, _padOne, LandPatternPadEventEnum.PAD_ONE);
      }
      else
      {
        setUserAssignedPadOne(_userAssignedPadOne);
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  void setUserAssignedPadOne(LandPatternPad userAssignedPadOne)
  {
    Assert.expect(userAssignedPadOne != null);

    _userAssignedPadOne = userAssignedPadOne;
    _padOne = _userAssignedPadOne;
    boolean foundUserAssignedPadOne = false;
    for (LandPatternPad landPatternPad : _landPatternPadNameToLandPatternPadMap.values())
    {
      if (landPatternPad == userAssignedPadOne)
      {
        foundUserAssignedPadOne = true;
        landPatternPad.setPadOneFromAutoAssign(true);
      }
      else
      {
        landPatternPad.setPadOneFromAutoAssign(false);
      }
    }
    // Ying-Huan.Chu [1st August 2013]
    if (foundUserAssignedPadOne == false)
    {
      if (_userAssignedPadOne != null)
      {
        _userAssignedPadOne = null;
        _padOne = null;
      }
      assignPadOneIfNecessary();
    }
    //Assert.expect(foundUserAssignedPadOne);
  }

  /**
   * @author Bill Darbie
   */
  public void setUserAssignedPadOneForReaders(LandPatternPad userAssignedPadOne)
  {
    Assert.expect(userAssignedPadOne != null);
    _userAssignedPadOne = userAssignedPadOne;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isPadOneUserAssigned()
  {
    if (_userAssignedPadOne == null)
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  public LandPatternPad getUserAssignedPadOne()
  {
    Assert.expect(_userAssignedPadOne != null);
    return _userAssignedPadOne;
  }

  /**
   * @author Bill Darbie
   */
  void invalidatePadOne()
  {
    if (_userAssignedPadOne == null)
    {
      for (LandPatternPad landPatternPad : getLandPatternPads())
        landPatternPad.invalidatePadOne();
    }
  }

  /**
   * @author Bill Darbie
   */
  public LandPatternPad getLandPatternPadOne()
  {
    assignPadOneIfNecessary();

    Assert.expect(_padOne != null);
    return _padOne;
  }

  /**
   * @author Bill Darbie
   */
  void invalidePadOneInCenterOfRowOrColumn()
  {
    _numRowsAndColsAndPadOneCenterValid = false;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isPadOneInCenterOfRowOrColumn()
  {
    calcNumRowsAndNumColumnsAndPadOneInCenter();

    return _isPadOneInCenterOfRowOrColumn;
  }

  /**
   * @author Bill Darbie
   */
  void invalidatePitchAndInterPadDistance()
  {
    // no stateChanged need here since this flag is set when a LandPatternPad state change has
    // occurred anyway
    _pitchAndInterPadDistanceIsValid = false;
  }

  /**
   * @author Bill Darbie
   */
  void invalidatePadSize()
  {
    // no stateChanged need here since this flag is set when a LandPatternPad state change has
    // occurred anyway
    _sizeValid = false;
  }

  /**
   * Assign a pitch to each LandPatternPad based on where the LandPatterPads are located
   * relative to each other.  Pitch is defined as the center to center distance between
   * two adjacent Pads.  If there is only one Pad on a side of a Component, its pitch
   * will be 0.
   *
   * @author Bill Darbie
   */
  void assignPitchAndInterPadDistanceToLandPatternPadsIfNecessary()
  {
    if (_pitchAndInterPadDistanceIsValid)
      return;

    // calculate distance from one pad to all others to find the closest pad
    // also put each Pad into a Set based on its size
    Map<Integer, Set<LandPatternPad>> padSizeToLandPatternPadsMap = new HashMap<Integer, Set<LandPatternPad>>();
    Map<Integer, Integer> padSizeToPitch = new HashMap<Integer, Integer>();
    Map<Integer, Integer> padSizeToInterPadDistance = new HashMap<Integer, Integer>();

    for (LandPatternPad landPatternPad1 : _landPatternPadNameToLandPatternPadMap.values())
    {
      int width = landPatternPad1.getWidthInNanoMeters();
      int length = landPatternPad1.getLengthInNanoMeters();
      int maxWidthOrLength = Math.max(width, length);
      int padSize = maxWidthOrLength;

      // check to see if this padSize is the same or within 5% of a previous pad size
      for (Integer padSizeInMap : padSizeToLandPatternPadsMap.keySet())
      {
        int max = Math.max(padSizeInMap, padSize);
        double percent =  100.0 * (double)Math.abs(padSizeInMap - padSize) / (double)max;
        if (percent < 5)
        {
          // the two sizes are within 5% of each other, so use the one that is already in
          // the map
          padSize = padSizeInMap;
          break;
        }
      }

      // put the landPatternPad into the padSizeToLandPatternPadsMap
      Set<LandPatternPad> landPatternPadSet = padSizeToLandPatternPadsMap.get(padSize);
      if (landPatternPadSet == null)
      {
        landPatternPadSet = new HashSet<LandPatternPad>();
        padSizeToLandPatternPadsMap.put(padSize, landPatternPadSet);
      }
      landPatternPadSet.add(landPatternPad1);

      ComponentCoordinate coord1 = landPatternPad1.getCoordinateInNanoMeters();
      double x1 = coord1.getX();
      double y1 = coord1.getY();

      double shortestSquaredDistance = 0;
      double distanceCoveredByPads = 0.0;

      Rectangle2D boundsOfThisLandPatternPad = landPatternPad1.getShapeInNanoMeters().getBounds2D();
      double halfWidthOfThisPad = boundsOfThisLandPatternPad.getWidth()/2;
      double halfHeightOfThisPad = boundsOfThisLandPatternPad.getHeight()/2;

      for (LandPatternPad landPatternPad2 : _landPatternPadNameToLandPatternPadMap.values())
      {
        if (landPatternPad1 != landPatternPad2)
        {
          ComponentCoordinate coord2 = landPatternPad2.getCoordinateInNanoMeters();
          double x2 = coord2.getX();
          double y2 = coord2.getY();
          double deltaX = x2 - x1;
          double deltaY = y2 - y1;
          double distanceSquaredBetweenPads = deltaX * deltaX + deltaY * deltaY;
          if (shortestSquaredDistance == 0 || (distanceSquaredBetweenPads < shortestSquaredDistance))
          {
            shortestSquaredDistance = distanceSquaredBetweenPads;
            if (Math.abs(deltaX) > Math.abs(deltaY))
            {
              distanceCoveredByPads = halfWidthOfThisPad +
                                      landPatternPad2.getShapeInNanoMeters().getBounds2D().getWidth()/2;
            }
            else
            {
              distanceCoveredByPads = (halfHeightOfThisPad) +
                                      landPatternPad2.getShapeInNanoMeters().getBounds2D().getHeight()/2;
            }
          }
        }
      }
      double shortestDistance = Math.sqrt(shortestSquaredDistance);
      int pitch = MathUtil.roundDoubleToInt(shortestDistance);

      // check to see if this pitch is the smallest one for this size pad
      Integer mapPitch = padSizeToPitch.get(padSize);
      if (mapPitch != null)
      {
        if (mapPitch < pitch)
          pitch = mapPitch;
      }
      padSizeToPitch.put(padSize, pitch);

      // figure out interPadDistance
      int interPadDistance = 0;
      if (shortestDistance != 0)
      {
        interPadDistance = (int)(shortestDistance - distanceCoveredByPads);
      }

      if (interPadDistance < 0)
      {
        // the Pads overlap, so set distance to 0
        interPadDistance = 0;
      }

      // check to see if this inter pad distance is the smallest one for this size pad
      Integer mapInterPadDistance = padSizeToInterPadDistance.get(padSize);
      if (mapInterPadDistance != null)
      {
        if (mapInterPadDistance < interPadDistance)
          interPadDistance = mapInterPadDistance;
      }
      padSizeToInterPadDistance.put(padSize, interPadDistance);
    }

    // ok, now set the pitch and interpad distance on all pads
    for (Map.Entry<Integer, Set<LandPatternPad>> entry : padSizeToLandPatternPadsMap.entrySet())
    {
      int padSize = entry.getKey();
      int pitch = padSizeToPitch.get(padSize);
      int interPadDistance = padSizeToInterPadDistance.get(padSize);
      for (LandPatternPad landPatternPad : entry.getValue())
      {

        landPatternPad.setPitchInNanoMeters(pitch);
        landPatternPad.setInterPadDistanceInNanoMeters(interPadDistance);
        // Wei Chin for single pad causing no inter pad distance bugs
        if(_landPatternPadNameToLandPatternPadMap.size() == 1)
        {
          landPatternPad.setInterPadDistanceInNanoMeters(ImageAnalysis.MAX_INTERPAD_DISTANCE_IN_NANOMETERS_FOR_ALGORITHMS);
        }
      }
    }

    _pitchAndInterPadDistanceIsValid = true;
  }

  /**
   * Sets the _widthInNanoMeters and _lengthInNanoMeters member variables
   * It does this by using the pads to calculate the extents of the components
   * @author Andy Mechtenberg
   * @Edited By Siew Yeng, Phang
   * - Bounding box will draw out extra area, when component land pattern reference point is not
   *   at center of the component.
   * - Set it to null will fixed this problem.
   */
  private void calculateWidthLengthAndLowerLeftCoordinateIfNecessary()
  {
    if (_sizeValid == false)
    {
      Collection<LandPatternPad> landPatternPads = getLandPatternPads();

      //Comment this
      //java.awt.Rectangle componentRect = new java.awt.Rectangle(0, 0);
      
      //using this right now
      java.awt.Rectangle componentRect = null;
      // iterate through all the pads and add the pad rectangle to the component rectangle outline
      for (LandPatternPad landPatternPad : landPatternPads)
      {
        ComponentCoordinate landPatternPadCoordinate = landPatternPad.getCoordinateInNanoMeters();
        int landPatternPadWidth = landPatternPad.getWidthInNanoMeters();
        int landPatternPadLength = landPatternPad.getLengthInNanoMeters();

        // use a double rect here because I'm dividing by 2.0 to convert the center based coordinate to the lower left coordinate
        DoubleCoordinate lowerLeftPadLocation = new DoubleCoordinate(landPatternPadCoordinate.getX() -
            landPatternPadWidth / 2.0,
            landPatternPadCoordinate.getY() - landPatternPadLength / 2.0);
        java.awt.geom.Rectangle2D.Double rect = new java.awt.geom.Rectangle2D.Double(lowerLeftPadLocation.getX(),
            lowerLeftPadLocation.getY(),
            landPatternPadWidth,
            landPatternPadLength);

        // we need the start the union rectangle with the first one found, otherwise we do the union
        // of the ORIGIN rectangle (x = 0, y = 0), and the union will ALWAYS include the origin.  That's bad because the land
        // pattern definition may not always do that.
        if (componentRect == null)
          componentRect = new java.awt.Rectangle((int)rect.getX(), (int)rect.getY(), (int)rect.getWidth(),
                                                 (int)rect.getHeight());
        else
          componentRect.add(rect); // creates the UNION of the passed in rect and itself.  Cool, eh?
      }
      setWidthInNanoMeters(componentRect.width);
      setLengthInNanoMeters(componentRect.height);

      setLowerLeftCoordinteInNanoMeters(new BoardCoordinate(componentRect.x, componentRect.y));
    }
    _sizeValid = true;
  }
  
  /**
   * @author Bill Darbie
   */
  private void calcNumRowsAndNumColumnsAndPadOneInCenter()
  {
    if (_numRowsAndColsAndPadOneCenterValid == false)
    {
      assignPadOneIfNecessary();

      Map<Integer, Set<LandPatternPad>> xCoordMap = new HashMap<Integer, Set<LandPatternPad>>();
      Map<Integer, Set<LandPatternPad>> yCoordMap = new HashMap<Integer, Set<LandPatternPad>>();
      for (LandPatternPad landPatternPad : getLandPatternPads())
      {
        ComponentCoordinate coord = landPatternPad.getCoordinateInNanoMeters();
        int x = coord.getX();
        int y = coord.getY();
        // populate xCoordMap
        Set<LandPatternPad> landPatternPadSet = xCoordMap.get(x);
        if (landPatternPadSet == null)
        {
          landPatternPadSet = new HashSet<LandPatternPad>();
          landPatternPadSet.add(landPatternPad);
          xCoordMap.put(x, landPatternPadSet);
        }
        else
          landPatternPadSet.add(landPatternPad);

        // populate xCoordMap
        landPatternPadSet = yCoordMap.get(y);
        if (landPatternPadSet == null)
        {
          landPatternPadSet = new HashSet<LandPatternPad>();
          landPatternPadSet.add(landPatternPad);
          yCoordMap.put(y, landPatternPadSet);
        }
        else
          landPatternPadSet.add(landPatternPad);
      }

      LandPatternPad padOne = getLandPatternPadOne();
      _isPadOneInCenterOfRowOrColumn = true;
      // iterate over a column of Pads, if the # of Pads in a column is more than 2,
      // then increment numCols
      int numCols = 0;
      for (Map.Entry<Integer, Set<LandPatternPad>> entry : xCoordMap.entrySet())
      {
        // if the number of pads that share the same x coord is more than 2 then
        // we have a column
        int x = entry.getKey();
        Set<LandPatternPad> landPatternPadSet = entry.getValue();
        if (landPatternPadSet.size() > 2)
        {
          ++numCols;
          if (_isPadOneInCenterOfRowOrColumn)
          {
            // build up yToLandPatternPadMap so we can tell if pad one is in the center of a row or column
            Map<Integer, LandPatternPad> yToLandPatternPadMap = new TreeMap<Integer, LandPatternPad>();
            for (LandPatternPad landPatternPad : landPatternPadSet)
            {
              Object prev = yToLandPatternPadMap.put(landPatternPad.getCoordinateInNanoMeters().getY(), landPatternPad);
              // it is possible to have 2 pads at the same x,y location, so do not assert here
              // Assert.expect(prev == null);
            }
            List<LandPatternPad> cols = new ArrayList<LandPatternPad>(yToLandPatternPadMap.values());
            if (cols.isEmpty() == false)
            {
              if ((padOne == cols.get(0)) || (padOne == cols.get(cols.size() - 1)))
                _isPadOneInCenterOfRowOrColumn = false;
            }
          }
        }
      }


      // iterate over a row of Pads, if the # of Pads in a row is more than 2,
      // then increment numRows
      int numRows = 0;
      for (Map.Entry<Integer, Set<LandPatternPad>> entry : yCoordMap.entrySet())
      {
        // if the number of pads that share the same y coord is more than 2 then
        // we have a row
        int y = entry.getKey();
        Set<LandPatternPad> landPatternPadSet = entry.getValue();
        if (landPatternPadSet.size() > 2)
        {
          ++numRows;
          if (_isPadOneInCenterOfRowOrColumn)
          {
            // build up xToLandPatternPadMap so we can tell if pad one is in the center of a row or column
            Map<Integer, LandPatternPad> xToLandPatternPadMap = new TreeMap<Integer, LandPatternPad>();
            for (LandPatternPad landPatternPad : landPatternPadSet)
            {
              Object prev = xToLandPatternPadMap.put(landPatternPad.getCoordinateInNanoMeters().getX(), landPatternPad);
              // it is possible to have 2 pads at the same x,y location, so do not assert here
              // Assert.expect(prev == null);
            }
            List<LandPatternPad> rows = new ArrayList<LandPatternPad>(xToLandPatternPadMap.values());
            if (rows.isEmpty() == false)
            {
              if ((padOne == rows.get(0)) || (padOne == rows.get(rows.size() - 1)))
                _isPadOneInCenterOfRowOrColumn = false;
            }
          }
        }
      }

      if ((numRows == 0) && (numCols == 0))
        _isPadOneInCenterOfRowOrColumn = false;

      setNumRows(numRows);
      setNumCols(numCols);
      _numRowsAndColsAndPadOneCenterValid = true;
    }
  }

  /**
   * @author Bill Darbie
   */
  private void setNumRows(int numRows)
  {
    Assert.expect(numRows >= 0);

    int oldValue = _numRows;
    _projectObservable.setEnabled(false);
    try
    {
      _numRows = numRows;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, numRows, LandPatternEventEnum.NUM_ROWS);
  }

  /**
   * @return the number of rows that have 3 or more pads in them with the same y coordinate.
   * @author Bill Darbie
   */
  public int getNumRows()
  {
    calcNumRowsAndNumColumnsAndPadOneInCenter();
    return _numRows;
  }

  /**
   * @author Bill Darbie
   */
  private void setNumCols(int numCols)
  {
    Assert.expect(numCols >= 0);

    int oldValue = _numCols;
    _projectObservable.setEnabled(false);
    try
    {
      _numCols = numCols;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, numCols, LandPatternEventEnum.NUM_COLS);
  }

  /**
   * @return the number of columns that have 3 or more pads in them with the same x coordinate.
   * @author Bill Darbie
   */
  public int getNumColumns()
  {
    calcNumRowsAndNumColumnsAndPadOneInCenter();
    return _numCols;
  }

  /**
   * @author Bill Darbie
   */
  void invalidateNumRowsAndColumnsAndPadOneInCenter()
  {
    _numRowsAndColsAndPadOneCenterValid = false;
  }

  /**
   * This would return the set of data which is use for measure correlation score
   * in Refine Match in Package Library for all the pads
   * @author Poh Kheng
   */
  public double[] getLandPatternPadsCorrelationData()
  {
    List<LandPatternPad> landPatternPads = getLandPatternPads();
    double[] padsCorrelationData = new double[landPatternPads.size()*5];
    int count = 0;

    for(LandPatternPad landPatternPad : landPatternPads)
    {
      padsCorrelationData[count++] = landPatternPad.getCoordinateInNanoMeters().getX();
      padsCorrelationData[count++] = landPatternPad.getCoordinateInNanoMeters().getY();
      padsCorrelationData[count++] = Math.round(landPatternPad.getAreaInNanoMeters());
      padsCorrelationData[count++] = landPatternPad.getLengthInNanoMeters();
      padsCorrelationData[count++] = landPatternPad.getWidthInNanoMeters();
    }
    return padsCorrelationData;

  }

  /**
   * @author Poh Kheng
   */
  public int getNumberOfLandPatternPads()
  {
    Assert.expect(_landPatternPadNameToLandPatternPadMap != null);

    return _landPatternPadNameToLandPatternPadMap.size();
  }

  /**
   * This would return a set of degree rotation for all the pads.
   *
   * @author Poh Kheng
   */
  public double[] getLandPatternPadsDegreeRotationData()
  {
    List<LandPatternPad> landPatternPads = getLandPatternPads();
    double[] padsCorrelationData = new double[landPatternPads.size()];
    int count = 0;

    for(LandPatternPad landPatternPad : landPatternPads)
    {
      padsCorrelationData[count++] = landPatternPad.getDegreesRotation();
    }
    return padsCorrelationData;
  }
}
