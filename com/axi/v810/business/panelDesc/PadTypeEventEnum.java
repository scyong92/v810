package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class PadTypeEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static PadTypeEventEnum CREATE_OR_DESTROY = new PadTypeEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static PadTypeEventEnum COMPONENT_TYPE = new PadTypeEventEnum(++_index);
  public static PadTypeEventEnum ADD_OR_REMOVE_PAD = new PadTypeEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static PadTypeEventEnum LAND_PATTERN_PAD = new PadTypeEventEnum(++_index);
  public static PadTypeEventEnum PAD_TYPE_SETTINGS = new PadTypeEventEnum(++_index);
  public static PadTypeEventEnum PACKAGE_PIN = new PadTypeEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private PadTypeEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private PadTypeEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
