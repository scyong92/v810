package com.axi.v810.business.panelDesc;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.util.*;

/**
 * For a Fiducial on a SideBoard, there is one FiducialType instance for each SideBoardType.
 * For a Fiducial on a Panel, there is one FiducialType for each Fiducial.
 *
 * @author Bill Darbie
 */
public class FiducialType implements Serializable
{
  // created by any PanelNdfReader, PanelReader, BoardTypeReader

  private String _name; // PanelNdfReader, PanelReader, BoardTypeReader
  private PanelCoordinate _coordinateInNanoMeters; // PanelNdfReader, PanelReader, BoardTypeReader
  private int _widthInNanoMeters = -1; // PanelNdfReader, PanelReader, BoardTypeReader
  private int _lengthInNanoMeters = -1; // PanelNdfReader, PanelReader, BoardTypeReader
  private ShapeEnum _shapeEnum; // PanelNdfReader, PanelReader, BoardTypeReader
  private Set<Fiducial> _fiducialSet = new HashSet<Fiducial>();

  private static transient ProjectObservable _projectObservable;

  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public FiducialType()
  {
    _projectObservable.stateChanged(this, null, this, FiducialTypeEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      Set<SideBoardType> sideBoardTypeSet = new HashSet<SideBoardType>();
      for (Fiducial fiducial : getFiducials())
      {
        if (fiducial.isOnBoard())
        {
          sideBoardTypeSet.add(fiducial.getSideBoard().getSideBoardType());
        }
        fiducial.destroy();
      }
      for (SideBoardType sideBoardType : sideBoardTypeSet)
      {
        sideBoardType.removeFiducialType(this);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, FiducialTypeEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void remove()
  {
    _projectObservable.setEnabled(false);
    try
    {
      Set<SideBoardType> sideBoardTypeSet = new HashSet<SideBoardType>();
      for (Fiducial fiducial : getFiducials())
      {
        if (fiducial.isOnBoard())
        {
          sideBoardTypeSet.add(fiducial.getSideBoard().getSideBoardType());
        }
        fiducial.getSideBoard().removeFiducial(fiducial);
      }
      for (SideBoardType sideBoardType : sideBoardTypeSet)
      {
        sideBoardType.removeFiducialType(this);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, FiducialTypeEventEnum.ADD_OR_REMOVE);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void add()
  {
    _projectObservable.setEnabled(false);
    try
    {
      Set<SideBoardType> sideBoardTypeSet = new HashSet<SideBoardType>();
      for (Fiducial fiducial : getFiducials())
      {
        fiducial.getSideBoard().addFiducial(fiducial);

        if (fiducial.isOnBoard())
        {
          sideBoardTypeSet.add(fiducial.getSideBoard().getSideBoardType());
        }
      }
      for (SideBoardType sideBoardType : sideBoardTypeSet)
      {
        sideBoardType.addFiducialType(this);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, this, FiducialTypeEventEnum.ADD_OR_REMOVE);

  }

  /**
   * @author Bill Darbie
   */
  public void setName(String name)
  {
    Assert.expect(name != null);

    if (name.equals(_name))
      return;

    String oldValue = _name;
    _projectObservable.setEnabled(false);
    try
    {
      updateNewName(name);
      _name = name.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, name, FiducialTypeEventEnum.NAME);
  }

  /**
   * @author Bill Darbie
   */
  public void updateNewName(String name)
  {
    Assert.expect(name != null);

    boolean panelNameChecked = false;
    boolean sideBoardNameChecked = false;
    for (Fiducial fiducial : getFiducials())
    {
      if (fiducial.isOnPanel())
      {
        Panel panel = fiducial.getPanel();
        if (panelNameChecked == false)
        {
          Assert.expect(panel.isFiducialNameValid(name));
          Assert.expect(panel.isFiducialNameDuplicate(name) == false);
          panelNameChecked = true;
        }
        panel.setNewFiducialName(fiducial, _name, name);
      }

      if (fiducial.isOnBoard())
      {
        SideBoard sideBoard = fiducial.getSideBoard();
        Board board = sideBoard.getBoard();
        SideBoardType sideBoardType = sideBoard.getSideBoardType();
        BoardType boardType = sideBoardType.getBoardType();
        if (sideBoardNameChecked == false)
        {
          Assert.expect(boardType.isFiducialTypeNameValid(name) );
          Assert.expect(boardType.isFiducialTypeNameDuplicate(name) == false);
          sideBoardNameChecked = true;
          boardType.setNewFiducialTypeName(this, sideBoardType, _name, name);
        }
        board.setNewFiducialName(fiducial, sideBoard, _name, name);
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  public String getName()
  {
    Assert.expect(_name != null);
    return _name;
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getName();
  }

  /**
   * For a Panel Fiducial.
   * @author Bill Darbie
   */
  public void setCoordinateInNanoMeters(PanelCoordinate coordinateInNanoMeters)
  {
    Assert.expect(coordinateInNanoMeters != null);

    if (coordinateInNanoMeters.equals(_coordinateInNanoMeters))
      return;

    PanelCoordinate oldValue = _coordinateInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _coordinateInNanoMeters = coordinateInNanoMeters;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, coordinateInNanoMeters, FiducialTypeEventEnum.COORDINATE);
  }

  /**
   * For a Board Fiducial.
   * @author Bill Darbie
   */
  public void setCoordinateInNanoMeters(BoardCoordinate coordinateInNanoMeters)
  {
    Assert.expect(coordinateInNanoMeters != null);
    setCoordinateInNanoMeters(new PanelCoordinate(coordinateInNanoMeters));
  }

  /**
   * @author Bill Darbie
   */
  public PanelCoordinate getCoordinateInNanoMeters()
  {
    Assert.expect(_coordinateInNanoMeters != null);

    return new PanelCoordinate(_coordinateInNanoMeters);
  }

  /**
   * @author Bill Darbie
   */
  public void setWidthInNanoMeters(int widthInNanoMeters)
  {
    Assert.expect(widthInNanoMeters > 0);

    if (widthInNanoMeters == _widthInNanoMeters)
      return;

    int oldValue = _widthInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _widthInNanoMeters = widthInNanoMeters;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, widthInNanoMeters, FiducialTypeEventEnum.WIDTH);
  }

  /**
   * @author Bill Darbie
   */
  public int getWidthInNanoMeters()
  {
    Assert.expect(_widthInNanoMeters > 0);

    return _widthInNanoMeters;
  }

  /**
   * @author Bill Darbie
   */
  public void setLengthInNanoMeters(int lengthInNanoMeters)
  {
    Assert.expect(lengthInNanoMeters > 0);

    if (lengthInNanoMeters == _lengthInNanoMeters)
      return;

    int oldValue = _lengthInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _lengthInNanoMeters = lengthInNanoMeters;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, lengthInNanoMeters, FiducialTypeEventEnum.LENGTH);
  }

  /**
   * @author Bill Darbie
   */
  public int getLengthInNanoMeters()
  {
    Assert.expect(_lengthInNanoMeters > 0);

    return _lengthInNanoMeters;
  }

  /**
   * @author Bill Darbie
   */
  public void setShapeEnum(ShapeEnum shapeEnum)
  {
    Assert.expect(shapeEnum != null);

    if (shapeEnum.equals(_shapeEnum))
      return;

    ShapeEnum oldValue = _shapeEnum;
    _projectObservable.setEnabled(false);
    try
    {
      _shapeEnum = shapeEnum;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, shapeEnum, FiducialTypeEventEnum.SHAPE_ENUM);
  }

  /**
   * @author Bill Darbie
   */
  void addFiducial(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);

    _projectObservable.setEnabled(false);
    try
    {
      Assert.expect(_fiducialSet != null);
      boolean added = _fiducialSet.add(fiducial);
      Assert.expect(added);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, fiducial, FiducialTypeEventEnum.ADD_OR_REMOVE_FIDUCIAL);
  }

  /**
   * @author Andy Mechtenberg
   */
  void removeFiducial(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);
    _projectObservable.setEnabled(false);
    try
    {
      Assert.expect(_fiducialSet != null);
      boolean removed = _fiducialSet.remove(fiducial);
      Assert.expect(removed);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, fiducial, null, FiducialTypeEventEnum.ADD_OR_REMOVE_FIDUCIAL);
  }

  /**
   * @author Bill Darbie
   */
  public List<Fiducial> getFiducials()
  {
    Assert.expect(_fiducialSet != null);

    return new ArrayList<Fiducial>(_fiducialSet);
  }

  /**
   * @author Bill Darbie
   */
  public ShapeEnum getShapeEnum()
  {
    Assert.expect(_shapeEnum != null);

    return _shapeEnum;
  }

  /**
   * @return the Shape not related to any board or panel setting.
   * @author Bill Darbie
   */
  public java.awt.Shape getShapeInNanoMeters()
  {
    PanelCoordinate padCoordinate = getCoordinateInNanoMeters();
    int padWidth = getWidthInNanoMeters();
    int padLength = getLengthInNanoMeters();

    // find the lower left corner
    DoubleCoordinate lowerLeftPadLocation = new DoubleCoordinate(padCoordinate.getX() - padWidth / 2.0,
                                                                 padCoordinate.getY() - padLength / 2.0);

    java.awt.Shape shape = null;
    if (getShapeEnum().equals(ShapeEnum.RECTANGLE))
    {
      shape = new Rectangle2D.Double(lowerLeftPadLocation.getX(),
                                     lowerLeftPadLocation.getY(),
                                     padWidth,
                                     padLength);
    }
    else if (getShapeEnum().equals(ShapeEnum.CIRCLE))
    {
      if (padWidth == padLength)
      {
        // this is a circle
        shape = new Ellipse2D.Double(lowerLeftPadLocation.getX(),
                                     lowerLeftPadLocation.getY(),
                                     padWidth,
                                     padLength);
      }
      else
      {
        shape =  GeomUtil.createObround((int)lowerLeftPadLocation.getX(),
                                        (int)lowerLeftPadLocation.getY(),
                                        padWidth,
                                        padLength);
      }
    }
    else
      Assert.expect(false, "The fiducial shape was neither a circle nor a rectangle");

    Assert.expect(shape != null);
    return shape;
  }

  /**
   * Retruns true for a board fiducial type and false for a panel fiducial type. This method should be called
   * before getSideBoardType()
   * @author Laura Cormos
   */
  public boolean hasSideBoardType()
  {
    Assert.expect(_fiducialSet != null);

    if (_fiducialSet.iterator().next().isOnBoard())
      return true;

    return false;
  }

  /**
   * @author Laura Cormos
   */
  public SideBoardType getSideBoardType()
  {
    Assert.expect(_fiducialSet != null);
    SideBoardType sideBoardType = null;
    // grab the side board type of the first fiducial in the set
    Fiducial firstFiducial = _fiducialSet.iterator().next();
    if (firstFiducial.isOnBoard())
    {
      sideBoardType = firstFiducial.getSideBoard().getSideBoardType();
      for(Fiducial fiducial : _fiducialSet)
      {
        SideBoardType otherSideBoardType = fiducial.getSideBoard().getSideBoardType();
        // verify assumption that all fiducials for this fiducial type are on the same side of the board
        Assert.expect(otherSideBoardType.equals(sideBoardType));
      }
    }
    else // a panel fiducial does not have a sideBoardType. The hasSideBoardType() should be called first to find out
      Assert.expect(false);

    return sideBoardType;
  }

  /**
   * @author Laura Cormos
   */
  public boolean isFiducialTypeWithinBoardTypeOutline()
  {
    java.awt.Shape boardTypeShape = getSideBoardType().getBoardType().getShapeInNanoMeters();
    java.awt.Shape fiducialTypeShape = getShapeInNanoMeters();

    boolean within = MathUtil.fuzzyContains(boardTypeShape, fiducialTypeShape, 500);
    return within;
  }
}
