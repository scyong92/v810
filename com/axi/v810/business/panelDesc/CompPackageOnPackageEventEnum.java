package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Khaw Chek Hau
 * POP Development
 */
public class CompPackageOnPackageEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static CompPackageOnPackageEventEnum CREATE_OR_DESTROY = new CompPackageOnPackageEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static CompPackageOnPackageEventEnum ADD_OR_REMOVE = new CompPackageOnPackageEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static CompPackageOnPackageEventEnum PANEL = new CompPackageOnPackageEventEnum(++_index);
  public static CompPackageOnPackageEventEnum NAME = new CompPackageOnPackageEventEnum(++_index);
  public static CompPackageOnPackageEventEnum ADD_OR_REMOVE_COMPONENT_TYPE = new CompPackageOnPackageEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private CompPackageOnPackageEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private CompPackageOnPackageEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
