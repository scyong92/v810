package com.axi.v810.business.panelDesc;

import java.io.*;
import java.util.*;
import java.util.zip.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class contains information for each package.  There should be one
 * CompPackage instance per unique package type on the Panel.  Many
 * Components on the panel will share a reference to the same
 * CompPackage instance.
 *
 * NOTE that the x-ray test system's concept of a package is slightly different
 * than a real package.  We create packages ourselves from the CAD data.  A
 * CompPackage will always have only one LandPattern associated with it, but
 * one LandPattern can have more than one CompPackage associated with it.
 * This is different than real CAD packages, but since we control how we create
 * CompPackages we can guarantee this relationship.
 *
 * @author Bill Darbie
 */
public class CompPackage implements Serializable
{
  // created by ComponentType, BoardTypeReader
  private Panel _panel; // ComponentType, BoardTypeReader
  private String _longName; // ComponentType, BoardTypeReader
  private String _shortName; // ComponentType, BoardTypeReader
  // do not use a comparator for _componentTypeSet since the same name can exist for different ComponentTypes
  private Set<ComponentType> _componentTypeSet = new HashSet<ComponentType>(); // ComponentNdfReader, BoardTypeReader
  // do not sort by name for _packagePinSet since the same name can occur more than once!
  private Set<PackagePin> _packagePinSet = new HashSet<PackagePin>(); // ComponentType, CompPackageReader
  private LandPattern _landPattern; // PanelDescBus, BoardTypeReader
  private BooleanRef _usesOneJointTypeEnum;
  private BooleanRef _usesOneJointHeight;

  private static transient ProjectObservable _projectObservable;

  private String _sourceLibraryPath = null;
  private long _libraryCheckSum = -1;
  private String _libraryLandpatternName = null;
  private boolean _imported = false;
  private long _previousSubtypeThresholdsCheckSum = 0;

  private transient long _currentSubtypeThresholdsCheckSum = 0;
  private transient String _libraryPath = null;
  private transient Set<SubtypeScheme> _subtypeSchemes = new HashSet<SubtypeScheme>();
  private transient Map<String, List<LibrarySubtypeScheme>> _libraryPathToLibrarySubtypeSchemesMap = new HashMap<String,List<LibrarySubtypeScheme>>();

  private transient Map<String, List<LibraryPackage>> _libraryPathToLibraryPackagesMap = new HashMap<String,List<LibraryPackage>>();

  private transient Set<LibrarySubtypeScheme> _librarySubtypeSchemes = new HashSet<LibrarySubtypeScheme>();

  private transient long _checkSum;
  private transient boolean _overwriteToLibrary = false;

  private transient String _backupSourceLibraryPath = null;
  private transient boolean _backupImportedFlag = false;
  private transient long _backupPreviousSubtypeThresholdsCheckSum = 0;
  private transient boolean _isBackup = false;
  
  private static transient PadNameComparator _padNameComparator;
  private static transient AlphaNumericComparator _alphaNumericComparator;
  
  //Siew Yeng - XCR-3094
  private transient BooleanRef _shouldClassifiesAtComponentOrMeasurementGroupLevel = null;

  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
    _padNameComparator = new PadNameComparator();
    _alphaNumericComparator = new AlphaNumericComparator();
  }

  /**
   * @author Bill Darbie
   */
  public CompPackage()
  {
    _projectObservable.stateChanged(this, null, this, CompPackageEventEnum.CREATE_OR_DESTROY);
    resetCheckSum();
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      for (PackagePin packagePin : getPackagePinsUnsorted())
        packagePin.destroy();
      _panel.removeCompPackage(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, CompPackageEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void setPanel(Panel panel)
  {
    Assert.expect(panel != null);

    Panel oldValue = _panel;
    _projectObservable.setEnabled(false);
    try
    {
      _panel = panel;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, panel, CompPackageEventEnum.PANEL);

  }

  /**
   * @author Bill Darbie
   */
  public void setLongName(String longName)
  {
    Assert.expect(longName != null);
    Assert.expect(_panel != null);
    Assert.expect(_panel.isCompPackageNameValid(longName));
    Assert.expect(_panel.isCompPackageNameDuplicate(longName) == false);

    if (longName.equals(_longName))
      return;

    String oldValue = _longName;
    _projectObservable.setEnabled(false);
    try
    {
      _panel.setNewCompPackageName(this, _longName, longName);
      _longName = longName.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, longName, CompPackageEventEnum.LONG_NAME);
  }

  /**
   * @author Bill Darbie
   */
  public String getLongName()
  {
    Assert.expect(_longName != null);

    return _longName;
  }

  /**
   * @author Bill Darbie
   */
  public void setShortName(String shortName)
  {
    Assert.expect(shortName != null);

    Assert.expect(_panel != null);
    Assert.expect(_panel.isCompPackageNameValid(shortName));
    // short name can be duplicate
    //Assert.expect(_panel.isCompPackageNameDuplicate(shortName) == false);

    if (shortName.equals(_shortName))
      return;

    String oldValue = _shortName;
    _projectObservable.setEnabled(false);
    try
    {
      _shortName = shortName.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, shortName, CompPackageEventEnum.SHORT_NAME);
  }

  /**
   * @author Bill Darbie
   */
  public String getShortName()
  {
    Assert.expect(_shortName != null);
    return _shortName;
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getShortName();
  }

  /**
   * @author Bill Darbie
   */
  public synchronized void addComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    Assert.expect(_componentTypeSet != null);

    _projectObservable.setEnabled(false);
    try
    {
      boolean added = _componentTypeSet.add(componentType);
      Assert.expect(added, "Package: " + getLongName() + " component type: " + componentType.getReferenceDesignator());
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, componentType, CompPackageEventEnum.ADD_OR_REMOVE_COMPONENT_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public synchronized void removeComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    Assert.expect(_componentTypeSet != null);

    _projectObservable.setEnabled(false);
    try
    {
      boolean removed = _componentTypeSet.remove(componentType);
      Assert.expect(removed, "Package: " + getLongName() + " component type: " + componentType.getReferenceDesignator());
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, componentType, null, CompPackageEventEnum.ADD_OR_REMOVE_COMPONENT_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public synchronized boolean hasComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    return _componentTypeSet.contains(componentType);
  }

  /**
   * @author Bill Darbie
   */
  public synchronized List<ComponentType> getComponentTypes()
  {
    Assert.expect(_componentTypeSet != null);

    List<ComponentType> componentTypes = new ArrayList<ComponentType>(_componentTypeSet);
    Collections.sort(componentTypes, _alphaNumericComparator);
    return componentTypes;
  }

  /**
   * @author Bill Darbie
   */
  public synchronized void addPackagePin(PackagePin packagePin)
  {
    Assert.expect(packagePin != null);

    _projectObservable.setEnabled(false);
    try
    {
      boolean added = _packagePinSet.add(packagePin);
      Assert.expect(added);
      invalidateUsesOneJointTypeEnum();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, packagePin, CompPackageEventEnum.ADD_OR_REMOVE_PACKAGE_PIN);
  }

  /**
   * @author Bill Darbie
   */
  public synchronized void removePackagePin(PackagePin packagePin)
  {
    Assert.expect(packagePin != null);
    Assert.expect(_packagePinSet != null);

    _projectObservable.setEnabled(false);
    try
    {
      boolean removed = _packagePinSet.remove(packagePin);
      Assert.expect(removed);
      invalidateUsesOneJointTypeEnum();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }

    _projectObservable.stateChanged(this, packagePin, null, CompPackageEventEnum.ADD_OR_REMOVE_PACKAGE_PIN);
  }

  /**
   * Remove the PackagePin that is associated with the passed in LandPatternPad
   * @author Bill Darbie
   */
  synchronized void removePackagePin(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    boolean packagePinRemoved = false;
    for (PackagePin packagePin : getPackagePinsUnsorted())
    {
      if (landPatternPad == packagePin.getLandPatternPad())
      {
        removePackagePin(packagePin);
        packagePinRemoved = true;
        break;
      }
    }
    Assert.expect(packagePinRemoved);
  }

  /**
   * @author Bill Darbie
   */
  public synchronized List<PackagePin> getPackagePins()
  {
    Assert.expect(_packagePinSet !=  null);

    List<PackagePin> packagePins = new ArrayList<PackagePin>(_packagePinSet);
    if(packagePins.size() == 2)      
      Collections.sort(packagePins, _padNameComparator);
    else      
      Collections.sort(packagePins, _alphaNumericComparator);
    return packagePins;
  }

  /**
   * @author Bill Darbie
   */
  public synchronized List<PackagePin> getPackagePinsUnsorted()
  {
    Assert.expect(_packagePinSet !=  null);

    List<PackagePin> packagePins = new ArrayList<PackagePin>(_packagePinSet);
    return packagePins;
  }

  /**
   * @author Bill Darbie
   */
  synchronized void invalidateUsesOneJointTypeEnum()
  {
    _usesOneJointTypeEnum = null;
    invalidateShouldClassifiesAtComponentOrMeasurementGroupLevel(); //Siew Yeng - XCR-3094
  }

  /**
   * @author Laura Cormos
   */
  synchronized void invalidateUsesOneJointHeight()
  {
    _usesOneJointHeight = null;
  }

  /**
   * @author Andy Mechtenberg
   */
  public synchronized boolean usesOneJointTypeEnum()
  {
    if (_usesOneJointTypeEnum != null)
      return _usesOneJointTypeEnum.getValue();

    Set<JointTypeEnum> jointTypeEnumSet = new HashSet<JointTypeEnum>();
    for(PackagePin packagePin : getPackagePinsUnsorted())
    {
      JointTypeEnum jointTypeEnum = packagePin.getJointTypeEnum();
      jointTypeEnumSet.add(jointTypeEnum);
      if (jointTypeEnumSet.size() > 1)
      {
        _usesOneJointTypeEnum = new BooleanRef(false);
        return false;
      }
    }

    _usesOneJointTypeEnum = new BooleanRef(true);
    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized boolean usesOneJointHeight()
  {
    if (_usesOneJointHeight != null)
      return _usesOneJointHeight.getValue();

    Set<Integer> jointHeightsSet = new HashSet<Integer>();
    for (PackagePin packagePin : getPackagePinsUnsorted())
    {
      int jointHeightInNanometers = packagePin.getJointHeightInNanoMeters();
      jointHeightsSet.add(jointHeightInNanometers);
      if (jointHeightsSet.size() > 1)
      {
        _usesOneJointHeight = new BooleanRef(false);
        return false;
      }
    }

    _usesOneJointHeight = new BooleanRef(true);
    return true;
  }


  /**
   * Gets the joint type (only works if there is ONE joint type for everything.
   * @author Andy Mechtenberg
   **/
  public synchronized JointTypeEnum getJointTypeEnum()
  {
    Assert.expect(usesOneJointTypeEnum());

    List<PackagePin> packagePins = getPackagePinsUnsorted();
    Assert.expect((packagePins.isEmpty() == false), " Package : " + getLongName());

    PackagePin packagePin = packagePins.get(0);
    return packagePin.getJointTypeEnum();
  }

  /**
   * Gets the joint height (only works if there is ONE joint height for everything.
   * @author Erica Wheatcroft
   **/
  public synchronized int getJointHeightInNanoMeters()
  {
    Assert.expect(usesOneJointHeight());

    List<PackagePin> packagePins = getPackagePinsUnsorted();
    Assert.expect((packagePins.isEmpty() == false), " Package : " + getLongName());

    PackagePin packagePin = packagePins.get(0);
    return packagePin.getJointHeightInNanoMeters();
  }

  /**
   * @author Bill Darbie
   */
  public void setLandPattern(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);

    if (landPattern == _landPattern)
      return;

    LandPattern oldValue = _landPattern;
    _projectObservable.setEnabled(false);
    try
    {
      _landPattern = landPattern;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, landPattern, CompPackageEventEnum.LAND_PATTERN);
  }

  /**
   * @author Bill Darbie
   */
  public LandPattern getLandPattern()
  {
    Assert.expect(_landPattern != null);

    return _landPattern;
  }

  /**
   * Set the passed in PackagePin of this CompPackage to the JointTypeEnum passed in.
   *
   * If no CompPackage already exists with the same LandPattern and JointTypeEnums
   * then this method will modify the PackagePins of the existing CompPackage
   * to the new JointTypeEnum.  A new subtype will be set for all PadTypes
   * of all ComponentTypes that use this CompPackage.
   *
   * If a CompPackage already exists that matches this LandPattern and JointTypeEnums
   * then the matching CompPackage will be removed and all ComponentTypes will be set to
   * the this CompPackage.  The subtypes of the ComponentTypes that were using the old
   * CompPackage will be set to new subtypes.

   * @author Bill Darbie
   */
  public synchronized void setJointTypeEnum(JointTypeEnum jointTypeEnum, PackagePin packagePin)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(packagePin != null);

    Pair<PackagePin, JointTypeEnum> oldValue = new Pair<PackagePin,JointTypeEnum>(packagePin, packagePin.getJointTypeEnum());
    _projectObservable.setEnabled(false);
    try
    {
      LandPattern landPattern = getLandPattern();
      List<JointTypeEnum> sortedJointTypeEnums = new ArrayList<JointTypeEnum>();
      for (PackagePin aPackagePin : getPackagePins())
      {
        if (aPackagePin == packagePin)
          sortedJointTypeEnums.add(jointTypeEnum);
        else
          sortedJointTypeEnums.add(aPackagePin.getJointTypeEnum());
      }

      CompPackage matchingCompPackage = _panel.getCompPackageIfItMatches(landPattern, sortedJointTypeEnums);

      if (matchingCompPackage != null)
      {
        // remove the matching one and set this one to be the one used by all ComponentTypes of the old one
        for (ComponentType componentType : matchingCompPackage.getComponentTypes())
        {
          matchingCompPackage.removeComponentType(componentType);
          addComponentType(componentType);
          // make sure the component realizes it has a new package
          componentType.setCompPackage(this);

          Iterator<JointTypeEnum> jointTypeEnumIt = sortedJointTypeEnums.iterator();
          for (PadType padType : componentType.getPadTypes())
          {
            JointTypeEnum aJointTypeEnum = jointTypeEnumIt.next();

            // if this PadType is associated with the PackagePin being changed, then make the appropriate subtype and jointTypeEnum change
            if (padType.getPackagePin() == packagePin)
            {
              Subtype subtype = _panel.getSubtypeCreatingIfNecessary(landPattern, aJointTypeEnum);
              PadTypeSettings padTypeSettings = padType.getPadTypeSettings();
              PackagePin aPackagePin = padType.getPackagePin();

              padTypeSettings.setSubtype(subtype);
              aPackagePin.setJointTypeEnum(aJointTypeEnum);
            }
          }
        }
        matchingCompPackage.destroy();

        // now modify this CompPackage
        Iterator<JointTypeEnum> jointTypeEnumIt = sortedJointTypeEnums.iterator();
        for (PackagePin aPackagePin : getPackagePins())
        {
          JointTypeEnum aJointTypeEnum = jointTypeEnumIt.next();

          // if the curent packagePin matches the PackagePin being changed, then make the appropriate jointTypeEnum change
          if (aPackagePin == packagePin)
          {
            aPackagePin.setJointTypeEnum(aJointTypeEnum);
          }
        }

        assignPinOrientationCorrectionsOverridingPreviousUserSettings(packagePin);
      }
      else
      {
        // assign a new subtype to the PadTypes of all ComponentTypes used by this CompPackage
        Iterator<JointTypeEnum> jointTypeEnumIt = sortedJointTypeEnums.iterator();
        for (ComponentType componentType : getComponentTypes())
        {
          jointTypeEnumIt = sortedJointTypeEnums.iterator();
          for (PadType padType : componentType.getPadTypes())
          {
            JointTypeEnum aJointTypeEnum = jointTypeEnumIt.next();

            // if this PadType is associated with the PackagePin being changed, then make the appropriate subtype and jointTypeEnum change
            if (padType.getPackagePin() == packagePin)
            {
              Subtype subtype = _panel.getSubtypeCreatingIfNecessary(landPattern, aJointTypeEnum);

              PadTypeSettings padTypeSettings = padType.getPadTypeSettings();
              PackagePin aPackagePin = padType.getPackagePin();
              aPackagePin.setJointTypeEnum(aJointTypeEnum);
              padTypeSettings.setSubtype(subtype);
            }
          }
        }

        assignPinOrientationCorrectionsOverridingPreviousUserSettings(packagePin);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    Pair<PackagePin, JointTypeEnum> newValue = new Pair<PackagePin, JointTypeEnum>(packagePin, jointTypeEnum);
    _projectObservable.stateChanged(this, oldValue, newValue, CompPackageEventEnum.JOINT_TYPE_ENUM);
  }

  /**
   * Set all PackagePins of this CompPackage to the JointTypeEnum passed in.
   *
   * If no CompPackage already exists with the same LandPattern and JointTypeEnums
   * then this method will modify the PackagePins of the existing CompPackage
   * to the new JointTypeEnum.  A new subtype will be set for all PadTypes
   * of all ComponentTypes that use this CompPackage.
   *
   * If a CompPackage already exists that matches this LandPattern and JointTypeEnums
   * then the matching CompPackage will be removed and all ComponentTypes will be set to
   * the this CompPackage.  The subtypes of the ComponentTypes that were using the old
   * CompPackage will be set to new subtypes.
   *
   * @author Bill Darbie
   */
  public synchronized void setJointTypeEnum(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    List<JointTypeEnum> oldValue = new ArrayList<JointTypeEnum>();
    for (PackagePin packagePin : getPackagePins())
      oldValue.add(packagePin.getJointTypeEnum());

    _projectObservable.setEnabled(false);
    try
    {
      LandPattern landPattern = getLandPattern();
      List<JointTypeEnum> jointTypeEnums = new ArrayList<JointTypeEnum>();
      int numPins = _packagePinSet.size();
      for (int i = 0; i < numPins; ++i)
        jointTypeEnums.add(jointTypeEnum);

      CompPackage matchingCompPackage = _panel.getCompPackageIfItMatches(landPattern, jointTypeEnums);

      if (matchingCompPackage != null)
      {
        if (matchingCompPackage != this)
        {
          Subtype subtype = _panel.getSubtypeCreatingIfNecessary(landPattern, jointTypeEnum);

          // go through this CompPackage and set its ComponentTypes to the correct Subtype and JointTypeEnum values
          for (ComponentType componentType : getComponentTypes())
          {
            for (PadType padType : componentType.getPadTypes())
            {
              padType.getPadTypeSettings().setSubtype(subtype);
              padType.getPackagePin().setJointTypeEnum(jointTypeEnum);
            }
          }

          // remove the matching CompPackage and set this CompPackage to be the one used by all ComponentTypes of the old one
          // go through all matching ComponentTypes and set their Subtype and JointTypeEnum to the correct values
          for (ComponentType matchingComponentType : matchingCompPackage.getComponentTypes())
          {
            // remove all the ComponentTypes that used to use the matching CompPackage from it
            matchingCompPackage.removeComponentType(matchingComponentType);
            // change the matching ComponentTypes to use this CompPackage
            matchingComponentType.setCompPackage(this);
            addComponentType(matchingComponentType);

            Iterator<PackagePin> packagePinIt = getPackagePinsUnsorted().iterator();
            for (PadType matchingPadType : matchingComponentType.getPadTypes())
            {
              // set the PackagePins of the CompPackage to the new JointTypeEnum
              PackagePin packagePin = packagePinIt.next();
              packagePin.setJointTypeEnum(jointTypeEnum);

              matchingPadType.setPackagePin(packagePin);
              PadTypeSettings matchingPadTypeSettings = matchingPadType.getPadTypeSettings();
              matchingPadTypeSettings.setSubtype(subtype);
            }
          }

          String longName = matchingCompPackage.getLongName();
          String shortName = matchingCompPackage.getShortName();

          matchingCompPackage.destroy();

          setLongName(longName);
          setShortName(shortName);

          assignPinOrientationCorrectionsOverridingPreviousUserSettings();
        }
      }
      else
      {
        // no exising CompPackage matches what we need so modify this one
        for (PackagePin packagePin : getPackagePinsUnsorted())
          packagePin.setJointTypeEnum(jointTypeEnum);

        Subtype subtype = _panel.getSubtypeCreatingIfNecessary(landPattern, jointTypeEnum);

        // assign a new subtype to the PadTypes of this ComponentType
        for (ComponentType componentType : getComponentTypes())
        {
          for (PadType padType : componentType.getPadTypes())
          {
            PadTypeSettings padTypeSettings = padType.getPadTypeSettings();
            PackagePin packagePin = padType.getPackagePin();
            packagePin.setJointTypeEnum(jointTypeEnum);
            padTypeSettings.setSubtype(subtype);
          }
        }

        assignPinOrientationCorrectionsOverridingPreviousUserSettings();
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, jointTypeEnum, CompPackageEventEnum.JOINT_TYPE_ENUM);
  }

  /**
   * @author Bill Darbie
   */
  public synchronized void assignPinOrientationCorrectionsOverridingPreviousUserSettings()
  {
    for (PackagePin packagePin : getPackagePinsUnsorted())
    {
      assignPinOrientationCorrections(packagePin, true);
    }
  }

  /**
   * @author Bill Darbie
   */
  public synchronized void assignPinOrientationCorrectionsWithoutOverridingPreviousUserSettings()
  {
    for (PackagePin packagePin : getPackagePinsUnsorted())
    {
      assignPinOrientationCorrections(packagePin, false);
    }
  }

  /**
   * @author Bill Darbie
   */
  private void assignPinOrientationCorrectionsOverridingPreviousUserSettings(PackagePin packagePin)
  {
    Assert.expect(packagePin != null);
    assignPinOrientationCorrections(packagePin, true);
  }

  /**
   * @author Bill Darbie
   */
  private void assignPinOrientationCorrectionsWithoutOverridingPreviousUserSettings(PackagePin packagePin)
  {
    Assert.expect(packagePin != null);
    assignPinOrientationCorrections(packagePin, false);
  }

  /**
   * @author Bill Darbie
   */
  private void assignPinOrientationCorrections(PackagePin packagePin, boolean overridePreviousUserSettings)
  {
    Assert.expect(packagePin != null);

    if ((packagePin.getJointTypeEnum().equals(JointTypeEnum.COLLAPSABLE_BGA)) ||
        (packagePin.getJointTypeEnum().equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR)) ||
        (packagePin.getJointTypeEnum().equals(JointTypeEnum.NON_COLLAPSABLE_BGA)) ||
        (packagePin.getJointTypeEnum().equals(JointTypeEnum.CGA)) ||
        (packagePin.getJointTypeEnum().equals(JointTypeEnum.PRESSFIT)) ||
        (packagePin.getJointTypeEnum().equals(JointTypeEnum.CHIP_SCALE_PACKAGE)) ||
        (packagePin.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE)) ||
        (packagePin.getJointTypeEnum().equals(JointTypeEnum.OVAL_THROUGH_HOLE)) || //Siew Yeng - XCR-3318 - Oval PTH
        (packagePin.getJointTypeEnum().equals(JointTypeEnum.SINGLE_PAD)) ||
        (packagePin.getJointTypeEnum().equals(JointTypeEnum.LGA)))
    {
      packagePin.setPinOrientationEnum(PinOrientationEnum.NOT_APPLICABLE);
    }
    else if ((packagePin.getJointTypeEnum().equals(JointTypeEnum.CAPACITOR)) ||
             (packagePin.getJointTypeEnum().equals(JointTypeEnum.GULLWING)) ||
             (packagePin.getJointTypeEnum().equals(JointTypeEnum.JLEAD)) ||
             (packagePin.getJointTypeEnum().equals(JointTypeEnum.LEADLESS)) ||
             (packagePin.getJointTypeEnum().equals(JointTypeEnum.POLARIZED_CAP)) ||
             (packagePin.getJointTypeEnum().equals(JointTypeEnum.RESISTOR)) ||
             (packagePin.getJointTypeEnum().equals(JointTypeEnum.RF_SHIELD)) ||
             (packagePin.getJointTypeEnum().equals(JointTypeEnum.SURFACE_MOUNT_CONNECTOR)) ||
             (packagePin.getJointTypeEnum().equals(JointTypeEnum.SMALL_OUTLINE_LEADLESS)) ||
             (packagePin.getJointTypeEnum().equals(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD)) ||
             (packagePin.getJointTypeEnum().equals(JointTypeEnum.EXPOSED_PAD))
             // Wei Chin (Tall Cap)
//             || (packagePin.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
             )

    {
      if (overridePreviousUserSettings)
        packagePin.clearPinOrientationEnum();
    }
    else
    {
      Assert.expect(false);
    }
  }

  /**
   * Retrieves a master list of Subtypes for every ComponentType belonging to this CompPackage.
   *
   * @author Matt Wharton
   */
  public Collection<Subtype> getSubtypes()
  {
    Assert.expect(_componentTypeSet != null);

    Collection<Subtype> subtypes = new HashSet<Subtype>();
    for (ComponentType componentType : _componentTypeSet)
    {
      subtypes.addAll(componentType.getSubtypes());
    }

    return subtypes;
  }

  /**
   * @author Wei Chin, Chong
   */
  private void generateSubTypeSchemas()
  {
    if(_subtypeSchemes == null)
      _subtypeSchemes = new HashSet<SubtypeScheme>();

    _subtypeSchemes.clear();

    List<ComponentType> componentTypes = new ArrayList<ComponentType>(_componentTypeSet);
    Collections.sort(componentTypes, _alphaNumericComparator);

    for(ComponentType componentType: componentTypes)
    {
      SubtypeScheme subtypeScheme = new SubtypeScheme(componentType);
       if(_subtypeSchemes.contains(subtypeScheme) == false)
         _subtypeSchemes.add(subtypeScheme);
       else
       {
         for (SubtypeScheme matchSubtypeScheme : _subtypeSchemes)
         {
           // XCR-3112 Assert when edit land pattern and edit subtype package during import
           if(matchSubtypeScheme.hashCode() == subtypeScheme.hashCode() &&
              matchSubtypeScheme.getCompPackage().getLongName().equalsIgnoreCase(subtypeScheme.getCompPackage().getLongName()))
           {
             matchSubtypeScheme.addComponentType(componentType);
             matchSubtypeScheme.compareInspectionMap(subtypeScheme.getPadNameToInspectableMap());
             matchSubtypeScheme.compareTestableMap(subtypeScheme.getPadNameToTestableMap());
             break;
           }
         }
         subtypeScheme = null;
       }
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  public List<SubtypeScheme> getSubtypeSchemes()
  {
    if(_subtypeSchemes == null)
      _subtypeSchemes = new HashSet<SubtypeScheme>();

    if(_subtypeSchemes.isEmpty())
      generateSubTypeSchemas();

    List<SubtypeScheme> subtypeSchemes = new ArrayList<SubtypeScheme>(_subtypeSchemes);
    Collections.sort(subtypeSchemes, _alphaNumericComparator);

    return subtypeSchemes;
  }

  /**
   * @param libraryPath String
   * @author Wei Chin, Chong
   */
  public void setLibraryPath(String libraryPath)
  {
    Assert.expect(libraryPath != null);

    _libraryPath = libraryPath;

    if (hasSubtypeSchemes())
    {
      for(SubtypeScheme subtypeScheme : _subtypeSchemes)
      {
        subtypeScheme.setLibraryPath(_libraryPath);
      }
    }
  }

  /**
   * @return String
   * @author Wei Chin, Chong
   */
  public String getLibraryPath()
  {
    Assert.expect(_libraryPath != null);
    return _libraryPath;
  }

  /**
   * @return String
   * @author Wei Chin, Chong
   */
  public String getSourceLibrary()
  {
    Assert.expect(_sourceLibraryPath != null);

    return _sourceLibraryPath;
  }

  /**
   * set the source library after read from compPackage file.
   * @param sourceLibrary String
   * @author Wei Chin, Chong
   */
  public void setSourceLibrary(String sourceLibrary)
  {
    Assert.expect(sourceLibrary != null);
    _sourceLibraryPath = sourceLibrary;
  }

  /**
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean hasSourceLibrary()
  {
    return (_sourceLibraryPath != null && _sourceLibraryPath.length() > 0);
  }

  /**
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean hasLibraryPath()
  {
    return (_libraryPath != null && _libraryPath.length() > 0);
  }

  /**
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean hasSubtypeSchemes()
  {
    return (_subtypeSchemes != null && _subtypeSchemes.isEmpty() == false);
  }

  /**
   * @param subtypeSchemes List
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean containSubtypeSchemes(List<SubtypeScheme> subtypeSchemes)
  {
    Assert.expect(_subtypeSchemes != null);
    Assert.expect(subtypeSchemes != null);

    for(SubtypeScheme subtypeScheme: _subtypeSchemes)
    {
      if(subtypeSchemes.contains(subtypeScheme))
        return true;
    }
    return false;
  }

  /**
   * Save the Exported Information to easy the
   * user to choose the unexported data or modified data for the coming export event
   * @author Wei Chin, Chong
   */
  public void saveExportState()
  {
    Assert.expect(hasLibraryPath());

    _projectObservable.setEnabled(false);
    try
    {
      setLibraryCheckSum(getCheckSum());
      setLibraryLandpatternName(getLandPattern().getName());
      setSourceLibrary(getLibraryPath());
      saveSubtypeThresholdsCheckSum();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, CompPackageEventEnum.EXPORT_COMPLETE_ENUM);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void saveImportState()
  {
    setLibraryCheckSum(getCheckSum());
    saveSubtypeThresholdsCheckSum();
  }

  /**
   * This method sets map of library path to a list of library subtype scheme.
   *
   * @param libraryPathToLibrarySubtypeSchemesMap Map
   *
   * @author Cheah Lee Herng
   */
  public void setLibraryPathToLibrarySubtypeSchemesMap(Map<String, List<LibrarySubtypeScheme>> libraryPathToLibrarySubtypeSchemesMap)
  {
    Assert.expect(libraryPathToLibrarySubtypeSchemesMap != null);
    _libraryPathToLibrarySubtypeSchemesMap = libraryPathToLibrarySubtypeSchemesMap;
  }

  /**
   * This method gets map of library path to a list of library subtype scheme.
   *
   * @return Map
   *
   * @author Cheah Lee Herng
   */
  public Map<String, List<LibrarySubtypeScheme>> getLibraryPathToLibrarySubtypeSchemesMap()
  {
    Assert.expect(_libraryPathToLibrarySubtypeSchemesMap != null);
    return _libraryPathToLibrarySubtypeSchemesMap;
  }

  /**
   * This return consolidate Joint Type string based on jointtype name for this package.
   * This is use to check against the library package joint type string.
   * This will speed up thing as we do not need to loop thru all the pin
   * for each joint type as there is whole lot of data in the library
   *
   * @author Poh Kheng
   */
  public String getJointTypeNames()
  {
    Assert.expect(_packagePinSet !=  null);
    String jointTypeNames = "";

    List<PackagePin> packagePins = new ArrayList<PackagePin>(_packagePinSet);
    Collections.sort(packagePins, _alphaNumericComparator);

    for(PackagePin packagePin : packagePins) {
      jointTypeNames = jointTypeNames + packagePin.getJointTypeEnum().getName() + ",";
    }

    Assert.expect(jointTypeNames != null);

    return jointTypeNames;
  }

  /**
   * This will add in library subtype scheme in a list
   *
   * @author Poh Kheng
   */
  public void addLibrarySubtypeScheme(LibrarySubtypeScheme librarySubtypeScheme)
  {
    Assert.expect(librarySubtypeScheme !=  null);
    _librarySubtypeSchemes.add(librarySubtypeScheme);
  }


  /**
   * This will add in a mapping of library path to a list of library package.
   * The library path will be more than one if user choose more than one library
   *
   * @author Poh Kheng
   */
  public void addLibraryPathToLibraryPackageMap(String libraryPath, LibraryPackage libraryPackage)
  {
    Assert.expect(libraryPath != null);
    Assert.expect(libraryPackage != null);

    if (_libraryPathToLibraryPackagesMap == null)
      _libraryPathToLibraryPackagesMap = new HashMap<String,List<LibraryPackage>>();

    if (_libraryPathToLibraryPackagesMap.containsKey(libraryPath))
    {
      _libraryPathToLibraryPackagesMap.get(libraryPath).add(libraryPackage);
    }
    else
    {
      List<LibraryPackage> libraryPackages = new LinkedList<LibraryPackage>();
      libraryPackages.add(libraryPackage);
      _libraryPathToLibraryPackagesMap.put(libraryPath, libraryPackages);
    }
  }

  /**
   * This will return a mapping of library path to a list of library package.
   *
   * @author Poh Kheng
   */
  public Map<String, List<LibraryPackage>> getLibraryPathToLibraryPackagesMap()
  {
    if (_libraryPathToLibraryPackagesMap == null)
      _libraryPathToLibraryPackagesMap = new HashMap<String,List<LibraryPackage>>();

    return _libraryPathToLibraryPackagesMap;
  }

  /**
   * @return boolean
   *
   * @author Wei Chin, Chong
   */
  public boolean isModifiedAfterImported()
  {
    Assert.expect(hasLibraryCheckSum());

    return _libraryCheckSum != getCheckSum();
  }

  /**
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean hasSubtypeModified()
  {
    if (hasPreviousSubtypeThresholdsCheckSum() == false)
      return true;
    else
      return getPreviousSubtypeThresholdsCheckSum() != getCurrentSubtypeThresholdCheckSum();
  }

  /**
   * This would check if the library path to library package is empty
   *
   * @author Poh Kheng
   */
  public boolean isLibraryPathToLibraryPackageMapIsEmpty()
  {
    if (_libraryPathToLibraryPackagesMap == null)
      _libraryPathToLibraryPackagesMap = new HashMap<String,List<LibraryPackage>>();

    return _libraryPathToLibraryPackagesMap.isEmpty();
  }

  /**
   * @author Wei Chin, Chong
   */
  public void clearUnusedTransientObjects()
  {
    if(_subtypeSchemes != null)
      _subtypeSchemes.clear();

    if (_libraryPathToLibraryPackagesMap != null)
      _libraryPathToLibraryPackagesMap.clear();

    if (_libraryPathToLibrarySubtypeSchemesMap != null)
      _libraryPathToLibrarySubtypeSchemesMap.clear();

    if (_librarySubtypeSchemes != null)
      _librarySubtypeSchemes.clear();

    _libraryPath = null;
    _subtypeSchemes = null;
    _libraryPathToLibraryPackagesMap = null;
    _libraryPathToLibrarySubtypeSchemesMap = null;
    _librarySubtypeSchemes = null;

    resetCheckSum();
    for(ComponentType componentType : _componentTypeSet)
    {
      componentType.clearAssignLibrarySubtypeScheme();
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  private void generateCheckSum()
  {
    Assert.expect( _landPattern != null);

    StringBuilder checkSumString = new StringBuilder();
//    checkSumString.append(_landPattern.getName());

    CRC32 checksumUtil = new CRC32();
    for( LandPatternPad landPatternPad : _landPattern.getLandPatternPads())
    {
      checkSumString.append(landPatternPad.getCoordinateInNanoMeters().getX());
      checkSumString.append(landPatternPad.getCoordinateInNanoMeters().getY());
      checkSumString.append(landPatternPad.getLengthInNanoMeters());
      checkSumString.append(landPatternPad.getWidthInNanoMeters());
      checkSumString.append(landPatternPad.getDegreesRotation());
      checkSumString.append(landPatternPad.getShapeEnum().getName());
      checkSumString.append(landPatternPad.isSurfaceMountPad());
      checkSumString.append(landPatternPad.isThroughHolePad());
      checkSumString.append(landPatternPad.isPadOne());
    }

    //to make sure all the pin are sorted
    Set<PackagePin> packagePinSet = new TreeSet<PackagePin>(new PackagePinComparator(true, PackagePinComparatorEnum.PIN_NAME));
    packagePinSet.addAll(_packagePinSet);

    for(PackagePin packagePin : packagePinSet)
    {
      checkSumString.append(packagePin.getJointTypeEnum().getName());
      checkSumString.append(packagePin.getPinOrientationEnum().getName());
    }

    checksumUtil.reset();
    checksumUtil.update(checkSumString.toString().getBytes());
    _checkSum = checksumUtil.getValue();
  }

  /**
   * @return long
   * @author Wei Chin, Chong
   */
  public long getCheckSum()
  {
    if(_checkSum <= 0)
      generateCheckSum();

    return _checkSum;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void resetCheckSum()
  {
    _checkSum = -1;
  }

  /**
   * @param libraryCheckSum long
   * @author Wei Chin, Chong
   */
  public void setLibraryCheckSum(long libraryCheckSum)
  {
    _libraryCheckSum = libraryCheckSum;
  }

  /**
   * @return long
   * @author Wei Chin, Chong
   */
  public long getLibraryCheckSum()
  {
    return _libraryCheckSum;
  }

  /**
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean hasLibraryCheckSum()
  {
    return _libraryCheckSum > 0;
  }

  /**
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean isOverwriteToLibrary()
  {
    return _overwriteToLibrary;
  }

  /**
   * @param overwriteToLibrary
   * @author Wei Chin, Chong
   */
  public void setOverwriteToLibrary(boolean overwriteToLibrary)
  {
    _overwriteToLibrary = overwriteToLibrary;
  }

  /**
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean hasLibraryLandPatternName()
  {
    return _libraryLandpatternName != null && _libraryLandpatternName.length() > 0;
  }
  /**
   * @return String _libraryLandpatternName
   * @author Wei Chin, Chong
   */
  public String getLibraryLandpatternName()
  {
    Assert.expect(_libraryLandpatternName != null);
    return _libraryLandpatternName;
  }

  /**
   * @param libraryLandpatternName the _libraryLandpatternName to set
   * @author Wei Chin, Chong
   */
  public void setLibraryLandpatternName(String libraryLandpatternName)
  {
    Assert.expect(libraryLandpatternName != null);
    _libraryLandpatternName = libraryLandpatternName;
  }

  /**
   * @author Wei Chin, Chong
   */
  private void generateSubtypeThresholdCheckSum()
  {
    StringBuilder checkSumString = new StringBuilder();
    CRC32 checksumUtil = new CRC32();
    checksumUtil.reset();
    Collection <Subtype> subypeCollections = getSubtypes();

    Assert.expect(subypeCollections.size() > 0);

    List<Subtype>  subypes = new ArrayList<Subtype>(subypeCollections);
    Collections.sort(subypes, new SubtypeJointTypeOrNameComparator(false,true));
    for(Subtype subtype : subypes)
    {
      checkSumString.append(subtype.getShortName());
      List<AlgorithmSettingEnum> algorithmSettings = subtype.getTunedAlgorithmSettingEnums();
      Collections.sort(algorithmSettings);
      for(AlgorithmSettingEnum algorithmSetting : algorithmSettings)
      {
        Serializable value = subtype.getAlgorithmSettingValue(algorithmSetting);
        checkSumString.append(algorithmSetting.getName() + value);
      }
    }
    checksumUtil.update(checkSumString.toString().getBytes());
    _currentSubtypeThresholdsCheckSum = checksumUtil.getValue();
    subypes.clear();
    subypeCollections.clear();
    subypes= null;
    subypeCollections = null;
    checksumUtil = null;
  }

  /**
   * @return long
   * @author Wei Chin, Chong
   */
  public long getCurrentSubtypeThresholdCheckSum()
  {
    generateSubtypeThresholdCheckSum();
    return _currentSubtypeThresholdsCheckSum;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void saveSubtypeThresholdsCheckSum()
  {
    generateSubtypeThresholdCheckSum();
    setPreviousSubtypeThresholdsCheckSum(_currentSubtypeThresholdsCheckSum);
  }

  /**
   * @return boolean _imported
   * @author Wei Chin, Chong
   */
  public boolean isImported()
  {
    return _imported;
  }

  /**
   * @param imported the _imported to set
   * @author Wei Chin, Chong
   */
  public void setImported(boolean imported)
  {
    _imported = imported;
  }

  /**
   * @return boolean
   * @author Cheah Lee Herng
   */
  public boolean hasPreviousSubtypeThresholdsCheckSum()
  {
    return (_previousSubtypeThresholdsCheckSum != 0);
  }

  /**
   * @return the _previousSubtypeThresholdsCheckSum
   * @author Wei Chin, Chong
   */
  public long getPreviousSubtypeThresholdsCheckSum()
  {
    Assert.expect(_previousSubtypeThresholdsCheckSum > 0);
    return _previousSubtypeThresholdsCheckSum;
  }

  /**
   * @param previousSubtypeThresholdsCheckSum the _previousSubtypeThresholdsCheckSum to set
   * @author Wei Chin, Chong
   */
  public void setPreviousSubtypeThresholdsCheckSum(long previousSubtypeThresholdsCheckSum)
  {
    Assert.expect(previousSubtypeThresholdsCheckSum > 0);
    _previousSubtypeThresholdsCheckSum = previousSubtypeThresholdsCheckSum;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void backup()
  {
    _backupSourceLibraryPath = _sourceLibraryPath;
    _backupImportedFlag = _imported;
    _backupPreviousSubtypeThresholdsCheckSum = _previousSubtypeThresholdsCheckSum;
    setBackupStatus(true);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void rollback()
  {
    _sourceLibraryPath = _backupSourceLibraryPath;
    _imported = _backupImportedFlag;
    _previousSubtypeThresholdsCheckSum = _backupPreviousSubtypeThresholdsCheckSum;
    setBackupStatus(false);
  }

  /**
   * @return boolean
   * @author Cheah Lee Herng
   */
  public boolean isBackup()
  {
    return _isBackup;
  }

  /**
   * @param status boolean
   * @author Cheah Lee Herng
   */
  public void setBackupStatus(boolean status)
  {
    _isBackup = status;
  }
  
  /**
   * @author Lim Seng Yew
   * XCR1650 - Open recipe and immediately save recipe, it will crash
   */
  public boolean hasLandPattern()
  {
    return (_landPattern != null);
  }
  
  /**
   * @author Siew Yeng   
   */
  public synchronized boolean shouldClassifyAtComponentOrMeasurementGroupLevel()
  {
    if(_shouldClassifiesAtComponentOrMeasurementGroupLevel != null)
      return _shouldClassifiesAtComponentOrMeasurementGroupLevel.getValue();
    
    for(PackagePin packagePin : _packagePinSet)
    {
      if(InspectionFamily.getInstance(packagePin.getJointTypeEnum()).classifiesAtComponentOrMeasurementGroupLevel())
      {
        _shouldClassifiesAtComponentOrMeasurementGroupLevel = new BooleanRef(true);
        return true;
      }
    }
    
    _shouldClassifiesAtComponentOrMeasurementGroupLevel = new BooleanRef(false);
    return false;
  }
  
  /**
   * @author Siew Yeng
   */
  synchronized void invalidateShouldClassifiesAtComponentOrMeasurementGroupLevel()
  { 
    _shouldClassifiesAtComponentOrMeasurementGroupLevel = null;
  }
}
