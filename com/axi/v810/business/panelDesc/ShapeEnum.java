package com.axi.v810.business.panelDesc;

import java.io.*;

import com.axi.util.*;
import java.util.Map;
import java.util.HashMap;
import com.axi.v810.business.imageAnalysis.AlgorithmSettingEnum;

/**
 * An enumeration to define the supported shapes for features on the 5dx.
 * Currently, supports circles and rectangles.
 * @author Bill Darbie
 */
public class ShapeEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;
  private static Map<String, ShapeEnum> _nameToShapeEnummMap = new HashMap<String, ShapeEnum>();
  public final static ShapeEnum CIRCLE = new ShapeEnum(++_index, "Circle");
  public final static ShapeEnum RECTANGLE = new ShapeEnum(++_index, "Rectangle");

  private String _name;

  /**
   * @author Poh Kheng
   */
  private ShapeEnum(int id, String name)
  {
    super(id);
    _name = name.intern();
    _nameToShapeEnummMap.put(_name, this);

  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public String getName()
  {
    return _name;
  }

  /**
   * This would return the shape enum based on the enum name
   *
   * @author Poh Kheng
   */
  public static ShapeEnum getShapeEnum(String shapeName)
  {
    final ShapeEnum shapeEnum = _nameToShapeEnummMap.get(shapeName);
    Assert.expect(shapeEnum != null);
    return shapeEnum;
  }

}
