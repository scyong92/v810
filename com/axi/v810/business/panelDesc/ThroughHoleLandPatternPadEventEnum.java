package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class ThroughHoleLandPatternPadEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static ThroughHoleLandPatternPadEventEnum CREATE_OR_DESTROY = new ThroughHoleLandPatternPadEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static ThroughHoleLandPatternPadEventEnum HOLE_COORDINATE = new ThroughHoleLandPatternPadEventEnum(++_index);
  public static ThroughHoleLandPatternPadEventEnum HOLE_DIAMETER = new ThroughHoleLandPatternPadEventEnum(++_index);//Siew Yeng - XCR-3318 - not using anymore
  public static ThroughHoleLandPatternPadEventEnum HOLE_WIDTH = new ThroughHoleLandPatternPadEventEnum(++_index);
  public static ThroughHoleLandPatternPadEventEnum HOLE_LENGTH = new ThroughHoleLandPatternPadEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private ThroughHoleLandPatternPadEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private ThroughHoleLandPatternPadEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
