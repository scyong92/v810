package com.axi.v810.business.panelDesc;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.business.panelSettings.*;
import java.util.*;

/**
 * @author Andy Mechtenberg
 */
class Test_ComponentType extends UnitTest
{
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ComponentType());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    ComponentType componentType = new ComponentType();

    // test setReferenceDesignator() and getReferenceDesignator()
    try
    {
      componentType.getReferenceDesignator();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    try
    {
      componentType.setReferenceDesignator(null);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    String refDes = "U1";
    Panel panel = new Panel();
    panel.disableChecksForNdfParsers();
    componentType.setPanel(panel);
    componentType.setReferenceDesignator(refDes);
    Expect.expect(componentType.getReferenceDesignator().equals(refDes));

    // test setPartNumber() and getPartNumber()
    try
    {
      componentType.getPartNumber();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    try
    {
      componentType.setPartNumber(null);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    String partNum = "partNumber";
    componentType.setPartNumber(partNum);
    Expect.expect(componentType.getPartNumber().equals(partNum));

    // test setPartLocationInFactory() and getPartLocationInFactory()
    try
    {
      componentType.getPartLocationInFactory();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    try
    {
      componentType.setPartLocationInFactory(null);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    String partLoc = "PartLocation";
    componentType.setPartLocationInFactory(partLoc);
    Expect.expect(componentType.getPartLocationInFactory().equals(partLoc));

    // test setCoordinateInNanoMeters() and getCoordinateInNanoMeters()
    try
    {
      componentType.getCoordinateInNanoMeters();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    try
    {
      componentType.setCoordinateInNanoMeters(null);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    BoardCoordinate coord = new BoardCoordinate(10, 20);
    componentType.setCoordinateInNanoMeters(coord);
    Expect.expect(componentType.getCoordinateInNanoMeters().equals(coord));

    // test setDegreesRotationRelativeToBoard() and getDegreesRotationRelativeToBoard()
    try
    {
      componentType.getDegreesRotationRelativeToBoard();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    int degrees = 90;
    componentType.setDegreesRotationRelativeToBoard(degrees);
    Expect.expect(componentType.getDegreesRotationRelativeToBoard() == degrees);

    // test setSideBoard() and getSideBoard()
    try
    {
      componentType.getSideBoardType();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    try
    {
      componentType.setSideBoardType(null);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    SideBoardType sideBoardType = new SideBoardType();
    componentType.setSideBoardType(sideBoardType);
    Expect.expect(componentType.getSideBoardType() == sideBoardType);

    // test setCompPackage() and getCompPackage()
    try
    {
      componentType.getCompPackage();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    try
    {
      componentType.setCompPackage(null);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    CompPackage compPackage = new CompPackage();
    componentType.setCompPackage(compPackage);
    Expect.expect(componentType.getCompPackage() == compPackage);

    // test setLandPattern() getLandPattern()
    try
    {
      componentType.getLandPattern();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    try
    {
      componentType.setLandPattern(null);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    LandPattern landPattern = new LandPattern();
    componentType.setLandPattern(landPattern);
    Expect.expect(componentType.getLandPattern() == landPattern);

    // getShapeInNanometers is tested in Test_Panel.java because it needs a fully loaded panel
    // transformShapeBasedOncomponentTypeAndBoardAndPanelRotationInNanoMeters is tested in Test_Panel.java because it needs a fully loaded panel

    testWithProjectLoaded();
// wpd virgo
//    // now test the copy constructor
//    // do this at the end because by now every little function has been tested successfully.
//    componentType = new ComponentType();
//    String name = "U1";
//    String partNumber = "E100-100";
//    String partLocation = "Warehouse";
//    PanelCoordinate coordinate = new PanelCoordinate(100, 300);
//    int degreesRotation = 50;
//    int legacyId = 5;
//    sideBoard = new SideBoard();
//    compPackage = new CompPackage();
//
//    pad = new Pad();
//    padName = "padName";
//    LandPatternPad landPatternPad = new SurfaceMountLandPatternPad();
//    landPatternPad.setName(padName);
//    pad.setLandPatternPad(landPatternPad);
//    padSettings = new PadSettings();
//    pad.setPadSettings(padSettings);
//    padSettings.setPad(pad);
//
//    landPattern = new LandPattern();
//    componentTypeSettings = new componentTypeSettings();
//
//    componentType.setReferenceDesignator(name);
//    componentType.setPartNumber(partNumber);
//    componentType.setPartLocationInFactory(partLocation);
//    componentType.setCoordinateInNanoMeters(coordinate);
//    componentType.setDegreesRotationRelativeToBoard(degreesRotation);
//    componentType.setSideBoard(sideBoard);
//    componentType.setCompPackage(compPackage);
//    componentType.addPad(pad);
//    componentType.setLandPattern(landPattern);
//    componentType.setcomponentTypeSettings(componentTypeSettings);
//
//    componentType copy = new componentType(componentType);
//    // the copy does a complex copy of the pin/pad/joint stuff, the rest is a simple copy
//    // let's verify the simple stuff first, then the complicated stuff
//    Expect.expect(copy.getReferenceDesignator() == name);
//    Expect.expect(copy.getPartNumber() == partNumber);
//    Expect.expect(copy.getPartLocationInFactory() == partLocation);
//    Expect.expect(copy.getCoordinateInNanoMeters().getX() == coordinate.getX());
//    Expect.expect(copy.getCoordinateInNanoMeters().getY() == coordinate.getY());
//    Expect.expect(copy.getCoordinateInNanoMeters() != coordinate);
//    Expect.expect(copy.getDegreesRotationRelativeToBoard() == degreesRotation);
//    Expect.expect(copy.getLegacyId() != componentType.getLegacyId());
//    Expect.expect(copy.getSideBoard() == sideBoard);
//    Expect.expect(copy.getCompPackage() == compPackage);
//    Expect.expect(copy.getLandPattern() == landPattern);
//    // componentType settings is a deep copy, so I'm checking here if a deep copy did happen
//    // If a shallow copy was happening, then this will fail
//    Expect.expect(componentType.getcomponentTypeSettings() != copy.getcomponentTypeSettings());
//
//    // ok, now we check pin/pad/joint copies
//    // all of these copies were deep copies, so we need to make sure no shallow ones happened.
//    Expect.expect(copy.getPads().size() == 1);
//    Expect.expect(copy.getPad(padName) != pad);
//
//    Expect.expect(copy.getPad(padName).getPadSettings() != padSettings);
//    Expect.expect(copy.getPad(padName).getPadSettings() != padSettings);
  }

  /**
   * @author Bill Darbie
   */
  private void testWithProjectLoaded()
  {
    Project project = new Project(false);

    try
    {
      // test JointTypeEnum
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();
      project = Project.importProjectFromNdfs("smallPanel", warnings);
      ComponentType componentType = project.getPanel().getComponentTypes().get(0);
      CompPackage compPackage = componentType.getCompPackage();
      PackagePin packagePin = compPackage.getPackagePins().get(0);
      JointTypeEnum origJointTypeEnum = packagePin.getJointTypeEnum();
      Assert.expect(origJointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) == false);
      Assert.expect(packagePin.getJointTypeEnum().equals(JointTypeEnum.COLLAPSABLE_BGA) == false);

      // first call setJointTypeEnum with the same JointTypeEnum as this ComponentType is currently using
      Assert.expect(componentType.getCompPackage().getPackagePins().get(0).getJointTypeEnum().equals(JointTypeEnum.CAPACITOR));
      componentType.setJointTypeEnum(JointTypeEnum.CAPACITOR);
      Assert.expect(componentType.getCompPackage().getPackagePins().get(0).getJointTypeEnum().equals(JointTypeEnum.CAPACITOR));

// wpd - if more checks are added this is handy information to print out
//      for (ComponentType aComponentType : project.getPanel().getComponentTypes())
//      {
//        System.out.println("wpd refDes: " + aComponentType.getReferenceDesignator());
//        System.out.println("  wpd boardname: " + aComponentType.getSideBoardType().getBoardType().getName());
//        System.out.println("  wpd: " + aComponentType.getLandPattern().getName());
//        for (PackagePin aPackagePin : aComponentType.getCompPackage().getPackagePins())
//          System.out.println("    wpd: " + aPackagePin.getJointTypeEnum());
//      }

      // now call setJointTypeEnum with a new JointTypeEnum
      CompPackage origCompPackage = componentType.getCompPackage();
      componentType.setJointTypeEnum(JointTypeEnum.COLLAPSABLE_BGA);
      compPackage = componentType.getCompPackage();
      packagePin = compPackage.getPackagePins().get(0);
      Assert.expect(packagePin.getJointTypeEnum().equals(JointTypeEnum.COLLAPSABLE_BGA));
      Assert.expect(origCompPackage != compPackage);

      // now call setJointTypeEnum on a component that will use an existing CompPackage
      // G1 and C8_2 both use LandPattern 1206
      // G1 is MiscDiscrete JointTypeEnum
      // C8_2 is PolarizedCap JointTypeEnum
      ComponentType comp1 = project.getPanel().getComponentType("boardType2", "G1");
      ComponentType comp2 = project.getPanel().getComponentType("boardType2", "C8_2");
      Assert.expect(comp1.getCompPackage().getPackagePins().get(0).getJointTypeEnum().equals(JointTypeEnum.LEADLESS));
      Assert.expect(comp2.getCompPackage().getPackagePins().get(0).getJointTypeEnum().equals(JointTypeEnum.POLARIZED_CAP));
      comp1.setJointTypeEnum(JointTypeEnum.POLARIZED_CAP);
      Assert.expect(comp1.getCompPackage().getPackagePins().get(0).getJointTypeEnum().equals(JointTypeEnum.POLARIZED_CAP));
      Assert.expect(comp2.getCompPackage().getPackagePins().get(0).getJointTypeEnum().equals(JointTypeEnum.POLARIZED_CAP));
      Assert.expect(comp1.getCompPackage() == comp2.getCompPackage());


      // test changeSide
      Component comp = componentType.getComponents().get(0);
      Expect.expect(comp.isTopSide());
      componentType.changeSide();
      Expect.expect(comp.isTopSide() == false);
      componentType.changeSide();
      Expect.expect(comp.isTopSide());
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}

