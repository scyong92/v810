package com.axi.v810.business.panelDesc;

import java.io.*;

import com.axi.util.*;
import java.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Bill Darbie
 * @author Andy Mechtenberg
 */
public class Test_CompPackage extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_CompPackage());
  }

  /**
   * @author Bill Darbie
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    basicTests();
    testWithProjectLoaded();
    testPinOrientationAssignment();
  }

  /**
   * @author Bill Darbie
   */

  private void basicTests()
  {
    CompPackage compPackage = new CompPackage();

    // test getName() and setName()
    try
    {
      compPackage.getLongName();
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    try
    {
      compPackage.setLongName(null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    String name = "name";
    Panel panel = new Panel();
    panel.disableChecksForNdfParsers();
    compPackage.setPanel(panel);
    compPackage.setLongName(name);
    Expect.expect(compPackage.getLongName() == name);

  }

  /**
   * @author Bill Darbie
   */
  private void testPinOrientationAssignment()
  {
    Set<String> refDesSet = new HashSet<String>();

    // components that we want to see information on
    refDesSet.add("U1731_C"); // SINGLE_PAD
    refDesSet.add("U59"); // CGA
    refDesSet.add("U8"); // BGA, pad one in center is false
    refDesSet.add("U4"); // JLEAD, also has a padOneInCenter
    refDesSet.add("C2"); // POLARIZED_CAPACITOR
    refDesSet.add("C1"); // CAPACITOR
    refDesSet.add("R1"); // RESISTOR
    refDesSet.add("J2"); // CONNECTOR
    refDesSet.add("CR1"); // GULLWING - also has 3 subtypes on 4 pads, and has 3 pitches for 4 pads
    refDesSet.add("FB1"); // MISC_DISCRETE
    refDesSet.add("CR6"); // GULLWING - 4 pin

    try
    {
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();
      Project project = Project.importProjectFromNdfs("cygnusAllFamilies", warnings);

      Set<CompPackage> compPackageSet = new HashSet<CompPackage>();
      Panel panel = project.getPanel();
      for (ComponentType componentType : panel.getComponentTypes())
      {
        String refDes = componentType.getReferenceDesignator();

        if (refDesSet.contains(refDes) == false)
          continue;

        System.out.println("component: " + refDes);

        CompPackage compPackage = componentType.getCompPackage();
        System.out.println("package: " + compPackage.getLongName());

        // print out CompPackage info
        if (compPackageSet.add(compPackage))
        {
          for (PackagePin packagePin : compPackage.getPackagePins())
          {
            System.out.println("    packagePin: " + packagePin.getName());
            System.out.println("      jointType: " + packagePin.getJointTypeEnum());
            System.out.println("      jointType: " + packagePin.getPinOrientationEnum());
          }
        }
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void testWithProjectLoaded()
  {
    Project project = new Project(false);

    try
    {
      // test JointTypeEnum
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();
      project = Project.importProjectFromNdfs("smallPanel", warnings);

      ComponentType componentType = project.getPanel().getComponentTypes().get(0);
      CompPackage compPackage = componentType.getCompPackage();

      PackagePin packagePin = compPackage.getPackagePins().get(0);
      JointTypeEnum origJointTypeEnum = packagePin.getJointTypeEnum();
      Assert.expect(origJointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) == false);
      Assert.expect(packagePin.getJointTypeEnum().equals(JointTypeEnum.COLLAPSABLE_BGA) == false);

      // first call setJointTypeEnum with the same JointTypeEnum as this ComponentType is currently using
      Assert.expect(compPackage.getPackagePins().get(0).getJointTypeEnum().equals(JointTypeEnum.CAPACITOR));
      compPackage.setJointTypeEnum(JointTypeEnum.CAPACITOR);
      Assert.expect(compPackage.getPackagePins().get(0).getJointTypeEnum().equals(JointTypeEnum.CAPACITOR));

// wpd - if more checks are added this is handy information to print out
//      for (ComponentType aComponentType : project.getPanel().getComponentTypes())
//      {
//        System.out.println("wpd refDes: " + aComponentType.getReferenceDesignator());
//        System.out.println("  wpd boardname: " + aComponentType.getSideBoardType().getBoardType().getName());
//        System.out.println("  wpd: " + aComponentType.getLandPattern().getName());
//        for (PackagePin aPackagePin : aComponentType.getCompPackage().getPackagePins())
//          System.out.println("    wpd: " + aPackagePin.getJointTypeEnum());
//      }

      // now call setJointTypeEnum with a new JointTypeEnum
      CompPackage origCompPackage = componentType.getCompPackage();
      compPackage.setJointTypeEnum(JointTypeEnum.COLLAPSABLE_BGA);
      packagePin = compPackage.getPackagePins().get(0);
      Assert.expect(packagePin.getJointTypeEnum().equals(JointTypeEnum.COLLAPSABLE_BGA));
      Assert.expect(origCompPackage == compPackage);

      // now call setJointTypeEnum on a component that will use an existing CompPackage
      // G1 and C8_2 both use LandPattern 1206
      // G1 is MiscDiscrete JointTypeEnum
      // C8_2 is PolarizedCap JointTypeEnum
      ComponentType comp1 = project.getPanel().getComponentType("boardType2", "G1");
      ComponentType comp2 = project.getPanel().getComponentType("boardType2", "C8_2");
      CompPackage compPack1 = comp1.getCompPackage();
      CompPackage compPack2 = comp2.getCompPackage();
      Assert.expect(compPack1.getPackagePins().get(0).getJointTypeEnum().equals(JointTypeEnum.LEADLESS));
      Assert.expect(compPack2.getPackagePins().get(0).getJointTypeEnum().equals(JointTypeEnum.POLARIZED_CAP));
      compPack1.setJointTypeEnum(JointTypeEnum.POLARIZED_CAP);
      Assert.expect(compPack1.getPackagePins().get(0).getJointTypeEnum().equals(JointTypeEnum.POLARIZED_CAP));
      Assert.expect(comp1.getCompPackage() == comp2.getCompPackage());


      // test changeSide
      Component comp = componentType.getComponents().get(0);
      Expect.expect(comp.isTopSide());
      componentType.changeSide();
      Expect.expect(comp.isTopSide() == false);
      componentType.changeSide();
      Expect.expect(comp.isTopSide());
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
