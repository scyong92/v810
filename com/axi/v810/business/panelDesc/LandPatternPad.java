package com.axi.v810.business.panelDesc;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.util.*;

/**
 * Each LandPattern instance has its own List of LandPatternPad instances.  Two
 * LandPatterns do NOT share a LandPatternPad.
 *
 * Many PackagePin instances can have a reference to the same LandPatternPad instance.
 * Many Pad instances will share a reference to the same LandPatternPad instance.
 *
 * @author Bill Darbie
 */
public abstract class LandPatternPad implements Serializable
{
  // constructed by SurfaceMountLandPatternNdfReader/ThroughHoleLandPatternNdfReader, LandPatternReader

  private LandPattern _landPattern; // SurfaceMountLandPatternNdfReader/ThroughHoleLandPatternNdfReader, LandPatternReader

  private String _name; // SurfaceMountLandPatternNdfReader/ThroughHoleLandPatternNdfReader/PinsNdfReader, LandPatternReader
  // relative to Components reference point that this LandPatternPad is associated with
  // the Components reference point may not be the Components center
  private ComponentCoordinate _coordinateInNanoMeters; // SurfaceMountLandPatternNdfReader/ThroughHoleLandPatternNdfReader, LandPatternReader
  private int _widthInNanoMeters; // SurfaceMountLandPatternNdfReader/ThroughHoleLandPatternNdfReader, LandPatternReader
  private int _lengthInNanoMeters; // SurfaceMountLandPatternNdfReader/ThroughHoleLandPatternNdfReader, LandPatternReader
  // circular or rectangular
  private ShapeEnum _shapeEnum; // SurfaceMountLandPatternNdfReader/ThroughHoleLandPatternNdfReader, LandPatternReader

  // degreesRotation is the amount the pad is rotated about its center
  private DoubleRef _degreesRotation; // SurfaceMountLandPatternNdfReader/ThroughHoleLandPatternNdfReader, LandPatternReader
  private double _origDegreesRotation;
  private int _pitchInNanoMeters = -1; // calculated
  private int _interPadDistanceInNanoMeters = -1; // calculated
  // pad orientation is the edge of the pad (at 0 degrees rotation)
  // that is closest to where the pin
  // begins exiting the component it is attached to
  // Note that the LandPatternPad contains the default setting for orientation which is based
  // only on Pad location.  In reality the orientation depends on the package,
  // but we do not have package information in CAD.  If the user changes
  // this orientation, it will get changed in the PackagePin class and NOT
  // here
  private PinOrientationEnum _pinOrientationEnum; // calculated
  private BooleanRef _isPadOne; // calculated
  private boolean _invalidateShapeEnabled = true;

  private transient java.awt.Shape _shape;
  private transient List<PadType> _removedPadTypes;

  private static transient ProjectObservable _projectObservable;


  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public LandPatternPad()
  {
    _projectObservable.stateChanged(this, null, this, LandPatternPadEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      for (ComponentType componentType : getLandPattern().getComponentTypesUnsorted())
        componentType.invalidateShapeForPadChangeOnly();

      // update LandPattern so it gets rid of this pad
      Assert.expect(_landPattern !=null);

      _landPattern.invalidatePadOne();
      _landPattern.removeLandPatternPad(this);

      // remove PackagePin and update CompPackage
      // remove Pads, PadType and update Component and ComponentType
      Set<CompPackage> compPackageSet = new HashSet<CompPackage>();
      for (ComponentType componentType : _landPattern.getComponentTypesUnsorted())
      {
        CompPackage compPackage = componentType.getCompPackage();
        // remove the PackagePin associated with the landPatternPad from CompPackage
        // only remove the landPatternPad from the same CompPackage once
        if (compPackageSet.add(compPackage))
          compPackage.removePackagePin(this);

        for (PadType padType : componentType.getPadTypes())
        {
          if (this  == padType.getLandPatternPad())
          {
            // remove this PadType from ComponentType and all Pads associated with it from Component
            componentType.removePadType(padType);
            for (Pad pad : padType.getPads())
            {
              pad.getComponent().removePad(pad);
            }
          }
        }
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, LandPatternPadEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Andy Mechtenberg
   * @author Ying-Huan.Chu
   */
  public void remove()
  {
    _projectObservable.setEnabled(false);
    try
    {
      for (ComponentType componentType : getLandPattern().getComponentTypesUnsorted())
        componentType.invalidateShapeForPadChangeOnly();

      // update LandPattern so it gets rid of this pad
      Assert.expect(_landPattern != null);
      
      if (_landPattern.isPadOneUserAssigned())
        _landPattern.invalidatePadOne();
      _landPattern.removeLandPatternPad(this);

      // remove PackagePin and update CompPackage
      // remove Pads, PadType and update Component and ComponentType
      if (_removedPadTypes == null)
        _removedPadTypes = new ArrayList<PadType>();
      else
        _removedPadTypes.clear();
      Set<CompPackage> compPackageSet = new HashSet<CompPackage>();
      for (ComponentType componentType : _landPattern.getComponentTypesUnsorted())
      {
        CompPackage compPackage = componentType.getCompPackage();
        // remove the PackagePin associated with the landPatternPad from CompPackage
        // only remove the landPatternPad from the same CompPackage once
        if (compPackageSet.add(compPackage))
          compPackage.removePackagePin(this);

        for (PadType padType : componentType.getPadTypes())
        {
          if (this == padType.getLandPatternPad())
          {
            padType.getSubtype().removeFromPadType(padType);
            _removedPadTypes.add(padType);
            // remove this PadType from ComponentType and all Pads associated with it from Component
            componentType.removePadType(padType);
            for (Pad pad : padType.getPads())
            {
              pad.getComponent().removePad(pad);
            }
          }
        }
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, LandPatternPadEventEnum.ADD_OR_REMOVE);
    _landPattern.assignPadOneIfNecessary();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void add()
  {
    _projectObservable.setEnabled(false);
    try
    {
      for (ComponentType componentType : getLandPattern().getComponentTypesUnsorted())
        componentType.invalidateShapeForPadChangeOnly();

      // update LandPattern so it gets rid of this pad
      Assert.expect(_landPattern != null);

      _landPattern.invalidatePadOne();
      _landPattern.addLandPatternPad(this);
      
      Set<CompPackage> compPackageSet = new HashSet<CompPackage>();
      for(PadType padType : _removedPadTypes)
      {
        ComponentType componentType = padType.getComponentType();
        componentType.addPadType(padType);
        CompPackage compPackage = componentType.getCompPackage();
        // remove the PackagePin associated with the landPatternPad from CompPackage
        // only remove the landPatternPad from the same CompPackage once
        if (compPackageSet.add(compPackage))
          compPackage.addPackagePin(padType.getPackagePin());
 
        //Jack Hwee - XCR1699 crash when undo landpattern delete
        padType.getSubtype().assignedToPadType(padType);

        for (Pad pad : padType.getPads())
        {
          pad.getComponent().addPad(pad);
        }
        _landPattern.assignPadOneIfNecessary();
      }
      _removedPadTypes.clear();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, this, LandPatternPadEventEnum.ADD_OR_REMOVE);
  }


  /**
   * @author Bill Darbie
   */
  public void setLandPattern(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);

    if (landPattern == _landPattern)
      return;

    LandPattern oldValue = _landPattern;
    _projectObservable.setEnabled(false);
    try
    {
      _landPattern = landPattern;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, landPattern, LandPatternPadEventEnum.LAND_PATTERN);
  }

  /**
   * @author Bill Darbie
   */
  public LandPattern getLandPattern()
  {
    Assert.expect(_landPattern != null);

    return _landPattern;
  }

  /**
   * @author Bill Darbie
   */
  public void setName(String name)
  {
    Assert.expect(name != null);
    Assert.expect(_landPattern != null);
    Assert.expect(_landPattern.isLandPatternPadNameValid(name));
    Assert.expect(_landPattern.isLandPatternPadNameDuplicate(name) == false);

    if (name.equals(_name))
      return;

    String oldValue = _name;
    _projectObservable.setEnabled(false);
    try
    {
      _landPattern.setNewLandPatternPadName(this, _name, name);
      _name = name.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, name, LandPatternPadEventEnum.NAME);
  }

  /**
   * @author Bill Darbie
   */
  public String getName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getName();
  }

  /**
   * Set the x,y coordinate in nanometers of the LandPatternPad center relative to the reference
   * point of the component it gets associated with.
   * The reference point is not necessarily the center of the component, but is the
   * point from which the pad/pin coordinates are referenced.
   *
   * @author Keith Lee
   */
  public void setCoordinateInNanoMeters(ComponentCoordinate coordinateInNanoMeters)
  {
    Assert.expect(coordinateInNanoMeters != null);

    if (coordinateInNanoMeters.equals(_coordinateInNanoMeters))
      return;

    ComponentCoordinate oldValue = _coordinateInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _coordinateInNanoMeters = coordinateInNanoMeters;

      Assert.expect(_landPattern != null);
      _landPattern.invalidatePitchAndInterPadDistance();
      _landPattern.invalidateNumRowsAndColumnsAndPadOneInCenter();
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, coordinateInNanoMeters, LandPatternPadEventEnum.COORDINATE);
  }

  /**
   * @return the coordinate of this LandPatternPad relative to the reference
   * point of the component it gets associated with.
   * The reference point is not necessarily the center of the component, but is the
   * point from which the pad/pin coordinates are referenced.
   *
   * @author Bill Darbie
   */
  public ComponentCoordinate getCoordinateInNanoMeters()
  {
    Assert.expect(_coordinateInNanoMeters != null);
    return new ComponentCoordinate(_coordinateInNanoMeters);
  }

  /**
   * @author George A. David
   */
  public ComponentCoordinate getCenterCoordinateInNanoMeters()
  {
    // this is the same as the coordinate, i added this function by mistake,
    // then I kept it in for clarity.
    return getCoordinateInNanoMeters();
  }

  /**
   * Set the width of the LandPatternPad at 0 degrees rotation.
   * @author Keith Lee
   */
  public void setWidthInNanoMeters(int widthInNanoMeters)
  {
    Assert.expect(widthInNanoMeters > 0);

    if (widthInNanoMeters == _widthInNanoMeters)
      return;

    int oldValue = _widthInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _widthInNanoMeters = widthInNanoMeters;

      Assert.expect(_landPattern != null);
      _landPattern.invalidatePitchAndInterPadDistance();
      _landPattern.invalidatePadSize();
      invalidateShape();
      invalidateJointHeight();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, widthInNanoMeters, LandPatternPadEventEnum.WIDTH);
  }

  /**
   * @return the x axis width of the LandPatternPad BEFORE any rotation has been applied.
   * @author Keith Lee
   */
  public int getWidthInNanoMeters()
  {
    Assert.expect(_widthInNanoMeters > 0);

    return _widthInNanoMeters;
  }

  /**
   * Set the length of the LandPatternPad at 0 degrees rotation.
   * @author Keith Lee
   */
  public void setLengthInNanoMeters(int lengthInNanoMeters)
  {
    Assert.expect(lengthInNanoMeters > 0);

    if (lengthInNanoMeters == _lengthInNanoMeters)
      return;

    int oldValue = _lengthInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _lengthInNanoMeters = lengthInNanoMeters;

      Assert.expect(_landPattern != null);
      _landPattern.invalidatePitchAndInterPadDistance();
      _landPattern.invalidatePadSize();
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, lengthInNanoMeters, LandPatternPadEventEnum.LENGTH);
  }

  /**
   * @return the y axis Length of the LandPatternPad BEFORE any rotation has been applied.
   * @author Keith Lee
   */
  public int getLengthInNanoMeters()
  {
    Assert.expect(_lengthInNanoMeters > 0);

    return _lengthInNanoMeters;
  }

  /**
   * @author Keith Lee
   */
  public void setShapeEnum(ShapeEnum shapeEnum)
  {
    Assert.expect(shapeEnum != null);

    if (shapeEnum.equals(_shapeEnum))
      return;

    ShapeEnum oldValue = _shapeEnum;
    _projectObservable.setEnabled(false);
    try
    {
      _shapeEnum = shapeEnum;
      _landPattern.invalidatePitchAndInterPadDistance();
      _landPattern.invalidatePadSize();
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, shapeEnum, LandPatternPadEventEnum.SHAPE_ENUM);
  }

  /**
   * @author Keith Lee
   */
  public ShapeEnum getShapeEnum()
  {
    Assert.expect(_shapeEnum != null);
    return _shapeEnum;
  }

  /**
   * @return true if this component is circular and has an equal width and length setting.
   * @author Bill Darbie
   */
  public boolean isPerfectCircle()
  {
    ShapeEnum shape = getShapeEnum();
    if ((shape.equals(ShapeEnum.CIRCLE) && (_widthInNanoMeters == _lengthInNanoMeters)))
      return true;

    return false;
  }

  /**
   * @author Keith Lee
   */
  public void setDegreesRotationWithoutAffectingPinOrientation(double degreesRotation)
  {
    degreesRotation = MathUtil.getDegreesWithin0To359(degreesRotation);

    if ((_degreesRotation != null) && (degreesRotation == _degreesRotation.getValue()))
      return;

    Double oldValue = null;
    _projectObservable.setEnabled(false);
    try
    {
      if (_degreesRotation == null)
        _degreesRotation = new DoubleRef(degreesRotation);
      else
      {
        oldValue = _degreesRotation.getValue();
        _degreesRotation.setValue(degreesRotation);
        _origDegreesRotation = degreesRotation;
        invalidateShape();
      }

      Assert.expect(_landPattern != null);
      _landPattern.invalidatePitchAndInterPadDistance();
      _landPattern.invalidatePadSize();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, degreesRotation, LandPatternPadEventEnum.DEGREES_ROTATION);
  }

  /**
   * @author Bill Darbie
   */
  public void setDegreesRotation(double degreesRotation)
  {
    degreesRotation = MathUtil.getDegreesWithin0To359(degreesRotation);

    double rotationDelta = MathUtil.getDegreesWithin0To359(_origDegreesRotation - degreesRotation);
    double origDegreesRotation = _origDegreesRotation;
    setDegreesRotationWithoutAffectingPinOrientation(degreesRotation);
    _origDegreesRotation = origDegreesRotation;

    if (_pinOrientationEnum != null)
    {
      if ((rotationDelta >= 90) && (rotationDelta < 270))
      {
        // flip the LandPatternPad orientation
        _origDegreesRotation = degreesRotation;
        _pinOrientationEnum = _pinOrientationEnum.getPinOrientationEnum(180 + _pinOrientationEnum.getDegrees());
        invalidateShape();

        // if a PackagePin has an overridden PinOrientationEnum, flip it too
        Set<CompPackage> compPackageSet = new HashSet<CompPackage>();
        for (ComponentType compType : getLandPattern().getComponentTypesUnsorted())
        {
          CompPackage compPackage = compType.getCompPackage();
          if (compPackageSet.add(compPackage))
          {
            for (PackagePin packagePin : compType.getCompPackage().getPackagePins())
            {
              if (packagePin.getLandPatternPad() == this)
              {
                if (packagePin.hasPinOrientationEnumBeenOverridden())
                {
                  packagePin.setPinOrientationEnum(PinOrientationEnum.getPinOrientationEnum(180 + packagePin.getPinOrientationEnum().getDegrees()));
                }
                break;
              }
            }
          }
        }
      }
    }
  }

  /**
   * @author Keith Lee
   */
  public double getDegreesRotation()
  {
    Assert.expect(_degreesRotation != null);

    return _degreesRotation.getValue();
  }

  /**
   * This should only be called from LandPattern.assignPitchAndInterPadDistanceToLandPatternPads().
   * @author Bill Darbie
   */
  void setPitchInNanoMeters(int pitchInNanoMeters)
  {
    Assert.expect(pitchInNanoMeters >= 0);

    if (pitchInNanoMeters == _pitchInNanoMeters)
      return;

    int oldValue = _pitchInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _pitchInNanoMeters = pitchInNanoMeters;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, pitchInNanoMeters, LandPatternPadEventEnum.PITCH);
  }

  /**
   * @author Bill Darbie
   */
  public int getPitchInNanoMeters()
  {
    Assert.expect(_landPattern != null);

    _landPattern.assignPitchAndInterPadDistanceToLandPatternPadsIfNecessary();

    Assert.expect(_pitchInNanoMeters >= 0);
    return _pitchInNanoMeters;
  }

  /**
   * Set the distance from this Pads edge to it's nearest neighbor.
   * This should only be called from LandPattern.assignPitchAndInterPadDistanceToLandPatternPads().
   * @author Bill Darbie
   */
  void setInterPadDistanceInNanoMeters(int interPadDistance)
  {
    Assert.expect(interPadDistance >= 0);

    if (interPadDistance == _interPadDistanceInNanoMeters)
      return;

    int oldValue = _interPadDistanceInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _interPadDistanceInNanoMeters = interPadDistance;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, interPadDistance, LandPatternPadEventEnum.INTERPAD_DISTANCE);
  }

  /**
   * @return the distance from this Pads edge to it's nearest neighbor
   * @author Bill Darbie
   */
  public int getInterPadDistanceInNanoMeters()
  {
    Assert.expect(_landPattern != null);

    _landPattern.assignPitchAndInterPadDistanceToLandPatternPadsIfNecessary();

    Assert.expect(_interPadDistanceInNanoMeters >= 0);
    return _interPadDistanceInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getLongestPadDimensionInNanoMeters()
  {
    Rectangle2D rect = getShapeInNanoMeters().getBounds2D();

    return (int)Math.max(rect.getWidth(), rect.getHeight());
  }

  /**
   * @author George A. David
   */
  public int getAlgorithmLimitedInterPadDistanceInNanoMeters()
  {
    return  (int)Math.rint(Math.min(getInterPadDistanceInNanoMeters(), ImageAnalysis.MAX_INTERPAD_DISTANCE_IN_NANOMETERS_FOR_ALGORITHMS));
  }

  /**
   * This call should only be made from LandPattern.  This is NOT set by the user.
   * Use PackagePin.setOrientationEnum() for user settings of the orientation.
   * @author Bill Darbie
   */
  void setPinOrientationEnum(PinOrientationEnum pinOrientationEnum)
  {
    Assert.expect(pinOrientationEnum != null);

    if (pinOrientationEnum.equals(_pinOrientationEnum))
      return;

    PinOrientationEnum oldValue = _pinOrientationEnum;
    _projectObservable.setEnabled(false);
    try
    {
      _pinOrientationEnum = pinOrientationEnum;

      double degrees = getDegreesRotation();
      if ((degrees >= 90.0) && (degrees < 270.0))
      {
        // if the pad is rotated then we need to flip the pin orientation
        _pinOrientationEnum = PinOrientationEnum.getPinOrientationEnum(180 + _pinOrientationEnum.getDegrees());
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, pinOrientationEnum, LandPatternPadEventEnum.PAD_ORIENTATION);
  }

  /**
   * This call should only be made from PackagePin.  Use PackagePin.getOrientationEnum()
   * to get orientation of the pin.
   * @return the edge of the pad (at 0 degrees rotation) that is closest
   * to the location where the pin is attached to the component.
   * @author Bill Darbie
   */
  PinOrientationEnum getPinOrientationEnum()
  {
    if (_pinOrientationEnum == null)
    {
      Assert.expect(_landPattern != null);
      _landPattern.assignPinOrientations();
    }

    Assert.expect(_pinOrientationEnum != null);
    return _pinOrientationEnum;
  }

  /**
   * This call should only be made from PackagePin.  Use PackagePin.getOrientationAfterRotationEnum()
   * to get orientation of the pin.
   * @author Bill Darbie
   */
  PinOrientationAfterRotationEnum getPinOrientationAfterRotationEnum()
  {
    PinOrientationEnum pinOrientationEnum = getPinOrientationEnum();

    return PinOrientationAfterRotationEnum.getPinOrientationAfterRotationEnum(pinOrientationEnum, getDegreesRotation());
  }

  /**
   * @author Bill Darbie
   */
  public boolean isPinOrientationEnumAssigned()
  {
    if (_pinOrientationEnum == null)
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  void setPadOneFromAutoAssign(boolean isPadOne)
  {
    setPadOnePrivate(isPadOne);
  }

  /**
   * @author Bill Darbie
   */
  public void setPadOne(boolean isPadOne)
  {
    setPadOnePrivate(isPadOne);
    getLandPattern().setUserAssignedPadOne(this);
  }

  /**
   * @author Bill Darbie
   */
  private void setPadOnePrivate(boolean isPadOne)
  {
    if ((_isPadOne != null) && (isPadOne == _isPadOne.getValue()))
      return;

    Boolean oldValue = null;
    _projectObservable.setEnabled(false);
    try
    {
      if (_isPadOne == null)
        _isPadOne = new BooleanRef(isPadOne);
      else
      {
        oldValue = _isPadOne.getValue();
        _isPadOne.setValue(isPadOne);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, isPadOne, LandPatternPadEventEnum.PAD_ONE);
  }

  /**
   * @return true if this pin is pin one and false otherwise
   * @author Bill Darbie
   */
  public boolean isPadOne()
  {
    if (_isPadOne == null)
    {
      Assert.expect(_landPattern != null);
      _landPattern.assignPadOneIfNecessary();
    }

    Assert.expect(_isPadOne != null);
    return _isPadOne.getValue();
  }

  /**
   * @author Bill Darbie
   */
  void invalidatePadOne()
  {
    BooleanRef oldValue = _isPadOne;
    _projectObservable.setEnabled(false);
    try
    {
      _isPadOne = null;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _isPadOne, LandPatternPadEventEnum.PAD_ONE);
  }

  /**
   * @return true if the pad is throughole, false otherwise
   * @author Andy Mechtenberg
   */
  public abstract boolean isThroughHolePad();

  /**
   * @return true if the pad is throughole, false otherwise
   * @author Bill Darbie
   */
  public abstract boolean isSurfaceMountPad();

  /**
   * @author Bill Darbie
   */
  LandPatternPad createDuplicate(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);

    LandPatternPad landPatternPad = null;
    if (isSurfaceMountPad())
      landPatternPad = new SurfaceMountLandPatternPad();
    else if (isThroughHolePad())
      landPatternPad = new ThroughHoleLandPatternPad();
    else
      Assert.expect(false);

    landPatternPad.setLandPattern(landPattern);
    landPatternPad.setCoordinateInNanoMeters(getCoordinateInNanoMeters());
    landPatternPad.setDegreesRotation(getDegreesRotation());
    landPatternPad.setWidthInNanoMeters(getWidthInNanoMeters());
    landPatternPad.setLengthInNanoMeters(getLengthInNanoMeters());
    landPatternPad.setPinOrientationEnum(getPinOrientationEnum());
    landPatternPad.setShapeEnum(getShapeEnum());

    landPatternPad.setName(getName());

    Assert.expect(landPatternPad != null);
    return landPatternPad;
  }
  
  /**
   * @author Kee Chin Seong
   */
  void invalidateOrientation()
  {
      if (_invalidateShapeEnabled == false)
      return;

    _shape = null;
    for (ComponentType componentType : getLandPattern().getComponentTypesUnsorted())
    {
      componentType.invalidateShapeForPadChangeOnly();
      for (Component component : componentType.getComponents())
      {
        for (Pad pad : component.getPads())
          pad.invalidateRectDegreeOrOrientation();
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  void invalidateShape()
  {
    if (_invalidateShapeEnabled == false)
      return;

    _shape = null;
    for (ComponentType componentType : getLandPattern().getComponentTypesUnsorted())
    {
      componentType.invalidateShapeForPadChangeOnly();
      for (Component component : componentType.getComponents())
      {
        for (Pad pad : component.getPads())
          pad.invalidateShape();
      }
    }
  }

  /**
   * @author George A. David
   */
  void invalidateJointHeight()
  {
    for(ComponentType compType : getLandPattern().getComponentTypes())
    {
      for(PackagePin pin : compType.getCompPackage().getPackagePinsUnsorted())
      {
        if(pin.getLandPatternPad() == this)
          pin.invalidateJointHeight();
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  void disableInvalidateShape()
  {
    _invalidateShapeEnabled = false;
  }

  /**
   * @author Bill Darbie
   */
  void enableInvalidateShape()
  {
    _invalidateShapeEnabled = true;
    invalidateShape();
  }

  /**
   * @return the Shape not related to any component, board or panel setting.
   *
   * @author Andy Mechtenberg
   */
  public java.awt.Shape getShapeInNanoMeters()
  {
    java.awt.Shape shape = null;
    if (_shape == null)
    {
      Assert.expect(_coordinateInNanoMeters != null);
      ComponentCoordinate padCoordinate = _coordinateInNanoMeters;
      int padWidth = getWidthInNanoMeters();
      int padLength = getLengthInNanoMeters();

      // find the lower left corner
      DoubleCoordinate lowerLeftPadLocation = new DoubleCoordinate(padCoordinate.getX() - padWidth / 2.0,
          padCoordinate.getY() - padLength / 2.0);

      if (getShapeEnum().equals(ShapeEnum.RECTANGLE))
      {
        shape = new Rectangle2D.Double(lowerLeftPadLocation.getX(),
                                       lowerLeftPadLocation.getY(),
                                       padWidth,
                                       padLength);
      }
      else if (getShapeEnum().equals(ShapeEnum.CIRCLE))
      {
        if (padWidth == padLength)
        {
          // this is a circle
          shape = new Ellipse2D.Double(lowerLeftPadLocation.getX(),
                                       lowerLeftPadLocation.getY(),
                                       padWidth,
                                       padLength);
        }
        else
        {
          shape = GeomUtil.createObround((int)lowerLeftPadLocation.getX(),
                                         (int)lowerLeftPadLocation.getY(),
                                         padWidth,
                                         padLength);
        }
      }
      else
        Assert.expect(false, "The pad shape was neither a circle or rectangle");


      // build up the correct transform for this Pad
      AffineTransform trans = AffineTransform.getRotateInstance(Math.toRadians(getDegreesRotation()),
          padCoordinate.getX(),
          padCoordinate.getY());

      // apply the transform
      shape = trans.createTransformedShape(shape);
      _shape = shape;
    }

    Assert.expect(_shape != null);
    return _shape;
  }

   /**
   * @return the Shape not related to any component, board or panel setting with hypothetical pad dimensions and/or coords
   *
   * @author Laura Cormos
   */
  public java.awt.Shape getHypotheticalShapeInNanoMeters(double xCoord, double yCoord, int xDim, int yDim)
  {
    Assert.expect(xDim >= 0);
    Assert.expect(yDim >= 0);

    java.awt.Shape shape = null;

    // find the lower left corner
    DoubleCoordinate lowerLeftPadLocation = new DoubleCoordinate(xCoord - xDim / 2.0, yCoord - yDim / 2.0);

    if (getShapeEnum().equals(ShapeEnum.RECTANGLE))
    {
      shape = new Rectangle2D.Double(lowerLeftPadLocation.getX(),
                                     lowerLeftPadLocation.getY(),
                                     xDim,
                                     yDim);
    }
    else if (getShapeEnum().equals(ShapeEnum.CIRCLE))
    {
      if (xDim == yDim)
      {
        // this is a circle
        shape = new Ellipse2D.Double(lowerLeftPadLocation.getX(),
                                     lowerLeftPadLocation.getY(),
                                     xDim,
                                     yDim);
      }
      else
      {
        shape = GeomUtil.createObround((int)lowerLeftPadLocation.getX(),
                                       (int)lowerLeftPadLocation.getY(),
                                       xDim,
                                       yDim);
      }
    }
    else
      Assert.expect(false, "The pad shape was neither a circle or rectangle");

    // build up the correct transform for this Pad
    AffineTransform trans = AffineTransform.getRotateInstance(Math.toRadians(getDegreesRotation()),
                                                              xCoord,
                                                              yCoord);
    // apply the transform
    shape = trans.createTransformedShape(shape);
    Assert.expect(shape != null);
    return shape;
  }

  /**
   * This would return area in square root nanometers for this pad
   * @author Poh Kheng
   */
  public double getAreaInNanoMeters()
  {
    double shapeArea = 0;

    if (getShapeEnum().equals(ShapeEnum.RECTANGLE))
    {
      shapeArea = MathUtil.sumSquareRootRectangleArea(getWidthInNanoMeters(), getLengthInNanoMeters());
    }
    else if (getShapeEnum().equals(ShapeEnum.CIRCLE))
    {
      shapeArea = MathUtil.sumSquareRootAbroundArea(getWidthInNanoMeters(), getLengthInNanoMeters());
    }

    return shapeArea;
  }

  /**
   * @author Poh Kheng
   */
  public ComponentCoordinate getMinPadCoordinateInNanoMeters()
  {
    Assert.expect(_coordinateInNanoMeters != null);
    return new ComponentCoordinate(_coordinateInNanoMeters.getX()-(_widthInNanoMeters/2),
                                   _coordinateInNanoMeters.getY()-(_lengthInNanoMeters/2));
  }

  /**
   * @author Poh Kheng
   */
  public ComponentCoordinate getMaxPadCoordinateInNanoMeters()
  {
    Assert.expect(_coordinateInNanoMeters != null);
    return new ComponentCoordinate(_coordinateInNanoMeters.getX()+(_widthInNanoMeters/2),
                                   _coordinateInNanoMeters.getY()+(_lengthInNanoMeters/2));
  }
}
