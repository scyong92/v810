package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class FiducialTypeEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static FiducialTypeEventEnum CREATE_OR_DESTROY = new FiducialTypeEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static FiducialTypeEventEnum ADD_OR_REMOVE = new FiducialTypeEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static FiducialTypeEventEnum NAME = new FiducialTypeEventEnum(++_index);
  public static FiducialTypeEventEnum COORDINATE = new FiducialTypeEventEnum(++_index);
  public static FiducialTypeEventEnum WIDTH = new FiducialTypeEventEnum(++_index);
  public static FiducialTypeEventEnum LENGTH = new FiducialTypeEventEnum(++_index);
  public static FiducialTypeEventEnum SHAPE_ENUM = new FiducialTypeEventEnum(++_index);
  public static FiducialTypeEventEnum ADD_OR_REMOVE_FIDUCIAL = new FiducialTypeEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);

  /**
   * @author Bill Darbie
   */
  private FiducialTypeEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private FiducialTypeEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
