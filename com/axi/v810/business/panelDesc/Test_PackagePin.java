package com.axi.v810.business.panelDesc;

import java.io.*;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class Test_PackagePin extends UnitTest
{
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_PackagePin());
  }

  /**
   * @author Keith Lee
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Expect.expect(is != null);
    Expect.expect(os != null);

    pinOrientationEnumTests();

  }

  /**
   * @author Bill Darbie
   */
  private void pinOrientationEnumTests()
  {
    LandPatternPad landPatternPad = new SurfaceMountLandPatternPad();
    LandPattern landPattern = new LandPattern();
    landPatternPad.setLandPattern(landPattern);

    PackagePin packagePin = new PackagePin();
    packagePin.setLandPatternPad(landPatternPad);

    // test PinOrientationEnum and PinOrientationAfterRotationEnum
    packagePin.setPinOrientationEnum(PinOrientationEnum.NOT_APPLICABLE);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.NOT_APPLICABLE));

    try
    {
      packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.NOT_APPLICABLE);
      Expect.expect(false);
    }
    catch (AssertException ae)
    {
      // do nothing
    }

    // N, S, E, Wa
    // North
    landPatternPad.setDegreesRotation(0);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));

    landPatternPad.setDegreesRotation(90);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));

    landPatternPad.setDegreesRotation(180);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));

    landPatternPad.setDegreesRotation(270);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));

    // South
    landPatternPad.setDegreesRotation(0);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));

    landPatternPad.setDegreesRotation(90);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));

    landPatternPad.setDegreesRotation(180);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));

    landPatternPad.setDegreesRotation(270);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));

    // East
    landPatternPad.setDegreesRotation(0);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));

    landPatternPad.setDegreesRotation(90);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));

    landPatternPad.setDegreesRotation(180);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));

    landPatternPad.setDegreesRotation(270);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));

    // West
    landPatternPad.setDegreesRotation(0);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));

    landPatternPad.setDegreesRotation(90);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));

    landPatternPad.setDegreesRotation(180);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));

    landPatternPad.setDegreesRotation(270);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));

    // NW, NE, SW, SE
    landPatternPad.setDegreesRotation(10);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));

    landPatternPad.setDegreesRotation(100);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));

    landPatternPad.setDegreesRotation(190);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));

    landPatternPad.setDegreesRotation(280);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));

    // South
    landPatternPad.setDegreesRotation(0);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));

    landPatternPad.setDegreesRotation(90);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));

    landPatternPad.setDegreesRotation(180);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));

    landPatternPad.setDegreesRotation(270);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));

    // East
    landPatternPad.setDegreesRotation(0);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));

    landPatternPad.setDegreesRotation(90);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));

    landPatternPad.setDegreesRotation(180);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));

    landPatternPad.setDegreesRotation(270);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));

    // West
    landPatternPad.setDegreesRotation(0);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));

    landPatternPad.setDegreesRotation(90.5);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));

    landPatternPad.setDegreesRotation(180);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));

    landPatternPad.setDegreesRotation(270);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));

    // NW, NE, SW, SE 2nd time
    // NE
    landPatternPad.setDegreesRotation(10);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));

    landPatternPad.setDegreesRotation(100);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));

    landPatternPad.setDegreesRotation(190);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));

    landPatternPad.setDegreesRotation(280);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));


    // NW
    landPatternPad.setDegreesRotation(0);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_WEST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));

    landPatternPad.setDegreesRotation(90);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_WEST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));

    landPatternPad.setDegreesRotation(180);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_WEST_OF_PIN);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN);

    landPatternPad.setDegreesRotation(270);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_WEST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));


    // SE
    landPatternPad.setDegreesRotation(0);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));

    landPatternPad.setDegreesRotation(90);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));

    landPatternPad.setDegreesRotation(180);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));

    landPatternPad.setDegreesRotation(270);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_EAST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));

    // SW
    landPatternPad.setDegreesRotation(0);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_WEST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));

    landPatternPad.setDegreesRotation(90.5);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_WEST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));

    landPatternPad.setDegreesRotation(180);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_WEST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));

    landPatternPad.setDegreesRotation(270);
    packagePin.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_WEST_OF_PIN);
    Expect.expect(packagePin.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));

  }
}
