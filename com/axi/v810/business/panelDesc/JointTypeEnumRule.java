package com.axi.v810.business.panelDesc;

import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Bill Darbie
 */
public class JointTypeEnumRule
{
  private static Pattern _numberOnlyPattern = Pattern.compile("^\\s*(-?\\d+)\\s*$");
  private static Pattern _greaterThanOrEqualToPattern = Pattern.compile("^\\s*>\\s*=\\s*(-?\\d+)\\s*$");
  private static Pattern _greaterThanPattern = Pattern.compile("^\\s*>\\s*(-?\\d+)\\s*$");
  private static Pattern _lessThanOrEqualToPattern = Pattern.compile("^\\s*<\\s*=\\s*(-?\\d+)\\s*$");
  private static Pattern _lessThanPattern = Pattern.compile("^\\s*<\\s*(-?\\d+)\\s*$");
  private static Pattern _equalPattern = Pattern.compile("^\\s*=\\s*(-?\\d+)\\s*$");
  // presumably no one will need more than 999,999,999 for number of pads, columns or rows --|
  //                                                                                         v
  private static Pattern _validNumberPattern = Pattern.compile("^\\s*(<|=|>|<=|>=)?\\s*\\d{1,9}\\s*$");

  private ComparisonEnum _calculatedComparisonEnum;
  private int _calculatedNum;

  private JointTypeEnum _jointTypeEnum;

  private String _refDesPattern;
  private Pattern _refDesCompiledPattern;

  private String _landPatternNamePattern;
  private Pattern _landPatternNameCompiledPattern;
  private String _numPadsPattern;
  private int _numPads = -1;
  private ComparisonEnum _numPadsComparisonEnum;
  private String _numRowsPattern;
  private int _numRows = -1;
  private ComparisonEnum _numRowsComparisonEnum;
  private String _numColsPattern;
  private int _numCols = -1;
  private ComparisonEnum _numColsComparisonEnum;
  private BooleanRef _padOneInCenterOfRow;
  private boolean _enabled = true;

  private ConfigObservable _configObservable = ConfigObservable.getInstance();

  /**
   * @author Bill Darbie
   */
  public JointTypeEnumRule(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);
    _jointTypeEnum = jointTypeEnum;
  }

  /**
   * @author Bill Darbie
   */
  public JointTypeEnum getJointTypeEnum()
  {
    Assert.expect(_jointTypeEnum != null);
    return _jointTypeEnum;
  }

  /**
   * @author Laura Cormos
   */
  public void setJointTypeEnum(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    _jointTypeEnum = jointTypeEnum;
    _configObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  public void setEnabled(boolean enabled)
  {
    _enabled = enabled;
    _configObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isEnabled()
  {
    return _enabled;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isRefDesPatternSet()
  {
    if (_refDesPattern == null)
      return false;
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public void clearRefDesPattern()
  {
    _refDesPattern = null;
    _configObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  public void setRefDesPattern(String refDesPattern) throws InvalidRegularExpressionDatastoreException
  {
    Assert.expect(refDesPattern != null);
    _refDesPattern = refDesPattern;
    try
    {
      _refDesCompiledPattern = Pattern.compile(refDesPattern, Pattern.CASE_INSENSITIVE);
    }
    catch (PatternSyntaxException ex)
    {
      InvalidRegularExpressionDatastoreException bex = new InvalidRegularExpressionDatastoreException(ex);
      bex.initCause(ex);
      throw bex;
    }
    _configObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  public String getRefDesPattern()
  {
    Assert.expect(_refDesPattern != null);
    return _refDesPattern;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isLandPatternNamePatternSet()
  {
    if (_landPatternNamePattern == null)
      return false;
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void clearLandPatternNamePattern()
  {
    _landPatternNamePattern = null;
    _configObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  public void setLandPatternNamePattern(String landPatternNamePattern) throws InvalidRegularExpressionDatastoreException
  {
    Assert.expect(landPatternNamePattern != null);
    _landPatternNamePattern = landPatternNamePattern;
    try
    {
      _landPatternNameCompiledPattern = Pattern.compile(landPatternNamePattern, Pattern.CASE_INSENSITIVE);
    }
    catch (PatternSyntaxException ex)
    {
      InvalidRegularExpressionDatastoreException bex = new InvalidRegularExpressionDatastoreException(ex);
      bex.initCause(ex);
      throw bex;
    }
    _configObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  public String getLandPatternNamePattern()
  {
    Assert.expect(_landPatternNamePattern != null);
    return _landPatternNamePattern;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isNumPadsSet()
  {
    if (_numPads == -1)
      return false;
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void clearNumPadsPattern()
  {
    _numPadsPattern = null;
    _numPads = -1;
    _configObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  public void setNumPadsPattern(String numPadsPattern)
  {
    Assert.expect(numPadsPattern != null);
    _numPadsPattern = numPadsPattern;
    calcNumAndComparisonEnum(numPadsPattern);
    _numPads = _calculatedNum;
    _numPadsComparisonEnum = _calculatedComparisonEnum;

    _configObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  public String getNumPadsPattern()
  {
    Assert.expect(_numPadsPattern != null);
    return _numPadsPattern;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isNumRowsSet()
  {
    if (_numRows == -1)
      return false;
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void clearNumRowsPattern()
  {
    _numRowsPattern = null;
    _numRows = -1;
    _configObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  public void setNumRowsPattern(String numRowsPattern)
  {
    Assert.expect(numRowsPattern != null);
    _numRowsPattern = numRowsPattern;
    calcNumAndComparisonEnum(numRowsPattern);
    _numRows = _calculatedNum;
    _numRowsComparisonEnum = _calculatedComparisonEnum;

    _configObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  public String getNumRowsPattern()
  {
    Assert.expect(_numRowsPattern != null);
    return _numRowsPattern;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isNumColsSet()
  {
    if (_numCols == -1)
      return false;
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void clearNumColsPattern()
  {
    _numColsPattern = null;
    _numCols = -1;
    _configObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  public void setNumColsPattern(String numColsPattern)
  {
    Assert.expect(numColsPattern != null);
    _numColsPattern = numColsPattern;
    calcNumAndComparisonEnum(numColsPattern);
    _numCols = _calculatedNum;
    _numColsComparisonEnum = _calculatedComparisonEnum;

    _configObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  public String getNumColsPattern()
  {
    Assert.expect(_numColsPattern != null);
    return _numColsPattern;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isPadOneInCenterOfRowSet()
  {
    if (_padOneInCenterOfRow == null || _padOneInCenterOfRow.getValue() == false)
      return false;
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public void clearPadOneInCenterOfRow()
  {
    _padOneInCenterOfRow = null;
    _configObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  public void setPadOneInCenterOfRow(boolean centerOfRow)
  {
    if(_padOneInCenterOfRow == null)
      _padOneInCenterOfRow = new BooleanRef(centerOfRow);
    else
      _padOneInCenterOfRow.setValue(centerOfRow);
    
    _configObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isPadOneInCenterOfRow()
  {
    Assert.expect(_padOneInCenterOfRow != null);
    return _padOneInCenterOfRow.getValue();
  }

  /**
   * @author Bill Darbie
   */
  private void calcNumAndComparisonEnum(String number)
  {
    Assert.expect(number != null);
    Assert.expect(isNumPatternValid(number));

    Matcher matcher = _numberOnlyPattern.matcher(number);
    if (matcher.matches())
    {
      _calculatedComparisonEnum = ComparisonEnum.EQUAL;
    }
    else
    {
      matcher = _equalPattern.matcher(number);
      if (matcher.matches())
      {
        _calculatedComparisonEnum = ComparisonEnum.EQUAL;
      }
      else
      {
        matcher = _greaterThanOrEqualToPattern.matcher(number);
        if (matcher.matches())
        {
          _calculatedComparisonEnum = ComparisonEnum.GREATER_THAN_OR_EQUAL;
        }
        else
        {
          matcher = _greaterThanPattern.matcher(number);
          if (matcher.matches())
          {
            _calculatedComparisonEnum = ComparisonEnum.GREATER_THAN;
          }
          else
          {
            matcher = _lessThanOrEqualToPattern.matcher(number);
            if (matcher.matches())
            {
              _calculatedComparisonEnum = ComparisonEnum.LESS_THAN_OR_EQUAL;
            }
            else
            {
              matcher = _lessThanPattern.matcher(number);
              if (matcher.matches())
              {
                _calculatedComparisonEnum = ComparisonEnum.LESS_THAN;
              }
              else
              {
                Assert.expect(false);
              }
            }
          }
        }
      }
    }

    try
    {
      _calculatedNum = StringUtil.convertStringToInt(matcher.group(1));
    }
    catch (BadFormatException bfe)
    {
      Assert.logException(bfe);
    }
  }

  /**
   * @author Bill Darbie
   */
  private boolean compareInt(int ruleNumber, int cadNumber, ComparisonEnum comparisonEnum)
  {
    Assert.expect(comparisonEnum != null);

    if (comparisonEnum.equals(ComparisonEnum.EQUAL))
    {
      if (cadNumber != ruleNumber)
        return false;
    }
    else if (comparisonEnum.equals(ComparisonEnum.GREATER_THAN))
    {
      if (cadNumber <= ruleNumber)
        return false;
    }
    else if (comparisonEnum.equals(ComparisonEnum.GREATER_THAN_OR_EQUAL))
    {
      if (cadNumber < ruleNumber)
        return false;
    }
    else if (comparisonEnum.equals(ComparisonEnum.LESS_THAN))
    {
      if (cadNumber >= ruleNumber)
        return false;
    }
    else if (comparisonEnum.equals(ComparisonEnum.LESS_THAN_OR_EQUAL))
    {
      if (cadNumber > ruleNumber)
        return false;
    }
    else
        Assert.expect(false);

    return true;
  }

  /**
   * @author Bill Darbie
   */
  boolean match(ComponentType componentType)
  {
    Assert.expect(componentType != null);

    if (_refDesCompiledPattern != null)
    {
      String refDes = componentType.getReferenceDesignator();
      Matcher matcher = _refDesCompiledPattern.matcher(refDes);
      if (matcher.matches() == false)
        return false;
    }

    if (_landPatternNameCompiledPattern != null)
    {
      String landPatternName = componentType.getLandPattern().getName();
      Matcher matcher = _landPatternNameCompiledPattern.matcher(landPatternName);
      if (matcher.matches() == false)
        return false;
    }

    if (_numPads != -1)
    {
      int numLandPatternPads = componentType.getLandPattern().getLandPatternPads().size();
      if (compareInt(_numPads, numLandPatternPads, _numPadsComparisonEnum) == false)
        return false;
    }

    if (_numRows != -1)
    {
      int numRows = componentType.getLandPattern().getNumRows();
      if (compareInt(_numRows, numRows, _numRowsComparisonEnum) == false)
        return false;
    }

    if (_numCols != -1)
    {
      int numCols = componentType.getLandPattern().getNumColumns();
      if (compareInt(_numRows, numCols, _numColsComparisonEnum) == false)
        return false;
    }

    if (_padOneInCenterOfRow != null)
    {
      if (_padOneInCenterOfRow.getValue() != componentType.getLandPattern().isPadOneInCenterOfRowOrColumn())
        return false;
    }

    return true;
  }

  /**
   * @author Bill Darbie
   * @author Laura Cormos
   */

  public boolean isNumPatternValid(String numberPattern)
  {
    Assert.expect(numberPattern != null);

    // an empty pattern is valid, is the equivalent of it not being set
    if (numberPattern.length() == 0)
      return true;

    Matcher matcher = _validNumberPattern.matcher(numberPattern);
    return matcher.matches();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isRegularExpressionValid(String regEx)
  {
    Assert.expect(regEx != null);

    boolean valid = true;
    try
    {
      Pattern.compile(regEx);
    }
    catch (PatternSyntaxException ex)
    {
      valid = false;
    }

    return valid;
  }

  /**
   * A rule is considered default, catch-all rule if it is enabled and none of its parameters are set
   * @author Laura Cormos
   */
  public boolean isDefaultRule()
  {
    if (isEnabled() &&
        isLandPatternNamePatternSet() == false &&
        isNumColsSet() == false &&
        isNumPadsSet() == false &&
        isNumRowsSet() == false &&
        isPadOneInCenterOfRowSet() == false &&
        isRefDesPatternSet() == false)
      return true;
    return false;
  }
}
