package com.axi.v810.business.panelDesc;

/**
 * @author Bill Darbie
 */
public class NodeTypeEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static final NodeTypeEnum SIGNAL = new NodeTypeEnum(++_index);
  public static final NodeTypeEnum GROUND = new NodeTypeEnum(++_index);
  public static final NodeTypeEnum NOT_USED = new NodeTypeEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private NodeTypeEnum(int id)
  {
    super(id);
  }
}
