package com.axi.v810.business.panelDesc;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import com.axi.util.*;

/**
 * This class will compare any two Strings.  It will take numbers into account to
 * determine ordering.  It is case insensitive.
 * For example it will order things like this:
 *
 * a1
 * A2
 * a3
 * a12
 * B94
 * B100
 *
 * For Special case (PCAP) contains positive pin P* and negative pin N*
 * order will be like this:
 * P / POS / P*
 * N / NEG / N*
 *
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class PadNameComparator extends AlphaNumericComparator
{
  private Pattern _pinNamePattern1 = Pattern.compile("^(P|N)[^\\d]*$", Pattern.CASE_INSENSITIVE);

  boolean _ascending;

  /**
   * @author Wei Chin, Chong
   */
  public PadNameComparator(boolean ascending)
  {
    super(ascending);
    _ascending = ascending;
  }

  /**
   * @author Wei Chin, Chong
   */
  public PadNameComparator()
  {
    super(true);
    _ascending = true;
  }

  /**
   * Implementation of the 'compare' method in the Comparator interface.  This method assumes that the Strings
   * being compared follow the regular expression pattern of (<letter>*<digit>*)*
   *
   * The <letter> portions are compared alphabetically.  If they are the same, the numeric portions are compared as
   * integers.  The process repeats recursively until one of the comparisons is unequal.
   *
   * For Special case like PCAP contains pin P* (Positive) and pin N* (Negative)
   * order will be positive first then follow by negative:
   * P / POS / P*
   * N / NEG / N*
   *
   * @param lhs first String being compared
   * @param rhs second String being compared
   * @return -1 if the first String is less than the second, 0 if the two Strings are equal, 1 if the first String is
   * greater than the second
   *
   * @author Wei Chin, Chong - handle positive pin and negative pin
   *
   */
  public int compare( Object lhs, Object rhs )
  {
    Assert.expect( lhs != null );
    Assert.expect( rhs != null );

    String firstString = "";
    String secondString = "";

    firstString = lhs.toString();
    secondString = rhs.toString();

    // Remove any leading or trailing spaces.
    firstString = firstString.trim();
    secondString = secondString.trim();

    Matcher matcher1 = _pinNamePattern1.matcher(firstString);
    Matcher matcher2 = _pinNamePattern1.matcher(secondString);

    if ( matcher1.matches() && matcher2.matches() && (firstString.charAt(0) != secondString.charAt(0)) )
    {
      if (_ascending == false)
      {
        String tempString = firstString;
        firstString = secondString;
        secondString = tempString;
      }

      if( StringUtil.startWithIgnoreCase(firstString, "P") &&
          StringUtil.startWithIgnoreCase(secondString, "N") )
        return -1;
      else if( StringUtil.startWithIgnoreCase(firstString, "N") &&
               StringUtil.startWithIgnoreCase(secondString,"P") )
        return 1;
    }
    return super.compare(lhs,rhs);
  }
}
