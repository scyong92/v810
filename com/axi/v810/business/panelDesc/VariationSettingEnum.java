package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 *
 * @author Kok Chun, Tan
 */
public class VariationSettingEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static VariationSettingEnum ADD_OR_REMOVE = new VariationSettingEnum(++_index);
  public static VariationSettingEnum NAME = new VariationSettingEnum(++_index);
  public static VariationSettingEnum ENABLE = new VariationSettingEnum(++_index);
  public static VariationSettingEnum PRESELECT = new VariationSettingEnum(++_index);
  public static VariationSettingEnum ENABLE_CHOOSABLE_IN_PRODUCTION = new VariationSettingEnum(++_index);
  
  /**
   * @author Kok Chun, Tan
   */
  private VariationSettingEnum(int id)
  {
    super(id);
  }

  /**
   * @author Kok Chun, Tan
   */
  private VariationSettingEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
