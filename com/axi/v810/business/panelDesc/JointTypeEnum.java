package com.axi.v810.business.panelDesc;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class JointTypeEnum extends com.axi.util.Enum implements Serializable
{
  private static Map<String, JointTypeEnum> _nameToJointTypeEnumMap = new TreeMap<String, JointTypeEnum>(new AlphaNumericComparator());

  private static int _index = -1;

  public final static JointTypeEnum CAPACITOR = new JointTypeEnum(++_index, "Capacitor", true, true, true);
  public final static JointTypeEnum RESISTOR = new JointTypeEnum(++_index, "Resistor", true, true, true);
  public final static JointTypeEnum POLARIZED_CAP = new JointTypeEnum(++_index, "Polarized Capacitor", true, false, true);
  public final static JointTypeEnum GULLWING = new JointTypeEnum(++_index, "Gullwing", true, true, true);
  public final static JointTypeEnum JLEAD = new JointTypeEnum(++_index, "JLead", true, true, true);
  public final static JointTypeEnum SURFACE_MOUNT_CONNECTOR = new JointTypeEnum(++_index, "SMT Connector", true, true, true);
  public final static JointTypeEnum NON_COLLAPSABLE_BGA = new JointTypeEnum(++_index, "Non Collapsible BGA", false, false, true);
  public final static JointTypeEnum COLLAPSABLE_BGA = new JointTypeEnum(++_index, "Collapsible BGA", false, false, true);
  public final static JointTypeEnum CGA = new JointTypeEnum(++_index, "CGA", false, false, false);
  public final static JointTypeEnum PRESSFIT = new JointTypeEnum(++_index, "Pressfit", false, false, false);
  public final static JointTypeEnum THROUGH_HOLE = new JointTypeEnum(++_index, "PTH", false, false, false);
  public final static JointTypeEnum OVAL_THROUGH_HOLE = new JointTypeEnum(++_index, "Oval PTH", false, false, false); //Siew Yeng - XCR-3318 - Oval PTH
  public final static JointTypeEnum SINGLE_PAD = new JointTypeEnum(++_index, "Single Pad", true, true, false);
  public final static JointTypeEnum RF_SHIELD = new JointTypeEnum(++_index, "RF Shield", true, true, false);
  public final static JointTypeEnum LEADLESS = new JointTypeEnum(++_index, "LeadLess", true, true, true);
  public final static JointTypeEnum SMALL_OUTLINE_LEADLESS = new JointTypeEnum(++_index, "SOT", true, true, true);
  public final static JointTypeEnum QUAD_FLAT_PACK_NOLEAD = new JointTypeEnum(++_index, "QFN", true, true, true);
  public final static JointTypeEnum CHIP_SCALE_PACKAGE = new JointTypeEnum(++_index, "CSP", false, false, true);
  public final static JointTypeEnum VARIABLE_HEIGHT_BGA_CONNECTOR =
      new JointTypeEnum(++_index, "Variable Height BGA Connector", false, false, true); // NexLev
  public final static JointTypeEnum EXPOSED_PAD = new JointTypeEnum(++_index, "Exposed Pad", true, false, true);
//  public final static JointTypeEnum TALL_CAPACITOR = new JointTypeEnum(++_index, "Tall Capacitor", true, true, true);
  public final static JointTypeEnum LGA = new JointTypeEnum(++_index, "LGA", false, false, true);

  private String _name;
  private boolean _isAvailableForRetestFailingPins;
  private boolean _isAvailableForRetestPassingOutliers;
  private boolean _useToBuildLimitedSurfaceModel;

  /**
   * @author Andy Mechtenberg
   */
  private JointTypeEnum(int id, String name, boolean retestFailingPins,
                        boolean retestPassingOutliers, boolean useToBuildLimitedSurfaceModel)
  {
    super(id);
    Assert.expect(name != null);
   _name = name.intern();
   _isAvailableForRetestFailingPins = retestFailingPins;
   _isAvailableForRetestPassingOutliers = retestPassingOutliers;
   _useToBuildLimitedSurfaceModel = useToBuildLimitedSurfaceModel;
   _nameToJointTypeEnumMap.put(_name, this);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getName()
  {
    return _name;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isAvailableForRetestFailingPins()
  {
    return _isAvailableForRetestFailingPins;
  }

  /**
   *  @author John Heumann
   */
  public boolean isAvailableForRetestPassingOutliers()
  {
    return _isAvailableForRetestPassingOutliers;
  }

  /**
   * @author John Heumann
   */
   public boolean isUseToBuildLimitedSurfaceModel()
   {
     return _useToBuildLimitedSurfaceModel;
   }

  /**
   * @author Bill Darbie
   */
  public String getNameWithoutSpaces()
  {
    StringBuilder sb = new StringBuilder();
    for (char ch : _name.toCharArray())
    {
      if (ch != ' ')
        sb.append(ch);
    }

    return sb.toString();
  }

  /**
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author Bill Darbie
   */
  public static JointTypeEnum getJointTypeEnum(String jointTypeName)
  {
    JointTypeEnum jointTypeEnum = _nameToJointTypeEnumMap.get(jointTypeName);
    Assert.expect(jointTypeEnum != null);
    return jointTypeEnum;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static boolean doesJointTypeEnumExist(String jointTypeName)
  {
    return _nameToJointTypeEnumMap.containsKey(jointTypeName);
  }

  /**
   * @author Andy Mechtenberg
   */
  public static List<JointTypeEnum> getAllJointTypeEnums()
  {
    return new ArrayList<JointTypeEnum>(_nameToJointTypeEnumMap.values());
  }
}
