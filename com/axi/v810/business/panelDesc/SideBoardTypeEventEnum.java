package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class SideBoardTypeEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static SideBoardTypeEventEnum CREATE_OR_DESTROY = new SideBoardTypeEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static SideBoardTypeEventEnum BOARD_TYPE = new SideBoardTypeEventEnum(++_index);
  public static SideBoardTypeEventEnum ADD_OR_REMOVE_COMPONENT_TYPE = new SideBoardTypeEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static SideBoardTypeEventEnum SIDE_BOARD_TYPE_SETTINGS = new SideBoardTypeEventEnum(++_index);
  public static SideBoardTypeEventEnum ADD_OR_REMOVE_SIDE_BOARD = new SideBoardTypeEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static SideBoardTypeEventEnum ADD_OR_REMOVE_FIDUCIAL_TYPE = new SideBoardTypeEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);

  /**
   * @author Bill Darbie
   */
  private SideBoardTypeEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private SideBoardTypeEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
