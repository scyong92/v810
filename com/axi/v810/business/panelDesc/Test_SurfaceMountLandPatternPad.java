package com.axi.v810.business.panelDesc;

import java.io.*;

import com.axi.util.*;

/**
 * @author Andy Mechtenberg
 */
public class Test_SurfaceMountLandPatternPad extends UnitTest
{
  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_SurfaceMountLandPatternPad());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    // do nothing.  SurfaceMountPad is a derived class from LandPatternPad
    // and there are no extra variables or methods in SurfaceMountPad.
    // LandPatternPad will be tested in it's own test class
  }
}
