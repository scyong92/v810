package com.axi.v810.business.panelDesc;

import java.util.*;

import com.axi.util.*;

/**
 * The pin orientation describes the direction of the center of the component is in relative
 * to the pin.
 *
 * A setting of NOT_APPLICABLE will be used for round pads.
 *
 * @author Bill Darbie
 */
public class PinOrientationAfterRotationEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  private static Map<Integer, PinOrientationAfterRotationEnum> _degreesToEnum = new HashMap<Integer, PinOrientationAfterRotationEnum>();

  private static List<PinOrientationAfterRotationEnum> _allEnums = new ArrayList<PinOrientationAfterRotationEnum>();

  public static PinOrientationAfterRotationEnum NOT_APPLICABLE = new PinOrientationAfterRotationEnum(++_index, "NA", 0);
  public static PinOrientationAfterRotationEnum COMPONENT_IS_NORTH_OF_PIN = new PinOrientationAfterRotationEnum(++_index, "North", 90);
  public static PinOrientationAfterRotationEnum COMPONENT_IS_SOUTH_OF_PIN = new PinOrientationAfterRotationEnum(++_index, "South", 270);
  public static PinOrientationAfterRotationEnum COMPONENT_IS_EAST_OF_PIN = new PinOrientationAfterRotationEnum(++_index, "East", 0);
  public static PinOrientationAfterRotationEnum COMPONENT_IS_WEST_OF_PIN = new PinOrientationAfterRotationEnum(++_index, "West", 180);

  public static PinOrientationAfterRotationEnum COMPONENT_IS_NORTH_WEST_OF_PIN = new PinOrientationAfterRotationEnum(++_index, "NW", 135);
  public static PinOrientationAfterRotationEnum COMPONENT_IS_SOUTH_WEST_OF_PIN = new PinOrientationAfterRotationEnum(++_index, "SW", 225);
  public static PinOrientationAfterRotationEnum COMPONENT_IS_NORTH_EAST_OF_PIN = new PinOrientationAfterRotationEnum(++_index, "NE", 45);
  public static PinOrientationAfterRotationEnum COMPONENT_IS_SOUTH_EAST_OF_PIN = new PinOrientationAfterRotationEnum(++_index, "SE", 315);

  private String _name;
  private int _degrees;


  /**
   * @author Bill Darbie
   */
  private PinOrientationAfterRotationEnum(int id, String name, int degrees)
  {
    super(id);

    Assert.expect(name != null);
    _name = name.intern();

    if (name.equals("NA") == false)
    {
      _allEnums.add(this);
      Object prev = _degreesToEnum.put(degrees, this);
      Assert.expect(prev == null);
    }

    _degrees = degrees;
  }

  /**
   * @author Bill Darbie
   */
  public String getName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author Bill Darbie
   */
  public static List<PinOrientationAfterRotationEnum> getAllPinOrientationEnums()
  {
    Assert.expect(_allEnums != null);

    return _allEnums;
  }

  /**
   * @author Bill Darbie
   */
  public static List<PinOrientationAfterRotationEnum> getAllOrthogonalPinOrientationEnums()
  {
    Assert.expect(_allEnums != null);

    List<PinOrientationAfterRotationEnum> enums = new ArrayList<PinOrientationAfterRotationEnum>();
    for (PinOrientationAfterRotationEnum pinOrientationEnum : getAllPinOrientationEnums())
    {
      if (pinOrientationEnum.isOrthogonal())
        enums.add(pinOrientationEnum);
    }

    return enums;
  }

  /**
   * @author Bill Darbie
   */
  public static List<PinOrientationAfterRotationEnum> getAllNonOrthogonalPinOrientationEnums()
  {
    Assert.expect(_allEnums != null);

    List<PinOrientationAfterRotationEnum> enums = new ArrayList<PinOrientationAfterRotationEnum>();
    for (PinOrientationAfterRotationEnum pinOrientationEnum : getAllPinOrientationEnums())
    {
      if (pinOrientationEnum.isOrthogonal() == false)
        enums.add(pinOrientationEnum);
    }

    return enums;
  }


  /**
   * @author Bill Darbie
   */
  public static PinOrientationAfterRotationEnum getPinOrientationAfterRotationEnum(PinOrientationEnum pinOrientationEnum)
  {
    Assert.expect(pinOrientationEnum != null);

    PinOrientationAfterRotationEnum pinOrientationAfterRotationEnum = PinOrientationAfterRotationEnum.NOT_APPLICABLE;
    if (pinOrientationEnum.equals(pinOrientationEnum.NOT_APPLICABLE) == false)
    {
      int degrees = pinOrientationEnum.getDegrees();
      pinOrientationAfterRotationEnum = _degreesToEnum.get(degrees);
    }

    Assert.expect(pinOrientationAfterRotationEnum != null);
    return pinOrientationAfterRotationEnum;
  }

  /**
   * @author Bill Darbie
   */
  public PinOrientationAfterRotationEnum getPinOrientationAfterRotationEnum(double degrees)
  {
    PinOrientationAfterRotationEnum pinOrienationAfterRotationEnum = PinOrientationAfterRotationEnum.NOT_APPLICABLE;
    if (this.equals(PinOrientationAfterRotationEnum.NOT_APPLICABLE) == false)
    {
      degrees = MathUtil.getDegreesWithin0To359(degrees);
      int degreeKey = (int)degrees + getDegrees();
      degreeKey = MathUtil.getDegreesWithin0To359(degreeKey);

      if ((degreeKey > 0) && (degreeKey < 90))
        degreeKey = 45;
      else if ((degreeKey > 90) && (degreeKey < 180))
        degreeKey = 135;
      else if ((degreeKey > 180) && (degreeKey < 270))
        degreeKey = 225;
      else if ((degreeKey > 270) && (degreeKey < 360))
        degreeKey = 315;

      pinOrienationAfterRotationEnum = _degreesToEnum.get(degreeKey);
      Assert.expect(pinOrienationAfterRotationEnum != null);
    }

    return pinOrienationAfterRotationEnum;
  }

  /**
   * @author Bill Darbie
   */
  public static PinOrientationAfterRotationEnum getPinOrientationAfterRotationEnumFromDegrees(double degrees)
  {
    PinOrientationAfterRotationEnum pinOrienationAfterRotationEnum = PinOrientationAfterRotationEnum.NOT_APPLICABLE;
    degrees = MathUtil.getDegreesWithin0To359(degrees);
    int degreeKey = (int)degrees;
    degreeKey = MathUtil.getDegreesWithin0To359(degreeKey);

    if ((degreeKey > 0) && (degreeKey < 90))
      degreeKey = 45;
    else if ((degreeKey > 90) && (degreeKey < 180))
      degreeKey = 135;
    else if ((degreeKey > 180) && (degreeKey < 270))
      degreeKey = 225;
    else if ((degreeKey > 270) && (degreeKey < 360))
      degreeKey = 315;

    pinOrienationAfterRotationEnum = _degreesToEnum.get(degreeKey);
    Assert.expect(pinOrienationAfterRotationEnum != null);

    return pinOrienationAfterRotationEnum;
  }



  /**
   * @author Bill Darbie
   */
  public PinOrientationEnum getPinOrientationEnum(double packagePinDegrees)
  {
    PinOrientationEnum pinOrientationEnum = PinOrientationEnum.NOT_APPLICABLE;
    if (this.equals(PinOrientationAfterRotationEnum.NOT_APPLICABLE) == false)
    {
      packagePinDegrees = MathUtil.getDegreesWithin0To359(packagePinDegrees);
      double enumDegrees = getDegrees();
      double degrees = enumDegrees - packagePinDegrees;
      degrees = MathUtil.getDegreesWithin0To359(degrees);
      int intDegrees = (int)degrees;

      if ((intDegrees > 0) && (intDegrees <= 45))
        intDegrees = 0;
      else if ((intDegrees > 45) && (intDegrees <= 135))
        intDegrees = 90;
      else if ((intDegrees > 135) && (intDegrees <= 225))
        intDegrees = 180;
      else if ((intDegrees > 225) && (intDegrees <= 315))
        intDegrees = 270;
      else if ((intDegrees > 315) && (intDegrees <= 360))
        intDegrees = 0;

      pinOrientationEnum = PinOrientationEnum.getPinOrientationEnum(intDegrees);
    }

    return pinOrientationEnum;
  }

  /**
   * @author Bill Darbie
   */
  public static PinOrientationAfterRotationEnum getPinOrientationAfterRotationEnum(PinOrientationEnum pinOrientationEnum, double degrees)
  {
    Assert.expect(pinOrientationEnum != null);

    PinOrientationAfterRotationEnum pinOrientationAfterRotationEnum = PinOrientationAfterRotationEnum.getPinOrientationAfterRotationEnum(pinOrientationEnum);
    pinOrientationAfterRotationEnum = pinOrientationAfterRotationEnum.getPinOrientationAfterRotationEnum(degrees);

    return pinOrientationAfterRotationEnum;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isOrthogonal()
  {
    if (_degrees % 90 == 0)
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public int getDegrees()
  {
    return _degrees;
  }
}
