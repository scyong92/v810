package com.axi.v810.business.panelDesc;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * This class represents either a top side board or a bottom side board.
 * A Board can have one or two SideBoard instances depending on whether it
 * is two sided or not.
 * There is one SideBoard instance for each physical SideBoard in the panel.
 * @author Bill Darbie
 */
public class SideBoard implements Serializable
{
  // created by PanelNdfReader

  private SideBoardType _sideBoardType; // PanelReader
  private Board _board; // PanelNdfReader
  private Map<String, Component> _refDesToComponentMap = new TreeMap<String, Component>(new AlphaNumericComparator()); // BoardNdfReader, PanelReader
  private Map<String, Fiducial> _fiducialNameToFiducialMap = new TreeMap<String, Fiducial>(new AlphaNumericComparator()); // no NdfReaders

  private static transient ProjectObservable _projectObservable;

  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public SideBoard()
  {
    _projectObservable.stateChanged(this, null, this, SideBoardEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      getBoard().removeSideBoard(this);
      getSideBoardType().removeSideBoard(this);
      for (Component component : getComponents())
        component.destroy();
      for (Fiducial fiducial : getFiducials())
        fiducial.destroy();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, SideBoardEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void setSideBoardType(SideBoardType sideBoardType)
  {
    Assert.expect(sideBoardType != null);

    if (sideBoardType == _sideBoardType)
      return;

    SideBoardType oldValue = _sideBoardType;
    _projectObservable.setEnabled(false);
    try
    {
      _sideBoardType = sideBoardType;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, sideBoardType, SideBoardEventEnum.SIDE_BOARD_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public SideBoardType getSideBoardType()
  {
    Assert.expect(_sideBoardType != null);

    return _sideBoardType;
  }

  /**
   * @author Bill Darbie
   */
  public SideBoardTypeSettings getSideBoardTypeSettings()
  {
    Assert.expect(_sideBoardType != null);

    return _sideBoardType.getSideBoardTypeSettings();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isSideBoard1()
  {
    Assert.expect(_board != null);
    if (_board.sideBoard1Exists())
    {
      if (this == _board.getSideBoard1())
        return true;
    }

    return false;
  }

  /**
   * @return true if this SideBoard is facing the x-ray source
   * @author Bill Darbie
   */
  public boolean isTopSide()
  {
    Assert.expect(_board != null);
    if ((_board.topSideBoardExists()) && (this == _board.getTopSideBoard()))
      return true;

    return false;
  }

  /**
   * @return true if this SideBoard is NOT facing the x-ray source.
   * @author Bill Darbie
   */
  public boolean isBottomSide()
  {
    return !isTopSide();
  }

  /**
   * The board that this side board belongs to
   * @author George A. David
   */
  public void setBoard(Board board)
  {
    Assert.expect(board != null);

    if (board == _board)
      return;

    Board oldValue = _board;
    _projectObservable.setEnabled(false);
    try
    {
      _board = board;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, board, SideBoardEventEnum.BOARD);
  }

  /**
   * The board that this side board belongs to
   * @author George A. David
   */
  public Board getBoard()
  {
    Assert.expect(_board != null);

    return _board;
  }

  /**
   * @author Keith Lee
   */
  public void addComponent(Component component)
  {
    Assert.expect(component != null);
    Assert.expect(_refDesToComponentMap != null);

    _projectObservable.setEnabled(false);
    try
    {
      String refDes = component.getReferenceDesignator();
      Component prev = _refDesToComponentMap.put(refDes, component);
      Assert.expect(prev == null);
      component.setSideBoard(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, component, SideBoardEventEnum.ADD_OR_REMOVE_COMPONENT);
  }

  /**
   * Create Components for this SideBoard using pre-existing ComponentTypes
   * @author Bill Darbie
   */
  void createComponents()
  {
    SideBoardType sideBoardType = getSideBoardType();
    for (ComponentType componentType : sideBoardType.getComponentTypes())
    {
      Component component = new Component();
      component.setSideBoard(this);
      component.setComponentType(componentType);
      component.createPads();
      addComponent(component);
    }
  }

  /**
   * Create Fiducials for this SideBoard using pre-existing FiducialTypes
   * @author Bill Darbie
   */
  void createFiducials()
  {
    SideBoardType sideBoardType = getSideBoardType();
    for (FiducialType fiducialType : sideBoardType.getFiducialTypes())
    {
      Fiducial fiducial = new Fiducial();
      fiducial.setSideBoard(this);
      fiducial.setFiducialType(fiducialType);
      setNewFiducialName(fiducial, null, fiducialType.getName());
    }
  }

  /**
   * @return a list of Component objects in order by name
   * @author Keith Lee
   */
  public List<Component> getComponents()
  {
    Assert.expect(_refDesToComponentMap != null);
    return new ArrayList<Component>(_refDesToComponentMap.values());
  }

  /**
   * @author George A. David
   */
  public boolean hasComponent(String referenceDesignator)
  {
    Assert.expect(referenceDesignator != null);
    Assert.expect(_refDesToComponentMap != null);

    return _refDesToComponentMap.containsKey(referenceDesignator);
  }

  /**
   * @author Keith Lee
   */
  public Component getComponent(String referenceDesignator)
  {
    Assert.expect(referenceDesignator != null);
    Assert.expect(_refDesToComponentMap != null);

    Component component = _refDesToComponentMap.get(referenceDesignator);
    Assert.expect(component != null);
    return component;
  }

  /**
   * This method will check to see if this sideboard contains a component.
   * @author Bill Darbie
   * @author Erica Wheatcroft
   */
  public boolean hasComponent(Component component)
  {
    Assert.expect(component != null);
    Assert.expect(_refDesToComponentMap != null);

    return _refDesToComponentMap.containsValue(component);
  }

  /**
   * Remove the component from this sideboard.
   * @author Bill Darbie
   * @author Erica Wheatcroft
   */
  public void removeComponent(Component component)
  {
    Assert.expect(component != null);
    Assert.expect(_refDesToComponentMap != null);

    _projectObservable.setEnabled(false);
    try
    {
      String referenceDesignator = component.getReferenceDesignator();
      Object prev = _refDesToComponentMap.remove(referenceDesignator);
      Assert.expect(prev != null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, component, null, SideBoardEventEnum.ADD_OR_REMOVE_COMPONENT);
  }

  /**
   * @return all the Subtype objects for this board
   * @author Bill Darbie
   */
  public List<Subtype> getSubtypes()
  {
    Assert.expect(_refDesToComponentMap != null);

    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    for (Component component : _refDesToComponentMap.values())
    {
      List<Pad> pads = component.getPads();
      for (Pad pad : pads)
      {
        Subtype subtype = pad.getSubtype();
        subtypeSet.add(subtype);
      }
    }

    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @return a List of getInspecionFamilyEnums of all the generic algorithm families
   * on this board type
   * @author Bill Darbie
   */
  public List<InspectionFamilyEnum> getInspectionFamilyEnums()
  {
    List<Subtype> subtypes = getSubtypes();

    Set<InspectionFamilyEnum> algoFamilyEnumSet = new HashSet<InspectionFamilyEnum>();  // use a set to avoid having duplicates in the list
    for (Subtype subtype : subtypes)
    {
      InspectionFamily inspectionFamily = subtype.getInspectionFamily();
      InspectionFamilyEnum inspectionFamilyEnum = inspectionFamily.getInspectionFamilyEnum();
      algoFamilyEnumSet.add(inspectionFamilyEnum);
    }

    List<InspectionFamilyEnum> algoFamilyEnums = new ArrayList<InspectionFamilyEnum>(algoFamilyEnumSet);

    return algoFamilyEnums;
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getInspectedSubtypes()
  {
    return getSideBoardType().getInspectedSubtypes();
  }

  /**
   * @author Andy Mechtenberg
   */
  void addFiducial(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);
    Assert.expect(_fiducialNameToFiducialMap != null);

    _projectObservable.setEnabled(false);
    try
    {
      Fiducial prev = _fiducialNameToFiducialMap.put(fiducial.getName(), fiducial);
      Assert.expect(prev == null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, fiducial, SideBoardEventEnum.ADD_OR_REMOVE_FIDUCIAL);
  }


  /**
   * @author Bill Darbie
   */
  void removeFiducial(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);
    Assert.expect(_fiducialNameToFiducialMap != null);

    _projectObservable.setEnabled(false);
    try
    {
      Fiducial prev = _fiducialNameToFiducialMap.remove(fiducial);
      Assert.expect(prev != null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, fiducial, null, SideBoardEventEnum.ADD_OR_REMOVE_FIDUCIAL);
  }

  /**
   * @author Bill Darbie
   */
  public List<Fiducial> getFiducials()
  {
    Assert.expect(_fiducialNameToFiducialMap != null);

    return new ArrayList<Fiducial>(_fiducialNameToFiducialMap.values());
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasFiducial(String fiducialName)
  {
    Assert.expect(fiducialName != null);

    return _fiducialNameToFiducialMap.containsKey(fiducialName);
  }

  /**
   * @author Bill Darbie
   */
  public Fiducial getFiducial(String fiducialName)
  {
    Assert.expect(fiducialName != null);

    Fiducial fiducial = _fiducialNameToFiducialMap.get(fiducialName);
    Assert.expect(fiducial !=  null);
    return fiducial;
  }

  /**
   * @author Bill Darbie
   */
  void setNewFiducialName(Fiducial fiducial, String oldName, String newName)
  {
    Assert.expect(fiducial != null);
    Assert.expect(newName != null);

    Fiducial prev = null;
    if (oldName != null)
    {
      prev = _fiducialNameToFiducialMap.remove(oldName);
      Assert.expect(prev != null);
    }
    prev = _fiducialNameToFiducialMap.put(newName, fiducial);
    Assert.expect(prev == null);

    if (oldName == null)
      _projectObservable.stateChanged(this, null, fiducial, SideBoardEventEnum.ADD_OR_REMOVE_FIDUCIAL);
  }

  /**
   * @author Bill Darbie
   */
  void setNewComponentName(Component component, String oldName, String newName)
  {
    Assert.expect(component != null);
    Assert.expect(newName != null);

    Component prev = null;
    if (oldName != null)
    {
      prev = _refDesToComponentMap.remove(oldName);
      Assert.expect(prev != null);
    }
    prev = _refDesToComponentMap.put(newName, component);
    Assert.expect(prev == null);

    if (oldName == null)
      _projectObservable.stateChanged(this, null, component, SideBoardEventEnum.ADD_OR_REMOVE_COMPONENT);
  }

  /**
   * @author Bill Darbie
   */
  public boolean willComponentsBeInsideBoardOutlineAfterOffset(int xOffsetAfterAllFlipsAndRotationsInNanoMeters, int yOffsetAfterAllFlipsAndRotationsInNanometers)
  {
    //  I will put in the brute force call here.  If this is too slow then I will need to do this:
    //  - SideBoard.isOffsetOkay() will be implemented by:
    //    - I will store a rectangle that is the bounds of all pads (maybe
    //      use component shape for speed here) - invalidate this
    //      when necessary based on ProjectState
    //    - do a call to see if pad rectangle is within board rectangle

    boolean compsInsideBoard = true;

    _projectObservable.setEnabled(false);
    try
    {
      // we will temporarily move the components to see if it falls outside the board outline.
      // suppress sending any ProjectStateEvents that might occur because of this since we will
      // immediatly return the components back to its original position
      getBoard().getPanel().getProject().getProjectState().disableProjectStateEvents();

      try
      {
        for (Component component : getComponents())
        {
          compsInsideBoard = component.willComponentBeInsideBoardOutlineAfterOffset(xOffsetAfterAllFlipsAndRotationsInNanoMeters,
                                                                                    yOffsetAfterAllFlipsAndRotationsInNanometers);
          if (compsInsideBoard == false)
            break;
        }
      }
      finally
      {
        getBoard().getPanel().getProject().getProjectState().enableProjectStateEvents();
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, SideBoardEventEnum.MOVE_BY_OFFSET);

    return compsInsideBoard;
  }

  /**
   * Move all the Components ComponentType by the offset passed in.  The offset passed in is an offset of the
   * Component as views from x-rays after all rotations and flips have occured.
   *
   * @author Bill Darbie
   */
  public void moveByOffSetInNanoMeters(int xOffsetAfterAllFlipsAndRotationsInNanoMeters, int yOffsetAfterAllFlipsAndRotationsInNanometers)
  {
    _projectObservable.setEnabled(false);
    try
    {
      for (Component component : getComponents())
      {
        component.moveByOffSetInNanoMeters(xOffsetAfterAllFlipsAndRotationsInNanoMeters, yOffsetAfterAllFlipsAndRotationsInNanometers);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    // use null for both orig and new values because the component calls will track state instead of this one.
    _projectObservable.stateChanged(this, null, null, SideBoardEventEnum.MOVE_BY_OFFSET);
  }

  /**
   * Convert a coordinate in renderer space to BoardCoordinate space.
   * @author Bill Darbie
   */
  public BoardCoordinate getBoardCoordinate(BoardCoordinate rendererCoordinate)
  {
    Assert.expect(rendererCoordinate != null);

    AffineTransform transform = AffineTransform.getTranslateInstance(0, 0);
    getBoard().preConcatenateShapeTransform(transform);

    // use the inverse of that transform to figure out the correct offset
    Point2D sourcePoint = new Point2D.Double(rendererCoordinate.getX(), rendererCoordinate.getY());
    Point2D destPoint = new Point2D.Double(0, 0);
    try
    {
      transform.inverseTransform(sourcePoint, destPoint);
    }
    catch (NoninvertibleTransformException nte)
    {
      Assert.expect(false);
    }

    int x = MathUtil.roundDoubleToInt(destPoint.getX());
    int y = MathUtil.roundDoubleToInt(destPoint.getY());

    return new BoardCoordinate(x, y);
  }
}
