package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class PanelEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static PanelEventEnum CREATE_OR_DESTROY = new PanelEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static PanelEventEnum PROJECT = new PanelEventEnum(++_index);
  public static PanelEventEnum NAME = new PanelEventEnum(++_index);
  public static PanelEventEnum CAD_VERSION = new PanelEventEnum(++_index);
  public static PanelEventEnum WIDTH = new PanelEventEnum(++_index);
  public static PanelEventEnum WIDTH_RETAINING_BOARD_COORDS = new PanelEventEnum(++_index);
  public static PanelEventEnum LENGTH = new PanelEventEnum(++_index);
  public static PanelEventEnum LENGTH_RETAINING_BOARD_COORDS = new PanelEventEnum(++_index);
  public static PanelEventEnum ADD_OR_REMOVE_BOARD = new PanelEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static PanelEventEnum ADD_OR_REMOVE_BOARD_TYPE = new PanelEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static PanelEventEnum PANEL_SETTINGS = new PanelEventEnum(++_index);
  public static PanelEventEnum THICKNESS = new PanelEventEnum(++_index);
  public static PanelEventEnum ADD_OR_REMOVE_COMP_PACKAGE = new PanelEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static PanelEventEnum ADD_OR_REMOVE_FIDUCIAL = new PanelEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static PanelEventEnum ADD_OR_REMOVE_LAND_PATTERN = new PanelEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static PanelEventEnum ADD_OR_REMOVE_SUBTYPE = new PanelEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static PanelEventEnum PANEL_SURFACE_MAP_SETTINGS = new PanelEventEnum(++_index);
  public static PanelEventEnum PANEL_ALIGNMENT_SURFACE_MAP_SETTINGS = new PanelEventEnum(++_index);
  public static PanelEventEnum PSP_SETTINGS = new PanelEventEnum(++_index);
  public static PanelEventEnum PANEL_MESH_SETTINGS = new PanelEventEnum(++_index);
  //Khaw Chek Hau - XCR3554: Package on package (PoP) development
  public static PanelEventEnum ADD_OR_REMOVE_COMP_PACKAGE_ON_PACKAGE = new PanelEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static PanelEventEnum PANEL_SURFACE_MODEL_SETTINGS = new PanelEventEnum(++_index); //Siew Yeng - XCR-3781

  /**
   * @author Bill Darbie
   */
  private PanelEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private PanelEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
