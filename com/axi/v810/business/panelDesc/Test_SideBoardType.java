package com.axi.v810.business.panelDesc;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
class Test_SideBoardType extends UnitTest
{
  /**
   * @author Keith Lee
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_SideBoardType());
  }

  /**
   * @author Keith Lee
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testGenericMethods();
    testCreateDestroyMethods();
  }

  /**
   * @author Bill Darbie
   */
  private void testGenericMethods()
  {
    SideBoardType sideBoardType = new SideBoardType();

    // test setBoard and getBoard
    try
    {
      sideBoardType.getBoardType();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      sideBoardType.setBoardType(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    BoardType boardType = new BoardType();
    sideBoardType.setBoardType(boardType);
    Expect.expect(sideBoardType.getBoardType() == boardType);

    // test addComponent, getComponents, hasComponent(name), getComponent(name), hasCompnent(Component), removeComponent(Component)
    try
    {
      sideBoardType.addComponentType(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    Expect.expect(sideBoardType.getComponentTypes().size() == 0);
    Panel panel = new Panel();
    panel.disableChecksForNdfParsers();
    ComponentType componentType = new ComponentType();
    componentType.setPanel(panel);
    String componentTypeName = "U1";
    componentType.setReferenceDesignator(componentTypeName);
    sideBoardType.addComponentType(componentType);
    Expect.expect(sideBoardType.getComponentTypes().size() == 1);
    Expect.expect(sideBoardType.getComponentTypes().get(0) == componentType);
    // add it again and catch the error
    try
    {
      sideBoardType.addComponentType(componentType);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    componentType = null;
    componentTypeName = null;

    try
    {
      sideBoardType.hasComponentType(componentType);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      sideBoardType.hasComponentType(componentTypeName);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    sideBoardType = new SideBoardType();
    componentType = new ComponentType();
    componentType.setPanel(panel);
    ComponentType componentType2 = new ComponentType();
    componentType2.setPanel(panel);
    componentTypeName = "U1";
    String componentTypeName2 = "U4";
    componentType.setReferenceDesignator(componentTypeName);
    componentType2.setReferenceDesignator(componentTypeName2);

    sideBoardType.addComponentType(componentType);
    // Component version
    Expect.expect(sideBoardType.hasComponentType(componentType) == true);
    Expect.expect(sideBoardType.hasComponentType(componentType2) == false);
    // string version
    Expect.expect(sideBoardType.hasComponentType(componentTypeName) == true);
    Expect.expect(sideBoardType.hasComponentType(componentTypeName2) == false);

    try
    {
      sideBoardType.removeComponentType(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      sideBoardType.removeComponentType(componentType2);  // has not been added
    }
    catch(AssertException e)
    {
      // do nothing
    }
    Expect.expect(sideBoardType.getComponentTypes().size() == 1);
    sideBoardType.removeComponentType(componentType);
    Expect.expect(sideBoardType.getComponentTypes().size() == 0);
    Expect.expect(sideBoardType.hasComponentType(componentType) == false);
    Expect.expect(sideBoardType.hasComponentType(componentTypeName) == false);


  }

  /**
   * @author Bill Darbie
   */
  void testCreateDestroyMethods()
  {
    try
    {
      // test createFiducial
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();
      Project project = Project.importProjectFromNdfs("smallPanel", warnings);
      Panel panel = project.getPanel();

      Board board = panel.getBoards().get(0);
      SideBoardType sideBoardType = board.getSideBoard1().getSideBoardType();
      int numFiducialTypes = sideBoardType.getFiducialTypes().size();

      // create the FiducialType
      FiducialType fiducialType = sideBoardType.createFiducialType("newFiducial", new BoardCoordinate(0,0));
      Assert.expect(sideBoardType.getFiducialTypes().size() == numFiducialTypes + 1);

      // destroy the FiducialType
      fiducialType.destroy();
      Assert.expect(sideBoardType.getFiducialTypes().size() == numFiducialTypes);

      // create the FiducialType, so we can remove and add it back
      fiducialType = sideBoardType.createFiducialType("newFiducial", new BoardCoordinate(0, 0));
      Assert.expect(sideBoardType.getFiducialTypes().size() == numFiducialTypes + 1);
      // remove the FiducialType
      fiducialType.remove();
      Assert.expect(sideBoardType.getFiducialTypes().size() == numFiducialTypes);
      fiducialType.add();
      Assert.expect(sideBoardType.getFiducialTypes().size() == numFiducialTypes + 1);

      // create ComponentType
      LandPattern landPattern = panel.getAllLandPatterns().get(0);
      int numComponentTypes = sideBoardType.getComponentTypes().size();
      int numComponents = panel.getNumComponents();
      ComponentType componentType = sideBoardType.createComponentType("newRefDes", landPattern, new BoardCoordinate(0,0), 0.0);
      Assert.expect(sideBoardType.getComponentTypes().size() == numComponentTypes + 1);
      Assert.expect(panel.getNumComponents() == numComponents + 2);

      // destroy ComponentType
      componentType.destroy();
      Assert.expect(sideBoardType.getComponentTypes().size() == numComponentTypes);
      Assert.expect(panel.getNumComponents() == numComponents);

      // create a component again so we can remove and add it
      componentType = sideBoardType.createComponentType("newRefDes", landPattern, new BoardCoordinate(0,0), 0.0);
      Assert.expect(sideBoardType.getComponentTypes().size() == numComponentTypes + 1);
      Assert.expect(panel.getNumComponents() == numComponents + 2);

      // remove ComponentType
      componentType.remove();
      Assert.expect(sideBoardType.getComponentTypes().size() == numComponentTypes);
      Assert.expect(panel.getNumComponents() == numComponents);

      // add ComponentType
      componentType.add();
      Assert.expect(sideBoardType.getComponentTypes().size() == numComponentTypes + 1);
      Assert.expect(panel.getNumComponents() == numComponents + 2);

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
