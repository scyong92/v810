package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class PackagePinEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static PackagePinEventEnum CREATE_OR_DESTROY = new PackagePinEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static PackagePinEventEnum COMP_PACKAGE = new PackagePinEventEnum(++_index);
  public static PackagePinEventEnum LAND_PATTERN_PAD = new PackagePinEventEnum(++_index);
  public static PackagePinEventEnum PAD_ORIENTATION_ENUM = new PackagePinEventEnum(++_index);
  public static PackagePinEventEnum JOINT_TYPE_ENUM = new PackagePinEventEnum(++_index);
  public static PackagePinEventEnum CUSTOM_JOINT_HEIGHT = new PackagePinEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private PackagePinEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private PackagePinEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
