package com.axi.v810.business.panelDesc;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class Test_LandPattern extends UnitTest
{
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_LandPattern());
  }

  /**
   * @author Keith Lee
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testGenericMethods();
    testOnePadOrientation();
    testTwoPadsOrientation();
    testRowOfPadsOrientation();
    testColumnOfPadsOrientation();
    testSquarePackageRotation();
    testTallRectanglePackageRotation();
    testLongRectanglePackageRotation();

    testSwapAndCreateDestroyMethods();
    testPitchAndIPDAndSubtypeAssignment();
  }

  /**
   * @author Bill Darbie
   */
  private void testGenericMethods()
  {
    LandPattern landPattern = new LandPattern();

    // getName and setName
    try
    {
      landPattern.getName();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    try
    {
      landPattern.setName(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    String name = "Test";
    Panel panel = new Panel();
    panel.disableChecksForNdfParsers();
    landPattern.setPanel(panel);
    landPattern.setName(name);
    Expect.expect(landPattern.getName().equals(name));

    //test addLandPatternPad and getLandPatternPads
    List<LandPatternPad> landPatternPads = landPattern.getLandPatternPads();
    Expect.expect(landPatternPads.isEmpty());
    SurfaceMountLandPatternPad surfaceMountPad = new SurfaceMountLandPatternPad();
    surfaceMountPad.setLandPattern(landPattern);
    surfaceMountPad.setName("1");
    landPatternPads = landPattern.getLandPatternPads();
    Expect.expect(landPatternPads.size() == 1);
    Expect.expect(landPatternPads.get(0) == surfaceMountPad);


    // test calculatePadOrientation()
    // to do this, we'll break it into the different test cases:
    // 1.  A 1 pad package
    // 2.  A 2 pad package
    // 3.  A Row of pads
    // 4.  A Column of pads
    // 5.  A square package
    // 6.  A tall rectangular package
    // 7.  A long rectangular package
  }

  /**
   * @author Andy Mechtenberg
   */
  void testOnePadOrientation()
  {
    Panel panel = new Panel();
    panel.disableChecksForNdfParsers();
    LandPattern landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("name");

    LandPatternPad landPatternPad = new SurfaceMountLandPatternPad();
    landPatternPad.setLandPattern(landPattern);
    landPatternPad.setCoordinateInNanoMeters(new ComponentCoordinate(20, 20));
    landPatternPad.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad.setName("1");
    landPatternPad.setDegreesRotation(0.0);

    landPattern.assignPinOrientations(); // x > 0
    Expect.expect(landPatternPad.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));

    landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("name");
    landPatternPad = new SurfaceMountLandPatternPad();
    landPatternPad.setLandPattern(landPattern);
    landPatternPad.setCoordinateInNanoMeters(new ComponentCoordinate( -3, 20));
    landPatternPad.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad.setName("2");
    landPatternPad.setDegreesRotation(0.0);
    landPattern.assignPinOrientations(); // x < 0, y > 0
    Expect.expect(landPatternPad.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));

    landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("name");
    landPatternPad = new SurfaceMountLandPatternPad();
    landPatternPad.setLandPattern(landPattern);
    landPatternPad.setCoordinateInNanoMeters(new ComponentCoordinate(0, 20));
    landPatternPad.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad.setName("3");
    landPatternPad.setDegreesRotation(0.0);
    landPattern.assignPinOrientations(); // x = 0, y > 0
    Expect.expect(landPatternPad.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));

    landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("name");
    landPatternPad = new SurfaceMountLandPatternPad();
    landPatternPad.setLandPattern(landPattern);
    landPatternPad.setCoordinateInNanoMeters(new ComponentCoordinate(0, 0));
    landPatternPad.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad.setName("4");
    landPatternPad.setDegreesRotation(0.0);
    landPattern.assignPinOrientations(); // x = 0, y = 0
    Expect.expect(landPatternPad.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));

    landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("name");
    landPatternPad = new SurfaceMountLandPatternPad();
    landPatternPad.setLandPattern(landPattern);
    landPatternPad.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad.setCoordinateInNanoMeters(new ComponentCoordinate(0, -4));
    landPatternPad.setName("5");
    landPatternPad.setDegreesRotation(0.0);
    landPattern.assignPinOrientations(); // x = 0, y < 0
    Expect.expect(landPatternPad.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));
  }

  /**
   * @author Andy Mechtenberg
   */
  void testTwoPadsOrientation()
  {
    Panel panel = new Panel();
    panel.disableChecksForNdfParsers();

    LandPattern landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("name");

    LandPatternPad landPatternPad1 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad2 = new SurfaceMountLandPatternPad();

    landPatternPad1.setLandPattern(landPattern);
    landPatternPad2.setLandPattern(landPattern);

    landPatternPad1.setName("1");
    landPatternPad2.setName("2");

    landPatternPad1.setCoordinateInNanoMeters(new ComponentCoordinate( -10, 0));
    landPatternPad1.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad1.setDegreesRotation(0.0);
    landPatternPad2.setCoordinateInNanoMeters(new ComponentCoordinate(0, 0));
    landPatternPad2.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad2.setDegreesRotation(0.0);
    landPattern.assignPinOrientations();
    Expect.expect(landPatternPad1.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));
    Expect.expect(landPatternPad2.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));

    landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("name");

    landPatternPad1 = new SurfaceMountLandPatternPad();
    landPatternPad2 = new SurfaceMountLandPatternPad();

    landPatternPad1.setLandPattern(landPattern);
    landPatternPad2.setLandPattern(landPattern);

    landPatternPad1.setName("1");
    landPatternPad2.setName("2");

    landPatternPad1.setCoordinateInNanoMeters(new ComponentCoordinate(0, -10));
    landPatternPad1.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad1.setDegreesRotation(0.0);
    landPatternPad2.setCoordinateInNanoMeters(new ComponentCoordinate(0, 0));
    landPatternPad2.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad2.setDegreesRotation(0.0);
    landPattern.assignPinOrientations();
    Expect.expect(landPatternPad1.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));
    Expect.expect(landPatternPad2.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));

  }

  /**
   * @author Andy Mechtenberg
   */
  void testRowOfPadsOrientation()
  {
    Panel panel = new Panel();
    panel.disableChecksForNdfParsers();
    // case 1, row of pads, y is <= 0
    LandPattern landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("name");

    LandPatternPad landPatternPad1 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad2 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad3 = new SurfaceMountLandPatternPad();

    landPatternPad1.setLandPattern(landPattern);
    landPatternPad2.setLandPattern(landPattern);
    landPatternPad3.setLandPattern(landPattern);

    landPatternPad1.setName("1");
    landPatternPad2.setName("2");
    landPatternPad3.setName("3");

    landPatternPad1.setCoordinateInNanoMeters(new ComponentCoordinate( -10, 0));
    landPatternPad1.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad2.setCoordinateInNanoMeters(new ComponentCoordinate(0, 0));
    landPatternPad2.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad3.setCoordinateInNanoMeters(new ComponentCoordinate(10, 0));
    landPatternPad3.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad1.setWidthInNanoMeters(10);
    landPatternPad1.setLengthInNanoMeters(5);
    landPatternPad1.setDegreesRotation(0.0);
    landPatternPad2.setWidthInNanoMeters(10);
    landPatternPad2.setLengthInNanoMeters(5);
    landPatternPad2.setDegreesRotation(0.0);
    landPatternPad3.setWidthInNanoMeters(10);
    landPatternPad3.setLengthInNanoMeters(5);
    landPatternPad3.setDegreesRotation(0.0);
    landPattern.assignPinOrientations();
    Expect.expect(landPatternPad1.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));
    Expect.expect(landPatternPad2.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));
    Expect.expect(landPatternPad3.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));

    // case 2, row of pads y > 0
    landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("name");

    landPatternPad1 = new SurfaceMountLandPatternPad();
    landPatternPad2 = new SurfaceMountLandPatternPad();
    landPatternPad3 = new SurfaceMountLandPatternPad();

    landPatternPad1.setLandPattern(landPattern);
    landPatternPad2.setLandPattern(landPattern);
    landPatternPad3.setLandPattern(landPattern);

    landPatternPad1.setName("1");
    landPatternPad2.setName("2");
    landPatternPad3.setName("3");

    landPatternPad1.setCoordinateInNanoMeters(new ComponentCoordinate( -10, 10));
    landPatternPad1.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad2.setCoordinateInNanoMeters(new ComponentCoordinate(0, 10));
    landPatternPad2.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad3.setCoordinateInNanoMeters(new ComponentCoordinate(10, 10));
    landPatternPad3.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad1.setWidthInNanoMeters(10);
    landPatternPad1.setLengthInNanoMeters(5);
    landPatternPad1.setDegreesRotation(0.0);
    landPatternPad2.setWidthInNanoMeters(10);
    landPatternPad2.setLengthInNanoMeters(5);
    landPatternPad2.setDegreesRotation(0.0);
    landPatternPad3.setWidthInNanoMeters(10);
    landPatternPad3.setLengthInNanoMeters(5);
    landPatternPad3.setDegreesRotation(0.0);

    landPattern.assignPinOrientations();
    Expect.expect(landPatternPad1.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));
    Expect.expect(landPatternPad2.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));
    Expect.expect(landPatternPad3.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));
  }

  /**
   * @author Andy Mechtenberg
   */
  void testColumnOfPadsOrientation()
  {
    Panel panel = new Panel();
    panel.disableChecksForNdfParsers();
    // case 1, column of pads, x is <= 0
    LandPattern landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("name");

    LandPatternPad landPatternPad1 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad2 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad3 = new SurfaceMountLandPatternPad();

    landPatternPad1.setLandPattern(landPattern);
    landPatternPad2.setLandPattern(landPattern);
    landPatternPad3.setLandPattern(landPattern);

    landPatternPad1.setName("1");
    landPatternPad2.setName("2");
    landPatternPad3.setName("3");

    landPatternPad1.setCoordinateInNanoMeters(new ComponentCoordinate(0, -10));
    landPatternPad1.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad2.setCoordinateInNanoMeters(new ComponentCoordinate(0, 0));
    landPatternPad2.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad3.setCoordinateInNanoMeters(new ComponentCoordinate(0, 10));
    landPatternPad3.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad1.setWidthInNanoMeters(10);
    landPatternPad1.setLengthInNanoMeters(5);
    landPatternPad1.setDegreesRotation(0.0);
    landPatternPad2.setWidthInNanoMeters(10);
    landPatternPad2.setLengthInNanoMeters(5);
    landPatternPad2.setDegreesRotation(0.0);
    landPatternPad3.setWidthInNanoMeters(10);
    landPatternPad3.setLengthInNanoMeters(5);
    landPatternPad3.setDegreesRotation(0.0);

    landPattern.assignPinOrientations();
    Expect.expect(landPatternPad1.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));
    Expect.expect(landPatternPad2.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));
    Expect.expect(landPatternPad3.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));

    // case 2, row of pads x > 0
    landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("name");

    landPatternPad1 = new SurfaceMountLandPatternPad();
    landPatternPad2 = new SurfaceMountLandPatternPad();
    landPatternPad3 = new SurfaceMountLandPatternPad();

    landPatternPad1.setLandPattern(landPattern);
    landPatternPad2.setLandPattern(landPattern);
    landPatternPad3.setLandPattern(landPattern);

    landPatternPad1.setName("1");
    landPatternPad2.setName("2");
    landPatternPad3.setName("3");

    landPatternPad1.setCoordinateInNanoMeters(new ComponentCoordinate(10, -10));
    landPatternPad1.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad2.setCoordinateInNanoMeters(new ComponentCoordinate(10, 0));
    landPatternPad2.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad3.setCoordinateInNanoMeters(new ComponentCoordinate(10, 10));
    landPatternPad3.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad1.setWidthInNanoMeters(10);
    landPatternPad1.setLengthInNanoMeters(5);
    landPatternPad1.setDegreesRotation(0.0);
    landPatternPad2.setWidthInNanoMeters(10);
    landPatternPad2.setLengthInNanoMeters(5);
    landPatternPad2.setDegreesRotation(0.0);
    landPatternPad3.setWidthInNanoMeters(10);
    landPatternPad3.setLengthInNanoMeters(5);
    landPatternPad3.setDegreesRotation(0.0);

    landPattern.assignPinOrientations();
    Expect.expect(landPatternPad1.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));
    Expect.expect(landPatternPad2.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));
    Expect.expect(landPatternPad3.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));
  }

  /**
   * @author Andy Mechtenberg
   */
  void testSquarePackageRotation()
  {
    Panel panel = new Panel();
    panel.disableChecksForNdfParsers();

    // first case is a simple 4 pad package
    LandPattern landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("name");

    LandPatternPad landPatternPad1 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad2 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad3 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad4 = new SurfaceMountLandPatternPad();

    landPatternPad1.setLandPattern(landPattern);
    landPatternPad2.setLandPattern(landPattern);
    landPatternPad3.setLandPattern(landPattern);
    landPatternPad4.setLandPattern(landPattern);

    landPatternPad1.setName("1");
    landPatternPad2.setName("2");
    landPatternPad3.setName("3");
    landPatternPad4.setName("4");

    landPatternPad1.setCoordinateInNanoMeters(new ComponentCoordinate( -10, -10));
    landPatternPad1.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad1.setDegreesRotation(0.0);
    landPatternPad2.setCoordinateInNanoMeters(new ComponentCoordinate(10, 0));
    landPatternPad2.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad2.setDegreesRotation(0.0);
    landPatternPad3.setCoordinateInNanoMeters(new ComponentCoordinate(0, 10));
    landPatternPad3.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad3.setDegreesRotation(0.0);
    landPatternPad4.setCoordinateInNanoMeters(new ComponentCoordinate(10, 10));
    landPatternPad4.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad4.setDegreesRotation(0.0);

    landPattern.assignPinOrientations();
    Expect.expect(landPatternPad1.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));
    Expect.expect(landPatternPad2.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));
    Expect.expect(landPatternPad3.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));
    Expect.expect(landPatternPad4.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));
  }

  /**
   * @author Andy Mechtenberg
   */
  void testTallRectanglePackageRotation()
  {
    Panel panel = new Panel();
    panel.disableChecksForNdfParsers();

    // next case is long rectangle
    LandPattern landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("name");

    LandPatternPad landPatternPad1 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad2 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad3 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad4 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad5 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad6 = new SurfaceMountLandPatternPad();

    landPatternPad1.setLandPattern(landPattern);
    landPatternPad2.setLandPattern(landPattern);
    landPatternPad3.setLandPattern(landPattern);
    landPatternPad4.setLandPattern(landPattern);
    landPatternPad5.setLandPattern(landPattern);
    landPatternPad6.setLandPattern(landPattern);

    landPatternPad1.setName("1");
    landPatternPad2.setName("2");
    landPatternPad3.setName("3");
    landPatternPad4.setName("4");
    landPatternPad5.setName("5");
    landPatternPad6.setName("6");

    // pads like this:
    // 1 2
    // 3 4
    // 5 6
    landPatternPad1.setCoordinateInNanoMeters(new ComponentCoordinate( -10, 10));
    landPatternPad1.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad2.setCoordinateInNanoMeters(new ComponentCoordinate(10, 10));
    landPatternPad2.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad3.setCoordinateInNanoMeters(new ComponentCoordinate( -10, 0));
    landPatternPad3.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad4.setCoordinateInNanoMeters(new ComponentCoordinate(10, 0));
    landPatternPad4.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad5.setCoordinateInNanoMeters(new ComponentCoordinate( -10, -10));
    landPatternPad5.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad6.setCoordinateInNanoMeters(new ComponentCoordinate(10, -10));
    landPatternPad6.setShapeEnum(ShapeEnum.RECTANGLE);

    // set width and length
    landPatternPad1.setWidthInNanoMeters(10);
    landPatternPad1.setLengthInNanoMeters(5);
    landPatternPad1.setDegreesRotation(0.0);
    landPatternPad2.setWidthInNanoMeters(10);
    landPatternPad2.setLengthInNanoMeters(5);
    landPatternPad2.setDegreesRotation(0.0);
    landPatternPad3.setWidthInNanoMeters(10);
    landPatternPad3.setLengthInNanoMeters(5);
    landPatternPad3.setDegreesRotation(0.0);
    landPatternPad4.setWidthInNanoMeters(10);
    landPatternPad4.setLengthInNanoMeters(5);
    landPatternPad4.setDegreesRotation(0.0);
    landPatternPad5.setWidthInNanoMeters(10);
    landPatternPad5.setLengthInNanoMeters(5);
    landPatternPad5.setDegreesRotation(0.0);
    landPatternPad6.setWidthInNanoMeters(10);
    landPatternPad6.setLengthInNanoMeters(5);
    landPatternPad6.setDegreesRotation(0.0);


    landPattern.assignPinOrientations();
    Expect.expect(landPatternPad1.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));
    Expect.expect(landPatternPad2.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));
    Expect.expect(landPatternPad3.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));
    Expect.expect(landPatternPad4.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));
    Expect.expect(landPatternPad5.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN));
    Expect.expect(landPatternPad6.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN));
  }


  /**
   * @author Andy Mechtenberg
   */
  void testLongRectanglePackageRotation()
  {
    Panel panel = new Panel();
    panel.disableChecksForNdfParsers();

    // next case is long rectangle
    LandPattern landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("name");

    LandPatternPad landPatternPad1 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad2 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad3 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad4 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad5 = new SurfaceMountLandPatternPad();
    LandPatternPad landPatternPad6 = new SurfaceMountLandPatternPad();

    landPatternPad1.setLandPattern(landPattern);
    landPatternPad2.setLandPattern(landPattern);
    landPatternPad3.setLandPattern(landPattern);
    landPatternPad4.setLandPattern(landPattern);
    landPatternPad5.setLandPattern(landPattern);
    landPatternPad6.setLandPattern(landPattern);

    landPatternPad1.setName("1");
    landPatternPad2.setName("2");
    landPatternPad3.setName("3");
    landPatternPad4.setName("4");
    landPatternPad5.setName("5");
    landPatternPad6.setName("6");

    // pads like this:
    // 1 2 3
    // 4 5 6
    landPatternPad1.setCoordinateInNanoMeters(new ComponentCoordinate( -10, 10));
    landPatternPad1.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad2.setCoordinateInNanoMeters(new ComponentCoordinate(0, 10));
    landPatternPad2.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad3.setCoordinateInNanoMeters(new ComponentCoordinate(10, 10));
    landPatternPad3.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad4.setCoordinateInNanoMeters(new ComponentCoordinate( -10, -10));
    landPatternPad4.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad5.setCoordinateInNanoMeters(new ComponentCoordinate(0, -10));
    landPatternPad5.setShapeEnum(ShapeEnum.RECTANGLE);
    landPatternPad6.setCoordinateInNanoMeters(new ComponentCoordinate(10, -10));
    landPatternPad6.setShapeEnum(ShapeEnum.RECTANGLE);

    // set width and length
    landPatternPad1.setWidthInNanoMeters(10);
    landPatternPad1.setLengthInNanoMeters(5);
    landPatternPad1.setDegreesRotation(0.0);
    landPatternPad2.setWidthInNanoMeters(10);
    landPatternPad2.setLengthInNanoMeters(5);
    landPatternPad2.setDegreesRotation(0.0);
    landPatternPad3.setWidthInNanoMeters(10);
    landPatternPad3.setLengthInNanoMeters(5);
    landPatternPad3.setDegreesRotation(0.0);
    landPatternPad4.setWidthInNanoMeters(10);
    landPatternPad4.setLengthInNanoMeters(5);
    landPatternPad4.setDegreesRotation(0.0);
    landPatternPad5.setWidthInNanoMeters(10);
    landPatternPad5.setLengthInNanoMeters(5);
    landPatternPad5.setDegreesRotation(0.0);
    landPatternPad6.setWidthInNanoMeters(10);
    landPatternPad6.setLengthInNanoMeters(5);
    landPatternPad6.setDegreesRotation(0.0);

    landPattern.assignPinOrientations();
    Expect.expect(landPatternPad1.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));
    Expect.expect(landPatternPad2.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));
    Expect.expect(landPatternPad3.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN));
    Expect.expect(landPatternPad4.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));
    Expect.expect(landPatternPad5.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));
    Expect.expect(landPatternPad6.getPinOrientationEnum().equals(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN));
  }

  /**
   * @author Bill Darbie
   */
  private void testSwapAndCreateDestroyMethods()
  {
    try
    {
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();
      Project project = Project.importProjectFromNdfs("smallPanel", warnings);
      LandPattern landPattern = project.getPanel().getAllLandPatterns().get(0);
      LandPatternPad landPatternPad = landPattern.getLandPatternPads().get(0);
      Assert.expect(landPatternPad instanceof ThroughHoleLandPatternPad);

      // test createSurfaceMountLandPatternPad
      int numPads = landPattern.getLandPatternPads().size();
      LandPatternPad pad = landPattern.createSurfaceMountLandPatternPad(new ComponentCoordinate(0,0),
                                                                        0.0,
                                                                        MathUtil.convertMilsToNanoMetersInteger(30),
                                                                        MathUtil.convertMilsToNanoMetersInteger(30),
                                                                        ShapeEnum.RECTANGLE);
      Assert.expect(landPattern.getLandPatternPads().size() == numPads + 1);

      // test destroy
      pad.destroy();
      Assert.expect(landPattern.getLandPatternPads().size() == numPads);

      pad = landPattern.createSurfaceMountLandPatternPad(new ComponentCoordinate(0,0),
                                                                        0.0,
                                                                        MathUtil.convertMilsToNanoMetersInteger(30),
                                                                        MathUtil.convertMilsToNanoMetersInteger(30),
                                                                        ShapeEnum.RECTANGLE);
      Assert.expect(landPattern.getLandPatternPads().size() == numPads + 1);

      // test remove/add
      pad.remove();
      Assert.expect(landPattern.getLandPatternPads().size() == numPads);
      pad.add();
      Assert.expect(landPattern.getLandPatternPads().size() == numPads + 1);
      pad.destroy();


      // test createThroughHoleLandPatternPad
      pad = landPattern.createThroughHoleLandPatternPad(new ComponentCoordinate(0,0),
                                                        0.0,
                                                        MathUtil.convertMilsToNanoMetersInteger(30),
                                                        MathUtil.convertMilsToNanoMetersInteger(30),
                                                        ShapeEnum.RECTANGLE);
      Assert.expect(landPattern.getLandPatternPads().size() == numPads + 1);

      // test destroy
      pad.destroy();
      Assert.expect(landPattern.getLandPatternPads().size() == numPads);

      pad = landPattern.createThroughHoleLandPatternPad(new ComponentCoordinate(0,0),
                                                        0.0,
                                                        MathUtil.convertMilsToNanoMetersInteger(30),
                                                        MathUtil.convertMilsToNanoMetersInteger(30),
                                                        ShapeEnum.RECTANGLE);
      Assert.expect(landPattern.getLandPatternPads().size() == numPads + 1);

      // test remove/add
      pad.remove();
      Assert.expect(landPattern.getLandPatternPads().size() == numPads);
      pad.add();
      Assert.expect(landPattern.getLandPatternPads().size() == numPads + 1);


      // test createDuplicate
      Panel panel = new Panel();
      landPattern.setPanel(panel);
      LandPattern dupLandPattern = landPattern.createDuplicate("duplicateLandPatternName");
      Assert.expect(landPattern.getLandPatternPads().size() ==  dupLandPattern.getLandPatternPads().size());
      Assert.expect(landPattern.getLengthInNanoMeters() ==  dupLandPattern.getLengthInNanoMeters());
      Assert.expect(landPattern.getWidthInNanoMeters() ==  dupLandPattern.getWidthInNanoMeters());
      Assert.expect(landPattern.getPadTypeEnum() ==  dupLandPattern.getPadTypeEnum());
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void testPitchAndIPDAndSubtypeAssignment()
  {
    Set<String> refDesSet = new HashSet<String>();

    // components that we want to see information on
    refDesSet.add("U1731_C"); // SINGLE_PAD
    refDesSet.add("U59"); // CGA
    refDesSet.add("U8"); // BGA, pad one in center is false
    refDesSet.add("U4"); // JLEAD, also has a padOneInCenter
    refDesSet.add("C2"); // POLARIZED_CAPACITOR
    refDesSet.add("C1"); // CAPACITOR
    refDesSet.add("R1"); // RESISTOR
    refDesSet.add("J2"); // CONNECTOR
    refDesSet.add("CR1"); // GULLWING - also has 3 subtypes on 4 pads, and has 3 pitches for 4 pads
    refDesSet.add("FB11"); // MISC_DISCRETE

    // land patterns to show PadType information on
    Set<String> padInfoSet = new HashSet<String>();
    padInfoSet.add("C1");
    padInfoSet.add("C2");
    padInfoSet.add("CR1");
    padInfoSet.add("U1731_C");

    try
    {
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();
      Project project = Project.importProjectFromNdfs("cygnusAllFamilies", warnings);

      Set<LandPattern> landPatternSet = new HashSet<LandPattern>();
      Panel panel = project.getPanel();
      for (ComponentType componentType : panel.getComponentTypes())
      {
        String refDes = componentType.getReferenceDesignator();

        if (refDesSet.contains(refDes) == false)
          continue;

        System.out.println("component: " + refDes);
        LandPattern landPattern = componentType.getLandPattern();
        System.out.println("landPattern: " + landPattern.getName());

        // print out LandPattern info
        if (landPatternSet.add(landPattern))
        {
          System.out.println("  numRows: " + landPattern.getNumRows());
          System.out.println("  numCols: " + landPattern.getNumColumns());
          System.out.println("  padOneInCenter: " + landPattern.isPadOneInCenterOfRowOrColumn());
        }

        if (padInfoSet.contains(refDes))
        {
          for (LandPatternPad landPatternPad : landPattern.getLandPatternPads())
          {
            System.out.println("    pad: " + landPatternPad.getName());
            System.out.println("      padOne: " + landPatternPad.isPadOne());
            ComponentCoordinate coord = landPatternPad.getCoordinateInNanoMeters();
            System.out.println("      x: " + coord.getX());
            System.out.println("      y: " + coord.getY());
            System.out.println("      width: " + landPatternPad.getWidthInNanoMeters());
            System.out.println("      length: " + landPatternPad.getLengthInNanoMeters());
            System.out.println("      pitch: " + landPatternPad.getPitchInNanoMeters());
            System.out.println("      IPD: " + landPatternPad.getInterPadDistanceInNanoMeters());
            System.out.println("      orientation: " + landPatternPad.getPinOrientationEnum());
          }
        }

        if (componentType.getJointTypeEnums().size() == 1)
          System.out.println("  subtype for all pads: " + componentType.getPadTypes().get(0).getSubtype().getLongName());
        else
        {
          // print out Pad information
          for (PadType padType : componentType.getPadTypes())
            System.out.println("  padType: " + padType.getName() + " subtype: " + padType.getSubtype().getLongName());
        }
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
