package com.axi.v810.business.panelDesc;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class Test_PadType extends UnitTest
{

  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_PadType());
  }

  /**
   * @author Keith Lee
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    PadType padType = new PadType();

    // test setComponentType and getComponentType
    try
    {
      padType.setComponentType(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      padType.getComponentType();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    ComponentType componentType = new ComponentType();
    padType.setComponentType(componentType);
    Expect.expect(padType.getComponentType() == componentType);

    // test setLandPatternPad and getLandPatternpadType
    try
    {
      padType.setLandPatternPad(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      padType.getLandPatternPad();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    LandPatternPad landPatternPad = new SurfaceMountLandPatternPad();
    padType.setLandPatternPad(landPatternPad);
    Expect.expect(padType.getLandPatternPad() == landPatternPad);

    // test getPadTypeSettings and setPadTypeSettings
    try
    {
      padType.getPadTypeSettings();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    try
    {
      padType.setPadTypeSettings(null);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    PadTypeSettings padTypeSettings = new PadTypeSettings();
    padType.setPadTypeSettings(padTypeSettings);
    Expect.expect(padType.getPadTypeSettings() == padTypeSettings);

    padType = new PadType();
    // test all methods that use _landPatternPad
    try
    {
      padType.getDegreesRotation();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      padType.getName();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      padType.getWidthInNanoMeters();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      padType.getLengthInNanoMeters();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      padType.getShapeEnum();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      padType.getCoordinateInNanoMeters();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    // now test methods that use _landPatternPad after it has been set up
    // properly
    landPatternPad = new SurfaceMountLandPatternPad();
    landPatternPad.setLandPattern(new LandPattern());
    padType.setLandPatternPad(landPatternPad);

    // test getDegreesRotation
    landPatternPad.setDegreesRotation(90);
    Expect.expect(padType.getDegreesRotation() == 90);

    // test getWidthInNanoMeters
    int widthInNanoMeters = 10;
    landPatternPad.setWidthInNanoMeters(widthInNanoMeters);
    Expect.expect(padType.getWidthInNanoMeters() == widthInNanoMeters);

    // test getLengthInNanoMeters
    int lengthInNanoMeters = 11;
    landPatternPad.setLengthInNanoMeters(lengthInNanoMeters);
    Expect.expect(padType.getLengthInNanoMeters() == lengthInNanoMeters);

    // test getShapeEnum
    ShapeEnum shapeEnum = ShapeEnum.CIRCLE;
    landPatternPad.setShapeEnum(shapeEnum);
    Expect.expect(padType.getShapeEnum().equals(shapeEnum));

    // test getCoordinateInNanometers
    ComponentCoordinate panelCoordinate = new ComponentCoordinate(3, 4);
    landPatternPad.setCoordinateInNanoMeters(panelCoordinate);
    Expect.expect(padType.getCoordinateInNanoMeters().equals(panelCoordinate));

// wpd virgo
//    // test copy constructor
//    try
//    {
//      new Pad(null);
//      Expect.expect(false);
//    }
//    catch(AssertException ae)
//    {
//      // do nothing
//    }
//    pad = new Pad();
//    component = new Component();
//    padType.setComponent(component);
//    landPatternPad = new SurfaceMountLandPatternPad();
//    padType.setLandPatternPad(landPatternPad);
//    padSettings = new PadSettings();
//    padType.setPadSettings(padSettings);
//    PackagePin packagePin = new PackagePin();
//    padType.setPackagePin(packagePin);
//
//    Pad copy = new Pad(pad);
//    // copy should not copy the Component or the Joint
//    try
//    {
//      copy.getComponent();
//      Expect.expect(false);
//    }
//    catch(AssertException ae)
//    {
//      // do nothing
//    }
//
//    // can test this with == because the copy constructor does a shallow copy of landPatternPad
//    Expect.expect(padType.getLandPatternPad() == copy.getLandPatternPad());
//    Expect.expect(padType.getPackagePin() == copy.getPackagePin());
//
//    // PadSettings should be a different object
//    Expect.expect(padType.getPadSettings() != copy.getPadSettings());

    // testing of getShapeInNanometers is done in Test_Panel.java
  }
}
