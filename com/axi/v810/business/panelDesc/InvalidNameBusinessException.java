package com.axi.v810.business.panelDesc;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class InvalidNameBusinessException extends BusinessException
{
  /**
   * @author Bill Darbie
   */
  public InvalidNameBusinessException(String componentName, String name, String invalidChars)
  {
    super(new LocalizedString("BUS_INVALID_NAME_EXCEPTION_KEY", new Object[]{componentName, name, invalidChars}));
    Assert.expect(componentName != null);
    Assert.expect(name != null);
    Assert.expect(invalidChars != null);
  }
}
