package com.axi.v810.business.panelDesc;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class DuplicateNameBusinessException extends BusinessException
{
  /**
   * @author Bill Darbie
   */
  public DuplicateNameBusinessException(String componentName, String name)
  {
    super(new LocalizedString("BUS_DUPLICATE_NAME_EXCEPTION_KEY", new Object[]{componentName, name}));
    Assert.expect(componentName != null);
    Assert.expect(name != null);
  }
}
