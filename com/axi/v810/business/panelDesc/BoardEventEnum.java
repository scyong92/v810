package com.axi.v810.business.panelDesc;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class BoardEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static BoardEventEnum CREATE_OR_DESTROY = new BoardEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static BoardEventEnum ADD_OR_REMOVE = new BoardEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static BoardEventEnum BOARD_TYPE = new BoardEventEnum(++_index);
  public static BoardEventEnum NAME = new BoardEventEnum(++_index);
  public static BoardEventEnum DEGREES_ROTATION = new BoardEventEnum(++_index);
  public static BoardEventEnum COORDINATE = new BoardEventEnum(++_index);
  public static BoardEventEnum LEFT_TO_RIGHT_FLIP = new BoardEventEnum(++_index);
  public static BoardEventEnum TOP_TO_BOTTOM_FLIP = new BoardEventEnum(++_index);
  public static BoardEventEnum SIDE_BOARD_1 = new BoardEventEnum(++_index);
  public static BoardEventEnum SIDE_BOARD_2 = new BoardEventEnum(++_index);
  public static BoardEventEnum BOARD_SETTINGS = new BoardEventEnum(++_index);
  public static BoardEventEnum BOARD_SURFACE_MAP_SETTINGS = new BoardEventEnum(++_index);
  public static BoardEventEnum BOARD_ALIGNMENT_SURFACE_MAP_SETTINGS = new BoardEventEnum(++_index);
  public static BoardEventEnum BOARD_MESH_SETTINGS = new BoardEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private BoardEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private BoardEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
