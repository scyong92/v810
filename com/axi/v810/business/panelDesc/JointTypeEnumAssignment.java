package com.axi.v810.business.panelDesc;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;


/**
 * @author Bill Darbie
 */
public class JointTypeEnumAssignment
{
  private static JointTypeEnumAssignment _instance;
  private static boolean _defaultFileWritten = false;
  private Config _config;
  private JointTypeAssignmentWriter _jointTypeAssignmentWriter;
  private JointTypeAssignmentReader _jointTypeAssignmentReader;

  private List<JointTypeEnumRule> _rules = new ArrayList<JointTypeEnumRule>();

  private Map<PadType, JointTypeEnum> _padTypeToJointTypeEnumMap = new HashMap<PadType, JointTypeEnum>();
  private JointTypeEnum _jointTypeEnum;
  private int _padSizeThresholdForSubtypeAssignmentPercent = 5;
  private Set<JointTypeEnum> _forceOneSubtypeForEntireComponentJointTypeSet = new HashSet<JointTypeEnum>();
  private boolean _forceOneSubtypeForEntireComponent = false;

  /**
   * @author Bill Darbie
   */
  public static synchronized JointTypeEnumAssignment getInstance()
  {
    if (_instance == null)
      _instance = new JointTypeEnumAssignment();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private JointTypeEnumAssignment()
  {
    _config = Config.getInstance();
    _jointTypeAssignmentWriter = JointTypeAssignmentWriter.getInstance();
    _jointTypeAssignmentReader = JointTypeAssignmentReader.getInstance();

    _forceOneSubtypeForEntireComponentJointTypeSet.add(JointTypeEnum.POLARIZED_CAP);
    _forceOneSubtypeForEntireComponentJointTypeSet.add(JointTypeEnum.CAPACITOR);
    _forceOneSubtypeForEntireComponentJointTypeSet.add(JointTypeEnum.RESISTOR);
    // Wei Chin (Tall Cap)
//    _forceOneSubtypeForEntireComponentJointTypeSet.add(JointTypeEnum.TALL_CAPACITOR);

    buildDefaultRules();
  }

  /**
   * @author Bill Darbie
   */
  public void save(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);
    _jointTypeAssignmentWriter.write(fileName, _rules);
  }

  /**
   * @author Bill Darbie
   */
  public void load() throws DatastoreException
  {
    String fileName = _config.getStringValue(SoftwareConfigEnum.JOINT_TYPE_ASSIGNMENT_FILENAME);
    Assert.expect(fileName.endsWith(FileName.getConfigFileExtension()));
    fileName = fileName.substring(0, fileName.length() - FileName.getConfigFileExtension().length());

    createDefaultFileIfNecessary();
    load(fileName);
  }

  /**
   * @author Bill Darbie
   */
  public void load(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);

    createDefaultFileIfNecessary();
    _rules = _jointTypeAssignmentReader.read(fileName);
  }

  /**
   * @author Bill Darbie
   */
  private void createDefaultFileIfNecessary() throws DatastoreException
  {
      buildDefaultRules();
      // Jen Ping - XCR-2809 - check and create directory if not exist
      if (FileUtilAxi.existsDirectory(Directory.getJointTypeAssignmentDir()) == false)
        FileUtilAxi.createDirectory(Directory.getJointTypeAssignmentDir());
      
      save(FileName.getJointTypeAssignmentDefaultFileWithoutExtension());
  }

  /**
   * @author Bill Darbie
   */
  public List<JointTypeEnumRule> getJointTypeEnumRules()
  {
    return new ArrayList<JointTypeEnumRule>(_rules);
  }

  /**
   * Clear out the old rules list and set it to the one passed in.
   * @author Bill Darbie
   */
  public void setNewRulesList(List<JointTypeEnumRule> jointTypeEnumRules)
  {
    _rules.clear();
    _rules.addAll(jointTypeEnumRules);
  }

  /**
   * @author Bill Darbie
   */
  private void buildDefaultRules()
  {
    try
    {
      _rules.clear();
      JointTypeEnumRule jointTypeEnumRule = new JointTypeEnumRule(JointTypeEnum.SINGLE_PAD);
      jointTypeEnumRule.setNumPadsPattern("1");
      _rules.add(jointTypeEnumRule);

      jointTypeEnumRule = new JointTypeEnumRule(JointTypeEnum.CGA);
      jointTypeEnumRule.setNumRowsPattern(">2");
      jointTypeEnumRule.setNumColsPattern(">2");
      jointTypeEnumRule.setLandPatternNamePattern(".*(CGA|CBGA).*");
      _rules.add(jointTypeEnumRule);
      
      jointTypeEnumRule = new JointTypeEnumRule(JointTypeEnum.LGA);
      jointTypeEnumRule.setNumRowsPattern(">2");
      jointTypeEnumRule.setNumColsPattern(">2");
      jointTypeEnumRule.setLandPatternNamePattern(".*LGA.*");
      _rules.add(jointTypeEnumRule);

      jointTypeEnumRule = new JointTypeEnumRule(JointTypeEnum.SURFACE_MOUNT_CONNECTOR);
      jointTypeEnumRule.setRefDesPattern("(J.*)|(P.*)");
      jointTypeEnumRule.setNumPadsPattern(">=2");
      _rules.add(jointTypeEnumRule);

      jointTypeEnumRule = new JointTypeEnumRule(JointTypeEnum.COLLAPSABLE_BGA);
      jointTypeEnumRule.setNumRowsPattern(">2");
      jointTypeEnumRule.setNumColsPattern(">2");
      _rules.add(jointTypeEnumRule);

      jointTypeEnumRule = new JointTypeEnumRule(JointTypeEnum.JLEAD);
      jointTypeEnumRule.setNumRowsPattern("2");
      jointTypeEnumRule.setNumColsPattern("2");
      jointTypeEnumRule.setPadOneInCenterOfRow(true);
      jointTypeEnumRule.setLandPatternNamePattern(".*PLCC.*");
      _rules.add(jointTypeEnumRule);

      jointTypeEnumRule = new JointTypeEnumRule(JointTypeEnum.POLARIZED_CAP);
      jointTypeEnumRule.setRefDesPattern("C.*");
      jointTypeEnumRule.setNumPadsPattern("2");
      jointTypeEnumRule.setLandPatternNamePattern(".*(TANT|PCAP|7343|6032|3528|3216).*");
      _rules.add(jointTypeEnumRule);

      jointTypeEnumRule = new JointTypeEnumRule(JointTypeEnum.CAPACITOR);
      jointTypeEnumRule.setRefDesPattern("C\\d.*");
      jointTypeEnumRule.setNumPadsPattern("2");
      _rules.add(jointTypeEnumRule);

      // Wei Chin (Tall Cap)
//      jointTypeEnumRule = new JointTypeEnumRule(JointTypeEnum.TALL_CAPACITOR);
//      jointTypeEnumRule.setRefDesPattern("TC\\d.*");
//      jointTypeEnumRule.setNumPadsPattern("2");
//      _rules.add(jointTypeEnumRule);

      jointTypeEnumRule = new JointTypeEnumRule(JointTypeEnum.RESISTOR);
      jointTypeEnumRule.setRefDesPattern("R.*");
      jointTypeEnumRule.setNumPadsPattern("2");
      _rules.add(jointTypeEnumRule);

      jointTypeEnumRule = new JointTypeEnumRule(JointTypeEnum.LEADLESS);
      jointTypeEnumRule.setNumPadsPattern("2");
      _rules.add(jointTypeEnumRule);

      jointTypeEnumRule = new JointTypeEnumRule(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD);
      jointTypeEnumRule.setLandPatternNamePattern(".*QFN.*");
      _rules.add(jointTypeEnumRule);

      jointTypeEnumRule = new JointTypeEnumRule(JointTypeEnum.GULLWING);
      _rules.add(jointTypeEnumRule);
    }
    catch (InvalidRegularExpressionDatastoreException ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * Clear out any maps that hold references that could be garbage collected
   * @author Bill Darbie
   */
  void clear()
  {
    _padTypeToJointTypeEnumMap.clear();
  }

  /**
   * @author Bill Darbie
   */
  void determineJointTypeAssignment(ComponentType componentType)
  {
    _padTypeToJointTypeEnumMap.clear();
    _jointTypeEnum = null;

    boolean allPadsAreThroughHole = true;
    int numPadTypesAssigned = 0;
    _forceOneSubtypeForEntireComponent = false;
    for (JointTypeEnumRule jointTypeEnumRule : _rules)
    {
      if (jointTypeEnumRule.isEnabled())
      {
        if (jointTypeEnumRule.match(componentType))
        {
          JointTypeEnum jointTypeEnum = jointTypeEnumRule.getJointTypeEnum();
          _jointTypeEnum = jointTypeEnum;
          if ((_forceOneSubtypeForEntireComponent == false) &&
              (_forceOneSubtypeForEntireComponentJointTypeSet.contains(jointTypeEnum)))
          {
            // these JointTypes only happen on 2 pin devices and both
            // pins must use the same subtype
            _forceOneSubtypeForEntireComponent = true;
          }
          for (PadType padType : componentType.getPadTypes())
          {
            if (padType.isThroughHolePad() == false)
              allPadsAreThroughHole = false;

            if (_forceOneSubtypeForEntireComponent)
              _padTypeToJointTypeEnumMap.put(padType, jointTypeEnum);
            else
            {
              // if a pad is a through hole pad, assign it to through hole UNLESS is was already
              // assigned to be pressfit
              if ((padType.isThroughHolePad()) && (jointTypeEnum.equals(JointTypeEnum.PRESSFIT) == false))
                _padTypeToJointTypeEnumMap.put(padType, JointTypeEnum.THROUGH_HOLE);
              else
                _padTypeToJointTypeEnumMap.put(padType, jointTypeEnum);
            }
            ++numPadTypesAssigned;
          }
          // once a rule has matched break out of the for loop
          break;
        }
      }
    }

    // if all Pads are through hole, then set _jointTypeEnum to be through hole instead of what
    // the rules said
    if (allPadsAreThroughHole)
    {
      _jointTypeEnum = JointTypeEnum.THROUGH_HOLE;

      for (PadType padType : componentType.getPadTypes())
      {
        JointTypeEnum jointTypeEnum = _padTypeToJointTypeEnumMap.get(padType);
        // if a pad is a through hole pad, assign it to through hole UNLESS is was already
        // assigned to be pressfit
        if ((jointTypeEnum != null) && (jointTypeEnum.equals(JointTypeEnum.PRESSFIT) == false))
          _padTypeToJointTypeEnumMap.put(padType, JointTypeEnum.THROUGH_HOLE);
      }
    }

    Assert.expect(numPadTypesAssigned == componentType.getPadTypes().size());
  }

  /**
   * @author Bill Darbie
   */
  Map<PadType, JointTypeEnum> getPadTypeToJointTypeEnumMap()
  {
    return new HashMap<PadType, JointTypeEnum>(_padTypeToJointTypeEnumMap);
  }

  /**
   * @author Bill Darbie
   */
  JointTypeEnum getJointTypeEnum()
  {
    Assert.expect(_jointTypeEnum != null);
    return _jointTypeEnum;
  }

  /**
   * @author Bill Darbie
   */
  int getPadSizeThresholdForSubtypeAssignmentPercent()
  {
    return _padSizeThresholdForSubtypeAssignmentPercent;
  }

  /**
   * @author Bill Darbie
   */
  boolean forceOneSubtypeForEntireComponent()
  {
    return _forceOneSubtypeForEntireComponent;
  }

  /**
   * @author Bill Darbie
   */
  public void setJointTypeAssignmentConfigFileNameWithoutExtension(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);
    Assert.expect(_config != null);

    Assert.expect(fileName.endsWith(FileName.getConfigFileExtension()) == false);
    String nameWithExtension = fileName + FileName.getConfigFileExtension();
    _config.setValue(SoftwareConfigEnum.JOINT_TYPE_ASSIGNMENT_FILENAME, nameWithExtension);
  }

  /**
   * @author Laura Cormos
   */
  public String getJointTypeAssignmentConfigFileNameWithoutExtension()
  {
    Assert.expect(_config != null);

    String fileName = _config.getStringValue(SoftwareConfigEnum.JOINT_TYPE_ASSIGNMENT_FILENAME);
    if (fileName.endsWith(FileName.getConfigFileExtension()))
      fileName = fileName.substring(0, fileName.length() - FileName.getConfigFileExtension().length());

    return fileName;
  }

  /**
    * @author Laura Cormos
    */
   public void deleteRuleSet(String fileName) throws DatastoreException
   {
     Assert.expect(fileName != null);

     // add .config extension and find the correct path on disk before deleting
     fileName = fileName + FileName.getConfigFileExtension();
     FileUtilAxi.delete(FileName.getJointTypeAssignmentFileFullPath(fileName));
  }
}
