package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class SideBoardEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static SideBoardEventEnum CREATE_OR_DESTROY = new SideBoardEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static SideBoardEventEnum SIDE_BOARD_TYPE = new SideBoardEventEnum(++_index);
  public static SideBoardEventEnum TOP_SIDE = new SideBoardEventEnum(++_index);
  public static SideBoardEventEnum BOARD = new SideBoardEventEnum(++_index);
  public static SideBoardEventEnum ADD_OR_REMOVE_COMPONENT = new SideBoardEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static SideBoardEventEnum ADD_OR_REMOVE_FIDUCIAL = new SideBoardEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static SideBoardEventEnum MOVE_BY_OFFSET = new SideBoardEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private SideBoardEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private SideBoardEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
