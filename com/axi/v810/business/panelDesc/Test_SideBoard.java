package com.axi.v810.business.panelDesc;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;


/**
 * @author Bill Darbie
 */
class Test_SideBoard extends UnitTest
{
  /**
   * @author Keith Lee
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_SideBoard());
  }

  /**
   * @author Keith Lee
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    SideBoard sideBoard = new SideBoard();

    // test setBoard and getBoard
    try
    {
      sideBoard.getBoard();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      sideBoard.setBoard(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    Board board = new Board();
    sideBoard.setBoard(board);
    Expect.expect(sideBoard.getBoard() == board);

    // setTopSide and getTopSide
    try
    {
      sideBoard.isTopSide();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    boolean side = true;

    // test addComponent, getComponents, hasComponent(name), getComponent(name), hasCompnent(Component), removeComponent(Component)
    try
    {
      sideBoard.addComponent(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    Expect.expect(sideBoard.getComponents().size() == 0);
    Panel panel = new Panel();
    panel.disableChecksForNdfParsers();
    Component component = new Component();
    ComponentType componentType = new ComponentType();
    componentType.setPanel(panel);
    component.setComponentType(componentType);
    String componentName = "U1";
    componentType.setReferenceDesignator(componentName);
    sideBoard.addComponent(component);
    Expect.expect(sideBoard.getComponents().size() == 1);
    Expect.expect(sideBoard.getComponents().get(0) == component);
    // add it again and catch the error
    try
    {
      sideBoard.addComponent(component);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    component = null;
    componentName = null;

    try
    {
      sideBoard.hasComponent(component);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      sideBoard.hasComponent(componentName);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      sideBoard.getComponent(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    sideBoard = new SideBoard();
    component = new Component();
    Component component2 = new Component();
    componentName = "U1";
    String componentName2 = "U4";
    ComponentType ct1 = new ComponentType();
    ct1.setPanel(panel);
    component.setComponentType(ct1);
    ct1.setReferenceDesignator(componentName);
    ComponentType ct2 = new ComponentType();
    ct2.setPanel(panel);
    component2.setComponentType(ct2);
    ct2.setReferenceDesignator(componentName2);

    sideBoard.addComponent(component);
    // Component version
    Expect.expect(sideBoard.hasComponent(component) == true);
    Expect.expect(sideBoard.hasComponent(component2) == false);
    // string version
    Expect.expect(sideBoard.hasComponent(componentName) == true);
    Expect.expect(sideBoard.hasComponent(componentName2) == false);

    try
    {
      sideBoard.removeComponent(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      sideBoard.removeComponent(component2);  // has not been added
    }
    catch(AssertException e)
    {
      // do nothing
    }
    Expect.expect(sideBoard.getComponents().size() == 1);
    sideBoard.removeComponent(component);
    Expect.expect(sideBoard.getComponents().size() == 0);
    Expect.expect(sideBoard.hasComponent(component) == false);
    Expect.expect(sideBoard.hasComponent(componentName) == false);

// wpd virgo
//    // test setSideBoardSettings and getSideBoardSettings
//    try
//    {
//      sideBoard.getSideBoardSettings();
//      Expect.expect(false);
//    }
//    catch(AssertException e)
//    {
//      // do nothing
//    }
//    try
//    {
//      sideBoard.setSideBoardSettings(null);
//      Expect.expect(false);
//    }
//    catch(AssertException e)
//    {
//      // do nothing
//    }
//    SideBoardSettings sideBoardSettings = new SideBoardSettings();
//    sideBoard.setSideBoardSettings(sideBoardSettings);
//    Expect.expect(sideBoard.getSideBoardSettings() == sideBoardSettings);

    // test getCustomAlgorithmFamilies
    sideBoard = new SideBoard();  // reset everything

    // now populate the side board with some interesting things (components, pads, etc.)
    component = new Component();
    componentType = new ComponentType();
    componentType.setPanel(panel);
    component.setComponentType(componentType);
    componentType.setReferenceDesignator("u1");
    Pad pad = new Pad();
    PadType padType = new PadType();
    pad.setPadType(padType);
    LandPattern landPattern = new LandPattern();
    landPattern.setPanel(panel);
    LandPatternPad landPatternPad = new SurfaceMountLandPatternPad();
    landPatternPad.setLandPattern(landPattern);
    landPatternPad.setName("1");
    padType.setLandPatternPad(landPatternPad);
    component.addPad(pad);
    PadTypeSettings padTypeSettings = new PadTypeSettings();
    padType.setPadTypeSettings(padTypeSettings);

    padType.setPadTypeSettings(padTypeSettings);
    sideBoard.addComponent(component);

    // add a second  pad with a different family/subtype
    Pad pad2 = new Pad();
    PadType padType2 = new PadType();
    PackagePin packagePin2 = new PackagePin();
    padType2.setPackagePin(packagePin2);
    pad2.setPadType(padType2);
    LandPatternPad landPatternPad2 = new SurfaceMountLandPatternPad();
    landPatternPad2.setLandPattern(landPattern);
    landPatternPad.setLandPattern(landPattern);
    landPatternPad2.setName("2");
    padType2.setLandPatternPad(landPatternPad2);
    component.addPad(pad2);
    PadTypeSettings padTypeSettings2 = new PadTypeSettings();
    padType2.setPadTypeSettings(padTypeSettings2);

    // getShapeInNanometers is tested in Test_Panel.java because it needs a fully loaded panel to work properly
    // transformShapeBasedOnBoardAndPanelRotationInNanoMeters is tested in Test_Panel.java because it needs a fully loaded panel to work properly
    // transformShapeToAsSeenFromTop is tested in Test_Panel.java because it needs a fully loaded panel to work properly
  }
}
