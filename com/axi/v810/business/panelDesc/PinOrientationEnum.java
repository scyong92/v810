package com.axi.v810.business.panelDesc;

import java.util.*;

import com.axi.util.*;

/**
 * The pin orientation describes the direction of the center of the component is in relative
 * to the pin when the pin as at 0 degrees.
 *
 * A setting of NOT_APPLICABLE will be used for round pads.
 *
 * @author Bill Darbie
 */
public class PinOrientationEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  private static Map<Integer, PinOrientationEnum> _degreesToEnum = new HashMap<Integer, PinOrientationEnum>();
  private static List<PinOrientationEnum> _allEnums = new ArrayList<PinOrientationEnum>();

  public static PinOrientationEnum NOT_APPLICABLE = new PinOrientationEnum(++_index, "NA", 0);
  public static PinOrientationEnum COMPONENT_IS_NORTH_OF_PIN = new PinOrientationEnum(++_index, "North", 90);
  public static PinOrientationEnum COMPONENT_IS_SOUTH_OF_PIN = new PinOrientationEnum(++_index, "South", 270);
  public static PinOrientationEnum COMPONENT_IS_EAST_OF_PIN = new PinOrientationEnum(++_index, "East", 0);
  public static PinOrientationEnum COMPONENT_IS_WEST_OF_PIN = new PinOrientationEnum(++_index, "West", 180);

  private String _name;
  private int _degrees;

  /**
   * @author Bill Darbie
   */
  private PinOrientationEnum(int id, String name, int degrees)
  {
    super(id);

    Assert.expect(name != null);
    _name = name.intern();

    if (name.equals("NA") == false)
    {
      _allEnums.add(this);
      Object prev = _degreesToEnum.put(degrees, this);
      Assert.expect(prev == null);
    }

    _degrees = degrees;

  }

  /**
   * @author Bill Darbie
   */
  public String getName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author Bill Darbie
   */
  public static List<PinOrientationEnum> getAllPadOrientationEnums()
  {
    Assert.expect(_allEnums != null);

    return _allEnums;
  }

  /**
   * @author Bill Darbie
   */
  public int getDegrees()
  {
    return _degrees;
  }

  /**
   * @author Bill Darbie
   */
  public static PinOrientationEnum getPinOrientationEnum(int degrees)
  {
    degrees = MathUtil.getDegreesWithin0To359(degrees);
    PinOrientationEnum pinOrientationEnum = _degreesToEnum.get(degrees);
    Assert.expect(pinOrientationEnum != null);
    return pinOrientationEnum;
  }
}
