package com.axi.v810.business.panelDesc;

import java.awt.geom.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class describes a single board in a panel.  It represents the combination
 * of the top and bottom side.  For each physical board on a panel there will
 * be an instance of this class.  For the case where a panel has 4 identical
 * boards on it there will be 4 instances of this class.  Even though the boards
 * will be identical, they will have separate instances on identical items, such
 * as components and top/bottom side boards. That way, each can be manipulated
 * independent of the others.
 *
 * @author Bill Darbie
 * @author Keith Lee
 */
public class Board implements Serializable
{
  // created by PanelNdfReader, PanelReader

  private BoardType _boardType; // PanelNdfReader, PanelReader

  private String _name; // PanelNdfReader, PanelReader
  private int _degreesRotationRelativeToPanel = -1; // PanelNdfReader, PanelReader
  private PanelCoordinate _coordinateInNanoMeters; // MainReader, PanelReader

  // if true the board has been flipped upside down
  private boolean _leftToRightFlip;
  private boolean _topToBottomFlip;
  private SideBoard _sideBoard1; // PanelNdfReader, PanelReader
  private SideBoard _sideBoard2; // PanelNdfReader, PanelReader
  private BoardSettings _boardSettings; // PanelNdfReader, BoardSettingsReader
  private BoardSurfaceMapSettings _boardSurfaceMapSettings; // BoardSurfaceMapSettingsReader
  private BoardAlignmentSurfaceMapSettings _boardAlignmentSurfaceMapSettings; // BoardAlignmentSurfaceMapSettingsReader
  private BoardMeshSettings _boardMeshSettings;  // BoardMeshSettingsReader

  private transient java.awt.Shape _shape;
  private transient IntegerRef _degrees;
  private transient static ProjectObservable _projectObservable;

  // Khang-Wah, Chnee, X-Out Investigation
  private transient AtomicInteger _numberOfFailedJoint = new AtomicInteger(0);
  private transient AtomicInteger _numberOfFailedComponent = new AtomicInteger(0);
  private transient boolean _isXedOut = false;
  private transient boolean _isXoutDetectionModeEnabled = false;
  private transient boolean _isDefectXoutDetectionMode = false;
  private transient int _minNumFailingJointsForXout = 0;
  
  //XCR1374, KhangWah, 2011-09-12
  private int _numOfInspectedJoints = -1;
  private int _numOfNoTestedJoints = -1;
  private int _numOfUnTestableJoints = -1;
  
  // XCR1624 - Wrong display of panel pins proccesed - Siew Yeng
  private transient boolean _alignmentException = false;
  
  private transient String _xoutReason = "";
  
  private transient boolean _isProductionMode = false;
  
  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public Board()
  {
    _projectObservable.stateChanged(this, null, this, BoardEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      getBoardType().removeBoard(this);

      _boardSettings.destroy();
      _boardSurfaceMapSettings.destroy();
      _boardAlignmentSurfaceMapSettings.destroy();
      _boardMeshSettings.destroy();

      getPanel().removeBoard(this);
      if (_sideBoard1 != null)
        _sideBoard1.destroy();
      if (_sideBoard2 != null)
        _sideBoard2.destroy();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, BoardEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * The remove() method should be used by the GUI layer to remove an existing object.  Use the
   * add() method to undo the remove() call.
   *
   * The GUI layer should never call addXXX or removeXXX.  They are only used by the business layer.
   *
   * The GUI layer should call these methods:
   * - create() should be used to create a new object()
   * - to remove the object call remove()
   * - to undo the remove() call add()
   * - do not call destroy()
   *
   * This method unhooks this instance from being referenced from all XXXType classes, which allows it to be
   * added back in with the add() method easily.
   *
   * @author Andy Mechtenberg
   * Edited by : Kee Chin Seong.
   */
  public void remove()
  {
    _projectObservable.setEnabled(false);
    try
    {
      getPanel().removeBoard(this);
      getBoardType().removeBoard(this);

      if (_sideBoard1 != null)
      {
        _sideBoard1.getSideBoardType().removeSideBoard(_sideBoard1);
        for (Component component : _sideBoard1.getComponents())
        {
          component.getComponentType().removeComponent(component);
          for (Pad pad : component.getPads())
          {
            pad.getPadType().removePad(pad);
          }
        }
        for (Fiducial fiducial : _sideBoard1.getFiducials())
        {
          fiducial.getFiducialType().removeFiducial(fiducial);
        }
      }
      if (_sideBoard2 != null)
      {
        _sideBoard2.getSideBoardType().removeSideBoard(_sideBoard2);
        for (Component component : _sideBoard2.getComponents())
        {
          component.getComponentType().removeComponent(component);
          for (Pad pad : component.getPads())
          {
            pad.getPadType().removePad(pad);
          }
        }
        for (Fiducial fiducial : _sideBoard2.getFiducials())
        {
          fiducial.getFiducialType().removeFiducial(fiducial);
        }
      }
      // clear out the subtype _pads reference since we're changing the number of boards
      for(Subtype subtype : getPanel().getSubtypes())
      {
        subtype.invalidatePads();
      }
      removePanelOpticalRegion();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, BoardEventEnum.ADD_OR_REMOVE);
  }

  /**
   * The add() method should be used by the GUI layer to add an existing object that has already
   * been removed using the remove() method.
   *
   * The GUI layer should never call addXXX or removeXXX.  They are only used by the business layer.
   *
   * The GUI layer should call these methods:
   * - create() should be used to create a new object()
   * - to remove the object call remove()
   * - to undo the remove() call add()
   * - do not call destroy()
   *
   * This method re-connects this instance with all the XXXType classes, undoing what the remove() method did.
   *
   * @author Andy Mechtenberg
   */
  public void add()
  {
    _projectObservable.setEnabled(false);
    try
    {
      getPanel().addBoard(this);
      getBoardType().addBoard(this);

      if (_sideBoard1 != null)
      {
        _sideBoard1.getSideBoardType().addSideBoard(_sideBoard1);
        for (Component component : _sideBoard1.getComponents())
        {
          component.getComponentType().addComponent(component);
          for (Pad pad : component.getPads())
          {
            pad.getPadType().addPad(pad);
          }
        }
        for (Fiducial fiducial : _sideBoard1.getFiducials())
        {
          fiducial.getFiducialType().addFiducial(fiducial);
        }
      }
      if (_sideBoard2 != null)
      {
        _sideBoard2.getSideBoardType().addSideBoard(_sideBoard2);
        for (Component component : _sideBoard2.getComponents())
        {
          component.getComponentType().addComponent(component);
          for (Pad pad : component.getPads())
          {
            pad.getPadType().addPad(pad);
          }
        }
        for (Fiducial fiducial : _sideBoard2.getFiducials())
        {
          fiducial.getFiducialType().addFiducial(fiducial);
        }
      }
      // clear out the subtype _pads reference since we're changing the number of boards
      for (Subtype subtype : getPanel().getSubtypes())
      {
        subtype.invalidatePads();
      }

    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, this, BoardEventEnum.ADD_OR_REMOVE);
  }

  /**
   * @author Bill Darbie
   */
  void removeSideBoard(SideBoard sideBoard)
  {
    Assert.expect(sideBoard != null);

    boolean sideBoard1 = false;
    _projectObservable.setEnabled(false);
    try
    {
      if (sideBoard == _sideBoard1)
      {
        sideBoard1 = true;
        _sideBoard1 = null;
      }
      else if (sideBoard == _sideBoard2)
      {
        sideBoard1 = false;
        _sideBoard2 = null;
      }
      else
        Assert.expect(false);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    if (sideBoard1)
      _projectObservable.stateChanged(this, null, sideBoard, BoardEventEnum.SIDE_BOARD_1);
    else
      _projectObservable.stateChanged(this, null, sideBoard, BoardEventEnum.SIDE_BOARD_2);
  }

  /**
   * @author Bill Darbie
   */
  public void setBoardType(BoardType boardType)
  {
    Assert.expect(boardType != null);

    if (boardType == _boardType)
      return;

    BoardType oldValue = _boardType;
    _projectObservable.setEnabled(false);
    try
    {
      _boardType = boardType;
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, boardType, BoardEventEnum.BOARD_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public BoardType getBoardType()
  {
    Assert.expect(_boardType != null);

    return _boardType;
  }

  /**
   * @author George A. David
   */
  public Panel getPanel()
  {
    return getBoardType().getPanel();
  }

  /**
   * @author Bill Darbie
   */
  public void setName(String name)
  {
    Assert.expect(name != null);
    Panel panel = getPanel();
    Assert.expect(panel.isBoardNameValid(name));
    Assert.expect(panel.isBoardNameDuplicate(name) == false);

    if (name.equals(_name))
      return;

    String oldValue = _name;
    _projectObservable.setEnabled(false);
    try
    {
      panel.setNewBoardName(this, _name, name);
      _name = name.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, name, BoardEventEnum.NAME);
  }

  /**
   * This will typically be "1" or "2" or "3" since it is a board instance
   * @author Keith Lee
   */
  public String getName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author Bill Darbie
   */
  public void setDegreesRotationRelativeToPanel(int degreesRotation)
  {
    int oldValue = _degreesRotationRelativeToPanel;
    _projectObservable.setEnabled(false);
    try
    {
      _degreesRotationRelativeToPanel = degreesRotation;
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, degreesRotation, BoardEventEnum.DEGREES_ROTATION);
  }

  /**
   * The rotation of the board only (not including the rotation of the panel)
   * @author Bill Darbie
   */
  public void setDegreesRotationRelativeToPanelFromNdfReaders(int degreesRotation)
  {
    degreesRotation = MathUtil.getDegreesWithin0To359(degreesRotation);

    if (_degreesRotationRelativeToPanel == -1)
    {
      // this is the first time this method has been called from the MainReader
      // so set to the proper type of flip based on rotation.  This could not
      // be done when flipSideBoardsForNdfReader() was originally called because
      // the rotation was not known then

      if (_leftToRightFlip)
      {
        if ((degreesRotation == 90) || (degreesRotation == 270))
        {
          setLeftToRightFlip(false);
          setTopToBottomFlip(true);
        }
      }
    }

    setDegreesRotationRelativeToPanel(degreesRotation);
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasComponent(String refDes)
  {
    if (_sideBoard1 != null)
    {
      if (_sideBoard1.hasComponent(refDes))
        return true;
    }
    if (_sideBoard2 != null)
    {
      if (_sideBoard2.hasComponent(refDes))
        return true;
    }

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public Component getComponent(String refDes)
  {
    Assert.expect(refDes != null);

    Component component = null;
    if (_sideBoard1 != null)
    {
      if (_sideBoard1.hasComponent(refDes))
        component = _sideBoard1.getComponent(refDes);
    }
    if (component == null)
    {
      if (_sideBoard2.hasComponent(refDes))
        component = _sideBoard2.getComponent(refDes);
    }

    Assert.expect(component != null);
    return component;
  }

  /**
   * The rotation of the board only (not including the rotation of the panel)
   * @author Bill Darbie
   */
  public int getDegreesRotationRelativeToPanel()
  {
    Assert.expect(_degreesRotationRelativeToPanel != -1);

    return _degreesRotationRelativeToPanel;
  }

  /**
   * @author Bill Darbie
   */
  private int getDegreesRotationRelativeToPanelIncludingPanelFlip()
  {
    Assert.expect(_degreesRotationRelativeToPanel != -1);

    int degrees = _degreesRotationRelativeToPanel;
    if (getPanel().getPanelSettings().isFlipped())
      degrees = -degrees;

    degrees = MathUtil.getDegreesWithin0To359(degrees);
    return degrees;
  }

  /**
   * Rotate the Board so that the coordinate of the Boards lower left corner is in the same
   * place before and after the rotation.
   *
   * @author Bill Darbie
   */
  public void setDegreesRotationRetainingLowerLeftCorner(int degreesRotation)
  {
    if (degreesRotation == _degreesRotationRelativeToPanel)
      return;

    Panel panel = getPanel();
    PanelSettings panelSettings = panel.getPanelSettings();
    int origPanelDegrees = panelSettings.getDegreesRotationRelativeToCad();

    // get the original board coordinate
    PanelCoordinate origCoord = getCoordinateInNanoMeters();

    // get the lower left corner of the board before the new rotation
    PanelCoordinate preRotationLowerLeftCoord = getLowerLeftCoordinateRelativeToPanelInNanoMeters();

    // set the new board rotation
    setDegreesRotationRelativeToPanel(degreesRotation);

    // get the lower left corner after the board rotation
    PanelCoordinate postRotationLowerLeftCoord = getLowerLeftCoordinateRelativeToPanelInNanoMeters();

    // figure out how far in x and y the origin needs to move
    int dx = preRotationLowerLeftCoord.getX() - postRotationLowerLeftCoord.getX();
    int dy = preRotationLowerLeftCoord.getY() - postRotationLowerLeftCoord.getY();

    // now translate dx, dy so it is not taking into account any panel rotation or flips
    AffineTransform trans = AffineTransform.getTranslateInstance(0, 0);

    // compenstate for panel rotation
    trans.preConcatenate(AffineTransform.getRotateInstance(Math.toRadians(-origPanelDegrees), 0, 0));

    // compensate for panel flippige
    if (panelSettings.isTopToBottomFlip())
    {
      // do a top-to-bottom flip before the rotation
      trans.preConcatenate(AffineTransform.getScaleInstance(1, -1));
    }

    if (panelSettings.isLeftToRightFlip())
    {
      // do a left-to-right flip before the rotation
      trans.preConcatenate(AffineTransform.getScaleInstance( -1, 1));
    }

    // apply the transform to the dx,dy point
    Point2D.Double origDxDyPoint = new Point2D.Double(dx, dy);
    Point2D.Double newDxDyPoint = new Point2D.Double();
    trans.transform(origDxDyPoint, newDxDyPoint);

    // calc new x, y with the transformed dx,dy point
    int x = origCoord.getX() + (int)newDxDyPoint.getX();
    int y = origCoord.getY() + (int)newDxDyPoint.getY();

    // set the new board coord
    setCoordinateInNanoMeters(new PanelCoordinate(x, y));
  }

  /**
   * @return the same degrees out that the setDegressRotationRetainingLowerLeftCorner() was
   * set to.
   * @author Bill Darbie
   */
  public int getDegreesRotationRetainingLowerLeftCorner()
  {
    int degrees = getDegreesRotationRelativeToPanelIncludingPanelFlip();
    if (getPanel().getPanelSettings().isFlipped())
      degrees = -degrees;

    degrees = MathUtil.getDegreesWithin0To359(degrees);
    return degrees;
  }

  /**
   * @return the degrees rotation after all rotations from this Board, and its Panel have been
   * applied.
   *
   * @author Bill Darbie
   */
  public int getDegreesRotationAfterAllRotations()
  {
    if (_degrees == null)
    {
      double degrees = getDegreesRotationRelativeToPanel();
      java.awt.geom.AffineTransform transform = java.awt.geom.AffineTransform.getRotateInstance(Math.toRadians(degrees));

      // apply the transform of the component.
      getPanel().preConcatenateShapeTransform(transform, false);

      Point2D unitVector = new Point2D.Double(1, 0);
      Point2D transformedPoint = transform.transform(unitVector, null);

      double x1 = unitVector.getX();
      double y1 = unitVector.getY();
      double x2 = transformedPoint.getX();
      double y2 = transformedPoint.getY();

      // get the angle between x1,y1 and x2,y2
      degrees = Math.toDegrees(Math.acos(x1 * x2 + y1 * y2));

      // if y2 is positive then the degrees should be from 0 to 180, so do nothing
      // if y2 is negative then the degrees should be from 180 to 360
      // since the acos returns degrees from 0 to 180 only, we must compenstate here
      if (y2 < 0)
        degrees = -degrees;

      degrees = MathUtil.getDegreesWithin0To359(degrees);
      int degreesInt = MathUtil.roundDoubleToInt(degrees);
      _degrees = new IntegerRef(degreesInt);
    }

    Assert.expect(_degrees !=  null);
    return _degrees.getValue();
  }

  /**
   * The lower left corner of the board before rotation
   * @author George A. David
   */
  public void setCoordinateInNanoMeters(PanelCoordinate coordinateInNanoMeters)
  {
    Assert.expect(coordinateInNanoMeters != null);

    if (coordinateInNanoMeters.equals(_coordinateInNanoMeters))
      return;

    PanelCoordinate oldValue = _coordinateInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _coordinateInNanoMeters = coordinateInNanoMeters;
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, coordinateInNanoMeters, BoardEventEnum.COORDINATE);
  }

  /**
   * The lower left corner of the board before rotation
   * @author George A. David
   */
  public PanelCoordinate getCoordinateInNanoMeters()
  {
    Assert.expect(_coordinateInNanoMeters != null);

    return new PanelCoordinate(_coordinateInNanoMeters);
  }

  /**
   * Set the Boards lower left corner coordinate.
   * @author Bill Darbie
   */
  public void setLowerLeftCoordinateRelativeToPanelInNanoMeters(PanelCoordinate coordinateInNanoMeters)
  {
    Assert.expect(coordinateInNanoMeters != null);

    if (coordinateInNanoMeters.equals(getLowerLeftCoordinateRelativeToPanelInNanoMeters()))
      return;

    PanelCoordinate oldLowerLeftCoord = getLowerLeftCoordinateRelativeToPanelInNanoMeters();
    PanelCoordinate oldCoord = getCoordinateInNanoMeters();
    int deltaX = coordinateInNanoMeters.getX() - oldLowerLeftCoord.getX();
    int deltaY = coordinateInNanoMeters.getY() - oldLowerLeftCoord.getY();

    // take Panel rotation into account
    Panel panel = getPanel();
    int panelDegrees = panel.getDegreesRotationRelativeToCad();
    if (panelDegrees == 0)
    {
      // do nothing
    }
    else if (panelDegrees == 90)
    {
      int temp = -deltaX;
      deltaX = deltaY;
      deltaY = temp;
    }
    else if (panelDegrees == 180)
    {
      deltaX = -deltaX;
      deltaY = -deltaY;
    }
    else if (panelDegrees == 270)
    {
      int temp = deltaX;
      deltaX = -deltaY;
      deltaY = temp;
    }
    else
      Assert.expect(false);

    // take Panel flipping into account

    if (panel.getPanelSettings().isLeftToRightFlip())
      deltaX = -deltaX;
    if (panel.getPanelSettings().isTopToBottomFlip())
      deltaY = -deltaY;

    int newX = oldCoord.getX() + deltaX;
    int newY = oldCoord.getY() + deltaY;

    setCoordinateInNanoMeters(new PanelCoordinate(newX, newY));
    
    removePanelOpticalRegion();
    removeBoardOpticalRegion();
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void removeBoardOpticalRegion()
  {
     //make sure to clean off the optical region, as this will causing problem while next loading of 
     //the recipes.
     if(hasBoardSurfaceMapSettings())
     {
       for(OpticalRegion opticalRegion : getBoardSurfaceMapSettings().getOpticalRegions())
       {
           getBoardSurfaceMapSettings().removeSurfaceMapRegion(opticalRegion);
       }
     }
  }
  
  /*
   * @author Kee Chin Seong
   */
  private void removePanelOpticalRegion()
  {
     //make sure to clean off the optical region, as this will causing problem while next loading of 
     //the recipes.
     if(getPanel().hasPanelSurfaceMapSettings())
     {
       for(OpticalRegion opticalRegion : getPanel().getPanelSurfaceMapSettings().getOpticalRegions())
       {
         if(opticalRegion.getBoards().contains(this))
         {
             getPanel().getPanelSurfaceMapSettings().removeSurfaceMapRegion(opticalRegion);
         }
       }
     }
  }

  /**
   * @return the lower left coordinate of this Board relative to its Panels bottom left corner
   * after all board and panel rotations and flips have been applied.
   * @author Bill Darbie
   */
  public PanelCoordinate getLowerLeftCoordinateRelativeToPanelInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    java.awt.geom.Rectangle2D bounds = shape.getBounds2D();

    int x = MathUtil.roundDoubleToInt(bounds.getMinX());
    int y = MathUtil.roundDoubleToInt(bounds.getMinY());

    return new PanelCoordinate(x, y);
  }

  /**
   * @return the center coordinate of this Board relative to its Panels bottom left corner.
   * @author Bill Darbie
   */
  public PanelCoordinate getCenterCoordinateRelativeToPanelInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    java.awt.geom.Rectangle2D bounds = shape.getBounds2D();

    int x = MathUtil.roundDoubleToInt(bounds.getCenterX());
    int y = MathUtil.roundDoubleToInt(bounds.getCenterY());

    return new PanelCoordinate(x, y);
  }

  /**
   * @author Bill Darbie
   */
  public void flipSideBoardsForNdfReader()
  {
    // at the time this method is called, the Board rotation is not known,
    // set _leftToRightFlip for now, the setDegreesRotationRelativeToPanel()
    // method will correct for rotation the first time it is called
    _leftToRightFlip = true;

    Assert.expect((_sideBoard1 != null) || (_sideBoard2 != null));
    SideBoard temp = _sideBoard1;
    _sideBoard1 = _sideBoard2;
    _sideBoard2 = temp;
  }

  /**
   * Flip the board over (left to right flip).
   *
   * @author Bill Darbie
   */
  public void flip()
  {
    Panel panel = getPanel();
    int degrees = _degreesRotationRelativeToPanel + panel.getDegreesRotationRelativeToCad();
    degrees = MathUtil.getDegreesWithin0To359(degrees);

    if ((degrees == 0) || (degrees == 180))
    {
      setLeftToRightFlip(!_leftToRightFlip);
    }
    else if ((degrees == 90) || (degrees == 270))
    {
      setTopToBottomFlip(!_topToBottomFlip);
    }
    else
      Assert.expect(false);
  }

  /**
    * @return true if the Board is flipped.
    * @author Bill Darbie
    */
   public boolean isFlipped()
   {
     if (((_leftToRightFlip) && (_topToBottomFlip == false)) ||
         ((_leftToRightFlip == false) && (_topToBottomFlip)))
       return true;

     return false;
   }

   /**
    * @author Bill Darbie
    */
   public void setLeftToRightFlip(boolean leftToRightFlip)
   {
     boolean oldValue = _leftToRightFlip;

     _projectObservable.setEnabled(false);
     try
     {
       _leftToRightFlip = leftToRightFlip;
       invalidateShape();
     }
     finally
     {
       _projectObservable.setEnabled(true);
     }
     _projectObservable.stateChanged(this, oldValue, leftToRightFlip, BoardEventEnum.LEFT_TO_RIGHT_FLIP);
   }

   /**
    * @author Bill Darbie
    */
   public boolean isLeftToRightFlip()
   {
     return _leftToRightFlip;
   }

   /**
    * @author Bill Darbie
    */
   public void setTopToBottomFlip(boolean topToBottomFlip)
   {
     boolean oldValue = _topToBottomFlip;

     _projectObservable.setEnabled(false);
     try
     {
       _topToBottomFlip = topToBottomFlip;
       invalidateShape();
     }
     finally
     {
       _projectObservable.setEnabled(true);
     }
     _projectObservable.stateChanged(this, oldValue, topToBottomFlip, BoardEventEnum.TOP_TO_BOTTOM_FLIP);
   }

   /**
    * @author Bill Darbie
    */
   public boolean isTopToBottomFlip()
   {
     return _topToBottomFlip;
   }

   /**
    * @author Bill Darbie
    */
   public boolean isLeftToRightFlipAfterAllFlips()
   {
     boolean flipped = isLeftToRightFlip();
     Panel panel = getPanel();
     if (panel.getPanelSettings().isLeftToRightFlip())
       flipped = !flipped;

     return flipped;
   }

   /**
    * @author Bill Darbie
    */
   public boolean isTopToBottomFlipAfterAllFlips()
   {
     boolean flipped = isTopToBottomFlip();
     Panel panel = getPanel();
     if (panel.getPanelSettings().isTopToBottomFlip())
       flipped = !flipped;

     return flipped;
   }


  /**
   * This method should only be called by a Reader
   * use the flip() call in all other cases.
   * @author Bill Darbie
   */
  public void setSideBoard1(SideBoard sideBoard1)
  {
    Assert.expect(sideBoard1 != null);
    // make sure this is only called once by a Reader
    Assert.expect(_sideBoard1 == null);

    SideBoard oldValue = _sideBoard1;
    _projectObservable.setEnabled(false);
    try
    {
      _sideBoard1 = sideBoard1;
      _sideBoard1.setBoard(this);
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, sideBoard1, BoardEventEnum.SIDE_BOARD_1);
  }

  /**
   * @author Bill Darbie
   */
  public boolean sideBoard1Exists()
  {
    if (_sideBoard1 != null)
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public SideBoard getSideBoard1()
  {
    Assert.expect(_sideBoard1 != null);
    return _sideBoard1;
  }

  /**
   * @author Bill Darbie
   */
  public void setSideBoard2(SideBoard sideBoard2)
  {
    Assert.expect(sideBoard2 != null);
    // make sure this is only called once by a Reader
    Assert.expect(_sideBoard2 == null);

    SideBoard oldValue = _sideBoard2;
    _projectObservable.setEnabled(false);
    try
    {
      _sideBoard2 = sideBoard2;
      _sideBoard2.setBoard(this);
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, sideBoard2, BoardEventEnum.SIDE_BOARD_2);
  }

  /**
   * @author Bill Darbie
   */
  public boolean sideBoard2Exists()
  {
    if (_sideBoard2 != null)
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public SideBoard getSideBoard2()
  {
    Assert.expect(_sideBoard2 != null);
    return _sideBoard2;
  }

  /**
   * @return if this Board is flipped after both the Board and Panel
   * flips are taken into account.
   * @author Bill Darbie
   */
  public boolean isFlippedAfterAllFlips()
  {
    boolean flipped = isFlipped();
    Panel panel = getPanel();
    if (panel.isFlipped())
      flipped = !flipped;

    return flipped;
  }

  /**
   * @return true if a SideBoard that is facing the x-ray source exists.
   * @author Bill Darbie
   */
  public boolean topSideBoardExists()
  {
    boolean exists = true;

    if (isFlippedAfterAllFlips())
    {
      if (_sideBoard2 == null)
        exists = false;
    }
    else
    {
      if (_sideBoard1 == null)
        return false;
    }

    return exists;
  }

  /**
   * @return the SideBoard that is facing the x-ray source.
   * @author Bill Darbie
   */
  public SideBoard getTopSideBoard()
  {
    Assert.expect(topSideBoardExists());
    SideBoard sideBoard = null;
    if (isFlippedAfterAllFlips())
      sideBoard = _sideBoard2;
    else
      sideBoard = _sideBoard1;

    Assert.expect(sideBoard != null);
    return sideBoard;
  }

  /**
   * @return  if a SideBoard exists that is NOT facing the XraySource.
   * @author Bill Darbie
   */
  public boolean bottomSideBoardExists()
  {
    boolean exists = true;
    if (isFlippedAfterAllFlips())
    {
      if (_sideBoard1 == null)
        exists = false;
    }
    else
    {
      if (_sideBoard2 == null)
        return false;
    }

    return exists;
  }

  /**
   * @return the SideBoard that is NOT facing the XraySource.
   * @author Bill Darbie
   */
  public SideBoard getBottomSideBoard()
  {
    Assert.expect(bottomSideBoardExists());
    SideBoard sideBoard = null;
    if (isFlippedAfterAllFlips())
      sideBoard = _sideBoard1;
    else
      sideBoard = _sideBoard2;

    Assert.expect(sideBoard != null);
    return sideBoard;
  }

  /**
   * Create new SideBoard instances for this Board using the pre-existing SideBoardTypes.
   * @author Bill Darbie
   */
  void createSideBoards()
  {
    List<SideBoard> sideBoards = new ArrayList<SideBoard>();

    BoardType boardType = getBoardType();
    if (boardType.sideBoardType1Exists())
    {
      SideBoardType sideBoardType = boardType.getSideBoardType1();
      SideBoard sideBoard = new SideBoard();
      sideBoard.setSideBoardType(sideBoardType);
      sideBoard.createComponents();
      sideBoard.createFiducials();
      setSideBoard1(sideBoard);
      sideBoards.add(sideBoard);
      sideBoardType.addSideBoard(sideBoard);
    }

    if (boardType.sideBoardType2Exists())
    {
      SideBoardType sideBoardType = boardType.getSideBoardType2();
      SideBoard sideBoard = new SideBoard();
      sideBoard.setSideBoardType(sideBoardType);
      sideBoard.createComponents();
      sideBoard.createFiducials();
      setSideBoard2(sideBoard);
      sideBoards.add(sideBoard);
      sideBoardType.addSideBoard(sideBoard);
    }
  }

  /**
   * @author Bill Darbie
   */
  public void createSecondSideBoard()
  {
    BoardType boardType = getBoardType();
    SideBoardType sideBoardType = boardType.createSecondSideBoardType();
    for (Board board : boardType.getBoards())
    {
      SideBoard sideBoard = new SideBoard();
      sideBoard.setBoard(board);
      sideBoard.setSideBoardType(sideBoardType);
      sideBoardType.addSideBoard(sideBoard);

      if (board.sideBoard1Exists() == false)
        board.setSideBoard1(sideBoard);
      else if (board.sideBoard2Exists() == false)
        board.setSideBoard2(sideBoard);
      else
        Assert.expect(false);
    }
  }

  /**
   * @author Keith Lee
   */
  public void setBoardSettings(BoardSettings boardSettings)
  {
    Assert.expect(boardSettings != null);

    if (boardSettings == _boardSettings)
      return;

    BoardSettings oldValue = _boardSettings;
    _projectObservable.setEnabled(false);
    try
    {
      _boardSettings = boardSettings;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, boardSettings, BoardEventEnum.BOARD_SETTINGS);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setBoardSurfaceMapSettings(BoardSurfaceMapSettings boardSurfaceMapSettings)
  {
    Assert.expect(boardSurfaceMapSettings != null);
      
    if (boardSurfaceMapSettings == _boardSurfaceMapSettings)
      return;
      
    BoardSurfaceMapSettings oldValue = _boardSurfaceMapSettings;
    _projectObservable.setEnabled(false);
    try
    {
      _boardSurfaceMapSettings = boardSurfaceMapSettings;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, boardSurfaceMapSettings, BoardEventEnum.BOARD_SURFACE_MAP_SETTINGS);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setBoardAlignmentSurfaceMapSettings(BoardAlignmentSurfaceMapSettings boardAlignmentSurfaceMapSettings)
  {
    Assert.expect(boardAlignmentSurfaceMapSettings != null);
      
    if (boardAlignmentSurfaceMapSettings == _boardAlignmentSurfaceMapSettings)
      return;
      
    BoardAlignmentSurfaceMapSettings oldValue = _boardAlignmentSurfaceMapSettings;
    _projectObservable.setEnabled(false);
    try
    {
      _boardAlignmentSurfaceMapSettings = boardAlignmentSurfaceMapSettings;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, boardAlignmentSurfaceMapSettings, BoardEventEnum.BOARD_ALIGNMENT_SURFACE_MAP_SETTINGS);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setBoardMeshSettings(BoardMeshSettings boardMeshSettings)
  {
    Assert.expect(boardMeshSettings != null);
    
    if (boardMeshSettings == _boardMeshSettings)
      return;
    
    BoardMeshSettings oldValue = _boardMeshSettings;
    _projectObservable.setEnabled(false);
    try
    {
      _boardMeshSettings = boardMeshSettings;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, boardMeshSettings, BoardEventEnum.BOARD_ALIGNMENT_SURFACE_MAP_SETTINGS);
  }

  /**
   * @author Keith Lee
   */
  public BoardSettings getBoardSettings()
  {
    Assert.expect(_boardSettings != null);

    return _boardSettings;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public BoardSurfaceMapSettings getBoardSurfaceMapSettings()
  {
    Assert.expect(_boardSurfaceMapSettings != null);
    
    return _boardSurfaceMapSettings;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public boolean hasBoardSurfaceMapSettings()
  {   
    if(_boardSurfaceMapSettings == null)
      return false;
    
    return true;
  }
  
  
  /**
   * @author Cheah Lee Herng
   */
  public BoardAlignmentSurfaceMapSettings getBoardAlignmentSurfaceMapSettings()
  {
    Assert.expect(_boardAlignmentSurfaceMapSettings != null);
    
    return _boardAlignmentSurfaceMapSettings;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasBoardAlignmentSurfaceMapSettings()
  {
    if (_boardAlignmentSurfaceMapSettings == null)
      return false;
    
    return true;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public BoardMeshSettings getBoardMeshSettings()
  {
    Assert.expect(_boardMeshSettings != null);
    return _boardMeshSettings;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasBoardMeshSettings()
  {
    if (_boardMeshSettings == null)
      return false;
    return true;
  }

  /**
   * @return a List of Component objects that are contained on this Board, both top and bottom
   * @author Keith Lee
   */
  public List<Component> getComponents()
  {
    List<Component> components = new ArrayList<Component>();
    if (_sideBoard1 != null)
    {
      for (Component component : _sideBoard1.getComponents())
      {
        boolean added = components.add(component);
        Assert.expect(added);
      }
    }

    if (_sideBoard2 != null)
    {
      for (Component component : _sideBoard2.getComponents())
      {
        boolean added = components.add(component);
        Assert.expect(added);
      }
    }

    Collections.sort(components, new AlphaNumericComparator());
    return components;
  }
  
  /**
   * @return all top components on this board
   * @author Ying-Huan.Chu
   */
  public List<Component> getTopComponents()
  {
    List<Component> components = new ArrayList<>();
    for (Component component : getComponents())
    {
      if (component.isTopSide())
        components.add(component);
    }
    
    Collections.sort(components, new AlphaNumericComparator());
    return components;
  }
  
  /**
   * @return all bottom components on this board
   * @author Ying-Huan.Chu
   */
  public List<Component> getBottomComponents()
  {
    List<Component> components = new ArrayList<>();
    for (Component component : getComponents())
    {
      if (component.isBottomSide())
        components.add(component);
    }
    
    Collections.sort(components, new AlphaNumericComparator());
    return components;
  }

  /**
   * @return all component packages for this board type
   * @author Erica Wheatcroft
   */
  public List<CompPackage> getCompPackages()
  {
    Assert.expect(_boardType != null);

    return _boardType.getCompPackages();
  }

  /**
   * @return a List of Components that are inspected.
   * @author Bill Darbie
   */
  public List<Component> getInspectedComponents()
  {
    List<Component> inspectedComponents = new ArrayList<Component>();

    Collection<Component> components = getComponents();
    for (Component component : components)
    {
      if (component.isInspected())
        inspectedComponents.add(component);
    }

    Collections.sort(inspectedComponents, new AlphaNumericComparator());
    return inspectedComponents;
  }

  /**
   * @return a List of Components that are not inspected.
   * @author Andy Mechtenberg
   */
  public List<Component> getUninspectedComponents()
  {
    List<Component> uninspectedComponents = getComponents();
    List<Component> inspectedComponents = getInspectedComponents();
    uninspectedComponents.removeAll(inspectedComponents);

    return uninspectedComponents;
  }

  /**
   * Removes the component from the Board.  Used to get rid of partially defined components.
   * @author Andy Mechtenberg
   */
  public void removeComponent(Component component)
  {
    Assert.expect(component != null);

    boolean removed = false;
    if (_sideBoard1 != null)
    {
      if (_sideBoard1.hasComponent(component))
      {
        _sideBoard1.removeComponent(component);
        removed = true;
      }
    }
    if (removed == false)
    {
      if (_sideBoard2.hasComponent(component))
      {
        _sideBoard2.removeComponent(component);
        removed = true;
      }
    }

    Assert.expect(removed);
  }

  /**
   * @author Bill Darbie
   */
  public ComponentType getComponentType(String refDes)
  {
    Assert.expect(refDes != null);
    return getBoardType().getComponentType(refDes);
  }

  /**
   * @return the PadType that has the id passed in.
   * @author Bill Darbie
   */
  public PadType getPadType(String refDes, String padName)
  {
    Assert.expect(refDes != null);
    Assert.expect(padName != null);

    ComponentType componentType = getComponentType(refDes);
    PadType padType = componentType.getPadType(padName);
    return padType;
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasFiducial(String fiducialName)
  {
    Assert.expect(fiducialName != null);
    if (_sideBoard1 != null)
    {
      if (_sideBoard1.hasFiducial(fiducialName))
        return true;
    }
    if (_sideBoard2 != null)
    {
      if (_sideBoard2.hasFiducial(fiducialName))
        return true;
    }

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public Fiducial getFiducial(String fiducialName)
  {
    Assert.expect(fiducialName != null);

    Fiducial fiducial = null;
    if (_sideBoard1 != null)
    {
      if (_sideBoard1.hasFiducial(fiducialName))
        fiducial = _sideBoard1.getFiducial(fiducialName);
    }
    if (fiducial == null)
    {
      if (_sideBoard2 != null)
      {
        if (_sideBoard2.hasFiducial(fiducialName))
        fiducial = _sideBoard2.getFiducial(fiducialName);
      }
    }

    Assert.expect(fiducial != null);
    return fiducial;
  }

  /**
   * @author Bill Darbie
   */
  public List<Fiducial> getFiducials()
  {
    Set<Fiducial> fiducialSet = new TreeSet<Fiducial>(new AlphaNumericComparator());
    if (_sideBoard1 != null)
    {
      for (Fiducial fiducial : _sideBoard1.getFiducials())
      {
        boolean added = fiducialSet.add(fiducial);
        Assert.expect(added);
      }
    }
    if (_sideBoard2 != null)
    {
      for (Fiducial fiducial : _sideBoard2.getFiducials())
      {
        boolean added = fiducialSet.add(fiducial);
        Assert.expect(added);
      }
    }

    return new ArrayList<Fiducial>(fiducialSet);
  }

  /**
   * @author Bill Darbie
   */
  public String getFiducialNameIllegalChars()
  {
    return " ";
  }

  /**
   * @author Bill Darbie
   */
  public boolean isFiducialNameValid(String name)
  {
    Assert.expect(name != null);
    if (name.contains(" "))
      return false;
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isFiducialNameDuplicate(String name)
  {
    Assert.expect(name != null);

    if (((_sideBoard1 != null) && (_sideBoard1.hasFiducial(name))) ||
        ((_sideBoard2 != null) && (_sideBoard2.hasFiducial(name))))
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public void setNewFiducialName(Fiducial fiducial, SideBoard sideBoard, String oldName, String newName)
  {
    Assert.expect(fiducial != null);
    Assert.expect(sideBoard != null);
    Assert.expect(newName != null);

    sideBoard.setNewFiducialName(fiducial, oldName, newName);
  }

  /**
   * @return all the Subtype objects for this board, both top and bottom
   * @author Bill Darbie
   */
  public List<Subtype> getSubtypes()
  {
    Set<Subtype> algFamilySet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    if (_sideBoard1 != null)
      algFamilySet.addAll(_sideBoard1.getSubtypes());
    if (_sideBoard2 != null)
      algFamilySet.addAll(_sideBoard2.getSubtypes());

    return new ArrayList<Subtype>(algFamilySet);
  }

  /**
   * @return all the Subtype objects for this board, both top and bottom
   * @author Bill Darbie
   */
  public Set<Subtype> getInspectedSubtypesUnsorted()
  {
    Assert.expect(_boardType != null);

    return _boardType.getInspectedSubtypesUnsorted();
  }

  /**
   * @return all the Subtype objects for this board, both top and bottom,
   * and un-imported from any library
   * @author Wei Chin, Chong
   */
  public Set<Subtype> getInspectedAndUnImportedSubtypesUnsorted()
  {
    Assert.expect(_boardType != null);

    return _boardType.getInspectedAndUnImportedSubtypesUnsorted();
  }

  /**
   * @return all the Subtype objects for this board, both top and bottom
   * @author Bill Darbie
   */
  public List<Subtype> getInspectedSubtypes()
  {
    Assert.expect(_boardType != null);

    return _boardType.getInspectedSubtypes();
  }

  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getTestableSubtypesUnsorted(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    if (isInspected())
      return getBoardType().getTestableSubtypesUnsorted(jointTypeEnum);

    return new HashSet<Subtype>();
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getTestableSubtypes(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    if (isInspected())
      return getBoardType().getTestableSubtypes(jointTypeEnum);

    return new ArrayList<Subtype>();
  }

  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getSubtypesUnsorted(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    if (isInspected())
      return getBoardType().getSubtypesUnsorted(jointTypeEnum);

    return new HashSet<Subtype>();
  }

  /**
   * @author Wei Chin
   */
  public Set<Subtype> getSubtypesUnsorted(LandPattern landPattern, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(landPattern != null);

    if (isInspected())
      return getBoardType().getSubtypesUnsorted(landPattern, jointTypeEnum);

    return new HashSet<Subtype>();
  }
  
  /**
   * @author Bill Darbie
   */
  public List<Subtype> getSubtypes(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    if (isInspected())
      return getBoardType().getSubtypes(jointTypeEnum);

    return new ArrayList<Subtype>();
  }


  /**
   * @return a List of AlgorithmFamilyEnums of all the generic algorithm families
   * on this board
   * @author Bill Darbie
   */
  public List<InspectionFamilyEnum> getInspectionFamilyEnums()
  {
    Set<InspectionFamilyEnum> inspectionFamilyEnumSet = new HashSet<InspectionFamilyEnum>();
    if (_sideBoard1 != null)
      inspectionFamilyEnumSet.addAll(_sideBoard1.getInspectionFamilyEnums());
    if (_sideBoard2 != null)
      inspectionFamilyEnumSet.addAll(_sideBoard2.getInspectionFamilyEnums());

    List<InspectionFamilyEnum> algFamilyEnums = new ArrayList<InspectionFamilyEnum>(inspectionFamilyEnumSet);
    return algFamilyEnums;
  }

  /**
   * @author Bill Darbie
   */
  public int getNumComponents()
  {
    int numComponents = 0;

    for (Component component : getComponents())
    {
      if (component.getComponentTypeSettings().isLoaded())
      {
        ++numComponents;
      }
    }

    return numComponents;
  }

  /**
   * @author Bill Darbie
   */
  public int getNumLoadedComponents()
  {
    int numLoadedComponents = 0;

    for (Component component : getComponents())
    {
      if (component.isLoaded())
      {
        ++numLoadedComponents;
      }
    }

    return numLoadedComponents;
  }

  /**
   * @author Bill Darbie
   */
  public int getNumTestedComponents()
  {
    int numTestedComponents = 0;

    for (Component component : getComponents())
    {
      if (component.isInspected())
      {
        ++numTestedComponents;
      }
    }

    return numTestedComponents;
  }

  /**
   * @return the total number of joints on the board (inspected or not).
   * @author Bill Darbie
   */
  public int getNumJoints()
  {
    int numJoints = 0;

    for (Component component : getComponents())
    {
      if (component.getComponentTypeSettings().isLoaded())
      {
        List<Pad> pads = component.getPads();
        numJoints += pads.size();
      }
    }

    return numJoints;
  }

  /**
   * @return the total number of inspected joints on the board
   * @author Bill Darbie
   */
  public int getNumInspectedJoints()
  {
    int numJoints = 0;

    for (Component component : getComponents())
    {
      if (component.isInspected())
      {
        // the component is inspected, now check how many pins on it are inspected
        for (Pad pad : component.getPads())
        {
          if (pad.isInspected())
            ++numJoints;
        }
      }
    }

    return numJoints;
  }
  
  /**
   * @return the total number of untestable joints on the board
   * @author Chnee Khang Wah, XCR1374
   * @2011-09-08
   */
  public int getNumUnTestableJoints()
  {
    int numJoints = 0;

    for (Component component : getComponents())
    {
      // check how many pins on are testable
      for (Pad pad : component.getPads())
      {
        if (pad.isTestable())
            continue;
          ++numJoints;
      }
    }

    return numJoints;
  }
  
  /**
   * @return the total number of inspected joints, no test joint, untestable joints on the board
   * @author Chnee Khang Wah, XCR1374
   * @2011-09-12
   */
  public void gatherJointsCountDetail()
  {
    int numOfInspectedJoints = 0;
    int numOfNoTestJoints = 0;
    int numOfUnTestableJoints = 0;

    for (Component component : getComponents())
    {
        if(component.isInspected() )
        {
            for (Pad pad : component.getPads())
            {
                if(pad.isInspected())
                    ++numOfInspectedJoints;
                else if(pad.isTestable()==true)
                    ++numOfNoTestJoints;
                else if(pad.isTestable()==false)
                    ++numOfUnTestableJoints;
            }
        }
        else
        {
            for (Pad pad : component.getPads())
            {
                if(pad.isInspected())
                    ++numOfInspectedJoints;
                else if(pad.isTestable()==true)
                    ++numOfNoTestJoints;
                else if(pad.isTestable()==false)
                    ++numOfUnTestableJoints;
            }
        }
    }
    
    _numOfInspectedJoints = numOfInspectedJoints;
    _numOfNoTestedJoints = numOfNoTestJoints;
    _numOfUnTestableJoints = numOfUnTestableJoints;
  }
  
  /**
   * @return the total number of inspected joints on the board
   * @author Chnee Khang Wah, XCR1374
   * @2011-09-12
   */
  public int getNumInspectedJointsFast()
  {
    return _numOfInspectedJoints;
  }
  
  /**
   * @return the total number of untestable joints on the board
   * @author Chnee Khang Wah, XCR1374
   * @2011-09-12
   */
  public int getNumUnTestableJointsFast()
  {
    return _numOfUnTestableJoints;
  }

   /**
   * @author Khang-Wah, Chnee 14-Jun-2010 - Investigation on X-out implementation
   * //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
   */
  public void initialize(boolean isProductionRealigned, boolean isProductionMode)
  {
    //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
    if (isProductionRealigned == false)
    {
      //XCR-3392 - Wei Chin
      if(_numberOfFailedJoint == null)
      {
        _numberOfFailedJoint = new AtomicInteger(0);
      }
      else
      {
        _numberOfFailedJoint.set(0);
      }
      
      if(_numberOfFailedComponent == null)
      {
        _numberOfFailedComponent = new AtomicInteger(0);
      }
      else
      {
        _numberOfFailedComponent.set(0);
      }
    }

    _isXedOut = false;
    _xoutReason = "";
    
    // XCR1624 - Wrong display of panel pins proccesed - Siew Yeng
    _alignmentException = false;
    
    _isProductionMode = isProductionMode;
    _isXoutDetectionModeEnabled = TestExecution.getInstance().isXoutDetectionModeEnabled();
    _isDefectXoutDetectionMode =TestExecution.getInstance().isDefectThresholdXoutDetectionModeEnabled();

    // Xed Out calculation
    int xoutThresholdValue = TestExecution.getInstance().getXoutDetectionThreshold(); // customer setting in config
    _minNumFailingJointsForXout = getNumInspectedJoints() * xoutThresholdValue / 100; // threshold % of total inspected joints (display option have show only use joints failure to determine)
  }
  
  /**
   * @author Khang-Wah, Chnee 14-Jun-2010 - Investigation on X-out implementation
   */
  public void addFailedJoint()
  {
    _numberOfFailedJoint.incrementAndGet();
  }

  /**
   * @return the total number of failed joints on the board
   * @author Khang-Wah, Chnee 14-Jun-2010 - Investigation on X-out implementation
   */
  public int getNumFailedJoints()
  {
    return _numberOfFailedJoint.get();
  }

    /**
   * @author Chin Seong, Kee 3-Sept-2010 - Investigation on X-out implementation
   */
  public void addFailedComponent()
  {
    _numberOfFailedComponent.incrementAndGet();
  }

  /**
   * @return the total number of failed components on the board
   * @author Chin Seong, Kee 3-Sept-2010 - Investigation on X-out implementation
   */
  public int getNumFailedComponents()
  {
    return _numberOfFailedComponent.get();
  }

  /**
   * @return XedOut status of the board, true - XedOuted, false - not XedOut
   * @author Khang-Wah, Chnee 14-Jun-2010 - Investigation on X-out implementation
   * @author Chin-Seong, Kee 06, Sept 2010, added getNumFailedComponents(), 1 component
   * @author Khaw Chek Hau - XCR3385: X-out board by defect threshold setting
   * failed represent 1 Pad failed
   */
  public boolean isXedOut()
  {
    //Khaw Chek Hau - XCR3385: X-out board by defect threshold setting
    if (_isProductionMode && _isXedOut == false && _isXoutDetectionModeEnabled == true && _isDefectXoutDetectionMode == true && getPanel().isMultiBoardPanel())
    {
      //Khaw Chek Hau - XCR3578: Assert when skip serial number for board based alignment
      if (_numberOfFailedJoint != null && getNumFailedJoints() >= _minNumFailingJointsForXout) 
      {
        _isXedOut = true;
        setXoutReason(StringLocalizer.keyToString("XOUT_DUE_TO_HIGH_FALSE_CALL_KEY"));
      }
    }
    return _isXedOut;
  }

  /**
   * @author Wei Chin
   */
  public void forceXout()
  {
    _isXedOut = true;
  }
  
  /*
   * @author Kee Chin Seong - this is to write out the reason 
   *                          why the board is xedout
   */
  public String getXoutReason()
  {
    return _xoutReason;
  }
  
  /*
   * @author Kee Chin Seong - this is to write out the reason 
   *                          why the board is xedout
   */
  public void setXoutReason(String xOutReason)
  {
    _xoutReason = xOutReason;
  }

  
  /**
   * @param exceptionCaught true if exception caught
   * @author Phang Siew Yeng
   */
  public void setBoardAlignmentExceptionCaught(boolean exceptionCaught)
  {
    _alignmentException =  exceptionCaught;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public boolean isBoardAlignmentExceptionCaught()
  {
    return _alignmentException;
  }
  
  /**
   * @author Bill Darbie
   */
  public Set<Pad> getPadsUnsorted()
  {
    Set<Pad> padSet = new HashSet<Pad>();
    for (Component component : getComponents())
    {
      padSet.addAll(component.getPads());
    }

    return padSet;
  }


  /**
   * WARNING: this can be a slow call, if you do not need Pads sorted alphabetically
   * you would be better off using getUnsortedPads().
   *
   * @author Bill Darbie
   */
  public List<Pad> getPads()
  {
    Set<Pad> padSet = new TreeSet<Pad>(new PadComparator());
    for (Pad pad : getPadsUnsorted())
    {
      boolean added = padSet.add(pad);
      Assert.expect(added);
    }

    return new ArrayList<Pad>(padSet);
  }

  /**
   * @author Bill Darbie
   */
  public List<JointTypeEnum> getJointTypeEnums()
  {
    Assert.expect(_boardType != null);
    return _boardType.getJointTypeEnums();
  }

  /**
   * @return a List of Subtype objects that all come from the same
   * algorithmFamilyName on this Panel and are being used by at least one Pad that is being inspected.
   * @author George A. David
   */
  public List<Subtype> getInspectedSubtypes(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    if (isInspected())
      return getBoardType().getInspectedSubtypes(jointTypeEnum);

    return new ArrayList<Subtype>();
  }

  /**
    * @return a List of Subtype objects that all come from the same
    * algorithmFamilyName on this Panel , un-imported from any library
    * and are being used by at least one Pad that is being inspected.
    * @author Wei Chin, Chong
    */
   public List<Subtype> getInspectedAndUnImportedSubtypes(JointTypeEnum jointTypeEnum)
   {
     Assert.expect(jointTypeEnum != null);

     if (isInspected())
       return getBoardType().getInspectedAndUnImportedSubtypes(jointTypeEnum);

     return new ArrayList<Subtype>();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isInspected()
  {
    return getBoardSettings().isInspected();
  }

  /**
   * @author Bill Darbie
   */
  public List<JointTypeEnum> getInspectedJointTypeEnums()
  {
    if (isInspected())
      return getBoardType().getInspectedJointTypeEnums();

    return new ArrayList<JointTypeEnum>();
  }

  /**
   * @author Bill Darbie
   */
  public List<JointTypeEnum> getTestableJointTypeEnums()
  {
    if (isInspected())
      return getBoardType().getTestableJointTypeEnums();

    return new ArrayList<JointTypeEnum>();
  }

  /**
   * @return the x axis width of the Board BEFORE any rotation has been applied.
   * @author Bill Darbie
   */
  public int getWidthInNanoMeters()
  {
    Assert.expect(_boardType != null);

    return _boardType.getWidthInNanoMeters();
  }

  /**
   * @return the x axis width of the Board AFTER the Board and Panel have been rotated.
   * @author Bill Darbie
   */
  public int getWidthAfterAllRotationsInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    java.awt.Rectangle bounds = shape.getBounds();

    return bounds.width;
  }

  /**
   * @return the y axis length of the Board BEFORE any rotation has been applied.
   * @author Bill Darbie
   */
  public int getLengthInNanoMeters()
  {
    Assert.expect(_boardType != null);

    return _boardType.getLengthInNanoMeters();
  }

  /**
   * @return the y axis length of the Board AFTER the Board and Panel have been rotated.
   * @author Bill Darbie
   */
  public int getLengthAfterAllRotationsInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    java.awt.Rectangle bounds = shape.getBounds();

    return bounds.height;
  }

  /**
   * @author Laura Cormos
   */
  public boolean isBoardSingleSided()
  {
    if (sideBoard1Exists() && sideBoard2Exists())
      return false;
    else
      return true;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isBoardWithinPanelOutline()
  {
    java.awt.Shape panelShape = getPanel().getShapeInNanoMeters();
    java.awt.Shape boardShape = getShapeRelativeToPanelInNanoMeters();

    // allow 100 nanometers for rounding errors
    return MathUtil.fuzzyContains(panelShape, boardShape, 500);
  }

  /**
   * @author Bill Darbie
   */
  public boolean willBoardBeInsidePanelOutlineAfterOffset(int xOffsetAfterAllFlipsAndRotationsInNanoMeters, int yOffsetAfterAllFlipsAndRotationsInNanometers)
  {
    // we will temporarily move the board to see if it falls outside the panel outline.
    // suppress sending any ProjectStateEvents that might occur because of this since we will
    // immediatly return the component back to its original position
    getPanel().getProject().getProjectState().disableProjectStateEvents();
    boolean boardWithinPanel = false;
    try
    {
      moveByOffSetInNanoMeters(xOffsetAfterAllFlipsAndRotationsInNanoMeters,
                               yOffsetAfterAllFlipsAndRotationsInNanometers);
      boardWithinPanel = isBoardWithinPanelOutline();
      moveByOffSetInNanoMeters( -xOffsetAfterAllFlipsAndRotationsInNanoMeters,
                               -yOffsetAfterAllFlipsAndRotationsInNanometers);
    }
    finally
    {
      getPanel().getProject().getProjectState().enableProjectStateEvents();
    }

    return boardWithinPanel;
  }

  /**
   * Move this Boards origin by the offset passed in.  The offset passed in is an offset of the
   * Board as viewed from x-rays after all rotations and flips have occured.
   *
   * @author Bill Darbie
   */
  public void moveByOffSetInNanoMeters(int xOffsetAfterAllFlipsAndRotationsInNanoMeters, int yOffsetAfterAllFlipsAndRotationsInNanometers)
  {
    // create the correct transform
    AffineTransform transform = AffineTransform.getTranslateInstance(0, 0);
    preConcatenateShapeTransform(transform, false, false, true);

    // use the inverse of that transform to figure out the correct offset
    Point2D sourcePoint = new Point2D.Double(xOffsetAfterAllFlipsAndRotationsInNanoMeters, yOffsetAfterAllFlipsAndRotationsInNanometers);
    Point2D destPoint = new Point2D.Double(0, 0);
    try
    {
      transform.inverseTransform(sourcePoint, destPoint);
    }
    catch (NoninvertibleTransformException nte)
    {
      Assert.expect(false);
    }

    int xOffset = MathUtil.roundDoubleToInt(destPoint.getX());
    int yOffset = MathUtil.roundDoubleToInt(destPoint.getY());

    // apply the offset to the ComponentType
    PanelCoordinate panelCoord = getCoordinateInNanoMeters();

    panelCoord.setX(panelCoord.getX() + xOffset);
    panelCoord.setY(panelCoord.getY() + yOffset);

    setCoordinateInNanoMeters(panelCoord);
  }

  /**
   * @author Bill Darbie
   */
  public java.awt.Shape getXaxisLineAfterAllRotationsInNanometers()
  {
    Point2D originPoint = new Point2D.Double(0, 0);
    Point2D axisPoint = new Point2D.Double(originPoint.getX() + getWidthInNanoMeters(), originPoint.getY());
    java.awt.geom.Line2D line = new Line2D.Double(originPoint, axisPoint);

    // build up the proper transform
    AffineTransform transform = AffineTransform.getTranslateInstance(0, 0);
    preConcatenateShapeTransform(transform);

    // create the initial shape
    java.awt.Shape shape = transform.createTransformedShape(line);

    return shape;
  }

  /**
   * @author Laura Cormos
   */
  public java.awt.Shape getYaxisLineAfterAllRotationsInNanometers()
  {
    Point2D originPoint = new Point2D.Double(0, 0);
    Point2D axisPoint = new Point2D.Double(originPoint.getX(), originPoint.getY() + getLengthInNanoMeters());
    java.awt.geom.Line2D line = new Line2D.Double(originPoint, axisPoint);

    // build up the proper transform
    AffineTransform transform = AffineTransform.getTranslateInstance(0, 0);
    preConcatenateShapeTransform(transform);

    // create the initial shape
    java.awt.Shape shape = transform.createTransformedShape(line);

    return shape;
  }


  /**
   * @return the origin of the CAD in PanelCoordinates.
   * @author Bill Darbie
   */
  public PanelCoordinate getCadOriginRelativeToPanelInNanometers()
  {
    java.awt.Shape shape = getCadOriginShapeRelativeToPanelInNanoMeters();
    java.awt.geom.Rectangle2D bounds = shape.getBounds2D();

    int x = MathUtil.roundDoubleToInt(bounds.getCenterX());
    int y = MathUtil.roundDoubleToInt(bounds.getCenterY());

    PanelCoordinate coord = new PanelCoordinate(x, y);
    return coord;
  }

  /**
   * @return the shape of the origin of the CAD in PanelCoordinates.
   * @author Bill Darbie
   */
  public java.awt.Shape getCadOriginShapeRelativeToPanelInNanoMeters()
  {
    java.awt.Shape shape = new Line2D.Double(0, 0, 0, 0);

    // build up the proper transform
    AffineTransform transform = AffineTransform.getTranslateInstance(0, 0);
    preConcatenateShapeTransform(transform);

    // apply the transform to it
    shape = transform.createTransformedShape(shape);

    Assert.expect(shape != null);
    return shape;
  }

  /**
   * @author Bill Darbie
   */
  void invalidateShape()
  {
    _shape = null;
    _degrees = null;
    for (Component component : getComponents())
      component.invalidateShape();
  }

  /**
   * @return the Board shape, not taking into account any panel rotation, as seen by x-rays.
   * @author Andy Mechtenberg
   */
  public java.awt.Shape getShapeInNanoMeters()
  {
    Assert.expect(_boardType != null);

    return _boardType.getShapeInNanoMeters();
  }

  /**
   * @return the Board shape, after rotating the board, and the panel
   * @author Andy Mechtenberg
   */
  public java.awt.Shape getShapeRelativeToPanelInNanoMeters()
  {
    if (_shape == null)
    {
      PanelCoordinate boardOriginCoord = getCoordinateInNanoMeters();
      int coordX = boardOriginCoord.getX();
      int coordY = boardOriginCoord.getY();
      Assert.expect(_boardType != null);

      _shape = _boardType.getShapeInNanoMeters();

      // build up the transform
      AffineTransform trans = AffineTransform.getTranslateInstance(0, 0);
      if (_leftToRightFlip)
      {
        // do a left-to-right flip before the rotation
        trans.preConcatenate(AffineTransform.getScaleInstance( -1, 1));
        trans.preConcatenate(AffineTransform.getTranslateInstance(getWidthInNanoMeters(), 0));
      }
      if (_topToBottomFlip)
      {
        // do a top-to-bottom flip before the rotation
        trans.preConcatenate(AffineTransform.getScaleInstance(1, -1));
        trans.preConcatenate(AffineTransform.getTranslateInstance(0, getLengthInNanoMeters()));
      }

//      trans = AffineTransform.getTranslateInstance(coordX, coordY);

      int degrees = getDegreesRotationRelativeToPanel();
//      trans.preConcatenate(AffineTransform.getRotateInstance(Math.toRadians(degrees), coordX, coordY));
      trans.preConcatenate(AffineTransform.getRotateInstance(Math.toRadians(degrees)));
      trans.preConcatenate(AffineTransform.getTranslateInstance(coordX, coordY));

      Panel panel = getPanel();
      panel.preConcatenateShapeTransform(trans);

      // apply the transform
      _shape = trans.createTransformedShape(_shape);
    }

    Assert.expect(_shape != null);
    return _shape;
  }

  /**
   * Take the transform passed in and modify it to take into account if things are on the top or bottom
   * side of the board as well as the Boards rotation and location relative to the Panel as seen by x-rays.
   *
   * @author Bill Darbie
   */
  public void preConcatenateShapeTransform(AffineTransform transform)
  {
    Assert.expect(transform != null);

    preConcatenateShapeTransform(transform, true, true, true);
  }
  
  /**
   * Take the transform passed in and modify it to take into account if things are on the top or bottom
   * side of the board as well as the Boards rotation and location relative to CAD origin.
   * 
   * @author Phang Siew Yeng
   */
  public void preConcatenateShapeTransform(AffineTransform transform, boolean panelTransformation)
  {
    Assert.expect(transform != null);

    preConcatenateShapeTransform(transform, true, true, panelTransformation);
  }

  /**
   * @author Bill Darbie
   * @edited by Siew Yeng, Phang
   * - applyPanelTransformation true - relative to panel
   * - applyPanelTransformation false - relative to CAD origin
   */
  void preConcatenateShapeTransform(AffineTransform transform,
                                    boolean applyPanelAndBoardOriginTranslation,
                                    boolean applyFlipAndRotation,
                                    boolean applyPanelTransformation)
  {
    Assert.expect(transform != null);

    PanelCoordinate boardCoordinate = getCoordinateInNanoMeters();

    if (applyFlipAndRotation)
    {
      if (_leftToRightFlip)
      {
        // do a left-to-right flip before the rotation
        transform.preConcatenate(AffineTransform.getScaleInstance( -1, 1));
        if (applyPanelAndBoardOriginTranslation)
          transform.preConcatenate(AffineTransform.getTranslateInstance(getWidthInNanoMeters(), 0));
      }

      if (_topToBottomFlip)
      {
        // do a top-to-bottom flip before the rotation
        transform.preConcatenate(AffineTransform.getScaleInstance(1, -1));
        if (applyPanelAndBoardOriginTranslation)
          transform.preConcatenate(AffineTransform.getTranslateInstance(0, getLengthInNanoMeters()));
      }

      // rotate the shape by the boards rotation
      int degrees = getDegreesRotationRelativeToPanel();
      transform.preConcatenate(AffineTransform.getRotateInstance(Math.toRadians(degrees)));
    }

    if (applyPanelAndBoardOriginTranslation)
    {
      // translate to get proper location relative to the panels lower left corner
      transform.preConcatenate(AffineTransform.getTranslateInstance(boardCoordinate.getX(),
                                                                    boardCoordinate.getY()));
    }
    
    // Siew Yeng - relative to panel if true
    if(applyPanelTransformation)
    {
      Panel panel = getPanel();
      panel.preConcatenateShapeTransform(transform, applyPanelAndBoardOriginTranslation);
    }
  }

  /**
   * @return
   * @author Wei Chin, Chong
   */
  public boolean isLongBoard()
  {
    if ( getLengthAfterAllRotationsInNanoMeters() > XrayTester.getMaxImageableAreaLengthInNanoMeters())
      return true;

    return false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isOpticalLongBoard()
  {
    if (getWidthAfterAllRotationsInNanoMeters() > XrayTester.getMaxOpticalImageableAreaWidthInNanoMeters())
      return true;
    
    return false;
  }
  
  /**
   * @author Chong Wei Chin
   */
  public int getAlignmentMaxImageableAreaLengthInNanoMeters()
  {
    int boardLengthAfterAllRotationInNanoMeters = getLengthAfterAllRotationsInNanoMeters() + 1 * Alignment.getAlignmentUncertaintyBorderInNanoMeters();

    int maxLength = ReconstructionRegion.getMaxVerificationRegionLengthInNanoMeters();

    int totalRows = (int) Math.ceil(boardLengthAfterAllRotationInNanoMeters / maxLength);

    int rowForHalfPanel = (int) Math.ceil( totalRows / 2);

    // add 2 Verification Region Overlap
    int width = (rowForHalfPanel + 2) * maxLength;

    return width - 1 * Alignment.getAlignmentUncertaintyBorderInNanoMeters();
  }

  /**
   * @author Chong Wei Chin
   */
  public PanelRectangle getRightSectionOfLongBoard()
  {
    PanelRectangle rightSection = new PanelRectangle(getShapeRelativeToPanelInNanoMeters());
    if(isLongBoard())
    {
      double yCoordinate = rightSection.getMinY();

      // Since it add 2 Verification Region Overlap from getAlignmentMaxImageableAreaLengthInNanoMeters() ,
//       Y-offset need to minus 2 VR Region
      double yOffset = rightSection.getHeight() - getAlignmentMaxImageableAreaLengthInNanoMeters();

      if (yOffset > 0)
        yCoordinate += yOffset;

      rightSection.setRect(rightSection.getMinX(),
                           yCoordinate,
                           rightSection.getWidth(),
                           getAlignmentMaxImageableAreaLengthInNanoMeters());

    }

    return rightSection;
  }

  /**
   * @return
   * @author Wei Chin
   */
  public PanelRectangle getLeftSectionOfLongBoard()
  {
    //XCR-3313, Assert when load board based long panel recipe
    //Assert.expect(isLongBoard());
    PanelRectangle leftSection = new PanelRectangle(getShapeRelativeToPanelInNanoMeters());

    leftSection.setRect( leftSection.getMinX(),
                         leftSection.getMinY(),
                         leftSection.getWidth(),
                         getAlignmentMaxImageableAreaLengthInNanoMeters());

    return leftSection;
  }
  
  /**
   * The maximum viewable area for Optical Camera 1 is defined in
   * XrayTester.getMaxOpticalImageableAreaWidthInNanoMeters(). So if the panel width
   * exceeds the limit, we need to divide the panel into 2 sections.
   * 
   * @author Cheah Lee Herng
   */
  public PanelRectangle getBottomSectionOfOpticalLongBoard()
  {
    PanelRectangle opticalBottomSection = new PanelRectangle(getShapeInNanoMeters());
    if (isOpticalLongBoard())
    {
      double xCoordinate = opticalBottomSection.getMaxX();
      double xOffset = XrayTester.getMaxOpticalImageableAreaWidthInNanoMeters();
      if (xOffset > 0)
        xCoordinate -= xOffset;

      opticalBottomSection.setRect(xCoordinate,
                                   opticalBottomSection.getMinY(),
                                   XrayTester.getMaxOpticalImageableAreaWidthInNanoMeters(),
                                   opticalBottomSection.getHeight());
    }
    return opticalBottomSection;
  }
  
  /**
   * The maximum viewable area for Optical Camera 1 is defined in
   * XrayTester.getMaxOpticalImageableAreaWidthInNanoMeters(). So if the panel width
   * exceeds the limit, we need to divide the panel into 2 sections and the rest of 
   * the area that Optical Camera 1 cannot cover will be the viewable area of Optical Camera 2.
   * 
   * @author Ying-Huan.Chu
   */
  public PanelRectangle getTopSectionOfOpticalLongBoard()
  {
    PanelRectangle opticalTopSection = new PanelRectangle(getShapeInNanoMeters());
    if (isOpticalLongBoard())
    {
      double xCoordinate = opticalTopSection.getMinX();
      double width = opticalTopSection.getWidth() - XrayTester.getMaxOpticalImageableAreaWidthInNanoMeters();
      opticalTopSection.setRect(xCoordinate,
                                opticalTopSection.getMinY(),
                                width,
                                opticalTopSection.getHeight());
    }
    return opticalTopSection;
  }
  
  /**
   * @param _alignmentGroupIndex
   * @param pads
   * @param fiducials
   * 
   * @author Wei Chin
   */
  public void addPadsToAlignmentGroups(int alignmentGroupIndex, List<Pad> pads, List<Fiducial> fiducials)
  {
    AlignmentGroup alignmentGroup = null;

    if (alignmentGroupIndex > 2)
    {
      alignmentGroup = getBoardSettings().getLeftAlignmentGroupsForLongPanel().get(alignmentGroupIndex % 3);
    }
    else
    {
      if(getBoardSettings().useLeftAlignmentGroup())
        alignmentGroup = getBoardSettings().getLeftAlignmentGroupsForLongPanel().get(alignmentGroupIndex);
      else
        alignmentGroup = getBoardSettings().getRightAlignmentGroupsForLongPanel().get(alignmentGroupIndex);
    }

    Assert.expect(alignmentGroup != null);

    List<Pad> newPadsToAdd = getPadToAddForNextBoard(pads, this);
    List<Fiducial> newFiducialsToAdd = getFiducialToAddForNextBoard(fiducials, this);
    for (Pad pad : newPadsToAdd)
    {
      if(alignmentGroup.contains(pad) == false)
        alignmentGroup.addPad(pad);
    }
    for (Fiducial fiducial : newFiducialsToAdd)
    {
      if(alignmentGroup.contains(fiducial) == false)
        alignmentGroup.addFiducial(fiducial);
    }
    newPadsToAdd.clear();
    newFiducialsToAdd.clear();
  }

    /**
   * @param _alignmentGroupIndex
   * @param pads
   * @param fiducials
   * 
   * @author Wei Chin
   */
  public void removePadsToAlignmentGroups(int alignmentGroupIndex, List<Pad> pads, List<Fiducial> fiducials)
  {
    AlignmentGroup alignmentGroup = null;
    if (alignmentGroupIndex > 2)
    {
      alignmentGroup = getBoardSettings().getLeftAlignmentGroupsForLongPanel().get(alignmentGroupIndex % 3);
    }
    else
    {
      if(getBoardSettings().useLeftAlignmentGroup())
        alignmentGroup = getBoardSettings().getLeftAlignmentGroupsForLongPanel().get(alignmentGroupIndex);
      else
        alignmentGroup = getBoardSettings().getRightAlignmentGroupsForLongPanel().get(alignmentGroupIndex);
    }
    Assert.expect(alignmentGroup != null);

    List<Pad> newPadsToAdd = getPadToAddForNextBoard(pads, this);
    List<Fiducial> newFiducialsToAdd = getFiducialToAddForNextBoard(fiducials, this);
    for (Pad pad : newPadsToAdd)
    {
      if(alignmentGroup.contains(pad))
        alignmentGroup.removePad(pad);
    }
    for (Fiducial fiducial : newFiducialsToAdd)
    {
      if(alignmentGroup.contains(fiducial))
        alignmentGroup.removeFiducial(fiducial);
    }
    newPadsToAdd.clear();
    newFiducialsToAdd.clear();
  }
  
  /**
   * @param padsToAdd
   * @param currentBoard
   * @author Wei Chin, Chong
   */
  private List<Pad> getPadToAddForNextBoard(List <Pad> padsToAdd, Board nextBoard)
  {
    Assert.expect(padsToAdd != null);

    List <Pad> newPadsToAdd = new ArrayList();

    for(Pad pad : padsToAdd)
    {
      newPadsToAdd.add(_boardType.getPanel().getPad(nextBoard.getName(), pad.getComponent().getReferenceDesignator(), pad.getName()));
    }
    return newPadsToAdd;
  }

  /**
   * @param fiducialsToAdd
   * @param currentBoard
   * @author Wei Chin, Chong
   */
  private List<Fiducial> getFiducialToAddForNextBoard(List<Fiducial> fiducialsToAdd, Board nextBoard)
  {
    Assert.expect(fiducialsToAdd != null);

    List <Fiducial> newFiducialsToAdd = new ArrayList();

    for(Fiducial fiducial : fiducialsToAdd)
    {
      newFiducialsToAdd.add(_boardType.getPanel().getFiducial(nextBoard.getName(), fiducial.getName()));
    }
    return newFiducialsToAdd;
  }
  
  /**
   * @param _alignmentGroupIndex
   * @param pads
   * @param fiducials
   * 
   * @author Wei Chin
   */
  public void syncPadsToAlignmentGroupsIfNecessary()
  {
    if(getBoardSettings().hasLeftAlignmentPads() || 
       getBoardSettings().hasRightAlignmentPads() )
    {
      return;
    }
    
    _projectObservable.setEnabled(false);
    try
    {
      int alignmentGroupIndex = 0;
      Board firstBoard = _boardType.getPanel().getFirstBoardContainsAlignmentRegions();

      if(firstBoard == null)
        return;

      if(firstBoard.getBoardSettings().isLongBoard())
      {
        for(alignmentGroupIndex=0; alignmentGroupIndex < 6; alignmentGroupIndex++)
        {
          AlignmentGroup alignmentGroup = firstBoard.getBoardSettings().getAllAlignmentGroups().get(alignmentGroupIndex);
          if(alignmentGroup.getPads().isEmpty() && alignmentGroup.getFiducials().isEmpty())
            continue;
          else
            addPadsToAlignmentGroups(alignmentGroupIndex,alignmentGroup.getPads(),alignmentGroup.getFiducials());
        }
      }
      else
      {
        for(alignmentGroupIndex=0; alignmentGroupIndex<3; alignmentGroupIndex++)
        {
          AlignmentGroup alignmentGroup = null;
          if(firstBoard.getBoardSettings().useLeftAlignmentGroup())
            alignmentGroup = firstBoard.getBoardSettings().getLeftAlignmentGroupsForLongPanel().get(alignmentGroupIndex);
          else
            alignmentGroup = firstBoard.getBoardSettings().getRightAlignmentGroupsForLongPanel().get(alignmentGroupIndex);

          if(alignmentGroup.getPads().isEmpty() && alignmentGroup.getFiducials().isEmpty())
            continue;
          else
          {
            if(getBoardSettings().useLeftAlignmentGroup())
              addPadsToAlignmentGroups(alignmentGroupIndex,alignmentGroup.getPads(),alignmentGroup.getFiducials());
            else
              addPadsToAlignmentGroups(alignmentGroupIndex,alignmentGroup.getPads(),alignmentGroup.getFiducials());
          }
        }
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
  }
  
  /**
   * Reset xout variable
   * @author Kok Chun, Tan
   */
  public void resetXout()
  {
    _isXedOut = false;
    _xoutReason = "";
  }
  
  /**
   * XCR3578: Assert when skip serial number for board based alignment
   * @author Khaw Chek Hau
   */
  public void resetNumberOfFailedJointsAndComponents()
  {
    if (_numberOfFailedJoint != null && _numberOfFailedComponent != null)
    {
      _numberOfFailedJoint.set(0);
      _numberOfFailedComponent.set(0);       
    }
  }
  
  public List<Subtype> getUsedSubtype()
  {
    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    for (Subtype subtype : getUsedSubtypesUnsorted())
    {
      boolean added = subtypeSet.add(subtype);
      Assert.expect(added);
    }

    return new ArrayList<Subtype>(subtypeSet);
  }
  
  public Set<Subtype> getUsedSubtypesUnsorted()
  {
    Set<Subtype> subtypeSet = new HashSet<Subtype>();

    subtypeSet.addAll(getBoardType().getSubtypesUnsorted());

    return subtypeSet;
  }
}