package com.axi.v810.business.panelDesc;

import java.io.*;
import java.awt.geom.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.util.*;

/**
 * This class describes a single unique through hole pad.
 * There is one ThroughHoleLandPatternPad instance for each unique ThroughHoleLandPatternPad on the Panel.
 * Many Pad instances on the same Panel will share a reference to the same
 * ThroughHoleLandPatternPad.
 * The hole coordinate is relative to land pattern center (just like the LandPatternPad's
 * coordinates are referenced from the the land pattern's center.)
 * All holes are assumed round
 *
 * @author Andy Mechtenberg
 * @author Keith Lee
 */
public class ThroughHoleLandPatternPad extends LandPatternPad implements Serializable
{
  // created by ThroughHoleLandPatternNdfReader, LandPatternReader

  // note that the hole coordinate may be different than the pad coordinate (for off center holes)
  // relative to the land pattern's center
  private ComponentCoordinate _holeCoordinateInNanoMeters; // ThroughHoleLandPatternNdfReader, LandPatternReader
//  private int _holeDiameterInNanoMeters; // ThroughHoleLandPatternNdfReader, LandPatternReader
  //Siew Yeng - XCR-3318 - Oval PTH
  private int _holeWidthInNanoMeters; // ThroughHoleLandPatternNdfReader, LandPatternReader
  private int _holeLengthInNanoMeters; // ThroughHoleLandPatternNdfReader, LandPatternReader

  private static transient ProjectObservable _projectObservable;

  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public ThroughHoleLandPatternPad()
  {
    // do nothing
  }

  /**
   * @author Keith Lee
   */
  public void setHoleCoordinateInNanoMeters(ComponentCoordinate holeCoordinateInNanoMeters)
  {
    Assert.expect(holeCoordinateInNanoMeters != null);

    if (holeCoordinateInNanoMeters == _holeCoordinateInNanoMeters)
      return;

    ComponentCoordinate oldValue = _holeCoordinateInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _holeCoordinateInNanoMeters = holeCoordinateInNanoMeters;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, holeCoordinateInNanoMeters, ThroughHoleLandPatternPadEventEnum.HOLE_COORDINATE);
  }

  /**
   * @author Keith Lee
   */
  public ComponentCoordinate getHoleCoordinateInNanoMeters()
  {
    Assert.expect(_holeCoordinateInNanoMeters != null);
    return new ComponentCoordinate(_holeCoordinateInNanoMeters);
  }

  /**
   * Not using anymore - Siew Yeng
   * @author Keith Lee
   */
//  public void setHoleDiameterInNanoMeters(int holeDiameterInNanoMeters)
//  {
//    Assert.expect(holeDiameterInNanoMeters > 0);
//
//    if (holeDiameterInNanoMeters == _holeDiameterInNanoMeters)
//      return;
//
//    int oldValue = _holeDiameterInNanoMeters;
//    _projectObservable.setEnabled(false);
//    try
//    {
//      _holeDiameterInNanoMeters = holeDiameterInNanoMeters;
//    }
//    finally
//    {
//      _projectObservable.setEnabled(true);
//    }
//    _projectObservable.stateChanged(this, oldValue, holeDiameterInNanoMeters, ThroughHoleLandPatternPadEventEnum.HOLE_DIAMETER);
//  }

  /**
   * Not using anymore - Siew Yeng
   * @author Keith Lee
   */
//  public int getHoleDiameterInNanoMeters()
//  {
//    Assert.expect(_holeDiameterInNanoMeters > 0);
//
//    return _holeDiameterInNanoMeters;
//  }

  /**
   * @author Patrick Lacz
   */
  public java.awt.Shape getHoleShapeInNanoMeters()
  {
    ComponentCoordinate holeCoordinateInNanoMeters = getHoleCoordinateInNanoMeters();
//    int holeDiameterInNanoMeters = getHoleDiameterInNanoMeters();
    //Siew Yeng - XCR-3318 - Oval PTH
    int holeWidthInNanometers = getHoleWidthInNanoMeters();
    int holeLengththInNanometers = getHoleLengthInNanoMeters();

    java.awt.Shape holeShape = null;
    
    if(isOvalThroughHole() == false)
    {
      holeShape = new Ellipse2D.Double(holeCoordinateInNanoMeters.getX() - holeWidthInNanometers / 2,
                                        holeCoordinateInNanoMeters.getY() - holeLengththInNanometers / 2,
                                        holeWidthInNanometers,
                                        holeLengththInNanometers);
    }
    else
    {
      holeShape = GeomUtil.createObround(holeCoordinateInNanoMeters.getX() - holeWidthInNanometers / 2,
                              holeCoordinateInNanoMeters.getY() - holeLengththInNanometers / 2,
                              holeWidthInNanometers,
                              holeLengththInNanometers);
    }
    return holeShape;
  }

  /**
   * @return true if the pad is throughole, false otherwise
   * @author Andy Mechtenberg
   */
  public boolean isThroughHolePad()
  {
    return true;
  }

  /**
   * @return true if the pad is throughole, false otherwise
   * @author Bill Darbie
   */
  public boolean isSurfaceMountPad()
  {
    return false;
  }

  /**
   * @author Bill Darbie
   */
  ThroughHoleLandPatternPad createDuplicate(LandPattern landPattern)
  {
    ThroughHoleLandPatternPad thLandPatternPad = (ThroughHoleLandPatternPad)super.createDuplicate(landPattern);
    thLandPatternPad.setHoleCoordinateInNanoMeters(getHoleCoordinateInNanoMeters());
//    thLandPatternPad.setHoleDiameterInNanoMeters(getHoleDiameterInNanoMeters());
    //Siew Yeng - XCR-3318 - Oval PTH
    thLandPatternPad.setHoleWidthInNanoMeters(getHoleWidthInNanoMeters());
    thLandPatternPad.setHoleLengthInNanoMeters(getHoleLengthInNanoMeters());

    return thLandPatternPad;
  }
  
  /**
   * @author Siew Yeng
   */
  public void setHoleWidthInNanoMeters(int holeWidthInNanoMeters)
  {
    Assert.expect(holeWidthInNanoMeters > 0);

    if (holeWidthInNanoMeters == _holeWidthInNanoMeters)
      return;

    int oldValue = _holeWidthInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _holeWidthInNanoMeters = holeWidthInNanoMeters;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, holeWidthInNanoMeters, ThroughHoleLandPatternPadEventEnum.HOLE_WIDTH);
  }
  
  /**
   * @author Siew Yeng
   */
  public int getHoleWidthInNanoMeters()
  {
    Assert.expect(_holeWidthInNanoMeters > 0);

    return _holeWidthInNanoMeters;
  }
  
  /**
   * @author Siew Yeng
   */
  public void setHoleLengthInNanoMeters(int holeLengthInNanoMeters)
  {
    Assert.expect(holeLengthInNanoMeters > 0);

    if (holeLengthInNanoMeters == _holeLengthInNanoMeters)
      return;

    int oldValue = _holeLengthInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _holeLengthInNanoMeters = holeLengthInNanoMeters;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, holeLengthInNanoMeters, ThroughHoleLandPatternPadEventEnum.HOLE_LENGTH);
  }
  
  /**
   * @author Siew Yeng
   */
  public int getHoleLengthInNanoMeters()
  {
    Assert.expect(_holeLengthInNanoMeters > 0);

    return _holeLengthInNanoMeters;
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean isOvalThroughHole()
  {
    return getHoleWidthInNanoMeters() != getHoleLengthInNanoMeters();
  }
}
