package com.axi.v810.business.panelDesc;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * There is one instance of this class for each type of board.  The Board class will
 * have one instance for every physical Board on the Panel.
 *
 * @author Bill Darbie
 */
public class BoardType implements Serializable
{
  // created by PanelNdfReader, BoardTypeReader

  private Panel _panel;

  private String _name; // PanelNdfReader, BoardTypeReader
  private int _widthInNanoMeters; // MainReader, BoardTypeReader
  private int _lengthInNanoMeters; // MainReader, BoardTypeReader

  private Set<Board> _boardSet = new HashSet<Board>(); // PanelNdfReader, PanelReader

  // topSide board is the board the is facing the x-ray tube
  private SideBoardType _sideBoardType1; // PanelNdfReader, BoardTypeReader
  // bottomSideBoard is the board that is furthest from the x-ray tube
  private SideBoardType _sideBoardType2; // PanelNdfReader, BoardTypeReader

  private transient java.awt.Shape _shape;
  private transient static ProjectObservable _projectObservable;

  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public BoardType()
  {
    _projectObservable.stateChanged(this, null, this, BoardTypeEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      for (Board board : getBoards())
        board.destroy();

      if (_sideBoardType1 != null)
        _sideBoardType1.destroy();
      if (_sideBoardType2 != null)
        _sideBoardType2.destroy();

    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, BoardTypeEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  SideBoardType createSecondSideBoardType()
  {
    SideBoardType sideBoardType = new SideBoardType();
    sideBoardType.setBoardType(this);
    SideBoardTypeSettings sideBoardTypeSettings = new SideBoardTypeSettings();
    sideBoardType.setSideBoardTypeSettings(sideBoardTypeSettings);

    if (sideBoardType1Exists() == false)
      setSideBoardType1(sideBoardType);
    else if (sideBoardType2Exists() == false)
      setSideBoardType2(sideBoardType);
    else
      Assert.expect(false);

    return sideBoardType;
  }

  /**
   * @author Bill Darbie
   */
  void removeSideBoardType(SideBoardType sideBoardType)
  {
    Assert.expect(sideBoardType != null);

    boolean sideBoardType1 = false;
    _projectObservable.setEnabled(false);
    try
    {
      if (sideBoardType == _sideBoardType1)
      {
        sideBoardType1 = true;
        _sideBoardType1 = null;
      }
      else if (sideBoardType == _sideBoardType2)
      {
        sideBoardType1 = false;
        _sideBoardType2 = null;
      }
      else
        Assert.expect(false);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    if (sideBoardType1)
      _projectObservable.stateChanged(this, null, sideBoardType, BoardTypeEventEnum.SIDE_BOARD_TYPE_1);
    else
      _projectObservable.stateChanged(this, null, sideBoardType, BoardTypeEventEnum.SIDE_BOARD_TYPE_2);
  }

  /**
   * @author George A. David
   */
  public void setPanel(Panel panel)
  {
    Assert.expect(panel != null);

    if (panel == _panel)
      return;

    Panel oldValue = _panel;
    _projectObservable.setEnabled(false);
    try
    {
      _panel = panel;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, panel, BoardTypeEventEnum.PANEL);
  }

  /**
   * @author George A. David
   */
  public Panel getPanel()
  {
    Assert.expect(_panel != null);

    return _panel;
  }

  /**
   * @author Keith Lee
   */
  public void setName(String name)
  {
    Assert.expect(name != null);
    Assert.expect(_panel != null);
    Assert.expect(_panel.isBoardTypeNameValid(name));
    Assert.expect(_panel.isBoardTypeNameDuplicate(name) == false);

    if (name.equals(_name))
      return;

    String oldValue = _name;
    _projectObservable.setEnabled(false);
    try
    {
      _panel.setNewBoardTypeName(this, _name, name);
      _name = name.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, name, BoardTypeEventEnum.NAME);
  }

  /**
   * @author Keith Lee
   */
  public String getName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    return getName();
  }

  /**
   * Set the width of the panel at 0 degrees rotation.
   * @author Keith Lee
   */
  public void setWidthInNanoMeters(int widthInNanoMeters)
  {
    Assert.expect(widthInNanoMeters > 0);

    if (widthInNanoMeters == _widthInNanoMeters)
      return;

    int oldValue = _widthInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _widthInNanoMeters = widthInNanoMeters;
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, widthInNanoMeters, BoardTypeEventEnum.WIDTH);
  }

  /**
   * @return the x axis width of the Board BEFORE any rotation has been applied.
   * @author Keith Lee
   */
  public int getWidthInNanoMeters()
  {
    Assert.expect(_widthInNanoMeters > 0);

    return _widthInNanoMeters;
  }


  /**
   * Set the length of the Board at 0 degrees rotation.
   * @author Keith Lee
   */
  public void setLengthInNanoMeters(int lengthInNanoMeters)
  {
    Assert.expect(lengthInNanoMeters > 0);

    if (lengthInNanoMeters == _lengthInNanoMeters)
      return;

    int oldValue = _lengthInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _lengthInNanoMeters = lengthInNanoMeters;
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, lengthInNanoMeters, BoardTypeEventEnum.LENGTH);
  }

  /**
   * @return the y axis length of the Board BEFORE any rotation has been applied.
   * @author Keith Lee
   */
  public int getLengthInNanoMeters()
  {
    Assert.expect(_lengthInNanoMeters > 0);

    return _lengthInNanoMeters;
  }

  /**
   * @author Bill Darbie
   */
  public void addBoard(Board board)
  {
    Assert.expect(board != null);
    Assert.expect(_boardSet != null);

    _projectObservable.setEnabled(false);
    try
    {
      boolean added = _boardSet.add(board);
      Assert.expect(added);
      board.setBoardType(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, board, BoardTypeEventEnum.ADD_OR_REMOVE_BOARD);
  }

  /**
   * @author Bill Darbie
   */
  void removeBoard(Board board)
  {
    Assert.expect(board != null);

    _projectObservable.setEnabled(false);
    try
    {
      boolean removed = _boardSet.remove(board);
      Assert.expect(removed);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, board, null, BoardTypeEventEnum.ADD_OR_REMOVE_BOARD);
  }

  /**
   * @author Bill Darbie
   */
  public List<Board> getBoards()
  {
    Assert.expect(_boardSet != null);

    List<Board> boards = new ArrayList<Board>(_boardSet);
    Collections.sort(boards, new AlphaNumericComparator());
    return boards;
  }

  /**
   * This method should only be called by a Reader
   * use the flip() call in all other cases.
   * @author Bill Darbie
   */
  public void setSideBoardType1(SideBoardType sideBoardType1)
  {
    Assert.expect(sideBoardType1 != null);

    if (sideBoardType1 == _sideBoardType1)
      return;

    SideBoardType oldValue = _sideBoardType1;
    _projectObservable.setEnabled(false);
    try
    {
      _sideBoardType1 = sideBoardType1;
      _sideBoardType1.setBoardType(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, sideBoardType1, BoardTypeEventEnum.SIDE_BOARD_TYPE_1);
  }

  /**
   * @author Bill Darbie
   */
  public boolean sideBoardType1Exists()
  {
    if (_sideBoardType1 == null)
      return false;

    return true;
  }

  /**
   * @author George A. David
   */
  public List<SideBoardType> getSideBoardTypes()
  {
    List<SideBoardType> types = new LinkedList<SideBoardType>();
    if(_sideBoardType1 != null)
      types.add(_sideBoardType1);
    if(_sideBoardType2 != null)
      types.add(_sideBoardType2);

    return types;
  }

  /**
   * @author Bill Darbie
   */
  public SideBoardType getSideBoardType1()
  {
    Assert.expect(_sideBoardType1 != null);

    return _sideBoardType1;
  }

  /**
   * @author Bill Darbie
   */
  public void setSideBoardType2(SideBoardType sideBoardType2)
  {
    Assert.expect(sideBoardType2 != null);

    if (sideBoardType2 == _sideBoardType2)
      return;

    SideBoardType oldValue = _sideBoardType2;
    _projectObservable.setEnabled(false);
    try
    {
      _sideBoardType2 = sideBoardType2;
      _sideBoardType2.setBoardType(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, sideBoardType2, BoardTypeEventEnum.SIDE_BOARD_TYPE_2);
  }

  /**
   * @author Bill Darbie
   */
  public boolean sideBoardType2Exists()
  {
    if (_sideBoardType2 == null)
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  public SideBoardType getSideBoardType2()
  {
    Assert.expect(_sideBoardType2 != null);

    return _sideBoardType2;
  }

  /**
   * @author Bill Darbie
   */
  public void checkComponentTypeReferenceDesignator(String name) throws BusinessException
  {
    if (isComponentTypeReferenceDesignatorValid(name) == false)
      throw new InvalidNameBusinessException("component", name, getComponentTypeReferenceDesignatorIllegalChars());

    if (isComponentTypeReferenceDesignatorDuplicate(name))
      throw new DuplicateNameBusinessException("component", name);
  }

  /**
   * @author Bill Darbie
   */
  public String getComponentTypeReferenceDesignatorIllegalChars()
  {
    return " ";
  }

  /**
   * @author Bill Darbie
   */
  public boolean isComponentTypeReferenceDesignatorValid(String refDes)
  {
    Assert.expect(refDes != null);

    if (refDes.contains(" "))
      return false;
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isComponentTypeReferenceDesignatorDuplicate(String refDes)
  {
    Assert.expect(refDes != null);

    if (_sideBoardType1 != null)
    {
      if (_sideBoardType1.isComponentTypeReferenceDesignatorDuplicate(refDes))
        return true;
    }

    if (_sideBoardType2 != null)
    {
      if (_sideBoardType2.isComponentTypeReferenceDesignatorDuplicate(refDes))
        return true;
    }

    return false;
  }

  /**
   * @return a List of ComponentTypes that are contained on this BoardType, both top and bottom
   * @author Bill Darbie
   * @author Kee Chin Seong
   */
  public List<ComponentType> getComponentTypes()
  {
    Set<ComponentType> componentTypeSet = new TreeSet<ComponentType>(new AlphaNumericComparator());
    if (_sideBoardType1 != null)
    {
      for (ComponentType componentType : _sideBoardType1.getComponentTypes())
      {
        boolean added = componentTypeSet.add(componentType);
        Assert.expect(added);
      }
    }

    if (_sideBoardType2 != null)
    {
      for (ComponentType componentType : _sideBoardType2.getComponentTypes())
      {
        boolean added = componentTypeSet.add(componentType);
        //Assert.expect(added); // we no longer need this, because bottom and top component
        //                         probally have same name.
      }
    }

    return new ArrayList<ComponentType>(componentTypeSet);
  }

  /**
   * @return all component packages for this board type
   * @author Erica Wheatcroft
   */
  public List<CompPackage> getCompPackages()
  {
    Set<CompPackage> compPackageSet = new TreeSet<CompPackage>(new CompPackageComparator(true, CompPackageComparatorEnum.LONG_NAME_ALPHANUMERICALLY));
    if (_sideBoardType1 != null)
      compPackageSet.addAll(_sideBoardType1.getCompPackages());

    if (_sideBoardType2 != null)
      compPackageSet.addAll(_sideBoardType2.getCompPackages());

    Assert.expect(compPackageSet != null);
    return new ArrayList<CompPackage>(compPackageSet);
  }

  /**
   * @author Bill Darbie
   */
  public List<FiducialType> getFiducialTypes()
  {
    Set<FiducialType> fiducialTypeSet = new TreeSet<FiducialType>(new AlphaNumericComparator());
    if (_sideBoardType1 != null)
    {
      for (FiducialType fiducialType : _sideBoardType1.getFiducialTypes())
      {
        boolean added = fiducialTypeSet.add(fiducialType);
        Assert.expect(added);
      }
    }

    if (_sideBoardType2 != null)
    {
      for (FiducialType fiducialType : _sideBoardType2.getFiducialTypes())
      {
        boolean added = fiducialTypeSet.add(fiducialType);
        Assert.expect(added);
      }
    }

    Assert.expect(fiducialTypeSet != null);
    return new ArrayList<FiducialType>(fiducialTypeSet);
  }

  /**
   * @author Bill Darbie
   */
  public String getFiducialTypeNameIllegalChars()
  {
    return " ";
  }

  /**
   * @author Bill Darbie
   */
  public boolean isFiducialTypeNameValid(String name)
  {
    Assert.expect(name != null);
    if (name.contains(" ") || name.equals(""))
      return false;
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isFiducialTypeNameDuplicate(String name)
  {
    Assert.expect(name != null);

    if (((_sideBoardType1 != null) && (_sideBoardType1.hasFiducialType(name))) ||
        ((_sideBoardType2 != null) && (_sideBoardType2.hasFiducialType(name))))
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  void setNewFiducialTypeName(FiducialType fiducialType, SideBoardType sideBoardType, String oldName, String newName)
  {
    Assert.expect(fiducialType != null);
    Assert.expect(sideBoardType != null);
    Assert.expect(newName != null);

    sideBoardType.setNewFiducialTypeName(fiducialType, oldName, newName);
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasComponentType(String refDes)
  {
    if (_sideBoardType1 != null)
    {
      if (_sideBoardType1.hasComponentType(refDes))
        return true;
    }

    if (_sideBoardType2 != null)
    {
      if (_sideBoardType2.hasComponentType(refDes))
        return true;
    }

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public ComponentType getComponentType(String refDes)
  {
    Assert.expect(refDes != null);

    ComponentType componentType = null;
    if (_sideBoardType1 != null)
    {
      if (_sideBoardType1.hasComponentType(refDes))
        componentType = _sideBoardType1.getComponentType(refDes);
    }
    if (componentType == null)
    {
      if (_sideBoardType2 != null)
      {
        if (_sideBoardType2.hasComponentType(refDes))
          componentType = _sideBoardType2.getComponentType(refDes);
      }
    }

    Assert.expect(componentType != null);
    return componentType;
  }

  /**
   * @author Bill Darbie
   */
  public List<LandPattern> getLandPatterns()
  {
    Set<LandPattern> landPatternSet = new TreeSet<LandPattern>(new AlphaNumericComparator());
    if (_sideBoardType1 != null)
      landPatternSet.addAll(_sideBoardType1.getLandPatterns());
    if (_sideBoardType2 != null)
      landPatternSet.addAll(_sideBoardType2.getLandPatterns());

    return new ArrayList<LandPattern>(landPatternSet);
  }

  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getSubtypesUnsorted()
  {
    Set<Subtype> subtypeSet = new HashSet<Subtype>();
    if (_sideBoardType1 != null)
      subtypeSet.addAll(_sideBoardType1.getSubtypesUnsorted());
    if (_sideBoardType2 != null)
      subtypeSet.addAll(_sideBoardType2.getSubtypesUnsorted());

    return subtypeSet;
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getSubtypes()
  {
    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    if (_sideBoardType1 != null)
      subtypeSet.addAll(_sideBoardType1.getSubtypes());
    if (_sideBoardType2 != null)
      subtypeSet.addAll(_sideBoardType2.getSubtypes());

    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getSubtypesUnsorted(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    Set<Subtype> algFamilySet = new HashSet<Subtype>();
    if (_sideBoardType1 != null)
      algFamilySet.addAll(_sideBoardType1.getSubtypesUnsorted(jointTypeEnum));
    if (_sideBoardType2 != null)
      algFamilySet.addAll(_sideBoardType2.getSubtypesUnsorted(jointTypeEnum));

    Assert.expect(algFamilySet != null);
    return algFamilySet;
  }

  /**
   * @author Wei Chin
   */
  public Set<Subtype> getSubtypesUnsorted(LandPattern landPattern, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(landPattern != null);

    Set<Subtype> algFamilySet = new HashSet<Subtype>();
    if (_sideBoardType1 != null)
      algFamilySet.addAll(_sideBoardType1.getSubtypesUnsorted(landPattern, jointTypeEnum));
    if (_sideBoardType2 != null)
      algFamilySet.addAll(_sideBoardType2.getSubtypesUnsorted(landPattern, jointTypeEnum));

    Assert.expect(algFamilySet != null);
    return algFamilySet;
  }  
  
  /**
   * @author Bill Darbie
   */
  public List<Subtype> getSubtypes(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    subtypeSet.addAll(getSubtypesUnsorted(jointTypeEnum));

    Assert.expect(subtypeSet != null);
    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getSubtypesUnsortedIncludingUnused(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    Set<Subtype> subtypeSet = getSubtypesUnsorted(jointTypeEnum);

    for (Subtype subtype : getPanel().getUnusedSubtypesUnsorted())
    {
      if (subtype.getJointTypeEnum().equals(jointTypeEnum))
        subtypeSet.add(subtype);
    }

    return subtypeSet;
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getSubtypesIncludingUnused(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    subtypeSet.addAll(getSubtypesUnsortedIncludingUnused(jointTypeEnum));

    Assert.expect(subtypeSet != null);
    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getInspectedSubtypesUnsorted()
  {
    Set<Subtype> subtypeSet = new HashSet<Subtype>();

    if (_sideBoardType1 != null)
      subtypeSet.addAll(_sideBoardType1.getInspectedSubtypesUnsorted());
    if (_sideBoardType2 != null)
      subtypeSet.addAll(_sideBoardType2.getInspectedSubtypesUnsorted());

    Assert.expect(subtypeSet != null);
    return subtypeSet;
  }

  /**
   * @author Wei Chin, Chong
   */
  public Set<Subtype> getInspectedAndUnImportedSubtypesUnsorted()
  {
    Set<Subtype> subtypeSet = new HashSet<Subtype>();

    if (_sideBoardType1 != null)
      subtypeSet.addAll(_sideBoardType1.getInspectedAndUnImportedSubtypesUnsorted());
    if (_sideBoardType2 != null)
      subtypeSet.addAll(_sideBoardType2.getInspectedAndUnImportedSubtypesUnsorted());

    Assert.expect(subtypeSet != null);
    return subtypeSet;
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getInspectedSubtypes()
  {
    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());

    for (Subtype subtype : getInspectedSubtypesUnsorted())
    {
      boolean added = subtypeSet.add(subtype);
      Assert.expect(added);
    }

    Assert.expect(subtypeSet != null);
    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getTestableSubtypesUnsorted()
  {
    Set<Subtype> subtypeSet = new HashSet<Subtype>();

    if (_sideBoardType1 != null)
      subtypeSet.addAll(_sideBoardType1.getTestableSubtypesUnsorted());
    if (_sideBoardType2 != null)
      subtypeSet.addAll(_sideBoardType2.getTestableSubtypesUnsorted());

    Assert.expect(subtypeSet != null);
    return subtypeSet;
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getTestableSubtypes()
  {
    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());

    subtypeSet.addAll(getTestableSubtypesUnsorted());

    Assert.expect(subtypeSet != null);
    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getInspectedSubtypes(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());

    if (_sideBoardType1 != null)
      subtypeSet.addAll(_sideBoardType1.getInspectedSubtypes(jointTypeEnum));
    if (_sideBoardType2 != null)
      subtypeSet.addAll(_sideBoardType2.getInspectedSubtypes(jointTypeEnum));

    Assert.expect(subtypeSet != null);
    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Wei Chin, Chong
   */
  public List<Subtype> getInspectedAndUnImportedSubtypes(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());

    if (_sideBoardType1 != null)
      subtypeSet.addAll(_sideBoardType1.getInspectedAndUnImportedSubtypes(jointTypeEnum));
    if (_sideBoardType2 != null)
      subtypeSet.addAll(_sideBoardType2.getInspectedAndUnImportedSubtypes(jointTypeEnum));

    Assert.expect(subtypeSet != null);
    return new ArrayList<Subtype>(subtypeSet);
  }


  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getTestableSubtypesUnsorted(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    Set<Subtype> subtypeSet = new HashSet<Subtype>();

    if (_sideBoardType1 != null)
      subtypeSet.addAll(_sideBoardType1.getTestableSubtypesUnsorted(jointTypeEnum));
    if (_sideBoardType2 != null)
      subtypeSet.addAll(_sideBoardType2.getTestableSubtypesUnsorted(jointTypeEnum));

    Assert.expect(subtypeSet != null);
    return subtypeSet;
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getTestableSubtypes(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    subtypeSet.addAll(getTestableSubtypesUnsorted(jointTypeEnum));

    Assert.expect(subtypeSet != null);
    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Bill Darbie
   */
  public List<JointTypeEnum> getJointTypeEnums()
  {
    Set<JointTypeEnum> jointTypeEnumSet = new TreeSet<JointTypeEnum>(new AlphaNumericComparator());
    for (ComponentType componentType : getComponentTypes())
    {
      jointTypeEnumSet.addAll(componentType.getJointTypeEnums());
    }

    return new ArrayList<JointTypeEnum>(jointTypeEnumSet);
  }

  /**
   * @author Bill Darbie
   */
  public List<JointTypeEnum> getInspectedJointTypeEnums()
  {
    Set<JointTypeEnum> jointTypeEnumSet = new TreeSet<JointTypeEnum>(new AlphaNumericComparator());
    for (Subtype subtype : getInspectedSubtypesUnsorted())
    {
      jointTypeEnumSet.add(subtype.getJointTypeEnum());
    }

    return new ArrayList<JointTypeEnum>(jointTypeEnumSet);
  }

  /**
   * @author Bill Darbie
   */
  public List<ComponentType> getInspectedComponentTypes()
  {
    List<ComponentType> inspectedComponentTypes = new ArrayList<ComponentType>();
    for (ComponentType componentType : getComponentTypes())
    {
      if (componentType.isInspected())
        inspectedComponentTypes.add(componentType);
    }

    return inspectedComponentTypes;
  }

  /**
   * @author Bill Darbie
   */
  public List<ComponentType> getTestableComponentTypes()
  {
    List<ComponentType> testableComponentTypes = new ArrayList<ComponentType>();
    for (ComponentType componentType : getComponentTypes())
    {       
      //if (componentType.isTestable())
      if (componentType.getComponentTypeSettings().areAllPadTypesNotTestable() == false)
        testableComponentTypes.add(componentType);
    }

    return testableComponentTypes;
  }

  /**
   * @author Bill Darbie
   */
  public List<JointTypeEnum> getTestableJointTypeEnums()
  {
    Set<JointTypeEnum> jointTypeEnumSet = new TreeSet<JointTypeEnum>(new AlphaNumericComparator());
    for (Subtype subtype : getTestableSubtypesUnsorted())
    {
      jointTypeEnumSet.add(subtype.getJointTypeEnum());
    }

    return new ArrayList<JointTypeEnum>(jointTypeEnumSet);
  }

  /**
   * @author Bill Darbie
   */
  public List<ComponentType> getComponentTypesOutsideBoardType()
  {
    List<ComponentType> componentTypes = new ArrayList<ComponentType>();
    for (ComponentType componentType : getComponentTypes())
    {
      if (componentType.isComponentTypeWithinBoardTypeOutline() == false)
        componentTypes.add(componentType);
    }

    return componentTypes;
  }

  /**
   * @author Laura Cormos
   */
  public List<FiducialType> getFiducialTypesOutsideBoardType()
  {
    List<FiducialType> fiducialTypes = new ArrayList<FiducialType>();
    for (FiducialType fiducialType : getFiducialTypes())
    {
      if (fiducialType.isFiducialTypeWithinBoardTypeOutline() == false)
        fiducialTypes.add(fiducialType);
    }

    return fiducialTypes;
  }

  /**
   * @author Bill Darbie
   */
  void invalidateShape()
  {
    _shape = null;
    for (Board board : getBoards())
      board.invalidateShape();
  }

  /**
   * @return the Board shape, not taking into account any panel rotation, as seen by x-rays.
   * @author Andy Mechtenberg
   */
  public java.awt.Shape getShapeInNanoMeters()
  {
    if (_shape == null)
    {
      // create the shape
      _shape = new Rectangle2D.Double(0, 0, getWidthInNanoMeters(), getLengthInNanoMeters());
    }

     Assert.expect(_shape != null);
    return _shape;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public List<CompPackageOnPackage> getCompPackageOnPackages()
  {
    Set<CompPackageOnPackage> compPackageOnPackageSet = new TreeSet<CompPackageOnPackage>(new CompPackageOnPackageComparator(true, CompPackageOnPackageComparatorEnum.NAME_ALPHANUMERICALLY));
    if (_sideBoardType1 != null)
      compPackageOnPackageSet.addAll(_sideBoardType1.getCompPackageOnPackages());

    if (_sideBoardType2 != null)
      compPackageOnPackageSet.addAll(_sideBoardType2.getCompPackageOnPackages());

    Assert.expect(compPackageOnPackageSet != null);
    return new ArrayList<CompPackageOnPackage>(compPackageOnPackageSet);
  }
}
