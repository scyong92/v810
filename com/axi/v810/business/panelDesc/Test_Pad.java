package com.axi.v810.business.panelDesc;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class Test_Pad extends UnitTest
{

  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Pad());
  }

  /**
   * @author Keith Lee
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Pad pad = new Pad();

    // test setComponent and getComponent
    try
    {
      pad.setComponent(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      pad.getComponent();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    Component component = new Component();
    pad.setComponent(component);
    Expect.expect(pad.getComponent() == component);

    pad = new Pad();
    // test all methods that use _landPatternPad
    try
    {
      pad.getDegreesRotation();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      pad.getName();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      pad.getWidthInNanoMeters();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      pad.getLengthInNanoMeters();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      pad.getShapeEnum();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      pad.getCoordinateInNanoMeters();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    // now test methods that use _landPatternPad after it has been set up
    // properly
    SurfaceMountLandPatternPad landPatternPad = new SurfaceMountLandPatternPad();
    landPatternPad.setLandPattern(new LandPattern());
    PadType padType = new PadType();
    pad.setPadType(padType);
    padType.setLandPatternPad(landPatternPad);

    // test getDegreesRotation
    landPatternPad.setDegreesRotation(90);
    Expect.expect(pad.getDegreesRotation() == 90);

    // test getWidthInNanoMeters
    int widthInNanoMeters = 10;
    landPatternPad.setWidthInNanoMeters(widthInNanoMeters);
    Expect.expect(pad.getWidthInNanoMeters() == widthInNanoMeters);

    // test getLengthInNanoMeters
    int lengthInNanoMeters = 11;
    landPatternPad.setLengthInNanoMeters(lengthInNanoMeters);
    Expect.expect(pad.getLengthInNanoMeters() == lengthInNanoMeters);

    // test getShapeEnum
    ShapeEnum shapeEnum = ShapeEnum.CIRCLE;
    landPatternPad.setShapeEnum(shapeEnum);
    Expect.expect(pad.getShapeEnum().equals(shapeEnum));

    // test getCoordinateInNanometers
    ComponentCoordinate panelCoordinate = new ComponentCoordinate(3, 4);
    landPatternPad.setCoordinateInNanoMeters(panelCoordinate);
    Expect.expect(pad.getCoordinateInNanoMeters().equals(panelCoordinate));

    // test copy constructor
    try
    {
      new Pad(null);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    pad = new Pad();
    padType = new PadType();
    pad.setPadType(padType);
    component = new Component();
    pad.setComponent(component);

    Pad copy = new Pad(pad);

    // can test this with == because the copy constructor does a shallow copy of landPatternPad
    Expect.expect(pad.getPadType() == copy.getPadType());
    Expect.expect(pad.getComponent() == copy.getComponent());


    // testing of getShapeInNanometers is done in Test_Panel.java
  }
}
