package com.axi.v810.business.panelDesc;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * There is one Pad instance for each physical Pad on the panel.
 * @author Bill Darbie
 */
public class Pad implements Serializable
{
  // created by BoardNdfReader, PanelReader

  // created by BoardNdfReader
  private PadSettings _padSettings; // Set whenever pads are created
  private PadType _padType; // BoardNdfReader, PanelReader
  private String _nodeName; /** @todo wpd - do we need this */
  private NodeTypeEnum _nodeTypeEnum; /** @todo wpd do we need this */


  // _component is transient only because we cannot save panels with more than about 20 boards
  // if it is not transient
  private transient Component _component; // BoardNdfReader, PanelReader
  private transient java.awt.Shape _shape;
  private transient PanelRectangle _panelRect;
  private transient DoubleRef _degrees;
  private transient DoubleRef _pinOrientationDegrees;
  private static transient ProjectObservable _projectObservable;
  private static transient Alignment _alignment;

  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
    _alignment = Alignment.getInstance();
  }

  /**
   * Make a partial copy the Pad object passed in.
   *
   * Component, Pin, Pad, and Joint all have references to each other,
   * because of this it is impossible to have a copy() method in each
   * class that can do the proper thing.  Instead this limited copy
   * copies what it can and sets the remaining variables to null.
   *
   * @author Bill Darbie
   */
  public Pad(Pad rhs)
  {
    Assert.expect(rhs != null);

    _projectObservable.setEnabled(false);
    try
    {
      // shallow copy _component
      _component = rhs._component;
      // Create new PadSettings (no state to copy)
      _padSettings = new PadSettings();
      _padSettings.setPad(this);
      // shallow copy PadType
      _padType = rhs._padType;
      _padType.addPad(this);
      _nodeName = rhs._nodeName;
      _nodeTypeEnum = rhs._nodeTypeEnum;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, this, PadEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public Pad()
  {
    _projectObservable.stateChanged(this, null, this, PadEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      getComponent().removePad(this);

      _padSettings.destroy();

      getPadType().removePad(this);
      _padSettings = null;

      if(getComponent().getSideBoard().getBoard().getPanel().getPanelSettings().isPanelBasedAlignment())
        getComponent().getSideBoard().getBoard().getPanel().getPanelSettings().removePadFromAlignmentGroupIfItIsUsed(this);
      else
        getComponent().getSideBoard().getBoard().getBoardSettings().removePadFromAlignmentGroupIfItIsUsed(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, PadEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void setPadType(PadType padType)
  {
    Assert.expect(padType != null);

    if (padType == _padType)
      return;

    PadType oldValue = _padType;
    _projectObservable.setEnabled(false);
    try
    {
      _padType = padType;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, padType, PadEventEnum.PAD_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public PadType getPadType()
  {
    Assert.expect(_padType != null);

    return _padType;
  }

  /**
   * @author Matt Wharton
   */
  public PadSettings getPadSettings()
  {
    Assert.expect(_padSettings != null);

    return _padSettings;
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasPadSettings()
  {
    return (_padSettings != null);
  }

  /**
   * @author Matt Wharton
   */
  public void setPadSettings(PadSettings padSettings)
  {
    Assert.expect(padSettings != null);

    PadSettings oldValue = _padSettings;
    _projectObservable.setEnabled(false);
    try
    {
      _padSettings = padSettings;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, padSettings, PadEventEnum.PAD_SETTINGS);
  }

  /**
   * @author Bill Darbie
   */
  public String getName()
  {
    Assert.expect(_padType != null);

    return _padType.getName();
  }

  /**
   * @return the name of the pad including the component name, for example: "U1-1"
   * @author Bill Darbie
   */
  public String getComponentAndPadName()
  {
    String refDes = getComponent().getReferenceDesignator();
    String fullName = refDes + "-" + getName();

    return fullName;
  }

  /**
   * @author Bill Darbie
   */
  public String getBoardAndComponentAndPadName()
  {
    return getComponent().getBoard().getName() + " " + getComponentAndPadName();
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author Keith Lee
   */
  public void setComponent(Component component)
  {
    Assert.expect(component != null);

    if (component == _component)
      return;

    Component oldValue = _component;
    _projectObservable.setEnabled(false);
    try
    {
      _component = component;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, component, PadEventEnum.COMPONENT);
  }

  /**
   * @author Keith Lee
   */
  public Component getComponent()
  {
    assignComponentIfNecessary();

    return _component;
  }

  /**
   * @author Keith Lee
   */
  public LandPatternPad getLandPatternPad()
  {
    Assert.expect(_padType != null);

    return _padType.getLandPatternPad();
  }

  /**
   * @author Bill Darbie
   */
  public int getPitchInNanoMeters()
  {
    Assert.expect(_padType != null);

    return _padType.getPitchInNanoMeters();
  }

  /**
   * InterPadDistance is the distance from one edge of a Pad to the edge of its nearest Pad.
   * In reality, the algorithm used does not always match this in all cases, but in the general
   * case id does.
   *
   * @author Bill Darbie
   */
  public int getInterPadDistanceInNanoMeters()
  {
    Assert.expect(_padType != null);

    return _padType.getInterPadDistanceInNanoMeters();
  }

  /**
   * @author George A. David
   */
  public int getAlgorithmLimitedInterPadDistanceInNanometers()
  {
    Assert.expect(_padType != null);

    return _padType.getAlgorithmLimitedInterPadDistanceInNanometers();
  }

  /**
   * @author Bill Darbie
   */
  public PadTypeSettings getPadTypeSettings()
  {
    Assert.expect(_padType != null);

    return _padType.getPadTypeSettings();
  }

  /**
   * @author Bill Darbie
   */
  public PackagePin getPackagePin()
  {
    Assert.expect(_padType != null);

    return _padType.getPackagePin();
  }

  /**
   * @author George A. David
   */
  public JointTypeEnum getJointTypeEnum()
  {
    Assert.expect(_padType != null);

    return _padType.getJointTypeEnum();
  }

  /**
   * @author Keith Lee
   */
  public ShapeEnum getShapeEnum()
  {
    Assert.expect(_padType != null);

    return _padType.getShapeEnum();
  }

  /**
   * @return the coordinate of this Pads center relative to the reference
   * point of the Component it gets associated with.
   * The reference point is not necessarily the center of the component, but is the
   * point from which the pad/pin coordinates are referenced.
   * @author Keith Lee
   */
  public ComponentCoordinate getCoordinateInNanoMeters()
  {
    Assert.expect(_padType != null);

    return _padType.getCoordinateInNanoMeters();
  }

  /**
   * @return the lower left coordinate of this Pad relative to its Panels bottom left corner.
   * @author Bill Darbie
   */
  public PanelCoordinate getLowerLeftCoordinateRelativeToPanelInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    Rectangle2D bounds = shape.getBounds2D();

    double x = Math.round(bounds.getMinX());
    double y = Math.round(bounds.getMinY());
    if (x > Integer.MAX_VALUE)
      x = Integer.MAX_VALUE;
    if (x < Integer.MIN_VALUE)
      x = Integer.MIN_VALUE;
    if (y > Integer.MAX_VALUE)
      y = Integer.MAX_VALUE;
    if (y < Integer.MIN_VALUE)
      y = Integer.MIN_VALUE;

    return new PanelCoordinate((int)x, (int)y);
  }

  /**
   * @return the lower left coordinate of this Pad relative to its Panels bottom left corner.
   * @author Bill Darbie
   */
  public PanelCoordinate getCenterCoordinateRelativeToPanelInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    Rectangle2D bounds = shape.getBounds2D();

    return new PanelCoordinate((int)Math.round(bounds.getCenterX()), (int)Math.round(bounds.getCenterY()));
  }


  /**
   * @author Keith Lee
   */
  public int getWidthInNanoMeters()
  {
    Assert.expect(_padType != null);

    return _padType.getWidthInNanoMeters();
  }

  /**
   * @return the x axis width of the Pad AFTER the Pad, Component, Board and Panel have been rotated.
   * @author Bill Darbie
   */
  public int getWidthAfterAllRotationsInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    java.awt.Rectangle bounds = shape.getBounds();

    return bounds.width;
  }

  /**
   * @author Keith Lee
   */
  public int getLengthInNanoMeters()
  {
    Assert.expect(_padType != null);

    return _padType.getLengthInNanoMeters();
  }

  /**
   * @return the y axis length of the Pad AFTER the Pad, Component, Board and Panel have been rotated.
   * @author Bill Darbie
   */
  public int getLengthAfterAllRotationsInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    java.awt.Rectangle bounds = shape.getBounds();

    return bounds.height;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isThroughHolePad()
  {
    Assert.expect(_padType != null);

    return _padType.isThroughHolePad();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isSurfaceMountPad()
  {
    Assert.expect(_padType != null);

    return _padType.isSurfaceMountPad();
  }

  /**
   * @return true if this pin is pin one and false otherwise
   * @author Bill Darbie
   */
  public boolean isPadOne()
  {
    Assert.expect(_padType != null);

    return _padType.isPadOne();
  }

  /**
   * @author Chnee Khang Wah, XCR1374
   */
  public boolean isTestable()
  {
    boolean isTestable = false;

    Assert.expect(_padSettings != null);
    if (_padSettings.isTestable())
      isTestable = true;

    return isTestable;
  }
  
  /**
   * A pad is considered inspected if it is loaded, is testable, and is set to be inspected.
   * @author Bill Darbie
   */
  public boolean isInspected()
  {
    boolean isInspected = false;

    Assert.expect(_padType != null);
    //if (_padType.isInspected())
    if ((_padType.getComponentType().isLoaded() && _padType.getPadTypeSettings().isInspectedFlagSet() == true && (isTestable() == true)))
      isInspected = true;

    return isInspected;
  }

  /**
   * @return true if this Pad is facing the x-ray source.
   * @author Bill Darbie
   */
  public boolean isTopSide()
  {
    assignComponentIfNecessary();

    return _component.isTopSide();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isSideBoard1()
  {
    return _component.getSideBoard().isSideBoard1();
  }

  /**
   * @return true if this Pad is NOT facing the x-ray source.
   * @author Bill Darbie
   */
  public boolean isBottomSide()
  {
    return !isTopSide();
  }

  /**
   * @author Bill Darbie
   */
  public Subtype getSubtype()
  {
    Assert.expect(_padType != null);

    return _padType.getSubtype();
  }

  /**
   * @author Bill Darbie
   */
  public void setNodeName(String nodeName)
  {
    Assert.expect(nodeName != null);

    String oldValue = _nodeName;
    _projectObservable.setEnabled(false);
    try
    {
      _nodeName = nodeName.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, nodeName, PadEventEnum.NODE_NAME);
  }

  /**
   * @author Bill Darbie
   */
  public String getNodeName()
  {
    Assert.expect(_nodeName != null);
    return _nodeName;
  }

  /**
   * Get degrees rotation of this Pad.  The rotation of a Pad describes
   * the rotation around the Pad's Center
   * @author Bill Darbie
   */
  public double getDegreesRotation()
  {
    Assert.expect(_padType != null);

    return _padType.getDegreesRotation();
  }

  /**
   * @return the degrees rotation after all rotations from this Pad, its Component, Board, and Panel have been
   * applied.
   *
   * @author Bill Darbie
   */
  public double getDegreesRotationAfterAllRotations()
  {
    if (_degrees == null)
    {
      double degrees = getDegreesRotation();
      java.awt.geom.AffineTransform transform = java.awt.geom.AffineTransform.getRotateInstance(Math.toRadians(degrees));

      // apply the transform of the component.
      getComponent().preConcatenateShapeTransform(transform, false, true, false);

      Point2D unitVector = new Point2D.Double(1, 0);
      Point2D transformedPoint = transform.transform(unitVector, null);

      double x1 = unitVector.getX();
      double y1 = unitVector.getY();
      double x2 = transformedPoint.getX();
      double y2 = transformedPoint.getY();

      // get the angle between x1,y1 and x2,y2
      degrees = Math.toDegrees(Math.acos(x1 * x2 + y1 * y2));

      // if y2 is positive then the degrees should be from 0 to 180, so do nothing
      // if y2 is negative then the degrees should be from 180 to 360
      // since the acos returns degrees from 0 to 180 only, we must compenstate here
      if (y2 < 0)
        degrees = -degrees;

      degrees = MathUtil.getDegreesWithin0To359(degrees);
      _degrees = new DoubleRef(degrees);
    }

    Assert.expect(_degrees !=  null);
    return _degrees.getValue();
  }

  /**
   * @author Bill Darbie
   */
  public PinOrientationEnum getPinOrientationEnum()
  {
    Assert.expect(_padType != null);

    return _padType.getPinOrientationEnum();
  }

  /**
   * Pin Orientation is the direction from the location the pin reaches the component to the pad direction.
   *
   * @author Bill Darbie
   */
  public double getPinOrientationInDegreesAfterAllRotations()
  {
    if (_pinOrientationDegrees == null)
    {
      double degrees = getPinOrientationEnum().getDegrees() + getDegreesRotation();
      java.awt.geom.AffineTransform transform = java.awt.geom.AffineTransform.getRotateInstance(Math.toRadians(degrees));

      // apply the transform of the component.
      getComponent().preConcatenateShapeTransform(transform, false, true, false);

      Point2D unitVector = new Point2D.Double(1, 0);
      Point2D transformedPoint = transform.transform(unitVector, null);

      double x1 = unitVector.getX();
      double y1 = unitVector.getY();
      double x2 = transformedPoint.getX();
      double y2 = transformedPoint.getY();

      // get the angle between x1,y1 and x2,y2
      degrees = Math.toDegrees(Math.acos(x1 * x2 + y1 * y2));

      // if y2 is positive then the degrees should be from 0 to 180, so do nothing
      // if y2 is negative then the degrees should be from 180 to 360
      // since the acos returns degrees from 0 to 180 only, we must compenstate here
      if (y2 < 0)
        degrees = -degrees;

      degrees = MathUtil.getDegreesWithin0To359(degrees);
      _pinOrientationDegrees = new DoubleRef(degrees);
    }

    Assert.expect(_pinOrientationDegrees !=  null);
    return _pinOrientationDegrees.getValue();
  }

  /**
   * @author George A. David
   */
  public PinOrientationAfterRotationEnum getPinOrientationAfterRotationEnum()
  {
    // The PackagePin will have _pinOrientationEnum set ONLY if its value has been
    // overridden from the orientation calculated from the LandPatternPad.  If it
    // has not been set, then find the setting from the LandPatternPad class and
    // return that
//    PinOrientationEnum pinOrientationEnum = getPinOrientationEnum();
//
//    return PinOrientationAfterRotationEnum.getPinOrientationAfterRotationEnum(pinOrientationEnum, getDegreesRotationAfterAllRotations());

    if (getPinOrientationEnum().equals(PinOrientationEnum.NOT_APPLICABLE))
      return PinOrientationAfterRotationEnum.NOT_APPLICABLE;

    java.awt.geom.AffineTransform orientationArrowTransform = getOrientationArrowTransform();

    // we now have the correct transform, but we need to extract the rotation component from it.
    // to do that, since the transform contains both translation and rotation, we need to remove the translation
    //
    // Method from John Heumann:
    //  apply the transform to (0,0)  (x0,y0)
    //  apply the transform to (1,0)  (x1,y1)
    //  subtract them to create x2,y2:  (x1-x0, y1-y0)
    //  Now, x2,y2 should just be the rotation aspect from (x1,y1)
    //  so, apply the normal formula to get the theta:
    //     V1 . V2 = |v1||v2|cos(theta)
    //                                x1*x2 + y1*y2
    //            =  acos ---------------------------------------
    //                    (sqrt(x1^2 * y1^2) * sqrt(x2^2 * y2^2))
    //
    // Then, if y2 is negative, negate the resulting degree

    Point2D zeroVector = new Point2D.Double(0, 0);
    Point2D unitVector = new Point2D.Double(1, 0);

    Point2D zeroTransformed = orientationArrowTransform.transform(zeroVector, null);
    Point2D transformedPoint = orientationArrowTransform.transform(unitVector, null);

    double x0 = zeroTransformed.getX();
    double y0 = zeroTransformed.getY();
    double x1 = transformedPoint.getX();
    double y1 = transformedPoint.getY();

    double x2 = x1 - x0;
    double y2 = y1 - y0;

    double numerator = 1 * x2 + 0 * y2;
    double denom1 = Math.sqrt(1 * 1 + 0 * 0);
    double denom2 = Math.sqrt(x2 * x2 + y2 * y2);

    double denom = denom1 * denom2;
    double result = numerator / denom;

    double rads = Math.acos(result);
    double degrees = Math.toDegrees(rads);

    // if y2 is positive then the degrees should be from 0 to 180, so do nothing
    // if y2 is negative then the degrees should be from 180 to 360
    // since the acos returns degrees from 0 to 180 only, we must compenstate here
    if (y2 < 0)
      degrees = -degrees;

    return PinOrientationAfterRotationEnum.getPinOrientationAfterRotationEnumFromDegrees(degrees);
  }

  /**
   * @author Andy Mechtenberg
   */
  public java.awt.geom.AffineTransform getOrientationArrowTransform()
  {
    // ok, now rotate it by its pin orientation enum and by its land pattern rotation case.
    java.awt.geom.Rectangle2D shapeRect = getLandPatternPad().getShapeInNanoMeters().getBounds2D();
    double centerX = shapeRect.getCenterX();
    double centerY = shapeRect.getCenterY();

    java.awt.geom.AffineTransform transform = java.awt.geom.AffineTransform.getRotateInstance(Math.toRadians(getPackagePin().getPinOrientationEnum().getDegrees() + getLandPatternPad().getDegreesRotation()), centerX, centerY);

    // apply the transform of the component.
    getComponent().preConcatenateShapeTransform(transform);

    return transform;
  }

  /**
   * @author Bill Darbie
   */
  private void assignComponentIfNecessary()
  {
    // we could not simply have an _component variable that gets serialized because the serialization would
    // fail due to stack overflow.  So this method will assign _component on the fly as needed.
    if (_component == null)
    {
      for (Component component : getPadType().getComponentType().getComponents())
      {
        for (Pad pad : component.getPads())
        {
          pad.setComponent(component);
        }
      }
    }

    Assert.expect(_component != null);
  }

  /**
   * invalid Shape should invalid all _panelRect, _degrees, _pinOrientationDegrees (Wei Chin)
   * @author Bill Darbie
   */
  void invalidateShape()
  {
    _shape = null;
    invalidateRectDegreeOrOrientation();
  }
  
  /*
   * @author Kee Chin Seong
   * Instead of invalidate SHAPE, we invalidate ORITENTATION since
   * Orientation is needed to invalidate instead of SHAPE.
   */
  void invalidateRectDegreeOrOrientation()
  {
    _panelRect = null;
    _degrees = null;
    _pinOrientationDegrees = null;
  }

  /**
   * @return the Shape not related to any component, board or panel setting.
   * @author Bill Darbie
   */
  public java.awt.Shape getShapeInNanoMeters()
  {
    Assert.expect(_padType != null);

    return _padType.getShapeInNanoMeters();
  }

  /**
   * Return the shape after relative to the Panels lower left corner as seen by x-rays.
   * @return Pad shape after component, board and panel transformations (rotations, etc) have been
   * applied to it.  The returned shape is the final, viewable shape.
   * @author Andy Mechtenberg
   */
  public java.awt.Shape getShapeRelativeToPanelInNanoMeters()
  {
    Assert.expect(_padType != null);

    if (_shape == null)
    {
      // get the Shape
      _shape = _padType.getShapeInNanoMeters();

      // get the correct transform
      AffineTransform trans = new AffineTransform();
      getComponent().preConcatenateShapeTransform(trans);
      // trans is premultiplied with the 'shape transform'

      // apply the transform
      _shape = trans.createTransformedShape(_shape);
    }

    Assert.expect(_shape != null);
    return _shape;
  }

  /**
   * Returns a shape relative to the Panel's lower left, but taking
   * into account the alignment matrix.
   * @author George A. David
   * @author Wei Chin, Chong
   */
  public java.awt.Shape getAlignedShapeRelativeToPanelInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    Panel panel = getComponent().getBoard().getPanel();

    if(panel.getPanelSettings().isPanelBasedAlignment())
    {
      if (panel.getPanelSettings().getRightSectionOfLongPanel().containsShape(shape))
        shape = _alignment.getCorrectedPositionInPanelCoordinates(shape, panel.getProject().getTestProgram().getTestSubProgram(PanelLocationInSystemEnum.RIGHT));
      else
        shape = _alignment.getCorrectedPositionInPanelCoordinates(shape, panel.getProject().getTestProgram().getTestSubProgram(PanelLocationInSystemEnum.LEFT));
    }
    else
    {
      if(getComponent().getBoard().getBoardSettings().isLongBoard())
      {
        if (getComponent().getBoard().getRightSectionOfLongBoard().containsShape(shape))
          shape = _alignment.getCorrectedPositionInPanelCoordinates(shape, panel.getProject().getTestProgram().getTestSubProgram(getComponent().getBoard(), PanelLocationInSystemEnum.RIGHT));
        else
          shape = _alignment.getCorrectedPositionInPanelCoordinates(shape, panel.getProject().getTestProgram().getTestSubProgram(getComponent().getBoard(), PanelLocationInSystemEnum.LEFT));
      }
      else
        shape = _alignment.getCorrectedPositionInPanelCoordinates(shape, panel.getProject().getTestProgram().getTestSubProgram(getComponent().getBoard()));
    }

    return shape;
  }

  /**
   * Return a PanelRectangle relative to the Panel's lower left corner.
   * @return com.axi.v810.util.PanelRectangle after component, board and panel transforms (rotations, etc)
   * have been applied to it.
   *
   * @author Peter Esbensen
   */
  public PanelRectangle getPanelRectangleWithLowerLeftOriginInNanoMeters()
  {
    if (_panelRect == null)
    {
      java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
      java.awt.geom.Rectangle2D topLeftBasedRect2D = shape.getBounds2D();

      // topLeftBasedRect2D is not what we want because this is based on the upper-left corner, but we
      // want a rectangle based on the lower-left corner
      // So return a PanelRectangle after modifying Y coordinate to match this convention:
      _panelRect = new PanelRectangle((int)Math.round(topLeftBasedRect2D.getMinX()),
                                      (int)Math.round(topLeftBasedRect2D.getMinY()),
                                      (int)Math.round(topLeftBasedRect2D.getWidth()),
                                      (int)Math.round(topLeftBasedRect2D.getHeight()));
    }

    Assert.expect(_panelRect != null);
    return _panelRect;
  }

  /**
   * Get pad shape relative to panel in pixels
   * @author sham
   */
  public RegionOfInterest getPadShapeRelativeToPanelInPixels()
  {
    MagnificationEnum currentMagnificationEnum = null;
    
    if( getSubtype().getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.LOW) )
      currentMagnificationEnum = MagnificationEnum.NOMINAL;
    else
      currentMagnificationEnum = MagnificationEnum.H_NOMINAL;
    
    final double NANOMETERS_PER_PIXEL = currentMagnificationEnum.getNanoMetersPerPixel();    
    final double PIXELS_PER_NANOMETER = 1.0 / NANOMETERS_PER_PIXEL;
    
    AffineTransform panelToImageCoordinateTransform = AffineTransform.getScaleInstance(PIXELS_PER_NANOMETER, -PIXELS_PER_NANOMETER);
    panelToImageCoordinateTransform.translate(0,getPanelRectangleWithLowerLeftOriginInNanoMeters().getHeight());
    java.awt.Shape transformedShape = panelToImageCoordinateTransform.createTransformedShape(
        getShapeRelativeToPanelInNanoMeters());
    ImageRectangle transformedRect = new ImageRectangle(transformedShape);
    int width = transformedRect.getWidth();
    int length = transformedRect.getHeight();
    if(width <= 0)
      width = 1;
    if(length <= 0)
      length = 1;
    transformedRect.setRect(transformedRect.getMinX(),
                            transformedRect.getMinY(),
                            width,
                            length);

    double  padOrientationInDegrees = getPinOrientationInDegreesAfterAllRotations();

    // the correction should move the orientation to an integer degree .. specifically a multiple of 90.
    Assert.expect( MathUtil.fuzzyEquals(padOrientationInDegrees, Math.round(padOrientationInDegrees), 0.0001) );

    // the orientation is with respect to the package, we need to add 180 to make the orientation match what
    // the algorithms expect.
    int padOrientationInDegreesInt = MathUtil.getDegreesWithin0To359((int)(Math.round(padOrientationInDegrees)) + 180);

    Assert.expect(padOrientationInDegreesInt % 90 == 0);

    RegionShapeEnum regionShapeEnum = null;
    if (getShapeEnum().equals(ShapeEnum.CIRCLE))
      regionShapeEnum = RegionShapeEnum.OBROUND;
    else if (getShapeEnum().equals(ShapeEnum.RECTANGLE))
      regionShapeEnum = RegionShapeEnum.RECTANGULAR;
    else
      Assert.expect(false);
    return new RegionOfInterest(transformedRect, padOrientationInDegreesInt, regionShapeEnum);
  }
}
