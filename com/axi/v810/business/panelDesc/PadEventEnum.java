package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class PadEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static final PadEventEnum CREATE_OR_DESTROY = new PadEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static final PadEventEnum PAD_TYPE = new PadEventEnum(++_index);
  public static final PadEventEnum PAD_SETTINGS = new PadEventEnum(++_index);
  public static final PadEventEnum NAME = new PadEventEnum(++_index);
  public static final PadEventEnum NODE_NAME = new PadEventEnum(++_index);
  public static final PadEventEnum COMPONENT = new PadEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private PadEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private PadEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
