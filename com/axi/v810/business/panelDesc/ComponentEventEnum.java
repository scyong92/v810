package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class ComponentEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static ComponentEventEnum CREATE_OR_DESTROY = new ComponentEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static ComponentEventEnum COMPONENT_TYPE = new ComponentEventEnum(++_index);
  public static ComponentEventEnum SIDE_BOARD = new ComponentEventEnum(++_index);
  public static ComponentEventEnum ADD_OR_REMOVE_PAD = new ComponentEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static ComponentEventEnum CHANGE_SIDE = new ComponentEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private ComponentEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private ComponentEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
