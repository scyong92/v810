package com.axi.v810.business.panelDesc;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * Each BoardType contains one or two SideBoardType instances depending on if the Board is
 * single or double sided.
 *
 * @author Bill Darbie
 */
public class SideBoardType implements Serializable
{
  // created by PanelNdfReader, BoardTypeReader

  private BoardType _boardType = null; // PanelNdfReader, BoardTypeReader
  private Map<String, ComponentType> _refDesToComponentTypeMap = new TreeMap<String, ComponentType>(new AlphaNumericComparator()); // BoardNdfReader, BoardTypeReader
  private SideBoardTypeSettings _sideBoardTypeSettings; // PanelNdfReader
  private Set<SideBoard> _sideBoardSet = new HashSet<SideBoard>(); // PanelNdfReader, PanelReader
  private Map<String, FiducialType> _fiducialNameToFiducialTypeMap = new TreeMap<String, FiducialType>(new AlphaNumericComparator()); // BoardTypeReader

  private static transient ProjectObservable _projectObservable;

  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public SideBoardType()
  {
    _projectObservable.stateChanged(this, null, this, SideBoardTypeEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      getBoardType().removeSideBoardType(this);

      for (SideBoard sideBoard : getSideBoards())
        sideBoard.destroy();

      _sideBoardTypeSettings.destroy();
      _sideBoardTypeSettings = null;

      for (ComponentType componentType : getComponentTypes())
        componentType.destroy();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, SideBoardTypeEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void setBoardType(BoardType boardType)
  {
    Assert.expect(boardType != null);

    if (boardType == _boardType)
      return;

    BoardType oldValue = _boardType;
    _projectObservable.setEnabled(false);
    try
    {
      _boardType = boardType;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, boardType, SideBoardTypeEventEnum.BOARD_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public BoardType getBoardType()
  {
    Assert.expect(_boardType != null);

    return _boardType;
  }

  /**
   * Create a new ComponentType and all associated Components for every SideBoard instance
   * using this SideBoardType.
   * @author Bill Darbie
   */
  public ComponentType createComponentType(String referenceDesignator,
                                           LandPattern landPattern,
                                           BoardCoordinate coordinateInNanoMeters,
                                           double degreesRotationRelativeToBoard)
  {
    Assert.expect(referenceDesignator != null);
    Assert.expect(landPattern != null);
    Assert.expect(coordinateInNanoMeters != null);

    ComponentType componentType = null;
    _projectObservable.setEnabled(false);
    try
    {
      // create a ComponentType and ComponentTypeSettings
      componentType = new ComponentType();
      Assert.expect(_boardType != null);
      componentType.setPanel(_boardType.getPanel());
      ComponentTypeSettings componentTypeSettings = new ComponentTypeSettings();
      componentTypeSettings.setComponentType(componentType);
      componentTypeSettings.setLoaded(true);
      componentTypeSettings.setInspected(true);
      componentType.setComponentTypeSettings(componentTypeSettings);

      componentType.setLandPattern(landPattern);
      componentType.setPartLocationInFactory("");
      componentType.setPartNumber("");
      componentType.setSideBoardType(this);
      componentType.setReferenceDesignator(referenceDesignator);

      landPattern.addComponentType(componentType);

      // create PadTypes and PadTypeSettings
      for (LandPatternPad landPatternPad : landPattern.getLandPatternPads())
      {
        PadType padType = new PadType();
        PadTypeSettings padTypeSettings = new PadTypeSettings();
//        padTypeSettings.setTestable(true);
        padTypeSettings.setInspected(true);
        padType.setComponentType(componentType);
        padType.setLandPatternPad(landPatternPad);
        padType.setPadTypeSettings(padTypeSettings);
        componentType.addPadType(padType);
      }

      componentType.setDegreesRotationRelativeToBoard(degreesRotationRelativeToBoard);
      componentType.setCoordinateInNanoMeters(coordinateInNanoMeters);

      // create Components
      for (SideBoard sideBoard : getSideBoards())
      {
        Component component = new Component();
        component.setComponentType(componentType);
        component.createPads();
        sideBoard.addComponent(component);
      }
      // this causes a lot of work to be done, so we'll do right when the component is created, rather than wait.
      componentType.assignOrModifyCompPackageAndSubtypeAndJointTypeEnums();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(componentType, null, componentType, ComponentTypeEventEnum.CREATE_OR_DESTROY);

    Assert.expect(componentType != null);
    return componentType;
  }

  /**
   * @author Keith Lee
   */
  public void addComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    Assert.expect(_refDesToComponentTypeMap != null);

    _projectObservable.setEnabled(false);
    try
    {
      String refDes = componentType.getReferenceDesignator();
      ComponentType prev = _refDesToComponentTypeMap.put(refDes, componentType);
      Assert.expect(prev == null);
      componentType.setSideBoardType(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, componentType, SideBoardTypeEventEnum.ADD_OR_REMOVE_COMPONENT_TYPE);
  }

  /**
   * @return a list of ComponentType objects in order by name
   * @author Keith Lee
   */
  public List<ComponentType> getComponentTypes()
  {
    Assert.expect(_refDesToComponentTypeMap != null);

    return new ArrayList<ComponentType>(_refDesToComponentTypeMap.values());
  }

  /**
   * @author George A. David
   */
  public boolean hasComponentType(String referenceDesignator)
  {
    Assert.expect(referenceDesignator != null);
    Assert.expect(_refDesToComponentTypeMap != null);

    return _refDesToComponentTypeMap.containsKey(referenceDesignator);
  }

  /**
   * @author Keith Lee
   */
  public ComponentType getComponentType(String referenceDesignator)
  {
    Assert.expect(referenceDesignator != null);
    Assert.expect(_refDesToComponentTypeMap != null);

    ComponentType componentType = _refDesToComponentTypeMap.get(referenceDesignator);
    Assert.expect(componentType != null);
    return componentType;
  }

  /**
   * This method will check to see if this sideboard contains a component.
   * @author Bill Darbie
   */
  public boolean hasComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    Assert.expect(_refDesToComponentTypeMap != null);

    return _refDesToComponentTypeMap.containsValue(componentType);
  }

  /**
   * Remove the component from this sideboard.
   * @author Bill Darbie
   */
  void removeComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    Assert.expect(_refDesToComponentTypeMap != null);

    _projectObservable.setEnabled(false);
    try
    {
      String referenceDesignator = componentType.getReferenceDesignator();
      ComponentType prev = _refDesToComponentTypeMap.remove(referenceDesignator);
      Assert.expect(prev != null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, componentType, null, SideBoardTypeEventEnum.ADD_OR_REMOVE_COMPONENT_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public void addFiducialType(FiducialType fiducialType)
  {
    Assert.expect(fiducialType != null);

    _projectObservable.setEnabled(false);
    try
    {
      FiducialType prev = _fiducialNameToFiducialTypeMap.put(fiducialType.getName(), fiducialType);
      Assert.expect(prev == null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, fiducialType, SideBoardTypeEventEnum.ADD_OR_REMOVE_FIDUCIAL_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  void removeFiducialType(FiducialType fiducialType)
  {
    Assert.expect(fiducialType != null);

    _projectObservable.setEnabled(false);
    try
    {
      FiducialType prev = _fiducialNameToFiducialTypeMap.remove(fiducialType.getName());
      Assert.expect(prev != null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, fiducialType, null, SideBoardTypeEventEnum.ADD_OR_REMOVE_FIDUCIAL_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public List<FiducialType> getFiducialTypes()
  {
    return new ArrayList<FiducialType>(_fiducialNameToFiducialTypeMap.values());
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasFiducialType(String fiducialName)
  {
    Assert.expect(fiducialName != null);

    return _fiducialNameToFiducialTypeMap.containsKey(fiducialName);
  }

  /**
   * @author Bill Darbie
   */
  public FiducialType getFiducialType(String fiducialName)
  {
    Assert.expect(fiducialName != null);

    FiducialType fiducialType = _fiducialNameToFiducialTypeMap.get(fiducialName);
    Assert.expect(fiducialType != null);
    return fiducialType;
  }

  /**
   * Create a new FiducialType and all associated Fiducials for every SideBoard instance.
   * @author Bill Darbie
   */
  public FiducialType createFiducialType(String fiducialName, BoardCoordinate coordinate)
  {
    Assert.expect(fiducialName != null);
    Assert.expect(coordinate != null);

    FiducialType fiducialType = null;
    _projectObservable.setEnabled(false);
    try
    {
      fiducialType = new FiducialType();
      fiducialType.setCoordinateInNanoMeters(new PanelCoordinate(0, 0));
      fiducialType.setWidthInNanoMeters(MathUtil.convertMilsToNanoMetersInteger(100));
      fiducialType.setLengthInNanoMeters(MathUtil.convertMilsToNanoMetersInteger(100));
      fiducialType.setShapeEnum(ShapeEnum.CIRCLE);

      // now add Fiducials to each SideBoard
      for (SideBoard sideBoard : getSideBoards())
      {
        Fiducial fiducial = new Fiducial();
        fiducial.setFiducialType(fiducialType);
        fiducial.setSideBoard(sideBoard);
      }
      fiducialType.setName(fiducialName);
      fiducialType.setCoordinateInNanoMeters(coordinate);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(fiducialType, null, fiducialType, FiducialTypeEventEnum.CREATE_OR_DESTROY);

    Assert.expect(fiducialType != null);
    return fiducialType;
  }

  /**
   * @return all component packages for this SideBoardType
   * @author Erica Wheatcroft
   */
  public List<CompPackage> getCompPackages()
  {
    Set<CompPackage> compPackageSet = new TreeSet<CompPackage>(new CompPackageComparator(true, CompPackageComparatorEnum.LONG_NAME)); 

    for (ComponentType componentType : _refDesToComponentTypeMap.values())
    {
      compPackageSet.add(componentType.getCompPackage());
    }

    return new ArrayList<CompPackage>(compPackageSet);
  }

  /**
   * @author Bill Darbie
   */
  public void setSideBoardTypeSettings(SideBoardTypeSettings sideBoardTypeSettings)
  {
    Assert.expect(sideBoardTypeSettings != null);

    if (sideBoardTypeSettings == _sideBoardTypeSettings)
      return;

    SideBoardTypeSettings oldValue = _sideBoardTypeSettings;
    _projectObservable.setEnabled(false);
    try
    {
      _sideBoardTypeSettings = sideBoardTypeSettings;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, sideBoardTypeSettings, SideBoardTypeEventEnum.SIDE_BOARD_TYPE_SETTINGS);
  }

  /**
   * @author Keith Lee
   */
  public SideBoardTypeSettings getSideBoardTypeSettings()
  {
    Assert.expect(_sideBoardTypeSettings != null);

    return _sideBoardTypeSettings;
  }

  /**
   * @author Bill Darbie
   */
  public void addSideBoard(SideBoard sideBoard)
  {
    Assert.expect(sideBoard != null);
    Assert.expect(_sideBoardSet != null);

    _projectObservable.setEnabled(false);
    try
    {
      boolean added = _sideBoardSet.add(sideBoard);
      Assert.expect(added);
      sideBoard.setSideBoardType(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, sideBoard, SideBoardTypeEventEnum.ADD_OR_REMOVE_SIDE_BOARD);
  }

  /**
   * @author Bill Darbie
   */
  void removeSideBoard(SideBoard sideBoard)
  {
    Assert.expect(sideBoard != null);

    _projectObservable.setEnabled(false);
    try
    {
      boolean removed = _sideBoardSet.remove(sideBoard);
      Assert.expect(removed);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, sideBoard, null, SideBoardTypeEventEnum.ADD_OR_REMOVE_SIDE_BOARD);
  }

  /**
   * @author Bill Darbie
   */
  public List<SideBoard> getSideBoards()
  {
    Assert.expect(_sideBoardSet != null);

    return new ArrayList<SideBoard>(_sideBoardSet);
  }

  /**
   * @author Bill Darbie
   */
  public Set<PadType> getPadTypesUnsorted()
  {
    Set<PadType> padTypeSet = new HashSet<PadType>();
    for (ComponentType componentType : _refDesToComponentTypeMap.values())
    {
      padTypeSet.addAll(componentType.getPadTypes());
    }

    return padTypeSet;
  }

  /**
   * @author Bill Darbie
   */
  public List<PadType> getPadTypes()
  {
    Set<PadType> padTypeSet = new TreeSet<PadType>(new PadTypeComparator());
    for (ComponentType componentType : _refDesToComponentTypeMap.values())
    {
      for (PadType padType : componentType.getPadTypes())
      {
        boolean added = padTypeSet.add(padType);
        Assert.expect(added);
      }
    }

    return new ArrayList<PadType>(padTypeSet);
  }

  /**
   * @author Bill Darbie
   */
  public List<LandPattern> getLandPatterns()
  {
    Set<LandPattern> landPatternSet = new TreeSet<LandPattern>(new AlphaNumericComparator());
    for (ComponentType componentType : getComponentTypes())
    {
      landPatternSet.add(componentType.getLandPattern());
    }

    return new ArrayList<LandPattern>(landPatternSet);
  }

  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getSubtypesUnsorted()
  {
    Set<Subtype> subtypeSet = new HashSet<Subtype>();
    for (PadType padType : getPadTypesUnsorted())
    {
      subtypeSet.add(padType.getSubtype());
    }

    return subtypeSet;
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getSubtypes()
  {
    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    for (PadType padType : getPadTypes())
    {
      subtypeSet.add(padType.getSubtype());
    }

    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getSubtypesUnsorted(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    Set<Subtype> subtypeSet = new HashSet<Subtype>();
    for (Subtype subtype : getSubtypesUnsorted())
    {
      if (subtype.getJointTypeEnum().equals(jointTypeEnum))
        subtypeSet.add(subtype);
    }

    Assert.expect(subtypeSet != null);
    return subtypeSet;
  }
  
  /**
   * @author Wei Chin
   */
  public Set<Subtype> getSubtypesUnsorted(LandPattern landPattern, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(landPattern != null);

    Set<Subtype> subtypeSet = new HashSet<Subtype>();
    for (Subtype subtype : getSubtypesUnsorted())
    {
      if (subtype.getLandPattern().equals(landPattern) && subtype.getJointTypeEnum().equals(jointTypeEnum))
        subtypeSet.add(subtype);
    }

    Assert.expect(subtypeSet != null);
    return subtypeSet;
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getSubtypes(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    subtypeSet.addAll(getSubtypesUnsorted(jointTypeEnum));

    Assert.expect(subtypeSet != null);
    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getInspectedSubtypesUnsorted()
  {
    Set<Subtype> subtypeSet = new HashSet<Subtype>();
    for (ComponentType componentType : _refDesToComponentTypeMap.values())
    {
      List<PadType> padTypes = componentType.getPadTypes();
      if (componentType.isLoaded())
      {
        for (PadType padType : padTypes)
        {
          if (padType.isInspected())
          {
            Subtype subtype = padType.getSubtype();
            subtypeSet.add(subtype);
          }
        }
      }
    }
    Assert.expect(subtypeSet != null);
    return subtypeSet;
  }

  /**
   * @author Wei Chin, Chong
   */
  public Set<Subtype> getInspectedAndUnImportedSubtypesUnsorted()
  {
    Set<Subtype> subtypeSet = new HashSet<Subtype>();
    for (ComponentType componentType : _refDesToComponentTypeMap.values())
    {
      List<PadType> padTypes = componentType.getPadTypes();
      if (componentType.isLoaded())
      {
        for (PadType padType : padTypes)
        {
          if (padType.isInspected())
          {
            Subtype subtype = padType.getSubtype();
            if(subtype.isImportedPackageLibrary() == false)
              subtypeSet.add(subtype);
          }
        }
      }
    }
    Assert.expect(subtypeSet != null);
    return subtypeSet;
  }


  /**
   * @author Bill Darbie
   */
  public List<Subtype> getInspectedSubtypes()
  {
    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    subtypeSet.addAll(getInspectedSubtypesUnsorted());

    Assert.expect(subtypeSet != null);
    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getInspectedSubtypes(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    for (ComponentType componentType : _refDesToComponentTypeMap.values())
    {
      List<PadType> padTypes = componentType.getPadTypes();
      if (componentType.isLoaded())
      {
        for (PadType padType : padTypes)
        {
          if (padType.isInspected())
          {
            Subtype subtype = padType.getSubtype();
            if (subtype.getJointTypeEnum().equals(jointTypeEnum))
              subtypeSet.add(subtype);
          }
        }
      }
    }
    Assert.expect(subtypeSet != null);
    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Wei Chin, Chong
   */
  public List<Subtype> getInspectedAndUnImportedSubtypes(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    for (ComponentType componentType : _refDesToComponentTypeMap.values())
    {
      List<PadType> padTypes = componentType.getPadTypes();
      if (componentType.isLoaded())
      {
        for (PadType padType : padTypes)
        {
          if (padType.isInspected())
          {
            Subtype subtype = padType.getSubtype();
            if (subtype.getJointTypeEnum().equals(jointTypeEnum) && subtype.isImportedPackageLibrary() == false)
              subtypeSet.add(subtype);
          }
        }
      }
    }
    Assert.expect(subtypeSet != null);
    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getTestableSubtypesUnsorted()
  {
    Set<Subtype> subtypeSet = new HashSet<Subtype>();
    for (ComponentType componentType : _refDesToComponentTypeMap.values())
    {
      List<PadType> padTypes = componentType.getPadTypes();
      for (PadType padType : padTypes)
      {
        if (padType.getPadTypeSettings().areAllPadsNotTestable() == false)
        {
          Subtype subtype = padType.getSubtype();
          subtypeSet.add(subtype);
        }
      }
    }
    Assert.expect(subtypeSet != null);
    return subtypeSet;
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getTestableSubtypes()
  {
    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new AlphaNumericComparator());
    subtypeSet.addAll(getTestableSubtypesUnsorted());

    Assert.expect(subtypeSet != null);
    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getTestableSubtypesUnsorted(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Set<Subtype> subtypeSet = new HashSet<Subtype>();
    for (ComponentType componentType : _refDesToComponentTypeMap.values())
    {
      List<PadType> padTypes = componentType.getPadTypes();
      for (PadType padType : padTypes)
      {
        if (padType.getPadTypeSettings().areAllPadsNotTestable() == false)
        {
          Subtype subtype = padType.getSubtype();
          if (subtype.getJointTypeEnum().equals(jointTypeEnum))
            subtypeSet.add(subtype);
        }
      }
    }
    Assert.expect(subtypeSet != null);
    return subtypeSet;
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getTestableSubtypes(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    subtypeSet.addAll(getTestableSubtypesUnsorted(jointTypeEnum));

    Assert.expect(subtypeSet != null);
    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Bill Darbie
   */
  boolean isComponentTypeReferenceDesignatorDuplicate(String refDes)
  {
    Assert.expect(refDes != null);
    if (_refDesToComponentTypeMap.containsKey(refDes))
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  void setNewComponentTypeReferenceDesignatorName(ComponentType componentType, String oldName, String newName)
  {
    Assert.expect(componentType != null);
    Assert.expect(newName != null);
    ComponentType prev = null;
    if (oldName != null)
    {
      prev = _refDesToComponentTypeMap.remove(oldName);
      Assert.expect(prev != null);
    }
    prev = _refDesToComponentTypeMap.put(newName, componentType);
    Assert.expect(prev == null);

    if (oldName == null)
      _projectObservable.stateChanged(this, null, componentType, SideBoardTypeEventEnum.ADD_OR_REMOVE_COMPONENT_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  void setNewFiducialTypeName(FiducialType fiducialType, String oldName, String newName)
  {
    Assert.expect(fiducialType != null);
    Assert.expect(newName != null);

    FiducialType prev = null;
    if (oldName != null)
    {
      prev = _fiducialNameToFiducialTypeMap.remove(oldName);
      Assert.expect(prev != null);
    }
    prev = _fiducialNameToFiducialTypeMap.put(newName, fiducialType);
    Assert.expect(prev == null);

    if (oldName == null)
      _projectObservable.stateChanged(this, null, fiducialType, SideBoardTypeEventEnum.ADD_OR_REMOVE_FIDUCIAL_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  boolean isSideBoardType1()
  {
    BoardType boardType = getBoardType();
    if (boardType.sideBoardType1Exists() && (this == getBoardType().getSideBoardType1()))
      return true;

    return false;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public List<CompPackageOnPackage> getCompPackageOnPackages()
  {
    Set<CompPackageOnPackage> compPackageOnPackageSet = new TreeSet<CompPackageOnPackage>(new CompPackageOnPackageComparator(true, CompPackageOnPackageComparatorEnum.NAME)); 

    for (ComponentType componentType : _refDesToComponentTypeMap.values())
    {
      if (componentType.hasCompPackageOnPackage() && _boardType.getPanel().getCompPackageOnPackages().contains(componentType.getCompPackageOnPackage()))
      {
        compPackageOnPackageSet.add(componentType.getCompPackageOnPackage());
      }
    }

    return new ArrayList<CompPackageOnPackage>(compPackageOnPackageSet);
  }
}
