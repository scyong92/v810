package com.axi.v810.business.panelDesc;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.datastore.ndfReaders.NdfReader;
import com.axi.v810.datastore.ProjectHistoryLog;


/**
 * There is one ComponentType instance for each unique Component on a BoardType.
 *
 * @author Bill Darbie
 */
public class ComponentType implements Serializable
{
  // created by ComponentNdfReader, BoardTypeReader
  private Panel _panel; // ComponentNdfReader, BoardTypeReader

  private SideBoardType _sideBoardType;
  // the component's name i.e. C1, U4, etc.
  private String _referenceDesignator; // ComponentNdfReader, BoardTypeReader
  // a customer specific part number - not used now, but we should when we have access to it
  // which will be when we use something besides ndf files as inputs
  private String _subtypeName; // bee hoon, specify the subtype name according to package name and subtype number
  private String _partNumber; // no NdfReader
  // a customer specific part number - not used now, but we should when we have access to it
  // which will be when we use something besides ndf files as inputs
  private String _partLocationInFactory; // no NdfReader

  // component coordinate relative to the board's origin (lower left corner)
  // this coordinate is not necessarily the center of the component, but is the
  // point from which the pad/pin coordinates are referenced.
  private BoardCoordinate _coordinateInNanoMeters; // BoardNdfReader, BoardTypeReader
  private DoubleRef _degreesRotationRelativeToBoard; // BoardNdfReader, BoardTypeReader

  private CompPackage _compPackage; // ComponentNdfReader, BoardTypeReader
  private LandPattern _landPattern; // BoardNdfReader, BoardTypeReader
  private ComponentTypeSettings _componentTypeSettings; // ComponentNdfReader, PanelReader, BoardTypeSettingsReader
  private Map<String, PadType> _padTypeNameToPadTypeMap = new TreeMap<String, PadType>(new AlphaNumericComparator()); // BoardNdfReader, CompPackageReader
  private Set<Component> _componentSet = new HashSet<Component>(); // BoardNdfReader, PanelReader
  private List<PadType> _interferencePatternAffectedPadTypes;

  private transient java.awt.Shape _shape;
  private transient java.awt.Shape _shapeSurroundingPads;
  private transient java.awt.Shape _shapeBoundedByPads;
  // this is the actual area taken up by the pads which is not to be confused
  // by the bounds of the pads. The area will know where the empty space is.
  // This is what I need. George A. David
  private transient java.awt.geom.Area _padArea;
  private static transient ProjectObservable _projectObservable;
  private static transient JointTypeEnumAssignment _jointTypeEnumAssignment;

  private static transient SubtypeSchemeObservable _subtypeSchemeObservable;
  private transient LibrarySubtypeScheme _assignLibrarySubtypeScheme;

  private boolean _isSetToUsePspResult = false;
  //Project's History log -hsia-fen.tan
  private static transient ProjectHistoryLog _fileWriter;
  private boolean _waitingForShapeCreatedBooleanLock = false;
  
  private static transient PadNameComparator _padNameComparator;
  private static transient AlphaNumericComparator _alphaNumericComparator;
  private static transient ComponentComparator _componentNameComparator;  
  
  //Khaw Chek Hau - XCR3554: Package on package (PoP) development
  private POPLayerIdEnum _popLayerId = POPLayerIdEnum.BASE;
  private CompPackageOnPackage _compPackageOnPackage = null;
  private int _popZHeightInNanometers = 0;
  
  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
    _jointTypeEnumAssignment = JointTypeEnumAssignment.getInstance();
    _subtypeSchemeObservable = SubtypeSchemeObservable.getInstance();
    _fileWriter = ProjectHistoryLog.getInstance();
    _padNameComparator = new PadNameComparator();
    _alphaNumericComparator = new AlphaNumericComparator();
    _componentNameComparator =  new ComponentComparator(true, ComponentComparatorEnum.FULLY_QUALIFIED_NAME);
  }

  /**
   * @author Bill Darbie
   * @author Yee Seong
   */
  public ComponentType()
  {
    _projectObservable.stateChanged(this, null, this, ComponentTypeEventEnum.CREATE_OR_DESTROY);
    _interferencePatternAffectedPadTypes = new ArrayList<PadType>();
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      getSideBoardType().removeComponentType(this);

      getLandPattern().removeComponentType(this);

      for (PadType padType : getPadTypes())
      {
        // a destroy can be called during import, before subtypes have been assigned
        if (padType.hasSubtype())
        {
          Subtype subtype = padType.getSubtype();
          subtype.removeFromPadType(padType);
        }
      }

      // calling getCompPackage would normally create one if it did not already
      // exist.  We do not want to do this, especially for the case where we
      // are deleting this ComponentType to replace it with a fiducial - since
      // Fiducials do not have CompPackages.
      if (hasCompPackageBeenAssigned())
      {
        CompPackage compPackage = getCompPackage();
        compPackage.removeComponentType(this);
        _panel.destroyCompPackageIfNotInUse(compPackage);
      }

      for (Component component : getComponents())
        component.destroy();

      _componentTypeSettings.destroy();
      _componentTypeSettings = null;

      for (PadType padType : getPadTypes())
        padType.destroy();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, ComponentTypeEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void remove()
  {
    _projectObservable.setEnabled(false);
    try
    {
      getSideBoardType().removeComponentType(this);

      getLandPattern().removeComponentType(this);

      for (PadType padType : getPadTypes())
      {
        Subtype subtype = padType.getSubtype();
        subtype.removeFromPadType(padType);
      }

      if (hasCompPackageOnPackage())
      {
        CompPackageOnPackage compPackageOnPackage = getCompPackageOnPackage(); 
        
        for (ComponentType componentType : compPackageOnPackage.getComponentTypes())
        {          
          compPackageOnPackage.removeComponentType(componentType);  
          componentType.removeCompPackageOnPackage();
        }
        _panel.removeCompPackageOnPackage(compPackageOnPackage);
      }
      
      // calling getCompPackage would normally create one if it did not already
      // exist.  We do not want to do this, especially for the case where we
      // are deleting this ComponentType to replace it with a fiducial - since
      // Fiducials do not have CompPackages.
      if (hasCompPackageBeenAssigned())
      {
        CompPackage compPackage = getCompPackage();
        compPackage.removeComponentType(this);

        if (compPackage.getComponentTypes().isEmpty())
        {
          // this ComponentType was the only one being used by this compPackage, so delete the CompPackage since
          // it is no longer needed
          // if this is not done, then we will get PackagePins that do not have LandPatternPads assigned to them
          // which will cause asserts
          _panel.removeCompPackage(compPackage);
        }
      }
      for (Component component : getComponents())
        component.getSideBoard().removeComponent(component);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, ComponentTypeEventEnum.ADD_OR_REMOVE);

  }

  /**
   * @author Andy Mechtenberg
   */
  public void add()
  {
    _projectObservable.setEnabled(false);
    try
    {
      getSideBoardType().addComponentType(this);

      getLandPattern().addComponentType(this);

      if ((_compPackage != null) && (_compPackage.getComponentTypes().isEmpty()))
        _panel.addCompPackage(_compPackage);

      for (PadType padType : getPadTypes())
      {
        Subtype subtype = padType.getSubtype();
        subtype.assignedToPadType(padType);
      }

      // calling getCompPackage would normally create one if it did not already
      // exist.  We do not want to do this, especially for the case where we
      // are deleting this ComponentType to replace it with a fiducial - since
      // Fiducials do not have CompPackages.
      if (hasCompPackageBeenAssigned())
      {
        CompPackage compPackage = getCompPackage();
        compPackage.addComponentType(this);
      }

      for (Component component : getComponents())
        component.getSideBoard().addComponent(component);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, this, ComponentTypeEventEnum.ADD_OR_REMOVE);

  }

  /**
   * @author Bill Darbie
   */
  public void setPanel(Panel panel)
  {
    Assert.expect(panel != null);
    _panel = panel;
  }

  /**
   * @author Bill Darbie
   */
  public void setSideBoardType(SideBoardType sideBoardType)
  {
    Assert.expect(sideBoardType != null);

    if (sideBoardType == _sideBoardType)
      return;

    SideBoardType oldValue = _sideBoardType;
    _projectObservable.setEnabled(false);
    try
    {
      _sideBoardType = sideBoardType;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, sideBoardType, ComponentTypeEventEnum.SIDE_BOARD_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public SideBoardType getSideBoardType()
  {
    Assert.expect(_sideBoardType != null);

    return _sideBoardType;
  }

  /**
   * @author Bill Darbie
   */
  public void setReferenceDesignator(String referenceDesignator)
  {
    Assert.expect(referenceDesignator != null);
    Assert.expect(_panel != null);
    if (_panel.areChecksDisabledForNdfparsers() == false)
    {
      Assert.expect(_sideBoardType != null);
      BoardType boardType = _sideBoardType.getBoardType();
      Assert.expect(boardType.isComponentTypeReferenceDesignatorValid(referenceDesignator));
      Assert.expect(boardType.isComponentTypeReferenceDesignatorDuplicate(referenceDesignator) == false);
    }

    if (referenceDesignator.equals(_referenceDesignator))
      return;

    String oldValue = _referenceDesignator;
    _projectObservable.setEnabled(false);
    try
    {
      if (_panel.areChecksDisabledForNdfparsers() == false)
      {
        for (Component component : getComponents())
          component.getSideBoard().setNewComponentName(component, _referenceDesignator, referenceDesignator);

        Assert.expect(_sideBoardType != null);
        _sideBoardType.setNewComponentTypeReferenceDesignatorName(this, _referenceDesignator, referenceDesignator);
      }
      _referenceDesignator = referenceDesignator.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, referenceDesignator, ComponentTypeEventEnum.REFERENCE_DESIGNATOR);
  }

  /**
   * @author Bill Darbie
   */
  public String getReferenceDesignator()
  {
    Assert.expect(_referenceDesignator != null);

    return _referenceDesignator;
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getReferenceDesignator();
  }

  /**
   * @author Bill Darbie
   */
  public void setPartNumber(String partNumber)
  {
    Assert.expect(partNumber != null);

    if (partNumber.equals(_partNumber))
      return;

    String oldValue = _partNumber;
    _projectObservable.setEnabled(false);
    try
    {
      _partNumber = partNumber.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, partNumber, ComponentTypeEventEnum.PART_NUMBER);
  }

  /**
   * @author Bill Darbie
   */
  public String getPartNumber()
  {
    Assert.expect(_partNumber != null);

    return _partNumber;
  }

  /**
   * @author Bill Darbie
   */
  public PadTypeEnum getPadTypeEnum()
  {
    Assert.expect(_landPattern != null);

    return _landPattern.getPadTypeEnum();
  }

  /**
   * @author Bill Darbie
   */
  public void setPartLocationInFactory(String partLocationInFactory)
  {
    Assert.expect(partLocationInFactory != null);

    if (partLocationInFactory == _partLocationInFactory)
      return;

    String oldValue = _partLocationInFactory;
    _projectObservable.setEnabled(false);
    try
    {
      _partLocationInFactory = partLocationInFactory.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, partLocationInFactory, ComponentTypeEventEnum.PART_LOCATION_IN_FACTORY);
  }

  /**
   * @author Bill Darbie
   */
  String getPartLocationInFactory()
  {
    Assert.expect(_partLocationInFactory != null);

    return _partLocationInFactory;
  }

  /**
   * Set the x,y coordinate of this component relative to its boards lower left corner.
   * This coordinate is not necessarily the center of the component, but is the
   * point from which the pad coordinates are referenced.
   * @author Bill Darbie
   */
  public void setCoordinateInNanoMeters(BoardCoordinate coordinateInNanoMeters)
  {
    Assert.expect(coordinateInNanoMeters != null);

    if (coordinateInNanoMeters.equals(_coordinateInNanoMeters))
      return;

    BoardCoordinate oldValue = _coordinateInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _coordinateInNanoMeters = coordinateInNanoMeters;
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, coordinateInNanoMeters, ComponentTypeEventEnum.COORDINATE);
  }

  /**
   * Get the x,y coordinate of this component relative to its SideBoards lower left corner.
   * This coordinate is not necessarily the center of the component, but is the
   * point from which the pad coordinates are referenced.
   * @author Bill Darbie
   */
  public BoardCoordinate getCoordinateInNanoMeters()
  {
    Assert.expect(_coordinateInNanoMeters != null);

    return new BoardCoordinate(_coordinateInNanoMeters);
  }

  /**
   * @return the x axis width of the ComponentType BEFORE any rotation has been applied.
   * @author Keith Lee
   */
  public int getWidthInNanoMeters()
  {
    Assert.expect(_landPattern != null);

    return _landPattern.getWidthInNanoMeters();
  }

  /**
   * @return the y axis length of the ComponentType BEFORE any rotation has been applied.
   * @author Keith Lee
   */
  public int getLengthInNanoMeters()
  {
    Assert.expect(_landPattern != null);

    return _landPattern.getLengthInNanoMeters();
  }


  /**
   * @author Bill Darbie
   */
  public void setDegreesRotationRelativeToBoard(double degreesRotationRelativeToBoard)
  {
    degreesRotationRelativeToBoard = MathUtil.getDegreesWithin0To359(degreesRotationRelativeToBoard);

    if ((_degreesRotationRelativeToBoard != null) && (degreesRotationRelativeToBoard == _degreesRotationRelativeToBoard.getValue()))
      return;

    DoubleRef oldValue = _degreesRotationRelativeToBoard;
    _projectObservable.setEnabled(false);
    try
    {
      if (_degreesRotationRelativeToBoard == null)
        _degreesRotationRelativeToBoard = new DoubleRef(degreesRotationRelativeToBoard);
      else
        _degreesRotationRelativeToBoard.setValue(degreesRotationRelativeToBoard);
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, new DoubleRef(degreesRotationRelativeToBoard), ComponentTypeEventEnum.DEGREES_ROTATION);
  }

  /**
   * @author Keith Lee
   */
  public double getDegreesRotationRelativeToBoard()
  {
    Assert.expect(_degreesRotationRelativeToBoard != null);

    return _degreesRotationRelativeToBoard.getValue();
  }

  /**
   * @author Bill Darbie
   */
  public void setCompPackage(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);

    if (compPackage == _compPackage)
      return;

    CompPackage oldValue = _compPackage;
    _projectObservable.setEnabled(false);
    try
    {
      _compPackage = compPackage;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, compPackage, ComponentTypeEventEnum.COMP_PACKAGE);
  }

  /**
   * @author Bill Darbie
   */
  boolean hasCompPackageBeenAssigned()
  {
    if (_compPackage == null)
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  public CompPackage getCompPackage()
  {
    if (_compPackage == null)
      assignOrModifyCompPackageAndSubtypeAndJointTypeEnums();

    Assert.expect(_compPackage != null);
    return _compPackage;
  }

  /**
   * @author Bill Darbie
   */
  private void modifyCompPackageIfNecessary(String longPackageName, String shortPackageName, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(longPackageName != null);
    Assert.expect(shortPackageName != null);
    Assert.expect(jointTypeEnum != null);

    Panel panel = getSideBoardType().getBoardType().getPanel();
    if (panel.doesCompPackageExist(longPackageName) == false)
    {
      // modify the existing CompPackage
      _compPackage.setLongName(longPackageName);
      if (_compPackage.getShortName().equals(shortPackageName) == false)
        _compPackage.setShortName(shortPackageName);
    }
    else
    {
      // the CompPackage exists already, assign it to the current ComponentType
      CompPackage compPackage = panel.getCompPackage(longPackageName);
      if (compPackage != _compPackage)
      {
        // remove the old CompPackage
        _compPackage.removeComponentType(this);
        if (_compPackage.getComponentTypes().isEmpty())
            _compPackage.destroy();
        // set this CompPackage to the new one
        _compPackage = compPackage;
      }
    }

    if (_compPackage.hasComponentType(this) == false)
      _compPackage.addComponentType(this);
  }

  /**
   * @author Bill Darbie
   */
  private boolean createNewCompPackageIfNecessary(String longPackageName, String shortPackageName, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(longPackageName != null);
    Assert.expect(shortPackageName != null);
    Assert.expect(jointTypeEnum != null);

    boolean newCompPackage = false;
    LandPattern landPattern = getLandPattern();
    Panel panel = getSideBoardType().getBoardType().getPanel();
    if (panel.doesCompPackageExist(longPackageName) == false)
    {
      // the CompPackage does not exist yet, so create it
      newCompPackage = true;

      CompPackage compPackage = new CompPackage();
      compPackage.setPanel(_panel);

      compPackage.setLongName(longPackageName);
      compPackage.setShortName(shortPackageName);

      compPackage.setLandPattern(landPattern);

      // create PackagePin and PackagePinSettings instances
      for (LandPatternPad landPatternPad : landPattern.getLandPatternPads())
      {
        PackagePin packagePin = new PackagePin();
        packagePin.setLandPatternPad(landPatternPad);
        packagePin.setCompPackage(compPackage);
        compPackage.addPackagePin(packagePin);
      }
      if (_compPackage != null)
      {
        _compPackage.removeComponentType(this);
        if (_compPackage.getComponentTypes().isEmpty())
            _compPackage.destroy();
      }
      setCompPackage(compPackage);
    }
    else
    {
      // the CompPackage exists already, assign it to the current ComponentType
      _compPackage = panel.getCompPackage(longPackageName);
    }

    if (_compPackage.hasComponentType(this) == false)
      _compPackage.addComponentType(this);

    return newCompPackage;
  }

  /**
   * @author Bill Darbie
   */
  public void assignOrModifyCompPackageAndSubtypeAndJointTypeEnums()
  {
    _jointTypeEnumAssignment.determineJointTypeAssignment(this);
    Map<PadType, JointTypeEnum> padTypeToJointTypeEnumMap = _jointTypeEnumAssignment.getPadTypeToJointTypeEnumMap();
    JointTypeEnum jointTypeEnum = _jointTypeEnumAssignment.getJointTypeEnum();
    _jointTypeEnumAssignment.clear();

    LandPattern landPattern = getLandPattern();

    String packageName = _panel.getLongCompPackageName(landPattern, jointTypeEnum);

    boolean newJointTypeEnumAssignment = false;
    boolean newCompPackage = createNewCompPackageIfNecessary(packageName, landPattern.getName(), jointTypeEnum);
    if (newCompPackage)
    {
      newJointTypeEnumAssignment = true;
    }

    // check that the LandPattern and the CompPackage have the
    // same number of Pads and Pins
    List<PadType> padTypes = getPadTypes();
    List<LandPatternPad> landPatternPads = landPattern.getLandPatternPads();
    List<PackagePin> packagePins = _compPackage.getPackagePins();

    int numPadTypes = padTypes.size();
    int numLandPatternPads = landPatternPads.size();
    int numPackagePins = packagePins.size();
    Assert.expect(numPadTypes == numLandPatternPads);
    Assert.expect(numLandPatternPads == numPackagePins);

    // now assign the appropriate PackagePin to each PadType
    Iterator padTypeIt = padTypes.iterator();
    Iterator landPatternPadIt = landPatternPads.iterator();
    Iterator packagePinsIt = packagePins.iterator();
    while (landPatternPadIt.hasNext())
    {
      LandPatternPad landPatternPad = (LandPatternPad)landPatternPadIt.next();
      PadType padType = (PadType)padTypeIt.next();
      PackagePin packagePin = (PackagePin)packagePinsIt.next();

      Assert.expect(padType.getName().equals(packagePin.getName()));
      padType.setPackagePin(packagePin);
      Assert.expect(padType.getName().equals(landPatternPad.getName()));
      padType.setLandPatternPad(landPatternPad);

      if ((newJointTypeEnumAssignment == false) && packagePin.hasJointTypeEnumAssigned())
        jointTypeEnum = packagePin.getJointTypeEnum();
      else
        jointTypeEnum = padTypeToJointTypeEnumMap.get(padType);
      setJointTypeEnumAndSubtype(padType, jointTypeEnum);

      // Wei Chin --> Why Not?
      // dont test components with only one pad
//      if (numLandPatternPads == 1)
//      {
//        PadTypeSettings padTypeSettings = padType.getPadTypeSettings();
//        padTypeSettings.setInspected(false);
//      }
      // don't test pressfits
//      if (jointTypeEnum.equals(JointTypeEnum.PRESSFIT))
//      {
//        PadTypeSettings padTypeSettings = padType.getPadTypeSettings();
//        padTypeSettings.setInspected(false);
//      }
    }

    assignNewSubtypesToPadsWithDifferentPitches();
  }

  /**
   * @author Bill Darbie
   */
  void assignNewSubtypesToPadsWithDifferentPitches()
  {
    int padSizeThresholdPercent = _jointTypeEnumAssignment.getPadSizeThresholdForSubtypeAssignmentPercent();
    // If a LandPattern has LandPatternPads that have different pad sizes, then
    // we need to have a different subtype for each

    // for some joint types we have to have one subtype only
    if (_jointTypeEnumAssignment.forceOneSubtypeForEntireComponent())
      return;

    // first figure out how many different subtypes we have assigned to each Pad
    Map<Subtype, Set<PadType>> subtypeToPadTypeMap = new HashMap<Subtype, Set<PadType>>();
    for (PadType padType : getPadTypes())
    {
      Subtype subtype = padType.getSubtype();
      Set<PadType> padTypeSet = subtypeToPadTypeMap.get(subtype);
      if (padTypeSet == null)
      {
        padTypeSet = new HashSet<PadType>();
        padTypeSet.add(padType);
        subtypeToPadTypeMap.put(subtype, padTypeSet);
      }
      else
      {
        padTypeSet.add(padType);
      }
    }

    // for each set of Pads for a given subtype, check to see if there is more
    // than one pad size.  Each set of pads with a different size really needs
    // its own subtype
    for (Map.Entry<Subtype, Set<PadType>> entry1 : subtypeToPadTypeMap.entrySet())
    {
      Subtype subtype = entry1.getKey();
      JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
      Set<PadType> padTypeSet = entry1.getValue();

      // use a TreeMap so that the map will be sorted by pad size
      Map<Integer, Set<PadType>> padSizeToPadTypeMap = new TreeMap<Integer, Set<PadType>>();
      for (PadType padType : padTypeSet)
      {
        LandPatternPad landPatternPad = padType.getLandPatternPad();

        int padSize = 0;
        if (landPatternPad.isThroughHolePad())
        {
          ThroughHoleLandPatternPad thLandPatternPad = (ThroughHoleLandPatternPad)landPatternPad;
//          padSize = thLandPatternPad.getHoleDiameterInNanoMeters();
          //Siew Yeng - XCR-3318 - Oval PTH
          int width = thLandPatternPad.getHoleWidthInNanoMeters();
          int length = thLandPatternPad.getHoleLengthInNanoMeters();
          padSize = Math.max(width, length);
        }
        else if (landPatternPad.isSurfaceMountPad())
        {
          int width = landPatternPad.getWidthInNanoMeters();
          int length = landPatternPad.getLengthInNanoMeters();
          padSize = Math.max(width, length);
        }
        else
          Assert.expect(false);

        // check to see if this padSize is the same or within 5% of a previous pad size
        for (Integer padSizeInMap : padSizeToPadTypeMap.keySet())
        {
          int max = Math.max(padSizeInMap, padSize);
          double percent =  100.0 * (double)Math.abs(padSizeInMap - padSize) / (double)max;
          if (percent < padSizeThresholdPercent)
          {
            // the two sizes are within 5% of each other, so use the one that is already in
            // the map
            padSize = padSizeInMap;
            break;
          }
        }

        Set<PadType> padTypeSetForPadSize = padSizeToPadTypeMap.get(padSize);
        if (padTypeSetForPadSize == null)
        {
          padTypeSetForPadSize = new HashSet<PadType>();
          padTypeSetForPadSize.add(padType);
          padSizeToPadTypeMap.put(padSize, padTypeSetForPadSize);
        }
        else
        {
          padTypeSetForPadSize.add(padType);
        }
      }

      // now padSizeToPadTypeMap contains all information
      LandPattern landPattern = getLandPattern();
      if (padSizeToPadTypeMap.keySet().size() > 1)
      {
        // we have more than one padSize for the current subtype
        int i = 0;
        for (Map.Entry<Integer, Set<PadType>> entry2 : padSizeToPadTypeMap.entrySet())
        {
          ++i;
          int padSize = entry2.getKey();
          padTypeSet = entry2.getValue();

          String shortName = _panel.getShortSubtypeName(landPattern);
          String shortNameForDiffPadSize = landPattern.getName() + "_" + Integer.toString(i);
          String longSubtypeName = _panel.getLongSubtypeName(landPattern, jointTypeEnum);
          String longSubtypeNameForDiffPadSize = longSubtypeName + "_" + Integer.toString(i);

          // case 1 - subtype name matches origSubtypeName
          //          subtype.setName(origSubtypeNameForDiffPadSize) needs to be called
          // case 2 - subtype name matches origSubtypeNameForDiffPadSize
          //          no action needed
          // case 3 - subtype name does not match origSubtypeName or origSubtypeNameForDiffPadSize
          //          create a new subtype with name origSubtypeNameForDiffPadSize
          //          and assign it to this ComponenentTypes padTypes

          String subtypeName = subtype.getLongName();
          if (subtypeName.equals(longSubtypeName) && (i == 1))
          {
            if (subtype.getShortName().equals(shortNameForDiffPadSize) == false)
            {
              // do not change the original name or extra subtypes will be created
              //subtype.setOriginalName(origSubtypeNameForDiffPadSize);
              subtype.setShortName(shortNameForDiffPadSize);
            }
          }
          else if (subtypeName.equals(longSubtypeNameForDiffPadSize))
          {
            // do nothing
          }
          else
          {
            if (_panel.doesSubtypeExist(longSubtypeNameForDiffPadSize))
            {
              subtype = _panel.getSubtype(longSubtypeNameForDiffPadSize);
            }
            else
            {
              subtype = Subtype.createNewSubtype(_panel,
                                                 longSubtypeNameForDiffPadSize,
                                                 shortNameForDiffPadSize,
                                                 jointTypeEnum);
            }
            // Wei Chin added
            subtype.setLandPattern(landPattern);

            // set all PadTypes with this pad size to use this subtype
            for (PadType padType : padTypeSet)
                padType.getPadTypeSettings().setSubtype(subtype);
          }
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  public void setLandPattern(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);

    if (landPattern == _landPattern)
      return;

    LandPattern oldValue = _landPattern;
    _projectObservable.setEnabled(false);
    try
    {
      _landPattern = landPattern;
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, landPattern, ComponentTypeEventEnum.LAND_PATTERN);
  }

  /**
   * @author Bill Darbie
   */
  public LandPattern getLandPattern()
  {
    Assert.expect(_landPattern != null);

    return _landPattern;
  }

  /**
   * @author Bill Darbie
   */
  public void setComponentTypeSettings(ComponentTypeSettings componentTypeSettings)
  {
    Assert.expect(componentTypeSettings != null);

    if (componentTypeSettings == _componentTypeSettings)
      return;

    ComponentTypeSettings oldValue = _componentTypeSettings;
    _projectObservable.setEnabled(false);
    try
    {
      _componentTypeSettings = componentTypeSettings;
      _componentTypeSettings.setComponentType(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, componentTypeSettings, ComponentTypeEventEnum.COMPONENT_TYPE_SETTINGS);
  }

  /**
   * @author Bill Darbie
   */
  public ComponentTypeSettings getComponentTypeSettings()
  {
    Assert.expect(_componentTypeSettings != null);

    return _componentTypeSettings;
  }

  /**
   * @author Bill Darbie
   */
  public void addPadType(PadType padType)
  {
    Assert.expect(padType != null);
    Assert.expect(_padTypeNameToPadTypeMap != null);

    _projectObservable.setEnabled(false);
    try
    {
      padType.setComponentType(this);
      
      PadType prev = _padTypeNameToPadTypeMap.put(padType.getName(), padType);
      Assert.expect(prev == null);
      if (_componentTypeSettings != null)
        _componentTypeSettings.invalidateUsesOneSubtype();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, padType, ComponentTypeEventEnum.ADD_OR_REMOVE_PAD_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public void removePadType(PadType padType)
  {
    Assert.expect(padType != null);

    _projectObservable.setEnabled(false);
    try
    {
      PadType prev = _padTypeNameToPadTypeMap.remove(padType.getName());
      Assert.expect(prev != null);
      if (_componentTypeSettings != null)
        _componentTypeSettings.invalidateUsesOneSubtype();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, padType, ComponentTypeEventEnum.ADD_OR_REMOVE_PAD_TYPE);
  }


  /**
   * @return a List of PadTypes for this ComponentType
   * @author Bill Darbie
   */
  public List<PadType> getPadTypes()
  {
    Assert.expect(_padTypeNameToPadTypeMap != null);

    List<PadType> padTypes = new ArrayList<PadType>(_padTypeNameToPadTypeMap.values());
    
    if(padTypes.size() == 2)      
      Collections.sort(padTypes, _padNameComparator);
    
    return padTypes;
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasPadType(String padName)
  {
    Assert.expect(padName != null);

    return _padTypeNameToPadTypeMap.containsKey(padName);
  }

  /**
   * @author Bill Darbie
   */
  public PadType getPadType(String padName)
  {
    Assert.expect(padName != null);

    PadType padType = _padTypeNameToPadTypeMap.get(padName);
    Assert.expect(padType != null);

    return padType;
  }

  /**
   * @author Bill Darbie
   */
  public void addComponent(Component component)
  {
    Assert.expect(component != null);
    Assert.expect(_componentSet != null);

    _projectObservable.setEnabled(false);
    try
    {
      boolean added = _componentSet.add(component);
      Assert.expect(added);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, component, ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT);
  }

  /**
   * @author Bill Darbie
   */
  void removeComponent(Component component)
  {
    Assert.expect(component != null);

    _projectObservable.setEnabled(false);
    try
    {
      //Focus region suppose to be remove as well, because the board has no longer 
      //has any component 
      getSideBoardType().getBoardType().getPanel().getProject().removeComponentFocusRegionInMap(component);
      boolean removed = _componentSet.remove(component);
      Assert.expect(removed);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, component, null, ComponentTypeEventEnum.ADD_OR_REMOVE_COMPONENT);
  }

  /**
   * @author Bill Darbie
   */
  public List<Component> getComponents()
  {
    Assert.expect(_componentSet != null);

    List<Component> components = new ArrayList<Component>(_componentSet);
    Collections.sort(components, _componentNameComparator);

    Assert.expect(components != null);
    return components;
  }
  
  /**
   * @author Siew Yeng
   */
  public Component getComponent(Board board)
  {
    Assert.expect(_componentSet != null);
    Assert.expect(board != null);

    Component component = null;
    for(Component comp : _componentSet)
    {
      if(comp.getBoard().equals(board))
        component = comp;
    }
    
    Assert.expect(component != null);
    return component;
  }

  /**
   * @author Bill Darbie
   */
  public List<JointTypeEnum> getJointTypeEnums()
  {
    Set<JointTypeEnum> jointTypeEnumSet = new TreeSet<JointTypeEnum>(_alphaNumericComparator);
    for (PadType padType : getPadTypes())
    {
      jointTypeEnumSet.add(padType.getPackagePin().getJointTypeEnum());
    }

    return new ArrayList<JointTypeEnum>(jointTypeEnumSet);
  }

  /**
   * Set all PackagePins used by this ComponentType to the jointTypeEnum passed in.
   *
   * If a CompPackage does not already exists with the same LandPattern and
   * JointTypeEnums, then a new one will be created and a
   * new subtype will be created and assigned to this ComponentTypes PadTypes.
   *
   * If a CompPackage already exists it will be used and a new subtype will
   * be assigned to this ComponentTypes PadTypes.
   *
   * @author Bill Darbie
   */
  public void setJointTypeEnum(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    _jointTypeEnumAssignment.determineJointTypeAssignment(this);
    _jointTypeEnumAssignment.clear();


    JointTypeEnum oldValue = null;
    CompPackage compPackage = getCompPackage();
    if (compPackage.usesOneJointTypeEnum())
      oldValue = compPackage.getJointTypeEnum();

    _projectObservable.setEnabled(false);
    try
    {
      LandPattern landPattern = getLandPattern();
      List<JointTypeEnum> jointTypeEnums = new ArrayList<JointTypeEnum>();
      int numPins = compPackage.getPackagePinsUnsorted().size();
      for (int i = 0; i < numPins; ++i)
        jointTypeEnums.add(jointTypeEnum);

      CompPackage matchingCompPackage = _panel.getCompPackageIfItMatches(landPattern, jointTypeEnums);

      if (matchingCompPackage != null)
      {
        // we found a CompPackage that matches, so use it
        CompPackage oldCompPackage = getCompPackage();
        if (matchingCompPackage != oldCompPackage)
        {
          oldCompPackage.removeComponentType(this);
          if (oldCompPackage.getComponentTypes().isEmpty())
            oldCompPackage.destroy();

          setCompPackage(matchingCompPackage);
          matchingCompPackage.addComponentType(this);
        }

        // assign a new subtype to the PadTypes of this ComponentType
        Iterator<PackagePin> packagePinIt = _compPackage.getPackagePins().iterator();
        for (PadType padType : getPadTypes())
        {
          PackagePin packagePin = packagePinIt.next();
          padType.setPackagePin(packagePin);
          setJointTypeEnumAndSubtype(padType, jointTypeEnum);
        }
        assignNewSubtypesToPadsWithDifferentPitches();
      }
      else
      {
        // create a new CompPackage
        String compPackageLongName = _panel.getUniqueCompPackageLongName(landPattern, jointTypeEnum);
        String compPackageShortName = getCompPackage().getShortName(); //Jack Hwee
       // String compPackageShortName = landPattern.getName();
        int index = compPackageLongName.lastIndexOf("_");


        if (index != -1) {
          String endStr = compPackageLongName.substring(index, compPackageLongName.length());
          compPackageLongName += endStr;
        }
        createNewCompPackageIfNecessary(compPackageLongName, compPackageShortName, jointTypeEnum);

        // assign a new subtype to the PadTypes of this ComponentType
        Iterator<PackagePin> packagePinIt = _compPackage.getPackagePins().iterator();
        for (PadType padType : getPadTypes())
        {
          PackagePin packagePin = packagePinIt.next();
          padType.setPackagePin(packagePin);
          setJointTypeEnumAndSubtype(padType, jointTypeEnum);
        }
        assignNewSubtypesToPadsWithDifferentPitches();

        getCompPackage().assignPinOrientationCorrectionsOverridingPreviousUserSettings();
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, jointTypeEnum, ComponentTypeEventEnum.JOINT_TYPE_ENUM);
    //write into history log-hsia-fen.tan
    _fileWriter.appendSubtype(_referenceDesignator,oldValue, jointTypeEnum);
  }

  /**
   * @author Bill Darbie
   */
  public void changeSide()
  {
    SideBoardType sideBoardType = null;
    SideBoardType newSideBoardType = null;

    _projectObservable.setEnabled(false);
    try
    {
      sideBoardType = getSideBoardType();
      BoardType boardType = sideBoardType.getBoardType();
      Assert.expect(boardType.sideBoardType1Exists());
      Assert.expect(boardType.sideBoardType2Exists());

      SideBoardType sideBoardType1 = boardType.getSideBoardType1();
      SideBoardType sideBoardType2 = boardType.getSideBoardType2();
      if (sideBoardType == sideBoardType1)
      {
        newSideBoardType = sideBoardType2;
      }
      else if (sideBoardType == sideBoardType2)
      {
        newSideBoardType = sideBoardType1;
      }
      else
        Assert.expect(false);

      _sideBoardType.removeComponentType(this);
      setSideBoardType(newSideBoardType);
      newSideBoardType.addComponentType(this);

      for (Component component : getComponents())
      {
        component.changeSide();
      }
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, sideBoardType, newSideBoardType, ComponentTypeEventEnum.CHANGE_SIDE);
  }

  /**
   * @author Bill Darbie
   */
  private void setJointTypeEnumAndSubtype(PadType padType, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(padType != null);
    Assert.expect(jointTypeEnum != null);
    
    Subtype subtype;
     
    LandPattern landPattern = padType.getComponentType().getLandPattern();
    PackagePin packagePin = padType.getPackagePin();
    PadTypeSettings padTypeSettings = padType.getPadTypeSettings();
	
	//bee hoon, differentiate the subtype name according to package name and subtype name
    if (NdfReader.get5dxNdfWithDiffSubtype() == '1') 
    {
      subtype = _panel.get5DXSubtypeCreatingIfNecessary(landPattern, jointTypeEnum, get5dxNewSubtypeName());
    } else {
      subtype = _panel.getSubtypeCreatingIfNecessary(landPattern, jointTypeEnum);
    }

    packagePin.setJointTypeEnum(jointTypeEnum);

    padTypeSettings.setSubtype(subtype);
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getSubtypes()
  {
    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    for (PadType padType : getPadTypes())
      subtypeSet.add(padType.getSubtype());

    return new ArrayList<Subtype>(subtypeSet);
  }
  
  /**
   * @author Siew Yeng
   */
  public List<Subtype> getSubtypes(InspectionFamilyEnum inspectionFamilyEnum)
  {
    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    for (PadType padType : getPadTypes())
    {
      if(padType.getSubtype().getInspectionFamilyEnum() == inspectionFamilyEnum)
        subtypeSet.add(padType.getSubtype());
    }

    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Bill Darbie
   */
  private void getMaxAndMinPitch(LandPattern landPattern, IntegerRef maxPitch, IntegerRef minPitch)
  {
    Assert.expect(landPattern != null);
    Assert.expect(maxPitch != null);
    Assert.expect(minPitch != null);

    int minPitchInNanos = 0;
    int maxPitchInNanos = 0;
    for (LandPatternPad landPatternPad : landPattern.getLandPatternPads())
    {
      int pitchInNanos = landPatternPad.getPitchInNanoMeters();
      if (minPitchInNanos == 0)
      {
        minPitchInNanos = pitchInNanos;
        maxPitchInNanos = pitchInNanos;
      }
      else
      {
        minPitchInNanos = Math.min(pitchInNanos, minPitchInNanos);
        maxPitchInNanos = Math.max(pitchInNanos, maxPitchInNanos);
      }
    }

    maxPitch.setValue(maxPitchInNanos);
    minPitch.setValue(minPitchInNanos);
  }

  /**
   * @author Bill Darbie
   */
  void setNewPadTypeName(LandPatternPad landPatternPad, String oldName, String newName)
  {
    Assert.expect(landPatternPad != null);
    Assert.expect(newName != null);

    for (Component component : getComponents())
      component.setNewPadName(oldName, newName);

    // find the PadType associated with the LandPatternPad passed in
    PadType padType = null;
    for (PadType aPadType : getPadTypes())
    {
      if (aPadType.getLandPatternPad() == landPatternPad)
      {
        padType = aPadType;
        break;
      }
    }
    Assert.expect(padType !=  null);

    PadType prev = null;
    if (oldName != null)
    {
      prev = _padTypeNameToPadTypeMap.remove(oldName);
      Assert.expect(prev != null);
    }
    prev = _padTypeNameToPadTypeMap.put(newName, padType);
    Assert.expect(prev == null);
    if (_componentTypeSettings != null)
        _componentTypeSettings.invalidateUsesOneSubtype();

    if (oldName == null)
      _projectObservable.stateChanged(this, null, padType, ComponentTypeEventEnum.ADD_OR_REMOVE_PAD_TYPE);
  }

  /**
   * @author George A. David
   */
  public PadType getPadTypeOne()
  {
    PadType padTypeOne = null;
    for(PadType padType : _padTypeNameToPadTypeMap.values())
    {
      if (padType.isPadOne())
      {
        padTypeOne = padType;
        break;
      }
    }

    Assert.expect(padTypeOne != null);
    return padTypeOne;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isLoaded()
  {
    return getComponentTypeSettings().isLoaded();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isInspected()
  {
    return getComponentTypeSettings().isInspected();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isTestable()
  {
    return getComponentTypeSettings().isTestable();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isOnSideBoard1()
  {
    if (getSideBoardType().isSideBoardType1())
      return true;
    return false;
  }

  /**
   * @author Bill Darbie
   */
  public List<PadType> getInspectedPadTypes()
  {
    List<PadType> inspectedPadTypes = new ArrayList<PadType>();

    for (PadType padType : getPadTypes())
    {
      if (padType.isInspected())
        inspectedPadTypes.add(padType);
    }

    return inspectedPadTypes;
  }

  /**
   * @author Bill Darbie
   */
  public List<PadType> getTestablePadTypes()
  {
    List<PadType> testablePadTypes = new ArrayList<PadType>();

    for (PadType padType : getPadTypes())
    {    
      if (padType.getPadTypeSettings().areAllPadsNotTestable() == false)
        testablePadTypes.add(padType);    
    }

    return testablePadTypes;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isComponentTypeWithinBoardTypeOutline()
  {
    java.awt.Shape boardTypeShape = getSideBoardType().getBoardType().getShapeInNanoMeters();
    java.awt.Shape componentTypeShape = getShapeSurroundingPadsInNanoMeters();

    boolean within = MathUtil.fuzzyContains(boardTypeShape, componentTypeShape, 500);
    return within;
  }

  /**
   * @return false if the process of moving this component to the new coordinate passed in will cause it
   * to move off the board.
   * @author Bill Darbie
   */
  public boolean willComponentTypeBeInsideBoardTypeOutlineAfterCoordinate(BoardCoordinate boardCoordinate)
  {
    Assert.expect(boardCoordinate != null);

    // we will temporarily move the component to see if it falls outside the board outline.
    // suppress sending any ProjectStateEvents that might occur because of this since we will
    // immediatly return the component back to its original position
    getSideBoardType().getBoardType().getPanel().getProject().getProjectState().disableProjectStateEvents();
    boolean compWithinBoard = false;
    try
    {
      BoardCoordinate origBoardCoordinate = getCoordinateInNanoMeters();
      setCoordinateInNanoMeters(boardCoordinate);
      compWithinBoard = isComponentTypeWithinBoardTypeOutline();
      setCoordinateInNanoMeters(origBoardCoordinate);
    }
    finally
    {
      getSideBoardType().getBoardType().getPanel().getProject().getProjectState().enableProjectStateEvents();
    }

    return compWithinBoard;
  }

  /**
   * @return false if the process of rotating this component to the new degrees passed in will cause it
   * to move off the board.
   * @author George Booth
   */
  public boolean willComponentTypeBeInsideBoardTypeOutlineAfterRotation(double degreesRotationRelativeToBoard)
  {
    // we will temporarily rotate the component to see if it falls outside the board outline.
    // suppress sending any ProjectStateEvents that might occur because of this since we will
    // immediatly return the component back to its original orientation
    getSideBoardType().getBoardType().getPanel().getProject().getProjectState().disableProjectStateEvents();
    boolean compWithinBoard = false;
    try
    {
      double origDegreesRotationRelativeToBoard = getDegreesRotationRelativeToBoard();
      setDegreesRotationRelativeToBoard(degreesRotationRelativeToBoard);
      compWithinBoard = isComponentTypeWithinBoardTypeOutline();
      setDegreesRotationRelativeToBoard(origDegreesRotationRelativeToBoard);
    }
    finally
    {
      getSideBoardType().getBoardType().getPanel().getProject().getProjectState().enableProjectStateEvents();
    }

    return compWithinBoard;
  }

  /**
   * @author Bill Darbie
   */
  void invalidateShape()
  {
    if (_waitingForShapeCreatedBooleanLock)
      return;
  
    _shape = null;
    _shapeSurroundingPads = null;
    _shapeBoundedByPads = null;
    _padArea = null;
    for (Component component : getComponents())
      component.invalidateShape();
  }

  /**
   * @author Bill Darbie
   */
  void invalidateShapeForPadChangeOnly()
  {
    if (_waitingForShapeCreatedBooleanLock)
      return;
   
    _shape = null;
    _shapeSurroundingPads = null;
    _shapeBoundedByPads = null;
    _padArea = null;
    for (Component component : getComponents())
      component.invalidateShapeForPadChangeOnly();
  }

  /**
   * @return the Component's shape, after rotating the component and shifting it relative to its Boards lower
   * left corner.
   * @author Bill Darbie
   */
  public java.awt.Shape getShapeInNanoMeters()
  {
    createShapesIfNecessary();
    return _shape;
  }

  /**
   * @author Matt Wharton
   */
  public java.awt.Shape getShapeBoundedByPadsInNanoMeters()
  {
    createShapesIfNecessary();
    return _shapeBoundedByPads;
  }

  /**
   * @author Bill Darbie
   */
  public java.awt.Shape getShapeSurroundingPadsInNanoMeters()
  {
    createShapesIfNecessary();
    return _shapeSurroundingPads;
  }

  /**
   * this is the actual area taken up by the pads which is not to be confused
   * by the bounds of the pads. The area will know where the empty space is.
   * This is what I need. George A. David
   * @author George A. David
   */
  public java.awt.geom.Area getPadAreaInNanoMeters()
  {
    createShapesIfNecessary();
    return _padArea;
  }

  /**
   * @author Bill Darbie
   */
  private void createShapesIfNecessary()
  {
    if (_shape == null ||
        _shapeSurroundingPads == null ||
        _padArea == null ||
        _shapeBoundedByPads == null)
    {
      _waitingForShapeCreatedBooleanLock = true;
      // The componentCoordinate X and Y are the reference point of the component.  Not necessarily
      // the center OR the lower left. First, we calculate the size of the component, based on pad extents,
      // then figure out the lower left coordinate of the component. Then, we create a shape (rectangle)
      // based on lower left coordinate, and the width and length
      // Then, rotate the shape about the componentCoordinate X and Y
      // Then apply the Board and Panel rotation and transformations
      BoardCoordinate coord = getCoordinateInNanoMeters();
      int componentX = coord.getX();
      int componentY = coord.getY();
      int widthInNanoMeters = getWidthInNanoMeters();
      int lengthInNanoMeters = getLengthInNanoMeters();

      Assert.expect(_landPattern != null);
      BoardCoordinate lowerLeftOffsets = _landPattern.getLowerLeftCoordinateInNanoMeters();
      DoubleCoordinate lowerLeftComponentLocation = new DoubleCoordinate(componentX + lowerLeftOffsets.getX(),
                                                                         componentY + lowerLeftOffsets.getY());
      
      //[XCR1603] Ying-Huan.Chu - Fixed it in setPinOrientationEnum(PinOrientationEnum pinOrientationEnum) function in PackagePin.java instead.
//      java.awt.Shape shape;
//      java.awt.Shape shapeBoundedByPads;
//      java.awt.Shape shapeSurroundingPads;
     
      // get a shape that surrounds the pads
      _shapeSurroundingPads = new Rectangle2D.Double(lowerLeftOffsets.getX(),
                                                     lowerLeftOffsets.getY(),
                                                     widthInNanoMeters,
                                                     lengthInNanoMeters);
     
      // Get a shape that is bounded by the pads.
      int maxPadWidth = 0;
      int maxPadLength = 0;
      for (LandPatternPad landPatternPad : _landPattern.getLandPatternPads())
      {
        int padWidth = landPatternPad.getWidthInNanoMeters();
        maxPadWidth = Math.max(maxPadWidth, padWidth);
        int padLength = landPatternPad.getLengthInNanoMeters();
        maxPadLength = Math.max(maxPadLength, padLength);
      }
      _shapeBoundedByPads = new Rectangle2D.Double(lowerLeftOffsets.getX() + maxPadWidth,
                                                   lowerLeftOffsets.getY() + maxPadLength,
                                                   widthInNanoMeters - (2 * maxPadWidth),
                                                   lengthInNanoMeters - (2 * maxPadLength));
      
      // Depending on the package joint type, we'll figure out whether to use the bounding box or
      // inscribed box as the default shape.
      CompPackage compPackage = getCompPackage();
      if ((compPackage.usesOneJointTypeEnum() && compPackage.getJointTypeEnum().equals(JointTypeEnum.GULLWING)))
      {
        _shape = _shapeBoundedByPads;
//        shape = shapeBoundedByPads;
      }
      else
      {
        _shape = _shapeSurroundingPads;
//        shape = shapeSurroundingPads;
      }
   
      // calculate the are of the pad types
      for(PadType padType : getPadTypes())
      {
        if(_padArea == null)
          _padArea = new Area(padType.getShapeInNanoMeters());
        else
          _padArea.add(new Area(padType.getShapeInNanoMeters()));
      }

      // rotate by the ComponentTypes rotation
      AffineTransform transform = AffineTransform.getTranslateInstance(0, 0);
      if (_sideBoardType.isSideBoardType1() == false)
      {
        // do a left-to-right flip before the rotation to get the shapes relative to the top sides
        // bottom left coordinate instead of the top sides bottom right coordinate
        transform.preConcatenate(AffineTransform.getScaleInstance(-1, 1));
      }
      transform.preConcatenate(AffineTransform.getTranslateInstance(componentX, componentY));

      double degrees = getDegreesRotationRelativeToBoard();

      if (isOnSideBoard1() == false)
      {
        // this component is on side board 2
        degrees = -degrees;
      }

      transform.preConcatenate(AffineTransform.getRotateInstance(Math.toRadians(degrees),
                                                                componentX,
                                                                componentY));

      
      //Added by Jack XCR1603 - sometimes the _shape will be null suspect due to transient, hence need to use local variable to assign to make sure it is not null.
      //[XCR1603] Ying-Huan.Chu - Fixed it in setPinOrientationEnum(PinOrientationEnum pinOrientationEnum) function in PackagePin.java instead.
//      if (_shape == null)
//      {
//        _shape = shape;
//      }
            
      _shape = transform.createTransformedShape(_shape);
      _shapeBoundedByPads = transform.createTransformedShape(_shapeBoundedByPads);
      _shapeSurroundingPads = transform.createTransformedShape(_shapeSurroundingPads);
      _padArea = new Area(transform.createTransformedShape(_padArea));    
    }
    
    Assert.expect(_shape != null);
    Assert.expect(_shapeBoundedByPads != null);
    Assert.expect(_shapeSurroundingPads != null);
    Assert.expect(_padArea != null);
    _waitingForShapeCreatedBooleanLock = false;
  }

  /**
   * Take the transform passed in and modify it to take into account this ComponentTypes rotation and where
   * it is relative to the Boards bottom left corner.
   *
   * @author Bill Darbie
   */
  void preConcatenateShapeTransform(AffineTransform transform,
                                    boolean applyComponentAndBoardOriginTranslation,
                                    boolean applyComponentRotation,
                                    boolean ignoreBoardSide)
  {
    Assert.expect(transform != null);

    BoardCoordinate coord = getCoordinateInNanoMeters();
    double degrees = getDegreesRotationRelativeToBoard();
    if (ignoreBoardSide == false)
    {
      if ((isOnSideBoard1() == false))
      {
        // do a left-to-right flip before the rotation to get the shapes relative to the top sides
        // bottom left coordinate instead of the top sides bottom right coordinate
        transform.preConcatenate(AffineTransform.getScaleInstance( -1, 1));
        degrees = -degrees;
      }
    }
    if (applyComponentRotation)
      transform.preConcatenate(AffineTransform.getRotateInstance(Math.toRadians(degrees)));

    if (applyComponentAndBoardOriginTranslation)
    {
      // now translate by the ComponentTypes x,y coordinate so things are relative to the Boards bottom left corner
      transform.preConcatenate(AffineTransform.getTranslateInstance(coord.getX(), coord.getY()));
    }
  }

  /**
   * Get Artifact Compensation Setting only works if there is one.
   *
   * @author Erica Wheatcroft
   */
  public ArtifactCompensationStateEnum getArtifactCompensationSetting()
  {
    Assert.expect(usesOneArtifactCompensationSetting());

    List<PadType> padTypes = getPadTypes();
    Assert.expect((padTypes.isEmpty() == false));

    PadType padType = padTypes.get(0);
    return padType.getPadTypeSettings().getEffectiveArtifactCompensationState();
  }

  /**
   * Get Integration Level only works if there is one.
   *
   * @author Erica Wheatcroft
   */
  public SignalCompensationEnum getSignalCompensationLevel()
  {
    Assert.expect(usesOneIntegrationLevelSetting());

    List<PadType> padTypes = getPadTypes();
    Assert.expect((padTypes.isEmpty() == false));

    PadType padType = padTypes.get(0);
    return padType.getPadTypeSettings().getSignalCompensation();
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized boolean usesOneArtifactCompensationSetting()
  {
    // comment by Wei Chin
    // no more using
//    if (_usesOneArtifactCompensation != null)
//      return _usesOneArtifactCompensation.getValue();
//
//    Set<String> artifactCompensationValuesSet = new HashSet<String>();
//    for (PadType padType : getPadTypes())
//    {
//      ArtifactCompensationStateEnum artifactCompensationStateEnum =
//          padType.getPadTypeSettings().getEffectiveArtifactCompensationState();
//      artifactCompensationValuesSet.add(artifactCompensationStateEnum.toString());
//      if (artifactCompensationValuesSet.size() > 1)
//      {
//        _usesOneArtifactCompensation = new BooleanRef(false);
//        return false;
//      }
//    }
//
//    _usesOneArtifactCompensation = new BooleanRef(true);
    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public synchronized boolean usesOneIntegrationLevelSetting()
  {
    // comment by Wei Chin
    //  no more using the function Move the IL to SubtypeAdvanceSettings
//    if (_usesOneSignalCompensation != null)
//      return _usesOneSignalCompensation.getValue();
//
//    Set<String> integrationLevelSettingsSet = new HashSet<String>();
//    for (PadType padType : getPadTypes())
//    {
//      SignalCompensationEnum signalCompensationStateEnum =
//          padType.getPadTypeSettings().getSignalCompensation();
//      integrationLevelSettingsSet.add(signalCompensationStateEnum.toString());
//      if (integrationLevelSettingsSet.size() > 1)
//      {
//        _usesOneSignalCompensation = new BooleanRef(false);
//        return false;
//      }
//    }
//
//    _usesOneSignalCompensation = new BooleanRef(true);
    return true;
  }

  /**
   * @author George A. David
   * @author Wei Chin
   */
  public SignalCompensationEnum getSignalCompensation()
  {
    // comment by Wei Chin
    //  no more using the function Move the IL to SubtypeAdvanceSettings
    SignalCompensationEnum signalComp = SignalCompensationEnum.DEFAULT_LOW;

    for(Subtype subtype : getSubtypes())
    {
      signalComp = SignalCompensationEnum.getHigherSignalCompensation(signalComp, subtype.getSubtypeAdvanceSettings().getSignalCompensation());
    }
    return signalComp;
  }

  
  /**
   * @author George A. David
   * @author Wei Chin
   */
  public MagnificationTypeEnum getHigherMagnificationType()
  {
    // comment by Wei Chin
    MagnificationTypeEnum magnificationType = MagnificationTypeEnum.LOW;

    for(Subtype subtype : getSubtypes())
    {
      magnificationType = MagnificationTypeEnum.getHigherMagnificationTypeEnum(magnificationType, subtype.getSubtypeAdvanceSettings().getMagnificationType());
    }
    return magnificationType;
  }

  /**
   * @author Yee Seong
   */
  public void addInterferenceAffectedPadType(PadType padType)
  {
    Assert.expect(padType != null);

    _interferencePatternAffectedPadTypes.add(padType);
  }

  /**
   * @author Yee Seong
   */
  public List<PadType> getInterferenceAffectedPadTypes()
  {
    return _interferencePatternAffectedPadTypes;
  }

  /**
   * @return LibrarySubtypeScheme
   * @author Cheah Lee Herng
   */
  public LibrarySubtypeScheme getAssignLibrarySubtypeScheme()
  {
    Assert.expect(_assignLibrarySubtypeScheme != null);
    return _assignLibrarySubtypeScheme;
  }

  /**
   * @param librarySubtypeScheme LibrarySubtypeScheme
   * @author Cheah Lee Herng
   */
  public void setAssignLibrarySubtypeScheme(LibrarySubtypeScheme librarySubtypeScheme)
  {
    Assert.expect(librarySubtypeScheme != null);

    LibrarySubtypeScheme oldValue = _assignLibrarySubtypeScheme;

    if(librarySubtypeScheme.equals(oldValue))
      return;

    _subtypeSchemeObservable.setEnabled(false);
    try
    {
      _assignLibrarySubtypeScheme = librarySubtypeScheme;
    }
    finally
    {
      _subtypeSchemeObservable.setEnabled(true);
    }
    _subtypeSchemeObservable.stateChanged(this, oldValue, librarySubtypeScheme, SubtypeSchemeEventEnum.ASSIGNED_LIBRARY_SUBTYPE_SCHEME);
  }

  /**
   * @return boolean
   * @author Cheah Lee Herng
   */
  public boolean isAssignLibrarySubtypeSchemeExists()
  {
    return (_assignLibrarySubtypeScheme != null);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void clearAssignLibrarySubtypeScheme()
  {
    _assignLibrarySubtypeScheme = null;
  }

  /**
   * @author Cheah, Lee Herng
   */
  public GlobalSurfaceModelEnum getGlobalSurfaceModel()
  {
    List<PadType> padTypes = getPadTypes();
    Assert.expect((padTypes.isEmpty() == false));

    PadType padType = padTypes.get(0);
    return padType.getPadTypeSettings().getGlobalSurfaceModel();
  }

  /**
   * @author sham
   */
  public UserGainEnum getUserGain()
  {
    List<PadType> padTypes = getPadTypes();
    Assert.expect((padTypes.isEmpty() == false));

    PadType padType = padTypes.get(0);
    return padType.getPadTypeSettings().getUserGain();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public MagnificationTypeEnum getMagnificationType()
  {
    List<PadType> padTypes = getPadTypes();
    Assert.expect((padTypes.isEmpty() == false));

    PadType padType = padTypes.get(0);
    return padType.getPadTypeSettings().getMagnificationType();
  }

   /**
   * @author Jack Hwee
   
  public PspEnum getPsp()
  {
    List<PadType> padTypes = getPadTypes();
    Assert.expect((padTypes.isEmpty() == false));

    PadType padType = padTypes.get(0);
    return padType.getPadTypeSettings().getPsp();
  } */
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isSetToUsePspResult()
  {
      return _isSetToUsePspResult;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setToUsePspResult(boolean isSetToUsePspResult)
  {
      _isSetToUsePspResult = isSetToUsePspResult;
  }
  
  /**
   * @author Cheah Lee Herng
   * @author Rick Gaudette
   */
  public boolean useToBuildGlobalSurfaceModel()
  {
    
    List<PadType> padTypes = getPadTypes();
    Assert.expect((padTypes.isEmpty() == false));
    
    
    PadType padType = padTypes.get(0);    
    if (padType.getPadTypeSettings().hasSubtype())
    {
      Subtype subtype = padType.getPadTypeSettings().getSubtype();
      JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
      if (jointTypeEnum == JointTypeEnum.PRESSFIT || jointTypeEnum == JointTypeEnum.THROUGH_HOLE || jointTypeEnum == JointTypeEnum.OVAL_THROUGH_HOLE)
        return false;
      else
        return true;

//      if (jointTypeEnum == JointTypeEnum.CAPACITOR ||
//          jointTypeEnum == JointTypeEnum.RESISTOR ||
//          jointTypeEnum == JointTypeEnum.LEADLESS) 
//      if (jointTypeEnum == JointTypeEnum.RESISTOR)
//      {
//        return true;
//      }
//      else
//        return false;
    }
    return false;
  }
  
   /**
   * @author Bee Hoon
   */
  public void set5dxNewSubtypeName(String subtypeName)
  {
    Assert.expect(subtypeName != null);

    if (subtypeName.equals(_subtypeName))
      return;

    String oldValue = _subtypeName;
    _projectObservable.setEnabled(false);
    try
    {
      _subtypeName = subtypeName.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, subtypeName, ComponentTypeEventEnum.SUBTYPE_NAME);
  }

  /**
   * @author Bee Hoon
   */
  public String get5dxNewSubtypeName()
  {
    Assert.expect(_subtypeName != null);

    return _subtypeName;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public POPLayerIdEnum getPOPLayerId()
  {
    Assert.expect(_popLayerId != null);
    
    return _popLayerId;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void setPOPLayerId(POPLayerIdEnum popLayerId)
  {
    Assert.expect(popLayerId != null);

    if (popLayerId.equals(_popLayerId))
      return;

    POPLayerIdEnum oldValue = _popLayerId;
    _projectObservable.setEnabled(false);

    _popLayerId = popLayerId;

    _projectObservable.setEnabled(true);
    _projectObservable.stateChanged(this, oldValue, popLayerId, ComponentTypeEventEnum.POP_LAYER_ID);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public CompPackageOnPackage getCompPackageOnPackage()
  {    
    Assert.expect(_compPackageOnPackage != null);
    
    return _compPackageOnPackage;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public boolean hasCompPackageOnPackage()
  {
    if (_compPackageOnPackage != null) 
      return true;
    
    return false;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void setCompPackageOnPackage(CompPackageOnPackage compPackageOnPackage)
  {
    if (compPackageOnPackage == _compPackageOnPackage)
      return;

    CompPackageOnPackage oldValue = _compPackageOnPackage;
    _projectObservable.setEnabled(false);
    try
    {
      _compPackageOnPackage = compPackageOnPackage;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, compPackageOnPackage, ComponentTypeEventEnum.COMP_PACKAGE_ON_PACKAGE);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void removeCompPackageOnPackage()
  {
    if (_compPackageOnPackage == null)
      return;

    CompPackageOnPackage oldValue = _compPackageOnPackage;
    _projectObservable.setEnabled(false);
    try
    {
      _compPackageOnPackage = null;
      this.setPOPZHeightInNanometers(0);
      this.setPOPLayerId(POPLayerIdEnum.BASE);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, null, ComponentTypeEventEnum.COMP_PACKAGE_ON_PACKAGE);
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void setPOPZHeightInNanometers(int popZHeightInNanometers)
  {
    if (popZHeightInNanometers == _popZHeightInNanometers)
      return;

    int oldValue = _popZHeightInNanometers;
    _projectObservable.setEnabled(false);
    try
    {
      _popZHeightInNanometers = popZHeightInNanometers;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, popZHeightInNanometers, ComponentTypeEventEnum.POP_ZHEIGHT);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public int getPOPZHeightInNanometers()
  {    
    return _popZHeightInNanometers;
  }
}
