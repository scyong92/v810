package com.axi.v810.business.panelDesc;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Keith Lee
 */
public class Test_ThroughHoleLandPatternPad extends UnitTest
{
  /**
   * @author Keith Lee
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ThroughHoleLandPatternPad());
  }

  /**
   * @author Keith Lee
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    ThroughHoleLandPatternPad throughHoleLandPatternPad = new ThroughHoleLandPatternPad();

    //hole coordinate
    try
    {
      throughHoleLandPatternPad.getHoleCoordinateInNanoMeters();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    try
    {
      throughHoleLandPatternPad.setHoleCoordinateInNanoMeters(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    ComponentCoordinate holeCoordinate = new ComponentCoordinate(5,6);
    throughHoleLandPatternPad.setHoleCoordinateInNanoMeters(holeCoordinate);
    Expect.expect(throughHoleLandPatternPad.getHoleCoordinateInNanoMeters().equals(holeCoordinate));

    //hole diameter
    try
    {
      throughHoleLandPatternPad.getHoleWidthInNanoMeters();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    try
    {
      throughHoleLandPatternPad.setHoleWidthInNanoMeters(0);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    try
    {
      throughHoleLandPatternPad.setHoleWidthInNanoMeters(-3);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    
    //Siew Yeng - XCR-3318 - Oval PTH
    try
    {
      throughHoleLandPatternPad.getHoleLengthInNanoMeters();
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    try
    {
      throughHoleLandPatternPad.setHoleLengthInNanoMeters(0);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    try
    {
      throughHoleLandPatternPad.setHoleLengthInNanoMeters(-3);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    Panel panel = new Panel();
    LandPattern landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("landPatternName1");
    throughHoleLandPatternPad.setLandPattern(landPattern);
    int holeDiameter = 7;
    throughHoleLandPatternPad.setWidthInNanoMeters(10);
    throughHoleLandPatternPad.setLengthInNanoMeters(10);
    throughHoleLandPatternPad.setHoleWidthInNanoMeters(holeDiameter);
    Expect.expect(throughHoleLandPatternPad.getHoleWidthInNanoMeters() == holeDiameter);
    throughHoleLandPatternPad.setHoleLengthInNanoMeters(holeDiameter);
    Expect.expect(throughHoleLandPatternPad.getHoleLengthInNanoMeters() == holeDiameter);

    // check createDuplicate
    throughHoleLandPatternPad.setCoordinateInNanoMeters(new ComponentCoordinate(10,20));
    throughHoleLandPatternPad.setDegreesRotation(0);
    throughHoleLandPatternPad.setShapeEnum(ShapeEnum.CIRCLE);
    throughHoleLandPatternPad.setName("1");

    landPattern = new LandPattern();
    landPattern.setPanel(panel);
    landPattern.setName("landPatternName2");

    ThroughHoleLandPatternPad thLandPatternPadDup = throughHoleLandPatternPad.createDuplicate(landPattern);
    Assert.expect(thLandPatternPadDup.getLandPattern() == landPattern);
    Assert.expect(thLandPatternPadDup.getCoordinateInNanoMeters().equals(throughHoleLandPatternPad.getCoordinateInNanoMeters()));
    Assert.expect(thLandPatternPadDup.getDegreesRotation() == throughHoleLandPatternPad.getDegreesRotation());
    Assert.expect(thLandPatternPadDup.getInterPadDistanceInNanoMeters() == throughHoleLandPatternPad.getInterPadDistanceInNanoMeters());
    Assert.expect(thLandPatternPadDup.getLengthInNanoMeters() == throughHoleLandPatternPad.getLengthInNanoMeters());
    Assert.expect(thLandPatternPadDup.getWidthInNanoMeters() == throughHoleLandPatternPad.getWidthInNanoMeters());
    Assert.expect(thLandPatternPadDup.getName().equals(throughHoleLandPatternPad.getName()));
    Assert.expect(thLandPatternPadDup.isPadOne() == throughHoleLandPatternPad.isPadOne());
    Assert.expect(thLandPatternPadDup.isThroughHolePad());
    Assert.expect(thLandPatternPadDup.getHoleCoordinateInNanoMeters().equals(throughHoleLandPatternPad.getHoleCoordinateInNanoMeters()));
    Assert.expect(thLandPatternPadDup.getHoleWidthInNanoMeters()== throughHoleLandPatternPad.getHoleWidthInNanoMeters());
    Assert.expect(thLandPatternPadDup.getHoleLengthInNanoMeters()== throughHoleLandPatternPad.getHoleLengthInNanoMeters());

  }
}
