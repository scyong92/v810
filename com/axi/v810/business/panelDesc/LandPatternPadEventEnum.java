package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class LandPatternPadEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static LandPatternPadEventEnum CREATE_OR_DESTROY = new LandPatternPadEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static LandPatternPadEventEnum ADD_OR_REMOVE = new LandPatternPadEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static LandPatternPadEventEnum LAND_PATTERN = new LandPatternPadEventEnum(++_index);
  public static LandPatternPadEventEnum NAME = new LandPatternPadEventEnum(++_index);
  public static LandPatternPadEventEnum COORDINATE = new LandPatternPadEventEnum(++_index);
  public static LandPatternPadEventEnum WIDTH = new LandPatternPadEventEnum(++_index);
  public static LandPatternPadEventEnum LENGTH = new LandPatternPadEventEnum(++_index);
  public static LandPatternPadEventEnum SHAPE_ENUM = new LandPatternPadEventEnum(++_index);
  public static LandPatternPadEventEnum DEGREES_ROTATION = new LandPatternPadEventEnum(++_index);
  public static LandPatternPadEventEnum PITCH = new LandPatternPadEventEnum(++_index);
  public static LandPatternPadEventEnum INTERPAD_DISTANCE = new LandPatternPadEventEnum(++_index);
  public static LandPatternPadEventEnum PAD_ORIENTATION = new LandPatternPadEventEnum(++_index);
  public static LandPatternPadEventEnum PAD_ONE = new LandPatternPadEventEnum(++_index);
  public static LandPatternPadEventEnum ADD_OR_REMOVE_LIST = new LandPatternPadEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);

  /**
   * @author Bill Darbie
   */
  private LandPatternPadEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private LandPatternPadEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
