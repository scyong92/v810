package com.axi.v810.business.panelDesc;

import java.io.*;

import com.axi.util.*;

/**
 * This enumeration distinguished between something that is surface mount, through
 * hole, or a combination of the two.
 *
 * @author Bill Darbie
 */
public class PadTypeEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;
  public static final PadTypeEnum SURFACE_MOUNT = new PadTypeEnum(++_index);
  public static final PadTypeEnum THROUGH_HOLE = new PadTypeEnum(++_index);
  public static final PadTypeEnum SURFACE_MOUNT_AND_THROUGH_HOLE = new PadTypeEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private PadTypeEnum(int index)
  {
    super(index);
  }
}
