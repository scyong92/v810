package com.axi.v810.business.panelDesc;

import java.awt.geom.*;
import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

/**
 * There is one Fiducial instance for each Fiducial on a Panel or Board.
 *
 * @author Bill Darbie
 */
public class Fiducial implements Serializable
{
  // populated by PanelNdfReader, PanelReader

  // a Fiducial is either on a SideBoard or a Panel
  // one of the two variables will be set below depending on which type of Fiducial it is.
  private SideBoard _sideBoard; // PanelReader
  private Panel _panel; // PanelNdfReader, PanelReader

  private BooleanRef _panelSide1; // applies for Panel Fiducial only // PanelNdfReader, PanelReader

  private FiducialType _fiducialType; // PanelNdfReader, PanelReader

  private static transient ProjectObservable _projectObservable;
  private static transient Alignment _alignment;

  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
    _alignment = Alignment.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public Fiducial()
  {
    _projectObservable.stateChanged(this, null, this, FiducialEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      removeFiducialFromPreviousBoardOrPanel();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }

    _projectObservable.stateChanged(this, this, null, FiducialEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void remove()
  {
    _projectObservable.setEnabled(false);
    try
    {
      Assert.expect(isOnPanel());
      getPanel().removeFiducial(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, FiducialEventEnum.ADD_OR_REMOVE);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void add()
  {
    _projectObservable.setEnabled(false);
    try
    {
      Assert.expect(isOnPanel());
      getPanel().addFiducial(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, this, FiducialEventEnum.ADD_OR_REMOVE);
  }

  /**
   * @author Bill Darbie
   */
  private void removeFiducialFromPreviousBoardOrPanel()
  {
    if (_sideBoard != null)
      _sideBoard.removeFiducial(this);

    if (_panel != null)
      _panel.removeFiducial(this);
  }

  /**
   * @author Bill Darbie
   */
  public void setSideBoard(SideBoard sideBoard)
  {
    // null boardType is allowed
    if (_sideBoard != null)
      Assert.expect(_panel == null);

    if (sideBoard == _sideBoard)
      return;

    SideBoard oldValue = _sideBoard;
    _projectObservable.setEnabled(false);
    try
    {
      removeFiducialFromPreviousBoardOrPanel();

      // don't call setNewFiducialName the first time this is called since
      // getName() will not be set yet
      if (_sideBoard != null)
      {
        Board board = sideBoard.getBoard();
        board.setNewFiducialName(this, sideBoard, null, getName());
      }

      _sideBoard = sideBoard;

    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, sideBoard, FiducialEventEnum.SIDE_BOARD);
  }

  /**
   * @author Bill Darbie
   */
  public void setPanel(Panel panel, boolean panelSide1)
  {
    // null panel is allowed
    if (panel != null)
      Assert.expect(_sideBoard == null);

    if ((panel == _panel) && (_panelSide1 != null) && (panelSide1 == _panelSide1.getValue()))
      return;

    BooleanRef oldValue = _panelSide1;
    _projectObservable.setEnabled(false);
    try
    {
      removeFiducialFromPreviousBoardOrPanel();

      // don't call setNewFiducialName the first time this is called since
      // getName() will not be set yet
      if (_panel != null)
        _panel.setNewFiducialName(this, null, getName());

      _panel = panel;

      setPanelSide1(panelSide1);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, panelSide1, FiducialEventEnum.PANEL);
  }

  /**
   * @author Bill Darbie
   */
  private void setPanelSide1(boolean panelSide1)
  {
    BooleanRef oldValue = null;
    if (_panelSide1 != null)
      oldValue = new BooleanRef(_panelSide1);
    _projectObservable.setEnabled(false);
    try
    {
      if (_panelSide1 == null)
        _panelSide1 = new BooleanRef(panelSide1);
      else
        _panelSide1.setValue(panelSide1);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, _panelSide1, FiducialEventEnum.PANEL_SIDE1);
  }

  /**
   *
   * @return true if this Fiducial is on a Board.
   * @author Bill Darbie
   */
  public boolean isOnBoard()
  {
    Assert.expect(((_sideBoard == null) && (_panel != null)) || ((_sideBoard != null) && (_panel == null)));

    if (_sideBoard != null)
      return true;

    return false;
  }

  /**
   * @return true if this fiducial is on a Panel.
   * @author Bill Darbie
   */
  public boolean isOnPanel()
  {
    Assert.expect(((_sideBoard == null) && (_panel != null)) || ((_sideBoard != null) && (_panel == null)));

    if (_panel != null)
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public Panel getPanel()
  {
    Assert.expect(_panel != null);

    return _panel;
  }

  /**
   * @author Bill Darbie
   */
  public SideBoard getSideBoard()
  {
    Assert.expect(_sideBoard != null);

    return _sideBoard;
  }

  /**
   * @author Bill Darbie
   */
  public void setFiducialType(FiducialType fiducialType)
  {
    Assert.expect(fiducialType != null);

    if (fiducialType == _fiducialType)
      return;

    FiducialType oldValue = _fiducialType;
    _projectObservable.setEnabled(false);
    try
    {
      _fiducialType = fiducialType;
      _fiducialType.addFiducial(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, fiducialType, FiducialEventEnum.FIDUCIAL_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public FiducialType getFiducialType()
  {
    Assert.expect(_fiducialType != null);

    return _fiducialType;
  }

  /**
   * @author Bill Darbie
   */
  public String getName()
  {
    Assert.expect(_fiducialType != null);

    return _fiducialType.getName();
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author Bill Darbie
   */
  public PanelCoordinate getPanelCoordinateInNanoMeters()
  {
    Assert.expect(_fiducialType != null);
    PanelCoordinate panelCoord = null;

    if (isOnPanel())
      panelCoord = _fiducialType.getCoordinateInNanoMeters();
    else
      panelCoord = getPanelCoordinateRelativeToPanelInNanoMeters();

    Assert.expect(panelCoord != null);
    return panelCoord;
  }

  /**
   * @author Bill Darbie
   */
  public BoardCoordinate getBoardCoordinateInNanoMeters()
  {
    Assert.expect(_fiducialType != null);
    Assert.expect(isOnBoard());

    PanelCoordinate coord = _fiducialType.getCoordinateInNanoMeters();
    return new BoardCoordinate(coord.getX(), coord.getY());
  }

  /**
   * @author Bill Darbie
   */
  public int getWidthInNanoMeters()
  {
    Assert.expect(_fiducialType != null);

    return _fiducialType.getWidthInNanoMeters();
  }

  /**
   * @return the x axis width of the Pad AFTER the Pad, Component, Board and Panel have been rotated.
   * @author Bill Darbie
   */
  public int getWidthAfterAllRotationsInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    java.awt.Rectangle bounds = shape.getBounds();

    return bounds.width;
  }


  /**
   * @author Bill Darbie
   */
  public int getLengthInNanoMeters()
  {
    Assert.expect(_fiducialType != null);

    return _fiducialType.getLengthInNanoMeters();
  }

  /**
   * @return the y axis length of the Pad AFTER the Pad, Component, Board and Panel have been rotated.
   * @author Bill Darbie
   */
  public int getLengthAfterAllRotationsInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();
    java.awt.Rectangle bounds = shape.getBounds();

    return bounds.height;
  }


  /**
   * @author Bill Darbie
   */
  public ShapeEnum getShapeEnum()
  {
    Assert.expect(_fiducialType != null);

    return _fiducialType.getShapeEnum();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isPanelSide1()
  {
    Assert.expect(_panelSide1 != null);
    return _panelSide1.getValue();
  }

  /**
   * @return true if this Fiducial is on the same side as the Panels top side (if
   * the Fiducial is on the Panel), or the Boards top side (if the Fiducial is
   * on a Board).
   * @author Bill Darbie
   */
  public boolean isTopSide()
  {
    Assert.expect(((_sideBoard == null) && (_panel != null)) || ((_sideBoard != null) && (_panel == null)));

    boolean isTopSide = false;
    if (_sideBoard != null)
      isTopSide = _sideBoard.isTopSide();
    else if (_panel != null)
    {
      isTopSide = _panelSide1.getValue();
      if (_panel.isFlipped())
        isTopSide = !isTopSide;
    }
    else
      Assert.expect(false);

    return isTopSide;
  }

  /**
   * @return true if this Fiducial is on the same side as the Panels bottom side (if
   * the Fiducial is on the Panel), or the Boards bottom side (if the Fiducial is
   * on a Board).
   * @author Bill Darbie
   */
  public boolean isBottomSide()
  {
    return !isTopSide();
  }

  /**
   * @return the Shape not related to any board or panel setting.
   * @author Bill Darbie
   */
  public java.awt.Shape getShapeInNanoMeters()
  {
    Assert.expect(_fiducialType != null);

    return _fiducialType.getShapeInNanoMeters();
  }

  /**
   * @return the coordinate of the fiducial in the panel relative to the lower left corner of the panel
   * after all the transformations have been applied. It relies on the getShapeRelativeToPanelInNanoMeters method
   * @author Laura Cormos
   */
  private PanelCoordinate getPanelCoordinateRelativeToPanelInNanoMeters()
  {
    // get the Shape
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();

    Rectangle2D bounds = shape.getBounds2D();
    double x = Math.round(bounds.getX());
    double y = Math.round(bounds.getY());
    if (x > Integer.MAX_VALUE)
      x = Integer.MAX_VALUE;
    if (x < Integer.MIN_VALUE)
      x = Integer.MIN_VALUE;
    if (y > Integer.MAX_VALUE)
      y = Integer.MAX_VALUE;
    if (y < Integer.MIN_VALUE)
      y = Integer.MIN_VALUE;

    return new PanelCoordinate((int)x, (int)y);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isFiducialWithinPanelOrBoardOutline()
  {
    java.awt.Shape fiducialShape = getShapeRelativeToPanelInNanoMeters();

    boolean withinPanelOrBoard = false;
    if (isOnBoard())
    {
      java.awt.Shape boardShape = getSideBoard().getBoard().getShapeRelativeToPanelInNanoMeters();
      // allow 500 nanometers for rounding errors
      withinPanelOrBoard = MathUtil.fuzzyContains(boardShape, fiducialShape, 500);
    }
    else if (isOnPanel())
    {
      java.awt.Shape panelShape = _panel.getShapeInNanoMeters();
      // allow 500 nanometers for rounding errors
      withinPanelOrBoard =MathUtil.fuzzyContains(panelShape, fiducialShape, 500);
    }
    else
      Assert.expect(false);

    return withinPanelOrBoard;
  }

  /**
   * @return false if the process of moving this Fiducial by the offset passed in will cause the Fiducial to
   * move off the board or Panel that it is on.
   * @author Bill Darbie
   */
  public boolean willFiducialBeInsideBoardOrPanelOutlineAfterOffset(int xOffsetAfterAllFlipsAndRotationsInNanoMeters, int yOffsetAfterAllFlipsAndRotationsInNanometers)
  {
    // we will temporarily move the component to see if it falls outside the board outline.
    // suppress sending any ProjectStateEvents that might occur because of this since we will
    // immediatly return the component back to its original position
    getSideBoard().getBoard().getPanel().getProject().getProjectState().disableProjectStateEvents();
    boolean fiducialWithinPanelOrBoard = false;
    try
    {
      moveByOffSetInNanoMeters(xOffsetAfterAllFlipsAndRotationsInNanoMeters,
                               yOffsetAfterAllFlipsAndRotationsInNanometers);
      fiducialWithinPanelOrBoard = isFiducialWithinPanelOrBoardOutline();
      moveByOffSetInNanoMeters( -xOffsetAfterAllFlipsAndRotationsInNanoMeters,
                               -yOffsetAfterAllFlipsAndRotationsInNanometers);
    }
    finally
    {
      getSideBoard().getBoard().getPanel().getProject().getProjectState().enableProjectStateEvents();
    }

    return fiducialWithinPanelOrBoard;
  }

  /**
   * Move this Fiducial's FiducialType by the offset passed in.  The offset passed in is an offset of the
   * Fiducial as viewed from x-rays after all rotations and flips have occured.
   *
   * @author Bill Darbie
   */
  public void moveByOffSetInNanoMeters(int xOffsetAfterAllFlipsAndRotationsInNanoMeters, int yOffsetAfterAllFlipsAndRotationsInNanometers)
  {
    // create the correct transform
    AffineTransform transform = AffineTransform.getTranslateInstance(0, 0);
    preConcatenateShapeTransform(transform);

    // use the inverse of that transform to figure out the correct offset
    Point2D sourcePoint = new Point2D.Double(xOffsetAfterAllFlipsAndRotationsInNanoMeters, yOffsetAfterAllFlipsAndRotationsInNanometers);
    Point2D destPoint = new Point2D.Double(0, 0);
    try
    {
      transform.inverseTransform(sourcePoint, destPoint);
    }
    catch (NoninvertibleTransformException nte)
    {
      Assert.expect(false);
    }

    int xOffset = MathUtil.roundDoubleToInt(destPoint.getX());
    int yOffset = MathUtil.roundDoubleToInt(destPoint.getY());

    // apply the offset to the FiducialType
    FiducialType fiducialType = getFiducialType();
    PanelCoordinate panelCoord = fiducialType.getCoordinateInNanoMeters();
    panelCoord.setX(panelCoord.getX() + xOffset);
    panelCoord.setY(panelCoord.getY() + yOffset);
    fiducialType.setCoordinateInNanoMeters(panelCoord);
  }


  /**
   * @return the Fiducial shape after board and panel transformations (rotations, etc) have been
   * applied to it.  The returned shape is the final, viewable shape.
   * @author Bill Darbie
   */
  public java.awt.Shape getShapeRelativeToPanelInNanoMeters()
  {
    // get the Shape
    java.awt.Shape shape = getShapeInNanoMeters();

    // get the correct transform
    AffineTransform trans = AffineTransform.getTranslateInstance(0, 0);

    preConcatenateShapeTransform(trans);

    // apply the transform
    shape = trans.createTransformedShape(shape);

    Assert.expect(shape != null);
    return shape;
  }

  /**
   * @author Bill Darbie
   */
  private void preConcatenateShapeTransform(AffineTransform transform)
  {
    Assert.expect(transform != null);

    Assert.expect(((_sideBoard == null) && (_panel != null)) || ((_sideBoard != null) && (_panel == null)));
    if (_sideBoard != null)
      _sideBoard.getBoard().preConcatenateShapeTransform(transform);
    else if (_panel != null)
      _panel.preConcatenateShapeTransform(transform);
    else
      Assert.expect(false);
  }

  /**
   * Returns a shape relative to the Panel's lower left, but taking
   * into account the alignment matrix.
   * @author George A. David
   */
  public java.awt.Shape getAlignedShapeRelativeToPanelInNanoMeters()
  {
    java.awt.Shape shape = getShapeRelativeToPanelInNanoMeters();

    Panel panel = null;
    if(_sideBoard != null)
      panel = _sideBoard.getBoard().getPanel();
    else if(_panel != null)
      panel = _panel;
    else
      Assert.expect(false);

    if(panel.getPanelSettings().isPanelBasedAlignment())
    {
      if (panel.getPanelSettings().getRightSectionOfLongPanel().containsShape(shape))
        shape = _alignment.getCorrectedPositionInPanelCoordinates(shape, panel.getProject().getTestProgram().getTestSubProgram(PanelLocationInSystemEnum.RIGHT));
      else
        shape = _alignment.getCorrectedPositionInPanelCoordinates(shape, panel.getProject().getTestProgram().getTestSubProgram(PanelLocationInSystemEnum.LEFT));
    }
    else
    {
      if(_sideBoard.getBoard().getBoardSettings().isLongBoard())
      {
        if (_sideBoard.getBoard().getRightSectionOfLongBoard().containsShape(shape))
          shape = _alignment.getCorrectedPositionInPanelCoordinates(shape, panel.getProject().getTestProgram().getTestSubProgram(_sideBoard.getBoard(), PanelLocationInSystemEnum.RIGHT));
        else
          shape = _alignment.getCorrectedPositionInPanelCoordinates(shape, panel.getProject().getTestProgram().getTestSubProgram(_sideBoard.getBoard(), PanelLocationInSystemEnum.LEFT));
      }
      else
        shape = _alignment.getCorrectedPositionInPanelCoordinates(shape, panel.getProject().getTestProgram().getTestSubProgram(_sideBoard.getBoard()));
    }

    return shape;
  }

   /**
   * @author Laura Cormos
   */
  public boolean isFiducialWithinPanelOutline()
  {
    java.awt.Shape panelShape = getPanel().getShapeInNanoMeters();
    java.awt.Shape fiducialShape = getShapeRelativeToPanelInNanoMeters();

    boolean within = MathUtil.fuzzyContains(panelShape, fiducialShape, 500);
    return within;
  }
}
