package com.axi.v810.business.panelDesc;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Andy Mechtenberg
 */
class Test_Component extends UnitTest
{
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Component());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testGeneric();
  }

  /**
   * @author Bill Darbie
   */
  private void testGeneric()
  {
    Component component = new Component();
    ComponentType componentType = new ComponentType();
    component.setComponentType(componentType);

    // test setReferenceDesignator() and getReferenceDesignator()
    try
    {
      component.getReferenceDesignator();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    // test setSideBoard() and getSideBoard()
    try
    {
      component.getSideBoard();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    try
    {
      component.setSideBoard(null);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }
    SideBoard sideBoard = new SideBoard();
    component.setSideBoard(sideBoard);
    Expect.expect(component.getSideBoard() == sideBoard);

    // test setCompPackage() and getCompPackage()
    try
    {
      component.getCompPackage();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    // test addPad(Pad pad) and getPads() and the hasPad / getPad methods
    List<Pad> pads = component.getPads();
    Expect.expect(pads.size() == 0);
    try
    {
      component.addPad(null);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }


    LandPattern landPattern = new LandPattern();
    landPattern.setPanel(new Panel());
    LandPatternPad landPatternPad = new SurfaceMountLandPatternPad();
    landPatternPad.setLandPattern(landPattern);
    landPatternPad.setName("1");
    Pad pad = new Pad();
    PadType padType = new PadType();
    padType.setLandPatternPad(landPatternPad);
    pad.setPadType(padType);
    component.addPad(pad);
    Expect.expect(component.getPads().get(0) == pad);

    // test hasPad(legacyNumber) and getPad(legacyNumber)
    Panel panel = new Panel();
    panel.disableChecksForNdfParsers();
    component = new Component();
    componentType = new ComponentType();
    component.setComponentType(componentType);
    pad = new Pad();
    padType = new PadType();
    pad.setPadType(padType);
    landPattern.setPanel(panel);
    landPatternPad.setLandPattern(landPattern);
    padType.setLandPatternPad(landPatternPad);
    String legacyPadId = "15";
    pad.getLandPatternPad().setName(legacyPadId);
    component.addPad(pad);
    Expect.expect(component.hasPad(legacyPadId) == true);
    Expect.expect(component.getPad(legacyPadId) == pad);
    try
    {
      component.getPad(legacyPadId + 1);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    pad = new Pad();
    padType = new PadType();
    pad.setPadType(padType);
    landPatternPad = new SurfaceMountLandPatternPad();
    landPatternPad.setLandPattern(landPattern);
    padType.setLandPatternPad(landPatternPad);
    String padName = "padName";
    pad.getLandPatternPad().setName(padName);
    component.addPad(pad);
    Expect.expect(component.hasPad(padName) == true);

    Expect.expect(component.getPad(padName) == pad);

    component = new Component();
    component.setComponentType(new ComponentType());
    // test getPads
    Expect.expect(component.getPads().size() == 0);
    try
    {
      component.addPad(null);
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    pad = new Pad();
    pad.setPadType(padType);
    component.addPad(pad);
    Expect.expect(component.getPads().get(0) == pad);

    // test setLandPattern() getLandPattern()
    try
    {
      component.getLandPattern();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    // test getComponentTypeSettings()
    try
    {
      component.getComponentTypeSettings();
      Expect.expect(false);
    }
    catch(AssertException ae)
    {
      // do nothing
    }

    // test isInspected()
    // if the component is marked as tested, each pad is checked to make sure it also is being inspected
    // the pads are checked by making sure at least one algorithm is enabled on the Joint
    // to test for that, we need to set up two pads
    component = new Component();
    sideBoard = new SideBoard();
    Board board = new Board();
    sideBoard.setBoard(board);
    BoardSettings boardSettings = new BoardSettings(board);
    boardSettings.setInspected(true);
    board.setBoardSettings(boardSettings);
    component.setSideBoard(sideBoard);
    componentType = new ComponentType();
    component.setComponentType(componentType);
    ComponentTypeSettings componentTypeSettings = new ComponentTypeSettings();
    componentType.setComponentTypeSettings(componentTypeSettings);

    //pad 1
    padType = new PadType();
    PackagePin packagePin = new PackagePin();
    padType.setPackagePin(packagePin);
    landPatternPad = new SurfaceMountLandPatternPad();
    landPatternPad.setLandPattern(landPattern);
    landPatternPad.setName("1");
    padType.setLandPatternPad(landPatternPad);
    pad.setPadType(padType);
    PadTypeSettings padTypeSettings = new PadTypeSettings();
    padType.setPadTypeSettings(padTypeSettings);

    PadTypeSettings padTypeSettings2 = new PadTypeSettings();

    Pad pad2 = new Pad();
    PadType padType2 = new PadType();
    PackagePin packagePin2 = new PackagePin();
    padType2.setPackagePin(packagePin2);
    LandPatternPad landPatternPad2 = new SurfaceMountLandPatternPad();
    landPatternPad2.setLandPattern(landPattern);
    landPatternPad2.setName("2");
    padType2.setLandPatternPad(landPatternPad2);
    pad2.setPadType(padType2);
    padType2.setPadTypeSettings(padTypeSettings2);

    component.addPad(pad2);

    // set component to inspect and check the cases
    componentTypeSettings.setLoaded(true);
    componentTypeSettings.setInspected(true);
    // both pins NOT tested
    padTypeSettings.setInspected(false);
    padTypeSettings2.setInspected(false);
    Expect.expect(component.isInspected() == false);

    // even if the pins are tested, if the component is NOT tested, it's not tested.
    componentTypeSettings.setLoaded(true);
    componentTypeSettings.setInspected(false);
    Expect.expect(component.isInspected() == false);


//  public boolean isOnTopSide()
    sideBoard = new SideBoard();
    component.setSideBoard(sideBoard);
//    sideBoard.setCadTopSide(false);
//    Expect.expect(component.isTopSide() == false);

    // getShapeInNanometers is tested in Test_Panel.java because it needs a fully loaded panel
    // transformShapeBasedOnComponentAndBoardAndPanelRotationInNanoMeters is tested in Test_Panel.java because it needs a fully loaded panel

    // now test the copy constructor
    // do this at the end because by now every little function has been tested successfully.
    component = new Component();
    componentType = new ComponentType();
    component.setComponentType(componentType);
    String name = "U1";
    sideBoard = new SideBoard();
    CompPackage compPackage = new CompPackage();

    pad = new Pad();
    padType = new PadType();
    pad.setPadType(padType);
    padName = "padName";
    landPatternPad = new SurfaceMountLandPatternPad();
    landPatternPad.setLandPattern(landPattern);
    landPatternPad.setName(padName);
    padType.setLandPatternPad(landPatternPad);
    padTypeSettings = new PadTypeSettings();
    padType.setPadTypeSettings(padTypeSettings);

    landPattern = new LandPattern();
    componentTypeSettings = new ComponentTypeSettings();
    componentType.setPanel(panel);
    componentType.setReferenceDesignator(name);

    component.setSideBoard(sideBoard);
    componentType.setCompPackage(compPackage);
    component.addPad(pad);
    componentType.setLandPattern(landPattern);
    componentType.setComponentTypeSettings(componentTypeSettings);

    Component copy = new Component(component);
    Expect.expect(copy.getReferenceDesignator() == name);
    Expect.expect(copy.getComponentType() == componentType);
    Expect.expect(copy.getSideBoard() == sideBoard);
    Expect.expect(copy.getCompPackage() == compPackage);
    Expect.expect(copy.getLandPattern() == landPattern);
    Expect.expect(copy.getPads().size() == 1);
    Expect.expect(copy.getPad(padName) != pad);

    Expect.expect(copy.getPad(padName).getPadTypeSettings() == padTypeSettings);
  }
}

