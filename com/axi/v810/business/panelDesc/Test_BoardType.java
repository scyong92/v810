package com.axi.v810.business.panelDesc;

import java.io.*;
import java.util.*;

import com.axi.v810.business.panelSettings.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
class Test_BoardType extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_BoardType());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      Panel panel = new Panel();
      panel.disableChecksForNdfParsers();
      // test setName() and getName()
      BoardType boardType = new BoardType();
      boardType.setPanel(panel);
      try
      {
        boardType.setName(null);
        Expect.expect(false);
      }
      catch (AssertException e)
      {
        // do nothing
      }
      String name = "boardTypeName";
      boardType.setName(name);
      Expect.expect(boardType.getName().equals(name));

// wpd remove virgo
//    // test setVersion() and getVersion()
//    try
//    {
//      boardType.setVersion(null);
//      Expect.expect(false);
//    }
//    catch(AssertException e)
//    {
//      // do nothing
//    }
//    try
//    {
//      boardType.getVersion();
//      Expect.expect(false);
//    }
//    catch(AssertException e)
//    {
//      // do nothing
//    }
//    String version = "Version";
//    boardType.setVersion(version);
//    Expect.expect(boardType.getVersion().equals(version));

      // test setPanel() and getPanel()
      try
      {
        boardType.setPanel(null);
        Expect.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing
      }
      boardType.setPanel(panel);
      Expect.expect(boardType.getPanel() == panel);

      // test setTopSideboardType() and topSideboardTypeExists() and getTopSideboardType()
      try
      {
        boardType.setSideBoardType1(null);
        Expect.expect(false);
      }
      catch (AssertException e)
      {
        // do nothing
      }
      Expect.expect(boardType.sideBoardType1Exists() == false);
      SideBoardType topSideBoardType = new SideBoardType();
      boardType.setSideBoardType1(topSideBoardType);
      Expect.expect(boardType.sideBoardType1Exists());
      Expect.expect(boardType.getSideBoardType1() == topSideBoardType);

      // test setBottomSideBoardType() and bottomSideBoardTypeExists() and getBottomSideBoardType()
      try
      {
        boardType.setSideBoardType2(null);
        Expect.expect(false);
      }
      catch (AssertException e)
      {
        // do nothing
      }
      Expect.expect(boardType.sideBoardType2Exists() == false);
      SideBoardType bottomSideBoardType = new SideBoardType();
      boardType.setSideBoardType2(bottomSideBoardType);
      Expect.expect(boardType.sideBoardType2Exists());
      Expect.expect(boardType.getSideBoardType2() == bottomSideBoardType);

      // test setWidthInNanoMeters and getWidthInNanoMeters
      try
      {
        boardType.getWidthInNanoMeters();
        Expect.expect(false);
      }
      catch (AssertException e)
      {
        // do nothing
      }
      try
      {
        boardType.setWidthInNanoMeters(0);
        Expect.expect(false);
      }
      catch (AssertException e)
      {
        // do nothing
      }
      int width = 50;
      boardType.setWidthInNanoMeters(width);
      Expect.expect(boardType.getWidthInNanoMeters() == width);

      // test setLengthInNanoMeters and getLengthInNanoMeters
      try
      {
        boardType.getLengthInNanoMeters();
        Expect.expect(false);
      }
      catch (AssertException e)
      {
        // do nothing
      }
      try
      {
        boardType.setLengthInNanoMeters(0);
        Expect.expect(false);
      }
      catch (AssertException e)
      {
        // do nothing
      }
      int length = 50;
      boardType.setLengthInNanoMeters(length);
      Expect.expect(boardType.getLengthInNanoMeters() == length);

      // test getComponentTypes()
      List<ComponentType> list = boardType.getComponentTypes();
      Expect.expect(list.size() == 0);

      ComponentType c1 = new ComponentType();
      c1.setPanel(panel);
      c1.setReferenceDesignator("u1");
      topSideBoardType.addComponentType(c1);
      list = boardType.getComponentTypes();
      Expect.expect(list.size() == 1);

      ComponentType c2 = new ComponentType();
      c2.setPanel(panel);
      c2.setReferenceDesignator("u2");
      topSideBoardType.addComponentType(c2);
      list = boardType.getComponentTypes();
      Expect.expect(list.size() == 2);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
