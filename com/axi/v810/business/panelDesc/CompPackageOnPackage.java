package com.axi.v810.business.panelDesc;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;

public class CompPackageOnPackage implements Serializable
{
  private Panel _panel; 
  private String _popName; 
  private Set<ComponentType> _componentTypeSet = new HashSet<ComponentType>();
  private static transient ProjectObservable _projectObservable;
  private static transient AlphaNumericComparator _alphaNumericComparator;
  private static transient final String _POP_NAME_EXTENSION = "_POP";

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
    _alphaNumericComparator = new AlphaNumericComparator();
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public CompPackageOnPackage()
  {
    _projectObservable.stateChanged(this, null, this, CompPackageOnPackageEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {      
      if (_componentTypeSet.isEmpty() == false)
      {        
        for (ComponentType componentType: _componentTypeSet)
        {
          componentType.removeCompPackageOnPackage();
        }
        _componentTypeSet.clear();
      }
      _panel.removeCompPackageOnPackage(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, CompPackageOnPackageEventEnum.CREATE_OR_DESTROY);
  }
    
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void add()
  {
    _projectObservable.setEnabled(false);
    try
    {      
      _panel.addCompPackageOnPackage(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, this, CompPackageOnPackageEventEnum.CREATE_OR_DESTROY);

  }  

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void setPanel(Panel panel)
  {
    Assert.expect(panel != null);

    Panel oldValue = _panel;
    _projectObservable.setEnabled(false);
    try
    {
      _panel = panel;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, panel, CompPackageOnPackageEventEnum.PANEL);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public Panel getPanel()
  {
    Assert.expect(_panel != null);

    return _panel;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void setName(String POPName)
  {
    Assert.expect(POPName != null);
    Assert.expect(_panel != null);
    Assert.expect(_panel.isCompPackageOnPackageNameValid(POPName));

    if (POPName.equals(_popName))
      return;

    if (POPName.contains(_POP_NAME_EXTENSION) == false)
      POPName = POPName + _POP_NAME_EXTENSION;
    
    String oldValue = _popName;
    _projectObservable.setEnabled(false);
    try
    {
      _panel.setNewCompPackageOnPackageName(this, _popName, POPName);
      _popName = POPName.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, POPName, CompPackageOnPackageEventEnum.NAME);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public String getPOPName()
  {
    Assert.expect(_popName != null);

    return _popName;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public String toString()
  {
    return getPOPName();
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public synchronized void addComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    Assert.expect(_componentTypeSet != null);

    _projectObservable.setEnabled(false);
    try
    {
      boolean added = _componentTypeSet.add(componentType);
      Assert.expect(added, "Package: " + getPOPName() + " component type: " + componentType.getReferenceDesignator());  
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, componentType, CompPackageOnPackageEventEnum.ADD_OR_REMOVE_COMPONENT_TYPE);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public synchronized void removeComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    Assert.expect(_componentTypeSet != null);

    _projectObservable.setEnabled(false);
    try
    {      
      boolean removed = _componentTypeSet.remove(componentType);
      
      Assert.expect(removed, "Package: " + getPOPName() + " component type: " + componentType.getReferenceDesignator());
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, componentType, null, CompPackageOnPackageEventEnum.ADD_OR_REMOVE_COMPONENT_TYPE);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public synchronized List<ComponentType> getComponentTypes()
  {
    Assert.expect(_componentTypeSet != null);

    List<ComponentType> componentTypes = new ArrayList<ComponentType>(_componentTypeSet);
    Collections.sort(componentTypes, _alphaNumericComparator);
    return componentTypes;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public synchronized int getTotalPOPLayerNumber()
  {
    Assert.expect(_componentTypeSet != null);

    return _componentTypeSet.size();
  }
}
