package com.axi.v810.business.panelDesc;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.datastore.config.*;

/**
 * Contains all the data that describes the panel being tested.
 * There is one instance of this class for each physical instance of
 * a panel.
 *
 * @author Bill Darbie
 */
public class Panel implements Serializable
{
  // created by PanelNdfReader, ProjectReader

  private Project _project; // PanelNdfReader

  // version found in the original CAD
  private String _cadVersion; // not set

  private int _widthInNanoMeters = -1; // PanelNdfReader, PanelReader
  private int _lengthInNanoMeters = -1; // PanelNdfReader, PanelReader

  private int _thicknessInNanoMeters = -1; // PanelNdfReader

  private PanelSettings _panelSettings;  // PanelNdfReader, PanelSettingsReader
  private Map<String, Fiducial> _fiducialNameToFiducialMap = new TreeMap<String, Fiducial>(new AlphaNumericComparator()); // PanelNdfReader

  private Map<String, BoardType> _boardTypeNameToBoardTypeMap = new TreeMap<String, BoardType>(new AlphaNumericComparator()); // PanelNdfReader, PanelReader
  private Map<String, Board> _boardNameToBoardMap = new TreeMap<String, Board>(new AlphaNumericComparator()); // PanelReader
  private Map<String, CompPackage> _compPackageNameToCompPackageMap = new TreeMap<String, CompPackage>(new AlphaNumericComparator()); // calculated
  private Map<String, LandPattern> _landPatternNameToLandPatternMap = new TreeMap<String, LandPattern>(new AlphaNumericComparator()); // calculated
  private Map<String, Subtype> _subtypeNameToSubtypeMap = new TreeMap<String, Subtype>(new AlphaNumericComparator()); // calculated // SubtypeSettingsReader
  private List<BoardSurfaceMapSettings> _surfaceMapList = new ArrayList<BoardSurfaceMapSettings>();   // SurfaceMapSettingsReader

  private transient boolean _disableChecksForNdfParsers = false;
  private transient java.awt.Shape _shape;
  private transient PanelHandler _panelHandler;

  private static transient ProjectObservable _projectObservable;
  
  private PanelSurfaceMapSettings _panelSurfaceMapSettings; // PanelSurfaceMapSettings
  private PanelAlignmentSurfaceMapSettings _panelAlignmentSurfaceMapSettings; // PanelAlignmentSurfaceMapSettings
  
  private PspSettings _pspSettings; // PspSettings
  private PanelMeshSettings _panelMeshSettings; // PanelMeshSettings
  
  private PshSettings _pshSettings; // PshSettings - Siew Yeg - XCR-3781
 
  private String _subtype5DXLongName = null;
  
  private List<String> _componentListUsingCompPackage = new ArrayList<String>();
  private List<Component> _allComponentList = new ArrayList<Component>();
  
  //Khaw Chek Hau - XCR3554: Package on package (PoP) development
  private Map<String, CompPackageOnPackage> _compPackageOnPackageNameToCompPackageOnPackageMap = new TreeMap<String, CompPackageOnPackage>(new AlphaNumericComparator()); // calculated

  /**
   * @author Bill Darbie
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public Panel()
  {
    _projectObservable.stateChanged(this, null, this, PanelEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  public void destroy()
  {
    _projectObservable.setEnabled(false);
    try
    {
      _panelSettings.destroy();
      _panelSurfaceMapSettings.destroy();
      _panelAlignmentSurfaceMapSettings.destroy();
      _pspSettings.destroy();
      _pshSettings.destroy();
      _panelMeshSettings.destroy();

      for (Board board : getBoards())
      {
        board.destroy();
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, this, null, PanelEventEnum.CREATE_OR_DESTROY);
  }

  /**
   * @author Bill Darbie
   */
  void destroyCompPackageIfNotInUse(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);

    boolean compPackageInUse = false;
    for (ComponentType componentType : getComponentTypes())
    {
      if (componentType.getCompPackage() == compPackage)
      {
        compPackageInUse = true;
        break;
      }
    }

    if (compPackageInUse == false)
      compPackage.destroy();
  }

  /**
   * @author Bill Darbie
   */
  public void setProject(Project project)
  {
    Assert.expect(project != null);

    if (project == _project)
      return;

    Project oldValue = _project;
    _projectObservable.setEnabled(false);
    try
    {
      _project = project;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, project, PanelEventEnum.PROJECT);
  }

  /**
   * @author Bill Darbie
   */
  public Project getProject()
  {
    Assert.expect(_project != null);
    return _project;
  }

  /**
   * @author Roy Williams
   */
  public void clearProject()
  {
    _project = null;
  }

  /**
   * @author Bill Darbie
   */
  public void enableChecksAfterNdfParsersAreComplete()
  {
    _disableChecksForNdfParsers = false;

    // now re-populate the map
    _landPatternNameToLandPatternMap.clear();
    for (ComponentType componentType : getComponentTypes())
    {
      LandPattern landPattern = componentType.getLandPattern();
      _landPatternNameToLandPatternMap.put(landPattern.getName(), landPattern);
    }
  }

  /**
   * @author Bill Darbie
   */
  boolean areChecksDisabledForNdfparsers()
  {
    return _disableChecksForNdfParsers;
  }

  /**
   * @author Bill Darbie
   */
  public void disableChecksForNdfParsers()
  {
    _disableChecksForNdfParsers = true;
  }

  /**
   * @author Bill Darbie
   */
  public void checkBoardName(String name) throws BusinessException
  {
    if (isBoardNameValid(name) == false)
      throw new InvalidNameBusinessException("board", name, getBoardNameIllegalChars());

    if (isBoardNameDuplicate(name))
      throw new DuplicateNameBusinessException("board", name);
  }

  /**
   * @author Bill Darbie
   */
  public String getBoardNameIllegalChars()
  {
    return " ";
  }

  /**
   * @author Bill Darbie
   */
  public boolean isBoardNameValid(String name)
  {
    Assert.expect(name != null);
    if (name.contains(" ") || name.equals(""))
      return false;
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isBoardNameDuplicate(String name)
  {
    Assert.expect(name != null);

    // now check for duplicates
    if (_boardNameToBoardMap.containsKey(name))
      return true;
    return false;
  }

  /**
   * @author Bill Darbie
   */
  void setNewBoardName(Board board, String oldName, String newName)
  {
    Assert.expect(board != null);
    Assert.expect(newName != null);
    Board prev = null;
    if (oldName != null)
    {
      prev = _boardNameToBoardMap.remove(oldName);
      Assert.expect(prev != null);
    }
    prev = _boardNameToBoardMap.put(newName, board);
    Assert.expect(prev == null);

    if (oldName == null)
      _projectObservable.stateChanged(this, null, board, PanelEventEnum.ADD_OR_REMOVE_BOARD);
  }

  /**
   * @author Bill Darbie
   */
  public void checkBoardTypeName(String name) throws BusinessException
  {
    if (isBoardTypeNameValid(name) == false)
      throw new InvalidNameBusinessException("boardType", name, getBoardTypeNameIllegalChars());

    if (isBoardTypeNameDuplicate(name))
      throw new DuplicateNameBusinessException("boardType", name);
  }

  /**
   * @author Bill Darbie
   */
  public String getBoardTypeNameIllegalChars()
  {
    return " ";
  }

  /**
   * @author Bill Darbie
   */
  public boolean isBoardTypeNameValid(String name)
  {
    Assert.expect(name != null);
    if (name.contains(" ") || name.equals(""))
      return false;
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isBoardTypeNameDuplicate(String name)
  {
    if (_boardTypeNameToBoardTypeMap.containsKey(name))
      return true;
    return false;
  }

  /**
   * @author Bill Darbie
   */
  public void removeBoardType(BoardType boardType)
  {
    Assert.expect(boardType != null);
    BoardType prev = _boardTypeNameToBoardTypeMap.remove(boardType);
    Assert.expect(prev != null);
  }

  /**
   * @author Bill Darbie
   */
  void setNewBoardTypeName(BoardType boardType, String oldName, String newName)
  {
    Assert.expect(boardType != null);
    Assert.expect(newName != null);
    BoardType prev = null;
    if (oldName != null)
    {
      prev = _boardTypeNameToBoardTypeMap.remove(oldName);
      Assert.expect(prev != null);
    }
    prev = _boardTypeNameToBoardTypeMap.put(newName, boardType);
    Assert.expect(prev == null);

    if (oldName == null)
      _projectObservable.stateChanged(this, null, boardType, PanelEventEnum.ADD_OR_REMOVE_BOARD_TYPE);
  }

  /**
   * @author Bill Darbie
   */
  public void checkCompPackageName(String name) throws BusinessException
  {
    if (isCompPackageNameValid(name) == false)
      throw new InvalidNameBusinessException("package", name, getCompPackageNameIllegalChars());

    if (isCompPackageNameDuplicate(name))
      throw new DuplicateNameBusinessException("package", name);
  }

  /**
   * @author Bill Darbie
   */
  public String getCompPackageNameIllegalChars()
  {
    return " ";
  }

  /**
   * @author Bill Darbie
   */
  public boolean isCompPackageNameValid(String name)
  {
    Assert.expect(name != null);
    if (name.contains(" ") || name.equals(""))
      return false;
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isCompPackageNameDuplicate(String name)
  {
    Assert.expect(name != null);
    // now check for duplicates
    if (_compPackageNameToCompPackageMap.containsKey(name))
      return true;
    return false;
  }

  /**
   * @author Bill Darbie
   */
  void setNewCompPackageName(CompPackage compPackage, String oldName, String newName)
  {
    Assert.expect(compPackage != null);
    Assert.expect(newName != null);
    CompPackage prev = null;
    if (oldName != null)
    {
      prev = _compPackageNameToCompPackageMap.remove(oldName);
      Assert.expect(prev != null);
    }
    prev = _compPackageNameToCompPackageMap.put(newName, compPackage);
    Assert.expect(prev == null);

    if (oldName == null)
      _projectObservable.stateChanged(this, null, compPackage, PanelEventEnum.ADD_OR_REMOVE_COMP_PACKAGE);
  }

  /**
   * @author Bill Darbie
   */
  public void checkFiducialName(String name) throws BusinessException
  {
    if (isFiducialNameValid(name) == false)
      throw new InvalidNameBusinessException("fiducial", name, getFiducialNameIllegalChars());

    if (isFiducialNameDuplicate(name))
      throw new DuplicateNameBusinessException("fiducial", name);
  }

  /**
   * @author Bill Darbie
   */
  public String getFiducialNameIllegalChars()
  {
    return " ";
  }

  /**
   * @author Bill Darbie
   */
  public boolean isFiducialNameValid(String name)
  {
    Assert.expect(name != null);
    if (name.contains(" ") || name.equals(""))
      return false;
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isFiducialNameDuplicate(String name)
  {
    Assert.expect(name != null);
    if (_fiducialNameToFiducialMap.containsKey(name))
      return true;
    return false;
  }

  /**
   * @author Bill Darbie
   */
  void setNewFiducialName(Fiducial fiducial, String oldName, String newName)
  {
    Assert.expect(fiducial != null);
    Assert.expect(newName != null);
    Fiducial prev = null;
    if (oldName != null)
    {
      prev = _fiducialNameToFiducialMap.remove(oldName);
      Assert.expect(prev != null);
    }
    prev = _fiducialNameToFiducialMap.put(newName, fiducial);
    Assert.expect(prev == null);

    if (oldName == null)
      _projectObservable.stateChanged(this, null, fiducial, PanelEventEnum.ADD_OR_REMOVE_FIDUCIAL);
  }

  /**
   * @author Bill Darbie
   */
  public void checkLandPatternName(String name) throws BusinessException
  {
    if (isLandPatternNameValid(name) == false)
      throw new InvalidNameBusinessException("land pattern", name, getLandPatternNameIllegalChars());

    if (isLandPatternNameDuplicate(name))
      throw new DuplicateNameBusinessException("land pattern", name);
  }

  /**
   * @author Bill Darbie
   */
  public String getLandPatternNameIllegalChars()
  {
    return " ";
  }

  /**
   * @author Bill Darbie
   */
  public boolean isLandPatternNameValid(String name)
  {
    Assert.expect(name != null);

    if (name.contains(" ") || name.equals(""))
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isLandPatternNameDuplicate(String name)
  {
    Assert.expect(name != null);
    if ((_disableChecksForNdfParsers == false) && _landPatternNameToLandPatternMap.containsKey(name))
      return true;
    return false;
  }

  /**
   * @author Bill Darbie
   */
  void setNewLandPatternName(LandPattern landPattern, String oldName, String newName)
  {
    Assert.expect(landPattern != null);
    Assert.expect(newName != null);

    if (_disableChecksForNdfParsers == false)
    {
      LandPattern prev = null;
      if (oldName != null)
      {
        prev = _landPatternNameToLandPatternMap.remove(oldName);
        Assert.expect(prev != null);
      }
      prev = _landPatternNameToLandPatternMap.put(newName, landPattern);
      Assert.expect(prev == null);

      if (oldName == null)
        _projectObservable.stateChanged(this, null, landPattern, PanelEventEnum.ADD_OR_REMOVE_LAND_PATTERN);
    }
  }

  /**
   * @author Bill Darbie
   */
  public void checkSubtypeName(String origName) throws BusinessException
  {
    if (isSubtypeNameValid(origName) == false)
      throw new InvalidNameBusinessException("subtype", origName, getLandPatternNameIllegalChars());

    if (isSubtypeNameDuplicate(origName))
      throw new DuplicateNameBusinessException("subtype", origName);
  }

  /**
   * @author Bill Darbie
   */
  public String getSubtypeNameIllegalChars()
  {
    return " ";
  }

  /**
   * @author Bill Darbie
   */
  public boolean isSubtypeNameValid(String name)
  {
    Assert.expect(name != null);

    if (name.equals(""))
      return false;

    String illegalChars = getSubtypeNameIllegalChars();
    for(int i = 0; i < illegalChars.length(); ++i)
    {
      if (name.indexOf(illegalChars.charAt(i)) != -1)
        return false;
    }

    return true;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isSubtypeNameDuplicate(String origName)
  {
    Assert.expect(origName != null);
    if (_subtypeNameToSubtypeMap.containsKey(origName))
      return true;
    return false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isSurfaceMapNameExists(BoardSurfaceMapSettings surfaceMap)
  {
    Assert.expect(surfaceMap != null);
    if (_surfaceMapList.contains(surfaceMap))
      return true;
    return false;
  }

  /**
   * @author Bill Darbie
   */
  public void setNewSubtypeName(Subtype subtype, String oldOrigName, String newOrigName)
  {
    Assert.expect(subtype != null);
    Assert.expect(newOrigName != null);

    Subtype prev = null;
    if (oldOrigName != null)
    {
      prev = _subtypeNameToSubtypeMap.remove(subtype.getLongName());
      Assert.expect(prev != null);
    }
    prev = _subtypeNameToSubtypeMap.put(newOrigName, subtype);
    Assert.expect(prev == null);

    if (oldOrigName == null)
    {
      _projectObservable.stateChanged(this, null, subtype, PanelEventEnum.ADD_OR_REMOVE_SUBTYPE);
    }
  }

  /**
   * @author Bill Darbie
   */
  public void checkPanelName(String name) throws BusinessException
  {
    if (isNameValid(name) == false)
      throw new InvalidNameBusinessException("panel", name, getPanelNameIllegalChars());
  }

  /**
   * @author Bill Darbie
   */
  public String getPanelNameIllegalChars()
  {
    return " " + FileName.getIllegalChars();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isNameValid(String panelName)
  {
    Assert.expect(panelName != null);

    if (panelName.contains(" ") || FileName.hasIllegalChars(panelName) || panelName.equals(""))
      return false;

    return true;
  }

//  /**
//   * @author Bill Darbie
//   */
//  public void setName(String name)
//  {
//    Assert.expect(name != null);
//    Assert.expect(isNameValid(name));
//
//    String oldValue = _name;
//    _projectObservable.setEnabled(false);
//    try
//    {
//      _name = name.intern();
//    }
//    finally
//    {
//      _projectObservable.setEnabled(true);
//    }
//    _projectObservable.stateChanged(this, oldValue, name, PanelEventEnum.NAME);
//  }
//
//  /**
//   * @author Bill Darbie
//   */
//  public String getName()
//  {
//    Assert.expect(_name != null);
//
//    return _name;
//  }

  /**
   * Set the CAD version.  This is the version specified in the CAD itself.  It
   * has nothing to do with the 5dx in any way.
   * @author Bill Darbie
   */
  public void setCadVersion(String cadVersion)
  {
    Assert.expect(cadVersion != null);

    if (cadVersion == _cadVersion)
      return;

    String oldValue = _cadVersion;
    _projectObservable.setEnabled(false);
    try
    {
      _cadVersion = cadVersion.intern();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, cadVersion, PanelEventEnum.CAD_VERSION);
  }

  /**
   * @author Bill Darbie
   */
  public String getCadVersion()
  {
    Assert.expect(_cadVersion != null);

    return _cadVersion;
  }

  /**
   * Set the width of the panel at 0 degrees rotation.
   * @author Bill Darbie
   */
  public void setWidthInNanoMeters(int widthInNanoMeters)
  {
    Assert.expect(widthInNanoMeters > 0);

    if (widthInNanoMeters == _widthInNanoMeters)
      return;

    int oldValue = _widthInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      boolean firstTimeSet = false;
      if (_widthInNanoMeters == -1)
        firstTimeSet = true;
      _widthInNanoMeters = widthInNanoMeters;
      if ((_panelSettings != null) && (firstTimeSet == false))
        _panelSettings.calculateShiftsToRetainLowerLeftCorner(getDegreesRotationRelativeToCad());
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, widthInNanoMeters, PanelEventEnum.WIDTH);
  }

  /**
   * @author Bill Darbie
   */
  public void setWidthInNanoMetersWhileRetainingBoardCoordinates(int widthInNanoMeters)
  {
    Assert.expect(widthInNanoMeters > 0);

    if (widthInNanoMeters == _widthInNanoMeters)
      return;

    int oldValue = _widthInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      // keep all boards at the same coordinate
      Map<Board, PanelCoordinate> boardOriginMap = new HashMap<Board, PanelCoordinate>();
      for (Board board : getBoards())
        boardOriginMap.put(board, board.getLowerLeftCoordinateRelativeToPanelInNanoMeters());

      setWidthInNanoMeters(widthInNanoMeters);

      // keep all boards at the same coordinate
      for (Board board : getBoards())
      {
        PanelCoordinate lowerLeftCoord = boardOriginMap.get(board);
        board.setLowerLeftCoordinateRelativeToPanelInNanoMeters(lowerLeftCoord);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, widthInNanoMeters, PanelEventEnum.WIDTH_RETAINING_BOARD_COORDS);
  }

  /**
   * @return the x axis width of the Panel BEFORE any rotation has been applied.
   * @author Bill Darbie
   */
  public int getWidthInNanoMeters()
  {
    Assert.expect(_widthInNanoMeters > 0);

    return _widthInNanoMeters;
  }

  /**
   * @return the x axis width of the Panel AFTER the Panel has been rotated.
   * @author Bill Darbie
   */
  public int getWidthAfterAllRotationsInNanoMeters()
  {
    int width = -1;
    Assert.expect(_panelSettings != null);
    int degrees = _panelSettings.getDegreesRotationRelativeToCad();
    if ((degrees == 0) || (degrees == 180))
    {
      width = getWidthInNanoMeters();
    }
    else if ((degrees == 90) || (degrees == 270))
    {
      width = getLengthInNanoMeters();
    }
    else
      Assert.expect(false);

    Assert.expect(width != -1);
    return width;
  }

  /**
   * @author Bill Darbie
   */
  public void setWidthAfterAllRotationsRetainingBoardCoordsInNanoMeters(int widthInNanoMeters)
  {
    Assert.expect(_panelSettings != null);
    int degrees = _panelSettings.getDegreesRotationRelativeToCad();
    if ((degrees == 0) || (degrees == 180))
    {
      setWidthInNanoMetersWhileRetainingBoardCoordinates(widthInNanoMeters);
    }
    else if ((degrees == 90) || (degrees == 270))
    {
     setLengthInNanoMetersWhileRetainingBoardCoords(widthInNanoMeters);
    }
    else
      Assert.expect(false);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getAutoPopulateSpacingInNanometers()
  {
    Assert.expect(_pspSettings != null);
    return _pspSettings.getAutoPopulateSpacingInNanometers();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setAutoPopulateSpacingInNanometers(int autoPopulateSpacingInNanometers)
  {
    Assert.expect(_pspSettings != null);
    _pspSettings.setAutoPopulateSpacingInNanometers(autoPopulateSpacingInNanometers);
  }
  
  /**
   * Set the length of the Panel at 0 degrees rotation.
   * @author Bill Darbie
   */
  public void setLengthInNanoMeters(int lengthInNanoMeters)
  {
    Assert.expect(lengthInNanoMeters > 0);

    if (lengthInNanoMeters == _lengthInNanoMeters)
      return;

    int oldValue = _lengthInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      boolean firstTimeSet = false;
      if (_lengthInNanoMeters == -1)
        firstTimeSet = true;
      _lengthInNanoMeters = lengthInNanoMeters;
      if ((_panelSettings != null) && (firstTimeSet == false))
        _panelSettings.calculateShiftsToRetainLowerLeftCorner(getDegreesRotationRelativeToCad());
      invalidateShape();
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, lengthInNanoMeters, PanelEventEnum.LENGTH);
  }

  /**
   * Set the length of the Panel at 0 degrees rotation.
   * @author Bill Darbie
   */
  public void setLengthInNanoMetersWhileRetainingBoardCoords(int lengthInNanoMeters)
  {
    Assert.expect(lengthInNanoMeters > 0);

    if (lengthInNanoMeters == _lengthInNanoMeters)
      return;

    int oldValue = _lengthInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      // keep all boards at the same coordinate
      Map<Board, PanelCoordinate> boardOriginMap = new HashMap<Board, PanelCoordinate>();
      for (Board board : getBoards())
        boardOriginMap.put(board, board.getLowerLeftCoordinateRelativeToPanelInNanoMeters());

      setLengthInNanoMeters(lengthInNanoMeters);

      // keep all boards at the same coordinate
      for (Board board : getBoards())
      {
        PanelCoordinate lowerLeftCoord = boardOriginMap.get(board);
        board.setLowerLeftCoordinateRelativeToPanelInNanoMeters(lowerLeftCoord);
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, lengthInNanoMeters, PanelEventEnum.LENGTH_RETAINING_BOARD_COORDS);
  }

  /**
   * @return the y axis Length of the panel BEFORE any rotation has been applied.
   * @author Bill Darbie
   */
  public int getLengthInNanoMeters()
  {
    Assert.expect(_lengthInNanoMeters > 0);

    return _lengthInNanoMeters;
  }

  /**
   * @return the y axis length of the Panel AFTER the Panel has been rotated.
   * @author Bill Darbie
   */
  public int getLengthAfterAllRotationsInNanoMeters()
  {
    int length = -1;
    Assert.expect(_panelSettings != null);
    int degrees = _panelSettings.getDegreesRotationRelativeToCad();
    if ((degrees == 0) || (degrees == 180))
    {
      length = getLengthInNanoMeters();
    }
    else if ((degrees == 90) || (degrees == 270))
    {
      length = getWidthInNanoMeters();
    }
    else
      Assert.expect(false);

    Assert.expect(length != -1);
    return length;
  }

  /**
   * @author Bill Darbie
   */
  public void setLengthAfterAllRotationsRetainingBoardCoordsInNanoMeters(int lengthInNanoMeters)
  {
    Assert.expect(_panelSettings != null);
    int degrees = _panelSettings.getDegreesRotationRelativeToCad();
    if ((degrees == 0) || (degrees == 180))
    {
      setLengthInNanoMetersWhileRetainingBoardCoords(lengthInNanoMeters);
    }
    else if ((degrees == 90) || (degrees == 270))
    {
      setWidthInNanoMetersWhileRetainingBoardCoordinates(lengthInNanoMeters);
    }
    else
      Assert.expect(false);
  }

  /**
   * Create a new Board instance using the BoardType passed in.
   * This method will do everything necessary to instantiate all other classes used by the new Board,
   * include Components, etc.
   *
   * @author Bill Darbie
   */
  public Board createBoard(BoardType boardType,
                           PanelCoordinate lowerLeftCoordinateAfterAllRotations,
                           int degreesRotation,
                           boolean flip)
  {
    Assert.expect(boardType != null);
    Assert.expect(lowerLeftCoordinateAfterAllRotations != null);

    Board board = null;
    _projectObservable.setEnabled(false);
    try
    {
      Panel panel = boardType.getPanel();

      board = new Board();
      board.setBoardType(boardType);
      // calling this instead of setLowerLeft because I do the rotations and flips AFTER setting this
      // coordinate.  Also, it's not allowed to set that lower left one as the ONLY coordinate, as it
      // uses the "normal" coordinate it calculate how to set it.
      board.setCoordinateInNanoMeters(new PanelCoordinate(0,0));
      board.setDegreesRotationRelativeToPanel(0);

      board.setDegreesRotationRelativeToPanel(degreesRotation);
      if (flip)
        board.flip();
      board.createSideBoards();
      assignUniqueBoardName(board);
      boardType.addBoard(board);

      BoardSettings boardSettings = new BoardSettings(board);
      boardSettings.setInspected(true);
      boardSettings.setPopulated(true);
      boardSettings.assignAlignmentGroups();
      board.setBoardSettings(boardSettings);
      board.setLowerLeftCoordinateRelativeToPanelInNanoMeters(lowerLeftCoordinateAfterAllRotations);
      
      BoardSurfaceMapSettings boardSurfaceMapSettings = new BoardSurfaceMapSettings(board);
      board.setBoardSurfaceMapSettings(boardSurfaceMapSettings);
      
      BoardAlignmentSurfaceMapSettings boardAlignmentSurfaceMapSettings = new BoardAlignmentSurfaceMapSettings(board);
      board.setBoardAlignmentSurfaceMapSettings(boardAlignmentSurfaceMapSettings);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(board, null, board, BoardEventEnum.CREATE_OR_DESTROY);

    Assert.expect(board != null);
    return board;
  }

  /**
   * @author Bill Darbie
   */
  private void assignUniqueBoardName(Board board)
  {
    Assert.expect(board != null);

    // find all existing board names
    Set<String> boardNameSet = new HashSet<String>();

    for (Board aBoard : _boardNameToBoardMap.values())
    {
      String name = aBoard.getName();
      boardNameSet.add(name);
    }

    // assign a unique board name
    String uniqueName = null;
    for(int i = 1; i < _boardNameToBoardMap.values().size() + 2; ++i)
    {
      String name = Integer.toString(i);
      if (boardNameSet.contains(name) == false)
        uniqueName = name;
    }

    Assert.expect(uniqueName != null);
    board.setName(uniqueName);
  }

  /**
   * @author Bill Darbie
   */
  public void removeBoard(Board board)
  {
    Assert.expect(board != null);

    _projectObservable.setEnabled(false);
    try
    {
      Board prevBoard = _boardNameToBoardMap.remove(board);
      Assert.expect(prevBoard != null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, board, null, PanelEventEnum.ADD_OR_REMOVE_BOARD);
  }


  /**
   * Return a List or Board instances sorted by name.
   *
   * @return a List of all the Board instances of this panel.  If the same board is placed on the panel
   * in two different locations, two instances of that board will be returned in this list.
   * @author Bill Darbie
   */
  public List<Board> getBoards()
  {
    Assert.expect(_boardNameToBoardMap != null);

    return new ArrayList<Board>(_boardNameToBoardMap.values());
  }

  /**
   * @author Bill Darbie
   */
  public int getNumBoards()
  {
    Assert.expect(_boardNameToBoardMap != null);

    return _boardNameToBoardMap.size();
  }

  /**
   * @author Siew Yeng
   * @return board name list
   */
  public List<String> getBoardNames()
  {
    Assert.expect(_boardNameToBoardMap != null);

    return new ArrayList<String>(_boardNameToBoardMap.keySet());
  }

  /**
   * Add a new Board to this Panel.  This method is used when creating a project in memory
   * for the first time.
   *
   * @author Bill Darbie
   */
  public void addBoard(Board board)
  {
    Assert.expect(board != null);

    _projectObservable.setEnabled(false);
    try
    {
      Board prev = _boardNameToBoardMap.put(board.getName(), board);
      Assert.expect(prev == null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, board, PanelEventEnum.ADD_OR_REMOVE_BOARD);
  }

  /**
   * @author Bill Darbie
   */
  void addCompPackage(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);

    _projectObservable.setEnabled(false);
    try
    {
      Object previous = _compPackageNameToCompPackageMap.put(compPackage.getLongName(), compPackage);
      Assert.expect(previous == null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, compPackage, PanelEventEnum.ADD_OR_REMOVE_COMP_PACKAGE);
  }



  /**
   * @author Bill Darbie
   */
  public void addBoardType(BoardType boardType)
  {
    Assert.expect(boardType != null);
    Assert.expect(_boardTypeNameToBoardTypeMap != null);

    _projectObservable.setEnabled(false);
    try
    {
      // if the same boardType is added more than once, that is OK
      // the set will only take one
      _boardTypeNameToBoardTypeMap.put(boardType.getName(), boardType);
      boardType.setPanel(this);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, boardType, PanelEventEnum.ADD_OR_REMOVE_BOARD_TYPE);
  }

  /**
   * Return a List of BoardTypes used in this Panel sorted by name.
   * @return a List of BoardTypes for this Panel.
   * @author Bill Darbie
   */
  public List<BoardType> getBoardTypes()
  {
    Assert.expect(_boardTypeNameToBoardTypeMap != null);

    List<BoardType> boardTypes = new ArrayList<BoardType>(_boardTypeNameToBoardTypeMap.values());

    return boardTypes;
  }

  /**
   * @return a List of BoardTypes used for this Panel.
   * @author Bill Darbie
   */
  public List<BoardType> getUsedBoardTypes()
  {
    Set<BoardType> boardTypes = new TreeSet<BoardType>(new AlphaNumericComparator());
    for(Board board : getBoards())
    {
      BoardType boardType = board.getBoardType();
      boardTypes.add(boardType);
    }

    return new ArrayList<BoardType>(boardTypes);
  }


  /**
   * @return the number of board types found on this Panel.
   * @author Bill Darbie
   */
  public int getNumBoardTypes()
  {
    Assert.expect(_boardTypeNameToBoardTypeMap != null);

    return _boardTypeNameToBoardTypeMap.size();
  }

  /**
   * @return all CompPackages, both used and unused.
   * @author Bill Darbie
   */
  public List<CompPackage> getCompPackages()
  {
    return new ArrayList<CompPackage>(_compPackageNameToCompPackageMap.values());
  }

  /**
   * @return unused CompPackages.
   * @author Bill Darbie
   */
  public List<CompPackage> getUnusedCompPackage()
  {
    Collection<CompPackage> allCompPackages = _compPackageNameToCompPackageMap.values();
    Set<CompPackage> usedCompPackageSet = new HashSet<CompPackage>(getUsedCompPackages());

    List<CompPackage> unusedCompPackages = new ArrayList<CompPackage>();
    for (CompPackage compPackage : allCompPackages)
    {
      if (usedCompPackageSet.contains(compPackage) == false)
        unusedCompPackages.add(compPackage);
    }

    return unusedCompPackages;
  }

  /**
   * @return all CompPackages that are set to some pad, independent of if the pad is loaded, inspected or
   * testable.
   * @author Bill Darbie
   */
  public List<CompPackage> getUsedCompPackages()
  {
    Set<CompPackage> compPackageSet = new TreeSet<CompPackage>(new AlphaNumericComparator());

    for (BoardType boardType : getBoardTypes())
    {
      for (ComponentType componentType : boardType.getComponentTypes())
      {
        compPackageSet.add(componentType.getCompPackage());
        // it is expected that the add above will get the same CompPackage sometimes
        // and therefore it can return true or false, either is OK
      }
    }
    Assert.expect(compPackageSet != null);
    return new ArrayList<CompPackage>(compPackageSet);
  }

  /**
   * @author Bill Darbie
   */
  public void setPanelSettings(PanelSettings panelSettings)
  {
    Assert.expect(panelSettings != null);

    if (panelSettings == _panelSettings)
      return;

    PanelSettings oldValue = _panelSettings;
    _projectObservable.setEnabled(false);
    try
    {
      _panelSettings = panelSettings;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, panelSettings, PanelEventEnum.PANEL_SETTINGS);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setPanelSurfaceMapSettings(PanelSurfaceMapSettings panelSurfaceMapSettings)
  {
    Assert.expect(panelSurfaceMapSettings != null);

    if (panelSurfaceMapSettings == _panelSurfaceMapSettings)
      return;

    PanelSurfaceMapSettings oldValue = _panelSurfaceMapSettings;
    _projectObservable.setEnabled(false);
    try
    {
      _panelSurfaceMapSettings = panelSurfaceMapSettings;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, panelSurfaceMapSettings, PanelEventEnum.PANEL_SURFACE_MAP_SETTINGS);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void setPanelMeshSettings(PanelMeshSettings panelMeshSettings)
  {
    Assert.expect(panelMeshSettings != null);
    
    if (panelMeshSettings == _panelMeshSettings)
      return;
    
    PanelMeshSettings oldValue = _panelMeshSettings;
    _projectObservable.setEnabled(false);
    try
    {
      _panelMeshSettings = panelMeshSettings;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, panelMeshSettings, PanelEventEnum.PANEL_MESH_SETTINGS);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setPanelAlignmentSurfaceMapSettings(PanelAlignmentSurfaceMapSettings panelAlignmentSurfaceMapSettings)
  {
    Assert.expect(panelAlignmentSurfaceMapSettings != null);

    if (panelAlignmentSurfaceMapSettings == _panelAlignmentSurfaceMapSettings)
      return;

    PanelAlignmentSurfaceMapSettings oldValue = _panelAlignmentSurfaceMapSettings;
    _projectObservable.setEnabled(false);
    try
    {
      _panelAlignmentSurfaceMapSettings = panelAlignmentSurfaceMapSettings;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, panelAlignmentSurfaceMapSettings, PanelEventEnum.PANEL_ALIGNMENT_SURFACE_MAP_SETTINGS);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setPspSettings(PspSettings pspSettings)
  {
    Assert.expect(pspSettings != null);

    if (pspSettings == _pspSettings)
      return;

    PspSettings oldValue = _pspSettings;
    _projectObservable.setEnabled(false);
    try
    {
      _pspSettings = pspSettings;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, pspSettings, PanelEventEnum.PANEL_SURFACE_MAP_SETTINGS);
  }

  /**
   * @author Keith Lee
   */
  public PanelSettings getPanelSettings()
  {
    Assert.expect(_panelSettings != null);

    return _panelSettings;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public PanelSurfaceMapSettings getPanelSurfaceMapSettings()
  {
    Assert.expect(_panelSurfaceMapSettings != null);

    return _panelSurfaceMapSettings;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public PanelMeshSettings getPanelMeshSettings()
  {
    Assert.expect(_panelMeshSettings != null);
    
    return _panelMeshSettings;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public boolean hasPanelSurfaceMapSettings()
  {
    if(_panelSurfaceMapSettings == null)
      return false;
    
    return true;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasPanelMeshSettings()
  {
    if(_panelMeshSettings == null)
      return false;
    
    return true;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public PanelAlignmentSurfaceMapSettings getPanelAlignmentSurfaceMapSettings()
  {
      Assert.expect(_panelAlignmentSurfaceMapSettings != null);
      return _panelAlignmentSurfaceMapSettings;
  }

  /**
   * @author Cheah Lee Herng
   */
  public PspSettings getPspSettings()
  {
    Assert.expect(_pspSettings != null);
    return _pspSettings;
  }
  
   /**
   * @author Cheah Lee Herng
   */
  public boolean hasPspSettings()
  {
    if (_pspSettings == null)
      return false;
    
    return true;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean hasPanelAlignmentSurfaceMapSettings()
  {
    if (_panelAlignmentSurfaceMapSettings == null)
      return false;
    
    return true;
  }
  
  /**
   * @return a list of all the Surface Map Points on this panel.
   * @author Jack Hwee
   */
  public List<OpticalRegion> getPanelOpticalRegions()
  {
    Assert.expect(_panelSurfaceMapSettings != null);
    List<OpticalRegion> opticalRegion = new ArrayList<OpticalRegion>();
  
    for (OpticalRegion panelOpticalRegion : _panelSurfaceMapSettings.getOpticalRegions())
    {
         opticalRegion.add(panelOpticalRegion);    
    }
    Collections.sort(opticalRegion, new AlphaNumericComparator());
    return opticalRegion;
  }
  
  /**
   * @return a list of all the Components on this panel.
   * @author Bill Darbie
   */
  public List<Component> getComponents()
  {
    List<Component> components = new ArrayList<Component>();
    for (Board board : _boardNameToBoardMap.values())
    {
      for (Component component : board.getComponents())
        components.add(component);
    }

    Collections.sort(components, new AlphaNumericComparator());
    return components;
  }
  
  /**
   * @return a list of all top side components on this panel.
   * @author Ying-Huan.Chu
   */
  public List<Component> getTopComponents()
  {
    List<Component> components = new ArrayList<>();
    for (Board board : _boardNameToBoardMap.values())
    {
      for (Component component : board.getComponents())
      {
        if (component.isTopSide())
          components.add(component);
      }
    }
    Collections.sort(components, new AlphaNumericComparator());
    return components;
  }
  
  /**
   * @return a list of all bottom side components on this panel.
   * @author Ying-Huan.Chu
   */
  public List<Component> getBottomComponents()
  {
    List<Component> components = new ArrayList<>();
    for (Board board : _boardNameToBoardMap.values())
    {
      for (Component component : board.getComponents())
      {
        if (component.isBottomSide())
          components.add(component);
      }
    }
    Collections.sort(components, new AlphaNumericComparator());
    return components;
  }
  
  /**
   * @author Bill Darbie
   */
  public List<ComponentType> getComponentTypes()
  {
    List<ComponentType> componentTypes = new ArrayList<ComponentType>();
    for (BoardType boardType : _boardTypeNameToBoardTypeMap.values())
    {
      componentTypes.addAll(boardType.getComponentTypes());
    }

    Collections.sort(componentTypes, new AlphaNumericComparator());
    return componentTypes;
  }

  /**
   * @return all subtypes, used or not.
   * @author Bill Darbie
   */
  public List<Subtype> getSubtypes()
  {
    return new ArrayList<Subtype>(_subtypeNameToSubtypeMap.values());
  }
  
  /**
   * This function returns a set of surface maps.
   * 
   * @author Cheah Lee Herng
   */
  public List<BoardSurfaceMapSettings> getSurfaceMaps()
  {
    return _surfaceMapList;  
  }

  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getUsedSubtypesUnsorted()
  {
    Set<Subtype> subtypeSet = new HashSet<Subtype>();
    for (BoardType boardType : getBoardTypes())
    {
      subtypeSet.addAll(boardType.getSubtypesUnsorted());
    }

    return subtypeSet;
  }

  /**
   * Get all subtypes that are set to some pad, independent of if the pad is loaded, inspected or
   * testable.
   * @author Bill Darbie
   */
  public List<Subtype> getUsedSubtypes()
  {
    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    for (Subtype subtype : getUsedSubtypesUnsorted())
    {
      boolean added = subtypeSet.add(subtype);
      Assert.expect(added);
    }

    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getUnusedSubtypesUnsorted()
  {
    Collection<Subtype> allSubtypes = _subtypeNameToSubtypeMap.values();
    Set<Subtype> usedSubtypeSet = new HashSet<Subtype>(getUsedSubtypesUnsorted());

    Set<Subtype> unusedSubtypes = new HashSet<Subtype>();
    for (Subtype subtype : allSubtypes)
    {
      if (usedSubtypeSet.contains(subtype) == false)
      {
        boolean added = unusedSubtypes.add(subtype);
        Assert.expect(added);
      }
    }

    return unusedSubtypes;
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getUnusedSubtypes()
  {
    Collection<Subtype> allSubtypes = _subtypeNameToSubtypeMap.values();
    Set<Subtype> usedSubtypeSet = new HashSet<Subtype>(getUsedSubtypes());

    List<Subtype> unusedSubtypes = new ArrayList<Subtype>();
    for (Subtype subtype : allSubtypes)
    {
      if (usedSubtypeSet.contains(subtype) == false)
        unusedSubtypes.add(subtype);
    }

    return unusedSubtypes;
  }

  /**
   * @author Yong Sheng Chuan
   * @return 
   */
  public List<Subtype> getLearntExposureSettingSubtypes()
  {
    Collection<Subtype> allSubtypes = getUsedSubtypes();
    List<Subtype> optimizedSubtypeSet = new ArrayList<Subtype>();

    for (Subtype subtype : allSubtypes)
    {
      if (subtype.getSubtypeAdvanceSettings().isAutoExposureSettingLearnt() && optimizedSubtypeSet.contains(subtype) == false)
        optimizedSubtypeSet.add(subtype);
    }

    return optimizedSubtypeSet;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public List<Subtype> getSubtypesWithoutExposureLearning()
  {
    Collection<Subtype> allSubtypes = getUsedSubtypes();
    List<Subtype> optimizedSubtypeSet = new ArrayList<Subtype>();

    for (Subtype subtype : allSubtypes)
    {
      if (subtype.getSubtypeAdvanceSettings().isAutoExposureSettingLearnt() == false && optimizedSubtypeSet.contains(subtype) == false)
        optimizedSubtypeSet.add(subtype);
    }

    return optimizedSubtypeSet;
  }
  
  /**
   * @return all Subtypes that have at least one Pad the is going to be inspected.
   * @author Bill Darbie
   */
  public Set<Subtype> getInspectedSubtypesUnsorted()
  {
    Set<Subtype> subtypeSet = new HashSet<Subtype>();
    for (Board board : getBoards())
    {
      subtypeSet.addAll(board.getInspectedSubtypesUnsorted());
    }

    return subtypeSet;
  }

  /**
   * @return all unimported Subtypes that have at least one Pad the is going to be inspected.
   * @author Wei Chin, Chong
   */
  public Set<Subtype> getInspectedAndUnImportedSubtypesUnsorted()
  {
    Set<Subtype> subtypeSet = new HashSet<Subtype>();
    for (Board board : getBoards())
    {
      subtypeSet.addAll(board.getInspectedAndUnImportedSubtypesUnsorted());
    }

    return subtypeSet;
  }

  /**
   * @return all Subtypes that have at least one Pad the is going to be inspected.
   * @author Bill Darbie
   */
  public List<Subtype> getInspectedSubtypes()
  {
    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    subtypeSet.addAll(getInspectedSubtypesUnsorted());

    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @return all JointTypeEnums that are assigned to at least one Pad the is going to be inspected.
   * @author Bill Darbie
   */
  public List<JointTypeEnum> getInspectedJointTypeEnums()
  {
    Set<JointTypeEnum> jointTypeEnumSet = new TreeSet<JointTypeEnum>(new AlphaNumericComparator());
    for (Board board : getBoards())
    {
      jointTypeEnumSet.addAll(board.getInspectedJointTypeEnums());
    }

    return new ArrayList<JointTypeEnum>(jointTypeEnumSet);
  }

  /**
   * @return all JointTypeEnums that have at least on Pad that is testable
   * @author Bill Darbie
   */
  public List<JointTypeEnum> getTestableJointTypeEnums()
  {
    Set<JointTypeEnum> jointTypeEnumSet = new TreeSet<JointTypeEnum>(new AlphaNumericComparator());
    for (Board board : getBoards())
    {
      jointTypeEnumSet.addAll(board.getTestableJointTypeEnums());
    }

    return new ArrayList<JointTypeEnum>(jointTypeEnumSet);
  }

  /**
   * @author Sunit Bhalla
   * Note: this method will ignore any subtypes that are not used.  Subtypes are unused when they are in the panel, but
   * not used on any board.
   */
  public Set<Subtype> getInspectedSubtypesWithoutShortLearnedUnsorted()
  {
    Set<Subtype> unlearnedSubtypeSet = new HashSet<Subtype>();
    for (Subtype subtype : getInspectedSubtypesUnsorted())
    {
      if (subtype.isJointSpecificDataLearned() == false)
        unlearnedSubtypeSet.add(subtype);
    }
    Assert.expect(unlearnedSubtypeSet != null);
    return unlearnedSubtypeSet;
  }

  /**
   * @author Sunit Bhalla
   * Note: this method will ignore any subtypes that are not used.  Subtypes are unused when they are in the panel, but
   * not used on any board.
   */
  public List<Subtype> getInspectedSubtypesWithoutShortLearned()
  {
    List<Subtype> unlearnedSubtypes = new ArrayList<Subtype>();
    for (Subtype subtype : getInspectedSubtypes())
    {
      if (subtype.isJointSpecificDataLearned() == false)
        unlearnedSubtypes.add(subtype);
    }
    Assert.expect(unlearnedSubtypes != null);
    return unlearnedSubtypes;
  }

  /**
   * @author George Booth
   * Note: this method will ignore any subtypes that are not used.  Subtypes are unused when they are in the panel, but
   * not used on any board.
   */
  public Set<Subtype> getInspectedSubtypesWithoutExpectedImagesLearnedUnsorted()
  {
    Set<Subtype> unlearnedSubtypeSet = new HashSet<Subtype>();
    for (Subtype subtype : getInspectedSubtypesUnsorted())
    {
      if (subtype.isExpectedImageDataLearned() == false)
        unlearnedSubtypeSet.add(subtype);
    }
    Assert.expect(unlearnedSubtypeSet != null);
    return unlearnedSubtypeSet;
  }

  /**
   * @author George Booth
   * Note: this method will ignore any subtypes that are not used.  Subtypes are unused when they are in the panel, but
   * not used on any board.
   */
  public List<Subtype> getInspectedSubtypesWithoutExpectedImagesLearned()
  {
    List<Subtype> unlearnedSubtypes = new ArrayList<Subtype>();
    for (Subtype subtype : getInspectedSubtypes())
    {
      if (subtype.hasExpectedImageDataToLearn() && subtype.isExpectedImageDataLearned() == false)
        unlearnedSubtypes.add(subtype);
    }
    Assert.expect(unlearnedSubtypes != null);
    return unlearnedSubtypes;
  }

  /**
   * @author Sunit Bhalla
   * Note: this method will ignore any subtypes that are not used.  Subtypes are unused when they are in the panel, but
   * not used on any board.
   */
  public Set<Subtype> getInspectedSubtypesWithoutAllAlgorithmsExceptShortLearnedUnsorted()
  {
    Set<Subtype> unlearnedSubtypeSet = new HashSet<Subtype>();
    for (Subtype subtype : getInspectedSubtypesUnsorted())
    {
      if (subtype.areAlgorithmSettingsLearned() == false)
        unlearnedSubtypeSet.add(subtype);
    }
    Assert.expect(unlearnedSubtypeSet != null);
    return unlearnedSubtypeSet;
  }

  /**
   * @author Sunit Bhalla
   * Note: this method will ignore any subtypes that are not used.  Subtypes are unused when they are in the panel, but
   * not used on any board.
   */
  public List<Subtype> getInspectedSubtypesWithoutAllAlgorithmsExceptShortLearned()
  {
    List<Subtype> unlearnedSubtypes = new ArrayList<Subtype>();
    for (Subtype subtype : getInspectedSubtypes())
    {
      if (subtype.areAlgorithmSettingsLearned() == false)
        unlearnedSubtypes.add(subtype);
    }
    Assert.expect(unlearnedSubtypes != null);
    return unlearnedSubtypes;
  }

  /**
   * @author Sunit Bhalla
   * This method returns the number of used subtypes on a panel.
   * Note: this method will ignore any subtypes that are not used.  Subtypes are unused when they are in the panel, but
   * not used on any board.
   */
  public int getNumberOfInspectedSubtypes()
  {
    return getInspectedSubtypesUnsorted().size();
  }

  /**
   * @author George Booth
   * This method returns the number of used subtypes on a panel.
   * Note: this method will ignore any subtypes that are not used.  Subtypes are unused when they are in the panel, but
   * not used on any board.
   */
  public int getNumberOfInspectedSubtypesWithExpectedImageLearning()
  {
    int subtypeCount = 0;
    for (Subtype subtype : getInspectedSubtypesUnsorted())
    {
      if (subtype.hasExpectedImageDataToLearn())
        subtypeCount++;
    }
    return subtypeCount;
  }

  /**
   * @author Sunit Bhalla
   * Note: this method will ignore any subtypes that are not used.  Subtypes are unused when they are in the panel, but
   * not used on any board.
   */
  public int getNumberOfInspectedSubtypesWithShortLearned()
  {
    int subtypeCount = 0;
    for (Subtype subtype : getInspectedSubtypesUnsorted())
    {
      if (subtype.isJointSpecificDataLearned())
        subtypeCount++;
    }
    return subtypeCount;
  }

  /**
   * @author George Booth
   * Note: this method will ignore any subtypes that are not used.  Subtypes are unused when they are in the panel, but
   * not used on any board.
   */
  public int getNumberOfInspectedSubtypesWithExpectedImageLearned()
  {
    int subtypeCount = 0;
    for (Subtype subtype : getInspectedSubtypesUnsorted())
    {
      if (subtype.isExpectedImageDataLearned())
        subtypeCount++;
    }
    return subtypeCount;
  }
  
  /**
   * @author sheng chuan
   * Note: this method will ignore any subtypes that are not used.  Subtypes are unused when they are in the panel, but
   * not used on any board.
   */
  public int getNumberOfInspectedSubtypesWithImageTemplateLearned()
  {
    int subtypeCount = 0;
    for (Subtype subtype : getInspectedSubtypesUnsorted())
    {
      if (subtype.isExpectedImageTemplateLearned())
        subtypeCount++;
    }
    return subtypeCount;
  }

  /**
   * @author Sunit Bhalla
   * Note: this method will ignore any subtypes that are not used.  Subtypes are unused when they are in the panel, but
   * not used on any board.
   */
  public int getNumberOfInspectedSubtypesWithAllAlgorithmsExceptShortLearned()
  {
    int subtypeCount = 0;
    for (Subtype subtype : getInspectedSubtypesUnsorted())
    {
      if (subtype.areAlgorithmSettingsLearned())
        subtypeCount++;
    }
    return subtypeCount;
  }

  /**
   * @author Bill Darbie
   */
  public Set<PadType> getPadTypesUnsorted()
  {
    Set<PadType> padTypeSet = new HashSet<PadType>();
    for (ComponentType componentType : getComponentTypes())
    {
      for (PadType padType : componentType.getPadTypes())
      {
        boolean added = padTypeSet.add(padType);
        Assert.expect(added);
      }
    }

    return padTypeSet;
  }

  /**
   * @author Bill Darbie
   */
  public List<PadType> getPadTypes()
  {
    Set<PadType> padTypeSet = new TreeSet<PadType>(new PadTypeComparator());
    for (ComponentType componentType : getComponentTypes())
    {
      for (PadType padType : componentType.getPadTypes())
      {
        boolean added = padTypeSet.add(padType);
        Assert.expect(added);
      }
    }

    return new ArrayList<PadType>(padTypeSet);
  }

  /**
   * Create a new Subtype.
   * @author Bill Darbie
   */
  public Subtype createSubtype(String subtypeName, LandPattern landPattern, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(subtypeName != null);
    Assert.expect(jointTypeEnum != null);
    Assert.expect(landPattern != null);

    Subtype subtype = null;
    _projectObservable.setEnabled(false);
    try
    {
      String longName = getUniqueSubtypeLongName(landPattern, jointTypeEnum);
      subtype = Subtype.createNewSubtype(this, longName, subtypeName, jointTypeEnum);
      subtype.setLandPattern(landPattern);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(subtype, null, subtype, SubtypeEventEnum.CREATE_OR_DESTROY);

    Assert.expect(subtype != null);
    return subtype;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isSubtypeInUse(Subtype subtype)
  {
    Assert.expect(subtype != null);

    Set<Subtype> subtypeSet = new HashSet<Subtype>(getUsedSubtypes());
    if (subtypeSet.contains(subtype))
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public void removeSubtype(Subtype subtype)
  {
    Assert.expect(subtype != null);

    _projectObservable.setEnabled(false);
    try
    {
      // check that the subtype is not referenced by any pads
      Assert.expect(isSubtypeInUse(subtype) == false);

      Subtype prev = _subtypeNameToSubtypeMap.remove(subtype.getLongName());
      Assert.expect(prev != null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, subtype, null, PanelEventEnum.ADD_OR_REMOVE_SUBTYPE);
  }

  /**
   * @author Bill Darbie
   */
  public void removeCompPackage(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);

    _projectObservable.setEnabled(false);
    try
    {
      CompPackage prev = _compPackageNameToCompPackageMap.remove(compPackage.getLongName());
      Assert.expect(prev != null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, compPackage, null, PanelEventEnum.ADD_OR_REMOVE_COMP_PACKAGE);
  }

  /**
   * @author Bill Darbie
   */
  public boolean doesSubtypeExist(String subtypeOrigName)
  {
    Assert.expect(subtypeOrigName != null);

    return _subtypeNameToSubtypeMap.containsKey(subtypeOrigName);
  }
  
   /**
   * This checking is to avoid duplicate subtype short name created
   * @author Jack Hwee
   */
  public boolean doesSubtypeShortNameAndJointTypeExist(String subtypeOrigName, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(subtypeOrigName != null);
    Assert.expect(jointTypeEnum != null);
    
    boolean subtypeExist = false;
    for (Map.Entry<String, Subtype> entry : _subtypeNameToSubtypeMap.entrySet())
    {
      Subtype subtype = entry.getValue();
      
      if (subtype.getJointTypeEnum().equals(jointTypeEnum) && subtype.getShortName().equals(subtypeOrigName))
      {
        subtypeExist = true;
        break;
      }
    }
    
    return subtypeExist;
  }

  /**
   * @author Bill Darbie
   */
  public Subtype getSubtype(String subtypeOrigName)
  {
    Assert.expect(subtypeOrigName != null);

    Subtype subtype = _subtypeNameToSubtypeMap.get(subtypeOrigName);

    Assert.expect(subtype != null);
    return subtype;
  }

  /**
   * @author Bill Darbie
   */
  public int getNumComponents()
  {
    int numComponents = 0;
    for (Board board : _boardNameToBoardMap.values())
    {
      numComponents += board.getNumComponents();
    }

    return numComponents;
  }

  /**
   * @author Bill Darbie
   */
  public int getNumLoadedComponents()
  {
    int numLoadedComponents = 0;
    for (Board board : _boardNameToBoardMap.values())
    {
      numLoadedComponents += board.getNumLoadedComponents();
    }

    return numLoadedComponents;
  }

  /**
   * @author Bill Darbie
   */
  public int getNumTestedComponents()
  {
    int numTestedComponents = 0;
    for (Board board : _boardNameToBoardMap.values())
    {
      numTestedComponents += board.getNumTestedComponents();
    }

    return numTestedComponents;
  }

  /**
   * @return the total number of joints on the panel (tested or not).
   * @author Bill Darbie
   */
  public int getNumJoints()
  {
    int jointCount = 0;

    for (Board board : _boardNameToBoardMap.values())
    {
      jointCount += board.getNumJoints();
    }

    return jointCount;
  }

  /**
   * @return the total number of tested joints on the panel
   * @author Bill Darbie
   */
  public int getNumInspectedJoints()
  {
    int jointCount = 0;

    for (Board board : _boardNameToBoardMap.values())
    {
      jointCount += board.getNumInspectedJoints();
    }

    return jointCount;
  }
  
   /**
   * @return the total number of untestable joints on the panel
   * @author Chnee Khang Wah, XCR1374
   * @2011-09-08
   */
  public int getNumUnTestableJoints()
  {
    int jointCount = 0;

    for (Board board : _boardNameToBoardMap.values())
    {
      jointCount += board.getNumUnTestableJoints();
    }

    return jointCount;
  }

  /**
   * @return the total number of inspected joints, no test joint, untestable joints on the panel
   * @author Chnee Khang Wah, XCR1374
   * @2011-09-12
   */
  public void gatherJointsCountDetail()
  {
      for (Board board : _boardNameToBoardMap.values())
          board.gatherJointsCountDetail();
  }
  
  /**
   * @return the total number of inspected joints on the panel
   * @author Chnee Khang Wah, XCR1374
   * @2011-09-12
   */
  public int getNumInspectedJointsFast()
  {
      int jointCount = 0;
      
      for (Board board : _boardNameToBoardMap.values())
          jointCount += board.getNumInspectedJointsFast();
      
      return jointCount ;
  }
  
  /**
   * @return the total number of untestable joints on the panel
   * @author Chnee Khang Wah, XCR1374
   * @2011-09-12
   */
  public int getNumUnTestableJointsFast()
  {
      int jointCount = 0;
      
      for (Board board : _boardNameToBoardMap.values())
          jointCount += board.getNumUnTestableJointsFast();
      
      return jointCount ;
  }
  
  /**
   * Set the measured pad to pad thickness of the panel here.
   * @author Bill Darbie
   */
  public void setThicknessInNanoMeters(int thicknessInNanoMeters)
  {
    Assert.expect(thicknessInNanoMeters >= 0);

    if (thicknessInNanoMeters == _thicknessInNanoMeters)
      return;

    int oldValue = _thicknessInNanoMeters;
    _projectObservable.setEnabled(false);
    try
    {
      _thicknessInNanoMeters = thicknessInNanoMeters;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, thicknessInNanoMeters, PanelEventEnum.THICKNESS);
  }

  /**
   * Get the measured pad to pad thickness of the panel here.
   * @author Bill Darbie
   * @Kee Chin Seong - Added the shrink panel thickness in order to get correct focus
   */
  public int getThicknessInNanometers()
  {
    Assert.expect(_thicknessInNanoMeters != -1);

    if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
        return _thicknessInNanoMeters + XrayTester.getSystemPanelThicknessOffset();//Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on";
    else
        return _thicknessInNanoMeters;
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  public int getMeansBoardZOffsetInNanometers()
  {
    int means = 0;
    int sizeOfBoards =  _boardNameToBoardMap.size();
    int sumOfZOffset = 0;
    for(Board board : _boardNameToBoardMap.values())
    {
      sumOfZOffset += board.getBoardSettings().getBoardZOffsetInNanometers();
      
      if(sizeOfBoards == 1)
        return sumOfZOffset;
    }
    means = MathUtil.roundDoubleToInt ((double) sumOfZOffset / sizeOfBoards);
    
    return means;
  }

  /**
   * Call this method to remove a Fiducial from the Panel.
   * @author Bill Darbie
   */
  public void addFiducial(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);

    _projectObservable.setEnabled(false);
    try
    {
      Fiducial prev = _fiducialNameToFiducialMap.put(fiducial.getName(), fiducial);
      Assert.expect(prev == null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, fiducial, PanelEventEnum.ADD_OR_REMOVE_FIDUCIAL);
  }

  /**
   * Call this method to remove a Fiducial from the Panel.
   * @author Bill Darbie
   */
  public void removeFiducial(Fiducial fiducial)
  {
    Assert.expect(fiducial != null);

    _projectObservable.setEnabled(false);
    try
    {
      Fiducial prev = _fiducialNameToFiducialMap.remove(fiducial.getName());
      Assert.expect(prev != null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, fiducial, null, PanelEventEnum.ADD_OR_REMOVE_FIDUCIAL);
  }

  /**
   * Create a new Panel Fiducial for a Project that has already been loaded.
   * @author Bill Darbie
   */
  public Fiducial createFiducial(String fiducialName, boolean topSide)
  {
    Assert.expect(fiducialName != null);

    Fiducial fiducial = null;
    _projectObservable.setEnabled(false);
    try
    {
      fiducial = new Fiducial();
      FiducialType fiducialType = new FiducialType();
      fiducialType.setCoordinateInNanoMeters(new PanelCoordinate(0, 0));
      fiducialType.setWidthInNanoMeters(MathUtil.convertMilsToNanoMetersInteger(100));
      fiducialType.setLengthInNanoMeters(MathUtil.convertMilsToNanoMetersInteger(100));
      fiducialType.setShapeEnum(ShapeEnum.CIRCLE);

      fiducial.setFiducialType(fiducialType);

      if (_panelSettings.isFlipped())
        topSide = !topSide;

      fiducial.setPanel(this, topSide);
      fiducialType.setName(fiducialName);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(fiducial, null, fiducial, FiducialEventEnum.CREATE_OR_DESTROY);

    Assert.expect(fiducial != null);
    return fiducial;
  }

  /**
   * @author Bill Darbie
   */
  public List<Fiducial> getFiducials()
  {
    Assert.expect(_fiducialNameToFiducialMap != null);

    return new ArrayList<Fiducial>(_fiducialNameToFiducialMap.values());
  }

  /**
   * @author Andy Mechtenberg
   */
  public List<FiducialType> getFiducialTypes()
  {
    List<FiducialType> fiducialTypes = new ArrayList<FiducialType>();

    List<Fiducial> fiducials = getFiducials();
    for(Fiducial fiducial : fiducials)
      fiducialTypes.add(fiducial.getFiducialType());

    return fiducialTypes;
  }

  /**
   * @author Bill Darbie
   */
  public List<Fiducial> getTopSideFiducials()
  {
    Set<Fiducial> fiducialSet = new TreeSet<Fiducial>(new AlphaNumericComparator());
    for (Fiducial fiducial : _fiducialNameToFiducialMap.values())
    {
      if (fiducial.isTopSide())
      {
        boolean added = fiducialSet.add(fiducial);
        Assert.expect(added);
      }
    }
    Assert.expect(fiducialSet != null);
    return new ArrayList<Fiducial>(fiducialSet);
  }

  /**
   * @author Bill Darbie
   */
  public List<Fiducial> getBottomSideFiducials()
  {
    Set<Fiducial> fiducialSet = new TreeSet<Fiducial>(new AlphaNumericComparator());
    for (Fiducial fiducial : _fiducialNameToFiducialMap.values())
    {
      if (fiducial.isBottomSide())
      {
        boolean added = fiducialSet.add(fiducial);
        Assert.expect(added);
      }
    }
    Assert.expect(fiducialSet != null);
    return new ArrayList<Fiducial>(fiducialSet);
  }

  /**
   * @author Bill Darbie
   */
  public Set<Pad> getPadsUnsorted()
  {
    Set<Pad> padSet = new HashSet<Pad>();

    for (Board board : getBoards())
    {
      for (Pad pad : board.getPadsUnsorted())
      {
        boolean added = padSet.add(pad);
        Assert.expect(added);
      }
    }

    return padSet;
  }

  /**
   * WARNING: this can be a slow call, if you do not need Pads sorted alphabetically
   * you would be better off using getUnsortedPads().
   *
   * @author Bill Darbie
   */
  public List<Pad> getPads()
  {
    Set<Pad> padSet = new TreeSet<Pad>(new PadComparator());

    for (Pad pad : getPadsUnsorted())
    {
      boolean added = padSet.add(pad);
      Assert.expect(added);
    }

    return new ArrayList<Pad>(padSet);
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasBoard(String boardName)
  {
    Assert.expect(boardName != null);
    return _boardNameToBoardMap.containsKey(boardName);
  }

  /**
   * @author Bill Darbie
   */
  public Board getBoard(String boardName)
  {
    Assert.expect(boardName != null);

    Board board = _boardNameToBoardMap.get(boardName);
    Assert.expect(board != null);
    return board;
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasComponent(String boardName, String refDes)
  {
    if (hasBoard(boardName) == false)
      return false;

    Board board = getBoard(boardName);
    if (board.hasComponent(refDes) == false)
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  public Component getComponent(String boardName, String refDes)
  {
    Assert.expect(boardName != null);
    Assert.expect(refDes != null);

    Board board = getBoard(boardName);
    Component component = board.getComponent(refDes);

    return component;
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasPad(String boardName, String refDes, String padName)
  {
    if (hasComponent(boardName, refDes) == false)
      return false;

    Component component = getComponent(boardName, refDes);
    if (component.hasPad(padName) == false)
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  public Pad getPad(String boardName, String refDes, String padName)
  {
    Assert.expect(boardName != null);
    Assert.expect(refDes != null);
    Assert.expect(padName != null);

    Component component = getComponent(boardName, refDes);
    Pad pad = component.getPad(padName);

    return pad;
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasBoardType(String boardTypeName)
  {
    return _boardTypeNameToBoardTypeMap.containsKey(boardTypeName);
  }

  /**
   * @author Bill Darbie
   */
  public BoardType getBoardType(String boardTypeName)
  {
    Assert.expect(boardTypeName != null);

    BoardType boardType = _boardTypeNameToBoardTypeMap.get(boardTypeName);
    Assert.expect(boardType != null);
    return boardType;
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasComponentType(String boardTypeName, String refDes)
  {
    if (hasBoardType(boardTypeName) == false)
      return false;

    BoardType boardType = getBoardType(boardTypeName);
    if (boardType.hasComponentType(refDes) == false)
      return false;

    return true;
  }

  /**
   * @author Bill Darbie
   */
  public ComponentType getComponentType(String boardTypeName, String refDes)
  {
    Assert.expect(boardTypeName != null);
    Assert.expect(refDes != null);

    BoardType boardType = getBoardType(boardTypeName);
    ComponentType componentType = boardType.getComponentType(refDes);

    return componentType;
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasPadType(String boardTypeName, String refDes, String padName)
  {
    Assert.expect(boardTypeName != null);
    Assert.expect(refDes != null);
    Assert.expect(padName != null);

    ComponentType componentType = getComponentType(boardTypeName, refDes);
    if (componentType.hasPadType(padName) == false)
    {
      return false;
    }

   return true;
  }

  /**
   * @author Bill Darbie
   */
  public PadType getPadType(String boardTypeName, String refDes, String padName)
  {
    Assert.expect(boardTypeName != null);
    Assert.expect(refDes != null);
    Assert.expect(padName != null);

    ComponentType componentType = getComponentType(boardTypeName, refDes);
    PadType padType = componentType.getPadType(padName);

    return padType;
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasFiducial(String fiducialName)
  {
    Assert.expect(fiducialName != null);
    return _fiducialNameToFiducialMap.containsKey(fiducialName);
  }

  /**
   * @return a panel based fiducial.
   * @author Bill Darbie
   */
  public Fiducial getFiducial(String fiducialName)
  {
    Assert.expect(fiducialName != null);

    Fiducial fiducial = _fiducialNameToFiducialMap.get(fiducialName);
    Assert.expect(fiducial !=  null);
    return fiducial;
  }

  /**
   * @author Bill Darbie
   */
  public boolean hasFiducial(String boardName, String fiducialName)
  {
    if (hasBoard(boardName) == false)
      return false;

    Board board = getBoard(boardName);
    if (board.hasFiducial(fiducialName) == false)
      return false;

    return true;
  }

  /**
   * @return a board based fiducial.
   * @author Bill Darbie
   */
  public Fiducial getFiducial(String boardName, String fiducialName)
  {
    Assert.expect(boardName != null);
    Assert.expect(fiducialName != null);

    Board board = getBoard(boardName);
    Fiducial fiducial = board.getFiducial(fiducialName);
    Assert.expect(fiducial != null);
    return fiducial;
  }

  /**
   * @author Bill Darbie
   */
  public List<JointTypeEnum> getJointTypeEnums()
  {
    Set<JointTypeEnum> jointTypeEnumSet = new TreeSet<JointTypeEnum>(new AlphaNumericComparator());
    for (Board board : getBoards())
    {
      jointTypeEnumSet.addAll(board.getJointTypeEnums());
    }

    return new ArrayList<JointTypeEnum>(jointTypeEnumSet);
  }

  /**
   * @return a List of Subtype objects that all come from the same
   * algorithmFamilyName on this Panel and are being used by at least one Pad that is being inspected.
   * @author George A. David
   */
  public List<Subtype> getInspectedSubtypes(JointTypeEnum jointTypeEnum)
  {
    Set<Subtype> subtypes = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    for (Board board : getBoards())
    {
      subtypes.addAll(board.getInspectedSubtypes(jointTypeEnum));
    }

    Assert.expect(subtypes != null);
    return new ArrayList<Subtype>(subtypes);
  }

  /**
   * @return a List of Subtype objects that all come from the same
   * algorithmFamilyName on this Panel , un-imported from any library
   * and are being used by at least one Pad that is being inspected.
   * @author Wei Chin, Chong
   */
  public List<Subtype> getInspectedAndUnImportedSubtypes(JointTypeEnum jointTypeEnum)
  {
    Set<Subtype> subtypes = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    for (Board board : getBoards())
    {
      subtypes.addAll(board.getInspectedAndUnImportedSubtypes(jointTypeEnum));
    }

    Assert.expect(subtypes != null);
    return new ArrayList<Subtype>(subtypes);
  }

  /**
   * @return all Subtypes that have at least on Pad that is testable
   * @author Bill Darbie
   */
  public Set<Subtype> getTestableSubtypesUnsorted(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    Set<Subtype> subtypes = new HashSet<Subtype>();
    for (Board board : getBoards())
    {
      subtypes.addAll(board.getTestableSubtypesUnsorted(jointTypeEnum));
    }

    return subtypes;
  }

  /**
   * @return all Subtypes that have at least on Pad that is testable
   * @author Bill Darbie
   */
  public List<Subtype> getTestableSubtypes(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    Set<Subtype> subtypes = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    for (Board board : getBoards())
    {
      subtypes.addAll(board.getTestableSubtypes(jointTypeEnum));
    }

    return new ArrayList<Subtype>(subtypes);
  }

  /**
   * @author Bill Darbie
   */
  public Set<Subtype> getSubtypesUnsorted(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    Set<Subtype> subtypeSet = new HashSet<Subtype>();
    for (Board board : getBoards())
    {
      subtypeSet.addAll(board.getSubtypesUnsorted(jointTypeEnum));
    }
    Assert.expect(subtypeSet != null);
    return subtypeSet;
  }
  
  /**
   * @author Wei Chin
   */
  public Set<Subtype> getSubtypesUnsorted(LandPattern landPattern, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(landPattern != null);

    Set<Subtype> subtypeSet = new HashSet<Subtype>();
    for (Board board : getBoards())
    {
      subtypeSet.addAll(board.getSubtypesUnsorted(landPattern, jointTypeEnum));
    }
    Assert.expect(subtypeSet != null);
    return subtypeSet;
  }

  /**
   * @author Bill Darbie
   */
  public List<Subtype> getSubtypes(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    Set<Subtype> subtypeSet = new TreeSet<Subtype>(new SubtypeAlphaNumericComparator());
    subtypeSet.addAll(getSubtypesUnsorted(jointTypeEnum));

    Assert.expect(subtypeSet != null);
    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Bill Darbie
   */
  boolean doesCompPackageExist(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);

    return doesCompPackageExist(compPackage.getLongName());
  }

  /**
   * @author Bill Darbie
   */
  boolean doesCompPackageExist(String compPackageName)
  {
    Assert.expect(compPackageName != null);

    return _compPackageNameToCompPackageMap.containsKey(compPackageName);
  }

  /**
   * @author Bill Darbie
   */
  String getLongCompPackageName(LandPattern landPattern, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(landPattern != null);
    Assert.expect(jointTypeEnum != null);

    return landPattern.getName() + "_" + jointTypeEnum.getNameWithoutSpaces();
  }



  /**
   * @author Bill Darbie
   */
  String getUniqueSubtypeLongName(LandPattern landPattern, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(landPattern != null);
    Assert.expect(jointTypeEnum != null);

    // find a unique name
    String longSubtypeName = getLongSubtypeName(landPattern, jointTypeEnum);

    Set<String> subtypeNames = new HashSet<String>();
    for (Subtype subtype: getSubtypes())
    {
      boolean added = subtypeNames.add(subtype.getLongName());
      Assert.expect(added);
    }

    String subtypeName = longSubtypeName;
    int i = 0;
    while (subtypeNames.contains(subtypeName))
    {
      ++i;
      subtypeName = longSubtypeName + "_" + Integer.toString(i);
    }

    return subtypeName;
  }

  /**
   * @author Bill Darbie
   */
  public String getUniqueCompPackageLongName(LandPattern landPattern, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(landPattern != null);
    Assert.expect(jointTypeEnum != null);

    // find a unique name
    String longCompPackageName = getLongCompPackageName(landPattern, jointTypeEnum);

    Set<String> compPackageNames = new HashSet<String>();
    for (CompPackage compPackage: getCompPackages())
    {
      boolean added = compPackageNames.add(compPackage.getLongName());
      Assert.expect(added);
    }

    String compPackageName = longCompPackageName;
    int i = 0;
    while (compPackageNames.contains(compPackageName))
    {
      ++i;
      compPackageName = longCompPackageName + "_" + Integer.toString(i);
    }

    return compPackageName;
  }

  /**
   * @author Bill Darbie
   */
  public boolean doesCompPackageExist(LandPattern landPattern, JointTypeEnum jointTypeEnum)
  {
    List<JointTypeEnum> jointTypeEnums = new ArrayList<JointTypeEnum>();
    int numPins = landPattern.getLandPatternPads().size();
    for (int i = 0; i < numPins; ++i)
      jointTypeEnums.add(jointTypeEnum);

    CompPackage matchingCompPackage = getCompPackageIfItMatches(landPattern, jointTypeEnums);
    if (matchingCompPackage == null)
      return false;

    return true;
  }

  /**
   * @return null if no CompPackage matches, otherwise return the matching CompPackage
   * @author Bill Darbie
   */
  CompPackage getCompPackageIfItMatches(LandPattern landPattern, List<JointTypeEnum> sortedJointTypeEnums)
  {
    Assert.expect(landPattern != null);
    Assert.expect(sortedJointTypeEnums != null);

    CompPackage matchingCompPackage = null;
    // see if any existing compPackages will match what we need
    for (CompPackage aCompPackage : getCompPackages())
    {
      if (aCompPackage.getLandPattern() != landPattern)
        continue;

      List<PackagePin> aPackagePins = aCompPackage.getPackagePinsUnsorted();
      if (aPackagePins.size() != sortedJointTypeEnums.size())
        continue;

      // we need a sorted list of package pins - this call is slower, so we use it now instead of a few lines above
      aPackagePins = aCompPackage.getPackagePins();

      boolean allPinsMatch = true;
      Iterator<JointTypeEnum> jointTypeEnumIt = sortedJointTypeEnums.iterator();
      for (PackagePin aPackagePin : aPackagePins)
      {
        JointTypeEnum jointTypeEnum = jointTypeEnumIt.next();
        if (jointTypeEnum.equals(aPackagePin.getJointTypeEnum()) == false)
        {
          allPinsMatch = false;
          break;
        }
      }

      if (allPinsMatch)
      {
        matchingCompPackage = aCompPackage;
        break;
      }
    }

    // this can be null
    return matchingCompPackage;
  }

  /**
   * @author Bill Darbie
   */
  CompPackage getCompPackage(String compPackageName)
  {
    Assert.expect(compPackageName != null);

    CompPackage compPackage = (CompPackage)_compPackageNameToCompPackageMap.get(compPackageName);
    Assert.expect(compPackage != null);

    return compPackage;
  }

  /**
   * @author Bill Darbie
   */
  Subtype getSubtypeCreatingIfNecessary(LandPattern landPattern, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(landPattern != null);
    Assert.expect(jointTypeEnum != null);

    Subtype subtype = null;
    String shortName = getShortSubtypeName(landPattern);
    String longName = getLongSubtypeName(landPattern, jointTypeEnum);
    if (doesSubtypeExist(longName))
      subtype = getSubtype(longName);
    else
      subtype = Subtype.createNewSubtype(this, longName, shortName, jointTypeEnum);

    subtype.setLandPattern(landPattern);
    Assert.expect(subtype != null);
    return subtype;
  }

  /* beehoon */
  Subtype get5DXSubtypeCreatingIfNecessary(LandPattern landPattern, JointTypeEnum jointTypeEnum, String subtypeName)
  {
    Assert.expect(landPattern != null);
    Assert.expect(jointTypeEnum != null);

    Subtype subtype = null;
    String shortName = get5DXShortSubtypeName(subtypeName);
    String longName = get5DXLongSubtypeName(jointTypeEnum, subtypeName);
    if (doesSubtypeExist(longName))
      subtype = getSubtype(longName);
    else
      subtype = Subtype.createNewSubtype(this, longName, shortName, jointTypeEnum);

    subtype.setLandPattern(landPattern);
    Assert.expect(subtype != null);
    return subtype;
  }
  
  /**
   * @author Bill Darbie
   */
  String getLongSubtypeName(LandPattern landPattern, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(landPattern != null);
    Assert.expect(jointTypeEnum != null);
    return landPattern.getName() + "_" + jointTypeEnum.getNameWithoutSpaces();
  }

  /**
   * @author Bee Hoon
   */
  String get5DXLongSubtypeName(JointTypeEnum jointTypeEnum, String subtypeName)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(subtypeName != null);
    
    return subtypeName + "_" + jointTypeEnum.getNameWithoutSpaces();
  }
  
  /**
   * @author Bill Darbie
   */
  String getShortSubtypeName(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);
    return landPattern.getName();
  }

  /* beehoon */
  String get5DXShortSubtypeName(String subtypeName) 
  {
    Assert.expect(subtypeName != null);
    return subtypeName;
  }

  /**
   * @author Bill Darbie
   */
  public boolean doesLandPatternExist(String landPatternName)
  {
    Assert.expect(landPatternName != null);

    Assert.expect(_landPatternNameToLandPatternMap != null);
    return _landPatternNameToLandPatternMap.containsKey(landPatternName);
  }

  /**
   * @author Bill Darbie
   */
  public boolean doesFiducialExist(String fiducialName)
  {
    Assert.expect(fiducialName != null);

    return _fiducialNameToFiducialMap.containsKey(fiducialName);
  }

  /**
   * @author Bill Darbie
   * @author Kee Chin Seong
   */
  public void addLandPattern(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);

    _projectObservable.setEnabled(false);
    try
    {
      Object previousObject = null;
      if(_landPatternNameToLandPatternMap.containsKey(landPattern.getName()))
      {
        _landPatternNameToLandPatternMap.remove(landPattern.getName());
      }
      previousObject = _landPatternNameToLandPatternMap.put(landPattern.getName(), landPattern);
      Assert.expect(previousObject == null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, landPattern, PanelEventEnum.ADD_OR_REMOVE_LAND_PATTERN);
  }

  /**
   * @author Bill Darbie
   */
  public void removeLandPattern(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);

    for (ComponentType componentType : getComponentTypes())
    {
      Assert.expect(componentType.getLandPattern() != landPattern);
    }
    removeLandPattern(landPattern.getName());
  }

  /**
   * @author Bill Darbie
   */
  private void removeLandPattern(String landPatternName)
  {
    Assert.expect(landPatternName != null);

    LandPattern removedLandPattern = null;
    _projectObservable.setEnabled(false);
    try
    {
      Assert.expect(_landPatternNameToLandPatternMap != null);
      removedLandPattern = _landPatternNameToLandPatternMap.remove(landPatternName);
      Assert.expect(removedLandPattern != null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, removedLandPattern, null, PanelEventEnum.ADD_OR_REMOVE_LAND_PATTERN);
  }

  /**
   * @author Bill Darbie
   */
  public LandPattern createLandPattern(String landPatternName)
  {
    Assert.expect(landPatternName != null);

    _projectObservable.setEnabled(false);
    LandPattern landPattern = null;
    try
    {
      landPattern = new LandPattern();
      landPattern.setPanel(this);
      landPattern.setName(landPatternName);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, landPattern, LandPatternEventEnum.CREATE_OR_DESTROY);

    Assert.expect(landPattern != null);
    return landPattern;
  }

  /**
   * @author Bill Darbie
   */
  public List<LandPattern> getLandPatterns()
  {
    return new ArrayList<LandPattern>(_landPatternNameToLandPatternMap.values());
  }

  /**
   * @author Laura Cormos
   */
  public LandPattern getLandPattern(String landPatternName)
  {
    Assert.expect(landPatternName != null);

    LandPattern landPattern = _landPatternNameToLandPatternMap.get(landPatternName);
    Assert.expect(landPattern != null);
    return landPattern;
  }

  /**
   * @author Bill Darbie
   */
  public List<LandPattern> getUnusedLandPatterns()
  {
    Collection<LandPattern> allLandPatterns = _landPatternNameToLandPatternMap.values();
    Set<LandPattern> usedLandPatternSet = new HashSet<LandPattern>(getUsedLandPatterns());

    List<LandPattern> unusedLandPatterns = new ArrayList<LandPattern>();
    for (LandPattern landPattern : allLandPatterns)
    {
      if (usedLandPatternSet.contains(landPattern) == false)
        unusedLandPatterns.add(landPattern);
    }

    return unusedLandPatterns;
  }

  /**
   * @return all LandPatterns that are set to some pad, independent of if the pad is loaded, inspected or
   * testable.
   * @author Bill Darbie
   */
  public List<LandPattern> getUsedLandPatterns()
  {
    Set<LandPattern> landPatterns = new TreeSet<LandPattern>(new AlphaNumericComparator());

    List<BoardType> boardTypes = getBoardTypes();
    for (BoardType boardType : boardTypes)
    {
      List<ComponentType> componentTypes = boardType.getComponentTypes();
      for (ComponentType componentType : componentTypes)
      {
        landPatterns.add(componentType.getLandPattern());
      }
    }

    return new ArrayList<LandPattern>(landPatterns);
  }

  /**
   * @author Bill Darbie
   */
  public List<LandPattern> getAllLandPatterns()
  {
    return new ArrayList<LandPattern>(_landPatternNameToLandPatternMap.values());
  }

  /**
   * @author Bill Darbie
   */
  public int getDegreesRotationRelativeToCad()
  {
    Assert.expect(_panelSettings != null);

    return _panelSettings.getDegreesRotationRelativeToCad();
  }

  /**
   * @return the degrees rotation after all rotations from this Panel have been
   * applied.
   *
   * @author Bill Darbie
   */
  public int getDegreesRotationAfterAllRotations()
  {
    Assert.expect(_panelSettings != null);
    return _panelSettings.getDegreesRotationAfterAllRotations();
  }

  /**
   * @author Bill Darbie
   */
  public boolean isFlipped()
  {
    Assert.expect(_panelSettings != null);
    return _panelSettings.isFlipped();
  }

  /**
   * @author Bill Darbie
   */
  public int getMaxPanelWidthInNanoMeters() throws XrayTesterException
  {
    if (_panelHandler == null)
      _panelHandler = PanelHandler.getInstance();

    return _panelHandler.getMaximumPanelWidthInNanoMeters();
  }

  /**
   * @author Bill Darbie
   */
  public int getMinPanelWidthInNanoMeters() throws XrayTesterException
  {
    if (_panelHandler == null)
      _panelHandler = PanelHandler.getInstance();

    return _panelHandler.getMinimumPanelWidthInNanoMeters();
  }

  /**
   * @author Bill Darbie
   */
  public int getMaxPanelLengthInNanoMeters() throws XrayTesterException
  {
    if (_panelHandler == null)
      _panelHandler = PanelHandler.getInstance();

    return _panelHandler.getMaximumPanelLengthInNanoMeters();
  }

  /**
   * @author Bill Darbie
   */
  public int getMinPanelLengthInNanoMeters()
  {
    if (_panelHandler == null)
      _panelHandler = PanelHandler.getInstance();

    return _panelHandler.getMinimumPanelLengthInNanoMeters();
  }

  /**
   * @author Bill Darbie
   */
  public int getMaxPanelThicknessInNanoMeters() throws XrayTesterException
  {
    if (_panelHandler == null)
      _panelHandler = PanelHandler.getInstance();

    return _panelHandler.getMaximumPanelThicknessInNanoMeters();
  }

  /**
   * @author Bill Darbie
   */
  public void splitSubtypesByPanelSide()
  {
    Assert.expect(false);
/** @todo wpd - put this in after Dick figures out what to do about 2 boards, same board type, one flipped */
//    for (BoardType boardType : getBoardTypes())
//    {
//     for (boardType.getTopSideBoard().getComponents()
//    }
  }

  /**
   * @author Bill Darbie
   */
  public PanelCoordinate getCadOriginInNanometers()
  {
    java.awt.Shape shape = getCadOriginShapeInNanometers();
    java.awt.geom.Rectangle2D bounds = shape.getBounds2D();
    int x = MathUtil.roundDoubleToInt(bounds.getCenterX());
    int y = MathUtil.roundDoubleToInt(bounds.getCenterY());

    PanelCoordinate coord = new PanelCoordinate(x, y);
    return coord;
  }

  /**
   * @author Bill Darbie
   */
  public java.awt.Shape getCadOriginShapeInNanometers()
  {
    // create the initial shape
    int size = 1905000;
    java.awt.Shape shape = new Ellipse2D.Double(- size / 2, - size / 2, size, size);

    AffineTransform trans = AffineTransform.getTranslateInstance(0, 0);
    preConcatenateShapeTransform(trans);

    // apply the transform
    shape = trans.createTransformedShape(shape);

    Assert.expect(shape != null);
    return shape;
  }

  /**
   * @author Bill Darbie
   */
  public void invalidateShape()
  {
    _shape = null;
    for (Board board : getBoards())
      board.invalidateShape();
  }

  /**
   * @author Andy Mechtenberg
   */
  public List<Board> getBoardsOutsidePanel()
  {
    List<Board> badBoards = new ArrayList<Board>();

    for (Board board : getBoards())
    {
      if (board.isBoardWithinPanelOutline() == false)
        badBoards.add(board);
    }
    return badBoards;
  }

  /**
   * Convert a coordinate in renderer space to PanelCoordinate space.
   * @author Bill Darbie
   */
  public PanelCoordinate getPanelCoordinate(PanelCoordinate rendererCoordinate)
  {
    Assert.expect(rendererCoordinate != null);

    AffineTransform transform = AffineTransform.getTranslateInstance(0, 0);
    preConcatenateShapeTransform(transform);

    // use the inverse of that transform to figure out the correct offset
    Point2D sourcePoint = new Point2D.Double(rendererCoordinate.getX(), rendererCoordinate.getY());
    Point2D destPoint = new Point2D.Double(0, 0);
    try
    {
      transform.inverseTransform(sourcePoint, destPoint);
    }
    catch (NoninvertibleTransformException nte)
    {
      Assert.expect(false);
    }

    int x = MathUtil.roundDoubleToInt(destPoint.getX());
    int y = MathUtil.roundDoubleToInt(destPoint.getY());

    return new PanelCoordinate(x, y);
  }

  /**
   * Return the shape after relative to the Panels lower left corner as seen by x-rays.
   * @return the Panel shape after rotating about it's lower left origin and placing back in the 1st quadrant
   * @author Andy Mechtenberg
   */
  public java.awt.Shape getShapeInNanoMeters()
  {
    if (_shape == null)
    {
      Assert.expect(_panelSettings != null);
      int degrees = _panelSettings.getDegreesRotationRelativeToCad();
      int width = getWidthInNanoMeters();
      int length = getLengthInNanoMeters();

      _shape = new Rectangle2D.Double(0, 0, width, length);

      AffineTransform trans = AffineTransform.getTranslateInstance(0, 0);
      if (_panelSettings.isLeftToRightFlip())
      {
        // do a left-to-right flip before the rotation
        trans.preConcatenate(AffineTransform.getScaleInstance( -1, 1));
        trans.preConcatenate(AffineTransform.getTranslateInstance(width, 0));
      }
      if (_panelSettings.isTopToBottomFlip())
      {
        // do a top-to-bottom flip before the rotation
        trans.preConcatenate(AffineTransform.getScaleInstance(1, -1));
        trans.preConcatenate(AffineTransform.getTranslateInstance(0, length));
      }

      // rotate the panel
      trans.preConcatenate(AffineTransform.getRotateInstance(Math.toRadians(degrees)));

      // translate to keep the lower left corner of the panel at 0,0
      int shiftXinNanoMeters = _panelSettings.getShiftXinNanoMeters();
      int shiftYinNanoMeters = _panelSettings.getShiftYinNanoMeters();
      trans.preConcatenate(AffineTransform.getTranslateInstance(shiftXinNanoMeters, shiftYinNanoMeters));

      _shape = trans.createTransformedShape(_shape);
    }

    Assert.expect(_shape != null);
    return _shape;
  }

  /**
   * Take the transform passed in and modify it to take into account this Panels rotation as seen by x-rays.
   *
   * @author Bill Darbie
   */
  void preConcatenateShapeTransform(AffineTransform transform)
  {
    Assert.expect(transform != null);

    preConcatenateShapeTransform(transform, true);
  }
  
    /**
   * @author Kee Chin Seong
   * @return 
   */
  public java.awt.Shape getStaticImagePanelShapeInNanoMeters()
  {
    Assert.expect(_panelSettings != null);
    int degrees = _panelSettings.getDegreesRotationRelativeToCad();
    int width = getWidthInNanoMeters();
    int length = getLengthInNanoMeters();
    
    java.awt.Shape shape = null;

    shape = new Rectangle2D.Double(0, 0, width, length);

    AffineTransform trans = AffineTransform.getTranslateInstance(0, 0);
    if (_panelSettings.isLeftToRightFlip())
    {
      // do a left-to-right flip before the rotation
      trans.preConcatenate(AffineTransform.getScaleInstance( -1, 1));
      trans.preConcatenate(AffineTransform.getTranslateInstance(width, 0));
    }
    if (_panelSettings.isTopToBottomFlip())
    {
      // do a top-to-bottom flip before the rotation
      trans.preConcatenate(AffineTransform.getScaleInstance(1, -1));
      trans.preConcatenate(AffineTransform.getTranslateInstance(0, length));
    }

    // rotate the panel
    trans.preConcatenate(AffineTransform.getRotateInstance(Math.toRadians(degrees)));

      // translate to keep the lower left corner of the panel at 0,0
    int shiftXinNanoMeters = _panelSettings.getShiftXinNanoMeters();
    int shiftYinNanoMeters = _panelSettings.getShiftYinNanoMeters();
    trans.preConcatenate(AffineTransform.getTranslateInstance(shiftXinNanoMeters, shiftYinNanoMeters));

    shape = trans.createTransformedShape(shape);
    
    Assert.expect(shape != null);
    return shape;
}

  /**
   * Take the transform passed in and modify it to take into account this Panels rotation as seen by x-rays.
   *
   * @author Bill Darbie
   */
  void preConcatenateShapeTransform(AffineTransform transform, boolean applyPanelTranslation)
  {
    Assert.expect(transform != null);

    Assert.expect(_panelSettings != null);
    int degrees = _panelSettings.getDegreesRotationRelativeToCad();

    // flip the panel
    if (_panelSettings.isLeftToRightFlip())
    {
      // do a left-to-right flip before the rotation
      transform.preConcatenate(AffineTransform.getScaleInstance( -1, 1));
      if (applyPanelTranslation)
        transform.preConcatenate(AffineTransform.getTranslateInstance(getWidthInNanoMeters(), 0));
    }

    if (_panelSettings.isTopToBottomFlip())
    {
      // do a top-to-bottom flip before the rotation
      transform.preConcatenate(AffineTransform.getScaleInstance(1, -1));
      if (applyPanelTranslation)
        transform.preConcatenate(AffineTransform.getTranslateInstance(0, getLengthInNanoMeters()));
    }

    // apply the panel rotation
    transform.preConcatenate(AffineTransform.getRotateInstance(Math.toRadians(degrees)));

    if (applyPanelTranslation)
    {
      // translate to keep the lower left corner of the panel at 0,0
      int shiftXinNanoMeters = _panelSettings.getShiftXinNanoMeters();
      int shiftYinNanoMeters = _panelSettings.getShiftYinNanoMeters();
      transform.preConcatenate(AffineTransform.getTranslateInstance(shiftXinNanoMeters, shiftYinNanoMeters));
    }
  }

  /**
   * @author Laura Cormos
   */
  public List<Fiducial> getFiducialTypesOutsidePanel()
  {
    List<Fiducial> fiducials = new ArrayList<Fiducial>();
    for (Fiducial fiducial : getFiducials())
    {
      if (fiducial.isFiducialWithinPanelOutline() == false)
        fiducials.add(fiducial);
    }

    return fiducials;
  }

  /**
   * @author Laura Cormos
   */
  public boolean isMultiBoardPanel()
  {
    if (getBoards().size() > 1)
      return true;
    return false;
  }

  /**
   * @author Laura Cormos
   */
  public boolean doAllSubtypesUsePredictiveSliceHeight()
  {
    List<Subtype> usedSubtypes = getInspectedSubtypes();
    for (Subtype subtype : usedSubtypes)
    {
      if (subtype.usePredictiveSliceHeight() == false)
        return false;
    }
    return true;
  }

  /**
   * @author Laura Cormos
   */
  public boolean areAlgorithmUnlearnedSubtypesNotUsingPredictiveSliceHeight()
  {
    boolean answer = false;
    for (Subtype subtype : getInspectedSubtypesUnsorted())
    {
      if (subtype.areAlgorithmSettingsLearned() == false && subtype.usePredictiveSliceHeight() == false)
      {
        answer = true;
        return answer;
      }
    }

    return answer;
  }

  /**
   * @author Laura Cormos
   */
  public boolean areShortsUnlearnedSubtypesNotUsingPredictiveSliceHeight()
  {
    boolean answer = false;
    for (Subtype subtype : getInspectedSubtypesUnsorted())
    {
      if (subtype.isJointSpecificDataLearned() == false && subtype.usePredictiveSliceHeight() == false)
      {
        answer = true;
        return answer;
      }
    }

    return answer;
  }

  /**
   * @author Laura Cormos
   */
  public boolean areExpectedImageUnlearnedSubtypesNotUsingPredictiveSliceHeight()
  {
    boolean answer = false;
    for (Subtype subtype : getInspectedSubtypesUnsorted())
    {
      if (subtype.hasExpectedImageDataToLearn() &&
          subtype.isExpectedImageDataLearned() == false &&
          subtype.usePredictiveSliceHeight() == false)
      {
        answer = true;
        return answer;
      }
    }

    return answer;
  }


  /**
   * @author Poh Kheng
   */
  public String getUniqueSubtypeLongName(LibraryLandPattern landPattern, JointTypeEnum jointTypeEnum)
  {
    Assert.expect(landPattern != null);
    Assert.expect(jointTypeEnum != null);

    // find a unique name
    String longSubtypeName = landPattern.getName() + "_" + jointTypeEnum.getNameWithoutSpaces();

    Set<String> subtypeNames = new HashSet<String>();
    for (Subtype subtype: getSubtypes())
    {
      boolean added = subtypeNames.add(subtype.getLongName().toLowerCase());
      Assert.expect(added);
    }

    String subtypeName = longSubtypeName;
    int i = 0;
    while (subtypeNames.contains(subtypeName.toLowerCase()))
    {
      ++i;
      subtypeName = longSubtypeName + "_" + Integer.toString(i);
    }

    return subtypeName;
  }
  
  /**
   * @author Chong, Wei Chin
   */
  public boolean useVariableMagnification()
  {
    boolean hasVariableMagnification = false;
    
    for(Subtype subtype: _subtypeNameToSubtypeMap.values())
    {
      if(subtype.getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
      {
        return true;
      }
    }
    return hasVariableMagnification;
  }

  /**
   * @author Jack Hwee
   */
  public boolean isRemoveComponentOnTopOfComponents()
  {
    Assert.expect(_pspSettings != null);
    return _pspSettings.isRemoveRectanglesOnTopOfComponent();
  }
  
  /**
   * @author Jack Hwee
   */
  public void setIsRemoveComponentOnTopOfComponents(boolean isRemoveComponentOnTopOfComponents)
  {
    Assert.expect(_pspSettings != null);
    _pspSettings.setIsRemoveRectanglesOnTopOfComponent(isRemoveComponentOnTopOfComponents);
  }
  
   /**
   * @author Jack Hwee
   */
  public boolean isExtendRectanglesToEdgeOfBoard()
  {
    Assert.expect(_pspSettings != null);
    return _pspSettings.isExtendRectanglesToEdgeOfBoard();
  }
  
  /**
   * @author Jack Hwee
   */
  public void setIsExtendRectanglesToEdgeOfBoard(boolean isExtendRectanglesToEdgeOfBoard)
  {
    Assert.expect(_pspSettings != null);
    _pspSettings.setIsExtendRectanglesToEdgeOfBoard(isExtendRectanglesToEdgeOfBoard);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean isUseMeshTriangle()
  {
    Assert.expect(_pspSettings != null);
    return _pspSettings.isUseMeshTriangle();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setIsUseMeshTriangle(boolean isUseMeshTriangle)
  {
    Assert.expect(_pspSettings != null);
    _pspSettings.setIsUseMeshTriangle(isUseMeshTriangle);
  }
  
   /**
   * @author Jack Hwee
   */
  public boolean isShowMeshTriangle()
  {
    Assert.expect(_pspSettings != null);
    return _pspSettings.isShowMeshTriangle();
  }
  
  /**
   * @author Jack Hwee
   */
  public void setIsShowMeshTriangle(boolean isShowMeshTriangle)
  {
    Assert.expect(_pspSettings != null);
    _pspSettings.setIsShowMeshTriangle(isShowMeshTriangle);
  }
  
   /**
   * @author Jack Hwee
   */
  public boolean isApplyToAllBoard()
  {
    Assert.expect(_pspSettings != null);
    return _pspSettings.isApplyToAllBoard();
  }
  
  /**
   * @author Jack Hwee
   */
  public void setIsApplyToAllBoard(boolean isApplyToAllBoard)
  {
    Assert.expect(_pspSettings != null);
    _pspSettings.setIsApplyToAllBoard(isApplyToAllBoard);
  }
  
   /**
   * @author Jack Hwee
   */
  public boolean isAutomaticallyIncludeAllComponents()
  {
    Assert.expect(_pspSettings != null);
    return _pspSettings.isAutomaticallyIncludeAllComponents();
  }
  
  /**
   * @author Jack Hwee
   */
  public void setIsAutomaticallyIncludeAllComponents(boolean isAutomaticallyIncludeAllComponents)
  {
    Assert.expect(_pspSettings != null);
    _pspSettings.setIsAutomaticallyIncludeAllComponents(isAutomaticallyIncludeAllComponents);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public boolean isLargerFOV()
  {
    Assert.expect(_pspSettings != null);
    return _pspSettings.isLargerFOV();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void setUseLargerFOV(boolean isLargerFOV)
  {
    Assert.expect(_pspSettings != null);
    _pspSettings.setUseLargerFOV(isLargerFOV);
  }
  
  /**
   * @author Bee Hoon
   * XCR1650 - Open recipe and immediately save recipe, it will crash
   * Loop thru all the component in the recipe that using this extra component package
   */
  public List<String> getComponentListUsingCompPackage(List<String> compPackageLongNameList)
  {
    _componentListUsingCompPackage = null;
    _allComponentList = getComponents();
    
    for (String compPackageLongName : compPackageLongNameList)
    {
      for (Component component : _allComponentList)
      {
        if (component.getCompPackage().getLongName().equals(compPackageLongName))
          _componentListUsingCompPackage.add(component.toString());
      }     
    }
    
    return _componentListUsingCompPackage;
  }
  
  /**
   * Return a First Board contains the alignment region
   * It can be null value is all boards doesn't have any alignment region.
   *
   * @author Wei Chin
   */
  public Board getFirstBoardContainsAlignmentRegions()
  {
    Assert.expect(_boardNameToBoardMap != null);

    for(Board board : _boardNameToBoardMap.values())
    {
      if(board.getBoardSettings().hasLeftAlignmentPads() || 
         board.getBoardSettings().hasRightAlignmentPads())
        return board;
    }
    return null;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public boolean isCompPackageOnPackageNameValid(String name)
  {
    Assert.expect(name != null);
    if (name.contains(" ") || name.equals(""))
      return false;
    return true;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  void setNewCompPackageOnPackageName(CompPackageOnPackage compPackageOnPackage, String oldName, String newName)
  {
    Assert.expect(compPackageOnPackage != null);
    Assert.expect(newName != null);
    CompPackageOnPackage prev = null;
    
    if (oldName != null)
    {
      prev = _compPackageOnPackageNameToCompPackageOnPackageMap.remove(oldName);
    }
    prev = _compPackageOnPackageNameToCompPackageOnPackageMap.put(newName, compPackageOnPackage);

    if (oldName == null)
      _projectObservable.stateChanged(this, null, compPackageOnPackage, PanelEventEnum.ADD_OR_REMOVE_COMP_PACKAGE_ON_PACKAGE);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public void addCompPackageOnPackage(CompPackageOnPackage compPackageOnPackage)
  {
    Assert.expect(compPackageOnPackage != null);

    _projectObservable.setEnabled(false);
    try
    {
      Object previous = _compPackageOnPackageNameToCompPackageOnPackageMap.put(compPackageOnPackage.getPOPName(), compPackageOnPackage);
      Assert.expect(previous == null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, compPackageOnPackage, PanelEventEnum.ADD_OR_REMOVE_COMP_PACKAGE_ON_PACKAGE);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  void removeCompPackageOnPackage(CompPackageOnPackage compPackageOnPackage)
  {
    Assert.expect(compPackageOnPackage != null);

    _projectObservable.setEnabled(false);
    try
    {
      CompPackageOnPackage prev = _compPackageOnPackageNameToCompPackageOnPackageMap.remove(compPackageOnPackage.getPOPName());
      Assert.expect(prev != null);
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, compPackageOnPackage, null, PanelEventEnum.ADD_OR_REMOVE_COMP_PACKAGE_ON_PACKAGE);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public List<CompPackageOnPackage> getCompPackageOnPackages()
  {
    return new ArrayList<CompPackageOnPackage>(_compPackageOnPackageNameToCompPackageOnPackageMap.values());
  }
  
  /**
   * @author Siew Yeng
   */
  public void setPshSettings(PshSettings pshSettings)
  {
    Assert.expect(pshSettings != null);

    if (pshSettings == _pshSettings)
      return;

    PshSettings oldValue = _pshSettings;
    _projectObservable.setEnabled(false);
    try
    {
      _pshSettings = pshSettings;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, oldValue, pshSettings, PanelEventEnum.PANEL_SURFACE_MODEL_SETTINGS);
  }
  
  /**
   * @author Siew Yeng
   */
  public PshSettings getPshSettings()
  {
    Assert.expect(_pshSettings != null);
    return _pshSettings;
  }
  
   /**
   * @author Siew Yeng
   */
  public boolean hasPshSettings()
  {
    if (_pshSettings == null)
      return false;
    
    return true;
  }
  
  /*
   * @author Siew Yeng
   */
  public List<Subtype> getInspectedPredictiveSliceHeightSubtypes()
  {
    List<Subtype> pshSubtypes = new ArrayList();
    //Siew Yeng - XCR-3865 - get inspected subtypes only
    for(Subtype subtype : getInspectedSubtypes())
    {
      if(subtype.usePredictiveSliceHeight())
        pshSubtypes.add(subtype);
    }
    
    return pshSubtypes;
  }
  
  /**
   * XCR-3837 Display untestable Area on virtual live CAD
   * @author Janan Wong
   * return the left side untestable area Shape
   */
  public java.awt.Shape getLeftUntestableArea()
  {
    int degrees = _panelSettings.getDegreesRotationRelativeToCad();
    int maxInspectionPanelWidth = ProgramGeneration.getInstance().getMaxPanelInspectableWidthInNanoMeters();
    int untestableAreaWidth = (getWidthAfterAllRotationsInNanoMeters() - maxInspectionPanelWidth) / 2;
    int untestableAreaLength = getLengthInNanoMeters();

    java.awt.Shape shape = new Rectangle2D.Double(0, 0, untestableAreaWidth, untestableAreaLength);

    AffineTransform trans = AffineTransform.getTranslateInstance(0, 0);
    if (_panelSettings.isLeftToRightFlip())
    {
      // do a left-to-right flip before the rotation
      trans.preConcatenate(AffineTransform.getScaleInstance(-1, 1));
      trans.preConcatenate(AffineTransform.getTranslateInstance(untestableAreaWidth, 0));
    }
    if (_panelSettings.isTopToBottomFlip())
    {
      // do a top-to-bottom flip before the rotation
      trans.preConcatenate(AffineTransform.getScaleInstance(1, -1));
      trans.preConcatenate(AffineTransform.getTranslateInstance(0, untestableAreaLength));
    }

    trans.preConcatenate(AffineTransform.getRotateInstance(Math.toRadians(degrees)));

    // translate to keep the lower left corner of the panel at 0,0
    int shiftXinNanoMeters = _panelSettings.getShiftXinNanoMeters();
    int shiftYinNanoMeters = _panelSettings.getShiftYinNanoMeters();
    trans.preConcatenate(AffineTransform.getTranslateInstance(shiftXinNanoMeters, shiftYinNanoMeters));

    shape = trans.createTransformedShape(shape);

    return shape;
  }
  
  /**
   * XCR-3837 Display untestable Area on virtual live CAD
   * @author Janan Wong
   * return the right side untestable area Shape
   */
  public java.awt.Shape getRightUntestableArea()
  {
    int degrees = _panelSettings.getDegreesRotationRelativeToCad();
    int maxInspectionPanelWidth = ProgramGeneration.getInstance().getMaxPanelInspectableWidthInNanoMeters();
    int untestableAreaWidth = (getWidthAfterAllRotationsInNanoMeters() - maxInspectionPanelWidth) / 2;
    int untestableAreaLength = getLengthInNanoMeters();
 
    java.awt.Shape shape = new Rectangle2D.Double(getWidthAfterAllRotationsInNanoMeters() - untestableAreaWidth , 0, untestableAreaWidth, untestableAreaLength);

    AffineTransform trans = AffineTransform.getTranslateInstance(0, 0);
    if (_panelSettings.isLeftToRightFlip())
    {
      // do a left-to-right flip before the rotation
      trans.preConcatenate(AffineTransform.getScaleInstance(-1, 1));
      trans.preConcatenate(AffineTransform.getTranslateInstance(untestableAreaWidth, 0));
    }
    if (_panelSettings.isTopToBottomFlip())
    {
      // do a top-to-bottom flip before the rotation
      trans.preConcatenate(AffineTransform.getScaleInstance(1, -1));
      trans.preConcatenate(AffineTransform.getTranslateInstance(0, untestableAreaLength));
    }

    trans.preConcatenate(AffineTransform.getRotateInstance(Math.toRadians(degrees)));

    // translate to keep the lower left corner of the panel at 0,0
    int shiftXinNanoMeters = _panelSettings.getShiftXinNanoMeters();
    int shiftYinNanoMeters = _panelSettings.getShiftYinNanoMeters();
    trans.preConcatenate(AffineTransform.getTranslateInstance(shiftXinNanoMeters, shiftYinNanoMeters));

    shape = trans.createTransformedShape(shape);

    return shape;
  }
  
  /**
   * XCR-3837 Display untestable Area on virtual live CAD
   * @author Janan Wong
   * return true if the loaded panel's width is larger than the max inspectable width
   */
  public boolean hasUntestableArea()
  {
    int panelWidth = getWidthAfterAllRotationsInNanoMeters();
    int maxInspectionPanelWidth = ProgramGeneration.getInstance().getMaxPanelInspectableWidthInNanoMeters();

    if (panelWidth > maxInspectionPanelWidth)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}
