package com.axi.v810.business.panelDesc;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * Test class for PinNumberComparator
 *
 * @author Wei Chin, Chong
 */

public class Test_PadNameComparator extends UnitTest
{

  /**
   * @author Wei Chin, Chong
   */
  public static void main(String[] args)
  {
    UnitTest.execute( new Test_PadNameComparator() );
  }

  /**
   * @author Wei Chin, Chong
   */
  public void test( BufferedReader is, PrintWriter os )
  {

    String[] unsortedListElements1 = { "p3", "P2", "P1", "N2", "N3", "N1" };
    String[] sortedListElements1 = { "N1", "N2", "N3", "P1", "P2", "p3" };

    String[] unsortedListElements2 = { "N", "p" };
    String[] sortedListElements2 = { "p" , "N" };

    String[] unsortedListElements3 = { "nEG", "POS" };
    String[] sortedListElements3 = { "POS" , "nEG" };

    String[] unsortedListElements4 = { "u1", "1", "11", "4_sdf", "u11", "u2", "2", "_", "", "u1_a", "u1_1", "u1_2",
                                          "u1_11", "u", "17.1f", "16d" };
    String[] sortedListElements4 = { "", "1", "2", "4_sdf", "11", "16d", "17.1f", "u", "u1", "u1_1", "u1_2", "u1_11", "u1_a", "u2",
                                    "u11", "_" };

    String[] unsortedListElements5 = { "p3p", "P2s", "P1s", "N2a", "N3z", "N1f" };
    String[] sortedListElements5 = { "N1f", "N2a", "N3z", "P1s", "P2s", "p3p" };

    String[][] unsortedListElementsGroup = { unsortedListElements1, unsortedListElements2, unsortedListElements3 ,
                                             unsortedListElements4, unsortedListElements5};
    String[][] sortedListElementsGroup = {sortedListElements1, sortedListElements2, sortedListElements3,
                                          sortedListElements4, sortedListElements5};

    Expect.expect( unsortedListElementsGroup.length == sortedListElementsGroup.length);

    for( int i=0; i < unsortedListElementsGroup.length; i++)
    {
      List<String> list = new ArrayList<String>();

      for ( int j = 0; j < unsortedListElementsGroup[i].length; j++ )
        list.add( unsortedListElementsGroup[i][j] );

      Collections.sort( list, new PadNameComparator() );

      for (int j=0; j < list.size(); j++)
      {
        Expect.expect( list.get(j).equals(sortedListElementsGroup[i][j]) );
      }
    }
  }
}
