package com.axi.v810.business.panelDesc;

import java.io.*;

/**
 * This class describes a single unique surface mount pad.
 * There is one SurfaceMountPad instance for each unique SurfaceMountPad on the Panel.
 * Many Pad instances on the same Panel will share a reference to the same
 * SurfaceMountPad.
 *
 * @author Bill Darbie
 */
public class SurfaceMountLandPatternPad extends LandPatternPad implements Serializable
{
  // created by LandPatternReader

  /**
   * @return true if the pad is throughole, false otherwise
   * @author Andy Mechtenberg
   */
  public boolean isThroughHolePad()
  {
    return false;
  }

  /**
   * @return true if the pad is throughole, false otherwise
   * @author Bill Darbie
   */
  public boolean isSurfaceMountPad()
  {
    return true;
  }

  /**
   * @author Bill Darbie
   */
  SurfaceMountLandPatternPad createDuplicate(LandPattern landPattern)
  {
    return (SurfaceMountLandPatternPad)super.createDuplicate(landPattern);
  }

}
