package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class ComponentTypeEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static ComponentTypeEventEnum CREATE_OR_DESTROY = new ComponentTypeEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static ComponentTypeEventEnum ADD_OR_REMOVE = new ComponentTypeEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static ComponentTypeEventEnum SIDE_BOARD_TYPE = new ComponentTypeEventEnum(++_index);
  public static ComponentTypeEventEnum REFERENCE_DESIGNATOR = new ComponentTypeEventEnum(++_index);
  public static ComponentTypeEventEnum PART_NUMBER = new ComponentTypeEventEnum(++_index);
  public static ComponentTypeEventEnum PART_LOCATION_IN_FACTORY = new ComponentTypeEventEnum(++_index);
  public static ComponentTypeEventEnum COORDINATE = new ComponentTypeEventEnum(++_index);
  public static ComponentTypeEventEnum CHANGE_SIDE = new ComponentTypeEventEnum(++_index);
  public static ComponentTypeEventEnum DEGREES_ROTATION = new ComponentTypeEventEnum(++_index);
  public static ComponentTypeEventEnum COMP_PACKAGE = new ComponentTypeEventEnum(++_index);
  public static ComponentTypeEventEnum LAND_PATTERN = new ComponentTypeEventEnum(++_index);
  public static ComponentTypeEventEnum COMPONENT_TYPE_SETTINGS = new ComponentTypeEventEnum(++_index);
  public static ComponentTypeEventEnum ADD_OR_REMOVE_PAD_TYPE = new ComponentTypeEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static ComponentTypeEventEnum ADD_OR_REMOVE_COMPONENT = new ComponentTypeEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static ComponentTypeEventEnum ADD_OR_REMOVE_COMPONENT_LIST = new ComponentTypeEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static ComponentTypeEventEnum JOINT_TYPE_ENUM = new ComponentTypeEventEnum(++_index);
  public static ComponentTypeEventEnum SUBTYPE_NAME = new ComponentTypeEventEnum(++_index);
  //Khaw Chek Hau - XCR3554: Package on package (PoP) development
  public static ComponentTypeEventEnum POP_LAYER_ID = new ComponentTypeEventEnum(++_index);
  public static ComponentTypeEventEnum POP_ZHEIGHT = new ComponentTypeEventEnum(++_index);
  public static ComponentTypeEventEnum COMP_PACKAGE_ON_PACKAGE = new ComponentTypeEventEnum(++_index);
  
  /**
   * @author Bill Darbie
   */
  private ComponentTypeEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private ComponentTypeEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
