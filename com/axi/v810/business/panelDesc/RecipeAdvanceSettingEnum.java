/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import java.util.*;

/**
 *
 * @author weng-jian.eoh
 */
public class RecipeAdvanceSettingEnum extends com.axi.util.Enum
{

  private static List<RecipeAdvanceSettingEnum> _recipeAdvanceSettingEnumList = new ArrayList<RecipeAdvanceSettingEnum>();

  private static int _index = -1;

  private String _name;
  private Object _defaultValue;
  private Object[] _validValues;
  
  private static String _generateMultiAngleView  = Boolean.toString(Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MULTI_ANGLE_IMAGES));
  private static String _generateLargeImageView = Boolean.toString(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_LARGE_IMAGE_VIEW));
  
  public static RecipeAdvanceSettingEnum RECIPE_GENERATE_LARGE_VIEW_IMAGE_INITIAL_SETTING = new RecipeAdvanceSettingEnum(++_index,
                                                                                                                         "Generate large view images",
                                                                                                                         _generateLargeImageView,
                                                                                                                         new Object[]
                                                                                                                         {"true","false"});
  
  public static RecipeAdvanceSettingEnum RECIPE_GENERATE_MULTI_ANGLE_IMAGE_INITIAL_SETTING = new RecipeAdvanceSettingEnum(++_index,
                                                                                                          "Generate 2.5D images",
                                                                                                          _generateMultiAngleView,
                                                                                                          new Object[]
                                                                                                          {"true","false"});
  
  public static RecipeAdvanceSettingEnum RECIPE_ENLARGE_RECONSTRUSION_REGION_INITIAL_SETTING= new RecipeAdvanceSettingEnum(++_index,
                                                                                                                           "Enlarge reconstruction region",
                                                                                                                           "false",
                                                                                                                           new Object[]
                                                                                                                           {"true","false"});
  public static RecipeAdvanceSettingEnum RECIPE_DRO_VERSION_INITIAL_SETTING = new RecipeAdvanceSettingEnum(++_index,
                                                                                                           "DRO Version",
                                                                                                           "0",
                                                                                                           new Object[]
                                                                                                           {
                                                                                                             "0", "1"
                                                                                                           });
         
  public static RecipeAdvanceSettingEnum RECIPE_SET_BGA_INSPECTION_REGION_TO_1X1_INTIAL_SETTING = new RecipeAdvanceSettingEnum(++_index,
                                                                                                                               "Joint-Based BGA inspection",
                                                                                                                               "false",
                                                                                                                               new Object[]
                                                                                                                               {"true","false"});
  
  public static RecipeAdvanceSettingEnum RECIPE_GENERATE_NEW_SCAN_ROUTE_INTIAL_SETTING  = new RecipeAdvanceSettingEnum(++_index,
                                                                                                                       "New Scan Route Generation",
                                                                                                                       "false",
                                                                                                                       new Object[]
                                                                                                                       {"true","false"});

  public static RecipeAdvanceSettingEnum RECIPE_PROJECT_BASED_BYPASS_MODE_INTIAL_SETTING = new RecipeAdvanceSettingEnum(++_index,
                                                                                                                        "set project based bypass mode",
                                                                                                                        "false",
                                                                                                                        new Object[]
                                                                                                                        {"true","false"});
  
  public static RecipeAdvanceSettingEnum RECIPE_ENLARGE_ALIGNMENT_REGION_INTIAL_SETTING = new RecipeAdvanceSettingEnum(++_index,
                                                                                                                       "Enlarge alignment region",
                                                                                                                       "false",
                                                                                                                       new Object[]
                                                                                                                       {"true","false"});
  public static RecipeAdvanceSettingEnum RECIPE_USER_GAIN_INTIAL_SETITNG = new RecipeAdvanceSettingEnum(++_index,
                                                                                                            "Subtype Initial UserGain Setting",
                                                                                                            UserGainEnum.ONE.toString(),
                                                                                                            UserGainEnum.getOrderedUserGainName().toArray());
  
  public static RecipeAdvanceSettingEnum RECIPE_STAGE_SPEED_INTIAL_SETTING = new RecipeAdvanceSettingEnum(++_index,
                                                                                                          "Subtype Stage Speed",
                                                                                                          new Object[]{StageSpeedEnum.ONE.toString()},
                                                                                                          StageSpeedEnum.getOrderStageSpeedName().toArray());
  
  public static RecipeAdvanceSettingEnum RECIPE_INTERFERENCE_COMPENSATION_INTIAL_SETITNG = new RecipeAdvanceSettingEnum(++_index,
                                                                                                            "Subtype Interference Compensation",
                                                                                                            "true",
                                                                                                            new Object[]
                                                                                                            {"true","false"});
  
  public static RecipeAdvanceSettingEnum RECIPE_SIGNAL_COMPENSATION_INITIAL_SETTING = new RecipeAdvanceSettingEnum(++_index, 
                                                                                                                   "Subtype Intergration Level",
                                                                                                                   SignalCompensationEnum.DEFAULT_LOW.toString(),
                                                                                                                   SignalCompensationEnum.getOrderSignalCompensationNameList().toArray());
  
  
//  public static RecipeAdvanceSettingEnum RECIPE_XRAY_ENABLE_CAMERA_INDEX_INTIAL_SETTING = new RecipeAdvanceSettingEnum(++_index,
//                                                                                                                       "Subtype Camera",
//                                                                                                                       XrayCameraIdEnum.getCameraIDNameList().toArray(),
//                                                                                                                       XrayCameraIdEnum.getCameraIDNameList().toArray());
                                                                                                                       
  public static RecipeAdvanceSettingEnum RECIPE_MAGNIFICATION_INTIAL_SETITNG = new RecipeAdvanceSettingEnum(++_index,
                                                                                                            "Subtype Magnification Type",
                                                                                                            MagnificationTypeEnum.LOW.toString(),
                                                                                                            MagnificationTypeEnum.getMagnificationTypeEnumList().toArray());
  
  
  public static RecipeAdvanceSettingEnum RECIPE_DRO_INTIAL_SETTING = new RecipeAdvanceSettingEnum(++_index, 
                                                                                                  "Subtype Dro Level",
                                                                                                  DynamicRangeOptimizationLevelEnum.ONE.toString(),
                                                                                                  DynamicRangeOptimizationLevelEnum.getOrderDroLevelNameList().toArray());
  
  
  public RecipeAdvanceSettingEnum(int id, String name,Object defaultValue, Object[] validValue)
  {
    super(id);
    Assert.expect(name != null);
    _name = name.intern();
    if (defaultValue instanceof Object[])
    {
      List<String> stringList = new ArrayList<String>();
      Object[] values  = (Object[]) defaultValue;
      for (Object o : values)
      {
        stringList.add((String) o);
      }
      _defaultValue = stringList;
    }
    else
      _defaultValue = defaultValue;
    
    _validValues = validValue;

    _recipeAdvanceSettingEnumList.add(this);
  }
  
  public String getName()
  {
    return _name;
  }
  
  public static List<RecipeAdvanceSettingEnum> getRecipeAdvanceSettingEnumList()
  {
    return _recipeAdvanceSettingEnumList;
  }
  
  public Object getDefaultValue()
  {
    return _defaultValue;
  }
  
  public Object[] getValidSelection()
  {
    return _validValues;
  }

  
  public static RecipeAdvanceSettingEnum getRecipeSettingEnumByName(String name)
  {
    Assert.expect(_recipeAdvanceSettingEnumList.isEmpty() == false);
    
    RecipeAdvanceSettingEnum selectedSetting = null;
    for (RecipeAdvanceSettingEnum setting : _recipeAdvanceSettingEnumList)
    {
      if (setting.getName().equals(name))
      {
        selectedSetting =  setting;
        break;
      }
    }
    
    Assert.expect(selectedSetting != null ,"The setting "+name+" is invalid.");
    
    return selectedSetting;
  }
  
  public void setValidSelectionBasedOnConfigValue(Object newValidSelection)
  {
    if (newValidSelection instanceof List)
    {
      _validValues = ((List<String>) newValidSelection).toArray();
    }
  }
}
