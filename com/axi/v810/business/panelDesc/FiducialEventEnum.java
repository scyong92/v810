package com.axi.v810.business.panelDesc;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class FiducialEventEnum extends ProjectChangeEventEnum
{
  private static int _index = -1;

  public static FiducialEventEnum CREATE_OR_DESTROY = new FiducialEventEnum(++_index, ProjectChangeTypeEnum.CREATE_OR_DESTROY);
  public static FiducialEventEnum ADD_OR_REMOVE = new FiducialEventEnum(++_index, ProjectChangeTypeEnum.ADD_OR_REMOVE);
  public static FiducialEventEnum SIDE_BOARD = new FiducialEventEnum(++_index);
  public static FiducialEventEnum PANEL = new FiducialEventEnum(++_index);
  public static FiducialEventEnum PANEL_SIDE1 = new FiducialEventEnum(++_index);
  public static FiducialEventEnum FIDUCIAL_TYPE = new FiducialEventEnum(++_index);

  /**
   * @author Bill Darbie
   */
  private FiducialEventEnum(int id)
  {
    super(id);
  }

  /**
   * @author Bill Darbie
   */
  private FiducialEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id, projectChangeTypeEnum);
    Assert.expect(projectChangeTypeEnum != null);
  }
}
