package com.axi.v810.business.packageLibrary;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class SubtypeSchemeComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static SubtypeSchemeComparatorEnum PACKAGE_NAME = new SubtypeSchemeComparatorEnum(++_index);
  public static SubtypeSchemeComparatorEnum JOINT_TYPE = new SubtypeSchemeComparatorEnum(++_index);
  public static SubtypeSchemeComparatorEnum SUBTYPE_NAME = new SubtypeSchemeComparatorEnum(++_index);
  public static SubtypeSchemeComparatorEnum ADD_UPDATE = new SubtypeSchemeComparatorEnum(++_index);
  public static SubtypeSchemeComparatorEnum NEW_MODIFIED = new SubtypeSchemeComparatorEnum(++_index);
  public static SubtypeSchemeComparatorEnum EXPORT_LIBRARY_PATH = new SubtypeSchemeComparatorEnum(++_index);
  public static SubtypeSchemeComparatorEnum IMPORT_LIBRARY_PATH = new SubtypeSchemeComparatorEnum(++_index);
  public static SubtypeSchemeComparatorEnum IMPORT_LIBRARY_PACKAGE = new SubtypeSchemeComparatorEnum(++_index);
  public static SubtypeSchemeComparatorEnum IMPORT_LIBRARY_JOINT_TYPE = new SubtypeSchemeComparatorEnum(++_index);
  public static SubtypeSchemeComparatorEnum IMPORT_LIBRARY_PROJECT = new SubtypeSchemeComparatorEnum(++_index);
  public static SubtypeSchemeComparatorEnum IMPORT_LIBRARY_SUBTYPE = new SubtypeSchemeComparatorEnum(++_index);
  public static SubtypeSchemeComparatorEnum IMPORT_LIBRARY_MATCH_PERCENTAGE = new SubtypeSchemeComparatorEnum(++_index);
  public static SubtypeSchemeComparatorEnum IMPORT_LIBRARY_SUBTYPE_DATE = new SubtypeSchemeComparatorEnum(++_index);

  /**
   * @param id int
   * @author Wei Chin, Chong
   */
  public SubtypeSchemeComparatorEnum(int id)
  {
    super(id);
  }
}
