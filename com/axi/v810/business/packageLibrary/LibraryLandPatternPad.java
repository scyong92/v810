package com.axi.v810.business.packageLibrary;

import com.axi.util.*;
import com.axi.v810.util.ComponentCoordinate;
import com.axi.v810.business.panelDesc.ShapeEnum;
import java.awt.geom.*;

/**
 * This class represents library land pattern pad.
 *
 * @author Cheah Lee Herng
 */
public class LibraryLandPatternPad
{
  private int _landPatternPadId = -1;
  private int _landPatternId = -1;
  private String _padName;
  private int _xLocation = -1;
  private int _yLocation = -1;
  private int _widthInNanoMeters = -1;
  private int _lengthInNanoMeters = -1;
  private double _degreesRotation = -1;
  private int _refShapeId = -1;
  private boolean _isSurfaceMount;
  private boolean _isPadOne;
  private int _holeDiameter = -1;
  //Siew Yeng - XCR-3318 - Oval PTH
  private int _holeWidth = -1;
  private int _holeLength = -1;
  private int _holeXLocation = -1;
  private int _holeYLocation = -1;
  private String _shapeName;
  private LibraryLandPattern _libraryLandPattern;
  private java.awt.Shape _shape;
  private ShapeEnum _shapeEnum;

  /**
   * @author Cheah Lee Herng
   */
  public LibraryLandPatternPad()
  {
    // do nothing
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getLandPatternPadId()
  {
    Assert.expect(_landPatternPadId > 0);
    return _landPatternPadId;
  }

  /**
   *
   * @param landPatternPadId int
   *
   * @author Cheah Lee Herng
   */
  public void setLandPatternPadId(int landPatternPadId)
  {
    Assert.expect(landPatternPadId > 0);
    _landPatternPadId = landPatternPadId;
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getLandPatternId()
  {
    Assert.expect(_landPatternPadId > 0);
    return _landPatternId;
  }

  /**
   *
   * @param landPatternId int
   */
  public void setLandPatternId(int landPatternId)
  {
    Assert.expect(landPatternId > 0);
    _landPatternId = landPatternId;
  }

  /**
   *
   * @return String
   *
   * @author Cheah Lee Herng
   */
  public String getPadName()
  {
    Assert.expect(_padName != null);
    return _padName;
  }

  /**
   *
   * @param padName String
   *
   * @author Cheah Lee Herng
   */
  public void setPadName(String padName)
  {
    Assert.expect(padName != null);
    _padName = padName;
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getXLocation()
  {
    return _xLocation;
  }

  /**
   *
   * @param xLocation int
   *
   * @author Cheah Lee Herng
   */
  public void setXLocation(int xLocation)
  {
    _xLocation = xLocation;
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getYLocation()
  {
    return _yLocation;
  }

  /**
   *
   * @param yLocation int
   *
   * @author Cheah Lee Herng
   */
  public void setYLocation(int yLocation)
  {
    _yLocation = yLocation;
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getWidthInNanoMeters()
  {
    Assert.expect(_widthInNanoMeters >= 0);
    return _widthInNanoMeters;
  }

  /**
   *
   * @param widthInNanoMeters int
   *
   * @author Cheah Lee Herng
   */
  public void setWidthInNanoMeters(int widthInNanoMeters)
  {
    Assert.expect(widthInNanoMeters >= 0);
    _widthInNanoMeters = widthInNanoMeters;
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getLengthInNanoMeters()
  {
    Assert.expect(_lengthInNanoMeters >= 0);
    return _lengthInNanoMeters;
  }

  /**
   *
   * @param lengthInNanoMeters int
   *
   * @author Cheah Lee Herng
   */
  public void setLengthInNanoMeters(int lengthInNanoMeters)
  {
    Assert.expect(lengthInNanoMeters >= 0);
    _lengthInNanoMeters = lengthInNanoMeters;
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public double getDegreesRotation()
  {
    Assert.expect(_degreesRotation >= 0);
    Assert.expect(_degreesRotation <= 360);
    return _degreesRotation;
  }

  public void setDegreesRotation(double degreesRotation)
  {
    Assert.expect(degreesRotation >= 0);
    Assert.expect(degreesRotation <= 360);
    _degreesRotation = degreesRotation;
  }

  /**
   * This would return shape id, which is the id keep in the database for shape
   * eg: Rectangle, Circle
   *
   * @author Cheah Lee Herng
   */
  public int getRefShapeId()
  {
    Assert.expect(_refShapeId > 0);
    return _refShapeId;
  }

  /**
   * This would set shape id, which is the id keep in the database for shape
   * eg: Rectangle, Circle
   *
   * @param refShapeId int
   *
   * @author Cheah Lee Herng
   */
  public void setRefShapeId(int refShapeId)
  {
    Assert.expect(refShapeId > 0);
    _refShapeId = refShapeId;
  }

  /**
   *
   * @param isSurfaceMount boolean
   *
   * @author Cheah Lee Herng
   */
  public void setIsSurfaceMount(boolean isSurfaceMount)
  {
    _isSurfaceMount = isSurfaceMount;
  }

  /**
   *
   * @return boolean
   *
   * @author Cheah Lee Herng
   */
  public boolean isPadOne()
  {
    return _isPadOne;
  }

  /**
   *
   * @param isPadOne boolean
   *
   * @author Cheah Lee Herng
   */
  public void setIsPadOne(boolean isPadOne)
  {
    _isPadOne = isPadOne;
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getHoleDiameter()
  {
    Assert.expect(_holeDiameter >= 0);
    return _holeDiameter;
  }

  /**
   *
   * @param holeDiameter int
   *
   * @author Cheah Lee Herng
   */
  public void setHoleDiameter(int holeDiameter)
  {
    Assert.expect(holeDiameter >= 0);
    _holeDiameter = holeDiameter;
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getHoleXLocation()
  {
    return _holeXLocation;
  }

  /**
   *
   * @param holeXLocation int
   *
   * @author Cheah Lee Herng
   */
  public void setHoleXLocation(int holeXLocation)
  {
    _holeXLocation = holeXLocation;
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getHoleYLocation()
  {
    return _holeYLocation;
  }

  /**
   *
   * @param holeYLocation int
   *
   * @author Cheah Lee Herng
   */
  public void setHoleYLocation(int holeYLocation)
  {
    _holeYLocation = holeYLocation;
  }

  /**
   *
   * @author Poh Kheng
   */
  public boolean isThroughHolePad()
  {
    //Siew Yeng - XCR-3318 - Oval PTH
    if(_holeWidth > 0 && _holeLength > 0)
      return true;
    else
      return false;
  }

  /**
   * @author Poh Kheng
   */
  public boolean isSurfaceMountPad()
  {
    return _isSurfaceMount;
  }

  /**
   * @author Poh Kheng
   */
  public java.awt.Shape getShapeInNanoMeters()
  {
    java.awt.Shape shape = null;
    if (_shape == null)
    {
      ComponentCoordinate padCoordinate = new ComponentCoordinate(getXLocation(), getYLocation());
      int padWidth = getWidthInNanoMeters();
      int padLength = getLengthInNanoMeters();

      // find the lower left corner
      DoubleCoordinate lowerLeftPadLocation = new DoubleCoordinate(padCoordinate.getX() - padWidth / 2.0,
          padCoordinate.getY() - padLength / 2.0);

      if (getShapeEnum().equals(ShapeEnum.RECTANGLE))
      {
        shape = new Rectangle2D.Double(lowerLeftPadLocation.getX(),
                                       lowerLeftPadLocation.getY(),
                                       padWidth,
                                       padLength);
      }
      else if (getShapeEnum().equals(ShapeEnum.CIRCLE))
      {
        if (padWidth == padLength)
        {
          // this is a circle
          shape = new Ellipse2D.Double(lowerLeftPadLocation.getX(),
                                       lowerLeftPadLocation.getY(),
                                       padWidth,
                                       padLength);
        }
        else
        {
          shape = GeomUtil.createObround((int)lowerLeftPadLocation.getX(),
                                         (int)lowerLeftPadLocation.getY(),
                                         padWidth,
                                         padLength);
        }
      }
      else
        Assert.expect(false, "The pad shape was neither a circle or rectangle");


      // build up the correct transform for this Pad
      AffineTransform trans = AffineTransform.getRotateInstance(Math.toRadians(getDegreesRotation()),
          padCoordinate.getX(),
          padCoordinate.getY());

      // apply the transform
      shape = trans.createTransformedShape(shape);
      _shape = shape;
    }

    Assert.expect(_shape != null);
    return _shape;
  }

  /**
   * @author Poh Kheng
   */
  public String getShapeName()
  {
    Assert.expect(_shapeName != null);
    return _shapeName;
  }

  /**
   * @author Poh Kheng
   */
  public void setShapeName(String shapeName)
  {
    Assert.expect(shapeName != null);
    _shapeName = shapeName;
  }

  /**
   * @author Poh Kheng
   */
  public ShapeEnum getShapeEnum()
  {
    if(_shapeEnum == null) {
      Assert.expect(_shapeName != null);
      _shapeEnum = ShapeEnum.getShapeEnum(_shapeName);
    }
    return _shapeEnum;
  }

  /**
   * @author Poh Kheng
   */
  public void setShapeEnum(ShapeEnum shapeEnum)
  {
    Assert.expect(shapeEnum != null);
    _shapeEnum = shapeEnum;
  }


  /**
   * @author Poh Kheng
   */
  public java.awt.Shape getHoleShapeInNanoMeters()
  {
//    int holeDiameterInNanoMeters = getHoleDiameter();
    //Siew Yeng - XCR-3318 - Oval PTH
    int holeWidthInNanometers = getHoleWidth();
    int holeLengththInNanometers = getHoleLength();

    java.awt.Shape holeShape = null;
    
    if(isOvalThroughHole() == false)
    {
      holeShape = new Ellipse2D.Double(getHoleXLocation() - holeWidthInNanometers / 2,
                                        getHoleYLocation() - holeWidthInNanometers / 2,
                                        holeWidthInNanometers,
                                        holeWidthInNanometers);
    }
    else
    {
      holeShape = GeomUtil.createObround(getHoleXLocation() - holeWidthInNanometers / 2,
                              getHoleYLocation() - holeLengththInNanometers / 2,
                              holeWidthInNanometers,
                              holeLengththInNanometers);
    }
    return holeShape;
  }

  /**
   * This is to set the librarySubtype to a list
   * @author Poh Kheng
   */
  public void setLibraryLandPattern(LibraryLandPattern libraryLandPattern)
  {
    Assert.expect(libraryLandPattern != null);

    _libraryLandPattern = libraryLandPattern;

  }

  /**
   * @author Poh Kheng
   */
  public LibraryLandPattern getLibraryLandPattern()
  {
    Assert.expect(_libraryLandPattern != null);

    return _libraryLandPattern;
  }
  
  /**
   * @author Siew Yeng
   */
  public int getHoleWidth()
  {
    Assert.expect(_holeWidth >= 0);
    return _holeWidth;
  }

  /**
   * @author Siew Yeng
   */
  public void setHoleWidth(int holeWidth)
  {
    Assert.expect(holeWidth >= 0);
    _holeWidth = holeWidth;
  }
  
  /**
   * @author Siew Yeng
   */
  public int getHoleLength()
  {
    Assert.expect(_holeLength >= 0);
    return _holeLength;
  }

  /**
   * @author Siew Yeng
   */
  public void setHoleLength(int holeLength)
  {
    Assert.expect(holeLength >= 0);
    _holeLength = holeLength;
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean isOvalThroughHole()
  {
    return getHoleWidth() != getHoleLength();
  }
}
