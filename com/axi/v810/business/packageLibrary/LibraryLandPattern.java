package com.axi.v810.business.packageLibrary;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.ComponentCoordinate;

/**
 * @author Cheah Lee Herng
 */
public class LibraryLandPattern
{
  private int _landPatternId = -1;
  private String _landPatternName;
  private int _numberOfPads = -1;
  private List<LibraryLandPatternPad> _libraryLandPatternPads;

  /**
   * @author Cheah Lee Herng
   */
  public LibraryLandPattern()
  {
    _libraryLandPatternPads = new ArrayList<LibraryLandPatternPad>();
  }

  /**
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getLandPatternId()
  {
    Assert.expect(_landPatternId > 0);
    return _landPatternId;
  }

  /**
   *
   * @param landPatternId int
   *
   * @author Cheah Lee Herng
   */
  public void setLandPatternId(int landPatternId)
  {
    Assert.expect(landPatternId > 0);
    _landPatternId = landPatternId;
  }

  /**
   *
   * @return String
   *
   * @author Cheah Lee Herng
   */
  public String getName()
  {
    Assert.expect(_landPatternName != null);
    return _landPatternName;
  }

  /**
   *
   * @param landPatternName String
   *
   * @author Cheah Lee Herng
   */
  public void setLandPatternName(String landPatternName)
  {
    Assert.expect(landPatternName != null);
    _landPatternName = landPatternName;
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getNumberOfPads()
  {
    Assert.expect(_numberOfPads >= 0);
    return _numberOfPads;
  }

  /**
   *
   * @param numberOfPads int
   *
   * @author Cheah Lee Herng
   */
  public void setNumberOfPads(int numberOfPads)
  {
    Assert.expect(numberOfPads >= 0);
    _numberOfPads = numberOfPads;
  }

  /**
   *
   * @return List
   *
   * @author Cheah Lee Herng
   */
  public List<LibraryLandPatternPad> getLibraryLandPatternPads()
  {
    Assert.expect(_libraryLandPatternPads != null);
    return _libraryLandPatternPads;
  }

  /**
   *
   * @param libraryLandPatternPad LibraryLandPatternPad
   *
   * @author Cheah Lee Herng
   */
  public void addLibraryLandPatternPad(LibraryLandPatternPad libraryLandPatternPad)
  {
    Assert.expect(libraryLandPatternPad != null);
    Assert.expect(_libraryLandPatternPads != null);

    _libraryLandPatternPads.add(libraryLandPatternPad);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void clean()
  {
    Assert.expect(_libraryLandPatternPads != null);

    _libraryLandPatternPads.clear();
  }

  /**
   * @author Poh Kheng
   */
  public void getMinAndMaxPadBoundCoordinateForAllLandPatternPads(ComponentCoordinate minPadCoordinate,
                                                             ComponentCoordinate maxPadCoordinate)
  {
    Assert.expect(minPadCoordinate != null);
    Assert.expect(maxPadCoordinate != null);
    boolean first = true;
    for (LibraryLandPatternPad libraryLandPatternPad : getLibraryLandPatternPads())
    {
      ComponentCoordinate padMinCoordinate =
          new ComponentCoordinate(libraryLandPatternPad.getXLocation() - libraryLandPatternPad.getWidthInNanoMeters()/2,
                                  libraryLandPatternPad.getYLocation() - libraryLandPatternPad.getLengthInNanoMeters()/2);
      ComponentCoordinate padMaxCoordinate =
          new ComponentCoordinate(libraryLandPatternPad.getXLocation() + libraryLandPatternPad.getWidthInNanoMeters()/2,
                                  libraryLandPatternPad.getYLocation() + libraryLandPatternPad.getLengthInNanoMeters()/2);
      if (first == true)
      {
        minPadCoordinate.setX(padMinCoordinate.getX());
        minPadCoordinate.setY(padMinCoordinate.getY());
        maxPadCoordinate.setX(padMaxCoordinate.getX());
        maxPadCoordinate.setY(padMaxCoordinate.getY());
        first = false;
      }
      else
      {
        if (padMinCoordinate.getX() < minPadCoordinate.getX())
          minPadCoordinate.setX(padMinCoordinate.getX());
        if (padMinCoordinate.getY() < minPadCoordinate.getY())
          minPadCoordinate.setY(padMinCoordinate.getY());
        if (padMaxCoordinate.getX() > maxPadCoordinate.getX())
          maxPadCoordinate.setX(padMaxCoordinate.getX());
        if (padMaxCoordinate.getY() > maxPadCoordinate.getY())
          maxPadCoordinate.setY(padMaxCoordinate.getY());
      }
    }
  }

}
