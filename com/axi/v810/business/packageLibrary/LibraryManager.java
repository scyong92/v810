package com.axi.v810.business.packageLibrary;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.datastore.projWriters.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */

public class LibraryManager
{
  private Set<String> _libraries = new TreeSet<String>(new CaseInsensitiveStringComparator());
  private List<String> _selectedLibraries = new ArrayList<String>();
  private static Map<String, Boolean> _libraryPathToExistOrNotMap = new TreeMap<String,Boolean>(new CaseInsensitiveStringComparator());
  private LibrariesWriter _libraryListWriter = null;
  private LibrariesReader _libarayListReader = null;

  private static LibraryManager _instance = null;
  public static final String START_OF_SELECTED_LIBRARIES = "---Selected Libraries Path---";

  /**
   * @return LibraryManager
   * @author Wei Chin, Chong
   */
  public static synchronized LibraryManager getInstance()
  {
    if (_instance == null)
      _instance = new LibraryManager();

    return _instance;
  }

  /**
   * @author Wei Chin, Chong
   */
  private LibraryManager()
  {
    _libraryListWriter = LibrariesWriter.getInstance();
    _libarayListReader = LibrariesReader.getInstance();
  }

  /**
   * @return List
   * @author Wei Chin, Chong
   */
  public List<String> getLibraries()
  {
    Assert.expect(_libraries != null);
    return new ArrayList<String>(_libraries);
  }

  /**
   *
   * @return List
   *
   * @author Tan Hock Zoon
   */
  public List<String> getSelectedLibraries()
  {
    Assert.expect(_selectedLibraries != null);
    return _selectedLibraries;
  }

  /**
   * @throws DatastoreException
   * @author Wei Chin, Chong
   */
  public void load() throws DatastoreException
  {
    Assert.expect(_libraries != null);
    Assert.expect(_libraryPathToExistOrNotMap != null);
    Assert.expect(_libarayListReader != null);

    _libraries.clear();
    _selectedLibraries.clear();
    _libraryPathToExistOrNotMap.clear();
    _libarayListReader.read(this);
  }

  /**
   * @param libraryPath String
   * @throws DatastoreException
   * @author Wei Chin, Chong
   */
  public void add(String libraryPath) throws DatastoreException
  {
    add(libraryPath, true);
  }

  /**
   * @param libraryPath String
   * @param saveFile boolean
   * @throws DatastoreException
   *
   * @author Wei Chin, Chong
   */
  public void add(String libraryPath, boolean saveFile) throws DatastoreException
  {
    Assert.expect(libraryPath != null);
    Assert.expect(_libraries != null);

    if ( _libraries.contains(libraryPath) == false)
    {
      boolean exist = LibraryUtil.getInstance().doeslibraryExist(libraryPath);

      if (exist == true)
      {
        boolean libraryAlreadyExists = _libraries.add(libraryPath);
        Assert.expect(libraryAlreadyExists);
        _libraryPathToExistOrNotMap.put(libraryPath, exist);
      }
    }

    if(saveFile)
      save();
  }

  /**
   *
   * @param libraryPath String
   * @param saveFile boolean
   * @throws DatastoreException
   *
   * @author Tan Hock Zoon
   */
  public void addSelectedLibrary(String libraryPath, boolean saveFile) throws DatastoreException
  {
    Assert.expect(libraryPath != null);

    if (_selectedLibraries.contains(libraryPath) == false)
    {
      boolean exist = LibraryUtil.getInstance().doeslibraryExist(libraryPath);

      if (exist == true)
      {
        boolean libraryAlreadyExists = _selectedLibraries.add(libraryPath);
        Assert.expect(libraryAlreadyExists);
      }
    }

    if(saveFile)
      save();
  }

  /**
   *
   * @param libraryPath String
   * @param saveFile boolean
   * @throws DatastoreException
   *
   * @author Tan Hock Zoon
   */
  public void removeSelectedLibrary(String libraryPath, boolean saveFile) throws DatastoreException
  {
    Assert.expect(libraryPath != null);

    if (_selectedLibraries.contains(libraryPath))
    {
      _selectedLibraries.remove(libraryPath);
    }

    if(saveFile)
      save();
  }

  /**
   * @author Tan Hock Zoon
   */
  public void removeAllSelectedLibraries()
  {
    Assert.expect(_selectedLibraries != null);
    _selectedLibraries.clear();
  }

  /**
   * @param libraryPath String
   * @throws DatastoreException
   * @author Wei Chin, Chong
   */
  public void remove(String libraryPath) throws DatastoreException
  {
    remove(libraryPath, true);
  }

  /**
   * @param libraryPath String
   * @param writeFileOrNot boolean
   * @throws DatastoreException
   * @author Wei Chin, Chong
   */
  public void remove(String libraryPath, boolean writeFileOrNot) throws DatastoreException
  {
    Assert.expect(libraryPath != null);
    Assert.expect(_libraries != null);

    if( _libraries.contains(libraryPath))
    {
      boolean libraryExsits = _libraries.remove(libraryPath);
      Assert.expect(libraryExsits);
      _libraryPathToExistOrNotMap.remove(libraryPath);
    }

    if(writeFileOrNot)
      save();
  }

  /**
   * @param libraryPath String
   * @return boolean
   * @author Wei Chin, Chong
   */
  public static boolean doesLibraryExist(String libraryPath)
  {
    Assert.expect(libraryPath != null);
    Assert.expect(_libraryPathToExistOrNotMap != null);

    if( _libraryPathToExistOrNotMap.containsKey(libraryPath))
      return _libraryPathToExistOrNotMap.get(libraryPath);

    return false;
  }

  /**
   * @throws DatastoreException
   * @author Wei Chin, Chong
   */
  public void save() throws DatastoreException
  {
    _libraryListWriter.write(this);
  }

  /**
   * @author Wei Chin, Chong
   */
  public void clear()
  {
    _libraryListWriter = null;
    _libarayListReader = null;
  }
}
