package com.axi.v810.business.packageLibrary;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class tracks the Import Library option.
 * By default, all the following options are imported into recipe.
 * 1) Thresholds
 * 2) 
 * 
 * @author Cheah Lee Herng
 */
public class ImportLibrarySettingPersistance implements Serializable
{
  private boolean _importThreshold = true;
  private boolean _importNominal = true;
  private boolean _importLandPattern = true;
  private boolean _importLibrarySubtypeName = true;
  
  /**
   * @author Cheah Lee Herng
   */
  public ImportLibrarySettingPersistance()
  {
    // Do nothing
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void setImportThreshold(boolean importThreshold)
  {
    _importThreshold = importThreshold;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private boolean isImportThreshold()
  {
    return _importThreshold;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void setImportNominal(boolean importNominal)
  {
    _importNominal = importNominal;
  }
  
  /**   
   * @return 
   */
  private boolean isImportNominal()
  {
    return _importNominal;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setImportLandPattern(boolean importLandPattern)
  {
    _importLandPattern = importLandPattern;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private boolean isImportLandPattern()
  {
    return _importLandPattern;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void setImportLibrarySubtypeName(boolean importLibrarySubtypeName)
  {
    _importLibrarySubtypeName = importLibrarySubtypeName;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private boolean isImportLibrarySubtypeName()
  {
    return _importLibrarySubtypeName;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public ImportLibrarySettingPersistance readSettings()
  {
    ImportLibrarySettingPersistance persistance = null;
    try
    {
      if (FileUtilAxi.exists(FileName.getPackageLibraryImportOptionPersistFullPath()))
        persistance = (ImportLibrarySettingPersistance) FileUtilAxi.loadObjectFromSerializedFile(FileName.getPackageLibraryImportOptionPersistFullPath());
      else
        persistance = new ImportLibrarySettingPersistance();
    }
    catch(DatastoreException de)
    {
      persistance = new ImportLibrarySettingPersistance();
    }

    return persistance;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void writeSettings() throws DatastoreException
  {
    FileUtilAxi.saveObjectToSerializedFile(this, FileName.getPackageLibraryImportOptionPersistFullPath());
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clearFilter()
  {
    _importThreshold = true;
    _importNominal = true;
    _importLandPattern = true;
    _importLibrarySubtypeName = true;
  }  
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setImportLibrarySettingStatus(ImportLibrarySettingEnum importLibrarySettingEnum, boolean setValue)
  {
    Assert.expect(importLibrarySettingEnum != null);
    
    if (importLibrarySettingEnum.equals(ImportLibrarySettingEnum.IMPORT_THRESHOLD))
      setImportThreshold(setValue);
    else if (importLibrarySettingEnum.equals(ImportLibrarySettingEnum.IMPORT_NOMINAL))
      setImportNominal(setValue);
    else if (importLibrarySettingEnum.equals(ImportLibrarySettingEnum.IMPORT_LANDPATTERN))
      setImportLandPattern(setValue);
    else if (importLibrarySettingEnum.equals(ImportLibrarySettingEnum.IMPORT_LIBRARY_SUBTYPE_NAME))
      setImportLibrarySubtypeName(setValue);
    else
      Assert.expect(false, "Invalid ImportLibrarySettingEnum");
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean getImportLibrarySettingStatus(ImportLibrarySettingEnum importLibrarySettingEnum)
  {
    Assert.expect(importLibrarySettingEnum != null);
    
    if (importLibrarySettingEnum.equals(ImportLibrarySettingEnum.IMPORT_THRESHOLD))
      return isImportThreshold();
    else if (importLibrarySettingEnum.equals(ImportLibrarySettingEnum.IMPORT_NOMINAL))
      return isImportNominal();
    else if (importLibrarySettingEnum.equals(ImportLibrarySettingEnum.IMPORT_LANDPATTERN))
      return isImportLandPattern();
    else if (importLibrarySettingEnum.equals(ImportLibrarySettingEnum.IMPORT_LIBRARY_SUBTYPE_NAME))
      return isImportLibrarySubtypeName();
    else
      Assert.expect(false, "Invalid ImportLibrarySettingEnum");
    
    return false;
  }
}
