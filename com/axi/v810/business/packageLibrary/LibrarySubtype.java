package com.axi.v810.business.packageLibrary;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.projWriters.*;

/**
 * This class represents library subtype.
 *
 * @author Cheah Lee Herng
 */
public class LibrarySubtype
{
  private int _subtypeId = -1;
  private String _subtypeName;
  private String _subtypeComment;
  private List<LibraryPackagePin> _libraryPackagePins;
  private Map<AlgorithmSettingEnum, Serializable> _thresholdNameToTunedValueMap;
  private Map<String, Integer> _thresholdNameToAlgoVersionMap;
  private Map<String, String> _thresholdNameToThresholdCommentMap;
  private Subtype _projectSubtype;
  private String _jointTypeName;
  private UserGainEnum _userGainEnum;
  private SignalCompensationEnum _signalCompensationEnum;
  private ArtifactCompensationStateEnum _artifactCompensationStateEnum;
  private MagnificationTypeEnum _magnificationTypeEnum;
  private DynamicRangeOptimizationLevelEnum _dynamicRangeOptimizationLevelEnum;

  private InspectionFamilyEnum _inspectionFamilyEnum; // SubtypeSettingsReader
  private Map<AlgorithmEnum, Integer> _algorithmEnumToAlgorithmVersionMap = new HashMap<AlgorithmEnum, Integer>();
  private List<Algorithm> _algorithms;
  
  private List<LibraryComponentType> _libraryComponentTypes;
  private Map<AlgorithmEnum, Boolean> _algorithmEnumToEnabledFlagMap;
  
  private boolean _isLegacyUserGainEnum = false;
  private boolean _isLegacySignalCompensationEnum = false;
  private boolean _isLegacyArtifactCompensationEnum = false;
  private boolean _isLegacyMagnificationTypeEnum = false;
  private boolean _isLegacyDynamicRangeOptimizationLevelEnum = false;

  /**
   * @author Cheah Lee Herng
   */
  public LibrarySubtype()
  {
    _libraryPackagePins = new ArrayList<LibraryPackagePin>();
    _thresholdNameToTunedValueMap = new HashMap<AlgorithmSettingEnum,Serializable>();
    _thresholdNameToAlgoVersionMap = new HashMap<String,Integer>();
    _thresholdNameToThresholdCommentMap = new HashMap<String,String>();
    _libraryComponentTypes = new ArrayList<LibraryComponentType>();
    _algorithmEnumToEnabledFlagMap = new HashMap<AlgorithmEnum,Boolean>();
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getSubtypeId()
  {
    Assert.expect(_subtypeId > 0);
    return _subtypeId;
  }

  /**
   *
   * @param subtypeId int
   *
   * @author Cheah Lee Herng
   */
  public void setSubtypeId(int subtypeId)
  {
    Assert.expect(subtypeId > 0);
    _subtypeId = subtypeId;
  }

  /**
   *
   * @return String
   *
   * @author Cheah Lee Herng
   */
  public String getSubtypeName()
  {
    Assert.expect(_subtypeName != null);
    return _subtypeName;
  }

  /**
   *
   * @param subtypeName String
   *
   * @author Cheah Lee Herng
   */
  public void setSubtypeName(String subtypeName)
  {
    Assert.expect(subtypeName != null);
    _subtypeName = subtypeName;
  }

  /**
   *
   * @return String
   *
   * @author Cheah Lee Herng
   */
  public String getSubtypeComment()
  {
    Assert.expect(_subtypeComment != null);
    return _subtypeComment;
  }

  /**
   *
   * @param subtypeComment String
   *
   * @author Cheah Lee Herng
   */
  public void setSubtypeComment(String subtypeComment)
  {
    Assert.expect(subtypeComment != null);
    _subtypeComment = subtypeComment;
  }

  /**
   * @return List
   *
   * @author Cheah Lee Herng
   */
  public List<LibraryPackagePin> getLibraryPackagePins()
  {
    Assert.expect(_libraryPackagePins != null);
    return _libraryPackagePins;
  }

  /**
   * @param libraryPackagePins List
   *
   * @author Cheah Lee Herng
   */
  public void setLibraryPackagePins(List<LibraryPackagePin> libraryPackagePins)
  {
    Assert.expect(libraryPackagePins != null);
    _libraryPackagePins = libraryPackagePins;
  }

  /**
   *
   * @param libraryPackagePin LibraryPackagePin
   *
   * @author Cheah Lee Herng
   */
  public void addLibraryPackagePin(LibraryPackagePin libraryPackagePin)
  {
    Assert.expect(libraryPackagePin != null);
    _libraryPackagePins.add(libraryPackagePin);
  }

  /**
   *
   * @return Map
   *
   * @author Cheah Lee Herng
   */
  public Map<AlgorithmSettingEnum, Serializable> getThresholdNameToTunedValueMap()
  {
    Assert.expect(_thresholdNameToTunedValueMap != null);
    return _thresholdNameToTunedValueMap;
  }

  /**
   * @param algorithmSettingEnum AlgorithmSettingEnum
   * @param tunedValue Serializable
   * @author Cheah Lee Herng
   */
  public void addThresholdNameToTunedValue(AlgorithmSettingEnum algorithmSettingEnum, Serializable tunedValue)
  {
    Assert.expect(algorithmSettingEnum != null);
    Assert.expect(tunedValue != null);

    if(_thresholdNameToTunedValueMap == null)
      _thresholdNameToTunedValueMap = new HashMap<AlgorithmSettingEnum,Serializable>();

    Serializable prevTunedValue = _thresholdNameToTunedValueMap.put(algorithmSettingEnum, tunedValue);
    Assert.expect(prevTunedValue == null);
  }

  /**
   *
   * @return Map
   *
   * @author Cheah Lee Herng
   */
  public Map<String, Integer> getThresholdNameToAlgoVersionMap()
  {
    Assert.expect(_thresholdNameToAlgoVersionMap != null);
    return _thresholdNameToAlgoVersionMap;
  }

  /**
   * @return Map
   * @author Cheah Lee Herng
   */
  public Map<String,String> getThresholdNameToThresholdCommentMap()
  {
    Assert.expect(_thresholdNameToThresholdCommentMap != null);
    return _thresholdNameToThresholdCommentMap;
  }

  /**
   * @param thresholdName String
   * @param thresholdComment String
   *
   * @author Cheah Lee Herng
   */
  public void addThresholdNameToThresholdComment(String thresholdName, String thresholdComment)
  {
    Assert.expect(thresholdName != null);
    Assert.expect(thresholdComment != null);

    if(_thresholdNameToThresholdCommentMap == null)
      _thresholdNameToThresholdCommentMap = new HashMap<String,String>();

    String prevThresholdComment = _thresholdNameToThresholdCommentMap.put(thresholdName, thresholdComment);
    Assert.expect(prevThresholdComment == null);
  }

  /**
   * @param algorithmSettingEnum AlgorithmSettingEnum
   * @return String
   *
   * @author Cheah Lee Herng
   */
  public String getThresholdComment(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    String thresholdName = EnumStringLookup.getInstance().getAlgorithmSettingString(algorithmSettingEnum);
    String thresholdComment = "";

    if (_thresholdNameToThresholdCommentMap.containsKey(thresholdName))
    {
      thresholdComment = _thresholdNameToThresholdCommentMap.get(thresholdName);
    }

    return thresholdComment;
  }

  /**
   * @param thresholdName String
   * @param algoVersion int
   * @author Cheah Lee Herng
   */
  public void addThresholdNameToAlgoVersion(String thresholdName, int algoVersion)
  {
    Assert.expect(thresholdName != null);
    Assert.expect(algoVersion > 0);

    if(_thresholdNameToAlgoVersionMap == null)
      _thresholdNameToAlgoVersionMap = new HashMap<String,Integer>();

    Integer prevAlgoVersion = _thresholdNameToAlgoVersionMap.put(thresholdName, algoVersion);
    Assert.expect(prevAlgoVersion == null);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public Map<AlgorithmEnum,Boolean> getAlgorithmEnumToEnabledFlagMap()
  {
    Assert.expect(_algorithmEnumToEnabledFlagMap != null);
    return _algorithmEnumToEnabledFlagMap;
  }
  
  /**   
   * @param algorithmEnum
   * @param isEnabled 
   */
  public void addAlgorithmEnumToEnabledFlag(AlgorithmEnum algorithmEnum, boolean isEnabled)
  {
    Assert.expect(algorithmEnum != null);    

    if(_algorithmEnumToEnabledFlagMap == null)
      _algorithmEnumToEnabledFlagMap = new HashMap<AlgorithmEnum,Boolean>();

    Object prev = _algorithmEnumToEnabledFlagMap.put(algorithmEnum, isEnabled);
    Assert.expect(prev == null);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isAlgorithmEnabled(AlgorithmEnum algorithmEnum)
  {
    Assert.expect(algorithmEnum != null);
    
    if (_algorithmEnumToEnabledFlagMap.keySet().contains(algorithmEnum))   
      return _algorithmEnumToEnabledFlagMap.get(algorithmEnum);    
    else
      return false;
  }

  /**
   *
   * @return Subtype
   *
   * @author Cheah Lee Herng
   */
  public Subtype getProjectSubtype()
  {
    Assert.expect(_projectSubtype != null);
    return _projectSubtype;
  }

  /**
   *
   * @param projectSubtype Subtype
   *
   * @author Cheah Lee Herng
   */
  public void setProjectSubtype(Subtype projectSubtype)
  {
    Assert.expect(projectSubtype != null);
    _projectSubtype = projectSubtype;
  }

  /**
   *
   * @param algorithmSettingEnum AlgorithmSettingEnum
   * @return Serializable
   *
   * @author Cheah Lee Herng
   */
  public Serializable getAlgorithmSettingValue(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);
    Assert.expect(_thresholdNameToTunedValueMap != null);
    Assert.expect(_thresholdNameToAlgoVersionMap != null);

    Serializable value = null;
    //String algoSettingName = EnumStringLookup.getInstance().getAlgorithmSettingString(algorithmSettingEnum);
    if (_thresholdNameToTunedValueMap.containsKey(algorithmSettingEnum))
      value = _thresholdNameToTunedValueMap.get(algorithmSettingEnum);
    else
    {
      value = getAlgorithmSettingDefaultValue(algorithmSettingEnum);
    }

    Assert.expect(value != null, "Tried to get value for non-existent setting: " + algorithmSettingEnum );
    return value;
  }

  /**
   *
   * @return String
   *
   * @author Cheah Lee Herng
   */
  public String getJointTypeName()
  {
    Assert.expect(_jointTypeName != null);
    return _jointTypeName;
  }

  /**
   *
   * @param jointTypeName String
   *
   * @author Cheah Lee Herng
   */
  public void setJointTypeName(String jointTypeName)
  {
    Assert.expect(jointTypeName != null);
    _jointTypeName = jointTypeName;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public UserGainEnum getUserGainEnum()
  {
    return _userGainEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setUserGainEnum(UserGainEnum userGainEnum)
  {
    Assert.expect(userGainEnum != null);
    _userGainEnum = userGainEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setLegacyUserGainEnum(boolean legacyUserGainEnum)
  {
    _isLegacyUserGainEnum = legacyUserGainEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isLegacyUserGainEnum()
  {
    return _isLegacyUserGainEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public SignalCompensationEnum getSignalCompensationEnum()
  {
    return _signalCompensationEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setSignalCompensationEnum(SignalCompensationEnum signalCompensationEnum)
  {
    Assert.expect(signalCompensationEnum != null);    
    _signalCompensationEnum = signalCompensationEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setLegacySignalCompensationEnum(boolean legacySignalCompensationEnum)
  {
    _isLegacySignalCompensationEnum = legacySignalCompensationEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isLegacySignalCompensationEnum()
  {
    return _isLegacySignalCompensationEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public ArtifactCompensationStateEnum getArtifactCompensationStateEnum()
  {
    return _artifactCompensationStateEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setArtifactCompensationStateEnum(ArtifactCompensationStateEnum artifactCompensationStateEnum)
  {
    Assert.expect(artifactCompensationStateEnum != null);
    _artifactCompensationStateEnum = artifactCompensationStateEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setLegacyArtifactCompensationEnum(boolean legacyArtifactCompensationEnum)
  {
    _isLegacyArtifactCompensationEnum = legacyArtifactCompensationEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isLegacyArtifactCompensationEnum()
  {
    return _isLegacyArtifactCompensationEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public MagnificationTypeEnum getMagnificationTypeEnum()
  {
    return _magnificationTypeEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setMagnificationTypeEnum(MagnificationTypeEnum magnificationTypeEnum)
  {
    Assert.expect(magnificationTypeEnum != null);
    _magnificationTypeEnum = magnificationTypeEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setLegacyMagnificationTypeEnum(boolean legacyMagnificationTypeEnum)
  {
    _isLegacyMagnificationTypeEnum = legacyMagnificationTypeEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isLegacyMagnificationTypeEnum()
  {
    return _isLegacyMagnificationTypeEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public DynamicRangeOptimizationLevelEnum getDynamicRangeOptimizationLevelEnum()
  {
    return _dynamicRangeOptimizationLevelEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setDynamicRangeOptimizationLevelEnum(DynamicRangeOptimizationLevelEnum dynamicRangeOptimizationLevelEnum)
  {
    Assert.expect(dynamicRangeOptimizationLevelEnum != null);
    _dynamicRangeOptimizationLevelEnum = dynamicRangeOptimizationLevelEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setLegacyDynamicRangeOptimizationLevelEnum(boolean legacyDynamicRangeOptimizationLevelEnum)
  {
    _isLegacyDynamicRangeOptimizationLevelEnum = legacyDynamicRangeOptimizationLevelEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isLegacyDynamicRangeOptimizationLevelEnum()
  {
    return _isLegacyDynamicRangeOptimizationLevelEnum;
  }

  /**
   *
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String toString()
  {
    return getSubtypeName();
  }

  /**
   * Gets the AlgorithmSettingTypeEnum for the specified AlgorithmSettingEnum.
   * Algorithm version is intrinsically compensated for.
   *
   * @param algorithmSettingEnum AlgorithmSettingEnum
   * @return AlgorithmSettingTypeEnum
   *
   * @author Wei Chin, Chong
   */
  public AlgorithmSettingTypeEnum getAlgorithmSettingTypeEnum(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    AlgorithmSettingTypeEnum algorithmSettingTypeEnum = null;

    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
      {
        Integer algorithmVersion = _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum());
        Assert.expect(algorithmVersion != null);
        algorithmSettingTypeEnum = algorithm.getAlgorithmSettingTypeEnum(
            algorithmSettingEnum,
            algorithmVersion);

        break;
      }
    }

    Assert.expect(algorithmSettingTypeEnum != null);
    return algorithmSettingTypeEnum;
  }

  /**
   * @author Wei Chin, Chong
   */
  private void generateAlgorithms()
  {
    syncAlgorithmEnumToAlgorithmVersionMap();

    _algorithms = new ArrayList<Algorithm>();
    for (AlgorithmEnum algorithmEnum : _algorithmEnumToAlgorithmVersionMap.keySet())
    {
      if (InspectionFamily.hasAlgorithm(_inspectionFamilyEnum, algorithmEnum))
      {
        Algorithm algorithm = InspectionFamily.getAlgorithm(_inspectionFamilyEnum, algorithmEnum);
        _algorithms.add(algorithm);
      }
    }
    // make sure the list is in inspection order
    Collections.sort(_algorithms, new AlgorithmInspectionOrderComparator());
  }

  /**
   * @author Wei Chin, Chong
   */
  private void syncAlgorithmEnumToAlgorithmVersionMap()
  {
    // We need to make sure our alg enum->version map is properly updated when we add a new algorithm
    // to a family.  This ensures that the algorithm is properly "registered" to this Subtype when an
    // existing project is loaded.  The new algorithm is disabled by default.
    InspectionFamily inspectionFamily = InspectionFamily.getInstance(_inspectionFamilyEnum);
    for (AlgorithmEnum algEnum : inspectionFamily.getAlgorithmEnums())
    {
      if (_algorithmEnumToAlgorithmVersionMap.containsKey(algEnum) == false)
      {
        Algorithm algorithm = InspectionFamily.getAlgorithm(_inspectionFamilyEnum, algEnum);
        _algorithmEnumToAlgorithmVersionMap.put(algEnum, algorithm.getVersion());
      }
    }
  }

  /**
   * @return a List of all Algorithms for this subtype, both enabled and disabled
   *
   * PE:  This needs to be sychronized so that the generateAlgorithms() call doesn't get out of sync when
   * multiple threads call this method.
   *
   * @author Wei Chin, Chong
   */
  public synchronized List<Algorithm> getAlgorithms()
  {
    if (_algorithms == null)
      generateAlgorithms();

    return _algorithms;
  }

  /**
   * @param inspectionFamilyEnum InspectionFamilyEnum
   *
   * @author Wei Chin, Chong
   */
  public void setInspectionFamilyEnum(InspectionFamilyEnum inspectionFamilyEnum)
  {
    Assert.expect(inspectionFamilyEnum != null);

    _inspectionFamilyEnum = inspectionFamilyEnum;
  }

  /**
   * @return InspectionFamily
   *
   * @author Wei Chin, Chong
   */
  public InspectionFamily getInspectionFamily()
  {
    Assert.expect(_inspectionFamilyEnum != null);
    return InspectionFamily.getInstance(_inspectionFamilyEnum);
  }

  /**
   * @return InspectionFamilyEnum
   *
   * @author Wei Chin, Chong
   */
  public InspectionFamilyEnum getInspectionFamilyEnum()
  {
    Assert.expect(_inspectionFamilyEnum != null);
    return _inspectionFamilyEnum;
  }

  /**
   * @param algorithmSettingEnum AlgorithmSettingEnum
   * @return Serializable
   *
   * @author Wei Chin, Chong
   */
  public Serializable getAlgorithmSettingDefaultValue(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);
    Serializable value = null;
    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
        value = algorithm.getSettingDefaultValue(algorithmSettingEnum,
                                                 _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
    }

    Assert.expect(value != null);
    return value;
  }

  /**
   * @return a List of all AlgorithmEnums for this subtype, both enabled and disabled
   * @author Wei Chin, Chong
   */
  public List<AlgorithmEnum> getAlgorithmEnums()
  {
    syncAlgorithmEnumToAlgorithmVersionMap();

    ArrayList<AlgorithmEnum> algorithmEnums = new ArrayList<AlgorithmEnum>(_algorithmEnumToAlgorithmVersionMap.keySet());
    Collections.sort(algorithmEnums, new AlgorithmEnumInspectionOrderComparator());

    return algorithmEnums;
  }

  /**
   * @param algorithmSettingEnum AlgorithmSettingEnum
   * @return MeasurementUnitsEnum
   *
   * @author Wei Chin, Chong
   */
  public MeasurementUnitsEnum getAlgorithmSettingUnitsEnum(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    MeasurementUnitsEnum units = null;

    for (Algorithm algorithm : getAlgorithms())
    {
      if (algorithm.doesAlgorithmSettingExist(algorithmSettingEnum))
      {
        units = algorithm.getSettingUnitsEnum(algorithmSettingEnum,
                                          _algorithmEnumToAlgorithmVersionMap.get(algorithm.getAlgorithmEnum()));
      }
    }

    Assert.expect(units != null);
    return units;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {
    Assert.expect(_libraryPackagePins != null);
    Assert.expect(_thresholdNameToTunedValueMap != null);
    Assert.expect(_thresholdNameToAlgoVersionMap != null);
    Assert.expect(_thresholdNameToThresholdCommentMap != null);

    _libraryPackagePins.clear();
    _thresholdNameToTunedValueMap.clear();
    _thresholdNameToAlgoVersionMap.clear();
    _thresholdNameToThresholdCommentMap.clear();
  }

  /**
   * @author Tan Hock Zoon
   */
  public void clearThresholdNameToTunedValueMap()
  {
    Assert.expect(_thresholdNameToTunedValueMap != null);
    _thresholdNameToTunedValueMap.clear();
  }

  /**
   * @author Tan Hock Zoon
   */
  public void clearThresholdNameToThresholdCommentMap()
  {
    Assert.expect(_thresholdNameToTunedValueMap != null);
    _thresholdNameToThresholdCommentMap.clear();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clearAlgorithmEnumToEnabledFlagMap()
  {
    Assert.expect(_algorithmEnumToEnabledFlagMap != null);
    _algorithmEnumToEnabledFlagMap.clear();
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void addLibraryComponentType(LibraryComponentType libraryComponentType)
  {
    Assert.expect(libraryComponentType != null);
    
    if (_libraryComponentTypes.contains(libraryComponentType) == false)
      _libraryComponentTypes.add(libraryComponentType);    
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public List<LibraryComponentType> getLibraryComponentTypes()
  {
    return _libraryComponentTypes;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clearLibraryComponentTypeList()
  {
    if (_libraryComponentTypes != null)
      _libraryComponentTypes.clear();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasLibraryComponentTypes()
  {
    if (_libraryComponentTypes == null || _libraryComponentTypes.isEmpty())
      return false;
    else
      return true;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isComponentTypeExist(String componentRefDes)
  {
    Assert.expect(componentRefDes != null);
    
    for(LibraryComponentType componentType : getLibraryComponentTypes())
    {
      if (componentType.getReferenceDesignator().equalsIgnoreCase(componentRefDes))
        return true;
    }
    return false;
  }
}
