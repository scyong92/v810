package com.axi.v810.business.packageLibrary;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * This class represents library subtype scheme.
 *
 * @author Cheah Lee Herng
 */
public class LibrarySubtypeScheme implements Comparable
{
  private final static String _MIXED = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");

  private int _librarySubtypeSchemeId = -1;
  private int _subtypeSchemeId = -1;
  private int _packageId = -1;
  private Map<LibrarySubtype, List<LibraryPackagePin>> _librarySubtypeToLibraryPackagePinMap;
  private List<LibrarySubtype> _librarySubtypes;
  private LibraryPackage _libraryPackage;
  private Set<LibraryPackagePin> _libraryPackagePinSet;
  private boolean _useOneSubtype;
  private boolean _useOneJointType;
  // this value range from -1 to 1. 1 means positive linear match and 0 mean totally non linear match.
  // -1 means perfect negative linear match
  private double _correlationScore = -2;
  private double _matchPercentage = -1;
  private Date _dateCreated;
  private Date _dateUpdated;
  
  private String _libraryProjectName;

  /**
   * @author Cheah Lee Herng
   */
  public LibrarySubtypeScheme()
  {
    _librarySubtypeToLibraryPackagePinMap = new HashMap<LibrarySubtype,List<LibraryPackagePin>>();
    _librarySubtypes = new ArrayList<LibrarySubtype>();
    _libraryPackagePinSet = new TreeSet<LibraryPackagePin>(new LibraryPackagePinComparator(true));
    
    _libraryProjectName = "";
  }

  /**
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getLibrarySubtypeSchemeId()
  {
    Assert.expect(_librarySubtypeSchemeId > 0);
    return _librarySubtypeSchemeId;
  }

  /**
   * @param librarySubtypeSchemeId int
   *
   * @author Cheah Lee Herng
   */
  public void setLibrarySubtypeSchemeId(int librarySubtypeSchemeId)
  {
    Assert.expect(librarySubtypeSchemeId > 0);
    _librarySubtypeSchemeId = librarySubtypeSchemeId;
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getSubtypeSchemeId()
  {
    Assert.expect(_subtypeSchemeId > 0);
    return _subtypeSchemeId;
  }

  /**
   *
   * @param subtypeSchemeId int
   *
   * @author Cheah Lee Herng
   */
  public void setSubtypeSchemeId(int subtypeSchemeId)
  {
    Assert.expect(subtypeSchemeId > 0);
    _subtypeSchemeId = subtypeSchemeId;
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getPackageId()
  {
    Assert.expect(_packageId > 0);
    return _packageId;
  }

  /**
   *
   * @param packageId int
   *
   * @author Cheah Lee Herng
   */
  public void setPackageId(int packageId)
  {
    Assert.expect(packageId > 0);
    _packageId = packageId;
  }

  /**
   *
   * @return Map
   *
   * @author Cheah Lee Herng
   */
  public Map<LibrarySubtype, List<LibraryPackagePin>> getLibrarySubtypeToLibraryPackagePinMap()
  {
    Assert.expect(_librarySubtypeToLibraryPackagePinMap != null);
    return _librarySubtypeToLibraryPackagePinMap;
  }

  /**
   * @return List
   *
   * @author Cheah Lee Herng
   */
  public List<LibrarySubtype> getLibrarySubtypes()
  {
    Assert.expect(_librarySubtypes != null);
    return _librarySubtypes;
  }

  /**
   * @param librarySubtypes List
   *
   * @author Cheah Lee Herng
   */
  public void setLibrarySubtypes(List<LibrarySubtype> librarySubtypes)
  {
    Assert.expect(librarySubtypes != null);
    _librarySubtypes = librarySubtypes;
  }

  /**
   *
   * param librarySubtype LibrarySubtype
   *
   * @author Cheah Lee Herng
   */
  public void addLibrarySubtype(LibrarySubtype librarySubtype)
  {
    Assert.expect(_librarySubtypes != null);
    _librarySubtypes.add(librarySubtype);
  }

  /**
   *
   * @return LibraryPackage
   *
   * @author Cheah Lee Herng
   */
  public LibraryPackage getLibraryPackage()
  {
    Assert.expect(_libraryPackage != null);
    return _libraryPackage;
  }

  /**
   *
   * @param libraryPackage LibraryPackage
   *
   * @author Cheah Lee Herng
   */
  public void setLibraryPackage(LibraryPackage libraryPackage)
  {
    Assert.expect(libraryPackage != null);
    _libraryPackage = libraryPackage;
  }

  /**
   *
   * @return boolean
   *
   * @author Cheah Lee Herng
   */
  public boolean useOneSubtype()
  {
    return _useOneSubtype;
  }

  /**
   *
   * @param useOneSubtype boolean
   *
   * @author Cheah Lee Herng
   */
  public void setUseOneSubtype(boolean useOneSubtype)
  {
    _useOneSubtype = useOneSubtype;
  }

  /**
   *
   * @return boolean
   *
   * @author Cheah Lee Herng
   */
  public boolean useOneJointType()
  {
    return _useOneJointType;
  }

  /**
   *
   * @param useOneJointType boolean
   *
   * @author Cheah Lee Herng
   */
  public void setUseOneJointType(boolean useOneJointType)
  {
    _useOneJointType = useOneJointType;
  }

  /**
   *
   * @return List
   *
   * @author Cheah Lee Herng
   */
  public List<LibraryPackagePin> getLibraryPackagePins()
  {
    Assert.expect(_libraryPackagePinSet != null);
    return new ArrayList<LibraryPackagePin>(_libraryPackagePinSet);
  }

  /**
   *
   * @param libraryPackagePinSet Set
   *
   * @author Cheah Lee Herng
   */
  public void setLibraryPackagePins(Set<LibraryPackagePin> libraryPackagePinSet)
  {
    Assert.expect(libraryPackagePinSet != null);
    _libraryPackagePinSet = libraryPackagePinSet;
  }

  /**
   *
   * @param libraryPackagePin LibraryPackagePin
   *
   * @author Cheah Lee Herng
   */
  public void addLibraryPackagePin(LibraryPackagePin libraryPackagePin)
  {
    Assert.expect(_libraryPackagePinSet != null);
    _libraryPackagePinSet.add(libraryPackagePin);
  }

  /**
   *
   * @author Poh Kheng
   */
  public double getCorrelationScore()
  {
    Assert.expect(_correlationScore >= 0);
    Assert.expect(_correlationScore <= 1);
    return _correlationScore;
  }

  /**
   *
   * @param correlationScore double
   *
   * @author Poh Kheng
   */
  public void setCorrelationScore(double correlationScore)
  {
    Assert.expect(correlationScore >= MathUtil._CORRELATION_MIN_VALUE);
    Assert.expect(correlationScore <= MathUtil._CORRELATION_MAX_VALUE);

    // if the correlation is have negative linear relationship, this would be consider non match.
    if(correlationScore < 0)
      correlationScore = 0;

    _correlationScore = correlationScore;
  }

  /**
   *
   * @author Poh Kheng
   */
  public double getMatchPercentage()
  {
    Assert.expect(_matchPercentage >= 0.00);
    Assert.expect(_matchPercentage <= 100.00);
    return MathUtil.roundToPlaces(_matchPercentage, 2);
  }

  /**
   *
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String toString()
  {
    return getName();
  }

  /**
   *
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String getName()
  {
    String value = null;

    if (_useOneSubtype)
    {
      value = _librarySubtypes.get(0).getSubtypeName();
    }
    else
    {
      value = _MIXED;
    }

    return value;
  }

  /**
   *
   * @author Poh Kheng
   */
  public void setMatchPercentage(double matchPercentage)
  {
    Assert.expect(matchPercentage >= 0.00);
    Assert.expect(matchPercentage <= 100.00);
    _matchPercentage = MathUtil.roundToPlaces(matchPercentage, 2);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {
    if(_librarySubtypeToLibraryPackagePinMap != null)
      _librarySubtypeToLibraryPackagePinMap.clear();

    if(_librarySubtypes != null)
      _librarySubtypes.clear();

    if(_libraryPackagePinSet != null)
      _libraryPackagePinSet.clear();

    _librarySubtypeToLibraryPackagePinMap = null;
    _librarySubtypes = null;
    _libraryPackagePinSet = null;
    _libraryPackage = null;
  }

  /**
   *
   * @param dateCreated Date
   *
   * @author Tan Hock Zoon
   */
  public void setDateCreated(Date dateCreated)
  {
    Assert.expect(dateCreated != null);
    _dateCreated = dateCreated;
  }

  /**
   *
   * @return Date
   *
   * @author Tan Hock Zoon
   */
  public Date getDateCreated()
  {
    Assert.expect(_dateCreated != null);
    return _dateCreated;
  }

  /**
   *
   * @return boolean
   *
   * @author Tan Hock Zoon
   */
  public boolean hasDateCreated()
  {
    return (_dateCreated != null);
  }

  /**
   *
   * @param dateUpdated Date
   *
   * @author Tan Hock Zoon
   */
  public void setDateUpdated(Date dateUpdated)
  {
    Assert.expect(dateUpdated != null);
    _dateUpdated = dateUpdated;
  }

  /**
   *
   * @return Date
   *
   * @author Tan Hock Zoon
   */
  public Date getDateUpdated()
  {
    Assert.expect(_dateUpdated != null);
    return _dateUpdated;
  }

  /**
   *
   * @return boolean
   *
   * @author Tan Hock Zoon
   */
  public boolean hasDateUpdated()
  {
    return (_dateUpdated != null);
  }

  /**
   * @author Tan Hock Zoon
   */
  public void clearLibrarySubtypeList()
  {
    _librarySubtypes.clear();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setLibraryProjectName(String libraryProjectName)
  {
    Assert.expect(libraryProjectName != null);
    _libraryProjectName = libraryProjectName;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public String getLibraryProjectName()
  {
    Assert.expect(_libraryProjectName != null);
    return _libraryProjectName;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isLegacy()
  {
    boolean isLegacy = false;    
    for(LibrarySubtype librarySubtype : getLibrarySubtypes())
    {
      isLegacy |= librarySubtype.isLegacyUserGainEnum();
      isLegacy |= librarySubtype.isLegacySignalCompensationEnum();
      isLegacy |= librarySubtype.isLegacyArtifactCompensationEnum();
      isLegacy |= librarySubtype.isLegacyMagnificationTypeEnum();
      isLegacy |= librarySubtype.isLegacyDynamicRangeOptimizationLevelEnum();
    }    
    return isLegacy;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public int compareTo(Object rhs)
  {
    Assert.expect(rhs instanceof LibrarySubtypeScheme, "LibrarySubtypeScheme should only be compared to other LibrarySubtypeScheme!");
    
    LibrarySubtypeScheme librarySubtypeScheme = (LibrarySubtypeScheme)rhs;
    
    String lhsName = getName();
    String rhsName = librarySubtypeScheme.getName();
    
    return lhsName.compareTo(rhsName);
  }
}
