package com.axi.v810.business.packageLibrary;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Wei Chin, CHong
 * @version 1.0
 */
public class Test_SubtypeScheme extends UnitTest
{
  /**
   * @author Wei Chin, Chong
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_SubtypeScheme());
  }

  /**
   * @param is BufferedReader
   * @param os PrintWriter
   * @author Wei Chin, Chong
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testWithProjectLoaded();
  }

  /**
   * @author Wei Chin, Chong
   */
  private void testWithProjectLoaded()
  {
    String libraryPath = "C:/Test/ABC";
    String newSubtypeName = "newSubtypeName";

    Project project = new Project(false);

    try
    {
      // test JointTypeEnum
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();
      project = Project.importProjectFromNdfs("smallPanel", warnings);

      ComponentType componentType = project.getPanel().getComponentTypes().get(0);
      SubtypeScheme subtypeScheme = new SubtypeScheme(componentType);

      Expect.expect( subtypeScheme.usesOneJointTypeEnum() == componentType.getCompPackage().usesOneJointTypeEnum());
      Expect.expect( subtypeScheme.usesOneSubtype() == componentType.getComponentTypeSettings().usesOneSubtype());
      Expect.expect( subtypeScheme.getSubtypes().size() == componentType.getSubtypes().size() );

      subtypeScheme.setLibraryPath(libraryPath);
      Expect.expect(subtypeScheme.getLibraryPath().endsWith(libraryPath));

      if( subtypeScheme.usesOneSubtype() )
      {
        subtypeScheme.setNewSubtypeName(newSubtypeName);
        Expect.expect(subtypeScheme.hasNewSubTypeName());
        Expect.expect(subtypeScheme.getNewSubtypeName().endsWith(newSubtypeName));

        subtypeScheme.setNewSubtypeName(subtypeScheme.getSubtype().getShortName());
        Expect.expect(subtypeScheme.hasNewSubTypeName() == false);
      }
    }
    catch (Exception ex)
    {
      Expect.expect(false);
      ex.printStackTrace();
    }
  }
}
