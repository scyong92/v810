package com.axi.v810.business.packageLibrary;

import java.io.*;
import java.text.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 *
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class SubtypeSchemeComparator implements Comparator <SubtypeScheme>
{
  private boolean _ascending;
  private SubtypeSchemeComparatorEnum _subtypeSchemeComparatorEnum;


  /** @todo APM Code Review Sorry, I know George said they don't need to be localized, but I feel that they do.
    * The reason is, these are visible strings displayed in the table, and we need to be sorting on these strings that
    * are the same strings that are displayed to the user.  If we changed the localization string in the table model from
    * "Mixed" to "Variable" (or something), then this sorting code wouldn't work anymore.  If set _MIXED to the localized
    * string, then all would work just fine. */
  private final String _MIXED = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
  private final String _UPDATE = StringLocalizer.keyToString("PLWK_EXPORT_UPDATE_TABLE_FIELD_KEY");
  private final String _ADD = StringLocalizer.keyToString("PLWK_EXPORT_ADD_TABLE_FIELD_KEY");
  private final String _MODIFIED = StringLocalizer.keyToString("PLWK_EXPORT_MODIFIED_TABLE_FIELD_KEY");
  private final String _NEW = StringLocalizer.keyToString("PLWK_EXPORT_NEW_TABLE_FIELD_KEY");
  private final String _IMPORTED = StringLocalizer.keyToString("PLWK_EXPORT_IMPORTED_TABLE_FIELD_KEY");
  private final String _NONE = StringLocalizer.keyToString("PLWK_EXPORT_NONE_SOURCE_LIBRARY_TABLE_FIELD_KEY");
  private SimpleDateFormat _simpleDateFormat = new SimpleDateFormat("MMM-dd-yyyy");

  /**
   * @param ascending boolean
   * @param subtypeSchemeComparatorEnum SubtypeSchemeComparatorEnum
   *
   * @author Wei Chin, Chong
   */
  public SubtypeSchemeComparator(boolean ascending, SubtypeSchemeComparatorEnum subtypeSchemeComparatorEnum)
  {
    Assert.expect(subtypeSchemeComparatorEnum != null);

    _ascending = ascending;
    _subtypeSchemeComparatorEnum = subtypeSchemeComparatorEnum;
  }

  /**
   * @param lhSubtypeScheme SubtypeScheme
   * @param rhSubtypeScheme SubtypeScheme
   * @return int
   *
   * @author Wei Chin, Chong
   */
  public int compare(SubtypeScheme lhSubtypeScheme, SubtypeScheme rhSubtypeScheme)
  {
    Assert.expect(lhSubtypeScheme != null);
    Assert.expect(rhSubtypeScheme != null);

    String lhName;
    String rhName;

    if(_subtypeSchemeComparatorEnum.equals(SubtypeSchemeComparatorEnum.PACKAGE_NAME))
    {
      lhName = lhSubtypeScheme.getPackageShortName();
      rhName = rhSubtypeScheme.getPackageShortName();

      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if(_subtypeSchemeComparatorEnum.equals(SubtypeSchemeComparatorEnum.JOINT_TYPE))
    {
      /** @todo APM Code Review This code is basically identical to code in ComponentTypeComparator -- can we
        * somehow leverage that code (call it here?) so to avoid the duplicate complex code? */
      lhName = _MIXED;
      rhName = _MIXED;
      if (lhSubtypeScheme.usesOneJointTypeEnum())
        lhName = lhSubtypeScheme.getJointTypeEnum().getName();
      if (rhSubtypeScheme.usesOneJointTypeEnum())
        rhName = rhSubtypeScheme.getJointTypeEnum().getName();
      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if(_subtypeSchemeComparatorEnum.equals(SubtypeSchemeComparatorEnum.SUBTYPE_NAME))
    {
      /** @todo APM Code Review This code is basically identical to code in ComponentTypeComparator -- can we
        * somehow leverage that code (call it here?) so to avoid the duplicate complex code? */
      lhName = _MIXED;
      rhName = _MIXED;
      if (lhSubtypeScheme.usesOneSubtype())
        lhName = lhSubtypeScheme.getSubtype().getShortName();
      if (rhSubtypeScheme.usesOneSubtype())
        rhName = rhSubtypeScheme.getSubtype().getShortName();
      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if (_subtypeSchemeComparatorEnum.equals(SubtypeSchemeComparatorEnum.ADD_UPDATE))
    {
      if (lhSubtypeScheme.isUpdatable())
        lhName = _UPDATE;
      else
        lhName = _ADD;

      if (rhSubtypeScheme.isUpdatable())
        rhName = _UPDATE;
      else
        rhName = _ADD;

      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if (_subtypeSchemeComparatorEnum.equals(SubtypeSchemeComparatorEnum.NEW_MODIFIED))
    {
      if(lhSubtypeScheme.isImportedSubtypeScheme() == false)
        lhName = _NEW;
      else if (lhSubtypeScheme.isModifiedAfterImported())
        lhName = _MODIFIED;
      else
        lhName = _IMPORTED;

      if(rhSubtypeScheme.isImportedSubtypeScheme() == false)
        rhName = _NEW;
      else if (rhSubtypeScheme.isModifiedAfterImported())
        rhName = _MODIFIED;
      else
        rhName = _IMPORTED;

      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if (_subtypeSchemeComparatorEnum.equals(SubtypeSchemeComparatorEnum.EXPORT_LIBRARY_PATH))
    {
      lhName = lhSubtypeScheme.getLibraryPath();
      rhName = rhSubtypeScheme.getLibraryPath();

      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if (_subtypeSchemeComparatorEnum.equals(SubtypeSchemeComparatorEnum.IMPORT_LIBRARY_PATH))
    {
      if (lhSubtypeScheme.getLibraryPathList().size() > 0)
      {
        lhName = lhSubtypeScheme.getSelectedLibraryPath();
        lhName = lhName.substring(lhName.lastIndexOf(File.separator) + 1, lhName.length());
      }
      else
      {
        lhName = _NONE;
      }

      if (rhSubtypeScheme.getLibraryPathList().size() > 0)
      {
        rhName = rhSubtypeScheme.getSelectedLibraryPath();
        rhName = rhName.substring(rhName.lastIndexOf(File.separator) + 1, rhName.length());
      }
      else
      {
        rhName = _NONE;
      }

      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if (_subtypeSchemeComparatorEnum.equals(SubtypeSchemeComparatorEnum.IMPORT_LIBRARY_PACKAGE))
    {
      if (lhSubtypeScheme.getLibraryPathList().size() > 0)
      {
        lhName = lhSubtypeScheme.getSelectedLibraryPackage().getPackageLongName();
      }
      else
      {
        lhName = _NONE;
      }

      if (rhSubtypeScheme.getLibraryPathList().size() > 0)
      {
        rhName = rhSubtypeScheme.getSelectedLibraryPackage().getPackageLongName();
      }
      else
      {
        rhName = _NONE;
      }

      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if (_subtypeSchemeComparatorEnum.equals(SubtypeSchemeComparatorEnum.IMPORT_LIBRARY_JOINT_TYPE))
    {
      if (lhSubtypeScheme.getLibraryPathList().size() > 0)
      {
        lhName = lhSubtypeScheme.getSelectedLibraryPackage().getSelectedJointType();
      }
      else
      {
        lhName = _NONE;
      }

      if (rhSubtypeScheme.getLibraryPathList().size() > 0)
      {
        rhName = rhSubtypeScheme.getSelectedLibraryPackage().getSelectedJointType();
      }
      else
      {
        rhName = _NONE;
      }

      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if (_subtypeSchemeComparatorEnum.equals(SubtypeSchemeComparatorEnum.IMPORT_LIBRARY_PROJECT))
    {
      if (lhSubtypeScheme.getLibraryPathList().size() > 0)
      {
        if (lhSubtypeScheme.getSelectedLibraryPackage().isSelectedMixedLibraryProjectName())
          lhName = _MIXED;
        else
          lhName = lhSubtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getLibraryProjectName();
      }
      else
      {
        lhName = _NONE;
      }
      
      if (rhSubtypeScheme.getLibraryPathList().size() > 0)
      {
        if (rhSubtypeScheme.getSelectedLibraryPackage().isSelectedMixedLibraryProjectName())
          rhName = _MIXED;
        else
          rhName = rhSubtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getLibraryProjectName();
      }
      else
      {
        rhName = _NONE;
      }
      
      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if (_subtypeSchemeComparatorEnum.equals(SubtypeSchemeComparatorEnum.IMPORT_LIBRARY_SUBTYPE))
    {
      if (lhSubtypeScheme.getLibraryPathList().size() > 0)
      {
        if (lhSubtypeScheme.getSelectedLibraryPackage().hasMixedLibrarySubtypeScheme(lhSubtypeScheme.getComponentTypes()))
          lhName = _MIXED;
        else
          lhName = lhSubtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getName();
      }
      else
      {
        lhName = _NONE;
      }

      if (rhSubtypeScheme.getLibraryPathList().size() > 0)
      {
        if (rhSubtypeScheme.getSelectedLibraryPackage().hasMixedLibrarySubtypeScheme(rhSubtypeScheme.getComponentTypes()))
          rhName = _MIXED;
        else
          rhName = rhSubtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getName();
      }
      else
      {
        rhName = _NONE;
      }

      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if (_subtypeSchemeComparatorEnum.equals(SubtypeSchemeComparatorEnum.IMPORT_LIBRARY_MATCH_PERCENTAGE))
    {
      Double lhDouble = 0d;
      Double rhDouble = 0d;

      if (lhSubtypeScheme.getLibraryPathList().size() > 0)
      {
        if (lhSubtypeScheme.getSelectedLibraryPackage().hasMixedLibrarySubtypeScheme(lhSubtypeScheme.getComponentTypes()) == false)
          lhDouble = lhSubtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getMatchPercentage();
      }

      if (rhSubtypeScheme.getLibraryPathList().size() > 0)
      {
        if (rhSubtypeScheme.getSelectedLibraryPackage().hasMixedLibrarySubtypeScheme(rhSubtypeScheme.getComponentTypes()) == false)
          rhDouble = rhSubtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getMatchPercentage();
      }

      if (_ascending)
        return lhDouble.compareTo(rhDouble);
      else
        return rhDouble.compareTo(lhDouble);
    }
    else if (_subtypeSchemeComparatorEnum.equals(SubtypeSchemeComparatorEnum.IMPORT_LIBRARY_SUBTYPE_DATE))
    {
      if (lhSubtypeScheme.getLibraryPathList().size() > 0)
      {
        if (lhSubtypeScheme.getSelectedLibraryPackage().hasMixedLibrarySubtypeScheme(lhSubtypeScheme.getComponentTypes()))
        {
          lhName = _NONE;
        }
        else
        {
          if (lhSubtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().hasDateUpdated())
            lhName = _simpleDateFormat.format(lhSubtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getDateUpdated());
          else
            lhName = _simpleDateFormat.format(lhSubtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getDateCreated());
        }
      }
      else
      {
        lhName = _NONE;
      }

      if (rhSubtypeScheme.getLibraryPathList().size() > 0)
      {
        if (rhSubtypeScheme.getSelectedLibraryPackage().hasMixedLibrarySubtypeScheme(rhSubtypeScheme.getComponentTypes()))
        {
          rhName = _NONE;
        }
        else
        {
          if (rhSubtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().hasDateUpdated())
            rhName = _simpleDateFormat.format(rhSubtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getDateUpdated());
          else
            rhName = _simpleDateFormat.format(rhSubtypeScheme.getSelectedLibraryPackage().getSelectedLibrarySubtypeScheme().getDateCreated());
        }
      }
      else
      {
        rhName = _NONE;
      }

      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else
    {
      Assert.expect(false, "Please add your new subtypeScheme attribute to sort on to this method");
      return 0;
    }
  }
}
