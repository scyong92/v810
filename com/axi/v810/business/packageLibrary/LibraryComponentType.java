package com.axi.v810.business.packageLibrary;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Cheah Lee Herng
 */
public class LibraryComponentType
{
  private int _componentTypeId;
  private String _refDes;
  private GlobalSurfaceModelEnum _globalSurfaceModelEnum;
  
  private boolean _isLegacyGlobalSurfaceModelEnum = false;
  
  /**
   * Default Constructor
   * @author Cheah Lee Herng
   */
  public LibraryComponentType()
  {
    _refDes = "";
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setComponentTypeId(int componentTypeId)
  {
    _componentTypeId = componentTypeId;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getComponentTypeId()
  {
    return _componentTypeId;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setReferenceDesignator(String refDes)
  {
    Assert.expect(refDes != null);
    _refDes = refDes;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public String getReferenceDesignator()
  {
    Assert.expect(_refDes != null);
    return _refDes;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setGlobalSurfaceModelEnum(GlobalSurfaceModelEnum globalSurfaceModelEnum)
  {
    Assert.expect(globalSurfaceModelEnum != null);
    _globalSurfaceModelEnum = globalSurfaceModelEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public GlobalSurfaceModelEnum getGlobalSurfaceModelEnum()
  {
    Assert.expect(_globalSurfaceModelEnum != null);
    return _globalSurfaceModelEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setLegacyGlobalSurfaceModelEnum(boolean legacyGlobalSurfaceModelEnum)
  {
    _isLegacyGlobalSurfaceModelEnum = legacyGlobalSurfaceModelEnum;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isLegacyGlobalSurfaceModelEnum()
  {
    return _isLegacyGlobalSurfaceModelEnum;
  }
}
