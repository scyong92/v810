package com.axi.v810.business.packageLibrary;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This class represents library package pin.
 *
 * @author Cheah Lee Herng
 */
public class LibraryPackagePin
{
  private int _packagePinId = -1;
  private int _landPatternPadId = -1;
  private int _refJointTypeId = -1;
  private int _orientation = -1;
  private double _jointHeight = -1;
  private int _packageId = -1;
  private String _landPatternPadName;
  private String _jointTypeName;
  private LibraryPackage _libraryPackage;
  private LibraryLandPatternPad _libraryLandPatternPad;
  private LibrarySubtype _librarySubtype;
  private BooleanRef _inspected;

  /**
   * @author Cheah Lee Herng
   */
  public LibraryPackagePin()
  {
    _inspected = new BooleanRef(false);
  }

  /**
   * @return int
   * @author Cheah Lee Herng
   */
  public int getPackagePinId()
  {
    Assert.expect(_packagePinId > 0);
    return _packagePinId;
  }

  /**
   * @param packagePinId int
   * @author Cheah Lee Herng
   */
  public void setPackagePinId(int packagePinId)
  {
    Assert.expect(packagePinId > 0);
    _packagePinId = packagePinId;
  }

  /**
   * @return int
   * @author Cheah Lee Herng
   */
  public int getLandPatternPadId()
  {
    Assert.expect(_landPatternPadId > 0);
    return _landPatternPadId;
  }

  /**
   * @param landPatternPadId int
   * @author Cheah Lee Herng
   */
  public void setLandPatternPadId(int landPatternPadId)
  {
    Assert.expect(landPatternPadId > 0);
    _landPatternPadId = landPatternPadId;
  }

  /**
   * @return int
   * @author Cheah Lee Herng
   */
  public int getRefJointTypeId()
  {
    Assert.expect(_refJointTypeId > 0);
    return _refJointTypeId;
  }

  /**
   * @param refJointTypeId int
   * @author Cheah Lee Herng
   */
  public void setRefJointTypeId(int refJointTypeId)
  {
    Assert.expect(refJointTypeId > 0);
    _refJointTypeId = refJointTypeId;
  }

  /**
   * @return int
   * @author Cheah Lee Herng
   */
  public int getOrientation()
  {
    Assert.expect(_orientation >= 0 && _orientation <= 360);
    return _orientation;
  }

  /**
   * @param orientation int
   * @author Cheah Lee Herng
   */
  public void setOrientation(int orientation)
  {
    Assert.expect(orientation >= 0 && orientation <= 360);
    _orientation = orientation;
  }

  /**
   * @return double
   * @author Cheah Lee Herng
   */
  public double getJointHeight()
  {
    Assert.expect(_jointHeight >= 0);
    return _jointHeight;
  }

  /**
   * @param jointHeight double
   * @author Cheah Lee Herng
   */
  public void setJointHeight(double jointHeight)
  {
    Assert.expect(jointHeight >= 0);
    _jointHeight = jointHeight;
  }

  /**
   * @return int
   * @author Cheah Lee Herng
   */
  public int getPackageId()
  {
    Assert.expect(_packageId > 0);
    return _packageId;
  }

  /**
   * @param packageId int
   * @author Cheah Lee Herng
   */
  public void setPackageId(int packageId)
  {
    Assert.expect(packageId > 0);
    _packageId = packageId;
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public String getLandPatternPadName()
  {
    Assert.expect(_landPatternPadName != null);
    return _landPatternPadName;
  }

  /**
   * @param landPatternPadName String
   * @author Cheah Lee Herng
   */
  public void setLandPatternPadName(String landPatternPadName)
  {
    Assert.expect(landPatternPadName != null);
    _landPatternPadName = landPatternPadName;
  }

  /**
   * @return LibraryPackage
   * @author Cheah Lee Herng
   */
  public LibraryPackage getLibraryPackage()
  {
    Assert.expect(_libraryPackage != null);
    return _libraryPackage;
  }

  /**
   * @param libraryPackage LibraryPackage
   * @author Cheah Lee Herng
   */
  public void setLibraryPackage(LibraryPackage libraryPackage)
  {
    Assert.expect(libraryPackage != null);
    _libraryPackage = libraryPackage;
  }

  /**
   * @return LibraryLandPatternPad
   * @author Cheah Lee Herng
   */
  public LibraryLandPatternPad getLibraryLandPatternPad()
  {
    Assert.expect(_libraryLandPatternPad != null);
    return _libraryLandPatternPad;
  }

  /**
   * @param libraryLandPatternPad LibraryLandPatternPad
   * @author Cheah Lee Herng
   */
  public void setLibraryLandPatternPad(LibraryLandPatternPad libraryLandPatternPad)
  {
    Assert.expect(libraryLandPatternPad != null);
    _libraryLandPatternPad = libraryLandPatternPad;
  }
  
  /**  
   * @author Cheah Lee Herng
   */
  public java.awt.Shape getShapeInNanoMeters()
  {
    Assert.expect(_libraryLandPatternPad != null);

    return _libraryLandPatternPad.getShapeInNanoMeters();
  }

  /**
   * @return LibrarySubtype
   * @author Cheah Lee Herng
   */
  public LibrarySubtype getLibrarySubtype()
  {
    Assert.expect(_librarySubtype != null);
    return _librarySubtype;
  }

  /**
   * @param librarySubtype LibrarySubtype
   * @author Cheah Lee Herng
   */
  public void setLibrarySubtype(LibrarySubtype librarySubtype)
  {
    Assert.expect(librarySubtype != null);
    _librarySubtype = librarySubtype;
  }

  /**
   * @author Poh Kheng
   */
  public String getJointTypeName()
  {
    Assert.expect(_jointTypeName != null);
    return _jointTypeName;
  }

  /**
   * @author Poh Kheng
   */
  public void setJointTypeName(String jointTypeName)
  {
    Assert.expect(jointTypeName != null);
    _jointTypeName = jointTypeName;
  }

  /**
   *
   * @return PinOrientationAfterRotationEnum
   *
   * @author Tan Hock Zoon
   */
  public PinOrientationAfterRotationEnum getPinOrientationAfterRotationEnum()
  {
    PinOrientationEnum pinOrientationEnum = PinOrientationEnum.getPinOrientationEnum(getOrientation());
    double degrees = getLibraryLandPatternPad().getDegreesRotation();

    if ((JointTypeEnum.getJointTypeEnum(getJointTypeName()).equals(JointTypeEnum.COLLAPSABLE_BGA) ||
        (JointTypeEnum.getJointTypeEnum(getJointTypeName()).equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR)) ||
        (JointTypeEnum.getJointTypeEnum(getJointTypeName()).equals(JointTypeEnum.NON_COLLAPSABLE_BGA)) ||
        (JointTypeEnum.getJointTypeEnum(getJointTypeName()).equals(JointTypeEnum.CGA)) ||
        (JointTypeEnum.getJointTypeEnum(getJointTypeName()).equals(JointTypeEnum.PRESSFIT)) ||
        (JointTypeEnum.getJointTypeEnum(getJointTypeName()).equals(JointTypeEnum.CHIP_SCALE_PACKAGE)) ||
        (JointTypeEnum.getJointTypeEnum(getJointTypeName()).equals(JointTypeEnum.THROUGH_HOLE)) ||
        (JointTypeEnum.getJointTypeEnum(getJointTypeName()).equals(JointTypeEnum.OVAL_THROUGH_HOLE)) || //Siew Yeng - XCR-3318 - Oval PTH
        (JointTypeEnum.getJointTypeEnum(getJointTypeName()).equals(JointTypeEnum.SINGLE_PAD)) ||
        (JointTypeEnum.getJointTypeEnum(getJointTypeName()).equals(JointTypeEnum.LGA))))
      return PinOrientationAfterRotationEnum.NOT_APPLICABLE;
    else
      return PinOrientationAfterRotationEnum.getPinOrientationAfterRotationEnum(pinOrientationEnum, degrees);
  }

  /**
   * @return PinOrientationEnum
   *
   * @author Cheah Lee Herng
   */
  public PinOrientationEnum getPinOrientationEnum()
  {
    return PinOrientationEnum.getPinOrientationEnum(getOrientation());
  }

  /**
   * @param inspected boolean
   * @author Cheah Lee Herng
   */
  public void setInspected(boolean inspected)
  {
    Assert.expect(_inspected != null);
    _inspected.setValue(inspected);
  }

  /**
   * @return boolean
   * @author Cheah Lee Herng
   */
  public boolean isInspected()
  {
    Assert.expect(_inspected != null);
    return _inspected.getValue();
  }

  /**
   *
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String getName()
  {
    Assert.expect(_libraryLandPatternPad != null);

    return _libraryLandPatternPad.getPadName();
  }

}
