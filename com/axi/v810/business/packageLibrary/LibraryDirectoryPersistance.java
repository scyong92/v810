package com.axi.v810.business.packageLibrary;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Wei Chin, Chong
 */
public class LibraryDirectoryPersistance implements Serializable
{
  private String _packageLibraryDirectory = Directory.getPackageLibraryDir();

  /**
   * @author Wei Chin, Chong
   */
  public LibraryDirectoryPersistance()
  {
    // do nothing
  }

  /**
   * @author Wei Chin, Chong
   */
  public void clearFilter()
  {
    _packageLibraryDirectory = Directory.getPackageLibraryDir();
  }

  /**
   * @param packageLibraryDirectory String
   * @author Wei Chin, Chong
   */
  public void setPackageLibraryDirectory(String packageLibraryDirectory)
  {
    Assert.expect(packageLibraryDirectory != null);
    _packageLibraryDirectory = packageLibraryDirectory;
  }

  /**
   * @return String
   * @author Wei Chin, Chong
   */
  public String getPackageLibraryDirectory()
  {
    return _packageLibraryDirectory;
  }

  /**
   * @return LibraryDirectoryPersistance
   * @author Wei Chin, Chong
   */
  public LibraryDirectoryPersistance readSettings()
  {
    LibraryDirectoryPersistance persistance = null;
    try
    {
      if (FileUtilAxi.exists(FileName.getPackageLibrarySearchPersistFullPath()))
        persistance = (LibraryDirectoryPersistance) FileUtilAxi.loadObjectFromSerializedFile(FileName.getPackageLibraryDirectoryFullPath());
      else
        persistance = new LibraryDirectoryPersistance();
    }
    catch(DatastoreException de)
    {
      persistance = new LibraryDirectoryPersistance();
    }
    return persistance;
  }

  /**
   * @throws DatastoreException
   * @author Wei Chin, Chong
   */
  public void writeSettings() throws DatastoreException
  {
    FileUtilAxi.saveObjectToSerializedFile(this, FileName.getPackageLibraryDirectoryFullPath());
  }
}
