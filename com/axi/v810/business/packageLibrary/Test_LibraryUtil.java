package com.axi.v810.business.packageLibrary;

import java.io.*;

import com.axi.v810.datastore.*;
import com.axi.util.*;
import com.axi.v810.datastore.database.*;
import com.axi.v810.datastore.config.Config;
import com.axi.v810.business.panelSettings.Project;
import java.util.List;
import java.util.ArrayList;
import com.axi.v810.util.XrayTesterException;
import com.axi.v810.datastore.config.SoftwareConfigEnum;
import com.axi.v810.business.panelDesc.CompPackage;
import java.util.Map;

/**
 * @author Wei Chin, Chong
 * @author Poh Kheng
 * @version 1.0
 */
public class Test_LibraryUtil extends UnitTest
{
  private Config _config = Config.getInstance();


  /**
   * @param args String[]
   *
   * @author Wei Chin, Chong
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_LibraryUtil());
  }

  /**
   * @param is BufferedReader
   * @param os PrintWriter
   * @author Wei Chin, Chong
   * @author Poh Kheng
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      String projectName = "0027parts";

      _config.loadIfNecessary();
      // this will get the type of database
      String databaseType = _config.getStringValue(SoftwareConfigEnum.DATABASE);
      String databasePath = getTestDataDir() + databaseType;

      String filePath = databasePath + "/TestDatabase";
      String filePath2 = databasePath + "/TestDatabase2";

      String localDatabasePath = filePath + "/abc";
      String localDatabasePath2 = filePath2 + "/abc";

      // test for Function Create New Library and Function Check Library
      databaseCreateTest(localDatabasePath);
      // test on another database to make sure we can create database on multiple path
      databaseCreateTest(localDatabasePath2);

      Project project = loadProject(projectName, false);
      List<SubtypeScheme> subtypeSchemes = loadSubtypeSchemes(project, localDatabasePath);

      // test export to database
      LibraryUtil.getInstance().exportToLibrary(subtypeSchemes);

      for(SubtypeScheme subtypeScheme : subtypeSchemes)
      {
        subtypeScheme.getCompPackage().setLibraryPath(subtypeScheme.getLibraryPath());
        subtypeScheme.getCompPackage().saveExportState();
      }

      LibraryUtil.getInstance().updateToLibrary(subtypeSchemes);

      // test import from database
      List<String> libraryPaths = new ArrayList<String>();
      libraryPaths.add(localDatabasePath);
      libraryPaths.add(localDatabasePath2);
      SearchFilterPersistance searchFilter = new SearchFilterPersistance();
      searchFilter.setLandPatternGeometryMatchPercentage(0);
      searchFilter.setLandPatternNameFilter("%", SearchOptionEnum.CONTAINS);
      ImportLibrarySettingPersistance importLibrarySettingPersistance = new ImportLibrarySettingPersistance();
      importLibrarySettingPersistance.setImportLibrarySettingStatus(ImportLibrarySettingEnum.IMPORT_LANDPATTERN, true);
      importLibrarySettingPersistance.setImportLibrarySettingStatus(ImportLibrarySettingEnum.IMPORT_LIBRARY_SUBTYPE_NAME, true);
      importLibrarySettingPersistance.setImportLibrarySettingStatus(ImportLibrarySettingEnum.IMPORT_NOMINAL, true);
      importLibrarySettingPersistance.setImportLibrarySettingStatus(ImportLibrarySettingEnum.IMPORT_THRESHOLD, true);

      LibraryUtil.getInstance().getRefineMatchLibrarySubtypeSchemeBySubtypeSchemes(subtypeSchemes, libraryPaths, searchFilter, importLibrarySettingPersistance);

      for (SubtypeScheme subtypeScheme : subtypeSchemes)
      {
        System.out.println(" --------------------------------------------- ");
        System.out.println("subtypeScheme.getCompPackage().getLongName(): " + subtypeScheme.getCompPackage().getLongName());
        System.out.println("subtypeScheme.getCheckSum(): " + subtypeScheme.getCheckSum());


        for (Map.Entry<String, List<LibraryPackage>> entry : subtypeScheme.getLibraryPathToLibrarySubtypeSchemesMap().entrySet())
        {
          String libraryPath = entry.getKey();
          //System.out.println("--libraryPath: " + libraryPath);

          for (LibraryPackage libraryPackage : entry.getValue())
          {
            System.out.println("----libraryPackage.getLandPatternID(): " + libraryPackage.getLandPatternId());
            System.out.println("----libraryPackage.getPackageName(): " + libraryPackage.getPackageName());
            System.out.println("----libraryPackage.getPackageLongName(): " + libraryPackage.getPackageLongName());
            System.out.println("----libraryPackage.getLandPatternId(): " + libraryPackage.getLandPattern().getLandPatternId());
            DatabaseManager.getInstance().getLibraryLandPatternPads(libraryPath, libraryPackage.getLandPattern());

//            for (LibrarySubtypeScheme librarySubtypeScheme : libraryPackage.getLibrarySubtypeSchemes())
//            {
//              System.out.println("------librarySubtypeScheme.getSubtypeSchemeID(): " + librarySubtypeScheme.getLibrarySubtypeSchemeId());
//              System.out.println("------librarySubtypeScheme.getMatchPercentage(): " + librarySubtypeScheme.getMatchPercentage());
//
//              for (LibrarySubtype librarySubtype : librarySubtypeScheme.getLibrarySubtypes())
//              {
//                System.out.println("--------librarySubtype.getSubtypeID(): " + librarySubtype.getSubtypeId());
//                System.out.println("--------librarySubtype.getSubtypeName(): " + librarySubtype.getSubtypeName());
//                System.out.println("--------librarySubtype.getJointTypeName(): " + librarySubtype.getJointTypeName());
//              }
//            }
          }
        }
      }

      // delete the database after test
      try
      {
        AbstractDatabase.getInstance().shutdownDatabase();
      }
      catch (DatastoreException ex)
      {
        Expect.expect(false);
        ex.printStackTrace();
      }

      try
      {
        FileUtil.delete(filePath);
      }
      catch (CouldNotDeleteFileException cndEx)
      {
        Expect.expect(false);
        cndEx.printStackTrace();
      }

      try
      {
        FileUtil.delete(filePath2);
      }
      catch (CouldNotDeleteFileException cndEx)
      {
        Expect.expect(false);
        cndEx.printStackTrace();
      }

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      os.flush();
    }
  }

  /**
   * @param localDatabasePath String
   * @author Wei Chin, Chong
   */
  public void databaseCreateTest(String localDatabasePath)
  {
    boolean libraryExist = true;
    try
    {
      libraryExist = LibraryUtil.getInstance().doeslibraryExist(localDatabasePath);
    }
    catch (DatastoreException ex)
    {
      Expect.expect(false);
      ex.printStackTrace();
    }

    Expect.expect(libraryExist == false);

    try
    {
      LibraryUtil.getInstance().createNewLibrary(localDatabasePath);
    }
    catch (DatastoreException ex)
    {
      Expect.expect(false);
      ex.printStackTrace();
    }

    try
    {
      libraryExist = LibraryUtil.getInstance().doeslibraryExist(localDatabasePath);
    }
    catch (DatastoreException ex)
    {
      Expect.expect(false);
      ex.printStackTrace();
    }
    Expect.expect(libraryExist);
  }

  /**
   * @author Poh Kheng
   */
  private static Project loadProject(String projectName, boolean alwaysImport)
  {
    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    Project project = null;
    try
    {
      if(alwaysImport || Project.doesProjectExistLocally(projectName) == false)
      {
        project = Project.importProjectFromNdfs(projectName, warnings);
      }
      else
        project = Project.load(projectName, new BooleanRef());
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
      Assert.expect(false);
    }

    return project;
  }

  /**
   * @author Poh Kheng
   */
  private static List<SubtypeScheme> loadSubtypeSchemes(Project project, String libraryPath)
  {
    List<SubtypeScheme> subtypeSchemes = new ArrayList<SubtypeScheme>();
    for (CompPackage compPackage : project.getPanel().getCompPackages())
    {
      for (SubtypeScheme subtypeScheme : compPackage.getSubtypeSchemes())
      {
        subtypeScheme.setLibraryPath(libraryPath);
        subtypeSchemes.add(subtypeScheme);
      }

    }
    return subtypeSchemes;
  }
}
