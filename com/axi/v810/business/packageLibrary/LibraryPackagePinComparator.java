package com.axi.v810.business.packageLibrary;

import java.util.*;

import com.axi.util.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class LibraryPackagePinComparator implements Comparator<LibraryPackagePin>
{
  private static AlphaNumericComparator _alphaNumericComparator = new AlphaNumericComparator();
  private boolean _ascending;

  /**
   * @param ascending boolean
   * @author Wei Chin, Chong
   */
  public LibraryPackagePinComparator(boolean ascending)
  {
    _ascending = ascending;
  }

  /**
   * @param lhLibraryPackagePin LibraryPackagePin
   * @param rhLibraryPackagePin LibraryPackagePin
   * @return int
   * @author Wei Chin, Chong
   */
  public int compare(LibraryPackagePin lhLibraryPackagePin, LibraryPackagePin rhLibraryPackagePin)
  {
    Assert.expect(lhLibraryPackagePin != null);
    Assert.expect(rhLibraryPackagePin != null);

    String lhName = lhLibraryPackagePin.getLandPatternPadName();
    String rhName = rhLibraryPackagePin.getLandPatternPadName();

    if (_ascending)
      return _alphaNumericComparator.compare(lhName, rhName);
    else
      return _alphaNumericComparator.compare(rhName, lhName);
  }
}
