package com.axi.v810.business.packageLibrary;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class SubtypeSchemeObservable extends Observable
{
  private static SubtypeSchemeObservable _instance = null;
  private int _numDisables = 0;

  /**
   * @author Wei Chin, Chong
   */
  private SubtypeSchemeObservable()
  {
    //do nothing
  }

  /**
   * @return SubtypeSchemeObservable
   * @author Wei Chin, CHong
   */
  public static synchronized SubtypeSchemeObservable getInstance()
  {
    if (_instance == null)
    {
      _instance = new SubtypeSchemeObservable();
    }
    return _instance;
  }
  /**
   * @return boolean
   *
   * @author Wei Chin, Chong
   */
  public boolean isEnabled()
  {
    if (_numDisables == 0)
      return true;

    return false;
  }

  /**
   * Set enable to true to cause subtype scheme events to trigger update() calls.
   * When enable is set to false no updates will be made.  Note that you must
   * pair each setEnabled(false) with a setEnabled(true) because this method
   * keeps track of nested calls in order to work properly.
   *
   * @param enable boolean
   *
   * @author Wei Chin, Chong
   */
  public void setEnabled(boolean enable)
  {
    if (enable)
      --_numDisables;
    else
      ++_numDisables;
  }

  /**
   * Subtype schemes object should call this method when ever it does something that changes the
   * state of the data.  Any Observers of this class will be notified that the Subtype schemes state
   * has changed.
   *
   * @param source Object
   * @param oldValue Object
   * @param newValue Object
   * @param projectChangeEventEnum ProjectChangeEventEnum
   *
   * @author Wei Chin, Chong
   */
  public void stateChanged(Object source, Object oldValue, Object newValue, ProjectChangeEventEnum projectChangeEventEnum)
  {
    Assert.expect(source != null);
    // oldValue can be null
    // newValue can be null
    Assert.expect(projectChangeEventEnum != null);

    Assert.expect(_numDisables >= 0);

    ProjectChangeEvent projChangeEvent = new ProjectChangeEvent(source, oldValue, newValue, projectChangeEventEnum);

    if (isEnabled())
    {
      // send up only the initiating state change call to observers
      setChanged();
      notifyObservers(projChangeEvent);
    }
  }
}
