package com.axi.v810.business.packageLibrary;


import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This is a filter for Match Criteria for Package Library
 * @author Poh Kheng
 */
public class SearchFilterPersistance implements Serializable
{

  // search filters
  private String _landPatternNameFilter = null;
  private boolean _jointTypeFilterOn = true;
  private boolean _landPatternNameFilterOn = false;
  private int _landPatternGeometryMatchPercentage = 80;
  private boolean _includesNonMatches = true;
  private SearchOptionEnum _searchOptionEnum = null;

  /**
   * @author Poh Kheng
   */
  public SearchFilterPersistance()
  {
    // do nothing
  }

  /**
   * @author Poh Kheng
   */
  public void clearFilter()
  {
    _landPatternNameFilter = null;
    _jointTypeFilterOn = false;
    _landPatternGeometryMatchPercentage = -1;
    _includesNonMatches = true;
  }


  /**
   * @author Poh Kheng
   */
  public void setJointTypeFilter(boolean jointTypeFilterOn)
  {
    _jointTypeFilterOn = jointTypeFilterOn;
  }

  /**
   * @author Poh Kheng
   */
  public boolean isJointTypeFilterOn()
  {
    return _jointTypeFilterOn;
  }

  /**
   * @author Poh Kheng
   */
  public void setLandPatternNameFilter(String landPatternNameFilter, SearchOptionEnum searchOptionEnum)
  {
    Assert.expect(landPatternNameFilter != null);
    Assert.expect(searchOptionEnum != null);

    _landPatternNameFilter = landPatternNameFilter;
    _searchOptionEnum = searchOptionEnum;
  }

  /**
   * @author Poh Kheng
   */
  public String getLandPatternNameFilter()
  {
    Assert.expect(_landPatternNameFilter != null);

    String landPatternNameWithFilter = null;

    if (_searchOptionEnum.equals(SearchOptionEnum.START_WITH)) {
      landPatternNameWithFilter = _landPatternNameFilter + "%";
    }
    else if (_searchOptionEnum.equals(SearchOptionEnum.END_WITH))
    {
      landPatternNameWithFilter =  "%" + _landPatternNameFilter;
    }
    else if (_searchOptionEnum.equals(SearchOptionEnum.CONTAINS))
    {
      landPatternNameWithFilter = "%" + _landPatternNameFilter + "%";
    }
    else
    {
      Assert.expect(false, "Unknown _searchOptionEnum type: "+_searchOptionEnum.getName());
    }

    return landPatternNameWithFilter;
  }

  /**
   * @author Poh Kheng
   */
  public String getLandPatternName()
  {
    Assert.expect(_landPatternNameFilter != null);
    return _landPatternNameFilter;
  }

  /**
   *
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public SearchOptionEnum getLandPatternNameSearchBy()
  {
    Assert.expect(_searchOptionEnum != null);
    return _searchOptionEnum;
  }

  /**
   * @author Poh Kheng
   */
  public void setLandPatternNameFilter(boolean landPatternNameFilterOn)
  {
    _landPatternNameFilterOn = landPatternNameFilterOn;
  }

  /**
   * @author Poh Kheng
   */
  public boolean isLandPatternNameFilterOn()
  {
    return _landPatternNameFilterOn;
  }

  /**
   * @author Poh Kheng
   */
  public void setLandPatternGeometryMatchPercentage(int landPatternGeometryMatchPercentage)
  {
    Assert.expect(landPatternGeometryMatchPercentage >= 0);
    Assert.expect(landPatternGeometryMatchPercentage <= 100);
    _landPatternGeometryMatchPercentage = landPatternGeometryMatchPercentage;
  }

  /**
   * @author Poh Kheng
   */
  public int getLandPatternGeometryMatchPercentage()
  {
    Assert.expect(_landPatternGeometryMatchPercentage >= 0);
    Assert.expect(_landPatternGeometryMatchPercentage <= 100);
    return _landPatternGeometryMatchPercentage;
  }

  /**
   * @author Poh Kheng
   */
  public boolean isLandPatternGeometryMatchPercentageFilterOn()
  {
    return (_landPatternGeometryMatchPercentage > 0);
  }

  /**
   * @author Poh Kheng
   */
  public void setIncludesNonMatches(boolean includesNonMatches)
  {
    _includesNonMatches = includesNonMatches;
  }

  /**
   * @author Poh Kheng
   */
  public boolean includesNonMatches()
  {
    return _includesNonMatches;
  }

  /**
   *
   * @return SearchFilterPersistance
   *
   * @author Tan Hock Zoon
   */
  public SearchFilterPersistance readSettings()
  {
    SearchFilterPersistance persistance = null;
    try
    {
      if (FileUtilAxi.exists(FileName.getPackageLibrarySearchPersistFullPath()))
        persistance = (SearchFilterPersistance) FileUtilAxi.loadObjectFromSerializedFile(FileName.getPackageLibrarySearchPersistFullPath());
      else
        persistance = new SearchFilterPersistance();
    }
    catch(DatastoreException de)
    {
      persistance = new SearchFilterPersistance();
    }

    return persistance;
  }
  /**
   *
   * @throws DatastoreException
   *
   * @author Tan Hock Zoon
   */
  public void writeSettings() throws DatastoreException
  {
    FileUtilAxi.saveObjectToSerializedFile(this, FileName.getPackageLibrarySearchPersistFullPath());
  }
}
