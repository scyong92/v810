package com.axi.v810.business.packageLibrary;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.database.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class Test_LibraryManager extends UnitTest
{
  private Config _config = Config.getInstance();
  /**
   * @param args String[]
   * @author Wei Chin, Chong
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_LibraryManager());
  }

  /**
   * @param is BufferedReader
   * @param os PrintWriter
   * @author Wei Chin, Chong
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      _config.loadIfNecessary();
      // this will get the type of database
      String databaseType = _config.getStringValue(SoftwareConfigEnum.DATABASE);
      String databasePath = getTestDataDir() + databaseType;
      String filePath = databasePath + "/TestDatabase3";

      String validLocalDatabasePath = filePath + "/abc";
      String invalidLibraryPath = "C:\\Test\\ABC";

      // create Library
      try
      {
        LibraryUtil.getInstance().createNewLibrary(validLocalDatabasePath);
      }
      catch (DatastoreException ex)
      {
        Expect.expect(false);
        ex.printStackTrace();
      }

      List<String> oldLibraries = null;
      List<String> newLibraries = null;
      LibraryManager libraryManger = LibraryManager.getInstance();

      // load library manager
      try
      {
        libraryManger.load();
      }
      catch (DatastoreException ex)
      {
        Expect.expect(false);
        ex.printStackTrace();
      }

      // get a list of libraries from library manager
      oldLibraries = libraryManger.getLibraries();

      Expect.expect(libraryManger.getLibraries().contains(invalidLibraryPath) == false);
      Expect.expect(libraryManger.getLibraries().contains(validLocalDatabasePath) == false);

      // add new path into library manager
      try
      {
        libraryManger.add(invalidLibraryPath);
        libraryManger.add(validLocalDatabasePath);
      }
      catch (DatastoreException ex)
      {
        Expect.expect(false);
        ex.printStackTrace();
      }

      // validate the new library path in library manager
      Expect.expect(libraryManger.getLibraries().contains(invalidLibraryPath) == false);
      Expect.expect(libraryManger.getLibraries().contains(validLocalDatabasePath));

      // remove the library add in library manager
      try
      {
        libraryManger.remove(validLocalDatabasePath);
      }
      catch (DatastoreException ex)
      {
        Expect.expect(false);
        ex.printStackTrace();
      }

      // validate again after remove
      Expect.expect(libraryManger.getLibraries().contains(validLocalDatabasePath) == false);

      // delete the database after test
      try
      {
        AbstractDatabase.getInstance().shutdownDatabase();
      }
      catch (DatastoreException ex)
      {
        Expect.expect(false);
        ex.printStackTrace();
      }

      try
      {
        FileUtil.delete(filePath);
      }
      catch (CouldNotDeleteFileException cndEx)
      {
        Expect.expect(false);
        cndEx.printStackTrace();
      }

      // save the libraries to file
      try
      {
        libraryManger.save();
      }
      catch (DatastoreException ex)
      {
        Expect.expect(false);
        ex.printStackTrace();
      }

      // load back the libraries from file
      try
      {
        libraryManger.load();
      }
      catch (DatastoreException ex)
      {
        Expect.expect(false);
        ex.printStackTrace();
      }

      // validate against new libraries list and previous libraries list.
      newLibraries = libraryManger.getLibraries();
      Expect.expect(newLibraries.containsAll(oldLibraries));
    }
    catch (DatastoreException ex)
    {
      ex.printStackTrace();
    }
  }
}
