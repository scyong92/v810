package com.axi.v810.business.packageLibrary;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class SubtypeSchemeEventEnum extends com.axi.v810.business.ProjectChangeEventEnum
{
  private static int _index = -1;

  public static SubtypeSchemeEventEnum SUBTYPE_NAME = new SubtypeSchemeEventEnum(++_index);
  public static SubtypeSchemeEventEnum ASSIGNED_LIBRARY_SUBTYPE_SCHEME = new SubtypeSchemeEventEnum(++_index);

  /**
   * @param id int
   *
   * @author Wei Chin, Chong
   */
  private SubtypeSchemeEventEnum(int id)
  {
    super(id);
  }
}
