package com.axi.v810.business.packageLibrary;

import java.awt.geom.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.database.*;
import com.axi.v810.util.*;
import com.axi.v810.business.ProjectObservable;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class LibraryUtil
{
  private static final char SLASH = '/';
  private static final String UNDERSCORE = "_";
  //Siew Yeng - XCR-3318
  private static final String COMMA = ",";
  private static final String SEMICOLON = ";";
  private final double MAX_PERCENT = 100.0;
  private static Map<String, Subtype> _subtypeSchemeCheckSumToSubtypeMap = new HashMap<String,Subtype>();
  private static Map<String, LandPatternPad> _subtypeSchemeCheckSumToLandPatternPadMap = new HashMap<String,LandPatternPad>();
  private static Map<String, PackagePin> _subtypeSchemeCheckSumToPackagePinMap = new HashMap<String,PackagePin>();
  private static List<Subtype> _newlyAddedSubtypes = new ArrayList<Subtype>();
  private static DatabaseProgressObservable _databaseProgressObservable = DatabaseProgressObservable.getInstance();
  private static boolean _cancelDatabaseOperation = false;
  private static boolean _startAssignNewSubtypeToPadType = false;
  private static LibraryUtil _instance;
  private ProjectObservable _projectObservable = ProjectObservable.getInstance();
  private static int imageDebugCounter;

  /**
   * @author Cheah Lee Herng
   */
  private LibraryUtil()
  {
    // Do nothing
  }

  /**
   * @return LibraryUtil
   *
   * @author Cheah Lee Herng
   */
  public static synchronized LibraryUtil getInstance()
  {
    if (_instance == null)
      _instance = new LibraryUtil();

    return _instance;
  }

  /**
   * create a new library
   *
   * @param libraryFullPath String
   * @author Wei Chin, Chong
   */
  public void createNewLibrary(String libraryFullPath) throws DatastoreException
  {
    Assert.expect(libraryFullPath != null);

    String databaseLocation = null;
    String databaseName = null;
    boolean proceedToCreateDatabase = false;

  // Get database name and database location
    Pair<String, String> databasePathInfoPair = RelationalDatabaseUtil.getDatabasePathInfo(libraryFullPath);
    databaseName = databasePathInfoPair.getFirst();
    databaseLocation = databasePathInfoPair.getSecond();
    /** @todo gad database name and location shouldn't be null */
    if (databaseName != null && databaseLocation != null)
      proceedToCreateDatabase = true;

    if (proceedToCreateDatabase)
    {
      DatabaseManager.getInstance().createDatabase(libraryFullPath);
    }
  }

  /**
   * @param libraryFullPath String
   * @return String
   * @author Wei Chin, Chong
   */
  public String convertToDatabasePath(String libraryFullPath)
  {
    Assert.expect(libraryFullPath != null);
    return libraryFullPath.replace(File.separatorChar, SLASH);
  }

  /**
   * @param libraryFullPath String
   * @return String
   * @author Wei Chin, Chong
   */
  public String convertToOriginalPath(String libraryFullPath)
  {
    Assert.expect(libraryFullPath != null);
    return libraryFullPath.replace(SLASH, File.separatorChar);
  }

  /**
   * @param libraryFullPath String
   * @author Wei Chin, Chong
   */
  public boolean doeslibraryExist(String libraryFullPath) throws DatastoreException
  {
    Assert.expect(libraryFullPath != null);

    boolean doeslibraryExist = false;

    doeslibraryExist = DatabaseManager.getInstance().doesDatabaseExists(libraryFullPath);

    return doeslibraryExist;
  }

  /**
   * export a list of subtype scheme to the library
   *
   * @param subtypeSchemes List
   * @author Wei Chin, Chong
   */
  public void exportToLibrary(List<SubtypeScheme> subtypeSchemes) throws DatastoreException
  {
    Map<String, List<SubtypeScheme>> databasePathToSubtypeSchemesMap = groupLibrarySubtypeSchemesByLibraryPath(subtypeSchemes);
    DatabaseManager.getInstance().exportSubtypeScheme(databasePathToSubtypeSchemesMap);
  }

  /**
   * update a list of subtype scheme to the library
   *
   * @param subtypeSchemes List
   * @author Wei Chin, Chong
   */
  public void updateToLibrary(List<SubtypeScheme> subtypeSchemes) throws DatastoreException
  {
    Map<String, List<SubtypeScheme>> databasePathToSubtypeSchemesMap = groupLibrarySubtypeSchemesByLibraryPath(subtypeSchemes);
    DatabaseManager.getInstance().updateSubtypeScheme(databasePathToSubtypeSchemesMap);
  }

  /**
   * @param subtype Subtype
   * @param libraryPath String
   * @return LibrarySubtype
   *
   * @author Wei Chin, Chong
   */
  public LibrarySubtype getMatchLibrarySubtype(Subtype subtype, String libraryPath) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(libraryPath != null);

    LibrarySubtype librarySubtype = null;

    librarySubtype = DatabaseManager.getInstance().getLibrarySubtype(subtype,libraryPath);

    Assert.expect(librarySubtype != null);

    return librarySubtype;
  }

  /**
   * @param subtypeSchemes List
   * @return Map
   * @author Wei Chin, Chong
   */
  public Map isMatchedLibrarySubtypeSchemeExist(List<SubtypeScheme> subtypeSchemes) throws DatastoreException
  {
    Assert.expect(subtypeSchemes != null);

    Map<SubtypeScheme, Boolean> subtypeSchemeToExistability = null;
    Map<String, List<SubtypeScheme>> databasePathToSubtypeSchemesMap = null;

    databasePathToSubtypeSchemesMap = groupLibrarySubtypeSchemesByLibraryPath(subtypeSchemes);
    subtypeSchemeToExistability = DatabaseManager.getInstance().doesMatchedLibrarySubtypeSchemeExist(databasePathToSubtypeSchemesMap);
    return subtypeSchemeToExistability;
  }

  /**
   *
   * @param subtypeSchemes List
   * @param libraryPath List
   * @param searchFilter SearchFilter
   *
   * @author Cheah Lee Herng
   */
  public void getRefineMatchLibrarySubtypeSchemeBySubtypeSchemes(List<SubtypeScheme> subtypeSchemes,
                                                                 List<String> libraryPath,
                                                                 SearchFilterPersistance searchFilter,
                                                                 ImportLibrarySettingPersistance importLibrarySettingPersistance)
  {
    Assert.expect(subtypeSchemes != null);
    Assert.expect(libraryPath != null);
    Assert.expect(searchFilter != null);
    Assert.expect(importLibrarySettingPersistance != null);
    try
    {
      DatabaseManager.getInstance().getRefineMatchLibrarySubtypeSchemeBySubtypeSchemes(subtypeSchemes, libraryPath, searchFilter, importLibrarySettingPersistance);
    }
    catch(DatastoreException dex)
    {
      /** @todo gad why assert a DatastoreExeption */
      Assert.logException(dex);
    }
  }

  /**
   *
   * @param subtypeSchemes List
   * @return Map
   *
   * @author Cheah Lee Herng
   */
  public Map<String, List<SubtypeScheme>> groupLibrarySubtypeSchemesByLibraryPath(List<SubtypeScheme> subtypeSchemes)
  {
    Map<String, List<SubtypeScheme>> databasePathToSubtypeSchemesMap = new HashMap<String,List<SubtypeScheme>>();
    List<SubtypeScheme> groupSubtypeSchemes = null;

    // Group SubtypeScheme objects of same database source into respective list
    for(SubtypeScheme subtypeScheme : subtypeSchemes)
    {
      if (databasePathToSubtypeSchemesMap.containsKey(subtypeScheme.getLibraryPath()) == false)
      {
        List<SubtypeScheme> newSubtypeSchemes = new ArrayList<SubtypeScheme>();
        newSubtypeSchemes.add(subtypeScheme);
        databasePathToSubtypeSchemesMap.put(subtypeScheme.getLibraryPath(), newSubtypeSchemes);
      }
      else
      {
        groupSubtypeSchemes = databasePathToSubtypeSchemesMap.get(subtypeScheme.getLibraryPath());
        groupSubtypeSchemes.add(subtypeScheme);
        databasePathToSubtypeSchemesMap.remove(subtypeScheme.getLibraryPath());
        databasePathToSubtypeSchemesMap.put(subtypeScheme.getLibraryPath(), groupSubtypeSchemes);
      }
    }

    return databasePathToSubtypeSchemesMap;
  }
  /**
   *
   * @author Poh Kheng
   */
  public int convertBooleanToInt(boolean status)
  {
      if(status)
        return 1;
      else
        return 0;
  }

  /**
   *
   * @author Poh Kheng
   */
  public boolean convertIntToBoolean(int status)
  {
      if (status == 1)
        return true;
      else if (status == 0)
        return false;
      else
        Assert.expect(false, "Not valid status number");

      return false;
  }

  /**
   * @author Poh Kheng
   */
  public LibraryLandPattern getLibraryLandPatternDetails(String libraryPath, LibraryLandPattern libraryLandPattern)
  {
    Assert.expect(libraryLandPattern != null);
    Assert.expect(libraryPath != null);
    try
    {
      libraryLandPattern = DatabaseManager.getInstance().getLibraryLandPatternPads(libraryPath, libraryLandPattern);
    }
    catch (DatastoreException dex)
    {
      Assert.logException(dex);
    }
    return libraryLandPattern;
  }

  /**
   * @author Poh Kheng
   */
  public void getLibraryPackagePinDetails(String libraryPath, LibraryPackage libraryPackage)
  {
    Assert.expect(libraryPath != null);
    Assert.expect(libraryPackage != null);
    try
    {
      DatabaseManager.getInstance().getLibraryPackagePins(libraryPath, libraryPackage);
    }
    catch (DatastoreException dex)
    {
      Assert.logException(dex);
    }
  }

  /**
   * @param projectSubtypeSchemes List
   * @author Cheah Lee Herng
   */
  public void rollbackImportSubtypeScheme(Project project, List<SubtypeScheme> projectSubtypeSchemes)
  {
    Assert.expect(project != null);
    Assert.expect(projectSubtypeSchemes != null);
    Assert.expect(_subtypeSchemeCheckSumToSubtypeMap != null);
    Assert.expect(_subtypeSchemeCheckSumToLandPatternPadMap != null);
    Assert.expect(_subtypeSchemeCheckSumToPackagePinMap != null);

    _projectObservable.setEnabled(false);
    try
    {
      // Remove previously assign subtype
      if (isStartAssigningNewSubtype())
        removeSubtypeFromPadType(projectSubtypeSchemes);

      for(SubtypeScheme subtypeScheme : projectSubtypeSchemes)
      {
        // Rollback compPackage values
        subtypeScheme.getCompPackage().rollback();

        List<ComponentType> componentTypes = subtypeScheme.getComponentTypes();
        for (ComponentType componentType : componentTypes)
        {
          List<PadType> padTypes = componentType.getPadTypes();
          for (PadType padType : padTypes)
          {
            // Generate subtype scheme checksum
            String subtypeSchemeChecksum = generatePadTypeChecksum(project.getName(),
                                                                   componentType.getCompPackage().getLongName(),
                                                                   componentType.getReferenceDesignator(),
                                                                   padType.getName());

            // Generate land pattern pad checksum
            String landPatternPadChecksum = generateLandPatternPadChecksum(project.getName(),
                                                                           componentType.getCompPackage().getLongName(),
                                                                           componentType.getReferenceDesignator(),
                                                                           padType.getName(),
                                                                           padType.getLandPatternPad().getName());
            // Generate package pin checksum
            String packagePinChecksum = generatePackagePinChecksum(project.getName(),
                                                                   componentType.getCompPackage().getLongName(),
                                                                   componentType.getReferenceDesignator(),
                                                                   padType.getName(),
                                                                   padType.getPackagePin().getName());

            // Assign back original land pattern pad
            if (_subtypeSchemeCheckSumToLandPatternPadMap.containsKey(landPatternPadChecksum))
            {
              padType.setLandPatternPad(_subtypeSchemeCheckSumToLandPatternPadMap.get(landPatternPadChecksum));
            }

            // Assign back original package pin
            if (_subtypeSchemeCheckSumToPackagePinMap.containsKey(packagePinChecksum))
            {
              padType.setPackagePin(_subtypeSchemeCheckSumToPackagePinMap.get(packagePinChecksum));
            }

            // Assign back original subtype
            if (_subtypeSchemeCheckSumToSubtypeMap.containsKey(subtypeSchemeChecksum))
            {
              Subtype subtype = _subtypeSchemeCheckSumToSubtypeMap.get(subtypeSchemeChecksum);
              subtype.assignPadsFromBackupPads();
              padType.getPadTypeSettings().setSubtype(_subtypeSchemeCheckSumToSubtypeMap.get(subtypeSchemeChecksum));
            }
          }
        }
      }

      removeUnusedSubtype(project.getPanel(), _newlyAddedSubtypes);
    }
    finally
    {
      clearMap();
      _newlyAddedSubtypes.clear();
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, ProjectEventEnum.LIBRARY_IMPORT_COMPLETE);
  }

  /**
   * @param project Project
   * @param projectSubtypeSchemes List
   *
   * @author Cheah Lee Herng
   */
  public void backup(Project project, List<SubtypeScheme> projectSubtypeSchemes)
  {
    Assert.expect(project != null);
    Assert.expect(projectSubtypeSchemes != null);
    Assert.expect(_subtypeSchemeCheckSumToSubtypeMap != null);

    for(SubtypeScheme subtypeScheme : projectSubtypeSchemes)
    {
      if (subtypeScheme.getCompPackage().isBackup() == false)
      {
        // Backup compPackage values
        subtypeScheme.getCompPackage().backup();
        subtypeScheme.getCompPackage().saveSubtypeThresholdsCheckSum();
      }
      List<ComponentType> componentTypes = subtypeScheme.getComponentTypes();
      for(ComponentType componentType : componentTypes)
      {
        List<PadType> padTypes = componentType.getPadTypes();
        for (PadType padType : padTypes)
        {
          if (cancelDatabaseOperation())
            break;

          Subtype subtype = padType.getPadTypeSettings().getSubtype();
          LandPatternPad landPatternPad = padType.getLandPatternPad();
          PackagePin packagePin = padType.getPackagePin();

          // Generate pad type checksum
          String subtypeSchemeChecksum = generatePadTypeChecksum(project.getName(),
                                                                 componentType.getCompPackage().getLongName(),
                                                                 componentType.getReferenceDesignator(),
                                                                 padType.getName());

          // Generate land pattern pad checksum
          String landPatternPadChecksum = generateLandPatternPadChecksum(project.getName(),
                                                                         componentType.getCompPackage().getLongName(),
                                                                         componentType.getReferenceDesignator(),
                                                                         padType.getName(),
                                                                         padType.getLandPatternPad().getName());

          // Generate package pin checksum
          String packagePinChecksum = generatePackagePinChecksum(project.getName(),
                                                                 componentType.getCompPackage().getLongName(),
                                                                 componentType.getReferenceDesignator(),
                                                                 padType.getName(),
                                                                 padType.getPackagePin().getName());

          // Backup pad type set
          subtype.setBackupPads(subtype.getPads());

          // Add entry into map
          Subtype prevSubtype = _subtypeSchemeCheckSumToSubtypeMap.put(subtypeSchemeChecksum, subtype);
          Assert.expect(prevSubtype == null);
          LandPatternPad prevLandPatternPad = _subtypeSchemeCheckSumToLandPatternPadMap.put(landPatternPadChecksum, landPatternPad);
          Assert.expect(prevLandPatternPad == null);
          PackagePin prevPackagePin = _subtypeSchemeCheckSumToPackagePinMap.put(packagePinChecksum, packagePin);
          Assert.expect(prevPackagePin == null);

          _databaseProgressObservable.incrementPercentDone();
        }
      }
    }
  }

  /**
   * @param projectName String
   * @param componentPackageLongName String
   * @param componentTypeRefDesignator String
   * @param padTypeName String
   * @return String
   * @author Cheah Lee Herng
   */
  private String generatePadTypeChecksum(String projectName,
                                         String componentPackageLongName,
                                         String componentTypeRefDesignator,
                                         String padTypeName)
  {
    Assert.expect(projectName != null);
    Assert.expect(componentPackageLongName != null);
    Assert.expect(componentTypeRefDesignator != null);
    Assert.expect(padTypeName != null);

    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(projectName);
    stringBuilder.append(UNDERSCORE);
    stringBuilder.append(componentPackageLongName);
    stringBuilder.append(UNDERSCORE);
    stringBuilder.append(componentTypeRefDesignator);
    stringBuilder.append(UNDERSCORE);
    stringBuilder.append(padTypeName);

    return stringBuilder.toString().intern();
  }

  /**
   * @param projectName String
   * @param componentPackageLongName String
   * @param componentTypeRefDesignator String
   * @param padTypeName String
   * @param landPatternPadName String
   * @return String
   * @author Cheah Lee Herng
   */
  private String generateLandPatternPadChecksum(String projectName,
                                                String componentPackageLongName,
                                                String componentTypeRefDesignator,
                                                String padTypeName,
                                                String landPatternPadName)
  {
    Assert.expect(projectName != null);
    Assert.expect(componentPackageLongName != null);
    Assert.expect(componentTypeRefDesignator != null);
    Assert.expect(padTypeName != null);
    Assert.expect(landPatternPadName != null);

    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(projectName);
    stringBuilder.append(UNDERSCORE);
    stringBuilder.append(componentPackageLongName);
    stringBuilder.append(UNDERSCORE);
    stringBuilder.append(componentTypeRefDesignator);
    stringBuilder.append(UNDERSCORE);
    stringBuilder.append(padTypeName);
    stringBuilder.append(UNDERSCORE);
    stringBuilder.append(landPatternPadName);

    return stringBuilder.toString().intern();
  }

  /**
   * @param projectName String
   * @param componentPackageLongName String
   * @param componentTypeRefDesignator String
   * @param padTypeName String
   * @param packagePinName String
   * @return String
   * @author Cheah Lee Herng
   */
  private String generatePackagePinChecksum(String projectName,
                                            String componentPackageLongName,
                                            String componentTypeRefDesignator,
                                            String padTypeName,
                                            String packagePinName)
  {
    Assert.expect(projectName != null);
    Assert.expect(componentPackageLongName != null);
    Assert.expect(componentTypeRefDesignator != null);
    Assert.expect(padTypeName != null);
    Assert.expect(packagePinName != null);

    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(projectName);
    stringBuilder.append(UNDERSCORE);
    stringBuilder.append(componentPackageLongName);
    stringBuilder.append(UNDERSCORE);
    stringBuilder.append(componentTypeRefDesignator);
    stringBuilder.append(UNDERSCORE);
    stringBuilder.append(padTypeName);
    stringBuilder.append(UNDERSCORE);
    stringBuilder.append(packagePinName);

    return stringBuilder.toString().intern();
  }

  /**
   * @author Cheah Lee Herng
   */
  private void setAlgorithmsSettingStatusToImported(Subtype subtype)
  {
    Assert.expect(subtype != null);

    List<AlgorithmSettingEnum> algorithmSettingEnums = subtype.getTunedAlgorithmSettingEnums();
    for (AlgorithmSettingEnum algorithmSettingEnum : algorithmSettingEnums)
    {
      subtype.addAlgorithmsSettingEnumToImportedValueMap(algorithmSettingEnum,
                                                         subtype.getAlgorithmSettingValue(algorithmSettingEnum));
    }
  }

  /**
   * @param componentType ComponentType
   * @param newSubtypeName String
   * @param jointType JointTypeEnum
   * @return boolean
   * @author Cheah Lee Herng
   */
  private boolean doesSubtypeNameExist(String subtypeShortName,
                                       JointTypeEnum jointType,
                                       Map<SubtypeScheme,List<Subtype>> subtypeSchemeToSubtypesMap,
                                       SubtypeScheme currentSubtypeScheme,
                                       boolean renameSubtypeName)
  {
    Assert.expect(subtypeShortName != null);
    Assert.expect(jointType != null);
    Assert.expect(subtypeSchemeToSubtypesMap != null);
    Assert.expect(currentSubtypeScheme != null);

    boolean doesSubtypeNameExist = false;

    // Looping through map of unselected subtype scheme
    for(Map.Entry<SubtypeScheme,List<Subtype>> entry : subtypeSchemeToSubtypesMap.entrySet())
    {
      if(entry.getKey().doesSubtypeHasIncompatibleInspectionFamily())
      {
         doesSubtypeNameExist = false;
         break;
      }
      
      doesSubtypeNameExist = doesSubtypeNameExistInCurrentSubtypeScheme(subtypeShortName, jointType, entry.getKey());
      if (doesSubtypeNameExist)
      {
        break;
      }
    }

    // If the new subtype name is not being used in elsewhere, make sure it is not being used in current
    if (doesSubtypeNameExist == false && renameSubtypeName)
    {
      if (doesSubtypeNameExistInCurrentSubtypeScheme(subtypeShortName, jointType, currentSubtypeScheme))
      {
        doesSubtypeNameExist = true;
      }
      else
      {
        doesSubtypeNameExist = false;
      }
    }

    return doesSubtypeNameExist;
  }

  /**
   * @param subtypeShortName String
   * @param jointType JointTypeEnum
   * @param subtypeScheme SubtypeScheme
   * @return boolean
   *
   * @author Cheah Lee Herng
   */
  public boolean doesSubtypeNameExistInCurrentSubtypeScheme(String subtypeShortName,
                                                            JointTypeEnum jointType,
                                                            SubtypeScheme subtypeScheme)
  {
    Assert.expect(subtypeShortName != null);
    Assert.expect(jointType != null);
    Assert.expect(subtypeScheme != null);

    List<Subtype> subtypes = subtypeScheme.getSubtypes();
    for (Subtype subtype : subtypes)
    {
      if (subtype.getShortName().equals(subtypeShortName) &&
          subtype.getJointTypeEnum().equals(jointType))
      {
        return true;
      }
    }
    return false;
  }

  /**
   * @param librarySubtype LibrarySubtype
   * @author Cheah Lee Herng
   */
  private void assignLibrarySubtypeThresholdvalueToProjectSubtype(String libraryPath,
                                                                  Subtype subtype,
                                                                  LibrarySubtype librarySubtype,
                                                                  String projectName,
                                                                  boolean importThreshold,
                                                                  boolean importNominal) throws DatastoreException
  {
    Assert.expect(libraryPath != null);
    Assert.expect(subtype != null);
    Assert.expect(librarySubtype != null);
    Assert.expect(projectName != null);

    DatabaseManager.getInstance().getLibrarySubtypeByRefSubtypeIdAndProjectName(libraryPath, librarySubtype, projectName);

    for (Map.Entry<AlgorithmSettingEnum, Serializable> mapEntry : librarySubtype.getThresholdNameToTunedValueMap().entrySet())
    {
      boolean proceedToUpdate = importThreshold & importNominal;
      if (proceedToUpdate == false)
      {
        if (importThreshold && importNominal == false)
        {
          AlgorithmSettingEnum algorithmSettingEnum = mapEntry.getKey();
          if (algorithmSettingEnum.getName().startsWith("Nominal"))
            proceedToUpdate = false;
          else
            proceedToUpdate = true;
        }
        else if (importThreshold == false && importNominal)
        {
          AlgorithmSettingEnum algorithmSettingEnum = mapEntry.getKey();
          if (algorithmSettingEnum.getName().startsWith("Nominal"))
            proceedToUpdate = true;
          else
            proceedToUpdate = false;
        }
        else if (importThreshold == false && importNominal == false)
        {
          proceedToUpdate = false;
        }
      }
            
      if (proceedToUpdate)
        subtype.setTunedValueFromLibrary(mapEntry.getKey(), mapEntry.getValue());
    }
  }
  
  /**   
   * @param subtype
   * @param librarySubtype
   * @throws DatastoreException 
   */
  private void updateAlgorithmEnumStatus(Subtype subtype, LibrarySubtype librarySubtype) throws DatastoreException 
  {
    Assert.expect(subtype != null);
    Assert.expect(librarySubtype != null);
    
    for (Map.Entry<AlgorithmEnum, Boolean> mapEntry : librarySubtype.getAlgorithmEnumToEnabledFlagMap().entrySet())
    {
      subtype.setAlgorithmEnabled(mapEntry.getKey(), mapEntry.getValue());
    }
  }

  /**
   * @param projectSubtypeName String
   * @param librarySubtypeName String
   * @author Cheah Lee Herng
   */
  private String createNewName(String name)
  {
    Assert.expect(name != null);

    String newSubtypeName = null;
    Pattern pattern = Pattern.compile("^(\\s*.*)_(\\d+)?$");
    Matcher matcher = pattern.matcher(name);

    if (matcher.matches())
    {
      newSubtypeName = matcher.group(1);
      if ((matcher.group(2) == null) == false)
      {
        int nextRunningNumber = Integer.parseInt(matcher.group(2)) + 1;

        newSubtypeName = newSubtypeName + "_" + Integer.toString(nextRunningNumber);
      }
      else
      {
        newSubtypeName = name + "_1";
      }
    }
    else
    {
      newSubtypeName = name + "_1";
    }
    Assert.expect(newSubtypeName != null);

    return newSubtypeName;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public String renameLandPatternName(Panel panel, String landPatternName)
  {
    Assert.expect(panel != null);
    Assert.expect(landPatternName != null);
    
    String newLandPatternName = landPatternName;
    while (panel.doesLandPatternExist(newLandPatternName))
    {
      newLandPatternName = createNewName(newLandPatternName);
    }
    return newLandPatternName;
  }

  /**
   * @param subtypeShortName String
   * @param jointType JointTypeEnum
   * @param subtypeSchemeToSubtypesMap Map
   * @param currentSubtypeScheme SubtypeScheme
   * @return String
   *
   * @author Cheah Lee Herng
   */
  public String renameSubtypeName(String subtypeShortName,
                                  JointTypeEnum jointType,
                                  Map<SubtypeScheme,List<Subtype>> subtypeSchemeToSubtypesMap,
                                  SubtypeScheme currentSubtypeScheme)
  {
    Assert.expect(subtypeShortName != null);
    Assert.expect(jointType != null);
    Assert.expect(subtypeSchemeToSubtypesMap != null);
    Assert.expect(currentSubtypeScheme != null);

    String newSubtypeName = subtypeShortName;
    boolean renameSubtypeName = false;

    // Check if newly created subtype name is used by other subtype
    while (doesSubtypeNameExist(newSubtypeName, jointType, subtypeSchemeToSubtypesMap, currentSubtypeScheme, renameSubtypeName))
    {
      newSubtypeName = createNewName(newSubtypeName);
      renameSubtypeName = true;
    }

    return newSubtypeName;
  }

  /**
   * @param padType PadType
   * @param subtype Subtype
   *
   * @author Cheah Lee Herng
   */
  private void removeSubtypeFromPadType(PadType padType, Subtype subtype)
  {
    Assert.expect(padType != null);
    Assert.expect(subtype != null);

    subtype.removeFromPadType(padType);
  }

  /**
   * @param subtypeSchemes List
   * @param panel Panel
   * @return Map
   *
   * @author Cheah Lee Herng
   */
  private void createSubtype(List<SubtypeScheme> subtypeSchemes,
                             Map<SubtypeScheme, List<Subtype>> unselectedSubtypeSchemeToSubtypesMap,
                             Panel panel,
                             ImportLibrarySettingPersistance importLibrarySettingPersistance,
                             Map<SubtypeScheme, Map<ComponentType,Map<PadType, Subtype>>> subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap,
                             Map<String, Subtype> subtypeLongNameToSubtypeMap,
                             Map<SubtypeScheme, Map<ComponentType, Map<PadType, String>>> subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap) throws DatastoreException
  {
    Assert.expect(subtypeSchemes != null);
    Assert.expect(unselectedSubtypeSchemeToSubtypesMap != null);
    Assert.expect(panel != null);
    Assert.expect(importLibrarySettingPersistance != null);
    Assert.expect(subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap != null);
    Assert.expect(subtypeLongNameToSubtypeMap != null);
    Assert.expect(subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap != null);
    
    Map<String, JointTypeEnum> subtypeNameToJointTypeEnumMap = new HashMap<String,JointTypeEnum>();    
    Subtype newSubtype = null;
    LibraryPackagePin libraryPackagePin = null;
    LibrarySubtype librarySubtype = null;
    JointTypeEnum jointType = null;
    
    boolean importThreshold = importLibrarySettingPersistance.getImportLibrarySettingStatus(ImportLibrarySettingEnum.IMPORT_THRESHOLD);
    boolean importNominal = importLibrarySettingPersistance.getImportLibrarySettingStatus(ImportLibrarySettingEnum.IMPORT_NOMINAL);
    boolean importLibrarySubtypeName = importLibrarySettingPersistance.getImportLibrarySettingStatus(ImportLibrarySettingEnum.IMPORT_LIBRARY_SUBTYPE_NAME);

    for(SubtypeScheme subtypeScheme : subtypeSchemes)
    {
      if (cancelDatabaseOperation())
        break;

      Map<ComponentType,Map<PadType,Subtype>> componentTypeToMapOfPadTypeToSubtypeMap = new HashMap<ComponentType,Map<PadType,Subtype>>();

      List<ComponentType> componentTypes = subtypeScheme.getComponentTypes();
      for(ComponentType componentType : componentTypes)
      {
        if (cancelDatabaseOperation())
          break;

        LibrarySubtypeScheme librarySubtypeScheme = componentType.getAssignLibrarySubtypeScheme();

        // Update compPackage info
        if (librarySubtypeScheme.getMatchPercentage() == MAX_PERCENT)
        {
          subtypeScheme.getCompPackage().setLibraryCheckSum(librarySubtypeScheme.getLibraryPackage().getJointTypeLibraryPackageCheckSum(subtypeScheme.getSelectedLibraryPackage().getSelectedJointType()));
          subtypeScheme.getCompPackage().setLibraryLandpatternName(librarySubtypeScheme.getLibraryPackage().getLandPattern().getName());
        }

        // Get list of package pins information from database by using library subtype scheme Id
        List<LibraryPackagePin> libraryPackagePins = new ArrayList<LibraryPackagePin>(
            DatabaseManager.getInstance().getLibraryPackagePinsByLibrarySubtypeSchemeId(subtypeScheme.getSelectedLibraryPath(),
                                                                          librarySubtypeScheme.getLibrarySubtypeSchemeId()));

        Map<PadType,Subtype> padTypeToSubtypeMap = new HashMap<PadType,Subtype>();
        int counter = 0;

        // Looping through pad type lists
        List<PadType> padTypes = componentType.getPadTypes();
        for (PadType padType : padTypes)
        {
          if (cancelDatabaseOperation())
            break;

          if(padType.getPadTypeSettings().isTestable() == false)
            continue;

          if (padTypes.size() == libraryPackagePins.size())
          {
            libraryPackagePin = libraryPackagePins.get(counter);
            librarySubtype = libraryPackagePin.getLibrarySubtype();
            jointType = JointTypeEnum.getJointTypeEnum(libraryPackagePin.getJointTypeName());
          }
          else
          {
            boolean foundLibraryPackagePin = false;
            
            // We need to figure out which LibraryPackagePin that has the same joint type as PadType
            for(LibraryPackagePin currentLibraryPackagePin : libraryPackagePins)
            {
              JointTypeEnum padTypeJointTypeEnum = padType.getJointTypeEnum();
              JointTypeEnum libraryPackageJointTypeEnum = JointTypeEnum.getJointTypeEnum(currentLibraryPackagePin.getJointTypeName());
              
              if (padTypeJointTypeEnum.equals(libraryPackageJointTypeEnum))
              {
                foundLibraryPackagePin = true;
                
                libraryPackagePin = currentLibraryPackagePin;
                librarySubtype = currentLibraryPackagePin.getLibrarySubtype();
                jointType = libraryPackageJointTypeEnum;
                
                break;
              }
            }
            
            if (foundLibraryPackagePin == false)
            {
              libraryPackagePin = libraryPackagePins.get(0);
              librarySubtype = libraryPackagePin.getLibrarySubtype();
              jointType = JointTypeEnum.getJointTypeEnum(libraryPackagePin.getJointTypeName());
            }
          }
          
          // Get subtype name
          String subtypeNameToBeUsed = librarySubtype.getSubtypeName();
          
          // XCR-3119 Unable to Retain Recipe Subtype Name After Import Library
          if (importLibrarySubtypeName == false)
          {
            if (subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap.containsKey(subtypeScheme))
            {
              Map<ComponentType, Map<PadType, String>> componentTypeToMapOfPadTypeToOriginalSubtypeNameMap = subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap.get(subtypeScheme);
              if (componentTypeToMapOfPadTypeToOriginalSubtypeNameMap.containsKey(componentType))
              {
                Map<PadType, String> padTypeToOriginalSubtypeNameMap = componentTypeToMapOfPadTypeToOriginalSubtypeNameMap.get(componentType);
                if (padTypeToOriginalSubtypeNameMap.isEmpty() == false)
                {
                  boolean foundOriginalSubtypeName = false;
                  for(Map.Entry<PadType, String> entry : padTypeToOriginalSubtypeNameMap.entrySet())
                  {
                    if (entry.getKey().getName().equalsIgnoreCase(padType.getName()))
                    {
                      subtypeNameToBeUsed = entry.getValue();
                      foundOriginalSubtypeName = true;
                      
                      break;
                    }
                  }
                  
                  if (foundOriginalSubtypeName == false)
                  {
                    // We could not find PadType but we do have at least 1 record for original Subtype. So we use that.
                    subtypeNameToBeUsed = padTypeToOriginalSubtypeNameMap.entrySet().iterator().next().getValue();
                  }
                }
              }
            }
          }

          // Generate new subtype name
          String newSubtypeName = renameSubtypeName(subtypeNameToBeUsed,
                                                    jointType,
                                                    unselectedSubtypeSchemeToSubtypesMap,
                                                    subtypeScheme);
          String newSubtypeNameAndJointTypeName = newSubtypeName + "_" +  jointType.getName();

          // Here, we make sure that different subtype object is created for different pad type.
          // If same subtype name and joint type found in the map, we will re-use the same subtype object.
          if(subtypeLongNameToSubtypeMap.containsKey(newSubtypeNameAndJointTypeName) == false)
          {
            // Create new subtype
            String newSubtypeLongName = panel.getUniqueSubtypeLongName(librarySubtypeScheme.getLibraryPackage().getLandPattern(), jointType);
            newSubtype = Subtype.createNewSubtype(panel, newSubtypeLongName, newSubtypeName, jointType);

            // Set new subtype name into panel
            if (panel.doesSubtypeExist(newSubtypeLongName) == false)
              panel.setNewSubtypeName(newSubtype, null, newSubtypeLongName);

            // Update subtype imported library path
            newSubtype.setImportedPackageLibraryPath(subtypeScheme.getSelectedLibraryPath());
            
            // Update newly-created subtype with library subtype info
            newSubtype.getSubtypeAdvanceSettings().setUserGain(librarySubtypeScheme.getLibrarySubtypes().get(0).getUserGainEnum());
            newSubtype.getSubtypeAdvanceSettings().setSignalCompensation(librarySubtypeScheme.getLibrarySubtypes().get(0).getSignalCompensationEnum());                        
            newSubtype.getSubtypeAdvanceSettings().setMagnificationType(librarySubtypeScheme.getLibrarySubtypes().get(0).getMagnificationTypeEnum());
            newSubtype.getSubtypeAdvanceSettings().setDynamicRangeOptimizationLevel(librarySubtypeScheme.getLibrarySubtypes().get(0).getDynamicRangeOptimizationLevelEnum());
            
            if (librarySubtypeScheme.getLibrarySubtypes().get(0).getArtifactCompensationStateEnum().equals(ArtifactCompensationStateEnum.NOT_APPLICABLE) == false)
              newSubtype.getSubtypeAdvanceSettings().setArtifactCompensationState(librarySubtypeScheme.getLibrarySubtypes().get(0).getArtifactCompensationStateEnum());                        

            // Assign library threshold value into newly created subtype
            if (importThreshold || importNominal)
            {
              assignLibrarySubtypeThresholdvalueToProjectSubtype(subtypeScheme.getSelectedLibraryPath(),
                                                                 newSubtype,
                                                                 librarySubtype,
                                                                 librarySubtypeScheme.getLibraryProjectName(),
                                                                 importThreshold,
                                                                 importNominal);
              
              // Update AlgorithmEnum status
              updateAlgorithmEnumStatus(newSubtype, librarySubtype);
              
              // Update algorithmSettings import status
              setAlgorithmsSettingStatusToImported(newSubtype);
            }

            // Add entry into subtypeNameToJointTypeEnum map
            subtypeNameToJointTypeEnumMap.put(newSubtypeName, jointType);

            // Add entry into subtypeNameToJointTypeEnum map
            subtypeLongNameToSubtypeMap.put(newSubtypeNameAndJointTypeName, newSubtype);

            _newlyAddedSubtypes.add(newSubtype);
          }
          
          // XCR-2853 Focus Method Settings are updated after comparison even if user has cancelled import library - Cheah Lee Herng 
          if (librarySubtypeScheme.getLibrarySubtypes().get(0).hasLibraryComponentTypes())
          {
            LibraryComponentType libraryComponentType = librarySubtypeScheme.getLibrarySubtypes().get(0).getLibraryComponentTypes().get(0);
            padType.getPadTypeSettings().setGlobalSurfaceModel(libraryComponentType.getGlobalSurfaceModelEnum());
          }

          // Add entry into padTypeToSubtype map
          padTypeToSubtypeMap.put(padType, subtypeLongNameToSubtypeMap.get(newSubtypeNameAndJointTypeName));

          counter++;
          _databaseProgressObservable.incrementPercentDone();
        }

        // Add entry into componentTypeToMapOfPadTypeToSubtype map
        componentTypeToMapOfPadTypeToSubtypeMap.put(componentType, padTypeToSubtypeMap);
      }

      // Add entry into subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtype map
      subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap.put(subtypeScheme, componentTypeToMapOfPadTypeToSubtypeMap);
    }
  }

  /**
   * @param subtypeSchemes List
   *
   * @author Cheah Lee Herng
   */
  private void removeSubtypeFromPadType(List<SubtypeScheme> subtypeSchemes)
  {
    Assert.expect(subtypeSchemes != null);

    for(SubtypeScheme subtypeScheme : subtypeSchemes)
    {
      List<ComponentType> componentTypes = subtypeScheme.getComponentTypes();
      for(ComponentType componentType : componentTypes)
      {
        List<PadType> padTypes = componentType.getPadTypes();
        for(PadType padType : padTypes)
        {
          if(padType.getPadTypeSettings().isTestable() == false)
            continue;

          removeSubtypeFromPadType(padType, padType.getSubtype());
          _databaseProgressObservable.incrementPercentDone();
        }
      }
    }
  }

  /**
   * @param project Project
   * @param subtypeSchemes List
   * @param searchByLandPatternGeometry boolean
   * @param importLibrarySettingPersistance
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   * @edited By Kee Chin Seong
   */
  public void importSubtypeScheme(Project project,
                                  List<SubtypeScheme> subtypeSchemes,
                                  boolean searchByLandPatternGeometry,
                                  ImportLibrarySettingPersistance importLibrarySettingPersistance) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(subtypeSchemes != null);
    Assert.expect(importLibrarySettingPersistance != null);

    Panel panel = project.getPanel();
    Map<SubtypeScheme, List<Subtype>> unselectedSubtypeSchemeToSubtypesMap = new HashMap<SubtypeScheme, List<Subtype>>();
    Map<SubtypeScheme, Map<ComponentType, Map<PadType, Subtype>>> subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap =
        new HashMap<SubtypeScheme, Map<ComponentType, Map<PadType, Subtype>>>();
    Map<String,String> componentTypeLongNameToSubtypeLongNameMap = new HashMap<String,String>();
    Map<String,String> componentTypeLongNameToSubtypeShortNameMap = new HashMap<String,String>();
    Map<String, Subtype> subtypeLongNameToSubtypeMap = new HashMap<String,Subtype>();
    Map<SubtypeScheme, Map<ComponentType, Map<PadType, String>>> subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap = 
            new HashMap<SubtypeScheme, Map<ComponentType, Map<PadType, String>>>();
    
    List<SubtypeScheme> exactMatchExactPackageInfoSubtypeSchemes = new ArrayList<SubtypeScheme>();
    List<SubtypeScheme> exactMatchNonExactPackageInfoSubtypeSchemes = new ArrayList<SubtypeScheme>();
    List<SubtypeScheme> nonExactMatchSubtypeSchemes = new ArrayList<SubtypeScheme>();
    
    Map<Integer, Pair<CompPackage, LandPattern>> libraryPackageToCompPackageLandPatternPairMap = new HashMap<Integer, Pair<CompPackage, LandPattern>>();

    _projectObservable.setEnabled(false);
    try
    {
      _startAssignNewSubtypeToPadType = false;

      if (cancelDatabaseOperation())
      {
        unselectedSubtypeSchemeToSubtypesMap.clear();
        subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap.clear();
        subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap.clear();
        componentTypeLongNameToSubtypeLongNameMap.clear();
        componentTypeLongNameToSubtypeShortNameMap.clear();
        subtypeLongNameToSubtypeMap.clear();
        return;
      }
      
      // Backup existing project data
      backup(project, subtypeSchemes);
      if (cancelDatabaseOperation())
      {
        unselectedSubtypeSchemeToSubtypesMap.clear();
        subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap.clear();
        subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap.clear();
        componentTypeLongNameToSubtypeLongNameMap.clear();
        componentTypeLongNameToSubtypeShortNameMap.clear();
        subtypeLongNameToSubtypeMap.clear();
        return;
      }
      
      // Get map of project subtype scheme to list of subtypes map
      unselectedSubtypeSchemeToSubtypesMap = getUnSelectedSubtypeSchemeToSubtypesMap(panel, subtypeSchemes);

      // XCR-3129 Unable to import subtype name 
      // XCR-3075 Software crash when import data from library by changing the package matching
      divideSubtypeSchemes(subtypeSchemes, exactMatchExactPackageInfoSubtypeSchemes, exactMatchNonExactPackageInfoSubtypeSchemes, nonExactMatchSubtypeSchemes);      

      // XCR-3127 Unable to customize import nominal and threshold
      if (exactMatchExactPackageInfoSubtypeSchemes.isEmpty() == false ||
          exactMatchNonExactPackageInfoSubtypeSchemes.isEmpty() == false)
      {
        // Merge SubtypeScheme
        exactMatchExactPackageInfoSubtypeSchemes.removeAll(exactMatchNonExactPackageInfoSubtypeSchemes);
        exactMatchExactPackageInfoSubtypeSchemes.addAll(exactMatchNonExactPackageInfoSubtypeSchemes);
        
        setLandPatternPad(panel, exactMatchExactPackageInfoSubtypeSchemes);
        if (cancelDatabaseOperation())
        {
          unselectedSubtypeSchemeToSubtypesMap.clear();
          subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap.clear();
          subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap.clear();
          componentTypeLongNameToSubtypeLongNameMap.clear();
          componentTypeLongNameToSubtypeShortNameMap.clear();
          subtypeLongNameToSubtypeMap.clear();
          return;
        }

        // Backup PadType original Subtype name before being overide. This is useful 
        // if user wants to retain original subtype name instead of library subtype name.
        for(SubtypeScheme subtypeScheme : exactMatchExactPackageInfoSubtypeSchemes)
        {
          if (cancelDatabaseOperation())
          {
            break;
          }

          List<ComponentType> componentTypes = subtypeScheme.getComponentTypes();
          for(ComponentType componentType : componentTypes)
          {
            backupSubtypeName(subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap, subtypeScheme, componentType);
          }
        }

        // Create new Subtype
        setupSubtype(exactMatchExactPackageInfoSubtypeSchemes,
                     unselectedSubtypeSchemeToSubtypesMap,
                     panel,
                     importLibrarySettingPersistance,
                     subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap,
                     subtypeLongNameToSubtypeMap,
                     subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap,
                     componentTypeLongNameToSubtypeLongNameMap,
                     componentTypeLongNameToSubtypeShortNameMap);

        // Remove the underlying subtype from pad type
        removeSubtypeFromPadType(exactMatchExactPackageInfoSubtypeSchemes);

        // Re-Assign the newly created subtype to each component type/pad type
        assignSubtype(exactMatchExactPackageInfoSubtypeSchemes,
                      unselectedSubtypeSchemeToSubtypesMap,
                      panel,
                      importLibrarySettingPersistance,
                      subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap,
                      subtypeLongNameToSubtypeMap,
                      subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap,
                      componentTypeLongNameToSubtypeLongNameMap,
                      componentTypeLongNameToSubtypeShortNameMap);

        // Assign package pin information (which includes pin orientation ONLY)
        setPackagaPinInfo(exactMatchExactPackageInfoSubtypeSchemes);
        if (cancelDatabaseOperation())
        {
          unselectedSubtypeSchemeToSubtypesMap.clear();
          subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap.clear();
          subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap.clear();
          componentTypeLongNameToSubtypeLongNameMap.clear();
          componentTypeLongNameToSubtypeShortNameMap.clear();
          subtypeLongNameToSubtypeMap.clear();
          return;
        }
      }

      // XCR-3127 Unable to customize import nominal and threshold
      if (nonExactMatchSubtypeSchemes.isEmpty() == false &&
          importLibrarySettingPersistance.getImportLibrarySettingStatus(ImportLibrarySettingEnum.IMPORT_LANDPATTERN) == true)
      {
        // XCR-3136 Assert when import subtype that is highlighted in Modify Subtype
        panel.getProject().getProjectState().disable();
        
        // Create Package and LandPattern for non-100% matched SubtypeSchemes
        setupPackageAndLandPattern(panel, nonExactMatchSubtypeSchemes, libraryPackageToCompPackageLandPatternPairMap, subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap);
        if (cancelDatabaseOperation())
        {
          unselectedSubtypeSchemeToSubtypesMap.clear();
          subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap.clear();
          subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap.clear();
          componentTypeLongNameToSubtypeLongNameMap.clear();
          componentTypeLongNameToSubtypeShortNameMap.clear();
          subtypeLongNameToSubtypeMap.clear();
          return;
        }

        // Create new Subtype
        setupSubtype(nonExactMatchSubtypeSchemes,
                     unselectedSubtypeSchemeToSubtypesMap,
                     panel,
                     importLibrarySettingPersistance,
                     subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap,
                     subtypeLongNameToSubtypeMap,
                     subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap,
                     componentTypeLongNameToSubtypeLongNameMap,
                     componentTypeLongNameToSubtypeShortNameMap);

        // Re-Assign the newly created subtype to each component type/pad type
        assignSubtype(nonExactMatchSubtypeSchemes,
                      unselectedSubtypeSchemeToSubtypesMap,
                      panel,
                      importLibrarySettingPersistance,
                      subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap,
                      subtypeLongNameToSubtypeMap,
                      subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap,
                      componentTypeLongNameToSubtypeLongNameMap,
                      componentTypeLongNameToSubtypeShortNameMap);
        
        // XCR-3136 Assert when import subtype that is highlighted in Modify Subtype
        panel.getProject().getProjectState().enable();
      }

      _databaseProgressObservable.reportProgress(100);
    }
    finally
    {
      // Clear off map and release memory
      unselectedSubtypeSchemeToSubtypesMap.clear();
      subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap.clear();
      subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap.clear();
      componentTypeLongNameToSubtypeLongNameMap.clear();
      componentTypeLongNameToSubtypeShortNameMap.clear();
      subtypeLongNameToSubtypeMap.clear();
      unselectedSubtypeSchemeToSubtypesMap = null;
      subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap = null;
      subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap = null;
      componentTypeLongNameToSubtypeLongNameMap = null;
      componentTypeLongNameToSubtypeShortNameMap = null;
      subtypeLongNameToSubtypeMap = null;
      
      exactMatchExactPackageInfoSubtypeSchemes.clear();
      exactMatchNonExactPackageInfoSubtypeSchemes.clear();
      nonExactMatchSubtypeSchemes.clear();
      
      libraryPackageToCompPackageLandPatternPairMap.clear();
      libraryPackageToCompPackageLandPatternPairMap = null;

      if (cancelDatabaseOperation() == false)
      {
        clearMap();
        _newlyAddedSubtypes.clear();

        for(SubtypeScheme subtypeScheme : subtypeSchemes)
        {
          subtypeScheme.getCompPackage().setBackupStatus(false);
        }
      }

      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, ProjectEventEnum.LIBRARY_IMPORT_COMPLETE);
  }

  /**
   * @param cancelDatabaseOperation boolean
   * @author Tan Hock Zoon
   */
  public void setCancelDatabaseOperation(boolean cancelDatabaseOperation)
  {
    _cancelDatabaseOperation = cancelDatabaseOperation;
  }

  /**
   * @return boolean
   * @author Tan Hock Zoon
   */
  public boolean cancelDatabaseOperation()
  {
    return _cancelDatabaseOperation;
  }


  /**
   * This is to populate all the threshold for a particular LibrarySubtypeScheme
   *   
   * @param libraryPath
   * @param libraryPackage
   * @param projectName
   * 
   * @author Poh Kheng
   */
  public void populateAlgorithmSettingByLibrarySubtypeScheme(String libraryPath, LibraryPackage libraryPackage) throws DatastoreException
  {
    Assert.expect(libraryPath != null);
    Assert.expect(libraryPackage != null);
    
    for(LibrarySubtypeScheme librarySubtypeScheme : libraryPackage.getSelectedLibrarySubtypeSchemes())
    {
      for (LibrarySubtype librarySubtype : librarySubtypeScheme.getLibrarySubtypes())
      {
        librarySubtype = DatabaseManager.getInstance().getLibrarySubtypeByRefSubtypeIdAndProjectName(libraryPath, 
                                                                                                     librarySubtype, 
                                                                                                     librarySubtypeScheme.getLibraryProjectName());
      }
    }
  }

  /**
   * @param panel Panel
   *
   * @author Cheah Lee Herng
   */
  public void removeUnusedSubtype(Panel panel)
  {
    Assert.expect(panel != null);

    _projectObservable.setEnabled(false);
    try
    {
      // Remove unused subtype from panel
      List<Subtype> unusedSubtypes = panel.getUnusedSubtypes();
      for (Subtype unusedSubtype : unusedSubtypes)
      {
        unusedSubtype.destroy();
        unusedSubtype = null;
      }
      unusedSubtypes.clear();
      unusedSubtypes = null;
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, ProjectEventEnum.LIBRARY_IMPORT_COMPLETE);
  }

  /**
   * @param panel Panel
   * @param subtypes List
   * @author Cheah Lee Herng
   */
  public void removeUnusedSubtype(Panel panel, List<Subtype> subtypes)
  {
    Assert.expect(panel != null);
    Assert.expect(subtypes != null);

    _projectObservable.setEnabled(false);
    try
    {
      // Remove unused subtype from panel
      for (Subtype subtype : subtypes)
      {
        if (panel.isSubtypeInUse(subtype) == false)
        {
          subtype.destroy();
          subtype = null;
        }
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    _projectObservable.stateChanged(this, null, null, ProjectEventEnum.LIBRARY_IMPORT_COMPLETE);
  }

  /**
   * @param panel Panel
   * @return Map
   *
   * @author Cheah Lee Herng
   */
  public Map<SubtypeScheme,List<Subtype>> getSubtypeSchemeToSubtypesMap(Panel panel)
  {
    Assert.expect(panel != null);

    Map<SubtypeScheme,List<Subtype>> projecSubtypeSchemeToSubtypes = new HashMap<SubtypeScheme,List<Subtype>>();
    List<SubtypeScheme> subtypeSchemes = new ArrayList<SubtypeScheme>();

    // Get all subtype schemes
    List<CompPackage> compPackages = panel.getCompPackages();
    Iterator<CompPackage> it = compPackages.iterator();
    CompPackage compPackage;
    while(it.hasNext())
    {
      compPackage = it.next();
      subtypeSchemes.addAll(compPackage.getSubtypeSchemes());
    }

    // Add entry into map
    for(SubtypeScheme subtypeScheme : subtypeSchemes)
    {
      projecSubtypeSchemeToSubtypes.put(subtypeScheme, subtypeScheme.getSubtypes());
    }

    // Release memory
    compPackages.clear();
    subtypeSchemes.clear();
    compPackages = null;
    subtypeSchemes = null;
    it = null;

    return projecSubtypeSchemeToSubtypes;
  }

  /**
   * @param panel Panel
   * @return Map
   *
   * @author Cheah Lee Herng
   */
  public Map<SubtypeScheme,List<Subtype>> getUnSelectedSubtypeSchemeToSubtypesMap(Panel panel, List<SubtypeScheme> subtypeSchemes)
  {
    Assert.expect(panel != null);
    Assert.expect(subtypeSchemes != null);

    Map<SubtypeScheme,List<Subtype>> subtypeSchemeToSubtypes = getSubtypeSchemeToSubtypesMap(panel);

    for(SubtypeScheme subtypeScheme : subtypeSchemes)
    {
      if (subtypeSchemeToSubtypes.containsKey(subtypeScheme))
      {
        subtypeSchemeToSubtypes.remove(subtypeScheme);
      }
    }

    return subtypeSchemeToSubtypes;
  }
  
  /**
   * XCR-3129 Unable to import subtype name 
   * To categorize SubtypeScheme into 3 different lists.
   * 
   * @param subtypeSchemes Selected SubtypeSchemes
   * @param exactMatchExactPackageInfoSubtypeSchemes SubtypeSchemes that has 100% matching percentage and matched package name and joint type
   * @param exactMatchNonExactPackageInfoSubtypeSchemes SubtypeSchemes that has 100% matching percentage but not matched package name or joint type
   * @param nonExactMatchSubtypeSchemes SubtypeSchemes that does not have 100% matching percentage and not matched package name and joint type
   * 
   * @author Cheah Lee Herng
   */
  private void divideSubtypeSchemes(List<SubtypeScheme> subtypeSchemes,
                                    List<SubtypeScheme> exactMatchExactPackageInfoSubtypeSchemes,
                                    List<SubtypeScheme> exactMatchNonExactPackageInfoSubtypeSchemes,
                                    List<SubtypeScheme> nonExactMatchSubtypeSchemes)
  {
    Assert.expect(subtypeSchemes != null);
    Assert.expect(exactMatchExactPackageInfoSubtypeSchemes != null);
    Assert.expect(exactMatchNonExactPackageInfoSubtypeSchemes != null);
    Assert.expect(nonExactMatchSubtypeSchemes != null);
    
    for(SubtypeScheme subtypeScheme : subtypeSchemes)
    {
      if (subtypeScheme.hasComponentTypes())
      {
        ComponentType componentType = subtypeScheme.getComponentTypes().get(0);
        LibrarySubtypeScheme librarySubtypeScheme = componentType.getAssignLibrarySubtypeScheme();
        
        // XCR-3075 Software crash when import data from library by changing the package matching
        if (subtypeScheme.getCompPackage().usesOneJointTypeEnum())
        {
          if (librarySubtypeScheme.getMatchPercentage() == MAX_PERCENT &&
              librarySubtypeScheme.getLibraryPackage().getSelectedJointType().equalsIgnoreCase(subtypeScheme.getCompPackage().getJointTypeEnum().getName()) &&
              librarySubtypeScheme.getLibraryPackage().getPackageLongName().equalsIgnoreCase(subtypeScheme.getCompPackage().getLongName()) &&
              exactMatchExactPackageInfoSubtypeSchemes.contains(subtypeScheme) == false)
          {
            exactMatchExactPackageInfoSubtypeSchemes.add(subtypeScheme);
          }
          else if (librarySubtypeScheme.getMatchPercentage() == MAX_PERCENT &&
                   exactMatchNonExactPackageInfoSubtypeSchemes.contains(subtypeScheme) == false)
          {
            exactMatchNonExactPackageInfoSubtypeSchemes.add(subtypeScheme);
          }
          else if (nonExactMatchSubtypeSchemes.contains(subtypeScheme) == false)
          {
            nonExactMatchSubtypeSchemes.add(subtypeScheme);
          }
        }
        else
        {
          List<String> recipeJointTypeNames = new ArrayList<String>();          
          for(PackagePin packagePin : componentType.getCompPackage().getPackagePins())
          {
            if (recipeJointTypeNames.contains(packagePin.getJointTypeEnum().getName()) == false)
              recipeJointTypeNames.add(packagePin.getJointTypeEnum().getName());
          }
          
          List<String> libraryJointTypeNames = new ArrayList<String>();
          for(LibraryPackagePin libraryPackagePin : librarySubtypeScheme.getLibraryPackage().getLibraryPackagePins())
          {
            if (libraryJointTypeNames.contains(libraryPackagePin.getJointTypeName()) == false)
              libraryJointTypeNames.add(libraryPackagePin.getJointTypeName());
          }
          
          // Figure out if we have different joint type comparison
          boolean hasLibraryJointType = true;
          for(String recipeJointTypeName : recipeJointTypeNames)
          {
            if (libraryJointTypeNames.contains(recipeJointTypeName) == false)
            {
              hasLibraryJointType = false;
              break;
            }
          }
          
          if (librarySubtypeScheme.getMatchPercentage() == MAX_PERCENT && 
              hasLibraryJointType &&
              librarySubtypeScheme.getLibraryPackage().getPackageLongName().equalsIgnoreCase(subtypeScheme.getCompPackage().getLongName()) &&
              exactMatchExactPackageInfoSubtypeSchemes.contains(subtypeScheme) == false)
          {
            exactMatchExactPackageInfoSubtypeSchemes.add(subtypeScheme);
          }
          else if (librarySubtypeScheme.getMatchPercentage() == MAX_PERCENT &&
                   exactMatchNonExactPackageInfoSubtypeSchemes.contains(subtypeScheme) == false)
          {
            exactMatchNonExactPackageInfoSubtypeSchemes.add(subtypeScheme);
          }
          else if (nonExactMatchSubtypeSchemes.contains(subtypeScheme) == false)
          {
            nonExactMatchSubtypeSchemes.add(subtypeScheme);
          }
        }
      }
    }
    
    // Sanity check
    Assert.expect(subtypeSchemes.size() == (exactMatchExactPackageInfoSubtypeSchemes.size() + 
                                            exactMatchNonExactPackageInfoSubtypeSchemes.size() + 
                                            nonExactMatchSubtypeSchemes.size()));
  }

  /**
   * @param subtypeShortName String
   * @param jointTypeName String
   * @return String
   *
   * @author Cheah Lee Herng
   */
  public String formatSubtypeLongName(String subtypeShortName, String jointTypeName)
  {
    Assert.expect(subtypeShortName != null);
    Assert.expect(jointTypeName != null);

    String formattedSubtypeName = null;
    Pattern pattern = Pattern.compile("^(\\s*.*)_(\\d+)$");
    Matcher matcher = pattern.matcher(subtypeShortName);
    if (matcher.matches())
    {
      formattedSubtypeName = matcher.group(1);
      String runningNumber = matcher.group(2);
      if (runningNumber.equals("1"))
      {
        formattedSubtypeName = formattedSubtypeName + "_" + jointTypeName;
      }
      else
      {
        formattedSubtypeName = formattedSubtypeName + "_" + runningNumber + "_" + jointTypeName;
      }
    }
    else
    {
      formattedSubtypeName = subtypeShortName + "_" + jointTypeName;
    }

    return formattedSubtypeName;
  }

  /**
   * @param padTypes List
   * @param padTypeToSubtypeMap Map
   * @param componentType ComponentType
   *
   * @author Cheah Lee Herng
   */
  public void setSubtype(List<PadType> padTypes, Map<PadType,Subtype> padTypeToSubtypeMap, ComponentType componentType)
  {
    Assert.expect(padTypes != null);
    Assert.expect(padTypeToSubtypeMap != null);
    Assert.expect(componentType != null);

    for (PadType padType : padTypes)
    {
      Subtype newSubtype = padTypeToSubtypeMap.get(padType);
      Assert.expect(newSubtype != null);

      // Assign joint type enum to package pin
      padType.getPackagePin().setJointTypeEnum(newSubtype.getJointTypeEnum());

      _databaseProgressObservable.incrementPercentDone();
    }

    // XCR-3075 Software crash when import data from library by changing the package matching
    // Commented out below due to incorrect package matching.
    // Assign joint type enum to component type
    //if (componentType.getCompPackage().usesOneJointTypeEnum() && padTypes.size() > 0)
    //  componentType.setJointTypeEnum(padTypes.get(0).getJointTypeEnum());

    for (PadType padType : padTypes)
    {
      Subtype newSubtype = padTypeToSubtypeMap.get(padType);
      Assert.expect(newSubtype != null);

      // Assign newly created subtype to replace existing pad type subtype
      padType.getPadTypeSettings().setSubtype(newSubtype);

      _databaseProgressObservable.incrementPercentDone();
    }
  }

  /**
   * @param padTypes List
   * @param componentTypeLongNameToSubtypeLongNameMap Map
   * @param componentTypeLongNameToSubtypeShortNameMap Map
   * @param componentType ComponentType
   * @param panel Panel
   * @author Cheah Lee Herng
   */
  public void setSubtypeByComponentTypeLongName(List<PadType> padTypes,
                                                Map<String,String> componentTypeLongNameToSubtypeLongNameMap,
                                                Map<String,String> componentTypeLongNameToSubtypeShortNameMap,
                                                ComponentType componentType,
                                                Panel panel)
  {
    Assert.expect(padTypes != null);
    Assert.expect(componentTypeLongNameToSubtypeLongNameMap != null);
    Assert.expect(componentTypeLongNameToSubtypeShortNameMap != null);
    Assert.expect(componentType != null);
    Assert.expect(panel != null);

    for (PadType padType : padTypes)
    {
      String componentTypeLongName = componentType.getReferenceDesignator() + "_" + padType.getName();
      String subtypeLongName = componentTypeLongNameToSubtypeLongNameMap.get(componentTypeLongName);
      Assert.expect(subtypeLongName != null);

      if (panel.doesSubtypeExist(subtypeLongName))
      {
        Subtype newSubtype = panel.getSubtype(subtypeLongName);
        newSubtype.setShortName(componentTypeLongNameToSubtypeShortNameMap.get(componentTypeLongName));
        padType.getPackagePin().setJointTypeEnum(newSubtype.getJointTypeEnum());
        padType.getPadTypeSettings().setSubtype(newSubtype);

        // Make sure current pad type is assigned to this subtype
        if (newSubtype.isPadTypeExist(padType) == false)
          newSubtype.assignedToPadType(padType);

        _databaseProgressObservable.incrementPercentDone();
      }
      else
      {
        // Should not happen
        Assert.expect(false);
      }
    }
  }

  /**
   * @author Poh Kheng
   * @author Siew Yeng - add parameter padHoleConsolidateInfo
   */
  public double calculateMatchPercentageByImagePixel(LandPattern projectLandPattern,
                                                     String padConsolidateData,
                                                     int libraryLandPatternWidthInNanoMeters,
                                                     int libraryLandPatternLengthInNanoMeters,
                                                     int numberOfLibraryLandPatternPads,
                                                     String padHoleConsolidateInfo) throws DatastoreException
  {
    Assert.expect(projectLandPattern != null);
    Assert.expect(padConsolidateData != null);
    Assert.expect(libraryLandPatternWidthInNanoMeters > 0);
    Assert.expect(libraryLandPatternLengthInNanoMeters > 0);
    Assert.expect(numberOfLibraryLandPatternPads > 0);
    int counter = 0;

    try
    {
      String[] padConsolidateDataSplit = padConsolidateData.split(COMMA);
      String[] holeLocationAndSize = padHoleConsolidateInfo.split(SEMICOLON);

      int maxWidth = Math.max(projectLandPattern.getWidthInNanoMeters(), libraryLandPatternWidthInNanoMeters);
      int maxHeight = Math.max(projectLandPattern.getLengthInNanoMeters(), libraryLandPatternLengthInNanoMeters);      

      LibraryLandPattern libraryLandPattern = new LibraryLandPattern();
      for (int i=0; i<numberOfLibraryLandPatternPads; i++)
      {
        LibraryLandPatternPad libraryLandPatternPad = new LibraryLandPatternPad();
        libraryLandPatternPad.setXLocation(Integer.parseInt(padConsolidateDataSplit[counter + (SubtypeScheme._X_COORDINATE_INDEX_PAD_DATA * numberOfLibraryLandPatternPads)]));
        libraryLandPatternPad.setYLocation(Integer.parseInt(padConsolidateDataSplit[counter + (SubtypeScheme._Y_COORDINATE_INDEX_PAD_DATA * numberOfLibraryLandPatternPads)]));
        libraryLandPatternPad.setLengthInNanoMeters(Integer.parseInt(padConsolidateDataSplit[counter + (SubtypeScheme._LENGTH_INDEX_PAD_DATA * numberOfLibraryLandPatternPads)]));
        libraryLandPatternPad.setWidthInNanoMeters(Integer.parseInt(padConsolidateDataSplit[counter + (SubtypeScheme._WIDTH_INDEX_PAD_DATA * numberOfLibraryLandPatternPads)]));
        libraryLandPatternPad.setDegreesRotation(Integer.parseInt(padConsolidateDataSplit[counter + (SubtypeScheme._DEGREE_ROTATION_INDEX_PAD_DATA * numberOfLibraryLandPatternPads)]));
        libraryLandPatternPad.setShapeEnum(ShapeEnum.getShapeEnum(padConsolidateDataSplit[counter + (SubtypeScheme._SHAPE_INDEX_PAD_DATA * numberOfLibraryLandPatternPads)]));
        
        String[] holeLocationAndSizeSplit = holeLocationAndSize[i].split(COMMA);
        libraryLandPatternPad.setHoleXLocation(Integer.parseInt(holeLocationAndSizeSplit[0]));
        libraryLandPatternPad.setHoleYLocation(Integer.parseInt(holeLocationAndSizeSplit[1]));
        libraryLandPatternPad.setHoleWidth(Integer.parseInt(holeLocationAndSizeSplit[2]));
        libraryLandPatternPad.setHoleLength(Integer.parseInt(holeLocationAndSizeSplit[3]));
        libraryLandPattern.addLibraryLandPatternPad(libraryLandPatternPad);

        counter++;
      }


      ComponentCoordinate minProjectPadCoordinate = new ComponentCoordinate();
      ComponentCoordinate maxProjectPadCoordinate = new ComponentCoordinate();
      ComponentCoordinate minLibraryPadCoordinate = new ComponentCoordinate();
      ComponentCoordinate maxLibraryPadCoordinate = new ComponentCoordinate();
      projectLandPattern.getMinAndMaxPadBoundCoordinateForAllLandPatternPads(minProjectPadCoordinate, maxProjectPadCoordinate);
      libraryLandPattern.getMinAndMaxPadBoundCoordinateForAllLandPatternPads(minLibraryPadCoordinate, maxLibraryPadCoordinate);

      double nanoMetersToPixelZoom = 1.0 / (MathUtil.NANOMETERS_PER_MIL * 3);
      AffineTransform nanoMetersToPixelTransform = AffineTransform.getScaleInstance(nanoMetersToPixelZoom, -nanoMetersToPixelZoom);
      AffineTransform projectTransform = new AffineTransform(nanoMetersToPixelTransform);
      AffineTransform libraryTransform = new AffineTransform(nanoMetersToPixelTransform);
      IntRectangle intProjectRectangle = new IntRectangle(new Rectangle2D.Double(minProjectPadCoordinate.getX(), minProjectPadCoordinate.getY(), maxWidth, maxHeight));
      IntRectangle intLibraryRectangle = new IntRectangle(new Rectangle2D.Double(minLibraryPadCoordinate.getX(), minLibraryPadCoordinate.getY(), maxWidth, maxHeight));
      Rectangle2D landPatternPixelRect = nanoMetersToPixelTransform.createTransformedShape(intProjectRectangle.getRectangle2D()).getBounds2D();

      BufferedImage adjustedProjectBufferedImage = new BufferedImage((int)Math.ceil(landPatternPixelRect.getWidth()),
                                                                     (int)Math.ceil(landPatternPixelRect.getHeight()),
                                                                     BufferedImage.TYPE_BYTE_GRAY);

      BufferedImage adjustedLibraryBufferedImage = new BufferedImage((int)Math.ceil(landPatternPixelRect.getWidth()),
                                                                     (int)Math.ceil(landPatternPixelRect.getHeight()),
                                                                     BufferedImage.TYPE_BYTE_GRAY);

      java.awt.Graphics2D projectGraphics2d = (java.awt.Graphics2D)adjustedProjectBufferedImage.getGraphics();
      java.awt.Graphics2D libraryGraphics2d = (java.awt.Graphics2D)adjustedLibraryBufferedImage.getGraphics();


      projectTransform.translate(intProjectRectangle.getWidth()/2 - intProjectRectangle.getCenterX(),
                          (-1 * intProjectRectangle.getHeight()/2) - intProjectRectangle.getCenterY());
      projectGraphics2d.setTransform(projectTransform);
//      projectGraphics2d.setColor(java.awt.Color.darkGray);
//      projectGraphics2d.fill(intProjectRectangle.getRectangle2D());
      projectGraphics2d.setColor(java.awt.Color.LIGHT_GRAY);
      libraryTransform.translate(intLibraryRectangle.getWidth()/2 - intLibraryRectangle.getCenterX(),
                          (-1 * intLibraryRectangle.getHeight()/2) - intLibraryRectangle.getCenterY());
      libraryGraphics2d.setTransform(libraryTransform);
//      libraryGraphics2d.setColor(java.awt.Color.darkGray);
//      libraryGraphics2d.fill(intLibraryRectangle.getRectangle2D());
      libraryGraphics2d.setColor(java.awt.Color.LIGHT_GRAY);

      for (LandPatternPad landPatternPad : projectLandPattern.getLandPatternPads())
      {
        projectGraphics2d.fill(landPatternPad.getShapeInNanoMeters());
        //Siew Yeng - XCR-3318 - fill hole shape
        if(landPatternPad.isThroughHolePad())
        {
          projectGraphics2d.setColor(java.awt.Color.DARK_GRAY);
          projectGraphics2d.fill(((ThroughHoleLandPatternPad)landPatternPad).getHoleShapeInNanoMeters());
          projectGraphics2d.setColor(java.awt.Color.LIGHT_GRAY);
        }
      }

      for (LibraryLandPatternPad libraryLandPatternPad : libraryLandPattern.getLibraryLandPatternPads())
      {
        libraryGraphics2d.fill(libraryLandPatternPad.getShapeInNanoMeters());
        //Siew Yeng - XCR-3318 - fill hole shape
        if(libraryLandPatternPad.isThroughHolePad())
        {
          libraryGraphics2d.setColor(java.awt.Color.DARK_GRAY);
          libraryGraphics2d.fill(libraryLandPatternPad.getHoleShapeInNanoMeters());
          libraryGraphics2d.setColor(java.awt.Color.LIGHT_GRAY);
        }
      }


      Image adjustedProjectImage = Image.createFloatImageFromBufferedImage(adjustedProjectBufferedImage);
      Image adjustedLibraryImage = Image.createFloatImageFromBufferedImage(adjustedLibraryBufferedImage);

//      ImageIoUtil.saveImage(adjustedProjectImage, "C:/debug/adjustedProjectImage"+imageDebugCounter+"_"+projectLandPattern.getName()+".png");
//      ImageIoUtil.saveImage(adjustedLibraryImage, "C:/debug/adjustedLibraryImage"+imageDebugCounter+"_"+libraryLandPatternName+".png");
      Image diffImage = Arithmetic.subtractImages(adjustedProjectImage, adjustedLibraryImage);
//      ImageIoUtil.saveImage(diffImage, "C:/debug/diffImage"+imageDebugCounter+".png");
//      imageDebugCounter++;

      Arithmetic.absoluteValueInPlace(diffImage);
      double totalDifference = Statistics.sumOfPixels(diffImage);
      double totalPixelsInBothLandPatterns = Statistics.sumOfPixels(adjustedProjectImage) + Statistics.sumOfPixels(adjustedLibraryImage);
      
      double percentOfPixelsMatching = 0.0;
      if (totalPixelsInBothLandPatterns > 0.0)
      {
        percentOfPixelsMatching = 100.0 * (1 - totalDifference / totalPixelsInBothLandPatterns);
      }

      adjustedProjectImage.decrementReferenceCount();
      adjustedLibraryImage.decrementReferenceCount();
      diffImage.decrementReferenceCount();

      return percentOfPixelsMatching;
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }
    Assert.expect(false);
    return 0.0;
  }

  /**
   * @param subtypeSchemes List
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  private void setPackagaPinInfo(List<SubtypeScheme> subtypeSchemes) throws DatastoreException
  {
    Assert.expect(subtypeSchemes != null);

    for(SubtypeScheme subtypeScheme : subtypeSchemes)
    {
      if (cancelDatabaseOperation())
      {
        break;
      }

      // Get project package pins
      List<PackagePin> packagePins = subtypeScheme.getCompPackage().getPackagePins();

      List<ComponentType> componentTypes = subtypeScheme.getComponentTypes();
      for(ComponentType componentType : componentTypes)
      {
        LibrarySubtypeScheme librarySubtypeScheme = componentType.getAssignLibrarySubtypeScheme();

        // Get list of package pins information from database by using library subtype scheme Id
        List<LibraryPackagePin> libraryPackagePins = new ArrayList<LibraryPackagePin>(
            DatabaseManager.getInstance().getLibraryPackagePinsByLibrarySubtypeSchemeId(subtypeScheme.getSelectedLibraryPath(),
                                                                          librarySubtypeScheme.getLibrarySubtypeSchemeId()));

        int counter = 0;

        if (libraryPackagePins.size() == packagePins.size())
        {
          // Looping through pad type lists
          List<PadType> padTypes = componentType.getPadTypes();
          for (PadType padType : padTypes)
          {
            if (padType.getPadTypeSettings().isTestable() == false)
            {
              counter++;
              continue;
            }

            LibraryPackagePin libraryPackagePin = libraryPackagePins.get(counter);
            PinOrientationEnum pinOrientationEnum = libraryPackagePin.getPinOrientationEnum();
            JointTypeEnum jointTypeEnum = padType.getJointTypeEnum();

            if ((JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.COLLAPSABLE_BGA) ||
                 (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR)) ||
                 (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.NON_COLLAPSABLE_BGA)) ||
                 (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.CGA)) ||
                 (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.PRESSFIT)) ||
                 (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.CHIP_SCALE_PACKAGE)) ||
                 (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.THROUGH_HOLE)) ||
                 (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.OVAL_THROUGH_HOLE)) || //Siew Yeng - XCR-3318 - Oval PTH
                 (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.SINGLE_PAD)) ||
                 (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.LGA))))
            {
              pinOrientationEnum = PinOrientationEnum.NOT_APPLICABLE;
            }

            // Assign pin orientation
            padType.getPackagePin().setPinOrientationEnum(pinOrientationEnum);

            // Assign pin status (Whether is Non-inspectable or inspectable)
            //padType.getPadTypeSettings().setInspected(libraryPackagePin.isInspected());

            counter++;
            _databaseProgressObservable.incrementPercentDone();
          }
        }
      }
    }
  }

  /**
   * @param panel Panel
   * @param subtypeSchemes List
   * @return boolean
   * @throws DatastoreException
   * @author Cheah Lee Herng
   */
  private boolean setLandPatternPad(Panel panel, List<SubtypeScheme> subtypeSchemes) throws DatastoreException
  {
    Assert.expect(panel != null);
    Assert.expect(subtypeSchemes != null);

    LandPatternPad newLandPatternPad = null;
    Map<String,LandPatternPad> landPatternPadLongNameToLandPatternPadMap = new HashMap<String,LandPatternPad>();
    boolean isDifferentLandPatternPadType = false;
    boolean setLandPatternPad = false;

    for(SubtypeScheme subtypeScheme : subtypeSchemes)
    {
      if (cancelDatabaseOperation())
      {
        break;
      }

      // Get project package pins
      List<PackagePin> packagePins = subtypeScheme.getCompPackage().getPackagePins();

      List<ComponentType> componentTypes = subtypeScheme.getComponentTypes();
      for(ComponentType componentType : componentTypes)
      {
        LibrarySubtypeScheme librarySubtypeScheme = componentType.getAssignLibrarySubtypeScheme();
        double matchPercentage = librarySubtypeScheme.getMatchPercentage();

        // We only change part from SMT --> TH or TH --> SMT when matched 100%
        if (matchPercentage == 100)
        {
          // Get list of package pins information from database by using library subtype scheme Id
          List<LibraryPackagePin> libraryPackagePins = new ArrayList<LibraryPackagePin>(
              DatabaseManager.getInstance().getLibraryPackagePinsByLibrarySubtypeSchemeId(subtypeScheme.getSelectedLibraryPath(),
                                                                                          librarySubtypeScheme.getLibrarySubtypeSchemeId()));

          // Set library land pattern pad
          setLibraryLandPatternPad(subtypeScheme.getSelectedLibraryPath(), libraryPackagePins);

          int counter = 0;

          if (libraryPackagePins.size() == packagePins.size())
          {
            // Looping through pad type lists
            List<PadType> padTypes = componentType.getPadTypes();
            for (PadType padType : padTypes)
            {
              if (padType.getPadTypeSettings().isTestable() == false)
              {
                counter++;
                continue;
              }

              LandPatternPad projectLandPatternPad = padType.getLandPatternPad();
              LibraryLandPatternPad libraryLandPatternPad = libraryPackagePins.get(counter).getLibraryLandPatternPad();
              LandPattern projectLandPattern = projectLandPatternPad.getLandPattern();
              String landPatternPadLongName = getLandPatternPadLongName(projectLandPattern, projectLandPatternPad);
              String originalLandPatternPadName = projectLandPatternPad.getName();

              // Check if project land pattern pad is of same type (surface-mount/through-hole) with
              // library land pattern pad
              if ((projectLandPatternPad.isSurfaceMountPad() && libraryLandPatternPad.isSurfaceMountPad() == false) ||
                  (projectLandPatternPad.isSurfaceMountPad() == false && libraryLandPatternPad.isSurfaceMountPad()))
              {
                isDifferentLandPatternPadType = true;
              }

              if (isDifferentLandPatternPadType)
              {
                setLandPatternPad = true;

                // Get project land pattern pad information
                ComponentCoordinate componentCoordinate = projectLandPatternPad.getCoordinateInNanoMeters();
                double rotation = projectLandPatternPad.getDegreesRotation();
                int length = projectLandPatternPad.getLengthInNanoMeters();
                int width = projectLandPatternPad.getWidthInNanoMeters();
                ShapeEnum shapeEnum = projectLandPatternPad.getShapeEnum();

                if (landPatternPadLongNameToLandPatternPadMap.containsKey(landPatternPadLongName) == false)
                {
                  if (libraryLandPatternPad.isSurfaceMountPad())
                  {
                    newLandPatternPad = projectLandPattern.createSurfaceMountLandPatternPad(componentCoordinate,
                                                                                            rotation,
                                                                                            length,
                                                                                            width,
                                                                                            shapeEnum);
                  }
                  else
                  {
                    newLandPatternPad = projectLandPattern.createThroughHoleLandPatternPad(componentCoordinate,
                                                                                           rotation,
                                                                                           length,
                                                                                           width,
                                                                                           shapeEnum);

                    ((ThroughHoleLandPatternPad)newLandPatternPad).setHoleCoordinateInNanoMeters(componentCoordinate);
//                    ((ThroughHoleLandPatternPad)newLandPatternPad).setHoleDiameterInNanoMeters(libraryLandPatternPad.getHoleDiameter());
                    //Siew Yeng - XCR-3318 - Oval PTH
                    ((ThroughHoleLandPatternPad)newLandPatternPad).setHoleWidthInNanoMeters(libraryLandPatternPad.getHoleWidth());
                    ((ThroughHoleLandPatternPad)newLandPatternPad).setHoleLengthInNanoMeters(libraryLandPatternPad.getHoleLength());
                  }
                  projectLandPatternPad.remove();
                  newLandPatternPad.setName(originalLandPatternPadName);

                  // Add entry into map
                  landPatternPadLongNameToLandPatternPadMap.put(landPatternPadLongName, newLandPatternPad);
                }
                else
                {
                  newLandPatternPad = landPatternPadLongNameToLandPatternPadMap.get(landPatternPadLongName);
                }

                // Assign new land pattern pad
                padType.setLandPatternPad(newLandPatternPad);
              }
              else if (landPatternPadLongNameToLandPatternPadMap.containsKey(landPatternPadLongName))
              {
                // Assign new land pattern pad
                padType.setLandPatternPad(landPatternPadLongNameToLandPatternPadMap.get(landPatternPadLongName));
              }
              else
              {
                // Update existing LandPatternPad info
                if (libraryLandPatternPad.isSurfaceMountPad() == false)
                {
                  ((ThroughHoleLandPatternPad)projectLandPatternPad).setHoleWidthInNanoMeters(libraryLandPatternPad.getHoleWidth());
                  ((ThroughHoleLandPatternPad)projectLandPatternPad).setHoleLengthInNanoMeters(libraryLandPatternPad.getHoleLength());
                }
                
                projectLandPatternPad.setDegreesRotationWithoutAffectingPinOrientation(projectLandPatternPad.getDegreesRotation());
              }

              counter++;
              _databaseProgressObservable.incrementPercentDone();
            }
            
            // XCR-3075 Software crash when import data from library by changing the package matching
            // Update CompPackage status
            componentType.getCompPackage().setImported(true);
            componentType.getCompPackage().setSourceLibrary(subtypeScheme.getSelectedLibraryPath());

            if (subtypeScheme.getCompPackage().getSubtypes().isEmpty() == false)
              subtypeScheme.getCompPackage().saveImportState();
          }
        }
      }

      isDifferentLandPatternPadType = false;
    }

    return setLandPatternPad;
  }

  /**
   * @param libraryPath String
   * @param libraryPackagePins List
   * @throws DatastoreException
   *
   * @author Cheah Lee Herng
   */
  private void setLibraryLandPatternPad(String libraryPath,
                                        List<LibraryPackagePin> libraryPackagePins) throws DatastoreException
  {
    Assert.expect(libraryPackagePins != null);

    for(LibraryPackagePin libraryPackagePin : libraryPackagePins)
    {
      DatabaseManager.getInstance().getLibraryLandPatternPadByLibraryPackagePinId(libraryPath, libraryPackagePin);
    }
  }

  /**
   * @param landPattern LandPattern
   * @param landPatternPad LandPatternPad
   * @return String
   *
   * @author Cheah Lee Herng
   */
  private String getLandPatternPadLongName(LandPattern landPattern, LandPatternPad landPatternPad)
  {
    Assert.expect(landPattern != null);
    Assert.expect(landPatternPad != null);

    return landPattern.getName() + "_" + landPatternPad.getName();
  }

  /**
   * @author Cheah Lee Herng
   */
  private void clearMap()
  {
    _subtypeSchemeCheckSumToSubtypeMap.clear();
    _subtypeSchemeCheckSumToLandPatternPadMap.clear();
    _subtypeSchemeCheckSumToPackagePinMap.clear();
  }


  /**
   * @return boolean
   * @author Cheah Lee Herng
   */
  private boolean isStartAssigningNewSubtype()
  {
    return _startAssignNewSubtypeToPadType;
  }

  /**
   * @param padType PadType
   * @param padTypeToSubtypeMap Map
   * @return Subtype
   * @author Cheah Lee Herng
   */
  private Subtype getSubtype(PadType padType, Map<PadType,Subtype> padTypeToSubtypeMap)
  {
    Assert.expect(padType != null);
    Assert.expect(padTypeToSubtypeMap != null);

    for (Map.Entry<PadType,Subtype> mapEntry : padTypeToSubtypeMap.entrySet())
    {
      PadType currentPadType = mapEntry.getKey();
      if (currentPadType.getName().equals(padType.getName()))
        return mapEntry.getValue();
    }

    return null;
  }

  /**
   * @param subtypeSchemes List
   * @param subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap Map
   * @return Map
   * @author Cheah Lee Herng
   */
  private void setupComponentTypeLongNameToSubtypeLongNameMap(
      List<SubtypeScheme> subtypeSchemes,
      Map<SubtypeScheme, Map<ComponentType, Map<PadType, Subtype>>> subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap,
      Map<String,String> componentTypeLongNameToSubtypeLongNameMap)
  {
    Assert.expect(subtypeSchemes != null);
    Assert.expect(subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap != null);
    Assert.expect(componentTypeLongNameToSubtypeLongNameMap != null);
    
    String componentTypeLongName = null;

    for (SubtypeScheme subtypeScheme : subtypeSchemes)
    {
      if (subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap.containsKey(subtypeScheme))
      {
        Map<ComponentType, Map<PadType, Subtype>> componentTypeToMapOfPadTypeToSubtypeMap =
            subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap.get(subtypeScheme);

        for (ComponentType componentType : subtypeScheme.getComponentTypes())
        {
          if (componentTypeToMapOfPadTypeToSubtypeMap.containsKey(componentType))
          {
            String componentTypeRefDesignator = componentType.getReferenceDesignator();
            Map<PadType, Subtype> padTypeToSubtypeMap = componentTypeToMapOfPadTypeToSubtypeMap.get(componentType);

            for (PadType padType : componentType.getTestablePadTypes())
            {
              componentTypeLongName = componentTypeRefDesignator + "_" + padType.getName();
              Subtype newSubtype = getSubtype(padType, padTypeToSubtypeMap);
              Assert.expect(newSubtype != null);

              componentTypeLongNameToSubtypeLongNameMap.put(componentTypeLongName, newSubtype.getLongName());
              componentTypeLongName = null;
              _databaseProgressObservable.incrementPercentDone();
            }
          }
          componentTypeLongName = null;
        }
      }
    }
  }

  /**
   * @param subtypeSchemes List
   * @param subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap Map
   * @return Map
   * @author Cheah Lee Herng
   */
  private void setupComponentTypeLongNameToSubtypeShortNameMap(
      List<SubtypeScheme> subtypeSchemes,
      Map<SubtypeScheme, Map<ComponentType, Map<PadType, Subtype>>> subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap,
      Map<String,String> componentTypeLongNameToSubtypeShortNameMap)
  {
    Assert.expect(subtypeSchemes != null);
    Assert.expect(subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap != null);
    Assert.expect(componentTypeLongNameToSubtypeShortNameMap != null);
    
    String componentTypeLongName = null;

    for (SubtypeScheme subtypeScheme : subtypeSchemes)
    {
      if (subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap.containsKey(subtypeScheme))
      {
        Map<ComponentType, Map<PadType, Subtype>> componentTypeToMapOfPadTypeToSubtypeMap =
            subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap.get(subtypeScheme);

        for (ComponentType componentType : subtypeScheme.getComponentTypes())
        {
          if (componentTypeToMapOfPadTypeToSubtypeMap.containsKey(componentType))
          {
            String componentTypeRefDesignator = componentType.getReferenceDesignator();
            Map<PadType, Subtype> padTypeToSubtypeMap = componentTypeToMapOfPadTypeToSubtypeMap.get(componentType);

            for (PadType padType : componentType.getTestablePadTypes())
            {
              componentTypeLongName = componentTypeRefDesignator + "_" + padType.getName();
              Subtype newSubtype = getSubtype(padType, padTypeToSubtypeMap);
              Assert.expect(newSubtype != null);

              componentTypeLongNameToSubtypeShortNameMap.put(componentTypeLongName, newSubtype.getShortName());
              componentTypeLongName = null;
              _databaseProgressObservable.incrementPercentDone();
            }
          }
          componentTypeLongName = null;
        }
      }
    }
  }
  
  /**   
   * @param panel
   * @param subtypeSchemes
   * @throws DatastoreException 
   * @author Cheah Lee Herng
   * @edited by Kee Chin Seong - Removing the old com package. 
   * @author Kee Chin Seong  - when the componentType has more than 1 component, it will crash, Pad area need to redefine.
   *                           Package Library has to be set etc.
   */
  private void setupPackageAndLandPattern(Panel panel, 
                                          List<SubtypeScheme> subtypeSchemes,
                                          Map<Integer, Pair<CompPackage, LandPattern>> libraryPackageToCompPackageLandPatternPairMap,
                                          Map<SubtypeScheme, Map<ComponentType, Map<PadType, String>>> subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap) throws DatastoreException
  {
    Assert.expect(panel != null);
    Assert.expect(subtypeSchemes != null);
    Assert.expect(libraryPackageToCompPackageLandPatternPairMap != null);
    Assert.expect(subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap != null);
    
    List<JointTypeEnum> jointTypeEnums = new ArrayList<JointTypeEnum>();
    
    for(SubtypeScheme subtypeScheme : subtypeSchemes)
    {
      if (cancelDatabaseOperation())
      {
        break;
      }
      
      // For every SubtypeScheme, clear JointTypeEnum list
      jointTypeEnums.clear();
      
      for(ComponentType componentType : subtypeScheme.getComponentTypes())
      {
        // Get assigned LibrarySubtypeScheme
        LibrarySubtypeScheme librarySubtypeScheme = componentType.getAssignLibrarySubtypeScheme();
        
        // Get associated LibraryPackage from LibrarySubtypeScheme
        List<LibraryPackage> libraryPackages = DatabaseManager.getInstance().getLibraryPackageByLibrarySubtypeSchemeId(subtypeScheme.getSelectedLibraryPath(),
                                                                                                                       librarySubtypeScheme.getLibrarySubtypeSchemeId());
        
        for(LibraryPackage libraryPackage : libraryPackages)
        {
          CompPackage compPackage = null;
          LandPattern landPattern = null;
          
          if (libraryPackageToCompPackageLandPatternPairMap.containsKey(libraryPackage.getPackageId()) == false)
          {
            String landPatternName = renameLandPatternName(panel, libraryPackage.getLandPattern().getName());
            landPattern = panel.createLandPattern(landPatternName);

            List<LibraryPackagePin> libraryPackagePins = libraryPackage.getLibraryPackagePins();

            for(LibraryPackagePin libraryPackagePin : libraryPackagePins)
            {
              LibraryLandPatternPad libraryLandPatternPad = libraryPackagePin.getLibraryLandPatternPad();

              // Create LandPatternPad
              ComponentCoordinate componentCoordinate = new ComponentCoordinate(libraryLandPatternPad.getXLocation(), libraryLandPatternPad.getYLocation());
              double rotation = libraryLandPatternPad.getDegreesRotation();
              int length = libraryLandPatternPad.getLengthInNanoMeters();
              int width = libraryLandPatternPad.getWidthInNanoMeters();
              ShapeEnum shapeEnum = libraryLandPatternPad.getShapeEnum();

              LandPatternPad landPatternPad = null;
              if (libraryLandPatternPad.isSurfaceMountPad())
              {
                landPatternPad = landPattern.createSurfaceMountLandPatternPad(componentCoordinate,
                                                                                 rotation,
                                                                                 length,
                                                                                 width,
                                                                                 shapeEnum);
              }
              else
              {
                landPatternPad = landPattern.createThroughHoleLandPatternPad(componentCoordinate,
                                                                                rotation,
                                                                                length,
                                                                                width,
                                                                                shapeEnum);

                ((ThroughHoleLandPatternPad)landPatternPad).setHoleCoordinateInNanoMeters(componentCoordinate);
//                ((ThroughHoleLandPatternPad)landPatternPad).setHoleDiameterInNanoMeters(libraryLandPatternPad.getHoleDiameter());
                //Siew Yeng - XCR-3318 - Oval PTH
                ((ThroughHoleLandPatternPad)landPatternPad).setHoleWidthInNanoMeters(libraryLandPatternPad.getHoleWidth());
                ((ThroughHoleLandPatternPad)landPatternPad).setHoleLengthInNanoMeters(libraryLandPatternPad.getHoleLength());
              }
              
              // XCR-3118 Unable to load recipe after import alignment point package
              // Only assign Library LandPatternPad name if it is not match with default LandPatternPad name
              if (landPatternPad.getName().equalsIgnoreCase(libraryLandPatternPad.getPadName()) == false)
                landPatternPad.setName(libraryLandPatternPad.getPadName());
              
              landPattern.addLandPatternPad(landPatternPad);
              jointTypeEnums.add(JointTypeEnum.getJointTypeEnum(libraryPackagePin.getLibrarySubtype().getJointTypeName()));

              _databaseProgressObservable.incrementPercentDone();
            }

            // Sanity check
            Assert.expect(jointTypeEnums.size() == landPattern.getLandPatternPads().size());

            // Create CompPackage
            JointTypeEnum compPackageJointTypeEnum = jointTypeEnums.get(0);
            String compPackageShortName  = landPattern.getName();
            String compPackageLongName   = panel.getUniqueCompPackageLongName(landPattern, compPackageJointTypeEnum);

            compPackage = new CompPackage();
            compPackage.setPanel(panel);
            compPackage.setLongName(compPackageLongName);
            compPackage.setShortName(compPackageShortName);
            compPackage.setLandPattern(landPattern);
            
            // Backup PadType original Subtype name before being overide. This is useful 
            // if user wants to retain original subtype name instead of library subtype name.
            backupSubtypeName(subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap, subtypeScheme, componentType);
            
            // Remove existing PadType from ComponentType
            removePadTypeFromComponentType(componentType);
            
            // Remove ComponentType from LandPattern
            LandPattern projectLandPattern = componentType.getLandPattern();
            projectLandPattern.removeComponentType(componentType);            
            
            // Process PackagePin and PadType
            processPackagePinAndPadType(landPattern, componentType, compPackage, jointTypeEnums, libraryPackagePins, true);

            // Remove ComponentType from old CompPackage
            CompPackage oldCompPackage = componentType.getCompPackage();
            oldCompPackage.removeComponentType(componentType); 
               
            // Setup new LandPattern and new CompPackage on ComponentType
            componentType.setLandPattern(landPattern);
            componentType.setCompPackage(compPackage);
            
            // XCR-3076 Software Crash when try to delete old land pattern after import land pattern (in library)
            // Add ComponentType into newly created LandPattern
            landPattern.addComponentType(componentType);
            
            // XCR-3112 Assert when edit land pattern and edit subtype package during import
            compPackage.addComponentType(componentType);
            
            //Removing old package
            removeUnusedCompPackage(panel, oldCompPackage);
            
            // XCR-3075 Software crash when import data from library by changing the package matching
            componentType.getCompPackage().setImported(true);
            componentType.getCompPackage().setSourceLibrary(subtypeScheme.getSelectedLibraryPath());
            
            if (subtypeScheme.getCompPackage().getSubtypes().isEmpty() == false)
              subtypeScheme.getCompPackage().saveImportState();
            
            // Remove unused LandPattern
            if (projectLandPattern.isInUse() == false)
              projectLandPattern.destroy();
            
            if (libraryPackageToCompPackageLandPatternPairMap.containsKey(libraryPackage.getPackageId()) == false)
               libraryPackageToCompPackageLandPatternPairMap.put(libraryPackage.getPackageId(), new Pair<CompPackage,LandPattern>(compPackage,landPattern));                        
          }
          else
          {
            //Kee Chin Seong - this part is to handle those component Type where there is more than one component inside component type.
            Pair<CompPackage, LandPattern> compPackageLandPatternPair = libraryPackageToCompPackageLandPatternPairMap.get(libraryPackage.getPackageId());
            
            compPackage = compPackageLandPatternPair.getFirst();
            landPattern = compPackageLandPatternPair.getSecond();
            
            //Retrieve the package pins
            List<LibraryPackagePin> libraryPackagePins = libraryPackage.getLibraryPackagePins();
            
            for(LibraryPackagePin libraryPackagePin : libraryPackagePins)
            {
              //if the Library pin not equals to jointTypeEnum means there is jointType has been cleared, add in back for creat
              if(jointTypeEnums.size() != landPattern.getLandPatternPads().size())
                jointTypeEnums.add(JointTypeEnum.getJointTypeEnum(libraryPackagePin.getLibrarySubtype().getJointTypeName()));

              _databaseProgressObservable.incrementPercentDone();
            }
            // Sanity check
            Assert.expect(jointTypeEnums.size() == landPattern.getLandPatternPads().size());

            // Backup PadType original Subtype name before being overide. This is useful 
            // if user wants to retain original subtype name instead of library subtype name.
            backupSubtypeName(subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap, subtypeScheme, componentType);
            
            // Remove existing PadType from ComponentType
            removePadTypeFromComponentType(componentType);
            
            // Process PackagePin and PadType
            processPackagePinAndPadType(landPattern, componentType, compPackage, jointTypeEnums, libraryPackagePins, false);
            
            CompPackage oldCompPackage = componentType.getCompPackage();
            oldCompPackage.removeComponentType(componentType);
            
            // Remove ComponentType from LandPattern
            LandPattern projectLandPattern = componentType.getLandPattern();
            projectLandPattern.removeComponentType(componentType);
            
            //setting back landPattern and compPackager
            componentType.setLandPattern(landPattern);
            componentType.setCompPackage(compPackage);
            
            // XCR-3076 Software Crash when try to delete old land pattern after import land pattern (in library)
            // Add ComponentType into newly created LandPattern
            landPattern.addComponentType(componentType);
            
            // XCR-3112 Assert when edit land pattern and edit subtype package during import
            compPackage.addComponentType(componentType);
            
            //Removing old package
            removeUnusedCompPackage(panel, oldCompPackage);
          }
        }
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void setupSubtype(List<SubtypeScheme> subtypeSchemes,
                            Map<SubtypeScheme, List<Subtype>> unselectedSubtypeSchemeToSubtypesMap,
                            Panel panel,
                            ImportLibrarySettingPersistance importLibrarySettingPersistance,
                            Map<SubtypeScheme, Map<ComponentType,Map<PadType, Subtype>>> subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap,
                            Map<String, Subtype> subtypeLongNameToSubtypeMap,
                            Map<SubtypeScheme, Map<ComponentType, Map<PadType, String>>> subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap,
                            Map<String,String> componentTypeLongNameToSubtypeLongNameMap,
                            Map<String,String> componentTypeLongNameToSubtypeShortNameMap) throws DatastoreException
  {
    Assert.expect(subtypeSchemes != null);
    Assert.expect(unselectedSubtypeSchemeToSubtypesMap != null);
    Assert.expect(panel != null);
    Assert.expect(importLibrarySettingPersistance != null);
    Assert.expect(subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap != null);
    Assert.expect(subtypeLongNameToSubtypeMap != null);
    Assert.expect(subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap != null);
    Assert.expect(componentTypeLongNameToSubtypeLongNameMap != null);
    Assert.expect(componentTypeLongNameToSubtypeShortNameMap != null);
    
    // Get map of project subtype scheme to map of component type to map of pad type to subtype map
    createSubtype(subtypeSchemes,
                  unselectedSubtypeSchemeToSubtypesMap,
                  panel,
                  importLibrarySettingPersistance,
                  subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap,
                  subtypeLongNameToSubtypeMap,
                  subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap);

    if (cancelDatabaseOperation())
    {
      unselectedSubtypeSchemeToSubtypesMap.clear();
      subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap.clear();
      subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap.clear();
      componentTypeLongNameToSubtypeLongNameMap.clear();
      componentTypeLongNameToSubtypeShortNameMap.clear();
      subtypeLongNameToSubtypeMap.clear();
      return;
    }

    setupComponentTypeLongNameToSubtypeLongNameMap(subtypeSchemes,
                                                   subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap,
                                                   componentTypeLongNameToSubtypeLongNameMap);
    setupComponentTypeLongNameToSubtypeShortNameMap(subtypeSchemes,
                                                   subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap,
                                                   componentTypeLongNameToSubtypeShortNameMap);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void assignSubtype(List<SubtypeScheme> subtypeSchemes,
                             Map<SubtypeScheme, List<Subtype>> unselectedSubtypeSchemeToSubtypesMap,
                             Panel panel,
                             ImportLibrarySettingPersistance importLibrarySettingPersistance,
                             Map<SubtypeScheme, Map<ComponentType,Map<PadType, Subtype>>> subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap,
                             Map<String, Subtype> subtypeLongNameToSubtypeMap,
                             Map<SubtypeScheme, Map<ComponentType, Map<PadType, String>>> subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap,
                             Map<String,String> componentTypeLongNameToSubtypeLongNameMap,
                             Map<String,String> componentTypeLongNameToSubtypeShortNameMap)
  {
    Assert.expect(subtypeSchemes != null);
    Assert.expect(unselectedSubtypeSchemeToSubtypesMap != null);
    Assert.expect(panel != null);
    Assert.expect(importLibrarySettingPersistance != null);
    Assert.expect(subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap != null);
    Assert.expect(subtypeLongNameToSubtypeMap != null);
    Assert.expect(subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap != null);
    Assert.expect(componentTypeLongNameToSubtypeLongNameMap != null);
    Assert.expect(componentTypeLongNameToSubtypeShortNameMap != null);
    
    for (SubtypeScheme subtypeScheme : subtypeSchemes)
    {
      if (cancelDatabaseOperation())
      {
        unselectedSubtypeSchemeToSubtypesMap.clear();
        subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap.clear();
        subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap.clear();
        componentTypeLongNameToSubtypeLongNameMap.clear();
        componentTypeLongNameToSubtypeShortNameMap.clear();
        subtypeLongNameToSubtypeMap.clear();
        return;
      }

      if (subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap.containsKey(subtypeScheme))
      {
        Map<ComponentType, Map<PadType, Subtype>> componentTypeToMapOfPadTypeToSubtypeMap =
            subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToSubtypeMap.get(subtypeScheme);

        for (ComponentType componentType : subtypeScheme.getComponentTypes())
        {
          if (componentTypeToMapOfPadTypeToSubtypeMap.containsKey(componentType))
          {
            // Assign new subtype
            setSubtypeByComponentTypeLongName(componentType.getTestablePadTypes(),
                                              componentTypeLongNameToSubtypeLongNameMap,
                                              componentTypeLongNameToSubtypeShortNameMap,
                                              componentType,
                                              panel);
            _startAssignNewSubtypeToPadType = true;
          }
        }
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void backupSubtypeName(Map<SubtypeScheme, Map<ComponentType, Map<PadType, String>>> subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap,
                                 SubtypeScheme subtypeScheme,
                                 ComponentType componentType)
  {
    Assert.expect(subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap != null);
    Assert.expect(subtypeScheme != null);
    Assert.expect(componentType != null);
    
    // Retain original Subtype name for each PadType for later re-usable
    if (subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap.containsKey(subtypeScheme) == false)
    {
      subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap.put(subtypeScheme, new HashMap<ComponentType, Map<PadType, String>>());
    }

    Map<ComponentType, Map<PadType, String>> componentTypeToMapOfPadTypeToOriginalSubtypeNameMap = subtypeSchemeToMapOfComponentTypeToMapOfPadTypeToOriginalSubtypeNameMap.get(subtypeScheme);
    
    for(PadType padType : componentType.getPadTypes())
    {
      // Keep a record for original Subtype
      if (componentTypeToMapOfPadTypeToOriginalSubtypeNameMap.containsKey(componentType) == false)
        componentTypeToMapOfPadTypeToOriginalSubtypeNameMap.put(componentType, new HashMap<PadType, String>());

      Map<PadType, String> padTypeToOriginalSubtypeNameMap = componentTypeToMapOfPadTypeToOriginalSubtypeNameMap.get(componentType);
      if (padTypeToOriginalSubtypeNameMap.containsKey(padType) == false)
        padTypeToOriginalSubtypeNameMap.put(padType, padType.getPadTypeSettings().getSubtype().getShortName());
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void removePadTypeFromComponentType(ComponentType componentType)
  {    
    Assert.expect(componentType != null);

    // Remove existing PadType from CompPackage
    for(PadType padType : componentType.getPadTypes())
    {
      // Remove PadType from ComponentType
      componentType.removePadType(padType);

      for(Component component : componentType.getComponents())
      {
        for(Pad pad : padType.getPads())
        {
          if (pad.getComponent().equals(component) && 
              component.hasPad(pad.getName()))
          {
            component.removePad(pad);
            break;
          }
        }
      }
    }
  }

  /**
   * XCR-3112 Assert when edit land pattern and edit subtype package during import
   * @author Cheah Lee Herng 
   */
  private void removeUnusedCompPackage(Panel panel, CompPackage compPackage)
  {
    Assert.expect(panel != null);
    Assert.expect(compPackage != null);
    
    List<CompPackage> unusedCompPackages = panel.getUnusedCompPackage();
    if (unusedCompPackages.contains(compPackage))
    {
      compPackage.destroy();
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void processPackagePinAndPadType(LandPattern landPattern, 
                                           ComponentType componentType, 
                                           CompPackage compPackage,
                                           List<JointTypeEnum> jointTypeEnums,
                                           List<LibraryPackagePin> libraryPackagePins,
                                           boolean createPackagePin)
  {
    Assert.expect(landPattern != null);
    Assert.expect(componentType != null);
    Assert.expect(compPackage != null);
    Assert.expect(libraryPackagePins != null);
    Assert.expect(jointTypeEnums != null);    
    
    int counter = 0;
    for(LandPatternPad landPatternPad : landPattern.getLandPatternPads())
    {
      int currentCounter = counter++;

      PackagePin packagePin = null;
      if (createPackagePin)
      {
        // Create PackagePin
        packagePin = new PackagePin();
        packagePin.setLandPatternPad(landPatternPad);
        packagePin.setCompPackage(compPackage);
        packagePin.setJointTypeEnum(jointTypeEnums.get(currentCounter));

        compPackage.addPackagePin(packagePin);
      }
      else
      {
        // Get current PackagePin
        packagePin = compPackage.getPackagePins().get(currentCounter);
      }
      Assert.expect(packagePin != null);

      // Create PadType
      PadType padType = new PadType();
      PadTypeSettings padTypeSettings = new PadTypeSettings();
      padTypeSettings.setInspected(true);
      padTypeSettings.setPadType(padType);

      padType.setComponentType(componentType);
      padType.setLandPatternPad(landPatternPad);
      padType.setPackagePin(packagePin);
      padType.setPadTypeSettings(padTypeSettings);

      componentType.addPadType(padType);

      // Update PadType to correct orientation
      LibraryPackagePin libraryPackagePin = libraryPackagePins.get(currentCounter);
      PinOrientationEnum pinOrientationEnum = libraryPackagePin.getPinOrientationEnum(); 
      JointTypeEnum jointTypeEnum = packagePin.getJointTypeEnum();

      if ((JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.COLLAPSABLE_BGA) ||
           (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR)) ||
           (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.NON_COLLAPSABLE_BGA)) ||
           (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.CGA)) ||
           (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.PRESSFIT)) ||
           (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.CHIP_SCALE_PACKAGE)) ||
           (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.THROUGH_HOLE)) ||
           (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.OVAL_THROUGH_HOLE)) || //Siew Yeng - XCR-3318 - Oval PTH
           (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.SINGLE_PAD)) ||
           (JointTypeEnum.getJointTypeEnum(jointTypeEnum.getName()).equals(JointTypeEnum.LGA))))
      {
        pinOrientationEnum = PinOrientationEnum.NOT_APPLICABLE;
      }            
      padType.getPackagePin().setPinOrientationEnum(pinOrientationEnum);

      for(Component component : componentType.getComponents())
      {
        // Create Pad
        Pad pad = new Pad();
        pad.setNodeName("");
        PadSettings padSettings = new PadSettings();
        padSettings.setTestable(true);
        pad.setPadSettings(padSettings);
        padSettings.setPad(pad);
        pad.setPadType(padType); 
        padType.addPad(pad);

        // Add Pad into Component
        component.addPad(pad);
      }

      _databaseProgressObservable.incrementPercentDone();
    }
  }
  
  /**
   * Update library if the database is not latest version
   * @author Siew Yeng
   */
  public void updateLibraryIfNecessary(List<String> libraryPath) throws DatastoreException
  {
    DatabaseManager.getInstance().updateLibraryIfNecessary(libraryPath);
  }
}
