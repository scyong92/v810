package com.axi.v810.business.packageLibrary;

import java.io.*;

import com.axi.util.*;
import java.util.Map;
import java.util.HashMap;
import com.axi.v810.util.StringLocalizer;

/**
 * An enumeration to define the search option for Package Library.
 * @author Poh Kheng
 */
public class SearchOptionEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;
  private static Map<String, SearchOptionEnum> _nameToSearchOptionEnumMap = new HashMap<String, SearchOptionEnum>();
  public final static SearchOptionEnum START_WITH = new SearchOptionEnum(++_index, StringLocalizer.keyToString("PLWK_START_WITH_KEY"));
  public final static SearchOptionEnum END_WITH = new SearchOptionEnum(++_index, StringLocalizer.keyToString("PLWK_END_WITH_KEY"));
  public final static SearchOptionEnum CONTAINS = new SearchOptionEnum(++_index, StringLocalizer.keyToString("PLWK_CONTAINS_KEY"));

  private String _name;

  /**
   * @author Poh Kheng
   */
  private SearchOptionEnum(int id, String name)
  {
    super(id);
    _name = name.intern();
    _nameToSearchOptionEnumMap.put(_name, this);

  }

  /**
   * @author Poh Kheng
   */
  public String getName()
  {
    return _name;
  }

  /**
   * This would return the search option enum based on the enum name
   *
   * @author Poh Kheng
   */
  public static SearchOptionEnum getSearchOptionEnum(String shapeName)
  {
    final SearchOptionEnum SearchOptionEnum = _nameToSearchOptionEnumMap.get(shapeName);
    Assert.expect(SearchOptionEnum != null);
    return SearchOptionEnum;
  }

  /**
   *
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String toString()
  {
    return _name;
  }
}
