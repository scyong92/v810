package com.axi.v810.business.packageLibrary;

import java.util.*;
import java.util.zip.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Wei Chin, Chong
 */
public class SubtypeScheme
{
  int _hashcode = -1;
  long _checkSum = -1;

  private CompPackage _compPackage = null;

  private String _projectName = null;
  // libraryPath = library to export
  private String _libraryPath= null;
  private String _landPatternName = null;

  private int _numOfLandPatternPads = -1;

  private List<PadType> _padTypes = null;
  private List<ComponentType> _componentTypes = null;

  private boolean _usesOneSubtype = true;
  private LandPattern _landPattern = null;

  private boolean _updatable = false;
  private String _padJointTypeNames = null;
  private String _concateForCalculationString = null;
  public static final int _COUNT_NUMBER_OF_PAD_DATA = 6;
  public static final int _X_COORDINATE_INDEX_PAD_DATA = 0;
  public static final int _Y_COORDINATE_INDEX_PAD_DATA = 1;
  public static final int _LENGTH_INDEX_PAD_DATA = 2;
  public static final int _WIDTH_INDEX_PAD_DATA = 3;
  public static final int _DEGREE_ROTATION_INDEX_PAD_DATA = 4;
  public static final int _SHAPE_INDEX_PAD_DATA = 5;

  private SubtypeSchemeObservable _subtypeSchemeObservable = SubtypeSchemeObservable.getInstance();

  private Map<String, List<LibraryPackage>> _libraryPathToLibraryPackagesMap = new LinkedHashMap<String,List<LibraryPackage>>();

  private Map <String, String> _subtypeNameToNewSubtypeName = new HashMap<String,String>();

  private String _selectedLibraryPath = null;
  private LibraryPackage _selectedLibraryPackage = null;

  private Map<String,Boolean> _padNameToInspectableMap = new HashMap<String,Boolean>();
  private Map<String,Boolean> _padNameToTestableMap = new HashMap<String,Boolean>();


  /**
   * @param componentType ComponentType
   * @author Wei Chin, Chong
   */
  public SubtypeScheme(ComponentType componentType)
  {
    Assert.expect(componentType != null);

    _compPackage = componentType.getCompPackage();

    _componentTypes = new ArrayList<ComponentType>();
    _componentTypes.add(componentType);

    _projectName = componentType.getLandPattern().getPanel().getProject().getName();
    _landPatternName = componentType.getLandPattern().getName();
    _numOfLandPatternPads = componentType.getLandPattern().getLandPatternPads().size();
    _usesOneSubtype = componentType.getComponentTypeSettings().usesOneSubtype();
    _landPattern = componentType.getLandPattern();

    // sourcelibraryPath = imported library
    if (componentType.getCompPackage().hasLibraryPath())
    {
      setLibraryPath(componentType.getCompPackage().getLibraryPath());
    }

    _padTypes = componentType.getPadTypes();
    List<LandPatternPad> landPatternPads = componentType.getLandPattern().getLandPatternPads();

    _padJointTypeNames = "";
    StringBuilder xCoordinate = new StringBuilder();
    StringBuilder yCoordinate = new StringBuilder();
    StringBuilder length = new StringBuilder();
    StringBuilder width  = new StringBuilder();
    StringBuilder rotationDeegree  = new StringBuilder();
    StringBuilder shape = new StringBuilder();
    for(PadType padType : _padTypes)
    {

      // Commented out by LeeHerng 30/03/2010 - There are cases where total number of pad types does not
      // equal to the total number of land pattern pads due to some reason.
      /*
      if(xCoordinate.toString().length() == 0)
        xCoordinate.append(padType.getLandPatternPad().getCoordinateInNanoMeters().getX());
      else
        xCoordinate.append("," + padType.getLandPatternPad().getCoordinateInNanoMeters().getX());

      yCoordinate.append("," + padType.getLandPatternPad().getCoordinateInNanoMeters().getY());
      length.append("," + padType.getLandPatternPad().getLengthInNanoMeters());
      width.append("," + padType.getLandPatternPad().getWidthInNanoMeters());
      rotationDeegree.append("," + Math.round(padType.getDegreesRotation()));
      shape.append("," + padType.getShapeEnum().getName());
      */
      _padJointTypeNames = _padJointTypeNames + padType.getJointTypeEnum().getName() + ",";

      _padNameToInspectableMap.put(padType.getName(), padType.isInspected());
      
      for(Pad pad : padType.getPads())
        _padNameToTestableMap.put(padType.getName(), pad.isTestable());
    }

    for(LandPatternPad landPatternPad : landPatternPads)
    {
      if(xCoordinate.toString().length() == 0)
        xCoordinate.append(landPatternPad.getCoordinateInNanoMeters().getX());
      else
        xCoordinate.append("," + landPatternPad.getCoordinateInNanoMeters().getX());

      yCoordinate.append("," + landPatternPad.getCoordinateInNanoMeters().getY());
      length.append("," + landPatternPad.getLengthInNanoMeters());
      width.append("," + landPatternPad.getWidthInNanoMeters());
      rotationDeegree.append("," + Math.round(landPatternPad.getDegreesRotation()));
      shape.append("," + landPatternPad.getShapeEnum().getName());
    }

    _concateForCalculationString = xCoordinate.toString() + yCoordinate.toString() + length.toString()
                                   + width.toString() + rotationDeegree.toString() + shape.toString();

    generateCheckSum();
  }

  /**
   * @return List
   * @author Wei Chin, Chong
   */
  public List<PadType> getPadTypes()
  {
    Assert.expect(_padTypes != null);

    return _padTypes;
  }

  /**
   * @return List
   * @author Wei Chin, Chong
   */
  public List<Subtype> getSubtypes()
  {
    Assert.expect(_padTypes != null);
    Assert.expect(_padTypes.size() > 0);

    Set<Subtype> subtypeSet = new HashSet<Subtype>();
    for(PadType padType : _padTypes)
    {
      subtypeSet.add(padType.getSubtype());
    }
    return new ArrayList<Subtype>(subtypeSet);
  }

  /**
   * @author Kee Chin Seong
   */
  public boolean doesSubtypeHasIncompatibleInspectionFamily()
  {
    Assert.expect(_padTypes != null);
    Assert.expect(_padTypes.size() > 0);

    for(PadType padType : _padTypes)
    {
      if(padType.doesSubtypeHasIncompatibleInspectionFamily())
         return true;
    }
    return false;
  }
  
  /**
   * @return the Subtype that all PadTypes share for this SubtypeScheme.  Call
   * usesSubtype() first to be sure that only one Subtype is used.
   *
   * @author Wei Chin, Chong
   */
  public Subtype getSubtype()
  {
    Assert.expect(_padTypes != null);

    // make sure that there is really only one subtype
    Assert.expect(usesOneSubtype());

    Assert.expect(_padTypes.size() > 0);
    PadType padType = (PadType)_padTypes.get(0);

    return padType.getSubtype();
  }

  /**
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean usesOneSubtype()
  {
    return _usesOneSubtype;
  }

  /**
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean usesOneJointTypeEnum()
  {
    Assert.expect(_compPackage != null);
    return _compPackage.usesOneJointTypeEnum();
  }

  /**
   * @return CompPackage
   * @author Wei Chin, Chong
   */
  public CompPackage getCompPackage()
  {
    Assert.expect(_compPackage != null);
    return _compPackage;
  }

  /**
   * @return String
   * @author Wei Chin, Chong
   */
  public String getPackageShortName()
  {
    Assert.expect(_compPackage != null);
    return _compPackage.getShortName();
  }

  /**
   * @return String
   * @author Cheah Lee Herng
   */
  public String getPackageLongName()
  {
    Assert.expect(_compPackage != null);
    return _compPackage.getLongName();
  }

  /**
   * Gets the joint type (only works if there is ONE joint type for everything.
   *
   * @return JointTypeEnum
   * @author Wei Chin, Chong
   **/
  public JointTypeEnum getJointTypeEnum()
  {
    Assert.expect(_padTypes != null);
    Assert.expect(_padTypes.size() > 0);
    Assert.expect(usesOneJointTypeEnum());

    return  _padTypes.get(0).getJointTypeEnum();
  }

  /**
   * Gets the all joint types
   *
   * @return JointTypeEnums
   * @author Wei Chin, Chong
   **/
  public List<JointTypeEnum> getJointTypeEnums()
  {
    Assert.expect(_padTypes != null);
    Assert.expect(_padTypes.size() > 0);

//    CRC32 jointTypesChecksum = new CRC32();

    Set<JointTypeEnum> jointTypeEnumSet = new HashSet<JointTypeEnum>();
    for(PadType padType : _padTypes)
    {
      jointTypeEnumSet.add(padType.getJointTypeEnum());
//      jointTypesChecksum.update(padType.getJointTypeEnum().getName().getBytes());
    }
    /** @todo gad do we have to update this every time? can we do this in the constructor? */
//    _padJointTypeCheckSum = jointTypesChecksum.getValue();
    return new ArrayList<JointTypeEnum>(jointTypeEnumSet);
  }

  /**
   * @return String
   * @author Wei Chin, Chong
   */
  public String getProjectName()
  {
    Assert.expect(_projectName != null);
    return _projectName;
  }

  /**
   * @return int
   * @author Wei Chin, Chong
   */
  public int hashCode()
  {
    return _hashcode;
  }

  /**
   * @param object Object
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean equals(Object object)
  {
    if((object instanceof SubtypeScheme) == false)
      return false;

    if(this == object)
      return true;

    SubtypeScheme subtypeScheme = (SubtypeScheme) object;

    // XCR-3110 Assert when import library with subtype package change to HDR2
    if (subtypeScheme.hashCode() == hashCode() &&
        subtypeScheme.getCompPackage().getLongName().equalsIgnoreCase(getCompPackage().getLongName()))
      return true;
    else
      return false;
  }

  /**
   * @param libraryPath String
   * @author Wei Chin, Chong
   */
  public void setLibraryPath(String libraryPath)
  {
    Assert.expect(libraryPath != null);

    _libraryPath = libraryPath;
  }

  /**
   *
   * @return String
   *
   * @author Wei Chin, Chong
   */
  public String getLibraryPath()
  {
    Assert.expect(_libraryPath != null);

    return _libraryPath;
  }

  /**
   * @return String
   * @author Wei Chin, Chong
   */
  public String getLandPatternName()
  {
    Assert.expect(_landPatternName != null);
    return _landPatternName;
  }

  /**
   * @return int
   * @author Wei Chin, Chong
   */
  public int getNumOfLandPatternPads()
  {
    Assert.expect(_numOfLandPatternPads != -1);
    return _numOfLandPatternPads;
  }

  /**
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean isUpdatable()
  {
//    if( hasNewSubTypeName() )
//      return false;
    return _updatable;
  }

  /**
   * @param updatable boolean
   * @author Wei Chin, Chong
   */
  public void setUpdatable(boolean updatable)
  {
    _updatable = updatable;
  }

  /**
   * @return LandPattern
   * @author Wei Chin, Chong
   */
  public LandPattern getLandPattern()
  {
    Assert.expect(_landPattern != null);
    return _landPattern;
  }

  /**
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean hasNewSubTypeName()
  {
    return _subtypeNameToNewSubtypeName != null && _subtypeNameToNewSubtypeName.keySet().size() > 0;
  }

  /**
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean hasNewSubTypeName(Subtype subtype)
  {
    return _subtypeNameToNewSubtypeName.containsKey(subtype.getShortName());
  }

  /**
   * @param newSubtypeName String
   * @author Wei Chin, Chong
   */
  public void setNewSubtypeName(String newSubtypeName)
  {
    Assert.expect(newSubtypeName != null);
    Assert.expect(usesOneSubtype());

    _subtypeSchemeObservable.setEnabled(false);
    String originalSubtypeName = getSubtype().getShortName();
    String oldName = _subtypeNameToNewSubtypeName.get(originalSubtypeName);
    try
    {
      if (originalSubtypeName.equals(newSubtypeName))
        _subtypeNameToNewSubtypeName.remove(originalSubtypeName);
      else
        _subtypeNameToNewSubtypeName.put(originalSubtypeName, newSubtypeName);
    }
    finally
    {
      _subtypeSchemeObservable.setEnabled(true);
    }
    _subtypeSchemeObservable.stateChanged(this, oldName, newSubtypeName, SubtypeSchemeEventEnum.SUBTYPE_NAME);

    // re-generate the new checkSum
    generateCheckSum();
  }

  /**
   * @param newSubtypeName String
   * @author Wei Chin, Chong
   */
  public void setNewSubtypeName(String newSubtypeName, Subtype subtype)
  {
    Assert.expect(newSubtypeName != null);
    Assert.expect(subtype != null);

    _subtypeSchemeObservable.setEnabled(false);
    String originalSubtypeName = subtype.getShortName();
    String oldName = _subtypeNameToNewSubtypeName.get(originalSubtypeName);
    try
    {
      if (originalSubtypeName.equals(newSubtypeName))
        _subtypeNameToNewSubtypeName.remove(originalSubtypeName);
      else
        _subtypeNameToNewSubtypeName.put(originalSubtypeName, newSubtypeName);
    }
    finally
    {
      _subtypeSchemeObservable.setEnabled(true);
    }
    _subtypeSchemeObservable.stateChanged(this, oldName, newSubtypeName, SubtypeSchemeEventEnum.SUBTYPE_NAME);

    // re-generate the new checkSum
    generateCheckSum();
  }


  /**
   * @return String
   * @author Wei Chin, Chong
   */
  public String getNewSubtypeName()
  {
    Assert.expect(_subtypeNameToNewSubtypeName.keySet().isEmpty() == false);
    Assert.expect(usesOneSubtype());

    return _subtypeNameToNewSubtypeName.get(getSubtype().getShortName());
  }

  /**
   * @return String
   * @author Wei Chin, Chong
   */
  public String getNewSubtypeName(Subtype subtype)
  {
    Assert.expect(_subtypeNameToNewSubtypeName.keySet().isEmpty() == false);
    Assert.expect(subtype != null);

    return _subtypeNameToNewSubtypeName.get(subtype.getShortName());
  }

  /**
   * @return long
   * @author Wei Chin, Chong
   */
  public long getCheckSum()
  {
    Assert.expect(_checkSum != -1);

    return _checkSum;
  }

  /**
   * @author Wei Chin, Chong
   */
  private void generateCheckSum()
  {
    Assert.expect(_landPatternName != null);
    Assert.expect(_padTypes != null);

    StringBuilder checkSumString = new StringBuilder();
    checkSumString.append(_numOfLandPatternPads);

    CRC32 checksumUtil = new CRC32();
    for (PadType padType : _padTypes)
    {
      if(hasNewSubTypeName())
        checkSumString.append("," + _subtypeNameToNewSubtypeName.get(padType.getSubtype().getShortName()));
      else
        checkSumString.append("," + padType.getSubtype().getShortName());
      checkSumString.append("," + padType.getJointTypeEnum().getName());
    }
    checksumUtil.reset();
    checksumUtil.update(checkSumString.toString().getBytes());
    _checkSum = checksumUtil.getValue();
    _hashcode = checkSumString.toString().hashCode();
  }

  /**
   * This would return the pad joint type names
   *
   * @return long
   * @author Poh Kheng
   */
  public String getPadJointTypeNames()
  {
    Assert.expect(_padJointTypeNames != null);
    return _padJointTypeNames;
  }

  /**
   * This would return concate string which consist of pad information.
   * This field is create to minimize the access to landpattern pad table when user do a Refine Match search.
   * It is comma separator
   *
   * @return String
   * @author Poh Kheng
   */
  public String getConcateForCalculationString()
  {
    Assert.expect(_concateForCalculationString != null);
    return _concateForCalculationString;
  }

  /**
   * @param componentType ComponentType
   * @author Wei Chin, Chong
   */
  public void addComponentType(ComponentType componentType)
  {
    Assert.expect(componentType != null);
    Assert.expect(_componentTypes != null);

    _componentTypes.add(componentType);
  }

  /**
   * @return List<ComponentType>
   * @author Wei Chin, Chong
   */
  public List<ComponentType> getComponentTypes()
  {
    Assert.expect(_componentTypes != null);
    return _componentTypes;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasComponentTypes()
  {
    Assert.expect(_componentTypes != null);
    
    if (_componentTypes.isEmpty())
      return false;
    else
      return true;
  }

  /**
   *
   * @param libraryPath String
   * @param libraryPackage LibraryPackage
   *
   * @author Cheah Lee Herng
   */
  public void addLibraryPathToLibraryPackageMap(String libraryPath, LibraryPackage libraryPackage)
  {
    Assert.expect(libraryPackage != null);
    Assert.expect(_libraryPathToLibraryPackagesMap != null);

    if (_libraryPathToLibraryPackagesMap.containsKey(libraryPath))
    {
      List<LibraryPackage> libraryPackages = _libraryPathToLibraryPackagesMap.get(libraryPath);
      boolean exist = false;
      for(LibraryPackage libraryPackageTmp : libraryPackages)
      {
        if(libraryPackageTmp.getPackageId() == libraryPackage.getPackageId())
        {
          exist = true;
        }
      }

      if(exist == false)
        _libraryPathToLibraryPackagesMap.get(libraryPath).add(libraryPackage);
    }
    else
    {
      List<LibraryPackage> libraryPackages = new LinkedList<LibraryPackage>();
      libraryPackages.add(libraryPackage);
      _libraryPathToLibraryPackagesMap.put(libraryPath, libraryPackages);
    }
  }

  /**
   *
   * @param selectedLibraryPath String
   *
   * @author Tan Hock Zoon
   */
  public void setSelectedLibraryPath(String selectedLibraryPath)
  {
    Assert.expect(selectedLibraryPath != null);
    _selectedLibraryPath = selectedLibraryPath;
  }

  /**
   *
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String getSelectedLibraryPath()
  {
    Assert.expect(_selectedLibraryPath != null);
    return _selectedLibraryPath;
  }

  /**
   *
   * @return boolean
   *
   * @author Tan Hock Zoon
   */
  public boolean hasSelectedLibraryPath()
  {
    return (_selectedLibraryPath != null && _selectedLibraryPath.length() > 0);
  }

  /**
   *
   * @return List
   *
   * @author Tan Hock Zoon
   */
  public List<String> getLibraryPathList()
  {
    List<String> libraryPathList = new ArrayList<String>();

    Iterator<String> it = _libraryPathToLibraryPackagesMap.keySet().iterator();

    while (it.hasNext())
    {
      libraryPathList.add(it.next());
    }

    return libraryPathList;
  }

  /**
   * @author Tan Hock Zoon
   */
  public void clearLibraryPath()
  {
    _selectedLibraryPath = null;
  }

  /**
   *
   * @return List
   *
   * @author Tan Hock Zoon
   */
  public List<LibraryPackage> getLibraryPackageList()
  {
    Assert.expect(_selectedLibraryPath != null);
    Assert.expect(_libraryPathToLibraryPackagesMap != null);
    return _libraryPathToLibraryPackagesMap.get(_selectedLibraryPath);
  }

  /**
   *
   * @return LibraryPackage
   *
   * @author Tan Hock Zoon
   */
  public LibraryPackage getSelectedLibraryPackage()
  {
    Assert.expect(_selectedLibraryPackage != null);

    return _selectedLibraryPackage;
  }

  /**
   *
   * @param selectedLibraryPackage LibraryPackage
   *
   * @author Tan Hock Zoon
   */
  public void setSelectedLibraryPackage(LibraryPackage selectedLibraryPackage)
  {
    _selectedLibraryPackage = selectedLibraryPackage;
  }

  /**
   * This would return the library package in a particulater library path to be reuse.
   *
   * @author Poh Kheng
   */
  public LibraryPackage getExistingLibraryPackage(String libraryPath, String libraryPackageLongName)
  {
    Assert.expect(libraryPath != null);
    Assert.expect(libraryPackageLongName != null);
    Assert.expect(_libraryPathToLibraryPackagesMap != null);

    for (LibraryPackage libraryPackage : _libraryPathToLibraryPackagesMap.get(libraryPath))
    {
      if (libraryPackage.getPackageLongName().equals(libraryPackageLongName))
      {
        return libraryPackage;
      }
    }

    // we should not reach this stage
    Assert.expect(false);
    return null;
  }

  /**
   * This would check if this library package for this library path already exist
   *
   * @author Poh Kheng
   */
  public boolean isLibraryPackageInLibraryPathMap(String libraryPath, String libraryPackageLongName, LibraryLandPattern libraryLandPattern)
  {
    Assert.expect(libraryPath != null);
    Assert.expect(libraryPackageLongName != null);
    Assert.expect(libraryLandPattern != null);
    Assert.expect(_libraryPathToLibraryPackagesMap != null);
    if(_libraryPathToLibraryPackagesMap.size() == 0 || _libraryPathToLibraryPackagesMap.get(libraryPath) == null)
      return false;
    for (LibraryPackage libraryPackage : _libraryPathToLibraryPackagesMap.get(libraryPath))
    {
      if (libraryPackage.getPackageLongName().equals(libraryPackageLongName) &&
          libraryPackage.getLandPattern().getLandPatternId() == libraryLandPattern.getLandPatternId())
      {
        return true;
      }
    }
    return false;
  }

  /**
   * @return boolean
   *
   * @author Wei Chin, Chong
   */
  public boolean isModifiedAfterImported()
  {
    Assert.expect( isImportedSubtypeScheme() );

    boolean isModified = false;

    if(usesOneSubtype())
    {
      isModified = getSubtype().isModifiedAfterImported();
    }
    else
    {
      for (Subtype subtype : getSubtypes())
      {
        isModified = subtype.isModifiedAfterImported();

        if (isModified)
          return isModified;
      }
    }
    return isModified;
  }

  /**
   * @return boolean
   *
   * @author Wei Chin, Chong
   */
  public boolean isImportedSubtypeScheme()
  {
    boolean isImported = true;
    if(usesOneSubtype())
    {
      isImported = getSubtype().isImportedPackageLibrary();
    }
    else
    {
      for (Subtype subtype : getSubtypes())
      {
        isImported = subtype.isImportedPackageLibrary();

        if (isImported == false)
          return isImported;
      }
    }
    return isImported;
  }

  /**
   *
   * @author Poh Kheng
   */
  public Map<String, List<LibraryPackage>> getLibraryPathToLibrarySubtypeSchemesMap()
  {
    Assert.expect(_libraryPathToLibraryPackagesMap != null);
    return _libraryPathToLibraryPackagesMap;
  }

  /**
   *
   * @author Poh Kheng
   */
  public boolean isLibraryPathToLibraryPackageMapEmpty()
  {
    Assert.expect(_libraryPathToLibraryPackagesMap != null);

    return _libraryPathToLibraryPackagesMap.isEmpty();
  }

  /**
   * @author Tan Hock Zoon
   */
  public void clear()
  {
    _libraryPathToLibraryPackagesMap.clear();
    clearLibraryPath();
  }

  /**
   * @author Tan Hock Zoon
   */
  public void sortLibraryPackageAndSet()
  {
    List<LibraryPackage> libraryPackages = getLibraryPackageList();

    String jointTypeName = null;
    String subtypeName   = null;

    if (usesOneJointTypeEnum())
    {
      jointTypeName = getJointTypeEnum().getName();
    }
    else
    {
      jointTypeName = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
    }
    
    if (usesOneSubtype())
    {
      subtypeName = getSubtype().getShortName();
    }
    else
    {
      subtypeName = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
    }

    for (LibraryPackage libraryPackage : libraryPackages)
    {
      libraryPackage.sortJointTypeNameAndSet(jointTypeName, subtypeName);
    }

    Collections.sort(libraryPackages, new LibraryPackageComparator());

    //obtain the highest match percentage after the sorting
    double percentage = libraryPackages.get(0).getLibrarySubtypeSchemes().get(0).getMatchPercentage();
    List<LibraryPackage> libraryPackagesWithMaxPercentage = new ArrayList<LibraryPackage>();

    //loop through the list to group the highest match percentage
    for (LibraryPackage libraryPackage : libraryPackages)
    {
      for(LibrarySubtypeScheme librarySubtypeScheme : libraryPackage.getLibrarySubtypeSchemes())
      {
        if (librarySubtypeScheme.getMatchPercentage() < percentage)
          break;
      }
      libraryPackagesWithMaxPercentage.add(libraryPackage);
    }

    //set the first library package
    setSelectedLibraryPackage(libraryPackages.get(0));

    for (LibraryPackage libraryPackage : libraryPackagesWithMaxPercentage)
    {
      if (_compPackage.usesOneJointTypeEnum())
      {
        jointTypeName = _compPackage.getJointTypeEnum().getName();
      }
      else
      {
        jointTypeName = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
      }

      if (libraryPackage.getSelectedJointType().equals(jointTypeName) &&
          libraryPackage.getPackageName().equalsIgnoreCase(_compPackage.getShortName()))
        setSelectedLibraryPackage(libraryPackage);
    }
  }

  /**
   * @param padNameToInspectableMap Map
   * @author Wei Chin, Chong
   */
  public void compareInspectionMap(Map<String,Boolean> padNameToInspectableMap)
  {
    Assert.expect(padNameToInspectableMap != null);
    Assert.expect(_padNameToInspectableMap != null);
    Assert.expect(_padNameToInspectableMap.size() == padNameToInspectableMap.size());

    List <String> padNameToChange = new ArrayList<String>();
    for(String padName : _padNameToInspectableMap.keySet())
    {
      if(_padNameToInspectableMap.get(padName) == false &&
         padNameToInspectableMap.get(padName) == true )
      {
        padNameToChange.add(padName);
      }
    }

    for(String padName : padNameToChange)
    {
      _padNameToInspectableMap.remove(padName);
      _padNameToInspectableMap.put(padName, true);
    }

    padNameToChange.clear();
    padNameToChange = null;
  }

  /**
   * @param padNameToTestableMap Map
   * @author Wei Chin, Chong
   */
  public void compareTestableMap(Map<String,Boolean> padNameToTestableMap)
  {
    Assert.expect(padNameToTestableMap != null);
    Assert.expect(_padNameToTestableMap != null);
    Assert.expect(_padNameToTestableMap.size() == padNameToTestableMap.size());

    List <String> padNameToChange = new ArrayList<String>();

    for(String padName : _padNameToTestableMap.keySet())
    {
      if(_padNameToTestableMap.get(padName) == false &&
         padNameToTestableMap.get(padName) == true )
      {
        padNameToChange.add(padName);
      }
    }

    for(String padName : padNameToChange)
    {
      _padNameToInspectableMap.remove(padName);
      _padNameToInspectableMap.put(padName, true);
    }

    padNameToChange.clear();
    padNameToChange = null;
  }

  /**
   * @return Map
   * @author Wei Chin, Chong
   */
  public Map<String,Boolean> getPadNameToInspectableMap()
  {
    Assert.expect(_padNameToInspectableMap != null);
    return _padNameToInspectableMap;
  }

  /**
   * @return Map
   * @author Wei Chin, Chong
   */
  public Map<String,Boolean> getPadNameToTestableMap()
  {
    Assert.expect(_padNameToTestableMap != null);
    return _padNameToTestableMap;
  }

  /**
   *
   * @return List
   *
   * @author Tan Hock Zoon
   */
  public List<LibraryPackage> getCompleteLibraryPackageList()
  {
    Assert.expect(_libraryPathToLibraryPackagesMap != null);

    List<LibraryPackage> completeLibraryPackages = new ArrayList<LibraryPackage>();

    for (String libraryPath : getLibraryPathList())
    {
      completeLibraryPackages.addAll(_libraryPathToLibraryPackagesMap.get(libraryPath));
    }

    return completeLibraryPackages;
  }

  /**
   * @author Tan Hock Zoon
   */
  public void sortLibraryPathListAndSet()
  {
    List<String> libraryPaths = getLibraryPathList();

    for (String libraryPath : libraryPaths)
    {
      List<LibraryPackage> libraryPackages = _libraryPathToLibraryPackagesMap.get(libraryPath);

      String jointTypeName = null;
      String subtypeName   = null;

      if (usesOneJointTypeEnum())
      {
        jointTypeName = getJointTypeEnum().getName();
      }
      else
      {
        jointTypeName = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
      }
      
      if (usesOneSubtype())
      {
        subtypeName = getSubtype().getShortName();
      }
      else
      {
        subtypeName = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
      }

      for (LibraryPackage libraryPackage : libraryPackages)
      {
        libraryPackage.clearSelectedLibrarySubtypeSchemes();
        libraryPackage.sortJointTypeNameAndSet(jointTypeName, subtypeName);
      }

      Collections.sort(libraryPackages, new LibraryPackageComparator());
    }

    Collections.sort(libraryPaths, new LibraryPathComparator(_libraryPathToLibraryPackagesMap));

    setSelectedLibraryPath(libraryPaths.get(0));
  }
}
