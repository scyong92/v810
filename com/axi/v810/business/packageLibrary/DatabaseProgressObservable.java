package com.axi.v810.business.packageLibrary;

import java.util.*;

import com.axi.util.*;

/**
 * <p>Company: Agilent Technogies</p>
 *
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class DatabaseProgressObservable extends Observable
{
  private static DatabaseProgressObservable _instance;

  private double _percentDone = 0;
  private double _stepSize = 0;
  private final double MAX_PERCENT = 100.0;
  private final double MIN_PERCENT = 0.0;

  /**
   * @return DatabaseProgressObservable
   * @author Wei Chin, Chong
   */
  public static synchronized DatabaseProgressObservable getInstance()
  {
    if (_instance == null)
    {
      _instance = new DatabaseProgressObservable();
    }
    return _instance;
  }

  /**
   * @return int percent done database transaction, or 0 if no progress have been received
   *
   * @author Wei Chin, Chong
   */
  public double getPercentDone()
  {
    Assert.expect(_percentDone <= MAX_PERCENT);
    Assert.expect(_percentDone >= MIN_PERCENT);

    return _percentDone;
  }

  /**
   * @param percent int
   * @author Wei Chin, Chong
   */
  public void reportProgress(double percent)
  {
    Assert.expect(percent <= MAX_PERCENT);
    Assert.expect(percent >= MIN_PERCENT);

    // notify observers if the observable's observable state has changed
    double percentDone = getPercentDone();

    _percentDone = percent;

    if (percentDone != _percentDone)
    {
      setChanged();
      notifyObservers();
    }
  }

  /**
   * @param stepSize double
   * @author Wei Chin, Chong
   */
  public void setStepSize(double stepSize)
  {
    Assert.expect(stepSize > 0);
    _stepSize = stepSize;
  }

  /**
   * @return double
   * @author Wei Chin, Chong
   */
  public double getStepSize()
  {
    Assert.expect(_stepSize >= 0);
    return _stepSize;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void incrementPercentDone()
  {
    Assert.expect(_stepSize > 0);

    // notify observers if the observable's observable state has changed
    double percentDone = getPercentDone();

    if( (_percentDone + _stepSize) < MAX_PERCENT )
      _percentDone += _stepSize;
    else
      _percentDone = 100.0;

    if (percentDone != _percentDone)
    {
      setChanged();
      notifyObservers();
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  public void reset()
  {
    _percentDone = 0;
    _stepSize = 0;
  }

  /**
   * @return boolean
   * @author Wei Chin, Chong
   */
  public boolean isComplete()
  {
    return _percentDone >= 100;
  }
}
