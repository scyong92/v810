package com.axi.v810.business.packageLibrary;

import java.util.*;

import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng 
 */
public class ImportLibrarySettingEnum extends com.axi.util.Enum 
{
  private static int _index = -1;
  private static Map<String, ImportLibrarySettingEnum> _nameToImportLibrarySettingEnumMap = new HashMap<String, ImportLibrarySettingEnum>();
  public final static ImportLibrarySettingEnum IMPORT_THRESHOLD = new ImportLibrarySettingEnum(++_index, StringLocalizer.keyToString("PLWK_IMPORT_THRESHOLD_KEY"));
  public final static ImportLibrarySettingEnum IMPORT_NOMINAL = new ImportLibrarySettingEnum(++_index, StringLocalizer.keyToString("PLWK_IMPORT_NOMINAL_KEY"));
  public final static ImportLibrarySettingEnum IMPORT_LANDPATTERN = new ImportLibrarySettingEnum(++_index, StringLocalizer.keyToString("PLWK_IMPORT_LANDPATTERN_KEY"));
  public final static ImportLibrarySettingEnum IMPORT_LIBRARY_SUBTYPE_NAME = new ImportLibrarySettingEnum(++_index, StringLocalizer.keyToString("PLWK_IMPORT_LIBRARY_SUBTYPE_NAME_KEY"));
  
  private String _name;
  
  /**
   * @author Cheah Lee Herng 
   */
  private ImportLibrarySettingEnum(int id, String name)
  {
    super(id);
    _name = name.intern();
    _nameToImportLibrarySettingEnumMap.put(_name, this);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public String toString()
  {
    return _name;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static java.util.List<ImportLibrarySettingEnum> getAllImportLibrarySettingList()
  {    
    return new ArrayList<ImportLibrarySettingEnum>(_nameToImportLibrarySettingEnumMap.values());
  }
}
