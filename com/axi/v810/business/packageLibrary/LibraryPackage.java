package com.axi.v810.business.packageLibrary;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * This class represents library package.
 *
 * @author Cheah Lee Herng
 */
public class LibraryPackage
{
  private int _packageId = -1;
  private String _packageName;
  private String _packageLongName;
  private int _landPatternId = -1;
  private LibraryLandPattern _libraryLandPattern;
  private Map<String, List<LibrarySubtypeScheme>> _jointTypeNameToLibrarySubtypeSchemesMap;
  private String _selectedJointType;
  private List<LibrarySubtypeScheme> _selectedLibrarySubtypeSchemes;
  private Map<LibraryComponentType, List<LibrarySubtypeScheme>> _libraryComponentTypeToLibrarySubtypeSchemeMap;
  private Set<LibraryPackagePin> _libraryPackagePinSet;
  private long _checkSum;
  private Map<String, Long> _jointTypeNameToLibraryPackageCheckSumMap;

  /**
   * @author Cheah Lee Herng
   */
  public LibraryPackage()
  {
    _selectedLibrarySubtypeSchemes = new ArrayList<LibrarySubtypeScheme>();
    _jointTypeNameToLibrarySubtypeSchemesMap = new HashMap<String,List<LibrarySubtypeScheme>>();
    _libraryPackagePinSet = new TreeSet<LibraryPackagePin>(new LibraryPackagePinComparator(true));
    _checkSum = -1;
    
    _libraryComponentTypeToLibrarySubtypeSchemeMap = new HashMap<LibraryComponentType, List<LibrarySubtypeScheme>>();
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getPackageId()
  {
    Assert.expect(_packageId > 0);
    return _packageId;
  }

  /**
   *
   * @param packageId int
   *
   * @author Cheah Lee Herng
   */
  public void setPackageId(int packageId)
  {
    Assert.expect(packageId > 0);
    _packageId = packageId;
  }

  /**
   *
   * @return String
   *
   * @author Cheah Lee Herng
   */
  public String getPackageName()
  {
    Assert.expect(_packageName != null);
    return _packageName;
  }

  /**
   *
   * @param packageName String
   *
   * @author Cheah Lee Herng
   */
  public void setPackageName(String packageName)
  {
    Assert.expect(packageName != null);
    _packageName = packageName;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public String getPackageLongName()
  {
    Assert.expect(_packageLongName != null);
    return _packageLongName;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setPackageLongName(String packageLongName)
  {
    Assert.expect(packageLongName != null);
    _packageLongName = packageLongName;
  }

  /**
   *
   * @return int
   *
   * @author Cheah Lee Herng
   */
  public int getLandPatternId()
  {
    Assert.expect(_landPatternId > 0);
    return _landPatternId;
  }

  /**
   *
   * @param landPatternId int
   *
   * @author Cheah Lee Herng
   */
  public void setLandPatternId(int landPatternId)
  {
    Assert.expect(landPatternId > 0);
    _landPatternId = landPatternId;
  }

  /**
   *
   * @return LibraryLandPattern
   *
   * @author Cheah Lee Herng
   */
  public LibraryLandPattern getLandPattern()
  {
    Assert.expect(_libraryLandPattern != null);
    return _libraryLandPattern;
  }

  /**
   *
   * @param libraryLandPattern LibraryLandPattern
   *
   * @author Cheah Lee Herng
   */
  public void setLibraryLandPattern(LibraryLandPattern libraryLandPattern)
  {
    Assert.expect(libraryLandPattern != null);
    _libraryLandPattern = libraryLandPattern;
  }

  /**
   *
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String toString()
  {
    Assert.expect(_packageName != null);
    return _packageName;
  }

  /**
   *
   * @return Map
   *
   * @author Cheah Lee Herng
   */
  public Map<String, List<LibrarySubtypeScheme>> getJointTypeNameToLibrarySubtypeSchemesMap()
  {
    Assert.expect(_jointTypeNameToLibrarySubtypeSchemesMap != null);
    return _jointTypeNameToLibrarySubtypeSchemesMap;
  }

  /**
   *
   * @param jointTypeName String
   * @param librarySubtypeScheme LibrarySubtypeScheme
   *
   * @author Cheah Lee Herng
   */
  public void addJointTypeNameToLibrarySubtypeSchemesMap(String jointTypeName, LibrarySubtypeScheme librarySubtypeScheme)
  {
    Assert.expect(jointTypeName != null);
    Assert.expect(librarySubtypeScheme != null);

    if(_jointTypeNameToLibrarySubtypeSchemesMap == null)
      _jointTypeNameToLibrarySubtypeSchemesMap = new HashMap<String,List<LibrarySubtypeScheme>>();

    if (_jointTypeNameToLibrarySubtypeSchemesMap.containsKey(jointTypeName))
    {
      _jointTypeNameToLibrarySubtypeSchemesMap.get(jointTypeName).add(librarySubtypeScheme);
    }
    else
    {
      List<LibrarySubtypeScheme> librarySubtypeSchemes = new LinkedList<LibrarySubtypeScheme>();
      librarySubtypeSchemes.add(librarySubtypeScheme);
      _jointTypeNameToLibrarySubtypeSchemesMap.put(jointTypeName, librarySubtypeSchemes);
    }
  }

  /**
   *
   * @param selectedJointType String
   *
   * @author Tan Hock Zoon
   */
  public void setSelectedJointType(String selectedJointType)
  {
    Assert.expect(selectedJointType != null);
    _selectedJointType = selectedJointType;
  }

  /**
   *
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public String getSelectedJointType()
  {
    Assert.expect(_selectedJointType != null);
    return _selectedJointType;
  }

  /**
   *
   * @return List
   *
   * @author Tan Hock Zoon
   */
  public List<LibrarySubtypeScheme> getLibrarySubtypeSchemesBySelectedJointType()
  {
    Assert.expect(_jointTypeNameToLibrarySubtypeSchemesMap != null);
    return _jointTypeNameToLibrarySubtypeSchemesMap.get(_selectedJointType);
  }

  /**
   *
   * @return List
   *
   * @author Tan Hock Zoon
   */
  public List<String> getJointTypeList()
  {
    Assert.expect(_jointTypeNameToLibrarySubtypeSchemesMap != null);

    List<String> jointTypeList = new ArrayList<String>();

    jointTypeList.addAll(_jointTypeNameToLibrarySubtypeSchemesMap.keySet());

    return jointTypeList;
  }

  /**
   *
   * @author Tan Hock Zoon
   */
  private void sortLibrarySubtypeSchemeByJointTypeName()
  {
    Assert.expect(_jointTypeNameToLibrarySubtypeSchemesMap != null);

    for(Map.Entry<String, List<LibrarySubtypeScheme>> mapEntry : _jointTypeNameToLibrarySubtypeSchemesMap.entrySet())
    {
      Collections.sort(mapEntry.getValue(), new LibrarySubtypeSchemeComparator());
    }
  }

  /**
   * @author Tan Hock Zoon
   */
  public void sortJointTypeNameAndSet(String jointTypeName, String subtypeName)
  {
    Assert.expect(subtypeName != null);
    
    sortLibrarySubtypeSchemeByJointTypeName();

    List<String> jointTypeNames = getJointTypeList();

    Collections.sort(jointTypeNames, new LibraryJointTypeComparator(_jointTypeNameToLibrarySubtypeSchemesMap));

    if (jointTypeNames.contains(jointTypeName))
    {
      setSelectedJointType(jointTypeName);
    }
    else
    {
      setSelectedJointType(jointTypeNames.get(0));
    }

    boolean isAssignedLibrarySubtypeScheme = false;
    List<LibrarySubtypeScheme> librarySubtypeSchemes = _jointTypeNameToLibrarySubtypeSchemesMap.get(getSelectedJointType());
    for(LibrarySubtypeScheme librarySubtypeScheme : librarySubtypeSchemes)
    {
      for(LibrarySubtype librarySubtype : librarySubtypeScheme.getLibrarySubtypes())
      {
        if (librarySubtype.getSubtypeName().equalsIgnoreCase(subtypeName))
        {
          addSelectedLibrarySubtypeScheme(librarySubtypeScheme);
          isAssignedLibrarySubtypeScheme = true;
          break;
        }
      }
    }
    
    // Assign first item if there is no specific LibrarySubtypeScheme being matched
    if (isAssignedLibrarySubtypeScheme == false)
      addSelectedLibrarySubtypeScheme(_jointTypeNameToLibrarySubtypeSchemesMap.get(getSelectedJointType()).get(0));
    
    // Add full list of LibrarySubtypeScheme
    addLibrarySubtypeScheme(librarySubtypeSchemes);
  }

  /**
   *
   * @return List
   *
   * @author Cheah Lee Herng
   */
  public List<LibraryPackagePin> getLibraryPackagePins()
  {
    Assert.expect(_libraryPackagePinSet != null);
    return new ArrayList<LibraryPackagePin>(_libraryPackagePinSet);
  }

  /**
   *
   * @param libraryPackagePin LibraryPackagePin
   *
   * @author Cheah Lee Herng
   */
  public void addLibraryPackagePin(LibraryPackagePin libraryPackagePin)
  {
    Assert.expect(_libraryPackagePinSet != null);
    _libraryPackagePinSet.add(libraryPackagePin);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void clear()
  {
    if (_selectedLibrarySubtypeSchemes != null)
      _selectedLibrarySubtypeSchemes.clear();   

    if (_jointTypeNameToLibrarySubtypeSchemesMap != null)
      _jointTypeNameToLibrarySubtypeSchemesMap.clear();

    if (_libraryPackagePinSet != null)
      _libraryPackagePinSet.clear();
    
    if (_libraryComponentTypeToLibrarySubtypeSchemeMap != null)
      _libraryComponentTypeToLibrarySubtypeSchemeMap.clear();

    _selectedLibrarySubtypeSchemes = null;
    _jointTypeNameToLibrarySubtypeSchemesMap = null;
    _libraryComponentTypeToLibrarySubtypeSchemeMap = null;
    _libraryPackagePinSet = null;
    _libraryLandPattern = null;
  }

  /**
   * @author Tan Hock Zoon
   */
  public void clearLibraryPackagePinSet()
  {
    Assert.expect(_libraryPackagePinSet != null);
    _libraryPackagePinSet.clear();
  }

  /**
   *
   * @param jointType String
   * @return Set
   *
   * @author Tan Hock Zoon
   */
  public Set<LibraryPackagePin> getLibraryPackagePinByJointType(String jointType)
  {
    Assert.expect(jointType != null);
    Assert.expect(_libraryPackagePinSet != null);

    Set<LibraryPackagePin> libraryPackagePins = new TreeSet<LibraryPackagePin>(new LibraryPackagePinComparator(true));

    for (LibraryPackagePin libraryPackagePin : _libraryPackagePinSet)
    {
      if (libraryPackagePin.getJointTypeName().equals(jointType))
        libraryPackagePins.add(libraryPackagePin);
    }

    return libraryPackagePins;
  }

  /**
   * @param checkSum long
   * @author Cheah Lee Herng
   */
  public void setCheckSum(long checkSum)
  {
    Assert.expect(checkSum != -1);
    _checkSum = checkSum;
  }

  /**
   * @return long
   * @author Cheah Lee Herng
   */
  public long getCheckSum()
  {
    Assert.expect(_checkSum != -1);
    return _checkSum;
  }

  /**
   * @author Poh Kheng
   */
  public void addJointTypeNameToLibraryPackageCheckSumMap(String jointTypeName, long libraryPackageCheckSum)
  {
    Assert.expect(jointTypeName != null);
    Assert.expect(libraryPackageCheckSum > 0);

    if(_jointTypeNameToLibraryPackageCheckSumMap == null)
      _jointTypeNameToLibraryPackageCheckSumMap = new HashMap<String,Long>();

    if(_jointTypeNameToLibraryPackageCheckSumMap.containsKey(jointTypeName))
      Assert.expect(_jointTypeNameToLibraryPackageCheckSumMap.get(jointTypeName) == libraryPackageCheckSum);
    else
      _jointTypeNameToLibraryPackageCheckSumMap.put(jointTypeName, Long.valueOf(libraryPackageCheckSum));
  }

  /**
   * @author Poh Kheng
   */
  public long getJointTypeLibraryPackageCheckSum(String jointTypeName)
  {
    Assert.expect(jointTypeName != null);
    Assert.expect(_jointTypeNameToLibraryPackageCheckSumMap.containsKey(jointTypeName));

    return _jointTypeNameToLibraryPackageCheckSumMap.get(jointTypeName);
  }

  /**
   *
   * @return List
   *
   * @author Tan Hock Zoon
   */
  public List<LibrarySubtypeScheme> getCompleteLibrarySubtypeSchemeList()
  {
    Assert.expect(_jointTypeNameToLibrarySubtypeSchemesMap != null);

    List<LibrarySubtypeScheme> completeLibrarySubtypeSchemes = new ArrayList<LibrarySubtypeScheme>();

    for (String jointTypeName : getJointTypeList())
    {
      completeLibrarySubtypeSchemes.addAll(_jointTypeNameToLibrarySubtypeSchemesMap.get(jointTypeName));
    }

    return completeLibrarySubtypeSchemes;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasSelectedMixedLibrarySubtypeScheme()
  {
    return (_selectedLibrarySubtypeSchemes.size() > 1);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasMixedLibrarySubtypeScheme(List<ComponentType> componentTypes)
  {
    Assert.expect(_libraryComponentTypeToLibrarySubtypeSchemeMap != null);
    Assert.expect(componentTypes != null);
    
    boolean isContainMixedLibrarySubtypeScheme = false;
    
    String prevLibrarySubtypeName = null;
    
    for(ComponentType componentType : componentTypes)
    {
      for(Map.Entry<LibraryComponentType, List<LibrarySubtypeScheme>> entry : _libraryComponentTypeToLibrarySubtypeSchemeMap.entrySet())
      {
        if (componentType.getReferenceDesignator().equalsIgnoreCase(entry.getKey().getReferenceDesignator()))
        {
          List<LibrarySubtypeScheme> librarySubtypeSchemes = entry.getValue();
          Collections.sort(librarySubtypeSchemes, new AlphaNumericComparator());

          String subtypeName = "";
          for(LibrarySubtypeScheme librarySubtypeScheme : librarySubtypeSchemes)
          {
            subtypeName += librarySubtypeScheme.getName();
          }

          if (prevLibrarySubtypeName == null)
            prevLibrarySubtypeName = subtypeName;
          else
          {
            if (prevLibrarySubtypeName.equalsIgnoreCase(subtypeName) == false)
            {
              isContainMixedLibrarySubtypeScheme = true;
              break;
            }
          }
        }
      }
    }
    
    return isContainMixedLibrarySubtypeScheme;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void addSelectedLibrarySubtypeScheme(LibrarySubtypeScheme librarySubtypeScheme)
  {
    if (_selectedLibrarySubtypeSchemes == null)
      _selectedLibrarySubtypeSchemes = new ArrayList<LibrarySubtypeScheme>();
    
    if (_selectedLibrarySubtypeSchemes.contains(librarySubtypeScheme) == false)
      _selectedLibrarySubtypeSchemes.add(librarySubtypeScheme);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clearSelectedLibrarySubtypeSchemes()
  {
    if (_selectedLibrarySubtypeSchemes != null)
      _selectedLibrarySubtypeSchemes.clear();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public List<LibrarySubtypeScheme> getSelectedLibrarySubtypeSchemes()
  {
    return new ArrayList<LibrarySubtypeScheme>(_selectedLibrarySubtypeSchemes);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public LibrarySubtypeScheme getSelectedLibrarySubtypeScheme()
  {
    Assert.expect(_selectedLibrarySubtypeSchemes != null);
    Assert.expect(_selectedLibrarySubtypeSchemes.isEmpty() == false);
    
    return _selectedLibrarySubtypeSchemes.get(0);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isSelectedMixedLibraryProjectName()
  {
    boolean isSelectedMixedLibraryProjectName = false;
    String prevLibraryProjectName = null;
    
    if (hasSelectedMixedLibrarySubtypeScheme())
    {
      for(LibrarySubtypeScheme librarySubtypeScheme : getSelectedLibrarySubtypeSchemes())
      {
        if (prevLibraryProjectName == null)
          prevLibraryProjectName = librarySubtypeScheme.getLibraryProjectName();

        if (prevLibraryProjectName.equalsIgnoreCase(librarySubtypeScheme.getLibraryProjectName()) == false)
        {
          isSelectedMixedLibraryProjectName = true;
          break;
        }

        prevLibraryProjectName = librarySubtypeScheme.getLibraryProjectName();
      }
    }    
    return isSelectedMixedLibraryProjectName;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void addLibrarySubtypeScheme(List<LibrarySubtypeScheme> librarySubtypeSchemes)
  {
    Assert.expect(librarySubtypeSchemes != null);
    
    for(LibrarySubtypeScheme librarySubtypeScheme : librarySubtypeSchemes)
    {
      addLibrarySubtypeScheme(librarySubtypeScheme);
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void addLibrarySubtypeScheme(LibrarySubtypeScheme librarySubtypeScheme)
  {
    Assert.expect(librarySubtypeScheme != null);
    Assert.expect(_libraryComponentTypeToLibrarySubtypeSchemeMap != null);
   
    for(LibrarySubtype librarySubtype : librarySubtypeScheme.getLibrarySubtypes())
    {
      for(LibraryComponentType libraryComponentType : librarySubtype.getLibraryComponentTypes())
      {
        if (_libraryComponentTypeToLibrarySubtypeSchemeMap.containsKey(libraryComponentType) == false)
          _libraryComponentTypeToLibrarySubtypeSchemeMap.put(libraryComponentType, new ArrayList<LibrarySubtypeScheme>());
        
        if (_libraryComponentTypeToLibrarySubtypeSchemeMap.get(libraryComponentType).contains(librarySubtypeScheme) == false)
          _libraryComponentTypeToLibrarySubtypeSchemeMap.get(libraryComponentType).add(librarySubtypeScheme);
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setupLibraryComponentTypeToLibrarySubtypeSchemeMapping()
  {
    for(LibrarySubtypeScheme librarySubtypeScheme : getLibrarySubtypeSchemesBySelectedJointType())
    {
      for(LibrarySubtype librarySubtype : librarySubtypeScheme.getLibrarySubtypes())
      {
        for(LibraryComponentType libraryComponentType : librarySubtype.getLibraryComponentTypes())
        {
          addLibraryComponentTypeToLibrarySubtypeSchemes(libraryComponentType, librarySubtypeScheme);
        }
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void addLibraryComponentTypeToLibrarySubtypeSchemes(LibraryComponentType libraryComponentType, LibrarySubtypeScheme librarySubtypeScheme)
  {
    Assert.expect(_libraryComponentTypeToLibrarySubtypeSchemeMap != null);
    Assert.expect(libraryComponentType != null);
    Assert.expect(librarySubtypeScheme != null);
    
    if (_libraryComponentTypeToLibrarySubtypeSchemeMap.containsKey(libraryComponentType) == false)
      _libraryComponentTypeToLibrarySubtypeSchemeMap.put(libraryComponentType, new ArrayList<LibrarySubtypeScheme>());
    
    if (_libraryComponentTypeToLibrarySubtypeSchemeMap.get(libraryComponentType).contains(librarySubtypeScheme) == false)
      _libraryComponentTypeToLibrarySubtypeSchemeMap.get(libraryComponentType).add(librarySubtypeScheme);
  }
  
  /**
   * XCR-3547 No matching package when click search with library from 5.7
   * @author Cheah Lee Herng 
   */
  public List<LibrarySubtypeScheme> getLibrarySubtypeSchemes()
  {
    return getCompleteLibrarySubtypeSchemeList();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public Map<LibraryComponentType, List<LibrarySubtypeScheme>> getLibraryComponentTypeToLibrarySubtypeSchemeMap()
  {
    Assert.expect(_libraryComponentTypeToLibrarySubtypeSchemeMap != null);
    return _libraryComponentTypeToLibrarySubtypeSchemeMap;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void assignComponentType(List<ComponentType> componentTypes)
  {
    Assert.expect(componentTypes != null);
    
    if (isSelectedMixedLibraryProjectName())
      assignComponentTypeToLibrarySubtypeScheme(componentTypes);
    else
      assignComponentTypeToSingleLibrarySubtypeScheme(componentTypes);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void assignComponentTypeToSingleLibrarySubtypeScheme(List<ComponentType> componentTypes)
  {
    Assert.expect(componentTypes != null);
    
    LibrarySubtypeScheme selectedLibrarySubtypeScheme = getSelectedLibrarySubtypeScheme();
    
    for (ComponentType compType : componentTypes)
    {
      compType.setAssignLibrarySubtypeScheme(selectedLibrarySubtypeScheme);
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void assignComponentTypeToLibrarySubtypeScheme(List<ComponentType> componentTypes)
  {
    Assert.expect(componentTypes != null);    
    
    // Assign LibrarySubtypeScheme to each ComponentType    
    for(ComponentType componentType : componentTypes)
    {
      boolean isAssignLibrarySubtypeScheme =false;
      for(Map.Entry<LibraryComponentType, List<LibrarySubtypeScheme>> entry : getLibraryComponentTypeToLibrarySubtypeSchemeMap().entrySet())
      {
        LibraryComponentType libraryComponentType = entry.getKey();
        List<LibrarySubtypeScheme> librarySubtypeSchemes = entry.getValue();
        
        // Sort LibrarySubtypeScheme list
        Collections.sort(librarySubtypeSchemes, new AlphaNumericComparator());
        
        if (libraryComponentType.getReferenceDesignator().equalsIgnoreCase(componentType.getReferenceDesignator()))
        {
          // Auto-assign first LibrarySubtypeScheme
          componentType.setAssignLibrarySubtypeScheme(librarySubtypeSchemes.get(0));
          isAssignLibrarySubtypeScheme = true;

          break;
        }
      }
      
      if (isAssignLibrarySubtypeScheme == false)
      {
        componentType.setAssignLibrarySubtypeScheme(getLibrarySubtypeSchemes().get(0));
      }
    }
  }
}
