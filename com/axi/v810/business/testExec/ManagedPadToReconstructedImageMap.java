package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class is a collection that maps Pads to ReconstructedImages, caching some
 * of the images to disk if we start running out of memory.
 *
 * @author Peter Esbensen
 */
public class ManagedPadToReconstructedImageMap
{
  private int _bytesUsed;
  private Map<Pad, ReconstructionRegion> _padToRegionMap = new HashMap<Pad, ReconstructionRegion>();
  private int _maximumAllowableMemoryUsageInBytes = -1;
  private Map<ReconstructionRegion, ReconstructedImages> _regionToImagesMap = new HashMap<ReconstructionRegion, ReconstructedImages>();
  private ImageManager _imageManager = ImageManager.getInstance();
  private ImageSetData _imageSetData = null;

  /**
   * @author Peter Esbensen
   */
  public ManagedPadToReconstructedImageMap(int maximumAllowableMemoryUsageInBytes)
  {
    Assert.expect(maximumAllowableMemoryUsageInBytes > 0);

    _maximumAllowableMemoryUsageInBytes = maximumAllowableMemoryUsageInBytes;
  }

  /**
   * @author Peter Esbensen
   */
  public boolean containsValue(ReconstructedImages reconstructedImages)
  {
    Assert.expect(reconstructedImages != null);

    return _regionToImagesMap.containsKey(reconstructedImages.getReconstructionRegion());
  }

  /**
   * @author Peter Esbensen
   */
  public synchronized void put(Pad pad, ReconstructedImages reconstructedImages) throws DatastoreException
  {
    Assert.expect(pad != null);
    Assert.expect(reconstructedImages != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

    ReconstructionRegion existingRegion = _padToRegionMap.put(pad, reconstructionRegion);
    Assert.expect(existingRegion == null, "pad added twice to the map");

    if (_regionToImagesMap.containsKey(reconstructionRegion) == false)
    {

      freeUpMemoryIfNecessary(pad.getComponent().getSideBoard().getSideBoardType().getBoardType().getPanel().getProject(), // this is ridiculous
                              reconstructedImages);
      _regionToImagesMap.put(reconstructionRegion, reconstructedImages);
      _bytesUsed += reconstructedImages.getMaximumExpectedMemorySize();
    }
    else
    {
      ReconstructedImages currentImagesInMap = _regionToImagesMap.get(reconstructionRegion);
      Assert.expect((reconstructedImages == currentImagesInMap) ||
                    (currentImagesInMap == null));
    }
  }

  /**
   * @author Peter Esbensen
   */
  public synchronized void remove(Pad pad)
  {
    Assert.expect(pad != null);
    ReconstructionRegion removedRegion = _padToRegionMap.remove(pad);
    if (_padToRegionMap.containsValue(removedRegion) == false)
    {
      ReconstructedImages imagesToRemove = _regionToImagesMap.get(removedRegion);
      if (imagesToRemove != null)
      {
        _bytesUsed -= imagesToRemove.getMaximumExpectedMemorySize();
        imagesToRemove.decrementReferenceCount();
      }
      _regionToImagesMap.remove(removedRegion);
    }

    // Assert.expect(removedRegion != null);  we don't keep track of which pads we've added so sometimes we will ask to remove one that wasn't tested
  }

  /**
   * @author Peter Esbensen
   */
  public synchronized ReconstructedImages get(Pad pad) throws DatastoreException
  {
    Assert.expect(pad != null);
    ReconstructionRegion region = _padToRegionMap.get(pad);
    Assert.expect(region != null);
    ReconstructedImages images = _regionToImagesMap.get(region);
    if (images == null)
    {
      Project project = pad.getPadType().getComponentType().getSideBoardType().getBoardType().getPanel().getProject();
      images = loadReconstructedImages(region);
      freeUpMemoryIfNecessary(project, images);
      _regionToImagesMap.put(region, images);
      _bytesUsed += images.getMaximumExpectedMemorySize();
//      System.out.println("padToImageMap usage : " + _bytesUsed);
    }
    return images;
  }

  /**
   * @author Peter Esbensen
   */
  private ReconstructedImages loadReconstructedImages(ReconstructionRegion reconstructionRegion) throws DatastoreException
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(_imageSetData != null);
    return _imageManager.loadInspectionImages(_imageSetData, reconstructionRegion);
  }

  /**
   * @author Peter Esbensen
   * @author khang-shian.sham
   */
  private void freeUpMemoryIfNecessary(Project project,
                                       ReconstructedImages reconstructedImages) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(reconstructedImages != null);

    long additionalBytesRequired = reconstructedImages.getMaximumExpectedMemorySize();
    Assert.expect(_maximumAllowableMemoryUsageInBytes > 0);

    Iterator<Map.Entry<ReconstructionRegion, ReconstructedImages>> regionIterator = _regionToImagesMap.entrySet().iterator();
    while (((_bytesUsed + additionalBytesRequired) > _maximumAllowableMemoryUsageInBytes) &&
           (regionIterator.hasNext()))
    {
      Map.Entry<ReconstructionRegion, ReconstructedImages> mapEntry = regionIterator.next();
      ReconstructedImages imagesToClear = mapEntry.getValue();
      if (imagesToClear != null)
      {
        generateImageSetDataIfNecessary(project);
        Assert.expect(_imageSetData != null);
        //add project name parameter in saveInspectionImages
        _imageManager.saveInspectionImages(_imageSetData, imagesToClear, project.getName());
        mapEntry.setValue(null);
        imagesToClear.decrementReferenceCount();
        if (UnitTest.unitTesting() == false)
          System.out.println("PE Warning: caching image to disk for ManagedPadToReconstructedImagesMap");
        long bytesFreed = imagesToClear.getMaximumExpectedMemorySize();
        _bytesUsed -= bytesFreed;
      }
    }
  }

  /**
   * @author Peter Esbensen
   */
  private void generateImageSetDataIfNecessary(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (_imageSetData == null)
    {
      String imageSetName = "temporaryCachedImages";
      String description = "cached images during inspection";
      _imageSetData = new ImageSetData();

      // Set up the image set root, name, type, description, project name, and version number.
      _imageSetData.setImageSetName(imageSetName);
      _imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.PANEL); // Always panel for now...
      _imageSetData.setUserDescription(description);
      _imageSetData.setProjectName(project.getName());
      _imageSetData.setDateInMils(System.currentTimeMillis());
      _imageSetData.setBoardPopulated(true); //@todo support unpopulated boards
      _imageSetData.setTestProgramVersionNumber(project.getTestProgramVersion());
      _imageSetData.setMachineSerialNumber("0");
      _imageSetData.setGenerateMultiAngleImages(project.isGenerateMultiAngleImage()); // Bee Hoon, Recipe Settings
      FileUtilAxi.createDirectory(_imageSetData.getDir());
    }
  }

  /**
   * @author Peter Esbensen
   */
  public synchronized void clear() throws DatastoreException
  {
    _padToRegionMap.clear();
    for (ReconstructedImages reconstructedImages : _regionToImagesMap.values())
    {
      if (reconstructedImages != null)
        reconstructedImages.decrementReferenceCount();
    }
    _regionToImagesMap.clear();
    if (_imageSetData != null)
      _imageManager.deleteImages(_imageSetData);
  }

  /**
   * @author Peter Esbensen
   */
  public synchronized boolean isEmpty()
  {
    return (_padToRegionMap.isEmpty() && _regionToImagesMap.isEmpty());
  }
}
