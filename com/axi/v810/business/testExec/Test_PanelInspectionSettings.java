package com.axi.v810.business.testExec;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;

/**
 * Test class for PanelInspectionSettings
 * @author Andy Mechtenberg
 */
public class Test_PanelInspectionSettings extends UnitTest
{
  /**
   * @author Andy Mechtenberg
   */
  public Test_PanelInspectionSettings()
  {
    // Do nothing.
  }

  /**
   * Main test method.
   *
   * @param in The input stream.
   * @param out The output stream.
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    testDiagnostics();
    testOnOff();

  }

  /**
   * @author Andy Mechtenberg
   */
  private void testDiagnostics()
  {
    PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();

    // test the all-all case
    panelInspectionSettings.clearAllInspectionSettings();
    panelInspectionSettings.setAllJointTypesAllAlgorithmsDiagnosticsEnabled(true);

    Expect.expect(panelInspectionSettings.areAnyDiagnosticsOn() == true);
    Collection<JointTypeEnum> jointTypes = JointTypeEnum.getAllJointTypeEnums();
    for(JointTypeEnum jointTypeEnum : jointTypes)
    {
      InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

      Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
      for (AlgorithmEnum algorithmEnum : algorithmEnums)
      {
        Expect.expect(panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithmEnum));
      }
    }

    panelInspectionSettings.clearAllInspectionSettings();

    for(JointTypeEnum jointTypeEnum : jointTypes)
    {
      InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

      Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
      for (AlgorithmEnum algorithmEnum : algorithmEnums)
      {
        Expect.expect(panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithmEnum) == false);
      }
    }

    // test the All-<ALG> case
    panelInspectionSettings.setAllJointTypesSingleAlgorithmDiagnosticsEnabled(AlgorithmEnum.MEASUREMENT, true);
    Expect.expect(panelInspectionSettings.areAnyDiagnosticsOn() == true);
    for(JointTypeEnum jointTypeEnum : jointTypes)
    {
      InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

      Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
      for (AlgorithmEnum algorithmEnum : algorithmEnums)
      {
        if (algorithmEnum.equals(AlgorithmEnum.MEASUREMENT))
          Expect.expect(panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithmEnum) == true);
        else
          Expect.expect(panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithmEnum) == false);
      }
    }

    panelInspectionSettings.clearAllInspectionSettings();
    // test the <JointType>-ALL case
    panelInspectionSettings.setSingleJointTypeAllAlgorithmsDiagnosticsEnabled(JointTypeEnum.CGA, true);
    Expect.expect(panelInspectionSettings.areAnyDiagnosticsOn() == true);
    for (JointTypeEnum jointTypeEnum : jointTypes)
    {

      if (jointTypeEnum.equals(JointTypeEnum.CGA))
      {
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

        Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
        for (AlgorithmEnum algorithmEnum : algorithmEnums)
        {
          Expect.expect(panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithmEnum) == true);
        }
      }
      else
      {
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

        Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
        for (AlgorithmEnum algorithmEnum : algorithmEnums)
        {
          Expect.expect(panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithmEnum) == false);
        }
      }
    }

    panelInspectionSettings.clearAllInspectionSettings();
    // test the <JointType>-ALL case
    panelInspectionSettings.setSingleJointTypeSingleAlgorithmDiagnosticsEnabled(JointTypeEnum.CAPACITOR, AlgorithmEnum.MEASUREMENT, true);
    Expect.expect(panelInspectionSettings.areAnyDiagnosticsOn() == true);
    for (JointTypeEnum jointTypeEnum : jointTypes)
    {
      if (jointTypeEnum.equals(JointTypeEnum.CAPACITOR))
      {
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

        Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
        for (AlgorithmEnum algorithmEnum : algorithmEnums)
        {
          if (algorithmEnum.equals(AlgorithmEnum.MEASUREMENT))
            Expect.expect(panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithmEnum) == true);
          else
            Expect.expect(panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithmEnum) == false);
        }
      }
      else
      {
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

        Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
        for (AlgorithmEnum algorithmEnum : algorithmEnums)
        {
          Expect.expect(panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithmEnum) == false);
        }
      }
    }

    // finally, we'll do a complex case:
    // set up diags for :
    //  All - Short
    //  BGA - Measurement
    //  CHIP - All
    panelInspectionSettings.clearAllInspectionSettings();
    // test the <JointType>-ALL case
    panelInspectionSettings.setAllJointTypesSingleAlgorithmDiagnosticsEnabled(AlgorithmEnum.SHORT, true);
    panelInspectionSettings.setSingleJointTypeSingleAlgorithmDiagnosticsEnabled(JointTypeEnum.COLLAPSABLE_BGA, AlgorithmEnum.MEASUREMENT, true);
    panelInspectionSettings.setSingleJointTypeAllAlgorithmsDiagnosticsEnabled(JointTypeEnum.CAPACITOR, true);
    Expect.expect(panelInspectionSettings.areAnyDiagnosticsOn() == true);
    for (JointTypeEnum jointTypeEnum : jointTypes)
    {
      if (jointTypeEnum.equals(JointTypeEnum.CAPACITOR))
      {
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

        Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
        for (AlgorithmEnum algorithmEnum : algorithmEnums)
        {
          Expect.expect(panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithmEnum) == true);
        }
      }
      else if (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
      {
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

        Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
        for (AlgorithmEnum algorithmEnum : algorithmEnums)
        {
          if (algorithmEnum.equals(AlgorithmEnum.MEASUREMENT))
            Expect.expect(panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithmEnum) == true);
          else if (algorithmEnum.equals(AlgorithmEnum.SHORT))
            Expect.expect(panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithmEnum) == true);
          else
            Expect.expect(panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithmEnum) == false);
        }
      }
      else
      {
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

        Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
        for (AlgorithmEnum algorithmEnum : algorithmEnums)
        {
          if (algorithmEnum.equals(AlgorithmEnum.SHORT))
            Expect.expect(panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithmEnum) == true);
          else
            Expect.expect(panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithmEnum) == false);
        }
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void testOnOff()
  {
    PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();

    // test the all-all case
    panelInspectionSettings.clearAllInspectionSettings();
    panelInspectionSettings.setAllJointTypesAllAlgorithmsDisabled(true);

    Expect.expect(panelInspectionSettings.isAnyAlgorithmDisabled() == true);
    Collection<JointTypeEnum> jointTypes = JointTypeEnum.getAllJointTypeEnums();
    for(JointTypeEnum jointTypeEnum : jointTypes)
    {
      InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

      Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
      for (AlgorithmEnum algorithmEnum : algorithmEnums)
      {
        Expect.expect(panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithmEnum));
      }
    }

    panelInspectionSettings.clearAllInspectionSettings();

    for(JointTypeEnum jointTypeEnum : jointTypes)
    {
      InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

      Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
      for (AlgorithmEnum algorithmEnum : algorithmEnums)
      {
        Expect.expect(panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithmEnum) == false);
      }
    }

    // test the All-<ALG> case
    panelInspectionSettings.setAllJointTypesSingleAlgorithmDisabled(AlgorithmEnum.MEASUREMENT, true);
    Expect.expect(panelInspectionSettings.isAnyAlgorithmDisabled() == true);
    for(JointTypeEnum jointTypeEnum : jointTypes)
    {
      InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

      Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
      for (AlgorithmEnum algorithmEnum : algorithmEnums)
      {
        if (algorithmEnum.equals(AlgorithmEnum.MEASUREMENT))
          Expect.expect(panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithmEnum) == true);
        else
          Expect.expect(panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithmEnum) == false);
      }
    }

    panelInspectionSettings.clearAllInspectionSettings();
    // test the <JointType>-ALL case
    panelInspectionSettings.setSingleJointTypeAllAlgorithmsDisabled(JointTypeEnum.CGA, true);
    Expect.expect(panelInspectionSettings.isAnyAlgorithmDisabled() == true);
    for (JointTypeEnum jointTypeEnum : jointTypes)
    {
      if (jointTypeEnum.equals(JointTypeEnum.CGA))
      {
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

        Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
        for (AlgorithmEnum algorithmEnum : algorithmEnums)
        {
          Expect.expect(panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithmEnum) == true);
        }
      }
      else
      {
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

        Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
        for (AlgorithmEnum algorithmEnum : algorithmEnums)
        {
          Expect.expect(panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithmEnum) == false);
        }
      }
    }

    panelInspectionSettings.clearAllInspectionSettings();
    // test the <JointType>-ALL case
    panelInspectionSettings.setSingleJointTypeSingleAlgorithmDisabled(JointTypeEnum.CAPACITOR, AlgorithmEnum.MEASUREMENT, true);
    Expect.expect(panelInspectionSettings.isAnyAlgorithmDisabled() == true);
    for (JointTypeEnum jointTypeEnum : jointTypes)
    {
      if (jointTypeEnum.equals(JointTypeEnum.CAPACITOR))
      {
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

        Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
        for (AlgorithmEnum algorithmEnum : algorithmEnums)
        {
          if (algorithmEnum.equals(AlgorithmEnum.MEASUREMENT))
            Expect.expect(panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithmEnum) == true);
          else
            Expect.expect(panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithmEnum) == false);
        }
      }
      else
      {
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

        Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
        for (AlgorithmEnum algorithmEnum : algorithmEnums)
        {
          Expect.expect(panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithmEnum) == false);
        }
      }
    }

    // finally, we'll do a complex case:
    // set up off for :
    //  All - Short
    //  BGA - Measurement
    //  CHIP - All
    panelInspectionSettings.clearAllInspectionSettings();
    // test the <JointType>-ALL case
    panelInspectionSettings.setAllJointTypesSingleAlgorithmDisabled(AlgorithmEnum.SHORT, true);
    panelInspectionSettings.setSingleJointTypeSingleAlgorithmDisabled(JointTypeEnum.COLLAPSABLE_BGA, AlgorithmEnum.MEASUREMENT, true);
    panelInspectionSettings.setSingleJointTypeAllAlgorithmsDisabled(JointTypeEnum.CAPACITOR, true);
    Expect.expect(panelInspectionSettings.isAnyAlgorithmDisabled() == true);
    for (JointTypeEnum jointTypeEnum : jointTypes)
    {
      if (jointTypeEnum.equals(JointTypeEnum.CAPACITOR))
      {
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

        Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
        for (AlgorithmEnum algorithmEnum : algorithmEnums)
        {
          Expect.expect(panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithmEnum) == true);
        }
      }
      else if (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
      {
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

        Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
        for (AlgorithmEnum algorithmEnum : algorithmEnums)
        {
          if (algorithmEnum.equals(AlgorithmEnum.MEASUREMENT))
            Expect.expect(panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithmEnum) == true);
          else if (algorithmEnum.equals(AlgorithmEnum.SHORT))
            Expect.expect(panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithmEnum) == true);
          else
            Expect.expect(panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithmEnum) == false);
        }
      }
      else
      {
        InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

        Collection<AlgorithmEnum> algorithmEnums = inspectionFamily.getAlgorithmEnums();
        for (AlgorithmEnum algorithmEnum : algorithmEnums)
        {
          if (algorithmEnum.equals(AlgorithmEnum.SHORT))
            Expect.expect(panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithmEnum) == true);
          else
            Expect.expect(panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithmEnum) == false);
        }
      }
    }
  }

  /**
   * Main entry point for the test fixture.
   *
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_PanelInspectionSettings());
  }
}
