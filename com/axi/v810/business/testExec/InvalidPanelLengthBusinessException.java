package com.axi.v810.business.testExec;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class InvalidPanelLengthBusinessException extends BusinessException
{
  /**
   * @author Bill Darbie
   */
  public InvalidPanelLengthBusinessException(MathUtilEnum units, double minLengthInUnits, double maxLengthInUnits, double actualLengthInUnits)
  {
    super(new LocalizedString("BUS_INVALID_PANEL_LENGTH_EXCEPTION_KEY", new Object[]{units, minLengthInUnits, maxLengthInUnits, actualLengthInUnits}));
    Assert.expect(units != null);
  }
}
