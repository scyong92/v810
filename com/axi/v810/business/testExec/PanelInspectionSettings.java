package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 * This class contains the mapping from each GenericAlgorithm to each AlgorithmInspectionSetting
 * @author Peter Esbensen
 * @author Bill Darbie
 * @author Matt Wharton
 */
public class PanelInspectionSettings
{
  private static PanelInspectionSettings _instance;

  // This Map keeps track of which (joint type, algorithm) pairs have diagnostics enabled.
  private Map<Pair<JointTypeEnum, AlgorithmEnum>, BooleanRef> _jointTypeAlgorithmPairToDiagnosticsEnabledMap =
      new HashMap<Pair<JointTypeEnum,AlgorithmEnum>, BooleanRef>();
  // This Map keeps track of which (joint type, algorithm) pairs are disabled.
  private Map<Pair<JointTypeEnum, AlgorithmEnum>, BooleanRef> _jointTypeAlgorithmPairToDisabledMap =
      new HashMap<Pair<JointTypeEnum,AlgorithmEnum>, BooleanRef>();
  // This Collection keeps track of the subtypes that are going to be inspected.
  private Collection<Subtype> _inspectedSubtypes = new HashSet<Subtype>();

  /**
   * @author Peter Esbensen
   */
  public static synchronized PanelInspectionSettings getInstance()
  {
    if (_instance == null)
    {
      _instance = new PanelInspectionSettings();
    }
    return _instance;
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  private PanelInspectionSettings()
  {
    for (JointTypeEnum jointTypeEnum : JointTypeEnum.getAllJointTypeEnums())
    {
      InspectionFamily inspectionFamily = InspectionFamily.getInspectionFamily(jointTypeEnum);

      for (AlgorithmEnum algorithmEnum : inspectionFamily.getAlgorithmEnums())
      {
        _jointTypeAlgorithmPairToDiagnosticsEnabledMap.put(new Pair<JointTypeEnum, AlgorithmEnum>(jointTypeEnum, algorithmEnum),
                                                           new BooleanRef(false));
        _jointTypeAlgorithmPairToDisabledMap.put(new Pair<JointTypeEnum, AlgorithmEnum>(jointTypeEnum, algorithmEnum),
                                                 new BooleanRef(false));

      }
    }
  }

  /**
   * Set all the algorithms' diagnostics enabled status to the value passed in.
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public void setAllJointTypesAllAlgorithmsDiagnosticsEnabled(boolean diagnosticsEnabled)
  {
    Assert.expect(_jointTypeAlgorithmPairToDiagnosticsEnabledMap != null);

    for (Map.Entry<Pair<JointTypeEnum, AlgorithmEnum>, BooleanRef> mapEntry : _jointTypeAlgorithmPairToDiagnosticsEnabledMap.entrySet())
    {
      BooleanRef currentValue = mapEntry.getValue();
      currentValue.setValue(diagnosticsEnabled);
    }
  }

  /**
   * Set all the algorithms' disabled status to the value passed in.
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public void setAllJointTypesAllAlgorithmsDisabled(boolean disabled)
  {
    Assert.expect(_jointTypeAlgorithmPairToDisabledMap != null);

    for (Map.Entry<Pair<JointTypeEnum, AlgorithmEnum>, BooleanRef> mapEntry : _jointTypeAlgorithmPairToDisabledMap.entrySet())
    {
      BooleanRef currentValue = mapEntry.getValue();
      currentValue.setValue(disabled);
    }
  }

  /**
   * Apply the diagnostics enabled setting to all the algorithms in the joint type passed in.
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public void setSingleJointTypeAllAlgorithmsDiagnosticsEnabled(JointTypeEnum jointTypeEnum, boolean diagnosticsEnabled)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(_jointTypeAlgorithmPairToDiagnosticsEnabledMap != null);

    // modify the settings of all the algorithms for the specified joint type
    for (Map.Entry<Pair<JointTypeEnum, AlgorithmEnum>, BooleanRef> mapEntry : _jointTypeAlgorithmPairToDiagnosticsEnabledMap.entrySet())
    {
      Pair<JointTypeEnum, AlgorithmEnum> jointTypeAlgorithmPair = mapEntry.getKey();
      if (jointTypeAlgorithmPair.getFirst().equals(jointTypeEnum))
      {
        BooleanRef currentValue = mapEntry.getValue();
        currentValue.setValue(diagnosticsEnabled);
      }
    }
  }

  /**
   * Apply the disabled setting to all the algorithms in the joint type passed in.
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public void setSingleJointTypeAllAlgorithmsDisabled(JointTypeEnum jointTypeEnum, boolean disabled)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(_jointTypeAlgorithmPairToDisabledMap != null);

    // modify the settings of all the algorithms for the specified joint type
    for (Map.Entry<Pair<JointTypeEnum, AlgorithmEnum>, BooleanRef> mapEntry : _jointTypeAlgorithmPairToDisabledMap.entrySet())
    {
      Pair<JointTypeEnum, AlgorithmEnum> jointTypeAlgorithmPair = mapEntry.getKey();
      if (jointTypeAlgorithmPair.getFirst().equals(jointTypeEnum))
      {
        BooleanRef currentValue = mapEntry.getValue();
        currentValue.setValue(disabled);
      }
    }
  }

  /**
   * Apply the diagnostics enabled flag passed in to the joint type and algorithm passed in.
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public void setSingleJointTypeSingleAlgorithmDiagnosticsEnabled(JointTypeEnum jointTypeEnum,
                                                                  AlgorithmEnum algorithmEnum,
                                                                  boolean diagnosticsEnabled)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithmEnum != null);
    Assert.expect(_jointTypeAlgorithmPairToDiagnosticsEnabledMap != null);

    Pair<JointTypeEnum, AlgorithmEnum> targetJointTypeAlgorithmPair = new Pair<JointTypeEnum, AlgorithmEnum>(jointTypeEnum, algorithmEnum);
    // modify the settings of all the algorithms for the specified joint type
    for (Map.Entry<Pair<JointTypeEnum, AlgorithmEnum>, BooleanRef> mapEntry : _jointTypeAlgorithmPairToDiagnosticsEnabledMap.entrySet())
    {
      Pair<JointTypeEnum, AlgorithmEnum> jointTypeAlgorithmPair = mapEntry.getKey();
      if (jointTypeAlgorithmPair.equals(targetJointTypeAlgorithmPair))
      {
        BooleanRef currentValue = mapEntry.getValue();
        currentValue.setValue(diagnosticsEnabled);
      }
    }
  }

  /**
   * Apply the algorithm disabled flag passed in to the joint type and algorithm passed in.
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public void setSingleJointTypeSingleAlgorithmDisabled(JointTypeEnum jointTypeEnum,
                                                        AlgorithmEnum algorithmEnum,
                                                        boolean disabled)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithmEnum != null);
    Assert.expect(_jointTypeAlgorithmPairToDisabledMap != null);

    Pair<JointTypeEnum, AlgorithmEnum> targetJointTypeAlgorithmPair = new Pair<JointTypeEnum, AlgorithmEnum>(jointTypeEnum, algorithmEnum);
    // modify the settings of all the algorithms for the specified joint type
    for (Map.Entry<Pair<JointTypeEnum, AlgorithmEnum>, BooleanRef> mapEntry : _jointTypeAlgorithmPairToDisabledMap.entrySet())
    {
      Pair<JointTypeEnum, AlgorithmEnum> jointTypeAlgorithmPair = mapEntry.getKey();
      if (jointTypeAlgorithmPair.equals(targetJointTypeAlgorithmPair))
      {
        BooleanRef currentValue = mapEntry.getValue();
        currentValue.setValue(disabled);
      }
    }
  }

  /**
   * Apply the diagnostics enabled flag passed in to the algorithm passed in for all families.
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public void setAllJointTypesSingleAlgorithmDiagnosticsEnabled(AlgorithmEnum algorithmEnum, boolean diagnosticsEnabled)
  {
    Assert.expect(algorithmEnum != null);
    Assert.expect(_jointTypeAlgorithmPairToDiagnosticsEnabledMap != null);

    for (Map.Entry<Pair<JointTypeEnum, AlgorithmEnum>, BooleanRef> mapEntry : _jointTypeAlgorithmPairToDiagnosticsEnabledMap.entrySet())
    {
      Pair<JointTypeEnum, AlgorithmEnum> jointTypeAlgorithmPair = mapEntry.getKey();
      if (jointTypeAlgorithmPair.getSecond().equals(algorithmEnum))
      {
        BooleanRef currentValue = mapEntry.getValue();
        currentValue.setValue(diagnosticsEnabled);
      }
    }
  }

  /**
   * Apply the algorithm disabled flag passed in to the algorithm passed in for all families.
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public void setAllJointTypesSingleAlgorithmDisabled(AlgorithmEnum algorithmEnum, boolean disabled)
  {
    Assert.expect(algorithmEnum != null);
    Assert.expect(_jointTypeAlgorithmPairToDisabledMap != null);

    for (Map.Entry<Pair<JointTypeEnum, AlgorithmEnum>, BooleanRef> mapEntry : _jointTypeAlgorithmPairToDisabledMap.entrySet())
    {
      Pair<JointTypeEnum, AlgorithmEnum> jointTypeAlgorithmPair = mapEntry.getKey();
      if (jointTypeAlgorithmPair.getSecond().equals(algorithmEnum))
      {
        BooleanRef currentValue = mapEntry.getValue();
        currentValue.setValue(disabled);
      }
    }
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   */
  public void clearAllInspectionSettings()
  {
    clearAllAlgorithmDiagnosticSettings();
    clearAllAlgorithmDisabledSettings();
  }

  /**
   * @author Matt Wharton
   */
  public void clearAllAlgorithmDiagnosticSettings()
  {
    Assert.expect(_jointTypeAlgorithmPairToDiagnosticsEnabledMap != null);

    // Reset all the "diagnostics enabled" settings to 'false'.
    for (Map.Entry<Pair<JointTypeEnum, AlgorithmEnum>, BooleanRef> mapEntry : _jointTypeAlgorithmPairToDiagnosticsEnabledMap.entrySet())
    {
      mapEntry.getValue().setValue(false);
    }
  }

  /**
   * @author Matt Wharton
   */
  public void clearAllAlgorithmDisabledSettings()
  {
    Assert.expect(_jointTypeAlgorithmPairToDisabledMap != null);

    // Reset all the "algorithm disabled" settings to 'false'.
    for (Map.Entry<Pair<JointTypeEnum, AlgorithmEnum>, BooleanRef> mapEntry : _jointTypeAlgorithmPairToDisabledMap.entrySet())
    {
      mapEntry.getValue().setValue(false);
    }
  }

  /**
   * Return true if algorithms should be run in diagnostic mode.  Note that individual algorithms may not all be
   * set to run with diagnostics on.  To find out if an individual algorithm has diagnostics on, you need to call
   * the <code>areDiagnosticsOn(JointTypeEnum, Algorithm)</code> method.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public boolean areAnyDiagnosticsOn()
  {
    Assert.expect(_jointTypeAlgorithmPairToDiagnosticsEnabledMap != null);

    return _jointTypeAlgorithmPairToDiagnosticsEnabledMap.containsValue(BooleanRef.TRUE);
  }

  /**
   * Return true if the given joint type/algorithm should be run in diagnostic mode.
   *
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  public boolean areDiagnosticsOn(JointTypeEnum jointTypeEnum, AlgorithmEnum algorithmEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithmEnum != null);
    Assert.expect(_jointTypeAlgorithmPairToDiagnosticsEnabledMap != null);

    Pair<JointTypeEnum, AlgorithmEnum> targetPair = new Pair<JointTypeEnum, AlgorithmEnum>(jointTypeEnum, algorithmEnum);
    BooleanRef diagsEnabled = _jointTypeAlgorithmPairToDiagnosticsEnabledMap.get(targetPair);
    Assert.expect(diagsEnabled != null, "Joint Type: " + jointTypeEnum.toString() + " algorithm: " + algorithmEnum.getName());

    return diagsEnabled.getValue();
  }

  /**
   * Return true if algorithms should disabled.  Note that individual algorithms may not all be set to be disabled.
   * To find out if an individual algorithm is disabled, you need to call the
   * <code>isAlgorithmDisabled(JointTypeEnum, Algorithm)</code> method.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public boolean isAnyAlgorithmDisabled()
  {
    Assert.expect(_jointTypeAlgorithmPairToDisabledMap != null);

    return _jointTypeAlgorithmPairToDisabledMap.containsValue(BooleanRef.TRUE);
  }

  /**
   * Return true if the given algorithm is currently disabled.
   *
   * @author Andy Mechtenberg
   * @author Matt Wharton
   */
  public boolean isAlgorithmDisabled(JointTypeEnum jointTypeEnum, AlgorithmEnum algorithmEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithmEnum != null);
    Assert.expect(_jointTypeAlgorithmPairToDisabledMap != null);

    Pair<JointTypeEnum, AlgorithmEnum> targetPair = new Pair<JointTypeEnum, AlgorithmEnum>(jointTypeEnum, algorithmEnum);
    BooleanRef algorithmDisabled = _jointTypeAlgorithmPairToDisabledMap.get(targetPair);
    Assert.expect(algorithmDisabled != null, "Joint Type: " + jointTypeEnum.toString() + " algorithm: " + algorithmEnum.getName());

    return algorithmDisabled.getValue();
  }

  /**
   * @author Matt Wharton
   */
  public void setInspectedSubtypes(Panel panel)
  {
    Assert.expect(panel != null);

    _inspectedSubtypes.clear();
    _inspectedSubtypes.addAll(panel.getSubtypes());
  }

  /**
   * @author Matt Wharton
   */
  public void setInspectedSubtypes(Board board)
  {
    Assert.expect(board != null);

    _inspectedSubtypes.clear();
    _inspectedSubtypes.addAll(board.getSubtypes());
  }

  /**
   * @author Matt Wharton
   */
  public void setInspectedSubtypes(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    Project project = Project.getCurrentlyLoadedProject();
    Panel panel = project.getPanel();

    _inspectedSubtypes.clear();
    _inspectedSubtypes.addAll(panel.getSubtypes(jointTypeEnum));
  }

  /**
   * @author Matt Wharton
   */
  public void setInspectedSubtypes(Subtype subtype)
  {
    Assert.expect(subtype != null);

    _inspectedSubtypes.clear();
    _inspectedSubtypes.add(subtype);
  }

  /**
   * @author Matt Wharton
   */
  public void setInspectedSubtypes(Collection<Subtype> subtypes)
  {
    Assert.expect(subtypes != null);

    _inspectedSubtypes.clear();
    _inspectedSubtypes.addAll(subtypes);
  }

  /**
   * @author Matt Wharton
   */
  public void setInspectedSubtypes(Component component)
  {
    Assert.expect(component != null);

    _inspectedSubtypes.clear();
    _inspectedSubtypes.addAll(component.getComponentType().getSubtypes());
  }

  /**
   * @author Matt Wharton
   */
  public void setInspectedSubtypes(Pad pad)
  {
    Assert.expect(pad != null);

    _inspectedSubtypes.clear();
    _inspectedSubtypes.add(pad.getSubtype());
  }

  /**
   * @author Matt Wharton
   */
  public void clearInspectedSubtypes()
  {
    _inspectedSubtypes.clear();
  }

  /**
   * @author Matt Wharton
   */
  public boolean isSubtypeInspected(Subtype subtype)
  {
    Assert.expect(subtype != null);

    return _inspectedSubtypes.contains(subtype);
  }
}
