package com.axi.v810.business.testExec;

/**
 * @author Bill Darbie
 */
public class JointOrComponentFailureInspectionEvent extends InspectionEvent
{
  /**
   * @author Bill Darbie
   */
  public JointOrComponentFailureInspectionEvent()
  {
    super(InspectionEventEnum.JOINT_OR_COMPONENT_FAILURE);
  }
}
