package com.axi.v810.business.testExec;

/**
 * @author Bill Darbie
 */
public class PanelLoadedIntoMachineInspectionEvent extends InspectionEvent
{
  /**
   * @author Bill Darbie
   */
  public PanelLoadedIntoMachineInspectionEvent()
  {
    super(InspectionEventEnum.PANEL_LOADED_INTO_MACHINE);
  }
}
