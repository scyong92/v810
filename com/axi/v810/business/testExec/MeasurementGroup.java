package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.business.panelSettings.Subtype;

/**
 * Used by the <code>TestExecutionEngine</code> to keep track of sets of joint measurements that will be used
 * as a group to do classification.  The <code>TestExecutionEngine</code> should first add all the joints that belong
 * to the MeasurementGroup.  Then, it should begin adding <code>JointInspectionResults</code> containing the
 * measurements for each joint.  <code>addMeasurement</code> will return true when the MeasurementGroup has at least one
 * <code>JointInspectionResult</code> for each joint on its list.
 * @author Peter Esbensen
 */
public class MeasurementGroup
{
  private Collection<Pad> _padsInMeasurementGroup = new HashSet<Pad>(); // JointInspectionData objects
  private Collection<JointInspectionResult> _jointInspectionResults = new ArrayList<JointInspectionResult>(); // JointInspectionResult objects
  private Collection<Pad> _padsWithAtLeastOneMeasurement = new HashSet<Pad>();
  private Map<Pad, JointInspectionData> _padToJointInspectionDataMap = new HashMap<Pad,JointInspectionData>();
  private Subtype _subtype;
  private ComponentInspectionData _componentInspectionData = null;

  /**
   * @author Peter Esbensen
   */
  public void setSubtype(Subtype subtype)
  {
    Assert.expect(subtype != null);
    _subtype = subtype;
  }

  /**
   * @author Peter Esbensen
   */
  public MeasurementGroup()
  {
    // do nothing
  }

  /**
   * @return true when there is at least one measurement for each Joint in this MeasurementGroup
   *
   * @author Peter Esbensen
   */
  public synchronized boolean addJointInspectionResult(JointInspectionResult jointInspectionResult)
  {
    Assert.expect(jointInspectionResult != null);
    _jointInspectionResults.add(jointInspectionResult);

    _padsWithAtLeastOneMeasurement.add(jointInspectionResult.getJointInspectionData().getPad());

    return _padsWithAtLeastOneMeasurement.equals(_padsInMeasurementGroup);
  }

  /**
   * @author Patrick Lacz
   */
  public ComponentInspectionData getComponentInspectionData()
  {
    Assert.expect(_componentInspectionData != null);
    return _componentInspectionData;
  }

  /**
   * Get the list of JointInspectionResults.
   * @author Peter Esbensen
   */
  public Collection<JointInspectionResult> getJointMeasurements()
  {
    Assert.expect(_jointInspectionResults != null);
    return _jointInspectionResults;
  }


  /**
   * Add the given joint to the MeasurementGroup.
   * @author Peter Esbensen
   */
  public synchronized void addJointInspectionData(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);
    // if we change the definition of a MeasurementGroup in the future, we may need to change this subtype and componentInspectionData behaviour
    if (_subtype == null)
      _subtype = jointInspectionData.getSubtype();
    else
      Assert.expect(_subtype == jointInspectionData.getSubtype());

    if (_componentInspectionData == null)
      _componentInspectionData = jointInspectionData.getComponentInspectionData();
    else
      Assert.expect(_componentInspectionData == jointInspectionData.getComponentInspectionData());

    Pad pad = jointInspectionData.getPad();
    _padsInMeasurementGroup.add(pad);
    _padToJointInspectionDataMap.put(pad, jointInspectionData);
  }

  /**
   * @author Peter Esbensen
   */
  public synchronized Collection<Pad> getPads()
  {
    Assert.expect(_padsInMeasurementGroup != null);
    return _padsInMeasurementGroup;
  }

  public synchronized Collection<JointInspectionData> getInspectableJointInspectionDataObjects()
  {
    Assert.expect(_padToJointInspectionDataMap != null);

    Collection<JointInspectionData> jointInspectionDataObjects = new HashSet<JointInspectionData>();

    for (JointInspectionData jointInspectionData : _padToJointInspectionDataMap.values())
    {
      if (jointInspectionData.getPad().isInspected())
        jointInspectionDataObjects.add(jointInspectionData);
    }
    return jointInspectionDataObjects;
  }

  /**
   * @author Peter Esbensen
   */
  public Subtype getSubtype()
  {
    Assert.expect(_subtype != null);
    return _subtype;
  }

  /**
   * @author Peter Esbensen
   */
  void classifyMeasurementGroup(MeasurementGroup measurementGroup)
  {

  }
}
