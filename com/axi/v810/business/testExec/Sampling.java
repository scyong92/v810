package com.axi.v810.business.testExec;

import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import com.axi.util.*;


/**
 * This class handles determining when various tests should be run based on sampling
 * settings.
 *
 * - panel sampling
 *
 * @author Bill Darbie
 */
public class Sampling
{
  private static Sampling _instance;

  private Config _config;
  private PanelSamplingModeEnum _panelSamplingModeEnum;
  private int _skipEveryNthPanel = 0;
  private int _testEveryNthPanel = 0;

  private PanelSamplingModeEnum _prevPanelSamplingModeEnum;
  private int _prevSkipEveryNthPanel = -1;
  private int _prevTestEveryNthPanel = -1;

  private int _numPanels = 0;

  /**
   * @author Bill Darbie
   */
  public static synchronized Sampling getInstance()
  {
    if (_instance == null)
      _instance = new Sampling();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private Sampling()
  {
    _config = Config.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  boolean testPanel() throws XrayTesterException
  {
    getSamplingMode();

    boolean testPanel = false;
    if (_panelSamplingModeEnum.equals(PanelSamplingModeEnum.NO_SAMPLING))
      testPanel = true;
    else
    {
      ++_numPanels;

      if (_panelSamplingModeEnum.equals(PanelSamplingModeEnum.SKIP_EVERY_NTH_PANEL))
      {
        if (_skipEveryNthPanel == 0)
        {
          _numPanels = 0;
          testPanel = true;
        }
        else if (_numPanels >= _skipEveryNthPanel)
        {
          // reset the count and skip this panel
          _numPanels = 0;
          testPanel = false;
        }
        else
          testPanel = true;
      }
      else if (_panelSamplingModeEnum.equals(PanelSamplingModeEnum.TEST_EVERY_NTH_PANEL))
      {
        if (_testEveryNthPanel == 0)
        {
          _numPanels = 0;
          testPanel = false;
        }
        else if (_numPanels == 1)
        {
          // always test the first panel if _testEveryNthPanel is not 0
          testPanel = true;
        }
        else if (_numPanels > _testEveryNthPanel)
        {
          // reset the count and test this panel
          _numPanels = 1;
          testPanel = true;
        }
        else
          testPanel = false;
      }
      else
        Assert.expect(false);
    }

    return testPanel;
  }

  /**
   * @author Bill Darbie
   */
  boolean isEnabled()
  {
    boolean isEnabled = false;
    PanelSamplingModeEnum panelSamplingModeEnum = getPanelSamplingModeEnum();
    if (panelSamplingModeEnum.equals(PanelSamplingModeEnum.NO_SAMPLING))
    {
      isEnabled = false;
    }
    else if (panelSamplingModeEnum.equals(PanelSamplingModeEnum.SKIP_EVERY_NTH_PANEL))
    {
      isEnabled = true;
    }
    else if (panelSamplingModeEnum.equals(PanelSamplingModeEnum.TEST_EVERY_NTH_PANEL))
    {
      isEnabled = true;
    }
    else
      Assert.expect(false);

    return isEnabled;
  }

  /**
   * @author Bill Darbie
   */
  private void getSamplingMode() throws DatastoreException
  {
    _panelSamplingModeEnum = getPanelSamplingModeEnum();
    _skipEveryNthPanel = getSkipEveryNthPanel();
    _testEveryNthPanel = getTestEveryNthPanel();

    if (_prevPanelSamplingModeEnum != _panelSamplingModeEnum)
    {
      // if a config file setting has changed reset the current count.
      _numPanels = 0;
    }

    if (_panelSamplingModeEnum.equals(PanelSamplingModeEnum.NO_SAMPLING))
    {
      // do nothing
    }
    else if (_panelSamplingModeEnum.equals(PanelSamplingModeEnum.SKIP_EVERY_NTH_PANEL))
    {
      if (_prevSkipEveryNthPanel != _skipEveryNthPanel)
      {
        // if a config file setting has changed reset the current count.
        _numPanels = 0;
      }
    }
    else if (_panelSamplingModeEnum.equals(PanelSamplingModeEnum.TEST_EVERY_NTH_PANEL))
    {
      if (_prevTestEveryNthPanel != _testEveryNthPanel)
      {
        // if a config file setting has changed reset the current count.
        _numPanels = 0;
      }
    }
    else
      Assert.expect(false);

    _prevPanelSamplingModeEnum = _panelSamplingModeEnum;
    _prevSkipEveryNthPanel = _skipEveryNthPanel;
    _prevTestEveryNthPanel = _testEveryNthPanel;
  }

  /**
   * @author Bill Darbie
   */
  public void setPanelSamplingModeEnum(PanelSamplingModeEnum panelSamplingModeEnum) throws DatastoreException
  {
    Assert.expect(panelSamplingModeEnum != null);
    _config.setValue(SoftwareConfigEnum.PANEL_SAMPLING_MODE, panelSamplingModeEnum.getConfigFileString());
  }

  /**
   * @author Bill Darbie
   */
  public PanelSamplingModeEnum getPanelSamplingModeEnum()
  {
    String panelSamplingEnabledStr = _config.getStringValue(SoftwareConfigEnum.PANEL_SAMPLING_MODE);
    return PanelSamplingModeEnum.getPanelSamplingModeEnum(panelSamplingEnabledStr);
  }

  /**
   * @author Bill Darbie
   */
  public void setSkipEveryNthPanel(int everyNpanels) throws DatastoreException
  {
    Assert.expect(everyNpanels >= 0);
    _config.setValue(SoftwareConfigEnum.SKIP_EVERY_NTH_PANEL_FOR_SAMPLING, everyNpanels);
  }

  /**
   * @author Bill Darbie
   */
  public int getSkipEveryNthPanel()
  {
    return _config.getIntValue(SoftwareConfigEnum.SKIP_EVERY_NTH_PANEL_FOR_SAMPLING);
  }

  /**
   * @author Bill Darbie
   */
  public void setTestEveryNthPanel(int everyNpanels) throws DatastoreException
  {
    Assert.expect(everyNpanels >= 0);
    _config.setValue(SoftwareConfigEnum.TEST_EVERY_NTH_PANEL_FOR_SAMPLING, everyNpanels);
  }

  /**
   * @author Bill Darbie
   */
  public int getTestEveryNthPanel()
  {
    return _config.getIntValue(SoftwareConfigEnum.TEST_EVERY_NTH_PANEL_FOR_SAMPLING);
  }
}
