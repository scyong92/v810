package com.axi.v810.business.testExec;

import com.axi.util.*;

/**
 * @author chin-seong.kee
 */
public class AlignmentFailedWaitForUserInputEvent extends MessageInspectionEvent{

   private AlignmentBusinessException _alignmentException;
  /**
   * @author Chin Seong, Kee
   */
  public AlignmentFailedWaitForUserInputEvent(AlignmentBusinessException alignmentException)
  {
    super(alignmentException.getLocalizedString());
    _alignmentException = alignmentException;
  }

  /**
   * @author Matt Wharton
   */
  public AlignmentBusinessException getAlignmentException()
  {
    Assert.expect(_alignmentException != null);

    return _alignmentException;
  }
}
