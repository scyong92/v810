package com.axi.v810.business.testExec;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class PreInspectionScriptErrorBusinessException extends BusinessException
{
  /**
   * @author Bill Darbie
   */
  public PreInspectionScriptErrorBusinessException(String script, String errorMessage)
  {
    super(new LocalizedString("TESTEXEC_ERROR_FROM_PRE_INSPECTION_SCRIPT_EXCEPTION_KEY", new Object[]{script, errorMessage}));
    Assert.expect(script != null);
    Assert.expect(errorMessage != null);
  }
}
