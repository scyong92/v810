package com.axi.v810.business.testExec;

import com.axi.util.*;

/**
 * This InspectionEvent is fired by the TestExecutionEngine whenever an inspection concludes.
 *
 * @author Matt Wharton
 */
public class CompletedInspectionEvent extends InspectionEvent
{
  private TestExecutionTimer _testExecutionTimer;
  private int _numberOfProcessedJoints = -1;
  private int _numberOfDefectiveJoints = -1;
  private int _numberOfDefectiveComponents = -1;

  /**
   * @author Matt Wharton
   */
  public CompletedInspectionEvent(TestExecutionTimer testExecutionTimer,
                                  int numberOfProcessedJoints,
                                  int numberOfDefectiveJoints,
                                  int numberOfDefectiveComponents)
  {
    super(InspectionEventEnum.INSPECTION_COMPLETED);

    Assert.expect(testExecutionTimer != null);
    Assert.expect(numberOfProcessedJoints >= 0);
    Assert.expect(numberOfDefectiveJoints >= 0);
    Assert.expect(numberOfDefectiveJoints <= numberOfProcessedJoints);

    _testExecutionTimer = testExecutionTimer;
    _numberOfProcessedJoints = numberOfProcessedJoints;
    _numberOfDefectiveJoints = numberOfDefectiveJoints;
    _numberOfDefectiveComponents = numberOfDefectiveComponents;
  }

  /**
   * @author Bill Darbie
   */
  public TestExecutionTimer getTestExecutionTimer()
  {
    return _testExecutionTimer;
  }

  /**
   * @author Andy Mechtenberg
   */
  public long getInspectionTimeInMillis()
  {
    Assert.expect(_testExecutionTimer != null);

    return _testExecutionTimer.getElapsedFullInspectionTimeInMillis();
  }

  /**
   * @author Matt Wharton
   */
  public int getNumberOfProcessedJoints()
  {
    Assert.expect(_numberOfProcessedJoints != -1);

    return _numberOfProcessedJoints;
  }

  /**
   * @author Matt Wharton
   */
  public int getNumberOfDefectiveJoints()
  {
    Assert.expect(_numberOfDefectiveJoints != -1);

    return _numberOfDefectiveJoints;
  }

  /**
   * @author Peter Esbensen
   */
  public int getNumberOfDefectiveComponents()
  {
    Assert.expect(_numberOfDefectiveComponents != -1);

    return _numberOfDefectiveComponents;
  }

}
