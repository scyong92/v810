package com.axi.v810.business.testExec;

import com.axi.util.LocalizedString;

/**
 *
 * @author chin-seong.kee
 */
public class ApplyLearnedBoardShortProfileToOtherBoardEvent extends MessageInspectionEvent
{
  public ApplyLearnedBoardShortProfileToOtherBoardEvent(LocalizedString messageText)
  {
    super(messageText);
  }
}