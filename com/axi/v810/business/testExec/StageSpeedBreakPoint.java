package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 *
 * @author khang-wah.chnee, 2016-05-05, MVEDR (Multiple Velocity Extended Dynamic Range)
 */
public class StageSpeedBreakPoint extends ScanPassBreakPoint
{
  private int _regionsThatMustBeMarkedCompletePriorToHardwareChange = -1;
  private ScanPass _scanPass;
  private List<ScanPass> _scanPasses;
  private StageSpeedEnum _stageSpeedEnum;
  private MagnificationTypeEnum _magnificationTypeEnum;
  
  /**
   * @author Khang Wah, 2016-05-05, MVEDR 
   */
  public StageSpeedBreakPoint(ScanPass scanPass,
                              List<ScanPass> scanPasses,
                              int regionThatMustBeMarkComplete,
                              StageSpeedEnum stageSpeedEnum,
                              MagnificationTypeEnum magnificationTypeEnum)
  {
    super(scanPass, stageSpeedEnum.toDouble());
    
    Assert.expect(regionThatMustBeMarkComplete >= 0);
    Assert.expect(scanPasses != null);
    Assert.expect(stageSpeedEnum != null);
    Assert.expect(magnificationTypeEnum != null);
    
    _scanPass = scanPass;
    _scanPasses = scanPasses;
    _regionsThatMustBeMarkedCompletePriorToHardwareChange = regionThatMustBeMarkComplete;
    _stageSpeedEnum = stageSpeedEnum;
    _magnificationTypeEnum = magnificationTypeEnum;
  }
  
  /**
   * @author Khang Wah, 2016-05-05, MVEDR 
   */
  public void executeAction() throws XrayTesterException 
  {
    TimerUtil timer = new TimerUtil();
    timer.restart();
    PanelPositioner panelPositioner = PanelPositioner.getInstance();
    int numberOfStartingPulses = CameraTrigger.getInstance().getNumberOfRequiredSampleTriggers();
    final int distanceInNanoMetersPerPulse = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
    
    // Set the scan motion profile that corresponds to the magnification
    if(_magnificationTypeEnum.equals(MagnificationTypeEnum.LOW)) // low mag
    {
      if(_stageSpeedEnum.equals(StageSpeedEnum.ONE))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.PROFILE0));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.TWO))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE3));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.THREE))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE4));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.FOUR))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE5));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.FIVE))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE6));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.SIX))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE7));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.SEVEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE8));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.EIGHT))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE9));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.NINE))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE10));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.TEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE11));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.ELEVEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE12));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.TWELVE))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE13));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.THIRTEEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE14));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.FOURTEEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE15));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.FIFTEEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE16));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.SIXTEEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE17));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.SEVENTEEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.LOWMAG_PROFILE18));
    }
    else //high mag
    {
      if(_stageSpeedEnum.equals(StageSpeedEnum.ONE))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.PROFILE1));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.TWO))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE19));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.THREE))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE20));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.FOUR))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE21));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.FIVE))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE22));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.SIX))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE23));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.SEVEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE24));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.EIGHT))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE25));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.NINE))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE26));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.TEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE27));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.ELEVEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE28));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.TWELVE))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE29));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.THIRTEEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE30));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.FOURTEEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE31));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.FIFTEEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE32));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.SIXTEEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE33));
      else if (_stageSpeedEnum.equals(StageSpeedEnum.SEVENTEEN))
        panelPositioner.setMotionProfile(new MotionProfile(ScanMotionProfileEnum.HIGHMAG_PROFILE34));
    }
    
    panelPositioner.disableScanPath();
    // Swee Yee Wong - include starting pulse information for scanpass adjustment
    // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
    panelPositioner.enableScanPath(_scanPasses, distanceInNanoMetersPerPulse, numberOfStartingPulses);

    // Set the position pulse
    panelPositioner.setPositionPulse(distanceInNanoMetersPerPulse, numberOfStartingPulses);
    timer.stop();
    if (_debug)
      System.out.println("Configuring Speed Setting : "+_stageSpeedEnum.toDouble() +","+_scanPasses.size());
  }
  
  /**
   * @author Khang Wah, 2016-05-05, MVEDR
   */
  public int getNumberOfRegionsThatMustBeMarkedCompletePriorToExecutingAction() 
  {    
    return _regionsThatMustBeMarkedCompletePriorToHardwareChange;
  }
  
  /**
   * @author Khang Wah, 2016-05-05, MVEDR 
   */
  public void clearBreakPoint() 
  {
    // Do nothing
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public ScanPassBreakPointEnum getBreakPointEnum()
  {
    return ScanPassBreakPointEnum.STAGE_SPEED;
  }
}
