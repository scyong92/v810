package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;

/**
 * <p>Title: ImageSetCollectionTypeEnum</p>
 *
 * <p>Description: This enum will be used to define what type of image set collection the user wants to perform.</p>
 *
 * <p>Copyright: Copyright (c) 2008</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Laura Cormos
 * @version 1.0
 */
public class ImageSetCollectionTypeEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  private static Map<String, ImageSetCollectionTypeEnum> _nameToImageSetType = new HashMap<String, ImageSetCollectionTypeEnum>();
  private String _name;

  public static final ImageSetCollectionTypeEnum PRECISION = new ImageSetCollectionTypeEnum(++_index, "Precision");
  public static final ImageSetCollectionTypeEnum FINE_TUNING = new ImageSetCollectionTypeEnum(++_index, "FineTuning");
  public static final ImageSetCollectionTypeEnum EXPOSURE_LEARNING = new ImageSetCollectionTypeEnum(++_index, "ExposureLearning");
  /**
   * @param value int
   * @author Laura Cormos
   */
  private ImageSetCollectionTypeEnum(int value, String name)
  {
    super(value);
    Assert.expect(name != null);
    _name = name;
    _nameToImageSetType.put(_name, this);
  }

  /**
   * @author Laura Cormos
   */
  public String getName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author Laura Cormos
   */
  public static ImageSetCollectionTypeEnum getImageSetvColletionType(String name)
  {
    Assert.expect(name != null);

    ImageSetCollectionTypeEnum imageSetCollectionType = _nameToImageSetType.get(name);

    Assert.expect(imageSetCollectionType != null);
    return imageSetCollectionType;
  }
}
