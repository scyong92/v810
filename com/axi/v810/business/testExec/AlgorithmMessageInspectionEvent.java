package com.axi.v810.business.testExec;

import com.axi.util.LocalizedString;

/**
 * @author Andy Mechtenberg
 */
public class AlgorithmMessageInspectionEvent extends MessageInspectionEvent
{
  public AlgorithmMessageInspectionEvent(LocalizedString messageText)
  {
    super(messageText);
  }
}
