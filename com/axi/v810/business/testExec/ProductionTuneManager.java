package com.axi.v810.business.testExec;

import java.io.*;
import java.text.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class manages all the production tune information needed by test execution and the GUI
 * @author Andy Mechtenberg
 */
public class ProductionTuneManager
{
  private static ProductionTuneManager _instance;
  private ProductionTuneDataReader _productionTuneDataReader;
  private ProductionTuneDataWriter _productionTuneDataWriter;
  private ProductionTuneSettings _productionTuneSettings = null;
  
  private Collection<ProductionTuneData> _productionTuneData = null;
  private String _defaultLocation = null;

  private boolean _lastOperationWasAdd = false;
  private ProductionTuneData _lastOperationProductionTuneData = null;
  private ConfigObservable _configObservable = ConfigObservable.getInstance();

  private InspectionEventObservable _inspectionEventObservable;
  private EmailSender _emailSender = new EmailSender();

  /**
   * @author Andy Mechtenberg
   */
  public static synchronized ProductionTuneManager getInstance()
  {
    if (_instance == null)
    {
      _instance = new ProductionTuneManager();
    }

    return _instance;
  }

  /**
   * @author Andy Mechtenberg
   */
  private ProductionTuneManager()
  {
    _productionTuneDataReader = ProductionTuneDataReader.getInstance();
    _productionTuneDataWriter = ProductionTuneDataWriter.getInstance();
    _inspectionEventObservable = InspectionEventObservable.getInstance();
    _productionTuneSettings = null;
    _productionTuneSettings = ProductionTuneSettings.readSettings();
  }

  /**
   * @return a Collection of all production tune data entries
   *
   * @author Andy Mechtenberg
   */
  public String getDefaultLocation() throws DatastoreException
  {
    try
    {
      if ((_defaultLocation == null) || (_productionTuneData == null))
      {
        _defaultLocation = _productionTuneDataReader.getDefaultLocation();
        _productionTuneData =  _productionTuneDataReader.getProductionTuneData();
      }
    }
    catch (FileNotFoundDatastoreException fnfe)
    {
      _defaultLocation = "";
      _productionTuneData = new ArrayList<ProductionTuneData>();
      save();
    }

    return _defaultLocation;
  }

  /**
   * @return a Collection of all production tune data entries
   *
   * @author Andy Mechtenberg
   */
  public Collection<ProductionTuneData> getProductionTuneData() throws DatastoreException
  {
    try
    {
      if ((_defaultLocation == null) || (_productionTuneData == null))
      {
        _defaultLocation = _productionTuneDataReader.getDefaultLocation();
        _productionTuneData =  _productionTuneDataReader.getProductionTuneData();
      }
    }
    catch (FileNotFoundDatastoreException fnfe)
    {
      _defaultLocation = "";
      _productionTuneData = new ArrayList<ProductionTuneData>();
      save();
    }

    return _productionTuneData;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void save() throws DatastoreException
  {
    if ((_defaultLocation == null) || (_productionTuneData == null))
      return;
    _productionTuneDataWriter.write(_defaultLocation, _productionTuneData);
    _defaultLocation = null;
    _productionTuneData.clear();
    _productionTuneData = null;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setDefaultLocation(String defaultLocation)
  {
    Assert.expect(defaultLocation != null);
    _defaultLocation = defaultLocation;
    _configObservable.stateChanged(_productionTuneDataWriter);  // indicates a default location event
  }

  /**
   * @author Andy Mechtenberg
   */
  public void add(ProductionTuneData productionTuneData) throws DatastoreException
  {
    Assert.expect(productionTuneData != null);

    if (_defaultLocation == null)
      _defaultLocation = "";
    if (_productionTuneData == null)
      _productionTuneData = new ArrayList<ProductionTuneData>();

    if (productionTuneData.getProjectName().length() > 0)
    {
      Iterator<ProductionTuneData> it = _productionTuneData.iterator();
      while (it.hasNext())
      {
        ProductionTuneData pda = it.next();
        if (pda.getProjectName().equalsIgnoreCase(productionTuneData.getProjectName()))
        {
          Assert.expect(false, "Project already exists: " + pda.getProjectName());
          return;
        }
      }
    }
    _productionTuneData.add(productionTuneData);
    _lastOperationProductionTuneData = productionTuneData;
    _lastOperationWasAdd = true;
    _configObservable.stateChanged(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  public void remove(ProductionTuneData productionTuneDataToRemove) throws DatastoreException
  {
    Assert.expect(productionTuneDataToRemove != null);

    Collection<ProductionTuneData> _productionTuneData = getProductionTuneData();
    
    // XCR-3007 Assert when press undo after add production tuning recipe - Cheah Lee Herng 23-Oct-2015
    // We implement Comparable for ProductionTuneData object so that we can compare the list directly
    Iterator<ProductionTuneData> iterator = _productionTuneData.iterator();
    while(iterator.hasNext())
    {
      if (iterator.next().getProjectName().equalsIgnoreCase(productionTuneDataToRemove.getProjectName()))
      {
        iterator.remove();
        break;
      }
    }

    _lastOperationProductionTuneData = productionTuneDataToRemove;
    _lastOperationWasAdd = false;
    _configObservable.stateChanged(this);
    return;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean wasLastOperationAdd()
  {
    Assert.expect(_lastOperationProductionTuneData != null);
    return _lastOperationWasAdd;
  }

  /**
   * @author Andy Mechtenberg
   */
  public ProductionTuneData getLastOperationData()
  {
    Assert.expect(_lastOperationProductionTuneData != null);
    return _lastOperationProductionTuneData;
  }

  /**
   * @author Andy Mechtenberg
   */
  private void initializeIfNecessary() throws DatastoreException
  {
    getDefaultLocation();
    getProductionTuneData();
    _productionTuneSettings = null;
    _productionTuneSettings = ProductionTuneSettings.readSettings();
  }

//  /**
//   * @author Andy Mechtenberg
//   */
//  public void productionInspectionWasRun(String projectName) throws DatastoreException
//  {
//    Assert.expect(projectName != null);
//
//
//    initializeIfNecessary();
//    Iterator<ProductionTuneData> it = _productionTuneData.iterator();
//    while(it.hasNext())
//    {
//      ProductionTuneData pda = it.next();
//      if (pda.getProjectName().equalsIgnoreCase(projectName))
//      {
//        // at least found the project
//        int currentCount = pda.getCurrentCount();
//        if (currentCount > 0)
//        {
//          currentCount--;
//          pda.setCurrentCount(currentCount);
//          System.out.println("productionInspectionWasrun: " + projectName);
//          // save changes
//          save();
//        }
//        return;
//      }
//    }
//  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isProjectEnabledForProductionTune(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

//    System.out.println("isProjectEnabledCalled: " + projectName);

    initializeIfNecessary();
    Iterator<ProductionTuneData> it = _productionTuneData.iterator();
    while(it.hasNext())
    {
      ProductionTuneData pda = it.next();
      if (pda.getProjectName().equalsIgnoreCase(projectName))
      {
        // at least found the project
        int currentCount = pda.getCurrentCount();
        if (currentCount > 0)
          return true;
        else
          return false;  // found the project, but it is NOT enabled
      }
    }
    return false;  // didn't find the project, so by definition it is NOT enabled
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getResultsDestination(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    initializeIfNecessary();
    if (_productionTuneData == null)
    {
      getProductionTuneData();
    }
    Iterator<ProductionTuneData> it = _productionTuneData.iterator();
    while (it.hasNext())
    {
      ProductionTuneData pda = it.next();
      if (pda.getProjectName().equalsIgnoreCase(projectName))
      {
        // at least found the project
        return pda.getResultsDestination();
      }
    }
    Assert.expect(false, "Couldn't find results dest for " + projectName);
    return ""; // didn't find the project
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getImageDestination(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    initializeIfNecessary();
    if (_productionTuneData == null)
    {
      getProductionTuneData();
    }
    Iterator<ProductionTuneData> it = _productionTuneData.iterator();
    while (it.hasNext())
    {
      ProductionTuneData pda = it.next();
      if (pda.getProjectName().equalsIgnoreCase(projectName))
      {
        // at least found the project
        return pda.getImageDestination();
      }
    }
    Assert.expect(false, "Couldn't find image dest for " + projectName);
    return ""; // didn't find the project
  }


  /**
   * @author Andy Mechtenberg
   */
  public void productionWasRun(String projectName, String serialNumber, int numberOfDefects) throws XrayTesterException
  {
    Assert.expect(projectName != null);
    Assert.expect(serialNumber != null);
    Assert.expect(numberOfDefects >= 0);

//    System.out.println("Defect Notification: " + projectName + " S/N: " + serialNumber + " defects: " + numberOfDefects);

    initializeIfNecessary();
    Iterator<ProductionTuneData> it = _productionTuneData.iterator();
    while (it.hasNext())
    {
      ProductionTuneData pda = it.next();
      if (pda.getProjectName().equalsIgnoreCase(projectName))
      {
        int currentCount = pda.getCurrentCount();
        // found the project
        if (currentCount > 0)
        {
          currentCount--;
          pda.setCurrentCount(currentCount);
//          System.out.println("productionInspectionWasrun: " + projectName);
          // save changes
          save();
          return;
        }
        else  // current count is zero, check to see if we should re-enable
        {
          if (numberOfDefects >= pda.getReEnableThreshold() && (pda.getReEnableCount() > 0))  // if re-enable count is zero, do nothing
          {
            // tripping the re-enable threshold!

            // send message to production operator
            // we are skipping this panel because of panel sampling
            LocalizedString localizedString = new LocalizedString("TESTEXEC_RESPROC_PRODUCTION_TUNE_RE_ENABLE_NOTIFY_KEY",
                                                                  new Object[] {projectName, pda.getReEnableThreshold()});
            MessageInspectionEvent event = new MessageInspectionEvent(localizedString);
            _inspectionEventObservable.sendEventInfo(event);

            // send email!
            sendNotificationEmail(pda, serialNumber, numberOfDefects);
            pda.setCurrentCount(pda.getReEnableCount());
            // save changes
            save();
          }
          return;
        }
      }
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void sendNotificationEmail(ProductionTuneData pda, String serialNumber, int numberOfDefects) throws XrayTesterException
  {
    Assert.expect(pda != null);
    Assert.expect(serialNumber != null);

    String smtpServer = Config.getInstance().getStringValue(SoftwareConfigEnum.SMTP_SERVER);
    String toAddresses = pda.getNotificationEmailAddress();

    // error checking -- can't send email if either the smtp server or the email address is empty
    if ((smtpServer.length() == 0) || (toAddresses.length() == 0))
      return;

    String projectName = pda.getProjectName();
    ProjectSummary projectSummary = ProjectSummaryReader.getInstance().read(projectName);
    String projectVersion = new Double(projectSummary.getVersion()).toString();

    long dateInMils = System.currentTimeMillis();
    Date date = new Date(dateInMils);
    DateFormat dateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
    String dateString = dateFormat.format(date);
    String systemType = XrayTester.getInstance().getMachineDescription();

    LocalizedString subjectMessage = new LocalizedString("TESTEXEC_RESPROC_PRODUCTION_TUNE_EMAIL_SUBJECT_KEY", new Object[]
                                                         {projectName});
    LocalizedString contentMessage = new LocalizedString("TESTEXEC_RESPROC_PRODUCTION_TUNE_EMAIL_CONTENT_KEY", new Object[]
                                                         {dateString,
                                                          projectName,
                                                          projectVersion,
                                                          systemType,
                                                          serialNumber,
                                                          numberOfDefects,
                                                          pda.getReEnableThreshold(),
                                                          pda.getReEnableCount(),
                                                          pda.getImageDestination(),
                                                          pda.getResultsDestination()});

    try
    {
      /** @todo return email address???? */
//      System.out.println("Sending EMAIL to : " + toAddresses);
      _emailSender.sendEmail(smtpServer,
                             "v810noReply@xxx.com", // return address - what should this be?
                             toAddresses,
                             StringLocalizer.keyToString(subjectMessage),
                             StringUtil.format(StringLocalizer.keyToString(contentMessage), 70));
    }
    catch (IOException ex)
    {
      CannotSendEmailBusinessException mex = new CannotSendEmailBusinessException(ex.getMessage());
      mex.initCause(ex);
      throw mex;
    }
  }

  /**
   * @return the _productionTuneSettings
   * @author Wei Chin
   */
  public ProductionTuneSettings getProductionTuneSettings()
  {
    return _productionTuneSettings;
  }
}
