package com.axi.v810.business.testExec;

import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import com.axi.util.*;
import java.util.*;
import java.io.*;

/**
 * This class handles determining when various tests should be run based on FalseCallMonitoring
 * settings.
 *
 * - panel sampling
 *
 * @author Jack Hwee
 */
public class FalseCallMonitoring
{
  private static FalseCallMonitoring _instance;

  private Config _config;
  private PanelSamplingModeEnum _panelSamplingModeEnum;
  private int _skipEveryNthPanel = 0;
  private int _testEveryNthPanel = 0;

  private PanelSamplingModeEnum _prevPanelSamplingModeEnum;
  private int _prevSkipEveryNthPanel = -1;
  private int _prevTestEveryNthPanel = -1;

  private int _numPanels = 0;
  
  private boolean _isFalseCallTriggeringTriggered = false;
  private int _consecutiveCount = 0;
  private int _totalInspection = 0;
  private int _totalSampleTriggering = 0;
  private static int _falseCallTriggeringError = 0;
  private static int _MAX_ERROR_TRIGGER = 1;
  private static int _CONSECUTIVE_COUNT_TRIGGER = 2;
  private static int _TOTAL_SAMPLE_TRIGGER = 3;
  
  private java.util.LinkedList<String> _logFileDataValues = null;
  private CSVFileWriterAxi _CSVFileWriterAxi = null;
  
  //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
  private int _currentDynamicCallDefectThreshold = 0;
  private final static List<String> _FALSE_CALL_TRIGGERING_LOG_COLUMN_HEADER_LIST = Arrays.asList(StringLocalizer.keyToString("CFGUI_FCT_TIME_STAMP_COLUMN_HEADER_KEY"), 
                                                                                                  StringLocalizer.keyToString("CFGUI_FCT_PROJECT_COLUMN_HEADER_KEY"),
                                                                                                  StringLocalizer.keyToString("CFGUI_FCT_SERIAL_NUMBER_COLUMN_HEADER_KEY"),
                                                                                                  StringLocalizer.keyToString("CFGUI_FCT_DEFECT_TYPE_COLUMN_HEADER_KEY"), 
                                                                                                  StringLocalizer.keyToString("CFGUI_FCT_TOTAL_PINS_PROCESSED_COLUMN_HEADER_KEY"),
                                                                                                  StringLocalizer.keyToString("CFGUI_FCT_TOTAL_DEFECTIVE_PINS_COLUMN_HEADER_KEY"), 
                                                                                                  StringLocalizer.keyToString("CFGUI_FCT_PPM_PINS_COLUMN_HEADER_KEY"), 
                                                                                                  StringLocalizer.keyToString("CFGUI_FCT_FAIL_MODE_COLUMN_HEADER_KEY"), 
                                                                                                  StringLocalizer.keyToString("CFGUI_FCT_MAX_NUMBER_OF_ERRORS_COLUMN_HEADER_KEY"), 
                                                                                                  StringLocalizer.keyToString("CFGUI_FCT_USER_NAME_COLUMN_HEADER_KEY"), 
                                                                                                  StringLocalizer.keyToString("CFGUI_FCT_REMARK_COLUMN_HEADER_KEY"));

  /**
   * @author Bill Darbie
   */
  public static synchronized FalseCallMonitoring getInstance()
  {
    if (_instance == null)
      _instance = new FalseCallMonitoring();

    return _instance;
  }

  /**
   * @author Jack Hwee
   */
  private FalseCallMonitoring()
  {
    _config = Config.getInstance();
  }
  
   /**
   * @author Jack Hwee
   */
  public boolean isFalseCallMonitoringModeEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.FALSE_CALL_MONITORING_ENABLED);
  }
  
   /**
   * @author Jack Hwee
   */
  public void setFalseCallMonitoringMode(boolean falseCallMonitoringMode) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.FALSE_CALL_MONITORING_ENABLED, falseCallMonitoringMode);
  }
  
  /**
   * @author Cheah Lee Herng
   * @return 
   */
  public boolean isEnforceAdministratorLogin()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.ENFORCE_ADMINISTRATOR_LOGIN);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setEnforceAdministratorLogin(boolean enforceAdministratorLogin) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.ENFORCE_ADMINISTRATOR_LOGIN, enforceAdministratorLogin);
  }
  
  /**
   * @author Cheah Lee Herng
   * @return 
   */
  public boolean isAutoDeleteProductionResults()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.AUTO_DELETE_PRODUCTION_RESULTS);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setAutoDeleteProductionResults(boolean autoDeleteProductionResults) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.AUTO_DELETE_PRODUCTION_RESULTS, autoDeleteProductionResults);
  }

  /**
   * @author Bill Darbie
   */
  boolean testPanel() throws XrayTesterException
  {
    getSamplingMode();

    boolean testPanel = false;
    if (_panelSamplingModeEnum.equals(PanelSamplingModeEnum.NO_SAMPLING))
      testPanel = true;
    else
    {
      ++_numPanels;

      if (_panelSamplingModeEnum.equals(PanelSamplingModeEnum.SKIP_EVERY_NTH_PANEL))
      {
        if (_skipEveryNthPanel == 0)
        {
          _numPanels = 0;
          testPanel = true;
        }
        else if (_numPanels >= _skipEveryNthPanel)
        {
          // reset the count and skip this panel
          _numPanels = 0;
          testPanel = false;
        }
        else
          testPanel = true;
      }
      else if (_panelSamplingModeEnum.equals(PanelSamplingModeEnum.TEST_EVERY_NTH_PANEL))
      {
        if (_testEveryNthPanel == 0)
        {
          _numPanels = 0;
          testPanel = false;
        }
        else if (_numPanels == 1)
        {
          // always test the first panel if _testEveryNthPanel is not 0
          testPanel = true;
        }
        else if (_numPanels > _testEveryNthPanel)
        {
          // reset the count and test this panel
          _numPanels = 1;
          testPanel = true;
        }
        else
          testPanel = false;
      }
      else
        Assert.expect(false);
    }

    return testPanel;
  }

  /**
   * @author Bill Darbie
   */
  boolean isEnabled()
  {
    boolean isEnabled = false;
    PanelSamplingModeEnum panelSamplingModeEnum = getPanelSamplingModeEnum();
    if (panelSamplingModeEnum.equals(PanelSamplingModeEnum.NO_SAMPLING))
    {
      isEnabled = false;
    }
    else if (panelSamplingModeEnum.equals(PanelSamplingModeEnum.SKIP_EVERY_NTH_PANEL))
    {
      isEnabled = true;
    }
    else if (panelSamplingModeEnum.equals(PanelSamplingModeEnum.TEST_EVERY_NTH_PANEL))
    {
      isEnabled = true;
    }
    else
      Assert.expect(false);

    return isEnabled;
  }

  /**
   * @author Bill Darbie
   */
  private void getSamplingMode() throws DatastoreException
  {
    _panelSamplingModeEnum = getPanelSamplingModeEnum();
  //  _skipEveryNthPanel = getSkipEveryNthPanel();
    _testEveryNthPanel = getTestEveryNthPanel();

    if (_prevPanelSamplingModeEnum != _panelSamplingModeEnum)
    {
      // if a config file setting has changed reset the current count.
      _numPanels = 0;
    }

    if (_panelSamplingModeEnum.equals(PanelSamplingModeEnum.NO_SAMPLING))
    {
      // do nothing
    }
    else if (_panelSamplingModeEnum.equals(PanelSamplingModeEnum.SKIP_EVERY_NTH_PANEL))
    {
      if (_prevSkipEveryNthPanel != _skipEveryNthPanel)
      {
        // if a config file setting has changed reset the current count.
        _numPanels = 0;
      }
    }
    else if (_panelSamplingModeEnum.equals(PanelSamplingModeEnum.TEST_EVERY_NTH_PANEL))
    {
      if (_prevTestEveryNthPanel != _testEveryNthPanel)
      {
        // if a config file setting has changed reset the current count.
        _numPanels = 0;
      }
    }
    else
      Assert.expect(false);

    _prevPanelSamplingModeEnum = _panelSamplingModeEnum;
    _prevSkipEveryNthPanel = _skipEveryNthPanel;
    _prevTestEveryNthPanel = _testEveryNthPanel;
  }

  /**
   * @author Bill Darbie
   */
  public void setPanelSamplingModeEnum(PanelSamplingModeEnum panelSamplingModeEnum) throws DatastoreException
  {
    Assert.expect(panelSamplingModeEnum != null);
    _config.setValue(SoftwareConfigEnum.PANEL_SAMPLING_MODE, panelSamplingModeEnum.getConfigFileString());
  }

  /**
   * @author Bill Darbie
   */
  public PanelSamplingModeEnum getPanelSamplingModeEnum()
  {
    String panelSamplingEnabledStr = _config.getStringValue(SoftwareConfigEnum.PANEL_SAMPLING_MODE);
    return PanelSamplingModeEnum.getPanelSamplingModeEnum(panelSamplingEnabledStr);
  }

  /**
   * @author Jack Hwee
   */
  public void setConsecutiveCount(int consecutiveCount) throws DatastoreException
  {
    Assert.expect(consecutiveCount >= 0);
    _config.setValue(FalseCallTriggeringConfigEnum.CONSECUTIVE_COUNT, consecutiveCount);
  }

  /**
   * @author Jack Hwee
   */
  public int getConsecutiveCount()
  {
    return _config.getIntValue(FalseCallTriggeringConfigEnum.CONSECUTIVE_COUNT);
  }
  
   /**
   * @author Jack Hwee
   */
  public void setTotalInspection(int totalCount) throws DatastoreException
  {
    Assert.expect(totalCount >= 0);
    _config.setValue(FalseCallTriggeringConfigEnum.TOTAL_INSPECTION, totalCount);
  }

  /**
   * @author Jack Hwee
   */
  public int getTotalInspection()
  {
    return _config.getIntValue(FalseCallTriggeringConfigEnum.TOTAL_INSPECTION);
  }
  
   /**
   * @author Jack Hwee
   */
  public void setTotalSampleTriggering(int totalSampleTriggering) throws DatastoreException
  {
    Assert.expect(totalSampleTriggering >= 0);
    _config.setValue(FalseCallTriggeringConfigEnum.TOTAL_SAMPLE_TRIGGERING, totalSampleTriggering);
  }

  /**
   * @author Jack Hwee
   */
  public int getTotalSampleTriggering()
  {
    return _config.getIntValue(FalseCallTriggeringConfigEnum.TOTAL_SAMPLE_TRIGGERING);
  }
  
  /**
   * @author Jack Hwee
   */
  public void setMaximumErrorTrigger(int maximumErrorTrigger) throws DatastoreException
  {
    Assert.expect(maximumErrorTrigger >= 0);
    _config.setValue(FalseCallTriggeringConfigEnum.MAXIMUM_ERROR_TRIGGER, maximumErrorTrigger);
  }

  /**
   * @author Jack Hwee
   */
  public int getMaximumErrorTrigger()
  {
    return _config.getIntValue(FalseCallTriggeringConfigEnum.MAXIMUM_ERROR_TRIGGER);
  }
  
   /**
   * @author Jack Hwee
   */
  public void setDynamicCallNumber(List<String> dynamicCallNumberList) throws DatastoreException
  {
    Assert.expect(dynamicCallNumberList != null);
 
    _config.setValue(FalseCallTriggeringConfigEnum.DYNAMIC_CALL_NUMBER, dynamicCallNumberList);  
  }

  /**
   * @author Jack Hwee
   */
  public List<String> getDynamicCallNumber()
  {
    return _config.getStringListValue(FalseCallTriggeringConfigEnum.DYNAMIC_CALL_NUMBER);
  }

  /**
   * @author Bill Darbie
   */
  public void setTestEveryNthPanel(int everyNpanels) throws DatastoreException
  {
    Assert.expect(everyNpanels >= 0);
    _config.setValue(SoftwareConfigEnum.TEST_EVERY_NTH_PANEL_FOR_SAMPLING, everyNpanels);
  }

  /**
   * @author Bill Darbie
   */
  public int getTestEveryNthPanel()
  {
    return _config.getIntValue(SoftwareConfigEnum.TEST_EVERY_NTH_PANEL_FOR_SAMPLING);
  }
  
    /**
   * @author Jack Hwee
   */
  public void triggerFalseCallTriggering()
  {
    setFalseCallTriggeringTriggered(true);

    //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
    resetAllCount();
  }
  
   /**
   * @author Jack Hwee
   */
  public void setLogFileData(java.util.LinkedList<String> dataValues)
  {
     Assert.expect(dataValues != null);
    
     _logFileDataValues = new java.util.LinkedList<String>();
     
     for (int i = 0; i < dataValues.size(); i++)
     {
         String data = dataValues.get(i);
         _logFileDataValues.add(data);
     }
  }
  
   /**
   * @author Jack Hwee
   */
  public java.util.LinkedList<String> getLogFileData()
  {   
     return _logFileDataValues;
  }
  
    /**
   * @author Jack Hwee
   */
  public void setCSVFileWriterAxi(CSVFileWriterAxi csvFileWriterAxi)
  {
     Assert.expect(csvFileWriterAxi != null);
    
     _CSVFileWriterAxi = csvFileWriterAxi;
  }
  
   /**
   * @author Jack Hwee
   */
  public CSVFileWriterAxi getCSVFileWriterAxi()
  {   
     return _CSVFileWriterAxi;
  }
  
   /**
   * @author Jack Hwee
   */
  public void setFalseCallTriggeringTriggered(boolean isTriggered)
  {
      _isFalseCallTriggeringTriggered = isTriggered;
  }
  
   /**
   * @author Jack Hwee
   */
  public boolean isFalseCallTriggeringTriggered()
  {
      return  _isFalseCallTriggeringTriggered;
  }
  
   /**
   * @author Jack Hwee
   */
  public static int getFalseCallTriggeringError()
  {
      return  _falseCallTriggeringError;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public void handleFalseCallTriggering(int pinsProcessed,
                                        int pinsDefective)                            
  {
    Assert.expect(pinsProcessed >= 0);
    Assert.expect(pinsDefective >= 0);

    setFalseCallTriggeringTriggered(false);
    _falseCallTriggeringError = 0;

    if (_logFileDataValues != null && _logFileDataValues.isEmpty() == false)
      _logFileDataValues.clear();

    java.util.List<String> dynamicCallNumberList = getDynamicCallNumber();
    
    if (dynamicCallNumberList.isEmpty())
      return;

    Map<Integer, Integer> dynamicCallMap = new TreeMap<Integer, Integer>(new AlphaNumericComparator(true));

    for (int i = 0; i <= dynamicCallNumberList.size(); i++)
    {
      dynamicCallMap.put(Integer.parseInt(dynamicCallNumberList.get(i)), Integer.parseInt(dynamicCallNumberList.get(i+1)));
      i++;
      if (i + 1 == dynamicCallNumberList.size())
        break;
    }     

    int defectNumber = 0;
     
    if (Config.getInstance().getIntValue(FalseCallTriggeringConfigEnum.DEFECT_THRESHOLD_TYPE) == FalseCallTriggeringConfigEnum.getDefectivePinsNumberThresholdType())
    {
      defectNumber = pinsDefective;   
    }
    else
    {
      defectNumber = Math.round(((float) pinsDefective / (float) pinsProcessed) * 1000000);
    }

    for (Map.Entry<Integer, Integer> entry : dynamicCallMap.entrySet())
    {
      int totalComponent = entry.getKey();
      int totalError = entry.getValue();
     
      if (pinsProcessed < totalComponent)
      {
        _totalInspection++;

        if (defectNumber > getMaximumErrorTrigger())
        {
          _falseCallTriggeringError = _MAX_ERROR_TRIGGER;
          triggerFalseCallTriggering();
        }
        else if (defectNumber > totalError)
        {            
          _consecutiveCount++;
          _totalSampleTriggering++;

          if (_totalInspection <= getTotalInspection())
          {
            if (_consecutiveCount >= getConsecutiveCount())
            {               
              _falseCallTriggeringError = _CONSECUTIVE_COUNT_TRIGGER;
              _currentDynamicCallDefectThreshold = totalError;
              triggerFalseCallTriggering();
            }
            else if (_totalSampleTriggering >= getTotalSampleTriggering())
            {            
              _falseCallTriggeringError = _TOTAL_SAMPLE_TRIGGER;
              _currentDynamicCallDefectThreshold = totalError;
              triggerFalseCallTriggering();
            }

            if (_totalInspection >= getTotalInspection())
            {
              resetAllCount();
            }
          }
          else
          {
            resetAllCount();
          }
        }
        else
        {
          _consecutiveCount = 0;
          if (_totalInspection >= getTotalInspection())
          {
            resetAllCount();
          }
        }
        break;
      }
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public void setFalseCallRemarkWrittenNeeded(boolean isFalseCallRemarkWrittenNeeded) throws DatastoreException
  {
    String falseCallRemarkTriggeredFilePath = FileName.getFalseCallRemarkTriggeredFullPath();
    
    try
    {
      if (isFalseCallRemarkWrittenNeeded)
      {
        PrintWriter writer = null;
        try
        {
          writer = new PrintWriter(falseCallRemarkTriggeredFilePath);
        }
        finally
        {
          if (writer != null)
            writer.close();
        }
      }
      else
      {
        if (FileUtilAxi.exists(falseCallRemarkTriggeredFilePath))
        {
          FileUtilAxi.delete(falseCallRemarkTriggeredFilePath);
        }
      }
    }
    catch(IOException ioe)
    {
      CannotOpenFileDatastoreException ex = new CannotOpenFileDatastoreException(falseCallRemarkTriggeredFilePath);
      ex.initCause(ioe);
      throw ex;
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public boolean isFalseCallRemarkWrittenNeeded()
  {
    return FileUtilAxi.exists(FileName.getFalseCallRemarkTriggeredFullPath());
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public List<String> getFalseCallTriggeringLogColumnHeaderList()
  {
    return _FALSE_CALL_TRIGGERING_LOG_COLUMN_HEADER_LIST;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public int getCurrentDynamicCallDefectThreshold()
  {
    return _currentDynamicCallDefectThreshold;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public void setDefectThresholdType(int defectThresholdType) throws DatastoreException
  {
    _config.setValue(FalseCallTriggeringConfigEnum.DEFECT_THRESHOLD_TYPE, defectThresholdType);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public int getDefectThresholdType()
  {
    return _config.getIntValue(FalseCallTriggeringConfigEnum.DEFECT_THRESHOLD_TYPE);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public boolean isEnforceWriteRemark()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.ENFORCE_WRITE_REMARK);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public void setEnforceWriteRemark(boolean enforceWriteRemark) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.ENFORCE_WRITE_REMARK, enforceWriteRemark);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  private void resetAllCount()
  {
    _consecutiveCount = 0;
    _totalInspection = 0;
    _totalSampleTriggering = 0; 
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public static int getMaxErrorTriggerErrorCode()
  {
    return _MAX_ERROR_TRIGGER;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public static int getConsecutiveCountTriggerErrorCode()
  {
    return _CONSECUTIVE_COUNT_TRIGGER;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public static int getTotalSampleTriggerErrorCode()
  {
    return _TOTAL_SAMPLE_TRIGGER;
  }
}
