package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * XCR2285: 2D Alignment on v810
 * @author Khaw Chek Hau
 */
public class Alignment2DDraggedRegionLocatedTooFarFromActualLocationBusinessException extends AlignmentBusinessException
{
  public Alignment2DDraggedRegionLocatedTooFarFromActualLocationBusinessException(TestSubProgram testSubProgram,
                                                                                    AlignmentGroup alignmentGroup,
                                                                                    double absoluteDraggedAlignmentOffsetDistanceInNanometers,
                                                                                    double maxDistanceBetweenLocatedAndDragged2DRegionCentroidsInNanometers,
                                                                                    String displayUnitInString)
  {
    super(testSubProgram, 
          new LocalizedString("BS_ALIGNMENT_2D_DRAGGED_REGION_LOCATED_TOO_FAR_FROM_ACTUAL_LOCATION_KEY",
                              new Object[]{testSubProgram.getMagnificationType().toString(), alignmentGroup.getName(), 
                                           absoluteDraggedAlignmentOffsetDistanceInNanometers,
                                           maxDistanceBetweenLocatedAndDragged2DRegionCentroidsInNanometers, displayUnitInString}));
  }
}
