package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * This InspectionEvent is fired by the TestExecutionEngine or StatisticalTuningEngine when there is a message to
 * display to the user.
 *
 * @author Sunit Bhalla
 */
public class MessageInspectionEvent extends InspectionEvent
{
  private LocalizedString _localizedString = null;

  public MessageInspectionEvent(LocalizedString localizedString)
  {
    super(InspectionEventEnum.MESSAGE);

    Assert.expect(localizedString != null);
    // image and inspectionRegion can be null

    _localizedString = localizedString;
  }

  /**
   * @author Bill Darbie
   */
  public String getMessage()
  {
    Assert.expect(_localizedString != null);
    return StringLocalizer.keyToString(_localizedString);
  }
}
