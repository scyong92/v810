package com.axi.v810.business.testExec;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Monitors the xray status. Short term shutdown xray after a period of inactivity
 * @author YingKat Choor
 */
public class AutoShortTermShutdownXrayProgressMonitor implements Observer
{
  private static AutoShortTermShutdownXrayProgressMonitor _instance;
  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance2();
  private LegacyXraySource _legacyXraySource;
  private HTubeXraySource _hTubeXraySource;
  //AbstractXraySource
  Config _config = Config.getInstance();
  private TimerUtil _timer = new TimerUtil();
  private static HardwareObservable _hardwareObservable;

  private boolean _isXrayOn;
  private boolean _isInnerBarrierClosed;
  private boolean _enableAutoShortTermShutdown;
  private long _currentTimeOutMilliSeconds;
  volatile private boolean _isMonitoring = false;
  
  private boolean _isAcquiringImage;

  // XCR-3733 Short Term shut down feature is not working after abort inspection
  private WorkerThread _abortWorkerThread = new WorkerThread("Test Execution Abort Worker Thread");

  /**
   * @author YingKat Choor
   */
  private AutoShortTermShutdownXrayProgressMonitor()
  {
    AbstractXraySource xraySource = AbstractXraySource.getInstance();

     //Config _config = Config.getInstance();

      String type = _config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE);
      if (type.equals("legacy"))
      {
        Assert.expect(xraySource instanceof LegacyXraySource);
        _legacyXraySource = (LegacyXraySource)xraySource;
      }
      else if (type.equals("standard"))
      {
        //Assert.expect(xraySource instanceof XraySource);
        //_legacyXraySource = (XraySource)xraySource;
      }
      else if (type.equals("htube"))
      {

        Assert.expect(xraySource instanceof HTubeXraySource);
        _hTubeXraySource = (HTubeXraySource)xraySource;

      }
      else
      {
        Assert.expect(false, "unknown xraySourceType from config file, HardwareConfigEnum bug?");
      }



    // We need to listen to certain hardware events. xRay and inner barrier event
    _hardwareObservable = HardwareObservable.getInstance();
    _hardwareObservable.addObserver(this);

    _currentTimeOutMilliSeconds = 0;
    _enableAutoShortTermShutdown = false;
    _isXrayOn = false;
    _isInnerBarrierClosed = false;
    _isAcquiringImage = false;
  }

  /**
   * @author YingKat Choor
   */
  public static synchronized AutoShortTermShutdownXrayProgressMonitor getInstance()
  {
    if (_instance == null)
        _instance = new AutoShortTermShutdownXrayProgressMonitor();

    return _instance;
  }

  /**
   * @author YingKat Choor
   */
  private void checkTimer()
  {
    long elapsedTime = _timer.getElapsedTimeInMillis();

    // if we have a number here > 24 hours there is a bug in the code!
    Assert.expect(elapsedTime < 24 * 60 * 60 * 1000);
    // let's check how we are doing
    if (elapsedTime > _currentTimeOutMilliSeconds)
    {
      try
      {
        if(_hTubeXraySource.isStartupRequired() == false)
        {
          if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
          {
            _legacyXraySource.offForServiceMode();
          }
          else if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
          {
            _hTubeXraySource.offForServiceMode();
          }
        }
      }
      catch (XrayTesterException xte)
      {
        _hardwareObservable.notifyObserversOfException(xte);
      }
      stopMonitoring();
    }
  }

  /**
   * set a flag to continue/stop monitoring
   * @author YingKat Choor
   */
  private void setIsMonitoring(boolean status)
  {
    _isMonitoring = status;
  }

  /**
   * @author YingKat Choor
   */
  private void startMonitoring()
  {
    if (UnitTest.unitTesting())
    {
      // makesure monitoring is run while doing unit testing
      // and the delay should not to long, let's fix it at 10000 ms
      _enableAutoShortTermShutdown = true;
    }
    else
    {
        if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
        {
            _enableAutoShortTermShutdown = _legacyXraySource.getUsesXrayAutoShortTermShutdownEnabled();
            //_enableAutoShortTermShutdown = _config.getBooleanValue(SoftwareConfigEnum.XRAY_AUTO_SHORT_TERM_SHUTDOWN_ENABLED);
        }
        if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
        {
            _enableAutoShortTermShutdown = _hTubeXraySource.getUsesXrayAutoShortTermShutdownEnabled();
            //_enableAutoShortTermShutdown = _config.getBooleanValue(SoftwareConfigEnum.XRAY_AUTO_SHORT_TERM_SHUTDOWN_ENABLED);
        }
        else
        {
            return;
        }
    }

    _currentTimeOutMilliSeconds = getTimeOutMilliSeconds();
    // if user does NOT enable this feature, just return
    //
    if (_enableAutoShortTermShutdown == false)
    {
      return;
    }

    // if is monitoring, no need to create a new thread for monitoring
    //
    if (_isMonitoring)
    {
      return;
    }
    setIsMonitoring(true);
    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public synchronized void run()
      {
        _timer.reset();
        _timer.start();

        while (_isMonitoring)
        {
          checkTimer();

          // wait 1 second
          try
          {
            wait(1000);
          }
          catch (InterruptedException ex)
          {
            // do nothing
          }
        }
      }
    });
  }

  /**
   * @author YingKat Choor
   */
  private synchronized void stopMonitoring()
  {
    setIsMonitoring(false);
  }

  /**
   * @author YingKat Choor
   */
  public synchronized void update(Observable observable, Object object)
  {
    Assert.expect(observable != null);

    if (observable instanceof HardwareObservable)
    {
      if (object instanceof HardwareEvent)
      {
        HardwareEvent event = (HardwareEvent)object;
        HardwareEventEnum eventEnum = event.getEventEnum();

        // XCR-3733 Short Term shut down feature is not working after abort inspection
        if (_isAcquiringImage)
        {
          _abortWorkerThread.invokeLater(new Runnable()
          {
            public void run()
            {
              if (TestExecution.getInstance().didUserAbort() && _isAcquiringImage)
              {
                _isAcquiringImage = false;
                startMonitoring();
              }
            }
          });
        }
        
        if (eventEnum instanceof XraySourceEventEnum)
        {
          if (event.isStart() == false && eventEnum.equals(XraySourceEventEnum.ON))
          {
            _isXrayOn = true;
            if (_isInnerBarrierClosed)
            {
              if (_isAcquiringImage == false)
              {
                startMonitoring();
              }
            }
          }
          else if (event.isStart() == false && eventEnum.equals(XraySourceEventEnum.POWER_OFF))
          {
            _isXrayOn = false;
            stopMonitoring();
          }
        }
        else if (eventEnum instanceof InnerBarrierEventEnum)
        {
          if (event.isStart() == false && eventEnum.equals(InnerBarrierEventEnum.BARRIER_OPEN))
          {
            _isInnerBarrierClosed = false;
            stopMonitoring();
          }
          if (event.isStart() == false && eventEnum.equals(InnerBarrierEventEnum.BARRIER_CLOSE))
          {
            _isInnerBarrierClosed = true;
            if (_isXrayOn == true)
            {
              if (_isAcquiringImage == false)
              {
                startMonitoring();
              }
            }
          }

        }
        else if (eventEnum instanceof TestExecutionEventEnum)
        {
          if (eventEnum.equals(TestExecutionEventEnum.INSPECT) ||
              eventEnum.equals(TestExecutionEventEnum.INSPECT_FOR_PRODUCTION) ||
              eventEnum.equals(TestExecutionEventEnum.ACQUIRE_OFFLINE_IMAGES) ||
              eventEnum.equals(TestExecutionEventEnum.ACQUIRE_VERIFICATION_IMAGES) ||
              eventEnum.equals(TestExecutionEventEnum.ACQUIRE_VIRTUAL_LIVE_IMAGES) ||
              eventEnum.equals(TestExecutionEventEnum.RUN_MANUAL_ALIGNMENT))
          {
            if (event.isStart())
            {
              stopMonitoring();

              _isAcquiringImage = true;
              
              // Make sure we turn on Xray and Inner Barrier for Image Acquisition
              //XCR-3090, Interlock break after panel failed to load/unload
              //turnOnXray();
            }
            else
            {
              startMonitoring();
              
              _isAcquiringImage = false;
            }
          }
        }
      }
    }
  }

  /**
   * get the time out period from config file
   * but fix a time for testing
   * @author YingKat Choor
   */
  public long getTimeOutMilliSeconds()
  {
   long currentTimeOutMilliSeconds = 0;
    if (UnitTest.unitTesting())
    {
      //the delay time for testing should not be too long, let's fix it at 10000 ms
      currentTimeOutMilliSeconds = 10000;
    }
    else
    {
       if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
       {
            currentTimeOutMilliSeconds = (long)(_legacyXraySource.getXrayAutoShortTermShutdownTimeoutMinutes()*60*1000);
            //currentTimeOutMilliSeconds = _config.getLongValue(SoftwareConfigEnum.XRAY_AUTO_SHORT_TERM_SHUTDOWN_TIMEOUT_MINUTES) * 1000;
       }
       else if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
       {
            currentTimeOutMilliSeconds = (long)(_hTubeXraySource.getXrayAutoShortTermShutdownTimeoutMinutes()*60*1000);
            //currentTimeOutMilliSeconds = _config.getLongValue(SoftwareConfigEnum.XRAY_AUTO_SHORT_TERM_SHUTDOWN_TIMEOUT_MINUTES) * 1000;
       }
    }
    return currentTimeOutMilliSeconds;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void turnOnXray()
  {
    try
    {
      if(_hTubeXraySource.isStartupRequired() == false)
      {
        if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("legacy"))
        {
          _legacyXraySource.on();
        }
        else if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
        {
          _hTubeXraySource.turnOnImagingXRays();
        }
      }
    }
    catch (XrayTesterException xte)
    {
      // Do nothing because if we have Xray interlock break, front-end will handle that.      
    }
  }
}
