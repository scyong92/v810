package com.axi.v810.business.testExec;

/**
 * @author Bill Darbie
 */
public class AcquiringImagesCompleteInspectionEvent extends InspectionEvent
{
  // completed normally (or due to some kind of exception)
  private boolean _normalExit;

  /**
   * @author Bill Darbie
   */
  public AcquiringImagesCompleteInspectionEvent(boolean normalExit)
  {
    super(InspectionEventEnum.IMAGE_ACQUISITION_COMPLETE);
    _normalExit = normalExit;
  }

  /**
   * @author Bill Darbie
   */
  public boolean didInspectionCompleteNormally()
  {
    return _normalExit;
  }
}
