package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;

/**
 * @author Kok Chun, Tan
 */
public class VelocityMapping
{
  private static  VelocityMapping _instance = null;
  private Config _config = Config.getInstance();
  private SystemTypeEnum _systemTypeEnum = XrayTester.getSystemType();
  private Map<String, VelocityMappingParameters> _lowMagVelocityToVelocityMappingParametersMap = new HashMap <String, VelocityMappingParameters> ();
  private Map<String, VelocityMappingParameters> _highMagVelocityToVelocityMappingParametersMap = new HashMap <String, VelocityMappingParameters> ();
  
  /**
   * @author Kok Chun, Tan
   */
  private VelocityMapping()
  {
    // nothing
  }

  /**
   * @author Kok Chun, Tan
   */
  public static synchronized VelocityMapping getInstance()
  {
    if (_instance == null)
    {
      _instance = new VelocityMapping();
    }
    return _instance;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void populateData() throws DatastoreException
  {
    String magnificationName = _config.getStringValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION);
    String velocityMappingFileFullPath = FileName.getVelocityMappingFullPath(magnificationName);
    _lowMagVelocityToVelocityMappingParametersMap = VelocityMappingFileReader.getInstance().readDataFile(velocityMappingFileFullPath);
    
    if (_config.getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED) || 
        _config.getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED))
    {
      magnificationName = _config.getStringValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_HIGH_MAGNIFICATION);
      velocityMappingFileFullPath = FileName.getVelocityMappingFullPath(magnificationName);
      _highMagVelocityToVelocityMappingParametersMap = VelocityMappingFileReader.getInstance().readDataFile(velocityMappingFileFullPath);
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public Set<VelocityMappingParameters> getVelocityMappingParameters(MagnificationTypeEnum mag, List<Double> velocityCombination)
  {
    Assert.expect(mag != null);
    Assert.expect(velocityCombination != null);
    Assert.expect(velocityCombination.size() >= 2); // minimum need 2 speed
    
    Set<VelocityMappingParameters> velocityMappingParametersList = new HashSet<VelocityMappingParameters> ();
    double slowestVelocity = velocityCombination.get(velocityCombination.size() - 1);
    for (int i = 0; i < velocityCombination.size() - 1; i++)
    {
      velocityMappingParametersList.add(getVelocityMappingParameters(mag, velocityCombination.get(i), slowestVelocity));
    }
    
    return velocityMappingParametersList;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private VelocityMappingParameters getVelocityMappingParameters(MagnificationTypeEnum mag, double firstVelocity, double secondVelocity)
  {
    Assert.expect(mag != null);
    
    VelocityMappingParameters parameters = null;
    String key = Integer.toString((int)(firstVelocity*10)) + Integer.toString((int)(secondVelocity*10));
    if (mag.equals(MagnificationTypeEnum.LOW))
      parameters = _lowMagVelocityToVelocityMappingParametersMap.get(key);
    else
      parameters = _highMagVelocityToVelocityMappingParametersMap.get(key);
    
    
    Assert.expect(parameters != null);
    return parameters;
  }
  
  /**
   * @author Yong Sheng CHuan
   */
  public void clear()
  {
    _highMagVelocityToVelocityMappingParametersMap.clear();
    _lowMagVelocityToVelocityMappingParametersMap.clear();
  }
}
