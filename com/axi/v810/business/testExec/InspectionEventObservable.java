package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;

/**
 *
 * @author Peter Esbensen
 * @author Matt Wharton
 * @author Patrick Lacz
 *
 * This object takes ownership of the Events that are sent into it. When it is finished notifying all observers, it will
 * call decrementReferenceCount on the Event. This approach does not work with ThreadSafeObservable!
 * See the comments in sendEventInfo.
 */
public class InspectionEventObservable extends Observable
{
  private static InspectionEventObservable _inspectionEventObservable = null;

  // Instance to the InspectionStateController for handling diagnostic pauses, etc.
  private InspectionStateController _inspectionStateController = InspectionStateController.getInstance();

  /**
   * @author George A. David
   */
  private InspectionEventObservable()
  {
    // Do nothing...
  }

  /**
   * Get the singleton (global) instance of this InspectionEventObservable
   *
   * @author Peter Esbensen
   */
  public static synchronized InspectionEventObservable getInstance()
  {
    if (_inspectionEventObservable == null)
      _inspectionEventObservable = new InspectionEventObservable();

    return _inspectionEventObservable;
  }

  /**
   * Sends event info to any objects that are registered Observers for this Observable, pausing
   * if necessary.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void sendEventInfo(InspectionEvent event)
  {
    Assert.expect(event != null);
    Assert.expect(_inspectionStateController != null);

    setChanged();
    notifyObservers(event);

    if (event instanceof AlgorithmDiagnosticInspectionEvent)
    {
      // Pause the inspection if required.
      _inspectionStateController.pauseIfRequired((AlgorithmDiagnosticInspectionEvent)event);
    }

    // The Observable takes ownership of the Event's lifetime when it is passed into it.
    // This decrement indicates that it is finished with it.
    // This approach DOES NOT WORK when the notifyObservers() call above is asynchronous (such as with ThreadSafeObservable).
    event.decrementReferenceCount();
  }

  /**
   * @author Patrick Lacz
   */
  protected void clearChanged()
  {
  }
}
