package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;

/**
 * XCR2285: 2D Alignment on v810
 * @author Khaw Chek Hau
 */
public class AlignmentFailedToFind2DAlignmentImageBusinessException extends AlignmentBusinessException
{
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public AlignmentFailedToFind2DAlignmentImageBusinessException(TestSubProgram testSubProgram,
    String alignment2DImageFileFullPath)
  {
    super(testSubProgram, new LocalizedString("BS_ALIGNMENT_FAILED_TO_FIND_2D_ALIGNMENT_IMAGE_KEY",
      new Object[]
      {
        testSubProgram.getMagnificationType().toString(),
        alignment2DImageFileFullPath      
      }));
  }
}
