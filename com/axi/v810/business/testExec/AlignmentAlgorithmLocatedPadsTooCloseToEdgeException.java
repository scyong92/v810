package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * This exception is thrown whenever the alignment pads are located too close to the edge of an alignment image.
 *
 * @author Matt Wharton
 */
public class AlignmentAlgorithmLocatedPadsTooCloseToEdgeException extends AlignmentBusinessException
{
  /**
   * @author Matt Wharton
   */
  AlignmentAlgorithmLocatedPadsTooCloseToEdgeException(TestSubProgram testSubProgram, AlignmentGroup alignmentGroup)
  {
    super(testSubProgram, new LocalizedString("BS_ALIGNMENT_ALGORITHM_LOCATED_PADS_TOO_CLOSE_TO_EDGE_KEY",
                                              new Object[]{testSubProgram.getMagnificationType().toString(), alignmentGroup.getName()}));
  }

  AlignmentAlgorithmLocatedPadsTooCloseToEdgeException(TestSubProgram testSubProgram, AlignmentGroup alignmentGroup, String failAlignmentImagePath)
  {
    super(testSubProgram, failAlignmentImagePath, new LocalizedString("BS_ALIGNMENT_ALGORITHM_LOCATED_PADS_TOO_CLOSE_TO_EDGE_KEY",
                                              new Object[]{testSubProgram.getMagnificationType().toString(), alignmentGroup.getName()}));
  }
}
