package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class ImageAcquisitionModeEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  private static Map<String, ImageAcquisitionModeEnum> _nameToImageSetType = new HashMap<String, ImageAcquisitionModeEnum>();
  private static List<ImageAcquisitionModeEnum> _allModes = new ArrayList<ImageAcquisitionModeEnum>();
  private String _name;

  public static final ImageAcquisitionModeEnum USE_HARDWARE_ALWAYS = new ImageAcquisitionModeEnum(++_index, StringLocalizer.keyToString("ATGUI_IMAGE_GENERATION_ALWAYS_USE_HARDWARE_KEY"));
  public static final ImageAcquisitionModeEnum USE_IMAGE_SETS = new ImageAcquisitionModeEnum(++_index, StringLocalizer.keyToString("ATGUI_IMAGE_GENERATION_ALWAYS_USE_IMAGE_SETS_KEY"));
  public static final ImageAcquisitionModeEnum USE_HARDWARE_WHEN_NECESSARY = new ImageAcquisitionModeEnum(++_index, StringLocalizer.keyToString("ATGUI_IMAGE_GENERATION_USE_HARDWARE_WHEN_NECESSARY_KEY"));


  /**
   * @author Andy Mechtenberg
   */
  private ImageAcquisitionModeEnum(int value, String name)
  {
    super(value);
    Assert.expect(name != null);
    _name = name;
    _nameToImageSetType.put(_name, this);
    _allModes.add(this);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static ImageAcquisitionModeEnum getImageAcquistionMode(String name)
  {
    Assert.expect(name != null);

    ImageAcquisitionModeEnum imageSetType = _nameToImageSetType.get(name);

    Assert.expect(imageSetType != null);
    return imageSetType;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static List<ImageAcquisitionModeEnum> getAllImageAcquisitionModes()
  {
    return _allModes;
  }
}
