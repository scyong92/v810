package com.axi.v810.business.testExec;

/**
 *
 * @author Administrator
 */
public class AcquiringOpticalImagesCompleteInspectionEvent extends InspectionEvent
{
    // completed normally (or due to some kind of exception)
    private boolean _normalExit;
    
    /**
     * @author Cheah Lee Herng 
     */
    public AcquiringOpticalImagesCompleteInspectionEvent(boolean normalExit)
    {
        super(InspectionEventEnum.OPTICAL_IMAGE_ACQUISITION_COMPLETE);
        _normalExit = normalExit;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public boolean didInspectionCompleteNormally()
    {
        return _normalExit;
    }
}
