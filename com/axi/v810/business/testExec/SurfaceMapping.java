package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import java.util.*;
import java.util.List;

/**
 *
 * @author zee-foo.hwee
 */
public class SurfaceMapping
{
  private static Config _config = Config.getInstance();
  private static final int MAXIMUM_PANEL_THICKNESS_FOR_OPTICAL_PROFILE_1_IN_NANOMETERS = (int)MathUtil.convertMillimetersToNanometers(3.00);
  private static SurfaceMapping _surfaceMapping;
  private BooleanLock _waitingForSurfaceMappingUserInput = new BooleanLock(false);
  private WorkerThread _surfaceMappingWorkerThread = new WorkerThread("surface mapping worker thread");
  private Project _project = null;
  PanelPositioner _panelPositioner;
  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  private java.awt.geom.AffineTransform _transformFromRendererCoords = new java.awt.geom.AffineTransform();
  //Motion profile to use when moving stage, use the fastest
  private PointToPointMotionProfileEnum _motionProfile = PointToPointMotionProfileEnum.PROFILE1;
  //private boolean _useCameraOne = false;
  private PspModuleEnum _moduleEnum;
  private StagePosition _currentStagePosition;
  private OpticalCameraRectangle _opticalCameraRectangle;
  private XrayTesterException _xrayTesterException;
  private static GreedySorterAlgorithm _greedySorterAlgorithm = GreedySorterAlgorithm.getInstance();
  private static TwoOptSorterAlgorithm _twoOptSorterAlgorithm = TwoOptSorterAlgorithm.getInstance();
  private static PspSettingEnum _settingEnum = PspSettingEnum.getEnum(Config.getInstance().getIntValue(HardwareConfigEnum.PSP_SETTING));
  
 /**
   * @author Jack Hwee
   */
  private SurfaceMapping()
  {
      // Do nothing
  }

  /**
   * @author Jack Hwee
   */
  public static synchronized SurfaceMapping getInstance()
  {
    if (_surfaceMapping == null)
    {
      _surfaceMapping = new SurfaceMapping();
    }
    return _surfaceMapping;
  }


  /**
   * @author Jack Hwee
   */
  private void waitForUserInput()
  {
    _waitingForSurfaceMappingUserInput.setValue(true);
    try
    {
      _waitingForSurfaceMappingUserInput.waitUntilFalse();
    }
    catch (InterruptedException ex)
    {
      // Do nothing ...
    }
  }

    /**
   * @author Jack Hwee
   */
  public void runToNextRegion()
  {
    _waitingForSurfaceMappingUserInput.setValue(false);
  }

    /**
   * @author Jack Hwee
   */
  public void cancelRunToNextRegion()
  {
   // _cancelSurfaceMapping = true;
    _waitingForSurfaceMappingUserInput.setValue(false);
  }

  /**
   * @author Jack Hwee
   */
  public void doRegionSurfaceMapping(OpticalCameraRectangle opticalCameraRectangle, 
                                     Board board) throws XrayTesterException
  {
    Assert.expect(opticalCameraRectangle != null);
    Assert.expect(board != null);
    
    _opticalCameraRectangle = opticalCameraRectangle;
    
    final java.awt.Rectangle rectangleInNanometers = opticalCameraRectangle.getRegion().getRectangle2D().getBounds();
    
    // Get currently loaded project
    _project = Project.getCurrentlyLoadedProject();
    
    // clear the XrayTesterException object first.
    _xrayTesterException = null;
    
    PanelCoordinate panelCoordinate = new PanelCoordinate();
    panelCoordinate.setLocation((int)rectangleInNanometers.getCenterX(), (int)rectangleInNanometers.getCenterY());
    
    com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
    PanelSettings panelSettings = panel.getPanelSettings();
    PanelRectangle bottomPanelRectangle = panelSettings.getBottomSectionOfOpticalLongPanel();
  
    int opticalFiducialStageCoordinateLocationXInNanometers = -1;
    int opticalFiducialStageCoordinateLocationYInNanometers = -1;
    
    if (bottomPanelRectangle.contains(panelCoordinate.getX(), panelCoordinate.getY()))
    {
      _moduleEnum = PspModuleEnum.FRONT;
      
      opticalFiducialStageCoordinateLocationXInNanometers = PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers();
      opticalFiducialStageCoordinateLocationYInNanometers = PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers();
    }
    else
    {
      _moduleEnum = PspModuleEnum.REAR;
    
      opticalFiducialStageCoordinateLocationXInNanometers = PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers();
      opticalFiducialStageCoordinateLocationYInNanometers = PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers();
    }
   
    // Before proceed, we get the flag for checkLatestProgram
    boolean isCheckLatestTestProgram = _project.isCheckLatestTestProgram();
    _project.setCheckLatestTestProgram(false);
    
    TestSubProgram surfaceMaptestSubProgram = getSurfaceMapTestSubProgram(opticalCameraRectangle);
    
    _project.setCheckLatestTestProgram(isCheckLatestTestProgram);
    
    final StagePosition stagePosition = OpticalMechanicalConversions.convertPanelCoordinateToStageCoordinate(surfaceMaptestSubProgram, 
                                                                                                             panelCoordinate, 
                                                                                                             surfaceMaptestSubProgram.getAggregateRuntimeAlignmentTransform(), 
                                                                                                             surfaceMaptestSubProgram.getPanelLocationInSystem(),
                                                                                                             PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationXInNanometers(),
                                                                                                             PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationYInNanometers(),
                                                                                                             opticalFiducialStageCoordinateLocationXInNanometers,
                                                                                                             opticalFiducialStageCoordinateLocationYInNanometers,
                                                                                                             new BooleanRef());
    
    final TestSubProgram finalTestSubProgram = surfaceMaptestSubProgram;

    _currentStagePosition = stagePosition;

    final boolean simulationMode = XrayTester.getInstance().isSimulationModeOn();
    final BooleanLock surfaceMapConfirmationLock = new BooleanLock(false);
    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (simulationMode == false)
          {
            // First, position the panel according to TestSubProgram
            if (finalTestSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
              PanelHandler.getInstance().movePanelToRightSide();
            else if (finalTestSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
              PanelHandler.getInstance().movePanelToLeftSide();
            else
              Assert.expect(false, "Invalid PanelLocationInSystemEnum");

            // Display white light prior to stage movement
            PspHardwareEngine.getInstance().turnOnLED(_moduleEnum);
            PspHardwareEngine.getInstance().displayWhiteLight(_moduleEnum);
            acquireSingleImageAndGenerateHeightMap();

            // Handle post operation
            PspImageAcquisitionEngine.getInstance().handlePostOpticalView(_moduleEnum, OpticalShiftNameEnum.WHITE_LIGHT);
          }
        }
        catch (final XrayTesterException xte)
        {
          _xrayTesterException = xte;
        }
        finally
        {
          //do nothing
          surfaceMapConfirmationLock.setValue(true);
        }
      }
    });
   
    try
    {
      surfaceMapConfirmationLock.waitUntilTrue();
    }
    catch (InterruptedException ex1)
    {
      Assert.logException(ex1);
    }
    finally
    {
      if (_xrayTesterException != null)
      {
        throw _xrayTesterException;
      }
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Ying-Huan.Chu
   */
  public TestSubProgram getSurfaceMapTestSubProgram(OpticalCameraRectangle opticalCameraRectangle)
  {
    Assert.expect(opticalCameraRectangle != null);
    
    _opticalCameraRectangle = opticalCameraRectangle;
    
    final java.awt.Rectangle rectangleInNanometers = _opticalCameraRectangle.getRegion().getRectangle2D().getBounds();
    
    // Get currently loaded project
    _project = Project.getCurrentlyLoadedProject();
    
    PanelCoordinate panelCoordinate = new PanelCoordinate();
    panelCoordinate.setLocation((int)rectangleInNanometers.getCenterX(), (int)rectangleInNanometers.getCenterY());
    
    com.axi.v810.business.panelDesc.Panel panel = _project.getPanel();
    PanelSettings panelSettings = panel.getPanelSettings();
    PanelRectangle bottomPanelRectangle = panelSettings.getBottomSectionOfOpticalLongPanel();
  
    _moduleEnum = bottomPanelRectangle.contains(panelCoordinate.getX(), panelCoordinate.getY()) ? PspModuleEnum.FRONT : PspModuleEnum.REAR; 
        
    TestSubProgram surfaceMapTestSubProgram = null;
    _project.setCheckLatestTestProgram(false);
    
    for (TestSubProgram testSubProgram : _project.getTestProgram().getAllInspectableTestSubPrograms())
    {
      if (testSubProgram.isSubProgramPerformSurfaceMap())
      {
        if (testSubProgram.getTestProgram().getProject().getPanel().getPanelSettings().isPanelBasedAlignment())
        {
          if (testSubProgram.getTestProgram().getProject().getPanel().getPanelSettings().isLongPanel())
          {
            if (testSubProgram.getImageableRegionInNanoMeters().contains(panelCoordinate.getX(), panelCoordinate.getY()))
            {
              surfaceMapTestSubProgram = testSubProgram;
              break;
            }
          }
          else
          {
            surfaceMapTestSubProgram = testSubProgram;
            break;
          }
        }
        else
        {
          Board testSubProgramBoard = testSubProgram.getBoard();
          if (testSubProgramBoard.equals(_opticalCameraRectangle.getBoard()))
          {
            if (testSubProgramBoard.isLongBoard())
            {
              if (testSubProgram.getImageableRegionInNanoMeters().contains(panelCoordinate.getX(), panelCoordinate.getY()))
              {
                surfaceMapTestSubProgram = testSubProgram;
                break;
              }
            }
            else
            {
              surfaceMapTestSubProgram = testSubProgram;
              break;
            }
          }
        }
      }
    }
    Assert.expect(surfaceMapTestSubProgram != null);
    return surfaceMapTestSubProgram;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void acquireSingleImage(final int currentSetting) throws XrayTesterException
  {
    final boolean simulationMode = XrayTester.getInstance().isSimulationModeOn();
    final BooleanLock surfaceMapConfirmationLock = new BooleanLock(false);
    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (simulationMode == false)
          {
            PspHardwareEngine.getInstance().turnOnLED(_moduleEnum);
            PspHardwareEngine.getInstance().adjustCurrent(_moduleEnum, currentSetting);
            PspHardwareEngine.getInstance().displayWhiteLight(_moduleEnum);
            acquireSingleImage();
          }
        }
        catch (final XrayTesterException xte)
        {
          _xrayTesterException = xte;
        }
        finally
        {
          try
          {
            // adjust back to default current setting after single image acquisition is done.
            PspHardwareEngine.getInstance().adjustCurrent(_moduleEnum, Config.getInstance().getIntValue(HardwareConfigEnum.ETHERNET_BASED_LIGHT_ENGINE_CURRENT_SETTING_MICRO_AMPS));
            // Handle post operation
            PspImageAcquisitionEngine.getInstance().handlePostOpticalView(_moduleEnum, OpticalShiftNameEnum.WHITE_LIGHT);
          }
          catch(XrayTesterException xte)
          {
            _xrayTesterException = xte;
          }
          finally
          {
            surfaceMapConfirmationLock.setValue(true);
          }
        }
      }
    });
   
    try
    {
      surfaceMapConfirmationLock.waitUntilTrue();
    }
    catch (InterruptedException ex1)
    {
      Assert.logException(ex1);
    }
    finally
    {
      if (_xrayTesterException != null)
      {
        throw _xrayTesterException;
      }
    }
  }
  
  /**
   * Move stage to the OpticalCameraRectangle's position.
   * @author Ying-Huan.Chu
   */
  public void moveStageToOpticalCameraRectanglePosition(OpticalCameraRectangle opticalCameraRectangle, Board board) throws XrayTesterException
  {
    Assert.expect(opticalCameraRectangle != null);
    Assert.expect(board != null);
    
    _opticalCameraRectangle = opticalCameraRectangle;
    
    final java.awt.Rectangle rectangleInNanometers = opticalCameraRectangle.getRegion().getRectangle2D().getBounds();
    
    // Get currently loaded project
    _project = Project.getCurrentlyLoadedProject();
    
    // clear the XrayTesterException object first.
    _xrayTesterException = null;
    
    PanelCoordinate panelCoordinate = new PanelCoordinate();
    panelCoordinate.setLocation((int)rectangleInNanometers.getCenterX(), (int)rectangleInNanometers.getCenterY());
    
    _moduleEnum = getPspModuleEnum(opticalCameraRectangle);
   
    // Before proceed, we get the flag for checkLatestProgram
    boolean isCheckLatestTestProgram = _project.isCheckLatestTestProgram();
    _project.setCheckLatestTestProgram(false);
    
    TestSubProgram surfaceMaptestSubProgram = getSurfaceMapTestSubProgram(opticalCameraRectangle);
    
    _project.setCheckLatestTestProgram(isCheckLatestTestProgram);
    
    int opticalFiducialStageCoordinateLocationXInNanometers = -1;
    int opticalFiducialStageCoordinateLocationYInNanometers = -1;
    if (_moduleEnum.equals(PspModuleEnum.FRONT))
    {
      opticalFiducialStageCoordinateLocationXInNanometers = PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers();
      opticalFiducialStageCoordinateLocationYInNanometers = PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers();
    }
    else
    {
      opticalFiducialStageCoordinateLocationXInNanometers = PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers();
      opticalFiducialStageCoordinateLocationYInNanometers = PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers();
    }
    
    final StagePosition stagePosition = OpticalMechanicalConversions.convertPanelCoordinateToStageCoordinate(surfaceMaptestSubProgram, 
                                                                                                             panelCoordinate, 
                                                                                                             surfaceMaptestSubProgram.getAggregateRuntimeAlignmentTransform(), 
                                                                                                             surfaceMaptestSubProgram.getPanelLocationInSystem(),
                                                                                                             PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationXInNanometers(),
                                                                                                             PspConfiguration.getInstance().getOpticalFiducialSystemCoordinateLocationYInNanometers(),
                                                                                                             opticalFiducialStageCoordinateLocationXInNanometers,
                                                                                                             opticalFiducialStageCoordinateLocationYInNanometers,
                                                                                                             new BooleanRef());
    
    final TestSubProgram finalTestSubProgram = surfaceMaptestSubProgram;

    _currentStagePosition = stagePosition;

    final boolean simulationMode = XrayTester.getInstance().isSimulationModeOn();
    final BooleanLock surfaceMapConfirmationLock = new BooleanLock(false);
    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (simulationMode == false)
          {
            // First, position the panel according to TestSubProgram
            if (finalTestSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
              PanelHandler.getInstance().movePanelToRightSide();
            else if (finalTestSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
              PanelHandler.getInstance().movePanelToLeftSide();
            else
              Assert.expect(false, "Invalid PanelLocationInSystemEnum");

            PspImageAcquisitionEngine.getInstance().moveStage(_currentStagePosition);
          }
        }
        catch (final XrayTesterException xte)
        {
          _xrayTesterException = xte;
        }
        finally
        {
          //do nothing
          surfaceMapConfirmationLock.setValue(true);
        }
      }
    });
   
    try
    {
      surfaceMapConfirmationLock.waitUntilTrue();
    }
    catch (InterruptedException ex1)
    {
      Assert.logException(ex1);
    }
    finally
    {
      if (_xrayTesterException != null)
      {
        throw _xrayTesterException;
      }
    }
  }
  
  
  /**
   * @author Cheah Lee Herng
   */
  public PspModuleEnum getPspModuleEnum()
  {
    return _moduleEnum;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public OpticalCameraRectangle getOpticalCameraRectangle()
  {
    return _opticalCameraRectangle;
  }
  
  /**
   * @author Jack Hwee
   */
  private void moveOpticalCameraToComponent(final int xCoordinate, final int yCoordinate) throws XrayTesterException
  {
    _panelPositioner = PanelPositioner.getInstance();
    if (_panelPositioner.isStartupRequired() == false)
    {
      final int finalXCoordinate = xCoordinate;
      final int finalYCoordinate = yCoordinate;

      StagePositionMutable stagePosition = new StagePositionMutable();

      //Set the stage position we are moving to
      stagePosition.setXInNanometers(finalXCoordinate);
      stagePosition.setYInNanometers(finalYCoordinate);

      // Save off the previously loaded motion profile
      MotionProfile originalMotionProfile = _panelPositioner.getActiveMotionProfile();

      //Set the motion profile we will use
      _panelPositioner.setMotionProfile(new MotionProfile(_motionProfile));

      //Move to the new location using the profile
      _panelPositioner.pointToPointMoveAllAxes(stagePosition);

      // Now that we are done with the move, restore the motion profile.
      _panelPositioner.setMotionProfile(originalMotionProfile);
    }
  }

   /**
   * Given x,y coordinates in pixels, return out a Point in Renderer coordinates.
   * @return a Point in Renderer coordinates transformed from pixel coordinates
   * @author Bill Darbie
   */
  public java.awt.geom.Point2D convertFromPixelToRendererCoordinates(int xInPixels, int yInPixels)
  {
    Assert.expect(_transformFromRendererCoords != null);
    
    java.awt.geom.Point2D point = new java.awt.geom.Point2D.Double(xInPixels, yInPixels);
    java.awt.geom.AffineTransform inverse = null;
  
    try
    {
      inverse = _transformFromRendererCoords.createInverse();
    }
    catch (java.awt.geom.NoninvertibleTransformException ex)
    {
      ex.printStackTrace();
      Assert.expect(false);
    }  
    inverse.transform(point, point);

    return point;
  }
  
  /**
   * @author Jack Hwee
   */
  public java.awt.geom.Point2D covertVideoPixelToPanelCoordinate(int xCoordinateFromVideo, int yCoordinateFromVideo, int xCoordinateFromPanel, int yCoordinateFromPanel, java.awt.geom.AffineTransform affineTransform)
  {
    Assert.expect(affineTransform != null);
    Assert.expect(xCoordinateFromVideo >= 0);
    Assert.expect(yCoordinateFromVideo >= 0);
    Assert.expect(xCoordinateFromPanel >= 0);
    Assert.expect(yCoordinateFromPanel >= 0);

    setCurrentAffineTransformForOpticalRegion(affineTransform);
    int xCoordinate = Math.round((xCoordinateFromPanel) + xCoordinateFromVideo / OpticalRegionGeneration.getCameraViewFactor());
    int yCoordinate = Math.round((yCoordinateFromPanel) + yCoordinateFromVideo / OpticalRegionGeneration.getCameraViewFactor());
    
    java.awt.Point point = new java.awt.Point();
    point.setLocation(xCoordinate, yCoordinate);

    java.awt.geom.Point2D graphicCoordInNanoMeters = convertFromPixelToRendererCoordinates((int) point.getX(), (int) point.getY());

    return graphicCoordInNanoMeters;
  }

  /**
   *
   * @author Jack Hwee
   */
  public void setCurrentAffineTransformForOpticalRegion(java.awt.geom.AffineTransform transformFromRendererCoords)
  {
    Assert.expect(_transformFromRendererCoords != null);
    _transformFromRendererCoords = transformFromRendererCoords;
  }
  
   /**
   * @author Jack Hwee
   */
  public java.awt.geom.AffineTransform getCurrentAffineTransformForOpticalRegion()
  {
    return _transformFromRendererCoords;
  }

  /**
   * @author Cheah Lee Herng
   */
  public static StagePositionMutable convertOpticalSystemFiducialPositionToStagePosition(IntCoordinate systemFiducialPosition,
                                                                                                java.awt.geom.AffineTransform opticalSystemFiducialPositionToStagePositionTransform)
  {
    Assert.expect(systemFiducialPosition != null);
    Assert.expect(opticalSystemFiducialPositionToStagePositionTransform != null);

    java.awt.geom.Point2D fromCoord = new java.awt.geom.Point2D.Double(systemFiducialPosition.getX(), systemFiducialPosition.getY());
    java.awt.geom.Point2D toCoord = new java.awt.geom.Point2D.Double();
    opticalSystemFiducialPositionToStagePositionTransform.transform(fromCoord, toCoord);

    StagePositionMutable stagePosition = new StagePositionMutable();
    stagePosition.setXInNanometers((int) Math.round(toCoord.getX()));
    stagePosition.setXInNanometers((int) Math.round(toCoord.getY()));

    return stagePosition;
  }

  /**
   * @author Jack Hwee
   */
  public void acquireSingleImageAndGenerateHeightMap() throws XrayTesterException 
  {
    // Lee Herng - Create folder if not exists
    if (FileUtilAxi.existsDirectory(Directory.getAlignmentOpticalImagesDir()) == false)
      FileUtilAxi.createDirectory(Directory.getAlignmentOpticalImagesDir());
    
    String stagePositionName = _currentStagePosition.getXInNanometers() + "_" + _currentStagePosition.getYInNanometers();
    String imageFullPath = FileName.getAlignmentOpticalImageFullPath("temp", 0);
    java.util.Map<String,Pair<Integer,Integer>> pointPanelPositionNameToPointPositionInPixelMap = new java.util.LinkedHashMap<String,Pair<Integer,Integer>>();
    String panelPositionName = _currentStagePosition.getXInNanometers() + "_" +  _currentStagePosition.getYInNanometers();
    
    //Pair<Integer,Integer> pointPositionInPixelPair = new Pair<Integer,Integer>( (int)getCurrentDividedRectangle().getBounds().getCenterX(), (int)getCurrentDividedRectangle().getBounds().getCenterY());
    Pair<Integer,Integer> pointPositionInPixelPair = new Pair<Integer,Integer>(0,0);
    pointPanelPositionNameToPointPositionInPixelMap.put(panelPositionName, pointPositionInPixelPair);
    
    AbstractOpticalCamera abstractOpticalCamera;

    OpticalPointToPointScan opticalPointToPointScan = new OpticalPointToPointScan();
   
    if (getPspModuleEnum().equals(PspModuleEnum.FRONT))
    {
      opticalPointToPointScan.setOpticalCameraIdEnum(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
      abstractOpticalCamera = OpticalCameraArray.getCamera(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
    }
    else
    {
      opticalPointToPointScan.setOpticalCameraIdEnum(OpticalCameraIdEnum.OPTICAL_CAMERA_2);
      abstractOpticalCamera = OpticalCameraArray.getCamera(OpticalCameraIdEnum.OPTICAL_CAMERA_2);
    }
    
    // Reset this boolean to make sure the height map is the latest one
    abstractOpticalCamera.setHeightMapValueAvailable(false);
    opticalPointToPointScan.setReturnActualHeightMap(true);
    opticalPointToPointScan.setRoiWidth(PspHardwareEngine.getInstance().getImageWidth());
    opticalPointToPointScan.setRoiHeight(PspHardwareEngine.getInstance().getImageHeight());
    opticalPointToPointScan.setRegionPositionName(stagePositionName);
    opticalPointToPointScan.setPointPositionName(imageFullPath);
    opticalPointToPointScan.setPointToPointStagePositionInNanoMeters(_currentStagePosition);
    opticalPointToPointScan.addPointPanelPositionNameToPointPositionInPixel(pointPanelPositionNameToPointPositionInPixelMap);

    // Get panel thickness. This is important because this will determine Optica Camera Profile to be used.
    int panelThicknessInNanometers = _project.getPanel().getThicknessInNanometers();
    if (panelThicknessInNanometers <= MAXIMUM_PANEL_THICKNESS_FOR_OPTICAL_PROFILE_1_IN_NANOMETERS)
      opticalPointToPointScan.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_1);
    else
      opticalPointToPointScan.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_2);

    PspEngine.getInstance().performSingleImageAndHeightMapOperationForLiveView(opticalPointToPointScan, _project.getName(), imageFullPath);
  
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void acquireSingleImage() throws XrayTesterException 
  {
    // Lee Herng - Create folder if not exists
    if (FileUtilAxi.existsDirectory(Directory.getAlignmentOpticalImagesDir()) == false)
      FileUtilAxi.createDirectory(Directory.getAlignmentOpticalImagesDir());
    
    String stagePositionName = _currentStagePosition.getXInNanometers() + "_" + _currentStagePosition.getYInNanometers();
    String imageFullPath = FileName.getAlignmentOpticalImageFullPath("temp", 0);
    java.util.Map<String,Pair<Integer,Integer>> pointPanelPositionNameToPointPositionInPixelMap = new java.util.LinkedHashMap<String,Pair<Integer,Integer>>();
    String panelPositionName = _currentStagePosition.getXInNanometers() + "_" +  _currentStagePosition.getYInNanometers();

    //Pair<Integer,Integer> pointPositionInPixelPair = new Pair<Integer,Integer>( (int)getCurrentDividedRectangle().getBounds().getCenterX(), (int)getCurrentDividedRectangle().getBounds().getCenterY());
    Pair<Integer,Integer> pointPositionInPixelPair = new Pair<Integer,Integer>(0,0);
    pointPanelPositionNameToPointPositionInPixelMap.put(panelPositionName, pointPositionInPixelPair);
    
    OpticalPointToPointScan opticalPointToPointScan = new OpticalPointToPointScan();

    opticalPointToPointScan.setOpticalCameraIdEnum(PspHardwareEngine.getInstance().getOpticalCameraIdEnum(getPspModuleEnum()));
    opticalPointToPointScan.setReturnActualHeightMap(true);
    opticalPointToPointScan.setRoiWidth(PspHardwareEngine.getInstance().getImageWidth());
    opticalPointToPointScan.setRoiHeight(PspHardwareEngine.getInstance().getImageHeight());
    opticalPointToPointScan.setRegionPositionName(stagePositionName);
    opticalPointToPointScan.setPointPositionName(imageFullPath);
    opticalPointToPointScan.setPointToPointStagePositionInNanoMeters(_currentStagePosition);
    opticalPointToPointScan.addPointPanelPositionNameToPointPositionInPixel(pointPanelPositionNameToPointPositionInPixelMap);

    // Get panel thickness. This is important because this will determine Optica Camera Profile to be used.
    int panelThicknessInNanometers = _project.getPanel().getThicknessInNanometers();
    if (panelThicknessInNanometers <= MAXIMUM_PANEL_THICKNESS_FOR_OPTICAL_PROFILE_1_IN_NANOMETERS)
      opticalPointToPointScan.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_1);
    else
      opticalPointToPointScan.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_2);

    PspEngine.getInstance().acquireSingleImageForLiveView(opticalPointToPointScan, _project.getName(), imageFullPath);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public double getDistanceSquare(java.awt.Rectangle rectangle1,
                                  java.awt.Rectangle rectangle2)
  {
    Assert.expect(rectangle1 != null);
    Assert.expect(rectangle2 != null);
    
    double deltaX = rectangle1.getCenterX() - rectangle2.getCenterX();
    double deltaY = rectangle1.getCenterY() - rectangle2.getCenterY();

    return (deltaX * deltaX) + (deltaY * deltaY);
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public Map<TestSubProgram, List<OpticalCameraRectangle>> getOpticalCameraRectangleBasedOnTestSubProgram(List<OpticalCameraRectangle> opticalCameraRectangleList)
  {
    _project = Project.getCurrentlyLoadedProject();
    
    return getOpticalCameraRectangleBasedOnTestSubProgram(opticalCameraRectangleList, _project.getTestProgram());
  }
  
  /**
   * Separate the optical camera rectangle based on test sub program.
   * @author Kok Chun, Tan
   */
  public Map<TestSubProgram, List<OpticalCameraRectangle>> getOpticalCameraRectangleBasedOnTestSubProgram(List<OpticalCameraRectangle> opticalCameraRectangleList, TestProgram testProgram)
  {
    Assert.expect(opticalCameraRectangleList != null);
    Assert.expect(testProgram != null);
    
    Map<TestSubProgram, List<OpticalCameraRectangle>> opticalCameraRectangleForTestSubProgram = new LinkedHashMap<>();
    
    boolean isProjectLoaded = Project.isCurrentProjectLoaded();
    if (isProjectLoaded == false)
    {
      return opticalCameraRectangleForTestSubProgram;
    }
    
    _project = Project.getCurrentlyLoadedProject();
    boolean isPanelBasedAlignment = _project.getPanel().getPanelSettings().isPanelBasedAlignment();
    List<TestSubProgram> testSubPrograms = new ArrayList<>();

    for (TestSubProgram testSubProgram : testProgram.getAllInspectableTestSubPrograms())
    {
      // if the project is a mixed-mag recipe, we should only consider low-mag TestSubProgram.
      if (testProgram.getProject().getPanel().getPanelSettings().hasLowMagnificationComponent()
        && testProgram.getProject().getPanel().getPanelSettings().hasHighMagnificationComponent())
      {
        if (testSubProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
        {
          testSubPrograms.add(testSubProgram);
        }
      }
      else
      {
        testSubPrograms.add(testSubProgram);
      }
    }

    for (TestSubProgram testSubProgram : testSubPrograms)
    {
      opticalCameraRectangleForTestSubProgram.put(testSubProgram, new ArrayList<OpticalCameraRectangle>());
    }

    if (testProgram.getProject().getPanel().getPanelSettings().isLongPanel())
    {
      for (OpticalCameraRectangle ocr : opticalCameraRectangleList)
      {
        boolean addFlag = false;
        for (TestSubProgram testSubProgram : testSubPrograms)
        {
          if (testSubProgram.getImageableRegionInNanoMeters().contains(ocr.getRegion()))
          {
            if (isPanelBasedAlignment)
            {
              opticalCameraRectangleForTestSubProgram.get(testSubProgram).add(ocr);
              addFlag = true;
              break;
            }
            else
            {
              // only board(s) that are involved in current Surface Mapping operation will be included.
              if (ocr.getBoard().equals(testSubProgram.getBoard()))
              {
                opticalCameraRectangleForTestSubProgram.get(testSubProgram).add(ocr);
                addFlag = true;
                break;
              }
            }
          }
        }
        if (addFlag == false)
        {
        // no TestSubProgram contains the OpticalCameraRectangle.
          // we should find the TestSubProgram that covers the most area of the OpticalCameraRectangle.
          Map<TestSubProgram, java.awt.Rectangle> testSubProgramToIntersectionAreaMap = new LinkedHashMap<>();
          for (TestSubProgram subProgram : testSubPrograms)
          {
            if (subProgram.getImageableRegionInNanoMeters().intersects(ocr.getRegion()))
            {
              // Checks the area of intersection between the TestSubProgram and OpticalCameraRectangle.
              java.awt.Rectangle intersection = subProgram.getImageableRegionInNanoMeters().getRectangle2D().getBounds().intersection(
                ocr.getRegion().getRectangle2D().getBounds());
              testSubProgramToIntersectionAreaMap.put(subProgram, intersection);
            }
          }
          java.awt.Rectangle largestIntersection = new java.awt.Rectangle(0, 0, 0, 0);
          TestSubProgram testSubProgramWithLargestIntersection = new TestSubProgram(_project.getTestProgram());
          // Loop through and check which TestSubProgram has the largest intersection area with the OpticalCameraRectangle.
          for (Map.Entry<TestSubProgram, java.awt.Rectangle> entry : testSubProgramToIntersectionAreaMap.entrySet())
          {
            TestSubProgram subProgram = entry.getKey();
            java.awt.Rectangle intersection = entry.getValue();
            // width x height = area
            if ((intersection.getWidth() * intersection.getHeight()) > (largestIntersection.getWidth() * largestIntersection.getHeight()))
            {
              if (isPanelBasedAlignment)
              {
                largestIntersection = intersection;
                testSubProgramWithLargestIntersection = subProgram;
              }
              else
              {
                // check if the boards are equal.
                if (ocr.getBoard().equals(subProgram.getBoard()))
                {
                  largestIntersection = intersection;
                  testSubProgramWithLargestIntersection = subProgram;
                }
              }
            }
          }
          for (TestSubProgram testSubProgram : testSubPrograms)
          {
            if (testSubProgram.equals(testSubProgramWithLargestIntersection))
            {
              opticalCameraRectangleForTestSubProgram.get(testSubProgram).add(ocr);
            }
          }
        }
      }
    }
    else
    {
      for (OpticalCameraRectangle ocr : opticalCameraRectangleList)
      {
        for (TestSubProgram testSubProgram : testSubPrograms)
        {
          if (isPanelBasedAlignment)
          {
            opticalCameraRectangleForTestSubProgram.get(testSubProgram).add(ocr);
            break;
          }
          else
          {
            // check if the boards are equal.
            if (ocr.getBoard().equals(testSubProgram.getBoard()))
            {
              opticalCameraRectangleForTestSubProgram.get(testSubProgram).add(ocr);
              break;
            }
          }
        }
      }
    }
    return opticalCameraRectangleForTestSubProgram;
  }
  
  /**
   * Convert coordinate list to OpticalPointToPointScan.
   * @author Kok Chun, Tan
   */
  private List<OpticalPointToPointScan> getOpticalPointToPointScan(List<java.awt.Point> sortedPoints, 
                                                                      List<OpticalPointToPointScan> opticalPointToPointScans)
  {
    Assert.expect(sortedPoints != null);
    Assert.expect(opticalPointToPointScans != null);
    
    List<OpticalPointToPointScan> newOpticalPointToPointScans = new ArrayList<OpticalPointToPointScan>();
    for(java.awt.Point point: sortedPoints)
    {
      for(int i=0; i<opticalPointToPointScans.size(); i++)
      {
        int xCoordinate = opticalPointToPointScans.get(i).getPointToPointStagePositionInNanoMeters().getXInNanometers();
        int yCoordinate = opticalPointToPointScans.get(i).getPointToPointStagePositionInNanoMeters().getYInNanometers();
        if((point.getX() == xCoordinate) && (point.getY() == yCoordinate))
        {
          newOpticalPointToPointScans.add(opticalPointToPointScans.get(i));
        }
      }
    }
    return newOpticalPointToPointScans;
  }
  
  /**
   * Sort OpticalPointToPointScan using GreedySorterAlgorithm and Two-Opt algorithm.
   * @author Kok Chun, Tan
   */
  public List<OpticalPointToPointScan> sortOpticalPointToPointScan(List<OpticalPointToPointScan> opticalPointToPointScans)
  {
    Assert.expect(opticalPointToPointScans != null);
    
    List<OpticalPointToPointScan> opticalPointToPointScansForCamera1 = new ArrayList<OpticalPointToPointScan>();
    List<OpticalPointToPointScan> opticalPointToPointScansForCamera2 = new ArrayList<OpticalPointToPointScan>();
    List<OpticalPointToPointScan> newOpticalPointToPointScans = new ArrayList<OpticalPointToPointScan>();
        
    for (OpticalPointToPointScan optps: opticalPointToPointScans)
    {
      if (optps.getOpticalCameraIdEnum().equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1))
      {
        opticalPointToPointScansForCamera1.add(optps);
      }
      else
      {
        opticalPointToPointScansForCamera2.add(optps);
      }
    }    
    
    if (opticalPointToPointScansForCamera1.isEmpty() == false)
    {
      List<java.awt.Point> sortedPoints = new ArrayList<java.awt.Point>();

      sortedPoints = _greedySorterAlgorithm.sort(extractOpticalPointToPointScanCoordinate(opticalPointToPointScansForCamera1));
      sortedPoints = _twoOptSorterAlgorithm.sort(sortedPoints);

      newOpticalPointToPointScans.addAll(getOpticalPointToPointScan(sortedPoints, opticalPointToPointScansForCamera1));
    }
    
    if (opticalPointToPointScansForCamera2.isEmpty() == false)
    {
      List<java.awt.Point> sortedPoints = new ArrayList<java.awt.Point>();

      sortedPoints = _greedySorterAlgorithm.sort(extractOpticalPointToPointScanCoordinate(opticalPointToPointScansForCamera2));
      sortedPoints = _twoOptSorterAlgorithm.sort(sortedPoints);

      newOpticalPointToPointScans.addAll(getOpticalPointToPointScan(sortedPoints, opticalPointToPointScansForCamera2));
    }
    
    return newOpticalPointToPointScans;
  }
  
  /**
   * Convert coordinate list to OpticalCameraRectangle.
   * @author Kok Chun, Tan
   */
  private List<OpticalCameraRectangle> getOpticalCameraRectangle(List<java.awt.Point> sortedPoints, List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    Assert.expect(sortedPoints != null);
    Assert.expect(opticalCameraRectangles != null);
    
    List<OpticalCameraRectangle> newOpticalCameraRectangle = new ArrayList<OpticalCameraRectangle>();
    for(java.awt.Point point: sortedPoints)
    {
      for(int i=0; i<opticalCameraRectangles.size(); i++)
      {
        int xCoordinate = opticalCameraRectangles.get(i).getRegion().getCenterX();
        int yCoordinate = opticalCameraRectangles.get(i).getRegion().getCenterY();
        if((point.getX() == xCoordinate) && (point.getY() == yCoordinate))
        {
          newOpticalCameraRectangle.add(opticalCameraRectangles.get(i));
          break;
        }
      }
    }
    
    return newOpticalCameraRectangle;
  }
  
  /**
   * Sort OpticalCameraRectangle using GreedySorterAlgorithm and Two-Opt algorithm.
   * @author Kok Chun, Tan
   */
  private List<OpticalCameraRectangle> sortOpticalCameraRectangle(List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    Assert.expect(opticalCameraRectangles != null);
    
    List<java.awt.Point> sortedPoints = new ArrayList<java.awt.Point>();
    
    sortedPoints = _greedySorterAlgorithm.sort(extractOpticalCameraRectangleCoordinate(opticalCameraRectangles));
    sortedPoints = _twoOptSorterAlgorithm.sort(sortedPoints);
    opticalCameraRectangles = getOpticalCameraRectangle(sortedPoints, opticalCameraRectangles);
    
    return opticalCameraRectangles;
  }
  
  /**
   * Sort the optical rectangle based on the test sub program
   * @author Kok Chun, Tan
   */
  public List<OpticalCameraRectangle> sortOpticalCameraRectangleBasedOnTestProgram(List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    Assert.expect(opticalCameraRectangles != null);

    List<OpticalCameraRectangle> newOpticalCameraRectangleList = new ArrayList<OpticalCameraRectangle>();
    Map<TestSubProgram, List<OpticalCameraRectangle>> opticalCameraRectangleMap = new LinkedHashMap<>();

    opticalCameraRectangleMap = getOpticalCameraRectangleBasedOnTestSubProgram(opticalCameraRectangles);

    for (Map.Entry<TestSubProgram, List<OpticalCameraRectangle>> entry : opticalCameraRectangleMap.entrySet())
    {
      if (newOpticalCameraRectangleList.containsAll(entry.getValue()) == false)
      {
        entry.setValue(sortOpticalCameraRectangle(entry.getValue()));
        newOpticalCameraRectangleList.addAll(entry.getValue());
      }
    }
    return newOpticalCameraRectangleList;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private List<java.awt.Point> extractOpticalPointToPointScanCoordinate(List<OpticalPointToPointScan> opticalPointToPointScans)
  {
    Assert.expect(opticalPointToPointScans != null);
    
    List<java.awt.Point> points = new ArrayList<java.awt.Point>();
    for(OpticalPointToPointScan opticalPointToPointScan : opticalPointToPointScans)
    {
      points.add(new java.awt.Point(opticalPointToPointScan.getPointToPointStagePositionInNanoMeters().getXInNanometers(),
                           opticalPointToPointScan.getPointToPointStagePositionInNanoMeters().getYInNanometers()));
    }
    return points;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private List<java.awt.Point> extractOpticalCameraRectangleCoordinate(List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    Assert.expect(opticalCameraRectangles != null);
    
    List<java.awt.Point> points = new ArrayList<java.awt.Point>();
    for(OpticalCameraRectangle ocr : opticalCameraRectangles)
    {
      points.add(new java.awt.Point(ocr.getRegion().getCenterX(),
                           ocr.getRegion().getCenterY()));
    }
    
    return points;
  }
  
  /**
   * @author Ying-Huan.Chu
   * @param isUseLargerFov pass in true to use Larger FOV, pass in false to use small FOV.
   * @throws com.axi.v810.util.XrayTesterException
   */
  public void setUseLargerFov(boolean isUseLargerFov) throws XrayTesterException
  {
    PspHardwareEngine.getInstance().setUseLargerFov(isUseLargerFov);
  }

  /**
   * @author Ying-Huan.Chu
   * @return true if OpticalCamera is currently using Larger FOV, return false if OpticalCamera is currently using small FOV.
   */
  public boolean isLargerFov()
  {
    return PspHardwareEngine.getInstance().isLargerFov();
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void adjustCurrent(final int currentSetting) throws XrayTesterException
  {
    final boolean simulationMode = XrayTester.getInstance().isSimulationModeOn();
    _hardwareWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (simulationMode == false)
          {
            // adjust current setting
            //PspHardwareEngine.getInstance().turnOffLED(_moduleEnum);
            //PspHardwareEngine.getInstance().turnOnLED(_moduleEnum);
            PspHardwareEngine.getInstance().adjustCurrent(_moduleEnum, currentSetting);
            PspHardwareEngine.getInstance().displayWhiteLight(_moduleEnum);
          }
        }
        catch (final XrayTesterException xte)
        {
          _xrayTesterException = xte;
        }
      }
    });

    if (_xrayTesterException != null)
    {
      throw _xrayTesterException;
    }
  }
  
  /**
   * remove all optical camera rectangle.
   * @author Kok Chun, Tan
   */
  public void removeAllOpticalCameraRectangle(Project project)
  {
    boolean isPanelBasedAlignment = project.getPanel().getPanelSettings().isPanelBasedAlignment();
    
    if (isPanelBasedAlignment)
    {
      if (project.getPanel().hasPanelSurfaceMapSettings())
      {
        if (project.getPanel().getPanelSurfaceMapSettings().hasOpticalRegion())
        {
          for (OpticalRegion region : project.getPanel().getPanelSurfaceMapSettings().getOpticalRegions())
          {
            region.remove();
          }
          project.getTestProgram().removeAllOpticalRegion();
          project.getPanel().setIsAutomaticallyIncludeAllComponents(false);
        }
      }
    }
    else
    {
      List<Board> boards = project.getPanel().getBoards();
      
      for (Board board: boards)
      {
        if (board.hasBoardSurfaceMapSettings())
        {
          if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
          {
            for (OpticalRegion region : board.getBoardSurfaceMapSettings().getOpticalRegions())
            {
              region.remove(board);
            }
            project.getTestProgram().removeAllOpticalRegion();
            project.getPanel().setIsAutomaticallyIncludeAllComponents(false);
          }
        }
      }
    }
  }
  
  /**
   * Check the optical camera used is enable or not. If not enable, return false.
   * @author Kok Chun, Tan
   */
  public boolean isOpticalCameraEnable(OpticalCameraRectangle ocr)
  {
    PspModuleEnum moduleEnum = getPspModuleEnum(ocr);
    
    return isOpticalCameraEnable(moduleEnum);
  }
  
    /**
   * Check the optical camera used is enable or not. If not enable, return false.
   * @author Kok Chun, Tan
   */
  public boolean isOpticalCameraEnable(PspModuleEnum moduleEnum)
  {
    if (moduleEnum.equals(PspModuleEnum.FRONT))
    {
      if (_settingEnum.equals(PspSettingEnum.SETTING_REAR))
      {
        return false;
      }
      else
      {
        return true;
      }
    }
    else
    {
      if (_settingEnum.equals(PspSettingEnum.SETTING_FRONT))
      {
        return false;
      }
      else
      {
        return true;
      }
    }  
  }
  
  /**
   * Get Psp module to use for the particular optical camera rectangle.
   * @author Kok Chun, Tan
   */
  public PspModuleEnum getPspModuleEnum(OpticalCameraRectangle opticalCameraRectangle)
  {
    PspModuleEnum moduleEnum = PspModuleEnum.FRONT;
    com.axi.v810.business.panelDesc.Panel panel = Project.getCurrentlyLoadedProject().getPanel();
    final java.awt.Rectangle rectangleInNanometers = opticalCameraRectangle.getRegion().getRectangle2D().getBounds();
    
    PanelCoordinate panelCoordinate = new PanelCoordinate();
    panelCoordinate.setLocation((int)rectangleInNanometers.getCenterX(), (int)rectangleInNanometers.getCenterY());
    
    PanelSettings panelSettings = panel.getPanelSettings();
    PanelRectangle bottomPanelRectangle = panelSettings.getBottomSectionOfOpticalLongPanel();

    if (bottomPanelRectangle.contains(panelCoordinate.getX(), panelCoordinate.getY()))
    {
      moduleEnum = PspModuleEnum.FRONT;
    }
    else
    {
      moduleEnum = PspModuleEnum.REAR;
    }
    
    return moduleEnum;
  }

  /**
   * allocate mesh triangle or optical camera rectangle to each reconstruction region
   * @author Kok Chun, Tan
   */
  public void allocateOpticalCameraRectangleToComponents(TestProgram testProgram)
  {
    if (Project.isCurrentProjectLoaded() == false)
    {
      return;
    }

    _project = Project.getCurrentlyLoadedProject();
    boolean isPanelBasedAlignment = _project.getPanel().getPanelSettings().isPanelBasedAlignment();
    java.util.List<com.axi.v810.business.panelDesc.Component> components = _project.getPanel().getComponents();

    if (isPanelBasedAlignment)
    {
      if (_project.getPanel().hasPanelSurfaceMapSettings())
      {
        if (_project.getPanel().getPanelSurfaceMapSettings().hasOpticalRegion())
        {
          OpticalRegion opticalRegion = _project.getPanel().getPanelSurfaceMapSettings().getOpticalRegion();
          java.util.List<MeshTriangle> meshTriangles = opticalRegion.getMeshTriangles();
          java.util.List<OpticalCameraRectangle> opticalCameraRectangleList = opticalRegion.getEnabledOpticalCameraRectangles();

          for (com.axi.v810.business.panelDesc.Component component : components)
          {
            // Kok Chun, Tan - XCR-3506 - no need to highlight if this component is different mag.
            if (isProceedToAllocateRectangle(component) == false)
              continue;
              
            if (meshTriangles.isEmpty() == false && _project.getPanel().isUseMeshTriangle())
            {
              for (com.axi.v810.business.testProgram.ReconstructionRegion inspectionRegion : component.getReconstructionRegionList(testProgram))
              {
                MeshUtil.getInstance().allocateMeshTriangle(inspectionRegion, meshTriangles);
              }
            }
            else if (opticalCameraRectangleList.isEmpty() == false)
            {
              SurroundingOpticalRectangleUtil.getInstance().allocateSurroundingOpticalCameraRectangles(component.getReconstructionRegionList(testProgram), opticalCameraRectangleList);
            }
          }
        }
      }
    }
    else
    {
      for (com.axi.v810.business.panelDesc.Component component : components)
      {
        // Kok Chun, Tan - XCR-3506 - no need to highlight if this component is different mag.
        if (isProceedToAllocateRectangle(component) == false)
          continue;
        
        Board board = component.getBoard();
        if (board.hasBoardSurfaceMapSettings())
        {
          if (board.getBoardSurfaceMapSettings().hasOpticalRegion())
          {
            OpticalRegion opticalRegion = board.getBoardSurfaceMapSettings().getOpticalRegion();
            java.util.List<MeshTriangle> meshTriangles = opticalRegion.getMeshTriangles();
            java.util.List<OpticalCameraRectangle> opticalCameraRectangleList = opticalRegion.getEnabledBoardOpticalCameraRectangles(board);

            if (meshTriangles.isEmpty() == false && _project.getPanel().isUseMeshTriangle())
            {
              for (com.axi.v810.business.testProgram.ReconstructionRegion inspectionRegion : component.getReconstructionRegionList(testProgram))
              {
                MeshUtil.getInstance().allocateMeshTriangle(inspectionRegion, meshTriangles);
              }
            }
            else if (opticalCameraRectangleList.isEmpty() == false)
            {
              SurroundingOpticalRectangleUtil.getInstance().allocateSurroundingOpticalCameraRectangles(component.getReconstructionRegionList(testProgram), opticalCameraRectangleList);
            }
          }
        }
      }
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean isProceedToAllocateRectangle(Component component)
  {
    Assert.expect(component != null);
    
    if (_config.getBooleanValue(HardwareConfigEnum.XRAY_MAGNIFICATION_CYLINDER_INSTALLED) == false
      && _config.getBooleanValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_INSTALLED) == false)
    {
      for (Subtype subtype : component.getComponentType().getSubtypes())
      {
        if (subtype.getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
        {
          return false;
        }
      }
    }
    return true;
  }
}
