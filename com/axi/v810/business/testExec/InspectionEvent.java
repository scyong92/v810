package com.axi.v810.business.testExec;

import com.axi.util.*;

/**
 * Base InspectionEvent class.
 *
 * @author Peter Esbensen
 * @author Matt Wharton
 */
public class InspectionEvent
{
  protected InspectionEventEnum _eventEnum = null;

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  private InspectionEvent()
  {
    // do nothing
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public InspectionEvent(InspectionEventEnum eventEnum)
  {
    Assert.expect(eventEnum != null);
    _eventEnum = eventEnum;
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  protected void setEventEnum(InspectionEventEnum eventEnum)
  {
    Assert.expect(eventEnum != null);
    _eventEnum = eventEnum;
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public InspectionEventEnum getInspectionEventEnum()
  {
    Assert.expect(_eventEnum != null);
    return _eventEnum;
  }

  /**
   * @author Patrick Lacz
   */
  public void incrementReferenceCount()
  {
    // do nothing
  }

  /**
   * @author Patrick Lacz
   */
  public void decrementReferenceCount()
  {
    // do nothing
  }
}
