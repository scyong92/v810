package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.communication.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Roy Williams
 */
public class SurfaceModelBreakPoint extends ScanPassBreakPoint
{
  private ZHeightEstimator _zHeightEstimator = ZHeightEstimator.getInstance();
  private List<? extends ImageGroup> _imageGroupsRequiringGlobalSurfaceModel;
  private int _regionsThatMustBeMarkedCompletePriorToUsingGlobalSurfaceModel = -1;
  private Map<Component, Set<JointInspectionData>> _componentJointsWithoutNeighborsMap;
  private TestSubProgram _testSubProgram;
  private PshSettings _pshSettings; //Siew Yeng - XCR-3781

  private static Comparator<JointInspectionData> _jointComparator;
  private static Comparator<Component> _componentComparator;

  /**
   * @author Roy Williams
   */
  public SurfaceModelBreakPoint(
      TestSubProgram testSubProgram,
      ScanPass scanPass,
      List<? extends ImageGroup> imageGroupsRequiringGlobalSurfaceModel,
      int allInspectionRegionsSize,
      double speedFactor)
  {
    super(scanPass, speedFactor);

    Assert.expect(testSubProgram != null);
    Assert.expect(scanPass != null);
    Assert.expect(imageGroupsRequiringGlobalSurfaceModel != null);
    Assert.expect(allInspectionRegionsSize > 0);

    _testSubProgram = testSubProgram;
    _pshSettings = testSubProgram.getTestProgram().getProject().getPanel().getPshSettings(); //Siew Yeng - XCR-3781
    
    _imageGroupsRequiringGlobalSurfaceModel = imageGroupsRequiringGlobalSurfaceModel;
    int regionsRequiringGlobalSurfaceModel = 0;
    for (ImageGroup group : imageGroupsRequiringGlobalSurfaceModel)
    {
      regionsRequiringGlobalSurfaceModel += group.getRegionPairsSortedInExecutionOrder().size();
    }
    _regionsThatMustBeMarkedCompletePriorToUsingGlobalSurfaceModel = allInspectionRegionsSize -
                                                                     regionsRequiringGlobalSurfaceModel;

    Assert.expect(_regionsThatMustBeMarkedCompletePriorToUsingGlobalSurfaceModel > 0);

    if (_jointComparator == null)
    {
      _jointComparator = new Comparator<JointInspectionData>()
      {
        public int compare(JointInspectionData jointData1, JointInspectionData jointData2)
        {
          Assert.expect(jointData1 != null);
          Assert.expect(jointData2 != null);
          String a = jointData1.getPad().getPackagePin().getName();
          return jointData1.getPad().getPackagePin().getName().compareTo(
              jointData2.getPad().getPackagePin().getName());
        }
      };
    }

    if (_componentComparator == null)
    {
      _componentComparator = new Comparator<Component>()
      {
        public int compare(Component component1, Component component2)
        {
          Assert.expect(component1 != null);
          Assert.expect(component2 != null);

          return component1.getBoardNameAndReferenceDesignator().compareTo(
              component2.getBoardNameAndReferenceDesignator());
        }
      };
    }
  }

  /**
   * @author Roy Williams
   * @author Siew Yeng - added parameter region for debug purpose
   * @param isTopSide component board side
   */
  private int getEstimatedZHeight(boolean isTopSide,
                                  SystemFiducialRectangle systemFiducialRectangle,
                                  JointTypeEnum jointType,
                                  int estimatedBoardThickness,
                                  BooleanRef estimateIsValid,
                                  ReconstructionRegion region)
  {
    //Siew Yeng - XCR-3781 - pass in pshSettings for selective boardside feature
    // first try at 750 mils (3/4 inch) around center of reconstruction region.
    int estimatedZHeight = _zHeightEstimator.getBestEstimatedZHeight(systemFiducialRectangle,
                                                                     isTopSide,
                                                                     estimateIsValid,
                                                                     _pshSettings,
                                                                     region);

    // second (if estimate not valid) try at 1500 mils (1.5 inch) around center of region.
    if (estimateIsValid.getValue() == false)
    {
      estimatedZHeight = _zHeightEstimator.getReasonablyEstimatedZHeight(systemFiducialRectangle,
                                                                         isTopSide,
                                                                         estimateIsValid,
                                                                         _pshSettings,
                                                                         region);
    }

    // Ok, we're getting really desperate here. Look up to 4 inches for 2 or more neighbors.
    if (estimateIsValid.getValue() == false)
    {
      estimatedZHeight = _zHeightEstimator.getEstimatedZHeight(systemFiducialRectangle,
                                                               isTopSide,
                                                               4000,             // 4000 mil search radius
                                                               2,                // minimum of 2 neighbors
                                                               estimateIsValid,
                                                               _pshSettings,
                                                               region);
    }

    // Finally, if estimate is still not valid set to z height 0 instead of -1.
    if (estimateIsValid.getValue() == false)
      estimatedZHeight = 0;

    return estimatedZHeight;
  }


  /**
   * @author Roy Williams
   */
  public void executeAction() throws XrayTesterException
  {
    if(Config.isRealTimePshEnabled() && _testSubProgram.isSufficientMemoryForRealTimePsh())
      return;
    // cache a couple of variables (not be changed)
    final int testSubProgramId = getTestSubProgramId();
    final ProgramGeneration programGenerator = ProgramGeneration.getInstance();
    final ReconstructedImagesProducer reconstructedImagesProducer = ImageAcquisitionEngine.getInstance().getReconstructedImagesProducer();
    final boolean isUnitTest = UnitTest.unitTesting() ? true : false;

    // Create a couple of variables to be reused in looping constructs below.
    ReconstructionRegion region                     = null;
    SystemFiducialRectangle systemFiducialRectangle = null;
    JointInspectionData jointInspectionData         = null;
    JointTypeEnum jointType                         = null;
    byte shape                                      = 0;
    byte focusMethodId                              = 0;
    boolean isTopSideComponent                      = true;
//    boolean isTopSideSurfaceModel                   = true;
    int zHeight                                     = 0;
    int zOffset                                     = 0;
    int percent                                     = 0;
    BooleanRef estimateIsValid                      = new BooleanRef();
    int estimatedBoardThickness                     = (int) _zHeightEstimator.getEstimatedBoardThickness();
    if (estimatedBoardThickness <= 0)
      estimatedBoardThickness = getProject().getPanel().getThicknessInNanometers();

    // If debugging print a header for the table...
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (isUnitTest ||Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.SURFACE_MODEL_BREAKPOINT_DEBUG))
    {
      String delimiter = ", ";
      System.out.println("SurfaceModelBreakPointAdjustment" + delimiter +
                         "isTopSide"                        + delimiter +
                         "RegionId"                         + delimiter +
                         "JointTypeEnumName"                + delimiter +
                         "FocusGroupId"                     + delimiter +
                         "SliceId"                          + delimiter +
                         "usesNeighbors"                    + delimiter +
                         "shape"                            + delimiter +
                         "zHeight"                          + delimiter +
                         "percent"                          + delimiter +
                         "zOffset"                          + delimiter +
                         "focusMethodId");
    }

    // Loop through the constructs and send new FocusInstruction and FocusProfileShape
    // info for each Slice...
    for (ImageGroup group : _imageGroupsRequiringGlobalSurfaceModel)
    {
      //XCR-2478, IRP Exception happened when abort at 30% during production
      if (reconstructedImagesProducer.checkForAbortInProgress() == true)
        return;
      
      for (Pair<ReconstructionRegion, SystemFiducialRectangle> regionPair : group.getRegionPairsSortedInExecutionOrder())
      {
        // Initialize reused variables.
        region = regionPair.getFirst();
        systemFiducialRectangle = regionPair.getSecond();
        jointInspectionData = region.getJointInspectionDataList().get(0);
        jointType = jointInspectionData.getJointTypeEnum();
        isTopSideComponent = region.isTopSide();
//        isTopSideSurfaceModel = jointInspectionData.isTopSideSurfaceModel();

        // Computed zHeight may not be valid if insufficient neightbors.
        estimateIsValid.setValue(false);
        //Siew Yeng - XCR-3781
        if(_pshSettings.isUsingSelectiveComponent(region.getComponent()))
        {
          //Siew Yeng - XCR-3883 - get isTopSide component
          zHeight = _zHeightEstimator.getSelectiveNeighboursEstimatedZHeight(_pshSettings, region, region.isTopSide(), estimateIsValid);
        }
        else
        {
          //Siew Yeng - XCR-3883 - get isTopSide component
          zHeight = getEstimatedZHeight(region.isTopSide(), systemFiducialRectangle,
                                        jointType, estimatedBoardThickness, estimateIsValid, region);
        }
        if (estimateIsValid.getValue())
        {
          // using LimiteSurfaceModel - computed zHeight is okay - don't recompute again.
          focusMethodId = ReconstructionTypeEnum.map(FocusMethodEnum.USE_Z_HEIGHT);
          shape = FocusProfileShapeTypeEnum.map(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
        }
        else
        {
          // don't use LimitedSurfaceModel = use autofocus in IRP.
          zHeight = 0; // set to zero but NOT USED BY IRP in SHARPEST calculations.
          focusMethodId = ReconstructionTypeEnum.map(FocusMethodEnum.SHARPEST);
          shape = FocusProfileShapeTypeEnum.map(FocusProfileShapeEnum.NO_SEARCH_PREDICTED_SURFACE);

          // Accumulate a list of joints that are not in map.
          if (_componentJointsWithoutNeighborsMap == null)
            _componentJointsWithoutNeighborsMap = new TreeMap<Component,Set<JointInspectionData>>(_componentComparator);
          Set<JointInspectionData> joints = _componentJointsWithoutNeighborsMap.get(region.getComponent());
          if (joints == null)
          {
            joints = new TreeSet<JointInspectionData>(_jointComparator);
            joints.addAll(region.getJointInspectionDataList());
            _componentJointsWithoutNeighborsMap.put(region.getComponent(), joints);
          }
          else
            joints.addAll(region.getJointInspectionDataList());
        }

        int zGuessFromPredictiveSliceHeightInNanometers = -1;
        // Loop through all focus groups and slices, changing focus profile shapes
        // and focus instructions as required
        for (FocusGroup focusGroup : region.getFocusGroups())
        {
          boolean focusProfileShapeModified = false;
          for (Slice slice : focusGroup.getSlices())
          {                
            // Skip uninspected slices and surface modeling slices.
            // Surface modeling slices are skipped since  a) we're not adding stuff
            // to the surface model at this point, so adjusting these would be pointless,
            // and b) these are dummy slices for which the getZOffset call below will
            // crash if we include them.
            if (slice.createSlice() &&
                slice.getSliceName() != SliceNameEnum.SURFACE_MODELING)
            {
              // khang-wah.chnee, 2012-12-28, 2.5D
              SliceNameEnum sliceName = slice.getSliceName();
              if (slice.getSliceType().equals(SliceTypeEnum.PROJECTION)==true)
              {
                if (region.getInspectionFamilyEnum().equals(InspectionFamilyEnum.THROUGHHOLE) ||
                    region.getInspectionFamilyEnum().equals(InspectionFamilyEnum.OVAL_THROUGHHOLE)) //Siew Yeng - XCR-3318 - Oval PTH
                {
                  sliceName = SliceNameEnum.THROUGHHOLE_BARREL;
                }
                else if (region.getInspectionFamilyEnum().equals(InspectionFamilyEnum.GRID_ARRAY))
                {
                  sliceName = SliceNameEnum.MIDBALL;
                }
                else
                {
                  continue;
                }
              }
              // end

              //Siew Yeng - XCR-3135 - fix irp exception for PCAP with PSH on
              //to fix Gridarray and PCAP PSH showing the same image for all slice
              if(region.getInspectionFamilyEnum().equals(InspectionFamilyEnum.GRID_ARRAY)|| 
                 slice.getSliceName().equals(SliceNameEnum.PCAP_SLUG)||
                 slice.getSliceName().equals(SliceNameEnum.HIGH_SHORT))
              {
                if(estimateIsValid.getValue() == false)
                  zHeight = -1;

                zGuessFromPredictiveSliceHeightInNanometers = zHeight;
                percent = slice.getFocusInstruction().getPercentValue();

                shape = FocusProfileShapeTypeEnum.map(focusGroup.getFocusSearchParameters().getFocusProfileShape()); 
                focusMethodId = ReconstructionTypeEnum.map(slice.getFocusInstruction().getFocusMethod());
              }

              if (isUnitTest == false && focusProfileShapeModified == false)
              {
                //XCR-2478, IRP Exception happened when abort at 30% during production
                if (reconstructedImagesProducer.checkForAbortInProgress() == true)
                  return;
                // Only modify focus profile shape for focus groups containing
                // at least one slice whose focus instruction is also being modified
                reconstructedImagesProducer.modifyFocusProfileShape(testSubProgramId,
                                                                    region,
                                                                    focusGroup.getId(),
                                                                    shape,
                                                                    zGuessFromPredictiveSliceHeightInNanometers,
                                                                    false,
                                                                    focusGroup.getSearchRangeLowLimitInNanometers(),
                                                                    focusGroup.getSearchRangeHighLimitInNanometers(),
                                                                    focusGroup.getPspZOffsetInNanometers());
                focusProfileShapeModified = true;
                if (reconstructedImagesProducer.checkForAbortInProgress() == true)
                  return;
              }

              // Lets also compute a new value for zOffset now that we have a more
              // accurate board thickness.   This is needed for both with and without
              // LSM availability because estimatedBoardThickness may have improved.
              zOffset = programGenerator.getZOffset(jointType,
                                                    sliceName, // khang-wah.chnee, 2012-12-28, 2.5D
                                                    jointInspectionData.getSubtype(),
                                                    isTopSideComponent,
                                                    estimatedBoardThickness);

              //XCR-2478, IRP Exception happened when abort at 30% during production
              if (reconstructedImagesProducer.checkForAbortInProgress() == true)
                return;
              
              if (isUnitTest == false)
                reconstructedImagesProducer.modifyFocusInstruction(testSubProgramId,
                                                                   region,
                                                                   slice.getSliceName().getId(),
                                                                   zHeight,
                                                                   percent,
                                                                   zOffset,
                                                                   focusMethodId);
              if (reconstructedImagesProducer.checkForAbortInProgress() == true)
                return;
              //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
              if (isUnitTest ||Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.SURFACE_MODEL_BREAKPOINT_DEBUG))
              {
                String delimiter = ", ";
                System.out.println("SurfaceModelBreakPointAdjustment"  + delimiter +
                                   isTopSideComponent                  + delimiter +
                                   region.getRegionId()                + delimiter +
                                   region.getJointTypeEnum().getName() + delimiter +
                                   focusGroup.getId()                  + delimiter +
                                   slice.getSliceName().getId()        + delimiter +
                                   estimateIsValid.getValue()          + delimiter +
                                   shape                               + delimiter +
                                   zHeight / 25400                     + delimiter +
                                   percent                             + delimiter +
                                   zOffset / 25400                     + delimiter +
                                   focusMethodId);
              }
            }  // end of if slice.isCreated()
          }    // end of slices
        }      // end of focusGroups
        // Khang Wah, 2013-06-17, New PSH Handling
        // reconstructedImagesProducer.kickStartSpecificRegion(testSubProgramId, region);
      }        // end of reconstruction region pairs
    }          // end of imageGroups

    // At this point we know that all ReconstructedImages that were going to come
    // in have already arrived.   Thus, our surface model is as complete as it is
    // going to be.   So, we will tell the IRPs to disableSurfaceModeling().
    // Subsequent reconstructed regions will NOT be added to the surface model.
    if (isUnitTest == false)
      reconstructedImagesProducer.disableSurfaceModeling();
    // Also tell the master to not add these points to its limited surface model
    InspectionEngine.getInstance().setDoSurfaceModeling(false);

    // Finally, make sure that all our modifications to the program have been
    // performed prior to continuing.
    if (isUnitTest == false)
      reconstructedImagesProducer.waitForPriorIrpCommandsToComplete();

    // If there are components/joints that did not have neighbors, print them for
    // the unit test case only.   If the GUI wants this list it can be requested
    // from the getComponentJointsWithoutNeighborsMap() method.
    if (_componentJointsWithoutNeighborsMap != null && _componentJointsWithoutNeighborsMap.size() > 0)
    {
      if (isUnitTest)
      {
        System.out.println("Components/joints with insufficient neighbors: " + _componentJointsWithoutNeighborsMap.size());
        String indent = "  ";
        for (Map.Entry<Component, Set<JointInspectionData>> entry : _componentJointsWithoutNeighborsMap.entrySet())
        {
          System.out.print(indent + entry.getKey().getBoardNameAndReferenceDesignator() + ": ");
          Iterator<JointInspectionData> iter = entry.getValue().iterator();
          while (iter.hasNext())
          {
            System.out.print(iter.next().getPad().getName());
            if (iter.hasNext())
              System.out.print(", ");
          }
          System.out.println();
        }
      }
      else
      {
        // We will go through extra work to traverse the JointInspectionData set because we
        // cannot ask the component for the subtype... because not "one" unique.   A Component
        // can have many subtypes.  Since this is only done once (at end of all processing)
        // the extra work is not a big deal.
        Map<String, Set<String>> jointNamesWithoutNeighbors = new TreeMap<String, Set<String>>();
        Map<String, Component> componentNameToComponent = new HashMap<String,Component>();
        for (Set<JointInspectionData> jointInspectionDataSet : _componentJointsWithoutNeighborsMap.values())
        {
          Assert.expect(jointInspectionDataSet.isEmpty() == false);
          Set<String> jointNames = new TreeSet<String>(new AlphaNumericComparator(true));
          // Getting first element out of the collection. We expect every one of
          // these joints to have the same subtype so we'll only do it for the first.
          for (JointInspectionData jointInspData : jointInspectionDataSet)
          {
            jointNames.add(jointInspData.getPad().getName());
          }
          Component component = jointInspectionDataSet.iterator().next().getComponent();
          jointNamesWithoutNeighbors.put(component.getReferenceDesignator(), jointNames);
          componentNameToComponent.put(component.getReferenceDesignator(), component);
        }
        String formattedMessage = new String();
        for (String componentName : jointNamesWithoutNeighbors.keySet())
        {
//          formattedMessage += "Component " + componentName + " , joints [";
          Component component = componentNameToComponent.get(componentName);
          String subtypeName = component.getPads().iterator().next().getSubtype().getShortName();
          LocalizedString ls = new LocalizedString("MMGUI_COMPONENT_JOINTS_WITH_NO_NEIGHBORS_KEY",
                                                  new Object[]{componentName, subtypeName});
          formattedMessage += StringLocalizer.keyToString(ls) + " [";
          int numOfJoints = jointNamesWithoutNeighbors.get(componentName).size();
          int totalNumOfJoints = component.getPads().size();
          if (numOfJoints == totalNumOfJoints)
            formattedMessage += StringLocalizer.keyToString("MMGUI_ALL_JOINTS_KEY");
//          formattedMessage += "all joints";
          else if (numOfJoints < totalNumOfJoints)
          {
            int i = 0;
            for (String jointName : jointNamesWithoutNeighbors.get(componentName))
            {
              i++;
              if (i < numOfJoints)
                formattedMessage += jointName + ", ";
              else
                formattedMessage += jointName;
            }
          }
          formattedMessage += "]\n";
        }
        if (Config.isDeveloperDebugModeOn())
        {
          LocalizedString msg = new LocalizedString("TESTEXEC_INCOMPLETE_PROGRAM_ADJUSTMENT_WARNING_KEY",new Object[]{formattedMessage});
          System.out.println(msg.getMessageKey());
          System.out.flush();
          System.out.println(formattedMessage);
        }
//        InspectionEventObservable.getInstance().sendEventInfo(
//            new IncompleteProgramAdjustmentInspectionEvent(new LocalizedString("TESTEXEC_INCOMPLETE_PROGRAM_ADJUSTMENT_WARNING_KEY",
//                                                                               new Object[]{formattedMessage})));
      }
    }
  }
  
  /**
   * @author Roy Williams
   */
  public void executeAction(ReconstructionRegion region) throws XrayTesterException
  {
    // cache a couple of variables (not be changed)
    final int testSubProgramId = getTestSubProgramId();
    final ProgramGeneration programGenerator = ProgramGeneration.getInstance();
    final ReconstructedImagesProducer reconstructedImagesProducer = ImageAcquisitionEngine.getInstance().getReconstructedImagesProducer();
    //XCR-2478, IRP Exception happened when abort at 30% during production
    if (reconstructedImagesProducer.checkForAbortInProgress() == true || ImageAcquisitionEngine.getInstance().isAbortInProgress())
      return;
    final boolean isUnitTest = UnitTest.unitTesting() ? true : false;

    PanelRectangle panelRectangle = region.getRegionRectangleRelativeToPanelInNanoMeters();
    SystemFiducialRectangle systemFiducialRectangle = MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(
                                                        panelRectangle, 
                                                        region.getManualAlignmentMatrix());
    JointInspectionData jointInspectionData         = region.getJointInspectionDataList().get(0);;
    JointTypeEnum jointType                         = jointInspectionData.getJointTypeEnum();
//    boolean isTopSideSurfaceModel                   = jointInspectionData.isTopSideSurfaceModel();
    int percent                                     = 0;
    BooleanRef estimateIsValid                      = new BooleanRef();
    int estimatedBoardThickness                     = (int) _zHeightEstimator.getEstimatedBoardThickness();
    if (estimatedBoardThickness <= 0)
      estimatedBoardThickness = getProject().getPanel().getThicknessInNanometers();
    
    // Computed zHeight may not be valid if insufficient neightbors.
    estimateIsValid.setValue(false);
    
    //Siew Yeng - XCR-3781
    int zHeight;
    if(_pshSettings.isUsingSelectiveComponent(region.getComponent()))
    {
      //Siew Yeng - XCR-3883 - get isTopSide component
      zHeight = _zHeightEstimator.getSelectiveNeighboursEstimatedZHeight(_pshSettings, region, region.isTopSide(), estimateIsValid);
    }
    else
    {
      //Siew Yeng - XCR-3883 - get isTopSide component
      zHeight = getEstimatedZHeight(region.isTopSide(), systemFiducialRectangle,
                                    jointType, estimatedBoardThickness, estimateIsValid, region);
    }
    byte shape = 0;
    byte focusMethodId = 0;
    if (estimateIsValid.getValue())
    {
      // using LimiteSurfaceModel - computed zHeight is okay - don't recompute again.
      focusMethodId = ReconstructionTypeEnum.map(FocusMethodEnum.USE_Z_HEIGHT);
      shape = FocusProfileShapeTypeEnum.map(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
    }
    else
    {
      // don't use LimitedSurfaceModel = use autofocus in IRP.
      zHeight = 0; // set to zero but NOT USED BY IRP in SHARPEST calculations.
      focusMethodId = ReconstructionTypeEnum.map(FocusMethodEnum.SHARPEST);
      shape = FocusProfileShapeTypeEnum.map(FocusProfileShapeEnum.NO_SEARCH_PREDICTED_SURFACE);
    }
    
    int zGuessFromPredictiveSliceHeightInNanometers = -1;
    // Loop through all focus groups and slices, changing focus profile shapes
    // and focus instructions as required
    for (FocusGroup focusGroup : region.getFocusGroups())
    {
      //XCR-2478, IRP Exception happened when abort at 30% during production
      if (reconstructedImagesProducer.checkForAbortInProgress() == true)
        return;
      
      boolean focusProfileShapeModified = false;
      for (Slice slice : focusGroup.getSlices())
      { 
        // Skip uninspected slices and surface modeling slices.
        // Surface modeling slices are skipped since  a) we're not adding stuff
        // to the surface model at this point, so adjusting these would be pointless,
        // and b) these are dummy slices for which the getZOffset call below will
        // crash if we include them.
        if (slice.createSlice() &&
            slice.getSliceName() != SliceNameEnum.SURFACE_MODELING)
        {
          // khang-wah.chnee, 2012-12-28, 2.5D
          SliceNameEnum sliceName = slice.getSliceName();
          if (slice.getSliceType().equals(SliceTypeEnum.PROJECTION)==true)
          {
            if (region.getInspectionFamilyEnum().equals(InspectionFamilyEnum.THROUGHHOLE) ||
                region.getInspectionFamilyEnum().equals(InspectionFamilyEnum.OVAL_THROUGHHOLE))//Siew Yeng - XCR-3318 - Oval PTH
            {
              sliceName = SliceNameEnum.THROUGHHOLE_BARREL;
            }
            else if (region.getInspectionFamilyEnum().equals(InspectionFamilyEnum.GRID_ARRAY))
            {
              sliceName = SliceNameEnum.MIDBALL;
            }
            else
            {
              continue;
            }
          }

          //Siew Yeng - XCR-3135 - fix irp exception for PCAP with PSH on
          //Lee Herng - to fix Gridarray and PCAP PSH showing the same image for all slice
          if(region.getInspectionFamilyEnum().equals(InspectionFamilyEnum.GRID_ARRAY)|| 
             slice.getSliceName().equals(SliceNameEnum.PCAP_SLUG)||
             slice.getSliceName().equals(SliceNameEnum.HIGH_SHORT))
          {
            if(estimateIsValid.getValue() == false)
              zHeight = -1;

            zGuessFromPredictiveSliceHeightInNanometers = zHeight;
            percent = slice.getFocusInstruction().getPercentValue();

            shape = FocusProfileShapeTypeEnum.map(focusGroup.getFocusSearchParameters().getFocusProfileShape()); 
            focusMethodId = ReconstructionTypeEnum.map(slice.getFocusInstruction().getFocusMethod());
          }

          // end
          if (isUnitTest == false && focusProfileShapeModified == false)
          {
            //XCR-2478, IRP Exception happened when abort at 30% during production
            if (reconstructedImagesProducer.checkForAbortInProgress() == true)
              return;
            
            // Only modify focus profile shape for focus groups containing
            // at least one slice whose focus instruction is also being modified
            reconstructedImagesProducer.modifyFocusProfileShape(testSubProgramId,
                                                                region,
                                                                focusGroup.getId(),
                                                                shape,
                                                                zGuessFromPredictiveSliceHeightInNanometers,
                                                                false,
                                                                focusGroup.getSearchRangeLowLimitInNanometers(),
                                                                focusGroup.getSearchRangeHighLimitInNanometers(),
                                                                focusGroup.getPspZOffsetInNanometers());
            focusProfileShapeModified = true;
          }

          if (reconstructedImagesProducer.checkForAbortInProgress() == true)
            return;
          
          // Lets also compute a new value for zOffset now that we have a more
          // accurate board thickness.   This is needed for both with and without
          // LSM availability because estimatedBoardThickness may have improved.
          int zOffset = programGenerator.getZOffset(jointType,
                                                    sliceName, // khang-wah.chnee, 2012-12-28, 2.5D
                                                    jointInspectionData.getSubtype(),
                                                    region.isTopSide(),
                                                    estimatedBoardThickness);
          //XCR-2478, IRP Exception happened when abort at 30% during production
          if (reconstructedImagesProducer.checkForAbortInProgress() == true)
            return;
          
          if (isUnitTest == false)
            reconstructedImagesProducer.modifyFocusInstruction(testSubProgramId,
                                                               region,
                                                               slice.getSliceName().getId(),
                                                               zHeight,
                                                               percent,
                                                               zOffset,
                                                               focusMethodId);
        }  // end of if slice.isCreated()
      } // end of slices
    } // end of focusGroups
    //XCR-2478, IRP Exception happened when abort at 30% during production
    if (reconstructedImagesProducer.checkForAbortInProgress() == true)
      return;
    
    reconstructedImagesProducer.kickStartSpecificRegion(testSubProgramId, region);
  }

  /**
   * @author Roy Williams
   */
  public int getNumberOfRegionsThatMustBeMarkedCompletePriorToExecutingAction()
  {
    return _regionsThatMustBeMarkedCompletePriorToUsingGlobalSurfaceModel;
  }

  /**
   * @author Roy Williams
   */
  public boolean hasComponentJointsWithoutNeighbors()
  {
    return _componentJointsWithoutNeighborsMap != null;
  }

  /**
   * @author Roy Williams
   */
  public Map<Component, Set<JointInspectionData>> getComponentJointsWithoutNeighborsMap()
  {
    Assert.expect(_componentJointsWithoutNeighborsMap != null);
    return _componentJointsWithoutNeighborsMap;
  }

  /**
   * @author Roy Williams
   */
  public int getTestSubProgramId()
  {
    return _testSubProgram.getId();
  }

  /**
   * @author Roy Williams
   */
  public Project getProject()
  {
    return _testSubProgram.getTestProgram().getProject();
  }

  /**
   * @author Poh Kheng
   */
  public void clearBreakPoint()
  {
    if (_imageGroupsRequiringGlobalSurfaceModel != null)
    {
      for(ImageGroup imageGroup : _imageGroupsRequiringGlobalSurfaceModel)
        imageGroup.clear();
        
      _imageGroupsRequiringGlobalSurfaceModel.clear();
    }
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public ScanPassBreakPointEnum getBreakPointEnum()
  {
    return ScanPassBreakPointEnum.SURFACE_MODEL;
  }
}
