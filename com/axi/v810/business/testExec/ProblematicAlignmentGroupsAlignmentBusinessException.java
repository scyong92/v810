package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;

/**
 * @author Matt Wharton
 */
public class ProblematicAlignmentGroupsAlignmentBusinessException extends AlignmentBusinessException
{
  /**
   * @author Matt Wharton
   */
  ProblematicAlignmentGroupsAlignmentBusinessException(TestSubProgram testSubProgram, String itemizedIssues)
  {
    super(testSubProgram, new LocalizedString("BS_PROBLEMATIC_ALIGNMENT_GROUPS_KEY", 
                                              new Object[]{testSubProgram.getMagnificationType().toString(), itemizedIssues}));
  }
}
