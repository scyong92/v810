package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * This InspectionEvent is fired by the StatisticalTuningEngine whenever a subtype's learned data is starting to
 * be deleted.
 *
 * @author Sunit Bhalla
 */
public class SubtypeDeletingLearnedDataStartedInspectionEvent extends InspectionEvent
{

  private Subtype _subtype;

  /*
   * @author Sunit Bhalla
   */
  public SubtypeDeletingLearnedDataStartedInspectionEvent(Subtype subtype)
  {
    super(InspectionEventEnum.SUBTYPE_DELETING_LEARNED_DATA_STARTED);
    Assert.expect(subtype != null);

    _subtype = subtype;
  }

  /**
   * @author Sunit Bhalla
   */
  public Subtype getSubtype()
  {
    Assert.expect(_subtype != null);

    return _subtype;
  }

}
