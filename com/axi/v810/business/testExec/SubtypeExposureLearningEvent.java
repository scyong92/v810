package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import java.util.*;

/**
 *
 * @author sheng-chuan.yong
 */
public class SubtypeExposureLearningEvent extends InspectionEvent
{
  private List<StageSpeedEnum> _speedCombinationList;
  private Subtype _subtype;

  /*
   * @author Sunit Bhalla
   */
  public SubtypeExposureLearningEvent(Subtype subtype, List<StageSpeedEnum> stageSpeedEnumList)
  {
    super(InspectionEventEnum.SUBTYPE_LEARNING_COMPLETED);
    Assert.expect(subtype != null);
    Assert.expect(stageSpeedEnumList != null);

    _speedCombinationList = stageSpeedEnumList;
    _subtype = subtype;
  }

  /**
   * @author Sunit Bhalla
   */
  public Subtype getSubtype()
  {
    Assert.expect(_subtype != null);

    return _subtype;
  }
  
  public List<StageSpeedEnum> getLearntExposureSetting()
  {
    Assert.expect(_speedCombinationList != null);

    return _speedCombinationList;
  }
}
