package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * This exception is thrown whenever the measured gray levels for a given alignment group are too different from
 * what was "learned" during Alignment Setup.
 *
 * @author Matt Wharton
 */
public class AlignmentAlgorithmGrayLevelsTooDifferentFromExpectedBusinessException extends AlignmentBusinessException
{
  /**
   * @author Matt Wharton
   */
  AlignmentAlgorithmGrayLevelsTooDifferentFromExpectedBusinessException(TestSubProgram testSubProgram,
                                                                        AlignmentGroup alignmentGroup,
                                                                        String failAlignmentImagePath)
  {
    super(testSubProgram, failAlignmentImagePath, new LocalizedString("BS_ALIGNMENT_ALGORITHM_GRAY_LEVELS_TOO_DIFFERENT_FROM_EXPECTED_KEY",
                                              new Object[]{testSubProgram.getMagnificationType().toString(), alignmentGroup.getName()}));
  }
}
