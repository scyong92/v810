package com.axi.v810.business.testExec;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Test class for AutoShortTermShutdownXrayProgressMonitor
 * @author YingKat Choor
 */
class Test_AutoShortTermShutdownXrayProgressMonitor extends UnitTest
{

  /**
   * @author YingKat Choor
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_AutoShortTermShutdownXrayProgressMonitor());
  }

  /**
   * @author YingKat Choor
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    AbstractXraySource xraySource = AbstractXraySource.getInstance();
    Assert.expect(xraySource instanceof HTubeXraySource);
    HTubeXraySource _htubeXraySource = (HTubeXraySource)xraySource;

    AutoShortTermShutdownXrayProgressMonitor _AutoShortTermShutdownXrayProgressMonitor = AutoShortTermShutdownXrayProgressMonitor.getInstance();

    // time out to shutdown xray after xray turn on and inactivity of a period of time.
    //
    long timeOutMilliSeconds = _AutoShortTermShutdownXrayProgressMonitor.getTimeOutMilliSeconds();

    try
    {
      _htubeXraySource.startup();

      try
      {
        // add 1000 is to give some time for xray to short term shutdown(in regressive test/simulation mode)
        // before call areXraysOn() to check xray new status
        //
        Thread.sleep(timeOutMilliSeconds + 5000);
      }
      catch (InterruptedException iex)
      {
        // do nothing
      }
      // if the AutoShortTermShutdownXrayProgressMonitor work,
      // the xray should be already off
      //
      Expect.expect(_htubeXraySource.areXraysOnForServiceMode() == false);

    }
    catch (XrayTesterException ex)
    {
      Expect.expect(false);
      ex.printStackTrace();
    }
  }
}
