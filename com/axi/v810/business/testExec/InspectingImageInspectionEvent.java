package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.config.Config;

/**
 * @author Matt Wharton
 * @author Patrick Lacz
 */
public class InspectingImageInspectionEvent extends InspectionEvent
{
  private ReconstructedImages _reconstructedImages;
  private SliceNameEnum _sliceNameEnum;
  private Image _imageToDisplay = null;

  // we keep our own reference count for this inspection event because we have an image that may be created
  // and I can't keep its reference count the same as the other images.
  private int _referenceCount;

  /**
   * @author Matt Wharton
   */
  public InspectingImageInspectionEvent(ReconstructedImages reconstructedImages,
                                        SliceNameEnum sliceNameEnum,
                                        InspectionEventEnum eventEnum)
  {
    super(eventEnum);

    Assert.expect(sliceNameEnum != null);
    Assert.expect(eventEnum.equals(InspectionEventEnum.BEGIN_INSPECTING_IMAGE) ||
                  eventEnum.equals(InspectionEventEnum.DONE_INSPECTING_IMAGE));

    _reconstructedImages = reconstructedImages;
    _reconstructedImages.incrementReferenceCount();

    _sliceNameEnum = sliceNameEnum;
    _referenceCount = 1;
  }

  /**
   * @author Matt Wharton
   */
  public ReconstructedImages getReconstructedImages()
  {
    Assert.expect(_reconstructedImages != null);

    return _reconstructedImages;
  }

  /**
   * @author Patrick Lacz
   * @author Khang Wah, Chnee
   * - remove 2.5d images from any contrast enhancement activity
   */
  public Image getImageToDisplay()
  {
    Assert.expect(_reconstructedImages != null);

    if (_imageToDisplay == null)
    {
      Map<SliceNameEnum, Image> contrastEnhancedImages = new HashMap<SliceNameEnum, Image>();
      for (ReconstructedSlice slice : _reconstructedImages.getReconstructedSlices())
        contrastEnhancedImages.put(slice.getSliceNameEnum(), new Image(slice.getOrthogonalImage()));

      ImageEnhancement.autoEnhanceContrastInPlace(ImageManager.getInstance().excludeUnnecessaryImageFromEnhancement(contrastEnhancedImages).values());

      _imageToDisplay = contrastEnhancedImages.get(_sliceNameEnum);
      _imageToDisplay.incrementReferenceCount(); // for the private reference

      for (Image image : contrastEnhancedImages.values())
        image.decrementReferenceCount();
    }
    _imageToDisplay.incrementReferenceCount(); // for the returned reference
    return _imageToDisplay;
  }

  /**
   * @author Matt Wharton
   */
  public ReconstructionRegion getInspectionRegion()
  {
    Assert.expect(_reconstructedImages != null);

    return _reconstructedImages.getReconstructionRegion();
  }

  /**
   * @author Matt Wharton
   */
  public SliceNameEnum getSliceNameEnum()
  {
    Assert.expect(_sliceNameEnum != null);

    return _sliceNameEnum;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void incrementReferenceCount()
  {
    Assert.expect(_referenceCount > 0);
    _referenceCount++;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void decrementReferenceCount()
  {
    Assert.expect(_referenceCount > 0);
    _referenceCount--;

    if (_referenceCount == 0)
    {
      Assert.expect(_reconstructedImages != null);
      _reconstructedImages.decrementReferenceCount();
      if (_imageToDisplay != null)
        _imageToDisplay.decrementReferenceCount();
      _reconstructedImages = null;
      _imageToDisplay = null;
    }
  }
}
