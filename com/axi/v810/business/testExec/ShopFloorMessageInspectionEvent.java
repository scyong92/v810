package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Khaw Chek Hau
 */
public class ShopFloorMessageInspectionEvent extends MessageInspectionEvent
{
  private DatastoreException _datastoreException;

  /**
   * @author Khaw Chek Hau
   */
  public ShopFloorMessageInspectionEvent(DatastoreException datastoreException)
  {
    super(datastoreException.getLocalizedString());
    _datastoreException = datastoreException;
  }

  /**
   * @author Khaw Chek Hau
   */
  public DatastoreException getDatastoreException()
  {
    Assert.expect(_datastoreException != null);

    return _datastoreException;
  }
}
