package com.axi.v810.business.testExec;

/**
 * @author Bill Darbie
 */
public class PanelLoadingIntoMachineInspectionEvent extends InspectionEvent
{
  /**
   * @author Bill Darbie
   */
  public PanelLoadingIntoMachineInspectionEvent()
  {
    super(InspectionEventEnum.PANEL_LOADING_INTO_MACHINE);
  }
}
