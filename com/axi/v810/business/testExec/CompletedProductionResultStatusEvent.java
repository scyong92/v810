package com.axi.v810.business.testExec;

/**
 * @author Khaw Chek Hau
 * @XCR3774: Display load & unload time during production
 */
public class CompletedProductionResultStatusEvent extends InspectionEvent
{
  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  public CompletedProductionResultStatusEvent()
  {
    super(InspectionEventEnum.INSPECTION_PRODUCTION_RESULT_STATUS);
  }
}
