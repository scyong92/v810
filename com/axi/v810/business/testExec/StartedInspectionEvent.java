package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * This InspectionEvent is fired by the TestExecutionEngine whenever an inspection commences.
 *
 * @author Matt Wharton
 * @author Peter Esbensen
 */
public class StartedInspectionEvent extends InspectionEvent
{
  private int _numberOfImagesToInspect;
  private ImageSetData _imageSetData;

  /**
   * @author Matt Wharton
   * @author Peter Esbensen
   */
  public StartedInspectionEvent(int numberOfImagesToInspect)
  {
    super(InspectionEventEnum.INSPECTION_STARTED);
    Assert.expect(numberOfImagesToInspect >= 0);

    _numberOfImagesToInspect = numberOfImagesToInspect;
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public int getNumberOfImagesToInspect()
  {
    Assert.expect(_numberOfImagesToInspect >= 0);

    return _numberOfImagesToInspect;
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasImageSetData()
  {
    return (_imageSetData != null);
  }

  /**
   * @author Matt Wharton
   */
  public ImageSetData getImageSetData()
  {
    Assert.expect(_imageSetData != null);

    return _imageSetData;
  }

  /**
   * @author Matt Wharton
   */
  void setImageSetData(ImageSetData imageSetData)
  {
    Assert.expect(imageSetData != null);

    _imageSetData = imageSetData;
  }
}
