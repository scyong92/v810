package com.axi.v810.business.testExec;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class ScriptRunner
{
  private static ScriptRunner _instance;

  private Runtime _runtime;
  private Config _config;
  private boolean _inspectPanel;
  private String _panelSerialNumber;
  private String _warningMessage;
  private String _errorMessage;
  
  private PreOrPostInspectionScriptOutputFileReader _inputReader = new PreOrPostInspectionScriptOutputFileReader();
  private PreOrPostInspectionScriptOutputFileReader _outputReader = new PreOrPostInspectionScriptOutputFileReader();
  
  private final String _PANELSERIALNUMBER = StringLocalizer.keyToString("CFGUI_PRE_POST_INSPECTION_SCRIPT_PANEL_SERIAL_NUMBER_KEY");
  private final String _ERRORMESSAGE = StringLocalizer.keyToString("CFGUI_PRE_POST_INSPECTION_SCRIPT_ERROR_MESSAGE_KEY");
  private final String _WARNINGMESSAGE = StringLocalizer.keyToString("CFGUI_PRE_POST_INSPECTION_SCRIPT_WARNING_MESSAGE_KEY");

  /**
   * @author Bill Darbie
   */
  public static synchronized ScriptRunner getInstance()
  {
    if (_instance == null)
      _instance = new ScriptRunner();
    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private ScriptRunner()
  {
    _runtime = Runtime.getRuntime();
    _config = Config.getInstance();
    
    _inputReader.setFilePath(FileName.getPreInspectionScriptInputConfigFullPath());
    _outputReader.setFilePath(FileName.getPreInspectionScriptOutputConfigFullPath());
  }

  /**
   * @author Bill Darbie
   */
  public void runPreInspectionScript(String panelSerialNumber, List<String> serialNumber, boolean isUsingBoardSerialNumber) throws DatastoreException, BusinessException
  {
    Assert.expect(panelSerialNumber != null);

    // set all variables that come from the input file to the correct defaults
    _inspectPanel = true;
    _panelSerialNumber = panelSerialNumber;
    _warningMessage = "";
    _errorMessage = "";

    if (_config.getBooleanValue(SoftwareConfigEnum.ENABLE_PRE_INSPECTION_SCRIPT))
    {
      String dir = Directory.getInspectionDataDir();
      if (FileUtilAxi.exists(dir) == false)
        FileUtilAxi.mkdirs(dir);

      // delete any previous output file
      String outputFileName = FileName.getPreInspectionScriptOutputDataFullPath();
      if (UnitTest.unitTesting() == false)
      {
        if (FileUtilAxi.exists(outputFileName))
          FileUtilAxi.delete(outputFileName);
      }

      // write out the file that the script might want to read in
      String inputFileName = FileName.getPreInspectionScriptInputDataFullPath();
      writeInputFileForPreInspectionScript(inputFileName, panelSerialNumber, serialNumber, isUsingBoardSerialNumber);

      String script = _config.getStringValue(SoftwareConfigEnum.PRE_INSPECTION_SCRIPT);
      runScript(script);

      // read in any output from the script
      if(isUsingBoardSerialNumber == false)
         readOutputFileFromPreInspectionScript(outputFileName, panelSerialNumber);
      else
         readOutputFileFromPreInspectionScript(outputFileName, serialNumber);
    }
  }

  /*
   * @author Kee Chin Seong
   */
  private void readOutputFileFromPreInspectionScript(String inputFileName,
                                                    List<String> panelSerialNumber) throws DatastoreException
  {
    Assert.expect(inputFileName != null);
    Assert.expect(panelSerialNumber != null);

    // expected syntax of this file:
    // if the file does not exist then the inspection will run
    // test: yes|no ( if set to no the panel will not be tested, it will be loaded and unloaded
    // panelSerialNumber: [string] (if any text is here, it will replace the original serial number)
    // warning: (any text here will be displayed in the messages area of test execution)
    // error: ( any text here will cause an error dialog will appear with this message)
    List<String>parameterSettings = null;
    if (UnitTest.unitTesting() == false)
    {
      try
      {
          _outputReader.readPreviousSequenceSettings();
          parameterSettings = _outputReader.getPreviousSequenceSettings();
      }
      catch(com.axi.v810.datastore.DatastoreException ex)
      {

      }
    }
    else
    {
      ArrayList<String> allSettings = new ArrayList<String>();
      allSettings.add(_PANELSERIALNUMBER);
      allSettings.add(_WARNINGMESSAGE);
      allSettings.add(_ERRORMESSAGE);
      parameterSettings = allSettings;
    }
    
    if (FileUtilAxi.exists(inputFileName))
    {
      String inputFileText = FileReaderUtilAxi.readFileContents(inputFileName);
      String[] lines = inputFileText.split("\\n");
      int lineNumber = 0;
      for (String line : lines)
      {
        ++lineNumber;
        // # is a comment
        if (line.startsWith("#"))
          continue;

        if (line.startsWith("test:"))
        {
          line = line.toLowerCase();
          line = line.trim();
          if (line.endsWith("yes"))
          {
            _inspectPanel = true;
          }
          else if (line.endsWith("no"))
          {
            _inspectPanel = false;
          }
          else
            throw new FileCorruptDatastoreException(inputFileName, "test: yes|no", line);
        }
        else if (line.startsWith("panelSerialNumber:") && parameterSettings.contains("panelSerialNumber"))
        {
          int index = line.indexOf(':');
          line = line.substring(index + 1, line.length());
          line =line.trim();
          _panelSerialNumber = line;
        }
        else if (line.startsWith("warning:") && parameterSettings.contains("warning"))
        {
          int index = line.indexOf(':');
          line = line.substring(index + 1, line.length());
          _warningMessage = line;
        }
        else if (line.startsWith("error:") && parameterSettings.contains("error"))
        {
          int index = line.indexOf(':');
          line = line.substring(index + 1, line.length());
          _errorMessage = StringUtil.convertAcsiiStringToUnicodeString(line);;
        }
      }
    }
  }
  
  /**
   * @author Bill Darbie
   */
  private void readOutputFileFromPreInspectionScript(String inputFileName,
                                                    String panelSerialNumber) throws DatastoreException
  {
    Assert.expect(inputFileName != null);
    Assert.expect(panelSerialNumber != null);

    // expected syntax of this file:
    // if the file does not exist then the inspection will run
    // test: yes|no ( if set to no the panel will not be tested, it will be loaded and unloaded
    // panelSerialNumber: [string] (if any text is here, it will replace the original serial number)
    // warning: (any text here will be displayed in the messages area of test execution)
    // error: ( any text here will cause an error dialog will appear with this message)
    List<String>parameterSettings = null;
    if (UnitTest.unitTesting() == false)
    {
      try
      {
          _outputReader.readPreviousSequenceSettings();
          parameterSettings = _outputReader.getPreviousSequenceSettings();
      }
      catch(com.axi.v810.datastore.DatastoreException ex)
      {

      }
    }
    else
    {
      ArrayList<String> allsetting = new ArrayList<String>();
      allsetting.add(_PANELSERIALNUMBER);
      allsetting.add(_WARNINGMESSAGE);
      allsetting.add(_ERRORMESSAGE);
      parameterSettings = allsetting;
    }
    
    if (FileUtilAxi.exists(inputFileName))
    {
      String inputFileText = FileReaderUtilAxi.readFileContents(inputFileName);
      String[] lines = inputFileText.split("\\n");
      int lineNumber = 0;
      for (String line : lines)
      {
        ++lineNumber;
        // # is a comment
        if (line.startsWith("#"))
          continue;

        if (line.startsWith("test:"))
        {
          line = line.toLowerCase();
          line = line.trim();
          if (line.endsWith("yes"))
          {
            _inspectPanel = true;
          }
          else if (line.endsWith("no"))
          {
            _inspectPanel = false;
          }
          else
            throw new FileCorruptDatastoreException(inputFileName, "test: yes|no", line);
        }
        else if (line.startsWith("panelSerialNumber:") && parameterSettings.contains("panelSerialNumber"))
        {
          int index = line.indexOf(':');
          line = line.substring(index + 1, line.length());
          line =line.trim();
          _panelSerialNumber = line;
        }
        else if (line.startsWith("warning:") && parameterSettings.contains("warning"))
        {
          int index = line.indexOf(':');
          line = line.substring(index + 1, line.length());
          _warningMessage = line;
        }
        else if (line.startsWith("error:") && parameterSettings.contains("error") )
        {
          int index = line.indexOf(':');
          line = line.substring(index + 1, line.length());
          _errorMessage = line;
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  boolean inspectPanel()
  {
    if (isPreInspectionScriptEnabled())
      return _inspectPanel;
    else
      return true;
  }

  /**
   * @author Bill Darbie
   */
  public String getPanelSerialNumber()
  {
    Assert.expect(_panelSerialNumber != null);
    return _panelSerialNumber;
  }

  /**
   * @author Bill Darbie
   */
  String getWarningMessage()
  {
    Assert.expect(_warningMessage != null);
    return _warningMessage;
  }

  /**
   * @author Bill Darbie
   */
  String getErrorMessage()
  {
    Assert.expect(_errorMessage != null);
    return _errorMessage;
  }

  /**
   * @author Bill Darbie
   * @author Kee Chin Seong
   * projectName: NT38017282+007_V1.0
   * serialNumber: 38017282-007 50830068
   * serialNumber: 38017282-007 50830065
   * userName: K
   * eqpCode: v810_3048
   */
  private void writeInputFileForPreInspectionScript(String fileName,
                                                    String panelSerialNumber,
                                                    List<String> serialNumberList,
                                                    boolean isUsingBoardSerialNumber) throws DatastoreException, BusinessException
  {
    Assert.expect(fileName != null);
    Assert.expect(panelSerialNumber != null);

    try
    {
        _inputReader.readPreviousSequenceSettings();
    }
    catch(com.axi.v810.datastore.DatastoreException ex)
    {
        
    }
    
    List<String>parameterSettings = _inputReader.getPreviousSequenceSettings();
    
    FileWriterUtil fileWriterUtil = new FileWriterUtil(fileName, false);
    try
    {
      fileWriterUtil.open();
      
      if(parameterSettings.contains("projectName"))
        fileWriterUtil.writeln("projectName: " + Project.getCurrentlyLoadedProject().getName());
      
      if(isUsingBoardSerialNumber == false)
        fileWriterUtil.writeln("panelSerialNumber: " + panelSerialNumber);
      else
      {
        for(String serialNumber : serialNumberList)
           fileWriterUtil.writeln("serialNumber: " + serialNumber); 
      }
      
      if(parameterSettings.contains("userName"))
        fileWriterUtil.writeln("userName: " + com.axi.v810.gui.mainMenu.LoginManager.getInstance().getUserNameFromLogIn());
      if(parameterSettings.contains("eqpCode"))
        fileWriterUtil.writeln("eqpCode: " + _config.getStringValue(HardwareConfigEnum.XRAYTESTER_SERIAL_NUMBER));
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      fileWriterUtil.close();
    }
  }
  
  /**
   * @author Bill Darbie
   */
  private void writeInputFileForPostInspectionScript(String fileName,
                                                     Project project,
                                                     String panelSerialNumber,
                                                     Map<String, String> boardNameToSerialNumberMap,
                                                     String currentXMLResultFullPath) throws DatastoreException, BusinessException
  {
    Assert.expect(fileName != null);
    Assert.expect(project != null);
    Assert.expect(panelSerialNumber != null);
    Assert.expect(boardNameToSerialNumberMap != null);
    Assert.expect(currentXMLResultFullPath != null);

    FileWriterUtil fileWriterUtil = new FileWriterUtil(fileName, false);
    try
    {
      fileWriterUtil.open();
      fileWriterUtil.writeln("projectName: " + project.getName());
      fileWriterUtil.writeln("panelSerialNumber: " + panelSerialNumber);
      fileWriterUtil.writeln("resultsFilePath: " + currentXMLResultFullPath);
      for (Map.Entry<String, String> entry : boardNameToSerialNumberMap.entrySet())
      {
        String boardName = entry.getKey();
        String serialNumber = entry.getValue();
        fileWriterUtil.writeln("boardNameAndSerialNumber: " + boardName + " " + serialNumber);
      }
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      fileWriterUtil.close();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void runScript(String script) throws BusinessException
  {
    Assert.expect(script != null);
    if (script.length() == 0)
    {
      ErrorExecutingCommandBusinessException busEx = new ErrorExecutingCommandBusinessException(script);
      throw busEx;
    }

    // if the script command contains a space in the path to it, then we must put quotes around the
    // entire string or it will fail to execute
    if ((script.startsWith("\"") == false) && (script.contains(" ")))
      script = "\"" + script + "\"";

    // run the script
    Process process = null;
    try
    {
      File dir = new File(Directory.getInspectionDataDir());
      // run the process with the working directory being the inspection data directory
      process = _runtime.exec(script, null, dir);
    }
    catch (IOException ioe)
    {
      ErrorExecutingCommandBusinessException busEx = new ErrorExecutingCommandBusinessException(script);
      busEx.initCause(ioe);
      throw busEx;
    }
    try
    {
      process.waitFor();
    }
    catch (InterruptedException iex)
    {
      // do nothing
    }
    if (process.exitValue() != 0)
      throw new ErrorExecutingCommandBusinessException(script);
  }

  /**
   * @author Bill Darbie
   */
  void runPostInspectionScript(Project project, String panelSerialNumber, Map<String, String> boardNameToSerialNumberMap, String currentXMLResultFullPath) throws DatastoreException, BusinessException
  {
    Assert.expect(project != null);
    Assert.expect(panelSerialNumber != null);
    Assert.expect(boardNameToSerialNumberMap != null);
    Assert.expect(currentXMLResultFullPath != null);

    if (_config.getBooleanValue(SoftwareConfigEnum.ENABLE_POST_INSPECTION_SCRIPT))
    {
      while(ResultsProcessor.isResultProcessDone() == false)
      {
        try
        {
          Thread.sleep(200);
        }
        catch(InterruptedException ie)
        {
          // do nothing
        }
      }

      String dir = Directory.getInspectionDataDir();
      if (FileUtilAxi.exists(dir) == false)
        FileUtilAxi.mkdirs(dir);
      String fileName = FileName.getPostInspectionScriptDataFullPath();

      writeInputFileForPostInspectionScript(fileName, project, panelSerialNumber, boardNameToSerialNumberMap, currentXMLResultFullPath);

      String script = _config.getStringValue(SoftwareConfigEnum.POST_INSPECTION_SCRIPT);
      runScript(script);
    }
  }

  /**
   * @author Bill Darbie
   */
  public boolean isPreInspectionScriptEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.ENABLE_PRE_INSPECTION_SCRIPT);
  }

  /**
   * @author Bill Darbie
   */
  public void setPreInspectionScriptEnabled(boolean enabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.ENABLE_PRE_INSPECTION_SCRIPT, enabled);
  }

  /**
   * @author Bill Darbie
   */
  public String getPreInspectionScript()
  {
    return _config.getStringValue(SoftwareConfigEnum.PRE_INSPECTION_SCRIPT);
  }

  /**
   * @author Bill Darbie
   */
  public void setPreInspectionScript(String scriptPathAndName) throws DatastoreException
  {
    Assert.expect(scriptPathAndName != null);
    _config.setValue(SoftwareConfigEnum.PRE_INSPECTION_SCRIPT, scriptPathAndName);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isPostInspectionScriptEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.ENABLE_POST_INSPECTION_SCRIPT);
  }

  /**
   * @author Bill Darbie
   */
  public void setPostInspectionScriptEnabled(boolean enabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.ENABLE_POST_INSPECTION_SCRIPT, enabled);
  }

  /**
   * @author Bill Darbie
   */
  public String getPostInspectionScript()
  {
    return _config.getStringValue(SoftwareConfigEnum.POST_INSPECTION_SCRIPT);
  }

  /**
   * @author Bill Darbie
   */
  public void setPostInspectionScript(String scriptPathAndName) throws DatastoreException
  {
    Assert.expect(scriptPathAndName != null);
    _config.setValue(SoftwareConfigEnum.POST_INSPECTION_SCRIPT, scriptPathAndName);
  }
}
