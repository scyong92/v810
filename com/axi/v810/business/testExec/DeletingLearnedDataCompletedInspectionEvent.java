package com.axi.v810.business.testExec;

/**
 * This InspectionEvent is fired by the StatisticalTuningEngine whenever deleting learned data is completed.
 *
 * @author Sunit Bhalla
 */
public class DeletingLearnedDataCompletedInspectionEvent extends InspectionEvent
{

  /**
   * @author Sunit Bhalla
   */
  public DeletingLearnedDataCompletedInspectionEvent()
  {
    super(InspectionEventEnum.DELETING_LEARNED_DATA_COMPLETED);
  }

}
