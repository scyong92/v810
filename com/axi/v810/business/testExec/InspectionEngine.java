package com.axi.v810.business.testExec;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.algorithmLearning.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.shopFloorSystem.*;

/**
 * Business layer interface for running inspections. This class coordinates
 * image acquisition, image analysis, and result reporting.
 *
 * PE: Note that this subsystem is incomplete right now. Bill will be adding in
 * functionality to handle production mode testing like barcode handling, light
 * stack management, etc. He may add that code to this class or he might be
 * wrapping this class with another one that contains that additional
 * functionality
 *
 * At a high level, this class is fairly simple. It kicks off image acquisition
 * and then starts processing the reconstructed images by passing them on to the
 * image acquisition subsystem. Once the image analysis is complete, it send the
 * JointInspectionResults and images to the results subystem to be put into our
 * results database.
 *
 * The one aspect that is kind of tricky is the interface to the Image Analysis
 * subsystem. As you might expect, for each ReconstructionRegion, we call
 * ImageAnalysis.classifyJoint on each joint in the region. However, once we've
 * called classifyJoint on all joints of a component of the same subtype, we
 * then call classifyMeasurementGroup. The classifyMeasurementGroup method
 * handles classifications that require knowledge about all the other joints on
 * the component of the same subtype. The Open Outlier indictment is an example
 * of one such defect call. Anyways, the TestExecutionEngine has to keep track
 * of when we can call classifyMeasurementGroup, so there is some complexity
 * around that.
 *
 * @author Peter Esbensen
 */
public class InspectionEngine
{
  private static InspectionEngine _instance;
  private static Config _config = Config.getInstance();

  private Project _project;

  private boolean _abortRequested;
  private WorkerThread _imageAcquisitionWorkerThread = new WorkerThread("image acquisition thread");
  private WorkerThread _opticalImageAcquisitionWorkerThread = new WorkerThread("optical image acquisition thread");
  private WorkerThread _alignmentOpticalImageAcquisitionWorkerThread = new WorkerThread("alignment optical image acquisition thread");
  private ImageAcquisitionEngine _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
  private OpticalImageAcquisitionEngine _opticalImageAcquisitionEngine = OpticalImageAcquisitionEngine.getInstance();
  private WorkerThread _mainImageAnalysisThread = new WorkerThread("main image analysis thread");
  private InspectionStateController _inspectionStateController = InspectionStateController.getInstance();
  private InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();

  private Map<Component, Map<Subtype, MeasurementGroup>> _componentToSubtypeToMeasurementGroupMap
    = new ConcurrentHashMap<Component, Map<Subtype, MeasurementGroup>>();

  private Map<Pad, ReconstructionRegion> _padToInspectionRegionMap = new ConcurrentHashMap<Pad, ReconstructionRegion>();
  private ReconstructedImagesMemoryManager _reconstructedImagesCachingManager
    = _imageAcquisitionEngine.getReconstructedImagesProducer().getReconstructedImagesManager().getReconstructedImagesMemoryManager();

  // Create a Map that contains the inspection region id/inspection region pair.
  // Inspection regions can be accessed more easily this way.
  private Map<Integer, ReconstructionRegion> _regionsToBeInspected
    = Collections.synchronizedMap(new HashMap<Integer, ReconstructionRegion>());
  private JointInspectionResultObservable _jointInspectionResultObservable = JointInspectionResultObservable.getInstance();
  private PanelInspectionSettings _panelInspectionSettings;
  private ImageAcquisitionModeEnum _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_IMAGE_SETS;
  private boolean _areAnyDiagnosticsOn = false;
  private boolean _precisionTuningImageAcquisition = false;
  private boolean _production;
  private boolean _productionTuneEnabled = false;
  private boolean _productionOrAlwaysGenerateAndProcessResultsForUnitTests; // = _production || ResultsProcessor.getTestResultsProcessingEnabled()
  private boolean _doSurfaceModeling = true;

  private Throwable _workerThreadException;
  private Alignment _alignment = Alignment.getInstance();

  // Reference to the "focus confirmation engine".
  private FocusConfirmationEngine _focusConfirmationEngine;

  // Inspection results writers.
  private InspectionResultsWriter _inspectionResultsWriter = new InspectionResultsWriter();
  private InspectionResultsXMLWriter _resultsXmlWriter;
  private InspectionMeasurementsXMLWriter _measurementsXmlWriter;

  private static final boolean _logAlgorithmProfileData
    = _config.getBooleanValue(SoftwareConfigEnum.LOG_ALGORITHM_PROFILE_DATA);
  private CSVFileWriterAxi _gullwingProfileDataWriter;
  private CSVFileWriterAxi _shortProfileDataWriter;

  private static final boolean _DROP_INSPECTION_IMAGES
    = _config.getBooleanValue(SoftwareConfigEnum.DROP_INSPECTION_IMAGES);

  private long _inspectionStartTime = -1;
  private long _inspectionStopTime = -1;

  private AtomicInteger _numberOfProcessedJoints = new AtomicInteger(0);
  private AtomicInteger _numberOfDefectiveJoints = new AtomicInteger(0);
  private AtomicInteger _numberOfDefectiveComponents = new AtomicInteger(0);

  private boolean _captureImagesForLastTest = true;
  private ImageGeneration _imageGeneration = ImageGeneration.getInstance();

  private List<String> _savedRepairImages = Collections.synchronizedList(new ArrayList<String>());
  private ImageManager _imageManager = ImageManager.getInstance();

  private BooleanLock _acquiringImages = new BooleanLock(false);

  private ImageSetData _onlineImageSetData;
  private ImageSetData _imageSetDataUnderTest;
  private ImageSetInfoWriter _imageSetInfoWriter = new ImageSetInfoWriter();
  private XrayTester _xRayTester = XrayTester.getInstance();

  private RepairImagesConfigManager _repairImagesConfigManager;
  private boolean _limitNumRepairImagesToSave = false;
  private int _numRepairImagesToSave = -1;

  private TestExecutionTimer _testExecutionTimer;
  private AtomicBoolean _firstInspectionImage = new AtomicBoolean(false);

  private Set<ReconstructionRegion> _unfreedImages = Collections.synchronizedSet(new HashSet<ReconstructionRegion>());
  private boolean _isInspectionInProgress = false;

  private boolean _generateMeasurementsXML = false;
  private boolean _currentBoardIsXout = false;

  private ProductionTuneManager _productionTuneManager;
  private Set<ReconstructionRegion> _productionTuneDefectiveReconstructionRegions = new HashSet<ReconstructionRegion>();
  private Set<ReconstructionRegion> _precisionTuningReconstructionRegions = new HashSet<ReconstructionRegion>();

  /**
   * @todo LC remove this before ship - investigating call effectiveness
   */
  private CSVFileWriterAxi _failedJointsAndComponentsDataWriter;
  private static boolean _FLAG_CALL_RATE_SPIKES = _config.getBooleanValue(SoftwareConfigEnum.FLAG_CALL_RATE_SPIKES);

  private static PerformanceLogUtil _performanceLog = PerformanceLogUtil.getInstance();

  // Chin Seong, Kee, Manual Alignment fixed
  private BooleanLock _waitingForUserInput = new BooleanLock(false);
  private BooleanLock _waitingForAlignmentExceptionActionUserInput = new BooleanLock(false);

  private BooleanLock _acquiringOpticalImages = new BooleanLock(false);
  private BooleanLock _acquiringAlignmentOpticalImages = new BooleanLock(false);
  private AtomicInteger _numberOfAlignmentRegionsProcessed = new AtomicInteger(0);
  private BooleanLock _abortOpticalImageAcquisitionFlag = new BooleanLock(false);
  private BooleanLock _abortAlignmentOpticalImageAcquisitionFlag = new BooleanLock(false);
  private boolean _isLowMag = false;
//  private List<Integer> _mixMagnificationSavedReconstructedRegionIdList = new LinkedList<Integer>();//Siew Yeng - no longer use

  private Board _previousBoard = null;

  //Lim, Lay Ngor - XCR1743:Benchmark
  //Set _CLASSIFY_JOINT_TIME_STAMP to true to print the algorithm time stamp if needed
  public static final boolean _CLASSIFY_JOINT_TIME_STAMP = false;
  
  //Siew Yeng - XCR-2168
  private Map<Component, List<MeasurementGroup>> _componentToMeasurementGroupsMap =
      new ConcurrentHashMap<Component, List<MeasurementGroup>>();
  
  public static final boolean _RUN_RESULTS_XML_AND_BINARY_PROCESSOR_IN_PARALLEL = true;
  private static WorkerThread _resultWorkerThread1 = new WorkerThread("Results Binary Processor");
  private static WorkerThread _resultWorkerThread2 = new WorkerThread("Results XML Processor");
  private XrayTesterException _xrayTesterExceptionInResultThread = null;
  
  
  //Kee Chin Seong - S2EX speedup fine tune boolean lock in order make sure eveyone is working proper before proceed
  

  protected static boolean _debug = Config.isDeveloperDebugModeOn();
  private static String _me = "InspectionEngine";

  //Khaw Chek Hau - XCR2654: CAMX Integration
  private AbstractShopFloorSystem _shopFloorSystem = AbstractShopFloorSystem.getInstance();
  
  //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
  private MachineUtilizationReportSystem _machineUtilizationReportSystem = MachineUtilizationReportSystem.getInstance();
  
  //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
  private VOneMachineStatusMonitoringSystem _vOneMachineStatusMonitoringSystem = VOneMachineStatusMonitoringSystem.getInstance();
  
  //Khaw Chek Hau - XCR3670: V-ONE AXI latest status monitoring log
  private MachineLatestStatusLogWriter _machineLatestStatusLogWriter = MachineLatestStatusLogWriter.getInstance();

  private List<Subtype> _subtypeStichImageForVVTS = new ArrayList<Subtype>();
  /**
   * @author Peter Esbensen
   */
  public static synchronized InspectionEngine getInstance()
  {
    if (_instance == null)
      _instance = new InspectionEngine();

    return _instance;
  }

  /**
   * @author Peter Esbensen
   */
  private InspectionEngine()
  {
    _resultsXmlWriter = InspectionResultsXMLWriter.getInstance();
    _measurementsXmlWriter = InspectionMeasurementsXMLWriter.getInstance();
    _repairImagesConfigManager = RepairImagesConfigManager.getInstance();
    _focusConfirmationEngine = FocusConfirmationEngine.getInstance();
    _generateMeasurementsXML = Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MEASUREMENTS_XML_FILE);
    _productionTuneManager = ProductionTuneManager.getInstance();
  }

  /**
   * Attempts to abort an inspection in progress. This may not occur
   * instantaneouly. No results will be written. This method will return
   * immediately, but you shouldn't assume that the abort has finished. The
   * abort is finished only when the original "inspect" call returns.
   *
   * @author Peter Esbensen
   */
  public synchronized void abort() throws XrayTesterException
  {
    if (_isInspectionInProgress == false)
      return;

    _abortRequested = true;
    
    //Khaw Chek Hau - XCR2654: CAMX Integration
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
    {
      try
      {
        _shopFloorSystem.inspectionAbort();
      }
      catch (DatastoreException ex)
      {
        DatastoreException datastoreException = (DatastoreException) ex;
        MessageInspectionEvent event = new ShopFloorMessageInspectionEvent(datastoreException);
        InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
        inspectionEventObservable.sendEventInfo(event);
      }
    }

    // Release any diagnostic pauses.
    _inspectionStateController.clearPauseRequests();
    _inspectionStateController.resumeInspection();

    // If diagnostics are enabled, disable them now.
    if (_areAnyDiagnosticsOn)
    {
      _panelInspectionSettings.clearAllAlgorithmDiagnosticSettings();
    }

    // Tell the optical image acquisition engine to stop acquiring images
    _opticalImageAcquisitionEngine.abort();

    // Tell the image acquisition engine to stop acquiring images.
    _imageAcquisitionEngine.abort();
        
    //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
    if (TestExecution.getInstance().didUserAbort() && _project != null)
    {
      TestProgram testProgram = _project.getTestProgram();
      testProgram.setRealignPerformed(false);
      testProgram.setRerunningProduction(false);
    
      for (TestSubProgram subProgram : testProgram.getAllTestSubPrograms())
      {
        subProgram.setInspectionDone(false);
      }  
    }
  }

  /**
   * @author Matt Wharton
   */
  public int getNumberOfProcessedJoints()
  {  
    return _numberOfProcessedJoints.get();
  }

  /**
   * @author Matt Wharton
   */
  private void incrementNumberOfProcessedJoints()
  {    
    //Siew Yeng - subtask XCR-2497 - no need to increment count if user abort
    if(TestExecution.getInstance().didUserAbort())
      return;
    
    for (Board board : _project.getPanel().getBoards())
    {
      //if (_project.getPanel().getBoards().size() > 1 )
      if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
      {
        if (board.isXedOut() == false)
        {
          //XCR1624 - Wrong display of panel pins proccesed - Siew Yeng
          if(board.isBoardAlignmentExceptionCaught())
          {
            if(_imageAcquisitionEngine.isAlignmentExceptionCaught())
            {
              //Siew Yeng - subtask XCR2543
              if(TestExecution.getInstance().isAlignmentFailAndUnloadPanel() || TestExecution.getInstance().isAlignmentFailAndRerunAlignment())
              {
                //Siew Yeng - reset to zero if unload board
                _numberOfProcessedJoints.set(0);
                break;
              }
              else
                continue;
            }
            
            if(TestExecution.getInstance().shouldPanelBeEjectedIfAlignmentFails())
            {
              //Siew Yeng - reset to zero if unload board
              _numberOfProcessedJoints.set(0);
              break;
            }
          }
          _numberOfProcessedJoints.addAndGet(board.getNumInspectedJoints());
        }
      }
      else
      {
        //Khaw Chek Hau - XCR3385: X-out board by defect threshold setting
        if (board.isXedOut() == false)
        {
          //XCR1624 - Wrong display of panel pins proccesed - Siew Yeng
          if(_imageAcquisitionEngine.isAlignmentExceptionCaught())
          {
            continue;
          }

          //Khaw Chek Hau - XCR2286: Improvement of V810 "SKIP" Function for Panel Based Alignment
          if(_project.getTestProgram().isBoardsToFilterOnEmpty() || _project.getTestProgram().isBoardFiltered(board))
          {
            _numberOfProcessedJoints.addAndGet(board.getNumInspectedJoints());
          }
        }
      }
    }
    //_numberOfProcessedJoints.incrementAndGet();
  }

  /**
   * @author Matt Wharton
   */
  public int getNumberOfDefectiveJoints()
  {
    return _numberOfDefectiveJoints.get();
  }

  /**
   * @author Matt Wharton
   */
  private void incrementNumberOfDefectiveJoints()
  {
    //Siew Yeng - subtask XCR-2497 - no need to increment count if user abort
    if(TestExecution.getInstance().didUserAbort())
      return;
    
    for (Board board : _project.getPanel().getBoards())
    {
      //if (_project.getPanel().getBoards().size() > 1) 
      if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
      {
        if (board.isXedOut() == false)
        {
          //XCR1624 - Wrong display of panel pins proccesed - Siew Yeng
          if(board.isBoardAlignmentExceptionCaught())
          {
            if(_imageAcquisitionEngine.isAlignmentExceptionCaught())
            {
              //Siew Yeng - subtask XCR2543
              if(TestExecution.getInstance().isAlignmentFailAndUnloadPanel() || TestExecution.getInstance().isAlignmentFailAndRerunAlignment())
              {
                //Siew Yeng - reset to zero if unload board
              _numberOfDefectiveJoints.set(0);
                break;
              }
              else
                continue;
            }

            if(TestExecution.getInstance().shouldPanelBeEjectedIfAlignmentFails())
            {
              //Siew Yeng - reset to zero if unload board
              _numberOfDefectiveJoints.set(0);
              break;
            }
          }
          _numberOfDefectiveJoints.addAndGet(board.getNumFailedJoints());
        }
      }
      else
      {
        //Khaw Chek Hau - XCR3385: X-out board by defect threshold setting
        if (board.isXedOut() == false)
        {
          //XCR1624 - Wrong display of panel pins proccesed - Siew Yeng
          if(_imageAcquisitionEngine.isAlignmentExceptionCaught())
          {
            continue;
          }
          _numberOfDefectiveJoints.addAndGet(board.getNumFailedJoints());
        }
      }
    }
    //_numberOfDefectiveJoints.incrementAndGet();
  }

  /**
   * @author Peter Esbensen
   */
  int getNumberOfDefectiveComponents()
  {
    return _numberOfDefectiveComponents.get();
  }

  /**
   * @author Peter Esbensen
   */
  private void incrementNumberOfDefectiveComponents()
  {
    //Siew Yeng - subtask XCR-2497 - no need to increment count if user abort
    if(TestExecution.getInstance().didUserAbort())
      return;
    
    for (Board board : _project.getPanel().getBoards())
    {
      //if (_project.getPanel().getBoards().size() > 1)
      if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
      {
        if (board.isXedOut() == false)
        {
          //XCR1624 - Wrong display of panel pins proccesed - Siew Yeng
          if(board.isBoardAlignmentExceptionCaught())
          {
            if(_imageAcquisitionEngine.isAlignmentExceptionCaught())
            {
              //Siew Yeng - subtask XCR2543
              if(TestExecution.getInstance().isAlignmentFailAndUnloadPanel() || TestExecution.getInstance().isAlignmentFailAndRerunAlignment())
              {
                //Siew Yeng - reset to zero if unload board
                _numberOfDefectiveComponents.set(0);
                break;
              }
              else
                continue;
            }

            if(TestExecution.getInstance().shouldPanelBeEjectedIfAlignmentFails())
            {
              //Siew Yeng - reset to zero if unload board
              _numberOfDefectiveComponents.set(0);
              break;
            }
          }
          _numberOfDefectiveComponents.addAndGet(board.getNumFailedComponents());
        }
      }
      else
      {
        //Khaw Chek Hau - XCR3385: X-out board by defect threshold setting
        if (board.isXedOut() == false)
        {
          //XCR1624 - Wrong display of panel pins proccesed - Siew Yeng
          if(_imageAcquisitionEngine.isAlignmentExceptionCaught())
          {
            continue;
          }
          _numberOfDefectiveComponents.addAndGet(board.getNumFailedComponents());
        }
      }
    }
    //_numberOfDefectiveComponents.incrementAndGet();
  }

  /**
   * Run an inspection using the hardware to get images.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void inspectForProduction(TestProgram testProgram,
                                   TestExecutionTimer testExecutionTimer,
                                   String panelSerialNumber,
                                   Map<String, String> boardNameToSerialNumberMap) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(panelSerialNumber != null);
    Assert.expect(boardNameToSerialNumberMap != null);

    try
    {
      _isInspectionInProgress = true;
      _production = true;
      _precisionTuningImageAcquisition = false;
      _imageAcquisitionEngine.setIsGeneratingPrecisionImageSet(_precisionTuningImageAcquisition);
      _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS;
      _testExecutionTimer = testExecutionTimer;

      if (_productionTuneManager.isProjectEnabledForProductionTune(testProgram.getProject().getName()))
      {
        _productionTuneDefectiveReconstructionRegions.clear();
        // create an online image set
        createOnlineImageSetData(testProgram);
        // set the num images to zero, this will be modified on the fly
        _onlineImageSetData.setNumberOfImagesSaved(0);
        String description = StringLocalizer.keyToString(new LocalizedString("BUS_PRODUCTION_TUNE_IMAGE_SET_DESCRIPTION_KEY",
                                                                      new Object[] {panelSerialNumber}));
        _onlineImageSetData.setUserDescription(description);
        _onlineImageSetData.setSystemDescription(description);
      }

      inspect(testProgram, null, panelSerialNumber, boardNameToSerialNumberMap);

      //Commented out by Jack Hwee - to fix the intermittent image set data error after production run
//      // finally, we need to re-save some image set data if we have saved defect images
//      if (_productionTuneManager.isProjectEnabledForProductionTune(testProgram.getProject().getName()))
//      {
//        if (_onlineImageSetData.getNumberOfImagesSaved() > 0)
//        {
//          _imageSetInfoWriter.write(_onlineImageSetData);
//          _imageManager.saveProductionTuneDefectImageInfo(_onlineImageSetData,
//                                                          testProgram,
//                                                          _productionTuneDefectiveReconstructionRegions);
//        }
//      }
    }
    finally
    {
      _isInspectionInProgress = false;
    }
  }

  /**
   * Run an inspection using the hardware to accquire a precision tuning image
   * set
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   * @author John Heumann
   */
  public void inspectForPrecisionTuningImageAcquisition(
    TestProgram testProgram,
    TestExecutionTimer testExecutionTimer,
    ImageSetData imageSetData) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(imageSetData != null);

    try
    {
      _isInspectionInProgress = true;
      _production = true;
      _precisionTuningImageAcquisition = true;
      _imageAcquisitionEngine.setIsGeneratingPrecisionImageSet(_precisionTuningImageAcquisition);
      _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS;
      _testExecutionTimer = testExecutionTimer;
      _onlineImageSetData = imageSetData;
      _precisionTuningReconstructionRegions.clear();

      String serialNumberOrDescription = imageSetData.getUserDescription();
      inspect(testProgram, imageSetData, serialNumberOrDescription,
              buildBoardNameToSerialNumMapForTestDevelpment(testProgram));

      // Save the precision tuning image set info
      if (_onlineImageSetData.getNumberOfImagesSaved() > 0)
      {
        _imageSetInfoWriter.write(_onlineImageSetData);
        _imageManager.saveInspectionImageInfo(_onlineImageSetData, testProgram);
      }
    }
    finally
    {
      _isInspectionInProgress = false;
      _precisionTuningReconstructionRegions.clear();
    }
  }

  /**
   * Run an inspection using the hardware to get images.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void inspectForTestDevelopment(TestProgram testProgram,
                                        TestExecutionTimer testExecutionTimer,
                                        String description) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(description != null);

    try
    {
      _isInspectionInProgress = true;
      _production = false;
      _precisionTuningImageAcquisition = false;
      _imageAcquisitionEngine.setIsGeneratingPrecisionImageSet(_precisionTuningImageAcquisition);
      _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS;
      _testExecutionTimer = testExecutionTimer;

      updateOnlineImageSetData(testProgram);

      inspect(testProgram,
              null,
              description,
              buildBoardNameToSerialNumMapForTestDevelpment(testProgram));

    }
    finally
    {
      _isInspectionInProgress = false;
    }
  }

  /**
   * @author George A. David
   */
  private void createOnlineImageSetData(TestProgram testProgram) throws DatastoreException
  {
    Assert.expect(testProgram != null);

    Project project = testProgram.getProject();

    _onlineImageSetData = new ImageSetData();
    long currentTimeInMillis = System.currentTimeMillis();
    _onlineImageSetData.setImageSetName(Directory.getDirName(currentTimeInMillis));
    _onlineImageSetData.setBoardPopulated(true);
    _onlineImageSetData.setDateInMils(currentTimeInMillis);
    _onlineImageSetData.setProjectName(project.getName());
    _onlineImageSetData.setTestProgramVersionNumber(project.getTestProgramVersion());
    _onlineImageSetData.setUserDescription("run time image set");
    _onlineImageSetData.setImageSetTypeEnum(ImageSetTypeEnum.PANEL);
    _onlineImageSetData.setSystemDescription("run time image set");
    _onlineImageSetData.setNumberOfImagesSaved(testProgram.getNumberOfInspectionImages());
    _onlineImageSetData.setMachineSerialNumber(_xRayTester.getSerialNumber());
    _onlineImageSetData.setIsOnlineTestDevelopment(true);
    _onlineImageSetData.setGenerateMultiAngleImages(project.isGenerateMultiAngleImage()); // Bee Hoon, Recipe Settings
    FileUtilAxi.createDirectory(_onlineImageSetData.getDir());
    _imageSetInfoWriter.write(_onlineImageSetData);
    _imageManager.saveInspectionImageInfo(_onlineImageSetData, testProgram);
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   * @author Aimee Ong
   */
  private void updateOnlineImageSetData(TestProgram testProgram) throws XrayTesterException
  {
    createOnlineImageSetDataIfNecessary(testProgram);
    saveOnlineImageSetInfosIfNecessary(testProgram);
  }

  /**
   * @author Aimee Ong
   */
  private void createOnlineImageSetDataIfNecessary(TestProgram testProgram) throws XrayTesterException
  {
    // If don't have an "online image set data", let's start by creating one.
    if (_onlineImageSetData == null
      || _imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS))

    {
      // delete any existing online image sets
      FileUtilAxi.deleteDirectoryContents(Directory.getOnlineXrayImagesDir());

      createOnlineImageSetData(testProgram);
    }
  }

  /**
   * @author Aimee Ong
   */
  private void saveOnlineImageSetInfosIfNecessary(TestProgram testProgram) throws XrayTesterException
  {
    // If don't have an "online image set data", let's start by creating one.
    if (_onlineImageSetData != null
      && _imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS) == false)
    {
      // ok, we already have some images saved.
      // with this next test, we are going to append
      // images to it. So we need to update some info.
      Set<String> inspectionImageInfo = _imageManager.getInspectionImageInfos(testProgram);
      Set<String> savedImageNames = _imageManager.getInspectionImageInfo(_onlineImageSetData);
      if (savedImageNames.containsAll(inspectionImageInfo) == false)
      {
        // ok, now combine them and reset the number of images.
        inspectionImageInfo.addAll(savedImageNames);
        _onlineImageSetData.setNumberOfImagesSaved(inspectionImageInfo.size());
        _imageSetInfoWriter.write(_onlineImageSetData);
        _imageManager.saveImageInfo(inspectionImageInfo, FileName.getInspectionImageInfoProgramFullPathForOnlineTestDevelopment(_onlineImageSetData.getImageSetName()), false);
      }
    }
  }

  /**
   * @author Matt Wharton
   */
  void dropOnlineImageSetInfo()
  {
    _onlineImageSetData = null;
  }

  /**
   * @author Matt Wharton
   * @author Aimee Ong
   */
  boolean needToUseHardwareToGenerateTemporaryOnlineImageSet(TestProgram testProgram) throws DatastoreException
  {
    Assert.expect(testProgram != null);

    // If we don't have a valid "online image set data" reference, we definitely will need to use the hardware.
    if (_onlineImageSetData == null)
    {
      return true;
    }

    boolean missingImages = false;
    List<ReconstructionRegion> inspectionReconstructionRegions = testProgram.getFilteredInspectionRegions();
    Set<ReconstructionRegion> recoRegionsWithCompatibleImages = _imageManager.whichReconstructionRegionsHaveCompatibleImages(_onlineImageSetData, inspectionReconstructionRegions);
    if (recoRegionsWithCompatibleImages.size() != inspectionReconstructionRegions.size())
      missingImages = true;

    return missingImages;
  }

  /**
   * Run an inspection using the specified image set data.
   *
   * @author Bill Darbie
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void inspectForTestDevelopmentUsingImageSetData(TestProgram testProgram,
                                                         TestExecutionTimer testExecutionTimer,
                                                         ImageSetData imageSetData,
                                                         String description) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(imageSetData != null);
    Assert.expect(description != null);

    try
    {
      _isInspectionInProgress = true;
      _production = false;
      _precisionTuningImageAcquisition = false;
      _imageAcquisitionEngine.setIsGeneratingPrecisionImageSet(_precisionTuningImageAcquisition);
      _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_IMAGE_SETS;
      _testExecutionTimer = testExecutionTimer;

      _imageSetDataUnderTest = imageSetData;
      inspect(testProgram, imageSetData, description, buildBoardNameToSerialNumMapForTestDevelpment(testProgram));
    }
    finally
    {
      _isInspectionInProgress = false;
    }
  }

  /**
   * Run an inspection using image set data
   *
   * @author Bill Darbie
   * @author Aimee Ong
   */
  public void inspectForTestDevelopmentUsingHardwareWhenNecessary(TestProgram testProgram,
                                                                  TestExecutionTimer testExecutionTimer,
                                                                  String description) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(description != null);

    try
    {
      _isInspectionInProgress = true;
      _production = false;
      _precisionTuningImageAcquisition = false;
      _imageAcquisitionEngine.setIsGeneratingPrecisionImageSet(_precisionTuningImageAcquisition);
      _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_HARDWARE_WHEN_NECESSARY;
      _testExecutionTimer = testExecutionTimer;

      // ok, save any images we don't have to disk, than run an inspection on those images.
      if (needToUseHardwareToGenerateTemporaryOnlineImageSet(testProgram))
      {
        createOnlineImageSetDataIfNecessary(testProgram);
        _imageGeneration.acquireOfflineImagesIfNecessary(testProgram, testExecutionTimer, _onlineImageSetData);
        saveOnlineImageSetInfosIfNecessary(testProgram);
      }

      inspectForTestDevelopmentUsingImageSetData(testProgram,
                                                 testExecutionTimer,
                                                 _onlineImageSetData,
                                                 description);
    }
    finally
    {
      _isInspectionInProgress = false;
      // we need to set this back, because the previous call will set this to USE_IMAGE_SETS.
      // this is used for tracking at the beginning of this function.
      _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_HARDWARE_WHEN_NECESSARY;
    }
  }

  /**
   * Take care of some initial bookkeeping and kick the inspection off on the
   * main image analysis thread.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   * @author Bill Darbie
   */
  private void inspect(TestProgram testProgram,
                       final ImageSetData imageSetData,
                       String panelSerialNumberOrDescription,
                       Map<String, String> boardNameToSerialNumberMap) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect( (imageSetData != null) ||
                   (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS) == false) );
    Assert.expect(panelSerialNumberOrDescription != null);
    Assert.expect(boardNameToSerialNumberMap != null);

    Assert.expect(_panelInspectionSettings != null);
    setAbortRequested(false);

    _project = testProgram.getProject();
    _resultsXmlWriter.setProductionMode(_production);
    if (_generateMeasurementsXML)
      _measurementsXmlWriter.setProductionMode(_production);

    final TestProgram finalTestProgram = testProgram;

    try
    {
      initialize(testProgram,
                 panelSerialNumberOrDescription,
                 boardNameToSerialNumberMap,
                 imageSetData);

      _mainImageAnalysisThread.invokeAndWait(new Runnable()
      {
        public void run()
        {
          try
          {
            // Kok Chun, Tan - XCR3051 - reset the flag before inspection
            finalTestProgram.resetIsTestSubProgramDone();
            
            if( _production && 
                _imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS) == false &&
                finalTestProgram.hasVariableMagnification())
            {
              for(MagnificationTypeEnum magnificationType : MagnificationTypeEnum.getMagnificationToEnumMap().values())
              {
                if (getAbortRequested())
                  return;
                if(magnificationType.equals(MagnificationTypeEnum.LOW))
                {
                  _isLowMag=true;
                }
                else
                {
                  _isLowMag=false;
                }
                
                //Khaw Chek Hau - XCR2541: Assert when misalignment happened during production
                boolean isAnyLowMagAlignmentFail = false;
                
                if (finalTestProgram.isRerunningProduction() && _isLowMag)
                {
                  for (TestSubProgram subProgram : finalTestProgram.getFilteredScanPathTestSubPrograms())
                  {
                    if (subProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW) && subProgram.isInspectionDone() == false)
                      isAnyLowMagAlignmentFail = true;  
                  }
                  
                  if (isAnyLowMagAlignmentFail == false)
                    continue;
                }
                
                finalTestProgram.clearTestSubProgramFilters();
                for(TestSubProgram subProgram : finalTestProgram.getAllInspectableTestSubPrograms())
                {
                  if(subProgram.getMagnificationType().equals(magnificationType))
                    finalTestProgram.addFilter(subProgram);
                }
                _regionsToBeInspected.clear();
                for (ReconstructionRegion inspectionRegion : finalTestProgram.getFilteredInspectionRegionsFromFilterTestSubProgram())
                {
                  _regionsToBeInspected.put(inspectionRegion.getRegionId(),
                                            inspectionRegion);                  
                }
                _unfreedImages.clear();

                // Initialize the IAE.
                _imageAcquisitionEngine.initialize();
                _waitingForAlignmentExceptionActionUserInput.setValue(false);

                // Initialize the alignment process.
                Assert.expect(_alignment != null);
                _alignment.initializeAlignmentProcess(finalTestProgram);

                inspect(finalTestProgram, imageSetData);
                _padToInspectionRegionMap.clear();
              }
            }
            else
            {
              if(finalTestProgram.getAllTestSubPrograms().get(0).getMagnificationType().equals(MagnificationTypeEnum.LOW))
              {
                _isLowMag=true;
              }
              else
              {
                _isLowMag=false;
              }
              inspect(finalTestProgram, imageSetData);
            }
          }
          catch (Throwable ex)
          {
            handleWorkerThreadException(ex);
          }
        }
      });
    }
    finally
    {
      // added by Jack Hwee - to make sure image set data is write before the temp folder image set has been delete,
      //                      only production run will need this
      // finally, we need to re-save some image set data if we have saved defect images
      if (_precisionTuningImageAcquisition == false && _production == true)
        saveProductionTuneDefectImageInfo(testProgram);
      endInspection(testProgram, panelSerialNumberOrDescription, boardNameToSerialNumberMap);
    }
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  private void initialize(TestProgram testProgram,
                          String panelSerialNumber,
                          Map<String, String> boardNameToSerialNumberMap,
                          ImageSetData userSpecifiedImageSetData) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(panelSerialNumber != null);
    Assert.expect(boardNameToSerialNumberMap != null);
    Assert.expect( (userSpecifiedImageSetData != null) ||
                   (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS) == false) );

    //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
    //Don't reset _inspectionStartTime if manual realignment is performed
    if (testProgram.isRealignPerformed() == false || testProgram.getProject().getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      _inspectionStartTime = System.currentTimeMillis();
	  
      //Khaw Chek Hau - XCR2654: CAMX Integration
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
      {
        try
        {
          _shopFloorSystem.inspectionStart();
        }
        catch (DatastoreException ex)
        {
          DatastoreException datastoreException = (DatastoreException) ex;
          MessageInspectionEvent event = new ShopFloorMessageInspectionEvent(datastoreException);
          InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
          inspectionEventObservable.sendEventInfo(event);
        }
      }
      
      //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT) && _production && _precisionTuningImageAcquisition == false)
      {
        try
        {
          _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.PRODUCTION_RUNNING);
        }
        catch (DatastoreException ex)
        {
          DatastoreException datastoreException = (DatastoreException) ex;
          MessageInspectionEvent event = new VOneMachineStatusMonitoringMessageInspectionEvent(datastoreException);
          InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
          inspectionEventObservable.sendEventInfo(event);
        }
      }
    }

    TestExecution.getInstance().setAlignmentFailAndUnloadPanel(false);
    TestExecution.getInstance().setAlignmentFailedAndRerunAlignment(false); //Siew Yeng - XCR1757
    // Initialize the IAE.
    _imageAcquisitionEngine.initialize();
    _waitingForAlignmentExceptionActionUserInput.setValue(false);

    // Initialize the alignment process.
    Assert.expect(_alignment != null);
    _alignment.initializeAlignmentProcess(testProgram);
    
    //Khaw Chek Hau - XCR3578: Assert when skip serial number for board based alignment
    boolean isInProductionMode = false;
    
    if (_production && _precisionTuningImageAcquisition == false)
      isInProductionMode = true;

    // Initialize the Xed-out condition
    //XCR2541 - Assert when misalignment happened during production
    if (testProgram.isRealignPerformed() && (testProgram.getProject().getPanel().getPanelSettings().isPanelBasedAlignment() == false))
    {
      for (Board board : testProgram.getProject().getPanel().getBoards())
        board.initialize(true, isInProductionMode);
    }
    else
    {
      for (Board board : testProgram.getProject().getPanel().getBoards())
        board.initialize(false, isInProductionMode);
    }

    // Reset the ZHeightEstimator
    Project project = testProgram.getProject();
    Panel panel = project.getPanel();
    ZHeightEstimator.getInstance().resetForNewInspection(panel.getThicknessInNanometers(), project.getName()); //Siew Yeng - XCR-3781
    XYLocationEstimator.getInstance().resetForNewInspection();

    // Open the connection to the algorithm shorts learning file only if it's not already open.
    AlgorithmShortsLearningReaderWriter algorithmShortsLearning = _project.getAlgorithmShortsLearningReaderWriter();
    if (algorithmShortsLearning.isOpen() == false)
    {
      algorithmShortsLearning.open();
    }

    // Open the connection to the algorithm expected image learning file only if it's not already open.
    AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning = _project.getAlgorithmExpectedImageLearningReaderWriter();
    if (algorithmExpectedImageLearning.isOpen() == false)
    {
      algorithmExpectedImageLearning.open();
    }
    
	//Broken Pin
    // Open the connection to the algorithm shorts learning file only if it's not already open.
    AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning = _project.getAlgorithmBrokenPinLearningReaderWriter();
    if (algorithmBrokenPinLearning.isOpen() == false)
    {
      algorithmBrokenPinLearning.open();
    }    

    //ShengChuan - Clear Tombstone
    AlgorithmExpectedImageTemplateLearningReaderWriter algorithmExpectedImageTemplateLearning = _project.getAlgorithmExpectedImageTemplateLearningReaderWriter();
    if (algorithmExpectedImageTemplateLearning.isOpen() == false)
    {
      algorithmExpectedImageTemplateLearning.open();
    }

    // Tell the "unfocused region retester" that we're ready to go.
    _focusConfirmationEngine.initialize();

    // Make sure that the Short and Expected Image learning status is up to date.
    PanelSettings panelSettings = panel.getPanelSettings();
    panelSettings.syncShortLearningStatusForAllSubtypes();
    panelSettings.syncExpectedImageLearningStatusForAllSubtypes();
    panelSettings.syncBrokenPinLearningStatusForAllSubtypes();//Broken Pin
    panelSettings.syncExpectedImageTemplateLearningStatusForAllSubtypes();//ShengChuan - Clear Tombstone

    // Open the connection to the results writer and set the timestamp
    String resultImagesDirectory = Directory.getInspectionResultsDir(project.getName(), _inspectionStartTime);
    FileUtilAxi.createDirectory(resultImagesDirectory);

    // Initialize production tuning boolean
    if (_production)
      _productionTuneEnabled = _productionTuneManager.isProjectEnabledForProductionTune(_project.getName());
    else
      _productionTuneEnabled = false;

    // make sure production tuning is off if we are collecting a precision image set
    if (_precisionTuningImageAcquisition)
      _productionTuneEnabled = false;

    if (_logAlgorithmProfileData)
    {
      openProfileDataFiles();
    }

    boolean imagesCollectedOnline = false;
    String imageSetName = "";
    String imageSetUserDescription = "";
    if ((_production == false) && (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS)))
    {
      imagesCollectedOnline = true;
    }
    else if (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_WHEN_NECESSARY))
    {
      Assert.expect(_production == false);
      imagesCollectedOnline = true;
    }
    else if (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS))
    {
      Assert.expect(_production == false);
      imagesCollectedOnline = false;
    }
    else if (_production == true)
    {
      if (_productionTuneEnabled)
      {
        imagesCollectedOnline = false;
        imageSetName = _onlineImageSetData.getImageSetName();
        imageSetUserDescription = _onlineImageSetData.getUserDescription();
      }
      else if (_precisionTuningImageAcquisition == true)
      {
        imagesCollectedOnline = true;
      }
    }
    else
    {
      Assert.expect(false);
    }

    if (userSpecifiedImageSetData != null)
    {
      imageSetName = userSpecifiedImageSetData.getImageSetName();
      imageSetUserDescription = userSpecifiedImageSetData.getUserDescription();
    }
    else
    {
      if (imagesCollectedOnline)
      {
        imageSetName = _onlineImageSetData.getImageSetName();
        imageSetUserDescription = _onlineImageSetData.getUserDescription();
      }
      // if we are online doing production test execution, there is no image set being saved,
      // so let imageSetName and the user description be the empty strings
    }
    
    //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
    //Don't perform open method if manual realignment is performed
    if (testProgram.isRerunningProduction() == false || testProgram.getProject().getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      _testExecutionTimer.startInspectionResultsWriterTimer();
      _inspectionResultsWriter.open(_project,
                                    _inspectionStartTime,
                                    panelSerialNumber,
                                    boardNameToSerialNumberMap,
                                    imageSetName,
                                    imageSetUserDescription,
                                    imagesCollectedOnline);
      _testExecutionTimer.stopInspectionResultsWriterTimer();
      for (TestSubProgram subProgram : testProgram.getAllTestSubPrograms())
        subProgram.setInspectionDone(false);
    }

    testProgram.clearSliceIdToReconstructedSliceMap();
    _productionOrAlwaysGenerateAndProcessResultsForUnitTests = _production || ResultsProcessor.getInstance().getGenerateAndProcessTestDevelopmentResultsForUnitTests();

    if (_productionOrAlwaysGenerateAndProcessResultsForUnitTests)
    {
      _testExecutionTimer.startXmlWriterTimer();
      _resultsXmlWriter.open(_project,
                             _inspectionStartTime,
                             panelSerialNumber,
                             boardNameToSerialNumberMap,
                             "yes");
      if (_generateMeasurementsXML)
        _measurementsXmlWriter.open(_project,
                               _inspectionStartTime,
                               panelSerialNumber,
                               boardNameToSerialNumberMap,
                               "yes");
      _testExecutionTimer.stopXmlWriterTimer();
      /** @todo LC - remove before ship (investigate call effectiveness */
      if (_FLAG_CALL_RATE_SPIKES)
      {
        String failedJointsAndComponentsDataFileName = Directory.getTempDir() + File.separator + "failedJointsAndComponentsData.csv";
        _failedJointsAndComponentsDataWriter = new CSVFileWriterAxi(Arrays.asList("Project", "Inspection Start", "Comp Name", "Subtype", "Joint Name", "Indictment Name"));
        _failedJointsAndComponentsDataWriter.open(failedJointsAndComponentsDataFileName, true);
      }
    }

    // Set the diagnostics flag based on the panel inspection settings.
    _areAnyDiagnosticsOn = _panelInspectionSettings.areAnyDiagnosticsOn();

    // Clear any pending pause requests.
    _inspectionStateController.clearPauseRequests();

    // If diagnostics are on, we want to queue up a pause on the next diagnostic.
    if (_areAnyDiagnosticsOn)
    {
      _inspectionStateController.requestPauseOnNextDiagnostic();
    }

    // Set the worker thread exception reference to null.
    _workerThreadException = null;

    // Reset the joints processed/defective counts to zero.
    _numberOfProcessedJoints.set(0);
    _numberOfDefectiveJoints.set(0);
    _numberOfDefectiveComponents.set(0);

    // Reset the number of processed alignment regions to zero
    _numberOfAlignmentRegionsProcessed.set(0);

    // increment the run id so that results can be kept in sync
    testProgram.startNewInspectionRun();

    // initialize whether all repair images should be saved or not and if so, how many
    _limitNumRepairImagesToSave = _repairImagesConfigManager.isLimitRepairImagesEnabled();
    if (_limitNumRepairImagesToSave == true)
    {
      _numRepairImagesToSave = _repairImagesConfigManager.getNumRepairImagesToSave();
    }
    else
    {
      _numRepairImagesToSave = Integer.MAX_VALUE;
    }
    _savedRepairImages.clear();

    _regionsToBeInspected.clear();

    for (ReconstructionRegion inspectionRegion : testProgram.getFilteredInspectionRegions())
    {
      _regionsToBeInspected.put(inspectionRegion.getRegionId(),
                                inspectionRegion);
      //chin-seong,kee - when fine tuning, number of processed joint will be calculate at here, to avoid to calculate whole
      //board processed joint as fine tuning have filter up the inspection
      //XCR-3403, Total joint processed is different by using TIFF image
      if(_production == false)
        _numberOfProcessedJoints.addAndGet(inspectionRegion.getInspectablePads().size());
    }

    _unfreedImages.clear();

    ImageAnalysis.resetTimers();

    // Post a message that we're starting an inspection.
    StartedInspectionEvent inspectionStartedEvent = new StartedInspectionEvent(_regionsToBeInspected.size());
    if (userSpecifiedImageSetData != null)
    {
      inspectionStartedEvent.setImageSetData(userSpecifiedImageSetData);
    }
    _inspectionEventObservable.sendEventInfo(inspectionStartedEvent);

    // Always start with surface modeling enabled
    _doSurfaceModeling = true;
  }

  /**
   * @author Matt Wharton
   */
  private void initializePadToInspectionRegionMap(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);
    Assert.expect(_padToInspectionRegionMap != null);

    synchronized (_padToInspectionRegionMap)
    {
      _padToInspectionRegionMap.clear();
      for (ReconstructionRegion inspectionRegion : testProgram.getFilteredInspectionRegionsFromFilterTestSubProgram())
      {
        for (Pad pad : inspectionRegion.getPads())
        {
          _padToInspectionRegionMap.put(pad, inspectionRegion);
        }
      }
    }
  }

  /**
   * @author Sunit Bhalla
   */
  private void raiseComponentLevelTestNotRunWarning(String componentAndSubtypeString)
  {
    Assert.expect(componentAndSubtypeString != null);
    LocalizedString warningText = new LocalizedString("ALGDIAG_COMPONENT_LEVEL_TEST_NOT_RUN_KEY",
                                                      new Object[] { componentAndSubtypeString});
    InspectionEvent inspectionEvent = new AlgorithmMessageInspectionEvent(warningText);
    Assert.expect (_inspectionEventObservable != null);
    _inspectionEventObservable.sendEventInfo(inspectionEvent);
  }

  /**
   * @author Sunit Bhalla
   */
  private void checkForUntestedMeasurementGroups(Map.Entry<Component, Map<Subtype, MeasurementGroup>> componentToSubtypeToMeasurementGroupMap)
  {
    Assert.expect(componentToSubtypeToMeasurementGroupMap != null);

    Component component = componentToSubtypeToMeasurementGroupMap.getKey();
    String componentName = component.getReferenceDesignator();

    Map<Subtype, MeasurementGroup> subtypeToMeasurementGroupMap = componentToSubtypeToMeasurementGroupMap.getValue();
    for (Map.Entry<Subtype, MeasurementGroup> subtypeToMeasurementGroupMapItem : subtypeToMeasurementGroupMap.entrySet())
    {
      Subtype subtype = subtypeToMeasurementGroupMapItem.getKey();
      if  (subtype.getInspectionFamily().classifiesAtComponentOrMeasurementGroupLevel() || 
           _componentToMeasurementGroupsMap.isEmpty() == false) //Siew Yeng - XCR2263 - this is to display component level test not running
        raiseComponentLevelTestNotRunWarning(componentName + " (subtype " + subtype.getShortName() + ") ");
    }
  }

  /**
   * Finish up an inspection that went through all the scheduled images. Stuff
   * that needs to be done, even if we didn't see all images (as in an abort
   * sequence) should be put in the endInspection() method instead.
   *
   * @author Peter Esbensen
   */
  private void finishRemainingClassification() throws XrayTesterException
  {
    // If the user asked for a partial inspection (ie- like a single pad), then
    // there may be some MeasurementGroups that still need to be classified.
    // Go ahead and classify them now.
    synchronized (_componentToSubtypeToMeasurementGroupMap)
    {
      //Siew Yeng - XCR-3253 - Result not tally when run test with re-subtype component
      //Need to remove this after implement BGA mixed subtype at 5.9(special handle for BGA 1x1 only)
      if(Project.getCurrentlyLoadedProject().isBGAInspectionRegionSetTo1X1())
      {
        if(_componentToMeasurementGroupsMap.isEmpty() == false)
        {
          Collection<List<MeasurementGroup>> measurementGroupLists = _componentToMeasurementGroupsMap.values();
          for(List<MeasurementGroup> measurementGroups : measurementGroupLists)
          {
            for(MeasurementGroup measurementGroup : measurementGroups)
            {
              if(measurementGroup.getSubtype().getInspectionFamilyEnum() == InspectionFamilyEnum.GRID_ARRAY)
              {
                // finalize the joints in that measurementGroup
                for (JointInspectionData groupJointInspectionDataObject : measurementGroup.getInspectableJointInspectionDataObjects())
                {
                  if (groupJointInspectionDataObject.jointInspectionResultExists())
                  {
                    finalizeInspectionOfJoint(groupJointInspectionDataObject);
                  }
                }
              }
            }
          }
        }
      }
      
      for (Map.Entry<Component, Map<Subtype, MeasurementGroup>> componentToSubtypeToMeasurementGroupMap : _componentToSubtypeToMeasurementGroupMap.entrySet())
      {
        checkForUntestedMeasurementGroups(componentToSubtypeToMeasurementGroupMap);

        Map<Subtype, MeasurementGroup> subtypeToMeasurementGroupMap = componentToSubtypeToMeasurementGroupMap.getValue();
        ComponentInspectionData componentInspectionData = null;
        for (Iterator subtypeIt = subtypeToMeasurementGroupMap.values().iterator(); subtypeIt.hasNext();)
        {
          MeasurementGroup measurementGroup = (MeasurementGroup)subtypeIt.next();
          componentInspectionData = measurementGroup.getComponentInspectionData();

          // classify the measurementGroup
          // classifyAndProcessResults(measurementGroup, _padToInspectionRegionMap);

          // finalize the joints in that measurementGroup
          for (JointInspectionData groupJointInspectionDataObject : measurementGroup.getInspectableJointInspectionDataObjects())
          {
            if (groupJointInspectionDataObject.jointInspectionResultExists())
            {
              finalizeInspectionOfJoint(groupJointInspectionDataObject);
            }
          }
        }
        Assert.expect(componentInspectionData != null);
        // at this point, we have processed all the measurementGroups on the component
        // write its result and clean up.

        finalizeInspectionOfComponent(componentInspectionData);
        boolean possiblyProcessingPartialTest = true;
        freeImageMemoryForMeasurementGroupsIfPossible(subtypeToMeasurementGroupMap.values(), possiblyProcessingPartialTest);
      }
    }
  }

  /**
   * Do end-of-inspection cleanup. This method is called even when the
   * inspection is only partially completed. Anything that requires a full
   * inspection should be put in the endCompleteInspection() method instead.
   *
   * @author Peter Esbensen
   */
  private void endInspection(TestProgram testProgram, String panelSerialNumber, Map<String, String> boardNameToSerialNumberMap) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(panelSerialNumber != null);
    Assert.expect(boardNameToSerialNumberMap != null);
    
    _xrayTesterExceptionInResultThread = null;
    // Clear any pending pause requests.
    _inspectionStateController.clearPauseRequests();

    _componentToSubtypeToMeasurementGroupMap.clear();
    _componentToMeasurementGroupsMap.clear(); // Siew Yeng - XCR-2168

    _padToInspectionRegionMap.clear();
    _inspectionStopTime = System.currentTimeMillis();
    
    _subtypeStichImageForVVTS.clear();

    //Khaw Chek Hau - XCR2654: CAMX Integration
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM) && _production && _precisionTuningImageAcquisition == false)
    {
      try
      {
        _shopFloorSystem.inspectionEnd();
      }
      catch (DatastoreException ex)
      {
        DatastoreException datastoreException = (DatastoreException) ex;
        MessageInspectionEvent event = new ShopFloorMessageInspectionEvent(datastoreException);
        InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
        inspectionEventObservable.sendEventInfo(event);
      }
    }
    
    //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT) && _production && _precisionTuningImageAcquisition == false)
    {
      try
      {
        int numberOfBoards = testProgram.getProject().getPanel().getBoards().size();
        if (getAbortRequested())
          numberOfBoards = 0;
        _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.PRODUCTION_RUN, numberOfBoards, _testExecutionTimer.getElapsedFullInspectionTimeInMillis(), testProgram.getProject().getName());
      }
      catch (DatastoreException ex)
      {
        DatastoreException datastoreException = (DatastoreException) ex;
        MessageInspectionEvent event = new MachineUtilizationReportMessageInspectionEvent(datastoreException);
        InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
        inspectionEventObservable.sendEventInfo(event);
      }
    }

    //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT) && _production && _precisionTuningImageAcquisition == false)
    {
      try
      {
        _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.PRODUCTION_STOP);
      }
      catch (DatastoreException ex)
      {
        DatastoreException datastoreException = (DatastoreException) ex;
        MessageInspectionEvent event = new VOneMachineStatusMonitoringMessageInspectionEvent(datastoreException);
        InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
        inspectionEventObservable.sendEventInfo(event);
      }
    }
    
    XrayTesterException xrayTesterException = null;

    boolean haveGeneratedXmlResults = false;
    final BooleanLock haveGeneratedXmlResultsInParallel = new BooleanLock(false);
    
    //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
    //Don't perform close method if manual realignment is performed
    if (testProgram.isRealignPerformed() == false || testProgram.getProject().getPanel().getPanelSettings().isPanelBasedAlignment())
    {
      //XCR-3350, Assert When Running Fine Tuning with Multiple Image Set
      if (_RUN_RESULTS_XML_AND_BINARY_PROCESSOR_IN_PARALLEL == true && _production)
      {
        _resultWorkerThread1.invokeLater(new Runnable()
        {
          public void run()
          {
            try
            {
              _testExecutionTimer.startInspectionResultsWriterTimer();
              _inspectionResultsWriter.close(_inspectionStopTime);
              _testExecutionTimer.stopInspectionResultsWriterTimer();
            }
            catch (XrayTesterException ex)
            {
              setXrayTesterExceptionInResultThread(ex);
	          // throw this towards the end of this method after other clean up work is done
              //              if (xrayTesterException == null)
              //                xrayTesterException = ex;
            }
          }
        });

      }
      else
      {
        try
        {
          _testExecutionTimer.startInspectionResultsWriterTimer();
          _inspectionResultsWriter.close(_inspectionStopTime);
          _testExecutionTimer.stopInspectionResultsWriterTimer();
        }
        catch (XrayTesterException ex)
        {
          // throw this towards the end of this method after other clean up work is done
          if (xrayTesterException == null)
          {
            xrayTesterException = ex;
          }
        }

      }
    }
    
    //Khaw Chek Hau - XCR2464: Software Skip Alignment for Right Side Of Long Panel After Realign In Production
    for (TestSubProgram subProgram : testProgram.getTestSubPrograms())
    {
      subProgram.setIsUsingRuntimeManualAlignmentTransform(false);
    }

    if (getAbortRequested())
    {
      XrayTesterException ex = abortResultsWriting();
      //set this flag to true so that the other thread will not wait forever
      haveGeneratedXmlResultsInParallel.setValue(true);
      if (xrayTesterException == null)
        xrayTesterException = ex;
    }
    else // no abort
    {
      if (_productionOrAlwaysGenerateAndProcessResultsForUnitTests)
      {
        //XCR-3350, Assert When Running Fine Tuning with Multiple Image Set
        //Lim, Lay Ngor - XCR-3454 - Software hang when finish run offline test with offline result generation set to true.
        if (_RUN_RESULTS_XML_AND_BINARY_PROCESSOR_IN_PARALLEL == true && _productionOrAlwaysGenerateAndProcessResultsForUnitTests)
        {
          _resultWorkerThread2.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                _testExecutionTimer.startXmlWriterTimer();
                _resultsXmlWriter.close(_inspectionStopTime);
                if (_generateMeasurementsXML)
                {
                  _measurementsXmlWriter.close(_inspectionStopTime);
                }
                _testExecutionTimer.stopXmlWriterTimer();
                haveGeneratedXmlResultsInParallel.setValue(true);
              }
              catch (XrayTesterException ex)
              {
                setXrayTesterExceptionInResultThread(ex);
	              // throw this towards the end of this method after other clean up work is done
                //              if (xrayTesterException == null)
                //                xrayTesterException = ex;
              }
            }
          });
        }
        else
        {
          try
          {
            _testExecutionTimer.startXmlWriterTimer();
            _resultsXmlWriter.close(_inspectionStopTime);
            if (_generateMeasurementsXML)
            {
              _measurementsXmlWriter.close(_inspectionStopTime);
            }
            _testExecutionTimer.stopXmlWriterTimer();
            haveGeneratedXmlResults = true;
          }
          catch (XrayTesterException ex)
          {
            // throw this towards the end of this method after other clean up work is done
            if (xrayTesterException == null)
            {
              xrayTesterException = ex;
            }
          }
        }
      }
      else
      {
          //Amos Chee - XCR-3227 - UnitTest Update for V810 (Phase 1)
		  //Will not print anything under unit test enviroment
          if(UnitTest.unitTesting() == false)
          {
              System.out.println("_productionOrAlwaysGenerateAndProcessResultsForUnitTests == false");
          }
      }
    }
    
    if (_xrayTesterExceptionInResultThread != null)
    {
      if (xrayTesterException == null)
      {
        xrayTesterException = _xrayTesterExceptionInResultThread;
      }
    }

    // something went wrong; don't process the results
    if (xrayTesterException != null)
    {
      if (_RUN_RESULTS_XML_AND_BINARY_PROCESSOR_IN_PARALLEL == true && _production)
      {
        haveGeneratedXmlResultsInParallel.setValue(false);
      }
      else
      {
        haveGeneratedXmlResults = false;
      }
    }

    String productionTuneImageSetFullPath = "";
    if (_production)
    {
      if (_productionTuneEnabled)
        productionTuneImageSetFullPath = _onlineImageSetData.getDir();
    }
    
    if (xrayTesterException == null && _productionOrAlwaysGenerateAndProcessResultsForUnitTests)
    {
      try
      {
        haveGeneratedXmlResultsInParallel.waitUntilTrue();
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
        Assert.expect(false);
      }
    }
    
    if (_RUN_RESULTS_XML_AND_BINARY_PROCESSOR_IN_PARALLEL == true && _production)
    {
      ResultsProcessor.getInstance().queueInspectionResultForProcessing(testProgram,
        _inspectionStartTime,
        panelSerialNumber,
        boardNameToSerialNumberMap, haveGeneratedXmlResultsInParallel.isTrue(),
        productionTuneImageSetFullPath);
    }
    else
    {

      ResultsProcessor.getInstance().queueInspectionResultForProcessing(testProgram,
        _inspectionStartTime,
        panelSerialNumber,
        boardNameToSerialNumberMap, haveGeneratedXmlResults,
        productionTuneImageSetFullPath);
    }

    if (_logAlgorithmProfileData)
    {
      closeProfileDataFiles();
    }

    // Tell the "unfocused region retester" that we're all done.
    _focusConfirmationEngine.inspectionComplete();

    //Chin-Seong, Kee - Xout Implementation, for Xout Board, there will be no included calculation inside
    //Chin-Seong, Kee - Only Production will calculate all, if not is fine tuning calculate process joint only
    if(_production == true)
        incrementNumberOfProcessedJoints();

    incrementNumberOfDefectiveJoints();
    incrementNumberOfDefectiveComponents();
    
    //Khaw Chek Hau - XCR3670: V-ONE AXI latest status monitoring log
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT) && _production && _precisionTuningImageAcquisition == false)
    {
      try
      {
        _machineLatestStatusLogWriter.writeMachineLatestStatusLog(MachineLatestStatusElementEventEnum.PRODUCTION_RUNNING_RECIPE, 
                                                                  _project.getName(), 
                                                                  _project.getSelectedVariationName(), 
                                                                  panelSerialNumber, 
                                                                  String.valueOf(getNumberOfProcessedJoints()), 
                                                                  String.valueOf(getNumberOfDefectiveJoints()), 
                                                                  String.valueOf(getNumberOfDefectiveComponents()), 
                                                                  StringUtil.convertMilliSecondsToElapsedTime(_testExecutionTimer.getElapsedFullInspectionTimeInMillis(), false), 
                                                                  StringUtil.convertMilliSecondsToTimeStamp(_inspectionStopTime));
      }
      catch (DatastoreException ex)
      {
        DatastoreException datastoreException = (DatastoreException) ex;
        MessageInspectionEvent event = new VOneMachineStatusMonitoringMessageInspectionEvent(datastoreException);
        InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
        inspectionEventObservable.sendEventInfo(event);
      }
    }

    // LeeHerng 23 March 2010 - I commented out this portion of code because it will cause assert when running Exposed Pad Image Learning
    // CR1018 Memory leak fix by LeeHerng
    /**
     * AlgorithmShortsLearningReaderWriter algorithmShortsLearningReaderWriter =
     * _project.getAlgorithmShortsLearningReaderWriter(); if
     * (algorithmShortsLearningReaderWriter.isOpen()) {
     * algorithmShortsLearningReaderWriter.close();
     * algorithmShortsLearningReaderWriter.clearProjectReference(); }
     *
     * AlgorithmExpectedImageLearningReaderWriter
     * algorithmExpectedImageLearningReaderWriter =
     * _project.getAlgorithmExpectedImageLearningReaderWriter(); if
     * (algorithmExpectedImageLearningReaderWriter.isOpen()) {
     * algorithmExpectedImageLearningReaderWriter.close();
     * algorithmExpectedImageLearningReaderWriter.clearProjectReference(); }
     *
     * // CR1018 Memory Leak fix by LeeHerng
     * _project.clearAlgorithmLearningReference();
    *
     */
    // Bee Hoon,XCR1650 - Make sure result process is done before start new inspection run
    if (_config.getBooleanValue(SoftwareConfigEnum.ENABLE_RESULTS_PROCESSING))
    {
      while(ResultsProcessor.isResultProcessDone() == false)
      {
        try
        {
          Thread.sleep(200);
        }
        catch(InterruptedException ie)
        {
          // do nothing
        }
      }
    }

    // Memory leak fix - by Lee Herng 28 Nov 2012
    Statistics.clear();

    //Lim, Lay Ngor - XCR1743:Benchmark
    //Set _CLASSIFY_JOINT_TIME_STAMP to true to print the algorithm time stamp if needed
    if(_CLASSIFY_JOINT_TIME_STAMP)
      ImageAnalysis.printTimingStats();
    
    _project = null;
    _inspectionStateController.reset();
    
    //Khaw Chek Hau - XCR2524: Skip alignment happened when alignment failed at Fine Tuning for mix mag recipe
    testProgram.clearTestSubProgramFilters();
    
    // now check if there were any exceptions, if there were throw the first one
    if (xrayTesterException != null)
      throw xrayTesterException;

    // See if we caught an exception on any of our numerous worker threads.
    checkForWorkerThreadException();
  }

  /**
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  private XrayTesterException abortResultsWriting()
  {
    XrayTesterException xrayTesterException = null;
    if (_productionOrAlwaysGenerateAndProcessResultsForUnitTests)
    {
      String projName = _project.getName();
      // delete the binary results file
      String resultsFile = FileName.getResultsFileFullPath(projName, _inspectionStartTime);
      try
      {
        //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
        //Don't delete the result file if the manual realignment is performed
        if ((FileUtilAxi.exists(resultsFile) && _project.getTestProgram().isRealignPerformed() == false)
              || (FileUtilAxi.exists(resultsFile) && _project.getTestProgram().getProject().getPanel().getPanelSettings().isPanelBasedAlignment()))
          FileUtilAxi.delete(resultsFile);
      }
      catch (XrayTesterException ex)
      {
        // throw this towards the end of this method after other clean up work is done
        if (xrayTesterException == null)
          xrayTesterException = ex;
      }

      // complete the write of the xml results file
      try
      {
        _testExecutionTimer.startXmlWriterTimer();
        _resultsXmlWriter.close(_inspectionStopTime);
        if (_generateMeasurementsXML)
          _measurementsXmlWriter.close(_inspectionStopTime);
        _testExecutionTimer.stopXmlWriterTimer();
      }
      catch (XrayTesterException ex)
      {
        // throw this towards the end of this method after other clean up work is done
        if (xrayTesterException == null)
          xrayTesterException = ex;
      }

      // delete the xml results file
      String resultsXmlFile = FileName.getXMLResultsFileFullPath(projName, _inspectionStartTime);
      try
      {
        if (FileUtilAxi.exists(resultsXmlFile))
        {                      
          //Khaw Chek Hau - XCR2287: Indictment error when abort production
          File resultFile = new File(resultsXmlFile);           
          if (resultFile.delete() == false)
            resultFile.deleteOnExit();
        }
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }

      // delete the xml schema files
      String resultsXmlSchemaFile = FileName.getResultsXmlSchemaForMRTOnlyInResultsDirFullPath(projName, _inspectionStartTime);
      try
      {
        if (FileUtilAxi.exists(resultsXmlSchemaFile))
          FileUtilAxi.delete(resultsXmlSchemaFile);
      }
      catch (XrayTesterException ex)
      {
        // throw this towards the end of this method after other clean up work is done
        if (xrayTesterException == null)
          xrayTesterException = ex;
      }
      String cadXmlSchemaFile = FileName.getCadXmlSchemaInResultsDirFullPath(projName, _inspectionStartTime);
      try
      {
        if (FileUtilAxi.exists(cadXmlSchemaFile))
          FileUtilAxi.delete(cadXmlSchemaFile);
      }
      catch (XrayTesterException ex)
      {
        // throw this towards the end of this method after other clean up work is done
        if (xrayTesterException == null)
          xrayTesterException = ex;
      }
      String settingsXmlSchemaFile = FileName.getSettingsXmlSchemaInResultsDirFullPath(projName, _inspectionStartTime);
      try
      {
        if (FileUtilAxi.exists(settingsXmlSchemaFile))
          FileUtilAxi.delete(settingsXmlSchemaFile);
      }
      catch (XrayTesterException ex)
      {
        // throw this towards the end of this method after other clean up work is done
        if (xrayTesterException == null)
          xrayTesterException = ex;
      }

      // delete all repair images
      try
      {
        String repairImageExt = FileName.getRepairImageExtension();
        String inspectionResultsDir = Directory.getInspectionResultsDir(projName, _inspectionStartTime);
        for (String fileName : FileUtilAxi.listAllFilesInDirectory(inspectionResultsDir))
        {
          if (FileUtilAxi.exists(fileName) && fileName.endsWith(repairImageExt))
            FileUtilAxi.delete(fileName);
        }
      }
      catch (XrayTesterException ex)
      {
        // throw this towards the end of this method after other clean up work is done
        if (xrayTesterException == null)
          xrayTesterException = ex;
      }
    }
   // _haveFinishCloseResultFileInParallel.setValue(true);
    
    return xrayTesterException;
  }

  /**
   * @author Peter Esbensen
   * @author Kee Chin Seong - Sometimes the Image Acquisition is hung while
   * collecting the image, this could be due to test Program has been corrupted
   * or something. refresh the test program by minor reset.
   */
  private void startAcquiringImagesOnSeparateThread(TestProgram testProgram, final ImageSetData imageSetData)
  {
    Assert.expect(testProgram != null);
    Assert.expect(_imageAcquisitionEngine != null);

    final TestProgram finalTestProgram = testProgram;

    _imageAcquisitionWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          checkForWorkerThreadException();

          _acquiringImages.setValue(true);
          if (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS))
          {
            if (_production)
              _imageAcquisitionEngine.acquireProductionImages(finalTestProgram, _areAnyDiagnosticsOn, _testExecutionTimer);
            else
              _imageAcquisitionEngine.acquireImageSetImages(finalTestProgram, _areAnyDiagnosticsOn, _testExecutionTimer);
          }
          else if (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS))
          {
            _imageAcquisitionEngine.acquireOfflineProductionImages(finalTestProgram, _testExecutionTimer, imageSetData);
          }
          else if (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_WHEN_NECESSARY))
          {
            /** @todo work to be done to support this mode */
            handleWorkerThreadException(new HardwareDoesNotExistException());
          }
          else
            Assert.expect(false, "Unknown image acquistion mode: " + _imageAcquisitionModeEnum);
        }
        catch (Throwable th)
        {
          handleWorkerThreadException(th);
          _project.resetTestProgram();
        }
        finally
        {
          _acquiringImages.setValue(false);
        }
      }
    });
  }

//  /**
//   * @author Cheah Lee Herng
//   */
//  private void performSurfaceMap(TestProgram testProgram) throws XrayTesterException
//  {
//    Assert.expect(testProgram != null);
//
//    final TestProgram finalTestProgram = testProgram;
//
//    _acquiringOpticalImages.setValue(true);
//    _opticalImageAcquisitionWorkerThread.invokeLater(new Runnable()
//    {
//      public void run()
//      {
//        try
//        {
//          _opticalImageAcquisitionEngine.acquireProductionOpticalImages(finalTestProgram, false, _testExecutionTimer);
//        }
//        catch (Throwable th)
//        {
//          _abortOpticalImageAcquisitionFlag.setValue(true);
//          handleWorkerThreadException(th);
//        }
//        finally
//        {
//          if (_opticalImageAcquisitionEngine.isUserAborted())
//            _abortOpticalImageAcquisitionFlag.setValue(true);
//                
//          _acquiringOpticalImages.setValue(false);
//        }
//      }
//    });
//
//    try
//    {
//      _acquiringOpticalImages.waitUntilFalse();
//    }
//    catch(InterruptedException ex)
//    {
//      // Do nothing
//    }
//  }

  /**
   * @author Bill Darbie
   * @author Patrick Lacz
   */
  private int getNumImageAnalysisThreads()
  {
    if (_panelInspectionSettings.areAnyDiagnosticsOn() == true)
      return 1; // with diagnostics, we always only do 1 thread.

    int numberOfThreadsConfigSetting = _config.getIntValue(SoftwareConfigEnum.NUM_IMAGE_ANALYSIS_THREADS);
    if (_production == true)
    {
      return numberOfThreadsConfigSetting;
    }
    else
    {
      return Math.min(numberOfThreadsConfigSetting, 5);
    }
  }

  /**
   * Iterate through all the ReconstructionRegions in the testProgram and call
   * image analysis on them. If we are configured to use more than one thread,
   * use a ThreadPoolExecutor to manage that work.
   *
   * @author Peter Esbensen
   */
  private void analyzeAllImages(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    if (_DROP_INSPECTION_IMAGES == false)
    {
      try
      {
        _firstInspectionImage.set(false);
        int numThreads = getNumImageAnalysisThreads();

        int numberOfRegionsToClassify = testProgram.getFilteredInspectionRegionsFromFilterTestSubProgram().size();
        final AtomicInteger numberOfRegionsClassified = new AtomicInteger(0);

        analyzeAllImagesOnMultipleThreads(numThreads, numberOfRegionsToClassify, numberOfRegionsClassified, testProgram);

        if (_regionsToBeInspected.isEmpty() == false)
        {
          // just ignore the remaining regions, we're never going to get them!  (Because we must have been doing a partial inspection in offline mode)
          _regionsToBeInspected.clear();

          if (getAbortRequested() == false)
          {
            finishRemainingClassification();
            if (_subtypeStichImageForVVTS.isEmpty() == false)
            {
              saveComponentMagnificationInfo();
            }
          }
        }

        freeRemainingImages();
      }
      finally
      {
        // Always wait for IAE to finish acquiring images or abort
        try
        {
          _acquiringImages.waitUntilFalse();
        }
        catch (InterruptedException iex)
        {
          // Do nothing...
        }

        _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_CLASSIFY);

        _testExecutionTimer.stopJointInspectionTimer();
      }
    }
    else
    {
      try
      {
        // All we should ever get back is the alignment images.
        for (int i = 0; i < testProgram.getAlignmentRegions().size(); ++i)
        {
          BooleanRef noImagesAvailable = new BooleanRef(false);
          ReconstructedImages alignmentImages = _imageAcquisitionEngine.getReconstructedImages(noImagesAvailable);
          if (noImagesAvailable.getValue() == true)
          {
            break;
          }
          ReconstructionRegion alignmentRegion = alignmentImages.getReconstructionRegion();
          Assert.expect(alignmentRegion.getReconstructionRegionTypeEnum().equals(ReconstructionRegionTypeEnum.ALIGNMENT));

          //Khaw Chek Hau - XCR2285: 2D Alignment on v810
          if (testProgram.getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod())
            _alignment.alignOn2DImage(alignmentImages, true);
          else
            _alignment.alignOnImage(alignmentImages, true);
            
          alignmentImages.decrementReferenceCount();
        }

        _regionsToBeInspected.clear();

      }
      finally
      {
        // Always wait for IAE to finish acquiring images or abort
        try
        {
          _acquiringImages.waitUntilFalse();
        }
        catch (InterruptedException iex)
        {
          // Do nothing...
        }

        _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_CLASSIFY);

        _testExecutionTimer.stopJointInspectionTimer();
      }
    }
  }

  /**
   * @author Peter Esbenesen
   * @author Matthew Wharton
   * @author Luc Besson
   * @author Pedro Almodóvar
   * @author Patrick Lacz
   */
  private void analyzeAllImagesOnMultipleThreads(int numThreads, int numberOfRegionsToClassify, final AtomicInteger numberOfRegionsClassified, TestProgram testProgram)
    throws XrayTesterException
  {
    // use a  ThreadPoolExecutor to manage the other threads
    int coreNumberOfThreads = numThreads;
    int maxNumberOfThreads = numThreads; // this value isn't used while using an unbounded LinkedBlockingQueue, as below.
    int subProgramId = 0;
    int prevSubProgramId = -1;

    // This only applies during Test Exec & precision image set generation only, so that it does not hang for large board (e.g. 79K joints).
    // Must use back 8 threads max for other mode, so that diagnostic display is not affected.
    if (_production == true)
    {
      coreNumberOfThreads = 500;
      maxNumberOfThreads = 500; // this value isn't used while using an unbounded LinkedBlockingQueue, as below.
    }
    // kill the temporary threads if they arn't being used.
    long threadKeepAliveTime = 30;
    TimeUnit timeUnitForTimeToWait = TimeUnit.SECONDS;

    BlockingQueue<Runnable> blockingQueue = new LinkedBlockingQueue<Runnable>(); // unbounded queue

    ThreadFactory imageAnalysisThreadFactory = new NamedThreadFactory("image analysis threads");
    RejectedExecutionHandler rejectedExecutionHandler = new ThreadPoolExecutor.AbortPolicy(); // throws a RejectedExecutionException. Indicates an error.
    final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(coreNumberOfThreads,
      maxNumberOfThreads,
      threadKeepAliveTime,
      timeUnitForTimeToWait,
      blockingQueue,
      imageAnalysisThreadFactory,
      rejectedExecutionHandler);

    final BooleanRef abortInspectionFlag = new BooleanRef(false);

    LogFile logFile = null;
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.INSPECTION_ENGINE_DEBUG))
    {
      logFile = new LogFile("PrecisionImagesQueue.log");
    }
    int i = 1;
    while ((numberOfRegionsClassified.get() < numberOfRegionsToClassify) && abortInspectionFlag.getValue() == false)
    {
      // Check to see if any exceptions have come up from any of the other worker threads.
      checkForWorkerThreadException();

      //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
      if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.INSPECTION_ENGINE_DEBUG))
      {
        try
        {
          if (numberOfRegionsClassified.get() % 10 == 0)
          {
            logFile.logMessage(Long.toString(System.currentTimeMillis()) + "," + threadPoolExecutor.getQueue().size());
          }
        }
        catch (Exception e)
        {
          // Do nothing.
        }
      }

      // XCR 1168: Align on image when user gain is one.
      // Apply alignment result for sub program with user gain not equal to one.
      // before get reconstructed image
      if (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS) == false)
      {
        if(_isLowMag)
        {
          subProgramId = testProgram.getSubProgramIdByTotalNumberOfLowMagReconstructionRegion(i++);
        }
        else
        {
          subProgramId = testProgram.getSubProgramIdByTotalNumberOfHighMagReconstructionRegion(i++);
        }
        if (subProgramId != 0)
        {
          TestSubProgram subProgram = testProgram.getTestSubProgramById(subProgramId);
          if (subProgram.isSubProgramPerformAlignment() == false)
          {
            if (prevSubProgramId != subProgramId)
            {
              int id = subProgram.getSubProgramRefferenceIdForAlignment();
              _imageAcquisitionEngine.applyAlignmentResult(subProgram.getTestProgram().getTestSubProgramById(id).getAggregateRuntimeAlignmentTransform(), subProgram.getId());
              prevSubProgramId = subProgramId;
            }
          }
        }
      }

      final ReconstructedImages reconstructedImages = _imageAcquisitionEngine.getReconstructedImages(abortInspectionFlag);

      ImageAcquisitionProgressMonitor.getInstance().inspectionStarted();

      if (abortInspectionFlag.getValue() == false)
      {
        // Turn on thread control mechanism during Test Exec & precision image set generation only.
        if (_production == true)
        {
          if (threadPoolExecutor.getQueue().size() > coreNumberOfThreads)
          {
// Pausing & resume IRP processes here is too risky - Seng Yew, Lim on 29-Dec-2009
//            _imageAcquisitionEngine.getReconstructedImagesProducer().getReconstructedImagesManager().sendPauseToIrps();
//            ImageAcquisitionProgressMonitor.getInstance().reconstructionIsPaused();
            while (true)
            {
              try
              {
                Thread.sleep(250);
              }
              catch (Exception e)
              {
                // Do nothing.
              }
              if (abortInspectionFlag.getValue() == true)
              {
                break;
              }
              if (threadPoolExecutor.getQueue().size() < coreNumberOfThreads / 2)
              {
//                _imageAcquisitionEngine.getReconstructedImagesProducer().getReconstructedImagesManager().checkForIRPResumeCondition();
//                ImageAcquisitionProgressMonitor.getInstance().reconstructionIsRunning();
                break;
              }
            }
          }
        }
        threadPoolExecutor.execute(new Runnable()
        {

          public void run()
          {
            try
            {
              ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

              if (reconstructionRegion.isAlignmentRegion() == false)
              {
                _unfreedImages.add(reconstructionRegion);
              }

              if ((_regionsToBeInspected.containsKey(reconstructionRegion.getRegionId())) ||
                  (reconstructionRegion.isAlignmentRegion()))
              {
                handleReconstructedImages(reconstructedImages);

                if (reconstructionRegion.isAlignmentRegion() == false)
                {
                  numberOfRegionsClassified.incrementAndGet();
                }
              }
              else
              {
                Assert.expect(false, "got back unexpected image from IAE");
              }
            }
            catch (Throwable throwable)
            {
              handleWorkerThreadException(throwable);
            }
            finally
            {
              reconstructedImages.decrementReferenceCount();
            }
          }
        });
      }
    }
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.INSPECTION_ENGINE_DEBUG))
    {
      logFile.closeLogFile();
    }

    try
    {
      // Wait until all our image analysis threads are done or we get an abort.
      threadPoolExecutor.shutdown();
      while ((threadPoolExecutor.isTerminated() == false))
      {
        // Turn on this during Test Exec & precision image set generation only, to avoid any threads being dropped.
        if (_production == true)
        {
          threadPoolExecutor.awaitTermination(5, TimeUnit.SECONDS);
        }
        else
        {
          threadPoolExecutor.awaitTermination(300, TimeUnit.SECONDS);
        }
      }
    }
    catch (InterruptedException iex)
    {
      // Do nothing ...
    }
  }

  /**
   * If we are only doing partial test, it is possible for us to be holding onto
   * images for measurementGroup classification that never happened. This method
   * is used to clear those out.
   *
   * @author Peter Esbensen
   */
  private void freeRemainingImages() throws XrayTesterException
  {
    for (ReconstructionRegion reconstructionRegion : _unfreedImages)
    {
      //debug("freeing image for " + reconstructionRegion);
      _imageAcquisitionEngine.freeReconstructedImages(reconstructionRegion);
    }
    _unfreedImages.clear();
  }

  /**
   * This is the method that tells the hardware to start acquiring images and
   * inspects them. If this class is configured to use multiple image analysis
   * threads, that is managed here also.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  private void inspect(TestProgram testProgram, ImageSetData imageSetData) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(_padToInspectionRegionMap.isEmpty());
    Assert.expect(_panelInspectionSettings != null);
    Assert.expect(_inspectionEventObservable != null);
    Assert.expect((imageSetData != null) ||
                  (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS) == false));

    // If there are no regions to be inspected, don't bother with setting up the threads, etc!
    if (_regionsToBeInspected.isEmpty() == false)
    {
      _abortOpticalImageAcquisitionFlag.setValue(false);
      initializePadToInspectionRegionMap(testProgram);

      // Reset parameter
      _abortAlignmentOpticalImageAcquisitionFlag.setValue(false);
      _abortOpticalImageAcquisitionFlag.setValue(false);

      // Commented by Lee Herng, 29 Aug 2014
      // This is because PSP alignment is still un-stable.
      /**
       * if
       * (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS)
       * == false && _config.getBooleanValue(SoftwareConfigEnum.ENABLE_PSP) &&
       * testProgram.getAllInspectableAlignmentSurfaceMapTestSubPrograms().size()
       * > 0) { // Perform Surface Map for Alignment usage
       * performAlignmentSurfaceMap(testProgram); }
      * *
       */
      if (_abortOpticalImageAcquisitionFlag.isFalse() && _abortAlignmentOpticalImageAcquisitionFlag.isFalse())
      {
        startAcquiringImagesOnSeparateThread(testProgram, imageSetData);

        analyzeAllImages(testProgram);
      }
    }
  }

  /**
   * Update the TestExecutionEngine with an image to be inspected. Should only
   * be called by hardware observable and should only pass up images originally
   * requested by the TestExecutionEngine. This method takes the given image,
   * puts it on the _imageQueue, and then schedules an image analysis thread to
   * inspect it. This method also saves the image if image collection is
   * activated.
   *
   * @author Peter Esbensen
   * @author Patrick Lacz
   * @author Matt Wharton
   * @author khang-shian.sham
   * @author Chong Wei Chin
   */
  public void handleReconstructedImages(ReconstructedImages reconstructedImages) throws Throwable
  {
    Assert.expect(reconstructedImages != null);

    // If an exception was caught, just return
    if (_workerThreadException != null)
    {
      return;
    }

    boolean abortRequested = getAbortRequested();

    if (abortRequested == false)
    {
      ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
      if (reconstructedImages.getReconstructionRegion().isAlignmentRegion())
      {
        // Chin Seong, Kee, Manual Alignment fixed
        try
        {
          if(shouldProcessImage(reconstructionRegion))
          {
            waitForPreviousAlignmentExceptionUnlock();

            // XCR1631 by Cheah Lee Herng 31 July 2013
            // For board-based alignment, we have to reset _numberOfAlignmentRegionsProcessed
            // so that we can start from new for new board
            if (_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
            {
              if (reconstructionRegion.getBoard().equals(_previousBoard) == false)
              {
                _previousBoard = reconstructionRegion.getBoard();
                //reset _currentBoardIsXout only after we start for a new board
                _currentBoardIsXout = false;
                // Reset alignment region counter for new board
                _numberOfAlignmentRegionsProcessed.set(0);
              }
            }

            //Khaw Chek Hau - XCR2285: 2D Alignment on v810
			boolean isUsing2DAlignmentMethod = reconstructionRegion.getTestSubProgram().getTestProgram().getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod();
            
            if (_config.getBooleanValue(SoftwareConfigEnum.ENABLE_PSP) == false)
            {
              if (isUsing2DAlignmentMethod)
                _alignment.alignOn2DImage(reconstructedImages, true);
              else
                _alignment.alignOnImage(reconstructedImages, true);      
            }
            else
            {
              if (reconstructionRegion.getTestSubProgram().hasEnabledOpticalCameraRectangles())
              {
                if (isUsing2DAlignmentMethod)
                  _alignment.alignOn2DImage(reconstructedImages, false);
                else
                  _alignment.alignOnImage(reconstructedImages, false);
              }
              else
              {
                if (isUsing2DAlignmentMethod)
                  _alignment.alignOn2DImage(reconstructedImages, true);
                else
                  _alignment.alignOnImage(reconstructedImages, true);
              }
            }
          }
          else
          {
            // Tell the IAE that we're done with the image.
            if (_imageAcquisitionEngine.isAbortInProgress() == false)
            {
              _imageAcquisitionEngine.reconstructionRegionComplete(reconstructionRegion, false);
            }
            _imageAcquisitionEngine.freeReconstructedImages(reconstructionRegion);
          }
        }
        catch (AlignmentBusinessException ex)
        {
          // Chin Seong, Kee, Manual Alignment fixed - ONLY PRODUCTION need to go in here.
          if (_production && _precisionTuningImageAcquisition == false)
          {
            boolean isBoardBaseAlignment = reconstructionRegion.getTestSubProgram().hasBoard();

            if(isBoardBaseAlignment)
            {
              // XCR1631 by Cheah Lee Herng 06 Aug 2013
              // In high mag, once we have alignment failure, we will just straigt
              // throw the error
//              if (_isLowMag == false)
//              {
//                reconstructionRegion.getBoard().setBoardAlignmentExceptionCaught(true);
//                throw ex;
//              }
              // 26 Aug 2013 by Siew Yeng
              // If board is xout for low mag, apply alignment result once for high mag to fix irp exception
              if (_isLowMag == false && 
                reconstructionRegion.getBoard().isXedOut() &&
                _currentBoardIsXout == false)
              {
                _currentBoardIsXout = true;
                _imageAcquisitionEngine.applyAlignmentResult(reconstructionRegion.getManualAlignmentMatrix(), reconstructionRegion.getTestSubProgram().getId());
                continueWithAlignmentInput();
                return;
              }

              if (reconstructionRegion.getBoard().isXedOut())
              {
                continueWithAlignmentInput();
                return;
              }

              // auto x-out enable
              if (TestExecution.getInstance().isXoutDetectionModeEnabled())
              {
                if (TestExecution.getInstance().getXoutDetectionMethod().equals(XoutDetectionMethodEnum.ALIGNMENT_FAILURE))
                {
                  reconstructionRegion.getBoard().forceXout();
                  //Kee Chin Seong - Added in Xout reason for XML output
                  reconstructionRegion.getBoard().setXoutReason(StringLocalizer.keyToString("XOUT_DUE_TO_ALIGNMENT_FAILED_KEY"));
                  // send once only
                  _imageAcquisitionEngine.applyAlignmentResult(reconstructionRegion.getManualAlignmentMatrix(), reconstructionRegion.getTestSubProgram().getId());
                  continueWithAlignmentInput();
                  return;
                }
              }

              //first time will be false
              waitForPreviousAlignmentExceptionUnlock();

              // auto unload when alignment fail
              if (TestExecution.getInstance().shouldPanelBeEjectedIfAlignmentFails()
                || TestExecution.getInstance().isAlignmentFailAndUnloadPanel())
              {
                //XCR1624 - Wrong display of panel pins proccesed - Siew Yeng
                reconstructionRegion.getBoard().setBoardAlignmentExceptionCaught(true);
                // throw exception
                throw ex;
              }

              //XCR1706 - System x-out 2 times for same board - Siew Yeng
              if(_currentBoardIsXout == false)
              {
                if(reconstructionRegion.getBoard().isBoardAlignmentExceptionCaught() || _imageAcquisitionEngine.isAlignmentExceptionCaught())
                {
                  return;
                }

                // lock the exception
                _waitingForAlignmentExceptionActionUserInput.setValue(true);
                AlignmentBusinessException alignmentException = (AlignmentBusinessException) ex;
                // send up a message telling the GUI that we had an alignment failure
                MessageInspectionEvent event = new AlignmentFailedWaitForUserInputEvent(alignmentException);
                _inspectionEventObservable.sendEventInfo(event);

                _imageAcquisitionEngine.setAlignmentExceptionCaught(true);

                //Khaw Chek Hau - XCR2662: Production inspection time only display for misalignment board
                TestExecution.getInstance().stopFullInspectionTimer();

                // wait for user input
                waitForUserInput();

                if (_currentBoardIsXout)
                {
                  reconstructionRegion.getBoard().forceXout();
                  //Kee Chin Seong - Added in Xout reason for XML output
                  reconstructionRegion.getBoard().setXoutReason(StringLocalizer.keyToString("XOUT_DUE_TO_ALIGNMENT_FAILED_KEY"));
                  // send once only
                  _imageAcquisitionEngine.applyAlignmentResult(reconstructionRegion.getManualAlignmentMatrix(), reconstructionRegion.getTestSubProgram().getId());
                  _imageAcquisitionEngine.setAlignmentExceptionCaught(false);
                  //_currentBoardIsXout = false;
                  return;
                }
                else if (reconstructionRegion.getBoard().isXedOut())
                {
                  continueWithAlignmentInput();
                  _imageAcquisitionEngine.setAlignmentExceptionCaught(false);
                  return;
                }
                else if (TestExecution.getInstance().isAlignmentFailAndUnloadPanel())
                {
                  //XCR1624 - Wrong display of panel pins proccesed - Siew Yeng
                  reconstructionRegion.getBoard().setBoardAlignmentExceptionCaught(true);
                  //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
                  for (TestSubProgram subProgram: reconstructionRegion.getTestSubProgram().getTestProgram().getAllTestSubPrograms())
                  {
                    subProgram.setInspectionDone(false);
                  }
                  throw ex;
                }
                else if(TestExecution.getInstance().isAlignmentFailAndRerunAlignment())
                {
                  //Siew Yeng -XCR1757 - runtime manual alignment
                  //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
                  reconstructionRegion.getTestSubProgram().getTestProgram().setRealignPerformed(true);
                  reconstructionRegion.getTestSubProgram().setInspectionDone(false);
                  reconstructionRegion.getBoard().setBoardAlignmentExceptionCaught(true);
                  throw ex;
                }
                _imageAcquisitionEngine.setAlignmentExceptionCaught(false);
              }
              else
                return;
            }
            else
            {
              //XCR1624 - Wrong display of panel pins proccesed - Siew Yeng
              _imageAcquisitionEngine.setAlignmentExceptionCaught(true);
              throw ex;
            }
          }
          // throw exception
          throw ex;
        }
      }
      else
      {
        // An inspection image.

        // start the timer when the first image gets in.
        if (_firstInspectionImage.getAndSet(true) == false)
        {
          _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_CLASSIFY);
          _testExecutionTimer.startJointInspectionTimer();
        }
        // we might be collecting the images
        if (_captureImagesForLastTest && _production == false)
        {
          if (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS))
          {
            //add project name parameter in saveInspectionImages function call
            _imageManager.saveInspectionImages(_onlineImageSetData, reconstructedImages, _project.getName());
          }
        }

        // Khang-Wah, Chnee 14-Jun-2010 - Investigation on X-out implementation
        // we need to stop here if the x-out threshold is reached.
        // perform the inspection
        if (reconstructionRegion.getBoard().isXedOut() == false)
        {
          inspect(reconstructedImages);
        }
        else
        {
          for (JointInspectionData data : reconstructionRegion.getJointInspectionDataList())
          {
            if (data != null)
            {
              freeImageMemoryForJointInspectionDataIfPossible(data, true);
            }
          }

          if ((_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS))
            || (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_WHEN_NECESSARY)))
          {
            // tell the IAE that we are done with the image
            // the following FALSE parameter is bogus, Dave says they don't need to
            // know if the image has failures or not, but they might want it in the future,
            // in which case we'd have to fix this
            if (_imageAcquisitionEngine.isAbortInProgress() == false)
            {
              _imageAcquisitionEngine.reconstructionRegionComplete(reconstructionRegion, false);
            }
          }

          // Swee Yee Wong - This line is added to make sure PSH will also be triggered when alignment failed and x-out is enabled
          // This can prevent IRP keep waiting for PSH images and cause IRP hung in the end
          if (Config.isRealTimePshEnabled() && reconstructionRegion.getTestSubProgram().isSufficientMemoryForRealTimePsh())
          {
            if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) == false
              || Config.getInstance().getBooleanValue(SoftwareConfigEnum.OFFLINE_RECONSTRUCTION))
            {
              reconstructionRegion.triggerConsumerRegion();
            }
          }

          // We're finished inspecting this reconstruction region.
          _imageAcquisitionEngine.getReconstructedImagesProducer().getReconstructedImagesManager().markRegionAsFinishedInspection(reconstructedImages.getReconstructionRegion());

          // this is a synchronized collection
          int regionId = reconstructionRegion.getRegionId();
          ReconstructionRegion removedRegion = null;
          boolean finishedAllRegions = false;
          synchronized (_regionsToBeInspected)
          {
            removedRegion = _regionsToBeInspected.remove(regionId);
            finishedAllRegions = _regionsToBeInspected.isEmpty();
          }

          if(finishedAllRegions)
          {
            synchronized (_unfreedImages)
            {
              if (_unfreedImages.contains(reconstructedImages.getReconstructionRegion()))
              {
                //debug("freeing image for " + reconstructedImages.getReconstructionRegion());

                _imageAcquisitionEngine.freeReconstructedImages(reconstructedImages.getReconstructionRegion());
                _unfreedImages.remove(reconstructedImages.getReconstructionRegion());
              }
            }
          }
        }
      }
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  private boolean shouldProcessImage(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);

    boolean shouldProcessImage = false;
    TestProgram testProgram = reconstructionRegion.getTestSubProgram().getTestProgram();

    if (testProgram.hasVariableMagnification())
    {
      shouldProcessImage = true;
    }
    else if (_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false &&
             reconstructionRegion.getBoard().isXedOut())
      shouldProcessImage = false;
    else
      shouldProcessImage = true;
    
    return shouldProcessImage;
  }

  /**
   * @author Chin Seong, Kee, Manual Alignment fixed
   */
  private void waitForUserInput()
  {
    _waitingForUserInput.setValue(true);
    //Siew Yeng - XCR-2753
    ImageAcquisitionProgressMonitor.getInstance().alignmentFailedWaitingUserInput();
    try
    {
      _waitingForUserInput.waitUntilFalse();
      _waitingForAlignmentExceptionActionUserInput.setValue(false); //set this event lock to false so that next event can go in
      ImageAcquisitionProgressMonitor.getInstance().alignmentFailedUserInputDone();
    }
    catch (InterruptedException ex)
    {
      //Do nothing ...
    }
  }

  /**
   * Returns the ReconstructedImages for the specified inspection region and
   * image type.
   *
   * @author Matt Wharton
   */
  public ReconstructedImages getImagesForRegion(ReconstructionRegion inspectionRegion,
                                                ImageTypeEnum imageTypeEnum) throws DatastoreException
  {
    Assert.expect(inspectionRegion != null);
    Assert.expect(imageTypeEnum != null);

    return _reconstructedImagesCachingManager.getReconstructedImages(inspectionRegion, imageTypeEnum);
  }

  /**
   * Acquires lightest images for the specified reconstruction region. Blocks
   * until the images are available. Returns immediately if an abort occurs.
   *
   * @author Matt Wharton
   */
  public ReconstructedImages getLightestImagesForRegion(ReconstructionRegion inspectionRegion,
                                                        BooleanRef lightestImagesAvailable) throws XrayTesterException
  {
    Assert.expect(inspectionRegion != null);
    Assert.expect(lightestImagesAvailable != null);

    ReconstructedImages lightestImages = null;
    if (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS))
    {
      // Request re-reconstruction of 'lightest' images.
      lightestImages = _imageAcquisitionEngine.reacquireLightestImagesAndWait(inspectionRegion, lightestImagesAvailable);
    }
    else if (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS))
    {
      // Load the 'lightest' images from disk (if available).
      Assert.expect(_imageSetDataUnderTest != null);
      lightestImages = _imageAcquisitionEngine.acquireOfflineLightestImages(_imageSetDataUnderTest,
                                                                            inspectionRegion,
                                                                            lightestImagesAvailable);
    }
    else
    {
      // Lightest images are not supported in other modes.  Indicate that the image is not available.
      lightestImages = null;
      lightestImagesAvailable.setValue(false);
    }

    // Make sure the lightest images have any necessary rotation correction applied to them.
    if (lightestImages != null)
    {
      lightestImages.ensureInspectedSlicesAreReadyForInspection();
    }

    return lightestImages;
  }

  /**
   * Acquires re-reconstructed images for the specified reconstruction region at
   * the specified offset. Blocks until the images are available. Returns
   * immediately if an abort occurs.
   *
   * @author Matt Wharton
   */
  public ReconstructedImages getReReconstructedImagesForRegionAtOffset(ReconstructionRegion inspectionRegion,
                                                                       int offsetInNanometers,
                                                                       BooleanRef reReconstructedImagesAvailable) throws XrayTesterException
  {
    Assert.expect(inspectionRegion != null);
    Assert.expect(reReconstructedImagesAvailable != null);

    ReconstructedImages reReconstructedImages = null;

    if (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS))
    {
      // Request re-reconstruction of the images at the specified offset.
      reReconstructedImages = _imageAcquisitionEngine.reacquireImagesAndWait(inspectionRegion,
                                                                             offsetInNanometers,
                                                                             reReconstructedImagesAvailable);
    }
    else if (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS))
    {
      // Load the re-reconstructed images from disk (if available).
      Assert.expect(_imageSetDataUnderTest != null);
      reReconstructedImages = _imageAcquisitionEngine.acquireOfflineReReconstructedImages(_imageSetDataUnderTest,
                                                                                          inspectionRegion,
                                                                                          offsetInNanometers,
                                                                                          reReconstructedImagesAvailable);
    }
    else
    {
      // Re-reconstructed images are not supported in other modes.  Indicate that the image is not available.
      reReconstructedImages = null;
      reReconstructedImagesAvailable.setValue(false);
    }

    // Make sure the re-reconstructed images have any necessary rotation correction applied to them.
    if (reReconstructedImages != null)
    {
      reReconstructedImages.ensureInspectedSlicesAreReadyForInspection();
    }

    return reReconstructedImages;
  }

  /**
   * If we've called classifyJoint on all the joints in the MeasurementGroup, go
   * ahead and call classifyMeasurementGroup if the InspectionFamily for that
   * MeasurementGroup requires it.
   *
   * @author Peter Esbensen
   */
  private void classifyMeasurementGroupIfNecessary(JointInspectionData jointInspectionData,
    ReconstructedImages reconstructedImages,
    boolean processingPartialTest) throws XrayTesterException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionData != null);

    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    Subtype subtype = jointInspectionData.getSubtype();

    Map<Subtype, MeasurementGroup> subtypeToMeasurementGroupMap =
        getSubtypeToMeasurementGroupMapForComponent(jointInspectionData.getComponentInspectionData());

    MeasurementGroup measurementGroup = subtypeToMeasurementGroupMap.get(subtype);

    // this problem can occur if the TestProgram is messed up.  For example, if a joint appears in multiple regions.
    Assert.expect(measurementGroup != null, "Could not find measurement group.");

    //Kee Chin Seong - XCR-2387 - Make sure the is only production
    //Lim, Lay Ngor move place to preserve the getJointInspectionResult value
    //Lim, Lay Ngor - XCR-3454 - able to save diagnostic Image when offline result generation set to true.
    if(ImageProcessingAlgorithm.needToSaveVoidDiagnosticImage(subtype) && _productionOrAlwaysGenerateAndProcessResultsForUnitTests)
      addDiagnosticSliceToJoint(jointInspectionData.getJointInspectionResult(), reconstructedImages); 
    
    //Siew Yeng - XCR-3141 - move place to preserve getJointInspectionResult value
    //Siew Yeng - XCR-2683 - add enhanced image
    //Lim, Lay Ngor - XCR-3454 - able to save enhance Image when offline result generation set to true.
    if(ImageProcessingAlgorithm.needToSaveEnhancedImage(subtype) && _productionOrAlwaysGenerateAndProcessResultsForUnitTests)
    {
      addEnhancedImageSliceToJoint(jointInspectionData.getJointInspectionResult(), reconstructedImages);
    }
    
    // if this is not classified at the component level, then go ahead and free the image, we won't be needing it anymore
    // UNLESS it is the image we would use for a component level call . . . keep that around so that we can use it as the component-level image
    //Siew Yeng - XCR-3094
    if (shouldClassifyAtComponentOrMeasurementGroupLevel(jointInspectionData) == false && 
        jointInspectionData.getPad().isInspected())
    {
      if (jointInspectionData != getJointToUseForRepairImageOfComponentLevelCall(jointInspectionData.getComponentInspectionData()))
      {
        finalizeInspectionOfJoint(jointInspectionData);
        freeImageMemoryForJointInspectionDataIfPossible(jointInspectionData, processingPartialTest);
      }
    }
    
    // if this joint is the last one in a measurementGroup, go ahead and classify the measurementGroup
    boolean finishedWithInspectingJointsForThisMeasurementGroup = false;
    // Encountered cases when a BGA component was splited to 2 IRP for processing, and crash might happens.
    // Crash does not happen everytime but once a while like once in a week or once per day.
    // Suspect this is the root cause when this jointInspectionResult addition process is not synchronized.
    synchronized(measurementGroup)
    {
      finishedWithInspectingJointsForThisMeasurementGroup = measurementGroup.addJointInspectionResult(jointInspectionResult);
    }
    if (finishedWithInspectingJointsForThisMeasurementGroup)
    {
      // classify the measurement group (the joints with the same subtype under a component)
      Assert.expect(measurementGroup != null);

      if (subtype.getInspectionFamily().classifiesAtComponentOrMeasurementGroupLevel())
      {
        //Siew Yeng - XCR-3210 - temporary fix for BGA 1x1
        if(subtype.getInspectionFamilyEnum() == InspectionFamilyEnum.LARGE_PAD ||
          (subtype.getInspectionFamilyEnum() == InspectionFamilyEnum.GRID_ARRAY && subtype.getPanel().getProject().isBGAInspectionRegionSetTo1X1()))
        {
          //XCR-2168 - Component Level Voiding for mixed subtypes
          //Siew Yeng - currently skip this for single pad and LGA only
          //          - will need to skip BGA as well in future.
        }
        else
          classifyAndProcessResults(Arrays.asList(measurementGroup), _padToInspectionRegionMap);
      }

      //Siew Yeng - XCR-3210 - temporary fix for BGA 1x1(need to remove this checking after implement mixed subtypes for BGA 1x1 component level)
      //          - skip this for BGA 1x1
      if((subtype.getInspectionFamilyEnum() == InspectionFamilyEnum.GRID_ARRAY && subtype.getPanel().getProject().isBGAInspectionRegionSetTo1X1()) == false)
      {
        // now finalize the joints within this group
        for (JointInspectionData groupJointInspectionDataObject : measurementGroup.getInspectableJointInspectionDataObjects())
        {
          if (groupJointInspectionDataObject.jointInspectionResultExists())
          {
            finalizeInspectionOfJoint(groupJointInspectionDataObject);
          }
        }
      }

      // if this was the last measurementGroup on this component, finalize the component stuff
      Assert.expect(subtypeToMeasurementGroupMap != null);
      Assert.expect(_componentToSubtypeToMeasurementGroupMap != null);

      // synchronize here because we don't want several threads registering as isEmpty()
      synchronized (subtypeToMeasurementGroupMap)
      {
        //Siew Yeng - XCR-2168 - classify measurementGroups for component level
        if(subtype.getInspectionFamilyEnum() == InspectionFamilyEnum.LARGE_PAD)
        {
          //Siew Yeng - XCR-2641 -  intermittent crash when run production due to sunchronize problem
          _componentToMeasurementGroupsMap.get(jointInspectionData.getComponentInspectionData().getComponent()).add(measurementGroup);
          if( _componentToMeasurementGroupsMap.get(jointInspectionData.getComponent()).size() == 
             jointInspectionData.getComponent().getComponentType().getSubtypes(subtype.getInspectionFamilyEnum()).size())
          {
            classifyAndProcessResults(_componentToMeasurementGroupsMap.get(jointInspectionData.getComponent()), _padToInspectionRegionMap);
            _componentToMeasurementGroupsMap.remove(jointInspectionData.getComponent());
          }
        }
        else if(subtype.getInspectionFamilyEnum() == InspectionFamilyEnum.GRID_ARRAY && subtype.getPanel().getProject().isBGAInspectionRegionSetTo1X1())
        {
          //Siew Yeng - XCR-3210 - temporary fix for BGA 1x1
          // need to remove this else if condition after implement mixed subtypes for BGA 1x1 component level
          _componentToMeasurementGroupsMap.get(jointInspectionData.getComponentInspectionData().getComponent()).add(measurementGroup);
          if( _componentToMeasurementGroupsMap.get(jointInspectionData.getComponent()).size() == 
             jointInspectionData.getComponent().getComponentType().getSubtypes(subtype.getInspectionFamilyEnum()).size())
          {
            List<MeasurementGroup> measurementGroups = _componentToMeasurementGroupsMap.get(jointInspectionData.getComponentInspectionData().getComponent());
            for(MeasurementGroup mg : measurementGroups)
            {
              classifyAndProcessResults(Arrays.asList(mg), _padToInspectionRegionMap);
            }
            
            for(MeasurementGroup mg : measurementGroups)
            {
              // now finalize the joints within this group
              for (JointInspectionData groupJointInspectionDataObject : mg.getInspectableJointInspectionDataObjects())
              {
                if (groupJointInspectionDataObject.jointInspectionResultExists())
                {
                  finalizeInspectionOfJoint(groupJointInspectionDataObject);
                }
              }
            }
            measurementGroups.clear();
            _componentToMeasurementGroupsMap.remove(jointInspectionData.getComponent());
          }
        }
        subtypeToMeasurementGroupMap.remove(subtype);
        boolean finishedInspectingAllSubtypesForThisComponent = subtypeToMeasurementGroupMap.isEmpty();
        if (finishedInspectingAllSubtypesForThisComponent)
        {
          finalizeInspectionOfComponent(jointInspectionData.getComponentInspectionData());
          boolean alreadyFreedImages = true;
          freeImageMemoryForComponentIfPossible(measurementGroup.getComponentInspectionData(), alreadyFreedImages);
          // remove the measurement group from our 'to-be-processed' list.
          _componentToSubtypeToMeasurementGroupMap.remove(measurementGroup.getComponentInspectionData().getComponent());
        }
      }
    }
  }

  /**
   * @author Peter Esbensen
   */
  private void finalizeInspectionOfComponent(ComponentInspectionData componentInspectionData) throws DatastoreException
  {
    Assert.expect(componentInspectionData != null);

    ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();
    // write out the binary results
    _testExecutionTimer.startInspectionResultsWriterTimer();
    if(componentInspectionData.getComponent().isComponentUseSingleMagnification()|| _isLowMag==false)
      _inspectionResultsWriter.writeComponent(componentInspectionResult);
    _testExecutionTimer.stopInspectionResultsWriterTimer();

    // write out the xml formatted results
    if (_productionOrAlwaysGenerateAndProcessResultsForUnitTests)
    {
      _testExecutionTimer.startXmlWriterTimer();
      
      //Siew Yeng - XCR-3094
      JointInspectionData jointInspectionData = getJointToUseForRepairImageOfComponentLevelCall(componentInspectionData);
      
      Set<JointInspectionData> jointInspectionDataList = new HashSet<JointInspectionData>();
      if(AlgorithmUtil.isAbleToStitchComponentImageAtVVTS(jointInspectionData.getSubtype()))
      {
        //Save ALL joint image for component level failure if stitch component image at VVTS is enable.
        jointInspectionDataList.addAll(componentInspectionData.getInspectableJointInspectionDataSet());
        if (_subtypeStichImageForVVTS.contains(jointInspectionData.getSubtype()) == false)
        {
          _subtypeStichImageForVVTS.add(jointInspectionData.getSubtype());
        }
      }
      else
      {
        //Save only ONE image for component level failure if stitch component image at VVTS is not enable.
        jointInspectionDataList.add(jointInspectionData);
      }
      
      // Siew Yeng - XCR-2141 - save all region image when fail component
      List<ReconstructionRegion> reconstructionRegionList = new ArrayList();
      for(JointInspectionData jointForRepairImage : jointInspectionDataList)
      {
        // If we don't have an image available, we must be in some abort mode.
        // In this case, no image will be saved (obviously) and no results will be written for the given component.
        ReconstructionRegion reconstructionRegion = jointForRepairImage.getInspectionRegion();
        //Siew Yeng - comment out this condition as it is no longer use
//        if (_reconstructedImagesCachingManager.hasReconstructedImages(reconstructionRegion, ImageTypeEnum.AVERAGE) || _mixMagnificationSavedReconstructedRegionIdList.contains(reconstructionRegion.getRegionId()))
//        {
//          if(_mixMagnificationSavedReconstructedRegionIdList.contains(reconstructionRegion.getRegionId())==false)
//            _mixMagnificationSavedReconstructedRegionIdList.add(reconstructionRegion.getRegionId());
//        }
//        
        //Siew Yeng - XCR-3241
        if(reconstructionRegionList.contains(reconstructionRegion) == false)
            reconstructionRegionList.add(reconstructionRegion);
      }
      
      synchronized (_resultsXmlWriter)
      {
        _testExecutionTimer.startXmlWriterTimer();
        if(componentInspectionData.getComponent().isComponentUseSingleMagnification()|| _isLowMag==false)
        {
          _resultsXmlWriter.writeComponentInspectionResult(componentInspectionResult, reconstructionRegionList);
        }
        if (_generateMeasurementsXML)
        {
          if(componentInspectionData.getComponent().isComponentUseSingleMagnification()|| _isLowMag==false)
          {
            _measurementsXmlWriter.writeComponentInspectionResult(componentInspectionResult);
          }
        }
        _testExecutionTimer.stopXmlWriterTimer();
        if (_FLAG_CALL_RATE_SPIKES)
        {
          if(componentInspectionData.getComponent().isComponentUseSingleMagnification()|| _isLowMag==false)
            writeFailedComponentData(componentInspectionResult);
        }
      }

      synchronized (_reconstructedImagesCachingManager)
      {
        ReconstructedImages reconstructedImages = null;

        /*** Following block should only become necessary if we someday create
         *   an inspectable component which contains no joints! Currently, all the
         *   precision tuning images are written by finalizeInspectionOfJoint()
        if (_precisionTuningImageAcquisition &&
            _precisionTuningReconstructionRegions.contains(reconstructionRegion) == false)
        {
          reconstructedImages = _reconstructedImagesCachingManager.getReconstructedImages(reconstructionRegion, ImageTypeEnum.AVERAGE);
          savePrecisionTuningImages(reconstructedImages);
          _precisionTuningReconstructionRegions.add(reconstructionRegion);
        }
        ***/

        if(componentInspectionData.getComponent().isComponentUseSingleMagnification()|| (componentInspectionData.getComponent().isComponentUseSingleMagnification()==false && _isLowMag==true))
        {
          if (componentInspectionResult.passed() == false)
          {
            // Siew Yeng - XCR-2141 - save all region image when fail component
            for(ReconstructionRegion reconstructionRegion : reconstructionRegionList)
            {
              // write repair images only for failing components
              if (_reconstructedImagesCachingManager.areImagesForRegionCachedAsRepairImages(reconstructionRegion, ImageTypeEnum.AVERAGE) == true)
              {
                // images are already on disk - don't read them back!
                Map<SliceNameEnum, String> sliceNameToImagePathMap = _reconstructedImagesCachingManager.getSliceNameToCachedImagePathMap(reconstructionRegion, ImageTypeEnum.AVERAGE);
                writeRepairImagesAsCopyFromCachedImages(reconstructionRegion, sliceNameToImagePathMap);
              }
              else
              {
                // write the images to disk from memory
                if (reconstructedImages == null)
                  reconstructedImages = _reconstructedImagesCachingManager.getReconstructedImages(reconstructionRegion, ImageTypeEnum.AVERAGE);

                // write repair images only for failing joints
                writeRepairImages(reconstructedImages);
              }

              if (_productionTuneEnabled)
              {
                saveInspectionImageIfNecessary(reconstructedImages, reconstructionRegion, componentInspectionResult.passed());
              }
              if (reconstructedImages != null)
              {
                reconstructedImages.decrementReferenceCount();
                reconstructedImages = null;
              }
            }
          }
        }
        if (reconstructedImages != null)
        {
          reconstructedImages.decrementReferenceCount();
          reconstructedImages = null;
        }
      } 
    }

    // Increment the number of failing components if the component failed
    if (componentInspectionResult.passed() == false)
    {
      //incrementNumberOfDefectiveComponents();
      // Chin Seong, Kee 3-Sept-2010 - X-out implementation
      if(componentInspectionData.getComponent().isComponentUseSingleMagnification()|| _isLowMag==false)
        componentInspectionData.getComponent().getBoard().addFailedComponent();//getBoard().addFailedJoint();
    }
    if(componentInspectionData.getComponent().isComponentUseSingleMagnification()|| _isLowMag==false)
      componentInspectionData.clearComponentInspectionResult();
    
  }

  /**
   * @author George A. David
   * @author khang-shian.sham
   */
  private void saveProductionTuneDefectImages(ReconstructedImages reconstructedImages) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);

    //add project name parameter in saveInspectionImages function call
    _imageManager.saveInspectionImages(_onlineImageSetData, reconstructedImages, _project.getName());
    int numImages = _onlineImageSetData.getNumberOfImagesSaved() + reconstructedImages.getNumberOfSlices();
    _onlineImageSetData.setNumberOfImagesSaved(numImages);
    _productionTuneDefectiveReconstructionRegions.add(reconstructedImages.getReconstructionRegion());
  }

  /**
   * @author George A. David
   * @author John Heumann
   * @author khang-shian.sham
   */
  private void savePrecisionTuningImages(ReconstructedImages reconstructedImages) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);

    //add project name parameter in saveInspectionImages function call
    _imageManager.saveInspectionImages(_onlineImageSetData, reconstructedImages, _project.getName());
    int numImages = _onlineImageSetData.getNumberOfImagesSaved() + reconstructedImages.getNumberOfSlices();
    _onlineImageSetData.setNumberOfImagesSaved(numImages);
  }

  /**
   * @author Peter Esbensen
   */
  private void freeImageMemoryForMeasurementGroupsIfPossible(Collection<MeasurementGroup> measurementGroups,
                                                             boolean possiblyProcessingPartialTest) throws XrayTesterException
  {
    Assert.expect(measurementGroups != null);

    for (MeasurementGroup measurementGroup : measurementGroups)
    {
      freeImageMemoryForComponentIfPossible(measurementGroup.getComponentInspectionData(),
                                            possiblyProcessingPartialTest);
    }
  }

  /**
   * @author Peter Esbensen
   */
  private void freeImageMemoryForComponentIfPossible(ComponentInspectionData componentInspectionData,
                                                     boolean possiblyProcessingPartialTest) throws XrayTesterException
  {
    Assert.expect(componentInspectionData != null);
    // if this does measurementGroup stuff then ALL the images need to be freed
    // see classifyMeasurementGroupIfNecessary for the rest of this wacky system for keeping track of when we can free images
    for (JointInspectionData jointInspectionData : componentInspectionData.getInspectableJointInspectionDataSet())
    {
      if (jointInspectionData.getSubtype().getInspectionFamily().classifiesAtComponentOrMeasurementGroupLevel())
      {
        freeImageMemoryForJointInspectionDataIfPossible(jointInspectionData, possiblyProcessingPartialTest);
      }
    }
  }

  /**
   * When a component-level call is made at repair, we need some image to show
   * for it. Since we split large components among several images, we don't have
   * an image that is guaranteed to show the entire component.
   *
   * For now, we will just use the image for pad one if it is available. if that
   * image is not available (because it is not inspected), then we just pick
   * another joint at random.
   *
   * In the future we may want to stitch together all the images of a component
   * or do something else that is more fancy.
   *
   * @author Peter Esbensen
   */
  private JointInspectionData getJointToUseForRepairImageOfComponentLevelCall(ComponentInspectionData componentInspectionData)
  {
    Assert.expect(componentInspectionData != null);

    // if the image for pad one is ok, just use that
    if (componentInspectionData.getComponent().getPadOne().isInspected())
    {
      JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
      return padOneJointInspectionData;
    }

    // ok then, we can't use the image for pad one, so just take the first joint we find that is inspected
    JointInspectionData jointToUse = null;
    for (JointInspectionData jointInspectionData : componentInspectionData.getInspectableJointInspectionDataSet())
    {
      if (jointInspectionData.getPad().isInspected())
      {
        jointToUse = jointInspectionData;
        break;
      }
    }
    Assert.expect(jointToUse != null);  // what!?! We shouldn't be trying to process components that have no inspectable joints!
    return jointToUse;
  }

  /**
   * The boolean parameter okForPadToBeAlreadyFreed may be set to true in case
   * we are finalizing a component when the user as only asked for a partial
   * inspection of that component. In that case, we may try to free the joint
   * that was never processed. Normally, we will assert in that case but we
   * ignore the error if okForPadToBeAlreadyFreed = true.
   *
   * @author Peter Esbensen
   */
  private void freeImageMemoryForJointInspectionDataIfPossible(JointInspectionData jointInspectionData,
    boolean okForPadToBeAlreadyFreed) throws XrayTesterException
  {
    Assert.expect(jointInspectionData != null);
    // remove the mapping to this image so that the image can be garbage collected if possible
    // NOTE: This current implementation does NOT support the notion of having joints from multiple subtypes
    //       in a single image.  If we start allowing this, the code below will need to be revised.  -MDW and PKE
    //debug("removing " + jointInspectionData.getFullyQualifiedPadName());
    ReconstructionRegion removedRegion = _padToInspectionRegionMap.remove(jointInspectionData.getPad());
    if (removedRegion == null)
    {
      if (okForPadToBeAlreadyFreed)
        return;
      else if (_abortRequested)
        return;
      else
        Assert.expect(false, "trying to free a joint that was already freed");
    }

    if (_padToInspectionRegionMap.containsValue(removedRegion) == false)
    {
      synchronized (_unfreedImages)
      {
        if (_unfreedImages.contains(removedRegion))
        {
          //debug("freeing image for " + removedRegion);

          _imageAcquisitionEngine.freeReconstructedImages(removedRegion);
          _unfreedImages.remove(removedRegion);
        }
      }
    }
  }

  /**
   * @author Patrick Lacz
   * @author Matt Wharton
   */
  private Map<Subtype, MeasurementGroup> getSubtypeToMeasurementGroupMapForComponent(
    ComponentInspectionData componentInspectionData)
  {
    Assert.expect(componentInspectionData != null);
    Map<Subtype, MeasurementGroup> subtypeToMeasurementGroupMap = null;
    synchronized (componentInspectionData)
    {
      boolean componentInMap = _componentToSubtypeToMeasurementGroupMap.containsKey(componentInspectionData.getComponent());
      if (componentInMap == false)
        addComponentDataToMaps(componentInspectionData);
      subtypeToMeasurementGroupMap = _componentToSubtypeToMeasurementGroupMap.get(componentInspectionData.getComponent());
    }
    Assert.expect(subtypeToMeasurementGroupMap != null);
    return subtypeToMeasurementGroupMap;
  }

  /**
   * Pull an image off of the image queue and inspect it. This involves
   * extracting measurements, grouping them, and then classifying the pads one
   * group at a time.
   *
   * @author Peter Esbensen
   */
  private void inspect(ReconstructedImages reconstructedImages) throws Throwable
  {
    Assert.expect(reconstructedImages != null);
    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    Assert.expect(reconstructionRegion.isAlignmentRegion() == false);

    Assert.expect(reconstructedImages.getNumberOfSlices() > 0);

    // check if we are skipping this region due to an abort
    if (getAbortRequested())
      return;

    reconstructedImages.ensureInspectedSlicesAreReadyForInspection();

    ImageAnalysis imageAnalysis = new ImageAnalysis();
    imageAnalysis.setDoSurfaceModeling(_doSurfaceModeling);

    Collection<Subtype> subtypesInRegion = reconstructionRegion.getSubtypes();
    Subtype firstSubtypeInRegion = subtypesInRegion.iterator().next();
    SliceNameEnum padSliceNameEnum = firstSubtypeInRegion.getInspectionFamily().getDefaultPadSliceNameEnum(firstSubtypeInRegion);

    InspectionEvent inspectingImageEvent = null;

    if (!_precisionTuningImageAcquisition)  // Display progress bar instead of images during acquisition
    {
      Assert.expect(_inspectionEventObservable != null);
      inspectingImageEvent = new InspectingImageInspectionEvent(
        reconstructedImages,
        padSliceNameEnum,
        InspectionEventEnum.BEGIN_INSPECTING_IMAGE);
      _inspectionEventObservable.sendEventInfo(inspectingImageEvent);
    }

    // since the focus confirmation engine can potentially modify the componentInspectionResults while we are using them,
    // we have to synchronize things so that the inspectionEngine and the focus confirmation engine are not classifying
    // a component at the same time.
    synchronized (_focusConfirmationEngine.getSynchronizationObject(reconstructionRegion))
    {
      //swee-yee.wong - XCR-3237 Intermittent software crash when generate precision tuning image
      //stop the classify joints process if there are any exception occurs or user aborted
      // check if we are skipping this region due to an abort
      if (getAbortRequested())
        return;
      imageAnalysis.classifyJoints(reconstructedImages);

      //fixing CR 1019 - Pull out the Measurement extra Infomation
      boolean isDumpZHeightInMeasurementXMLFile = Config.getInstance().getBooleanValue(SoftwareConfigEnum.DUMP_ZHEIGHT_IN_MEASUREMENTS_XML_FILE);
      if (isDumpZHeightInMeasurementXMLFile)
        addRegionToSliceMap(reconstructionRegion.getTestSubProgram().getTestProgram(), reconstructedImages);
    }

    // If we're in production mode do any necessary re-analysis of misfocused images.
    if (_production)
    {
      _focusConfirmationEngine.retestRegionIfNeeded(reconstructedImages, _doSurfaceModeling);
      //debug(reconstructionRegion.getComponent().getReferenceDesignator());
      //if(reconstructionRegion.getComponent().getReferenceDesignator().matches("U4"))
      //_focusConfirmationEngine.dumpPlotInfo(reconstructedImages); /// Chin Seong, Will be Commented (memory problem)
    }

    // we're done with the images (but before the point when we would release them). The
    // measurement group classificiation only works from measurements.
    if (!_precisionTuningImageAcquisition)
    {
      inspectingImageEvent = new InspectingImageInspectionEvent(
        reconstructedImages,
        padSliceNameEnum,
        InspectionEventEnum.DONE_INSPECTING_IMAGE);
      _inspectionEventObservable.sendEventInfo(inspectingImageEvent);
    }

    final boolean processingPartialTest = true;
    for (Subtype subtype : subtypesInRegion)
    {
      List<JointInspectionData> jointInspectionDataObjects =
          reconstructionRegion.getInspectableJointInspectionDataList(subtype);

      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
// Comment for fixing CR 1019 - Pull out the Measurement extra Infomation added by WC
// Comment by Wei Chin
//        setSliceIdToReconstructedSliceMapForInspectionResult(jointInspectionData, reconstructedImages);

        classifyMeasurementGroupIfNecessary(jointInspectionData, reconstructedImages, processingPartialTest);
      }
    }

    // this is a synchronized collection
    int regionId = reconstructionRegion.getRegionId();
    ReconstructionRegion removedRegion = null;
    boolean finishedAllRegions = false;
    synchronized (_regionsToBeInspected)
    {
      removedRegion = _regionsToBeInspected.remove(regionId);
      finishedAllRegions = _regionsToBeInspected.isEmpty();
    }
    // If we're aborting, this list may have already been cleared out, in which case it's ok if the
    // region is not there.  Otherwise, something got screwed up.
    if (_abortRequested == false)
    {
      Assert.expect(removedRegion != null, "We just inspected a region we weren't supposed to!  Region: " + regionId);
    }

    if ((_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS)) ||
        (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_WHEN_NECESSARY)))
    {
      // tell the IAE that we are done with the image
      // the following FALSE parameter is bogus, Dave says they don't need to
      // know if the image has failures or not, but they might want it in the future,
      // in which case we'd have to fix this
      if (_imageAcquisitionEngine.isAbortInProgress() == false)
      {
        _imageAcquisitionEngine.reconstructionRegionComplete(reconstructionRegion, false);
      }
    }

    // We're finished inspecting this reconstruction region.
    // Added by Khang Wah, 2013-08-29, RealTime PSH
    // Before mark the region as complete, we may want to update the counter on PSH region
    if (_productionTuneEnabled || _precisionTuningImageAcquisition)
    {
      if(Config.isRealTimePshEnabled() && reconstructionRegion.getTestSubProgram().isSufficientMemoryForRealTimePsh())
      {
        if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION)== false ||
            Config.getInstance().getBooleanValue(SoftwareConfigEnum.OFFLINE_RECONSTRUCTION))
          reconstructionRegion.triggerConsumerRegion();
      }

      // RMS/JMH: For production and precision tuning image acquisition, we need to keep the
      // recontructed images until the MeasurementGroup and Component are finalized.
      // Tell the ReconstructedImagesManager we're finished with this region, but don't explicitly free images.
      // Images will be freed inside of classifyMeasurementGroupIfNecessary().
      _imageAcquisitionEngine.getReconstructedImagesProducer().getReconstructedImagesManager().markRegionAsFinishedInspection(reconstructionRegion);
    }
    else
    {
      // If number of repair images to save has a limit, and we've exceeded it, free these images now.
      // Don't bother telling ReconstructedImagesManager we're finished in this case.
      if (_productionOrAlwaysGenerateAndProcessResultsForUnitTests == false ||
          (_limitNumRepairImagesToSave && _savedRepairImages.size() >= _numRepairImagesToSave))
      {
        synchronized (_unfreedImages)
        {
          if (_unfreedImages.contains(reconstructionRegion))
          {
            _imageAcquisitionEngine.freeReconstructedImages(reconstructionRegion);
            _unfreedImages.remove(reconstructionRegion);
          }
        }
      }
      else
      {
        if (Config.isRealTimePshEnabled() && reconstructionRegion.getTestSubProgram().isSufficientMemoryForRealTimePsh()) 
        {
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION)== false ||
              Config.getInstance().getBooleanValue(SoftwareConfigEnum.OFFLINE_RECONSTRUCTION))
            reconstructionRegion.triggerConsumerRegion();
        }
        // RMS: From this point on, we may need repair images, but never inspectable images.
        // Mark the region as complete, but keep repair images in case a component/groups failure occurs.
        _imageAcquisitionEngine.getReconstructedImagesProducer().getReconstructedImagesManager().markRegionAsFinishedInspection(reconstructionRegion);
        _reconstructedImagesCachingManager.setRegionToCacheAsRepairImage(reconstructionRegion, ImageTypeEnum.AVERAGE);
      }
    }

    if (finishedAllRegions)
    {
      // do the stuff that only happens if we have processed all the scheduled regions
      finishRemainingClassification();
      if (_subtypeStichImageForVVTS.isEmpty() == false)
      {
        saveComponentMagnificationInfo();
      }
    }
  }

  /**
   * When this method is called, it can be assumed that we are totally done with
   * this joint.
   *
   * @author Peter Esbensen
   */
  private void finalizeInspectionOfJoint(JointInspectionData jointInspectionData) throws XrayTesterException
  {
    Assert.expect(jointInspectionData != null);

    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    ReconstructionRegion reconstructionRegion = _padToInspectionRegionMap.get(jointInspectionData.getPad());
    boolean hasBeenPreviouslyFinalized = reconstructionRegion == null;
    if (hasBeenPreviouslyFinalized)
      return;

    //Chin Seong :: In order to suit XedOut Implementation, this must be commented
    /*if (isSubtypeInspected(jointInspectionData.getSubtype()))
     {
     // Increment the number of joints processed.
     incrementNumberOfProcessedJoints();
     }*/
    // Increment the number of failing joints if the joint failed
    synchronized (_reconstructedImagesCachingManager)
    {
      ReconstructedImages reconstructedImages = null;

      if (_precisionTuningImageAcquisition &&
          _precisionTuningReconstructionRegions.contains(reconstructionRegion) == false)
      {
        reconstructedImages = _reconstructedImagesCachingManager.getReconstructedImages(reconstructionRegion, ImageTypeEnum.AVERAGE);
        savePrecisionTuningImages(reconstructedImages);
        _precisionTuningReconstructionRegions.add(reconstructionRegion);
      }

      if (jointInspectionResult.passed() == false)
      {
        //Chin Seong :: in order to suit Xout Implementation there will be no more increment in own class
        //
        //incrementNumberOfDefectiveJoints();

        // Khang-Wah, Chnee 14-Jun-2010 - X-out implementation
        jointInspectionData.getBoard().addFailedJoint();

        // check if number of repair images to save has a limit or not and if so, did we cross it?
        if (_productionOrAlwaysGenerateAndProcessResultsForUnitTests &&
            (_limitNumRepairImagesToSave == false || _savedRepairImages.size() < _numRepairImagesToSave))
        {
          if (_reconstructedImagesCachingManager.areImagesForRegionCachedAsRepairImages(reconstructionRegion, ImageTypeEnum.AVERAGE) == true)
          {
            // images are already on disk - don't read them back!
            Map<SliceNameEnum, String> sliceNameToImagePathMap = _reconstructedImagesCachingManager.getSliceNameToCachedImagePathMap(reconstructionRegion, ImageTypeEnum.AVERAGE);
            writeRepairImagesAsCopyFromCachedImages(reconstructionRegion, sliceNameToImagePathMap);
          }
          else
          {
            if (_reconstructedImagesCachingManager.hasReconstructedImages(reconstructionRegion, ImageTypeEnum.AVERAGE))
            {
              // write the images to disk from memory
              if (reconstructedImages == null)
                reconstructedImages = _reconstructedImagesCachingManager.getReconstructedImages(reconstructionRegion, ImageTypeEnum.AVERAGE);

              // write repair images only for failing joints
              writeRepairImages(reconstructedImages);
            }
          }

          // Write defective images for production tune.
          if (_productionTuneEnabled)
             saveInspectionImageIfNecessary(reconstructedImages, reconstructionRegion, jointInspectionResult.passed());
        }

        // RMS: This block of code is a candidate for removal.
//        if (_limitNumRepairImagesToSave == true && _savedRepairImages.size() >= _numRepairImagesToSave)
//        {
//          for (Pair<ReconstructionRegion, ImageTypeEnum> entry : _reconstructedImagesCachingManager.getRegionsFlaggedAsRepairImages())
//          {
//            ReconstructionRegion region = entry.getFirst();
//            synchronized (_unfreedImages)
//            {
//              if (_unfreedImages.contains(region))
//              {
//                _imageAcquisitionEngine.freeReconstructedImages(region);
//                _unfreedImages.remove(region);
//              }
//            }
//          }
//        }
      }
      else
      {
        // pass joint
        if (_productionTuneEnabled)
        {
          saveInspectionImageIfNecessary(reconstructedImages, reconstructionRegion, jointInspectionResult.passed());
        }
      }

      if (reconstructedImages != null)
      {
        reconstructedImages.decrementReferenceCount();
        reconstructedImages = null;
      }
    }

    // write results
    writeResults(jointInspectionResult, reconstructionRegion);

    _jointInspectionResultObservable.addJointInspectionResult(jointInspectionResult);

    jointInspectionData.clearJointInspectionResult();
  }

  /**
   * @author Laura Cormos
   * @author Peter Esbensen
   */
  void writeRepairImages(ReconstructedImages reconstructedImages) throws DatastoreException
  {
    writeRepairImages(reconstructedImages, 0);
  }

  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   * @author Rex Shang
   */
  void writeRepairImagesAsCopyFromCachedImages(ReconstructionRegion reconstructionRegion,
                                               Map<SliceNameEnum, String> sliceNameToCachePathMap) throws DatastoreException
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameToCachePathMap != null);

    // check if number of repair images to save has a limit or not and if so, did we cross it?
    if (_limitNumRepairImagesToSave)
    {
      if (_savedRepairImages.size() >= _numRepairImagesToSave)
        return;
    }

    // write repair images to disk and keep track of which ones are already saved
    if (_productionOrAlwaysGenerateAndProcessResultsForUnitTests)
    {
      List<String> currentRepairImagePaths = _imageManager.getResultImagePaths(reconstructionRegion, _project.getName(), _inspectionStartTime);
      if (_savedRepairImages.containsAll(currentRepairImagePaths) == false)
      {
        _testExecutionTimer.startImageSaveTimer();
        try
        {
          Map<SliceNameEnum, String> sliceNameToCachedImagePathMap =
              _reconstructedImagesCachingManager.getSliceNameToCachedImagePathMap(reconstructionRegion, ImageTypeEnum.AVERAGE);

          // RMS: With focus confirmation, it is possible that there are more
          // slices in _reconstructedImagesCachingManager.getSliceNameToCachedImagePathMap()
          // than what we need.
          List<String> repairImageNames = new ArrayList<String>();
          for (String repairImagePath : currentRepairImagePaths)
            repairImageNames.add(FileUtilAxi.getNameWithoutPath(repairImagePath));

          String resultImagesDirectory = Directory.getInspectionResultsDir(_project.getName(), _inspectionStartTime);

          for (Map.Entry<SliceNameEnum, String> entry : sliceNameToCachedImagePathMap.entrySet())
          {
            String sourceFileNameWithPath = entry.getValue();
            String fileName = FileUtilAxi.getNameWithoutPath(sourceFileNameWithPath);

            if (repairImageNames.contains(fileName))
            {
              repairImageNames.remove(fileName);
              String targetFileNameWithPath = resultImagesDirectory + File.separator + fileName;

              Assert.expect(currentRepairImagePaths.contains(targetFileNameWithPath));

              FileUtilAxi.copy(sourceFileNameWithPath, targetFileNameWithPath);
            }
          }

//          Assert.expect(repairImageNames.size() == 0, "Not all repair images were found.");
          if(repairImageNames.isEmpty() == false)
          {
            System.out.println("Err: Not all repair images were found");
            System.out.println("repairImageNames : " + repairImageNames.toString());
            repairImageNames.clear();
          }
        }
        finally
        {
          _testExecutionTimer.stopImageSaveTimer();
        }
        // keep track of which images were saved
        _savedRepairImages.addAll(currentRepairImagePaths);
      }
    }
  }

  /**
   * @author Matt Wharton
   * @author Laura Cormos
   */
  void writeRepairImages(ReconstructedImages reconstructedImages,
                         int focusOffsetInNanometers) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);

    // check if number of repair images to save has a limit or not and if so, did we cross it?
    if (_limitNumRepairImagesToSave)
    {
      if ( _savedRepairImages.size() >= _numRepairImagesToSave)
        return;
    }
    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

    // write repair images to disk and keep track of which ones are already saved
    if (_productionOrAlwaysGenerateAndProcessResultsForUnitTests)
    {
      List<String> currentRepairImagePaths = _imageManager.getResultImagePaths(reconstructionRegion,
                                                                               focusOffsetInNanometers,
                                                                               _project.getName(),
                                                                               _inspectionStartTime);

      if (_savedRepairImages.containsAll(currentRepairImagePaths) == false)
      {
        _testExecutionTimer.startImageSaveTimer();
        try
        {
          _imageManager.saveRepairImages(reconstructedImages, focusOffsetInNanometers, _project.getName(), _inspectionStartTime);
        }
        finally
        {
          _testExecutionTimer.stopImageSaveTimer();
        }

        // keep track of which images were saved
        _savedRepairImages.addAll(currentRepairImagePaths);
      }
    }
  }

  /**
   * @author Matt Wharton
   * @author Laura Cormos
   * @author John Heumann
   */
  void writeRepairImages(ReconstructedImages reconstructedImages,
                         int focusOffsetInNanometers,
                         Set<SliceNameEnum> slicesToExclude) throws DatastoreException
  {
    if (slicesToExclude.size() == 0)
    {
      writeRepairImages(reconstructedImages, focusOffsetInNanometers);
      return;
    }

    Assert.expect(reconstructedImages != null);

    // check if number of repair images to save has a limit or not and if so, did we cross it?
    if (_limitNumRepairImagesToSave)
    {
      if ( _savedRepairImages.size() >= _numRepairImagesToSave)
        return;
    }
    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

    // write repair images to disk and keep track of which ones are already saved
    if (_productionOrAlwaysGenerateAndProcessResultsForUnitTests)
    {
      List<String> currentRepairImagePaths = _imageManager.getResultImagePaths(reconstructionRegion,
                                                                               focusOffsetInNanometers,
                                                                               slicesToExclude,
                                                                               _project.getName(),
                                                                               _inspectionStartTime);

      if (_savedRepairImages.containsAll(currentRepairImagePaths) == false)
      {
        _testExecutionTimer.startImageSaveTimer();
        try
        {
          _imageManager.saveRepairImages(reconstructedImages, focusOffsetInNanometers,
                                         slicesToExclude, _project.getName(), _inspectionStartTime);
        }
        finally
        {
          _testExecutionTimer.stopImageSaveTimer();
        }

        // keep track of which images were saved
        _savedRepairImages.addAll(currentRepairImagePaths);
      }
    }
  }

  /**
   * @author Laura Cormos
   * @author Peter Esbensen
   */
  private void writeResults(JointInspectionResult jointInspectionResult,
                            ReconstructionRegion reconstructionRegion) throws DatastoreException
  {
    Assert.expect(jointInspectionResult != null);
    Assert.expect(reconstructionRegion != null);

    _testExecutionTimer.startInspectionResultsWriterTimer();
    _inspectionResultsWriter.writeJoint(jointInspectionResult);
    _testExecutionTimer.stopInspectionResultsWriterTimer();

    if (_productionOrAlwaysGenerateAndProcessResultsForUnitTests)
    {
      synchronized (_resultsXmlWriter)
      {
        _testExecutionTimer.startXmlWriterTimer();
        _resultsXmlWriter.writeJointInspectionResult(jointInspectionResult, reconstructionRegion);
        if (_generateMeasurementsXML)
          _measurementsXmlWriter.writeJointInspectionResult(jointInspectionResult);
        _testExecutionTimer.stopXmlWriterTimer();

        /** @todo LC remove before ship - investigating call effectiveness */
        if (_FLAG_CALL_RATE_SPIKES)
        {
          writeFailedJointData(jointInspectionResult);
        }
      }
    }
  }

  /**
   * @author Matt Wharton
   */
  synchronized void addSliceOffsetToJoint(JointInspectionResult jointInspectionResult,
                                          ReconstructedImages reconstructedImages,
                                          int sliceOffset)
  {
    _resultsXmlWriter.addSliceOffsetToJoint(jointInspectionResult, reconstructedImages, sliceOffset);
  }
  
  /**
   * @author Wei Chin
   */
  synchronized void addDiagnosticSliceToJoint(JointInspectionResult jointInspectionResult,
                                          ReconstructedImages reconstructedImages)
  {
    _resultsXmlWriter.addDiagnosticSliceToJoint(jointInspectionResult, reconstructedImages);
  }
  
  /**
   * @author Siew Yeng
   */
  synchronized void addEnhancedImageSliceToJoint(JointInspectionResult jointInspectionResult,
                                          ReconstructedImages reconstructedImages)
  {
    _resultsXmlWriter.addEnhancedImageSliceToJoint(jointInspectionResult, reconstructedImages);
  }

  /**
   * @author Matt Wharton
   */
  synchronized void addSliceOffsetToComponent(ComponentInspectionResult componentInspectionResult,
                                              ReconstructedImages reconstructedImages,
                                              int sliceOffset)
  {
    _resultsXmlWriter.addSliceOffsetToComponent(componentInspectionResult, reconstructedImages, sliceOffset);
  }

  /**
   * Ask the Image Analysis Subsystem to classify all the joints in this measurement group and
   * store the results.
   *
   * @author Peter Esbensen
   **/
  private void classifyAndProcessResults(MeasurementGroup measurementGroup,
                                         Map<Pad, ReconstructionRegion> padToImageMap) throws XrayTesterException
  {
    Assert.expect(measurementGroup != null);
    Assert.expect(padToImageMap != null);

    ImageAnalysis imageAnalysis = new ImageAnalysis();
    imageAnalysis.setDoSurfaceModeling(_doSurfaceModeling);
    imageAnalysis.classifyMeasurementGroup(measurementGroup, padToImageMap);

    Assert.expect(_padToInspectionRegionMap != null);
  }
  
   /**
   * Ask the Image Analysis Subsystem to classify all the joints in this measurement group and
   * store the results.
   *
   * @author Peter Esbensen
   * @Edited by Siew Yeng - XCR-2168
   **/
  private void classifyAndProcessResults(List<MeasurementGroup> measurementGroups,
                                         Map<Pad, ReconstructionRegion> padToImageMap) throws XrayTesterException
  {
    Assert.expect(measurementGroups != null);
    Assert.expect(padToImageMap != null);

    ImageAnalysis imageAnalysis = new ImageAnalysis();
    imageAnalysis.setDoSurfaceModeling(_doSurfaceModeling);
    if(measurementGroups.size() == 1)
      imageAnalysis.classifyMeasurementGroup(measurementGroups.get(0), padToImageMap);
    else
      imageAnalysis.classifyMeasurementGroups(measurementGroups, padToImageMap);

    Assert.expect(_padToInspectionRegionMap != null);
  }

  /**
   * Handles the specified Throwable from a worker thread. Tracks the exception
   * and triggers an inspection abort.
   *
   * @author Matt Wharton
   */
  private synchronized void handleWorkerThreadException(Throwable ex)
  {
    Assert.expect(ex != null);

    if (_workerThreadException == null)
    {
      _workerThreadException = ex;
    }

    try
    {
      // Abort the inspection.
      abort();
    }
    catch (Throwable innerEx)
    {
      // We're already in a bad state and will throw the first exception we caught.
      // Throwing another exception at this point won't make a difference.
    }
  }

  /**
   * Checks to see if a worker thread exception was caught and rethrows it on
   * whatever thread called this method.
   *
   * @author Matt Wharton
   */
  private synchronized void checkForWorkerThreadException() throws XrayTesterException
  {
    if (_workerThreadException != null)
    {
      if (_workerThreadException instanceof XrayTesterException)
      {
        throw (XrayTesterException) _workerThreadException;
      }
      else if (_workerThreadException instanceof RuntimeException)
      {
        throw (RuntimeException) _workerThreadException;
      }
      else
      {
        // Unexpected exception.  Dump the stack trace and assert.
        Assert.logException(_workerThreadException);
      }
    }
  }

  /**
   * @author Peter Esbensen
   */
  private synchronized boolean getAbortRequested()
  {
    return _abortRequested;
  }

  /**
   * @author Peter Esbensen
   */
  private synchronized void setAbortRequested(boolean newValue)
  {
    _abortRequested = newValue;
  }

  /**
   * Add the component name to the internal map data structures.
   *
   * @author Peter Esbensen
   */
  private void addComponentDataToMaps(ComponentInspectionData componentInspectionData)
  {
    Assert.expect(componentInspectionData != null);

    Component component = componentInspectionData.getComponent();

    HashMap<Subtype, MeasurementGroup> subtypeToMeasurementGroupMap = new HashMap<Subtype, MeasurementGroup>();

    for (JointInspectionData jointInspectionData : componentInspectionData.getInspectableJointInspectionDataSet())
    {
      Pad pad = jointInspectionData.getPad();
      MeasurementGroup measurementGroup = null;
      Subtype subtype = pad.getSubtype();
      if (subtypeToMeasurementGroupMap.containsKey(subtype))
      {
        measurementGroup = subtypeToMeasurementGroupMap.get(subtype);
      }
      else
      {
        measurementGroup = new MeasurementGroup();
        subtypeToMeasurementGroupMap.put(subtype, measurementGroup);
        measurementGroup.setSubtype(subtype);
      }
      Assert.expect(measurementGroup != null);
      measurementGroup.addJointInspectionData(jointInspectionData);
    }

    Assert.expect(_componentToSubtypeToMeasurementGroupMap != null);
    _componentToSubtypeToMeasurementGroupMap.put(component, subtypeToMeasurementGroupMap);
    _componentToMeasurementGroupsMap.put(component, new ArrayList()); // Siew Yeng
  }

  /**
   * If an inspection is paused, this will cause the inspection to continue
   * until we encounter the next enabled diagnostic on the next joint.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public synchronized void runToNextJoint()
  {
    Assert.expect(_inspectionStateController != null);

    _inspectionStateController.requestPauseOnNextJoint();
    _inspectionStateController.resumeInspection();
  }

  /**
   * If an inspection is paused, this will cause the inspection to continue
   * until the next enabled diagnostic on the next slice.
   *
   * @author Matt Wharton
   */
  public synchronized void runToNextSlice()
  {
    Assert.expect(_inspectionStateController != null);

    _inspectionStateController.requestPauseOnNextSlice();
    _inspectionStateController.resumeInspection();
  }

  /**
   * If an inspection is paused, this will cause the inspection to continue until the next enabled
   * diagnostic on the next algorithm.
   *
   * @author Matt Wharton
   */
  public synchronized void runToNextAlgorithm()
  {
    Assert.expect(_inspectionStateController != null);

    _inspectionStateController.requestPauseOnNextAlgorithm();
    _inspectionStateController.resumeInspection();
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public synchronized void runToNextInspectionRegion()
  {
    Assert.expect(_inspectionStateController != null);

    _inspectionStateController.requestPauseOnNextInspectionRegion();
    _inspectionStateController.resumeInspection();
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public synchronized void runToNextDiagnosticEvent()
  {
    Assert.expect(_inspectionStateController != null);

    _inspectionStateController.requestPauseOnNextDiagnostic();
    _inspectionStateController.resumeInspection();
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public synchronized void continueInspection()
  {
    Assert.expect(_inspectionStateController != null);

    _inspectionStateController.resumeInspection();
  }

  /**
   * @author Matt Wharton
   */
  public synchronized void pauseInspection()
  {
    Assert.expect(_inspectionStateController != null);

    _inspectionStateController.requestPauseInspection();
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setPanelInspectionSettings(PanelInspectionSettings panelInspectionSettings)
  {
    Assert.expect(panelInspectionSettings != null);
    _panelInspectionSettings = panelInspectionSettings;
  }

  /**
   * Returns true if diagnostics are enabled, false otherwise.
   *
   * @author Matt Wharton
   */
  public boolean areDiagnosticsEnabled(JointTypeEnum jointTypeEnum, Algorithm algorithm)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithm != null);
    Assert.expect(_panelInspectionSettings != null);

    return _panelInspectionSettings.areDiagnosticsOn(jointTypeEnum, algorithm.getAlgorithmEnum());
  }

  /**
   * @author Matt Wharton
   */
  public boolean isAlgorithmDisabled(JointTypeEnum jointTypeEnum, Algorithm algorithm)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithm != null);
    Assert.expect(_panelInspectionSettings != null);

    return _panelInspectionSettings.isAlgorithmDisabled(jointTypeEnum, algorithm.getAlgorithmEnum());
  }

  /**
   * @author Matt Wharton
   */
  public boolean isSubtypeInspected(Subtype subtype)
  {
    Assert.expect(subtype != null);

    return _panelInspectionSettings.isSubtypeInspected(subtype);
  }

  /**
   * Provides a rough estimate of how long it will take (in millis) to inspect
   * the specified TestProgram.
   *
   * @author Matt Wharton
   */
  public int getEstimatedTestTimeInMillis(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    int estimatedScanTime = _imageAcquisitionEngine.getEstimatedScanTimeInMillis(testProgram);

    return estimatedScanTime;
  }

  /**
   * Returns the current inspection run's start time in milliseconds
   *
   * @author Laura Cormos
   */
  public long getInspectionStartTimeInMillis()
  {
    Assert.expect(_inspectionStartTime != -1);
    return _inspectionStartTime;
  }

  /**
   * @author Laura Cormos
   */
  private Map<String, String> buildBoardNameToSerialNumMapForTestDevelpment(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    Map<String, String> boardNameToSerialNumMap = new HashMap<String, String>();
    List<Board> boards = testProgram.getProject().getPanel().getBoards();
    for(Board board : boards)
    {
      boardNameToSerialNumMap.put(board.getName(), "");
    }
    return boardNameToSerialNumMap;
  }

  /**
   * @author Matt Wharton
   */
  private void openProfileDataFiles() throws DatastoreException
  {
    _gullwingProfileDataWriter = new CSVFileWriterAxi(Arrays.asList("Unique Pad Name", "Profile Bin", "Thickness (mils)"));
    _gullwingProfileDataWriter.open(Directory.getTempDir() + File.separator + "GullwingProfileData.csv");

    _shortProfileDataWriter = new CSVFileWriterAxi(Arrays.asList("Unique Pad Name", "Slice", "Profile Bin", "Thickness (mils)"));
    _shortProfileDataWriter.open(Directory.getTempDir() + File.separator + "ShortProfileData.csv");
  }

  /**
   * @author Matt Wharton
   */
  private void closeProfileDataFiles()
  {
    Assert.expect(_gullwingProfileDataWriter != null);
    Assert.expect(_shortProfileDataWriter != null);

    _gullwingProfileDataWriter.close();
    _gullwingProfileDataWriter = null;

    _shortProfileDataWriter.close();
    _shortProfileDataWriter = null;
  }

  /**
   * @author Matt Wharton
   */
  public CSVFileWriterAxi getGullwingProfileDataWriter()
  {
    Assert.expect(_gullwingProfileDataWriter != null);

    return _gullwingProfileDataWriter;
  }

  /**
   * @author Matt Wharton
   */
  public CSVFileWriterAxi getShortProfileDataWriter()
  {
    Assert.expect(_shortProfileDataWriter != null);

    return _shortProfileDataWriter;
  }

  /** @todo LC remove everything below here before ship - investigating call effectiveness */
  public void writeFailedJointData(JointInspectionResult jir)
  {
    Assert.expect(jir != null);
    for (JointIndictment ind : jir.getIndictments())
    {
      JointInspectionData jid = jir.getJointInspectionData();
      _failedJointsAndComponentsDataWriter.writeDataLine(Arrays.asList(_project.getName(),
                                                                       String.valueOf(getInspectionStartTimeInMillis()),
                                                                       jid.getComponent().getReferenceDesignator(),
                                                                       jid.getPad().getName(),
                                                                       jid.getSubtype().getShortName(),
                                                                       ind.getIndictmentName()));
    }
  }

  public void writeFailedComponentData(ComponentInspectionResult cir)
  {
    Assert.expect(cir != null);
    for (ComponentIndictment ind : cir.getIndictments())
    {
      ComponentInspectionData cid = cir.getComponentInspectionData();
      //Siew Yeng - XCR-2168
      if(ind.getSubtypes().size() > 1)
      {
        String subtypeStr = "";
        for(Subtype subtype : ind.getSubtypes())
        {
          if(subtypeStr.isEmpty() == false)
            subtypeStr = subtypeStr + "|";
            
          subtypeStr = subtypeStr + subtype.getShortName();
        }
        _failedJointsAndComponentsDataWriter.writeDataLine(Arrays.asList(_project.getName(),
                                                                         String.valueOf(getInspectionStartTimeInMillis()),
                                                                         cid.getComponent().getReferenceDesignator(),
                                                                         "<comp>",
                                                                         subtypeStr,
                                                                         ind.getIndictmentName()));
      }
      else if(ind.getSubtypes().size() == 1)
      {
        _failedJointsAndComponentsDataWriter.writeDataLine(Arrays.asList(_project.getName(),
                                                                         String.valueOf(getInspectionStartTimeInMillis()),
                                                                         cid.getComponent().getReferenceDesignator(),
                                                                         "<comp>",
                                                                         ind.getSubtypes().get(0).getShortName(),
                                                                         ind.getIndictmentName()));
      }
    }
  }

  public void initializeFailedJointsAndComponentsCSVFileWriter()
  {
    String failedJointsAndComponentsDataFileName = Directory.getTempDir() + File.separator + "failedJointsAndComponentsData.csv";
    if (_failedJointsAndComponentsDataWriter == null)
    {
      try
      {
        _failedJointsAndComponentsDataWriter = new CSVFileWriterAxi(Arrays.asList("Project", "Inspection Start", "Comp Name", "Subtype", "Joint Name", "Indictment Name"));
        _failedJointsAndComponentsDataWriter.open(failedJointsAndComponentsDataFileName, true);
      }
      catch (DatastoreException dex)
      {
        handleWorkerThreadException(dex);
      }
    }
    else
    {
      try
      {
        _failedJointsAndComponentsDataWriter.open(failedJointsAndComponentsDataFileName, true);
      }
      catch (DatastoreException dex)
      {
        handleWorkerThreadException(dex);
      }
    }
  }

  public void closeFailedJointsAndComponentsCSVFileWriter()
  {
    _failedJointsAndComponentsDataWriter = null;
  }

  /**
   * @author John Heumann
   */
  public void setDoSurfaceModeling(boolean trueOrFalse)
  {
    _doSurfaceModeling = trueOrFalse;
  }

  /**
   * fixing CR 1019 - Pull out the Measurement extra Infomation Put the ZHeight
   * value into a map by regionID and Slice ID
   *
   * @author Chin Seong, Kee
   */
  private void addRegionToSliceMap(TestProgram testProgram, ReconstructedImages reconstructedImages)
  {
    Map<Integer, Integer> _sliceIdToZheightMap = new HashMap<Integer, Integer>();

    for(ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
    {
      //reconstructedImages.getReconstructedSlices().iterator().next().getHeightInNanometers();
      //debug("RegionID = " + reconstructedSlice.getSliceNameEnum().getId() + "ZHeight = " + reconstructedSlice.getHeightInNanometers());
      _sliceIdToZheightMap.put(reconstructedSlice.getSliceNameEnum().getId(), reconstructedSlice.getHeightInNanometers());
      _waitingForAlignmentExceptionActionUserInput.setValue(false);
    }
    testProgram.addSliceIdToReconstructedSliceMap(reconstructedImages.getReconstructionRegionId(), _sliceIdToZheightMap);
  }

  /**
   * @author Wei Chin
   */
  public void waitForPreviousAlignmentExceptionUnlock()
  {
    try
    {
      _waitingForAlignmentExceptionActionUserInput.waitUntilFalse();
    }
    catch (InterruptedException ex)
    {
      //Do nothing ...
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  public void continueWithAlignmentInput()
  {
    _waitingForAlignmentExceptionActionUserInput.setValue(false);
    _waitingForUserInput.setValue(false);
    //exceptionList.remove(0);
    //if(exceptionList.size() == 0)
    //   feedbackUserInput();
  }

  /**
   * @author Wei Chin
   */
  public void setCurrentBoardIsXout()
  {
    _currentBoardIsXout = true;
  }

  /**
   * @author Wei Chin
   */
  public void resetXoutStatus()
  {
    _currentBoardIsXout = false;
  }

  /**
   * @author Jack Hwee
   */
  private void saveProductionTuneDefectImageInfo(TestProgram testProgram) throws DatastoreException
  {
    // finally, we need to re-save some image set data if we have saved defect images
    if (_productionTuneManager.isProjectEnabledForProductionTune(testProgram.getProject().getName()))
    {
      if (_onlineImageSetData.getNumberOfImagesSaved() > 0)
      {
        _imageSetInfoWriter.write(_onlineImageSetData);
        _imageManager.saveProductionTuneDefectImageInfo(_onlineImageSetData,
                                                        testProgram,
                                                        _productionTuneDefectiveReconstructionRegions);
      }
    }
  }

// Comment for fixing CR 1019 - Pull out the Measurement extra Infomation added by WC
// Comment by Wei Chin
  /**
   * @param jointInspectionData JointInspectionData
   * @param reconstructedImages ReconstructedImages
   * @author Wei Chin, Chong
   */
//  public void setSliceIdToReconstructedSliceMapForInspectionResult(JointInspectionData jointInspectionData, ReconstructedImages reconstructedImages)
//  {
//    Assert.expect(jointInspectionData != null);
//    Assert.expect(reconstructedImages != null);
//
//    jointInspectionData.getComponentInspectionData().getComponentInspectionResult().setSliceIdToReconstructedSliceMap(reconstructedImages.getSliceIdToReconstructedSliceMap());
//    jointInspectionData.getJointInspectionResult().setSliceIdToReconstructedSliceMap(reconstructedImages.getSliceIdToReconstructedSliceMap());
//  }
  /**
   * @author Cheah Lee Herng
   */
  private void performAlignmentSurfaceMap(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    final TestProgram finalTestProgram = testProgram;
    _acquiringAlignmentOpticalImages.setValue(true);
    _alignmentOpticalImageAcquisitionWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          _opticalImageAcquisitionEngine.acquireAlignmentOpticalImages(finalTestProgram, false, _testExecutionTimer);
        }
        catch (Throwable th)
        {
          _abortAlignmentOpticalImageAcquisitionFlag.setValue(true);
          handleWorkerThreadException(th);
        }
        finally
        {
          if (_opticalImageAcquisitionEngine.isUserAborted())
          {
            _abortAlignmentOpticalImageAcquisitionFlag.setValue(true);
          }

          _acquiringAlignmentOpticalImages.setValue(false);
        }
      }
    });

    try
    {
      _acquiringAlignmentOpticalImages.waitUntilFalse();
    }
    catch (InterruptedException ex)
    {
      // Do nothing
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  private void debug(String message)
  {
    if (_debug)
      System.out.println(_me + ": " + message);
  }
  
  private void setXrayTesterExceptionInResultThread(XrayTesterException xrayTesterExceptionInResultThread)
  {
    if(_xrayTesterExceptionInResultThread == null)
      _xrayTesterExceptionInResultThread = xrayTesterExceptionInResultThread;
  }
  
  /**
   * standardize the save inspection image when production tune setting enabled
   * @param reconstructedImages
   * @param reconstructionRegion
   * @param isPassed
   * @throws DatastoreException 
   */
  private void saveInspectionImageIfNecessary(ReconstructedImages reconstructedImages, ReconstructionRegion reconstructionRegion, boolean isPassed) throws DatastoreException
  {
    boolean needToDecrementCount = false;
    if (_productionTuneManager.getProductionTuneSettings().isSaveOnlySelectedImages())
    {
      if (_productionTuneManager.getProductionTuneSettings().isSelectedSaveImages(reconstructionRegion))
      {
        if (_productionTuneDefectiveReconstructionRegions.contains(reconstructionRegion) == false)
        {
          if (reconstructedImages == null)
          {
            reconstructedImages = _reconstructedImagesCachingManager.getReconstructedImages(reconstructionRegion, ImageTypeEnum.AVERAGE);
            needToDecrementCount = true;
          }
          saveProductionTuneDefectImages(reconstructedImages);

          if (_productionTuneManager.getProductionTuneSettings().hasDirectoryPath())
          {
            _imageManager.saveProductionTuneDefectImages(reconstructedImages,
              _productionTuneManager.getProductionTuneSettings().getDirectoryPath(),
              _onlineImageSetData.getImageSetName());
          }  
        }
      }
    }
    // this apply to saveAllImages and saveOnlydefectImages Option
    else if(_productionTuneManager.getProductionTuneSettings().isSaveAllImages() || (_productionTuneManager.getProductionTuneSettings().isSaveOnlyDefectImages() && isPassed == false))
    {
      if (_productionTuneDefectiveReconstructionRegions.contains(reconstructionRegion) == false)
      {
        if (reconstructedImages == null)
        {
          reconstructedImages = _reconstructedImagesCachingManager.getReconstructedImages(reconstructionRegion, ImageTypeEnum.AVERAGE);
          needToDecrementCount = true;
        }
        saveProductionTuneDefectImages(reconstructedImages);
      }
    }
    
    if(needToDecrementCount)
      reconstructedImages.decrementReferenceCount();
  }
  
  /**
   * @author Siew Yeng - XCR-3094
   */
  private boolean shouldClassifyAtComponentOrMeasurementGroupLevel(JointInspectionData jointInspectionData)
  {
    Subtype subtype = jointInspectionData.getSubtype();
    
    if(AlgorithmUtil.isAbleToStitchComponentImageAtVVTS(subtype))
    {
      boolean usesOneJointType = jointInspectionData.getComponent().getCompPackage().usesOneJointTypeEnum();
      if((usesOneJointType && subtype.getInspectionFamily().classifiesAtComponentOrMeasurementGroupLevel()) ||
          usesOneJointType == false && jointInspectionData.getComponent().getCompPackage().shouldClassifyAtComponentOrMeasurementGroupLevel())//mixed JointType
      {
        return true;
      } 
    }

    return subtype.getInspectionFamily().classifiesAtComponentOrMeasurementGroupLevel();
  }
  
  private void saveComponentMagnificationInfo() throws DatastoreException
  {
    Assert.expect(_project != null);
    
    String componentMagnificationInfoFilePath = FileName.getComponentMagnificationInfoFullPath(_project.getName(), _inspectionStartTime);
 
    FileWriterUtilAxi writer = new FileWriterUtilAxi(componentMagnificationInfoFilePath, false);
    try
    {
      writer.open();
      for (Subtype subtype : _subtypeStichImageForVVTS)
      {
        for (ComponentType componentType : subtype.getComponentTypes())
        {
          if (componentType.getComponentTypeSettings().usesOneSubtype())
          {
            if (subtype.getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.LOW))
            {
              writer.writeln(componentType.toString()+","+Config.getSystemCurrentLowMagnification());
            }
            else
            {
              writer.writeln(componentType.toString()+","+Config.getSystemCurrentHighMagnification());
            }
          }
          else
          {
            writer.writeln(componentType.toString()+","+Config.getSystemCurrentLowMagnification());
          }
        }
      }
    }
    finally
    {
      writer.close();
    }
    
  }
  
  /**
   * @author Janan XCR-3551 
   * Set enabled generate Measurements XML 
   */
  public void setGenerateMeasurementsXML(boolean bool)
  {
    _generateMeasurementsXML = bool;
  }
  
  /**
   * @author Siew Yeng
   */
  public boolean isProductionMode()
  {
    return _production;
  }
}