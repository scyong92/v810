package com.axi.v810.business.testExec;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import Jama.*;
import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class is responsible for both automatic and manual alignment.
 *
 * @author Peter Esbensen
 * @author Matt Wharton
 */
public class Alignment
{
  private static Alignment _alignment;

  private boolean _performingManualAlignmentVerification = false;
  private final static int _NUMBER_OF_ALIGNMENT_GROUPS_PER_ALIGNMENT = 3;
  private ImageAcquisitionEngine _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
  private static Config _config = Config.getInstance();
  private InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
  private boolean _cancelManualAlignment = false;
  private BooleanLock _waitingForManualAlignmentUserInput = new BooleanLock(false);
  private List<AlignmentInspectionEvent> _manualAlignmentEvents =
      Collections.synchronizedList(new LinkedList<AlignmentInspectionEvent>());
  private BooleanLock _manualAlignmentComplete = new BooleanLock(false);
  private Throwable _miscThrowableFromManualAlignment = null;
  private AlignmentTransformSettingsWriter _alignmentTransformSettingsWriter;

  private WorkerThread _manualAlignmentWorkerThread = new WorkerThread("manual alignment worker thread");
  private WorkerThread _alignmentImageAcquisitionWorker = new WorkerThread("alignment image acquisition worker thread");

  // these are used for manual alignment
  private double _currentManualAlignmentXOffsetRelativeToPanelInNanoMeters = 0.0;
  private double _currentManualAlignmentYOffsetRelativeToPanelInNanoMeters = 0.0;

  private static final int _ALIGNMENT_FEATURE_BORDER_WIDTH = 3;

  private static final boolean _DEVELOPER_DEBUG = Config.isDeveloperDebugModeOn();

  private boolean _performingRuntimeManualAlignment = false; //Siew Yeng - XCR1757
  
  //Khaw Chek Hau - XCR2285: 2D Alignment on v810
  private ProgramGeneration _programGeneration = null;
  private static final Pattern _alignment2DImageFileNamePattern = Pattern.compile(FileName.getAlignment2DImageFileNamePattern());
  private final static int _MAX_DISTANCE_BETWEEN_LOCATED_AND_DRAGGED_2D_REGION_CENTROIDS_IN_NANOS = 25400000; //1000 mils
  
  private boolean _isRunTimeAlignmentNeededAppliedInVerificationImages = false;
  
  //XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
  private boolean _isManualAlignmentInitiallyComplete = false;

  /**
   * @author Peter Esbensen
   */
  private Alignment()
  {
    _alignmentTransformSettingsWriter = AlignmentTransformSettingsWriter.getInstance();
  }

  /**
   * @author Peter Esbensen
   */
  public static synchronized Alignment getInstance()
  {
    if (_alignment == null)
    {
      _alignment = new Alignment();
    }
    return _alignment;
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void initializeAlignmentProcess(TestProgram testProgram) throws DatastoreException
  {
    Assert.expect(testProgram != null);

    for (TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
    {
      initializeAlignmentProcess(testSubProgram);
    }
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void initializeAlignmentProcess(TestSubProgram subProgram) throws DatastoreException
  {
    Assert.expect(subProgram != null);

    Project project = subProgram.getTestProgram().getProject();
    String projectName = project.getName();
    Panel panel = project.getPanel();
    PanelSettings panelSettings = panel.getPanelSettings();
    List<AlignmentGroup> alignmentGroups = new ArrayList<AlignmentGroup>();

    if (panelSettings.isPanelBasedAlignment())
    {
      if (panelSettings.isLongPanel())
      {
        if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
          alignmentGroups.addAll(panelSettings.getLeftAlignmentGroupsForLongPanel());
        else if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
          alignmentGroups.addAll(panelSettings.getRightAlignmentGroupsForLongPanel());
        else
          Assert.expect(false);
      }
      else
      {
        alignmentGroups.addAll(panelSettings.getAlignmentGroupsForShortPanel());
      }
    }
    else
    {
      if (subProgram.getBoard().getBoardSettings().isLongBoard())
      {
        if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
          alignmentGroups.addAll(subProgram.getBoard().getBoardSettings().getLeftAlignmentGroupsForLongPanel());
        else if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
          alignmentGroups.addAll(subProgram.getBoard().getBoardSettings().getRightAlignmentGroupsForLongPanel());
        else
          Assert.expect(false);
      }
      else
      {
        alignmentGroups.addAll(subProgram.getBoard().getBoardSettings().getAlignmentGroupsForShortPanel());
      }
    }

    // Delete any pre-existing alignment images if they exist.
    PanelLocationInSystemEnum panelLocationInSystem = subProgram.getPanelLocationInSystem();
    boolean isSecondPortionOfLongPanel = panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT);
    for (AlignmentGroup alignmentGroup : alignmentGroups)
    {
      String rawAlignmentImageFileName = FileName.getRawAlignmentImageFullPath(projectName,
                                                                               alignmentGroup.getName(),
                                                                               isSecondPortionOfLongPanel);
      String alignmentImageFileName = FileName.getAlignmentVerificationImageFullPath(projectName,
                                                                                     alignmentGroup.getName(),
                                                                                     isSecondPortionOfLongPanel);
      FileUtilAxi.delete(rawAlignmentImageFileName);
      FileUtilAxi.delete(alignmentImageFileName);
    }

    // mdw - we may want to move this up in the UI at some point.  For now, it's just a config file setting.
    boolean alignOnDarkPadsWithLightBackgrounds =
        _config.getBooleanValue(SoftwareConfigEnum.ALIGN_ON_DARK_PADS_WITH_LIGHT_BACKGROUNDS);

    for (AlignmentGroup alignmentGroup : alignmentGroups)
    {
      alignmentGroup.clearLocatedCentroid();

      // mdw - we may want to move this up in the UI at some point.  For now, it's just a config file setting.
      alignmentGroup.setDarkPadsWithLightBackground(alignOnDarkPadsWithLightBackgrounds);
    }
  }

  /**
   * block until an AlignmentInspectionEvent is available, then return that event.
   * @author George A. David
   * @author Matt Wharton
   */
  private synchronized AlignmentInspectionEvent getManualAlignmentInspectionEvent()
  {
    while (_manualAlignmentEvents.isEmpty())
    {
      try
      {
        wait(200);
      }
      catch (InterruptedException ie)
      {
        // do nothing
      }
    }

    return _manualAlignmentEvents.remove(0);
  }

  /**
   * Whenever and AlignmentInspectionEvent is available, send off the event
   * and wait for the user to align the image, then move on to the next event.
   *
   * @author George A. David
   * @author Matt Wharton
   * @author Siew Yeng 
   * - Edited: if performing manual alignment at production, set the new alignment transform to 
   * runtime manual alignment transform, do not affect the original alignment transform
   */
  private void runManualAlignmentProcess(final TestSubProgram subProgram, final TestExecutionTimer testExecutionTimer)
  {
    Assert.expect(subProgram != null);
    Assert.expect(testExecutionTimer != null);

    _miscThrowableFromManualAlignment = null;

    Runnable manualAlignmentTask = new Runnable()
    {
      public void run()
      {
        try
        {
          AlignmentInspectionEvent inspectionEvent = new AlignmentInspectionEvent(InspectionEventEnum.
                                                                                  MANUAL_ALIGNMENT_STARTED);
          inspectionEvent.setMagnificationType(subProgram.getMagnificationType());
          inspectionEvent.setPanelLocationInSystem(subProgram.getPanelLocationInSystem());
          inspectionEvent.setTestSubProgramId(subProgram.getId());
          _inspectionEventObservable.sendEventInfo(inspectionEvent);

          {
            Collection<AlignmentGroup> alignmentGroupsForTestSubProgram = subProgram.getAlignmentGroups();
            for (int i = 0; i < alignmentGroupsForTestSubProgram.size(); ++i)
            {
              _currentManualAlignmentXOffsetRelativeToPanelInNanoMeters = 0.0;
              _currentManualAlignmentYOffsetRelativeToPanelInNanoMeters = 0.0;
              AlignmentInspectionEvent alignmentInspectionEvent = getManualAlignmentInspectionEvent();
              _inspectionEventObservable.sendEventInfo(alignmentInspectionEvent);
              waitForUserInput();
              if (_cancelManualAlignment == false)
              {
                AlignmentGroup currentAlignmentGroup = alignmentInspectionEvent.getAlignmentGroup();
                PanelCoordinate expectedCentroidOfAlignmentGroup = currentAlignmentGroup.getExpectedCentroidInNanometers();
                int locatedXCoordinateOnPanel =
                    (int)Math.round(expectedCentroidOfAlignmentGroup.getX() + _currentManualAlignmentXOffsetRelativeToPanelInNanoMeters);
                int locatedYCoordinateOnPanel =
                    (int)Math.round(expectedCentroidOfAlignmentGroup.getY() + _currentManualAlignmentYOffsetRelativeToPanelInNanoMeters);
                PanelCoordinate actualCentroidOfAlignmentGroup = new PanelCoordinate(locatedXCoordinateOnPanel,
                                                                                     locatedYCoordinateOnPanel);
                if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
                {
                  System.out.print("Manual Alignment -- Alignment Group " + currentAlignmentGroup.getName() +
                                   " expected centroid location (in panel coords): ");
                  System.out.print("( " + expectedCentroidOfAlignmentGroup.getX() + " , " + expectedCentroidOfAlignmentGroup.getY() +
                                   " )");
                  System.out.println();
                  System.out.print("Manual Alignment -- Alignment Group " + currentAlignmentGroup.getName() +
                                   " centroid offset (in panel coords): ");
                  System.out.print("( " + _currentManualAlignmentXOffsetRelativeToPanelInNanoMeters + " , " + _currentManualAlignmentYOffsetRelativeToPanelInNanoMeters +
                                   " )");
                  System.out.println();
                  System.out.print("Manual Alignment -- Alignment Group " + currentAlignmentGroup.getName() +
                                   " actual centroid location (in panel coords): ");
                  System.out.print("( " + actualCentroidOfAlignmentGroup.getX() + " , " +
                                   actualCentroidOfAlignmentGroup.getY() + " )");
                  System.out.println();
                }

                if(subProgram.isLowMagnification())
                  currentAlignmentGroup.setLocatedCentroidInNanometers(actualCentroidOfAlignmentGroup);
                else
                  currentAlignmentGroup.setLocatedCentroidInNanometersForHighMag(actualCentroidOfAlignmentGroup);

                // Make sure that the user didn't drag the alignment point unreasonably far away.
                verifyUserSpecifiedAlignmentOffsetIsWithinAcceptableLimits(subProgram, currentAlignmentGroup);
              }
              else
              {
                _performingManualAlignmentVerification = false;
                _performingRuntimeManualAlignment = false;
                _manualAlignmentComplete.setValue(true);
                // Khaw Chek Hau - XCR2181 - Able to run alignment starting from failed board
                subProgram.setRunAlignmentLearned(false);
                return;
              }
            }

            if (_cancelManualAlignment == false)
            {
              // Calculate the initial manual transform.  We only want to preserve the translation components of the
              // transform at this point.
              Project project = subProgram.getTestProgram().getProject();
              Panel panel = project.getPanel();
              PanelSettings panelSettings = panel.getPanelSettings();
              //XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
              if (_isManualAlignmentInitiallyComplete)
                panelSettings.setIsManualAlignmentAllCompleted(true);
              else
                panelSettings.setIsManualAlignmentAllCompleted(false);
              java.awt.geom.AffineTransform manualAlignmentTransform = computeAlignmentTransformForTestSubProgram(subProgram);

              if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
              {
                java.awt.geom.AffineTransform defaultAlignmentTransform = subProgram.getDefaultCoreAlignmentTransform();
                System.out.println("Default transform matrix : " + defaultAlignmentTransform);
                System.out.println("Initial manual transform matrix : " + manualAlignmentTransform);
              }

              PanelLocationInSystemEnum panelLocationInSystem = subProgram.getPanelLocationInSystem();

              if( panelSettings.isPanelBasedAlignment() )
              {
                if (panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT))
                {
                  if(subProgram.isLowMagnification())
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      panelSettings.setLeftRuntimeManualAlignmentTransform(manualAlignmentTransform);
                    }
                    else
                    {
                      panelSettings.setLeftManualAlignmentTransform(manualAlignmentTransform);
                      panelSettings.setLeftRuntimeAlignmentTransform(manualAlignmentTransform);
                    }
                  }
                  else
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      panelSettings.setLeftRuntimeManualAlignmentTransformForHighMag(manualAlignmentTransform); 
                    }
                    else
                    {
                      panelSettings.setLeftManualAlignmentTransformForHighMag(manualAlignmentTransform);
                      panelSettings.setLeftRuntimeAlignmentTransformForHighMag(manualAlignmentTransform);  
                    }
                  }
                }
                else if (panelLocationInSystem.equals(PanelLocationInSystemEnum.RIGHT))
                {
                  if(subProgram.isLowMagnification())
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      panelSettings.setRightRuntimeManualAlignmentTransform(manualAlignmentTransform);
                    }
                    else
                    {
                      panelSettings.setRightManualAlignmentTransform(manualAlignmentTransform);
                      panelSettings.setRightRuntimeAlignmentTransform(manualAlignmentTransform);
                    }
                  }
                  else
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      panelSettings.setRightRuntimeManualAlignmentTransformForHighMag(manualAlignmentTransform);
                    }
                    else
                    {
                      panelSettings.setRightManualAlignmentTransformForHighMag(manualAlignmentTransform);
                      panelSettings.setRightRuntimeAlignmentTransformForHighMag(manualAlignmentTransform);
                    }
                  }
                }
                else
                {
                  Assert.expect(false, "Unknown panel location!");
                }
              }
              else
              {
                // Wei Chin added for individual Alignment
                if (panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT))
                {
                  if(subProgram.isLowMagnification())
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      subProgram.getBoard().getBoardSettings().setLeftRuntimeManualAlignmentTransform(manualAlignmentTransform);
                    }
                    else
                    {
                      subProgram.getBoard().getBoardSettings().setLeftManualAlignmentTransform(manualAlignmentTransform);
                      subProgram.getBoard().getBoardSettings().setLeftRuntimeAlignmentTransform(manualAlignmentTransform);
                    }
                  }
                  else
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      subProgram.getBoard().getBoardSettings().setLeftRuntimeManualAlignmentTransformForHighMag(manualAlignmentTransform);
                    }
                    else
                    {
                      subProgram.getBoard().getBoardSettings().setLeftManualAlignmentTransformForHighMag(manualAlignmentTransform);
                      subProgram.getBoard().getBoardSettings().setLeftRuntimeAlignmentTransformForHighMag(manualAlignmentTransform);
                    }
                  }
                }
                else if (panelLocationInSystem.equals(PanelLocationInSystemEnum.RIGHT))
                {
                  if(subProgram.isLowMagnification())
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      subProgram.getBoard().getBoardSettings().setRightRuntimeManualAlignmentTransform(manualAlignmentTransform);
                    }
                    else
                    {
                      subProgram.getBoard().getBoardSettings().setRightManualAlignmentTransform(manualAlignmentTransform);
                      subProgram.getBoard().getBoardSettings().setRightRuntimeAlignmentTransform(manualAlignmentTransform);
                    }
                  }
                  else
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      subProgram.getBoard().getBoardSettings().setRightRuntimeManualAlignmentTransformForHighMag(manualAlignmentTransform);
                    }
                    else
                    {
                      subProgram.getBoard().getBoardSettings().setRightManualAlignmentTransformForHighMag(manualAlignmentTransform);
                      subProgram.getBoard().getBoardSettings().setRightRuntimeAlignmentTransformForHighMag(manualAlignmentTransform);
                    }
                  }
                }
                else
                {
                  Assert.expect(false, "Unknown panel location!");
                }
              }
            }
            else
            {
              _performingRuntimeManualAlignment = false;
              _performingManualAlignmentVerification = false;
              _manualAlignmentComplete.setValue(true);
              // Khaw Chek Hau - XCR2181 - Able to run alignment starting from failed board
              subProgram.setRunAlignmentLearned(false);
              return;
            }
          }

          if(_performingRuntimeManualAlignment == false)
          {
            // Run the automatic alignment algorithm to check that we got a good manual alignment.
            _performingManualAlignmentVerification = true;
            autoAlignForManualAlignmentConfirmation(subProgram, testExecutionTimer);
          }
        }
        catch (Throwable throwable)
        {
          // Khaw Chek Hau - XCR2181 - Able to run alignment starting from failed board
          subProgram.setRunAlignmentLearned(false);
          _miscThrowableFromManualAlignment = throwable;
        }
        finally
        {
          _performingManualAlignmentVerification = false;
          _performingRuntimeManualAlignment = false;
          _manualAlignmentComplete.setValue(true);
          AlignmentInspectionEvent inspectionEvent = new AlignmentInspectionEvent(InspectionEventEnum.
                                                                                  MANUAL_ALIGNMENT_COMPLETED);
          inspectionEvent.setMagnificationType(subProgram.getMagnificationType());
          _inspectionEventObservable.sendEventInfo(inspectionEvent);
        }
      }
    };

    _manualAlignmentWorkerThread.invokeLater(manualAlignmentTask);
  }

  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  private void run2DManualAlignmentProcess(final TestSubProgram subProgram, final TestExecutionTimer testExecutionTimer)
  {
    Assert.expect(subProgram != null);
    Assert.expect(testExecutionTimer != null);

    _miscThrowableFromManualAlignment = null;

    Runnable manualAlignmentTask = new Runnable()
    {
      public void run()
      {
        try
        {         
          AlignmentInspectionEvent inspectionEvent = new AlignmentInspectionEvent(InspectionEventEnum.
                                                                                  MANUAL_2D_ALIGNMENT_STARTED);
          inspectionEvent.setMagnificationType(subProgram.getMagnificationType());
          inspectionEvent.setPanelLocationInSystem(subProgram.getPanelLocationInSystem());
          inspectionEvent.setTestSubProgramId(subProgram.getId());
          _inspectionEventObservable.sendEventInfo(inspectionEvent);
          {
            Collection<AlignmentGroup> alignmentGroupsForTestSubProgram = subProgram.getAlignmentGroups();
            for (int i = 0; i < alignmentGroupsForTestSubProgram.size(); ++i)
            {              
              _currentManualAlignmentXOffsetRelativeToPanelInNanoMeters = 0.0;
              _currentManualAlignmentYOffsetRelativeToPanelInNanoMeters = 0.0;
              AlignmentInspectionEvent alignmentInspectionEvent = getManualAlignmentInspectionEvent();
              _inspectionEventObservable.sendEventInfo(alignmentInspectionEvent);
              
              AlignmentInspectionEvent alignmentEvent = new AlignmentInspectionEvent(InspectionEventEnum.DISABLE_NEXT_BUTTON_BEFORE_IMAGE_CROPPED);
              alignmentEvent.setAlignmentGroup(alignmentInspectionEvent.getAlignmentGroup());
              alignmentEvent.setMagnificationType(alignmentInspectionEvent.getMagnificationType());
              _inspectionEventObservable.sendEventInfo(alignmentEvent);
              
              while (alignmentInspectionEvent.getAlignmentGroup().hasDraggedRegion() == false)
              {
                waitInMilliSeconds(500);
                
                if (_cancelManualAlignment)
                  break;
              }
              
              if (_cancelManualAlignment == false)
              {
                alignmentEvent = new AlignmentInspectionEvent(InspectionEventEnum.ENABLE_NEXT_BUTTON_AFTER_IMAGE_CROPPED);
                alignmentEvent.setMagnificationType(subProgram.getMagnificationType());
                _inspectionEventObservable.sendEventInfo(alignmentEvent);
                waitForUserInput(); //waiting for user to click on "Aligned" buttons
              }                

              if (_cancelManualAlignment == false)
              { 
                AlignmentGroup currentAlignmentGroup = alignmentInspectionEvent.getAlignmentGroup();
                currentAlignmentGroup.setCurrentManualAlignmentXOffsetRelativeToPanelInNanoMeters((int) _currentManualAlignmentXOffsetRelativeToPanelInNanoMeters);
                currentAlignmentGroup.setCurrentManualAlignmentYOffsetRelativeToPanelInNanoMeters((int) _currentManualAlignmentYOffsetRelativeToPanelInNanoMeters);

                alignmentEvent = new AlignmentInspectionEvent(InspectionEventEnum.DETERMINE_CROPPED_ALIGNMENT_REGION);
                alignmentEvent.setMagnificationType(subProgram.getMagnificationType());
                _inspectionEventObservable.sendEventInfo(alignmentEvent);

                alignmentEvent = new AlignmentInspectionEvent(InspectionEventEnum.RESET_ZOOM_FACTOR);
                alignmentEvent.setMagnificationType(subProgram.getMagnificationType());
                _inspectionEventObservable.sendEventInfo(alignmentEvent);

                waitInMilliSeconds(500); //delay for GUI zooming for alignment region

                int draggedImageMinX = currentAlignmentGroup.getDraggedExpectedMinX();
                int draggedImageMinY = currentAlignmentGroup.getDraggedExpectedMinY();
                int draggedImageWidth = currentAlignmentGroup.getDraggedWidth();
                int draggedImageHeight = currentAlignmentGroup.getDraggedHeight();
                
                PanelCoordinate expectedDraggedCentroidAlignmentCoordinate = new PanelCoordinate((int)Math.round(draggedImageMinX  + draggedImageWidth/2), 
                                                                         (int)Math.round(draggedImageMinY + draggedImageHeight/2));
                currentAlignmentGroup.setExpectedDraggedImageCoordinateInNanometers(expectedDraggedCentroidAlignmentCoordinate);
                                
                int locatedXCoordinateOnPanel =
                    (int)Math.round(expectedDraggedCentroidAlignmentCoordinate.getX() + _currentManualAlignmentXOffsetRelativeToPanelInNanoMeters);
                int locatedYCoordinateOnPanel =
                    (int)Math.round(expectedDraggedCentroidAlignmentCoordinate.getY() + _currentManualAlignmentYOffsetRelativeToPanelInNanoMeters);
                
                PanelCoordinate actualCentroidOfAlignmentGroup = new PanelCoordinate(locatedXCoordinateOnPanel,
                                                                                     locatedYCoordinateOnPanel);
                
                if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
                {
                  System.out.print("Manual Alignment -- Alignment Group " + currentAlignmentGroup.getName() +
                                   " expected centroid location (in panel coords): ");
                  System.out.print("( " + expectedDraggedCentroidAlignmentCoordinate.getX() + " , " + expectedDraggedCentroidAlignmentCoordinate.getY() +
                                   " )");
                  System.out.println();
                  System.out.print("Manual Alignment -- Alignment Group " + currentAlignmentGroup.getName() +
                                   " centroid offset (in panel coords): ");
                  System.out.print("( " + _currentManualAlignmentXOffsetRelativeToPanelInNanoMeters + " , " + _currentManualAlignmentYOffsetRelativeToPanelInNanoMeters +
                                   " )");
                  System.out.println();
                  System.out.print("Manual Alignment -- Alignment Group " + currentAlignmentGroup.getName() +
                                   " actual centroid location (in panel coords): ");
                  System.out.print("( " + actualCentroidOfAlignmentGroup.getX() + " , " +
                                   actualCentroidOfAlignmentGroup.getY() + " )");
                  System.out.println();
                }

                if(subProgram.isLowMagnification())
                  currentAlignmentGroup.setLocatedCentroidInNanometers(actualCentroidOfAlignmentGroup);
                else
                  currentAlignmentGroup.setLocatedCentroidInNanometersForHighMag(actualCentroidOfAlignmentGroup);

                // Make sure that the user didn't drag the alignment point unreasonably far away.
                verifyUserSpecifiedAlignmentOffsetIsWithinAcceptableLimits(subProgram, currentAlignmentGroup);
                verifyUserDragged2DAlignmentRegionOffsetIsWithinAcceptableLimits(subProgram, currentAlignmentGroup);
              }
              else
              {
                _performingManualAlignmentVerification = false;
                _performingRuntimeManualAlignment = false;
                _manualAlignmentComplete.setValue(true);
                // Khaw Chek Hau - XCR2181 - Able to run alignment starting from failed board
                subProgram.setRunAlignmentLearned(false);
                return;
              }
            }

            if (_cancelManualAlignment == false)
            {
              //create alignment regions and modify focus group
              _programGeneration = ProgramGeneration.getInstance();
              _programGeneration.create2DAlignmentRegions(subProgram, alignmentGroupsForTestSubProgram);
              
              // Calculate the initial manual transform.  We only want to preserve the translation components of the
              // transform at this point.
              Project project = subProgram.getTestProgram().getProject();
              Panel panel = project.getPanel();
              PanelSettings panelSettings = panel.getPanelSettings();
              java.awt.geom.AffineTransform manualAlignmentTransform = computeAlignmentTransformForTestSubProgram(subProgram);

              if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
              {
                java.awt.geom.AffineTransform defaultAlignmentTransform = subProgram.getDefaultCoreAlignmentTransform();
                System.out.println("Default transform matrix : " + defaultAlignmentTransform);
                System.out.println("Initial manual transform matrix : " + manualAlignmentTransform);
              }

              PanelLocationInSystemEnum panelLocationInSystem = subProgram.getPanelLocationInSystem();

              if( panelSettings.isPanelBasedAlignment() )
              {
                if (panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT))
                {
                  if(subProgram.isLowMagnification())
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      panelSettings.setLeftRuntimeManualAlignmentTransform(manualAlignmentTransform);
                    }
                    else
                    {
                      panelSettings.setLeftManualAlignmentTransform(manualAlignmentTransform);
                      panelSettings.setLeftRuntimeAlignmentTransform(manualAlignmentTransform);
                    }
                  }
                  else
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      panelSettings.setLeftRuntimeManualAlignmentTransformForHighMag(manualAlignmentTransform); 
                    }
                    else
                    {
                      panelSettings.setLeftManualAlignmentTransformForHighMag(manualAlignmentTransform);
                      panelSettings.setLeftRuntimeAlignmentTransformForHighMag(manualAlignmentTransform);  
                    }
                  }
                }
                else if (panelLocationInSystem.equals(PanelLocationInSystemEnum.RIGHT))
                {
                  if(subProgram.isLowMagnification())
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      panelSettings.setRightRuntimeManualAlignmentTransform(manualAlignmentTransform);
                    }
                    else
                    {
                      panelSettings.setRightManualAlignmentTransform(manualAlignmentTransform);
                      panelSettings.setRightRuntimeAlignmentTransform(manualAlignmentTransform);
                    }
                  }
                  else
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      panelSettings.setRightRuntimeManualAlignmentTransformForHighMag(manualAlignmentTransform);
                    }
                    else
                    {
                      panelSettings.setRightManualAlignmentTransformForHighMag(manualAlignmentTransform);
                      panelSettings.setRightRuntimeAlignmentTransformForHighMag(manualAlignmentTransform);
                    }
                  }
                }
                else
                {
                  Assert.expect(false, "Unknown panel location!");
                }
              }
              else
              {
                // Wei Chin added for individual Alignment
                if (panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT))
                {
                  if(subProgram.isLowMagnification())
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      subProgram.getBoard().getBoardSettings().setLeftRuntimeManualAlignmentTransform(manualAlignmentTransform);
                    }
                    else
                    {
                      subProgram.getBoard().getBoardSettings().setLeftManualAlignmentTransform(manualAlignmentTransform);
                      subProgram.getBoard().getBoardSettings().setLeftRuntimeAlignmentTransform(manualAlignmentTransform);
                    }
                  }
                  else
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      subProgram.getBoard().getBoardSettings().setLeftRuntimeManualAlignmentTransformForHighMag(manualAlignmentTransform);
                    }
                    else
                    {
                      subProgram.getBoard().getBoardSettings().setLeftManualAlignmentTransformForHighMag(manualAlignmentTransform);
                      subProgram.getBoard().getBoardSettings().setLeftRuntimeAlignmentTransformForHighMag(manualAlignmentTransform);
                    }
                  }
                }
                else if (panelLocationInSystem.equals(PanelLocationInSystemEnum.RIGHT))
                {
                  if(subProgram.isLowMagnification())
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      subProgram.getBoard().getBoardSettings().setRightRuntimeManualAlignmentTransform(manualAlignmentTransform);
                    }
                    else
                    {
                      subProgram.getBoard().getBoardSettings().setRightManualAlignmentTransform(manualAlignmentTransform);
                      subProgram.getBoard().getBoardSettings().setRightRuntimeAlignmentTransform(manualAlignmentTransform);
                    }
                  }
                  else
                  {
                    if(_performingRuntimeManualAlignment)
                    {
                      subProgram.getBoard().getBoardSettings().setRightRuntimeManualAlignmentTransformForHighMag(manualAlignmentTransform);
                    }
                    else
                    {
                      subProgram.getBoard().getBoardSettings().setRightManualAlignmentTransformForHighMag(manualAlignmentTransform);
                      subProgram.getBoard().getBoardSettings().setRightRuntimeAlignmentTransformForHighMag(manualAlignmentTransform);
                    }
                  }
                }
                else
                {
                  Assert.expect(false, "Unknown panel location!");
                }
              }
            }
            else
            {
              _performingRuntimeManualAlignment = false;
              _performingManualAlignmentVerification = false;
              _manualAlignmentComplete.setValue(true);
              // Khaw Chek Hau - XCR2181 - Able to run alignment starting from failed board
              subProgram.setRunAlignmentLearned(false);
              return;
            }
          }

          if(_performingRuntimeManualAlignment == false)
          {
            // Run the automatic alignment algorithm to check that we got a good manual alignment.
            _performingManualAlignmentVerification = true;
            autoAlignForManualAlignmentConfirmation(subProgram, testExecutionTimer);
          }
        }
        catch (Throwable throwable)
        {
          // Khaw Chek Hau - XCR2181 - Able to run alignment starting from failed board
          subProgram.setRunAlignmentLearned(false);
          _miscThrowableFromManualAlignment = throwable;
        }
        finally
        {
          _performingManualAlignmentVerification = false;
          _performingRuntimeManualAlignment = false;
          _manualAlignmentComplete.setValue(true);
          AlignmentInspectionEvent inspectionEvent = new AlignmentInspectionEvent(InspectionEventEnum.
                                                                                  MANUAL_2D_ALIGNMENT_COMPLETED);
          inspectionEvent.setMagnificationType(subProgram.getMagnificationType());
          _inspectionEventObservable.sendEventInfo(inspectionEvent);
        }
      }
    };

    _manualAlignmentWorkerThread.invokeLater(manualAlignmentTask);
  }
  
  /**
   * Qualifies the alignment groups in the specified TestSubProgram.  This consists of the following
   *
   * @author Roy Williams
   * @author Matt Wharton
   */
  public void qualifySelectedAlignmentPoints(TestSubProgram testSubProgram) throws AlignmentBusinessException
  {
    Assert.expect(testSubProgram != null);

    // Check to see if we have any alignment group violations.
    Collection<AlignmentGroup> insufficientCoverageAlignmentGroups = new HashSet<AlignmentGroup>();
    insufficientCoverageAlignmentGroups = checkTestSubProgramForSufficientAlignmentCoverageOfImageableArea(testSubProgram);
    Collection<AlignmentGroup> nonSingleSidedAlignmentGroups =
        checkTestSubProgramForAlignmentGroupsInSingleSidedArea(testSubProgram);
    Collection<AlignmentGroup> sparselyPopulatedAlignmentGroups =
        checkTestSubProgramForSufficientlyDenseAlignmentGroups(testSubProgram);
    Collection<AlignmentGroup> alignmentGroupsTooCloseToAreaArrays =
        checkTestSubProgramForAlignmentGroupProximityToAreaArrays(testSubProgram);
    Collection<AlignmentGroup> alignmentGroupsWithPcaps =
        checkTestSubProgramForAlignmentGroupsWithPcaps(testSubProgram);
    Collection<AlignmentGroup> alignmentGroupsWhichEncroachOnLeadFrames =
        checkTestSubProgramForAlignmentGroupsWhichEncroachOnLeadFrames(testSubProgram);

    // Now, if we got any problem alignment groups, we have to create an exception and throw it.
    if ((insufficientCoverageAlignmentGroups.isEmpty() == false) ||
        (nonSingleSidedAlignmentGroups.isEmpty() == false) ||
        (sparselyPopulatedAlignmentGroups.isEmpty() == false) ||
        (alignmentGroupsTooCloseToAreaArrays.isEmpty() == false) ||
        (alignmentGroupsWithPcaps.isEmpty() == false) ||
        (alignmentGroupsWhichEncroachOnLeadFrames.isEmpty() == false))
    {
      ProblematicAlignmentGroupsAlignmentBusinessException problematicGroupsException =
          createProblematicAlignmentGroupsException(testSubProgram,
                                                    insufficientCoverageAlignmentGroups,
                                                    nonSingleSidedAlignmentGroups,
                                                    sparselyPopulatedAlignmentGroups,
                                                    alignmentGroupsTooCloseToAreaArrays,
                                                    alignmentGroupsWithPcaps,
                                                    alignmentGroupsWhichEncroachOnLeadFrames);

      throw problematicGroupsException;
    }
  }

  /**
   * Checks the specified TestSubProgram to make sure that the triangle formed by its three alignment groups
   * covers a sufficient percentage of the TestSubProgram's total imageable area.
   *
   * @return a Collection of AlignmentGroups which violate the area check (note, this will either be empty or
   *         will contain all of the AlignmentGroups in the sub program.
   * @author Roy Williams
   * @author Matt Wharton
   */
  private Collection<AlignmentGroup> checkTestSubProgramForSufficientAlignmentCoverageOfImageableArea(TestSubProgram testSubProgram)
  {
    Assert.expect(testSubProgram != null);

    // Get imageable area of panel
    // We'll subtract off the width and height of an maximumAlignmentFeature because
    // the points (p0, p1, and p2 below) use the computed/expected centroid of the
    // pads in the alignment group.  By reducing the size of the imageable area, there
    // is less error for the computed percentage.
    PanelRectangle panelRectangle = testSubProgram.getInspectionRegionsBoundsInNanoMeters();
    double width  = panelRectangle.getWidth()  - ReconstructionRegion.getMaxAlignmentFeatureLengthInNanoMeters();
    double length = panelRectangle.getHeight() - ReconstructionRegion.getMaxAlignmentFeatureWidthInNanoMeters();
    double imageableAreaOfPanel = width * length;

    // Build a little triangle of the imageable alignmentGroups
    Collection<AlignmentGroup> alignmentGroups = testSubProgram.getAlignmentGroups();
    Assert.expect(alignmentGroups.size() == _NUMBER_OF_ALIGNMENT_GROUPS_PER_ALIGNMENT);
    AlignmentGroup alignmentGroup[] = alignmentGroups.toArray(new AlignmentGroup[0]);
    java.awt.geom.Point2D point0 = alignmentGroup[0].getExpectedCentroidInNanometers().getPoint2D();
    java.awt.geom.Point2D point1 = alignmentGroup[1].getExpectedCentroidInNanometers().getPoint2D();
    java.awt.geom.Point2D point2 = alignmentGroup[2].getExpectedCentroidInNanometers().getPoint2D();

    double distanceOpposite0 = java.awt.geom.Point2D.distance(point1.getX(), point1.getY(), point2.getX(), point2.getY());
    double distanceOpposite1 = java.awt.geom.Point2D.distance(point0.getX(), point0.getY(), point2.getX(), point2.getY());
    double distanceOpposite2 = java.awt.geom.Point2D.distance(point0.getX(), point0.getY(), point1.getX(), point1.getY());

    // Use Law of Cosine to figure out angle.
    double theta0 = MathUtil.lawOfCosines(distanceOpposite0, distanceOpposite1, distanceOpposite2);
    double theta1 = MathUtil.lawOfCosines(distanceOpposite1, distanceOpposite0, distanceOpposite2);
    double theta2 = MathUtil.lawOfCosines(distanceOpposite2, distanceOpposite0, distanceOpposite1);
    Assert.expect((theta0 + theta1 + theta2) > 179.9);   // must be 180 but have rounding error.
    Assert.expect((theta0 + theta1 + theta2) < 180.1);   // must be 180 but have rounding error.

    // Using abreviations for theta0 (t0), point0 (p0), and distanceOpposite0 (d0)
    //
    //                   d2
    // p0 *-------------------------------* p1
    //     \   t0                   t1   /
    //      \                           /
    //       \                         /
    //        \                       /
    //         \                     /
    //          \                   /
    //           \                 /
    //      d1    \               /d0
    //             \             /
    //              \           /
    //               \         /
    //                \       /
    //                 \ t2  /
    //                  \   /
    //                   \ /
    //                    * p2
    //
    //    A = 1/2 * base  * height
    double base   = distanceOpposite0;
    double height = distanceOpposite2 * Math.sin(Math.toRadians(theta1));
    double areaOfAlignmentRegion = 0.5 * base * height;

    // Compute the percentage and compare it to what is allowable from the config file.
    Collection<AlignmentGroup> problemAlignmentGroups = new HashSet<AlignmentGroup>();
    double percentageArea = (areaOfAlignmentRegion / imageableAreaOfPanel) * 100.d;
    double minimumPercentCoverageOfImageableAreaForAlignmentGroups = getMinimumPercentCoverageOfImageableAreaForAlignmentGroups();
    if (percentageArea < minimumPercentCoverageOfImageableAreaForAlignmentGroups)
    {
      problemAlignmentGroups.addAll(alignmentGroups);
    }

    return problemAlignmentGroups;
  }

  /**
   * Checks to see if the specified TestSubProgram contains any alignment regions whose bounding box is in a
   * single sided region of the board.
   *
   * @return a Collection of the AlignmentGroups which violate this rule.
   * @author Matt Wharton
   */
  private Collection<AlignmentGroup> checkTestSubProgramForAlignmentGroupsInSingleSidedArea(TestSubProgram testSubProgram)
  {
    Assert.expect(testSubProgram != null);

    Collection<AlignmentGroup> problemAlignmentGroups = new HashSet<AlignmentGroup>();
    TestProgram testProgram = testSubProgram.getTestProgram();
    Project project = testProgram.getProject();
    Panel panel = project.getPanel();

    // Expand our bounding box to include a border that's equal to
    // max([alignment uncertainty] + 0.5 * [panel thickness], 100 mils)
    final int ALIGNMENT_UNCERTAINTY_BORDER_IN_NANOMETERS = getAlignmentUncertaintyBorderInNanoMeters();
    int panelThicknessInNanoMeters = panel.getThicknessInNanometers();
    int boundingBoxBorderInNanometers =
        (int)Math.round((double)ALIGNMENT_UNCERTAINTY_BORDER_IN_NANOMETERS + (0.5d * (double)panelThicknessInNanoMeters));
    final int MAXIMUM_BORDER_FOR_SINGLE_SIDED_ALIGNMENT_REGION_TEST_IN_NANOMETERS =
        getMaximumBorderForSingleSidedAlignmentRegionTestInNanoMeters();
    boundingBoxBorderInNanometers = Math.min(boundingBoxBorderInNanometers,
                                             MAXIMUM_BORDER_FOR_SINGLE_SIDED_ALIGNMENT_REGION_TEST_IN_NANOMETERS);

    SurroundingPadUtil surroundingPadUtil = testProgram.getSurroundingPadUtil();
    for (ReconstructionRegion alignmentRegion : testSubProgram.getAlignmentRegions())
    {
      // We want to make sure the bounding box of the alignment features doesn't contain any features from the
      // OTHER side of the panel.
      boolean oppositeSideIsTopSide = (alignmentRegion.isTopSide() == false);
      PanelRectangle boundingBoxOfAlignmentFeaturesInNanos = alignmentRegion.getAlignmentFeatureBoundsInNanoMeters();
      PanelRectangle expandedBoundingBoxOfAlignmentFeaturesInNanos =
          new PanelRectangle(boundingBoxOfAlignmentFeaturesInNanos.getMinX() - boundingBoxBorderInNanometers,
                             boundingBoxOfAlignmentFeaturesInNanos.getMinY() - boundingBoxBorderInNanometers,
                             boundingBoxOfAlignmentFeaturesInNanos.getWidth() + (2 * boundingBoxBorderInNanometers),
                             boundingBoxOfAlignmentFeaturesInNanos.getHeight() + (2 * boundingBoxBorderInNanometers));

      Set<Pad> padsInBoundingBoxOnOppositeSide =
          surroundingPadUtil.getIntersectingPadsInRectangle(expandedBoundingBoxOfAlignmentFeaturesInNanos,
                                                            oppositeSideIsTopSide);
      if (padsInBoundingBoxOnOppositeSide.isEmpty() == false)
      {
        problemAlignmentGroups.add(alignmentRegion.getAlignmentGroup());
      }
    }

    return problemAlignmentGroups;
  }

  /**
   * Checks to see if the specified TestSubProgram contains any alignment regions whose bounding box isn't
   * sufficiently covered by alignment features (or context alignment features).  In other words, this check
   * makes sure the alignment regions are sufficiently 'dense'.
   *
   * @return a Collection of the AlignmentGroups which violate this rule.
   * @author Matt Wharton
   */
  private Collection<AlignmentGroup> checkTestSubProgramForSufficientlyDenseAlignmentGroups(TestSubProgram testSubProgram)
  {
    Assert.expect(testSubProgram != null);

    Collection<AlignmentGroup> problemAlignmentGroups = new HashSet<AlignmentGroup>();
    for (ReconstructionRegion alignmentRegion : testSubProgram.getAlignmentRegions())
    {
      Collection<Feature> alignmentFeaturesAndContextFeatures = alignmentRegion.getAlignmentFeatures();
      alignmentFeaturesAndContextFeatures.addAll(alignmentRegion.getAlignmentContextFeatures());

      // Figure out how much area is occupied by alignment features and context features.
      long areaOccupiedByAlignmentFeaturesInNanos = 0L;
      for (Feature alignmentFeature : alignmentFeaturesAndContextFeatures)
      {
        PanelRectangle alignmentFeatureRectangleInNanos =
            alignmentFeature.getRegionRectangleRelativeToPanelOriginInNanoMeters();
        long areaOfAlignmentFeatureInNanos =
            (long)alignmentFeatureRectangleInNanos.getWidth() * (long)alignmentFeatureRectangleInNanos.getHeight();
        areaOccupiedByAlignmentFeaturesInNanos += areaOfAlignmentFeatureInNanos;
      }

      // Figure out the total area of the alignment features bounding box.
      PanelRectangle boundingBoxOfAlignmentFeaturesInNanos = alignmentRegion.getAlignmentFeatureBoundsInNanoMeters();
      long areaOfAlignmentFeaturesBoundingBoxInNanos =
          (long)boundingBoxOfAlignmentFeaturesInNanos.getWidth() * (long)boundingBoxOfAlignmentFeaturesInNanos.getHeight();

      // Check to make sure the area occupied by the features and context features is a sufficient percentage of the
      // total bounding box area.
      double percentageOfBoundingBoxAreaOccupiedByFeatures =
          ((double)areaOccupiedByAlignmentFeaturesInNanos / (double)areaOfAlignmentFeaturesBoundingBoxInNanos) * 100.0;
      final double MINIMUM_ALIGNMENT_REGION_DENSITY_PERCENTAGE = getMinimumAlignmentRegionDensityPercentage();
      if (percentageOfBoundingBoxAreaOccupiedByFeatures < MINIMUM_ALIGNMENT_REGION_DENSITY_PERCENTAGE)
      {
        problemAlignmentGroups.add(alignmentRegion.getAlignmentGroup());
      }
    }

    return problemAlignmentGroups;
  }

  /**
   * Checks the specified TestSubProgram for alignment groups whose bounding box is in too close of a proximity
   * to area array.  This involves the following two checks:
   *   1.  Checking to see if the alignment region bounding box area is occupied more than 10% by BGAs on the same
   *       side of the board.
   *   2.  Checking to see if the alignment region bounding box plus a 200 mil proximity margin contains any
   *       of the following joint types on the same or opposite side: CGA, PTH, Pressfit, SMT Connector.
   *
   * @return a Collection of AlignmentGroups which violate this rule.
   * @author Matt Wharton
   */
  private Collection<AlignmentGroup> checkTestSubProgramForAlignmentGroupProximityToAreaArrays(TestSubProgram testSubProgram)
  {
    Assert.expect(testSubProgram != null);

    Collection<JointTypeEnum> jointTypesWhichShouldBeNowhereNearOrUnderneathBoundingBox =
        Arrays.asList(JointTypeEnum.CGA,
                      JointTypeEnum.PRESSFIT,
                      JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                      JointTypeEnum.THROUGH_HOLE,
                      JointTypeEnum.OVAL_THROUGH_HOLE); //Siew Yeng - XCR-3318 - Oval PTH
    Collection<JointTypeEnum> jointTypesThatShouldOccupyMinimalBoundingBoxArea =
        Arrays.asList(JointTypeEnum.CHIP_SCALE_PACKAGE,
                      JointTypeEnum.COLLAPSABLE_BGA,
                      JointTypeEnum.NON_COLLAPSABLE_BGA,
                      JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);

    TestProgram testProgram = testSubProgram.getTestProgram();
    SurroundingPadUtil surroundingPadUtil = testProgram.getSurroundingPadUtil();
    Collection<AlignmentGroup> problemAlignmentGroups = new HashSet<AlignmentGroup>();
    for (ReconstructionRegion alignmentRegion : testSubProgram.getAlignmentRegions())
    {
      // First, let's check to see if the alignment region bounding box is overly occupied with BGAs.
      PanelRectangle boundingBoxOfAlignmentFeaturesInNanos = alignmentRegion.getAlignmentFeatureBoundsInNanoMeters();
      long boundingBoxAreaInNanos =
          (long)boundingBoxOfAlignmentFeaturesInNanos.getWidth() * (long)boundingBoxOfAlignmentFeaturesInNanos.getHeight();
      long boundingBoxAreaOccupiedByBGAsInNanos = 0L;
      Collection<Pad> padsInAlignmentRegionBoundingBoxOnSameSideOfBoard =
          surroundingPadUtil.getIntersectingPadsInRectangle(boundingBoxOfAlignmentFeaturesInNanos, alignmentRegion.isTopSide());
      for (Pad padInBoundingBoxOnSameSideOfBoard : padsInAlignmentRegionBoundingBoxOnSameSideOfBoard)
      {
        JointTypeEnum jointTypeEnum = padInBoundingBoxOnSameSideOfBoard.getJointTypeEnum();
        if (jointTypesThatShouldOccupyMinimalBoundingBoxArea.contains(jointTypeEnum))
        {
          PanelRectangle padRectangleInNanos =
              padInBoundingBoxOnSameSideOfBoard.getPanelRectangleWithLowerLeftOriginInNanoMeters();
          long padAreaInNanos = (long)padRectangleInNanos.getWidth() * (long)padRectangleInNanos.getHeight();
          boundingBoxAreaOccupiedByBGAsInNanos += padAreaInNanos;
        }
      }
      double percentageOfBoundingBoxAreaOccupiedByBGAs =
          ((double)boundingBoxAreaOccupiedByBGAsInNanos / (double)boundingBoxAreaInNanos) * 100.d;
      final double MAXIMUM_ALIGNMENT_REGION_PERCENTAGE_OCCUPIED_BY_BGAS = getMaximumAlignmentRegionPercentageOccupiedByBGAs();
      if (percentageOfBoundingBoxAreaOccupiedByBGAs > MAXIMUM_ALIGNMENT_REGION_PERCENTAGE_OCCUPIED_BY_BGAS)
      {
        problemAlignmentGroups.add(alignmentRegion.getAlignmentGroup());
        // We're already violating the rule about minimal BGA area occupation.  No sense in doing the
        // "near and underneath" checks.
        continue;
      }

      // Next, let's make sure there are no CGA, PTH, Pressfit, or SMT Connector joints anywhere near or underneath
      // the alignment region bounding box.
      // "Near" in this context is defined to be a 200 mil margin around the alignment region bounding box.
      final int ALIGNMENT_REGION_AREA_ARRAY_PROXIMITY_MARGIN_IN_NANOS = getAlignmentRegionAreaArrayProximityMarginInNanoMeters();
      PanelRectangle extendedBoundingBoxOfAlignmentFeaturesInNanos =
          new PanelRectangle(boundingBoxOfAlignmentFeaturesInNanos.getMinX() - ALIGNMENT_REGION_AREA_ARRAY_PROXIMITY_MARGIN_IN_NANOS,
                             boundingBoxOfAlignmentFeaturesInNanos.getMinY() - ALIGNMENT_REGION_AREA_ARRAY_PROXIMITY_MARGIN_IN_NANOS,
                             boundingBoxOfAlignmentFeaturesInNanos.getWidth() + (2 * ALIGNMENT_REGION_AREA_ARRAY_PROXIMITY_MARGIN_IN_NANOS),
                             boundingBoxOfAlignmentFeaturesInNanos.getHeight() + (2 * ALIGNMENT_REGION_AREA_ARRAY_PROXIMITY_MARGIN_IN_NANOS));
      Collection<Pad> padsNearOrUnderneathAlignmentRegionBoundingBox =
          surroundingPadUtil.getIntersectingPadsInRectangle(extendedBoundingBoxOfAlignmentFeaturesInNanos);
      for (Pad padNearOrUnderneathAlignmentRegionBoundingBox : padsNearOrUnderneathAlignmentRegionBoundingBox)
      {
        JointTypeEnum jointTypeEnum = padNearOrUnderneathAlignmentRegionBoundingBox.getJointTypeEnum();
        if (jointTypesWhichShouldBeNowhereNearOrUnderneathBoundingBox.contains(jointTypeEnum))
        {
          // We found a problematic joint type in proximity of our bounding box.  Mark the alignment group as
          // problematic and move on to the next one.
          problemAlignmentGroups.add(alignmentRegion.getAlignmentGroup());
          break;
        }
      }
    }

    return problemAlignmentGroups;
  }

  /**
   * Checks the specified TestSubProgram for alignment groups whose bounding box contains PCAPs on the
   * same side of the board.
   *
   * @return a Collection of AlignmentGroups which violate this rule.
   * @author Matt Wharton
   */
  private Collection<AlignmentGroup> checkTestSubProgramForAlignmentGroupsWithPcaps(TestSubProgram testSubProgram)
  {
    Assert.expect(testSubProgram != null);

    TestProgram testProgram = testSubProgram.getTestProgram();
    SurroundingPadUtil surroundingPadUtil = testProgram.getSurroundingPadUtil();
    Collection<AlignmentGroup> problemAlignmentGroups = new HashSet<AlignmentGroup>();
    for (ReconstructionRegion alignmentRegion : testSubProgram.getAlignmentRegions())
    {
      PanelRectangle boundingBoxOfAlignmentFeaturesInNanos = alignmentRegion.getAlignmentFeatureBoundsInNanoMeters();
      Collection<Pad> padsInBoundingBoxOnSameBoardSide =
          surroundingPadUtil.getIntersectingPadsInRectangle(boundingBoxOfAlignmentFeaturesInNanos,
                                                            alignmentRegion.isTopSide());
      for (Pad padInBoundingBoxOnSameBoardSide : padsInBoundingBoxOnSameBoardSide)
      {
        JointTypeEnum jointTypeEnum = padInBoundingBoxOnSameBoardSide.getJointTypeEnum();
        if (jointTypeEnum.equals(JointTypeEnum.POLARIZED_CAP))
        {
          // Oops, found a PCAP.  Make the alignment group as problematic and move on to the next.
          problemAlignmentGroups.add(alignmentRegion.getAlignmentGroup());
          break;
        }
      }
    }

    return problemAlignmentGroups;
  }

  /**
   * Checks the specified TestSubProgram for AlignmentGroups which encroach on componet lead frames.
   *
   * @return a Collection of AlignmentGroups which violate this rule.
   * @author Matt Wharton
   */
  private Collection<AlignmentGroup> checkTestSubProgramForAlignmentGroupsWhichEncroachOnLeadFrames(TestSubProgram testSubProgram)
  {
    Assert.expect(testSubProgram != null);

    Collection<JointTypeEnum> jointTypesWithPotentialLeadFrameIssues =
        Arrays.asList(JointTypeEnum.GULLWING,
                      JointTypeEnum.JLEAD,
                      JointTypeEnum.LEADLESS,
                      JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                      JointTypeEnum.SMALL_OUTLINE_LEADLESS);

    TestProgram testProgram = testSubProgram.getTestProgram();
    SurroundingPadUtil surroundingPadUtil = testProgram.getSurroundingPadUtil();
    Collection<AlignmentGroup> problemAlignmentGroups = new HashSet<AlignmentGroup>();
    for (ReconstructionRegion alignmentRegion : testSubProgram.getAlignmentRegions())
    {
      // First, let's figure out if we have any components contained in the bounding box that could
      // present a problem.
      PanelRectangle boundingBoxOfAlignmentFeaturesInNanos = alignmentRegion.getAlignmentFeatureBoundsInNanoMeters();
      Collection<Pad> padsInBoundingBoxOnSameBoardSide =
          surroundingPadUtil.getIntersectingPadsInRectangle(boundingBoxOfAlignmentFeaturesInNanos,
                                                            alignmentRegion.isTopSide());
      Collection<Component> potentiallyProblematicComponents = new HashSet<Component>();
      for (Pad padInBoundingBoxOnSameBoardSide : padsInBoundingBoxOnSameBoardSide)
      {
        JointTypeEnum jointTypeEnum = padInBoundingBoxOnSameBoardSide.getJointTypeEnum();
        if (jointTypesWithPotentialLeadFrameIssues.contains(jointTypeEnum))
        {
          // We can ignore components with two pins or less.
          Component potentiallyProblematicComponent = padInBoundingBoxOnSameBoardSide.getComponent();
          if (potentiallyProblematicComponent.getPads().size() > 2)
          {
            // Found a pad with a potential lead frame issue.  Flag its component.
            potentiallyProblematicComponents.add(potentiallyProblematicComponent);
          }
        }
      }

      // Now, for each of our potentially problematic components, we want to make sure that the component's bounds
      // occupy no more than 10% of the alignment feature bounding box.
      long boundingBoxAreaInNanos =
          (long)boundingBoxOfAlignmentFeaturesInNanos.getWidth() * (long)boundingBoxOfAlignmentFeaturesInNanos.getHeight();
      final double MAXIMUM_ALIGNMENT_FEATURE_BOUNDING_BOX_PERCENTAGE_OCCUPIED_BY_PROBLEMATIC_LEAD_FRAMES =
          getMaximumAlignmentFeatureBoundingBoxPercentageOccupiedByProblematicLeadFrames();
      for (Component potentiallyProblematicComponent : potentiallyProblematicComponents)
      {
        // Calculate the intersection of the component rectangle and the alignment feature bounding box.
        java.awt.geom.Rectangle2D componentRectangleInNanos =
            potentiallyProblematicComponent.getShapeBoundedByPadsRelativeToPanelInNanoMeters().getBounds2D();
        java.awt.geom.Rectangle2D intersectionOfBoundingBoxAndComponent =
            componentRectangleInNanos.createIntersection(boundingBoxOfAlignmentFeaturesInNanos.getRectangle2D());

        // Check to see if the area of the intersection exceeds our limit.
        double areaOfIntersectionRectangle =
            intersectionOfBoundingBoxAndComponent.getWidth() * intersectionOfBoundingBoxAndComponent.getHeight();
        double percentageAreaOfBoundingBoxOccupiedByProblematicLeadFrame =
            (areaOfIntersectionRectangle / (double)boundingBoxAreaInNanos) * 100.d;
        if (percentageAreaOfBoundingBoxOccupiedByProblematicLeadFrame >
            MAXIMUM_ALIGNMENT_FEATURE_BOUNDING_BOX_PERCENTAGE_OCCUPIED_BY_PROBLEMATIC_LEAD_FRAMES)
        {
          problemAlignmentGroups.add(alignmentRegion.getAlignmentGroup());
          // Already marked this alignment group as problematic.  Let's move on to the next.
          break;
        }
      }
    }

    return problemAlignmentGroups;
  }

  /**
   * @author Matt Wharton
   */
  private ProblematicAlignmentGroupsAlignmentBusinessException createProblematicAlignmentGroupsException(
      TestSubProgram testSubProgram,
      Collection<AlignmentGroup> insufficientCoverageAreaAlignmentGroups,
      Collection<AlignmentGroup> nonSingleSidedAlignmentGroups,
      Collection<AlignmentGroup> sparselyPopulatedAlignmentGroups,
      Collection<AlignmentGroup> alignmentGroupsTooCloseToAreaArrays,
      Collection<AlignmentGroup> alignmentGroupsWithPcaps,
      Collection<AlignmentGroup> alignmentGroupsWhichEncroachOnLeadFrames)
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(insufficientCoverageAreaAlignmentGroups != null);
    Assert.expect(nonSingleSidedAlignmentGroups != null);
    Assert.expect(sparselyPopulatedAlignmentGroups != null);
    Assert.expect(alignmentGroupsTooCloseToAreaArrays != null);
    Assert.expect(alignmentGroupsWithPcaps != null);
    Assert.expect(alignmentGroupsWhichEncroachOnLeadFrames != null);

    // Build up our itemized list of warnings.
    StringBuilder itemizedIssuesStringBuilder = new StringBuilder();
    itemizedIssuesStringBuilder.append("<html>");
    itemizedIssuesStringBuilder.append("<ol>");
    itemizedIssuesStringBuilder.append(buildAlignmentGroupWarningHtmlListItem(insufficientCoverageAreaAlignmentGroups,
                                                                              "BS_ALIGNMENT_GROUPS_COVER_INSUFFICIENT_AREA_KEY"));
    itemizedIssuesStringBuilder.append(buildAlignmentGroupWarningHtmlListItem(nonSingleSidedAlignmentGroups,
                                                                              "BS_ALIGNMENT_GROUPS_IN_NON_SINGLE_SIDED_AREA_KEY"));
    itemizedIssuesStringBuilder.append(buildAlignmentGroupWarningHtmlListItem(sparselyPopulatedAlignmentGroups,
                                                                              "BS_ALIGNMENT_GROUPS_SPARSELY_POPULATED_KEY"));
    itemizedIssuesStringBuilder.append(buildAlignmentGroupWarningHtmlListItem(alignmentGroupsTooCloseToAreaArrays,
                                                                              "BS_ALIGNMENT_GROUPS_TOO_NEAR_AREA_ARRAY_PACKAGES_KEY"));
    itemizedIssuesStringBuilder.append(buildAlignmentGroupWarningHtmlListItem(alignmentGroupsWithPcaps,
                                                                              "BS_ALIGNMENT_GROUPS_CONTAINING_PCAPS_KEY"));
    itemizedIssuesStringBuilder.append(buildAlignmentGroupWarningHtmlListItem(alignmentGroupsWhichEncroachOnLeadFrames,
                                                                              "BS_ALIGNMENT_GROUPS_ENCROACHING_ON_LEAD_FRAMES_KEY"));
    itemizedIssuesStringBuilder.append("</ol>");
    itemizedIssuesStringBuilder.append("</html>");

    // Create our uber warning exception.
    ProblematicAlignmentGroupsAlignmentBusinessException problematicGroupsException =
        new ProblematicAlignmentGroupsAlignmentBusinessException(testSubProgram, itemizedIssuesStringBuilder.toString());

    return problematicGroupsException;
  }

  /**
   * @author Matt Wharton
   */
  private String buildAlignmentGroupWarningHtmlListItem(Collection<AlignmentGroup> problemAlignmentGroups,
                                                        String warningLocalizationKey)
  {
    Assert.expect(problemAlignmentGroups != null);
    Assert.expect(warningLocalizationKey != null);

    String warningListItem = "";
    if (problemAlignmentGroups.isEmpty() == false)
    {
      List<String> affectedAlignmentGroupNames = new ArrayList<String>();
      for (AlignmentGroup alignmentGroup : problemAlignmentGroups)
      {
        affectedAlignmentGroupNames.add(alignmentGroup.getName());
      }
      Collections.sort(affectedAlignmentGroupNames);

      LocalizedString warningLocalizedString =
          new LocalizedString(warningLocalizationKey, new Object[]{StringUtil.join(affectedAlignmentGroupNames, ", ")});
      String warningLine = StringLocalizer.keyToString(warningLocalizedString);

      warningListItem += "<li>";
      warningListItem += warningLine;
      warningListItem += "</li>";
    }

    return warningListItem;
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   * @edited hee-jihn.chuah XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
   */
  public void runManualAlignment(TestSubProgram subProgram, TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(subProgram != null);
    Assert.expect(testExecutionTimer != null);
    _cancelManualAlignment = false;
    boolean isUsing2DAlignment = subProgram.getTestProgram().getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod();

    // Khaw Chek Hau - XCR2181 - Able to run alignment starting from failed board
    if (subProgram.isRunAlignmentLearned())
    {
      _performingManualAlignmentVerification = false;
      _performingRuntimeManualAlignment = false;
      _manualAlignmentComplete.setValue(true);
      AlignmentInspectionEvent inspectionEvent = new AlignmentInspectionEvent(InspectionEventEnum.
                                                                              MANUAL_ALIGNMENT_COMPLETED);
      inspectionEvent.setMagnificationType(subProgram.getMagnificationType());
      _inspectionEventObservable.sendEventInfo(inspectionEvent);

      return;
    }

    _manualAlignmentComplete.setValue(false);
    
    _performingRuntimeManualAlignment = subProgram.isUsingRuntimeManualAlignmentTransform();

    PanelSettings panelSettings = subProgram.getTestProgram().getProject().getPanel().getPanelSettings();

    // Keep track of the original left and right transform values.
    java.awt.geom.AffineTransform originalManualAlignmentTransform = subProgram.getCoreManualAlignmentTransform();
    java.awt.geom.AffineTransform originalRuntimeAlignmentTransform = subProgram.getCoreRuntimeAlignmentTransform();

    
    // Kick off the manual alignment process.
    initializeAlignmentProcess(subProgram);
    // Khaw Chek Hau - XCR2181 - Able to run alignment starting from failed board
    subProgram.setRunAlignmentLearned(true);
    
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    if (isUsing2DAlignment)
      run2DManualAlignmentProcess(subProgram, testExecutionTimer);
    else
      runManualAlignmentProcess(subProgram, testExecutionTimer);

    Collection<AlignmentGroup> alignmentGroups = subProgram.getAlignmentGroups();

    // for each alignment group, send off the AlignmentInspectionEvent
    for (AlignmentGroup alignmentGroup : alignmentGroups)
    {
      AlignmentInspectionEvent inspectionEvent;
      if (isUsing2DAlignment)
        inspectionEvent = new AlignmentInspectionEvent(InspectionEventEnum.WAITING_FOR_USER_TO_CROP_IMAGE);
      else
        inspectionEvent = new AlignmentInspectionEvent(InspectionEventEnum.WAITING_FOR_USER_TO_ALIGN_IMAGE);
      
      inspectionEvent.setAlignmentGroup(alignmentGroup);
      inspectionEvent.setMagnificationType(subProgram.getMagnificationType());
      Assert.expect(inspectionEvent != null);

      // send off the event
      _manualAlignmentEvents.add(inspectionEvent);
    }

    // Wait until alignment is complete.
    try
    {
      _manualAlignmentComplete.waitUntilTrue();
    }
    catch (InterruptedException iex)
    {
      // Do nothing ...
    }

    // Check to see if we encountered any problems during the manual alignment process.
    //Siew Yeng - XCR1757 - do not need to check if it is running runtime manual alignment
    if (_miscThrowableFromManualAlignment != null)
    {
      // Clear any manual alignment events.
      _manualAlignmentEvents.clear();
      
      //Khaw Chek Hau - XCR2464: Software Skip Alignment for Right Side Of Long Panel After Realign In Production
      if (subProgram.isUsingRuntimeManualAlignmentTransform() == false)
      {
        if(panelSettings.isPanelBasedAlignment())
        {
          // Remove any previous alignment transforms that were set.
  //        panelSettings.clearAllAlignmentTransforms();
          //Khaw Chek Hau - XCR2619: The initial alignment can't change state to "completed" when doing manual alignment on mixed mag recipe
          if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
          {
            if (subProgram.isHighMagnification())
              panelSettings.clearRightAllAlignmentTransformForHighMag();  
            else    
              panelSettings.clearRightAllAlignmentTransformForLowMag(); 
          }
          else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
          {
            if (subProgram.isHighMagnification())
              panelSettings.clearLeftAllAlignmentTransformForHighMag();   
            else 
              panelSettings.clearLeftAllAlignmentTransformForLowMag();  
          }

          // If the original transforms were not simply the default, go ahead and restore them.
          if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
          {
            if (originalManualAlignmentTransform.isIdentity() == false)
            {
              if(subProgram.isLowMagnification())
                panelSettings.setLeftManualAlignmentTransform(originalManualAlignmentTransform);
              else
                panelSettings.setLeftManualAlignmentTransformForHighMag(originalManualAlignmentTransform);
            }

            if (originalRuntimeAlignmentTransform.isIdentity() == false)
            {
              if(subProgram.isLowMagnification())
                panelSettings.setLeftRuntimeAlignmentTransform(originalRuntimeAlignmentTransform);
              else
                panelSettings.setLeftRuntimeAlignmentTransformForHighMag(originalRuntimeAlignmentTransform);            
            }
          }
          else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
          {
            if (originalManualAlignmentTransform.isIdentity() == false)
            {
              if(subProgram.isLowMagnification())
                panelSettings.setRightManualAlignmentTransform(originalManualAlignmentTransform);
              else
                panelSettings.setRightManualAlignmentTransformForHighMag(originalManualAlignmentTransform);
            }

            if (originalRuntimeAlignmentTransform.isIdentity() == false)
            {
              if(subProgram.isLowMagnification())
                panelSettings.setRightRuntimeAlignmentTransform(originalRuntimeAlignmentTransform);
              else
                panelSettings.setRightRuntimeAlignmentTransformForHighMag(originalRuntimeAlignmentTransform);
            }
          }
          else
            Assert.expect(false);
        }
        else
        {
          // Wei Chin added for individual Board Alignment
          // Remove any previous alignment transforms that were set.
          //Khaw Chek Hau - XCR2619: The initial alignment can't change state to "completed" when doing manual alignment on mixed mag recipe
          if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
          {
            if (subProgram.isHighMagnification())
              subProgram.getBoard().getBoardSettings().clearRightAllAlignmentTransformsForHighMag();
            else
              subProgram.getBoard().getBoardSettings().clearRightAllAlignmentTransformsForLowMag();
          }
          else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
          {
            if (subProgram.isHighMagnification())
              subProgram.getBoard().getBoardSettings().clearLeftAllAlignmentTransformsForHighMag();
            else
              subProgram.getBoard().getBoardSettings().clearLeftAllAlignmentTransformsForLowMag(); 
          }

          // If the original transforms were not simply the default, go ahead and restore them.
          if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
          {
            if (originalManualAlignmentTransform.isIdentity() == false)
            {
              if(subProgram.isLowMagnification())
                subProgram.getBoard().getBoardSettings().setLeftManualAlignmentTransform(originalManualAlignmentTransform);
              else
                subProgram.getBoard().getBoardSettings().setLeftManualAlignmentTransformForHighMag(originalManualAlignmentTransform);
            }

            if (originalRuntimeAlignmentTransform.isIdentity() == false)
            {
              if(subProgram.isLowMagnification())
                subProgram.getBoard().getBoardSettings().setLeftRuntimeAlignmentTransform(originalRuntimeAlignmentTransform);
              else
                subProgram.getBoard().getBoardSettings().setLeftRuntimeAlignmentTransformForHighMag(originalRuntimeAlignmentTransform);
            }
          }
          else if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
          {
            if (originalManualAlignmentTransform.isIdentity() == false)
            {
              if(subProgram.isLowMagnification())
                subProgram.getBoard().getBoardSettings().setRightManualAlignmentTransform(originalManualAlignmentTransform);
              else
                subProgram.getBoard().getBoardSettings().setRightManualAlignmentTransformForHighMag(originalManualAlignmentTransform);
            }

            if (originalRuntimeAlignmentTransform.isIdentity() == false)
            {
              if(subProgram.isLowMagnification())
                subProgram.getBoard().getBoardSettings().setRightRuntimeAlignmentTransform(originalRuntimeAlignmentTransform);
              else
                subProgram.getBoard().getBoardSettings().setRightRuntimeAlignmentTransformForHighMag(originalRuntimeAlignmentTransform);
            }
          }
          else
            Assert.expect(false);
        }
      }
    }

    checkForException();
  }

  /**
   * @author George A. David
   */
  private void checkForException() throws XrayTesterException
  {
    // If we encountered an XrayTesterException, rethrow it.
    if (_miscThrowableFromManualAlignment instanceof XrayTesterException)
    {
      throw (XrayTesterException)_miscThrowableFromManualAlignment;
    }
    // If there was a RuntimeException, also rethrow that.
    else if (_miscThrowableFromManualAlignment instanceof RuntimeException)
    {
      throw (RuntimeException)_miscThrowableFromManualAlignment;
    }
    // If there were any Throwables that are unaccounted for, let's assert.
    else if (_miscThrowableFromManualAlignment != null)
    {
      Assert.logException(_miscThrowableFromManualAlignment);
    }
  }

  /**
   * @author George A. David
   */
  private void waitForUserInput()
  {
    _waitingForManualAlignmentUserInput.setValue(true);
    try
    {
      _waitingForManualAlignmentUserInput.waitUntilFalse();
    }
    catch (InterruptedException ex)
    {
      // Do nothing ...
    }
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   */
  public void imageManuallyAligned(double xOffsetInNanoMeters, double yOffsetInNanoMeters)
  {
    _currentManualAlignmentXOffsetRelativeToPanelInNanoMeters = xOffsetInNanoMeters;
    _currentManualAlignmentYOffsetRelativeToPanelInNanoMeters = yOffsetInNanoMeters;
    _waitingForManualAlignmentUserInput.setValue(false);
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void setIsRunTimeAlignmentNeededAppliedInVerificationImages(boolean isRunTimeAlignmentNeededAppliedInVerificationImages)
  {
     _isRunTimeAlignmentNeededAppliedInVerificationImages = isRunTimeAlignmentNeededAppliedInVerificationImages;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public boolean isRuntimeAlignmentNeededAppliedForVerificationImages()
  {
     return _isRunTimeAlignmentNeededAppliedInVerificationImages;  
  }

  /**
   * @author George A. David
   */
  public void cancelManualAlignment()
  {
    _cancelManualAlignment = true;
    _manualAlignmentEvents.clear();
    _waitingForManualAlignmentUserInput.setValue(false);
  }
  
  /**
   * @author hee-jihn.chuah XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
   */
  public boolean isCancelManualAlignment()
  {
    return _cancelManualAlignment;
  }
  
  /**
   * @author hee-jihn.chuah XCR-2468: Initial alignment is complete after 1st region of alignment group is complete
   */
  public void setIsManualAlignmentInitiallyComplete(boolean isManualAlignmentInitiallyComplete)
  {
    _isManualAlignmentInitiallyComplete = isManualAlignmentInitiallyComplete;
  }  

  /**
   * Saves the specified exception from a worker thread if there isn't already another pending exception.
   *
   * @author Matt Wharton
   */
  private synchronized void handleWorkerThreadException(Throwable ex)
  {
    Assert.expect(ex != null);

    if (_miscThrowableFromManualAlignment == null)
    {
      _miscThrowableFromManualAlignment = ex;
    }
  }

  /**
   * @author George A. David
   * @author Matt Wharton
   */
  public void autoAlignForVerificationImageGeneration(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    _miscThrowableFromManualAlignment = null;
    autoAlign(testProgram, new TestExecutionTimer());
    checkForException();
  }

  /**
   * @author George A. David
   */
  private void autoAlign(final TestProgram testProgram,
                         final TestExecutionTimer testExecutionTimer)
  {
    // Keeps track of whether we're in the process of acquiring images.
    final BooleanLock imageAcquisitionInProgress = new BooleanLock(false);

    try
    {
      // Initialize the alignment data structures.
      initializeAlignmentProcess(testProgram);

      // Initialize the IAE.
      _imageAcquisitionEngine.initialize();

      // Kick off acquisition of the alignment images on a worker thread.
      _alignmentImageAcquisitionWorker.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            // Mark that we've started image acquisition.
            imageAcquisitionInProgress.setValue(true);

            // Start acquiring just the alignment images.
            _imageAcquisitionEngine.acquireAlignmentImages(testProgram, testExecutionTimer);
          }
          catch (Throwable ex)
          {
            handleWorkerThreadException(ex);
            _manualAlignmentComplete.setValue(true);
          }
          finally
          {
            // Mark that image acquisition is complete.
            imageAcquisitionInProgress.setValue(false);
          }
        }
      });

      List<ReconstructionRegion> alignmentRegions = testProgram.getAlignmentRegions();
      for (int i = 0; i < alignmentRegions.size(); ++i)
      {
        // Align on the reconstructed alignment images as they come up.
        BooleanRef noImagesAvailable = new BooleanRef(false);
        ReconstructedImages reconstructedAlignmentImages = _imageAcquisitionEngine.getReconstructedImages(noImagesAvailable);

        if (noImagesAvailable.getValue() == false)
        {
          //Khaw Chek Hau - XCR2285: 2D Alignment on v810
          if (testProgram.getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod())
            alignOn2DImage(reconstructedAlignmentImages, false);
          else
            alignOnImage(reconstructedAlignmentImages, false);
          
          reconstructedAlignmentImages.decrementReferenceCount();
        }
      }
    }
    catch (Throwable ex)
    {
      // Send an abort request to the IAE.
      try
      {
        _imageAcquisitionEngine.abort();
      }
      catch(XrayTesterException xex)
      {
        // we'd like to display this, but we already
        // have an exception to deal with. So let's just
        // print it out.
        xex.printStackTrace();
      }

      // Mark that we got an exception on a worker thread.
      handleWorkerThreadException(ex);
    }
    finally
    {
      // Block until image acquisition is complete.
      try
      {
        imageAcquisitionInProgress.waitUntilFalse();
      }
      catch (InterruptedException iex)
      {
        // Do nothing ...
      }
    }
  }

  /**
   * @author George A. David
   */
  private void autoAlign(final TestSubProgram subProgram,
                         final TestExecutionTimer testExecutionTimer)
  {
    // Keeps track of whether we're in the process of acquiring images.
    final BooleanLock imageAcquisitionInProgress = new BooleanLock(false);

    try
    {
      boolean isSimMode = XrayTester.getInstance().isSimulationModeOn();
      
//      ImageAcquisitionEngine.changeMagnification(subProgram.getMagnificationType());

      // Initialize the alignment data structures.
      initializeAlignmentProcess(subProgram);

      // Initialize the IAE.
      _imageAcquisitionEngine.initialize();

      // Kick off acquisition of the alignment images on a worker thread.
      _alignmentImageAcquisitionWorker.invokeLater(new Runnable()
      {
        public void run()
        {
          try
          {
            // Mark that we've started image acquisition.
            imageAcquisitionInProgress.setValue(true);

            // Start acquiring just the alignment images.
            _imageAcquisitionEngine.acquireAlignmentImages(subProgram, testExecutionTimer); 
          }
          catch (Throwable ex)
          {
            handleWorkerThreadException(ex);
            _manualAlignmentComplete.setValue(true);
            // Khaw Chek Hau - XCR2181 - Able to run alignment starting from failed board
            subProgram.setRunAlignmentLearned(false);
          }
          finally
          {
            // Mark that image acquisition is complete.
            imageAcquisitionInProgress.setValue(false);
          }
        }
      });
      
      Collection<ReconstructionRegion> alignmentRegions = subProgram.getAlignmentRegions();  
      for (int i = 0; i < alignmentRegions.size(); ++i)
      {
        // Align on the reconstructed alignment images as they come up.
        BooleanRef noImagesAvailable = new BooleanRef(false);
        ReconstructedImages reconstructedAlignmentImages = _imageAcquisitionEngine.getReconstructedImages(noImagesAvailable);

        if (noImagesAvailable.getValue() == false)
        {
          //Khaw Chek Hau - XCR2285: 2D Alignment on v810
          if (subProgram.getTestProgram().getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod())
            alignOn2DImage(reconstructedAlignmentImages, false); 
          else
            alignOnImage(reconstructedAlignmentImages, false); 
            
          reconstructedAlignmentImages.decrementReferenceCount();
        }
      }
    }
    catch (Throwable ex)
    {
      // Send an abort request to the IAE.
      try
      {
        _imageAcquisitionEngine.abort();
      }
      catch(XrayTesterException xex)
      {
        // we'd like to display this, but we already
        // have an exception to deal with. So let's just
        // print it out.
        xex.printStackTrace();
      }

      // Mark that we got an exception on a worker thread.
      handleWorkerThreadException(ex);
    }
    finally
    {
      // Block until image acquisition is complete.
      try
      {
        imageAcquisitionInProgress.waitUntilFalse();
      }
      catch (InterruptedException iex)
      {
        // Do nothing ...
      }
    }
  }

  /**
   * Runs an "automatic" alignment to verify that manual alignment was successful.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  private void autoAlignForManualAlignmentConfirmation(final TestSubProgram subProgram,
                                                       final TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(subProgram != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(_performingManualAlignmentVerification);
    Assert.expect(_imageAcquisitionEngine != null);
    Assert.expect(_inspectionEventObservable != null);

    // Fire an event that we're starting manual alignment verification.
    AlignmentInspectionEvent manualAlignmentVerificationStartedEvent =
        new AlignmentInspectionEvent(InspectionEventEnum.MANUAL_ALIGNMENT_VERIFICATION_STARTED);
    manualAlignmentVerificationStartedEvent.setMagnificationType(subProgram.getMagnificationType());
    _inspectionEventObservable.sendEventInfo(manualAlignmentVerificationStartedEvent);
    try
    {
      autoAlign(subProgram, testExecutionTimer);
    }
    finally
    {
      // Fire an event that we're done with manual alignment verification.
      AlignmentInspectionEvent manualAlignmentVerificationCompletedEvent =
          new AlignmentInspectionEvent(InspectionEventEnum.MANUAL_ALIGNMENT_VERIFICATION_COMPLETED);
      manualAlignmentVerificationCompletedEvent.setMagnificationType(subProgram.getMagnificationType());
      _inspectionEventObservable.sendEventInfo(manualAlignmentVerificationCompletedEvent);
    }
  }

  /**
   * Runs an "automatic" alignment to verify that manual alignment was successful, without going through manual alignment process.
   * Please note that this function will not update alignment transform and will throw any exception found.
   *
   * @author Seng-Yew Lim
   * @author Ying-Huan.Chu
   */
  public void autoAlignForAlignmentConfirmation(final TestSubProgram subProgram,
                                                       final TestExecutionTimer testExecutionTimer) throws XrayTesterException
  {
    Assert.expect(subProgram != null);
    Assert.expect(testExecutionTimer != null);
    Assert.expect(_imageAcquisitionEngine != null);
    Assert.expect(_inspectionEventObservable != null);

    _miscThrowableFromManualAlignment = null;
    
    // Fire an event that we're starting manual alignment verification.
    AlignmentInspectionEvent manualAlignmentVerificationStartedEvent =
        new AlignmentInspectionEvent(InspectionEventEnum.MANUAL_ALIGNMENT_VERIFICATION_STARTED);
    manualAlignmentVerificationStartedEvent.setMagnificationType(subProgram.getMagnificationType());
    _inspectionEventObservable.sendEventInfo(manualAlignmentVerificationStartedEvent);
    try
    {
      autoAlign(subProgram, testExecutionTimer);
    }
    finally
    {
      // Fire an event that we're done with manual alignment verification.
      AlignmentInspectionEvent manualAlignmentVerificationCompletedEvent =
          new AlignmentInspectionEvent(InspectionEventEnum.MANUAL_ALIGNMENT_VERIFICATION_COMPLETED);
      manualAlignmentVerificationCompletedEvent.setMagnificationType(subProgram.getMagnificationType());
      _inspectionEventObservable.sendEventInfo(manualAlignmentVerificationCompletedEvent);
    }
    checkForException();
  }

  /**
   * Aligns on the specified image.  Also checks to see if all the alignment groups have been aligned
   * on and computes the alignment matrix if so.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public synchronized void alignOnImage(ReconstructedImages reconstructedImages,
                                        boolean sendTransformToReconstruction) throws XrayTesterException
  {
    Assert.expect(reconstructedImages != null);
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
//    Assert.expect(reconstructedImages.getNumberOfSlices() == 1); // can't handle multiple slices
    Assert.expect(_imageAcquisitionEngine != null);

    ReconstructionRegion alignmentRegion = null;

    try
    {      
      // Make sure the image has any necessary rotation corrections applied to it.
      reconstructedImages.ensureAllSlicesAreReadyForInspection();
     
      alignmentRegion = reconstructedImages.getReconstructionRegion();
      AlignmentGroup alignmentGroup = alignmentRegion.getAlignmentGroup();
      TestSubProgram testSubProgram = alignmentRegion.getTestSubProgram();
      TestProgram testProgram = testSubProgram.getTestProgram();
      Project project = testProgram.getProject();
      Panel panel = project.getPanel();
      PanelSettings panelSettings = panel.getPanelSettings();
      ReconstructedSlice reconstructedAlignmentSlice = reconstructedImages.getFirstSlice();
      Image alignmentImage = reconstructedAlignmentSlice.getOrthogonalImage();
      Image uncorrectedAlignmentImage = reconstructedAlignmentSlice.getImage();
      ImageCoordinate expectedAlignmentTemplateLocationInImage = null;
      // Save the raw alignment image.
      if (_performingManualAlignmentVerification)
      {
        saveRawManualAlignmentImage(uncorrectedAlignmentImage, alignmentRegion);
      }
      else
      {
        saveRawAlignmentImage(uncorrectedAlignmentImage, alignmentRegion);
      }

      // We'd better have at least one fiducial and/or alignment pad defined.
      List<JointInspectionData> jointInspectionDataObjects = alignmentRegion.getJointInspectionDataList();
      List<FiducialInspectionData> fiducialInspectionDataObjects = alignmentRegion.getFiducialInspectionDataList();
      Assert.expect((jointInspectionDataObjects.isEmpty() == false) || (fiducialInspectionDataObjects.isEmpty() == false));

      // Generate our alignment template.
      List<RegionOfInterest> alignmentFeaturesInTemplateImage = new LinkedList<RegionOfInterest>();
      List<RegionOfInterest> alignmentContextFeaturesInTemplateImage = new LinkedList<RegionOfInterest>();
      ImageCoordinate actualAlignmentTemplateLocationInImage = null;
      // Locate the alignment features in the image.
      DoubleRef matchQualityRef = new DoubleRef();
      Image newAlignmentImage = null; 
//      if(alignmentGroup.isUseShapeMatching())
//      {
//        List<java.awt.Shape> alignmentTemplateShapeInPixelsList = new LinkedList<java.awt.Shape>();
//        Map<AlignmentExcludedAreaEnum,java.awt.Shape> alignmentExcludedAreaInTemplateRoiList = new HashMap<AlignmentExcludedAreaEnum,java.awt.Shape>();
//        Pair<RegionOfInterest,Image> alignmentTemplateRegionAndImage =
//                generateAlignmentTemplateImageForShapeMatchingAlgorithm(
//                alignmentImage,
//                alignmentRegion,
//                alignmentFeaturesInTemplateImage,
//                alignmentContextFeaturesInTemplateImage,
//                alignmentTemplateShapeInPixelsList,
//                alignmentExcludedAreaInTemplateRoiList);
//
//        RegionOfInterest alignmentTemplateRoi = alignmentTemplateRegionAndImage.getFirst();
//        expectedAlignmentTemplateLocationInImage = new ImageCoordinate(alignmentTemplateRoi.getMinX(),alignmentTemplateRoi.getMinY());
//
//      String imagesDir = Directory.getAlignmentLogDir();
//      String imageFileName = "Anthony SHAPE_MATCH_Alignment_Template_Image_" + System.currentTimeMillis() + ".png";
//      XrayImageIoUtil.savePngImage(alignmentImage.getBufferedImage(),
//              imagesDir + File.separator + imageFileName);
//
//
//        actualAlignmentTemplateLocationInImage = locatePatternByShapeMatchingAlgorithm(alignmentImage.getBufferedImage(),
//                alignmentTemplateShapeInPixelsList,alignmentRegion.getRegionRectangleInPixels().getRectangle2D().getBounds(),
//                matchQualityRef,alignmentExcludedAreaInTemplateRoiList);
//
//        //System.out.println("actualAlignmentTemplateLocationInImage:"+actualAlignmentTemplateLocationInImage);
//        //System.out.println("expectedAlignmentTemplateLocationInImage: "+expectedAlignmentTemplateLocationInImage);
//      }
//      else
      {

        //generateAlignmentTemplateImage
        Pair<RegionOfInterest,Image> alignmentTemplateRegionAndImage =
          generateAlignmentTemplateImage(alignmentImage,
                                         alignmentRegion,
                                         alignmentFeaturesInTemplateImage,
                                         alignmentContextFeaturesInTemplateImage, alignmentGroup.isUseShapeMatching());
      RegionOfInterest alignmentTemplateRoi = alignmentTemplateRegionAndImage.getFirst();
        expectedAlignmentTemplateLocationInImage = new ImageCoordinate(alignmentTemplateRoi.getMinX(),
                                                                                     alignmentTemplateRoi.getMinY());
      Image alignmentTemplateImage = alignmentTemplateRegionAndImage.getSecond();
      
      // JMH - following block of code is for debugging an IPP out of memory error during
      // alignment, which has been occurring frequantely at Motoral Penang.  It should not
      // be left in the main release, and can be deleted once this problem is resolved.
      /***
      boolean isSecondPortionOfLongPanel =
          testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT);
      XrayImageIoUtil.saveFullPrecisionImage(alignmentImage,
                                             FileName.getRawAlignmentFullPrecisionImageFullPath(
                                                testProgram.getProject().getName(),
                                                alignmentGroup.getName(),
                                                isSecondPortionOfLongPanel));
      XrayImageIoUtil.saveFullPrecisionImage(alignmentTemplateImage,
                                             FileName.getAlignmentTemplateFullPrecisionImageFullPath(
                                                testProgram.getProject().getName(),
                                                alignmentGroup.getName(),
                                                isSecondPortionOfLongPanel));
      ***/
      if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
      {
        long timeStampInMillis = System.currentTimeMillis();
        boolean isSecondPortionOfLongPanel =
            testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT);
        XrayImageIoUtil.savePngImage(alignmentImage,
                                               FileName.getRawAlignmentImageFullPath(
                                                  testProgram.getProject().getName(),
                                                  alignmentGroup.getName(),
                                                  isSecondPortionOfLongPanel,
                                                  timeStampInMillis));
        // Preprocess image before saving to see the don't care, light and dark mask value.
        Image templateImage = new Image(alignmentTemplateImage);        
        for (int iRow=0; iRow < templateImage.getHeight(); iRow++)
        {
          for (int iCol=0; iCol < templateImage.getWidth(); iCol++)
          {
            templateImage.setPixelValue(iCol, iRow, (templateImage.getPixelValue(iCol, iRow)+1)*127.0f);
          }
        }        
        XrayImageIoUtil.savePngImage(templateImage,
                                               FileName.getTemplateAlignmentImageFullPath(
                                                  testProgram.getProject().getName(),
                                                  alignmentGroup.getName(),
                                                  isSecondPortionOfLongPanel,
                                                  timeStampInMillis));
        templateImage.decrementReferenceCount();
        // Can I assume that this is already available?
        String sourceFile = FileName.getAlignmentVerificationImageFullPath(testProgram.getProject().getName(),
                                                                           alignmentGroup.getName(),
                                                                           isSecondPortionOfLongPanel);
        if (FileUtilAxi.exists(sourceFile))
        {
          String destinationFile = FileName.getAlignmentImageFullPath(testProgram.getProject().getName(),
                                                                      alignmentGroup.getName(),
                                                                      isSecondPortionOfLongPanel,
                                                                      timeStampInMillis);

          FileUtilAxi.copy(sourceFile, destinationFile);
        }
      }
      // Locate the alignment features in the image.
      matchQualityRef = new DoubleRef();
      //ImageCoordinate actualAlignmentTemplateLocationInImage;

      //sham, 2012-05-14, Alignment Enhancement
      /*following block of code is for debugging the online failed alignment image in offline by load image manually.
        try
        {
          String file ="";
          if(alignmentRegion.getJointInspectionDataList().get(0).getPad().getComponent().getReferenceDesignator().equals("U1"))
            file = "C:\\U1.png";
          else if(alignmentRegion.getJointInspectionDataList().get(0).getPad().getComponent().getReferenceDesignator().equals("U5"))
            file = "C:\\U5.png";
          else if(alignmentRegion.getJointInspectionDataList().get(0).getPad().getComponent().getReferenceDesignator().equals("U15"))
            file = "C:\\U15.png";

          BufferedImage imageBuffer = null;
          imageBuffer = ImageIO.read(new File(file));
          Image loadedAlignmentImage = Image.createFloatImageFromBufferedImage(imageBuffer);
          alignmentImage = loadedAlignmentImage;
        }
        catch (IOException ex)
        {
          //continous
      }*/
        if(alignmentGroup.isUseShapeMatching())
        {
            //newAlignmentImage = ShapeMatching.getInstance().extractSauvolaImage(alignmentImage.getBufferedImage(),alignmentFeaturesInTemplateImage);
            newAlignmentImage = ShapeMatching.getInstance().extractLAABImage(alignmentImage.getBufferedImage(),alignmentFeaturesInTemplateImage);

            if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
            {
                long timeStampInMillis = System.currentTimeMillis();
                boolean isSecondPortionOfLongPanel =
                    testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT);
                XrayImageIoUtil.savePngImage(newAlignmentImage,
                                                       FileName.getRawAlignmentImageFullPath(
                                                          "LAAB_"+testProgram.getProject().getName(),
                                                          alignmentGroup.getName(),
                                                          isSecondPortionOfLongPanel,
                                                          timeStampInMillis));
            }

            actualAlignmentTemplateLocationInImage = locatePattern(newAlignmentImage,
                                                                                 alignmentTemplateImage,
                                                                                 matchQualityRef);
            //sourceShapeListWithBinaryImage.getSecond().decrementReferenceCount();
      }
      else
      {             
          actualAlignmentTemplateLocationInImage = locatePattern(alignmentImage,
                  alignmentTemplateImage,
                  matchQualityRef);
          alignmentTemplateImage.decrementReferenceCount();
      }

      //sham, 2012-05-14, Alignment Enhancement
      //save alignment image to alignment log for debug purpose
      if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
      {
	    String imagesDir = Directory.getAlignmentLogDir();
	    String imageFileName = String.valueOf(System.currentTimeMillis()) + "_Raw_Alignment_Image_" + alignmentRegion.getName() + ".png";
	    XrayImageIoUtil.savePngImage(alignmentImage.getBufferedImage(),
                                 imagesDir + File.separator + imageFileName);
      }
     }
      // Send up an event indicating where we located the alignment features in the image.
      postAlignedOnImageEvent(alignmentImage,
                              alignmentRegion,
                              alignmentFeaturesInTemplateImage,
                              alignmentContextFeaturesInTemplateImage,
                              actualAlignmentTemplateLocationInImage);

      // Measure the gray levels for this alignment region.
      Pair<Double, Double> grayLevels = measureAlignmentRegionGrayLevels(alignmentImage,
                                                                         alignmentRegion,
                                                                         actualAlignmentTemplateLocationInImage,
                                                                         alignmentFeaturesInTemplateImage);
      double foregroundGrayLevel = grayLevels.getFirst();
      double backgroundGrayLevel = grayLevels.getSecond();
      double deltaGrayLevel = Math.abs(backgroundGrayLevel - foregroundGrayLevel);

      boolean matchQualityIsAcceptable = true;
      boolean grayLevelsAreAcceptable = true;
      double matchQuality = matchQualityRef.getValue();
      if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
      {
        System.out.println("Alignment group " + alignmentGroup.getName() + " match quality: " + matchQuality);
      }

      final double MATCH_QUALITY_ABSOLUTE_LOWER_LIMIT = 0.2;
      if (_performingManualAlignmentVerification)
      {
        // Do an absolute check to make sure match quality is something reasonable.
        if (MathUtil.fuzzyGreaterThanOrEquals(matchQuality, MATCH_QUALITY_ABSOLUTE_LOWER_LIMIT))
        {
          if(alignmentGroup.isMatchQualityAcceptable(matchQuality))
          {
            alignmentGroup.setMatchQuality(matchQuality);
            matchQualityIsAcceptable = true;
          }
          else
          {
            matchQualityIsAcceptable = false;
            throw new AlignmentAlgorithmFailedToLocatePadsBusinessException(testSubProgram, alignmentGroup, FileName.getAlignmentVerificationImageFullPath(project.getName(),
                                                                        alignmentRegion.getAlignmentGroup().getName(),
                                                                        testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT)));
          }
        }
        else
        {
          matchQualityIsAcceptable = false;
        }

        // Set the "expected" gray levels for the alignment group.
        if(testSubProgram.isLowMagnification())
        {
          alignmentGroup.setForegroundGrayLevel(foregroundGrayLevel);
          alignmentGroup.setBackgroundGrayLevel(backgroundGrayLevel);
        }
        else
        {
          alignmentGroup.setForegroundGrayLevelInHighMag(foregroundGrayLevel);
          alignmentGroup.setBackgroundGrayLevelInHighMag(backgroundGrayLevel);
        }
        grayLevelsAreAcceptable = true;
      }
      // Bypass match quality & graylevel check if it's in hardware simulation mode and not unit testing
      else if (XrayTester.getInstance().isSimulationModeOn() && (UnitTest.unitTesting() == false))
      {
        matchQualityIsAcceptable = true;
        grayLevelsAreAcceptable = true;
      }
      else
      {
        // Get the learned match quality.
        Assert.expect(alignmentGroup.hasMatchQuality());
        double learnedMatchQuality = alignmentGroup.getMatchQuality();

        final double MATCH_QUALITY_TOLERANCE = 0.25;
        double matchQualityLowerLimit = Math.max(learnedMatchQuality - MATCH_QUALITY_TOLERANCE,
                                                 MATCH_QUALITY_ABSOLUTE_LOWER_LIMIT);
        if (MathUtil.fuzzyGreaterThanOrEquals(matchQuality, matchQualityLowerLimit))
        {
          matchQualityIsAcceptable = true;
        }
        else
        {
          matchQualityIsAcceptable = false;
        }
        
        //Siew Yeng - XCR1757
        if(testSubProgram.isUsingRuntimeManualAlignmentTransform())
        {
          if (MathUtil.fuzzyGreaterThanOrEquals(matchQuality, MATCH_QUALITY_ABSOLUTE_LOWER_LIMIT))
          {
            matchQualityIsAcceptable = true;
          }
          else
          {
            matchQualityIsAcceptable = false;
          }
        }
        
        if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
        {
          System.out.println("Alignment group " + alignmentGroup.getName() + " minimum match quality: " + matchQualityLowerLimit);
        }

        // Check to see if the measured gray levels are within acceptable limits from what we learned at Initial Alignment.
        final double MAX_ALLOWABLE_BACKGROUND_GRAY_LEVEL_ERROR = getMaximumAlignmentBackgroundGrayLevelError();
        final double MAX_ALLOWABLE_DELTA_GRAY_LEVEL_ERROR_FACTOR = getMaximumAlignmentDeltaGrayLevelErrorFactor();
        double expectedForegroundGrayLevel = 0;
        double expectedBackgroundGrayLevel = 0;
        if(testSubProgram.isLowMagnification())
        {
          expectedForegroundGrayLevel = alignmentGroup.getForegroundGrayLevel();
          expectedBackgroundGrayLevel = alignmentGroup.getBackgroundGrayLevel();
        }
        else
        {
          expectedForegroundGrayLevel = alignmentGroup.getForegroundGrayLevelInHighMag();
          expectedBackgroundGrayLevel = alignmentGroup.getBackgroundGrayLevelInHighMag();
        }
        
        //Siew Yeng - XCR1757
        if(testSubProgram.isUsingRuntimeManualAlignmentTransform())
        {
          expectedForegroundGrayLevel = foregroundGrayLevel;
          expectedBackgroundGrayLevel = backgroundGrayLevel;
        }
        
        double expectedDeltaGrayLevel = Math.max(expectedBackgroundGrayLevel - expectedForegroundGrayLevel, 0d);
        if(!alignmentGroup.hasDarkPadsWithLightBackground())
        {
          expectedDeltaGrayLevel = Math.max(expectedForegroundGrayLevel - expectedBackgroundGrayLevel , 0d);
        }

        double backgroundGrayLevelError = Math.abs(backgroundGrayLevel - expectedBackgroundGrayLevel);
        if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
        {
          System.out.println("Alignment group " + alignmentGroup.getName() + " foregroundGrayLevel: " + foregroundGrayLevel);
          System.out.println("Alignment group " + alignmentGroup.getName() + " backgroundGrayLevel: " + backgroundGrayLevel);
          System.out.println("Alignment group " + alignmentGroup.getName() + " expectedForegroundGrayLevel: " + expectedForegroundGrayLevel);
          System.out.println("Alignment group " + alignmentGroup.getName() + " expectedBackgroundGrayLevel: " + expectedBackgroundGrayLevel);
        }
        // Modified by Seng Yew on 14-Apr-2014
        // Whenever generate precision image set, save the learned foreground & background graylevel values.
        if (_imageAcquisitionEngine.isGeneratingPrecisionImageSet() == true)
        {
          // Set the "expected" gray levels for the alignment group.
          // This will trigger dirty flag of the recipe and will need to save recipe later.
          if (testSubProgram.isLowMagnification())
          {
            alignmentGroup.setForegroundGrayLevel(foregroundGrayLevel);
            alignmentGroup.setBackgroundGrayLevel(backgroundGrayLevel);
          }
          else
          {
            alignmentGroup.setForegroundGrayLevelInHighMag(foregroundGrayLevel);
            alignmentGroup.setBackgroundGrayLevelInHighMag(backgroundGrayLevel);
          }
          grayLevelsAreAcceptable = true;
          _alignmentTransformSettingsWriter.write(project);
        }
        else
        {
          if ((backgroundGrayLevelError <= MAX_ALLOWABLE_BACKGROUND_GRAY_LEVEL_ERROR)
                  && (deltaGrayLevel >= (expectedDeltaGrayLevel / MAX_ALLOWABLE_DELTA_GRAY_LEVEL_ERROR_FACTOR))
                  && (deltaGrayLevel <= (expectedDeltaGrayLevel * MAX_ALLOWABLE_DELTA_GRAY_LEVEL_ERROR_FACTOR)))
          {
            grayLevelsAreAcceptable = true;
          }
          else
          {
            if (!alignmentGroup.isUseShapeMatching())
            {
              grayLevelsAreAcceptable = false;
            }
          }
        }
      }
      if (matchQualityIsAcceptable && grayLevelsAreAcceptable)
      {
        // Get the actual located alignment point in system fiducial coordinates.
        // First, we need to calculate the expected lock location offset from actual (in pixels).
        ImageCoordinate lockLocationOffsetInPixels = new ImageCoordinate(
            actualAlignmentTemplateLocationInImage.getX() - expectedAlignmentTemplateLocationInImage.getX(),
            actualAlignmentTemplateLocationInImage.getY() - expectedAlignmentTemplateLocationInImage.getY());
                
        // Next, we'll convert the offset to panel coordinates.
        double NANOMETERS_PER_PIXEL;
        if(testSubProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
          NANOMETERS_PER_PIXEL = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
        else
          NANOMETERS_PER_PIXEL = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
        java.awt.geom.AffineTransform imageSpaceToPanelSpaceTransform =
            java.awt.geom.AffineTransform.getScaleInstance(NANOMETERS_PER_PIXEL, -NANOMETERS_PER_PIXEL);
        java.awt.geom.Point2D transformedPoint = imageSpaceToPanelSpaceTransform.transform(lockLocationOffsetInPixels.getPoint2D(), null);
        PanelCoordinate lockLocationOffsetInNanometers = new PanelCoordinate((int)Math.round(transformedPoint.getX()),
                                                                             (int)Math.round(transformedPoint.getY()));

        // Apply the offset to the expected panel coordinate (as per the user specified manual transform)
        // of the alignment group.
        java.awt.geom.Point2D originalExpectedAlignmentGroupLocationPoint = alignmentGroup.getExpectedCentroidInNanometers().getPoint2D();
        java.awt.geom.AffineTransform userDefinedManualAlignmentTransform = testSubProgram.getCoreManualAlignmentTransform();
        java.awt.geom.Point2D correctedExpectedAlignmentGroupLocationPoint =
          userDefinedManualAlignmentTransform.transform(originalExpectedAlignmentGroupLocationPoint, null);
        PanelCoordinate correctedExpectedAlignmentGroupPanelCoordinate =
          new PanelCoordinate((int)Math.round(correctedExpectedAlignmentGroupLocationPoint.getX()),
                              (int)Math.round(correctedExpectedAlignmentGroupLocationPoint.getY()));
        PanelCoordinate measuredAlignmentGroupPanelCoordinate =
            correctedExpectedAlignmentGroupPanelCoordinate.plus(lockLocationOffsetInNanometers);

        if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
        {
          System.out.print("Automatic Alignment -- Alignment Group " + alignmentGroup.getName() +
                           " expected centroid location (in panel coords): ");
          System.out.print("( " + correctedExpectedAlignmentGroupLocationPoint.getX() + " , " + correctedExpectedAlignmentGroupLocationPoint.getY() +
                           " )");
          System.out.println();
          System.out.print("Automatic Alignment -- Alignment Group " + alignmentGroup.getName() +
                           " centroid offset (in panel coords): ");
          System.out.print("( " + lockLocationOffsetInNanometers.getX() + " , " + lockLocationOffsetInNanometers.getY() +
                           " )");
          System.out.println();
          System.out.print("Automatic Alignment -- Alignment Group " + alignmentGroup.getName() +
                           " actual centroid location (in panel coords): ");
          System.out.print("( " + measuredAlignmentGroupPanelCoordinate.getX() + " , " + measuredAlignmentGroupPanelCoordinate.getY() +
                           " )");
          System.out.println();
        }

        // Store the located coordinate to the AlignmentGroup.
        if(testSubProgram.isLowMagnification())
          alignmentGroup.setLocatedCentroidInNanometers(measuredAlignmentGroupPanelCoordinate);
        else
          alignmentGroup.setLocatedCentroidInNanometersForHighMag(measuredAlignmentGroupPanelCoordinate);

        Collection<AlignmentGroup> alignmentGroups = testSubProgram.getAlignmentGroups();
        
        boolean isAllAlignmentGroupsHaveBeenLocated = false;
        if(testSubProgram.isLowMagnification())
          isAllAlignmentGroupsHaveBeenLocated = allAlignmentGroupsHaveBeenLocated(alignmentGroups);
        else
          isAllAlignmentGroupsHaveBeenLocated = allAlignmentGroupsHaveBeenLocatedForHighMag(alignmentGroups);
        
        if (isAllAlignmentGroupsHaveBeenLocated)
        {
          // Compute the alignment transform and validate that it's within acceptable limits.
          java.awt.geom.AffineTransform alignmentTransform = computeAlignmentTransformForTestSubProgram(testSubProgram);
          if(panelSettings.isBypassAlignmentTranformWithinAcceptableLimit() == false)
          {
            verifyAlignmentTransformIsWithinAcceptableLimits(testSubProgram, alignmentTransform);
          }
          else
          {
            try
            {
              verifyAlignmentTransformIsWithinAcceptableLimits(testSubProgram, alignmentTransform);
            }
            catch(AlignmentAlgorithmResultOutOfBoundsBusinessException aex)
            {
              alignmentTransform = testSubProgram.getCoreManualAlignmentTransform();
            }
          }
            

          if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
          {
            System.out.println("Automatic alignment transform matrix : " + alignmentTransform);
          }

          // Update the stored alignment transform(s).
          PanelLocationInSystemEnum panelLocationInSystem = testSubProgram.getPanelLocationInSystem();

          if(panelSettings.isPanelBasedAlignment())
          {
            if (panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT))
            {
              // Only update the manual transform if we're performing manual alignment verification.
              if (_performingManualAlignmentVerification)
              {
                if(testSubProgram.isLowMagnification())
                  panelSettings.setLeftManualAlignmentTransform(alignmentTransform);
                else
                  panelSettings.setLeftManualAlignmentTransformForHighMag(alignmentTransform);
              }

              // Always update the runtime transform.
              //Siew Yeng - XCR1757 - do not need to update if the subprogram is using runtime manual alignment transform
              if(testSubProgram.isLowMagnification())
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                  panelSettings.setLeftRuntimeAlignmentTransform(alignmentTransform);
              }
              else
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                  panelSettings.setLeftRuntimeAlignmentTransformForHighMag(alignmentTransform);
              }
            }
            else if (panelLocationInSystem.equals(PanelLocationInSystemEnum.RIGHT))
            {
              // Only update the manual transform if we're performing manual alignment verification.
              if (_performingManualAlignmentVerification)
              {
                if(testSubProgram.isLowMagnification())
                  panelSettings.setRightManualAlignmentTransform(alignmentTransform);
                else
                  panelSettings.setRightManualAlignmentTransformForHighMag(alignmentTransform);
              }

              // Always update the runtime transform.
              //Siew Yeng - XCR1757 - do not need to update if the subprogram is using runtime manual alignment transform
              if(testSubProgram.isLowMagnification())
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                  panelSettings.setRightRuntimeAlignmentTransform(alignmentTransform);
              }
              else
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                  panelSettings.setRightRuntimeAlignmentTransformForHighMag(alignmentTransform);
              }
            }
            else
            {
              Assert.expect(false, "Unknown panel location!");
            }
          }
          else
          {
            // wei chin added for individual alignment
            if (panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT))
            {
              // Only update the manual transform if we're performing manual alignment verification.
              if (_performingManualAlignmentVerification)
              {
                if(testSubProgram.isLowMagnification())
                  testSubProgram.getBoard().getBoardSettings().setLeftManualAlignmentTransform(alignmentTransform);
                else
                  testSubProgram.getBoard().getBoardSettings().setLeftManualAlignmentTransformForHighMag(alignmentTransform);
              }

              // Always update the runtime transform.
              //Siew Yeng - XCR1757 - do not need to update if the subprogram is using runtime manual alignment transform
              if(testSubProgram.isLowMagnification())
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                  testSubProgram.getBoard().getBoardSettings().setLeftRuntimeAlignmentTransform(alignmentTransform);
              }
              else
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                  testSubProgram.getBoard().getBoardSettings().setLeftRuntimeAlignmentTransformForHighMag(alignmentTransform);
              }
            }
            else if (panelLocationInSystem.equals(PanelLocationInSystemEnum.RIGHT))
            {
              // Only update the manual transform if we're performing manual alignment verification.
              if (_performingManualAlignmentVerification)
              {
                if(testSubProgram.isLowMagnification())
                  testSubProgram.getBoard().getBoardSettings().setRightManualAlignmentTransform(alignmentTransform);
                else
                  testSubProgram.getBoard().getBoardSettings().setRightManualAlignmentTransformForHighMag(alignmentTransform);
              }

              // Always update the runtime transform.
              //Siew Yeng - XCR1757 - do not need to update if the subprogram is using runtime manual alignment transform
              if(testSubProgram.isLowMagnification())
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                  testSubProgram.getBoard().getBoardSettings().setRightRuntimeAlignmentTransform(alignmentTransform);
              }
              else
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                  testSubProgram.getBoard().getBoardSettings().setRightRuntimeAlignmentTransformForHighMag(alignmentTransform);
              }
            }
            else
            {
              Assert.expect(false, "Unknown panel location!");
            }
          }

          // Send the transform to reconstruction if required.
          if (sendTransformToReconstruction)
          {
            // Create the aggregate alignment transform.
            java.awt.geom.AffineTransform aggregateAlignmentTransform = new java.awt.geom.AffineTransform(alignmentTransform);

            java.awt.geom.AffineTransform panelToSystemFiducialCoordinateTransform =
                  testSubProgram.getPanelToSystemFiducialCoordinateTransform();
            aggregateAlignmentTransform.preConcatenate(panelToSystemFiducialCoordinateTransform);

            java.awt.geom.AffineTransform machineSpecificTransform = testSubProgram.getMachineSpecificTransform();
            aggregateAlignmentTransform.preConcatenate(machineSpecificTransform);
            
            if (_imageAcquisitionEngine.isAbortInProgress() == false)
            {
              _imageAcquisitionEngine.applyAlignmentResult(aggregateAlignmentTransform, testSubProgram.getId());
            }
          }
          // need to notify the UI that the alignment is complete - this used to be in ReconstructedImagesProducer, but
          // for verification image generation, the IPRs are not notified, so it never got generated.
          // at least here, it will be generated all the time.
          AlignmentInspectionEvent runtimeAlignmentCompletedEvent =
              new AlignmentInspectionEvent(InspectionEventEnum.RUNTIME_ALIGNMENT_COMPLETED);
          runtimeAlignmentCompletedEvent.setMagnificationType(testSubProgram.getMagnificationType());
          _inspectionEventObservable.sendEventInfo(runtimeAlignmentCompletedEvent);
        }
      }
      else if (matchQualityIsAcceptable == false)
      {
        //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
        testSubProgram.setRunAlignmentLearned(false);
        // Match quality was unacceptable.  Throw an exception that we were unable to locate pads.
        if (isDetailedAlignmentFailureLoggingEnabled())
        {
          dumpFailingAlignmentImageCSV(alignmentImage, alignmentRegion);
        }
        throw new AlignmentAlgorithmFailedToLocatePadsBusinessException(testSubProgram, alignmentGroup, FileName.getAlignmentVerificationImageFullPath(project.getName(),
                                                                        alignmentRegion.getAlignmentGroup().getName(),
                                                                        testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT)));
      }
      else if (grayLevelsAreAcceptable == false)
      {
        //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
        testSubProgram.setRunAlignmentLearned(false);
        // Gray levels were unacceptable.  Throw an exception.
        if (isDetailedAlignmentFailureLoggingEnabled())
        {
          dumpFailingAlignmentImageCSV(alignmentImage, alignmentRegion);
        }
        throw new AlignmentAlgorithmGrayLevelsTooDifferentFromExpectedBusinessException(testSubProgram, alignmentGroup, FileName.getAlignmentVerificationImageFullPath(project.getName(),
                                                                                        alignmentRegion.getAlignmentGroup().getName(),
                                                                                        testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT)));
      }

      // If we've completed the alignment process for the entire test program, write the transform settings file.
      if (allAlignmentGroupsHaveBeenLocated(testProgram))
      {
        _alignmentTransformSettingsWriter.write(project);
//        _alignmentTransformSettingsWriter.write(project, ShapeMatching.getInstance().getLogTime());
      }

      if (newAlignmentImage!=null) newAlignmentImage.decrementReferenceCount();
    }
    finally
    {
      if (alignmentRegion != null)
      {
        // Tell the IAE that we're done with the image.
        if (_imageAcquisitionEngine.isAbortInProgress() == false)
        {
          _imageAcquisitionEngine.reconstructionRegionComplete(alignmentRegion, false);
        }

        // Free the image via the IAE.
        _imageAcquisitionEngine.freeReconstructedImages(alignmentRegion);
      }
    }
  }

  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public synchronized void alignOn2DImage(ReconstructedImages reconstructedImages,
                                          boolean sendTransformToReconstruction) throws XrayTesterException
  {
    Assert.expect(reconstructedImages != null);
//    Assert.expect(reconstructedImages.getNumberOfSlices() == 1); // can't handle multiple slices
    Assert.expect(_imageAcquisitionEngine != null);

    ReconstructionRegion alignmentRegion = null;

    try
    {
      // Make sure the image has any necessary rotation corrections applied to it.
      reconstructedImages.ensureAllSlicesAreReadyForInspection();
      
      alignmentRegion = reconstructedImages.getReconstructionRegion();
      AlignmentGroup alignmentGroup = alignmentRegion.getAlignmentGroup();
      TestSubProgram testSubProgram = alignmentRegion.getTestSubProgram();
      TestProgram testProgram = testSubProgram.getTestProgram();
      Project project = testProgram.getProject();
      Panel panel = project.getPanel();
      PanelSettings panelSettings = panel.getPanelSettings();
      
      ReconstructedSlice reconstructedAlignmentSlice = reconstructedImages.getReconstructedSlice(SliceNameEnum.CAMERA_0);
      String groupName = alignmentGroup.getName();    
      Image alignmentImage = reconstructedAlignmentSlice.getOrthogonalImage();
      Image uncorrectedAlignmentImage = reconstructedAlignmentSlice.getImage();
      ImageCoordinate expectedAlignmentTemplateLocationInImage = null;
      Image alignment2DTemplateImage = null;
      int expectedAlignmentTemplateLocationXInImage = 0;
      int expectedAlignmentTemplateLocationYInImage = 0;
      
      String magnification = null;

      if (testSubProgram.getMagnificationType() == MagnificationTypeEnum.LOW)
      {
        if (Config.getSystemCurrentLowMagnification().equals("M19"))
          magnification = "M19";   
        else if (Config.getSystemCurrentLowMagnification().equals("M23"))
          magnification = "M23"; 
      }
      else
      {
        magnification = "M11";  
      }
      
      if (testProgram.isManual2DAlignmentInProgress() == false)
      {
        String pathToScan = Directory.getAlignment2DImagesDir(project.getName());
        String alignmentFileName = null;

        if (FileUtilAxi.exists(pathToScan) == false)
          FileUtilAxi.mkdirs(pathToScan);

        String fileInDirectory = null;
        java.io.File folderToScan = new java.io.File(pathToScan); 
        java.io.File[] listOfFiles = folderToScan.listFiles();

        for (java.io.File file : listOfFiles) 
        {
          if (file.isFile()) 
          {
            fileInDirectory = file.getName();
            Matcher matcher = _alignment2DImageFileNamePattern.matcher(fileInDirectory);
            matcher.reset(fileInDirectory); 

            if (matcher.find()) 
            {
              if (fileInDirectory.contains(magnification + "_G" + alignmentGroup.getName()))
              {
                alignmentFileName = fileInDirectory;
                break;
              }
            }
          }
        } 

        if (alignmentFileName == null)
        {
          throw new AlignmentFailedToFind2DAlignmentImageBusinessException(testSubProgram, alignmentFileName);
        }

        String[] alignmentImageInfoArray = alignmentFileName.split("G" + alignmentGroup.getName());
        String alignmentImageInfo = alignmentImageInfoArray[1]; 
        String[] strArray = null;      
        strArray = ((String)alignmentImageInfo).split("_");

        Assert.expect(strArray[1].matches("^[+-]?\\d+$") && strArray[2].matches("^[+-]?\\d+$") 
                      && strArray[3].matches("^[+-]?\\d+$") && strArray[4].matches("^[+-]?\\d+$"),
                      "The variable is not integer, the file name is: " + (String)alignmentFileName);

        int draggedImageMinX = Integer.parseInt(strArray[1]);
        int draggedImageMinY = Integer.parseInt(strArray[2]);
        int draggedImageWidth = Integer.parseInt(strArray[3]);
        int draggedImageHeight = Integer.parseInt(strArray[4]);

        alignmentGroup.setDraggedExpectedMinX(draggedImageMinX);
        alignmentGroup.setDraggedExpectedMinY(draggedImageMinY);
        alignmentGroup.setDraggedWidth(draggedImageWidth);
        alignmentGroup.setDraggedHeight(draggedImageHeight);
        
        PanelCoordinate expectedCoordinate = new PanelCoordinate((int)Math.round(draggedImageMinX  + draggedImageWidth/2), 
                                                                 (int)Math.round(draggedImageMinY + draggedImageHeight/2));
        alignmentGroup.setExpectedDraggedImageCoordinateInNanometers(expectedCoordinate);  

        String alignment2DFileNameFullPath = Directory.getAlignment2DImageFileFullPath(project.getName(), alignmentFileName);
        alignment2DTemplateImage = XrayImageIoUtil.loadPngImage(alignment2DFileNameFullPath);
        
        expectedAlignmentTemplateLocationXInImage = (int) Math.round((double)(alignmentImage.getWidth() - alignment2DTemplateImage.getWidth()) / (double) 2);
        expectedAlignmentTemplateLocationYInImage = (int) Math.round((double)(alignmentImage.getHeight() - alignment2DTemplateImage.getHeight()) / (double) 2);
      }
      else
      {
        expectedAlignmentTemplateLocationXInImage = (int) Math.round((double)(alignmentImage.getWidth() - alignmentGroup.getDraggedRegionRecWidth()) / (double) 2);
        expectedAlignmentTemplateLocationYInImage = (int) Math.round((double)(alignmentImage.getHeight() - alignmentGroup.getDraggedRegionRecHeight()) / (double) 2);
        
        java.awt.image.BufferedImage alignment2DBufferedImage = alignmentImage.getBufferedImage().getSubimage(expectedAlignmentTemplateLocationXInImage, 
                                                                                                              expectedAlignmentTemplateLocationYInImage, 
                                                                                                              alignmentGroup.getDraggedRegionRecWidth(), 
                                                                                                              alignmentGroup.getDraggedRegionRecHeight()); 

        java.awt.image.BufferedImage processedImage = new java.awt.image.BufferedImage(alignment2DBufferedImage.getWidth(), 
                                                                                       alignment2DBufferedImage.getHeight(), 
                                                                                       java.awt.image.BufferedImage.TYPE_BYTE_GRAY);

        java.awt.Graphics2D gfx = (java.awt.Graphics2D) processedImage.createGraphics();
        gfx.drawImage(alignment2DBufferedImage, alignment2DBufferedImage.getMinX(), alignment2DBufferedImage.getMinY(), alignment2DBufferedImage.getWidth(), 
                      alignment2DBufferedImage.getHeight(), null);
        gfx.dispose();

        alignment2DTemplateImage = Image.createFloatImageFromBufferedImage(processedImage);
        
        String alignment2DFileName = null;
        if (testSubProgram.getMagnificationType() == MagnificationTypeEnum.LOW)
          alignment2DFileName = alignmentGroup.getDraggedImageFileName();
        else
          alignment2DFileName = alignmentGroup.getDraggedImageFileNameForHighMag();
      
        String alignment2DImagesDir = Directory.getAlignment2DImagesDir(alignmentGroup.getProject().getName());

        if (FileUtilAxi.exists(alignment2DImagesDir) == false)
          FileUtilAxi.mkdirs(alignment2DImagesDir);
      
        String alignment2DImageFileHeader = "DraggedImage_" + magnification + "_G" + alignmentGroup.getName();
        String currentFileName = null;
        java.io.File folderToScan = new java.io.File(alignment2DImagesDir); 
        java.io.File[] listOfFiles = folderToScan.listFiles();

        for (java.io.File alignmentFile : listOfFiles) 
        {
          if (alignmentFile.isFile()) 
          {
            currentFileName = alignmentFile.getName();
            try
            {
              if (currentFileName.startsWith(alignment2DImageFileHeader)
                    && currentFileName.endsWith(".png")) 
              {
                FileUtil.deleteFileOrDirectory(alignment2DImagesDir + java.io.File.separator + currentFileName);
              }
            }
            catch (IOException iex)
            {
              throw new CannotDeleteFileDatastoreException(currentFileName);
            }
          }
        }
        
        String alignment2DFileNameFullPath = Directory.getAlignment2DImageFileFullPath(project.getName(), alignment2DFileName);
        XrayImageIoUtil.savePngImage(alignment2DBufferedImage, alignment2DFileNameFullPath);
      }
      
      expectedAlignmentTemplateLocationInImage = new ImageCoordinate(expectedAlignmentTemplateLocationXInImage,
                                                                     expectedAlignmentTemplateLocationYInImage);
      
      PanelCoordinate expectedCoordinate = new PanelCoordinate((int)Math.round(alignmentGroup.getDraggedExpectedMinX()  + alignmentGroup.getDraggedWidth()/2), 
                                                               (int)Math.round(alignmentGroup.getDraggedExpectedMinY() + alignmentGroup.getDraggedHeight()/2));
      alignmentGroup.setExpectedDraggedImageCoordinateInNanometers(expectedCoordinate);    

      // Save the raw alignment image.
      if (_performingManualAlignmentVerification)
      {
        saveRawManualAlignmentImage(uncorrectedAlignmentImage, alignmentRegion);
      }
      else
      {
        saveRawAlignmentImage(uncorrectedAlignmentImage, alignmentRegion);
      }

      // Generate our alignment template.
      ImageCoordinate actualAlignmentTemplateLocationInImage = null;
      // Locate the alignment features in the image.
      DoubleRef matchQualityRef = new DoubleRef();
      Image newAlignmentImage = null; 

      long timeStampInMillis = System.currentTimeMillis();
      boolean isSecondPortionOfLongPanel =
          testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT);

      // Preprocess image before saving to see the don't care, light and dark mask value.
      // Can I assume that this is already available?
      String sourceFile = FileName.getAlignmentVerificationImageFullPath(testProgram.getProject().getName(),
                                                                         alignmentGroup.getName(),
                                                                         isSecondPortionOfLongPanel);
      if (FileUtilAxi.exists(sourceFile))
      {
        String destinationFile = FileName.getAlignmentImageFullPath(testProgram.getProject().getName(),
                                                                    alignmentGroup.getName(),
                                                                    isSecondPortionOfLongPanel,
                                                                    timeStampInMillis);

        FileUtilAxi.copy(sourceFile, destinationFile);
      }

      // Locate the alignment features in the image.
      matchQualityRef = new DoubleRef();

      actualAlignmentTemplateLocationInImage = locatePattern(alignmentImage,
                                                             alignment2DTemplateImage,
                                                             matchQualityRef);
      
      if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
      {
        java.io.File alignmentFile = null;
        try
        {
          String alignment2DDebugDir = Directory.getAlignment2DImagesDir(project.getName()) + File.separator + "debug";
          
          if (FileUtilAxi.exists(alignment2DDebugDir) == false)
            FileUtilAxi.mkdirs(alignment2DDebugDir);
          
          alignmentFile = new java.io.File(alignment2DDebugDir + File.separator +
                                           "returnLocatedImage" + groupName + "_" + magnification + ".jpg");
                                                                        
          javax.imageio.ImageIO.write(alignmentImage.getBufferedImage().getSubimage(actualAlignmentTemplateLocationInImage.getX(), 
                                                                                    actualAlignmentTemplateLocationInImage.getY(), 
                                                                                    alignment2DTemplateImage.getWidth(), 
                                                                                    alignment2DTemplateImage.getHeight()),
                                                                                    "jpeg",alignmentFile);    

          alignmentFile = new java.io.File(alignment2DDebugDir+ File.separator + "ActualImage" + groupName + "_" + magnification + ".jpg");
          
          javax.imageio.ImageIO.write(alignmentImage.getBufferedImage(),"jpeg",alignmentFile);    
        }
        catch (IOException iex)
        {
          throw new CannotOpenFileDatastoreException(alignmentFile.toString());
        }
      }
      
      alignment2DTemplateImage.decrementReferenceCount();
    
      postAlignedOn2DImageEvent(alignmentImage,
                                alignmentRegion,
                                actualAlignmentTemplateLocationInImage);

      boolean matchQualityIsAcceptable = true;
      boolean grayLevelsAreAcceptable = true;
      double matchQuality = matchQualityRef.getValue();
      double foregroundGrayLevel = 64;
      double backgroundGrayLevel = 192;
      
      if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
      {
        System.out.println("Alignment group " + alignmentGroup.getName() + " match quality: " + matchQuality);
      }

      final double MATCH_QUALITY_ABSOLUTE_LOWER_LIMIT = 0.8;
      if (_performingManualAlignmentVerification)
      {
        // Do an absolute check to make sure match quality is something reasonable.
        if (MathUtil.fuzzyGreaterThanOrEquals(matchQuality, MATCH_QUALITY_ABSOLUTE_LOWER_LIMIT))
        {
          if(alignmentGroup.isMatchQualityAcceptable(matchQuality))
          {
            alignmentGroup.setMatchQuality(matchQuality);
            matchQualityIsAcceptable = true;
          }
          else
          {
            matchQualityIsAcceptable = false;
            throw new AlignmentAlgorithmFailedToLocatePadsBusinessException(testSubProgram, alignmentGroup, FileName.getAlignmentVerificationImageFullPath(project.getName(),
                                                                        alignmentRegion.getAlignmentGroup().getName(),
                                                                        testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT)));
          }
        }
        else
        {
          matchQualityIsAcceptable = false;
        }

        // Set the "expected" gray levels for the alignment group.
        if(testSubProgram.isLowMagnification())
        {
          alignmentGroup.setForegroundGrayLevel(foregroundGrayLevel);
          alignmentGroup.setBackgroundGrayLevel(backgroundGrayLevel);
        }
        else
        {
          alignmentGroup.setForegroundGrayLevelInHighMag(foregroundGrayLevel);
          alignmentGroup.setBackgroundGrayLevelInHighMag(backgroundGrayLevel);
        }
        grayLevelsAreAcceptable = true;
      }
      // Bypass match quality & graylevel check if it's in hardware simulation mode and not unit testing
      else if (XrayTester.getInstance().isSimulationModeOn() && (UnitTest.unitTesting() == false))
      {
        matchQualityIsAcceptable = true;
        grayLevelsAreAcceptable = true;
      }
      else
      {
        // Get the learned match quality.
        Assert.expect(alignmentGroup.hasMatchQuality());
        double learnedMatchQuality = alignmentGroup.getMatchQuality();

        final double MATCH_QUALITY_TOLERANCE = 0.2;
        double matchQualityLowerLimit = Math.max(learnedMatchQuality - MATCH_QUALITY_TOLERANCE,
                                                 MATCH_QUALITY_ABSOLUTE_LOWER_LIMIT);
        if (MathUtil.fuzzyGreaterThanOrEquals(matchQuality, matchQualityLowerLimit))
        {
          matchQualityIsAcceptable = true;
        }
        else
        {
          matchQualityIsAcceptable = false;
        }
        
        //Siew Yeng - XCR1757
        if(testSubProgram.isUsingRuntimeManualAlignmentTransform())
        {
          if (MathUtil.fuzzyGreaterThanOrEquals(matchQuality, MATCH_QUALITY_ABSOLUTE_LOWER_LIMIT))
          {
            matchQualityIsAcceptable = true;
          }
          else
          {
            matchQualityIsAcceptable = false;
          }
        }
        
        if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
        {
          System.out.println("Alignment group " + alignmentGroup.getName() + " minimum match quality: " + matchQualityLowerLimit);
        }

        // Modified by Seng Yew on 14-Apr-2014
        // Whenever generate precision image set, save the learned foreground & background graylevel values.
        if (_imageAcquisitionEngine.isGeneratingPrecisionImageSet() == true)
        {
          // Set the "expected" gray levels for the alignment group.
          // This will trigger dirty flag of the recipe and will need to save recipe later.
          if (testSubProgram.isLowMagnification())
          {
            alignmentGroup.setForegroundGrayLevel(foregroundGrayLevel);
            alignmentGroup.setBackgroundGrayLevel(backgroundGrayLevel);
          }
          else
          {
            alignmentGroup.setForegroundGrayLevelInHighMag(foregroundGrayLevel);
            alignmentGroup.setBackgroundGrayLevelInHighMag(backgroundGrayLevel);
          }
		  if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
	      {
            System.out.println("AlignmentLearnedValuesWhenGeneratePrecisionImageSet:"+project.getName()+",foregroundGrayLevel:"+foregroundGrayLevel+",backgroundGrayLevel:"+backgroundGrayLevel);
          }
		  grayLevelsAreAcceptable = true;
          _alignmentTransformSettingsWriter.write(project);
        }
        else
        {
          grayLevelsAreAcceptable = true;
        }
      }
      
      if (matchQualityIsAcceptable && grayLevelsAreAcceptable)
      {
        // Get the actual located alignment point in system fiducial coordinates.
        // First, we need to calculate the expected lock location offset from actual (in pixels).
        ImageCoordinate lockLocationOffsetInPixels = new ImageCoordinate(
            actualAlignmentTemplateLocationInImage.getX() - expectedAlignmentTemplateLocationInImage.getX(),
            actualAlignmentTemplateLocationInImage.getY() - expectedAlignmentTemplateLocationInImage.getY());

        // Next, we'll convert the offset to panel coordinates.
        double NANOMETERS_PER_PIXEL;
        if(testSubProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
          NANOMETERS_PER_PIXEL = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
        else
          NANOMETERS_PER_PIXEL = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
        java.awt.geom.AffineTransform imageSpaceToPanelSpaceTransform =
            java.awt.geom.AffineTransform.getScaleInstance(NANOMETERS_PER_PIXEL, -NANOMETERS_PER_PIXEL);
        java.awt.geom.Point2D transformedPoint = imageSpaceToPanelSpaceTransform.transform(lockLocationOffsetInPixels.getPoint2D(), null);
        PanelCoordinate lockLocationOffsetInNanometers = new PanelCoordinate((int)Math.round(transformedPoint.getX()),
                                                                             (int)Math.round(transformedPoint.getY()));

        // Apply the offset to the expected panel coordinate (as per the user specified manual transform)
        // of the alignment group.
        java.awt.geom.Point2D originalExpectedAlignmentGroupLocationPoint = alignmentGroup.getExpectedDraggedImageCoordinateInNanometers().getPoint2D();
        java.awt.geom.AffineTransform userDefinedManualAlignmentTransform = testSubProgram.getCoreManualAlignmentTransform();
        java.awt.geom.Point2D correctedExpectedAlignmentGroupLocationPoint =
          userDefinedManualAlignmentTransform.transform(originalExpectedAlignmentGroupLocationPoint, null);
        PanelCoordinate correctedExpectedAlignmentGroupPanelCoordinate =
          new PanelCoordinate((int)Math.round(correctedExpectedAlignmentGroupLocationPoint.getX()),
                              (int)Math.round(correctedExpectedAlignmentGroupLocationPoint.getY()));
        PanelCoordinate measuredAlignmentGroupPanelCoordinate =
            correctedExpectedAlignmentGroupPanelCoordinate.plus(lockLocationOffsetInNanometers);

        if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
        {
          System.out.println("matchQuality of " + groupName + ": " + matchQualityRef.getValue());
          System.out.print("Automatic Alignment -- Alignment Group " + alignmentGroup.getName() +
                           " expected centroid location (in panel coords): ");
          System.out.print("( " + correctedExpectedAlignmentGroupLocationPoint.getX() + " , " + correctedExpectedAlignmentGroupLocationPoint.getY() +
                           " )");
          System.out.println();
          System.out.print("Automatic Alignment -- Alignment Group " + alignmentGroup.getName() +
                           " centroid offset (in panel coords): ");
          System.out.print("( " + lockLocationOffsetInNanometers.getX() + " , " + lockLocationOffsetInNanometers.getY() +
                           " )");
          System.out.println();
          System.out.print("Automatic Alignment -- Alignment Group " + alignmentGroup.getName() +
                           " actual centroid location (in panel coords): ");
          System.out.print("( " + measuredAlignmentGroupPanelCoordinate.getX() + " , " + measuredAlignmentGroupPanelCoordinate.getY() +
                           " )");
          System.out.println();
        }

        // Store the located coordinate to the AlignmentGroup.
        if(testSubProgram.isLowMagnification())
          alignmentGroup.setLocatedCentroidInNanometers(measuredAlignmentGroupPanelCoordinate);
        else
          alignmentGroup.setLocatedCentroidInNanometersForHighMag(measuredAlignmentGroupPanelCoordinate);

        Collection<AlignmentGroup> alignmentGroups = testSubProgram.getAlignmentGroups();
        
        boolean isAllAlignmentGroupsHaveBeenLocated = false;
        if(testSubProgram.isLowMagnification())
          isAllAlignmentGroupsHaveBeenLocated = allAlignmentGroupsHaveBeenLocated(alignmentGroups);
        else
          isAllAlignmentGroupsHaveBeenLocated = allAlignmentGroupsHaveBeenLocatedForHighMag(alignmentGroups);
        
        if (isAllAlignmentGroupsHaveBeenLocated)
        {
          // Compute the alignment transform and validate that it's within acceptable limits.
          java.awt.geom.AffineTransform alignmentTransform = computeAlignmentTransformForTestSubProgram(testSubProgram);            

          if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
          {
            System.out.println("Automatic alignment transform matrix : " + alignmentTransform);
          }

          // Update the stored alignment transform(s).
          PanelLocationInSystemEnum panelLocationInSystem = testSubProgram.getPanelLocationInSystem();

          if(panelSettings.isPanelBasedAlignment())
          {
            if (panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT))
            {
              // Only update the manual transform if we're performing manual alignment verification.
              if (_performingManualAlignmentVerification)
              {
                if(testSubProgram.isLowMagnification())
                  panelSettings.setLeftManualAlignmentTransform(alignmentTransform);
                else
                  panelSettings.setLeftManualAlignmentTransformForHighMag(alignmentTransform);
              }

              // Always update the runtime transform.
              //Siew Yeng - XCR1757 - do not need to update if the subprogram is using runtime manual alignment transform
              if(testSubProgram.isLowMagnification())
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                {
                  panelSettings.setLeftRuntimeAlignmentTransform(alignmentTransform);
                  if(_isRunTimeAlignmentNeededAppliedInVerificationImages)
                     panelSettings.setLeftLastSavedViewCadRuntimeAlignmentTransform(alignmentTransform);
                }
              }
              else
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                {
                  panelSettings.setLeftRuntimeAlignmentTransformForHighMag(alignmentTransform);
                  if(_isRunTimeAlignmentNeededAppliedInVerificationImages)
                     panelSettings.setLeftLastSavedRuntimeAlignmentTransformForHighMag(alignmentTransform);
                }
              }
            }
            else if (panelLocationInSystem.equals(PanelLocationInSystemEnum.RIGHT))
            {
              // Only update the manual transform if we're performing manual alignment verification.
              if (_performingManualAlignmentVerification)
              {
                if(testSubProgram.isLowMagnification())
                  panelSettings.setRightManualAlignmentTransform(alignmentTransform);
                else
                  panelSettings.setRightManualAlignmentTransformForHighMag(alignmentTransform);
              }

              // Always update the runtime transform.
              //Siew Yeng - XCR1757 - do not need to update if the subprogram is using runtime manual alignment transform
              if(testSubProgram.isLowMagnification())
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                {
                  panelSettings.setRightRuntimeAlignmentTransform(alignmentTransform);
                  if(_isRunTimeAlignmentNeededAppliedInVerificationImages)
                    panelSettings.setRightLastSavedViewCadRuntimeAlignmentTransform(alignmentTransform);
                }
              }
              else
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                {
                  panelSettings.setRightRuntimeAlignmentTransformForHighMag(alignmentTransform);
                  if(_isRunTimeAlignmentNeededAppliedInVerificationImages)
                     panelSettings.setRightLastSavedViewCadRuntimeAlignmentTransformForHighMag(alignmentTransform);
                }
              }
            }
            else
            {
              Assert.expect(false, "Unknown panel location!");
            }
          }
          else
          {
            // wei chin added for individual alignment
            if (panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT))
            {
              // Only update the manual transform if we're performing manual alignment verification.
              if (_performingManualAlignmentVerification)
              {
                if(testSubProgram.isLowMagnification())
                  testSubProgram.getBoard().getBoardSettings().setLeftManualAlignmentTransform(alignmentTransform);
                else
                  testSubProgram.getBoard().getBoardSettings().setLeftManualAlignmentTransformForHighMag(alignmentTransform);
              }

              // Always update the runtime transform.
              //Siew Yeng - XCR1757 - do not need to update if the subprogram is using runtime manual alignment transform
              if(testSubProgram.isLowMagnification())
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                {
                  testSubProgram.getBoard().getBoardSettings().setLeftRuntimeAlignmentTransform(alignmentTransform);
                  if(_isRunTimeAlignmentNeededAppliedInVerificationImages)
                    testSubProgram.getBoard().getBoardSettings().setLeftLastSavedViewCadRuntimeAlignmentTransform(alignmentTransform);
                }
              }
              else
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                {
                  testSubProgram.getBoard().getBoardSettings().setLeftRuntimeAlignmentTransformForHighMag(alignmentTransform);
                  if(_isRunTimeAlignmentNeededAppliedInVerificationImages)
                    testSubProgram.getBoard().getBoardSettings().setLeftLastSavedViewCadRuntimeAlignmentTransformForHighMag(alignmentTransform);
                }
              }
            }
            else if (panelLocationInSystem.equals(PanelLocationInSystemEnum.RIGHT))
            {
              // Only update the manual transform if we're performing manual alignment verification.
              if (_performingManualAlignmentVerification)
              {
                if(testSubProgram.isLowMagnification())
                  testSubProgram.getBoard().getBoardSettings().setRightManualAlignmentTransform(alignmentTransform);
                else
                  testSubProgram.getBoard().getBoardSettings().setRightManualAlignmentTransformForHighMag(alignmentTransform);
              }

              // Always update the runtime transform.
              //Siew Yeng - XCR1757 - do not need to update if the subprogram is using runtime manual alignment transform
              if(testSubProgram.isLowMagnification())
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                {
                  testSubProgram.getBoard().getBoardSettings().setRightRuntimeAlignmentTransform(alignmentTransform);
                  if(_isRunTimeAlignmentNeededAppliedInVerificationImages)
                     testSubProgram.getBoard().getBoardSettings().setRightLastSavedViewCadRuntimeAlignmentTransform(alignmentTransform);
                }
              }
              else
              {
                if(testSubProgram.isUsingRuntimeManualAlignmentTransform() == false)
                {
                  testSubProgram.getBoard().getBoardSettings().setRightRuntimeAlignmentTransformForHighMag(alignmentTransform);
                  if(_isRunTimeAlignmentNeededAppliedInVerificationImages)
                     testSubProgram.getBoard().getBoardSettings().setRightLastSavedViewCadRuntimeAlignmentTransformForHighMag(alignmentTransform);
                }
              }
            }
            else
            {
              Assert.expect(false, "Unknown panel location!");
            }
          }

          // Send the transform to reconstruction if required.
          if (sendTransformToReconstruction)
          {
            // Create the aggregate alignment transform.
            java.awt.geom.AffineTransform aggregateAlignmentTransform = new java.awt.geom.AffineTransform(alignmentTransform);

            java.awt.geom.AffineTransform panelToSystemFiducialCoordinateTransform =
                  testSubProgram.getPanelToSystemFiducialCoordinateTransform();
            aggregateAlignmentTransform.preConcatenate(panelToSystemFiducialCoordinateTransform);

            java.awt.geom.AffineTransform machineSpecificTransform = testSubProgram.getMachineSpecificTransform();
            aggregateAlignmentTransform.preConcatenate(machineSpecificTransform);
            
            if (_imageAcquisitionEngine.isAbortInProgress() == false)
            {
              _imageAcquisitionEngine.applyAlignmentResult(aggregateAlignmentTransform, testSubProgram.getId());
            }
          }
          // need to notify the UI that the alignment is complete - this used to be in ReconstructedImagesProducer, but
          // for verification image generation, the IPRs are not notified, so it never got generated.
          // at least here, it will be generated all the time.
          AlignmentInspectionEvent runtimeAlignmentCompletedEvent =
              new AlignmentInspectionEvent(InspectionEventEnum.RUNTIME_ALIGNMENT_COMPLETED);
          runtimeAlignmentCompletedEvent.setMagnificationType(testSubProgram.getMagnificationType());
          _inspectionEventObservable.sendEventInfo(runtimeAlignmentCompletedEvent);
        }
      }
      else if (matchQualityIsAcceptable == false)
      {
        //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
        testSubProgram.setRunAlignmentLearned(false);
        // Match quality was unacceptable.  Throw an exception that we were unable to locate pads.
        if (isDetailedAlignmentFailureLoggingEnabled())
        {
          dumpFailingAlignmentImageCSV(alignmentImage, alignmentRegion);
        }
        throw new AlignmentAlgorithmFailedToLocatePadsBusinessException(testSubProgram, alignmentGroup, FileName.getAlignmentVerificationImageFullPath(project.getName(),
                                                                        alignmentRegion.getAlignmentGroup().getName(),
                                                                        testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT)));
      }
      else if (grayLevelsAreAcceptable == false)
      {
        //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
        testSubProgram.setRunAlignmentLearned(false);
        // Gray levels were unacceptable.  Throw an exception.
        if (isDetailedAlignmentFailureLoggingEnabled())
        {
          dumpFailingAlignmentImageCSV(alignmentImage, alignmentRegion);
        }
        throw new AlignmentAlgorithmGrayLevelsTooDifferentFromExpectedBusinessException(testSubProgram, alignmentGroup, FileName.getAlignmentVerificationImageFullPath(project.getName(),
                                                                                        alignmentRegion.getAlignmentGroup().getName(),
                                                                                        testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT)));
      }

      // If we've completed the alignment process for the entire test program, write the transform settings file.
      if (allAlignmentGroupsHaveBeenLocated(testProgram))
      {
        _alignmentTransformSettingsWriter.write(project);
//        _alignmentTransformSettingsWriter.write(project, ShapeMatching.getInstance().getLogTime());
      }

      if (newAlignmentImage!=null) newAlignmentImage.decrementReferenceCount();
    }
    finally
    {
      if (alignmentRegion != null)
      {
        // Tell the IAE that we're done with the image.
        if (_imageAcquisitionEngine.isAbortInProgress() == false)
        {
          _imageAcquisitionEngine.reconstructionRegionComplete(alignmentRegion, false);
        }

        // Free the image via the IAE.
        _imageAcquisitionEngine.freeReconstructedImages(alignmentRegion);
      }
    }
  }

  /**
   * Given the specified Shape (in panel coords), returns an updated Shape (also in panel coords) which takes the
   * results of runtime alignment into account.
   *
   * This simply uses the "core" runtime alignment transform.
   *
   * @author Matt Wharton
   */
  public java.awt.Shape getCorrectedPositionInPanelCoordinates(java.awt.Shape shapeInExpectedPanelCoordinates,
                                                               TestSubProgram testSubProgram)
  {
    Assert.expect(shapeInExpectedPanelCoordinates != null);
    Assert.expect(testSubProgram != null);

    // Get the core runtime alignment transform.
    java.awt.geom.AffineTransform coreRuntimeAlignmentTransform = testSubProgram.getCoreRuntimeAlignmentTransform();

    // Apply the transformation to the Shape.
    java.awt.Shape shapeInActualPanelCoordinates =
        coreRuntimeAlignmentTransform.createTransformedShape(shapeInExpectedPanelCoordinates);

    return shapeInActualPanelCoordinates;
  }

  /**
   * Given the specified PanelCoordinate, returns an updated PanelCoordinate which takes the results of
   * runtime alignment into account.
   *
   * This simply uses the "core" manual alignment transform.
   *
   * @author Matt Wharton
   */
  public PanelCoordinate getCorrectedPositionInPanelCoordinates(PanelCoordinate expectedPanelCoordinate,
                                                                TestSubProgram testSubProgram)
  {
    Assert.expect(expectedPanelCoordinate != null);
    Assert.expect(testSubProgram != null);

    // Get the core runtime alignment transform.
    java.awt.geom.AffineTransform coreRuntimeAlignmentTransform = testSubProgram.getCoreRuntimeAlignmentTransform();

    // Apply the transformation to the coordinate.
    java.awt.geom.Point2D expectedPanelCoordinatePoint2d = new java.awt.geom.Point2D.Double(expectedPanelCoordinate.getX(),
                                                                expectedPanelCoordinate.getY());
    java.awt.geom.Point2D actualPanelCoordinatePoint2d = coreRuntimeAlignmentTransform.transform(expectedPanelCoordinatePoint2d, null);
    PanelCoordinate actualPanelCoordinate = new PanelCoordinate((int)Math.round(actualPanelCoordinatePoint2d.getX()),
                                                                (int)Math.round(actualPanelCoordinatePoint2d.getY()));

    return actualPanelCoordinate;
  }

  /**
   * Converts the specified ImageCoordinate to a PanelCoordinate.
   *
   * @author Matt Wharton
   */
  private PanelCoordinate convertImageCoordinateToPanelCoordinate(ImageCoordinate imageCoordinate,
                                                                  ReconstructedImages reconstructedImages)
  {
    Assert.expect(imageCoordinate != null);
    Assert.expect(reconstructedImages != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    PanelRectangle regionInPanelCoords = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    int regionOffsetXInPanelCoords = regionInPanelCoords.getMinX();
    int regionOffsetYInPanelCoords = regionInPanelCoords.getMaxY();
    final double NANOMETERS_PER_PIXEL = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();

    java.awt.geom.AffineTransform imageSpaceToPanelSpaceTransform =
        java.awt.geom.AffineTransform.getTranslateInstance(regionOffsetXInPanelCoords, regionOffsetYInPanelCoords);
    imageSpaceToPanelSpaceTransform.scale(NANOMETERS_PER_PIXEL, -NANOMETERS_PER_PIXEL);
    java.awt.geom.Point2D transformedPoint = imageSpaceToPanelSpaceTransform.transform(imageCoordinate.getPoint2D(), null);
    PanelCoordinate panelCoordinate = new PanelCoordinate((int)Math.round(transformedPoint.getX()),
                                                          (int)Math.round(transformedPoint.getY()));

    return panelCoordinate;
  }

  /**
   * Generates an alignment template for the specified alignment region and attempts to
   * match that template against the specified alignment image.  Returns the ImageCoordinate with
   * the strongest match.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  private ImageCoordinate locatePattern(Image alignmentImage,
                                        Image alignmentTemplateImage,
                                        DoubleRef matchQuality) throws XrayTesterException
  {
    Assert.expect(alignmentImage != null);
    Assert.expect(alignmentTemplateImage != null);
    Assert.expect(matchQuality != null);

    // Match our template against the alignment image.
    ImageCoordinate upperLeftCornerOfMatchLocation = Filter.matchTemplate(alignmentImage,
                                                                          alignmentTemplateImage,
                                                                          MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING,
                                                                          matchQuality);

    return upperLeftCornerOfMatchLocation;
  }

  /**
   * <pre>
   * This method does the following:
   *   1.  For each alignment feature in the region.
   *       i.   Takes the mean graylevel in the feature ROI--call this foreground.
   *       ii.  Takes the mean graylevel in a ring around the feature--call this background.
   *       iii. Takes the delta graylevel (background - foreground)
   *   2.  Returns the foregrond and background values for the alignment feature which produces the minimum
   *       delta gray.
   * </pre>
   *
   * @author Matt Wharton
   */
  private Pair<Double, Double> measureAlignmentRegionGrayLevels(Image alignmentImage,
                                                                ReconstructionRegion alignmentRegion,
                                                                ImageCoordinate upperLeftCornerOfMatchLocation,
                                                                List<RegionOfInterest> alignmentFeaturesInTemplateImage) throws AlignmentBusinessException
  {
    Assert.expect(alignmentImage != null);
    Assert.expect(alignmentRegion != null);
    Assert.expect(upperLeftCornerOfMatchLocation != null);
    Assert.expect(alignmentFeaturesInTemplateImage != null);

    double minDeltaGrayLevel = Double.MAX_VALUE;
    double minDeltaGrayForeground = Double.MAX_VALUE;
    double minDeltaGrayBackground = Double.MAX_VALUE;
    for (RegionOfInterest alignmentFeatureInTemplateImage : alignmentFeaturesInTemplateImage)
    {
      // Measure the average gray level in the alignment feature ROI--we consider this the foregronud.
      RegionOfInterest regionInAlignmentImage =
          new RegionOfInterest(upperLeftCornerOfMatchLocation.getX() + alignmentFeatureInTemplateImage.getMinX(),
                               upperLeftCornerOfMatchLocation.getY() + alignmentFeatureInTemplateImage.getMinY(),
                               alignmentFeatureInTemplateImage.getWidth(),
                               alignmentFeatureInTemplateImage.getHeight(),
                               alignmentFeatureInTemplateImage.getOrientationInDegrees(),
                               RegionShapeEnum.RECTANGULAR);
      double foregroundGrayLevel = Statistics.mean(alignmentImage, regionInAlignmentImage);

      // Measure the average in the ring around the alignment feature--we consider this the background.
      final int INNER_RING_OFFSET = 4;
      RegionOfInterest innerRingEdgeRoi = new RegionOfInterest(regionInAlignmentImage);
      innerRingEdgeRoi.setWidthKeepingSameCenter(innerRingEdgeRoi.getWidth() + INNER_RING_OFFSET);
      innerRingEdgeRoi.setHeightKeepingSameCenter(innerRingEdgeRoi.getHeight() + INNER_RING_OFFSET);
      final int OUTER_RING_OFFSET = 12;
      RegionOfInterest outerRingEdgeRoi = new RegionOfInterest(regionInAlignmentImage);
      outerRingEdgeRoi.setWidthKeepingSameCenter(outerRingEdgeRoi.getWidth() + OUTER_RING_OFFSET);
      outerRingEdgeRoi.setHeightKeepingSameCenter(outerRingEdgeRoi.getHeight() + OUTER_RING_OFFSET);
      if ((innerRingEdgeRoi.fitsWithinImage(alignmentImage) == false) || (outerRingEdgeRoi.fitsWithinImage(alignmentImage) == false))
      {
        //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
        alignmentRegion.getTestSubProgram().setRunAlignmentLearned(false);
        throw new AlignmentAlgorithmLocatedPadsTooCloseToEdgeException(alignmentRegion.getTestSubProgram(),
                                                                       alignmentRegion.getAlignmentGroup(), FileName.getAlignmentVerificationImageFullPath(alignmentRegion.getTestSubProgram().getTestProgram().getProject().getName(),
                                                                        alignmentRegion.getAlignmentGroup().getName(),
                                                                        alignmentRegion.getTestSubProgram().getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT)));
      }
      double backgroundGrayLevel = Statistics.meanAroundRegion(alignmentImage, outerRingEdgeRoi, innerRingEdgeRoi);

      // Compute the delta gray level abs(background - foreground).
      double deltaGrayLevel = Math.abs(backgroundGrayLevel - foregroundGrayLevel);

      // See if we have a new minimum delta gray.
      if (deltaGrayLevel < minDeltaGrayLevel)
      {
        minDeltaGrayLevel = deltaGrayLevel;
        minDeltaGrayForeground = foregroundGrayLevel;
        minDeltaGrayBackground = backgroundGrayLevel;
      }
    }

    Assert.expect(minDeltaGrayLevel != Double.MAX_VALUE);
    Assert.expect(minDeltaGrayForeground != Double.MAX_VALUE);
    Assert.expect(minDeltaGrayBackground != Double.MAX_VALUE);

    return new Pair<Double, Double>(minDeltaGrayForeground, minDeltaGrayBackground);
  }

  /**
   * @author Matt Wharton
   */
  private void postAlignedOnImageEvent(Image alignmentImage,
                                       ReconstructionRegion alignmentRegion,
                                       List<RegionOfInterest> alignmentFeaturesInTemplateImage,
                                       List<RegionOfInterest> alignmentContextFeaturesInTemplateImage,
                                       ImageCoordinate upperLeftCornerOfMatchLocation)
  {
    Assert.expect(alignmentImage != null);
    Assert.expect(alignmentRegion != null);
    Assert.expect(alignmentFeaturesInTemplateImage != null);
    Assert.expect(alignmentContextFeaturesInTemplateImage != null);
    Assert.expect(upperLeftCornerOfMatchLocation != null);

    // Plot the locked on region locations.
    java.awt.geom.GeneralPath locatedAlignmentPadRegions = new java.awt.geom.GeneralPath();
    for (RegionOfInterest alignmentFeatureInTemplateImage : alignmentFeaturesInTemplateImage)
    {
      RegionOfInterest regionInAlignmentImage =
          new RegionOfInterest(upperLeftCornerOfMatchLocation.getX() + alignmentFeatureInTemplateImage.getMinX(),
                               upperLeftCornerOfMatchLocation.getY() + alignmentFeatureInTemplateImage.getMinY(),
                               alignmentFeatureInTemplateImage.getWidth(),
                               alignmentFeatureInTemplateImage.getHeight(),
                               alignmentFeatureInTemplateImage.getOrientationInDegrees(),
                               alignmentFeatureInTemplateImage.getRegionShapeEnum());

      locatedAlignmentPadRegions.append(regionInAlignmentImage.getShape(), false);
    }

    // Plot the locked on context region locations.
    java.awt.geom.GeneralPath locatedAlignmentContextPadRegions = new java.awt.geom.GeneralPath();
    for (RegionOfInterest alignmentContextFeatureInTemplateImage : alignmentContextFeaturesInTemplateImage)
    {
      RegionOfInterest contextRegionInAlignmentImage =
          new RegionOfInterest(upperLeftCornerOfMatchLocation.getX() + alignmentContextFeatureInTemplateImage.getMinX(),
                               upperLeftCornerOfMatchLocation.getY() + alignmentContextFeatureInTemplateImage.getMinY(),
                               alignmentContextFeatureInTemplateImage.getWidth(),
                               alignmentContextFeatureInTemplateImage.getHeight(),
                               alignmentContextFeatureInTemplateImage.getOrientationInDegrees(),
                               alignmentContextFeatureInTemplateImage.getRegionShapeEnum());

      locatedAlignmentContextPadRegions.append(contextRegionInAlignmentImage.getShape(), false);
    }

    MeasurementRegionDiagnosticInfo locatedAlignmentFeaturesDiagInfo =
        new MeasurementRegionDiagnosticInfo(locatedAlignmentPadRegions, MeasurementRegionEnum.ALIGNMENT_PAD_REGION);

    MeasurementRegionDiagnosticInfo locatedContextAlignmentFeaturesDiagInfo =
        new MeasurementRegionDiagnosticInfo(locatedAlignmentContextPadRegions, MeasurementRegionEnum.ALIGNMENT_CONTEXT_PAD_REGION);

    TestSubProgram testSubProgram = alignmentRegion.getTestSubProgram();
    PanelLocationInSystemEnum panelLocationInSystem = testSubProgram.getPanelLocationInSystem();

    Assert.expect(_inspectionEventObservable != null);
    AlignmentInspectionEvent alignedOnImageEvent = new AlignmentInspectionEvent(InspectionEventEnum.ALIGNED_ON_IMAGE);
    alignedOnImageEvent.setAlignmentGroup(alignmentRegion.getAlignmentGroup());
    alignedOnImageEvent.setMagnificationType(testSubProgram.getMagnificationType());
    alignedOnImageEvent.setPanelLocationInSystem(panelLocationInSystem);
    alignedOnImageEvent.setImage(alignmentImage);
    alignedOnImageEvent.setLocatedAlignmentFeaturesDiagnosticInfo(locatedAlignmentFeaturesDiagInfo);
    alignedOnImageEvent.setLocatedAlignmentContextFeaturesDiagnosticInfo(locatedContextAlignmentFeaturesDiagInfo);
    _inspectionEventObservable.sendEventInfo(alignedOnImageEvent);

    // If we're in sim-mode, we need to give this event time to be processed (since the images get generated so fast).
    if (XrayTester.getInstance().isSimulationModeOn())
    {
      try
      {
        Thread.sleep(500);
      }
      catch (InterruptedException iex)
      {
      }
    }
  }

  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  private void postAlignedOn2DImageEvent(Image alignmentImage,
                                         ReconstructionRegion alignmentRegion,
                                         ImageCoordinate upperLeftCornerOfMatchLocation)
  {
    Assert.expect(alignmentImage != null);
    Assert.expect(alignmentRegion != null);
    Assert.expect(upperLeftCornerOfMatchLocation != null);

    TestSubProgram testSubProgram = alignmentRegion.getTestSubProgram();
    PanelLocationInSystemEnum panelLocationInSystem = testSubProgram.getPanelLocationInSystem();

    Assert.expect(_inspectionEventObservable != null);
    AlignmentInspectionEvent alignedOnImageEvent = new AlignmentInspectionEvent(InspectionEventEnum.ALIGNED_ON_2D_IMAGE);
    alignedOnImageEvent.setAlignmentGroup(alignmentRegion.getAlignmentGroup());
    alignedOnImageEvent.setMagnificationType(testSubProgram.getMagnificationType());
    alignedOnImageEvent.setPanelLocationInSystem(panelLocationInSystem);
    alignedOnImageEvent.setImage(alignmentImage);
    _inspectionEventObservable.sendEventInfo(alignedOnImageEvent);

    // If we're in sim-mode, we need to give this event time to be processed (since the images get generated so fast).
    if (XrayTester.getInstance().isSimulationModeOn())
    {
      try
      {
        Thread.sleep(500);
      }
      catch (InterruptedException iex)
      {
      }
    }
  }
  
  /**
   * Based upon the specified alignment region, generates a template image to match against the features
   * in the alignment region.
   *
   * Modify by Wei Chin 21-11-2013
   * - Handle alignment region won't affect by resize feature 
   * 
   * @author Matt Wharton
   */
  private Pair<RegionOfInterest, Image> generateAlignmentTemplateImage(Image alignmentImage,
                                                                       ReconstructionRegion alignmentRegion,
                                                                       List<RegionOfInterest> alignmentFeaturesInTemplateImage,
                                                                       List<RegionOfInterest> alignmentContextFeaturesInTemplateImage,
                                                                       boolean isUseShapeMatching) throws DatastoreException
  {
    Assert.expect(alignmentImage != null);
    Assert.expect(alignmentRegion != null);
    Assert.expect(alignmentRegion.isAlignmentRegion());
    Assert.expect(alignmentFeaturesInTemplateImage != null);
    Assert.expect(alignmentContextFeaturesInTemplateImage != null);

    // Figure out whether we're trying to lock onto dark or light features.
    AlignmentGroup alignmentGroup = alignmentRegion.getAlignmentGroup();
    boolean alignOnDarkPadsWithLightBackgrounds = alignmentGroup.hasDarkPadsWithLightBackground();
    float templateDontCareValue = 0f;
    float templateDarkValue = 0f;
    float templateLightValue = 0f;
    if (alignOnDarkPadsWithLightBackgrounds)
    {
      templateDarkValue = -1f;
      templateLightValue = 1f;
    }
    else
    {
      templateDarkValue = 1f;
      templateLightValue = -1f;
    }

    // Build a bounding box around the alignment pads and/or fiducials.
    List<JointInspectionData> alignmentPads = alignmentRegion.getJointInspectionDataList();
    List<FiducialInspectionData> alignmentFiducials = alignmentRegion.getFiducialInspectionDataList();
    List<Feature> alignmentFeatures = new LinkedList<Feature>(alignmentPads);
    alignmentFeatures.addAll(alignmentFiducials);
    List<Feature> alignmentFeaturesAndContextFeatures = new LinkedList<Feature>(alignmentFeatures);
    Collection<JointInspectionData> alignmentContextFeatures = alignmentRegion.getAlignmentContextFeatures();
    alignmentFeaturesAndContextFeatures.addAll(alignmentContextFeatures);
    List<RegionOfInterest> ghostRois = calculateGhostRoisForAlignmentRegion(alignmentRegion);
    RegionOfInterest alignmentFeaturesBoundingRegionInPixels = createAlignmentTemplateBoundingBox(alignmentFeaturesAndContextFeatures,
                                                                                                  ghostRois);

    // Now, create the template image.
    Image templateImage = new Image(alignmentFeaturesBoundingRegionInPixels.getWidth(),
                                    alignmentFeaturesBoundingRegionInPixels.getHeight());
    if(isUseShapeMatching)
    {
        // Fill in the background with the "Light" value.
        Paint.fillImage(templateImage, templateLightValue);   
    }
    else
    {
        // Fill in the background with the "don't care" value.
        Paint.fillImage(templateImage, templateDontCareValue);
    }
        
    // Clear out the list of alignment features and context features in the template image.
    alignmentFeaturesInTemplateImage.clear();
    alignmentContextFeaturesInTemplateImage.clear();

    // Fill in the joint/fiducial ROIs (and border) with the applicable foreground and background colors.
    int templateImageXOffsetInAlignmentImage = alignmentFeaturesBoundingRegionInPixels.getMinX();
    int templateImageYOffsetInAlignmentImage = alignmentFeaturesBoundingRegionInPixels.getMinY();
    // Fill in the alignment features and context features with the applicable foreground and background colors.
    for (Feature feature : alignmentFeaturesAndContextFeatures)
    {
      // The alignment pad ROIs are in terms of the whole alignment image.  We have to translate the coordinates
      // to be relative to the origin of the smaller template image.
      RegionOfInterest alignmentOrContextFeatureRoiInTemplateImage = null;
      RegionOfInterest alignmentOrContextFeatureRoiInTemplateImagePin2 = null;
      boolean isContextPad = false;
      if (feature instanceof JointInspectionData)
      {
        JointInspectionData alignmentOrContextPad = (JointInspectionData)feature;
        if (ImageAnalysis.isSpecialTwoPinComponent(alignmentOrContextPad.getJointTypeEnum()))
        {
          if (alignmentOrContextPad.getJointTypeEnum().equals(JointTypeEnum.RESISTOR))
          {
            // Handle resistor type only
            Assert.expect(alignmentOrContextPad.getComponentInspectionData().getJointInspectionDataSet().size() == 2);
            Iterator<JointInspectionData> jointIterator = alignmentOrContextPad.getComponentInspectionData().getJointInspectionDataSet().iterator();
            alignmentOrContextFeatureRoiInTemplateImage = new RegionOfInterest(jointIterator.next().getOrthogonalRegionOfInterestInPixels(false));
            alignmentOrContextFeatureRoiInTemplateImagePin2 = new RegionOfInterest(jointIterator.next().getOrthogonalRegionOfInterestInPixels(false));
          }
          else
          {
            ComponentInspectionData componentInspectionData = alignmentOrContextPad.getComponentInspectionData();
            alignmentOrContextFeatureRoiInTemplateImage = new RegionOfInterest(componentInspectionData.getOrthogonalComponentRegionOfInterest(false));
          }
        }
        else
        {
          alignmentOrContextFeatureRoiInTemplateImage = new RegionOfInterest(alignmentOrContextPad.getOrthogonalRegionOfInterestInPixels(false));
        }

        isContextPad = alignmentContextFeatures.contains(alignmentOrContextPad);
      }
      else if (feature instanceof FiducialInspectionData)
      {
        FiducialInspectionData alignmentFiducial = (FiducialInspectionData)feature;
        alignmentOrContextFeatureRoiInTemplateImage = new RegionOfInterest(alignmentFiducial.getRegionOfInterestRelativeToInspectionRegionInPixels());
      }
      else
      {
        Assert.expect(false, "Unexpected feature type: " + feature);
      }

      alignmentOrContextFeatureRoiInTemplateImage.translateXY(-templateImageXOffsetInAlignmentImage, -templateImageYOffsetInAlignmentImage);
      // Handle resistor type only
      if (alignmentOrContextFeatureRoiInTemplateImagePin2 != null)
        alignmentOrContextFeatureRoiInTemplateImagePin2.translateXY(-templateImageXOffsetInAlignmentImage, -templateImageYOffsetInAlignmentImage);

      // Fill in the border region around the feature with the "light" value and use the "dark" value on the
      // feature itself.
      RegionOfInterest alignmentPadRoiWithBorderInTemplateImage = new RegionOfInterest(alignmentOrContextFeatureRoiInTemplateImage);
      alignmentPadRoiWithBorderInTemplateImage.setWidthKeepingSameCenter(
          alignmentPadRoiWithBorderInTemplateImage.getWidth() + (2 * _ALIGNMENT_FEATURE_BORDER_WIDTH));
      alignmentPadRoiWithBorderInTemplateImage.setHeightKeepingSameCenter(
          alignmentPadRoiWithBorderInTemplateImage.getHeight() + (2 * _ALIGNMENT_FEATURE_BORDER_WIDTH));
      RegionOfInterest alignmentPadRoiWithBorderInTemplateImagePin2 = null;
      // Handle second pad of special two pins components
      if (alignmentOrContextFeatureRoiInTemplateImagePin2 != null)
      {
        alignmentPadRoiWithBorderInTemplateImagePin2 = new RegionOfInterest(alignmentOrContextFeatureRoiInTemplateImagePin2);
        alignmentPadRoiWithBorderInTemplateImagePin2.setWidthKeepingSameCenter(
            alignmentPadRoiWithBorderInTemplateImagePin2.getWidth() + (2 * _ALIGNMENT_FEATURE_BORDER_WIDTH));
        alignmentPadRoiWithBorderInTemplateImagePin2.setHeightKeepingSameCenter(
            alignmentPadRoiWithBorderInTemplateImagePin2.getHeight() + (2 * _ALIGNMENT_FEATURE_BORDER_WIDTH));
      }

      // Trying to create mask according to the shapes of joints
      if (alignmentPadRoiWithBorderInTemplateImage.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR))
      {
        Paint.fillRegionOfInterest(templateImage, alignmentPadRoiWithBorderInTemplateImage, templateLightValue);
        // Handle second pad of special two pins components
        if (alignmentPadRoiWithBorderInTemplateImagePin2 != null)
          Paint.fillRegionOfInterest(templateImage, alignmentPadRoiWithBorderInTemplateImagePin2, templateLightValue);
      }
      else
      {
        Paint.fillObround(templateImage,
                         alignmentPadRoiWithBorderInTemplateImage,
                         new DoubleCoordinate(alignmentPadRoiWithBorderInTemplateImage.getCenterX(),alignmentPadRoiWithBorderInTemplateImage.getCenterY()),
                         templateLightValue);
      }
      if (alignmentOrContextFeatureRoiInTemplateImage.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR))
      {
        Paint.fillRegionOfInterest(templateImage, alignmentOrContextFeatureRoiInTemplateImage, templateDarkValue);
        // Handle second pad of special two pins components
        if (alignmentOrContextFeatureRoiInTemplateImagePin2 != null)
          Paint.fillRegionOfInterest(templateImage, alignmentOrContextFeatureRoiInTemplateImagePin2, templateDarkValue);
      }
      else
      {
        Paint.fillObround(templateImage,
                         alignmentOrContextFeatureRoiInTemplateImage,
                         new DoubleCoordinate(alignmentOrContextFeatureRoiInTemplateImage.getCenterX(),alignmentOrContextFeatureRoiInTemplateImage.getCenterY()),
                         templateDarkValue);
      }

      if (isContextPad == false)
      {
        alignmentFeaturesInTemplateImage.add(alignmentOrContextFeatureRoiInTemplateImage);
        // Handle second pad of special two pins components
        if (alignmentOrContextFeatureRoiInTemplateImagePin2 != null)
          alignmentFeaturesInTemplateImage.add(alignmentOrContextFeatureRoiInTemplateImagePin2);
      }
      else
      {
        alignmentContextFeaturesInTemplateImage.add(alignmentOrContextFeatureRoiInTemplateImage);
        // Handle second pad of special two pins components
        if (alignmentOrContextFeatureRoiInTemplateImagePin2 != null)
          alignmentContextFeaturesInTemplateImage.add(alignmentOrContextFeatureRoiInTemplateImagePin2);
      }
    }

    // Finally, let's fill in any "ghost ROIs" we added with the "light" value.
    for (RegionOfInterest ghostRoi : ghostRois)
    {
      // The "ghost ROI" is in terms of the whole alignment iamge.  We have to translate the coordinates to be relative
      // to the origin of the smaller template image.
      RegionOfInterest ghostRoiInTemplateImage = new RegionOfInterest(ghostRoi);
      ghostRoiInTemplateImage.translateXY(-templateImageXOffsetInAlignmentImage, -templateImageYOffsetInAlignmentImage);

      // Trying to create mask according to the shapes of joints
      if (ghostRoiInTemplateImage.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR))
      {
        Paint.fillRegionOfInterest(templateImage, ghostRoiInTemplateImage, templateLightValue);
      }
      else
      {
        Paint.fillObround(templateImage,
                         ghostRoiInTemplateImage,
                         new DoubleCoordinate(ghostRoiInTemplateImage.getCenterX(),ghostRoiInTemplateImage.getCenterY()),
                         templateLightValue);
      }
    }

    // MDW - uncomment this code to save the alignment template images to disk.
//    String imagesDir = Directory.getOnlineXrayImagesDir();
//    String imageFileName = "Alignment_Template_Image_" + alignmentRegion.getName() + ".png";
//    XrayImageIoUtil.savePngImage(templateImage.getBufferedImage(),
//                                 imagesDir + File.separator + imageFileName);

    return new Pair<RegionOfInterest, Image>(alignmentFeaturesBoundingRegionInPixels, templateImage);
  }

  /**
   * Creates a bounding box for the specified alignment (and context) features and "ghost pads".
   *
   * @author Matt Wharton
   */
  private RegionOfInterest createAlignmentTemplateBoundingBox(List<Feature> alignmentAndContextFeatures,
                                                              List<RegionOfInterest> ghostRois)
  {
    Assert.expect(alignmentAndContextFeatures != null);
    Assert.expect(ghostRois != null);

    // Make space for the alignment pads and fiducials.
    RegionOfInterest alignmentFeaturesBoundingRegionInPixels = null;
    for (Feature alignmentOrContextFeature : alignmentAndContextFeatures)
    {
      RegionOfInterest alignmentFeatureRoi = null;
      if (alignmentOrContextFeature instanceof JointInspectionData)
      {
        JointInspectionData alignmentPad = (JointInspectionData)alignmentOrContextFeature;
        if (ImageAnalysis.isSpecialTwoPinComponent(alignmentPad.getJointTypeEnum()))
        {
          ComponentInspectionData componentInspectionData = alignmentPad.getComponentInspectionData();
          alignmentFeatureRoi = new RegionOfInterest(componentInspectionData.getOrthogonalComponentRegionOfInterest(false));
        }
        else
        {
          alignmentFeatureRoi = new RegionOfInterest(alignmentPad.getOrthogonalRegionOfInterestInPixels(false));
        }
      }
      else if (alignmentOrContextFeature instanceof FiducialInspectionData)
      {
        FiducialInspectionData alignmentFiducial = (FiducialInspectionData)alignmentOrContextFeature;
        alignmentFeatureRoi =
            new RegionOfInterest(alignmentFiducial.getRegionOfInterestRelativeToInspectionRegionInPixels());
      }
      else
      {
        Assert.expect(false, "Unexpected alignment feature type: " + alignmentOrContextFeature);
      }

      // Make room for a border around the ROI (this helps with cross correlation).
      alignmentFeatureRoi.setWidthKeepingSameCenter(
          alignmentFeatureRoi.getWidth() + (2 * _ALIGNMENT_FEATURE_BORDER_WIDTH));
      alignmentFeatureRoi.setHeightKeepingSameCenter(
          alignmentFeatureRoi.getHeight() + (2 * _ALIGNMENT_FEATURE_BORDER_WIDTH));

      // Add space for the final feature ROI in the template image ROI.
      if (alignmentFeaturesBoundingRegionInPixels == null)
      {
        alignmentFeaturesBoundingRegionInPixels = new RegionOfInterest(alignmentFeatureRoi);
      }
      else
      {
        alignmentFeaturesBoundingRegionInPixels.add(alignmentFeatureRoi);
      }
    }

    // Now let's add space for some "ghost ROIs" in the template.
    if (ghostRois.isEmpty() == false)
    {
      // We have some "ghost ROIs" so let's try add them.
      for (RegionOfInterest ghostRoi : ghostRois)
      {
        alignmentFeaturesBoundingRegionInPixels.add(ghostRoi);
      }
    }

    return alignmentFeaturesBoundingRegionInPixels;
  }

  /**
   * Creates a List of "ghost ROIs" for the specified alignment region.  These are used when creating
   * the template image for an alignment region lock.
   *
   * Modify by Wei Chin 21-11-2013
   * - Handle alignment region won't affect by resize feature 
   * 
   * @author Matt Wharton
   * @author Tad Ghostal (a.k.a. Space Ghost)
   */
  private List<RegionOfInterest> calculateGhostRoisForAlignmentRegion(ReconstructionRegion alignmentRegion)
  {
    Assert.expect(alignmentRegion != null);
    Assert.expect(alignmentRegion.isAlignmentRegion());

    // We should have at least one fiducial or alignment pad defined.
    List<JointInspectionData> alignmentPads = alignmentRegion.getJointInspectionDataList();
    List<FiducialInspectionData> alignmentFiducials = alignmentRegion.getFiducialInspectionDataList();
    Assert.expect((alignmentPads.isEmpty() == false) || (alignmentFiducials.isEmpty() == false));

    // If we are aligning on fiducials, no "ghost pads" will be necessary.
    if (alignmentFiducials.isEmpty() == false)
    {
      return Collections.emptyList();
    }

    // If we are aligning on more than one component, we can assume that "ghost pads" will not be necesssary.
    // Also, if we have any "two pin" devices, "ghost pads" aren't needed.
    Component componentOfFirstAlignmentFeature = alignmentPads.iterator().next().getComponent();
    for (JointInspectionData jointInspectionData : alignmentPads)
    {
      if (componentOfFirstAlignmentFeature != jointInspectionData.getComponent())
      {
        return Collections.emptyList();
      }
      else if (ImageAnalysis.isSpecialTwoPinComponent(jointInspectionData.getJointTypeEnum()))
      {
        return Collections.emptyList();
      }
    }

    // Get the component center coordinate.
    PanelCoordinate componentCenter = componentOfFirstAlignmentFeature.getCenterCoordinateRelativeToPanelInNanoMeters();

    // Locate the most horizontally and vertically "extreme" joints based on relative position to the component center.
    boolean encounteredHorizontallyOrientedJoint = false;
    boolean encounteredVerticallyOrientedJoint = false;
    JointInspectionData mostHorizontallyExtremeJoint = null;
    int greatestAbsoluteXDistanceFromComponentCenter = 0;
    int horizontalDirection = 0;
    JointInspectionData mostVerticallyExtremeJoint = null;
    int greatestAbsoluteYDistanceFromComponentCenter = 0;
    int verticalDirection = 0;
    for (JointInspectionData alignmentPad : alignmentPads)
    {
      RegionOfInterest jointRegionInPixels = alignmentPad.getOrthogonalRegionOfInterestInPixels(false);
      int jointOrientation = jointRegionInPixels.getOrientationInDegrees();

      Pad pad = alignmentPad.getPad();
      PanelCoordinate padCenter = pad.getCenterCoordinateRelativeToPanelInNanoMeters();

      //sham, 2012-05-14, Alignment Enhancement
      //previous version will only add one ghost joint on either most vertical or most horizontal
      encounteredHorizontallyOrientedJoint = true;
      int yDistanceFromComponentCenter = padCenter.getY() - componentCenter.getY();
      int absoluteYDistanceFromComponentCenter = Math.abs(yDistanceFromComponentCenter);
      if (mostVerticallyExtremeJoint == null)
      {
        greatestAbsoluteYDistanceFromComponentCenter = absoluteYDistanceFromComponentCenter;
        mostVerticallyExtremeJoint = alignmentPad;
        // Y direction is inverted between panel and image coords.  Ergo, we need to take the negative here.
        verticalDirection = -(int)Math.signum(yDistanceFromComponentCenter);
      }
      else if (absoluteYDistanceFromComponentCenter > greatestAbsoluteYDistanceFromComponentCenter)
      {
        greatestAbsoluteYDistanceFromComponentCenter = absoluteYDistanceFromComponentCenter;
        mostVerticallyExtremeJoint = alignmentPad;
        // Y direction is inverted between panel and image coords.  Ergo, we need to take the negative here.
        verticalDirection = -(int)Math.signum(yDistanceFromComponentCenter);
      }
      encounteredVerticallyOrientedJoint = true;
      int xDistanceFromComponentCenter = padCenter.getX() - componentCenter.getX();
      int absoluteXDistanceFromComponentCenter = Math.abs(xDistanceFromComponentCenter);
      if (mostHorizontallyExtremeJoint == null)
      {
        greatestAbsoluteXDistanceFromComponentCenter = absoluteXDistanceFromComponentCenter;
        mostHorizontallyExtremeJoint = alignmentPad;
        horizontalDirection = (int)Math.signum(xDistanceFromComponentCenter);
      }
      else if (absoluteXDistanceFromComponentCenter > greatestAbsoluteXDistanceFromComponentCenter)
      {
        greatestAbsoluteXDistanceFromComponentCenter = absoluteXDistanceFromComponentCenter;
        mostHorizontallyExtremeJoint = alignmentPad;
        horizontalDirection = (int)Math.signum(xDistanceFromComponentCenter);
      }
    }

    // Add all "hoizontally extreme" and "vertically extreme" pads into list.
    List<JointInspectionData> mostHorizontallyExtremeJointList = new ArrayList<JointInspectionData>();    
    List<JointInspectionData> mostVerticallyExtremeJointList = new ArrayList<JointInspectionData>();
    mostHorizontallyExtremeJointList.add(mostHorizontallyExtremeJoint);
    mostVerticallyExtremeJointList.add(mostVerticallyExtremeJoint);
    Pad mostHorizontallyExtremeJointPad = mostHorizontallyExtremeJoint.getPad();
    PanelCoordinate mostHorizontallyExtremeJointPadCenter = mostHorizontallyExtremeJointPad.getCenterCoordinateRelativeToPanelInNanoMeters();
    Pad mostVerticallyExtremeJointPad = mostVerticallyExtremeJoint.getPad();
    PanelCoordinate mostVerticallyExtremeJointPadCenter = mostVerticallyExtremeJointPad.getCenterCoordinateRelativeToPanelInNanoMeters();
        
    for (JointInspectionData alignmentPad : alignmentPads)
    {
      Pad pad = alignmentPad.getPad();
      PanelCoordinate padCenter = pad.getCenterCoordinateRelativeToPanelInNanoMeters();
      if(padCenter.getX() == mostHorizontallyExtremeJointPadCenter.getX())
      {
        mostHorizontallyExtremeJointList.add(alignmentPad);
      }
      if(padCenter.getY() == mostVerticallyExtremeJointPadCenter.getY())
      {
        mostVerticallyExtremeJointList.add(alignmentPad);
      }
    }
    
    // Depending on the joint types and orientations we've encountered, make a decision on whether or not to
    // proceed with ghost pads.
    boolean needGhostPads = false;
    List<JointTypeEnum> jointTypesOnComponent = componentOfFirstAlignmentFeature.getJointTypeEnums();
    if (jointTypesOnComponent.contains(JointTypeEnum.CGA) ||
        jointTypesOnComponent.contains(JointTypeEnum.CHIP_SCALE_PACKAGE) ||
        jointTypesOnComponent.contains(JointTypeEnum.COLLAPSABLE_BGA) ||
        jointTypesOnComponent.contains(JointTypeEnum.NON_COLLAPSABLE_BGA) ||
        jointTypesOnComponent.contains(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) ||
        jointTypesOnComponent.contains(JointTypeEnum.PRESSFIT) ||
        jointTypesOnComponent.contains(JointTypeEnum.THROUGH_HOLE))
    {
      // We probably need ghost pads.
      needGhostPads = true;
    }
    else if (encounteredHorizontallyOrientedJoint && encounteredVerticallyOrientedJoint)
    {
      // We probably need ghost pads.
      needGhostPads = true;
    }
    else
    {
      // Probably DON'T need ghost pads.
      needGhostPads = false;
    }

    // If we've determined we don't need any ghost pads, we're done.
    if (needGhostPads == false)
    {
      return Collections.emptyList();
    }

    // Now create the list of "ghost ROIs".
    List<RegionOfInterest> ghostRois = new LinkedList<RegionOfInterest>();

    // Create a ghost ROI either one pitch left or one pitch right of the most "hoizontally extreme" joint in that column.
    if (mostHorizontallyExtremeJoint != null)
    {
      for(JointInspectionData joint:mostHorizontallyExtremeJointList)
      {
        int pitchInPixels = joint.getPitchInPixels();
        RegionOfInterest mostHorizontallyExtremeJointRoi =
            joint.getOrthogonalRegionOfInterestInPixels(false);
        RegionOfInterest horizontalGhostRoi = new RegionOfInterest(mostHorizontallyExtremeJointRoi);
        horizontalGhostRoi.translateXY(pitchInPixels * horizontalDirection, 0);

        ghostRois.add(horizontalGhostRoi);
      }
    }

    // Create a ghost ROI either one pitch up or one pitch down from the most "vertically extreme" joint in that row.
    if (mostVerticallyExtremeJoint != null)
    {
      for(JointInspectionData joint:mostVerticallyExtremeJointList)
      {
        int pitchInPixels = joint.getPitchInPixels();
        RegionOfInterest mostVerticallyExtremeJointRoi =
            joint.getOrthogonalRegionOfInterestInPixels(false);
        RegionOfInterest verticalGhostRoi = new RegionOfInterest(mostVerticallyExtremeJointRoi);
        verticalGhostRoi.translateXY(0, pitchInPixels * verticalDirection);

        ghostRois.add(verticalGhostRoi);
      }
    }

    // Prune out any ghost ROIs which don't fit in the image or overlap alignment features.
    ImageRectangle alignmentRegionImageRectangle = alignmentRegion.getRegionRectangleInPixels();
    Iterator<RegionOfInterest> ghostRoiIt = ghostRois.iterator();
    while (ghostRoiIt.hasNext())
    {
      RegionOfInterest candidateGhostRoi = ghostRoiIt.next();

      // Only add the ghost pad if it fits in the alignment region and does not overlap any actual alignment features.
      if (alignmentRegionImageRectangle.contains(candidateGhostRoi))
      {
        for (JointInspectionData alignmentPad : alignmentPads)
        {
          if (candidateGhostRoi.intersects(alignmentPad.getOrthogonalRegionOfInterestInPixels(false)))
          {
            ghostRoiIt.remove();
            break;
          }
        }
      }
      else
      {
        ghostRoiIt.remove();
      }
    }

    return ghostRois;
  }

  /**
   * @author Peter Esbensen
   */
  private boolean allAlignmentGroupsHaveBeenLocated(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    List<AlignmentGroup> alignmentGroups = new ArrayList<AlignmentGroup>();
    for (TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
    {
      alignmentGroups.addAll(testSubProgram.getAlignmentGroups());
    }

    return allAlignmentGroupsHaveBeenLocated(alignmentGroups) && allAlignmentGroupsHaveBeenLocatedForHighMag(alignmentGroups);
  }

  /**
   * @author Peter Esbensen
   */
  private boolean allAlignmentGroupsHaveBeenLocated(Collection<AlignmentGroup> alignmentGroups)
  {
    Assert.expect(alignmentGroups != null);

    for (AlignmentGroup alignmentGroup : alignmentGroups)
    {
      if (alignmentGroup.locatedCentroidExists() == false)
        return false;
    }
    return true;
  }

  /**
   * @author Peter Esbensen
   */
  private boolean allAlignmentGroupsHaveBeenLocatedForHighMag(Collection<AlignmentGroup> alignmentGroups)
  {
    Assert.expect(alignmentGroups != null);

    for (AlignmentGroup alignmentGroup : alignmentGroups)
    {
      if (alignmentGroup.locatedCentroidForHighMagExists() == false)
        return false;
    }
    return true;
  }

  /**
   * Return the AffineTransform needed to convert between the expected value
   * of three coordinates to the measured values of three coordinates.  In
   * other words, return the linear transform that converts:
   *
   * (expectedX1, expectedY1), (expectedX2, expectedY2), (expectedX3, expectedY3)
   *    to
   * (measuredX1, measuredY1), (measuredX2, measuredY2), (measuredX3, measuredY3)
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  private java.awt.geom.AffineTransform calculateAffineTransformFromThreeMeasuredPoints(
      double expectedX1, double expectedY1,
      double measuredX1, double measuredY1,
      double expectedX2, double expectedY2,
      double measuredX2, double measuredY2,
      double expectedX3, double expectedY3,
      double measuredX3, double measuredY3,
      BooleanRef singularMatrixGenerated)
  {
    Assert.expect(singularMatrixGenerated != null);

    // Set up the matrices.  We want to solve for x where Ax = B.
    Matrix aMatrix = new Matrix(3, 3);
    Matrix bMatrix = new Matrix(3, 2);

    // The "A" matrix contains the expected points and the "B" contains the measured points.

    // Set up the system of equations.
    int row = 0;
    aMatrix.set(row, 0, expectedX1);
    aMatrix.set(row, 1, expectedY1);
    aMatrix.set(row, 2, 1);
    bMatrix.set(0, 0, measuredX1);
    bMatrix.set(0, 1, measuredY1);

    row = 1;
    aMatrix.set(row, 0, expectedX2);
    aMatrix.set(row, 1, expectedY2);
    aMatrix.set(row, 2, 1);
    bMatrix.set(1, 0, measuredX2);
    bMatrix.set(1, 0, measuredX2);
    bMatrix.set(1, 1, measuredY2);

    row = 2;
    aMatrix.set(row, 0, expectedX3);
    aMatrix.set(row, 1, expectedY3);
    aMatrix.set(row, 2, 1);
    bMatrix.set(2, 0, measuredX3);
    bMatrix.set(2, 1, measuredY3);

    // Get the LU Decomposition of the "A" matrix.
    LUDecomposition luDecomposition = aMatrix.lu();

    // Make sure the matrix is solvable (i.e. "A" is non-singular).
    singularMatrixGenerated.setValue(luDecomposition.isNonsingular() == false);

    if (singularMatrixGenerated.getValue())
      return new java.awt.geom.AffineTransform();  // return an identity transform

    // Solve.
    Matrix xMatrix = luDecomposition.solve(bMatrix);

    // Package up the results in an AffineTransform and return.
    double[] affineTransformMatrixElements = new double[6];
    affineTransformMatrixElements[0] = xMatrix.get(0, 0);
    affineTransformMatrixElements[1] = xMatrix.get(0, 1);
    affineTransformMatrixElements[2] = xMatrix.get(1, 0);
    affineTransformMatrixElements[3] = xMatrix.get(1, 1);
    affineTransformMatrixElements[4] = xMatrix.get(2, 0);
    affineTransformMatrixElements[5] = xMatrix.get(2, 1);

    return new java.awt.geom.AffineTransform(affineTransformMatrixElements);
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  private java.awt.geom.AffineTransform computeAlignmentTransformForTestSubProgram(TestSubProgram testSubProgram) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);

    AlignmentGroup[] alignmentGroups = testSubProgram.getAlignmentGroups().toArray(new AlignmentGroup[0]);
    Assert.expect(alignmentGroups.length == _NUMBER_OF_ALIGNMENT_GROUPS_PER_ALIGNMENT);

    double[] expectedX = new double[_NUMBER_OF_ALIGNMENT_GROUPS_PER_ALIGNMENT];
    double[] expectedY = new double[_NUMBER_OF_ALIGNMENT_GROUPS_PER_ALIGNMENT];
    double[] measuredX = new double[_NUMBER_OF_ALIGNMENT_GROUPS_PER_ALIGNMENT];
    double[] measuredY = new double[_NUMBER_OF_ALIGNMENT_GROUPS_PER_ALIGNMENT];

    for (int i = 0; i < alignmentGroups.length; ++i)
    {
      AlignmentGroup alignmentGroup = alignmentGroups[i];
      IntCoordinate expectedCentroid = null;
      IntCoordinate locatedCentroid = null;
      
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      if (testSubProgram.getTestProgram().getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod())
        expectedCentroid = alignmentGroup.getExpectedDraggedImageCoordinateInNanometers();
      else
        expectedCentroid = alignmentGroup.getExpectedCentroidInNanometers();
      
      if(testSubProgram.isLowMagnification())
        locatedCentroid = alignmentGroup.getLocatedCentroidInNanometers();
      else
        locatedCentroid = alignmentGroup.getLocatedCentroidInNanometersForHighMag();
      expectedX[i] = expectedCentroid.getX();
      expectedY[i] = expectedCentroid.getY();
      measuredX[i] = locatedCentroid.getX();
      measuredY[i] = locatedCentroid.getY();
    }

    // Calculate the transform.
    BooleanRef singularMatrixGenerated = new BooleanRef(false);
    java.awt.geom.AffineTransform calculatedAlignmentTransform =
        calculateAffineTransformFromThreeMeasuredPoints(expectedX[0], expectedY[0],
                                                        measuredX[0], measuredY[0],
                                                        expectedX[1], expectedY[1],
                                                        measuredX[1], measuredY[1],
                                                        expectedX[2], expectedY[2],
                                                        measuredX[2], measuredY[2],
                                                        singularMatrixGenerated);

    // Make sure we didn't compute a singular matrix.
    if (singularMatrixGenerated.getValue() == true)
    {
      //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
      testSubProgram.setRunAlignmentLearned(false);
      throw new AlignmentAlgorithmFailedToCalculateTransformBusinessException(testSubProgram,
                                                                              alignmentGroups[0],
                                                                              alignmentGroups[1],
                                                                              alignmentGroups[2]);
    }

    return calculatedAlignmentTransform;
  }
  
  /**
   * Verifies that the alignment transform is within acceptable limits.  This is accomplished by:
   *
   * 1.  Taking the bounding PanelRectangle of all the inspection regions in the sub program and then taking the four
   *     corner points of that rectangle.
   * 2.  Running the expected panel coordinates through the currently defined "core" manual alignment transform
   *     and noting those updated coordinate values.
   * 3.  Running the expected panel coordinates through the specified calculated alignment transform
   *     and noting those updated coordinate values.
   * 4.  Taking the absolute deltas between the manual alignment and calculated transform corner point
   *     locations (in both x and y).
   * 5.  Verifying that those deltas are within the alignment uncertainty tolerance.
   *
   * @author Matt Wharton
   */
  private void verifyAlignmentTransformIsWithinAcceptableLimits(TestSubProgram testSubProgram,
                                                                java.awt.geom.AffineTransform calculatedAlignmentTransform) throws XrayTesterException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(calculatedAlignmentTransform != null);

    // Find the four corners of the "bounding box" of the TestSubProgram.
    PanelRectangle boundingBoxOfTestSubProgram = testSubProgram.getInspectionRegionsBoundsInNanoMeters();
    PanelCoordinate lowerLeftPanelCoordinate = new PanelCoordinate(boundingBoxOfTestSubProgram.getMinX(),
                                                                   boundingBoxOfTestSubProgram.getMinY());
    PanelCoordinate upperLeftPanelCoordinate = new PanelCoordinate(boundingBoxOfTestSubProgram.getMinX(),
                                                                   boundingBoxOfTestSubProgram.getMaxY());
    PanelCoordinate lowerRightPanelCoordinate = new PanelCoordinate(boundingBoxOfTestSubProgram.getMaxX(),
                                                                    boundingBoxOfTestSubProgram.getMinY());
    PanelCoordinate upperRightPanelCoordinate = new PanelCoordinate(boundingBoxOfTestSubProgram.getMaxX(),
                                                                    boundingBoxOfTestSubProgram.getMaxY());

    // Run each of the extents through the currently defined manual alignment transform.
    java.awt.geom.AffineTransform coreManualAlignmentTransform = testSubProgram.getCoreManualAlignmentTransform();
    java.awt.geom.Point2D transformedPoint = coreManualAlignmentTransform.transform(lowerLeftPanelCoordinate.getPoint2D(), null);
    PanelCoordinate lowerLeftPanelCoordinateFromManualAlignmentTransform =
      new PanelCoordinate((int)Math.round(transformedPoint.getX()),
                          (int)Math.round(transformedPoint.getY()));
    transformedPoint = coreManualAlignmentTransform.transform(upperLeftPanelCoordinate.getPoint2D(), null);
    PanelCoordinate upperLeftPanelCoordinateFromManualAlignmentTransform =
      new PanelCoordinate((int)Math.round(transformedPoint.getX()),
                          (int)Math.round(transformedPoint.getY()));
    transformedPoint = coreManualAlignmentTransform.transform(lowerRightPanelCoordinate.getPoint2D(), null);
    PanelCoordinate lowerRightPanelCoordinateFromManualAlignmentTransform =
      new PanelCoordinate((int)Math.round(transformedPoint.getX()),
                          (int)Math.round(transformedPoint.getY()));
    transformedPoint = coreManualAlignmentTransform.transform(upperRightPanelCoordinate.getPoint2D(), null);
    PanelCoordinate upperRightPanelCoordinateFromManualAlignmentTransform =
      new PanelCoordinate((int)Math.round(transformedPoint.getX()),
                          (int)Math.round(transformedPoint.getY()));

    // Run each of the extents through the calculated alignment transform.
    transformedPoint = calculatedAlignmentTransform.transform(lowerLeftPanelCoordinate.getPoint2D(), null);
    PanelCoordinate lowerLeftPanelCoordinateFromCalculatedTransform =
      new PanelCoordinate((int)Math.round(transformedPoint.getX()),
                          (int)Math.round(transformedPoint.getY()));
    transformedPoint = calculatedAlignmentTransform.transform(upperLeftPanelCoordinate.getPoint2D(), null);
    PanelCoordinate upperLeftPanelCoordinateFromCalculatedTransform =
      new PanelCoordinate((int)Math.round(transformedPoint.getX()),
                          (int)Math.round(transformedPoint.getY()));
    transformedPoint = calculatedAlignmentTransform.transform(lowerRightPanelCoordinate.getPoint2D(), null);
    PanelCoordinate lowerRightPanelCoordinateFromCalculatedTransform =
      new PanelCoordinate((int)Math.round(transformedPoint.getX()),
                          (int)Math.round(transformedPoint.getY()));
    transformedPoint = calculatedAlignmentTransform.transform(upperRightPanelCoordinate.getPoint2D(), null);
    PanelCoordinate upperRightPanelCoordinateFromCalculatedTransform =
      new PanelCoordinate((int)Math.round(transformedPoint.getX()),
                          (int)Math.round(transformedPoint.getY()));

    // Measure the absolute deltas between the extents from the manual alignment and calculated transforms.
    PanelCoordinate lowerLeftCornerDelta =
      new PanelCoordinate(Math.abs(lowerLeftPanelCoordinateFromManualAlignmentTransform.getX() - lowerLeftPanelCoordinateFromCalculatedTransform.getX()),
                          Math.abs(lowerLeftPanelCoordinateFromManualAlignmentTransform.getY() - lowerLeftPanelCoordinateFromCalculatedTransform.getY()));
    PanelCoordinate upperLeftCornerDelta =
      new PanelCoordinate(Math.abs(upperLeftPanelCoordinateFromManualAlignmentTransform.getX() - upperLeftPanelCoordinateFromCalculatedTransform.getX()),
                          Math.abs(upperLeftPanelCoordinateFromManualAlignmentTransform.getY() - upperLeftPanelCoordinateFromCalculatedTransform.getY()));
    PanelCoordinate lowerRightCornerDelta =
      new PanelCoordinate(Math.abs(lowerRightPanelCoordinateFromManualAlignmentTransform.getX() - lowerRightPanelCoordinateFromCalculatedTransform.getX()),
                          Math.abs(lowerRightPanelCoordinateFromManualAlignmentTransform.getY() - lowerRightPanelCoordinateFromCalculatedTransform.getY()));
    PanelCoordinate upperRightCornerDelta =
      new PanelCoordinate(Math.abs(upperRightPanelCoordinateFromManualAlignmentTransform.getX() - upperRightPanelCoordinateFromCalculatedTransform.getX()),
                          Math.abs(upperRightPanelCoordinateFromManualAlignmentTransform.getY() - upperRightPanelCoordinateFromCalculatedTransform.getY()));

    // Make sure that each delta (in both x and y) is within the alignment uncertainty tolerance.
    boolean alignmentWithinAcceptableLimits = true;
    final int ALIGNMENT_UNCERTAINTY_TOLERANCE = getAlignmentUncertaintyBorderInNanoMeters();
    if ((lowerLeftCornerDelta.getX() > ALIGNMENT_UNCERTAINTY_TOLERANCE)
        || (lowerLeftCornerDelta.getY() > ALIGNMENT_UNCERTAINTY_TOLERANCE))
    {
      alignmentWithinAcceptableLimits = false;
    }
    if ((upperLeftCornerDelta.getX() > ALIGNMENT_UNCERTAINTY_TOLERANCE)
        || (upperLeftCornerDelta.getY() > ALIGNMENT_UNCERTAINTY_TOLERANCE))
    {
      alignmentWithinAcceptableLimits = false;
    }
    if ((lowerRightCornerDelta.getX() > ALIGNMENT_UNCERTAINTY_TOLERANCE)
        || (lowerRightCornerDelta.getY() > ALIGNMENT_UNCERTAINTY_TOLERANCE))
    {
      alignmentWithinAcceptableLimits = false;
    }
    if ((upperRightCornerDelta.getX() > ALIGNMENT_UNCERTAINTY_TOLERANCE)
        || (upperRightCornerDelta.getY() > ALIGNMENT_UNCERTAINTY_TOLERANCE))
    {
      alignmentWithinAcceptableLimits = false;
    }

    // If the alignment is not within acceptable limits, throw an exception.
    if (alignmentWithinAcceptableLimits == false)
    {
      AlignmentGroup[] alignmentGroups = testSubProgram.getAlignmentGroups().toArray(new AlignmentGroup[0]);
      Assert.expect(alignmentGroups.length == _NUMBER_OF_ALIGNMENT_GROUPS_PER_ALIGNMENT);
      //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
      testSubProgram.setRunAlignmentLearned(false);
      throw new AlignmentAlgorithmResultOutOfBoundsBusinessException(testSubProgram,
                                                                     alignmentGroups[0],
                                                                     alignmentGroups[1],
                                                                     alignmentGroups[2]);
    }
  }

  /**
   * @author Matt Wharton
   */
  private void verifyUserSpecifiedAlignmentOffsetIsWithinAcceptableLimits(TestSubProgram testSubProgram,
                                                                          AlignmentGroup alignmentGroup) throws BusinessException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(alignmentGroup != null);

    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
//    java.awt.geom.Point2D expectedCoordinate = alignmentGroup.getExpectedCentroidInNanometers().getPoint2D();
    java.awt.geom.Point2D expectedCoordinate;
    if (testSubProgram.getTestProgram().isManual2DAlignmentInProgress())
      expectedCoordinate = alignmentGroup.getExpectedDraggedImageCoordinateInNanometers().getPoint2D();  
    else
      expectedCoordinate = alignmentGroup.getExpectedCentroidInNanometers().getPoint2D();
    
    java.awt.geom.Point2D locatedCoordinate = null;
    if(testSubProgram.isLowMagnification())
      locatedCoordinate = alignmentGroup.getLocatedCentroidInNanometers().getPoint2D();
    else
      locatedCoordinate = alignmentGroup.getLocatedCentroidInNanometersForHighMag().getPoint2D();

    // Make sure the the distance between the expected and located centroid is within reasonable limits.
    // For now, we'll use 1000% of the alignment uncertainty.
    int MAX_DISTANCE_BETWEEN_EXPECTED_AND_LOCATED_CENTROIDS_IN_NANOS = 10 * getAlignmentUncertaintyBorderInNanoMeters();
    double absoluteAlignmentOffsetDistanceInNanometers = expectedCoordinate.distance(locatedCoordinate);
    if (MathUtil.fuzzyGreaterThan(absoluteAlignmentOffsetDistanceInNanometers,
                                  MAX_DISTANCE_BETWEEN_EXPECTED_AND_LOCATED_CENTROIDS_IN_NANOS))
    {
      //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
      testSubProgram.setRunAlignmentLearned(false);
      throw new AlignmentGroupLocatedTooFarFromExpectedLocationBusinessException(testSubProgram, alignmentGroup);
    }
  }

  /**
   * @author Matt Wharton
   */
  public void saveFailingAlignmentImages(TestSubProgram testSubProgram, long timeStampInMillis) throws DatastoreException
  {
    Assert.expect(testSubProgram != null);

    Project project = testSubProgram.getTestProgram().getProject();
    String projectName = project.getName();
    PanelLocationInSystemEnum panelLocationInSystem = testSubProgram.getPanelLocationInSystem();
    boolean isSecondPortionOfLongPanel = panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT);

    // Create the failing alignment images directory if it doesn't already exist.
    String failingAlignmentImagesDir = Directory.getFailingAlignmentImagesDir();
    if (FileUtilAxi.existsDirectory(failingAlignmentImagesDir) == false)
    {
      FileUtilAxi.createDirectory(failingAlignmentImagesDir);
    }

    // Copy the failing alignment images over (if they exist).
    for (AlignmentGroup alignmentGroup : testSubProgram.getAlignmentGroups())
    {
      String alignmentGroupName = alignmentGroup.getName();

      String sourceFile = FileName.getAlignmentVerificationImageFullPath(projectName,
                                                                         alignmentGroupName,
                                                                         isSecondPortionOfLongPanel);
      if (FileUtilAxi.exists(sourceFile))
      {
        String destinationFile = FileName.getFailingAlignmentImageFullPath(project.getName(),
                                                                           alignmentGroupName,
                                                                           isSecondPortionOfLongPanel,
                                                                           timeStampInMillis);

        FileUtilAxi.copy(sourceFile, destinationFile);
      }

      sourceFile = FileName.getRawAlignmentImageFullPath(projectName, alignmentGroupName, isSecondPortionOfLongPanel);
      if (FileUtilAxi.exists(sourceFile))
      {
        String destinationFile = FileName.getRawFailingAlignmentImageFullPath(project.getName(),
                                                                              alignmentGroupName,
                                                                              isSecondPortionOfLongPanel,
                                                                              timeStampInMillis);

        FileUtilAxi.copy(sourceFile, destinationFile);
      }
    }
  }

  /**
   * @author Matt Wharton
   */
  public void saveCallRateSpikeAlignmentImages(TestProgram testProgram, long timeStampInMillis) throws DatastoreException
  {
    Assert.expect(testProgram != null);

    Project project = testProgram.getProject();
    String projectName = project.getName();

    for (TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
    {
      PanelLocationInSystemEnum panelLocationInSystem = testSubProgram.getPanelLocationInSystem();
      boolean isSecondPortionOfLongPanel = panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT);

      // Create the failing alignment images directory if it doesn't already exist.
      String callRateSpikeAlignmentImagesDir = Directory.getCallRateSpikeAlignmentImagesDir();
      if (FileUtilAxi.existsDirectory(callRateSpikeAlignmentImagesDir) == false)
      {
        FileUtilAxi.createDirectory(callRateSpikeAlignmentImagesDir);
      }

      // Copy the failing alignment images over (if they exist).
      for (AlignmentGroup alignmentGroup : testSubProgram.getAlignmentGroups())
      {
        String alignmentGroupName = alignmentGroup.getName();

        String sourceFile = FileName.getAlignmentVerificationImageFullPath(projectName,
                                                                           alignmentGroupName,
                                                                           isSecondPortionOfLongPanel);
        if (FileUtilAxi.exists(sourceFile))
        {
          String destinationFile = FileName.getCallRateSpikeAlignmentImageFullPath(project.getName(),
                                                                                   alignmentGroup.getName(),
                                                                                   isSecondPortionOfLongPanel,
                                                                                   timeStampInMillis);

          FileUtilAxi.copy(sourceFile, destinationFile);
        }
      }
    }
  }

  /**
   * @author Matt Wharton
   */
  private void dumpFailingAlignmentImageCSV(Image failingAlignmentImage,
                                            ReconstructionRegion failingAlignmentRegion) throws DatastoreException
  {
    Assert.expect(failingAlignmentImage != null);
    Assert.expect(failingAlignmentRegion != null);

    TestSubProgram testSubProgram = failingAlignmentRegion.getTestSubProgram();
    TestProgram testProgram = testSubProgram.getTestProgram();
    Project project = testProgram.getProject();
    PanelLocationInSystemEnum panelLocationInSystem = testSubProgram.getPanelLocationInSystem();
    boolean isSecondPortionOfLongPanel = panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT);
    AlignmentGroup alignmentGroup = failingAlignmentRegion.getAlignmentGroup();
    String failingAlignmentImageCSVPath = FileName.getFailingAlignmentImageCSVFullPath(project.getName(),
                                                                                       alignmentGroup.getName(),
                                                                                       isSecondPortionOfLongPanel,
                                                                                       System.currentTimeMillis());
    PrintStream csvStream = null;
    try
    {
      csvStream = new PrintStream(failingAlignmentImageCSVPath);
    }
    catch (IOException iex)
    {
      throw new CannotOpenFileDatastoreException(failingAlignmentImageCSVPath);
    }

    Assert.expect(csvStream != null);
    failingAlignmentImage.printImageCSV("Alignment group " + alignmentGroup.getName(), csvStream);
  }

  /**
   * @author Matt Wharton
   */
  private void saveRawAlignmentImage(Image alignmentImage, ReconstructionRegion alignmentRegion) throws DatastoreException
  {
    Assert.expect(alignmentImage != null);
    Assert.expect(alignmentRegion != null);

    TestSubProgram testSubProgram = alignmentRegion.getTestSubProgram();
    TestProgram testProgram = testSubProgram.getTestProgram();
    Project project = testProgram.getProject();
    AlignmentGroup alignmentGroup = alignmentRegion.getAlignmentGroup();
    PanelLocationInSystemEnum panelLocationInSystem = testSubProgram.getPanelLocationInSystem();
    boolean isSecondPortionOfLongPanel = panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT);
    String rawAlignmentImagePath = FileName.getRawAlignmentImageFullPath(project.getName(),
                                                                         alignmentGroup.getName(),
                                                                         isSecondPortionOfLongPanel);

    XrayImageIoUtil.savePngImage(alignmentImage.getBufferedImage(), rawAlignmentImagePath);
  }

  /**
   * @author Matt Wharton
   */
  private void saveRawManualAlignmentImage(Image alignmentImage, ReconstructionRegion alignmentRegion) throws DatastoreException
  {
    Assert.expect(alignmentImage != null);
    Assert.expect(alignmentRegion != null);

    TestSubProgram testSubProgram = alignmentRegion.getTestSubProgram();
    TestProgram testProgram = testSubProgram.getTestProgram();
    Project project = testProgram.getProject();
    AlignmentGroup alignmentGroup = alignmentRegion.getAlignmentGroup();
    PanelLocationInSystemEnum panelLocationInSystem = testSubProgram.getPanelLocationInSystem();
    boolean isSecondPortionOfLongPanel = panelLocationInSystem.equals(PanelLocationInSystemEnum.LEFT);
    String rawAlignmentImagePath = FileName.getRawManualAlignmentImageFullPath(project.getName(),
                                                                               alignmentGroup.getName(),
                                                                               isSecondPortionOfLongPanel);

    XrayImageIoUtil.savePngImage(alignmentImage.getBufferedImage(), rawAlignmentImagePath);
  }

  /**
   * @author George A. David
   */
  public static int getAlignmentUncertaintyBorderInNanoMeters()
  {
    return _config.getIntValue(SoftwareConfigEnum.PANEL_ALIGNMENT_UNCERTAINTY_BORDER_IN_NANOMETERS);
  }

  /**
   * @author George A. David
   */
  public static int getAlignmentUncertaintyAfterAutomaticAlignmentInNanoMeters()
  {
    return _config.getIntValue(SoftwareConfigEnum.PANEL_ALIGNMENT_UNCERTAINTY_BORDER_AFTER_AUTOMATIC_ALIGNMENT_IN_NANOMETERS);
  }

  /**
   * @author Matt Wharton
   */
  private static double getMinimumPercentCoverageOfImageableAreaForAlignmentGroups()
  {
    return _config.getDoubleValue(SoftwareConfigEnum.MINIMUM_PERCENT_COVERAGE_OF_IMAGEABLE_AREA_FOR_ALIGNMENT_GROUPS);
  }

  /**
   * @author Matt Wharton
   */
  private static int getMaximumBorderForSingleSidedAlignmentRegionTestInNanoMeters()
  {
    return _config.getIntValue(SoftwareConfigEnum.MAXIMUM_BORDER_FOR_SINGLE_SIDED_ALIGNMENT_REGION_TEST_IN_NANOMETERS);
  }

  /**
   * @author Matt Wharton
   */
  private static double getMinimumAlignmentRegionDensityPercentage()
  {
    return _config.getDoubleValue(SoftwareConfigEnum.MINIMUM_ALIGNMENT_REGION_DENSITY_PERCENTAGE);
  }

  /**
   * @author Matt Wharton
   */
  private static double getMaximumAlignmentRegionPercentageOccupiedByBGAs()
  {
    return _config.getDoubleValue(SoftwareConfigEnum.MAXIMUM_ALIGNMENT_REGION_PERCENTAGE_OCCUPIED_BY_BGAS);
  }

  /**
   * @author Matt Wharton
   */
  private static int getAlignmentRegionAreaArrayProximityMarginInNanoMeters()
  {
    return _config.getIntValue(SoftwareConfigEnum.ALIGNMENT_REGION_AREA_ARRAY_PROXIMITY_MARGIN_IN_NANOMETERS);
  }

  /**
   * @author Matt Wharton
   */
  private static double getMaximumAlignmentFeatureBoundingBoxPercentageOccupiedByProblematicLeadFrames()
  {
    return _config.getDoubleValue(SoftwareConfigEnum.MAXIMUM_ALIGNMENT_FEATURE_BOUNDING_BOX_PERCENTAGE_OCCUPIED_BY_PROBLEMATIC_LEAD_FRAMES);
  }

  /**
   * @author Matt Wharton
   */
  private static double getMaximumAlignmentBackgroundGrayLevelError()
  {
    return _config.getDoubleValue(SoftwareConfigEnum.MAXIMUM_ALIGNMENT_BACKGROUND_GRAY_LEVEL_ERROR);
  }

  /**
   * @author Matt Wharton
   */
  private static double getMaximumAlignmentDeltaGrayLevelErrorFactor()
  {
    return _config.getDoubleValue(SoftwareConfigEnum.MAXIMUM_ALIGNMENT_DELTA_GRAY_LEVEL_ERROR_FACTOR);
  }

  /**
   * @author Matt Wharton
   */
  private static boolean isDetailedAlignmentFailureLoggingEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.LOG_DETAILED_ALIGNMENT_FAILURE_DATA);
  }
  
  /**
   * Converts the low mag to high mag
   *
   * @author Chong Wei Chin
   */
  public static java.awt.geom.AffineTransform convertToHighMagnificationTransform(java.awt.geom.AffineTransform lowMagnificationAffineTransform)
  {
    Assert.expect(lowMagnificationAffineTransform != null);
    
    java.awt.geom.AffineTransform highMagnificationAffineTransform = new java.awt.geom.AffineTransform();    
    highMagnificationAffineTransform.preConcatenate(lowMagnificationAffineTransform);
    
//    double referencePlaneHighMagnification = Config.getInstance().getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION);
//    double referencePlaneLowMagnification = Config.getInstance().getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION);
//    double scaleFactor = referencePlaneHighMagnification / referencePlaneLowMagnification;
//    highMagnificationAffineTransform.scale(scaleFactor, scaleFactor);
    
//    int xRaySpotInX = Config.getInstance().getIntValue(HardwareCalibEnum.XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
//    int xRaySpotInY = Config.getInstance().getIntValue(HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
//    int xRaySpotInXForHighMag = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
//    int xRaySpotInYForHighMag = Config.getInstance().getIntValue(HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
//    int xRayOffsetInX = xRaySpotInX - xRaySpotInXForHighMag;
//    int xRayOffsetInY = xRaySpotInY - xRaySpotInYForHighMag;
//    
//    System.out.println("xRaySpotInX : " + xRaySpotInX);
//    System.out.println("xRaySpotInY : " + xRaySpotInY);
//    System.out.println("xRaySpotInXForHighMag : " + xRaySpotInXForHighMag);
//    System.out.println("xRaySpotInYForHighMag : " + xRaySpotInYForHighMag);
//    System.out.println("xRayOffsetInX : " + xRayOffsetInX);
//    System.out.println("xRayOffsetInY : " + xRayOffsetInY);
//    
//    highMagnificationAffineTransform.translate(-xRayOffsetInX/2, -xRayOffsetInY/2);

    System.out.println("Convert To High");
    return highMagnificationAffineTransform;
  }

  /**
   * Generates an alignment template for the specified alignment region and attempts to
   * match that template against the specified alignment image.  Returns the ImageCoordinate with
   * the strongest match.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  private ImageCoordinate locatePatternByShapeMatchingAlgorithm(java.awt.image.BufferedImage alignmentImage, List<java.awt.Shape> alignmentTemplateShapeInPixelsList,java.awt.Rectangle searchMaxBound,
          DoubleRef matchQuality,Map<AlignmentExcludedAreaEnum,java.awt.Shape> alignmentExcludedAreaInTemplateRoiList) throws XrayTesterException
  {
    Assert.expect(alignmentTemplateShapeInPixelsList.size() > 0);
    Assert.expect(matchQuality != null);

    // Match our template against the alignment image.
    Pair<Double,ImageCoordinate> upperLeftCornerOfMatchLocationList = ShapeMatching.getInstance().matchTemplateByShapeList(alignmentImage, alignmentTemplateShapeInPixelsList,searchMaxBound,alignmentExcludedAreaInTemplateRoiList);
    matchQuality.setValue(upperLeftCornerOfMatchLocationList.getFirst());
//    System.out.println("upperLeftCornerOfMatchLocationList :"+ upperLeftCornerOfMatchLocationList.getSecond());
    if(upperLeftCornerOfMatchLocationList.getSecond().getX() < 0 || upperLeftCornerOfMatchLocationList.getSecond().getY() < 0)
    {
      return new ImageCoordinate(0,0);
    }

    return upperLeftCornerOfMatchLocationList.getSecond();
  }

  /**
   * Based upon the specified alignment region, generates a template image to match against the features
   * in the alignment region.
   *
   * @author Matt Wharton
   */
  private Pair<RegionOfInterest,Image> generateAlignmentTemplateImageForShapeMatchingAlgorithm(Image alignmentImage,
          ReconstructionRegion alignmentRegion,
          List<RegionOfInterest> alignmentFeaturesInTemplateImage,
          List<RegionOfInterest> alignmentContextFeaturesInTemplateImage,
          List<java.awt.Shape> alignmentFeaturesWithinBoundInTemplateRoiList,
          Map<AlignmentExcludedAreaEnum,java.awt.Shape> alignmentExcludedAreaInTemplateRoiList) throws DatastoreException
  {
    Assert.expect(alignmentImage != null);
    Assert.expect(alignmentRegion != null);
    Assert.expect(alignmentRegion.isAlignmentRegion());
    Assert.expect(alignmentFeaturesInTemplateImage != null);
    Assert.expect(alignmentContextFeaturesInTemplateImage != null);
    Assert.expect(alignmentFeaturesWithinBoundInTemplateRoiList != null);
    Assert.expect(alignmentExcludedAreaInTemplateRoiList != null);

    // Figure out whether we're trying to lock onto dark or light features.
    AlignmentGroup alignmentGroup = alignmentRegion.getAlignmentGroup();
    boolean alignOnDarkPadsWithLightBackgrounds = alignmentGroup.hasDarkPadsWithLightBackground();
    float templateDontCareValue = 0f;
    float templateDarkValue = 0f;
    float templateLightValue = 0f;

    // Build a bounding box around the alignment pads and/or fiducials.
    List<JointInspectionData> alignmentPads = alignmentRegion.getJointInspectionDataList();
    List<FiducialInspectionData> alignmentFiducials = alignmentRegion.getFiducialInspectionDataList();
    List<Feature> alignmentFeatures = new LinkedList<Feature>(alignmentPads);
    alignmentFeatures.addAll(alignmentFiducials);
    List<Feature> alignmentFeaturesAndContextFeatures = new LinkedList<Feature>(alignmentFeatures);
    Collection<JointInspectionData> alignmentContextFeatures = alignmentRegion.getAlignmentContextFeatures();
    alignmentFeaturesAndContextFeatures.addAll(alignmentContextFeatures);
    RegionOfInterest alignmentFeaturesBoundingRegionInPixels = createAlignmentTemplateWithinBoundFeturesBoundingBoxForShapeMatchingAlgorithm(alignmentFeaturesAndContextFeatures);


    // Clear out the list of alignment features and context features in the template image.
    alignmentFeaturesInTemplateImage.clear();
    alignmentContextFeaturesInTemplateImage.clear();

    // Fill in the joint/fiducial ROIs (and border) with the applicable foreground and background colors.
    int templateImageXOffsetInAlignmentImage = alignmentFeaturesBoundingRegionInPixels.getMinX();
    int templateImageYOffsetInAlignmentImage = alignmentFeaturesBoundingRegionInPixels.getMinY();
    // Fill in the alignment features and context features with the applicable foreground and background colors.
    for (Feature feature : alignmentFeaturesAndContextFeatures)
    {
      // The alignment pad ROIs are in terms of the whole alignment image.  We have to translate the coordinates
      // to be relative to the origin of the smaller template image.
      RegionOfInterest alignmentOrContextFeatureRoiInTemplateImage = null;
      RegionOfInterest alignmentOrContextFeatureRoiInTemplateImagePin2 = null;
      boolean isContextPad = false;
      if(feature instanceof JointInspectionData)
      {
        JointInspectionData alignmentOrContextPad = (JointInspectionData) feature;
        if(ImageAnalysis.isSpecialTwoPinComponent(alignmentOrContextPad.getJointTypeEnum()))
        {
          if(alignmentOrContextPad.getJointTypeEnum().equals(JointTypeEnum.RESISTOR))
          {
            // Handle resistor type only
            Assert.expect(alignmentOrContextPad.getComponentInspectionData().getJointInspectionDataSet().size() == 2);
            Iterator<JointInspectionData> jointIterator = alignmentOrContextPad.getComponentInspectionData().getJointInspectionDataSet().iterator();
            alignmentOrContextFeatureRoiInTemplateImage = new RegionOfInterest(jointIterator.next().getOrthogonalRegionOfInterestInPixels(false));
            alignmentOrContextFeatureRoiInTemplateImagePin2 = new RegionOfInterest(jointIterator.next().getOrthogonalRegionOfInterestInPixels(false));
          }
          else
          {
            ComponentInspectionData componentInspectionData = alignmentOrContextPad.getComponentInspectionData();
            alignmentOrContextFeatureRoiInTemplateImage = new RegionOfInterest(componentInspectionData.getOrthogonalComponentRegionOfInterest(false));
          }
        }
        else
        {
          alignmentOrContextFeatureRoiInTemplateImage = new RegionOfInterest(alignmentOrContextPad.getOrthogonalRegionOfInterestInPixels(false));
        }

        isContextPad = alignmentContextFeatures.contains(alignmentOrContextPad);
      }
      else if(feature instanceof FiducialInspectionData)
      {
        FiducialInspectionData alignmentFiducial = (FiducialInspectionData) feature;
        alignmentOrContextFeatureRoiInTemplateImage = new RegionOfInterest(alignmentFiducial.getRegionOfInterestRelativeToInspectionRegionInPixels());
      }
      else
      {
        Assert.expect(false,"Unexpected feature type: " + feature);
      }

      alignmentOrContextFeatureRoiInTemplateImage.translateXY(-templateImageXOffsetInAlignmentImage,-templateImageYOffsetInAlignmentImage);
      // Handle resistor type only
      if(alignmentOrContextFeatureRoiInTemplateImagePin2 != null)
      {
        alignmentOrContextFeatureRoiInTemplateImagePin2.translateXY(-templateImageXOffsetInAlignmentImage,-templateImageYOffsetInAlignmentImage);
      }

      // Fill in the border region around the feature with the "light" value and use the "dark" value on the
      // feature itself.
      RegionOfInterest alignmentPadRoiWithBorderInTemplateImage = new RegionOfInterest(alignmentOrContextFeatureRoiInTemplateImage);
      alignmentPadRoiWithBorderInTemplateImage.setWidthKeepingSameCenter(
              alignmentPadRoiWithBorderInTemplateImage.getWidth());// + (2 * _ALIGNMENT_FEATURE_BORDER_WIDTH));
      alignmentPadRoiWithBorderInTemplateImage.setHeightKeepingSameCenter(
              alignmentPadRoiWithBorderInTemplateImage.getHeight());// + (2 * _ALIGNMENT_FEATURE_BORDER_WIDTH));
      RegionOfInterest alignmentPadRoiWithBorderInTemplateImagePin2 = null;
      // Handle second pad of special two pins components
      if(alignmentOrContextFeatureRoiInTemplateImagePin2 != null)
      {
        alignmentPadRoiWithBorderInTemplateImagePin2 = new RegionOfInterest(alignmentOrContextFeatureRoiInTemplateImagePin2);
        alignmentPadRoiWithBorderInTemplateImagePin2.setWidthKeepingSameCenter(
                alignmentPadRoiWithBorderInTemplateImagePin2.getWidth());// + (2 * _ALIGNMENT_FEATURE_BORDER_WIDTH));
        alignmentPadRoiWithBorderInTemplateImagePin2.setHeightKeepingSameCenter(
                alignmentPadRoiWithBorderInTemplateImagePin2.getHeight());// + (2 * _ALIGNMENT_FEATURE_BORDER_WIDTH));
      }

      if(isContextPad == false)
      {
        alignmentFeaturesInTemplateImage.add(alignmentOrContextFeatureRoiInTemplateImage);
        // Handle second pad of special two pins components
        if(alignmentOrContextFeatureRoiInTemplateImagePin2 != null)
        {
          alignmentFeaturesInTemplateImage.add(alignmentOrContextFeatureRoiInTemplateImagePin2);
        }
      }
      else
      {
        alignmentContextFeaturesInTemplateImage.add(alignmentOrContextFeatureRoiInTemplateImage);
        // Handle second pads of special two pins components
        if(alignmentOrContextFeatureRoiInTemplateImagePin2 != null)
        {
          alignmentContextFeaturesInTemplateImage.add(alignmentOrContextFeatureRoiInTemplateImagePin2);
        }
      }
    }

    //Create feature list for shape matching algorithm
    List<JointInspectionData> alignmentPadsWithinBound = alignmentRegion.getJointInspectionDataWithinBoundList();
    List<Feature> alignmentFeaturesWithinBound = new LinkedList<Feature>(alignmentPadsWithinBound);
    RegionOfInterest alignmentWithinBoundFeaturesBoundingRegionInPixels = null;//template region of interest exclude padding
    java.awt.Rectangle padsMaxBoundWithPaddingRectangle = null;//alignment region rectangle
    java.awt.Rectangle padsMaxBoundWithoutPaddingRectangle = null;//template rectangle exclude padding
    final double NANOMETERS_PER_PIXEL = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
    final int ALIGNMENT_UNCERTAINTY_BORDER_IN_PIXELS = (int) Math.round(getAlignmentUncertaintyBorderInNanoMeters() / NANOMETERS_PER_PIXEL);// + 1;
    //use for searching the pad in padding area, in nanometer and relative to panel
    java.awt.Rectangle leftExcludedAreaInNanoMeter = new java.awt.Rectangle(alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinX(),
            alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinY(),
            getAlignmentUncertaintyBorderInNanoMeters(),
            alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters().getHeight());
    java.awt.Rectangle rightExcludedAreaInNanoMeter = new java.awt.Rectangle(alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMaxX() - getAlignmentUncertaintyBorderInNanoMeters(),
            alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinY(),
            getAlignmentUncertaintyBorderInNanoMeters(),
            alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters().getHeight());
    java.awt.Rectangle topExcludedAreaInNanoMeter = new java.awt.Rectangle(alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinX(),
            alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMaxY() - getAlignmentUncertaintyBorderInNanoMeters(),
            alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters().getWidth(),
            getAlignmentUncertaintyBorderInNanoMeters());
    java.awt.Rectangle bottomExcludedAreaInNanoMeter = new java.awt.Rectangle(alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinX(),
            alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters().getMinY(),
            alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters().getWidth(),
            getAlignmentUncertaintyBorderInNanoMeters());
    //use for matching algorithm, in pixels(image coordinate) and relative to alignment region
    RegionOfInterest leftExcludedAreaInPixels = new RegionOfInterest(alignmentRegion.getOrthogonalRegionRectangleInPixels().getMinX(),
            alignmentRegion.getOrthogonalRegionRectangleInPixels().getMinY() + ALIGNMENT_UNCERTAINTY_BORDER_IN_PIXELS,
            ALIGNMENT_UNCERTAINTY_BORDER_IN_PIXELS,
            alignmentRegion.getOrthogonalRegionRectangleInPixels().getHeight() - 2 * ALIGNMENT_UNCERTAINTY_BORDER_IN_PIXELS ,
            0,RegionShapeEnum.RECTANGULAR);
    RegionOfInterest rightExcludedAreaInPixels = new RegionOfInterest(alignmentRegion.getOrthogonalRegionRectangleInPixels().getMaxX() - ALIGNMENT_UNCERTAINTY_BORDER_IN_PIXELS,
            alignmentRegion.getOrthogonalRegionRectangleInPixels().getMinY() + ALIGNMENT_UNCERTAINTY_BORDER_IN_PIXELS,
            ALIGNMENT_UNCERTAINTY_BORDER_IN_PIXELS,
            alignmentRegion.getOrthogonalRegionRectangleInPixels().getHeight() - 2 * ALIGNMENT_UNCERTAINTY_BORDER_IN_PIXELS,0,RegionShapeEnum.RECTANGULAR);
    RegionOfInterest topExcludedAreaInPixels = new RegionOfInterest(alignmentRegion.getOrthogonalRegionRectangleInPixels().getMinX(),
            alignmentRegion.getOrthogonalRegionRectangleInPixels().getMinY(),
            alignmentRegion.getOrthogonalRegionRectangleInPixels().getWidth() - 1,
            ALIGNMENT_UNCERTAINTY_BORDER_IN_PIXELS,0,RegionShapeEnum.RECTANGULAR);
    RegionOfInterest bottomExcludedAreaInPixels = new RegionOfInterest(alignmentRegion.getOrthogonalRegionRectangleInPixels().getMinX(),
            alignmentRegion.getOrthogonalRegionRectangleInPixels().getMaxY() - ALIGNMENT_UNCERTAINTY_BORDER_IN_PIXELS,
            alignmentRegion.getOrthogonalRegionRectangleInPixels().getWidth() - 1,
            ALIGNMENT_UNCERTAINTY_BORDER_IN_PIXELS,0,RegionShapeEnum.RECTANGULAR);

    List<Component> componentList = new LinkedList<Component>();


    //clear list
    alignmentFeaturesWithinBoundInTemplateRoiList.clear();
    alignmentExcludedAreaInTemplateRoiList.clear();

    //Define template image offset relative to alignment region
    alignmentWithinBoundFeaturesBoundingRegionInPixels = createAlignmentTemplateWithinBoundFeturesBoundingBoxForShapeMatchingAlgorithm(alignmentFeaturesAndContextFeatures);//alignmentFeaturesWithinBound);
    padsMaxBoundWithPaddingRectangle = alignmentRegion.getRegionRectangleInPixels().getRectangle2D().getBounds();
    padsMaxBoundWithoutPaddingRectangle = alignmentWithinBoundFeaturesBoundingRegionInPixels.getRectangle2D().getBounds();
    templateImageXOffsetInAlignmentImage = alignmentWithinBoundFeaturesBoundingRegionInPixels.getMinX();
    templateImageYOffsetInAlignmentImage = alignmentWithinBoundFeaturesBoundingRegionInPixels.getMinY();
    leftExcludedAreaInPixels.translateXY(-templateImageXOffsetInAlignmentImage,-templateImageYOffsetInAlignmentImage);
    rightExcludedAreaInPixels.translateXY(-templateImageXOffsetInAlignmentImage,-templateImageYOffsetInAlignmentImage);
    topExcludedAreaInPixels.translateXY(-templateImageXOffsetInAlignmentImage,-templateImageYOffsetInAlignmentImage);
    bottomExcludedAreaInPixels.translateXY(-templateImageXOffsetInAlignmentImage,-templateImageYOffsetInAlignmentImage);
    alignmentExcludedAreaInTemplateRoiList.put(AlignmentExcludedAreaEnum.LEFT,leftExcludedAreaInPixels.getRectangle2D().getBounds());
    alignmentExcludedAreaInTemplateRoiList.put(AlignmentExcludedAreaEnum.RIGHT,rightExcludedAreaInPixels.getRectangle2D().getBounds());
    alignmentExcludedAreaInTemplateRoiList.put(AlignmentExcludedAreaEnum.TOP,topExcludedAreaInPixels.getRectangle2D().getBounds());
    alignmentExcludedAreaInTemplateRoiList.put(AlignmentExcludedAreaEnum.BOTTOM,bottomExcludedAreaInPixels.getRectangle2D().getBounds());
    //Define exclude area
    for (Feature feature : alignmentPadsWithinBound)
    {
      Pad pad = ((JointInspectionData) feature).getPad();
      if(leftExcludedAreaInNanoMeter.getWidth() > 0)
      {
        if(leftExcludedAreaInNanoMeter.intersects(pad.getPanelRectangleWithLowerLeftOriginInNanoMeters().getRectangle2D().getBounds()))
        {
          alignmentExcludedAreaInTemplateRoiList.remove("left");
          leftExcludedAreaInNanoMeter = new java.awt.Rectangle(0,0,0,0);
        }
      }
      //check right
      if(rightExcludedAreaInNanoMeter.getWidth() > 0)
      {
        if(rightExcludedAreaInNanoMeter.intersects(pad.getPanelRectangleWithLowerLeftOriginInNanoMeters().getRectangle2D().getBounds()))
        {
          alignmentExcludedAreaInTemplateRoiList.remove("right");
          rightExcludedAreaInNanoMeter = new java.awt.Rectangle(0,0,0,0);
        }
      }
      //check top
      if(topExcludedAreaInNanoMeter.getWidth() > 0)
      {
        if(topExcludedAreaInNanoMeter.intersects(pad.getPanelRectangleWithLowerLeftOriginInNanoMeters().getRectangle2D().getBounds()))
        {
          alignmentExcludedAreaInTemplateRoiList.remove("top");
          topExcludedAreaInNanoMeter = new java.awt.Rectangle(0,0,0,0);
        }
      }

      //check bottom
      if(bottomExcludedAreaInNanoMeter.getWidth() > 0)
      {
        if(bottomExcludedAreaInNanoMeter.intersects(pad.getPanelRectangleWithLowerLeftOriginInNanoMeters().getRectangle2D().getBounds()))
        {
          alignmentExcludedAreaInTemplateRoiList.remove("bottom");
          bottomExcludedAreaInNanoMeter = new java.awt.Rectangle(0,0,0,0);
        }
      }
    }
    for (Feature feature : alignmentFeaturesWithinBound)
    {
      // The alignment pad ROIs are in terms of the whole alignment image.  We have to translate the coordinates
      // to be relative to the origin of the smaller template image.
      RegionOfInterest alignmentOrContextFeatureRoiInTemplateImage = null;
      RegionOfInterest alignmentOrContextFeatureRoiInTemplateImageComponent = null;
      int index = -1;
      if(feature instanceof JointInspectionData)
      {
        JointInspectionData alignmentOrContextPad = (JointInspectionData) feature;
        if(ImageAnalysis.isSpecialTwoPinComponent(alignmentOrContextPad.getJointTypeEnum()) && alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters().contains(alignmentOrContextPad.getComponent().getShapeRelativeToPanelInNanoMeters().getBounds()))
        {
          index = componentList.indexOf(alignmentOrContextPad.getComponent());
          if(index == -1)
          {
            componentList.add(alignmentOrContextPad.getComponent());
            ComponentInspectionData componentInspectionData = alignmentOrContextPad.getComponentInspectionData();
            alignmentOrContextFeatureRoiInTemplateImageComponent = new RegionOfInterest(componentInspectionData.getOrthogonalComponentRegionOfInterest(false));
          }

          alignmentOrContextFeatureRoiInTemplateImage = new RegionOfInterest(alignmentOrContextPad.getOrthogonalRegionOfInterestInPixels(false));
        }
        else
        {
          if(alignmentOrContextPad.getPad().getShapeEnum().equals(ShapeEnum.CIRCLE))
          {
            ImageRectangle rectInPixels = new ImageRectangle(alignmentOrContextPad.getOrthogonalRegionOfInterestInPixels(false).getMinX(),
                    alignmentOrContextPad.getOrthogonalRegionOfInterestInPixels(false).getMinY(),
                    alignmentOrContextPad.getOrthogonalRegionOfInterestInPixels(false).getLengthAcross(),alignmentOrContextPad.getOrthogonalRegionOfInterestInPixels(false).getHeight());
            alignmentOrContextFeatureRoiInTemplateImage = new RegionOfInterest(rectInPixels,0,RegionShapeEnum.RECTANGULAR);
          }
          else
          {
            alignmentOrContextFeatureRoiInTemplateImage = new RegionOfInterest(alignmentOrContextPad.getOrthogonalRegionOfInterestInPixels(false));
          }
        }
      }
      else
      {
        Assert.expect(false,"Unexpected feature type: " + feature);
      }
      alignmentOrContextFeatureRoiInTemplateImage.translateXY(-templateImageXOffsetInAlignmentImage,-templateImageYOffsetInAlignmentImage);
      alignmentFeaturesWithinBoundInTemplateRoiList.add(alignmentOrContextFeatureRoiInTemplateImage.getShape());
      // Handle second pad of special two pins components
      if(alignmentOrContextFeatureRoiInTemplateImageComponent != null)
      {
        alignmentOrContextFeatureRoiInTemplateImageComponent.translateXY(-templateImageXOffsetInAlignmentImage,-templateImageYOffsetInAlignmentImage);
        alignmentFeaturesWithinBoundInTemplateRoiList.add(alignmentOrContextFeatureRoiInTemplateImageComponent.getShape());
        alignmentOrContextFeatureRoiInTemplateImageComponent = null;
      }
    }
    componentList.clear();
    Image templateImage=null;
    if(_DEVELOPER_DEBUG)
    {
      /**
       * Dump out template image
       */
      java.awt.Rectangle templateBound = new java.awt.Rectangle(0,0,0,0);
      RegionOfInterest templateRegionOfIntrest;
      for (java.awt.Shape sp : alignmentFeaturesWithinBoundInTemplateRoiList)
      {
        templateBound.add(sp.getBounds());
      }
      for (java.awt.Shape sp : alignmentExcludedAreaInTemplateRoiList.values())
      {
        templateBound.add(sp.getBounds());
      }
      int offsetX = (int) Math.abs(templateBound.getMinX()) - 0;
      int offsetY = (int) Math.abs(templateBound.getMinY()) - 0;
      templateImage = new Image((int) templateBound.getWidth(),(int) templateBound.getHeight());
      Paint.fillImage(templateImage,250);

      for (java.awt.Shape sp : alignmentFeaturesWithinBoundInTemplateRoiList)
      {
        templateRegionOfIntrest = new RegionOfInterest((int) sp.getBounds().getMinX(),(int) sp.getBounds().getMinY(),(int) sp.getBounds().getWidth(),(int) sp.getBounds().getHeight(),0,RegionShapeEnum.RECTANGULAR);
        templateRegionOfIntrest.translateXY(+offsetX,+offsetY);
        Paint.fillRegion(templateImage,templateRegionOfIntrest.getRectangle2D().getBounds(),50);

      }

      for (java.awt.Shape sp : alignmentExcludedAreaInTemplateRoiList.values())
      {
        templateRegionOfIntrest = new RegionOfInterest((int) sp.getBounds().getMinX(),(int) sp.getBounds().getMinY(),(int) sp.getBounds().getWidth(),(int) sp.getBounds().getHeight(),0,RegionShapeEnum.RECTANGULAR);
        templateRegionOfIntrest.translateXY(+offsetX,+offsetY);
        Paint.fillRegion(templateImage,templateRegionOfIntrest.getRectangle2D().getBounds(),120);
      }

      // MDW - uncomment this code to save the alignment template images to disk.
      String imagesDir = Directory.getAlignmentLogDir();
      String imageFileName = "SHAPE_MATCH_Alignment_Template_Image_" + System.currentTimeMillis() + ".png";
      XrayImageIoUtil.savePngImage(templateImage.getBufferedImage(),
              imagesDir + File.separator + imageFileName);
      templateImage.decrementReferenceCount();
    }

    return new Pair<RegionOfInterest,Image>(alignmentFeaturesBoundingRegionInPixels,templateImage);
  }

  /**
   * Creates a bounding box for the specified alignment (and context) features and "ghost pads".
   *
   * @author Matt Wharton
   */
  private RegionOfInterest createAlignmentTemplateWithinBoundFeturesBoundingBoxForShapeMatchingAlgorithm(List<Feature> alignmentAndContextFeatures)
  {
    Assert.expect(alignmentAndContextFeatures != null);

    // Make space for the alignment pads and fiducials.
    RegionOfInterest alignmentFeaturesBoundingRegionInPixels = null;
    for (Feature alignmentOrContextFeature : alignmentAndContextFeatures)
    {
      RegionOfInterest alignmentFeatureRoi = null;
      if(alignmentOrContextFeature instanceof JointInspectionData)
      {
        JointInspectionData alignmentPad = (JointInspectionData) alignmentOrContextFeature;
        if(ImageAnalysis.isSpecialTwoPinComponent(alignmentPad.getJointTypeEnum()))
        {
          ComponentInspectionData componentInspectionData = alignmentPad.getComponentInspectionData();
          alignmentFeatureRoi = new RegionOfInterest(componentInspectionData.getOrthogonalComponentRegionOfInterest(false));

        }
        else
        {
          alignmentFeatureRoi = new RegionOfInterest(alignmentPad.getOrthogonalRegionOfInterestInPixels(false));
        }
      }
      else if(alignmentOrContextFeature instanceof FiducialInspectionData)
      {
        FiducialInspectionData alignmentFiducial = (FiducialInspectionData) alignmentOrContextFeature;
        alignmentFeatureRoi = new RegionOfInterest(alignmentFiducial.getRegionOfInterestRelativeToInspectionRegionInPixels());
      }
      else
      {
        Assert.expect(false,"Unexpected alignment feature type: " + alignmentOrContextFeature);
      }

      if(alignmentFeaturesBoundingRegionInPixels == null)
      {
        alignmentFeaturesBoundingRegionInPixels = new RegionOfInterest(alignmentFeatureRoi);
      }
      else
      {
        alignmentFeaturesBoundingRegionInPixels.add(alignmentFeatureRoi);
      }
    }

    return alignmentFeaturesBoundingRegionInPixels;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  private void waitInMilliSeconds(int miliseconds)
  {
    try
    {
      Thread.sleep(miliseconds);
    }
    catch (InterruptedException iex)
    {
      //do nothing
    }
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  private void verifyUserDragged2DAlignmentRegionOffsetIsWithinAcceptableLimits(TestSubProgram testSubProgram,
                                                                                AlignmentGroup alignmentGroup) throws BusinessException
  {
    Assert.expect(testSubProgram != null);
    Assert.expect(alignmentGroup != null);
    
    if (testSubProgram.getTestProgram().isManual2DAlignmentInProgress())
    { 
      java.awt.geom.Point2D located2DDraggedRegionCoordinate = null;
      if(testSubProgram.isLowMagnification())
        located2DDraggedRegionCoordinate = alignmentGroup.getLocatedCentroidInNanometers().getPoint2D();
      else
        located2DDraggedRegionCoordinate = alignmentGroup.getLocatedCentroidInNanometersForHighMag().getPoint2D();
      
      PanelCoordinate locatedAlignmentPointCoordinate = new PanelCoordinate((int)Math.round(alignmentGroup.getExpectedCentroidInNanometers().getX() + _currentManualAlignmentXOffsetRelativeToPanelInNanoMeters), 
                                                                            (int)Math.round(alignmentGroup.getExpectedCentroidInNanometers().getY() + _currentManualAlignmentYOffsetRelativeToPanelInNanoMeters));
     
      double absoluteDraggedAlignmentOffsetDistanceInNanometers = located2DDraggedRegionCoordinate.distance(locatedAlignmentPointCoordinate.getPoint2D());

      if (MathUtil.fuzzyGreaterThan(absoluteDraggedAlignmentOffsetDistanceInNanometers,
                                    _MAX_DISTANCE_BETWEEN_LOCATED_AND_DRAGGED_2D_REGION_CENTROIDS_IN_NANOS))
      {
        //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
        testSubProgram.setRunAlignmentLearned(false);
        absoluteDraggedAlignmentOffsetDistanceInNanometers = MathUtil.convertUnits(absoluteDraggedAlignmentOffsetDistanceInNanometers, MathUtilEnum.NANOMETERS, testSubProgram.getTestProgram().getProject().getDisplayUnits());
        double maxDistanceBetweenLocatedAndDragged2DRegionCentroids = MathUtil.convertUnits(_MAX_DISTANCE_BETWEEN_LOCATED_AND_DRAGGED_2D_REGION_CENTROIDS_IN_NANOS, MathUtilEnum.NANOMETERS, testSubProgram.getTestProgram().getProject().getDisplayUnits());
        
        throw new Alignment2DDraggedRegionLocatedTooFarFromActualLocationBusinessException(testSubProgram, 
                                                                                            alignmentGroup, 
                                                                                            absoluteDraggedAlignmentOffsetDistanceInNanometers,
                                                                                            maxDistanceBetweenLocatedAndDragged2DRegionCentroids,
                                                                                            testSubProgram.getTestProgram().getProject().getDisplayUnits().toString());
                                                                                          
      }
    }
  }
}
