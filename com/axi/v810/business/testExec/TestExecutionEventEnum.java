package com.axi.v810.business.testExec;

import com.axi.v810.hardware.*;
import com.axi.util.Assert;

/**
 * @author Andy Mechtenberg
 */
public class TestExecutionEventEnum extends HardwareEventEnum
{
  private static int _index = 0;
  public static final TestExecutionEventEnum INSPECT = new TestExecutionEventEnum(_index++, "Test Execution, Inspect");
  public static final TestExecutionEventEnum INSPECT_FOR_PRODUCTION = new TestExecutionEventEnum(_index++, "Test Excecution, Inspect for Production");
  public static final TestExecutionEventEnum INSPECT_USING_IMAGE_SETS = new TestExecutionEventEnum(_index++, "Test Execution, Inspect Using Image Sets");
  public static final TestExecutionEventEnum ACQUIRE_OFFLINE_IMAGES = new TestExecutionEventEnum(_index++, "Test Execution, Acquire Offline Images");
  public static final TestExecutionEventEnum ACQUIRE_VERIFICATION_IMAGES = new TestExecutionEventEnum(_index++, "Test Execution, Acquire Verification Images");
  public static final TestExecutionEventEnum ACQUIRE_ADJUST_FOCUS_IMAGES = new TestExecutionEventEnum(_index++, "Test Execution, Acquire Adjust Focus Images");
  public static final TestExecutionEventEnum ACQUIRE_VIRTUAL_LIVE_IMAGES = new TestExecutionEventEnum(_index++, "Test Execution, Acquire Virtual Live Images");
  public static final TestExecutionEventEnum RUN_MANUAL_ALIGNMENT = new TestExecutionEventEnum(_index++, "Test Execution, Run Manual Alignment");

  private String _name;

  /**
   * @author Andy Mechtenberg
   */
  private TestExecutionEventEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);

    _name = name;
  }

  /**
   * @author George Booth
   */
  public String toString()
  {
    return _name;
  }
}
