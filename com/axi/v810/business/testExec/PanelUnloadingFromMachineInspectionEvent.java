package com.axi.v810.business.testExec;

/**
 * @author Bill Darbie
 */
public class PanelUnloadingFromMachineInspectionEvent extends InspectionEvent
{
  /**
   * @author Bill Darbie
   */
  public PanelUnloadingFromMachineInspectionEvent()
  {
    super(InspectionEventEnum.PANEL_UNLOADING_FROM_MACHINE);
  }
}
