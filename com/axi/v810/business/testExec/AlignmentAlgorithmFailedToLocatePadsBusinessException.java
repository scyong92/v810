package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * This exception is thrown if the low level alignment algorithm failed to lock onto the alignment pads on a
 * given alignment image.
 *
 * @author Matt Wharton
 */
public class AlignmentAlgorithmFailedToLocatePadsBusinessException extends AlignmentBusinessException
{
  /**
   * @author Matt Wharton
   */
  public AlignmentAlgorithmFailedToLocatePadsBusinessException(TestSubProgram testSubProgram,
    AlignmentGroup alignmentGroup,
    String failAlignmentImagePath)
  {
    super(testSubProgram, failAlignmentImagePath, new LocalizedString("BS_ALIGNMENT_ALGORITHM_FAILED_TO_LOCATE_PADS_KEY",
      new Object[]
      {
        testSubProgram.getMagnificationType().toString(),
        alignmentGroup.getName()        
      }));
  }
}
