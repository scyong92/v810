package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;

/**
 * <p>Title: ImageSetTypeEnum</p>
 *
 * <p>Description: This enum will be used to define what type of image run the user wants to perform.</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Erica Wheatcroft
 * @version 1.0
 */
public class ImageSetTypeEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  private static Map<String, ImageSetTypeEnum> _nameToImageSetType = new HashMap<String, ImageSetTypeEnum>();
  private String _name;

  public static final ImageSetTypeEnum PANEL = new ImageSetTypeEnum(++_index, "Panel");
  public static final ImageSetTypeEnum BOARD = new ImageSetTypeEnum(++_index, "Board");
  public static final ImageSetTypeEnum COMPONENT = new ImageSetTypeEnum(++_index, "Component");
  public static final ImageSetTypeEnum PIN = new ImageSetTypeEnum(++_index, "Pin");
  public static final ImageSetTypeEnum JOINT_TYPE = new ImageSetTypeEnum(++_index, "JointType");
  public static final ImageSetTypeEnum SUBTYPE = new ImageSetTypeEnum(++_index, "Subtype");

  /**
   * @param value int
   * @author Erica Wheatcroft
   */
  private ImageSetTypeEnum(int value, String name)
  {
    super(value);
    Assert.expect(name != null);
    _name = name;
    _nameToImageSetType.put(_name, this);
  }

  /**
   * @author George A. David
   */
  public String getName()
  {
    Assert.expect(_name != null);

    return _name;
  }

  /**
   * @author George A. David
   */
  public static ImageSetTypeEnum getImageSetType(String name)
  {
    Assert.expect(name != null);

    ImageSetTypeEnum imageSetType = _nameToImageSetType.get(name);

    Assert.expect(imageSetType != null);
    return imageSetType;
  }
}
