package com.axi.v810.business.testExec;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.chip.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.util.image.ImageEnhancement;
import com.axi.util.image.Image;

/**
 * Handles retesting of designated recontruction regions at different focus heights.
 *
 * @author Matt Wharton
 */
public class FocusConfirmationEngine
{
  private static FocusConfirmationEngine _instance;

  private static Config _config = Config.getInstance();
  private static final boolean _LOG_FOCUS_CONFIRMATION_DATA =
    _config.getBooleanValue(SoftwareConfigEnum.LOG_FOCUS_CONFIRMATION_DATA);
  private static final boolean _USE_SURFACE_MODEL_DURING_FOCUS_CONFIRMATION =
    _config.getBooleanValue(SoftwareConfigEnum.USE_SURFACE_MODEL_DURING_FOCUS_CONFIRMATION);
  private static final boolean _RETEST_PASSING_OUTLIERS_DURING_FOCUS_CONFIRMATION =
    _config.getBooleanValue(SoftwareConfigEnum.RETEST_PASSING_OUTLIERS_DURING_FOCUS_CONFIRMATION);

  private ZHeightEstimator _zHeightEstimator = ZHeightEstimator.getInstance();

  private ImageAnalysis _imageAnalysis;

  private CSVFileWriterAxi _focusConfirmationDataWriter;

  private long _focusConfirmationStartTime = -1;
  // Don't retest shorts unless LSM indicates a region is misfocused by
  // at least 10 mils. NOTE: setting this value to less than 3 mils for
  // CGAs, or 10 mils for other joint types, will currently have no effect.
  // See getEstimatedZHeightError().
  private static final int _MINIMUM_Z_ERROR_FOR_SHORTS_RETEST = 10  * 25400;

  //private static final Integer _ORIGINAL_IMAGE_FLAG = 1234567890;

  // These Collections keep track of which measurements and indictments should be filtered out of the retest passes.
  // Any occurences of these will have the original values preserved the first inspection slice height (zero offset).
  private static Collection<MeasurementEnum> _measurementEnumsToExcludeFromRetest = new HashSet<MeasurementEnum>();
  private static Collection<IndictmentEnum> _indictmentEnumsToExcludeFromRetest = new HashSet<IndictmentEnum>();

  // This Collection contains our candidate retest offsets in nanos.
  private static Collection<Integer> _focusConfirmationOffsets = new LinkedHashSet<Integer>();

  // This allows each image analysis thread to maintain it's own seperate offset index.
  private static ThreadLocal<Integer> _focusConfirmationOffsetForCurrentThread = new ThreadLocal<Integer>()
  {
    protected Integer initialValue()
    {
      return 0;
    }
  };

  /**
   * @author Matt Wharton
   */
  static
  {
    initializeExcludedMeasurementEnums();
    initializeExcludedIndictmentEnums();
    initializeFocusConfirmationOffsets();
  }

  /**
   * @author Matt Wharton
   */
  private FocusConfirmationEngine()
  {
    // Do nothing ...
  }

  /**
   * @author Matt Wharton
   */
  public static synchronized FocusConfirmationEngine getInstance()
  {
    if (_instance == null)
    {
      _instance = new FocusConfirmationEngine();
    }

    return _instance;
  }

  /**
   * @author Matt Wharton
   */
  private static void initializeExcludedMeasurementEnums()
  {
    _measurementEnumsToExcludeFromRetest.clear();

    _measurementEnumsToExcludeFromRetest.add(MeasurementEnum.SHORT_THICKNESS);
    _measurementEnumsToExcludeFromRetest.add(MeasurementEnum.SHORT_LENGTH);
    //Lim, Lay Ngor - XCR1743:Benchmark
    _measurementEnumsToExcludeFromRetest.add(MeasurementEnum.SOLDER_BALL_SHORT_DELTA_THICKNESS);
  }

  /**
   * @author Matt Wharton
   */
  private static void initializeExcludedIndictmentEnums()
  {
    _indictmentEnumsToExcludeFromRetest.clear();

    _indictmentEnumsToExcludeFromRetest.add(IndictmentEnum.SHORT);
  }

  /**
   * @author Matt Wharton
   */
  private static void initializeFocusConfirmationOffsets()
  {
    _focusConfirmationOffsets.clear();

    int numberOfStepsAbove = _config.getIntValue(SoftwareConfigEnum.FOCUS_CONFIRMATION_NUM_SLICES_ABOVE);
    int numberOfStepsBelow = _config.getIntValue(SoftwareConfigEnum.FOCUS_CONFIRMATION_NUM_SLICES_BELOW);
    int stepSizeInNanoMeters = _config.getIntValue(SoftwareConfigEnum.FOCUS_CONFIRMATION_STEP_SIZE_IN_NANOMETERS);
    Assert.expect(numberOfStepsAbove >= 0);
    Assert.expect(numberOfStepsBelow >= 0);
//    Assert.expect(stepSizeInNanoMeters >= 0);
    if (stepSizeInNanoMeters < 0)
      return;

    int nMaxSteps = Math.max(numberOfStepsAbove, numberOfStepsBelow);
    for (int offsetIndex = 1; offsetIndex <= nMaxSteps; ++offsetIndex)
    {
      int retestOffsetInNanoMeters = offsetIndex * stepSizeInNanoMeters;
      if (offsetIndex <= numberOfStepsBelow)
      {
        _focusConfirmationOffsets.add(-retestOffsetInNanoMeters);
      }
      if (offsetIndex <= numberOfStepsAbove)
      {
        _focusConfirmationOffsets.add(retestOffsetInNanoMeters);
      }
    }
  }

  /**
   * @author Matt Wharton
   */
  void initialize() throws DatastoreException
  {
    // Create an image analysis instance.
    _imageAnalysis = new ImageAnalysis();
    _imageAnalysis.setDoSurfaceModeling(false);  // when we retest things, we don't want to corrupt the surface modeling

    if (_LOG_FOCUS_CONFIRMATION_DATA)
    {
      InspectionEngine inspectionEngine = InspectionEngine.getInstance();
      _focusConfirmationDataWriter = new CSVFileWriterAxi(Arrays.asList("Component", "Region ID", "Retest Failing Pins?",
                                                                        "Retest Passing Outliers?", "Joint Type",
                                                                        "Subtype", "Offset(um)", "Passed", "EstimatedZError(um)"));
      _focusConfirmationDataWriter.open(Directory.getTempDir() + File.separator + "FocusConfirmationData_" + Directory.getDirName(inspectionEngine.getInspectionStartTimeInMillis()) + ".csv");

      // JMH: at some point, we may want to create a separate software.config entry controlling saving of
      //      focus confirmation debug images. Currently, this is controlled by the logging entry.
      String focusConfirmationImagesDir = Directory.getFocusConfirmationImagesDir(inspectionEngine.getInspectionStartTimeInMillis());
      if (FileUtilAxi.existsDirectory(focusConfirmationImagesDir))
      {
        FileUtilAxi.deleteDirectoryContents(focusConfirmationImagesDir);
      }
      FileUtilAxi.createDirectory(focusConfirmationImagesDir);
    }
  }

  /**
   * @author Matt Wharton
   */
  void inspectionComplete()
  {
    _imageAnalysis = null;

    if (_LOG_FOCUS_CONFIRMATION_DATA)
    {
      _focusConfirmationDataWriter.close();
      _focusConfirmationDataWriter = null;
    }
  }

  /**
   * @return The current focus confirmation offset (in nanometers) for the applicable image analysis thread.
   *
   * @author Matt Wharton
   */
  public int getFocusConfirmationOffsetInNanometersForCurrentThread()
  {
    Integer focusConfirmationOffsetIntegerForCurrentThread = _focusConfirmationOffsetForCurrentThread.get();
    Assert.expect(focusConfirmationOffsetIntegerForCurrentThread != null);
    int focusConfirmationOffsetForCurrentThread = focusConfirmationOffsetIntegerForCurrentThread;

    return focusConfirmationOffsetForCurrentThread;
  }

  /**
   * @return The Collection of candidate focus confirmation offsets in nanos.
   *
   * @author Matt Wharton
   */
  public static Collection<Integer> getCandidateFocusConfirmationOffsets()
  {
    Assert.expect(_focusConfirmationOffsets != null);

    return _focusConfirmationOffsets;
  }

  /**
   * Get the estimated z height error from the ZHeightEstimator.  Be careful: this estimate may be constantly changing
   * as the model gathers more data on other threads.  So you can't expect this to return the same value when you call
   * it repeatedly.
   *
   * @author Peter Esbensen
   */
  private int getRawEstimatedZHeightError(JointInspectionData jointInspectionData,
                                          ReconstructedImages reconstructedImages,
                                          Map<Subtype, Integer> subtypeToRawEstimatedZHeightErrorMap)

  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructedImages != null);

    Subtype subtype = jointInspectionData.getSubtype();
    if (subtypeToRawEstimatedZHeightErrorMap.containsKey(subtype))
      return subtypeToRawEstimatedZHeightErrorMap.get(subtype);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    BooleanRef estimateIsValid = new BooleanRef(false);
    SystemFiducialRectangle systemFiducialRectangle = MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(
        reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters(), reconstructionRegion.getManualAlignmentMatrix());

//    boolean isTopSide = jointInspectionData.isTopSideSurfaceModel();
    //Siew Yeng - XCR-3883 - get component board side
    boolean isTopSide = reconstructionRegion.isTopSide();
    int estimatedZHeight = _zHeightEstimator.getBestEstimatedZHeight(systemFiducialRectangle,
                                                                     isTopSide,
                                                                     estimateIsValid,
                                                                     reconstructionRegion);

    if (estimateIsValid.getValue() == false)
    {
      estimatedZHeight = _zHeightEstimator.getReasonablyEstimatedZHeight(systemFiducialRectangle,
                                                                         isTopSide,
                                                                         estimateIsValid,
                                                                         reconstructionRegion);
    }

    int estimatedZHeightError = 0;

    if (estimateIsValid.getValue() == true)
    {
      int padSliceZHeight = _zHeightEstimator.getPadSliceZHeight(jointInspectionData, reconstructedImages);
      estimatedZHeightError = estimatedZHeight - padSliceZHeight;
    }

    subtypeToRawEstimatedZHeightErrorMap.put(subtype, estimatedZHeightError);

    return estimatedZHeightError;
  }

  /**
   * @todo PE see if this can be consolidated with similar code in ZHeightEstimator.
   *
   * @author Peter Esbensen
   */
  private int getEstimatedZHeightError(JointInspectionData aJoint,
                                       ReconstructedImages reconstructedImages,
                                       Map<Subtype, Integer> subtypeToEstimatedZHeightErrorMap,
                                       Map<Subtype, Integer> subtypeToRawEstimatedZHeightErrorMap)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(aJoint != null);

    if (_USE_SURFACE_MODEL_DURING_FOCUS_CONFIRMATION)
    {
      Subtype subtype = aJoint.getSubtype();
      if (subtypeToEstimatedZHeightErrorMap.containsKey(subtype))
        return subtypeToEstimatedZHeightErrorMap.get(subtype);

      int estimatedZHeightError = getRawEstimatedZHeightError(aJoint, reconstructedImages, subtypeToRawEstimatedZHeightErrorMap);

      // if the error is less than 10 mils then assume that there was actually no error
      int minError = 10 * 25400; // 10 mils
      if (aJoint.getJointTypeEnum().equals(JointTypeEnum.CGA))
        minError = 3 * 25400;  // 3 mils
      if (Math.abs(estimatedZHeightError) < minError)
        estimatedZHeightError = 0;

      subtypeToEstimatedZHeightErrorMap.put(subtype, estimatedZHeightError);

      return estimatedZHeightError;
    }
    return 0;
  }

  /**
   * @author Matt Wharton
   * @author John Heumann (split out from retestRegionIfNeeded)
   */
  private void getJointAndComponentResults(
    ReconstructedImages reconstructedImages,
    Map<JointInspectionData, JointInspectionResult> jointInspectionDataToResultMap,
    Map<JointInspectionData, Pair<Collection<JointMeasurement>, Collection<JointIndictment>>> jointInspectionDataToExcludedResultsMap,
    Map<ComponentInspectionData, ComponentInspectionResult> componentInspectionDataToResultMap,
    Map<ComponentInspectionData, Pair<Collection<ComponentMeasurement>, Collection<ComponentIndictment>>> componentInspectionDataToExcludedResultsMap,
    Map<Subtype, Integer> subtypeToEstimatedZHeightErrorMap,
    Map<Subtype, Integer> subtypeToRawEstimatedZHeightErrorMap)
  {
    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

    Set<ComponentInspectionData> visitedComponentInspectionDataObjects = new HashSet<ComponentInspectionData>();

    for (JointInspectionData jointInspectionData : reconstructionRegion.getJointInspectionDataList())
    {
      visitedComponentInspectionDataObjects.add(jointInspectionData.getComponentInspectionData());

      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

      // Save the 'excluded' joint measurements and indictments.
      Pair<Collection<JointMeasurement>, Collection<JointIndictment>> excludedJointResults =
        getExcludedJointMeasurementsAndIndictments( jointInspectionData,
                                                    reconstructedImages,
                                                    subtypeToEstimatedZHeightErrorMap,
                                                    subtypeToRawEstimatedZHeightErrorMap);

      jointInspectionDataToExcludedResultsMap.put(jointInspectionData, excludedJointResults);

      jointInspectionDataToResultMap.put(jointInspectionData, jointInspectionResult);
    }
    for (ComponentInspectionData componentInspectionData : visitedComponentInspectionDataObjects)
    {
      ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();

      // Save the 'excluded' component measurements and indictments.
      Pair<Collection<ComponentMeasurement>, Collection<ComponentIndictment>> excludedComponentResults =
        getExcludedComponentMeasurementsAndIndictments(componentInspectionData,
                                                       reconstructedImages,
                                                       subtypeToEstimatedZHeightErrorMap,
                                                       subtypeToRawEstimatedZHeightErrorMap);

      componentInspectionDataToExcludedResultsMap.put(componentInspectionData, excludedComponentResults);

      componentInspectionDataToResultMap.put(componentInspectionData, componentInspectionResult);
    }
  }

  /**
   * @author Matt Wharton
   * @author John Heumann (split out from retestRegionIfNeeded)
   */
  private boolean jointHasOnlyExcludedIndictments(
    JointInspectionData jointInspectionData,
    Map<JointInspectionData, Pair<Collection<JointMeasurement>, Collection<JointIndictment>>> jointInspectionDataToExcludedResultsMap,
    Map<ComponentInspectionData, Pair<Collection<ComponentMeasurement>, Collection<ComponentIndictment>>> componentInspectionDataToExcludedResultsMap,
    IntegerRef totalIndictments)
  {
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
    ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();

    Collection<JointIndictment> allJointIndictments = jointInspectionResult.getIndictments();
    Collection<JointIndictment> excludedJointIndictments =
      jointInspectionDataToExcludedResultsMap.get(jointInspectionData).getSecond();
    Collection<ComponentIndictment> allComponentIndictments = componentInspectionResult.getIndictments();
    Collection<ComponentIndictment> excludedComponentIndictments =
      componentInspectionDataToExcludedResultsMap.get(componentInspectionData).getSecond();

    totalIndictments.setValue(allJointIndictments.size() + allComponentIndictments.size());

    return ((allJointIndictments.size() == excludedJointIndictments.size()) &&
            (allComponentIndictments.size() == excludedComponentIndictments.size()));
  }

  /**
   * @author Matt Wharton
   * @author John Heumann (split out from retestRegionIfNeeded)
   */
  private boolean retestJoints(
    Subtype subtypeToRetest,
    List<JointInspectionData> jointsInSubtype,
    ReconstructedImages reconstructedImagesAtOffset) throws XrayTesterException
  {
    //Siew Yeng - XCR1745 - Semi-automated mode
    if(TestExecution.getInstance().isSemiAutomatedModeEnabled())
      return false;
    
    // Clear out any old joint and component results for this retest pass.
    for (JointInspectionData jointInspectionData : jointsInSubtype)
    {
      ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
       jointInspectionData.clearJointInspectionResult();
       componentInspectionData.clearComponentInspectionResult();
    }

    // Reclassify this subtype.
    _imageAnalysis.reclassifyJoints(reconstructedImagesAtOffset, subtypeToRetest);

    // Subtype is innocent until proven guilty on each retest offset.
    boolean subtypePassedAtCurrentRetestOffset = true;

    for (JointInspectionData jointInspectionData : jointsInSubtype)
    {
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
      ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();

      subtypePassedAtCurrentRetestOffset =
        subtypePassedAtCurrentRetestOffset && (jointInspectionResult.passed() && componentInspectionResult.passed());

      // If we enconter a failure, there's no need to continue looking at the joint/component results.
      if (subtypePassedAtCurrentRetestOffset == false)
      {
        break;
      }
    }

    return subtypePassedAtCurrentRetestOffset;
  }

  /**
   * @author Matt Wharton
   * @author John Heumann (split out from retestRegionIfNeeded)
   */
  private void updateResultMaps(
    List<JointInspectionData> jointsInSubtype,
    Map<JointInspectionData, JointInspectionResult> jointInspectionDataToResultMap,
    Map<JointInspectionData, Pair<Collection<JointMeasurement>, Collection<JointIndictment>>> jointInspectionDataToExcludedResultsMap,
    Map<ComponentInspectionData, ComponentInspectionResult> componentInspectionDataToResultMap,
    Map<ComponentInspectionData, Pair<Collection<ComponentMeasurement>, Collection<ComponentIndictment>>> componentInspectionDataToExcludedResultsMap)
  {
    // Update the result maps with the passing results.
    Set<ComponentInspectionData> visitedComponentInspectionDataObjects = new HashSet<ComponentInspectionData>();
    for (JointInspectionData jointInspectionData : jointsInSubtype)
    {
      visitedComponentInspectionDataObjects.add(jointInspectionData.getComponentInspectionData());

      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

      // Re-attach any 'excluded' joint measurements and indictments that were saved from the original results.
//      Pair<Collection<JointMeasurement>, Collection<JointIndictment>> excludedJointResults =
//          jointInspectionDataToExcludedResultsMap.get(jointInspectionData);
//      for (JointMeasurement jointMeasurement : excludedJointResults.getFirst())
//      {
//        jointInspectionResult.addMeasurement(jointMeasurement);
//      }
//      for (JointIndictment jointIndictment : excludedJointResults.getSecond())
//      {
//        jointInspectionResult.addIndictment(jointIndictment);
//      }
//      }

      jointInspectionDataToResultMap.put(jointInspectionData, jointInspectionResult);
    }
    for (ComponentInspectionData componentInspectionData : visitedComponentInspectionDataObjects)
    {
      ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();

      // Re-attach any 'excluded' component measurements and indictments that were saved from the original results.
//      Pair<Collection<ComponentMeasurement>, Collection<ComponentIndictment>> excludedComponentResults =
//          componentInspectionDataToExcludedResultsMap.get(componentInspectionData);
//      for (ComponentMeasurement componentMeasurement : excludedComponentResults.getFirst())
//      {
//        componentInspectionResult.addMeasurement(componentMeasurement);
//      }
//      for (ComponentIndictment componentIndictment : excludedComponentResults.getSecond())
//      {
//        componentInspectionResult.addIndictment(componentIndictment);
//      }

      componentInspectionDataToResultMap.put(componentInspectionData, componentInspectionResult);
    }
  }

  /**
   * @author Matt Wharton
   * @author John Heumann (split out from retestRegionIfNeeded)
   */
  private void updateRepairImagesAndResults(
    ReconstructionRegion reconstructionRegion,
    boolean anySubtypesFailing,
    Map<Integer, ReconstructedImages> retestOffsetToReconstructedImagesMap,
    Map<JointInspectionData, JointInspectionResult> jointInspectionDataToResultMap,
    Map<ComponentInspectionData, ComponentInspectionResult> componentInspectionDataToResultMap,
    Set<SliceNameEnum> slicesToExcludeFromRepairImages) throws XrayTesterException
  {
    Set<Integer> retestOffsetsIncludingEstimatedError = new HashSet<Integer>(retestOffsetToReconstructedImagesMap.keySet());
    InspectionEngine inspectionEngine = InspectionEngine.getInstance();

    // Save our repair images if needed.
    boolean saveFocusConfirmationRepairImages =
        _config.getBooleanValue(SoftwareConfigEnum.SAVE_FOCUS_CONFIRMATION_REPAIR_IMAGES);
    
    //Siew Yeng - XCR1745 - Semi-automated mode
    if(TestExecution.getInstance().isSemiAutomatedModeEnabled())
      saveFocusConfirmationRepairImages = true;
    
    if (anySubtypesFailing)
    {
      if (saveFocusConfirmationRepairImages)
      {
        for (Integer retestOffsetIncludingEstimatedError : retestOffsetsIncludingEstimatedError)
        {
          ReconstructedImages repairImages = retestOffsetToReconstructedImagesMap.get(retestOffsetIncludingEstimatedError);
          Assert.expect(repairImages != null);
          inspectionEngine.writeRepairImages(repairImages,
                                             retestOffsetIncludingEstimatedError,
                                             slicesToExcludeFromRepairImages);
        }
      }
    }

    // Restore the proper joint and component results.  Also, make sure the joint/component results records
    // get properly linked with the offset slice images.
    Set<ComponentInspectionData> visitedComponentInspectionDataObjects = new HashSet<ComponentInspectionData>();
    for (JointInspectionData jointInspectionData : reconstructionRegion.getJointInspectionDataList())
    {
      visitedComponentInspectionDataObjects.add(jointInspectionData.getComponentInspectionData());
      JointInspectionResult jointInspectionResult = jointInspectionDataToResultMap.get(jointInspectionData);

      jointInspectionData.setJointInspectionResult(jointInspectionResult);

      // If applicable, make sure the joint is tied to the proper repair images.
      if (jointInspectionResult.passed() == false)
      {
        if (saveFocusConfirmationRepairImages)
        {
          for (Integer retestOffsetIncludingEstimatedError : retestOffsetsIncludingEstimatedError)
          {
            ReconstructedImages repairImages = retestOffsetToReconstructedImagesMap.get(retestOffsetIncludingEstimatedError);
            Assert.expect(repairImages != null);
            inspectionEngine.addSliceOffsetToJoint(jointInspectionResult, repairImages, retestOffsetIncludingEstimatedError);
//            inspectionEngine.addSliceOffsetToJoint(jointInspectionResult, repairImages, _ORIGINAL_IMAGE_FLAG);
          }
        }
      }
    }
    for (ComponentInspectionData componentInspectionData : visitedComponentInspectionDataObjects)
    {
      ComponentInspectionResult componentInspectionResult = componentInspectionDataToResultMap.get(componentInspectionData);

      componentInspectionData.setComponentInspectionResult(componentInspectionResult);

      // If applicable, make sure the component is tied to the proper repair images.
      if (componentInspectionResult.passed() == false)
      {
        if (saveFocusConfirmationRepairImages)
        {
          for (Integer retestOffsetIncludingEstimatedError : retestOffsetsIncludingEstimatedError)
          {
            ReconstructedImages repairImages = retestOffsetToReconstructedImagesMap.get(retestOffsetIncludingEstimatedError);
            Assert.expect(repairImages != null);
            inspectionEngine.addSliceOffsetToComponent(componentInspectionResult, repairImages, retestOffsetIncludingEstimatedError);
//            inspectionEngine.addSliceOffsetToComponent(componentInspectionResult, repairImages, _ORIGINAL_IMAGE_FLAG);
          }
        }
      }
    }
  }

  /**
   * Retests any subtypes in the specified ReconstructionRegion which contain no failing pins. This is accomplished by:
   *
   * 1.  For each passing subtype wit retestPassingOutliers enabled:
   *     a. Get the error between the slice heights and those predicted by the limited surface model.
   *     b. Retest at the the new slice height(s)
   *     c. If there are any defects at the new heights, attach these new results to the joints and save the
   *        new repair images.
   *
   * @author John Heumann
   */
  private void retestSubtypesWithNoFailingPins(
      ReconstructedImages reconstructedImages, ReconstructionRegion reconstructionRegion,
      InspectionEngine inspectionEngine,
      List<Subtype> subtypesWithNoDefects,
      List<Subtype> subtypesWithDefects,
      Map<Integer, ReconstructedImages> retestOffsetToReconstructedImagesMap,
      Map<JointInspectionData, Pair<Collection<JointMeasurement>, Collection<JointIndictment>>> jointInspectionDataToExcludedResultsMap,
      Map<ComponentInspectionData, Pair<Collection<ComponentMeasurement>, Collection<ComponentIndictment>>> componentInspectionDataToExcludedResultsMap,
      Map<JointInspectionData, JointInspectionResult> jointInspectionDataToResultMap,
      Map<ComponentInspectionData, ComponentInspectionResult> componentInspectionDataToResultMap,
      Map<Subtype, Integer> subtypeToEstimatedZHeightErrorMap,
      Map<Subtype, Integer> subtypeToRawEstimatedZHeightErrorMap,
      Map<Subtype, Integer> subtypeToRetestPassingOffsetMap,
      BooleanRef limitedSurfaceModelUsed) throws XrayTesterException

  {
    boolean anySubtypesFailing = false;
    boolean originalSliceImagesReplaced = false;
    List<Subtype> newlyFailingSubtypes = new LinkedList<Subtype>();
    Set<SliceNameEnum> slicesToExcludeFromRepairImages = new HashSet<SliceNameEnum>();

    if (subtypesWithNoDefects.isEmpty())
      return;

    try
    {
      Map<Subtype, List<JointInspectionData>> subtypeToInspectableJointsMap =
        reconstructionRegion.getSubtypeToInspectableJointListMap();

      // Retest only those subtypes which have no defects.
      for (Subtype subtypeToRetest : subtypesWithNoDefects)
      {
        // If a subtype is marked to skip Focus Confirmation, we will skip it here.
        if (subtypeToRetest.doesAlgorithmSettingExist(AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION))
        {
            boolean isFocusConfirmationEnableForSubtype = false;
            Serializable value = subtypeToRetest.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION);
            Assert.expect(value instanceof String);
            if (value.equals("Off"))
                isFocusConfirmationEnableForSubtype = false;
            else if (value.equals("On"))
                isFocusConfirmationEnableForSubtype = true;
            else
              Assert.expect(false, "Unexpected Focus Confirmation value.");
            
            if (isFocusConfirmationEnableForSubtype == false)
                continue;
        }
          
        List<JointInspectionData> jointsInSubtype = subtypeToInspectableJointsMap.get(subtypeToRetest);
        JointInspectionData firstJoint = jointsInSubtype.get(0);

        // Get the slice height error predicted by limited surface model. If small, do nothing.
        int estimatedZHeightError = getEstimatedZHeightError(firstJoint,
                                                             reconstructedImages,
                                                             subtypeToEstimatedZHeightErrorMap,
                                                             subtypeToRawEstimatedZHeightErrorMap);
        if (estimatedZHeightError == 0)
        {
          return;
        }
        _focusConfirmationOffsetForCurrentThread.set(estimatedZHeightError);
        limitedSurfaceModelUsed.setValue(true);

        // If we already have the image for this offset in our cache, just use it.  Otherwise, acquire it and add it
        // to the cache.
        ReconstructedImages reconstructedImagesAtOffset = retestOffsetToReconstructedImagesMap.get(estimatedZHeightError);
        if (reconstructedImagesAtOffset == null)
        {
          BooleanRef reReconstructedImagesAvailable = new BooleanRef(true);
          reconstructedImagesAtOffset =
            inspectionEngine.getReReconstructedImagesForRegionAtOffset(reconstructionRegion,
                                                                       estimatedZHeightError,
                                                                       reReconstructedImagesAvailable);

          // If we didn't get our re-reconstructed images back, we must be in the middle of an abort or
          // something.  Just return if this is the case.
          if (reReconstructedImagesAvailable.getValue() == false)
          {
            return;
          }

          // Special case kludge. The above call offsets all slices by estimatedZHeightError.
          // This is correct for single slice regions, and for pth where multiple slices are all at
          // fixed steps from a  single autofocused slice height.  For chips and pcaps, we want to offset
          // only the real pad slice, so we restore the original image for the other slice.
          InspectionFamilyEnum inspectionFamilyEnum = reconstructionRegion.getInspectionFamilyEnum();
          if (inspectionFamilyEnum.equals(InspectionFamilyEnum.CHIP) ||
              inspectionFamilyEnum.equals(InspectionFamilyEnum.POLARIZED_CAP))

          {
            if (ChipMeasurementAlgorithm.testAsOpaque(firstJoint))
            {
              reconstructedImagesAtOffset.replaceASlice(reconstructedImages, SliceNameEnum.CLEAR_CHIP_PAD);
              slicesToExcludeFromRepairImages.add(SliceNameEnum.CLEAR_CHIP_PAD);
            }
            else
            {
              reconstructedImagesAtOffset.replaceASlice(reconstructedImages, SliceNameEnum.OPAQUE_CHIP_PAD);
              slicesToExcludeFromRepairImages.add(SliceNameEnum.OPAQUE_CHIP_PAD);
            }
          }

          retestOffsetToReconstructedImagesMap.put(estimatedZHeightError, reconstructedImagesAtOffset);
        }

        boolean subtypePassed = retestJoints(subtypeToRetest, jointsInSubtype, reconstructedImagesAtOffset);
        anySubtypesFailing |= (subtypePassed == false);

        // Log the retest data if applicable.
        if (_LOG_FOCUS_CONFIRMATION_DATA)
        {
          String refDes = reconstructionRegion.getComponent().getReferenceDesignator();
          String regionId = String.valueOf(reconstructionRegion.getRegionId());
          String retestFailingPinsStr = new String("?");
          String retestPassingOutliersStr = new String("true");
          String subtypeName = subtypeToRetest.getShortName();
          String jointTypeName = subtypeToRetest.getJointTypeEnum().getName();
          String retestOffsetInMicrometersStr = String.valueOf(estimatedZHeightError / 1000);
          String passedStr = Boolean.toString(subtypePassed);
          String estimatedZHeightErrorString = String.valueOf(
            getRawEstimatedZHeightError(firstJoint, reconstructedImages, subtypeToRawEstimatedZHeightErrorMap) / 1000);
          _focusConfirmationDataWriter.writeDataLine(Arrays.asList(refDes, regionId, retestFailingPinsStr,
                                                                   retestPassingOutliersStr, jointTypeName, subtypeName,
                                                                   retestOffsetInMicrometersStr, passedStr,
                                                                   estimatedZHeightErrorString));
        }

        // If our subtype fails replace the slices in reconstructedImages and save results.
        // Note that repair/tuning images will not be correct if different offsets are chosen for
        // different subtypes. (I.e. we assume a single offset will work for all subtypes in a region).
        if (subtypePassed == false)
        {
          newlyFailingSubtypes.add(subtypeToRetest);

          // Save the offset used for use by RetestSubtypesWithFailingPins
          subtypeToRetestPassingOffsetMap.put(subtypeToRetest, estimatedZHeightError);

          // Since we've changed to the height predicted by the limited surface model,
          // update the cached error maps to reflect this
          subtypeToEstimatedZHeightErrorMap.put(subtypeToRetest, 0);
          subtypeToRawEstimatedZHeightErrorMap.put(subtypeToRetest, 0);

          /*****
          // Save original (unoffset) repair images at offset 1234567890 if needed.
          if (_config.getBooleanValue(SoftwareConfigEnum.SAVE_FOCUS_CONFIRMATION_REPAIR_IMAGES))
          {
            inspectionEngine.writeRepairImages(reconstructedImages, _ORIGINAL_IMAGE_FLAG);
          }
          *****/

          // Replace original slice images with those at the chosen offset
          if (originalSliceImagesReplaced == false)
          {
            if (_LOG_FOCUS_CONFIRMATION_DATA)
            {
               saveFocusConfirmationDebugImages(reconstructionRegion, reconstructedImages, reconstructedImagesAtOffset, true);
            }
            reconstructedImages.replaceAllSlices(reconstructedImagesAtOffset);
            originalSliceImagesReplaced = true;
          }

          // Update local maps to reflect the newly obtained results
          updateResultMaps(jointsInSubtype,
                           jointInspectionDataToResultMap,
                           jointInspectionDataToExcludedResultsMap,
                           componentInspectionDataToResultMap,
                           componentInspectionDataToExcludedResultsMap);


          System.out.println("failed " + subtypeToRetest.getJointTypeEnum().getName() + " " +
                             jointsInSubtype.get(0).getFullyQualifiedPadName() + " " +
                             subtypeToRetest.getLongName() + " offset " + estimatedZHeightError);
        }
      }
    }
    finally
    {
      updateRepairImagesAndResults(reconstructionRegion,
                                   anySubtypesFailing,
                                   retestOffsetToReconstructedImagesMap,
                                   jointInspectionDataToResultMap,
                                   componentInspectionDataToResultMap,
                                   slicesToExcludeFromRepairImages);

      subtypesWithNoDefects.removeAll(newlyFailingSubtypes);
      subtypesWithDefects.addAll(newlyFailingSubtypes);

      // Clear the image cache so retestFailing can start fresh
      for (ReconstructedImages offsetReconstructedImages : retestOffsetToReconstructedImagesMap.values())
        offsetReconstructedImages.decrementReferenceCount();
      retestOffsetToReconstructedImagesMap.clear();

      // Reset our offset index to 0.
      _focusConfirmationOffsetForCurrentThread.set(0);
    }
  }

  /**
   * Retests any subtypes in the specified ReconstructionRegion which contain failing pins. This is accomplished by:
   *
   * 1.  For each defective subtype:
   *     a.  For each retest slice height (excluding the 'zero' offset, of course):
   *         i.   Re-reconstruct at the specified slice offset.
   *         ii.  Re-run the algorithms on the re-reconstructed images.
   *         iii. If there are no defects at the new height, attach these new results to the joints (and reattach any
   *              'retest excluded' results and stop iterating over slice offsets.  Otherwise, save the
   *              re-reconstructed images for repair and move on to the next slice offset.
   *
   * @author Matt Wharton
   * @author John Heumann (split out from retestRegionIfNeeded)
   */
  private void retestSubtypesWithFailingPins(
      ReconstructedImages reconstructedImages, ReconstructionRegion reconstructionRegion,
      InspectionEngine inspectionEngine,
      List<Subtype> subtypesWithNoDefects,
      List<Subtype> subtypesWithDefects,
      Map<Integer, ReconstructedImages> retestOffsetToReconstructedImagesMap,
      Map<JointInspectionData, Pair<Collection<JointMeasurement>, Collection<JointIndictment>>> jointInspectionDataToExcludedResultsMap,
      Map<ComponentInspectionData, Pair<Collection<ComponentMeasurement>, Collection<ComponentIndictment>>> componentInspectionDataToExcludedResultsMap,
      Map<JointInspectionData, JointInspectionResult> jointInspectionDataToResultMap,
      Map<ComponentInspectionData, ComponentInspectionResult> componentInspectionDataToResultMap,
      Map<Subtype, Integer> subtypeToEstimatedZHeightErrorMap,
      Map<Subtype, Integer> subtypeToRawEstimatedZHeightErrorMap,
      Map<Subtype, Integer> subtypeToRetestPassingOffsetMap,
      BooleanRef limitedSurfaceModelUsed,
      boolean doSurfaceModeling) throws XrayTesterException
  {
    // Keep track of whether or not we have any subtypes still failing after we complete our retest sequence.
    boolean anySubtypesStillFailing = false;
    boolean originalSliceImagesReplaced = false;
    List<Subtype> newlyPassingSubtypes = new LinkedList<Subtype>();
    Set<SliceNameEnum> slicesToExcludeFromRepairImages = new HashSet<SliceNameEnum>();
    Map<Subtype, List<JointInspectionData>> subtypeToInspectableJointsMap = null;
    ReconstructedImages reconstructedImagesAtOffset = null;

    if (subtypesWithDefects.isEmpty())
      return;

    try
    {
      subtypeToInspectableJointsMap = reconstructionRegion.getSubtypeToInspectableJointListMap();

      // Retest only those subtypes which have defects.
      for (Subtype subtypeToRetest : subtypesWithDefects)
      {
        // If a subtype is marked to skip Focus Confirmation, we will skip it here.
        if (subtypeToRetest.doesAlgorithmSettingExist(AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION))
        {
            boolean isFocusConfirmationEnableForSubtype = false;
            Serializable value = subtypeToRetest.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION);
            Assert.expect(value instanceof String);
            if (value.equals("Off"))
                isFocusConfirmationEnableForSubtype = false;
            else if (value.equals("On"))
                isFocusConfirmationEnableForSubtype = true;
            else
              Assert.expect(false, "Unexpected Focus Confirmation value.");
            
            //Siew Yeng - XCR1745 - Semi-automated mode
            if (isFocusConfirmationEnableForSubtype == false && TestExecution.getInstance().isSemiAutomatedModeEnabled() == false)
                continue;
        }  
          
        List<JointInspectionData> jointsInSubtype = subtypeToInspectableJointsMap.get(subtypeToRetest);

        IntegerRef totalIndictments = new IntegerRef(0);

        // If our only defects on this subtype are 'excluded' defects, just skip retest of this subtype.
        boolean subtypeHasOnlyExcludedIndictments = true;
        JointInspectionData failingJoint = null;

        for (JointInspectionData jointInspectionData : jointsInSubtype)
        {
          if (jointHasOnlyExcludedIndictments(jointInspectionData,
                                              jointInspectionDataToExcludedResultsMap,
                                              componentInspectionDataToExcludedResultsMap,
                                              totalIndictments) == false)
          {
            subtypeHasOnlyExcludedIndictments = false;
            failingJoint = jointInspectionData;
            break;
          }

        }
        if (subtypeHasOnlyExcludedIndictments)
        {
          // Automatically mark that we have failing subtypes.
          anySubtypesStillFailing = true;
          continue;
        }

        // Iterate over our offsets until all joints/components are passing or we exhaust our candidate slices.
        boolean subtypeExoneratedDuringRetest = false;
        Assert.expect(failingJoint != null);
        int estimatedZHeightError = getEstimatedZHeightError(failingJoint,
                                                             reconstructedImages,
                                                             subtypeToEstimatedZHeightErrorMap,
                                                             subtypeToRawEstimatedZHeightErrorMap);
        Collection<Integer> _activeFocusConfirmationOffsets = new Vector<Integer>();
        int _FOCUS_CONFIRMATION_STEP_SIZE_IN_NANOMETERS =
          _config.getIntValue(SoftwareConfigEnum.FOCUS_CONFIRMATION_STEP_SIZE_IN_NANOMETERS);
        // By default, 0 offset is not included in _focusConfirmationOffsets
        // With surface modeling, the added estimatedZHeightError can move it away from 0
        // In this case, we want to include it in retest offsets
        if (estimatedZHeightError != 0 || _FOCUS_CONFIRMATION_STEP_SIZE_IN_NANOMETERS < 0)
        {
          _activeFocusConfirmationOffsets.add(0);
          limitedSurfaceModelUsed.setValue(true);
        }
        _activeFocusConfirmationOffsets.addAll(_focusConfirmationOffsets);
//        System.out.println("retestSubtypesWithFailingPins: RRID:"+reconstructionRegion.getRegionId()+",estimatedZHeightError:"+estimatedZHeightError+",_activeFocusConfirmationOffsets:"+_activeFocusConfirmationOffsets.size());
        for (int retestOffsetInNanometers : _activeFocusConfirmationOffsets)
        {
          int zOffsetInNanometers = retestOffsetInNanometers + estimatedZHeightError;
          // If we previously offset the z height in retestSubtypesWithNoFailingPins,
          // estimatedZError will now be 0, but we want the focus confirmation offsets
          // to be relative to the height chosen by retest passing, not the original.
          if (subtypeToRetestPassingOffsetMap.containsKey(subtypeToRetest))
            zOffsetInNanometers += subtypeToRetestPassingOffsetMap.get(subtypeToRetest);
          _focusConfirmationOffsetForCurrentThread.set(zOffsetInNanometers);

          // If we already have the image for this offset in our cache, just use it.  Otherwise, acquire it and add it
          // to the cache.
          reconstructedImagesAtOffset = retestOffsetToReconstructedImagesMap.get(zOffsetInNanometers);
          if (reconstructedImagesAtOffset == null)
          {
            BooleanRef reReconstructedImagesAvailable = new BooleanRef(true);
            reconstructedImagesAtOffset =
              inspectionEngine.getReReconstructedImagesForRegionAtOffset(reconstructionRegion,
                                                                         zOffsetInNanometers,
                                                                         reReconstructedImagesAvailable);

            // If we didn't get our re-reconstructed images back, we must be in the middle of an abort or
            // something.  Just return if this is the case.
            if (reReconstructedImagesAvailable.getValue() == false)
            {
              return;
            }

            // Special case kludge. The above call offsets all slices by estimatedZHeightError.
            // This is correct for single slice regions, and for pth where multiple slices are all at
            // fixed steps from a  single autofocused slice height.  For chips and pcaps, we want to offset
            // only the real pad slice, so we restore the original image for the other slice.
            InspectionFamilyEnum inspectionFamilyEnum = reconstructionRegion.getInspectionFamilyEnum();
            if (inspectionFamilyEnum.equals(InspectionFamilyEnum.CHIP) ||
                inspectionFamilyEnum.equals(InspectionFamilyEnum.POLARIZED_CAP))
            {
              JointInspectionData firstJoint = jointsInSubtype.get(0);
              if (ChipMeasurementAlgorithm.testAsOpaque(firstJoint))
              {
                reconstructedImagesAtOffset.replaceASlice(reconstructedImages, SliceNameEnum.CLEAR_CHIP_PAD);
                slicesToExcludeFromRepairImages.add(SliceNameEnum.CLEAR_CHIP_PAD);
              }
              else
              {
                reconstructedImagesAtOffset.replaceASlice(reconstructedImages, SliceNameEnum.OPAQUE_CHIP_PAD);
                slicesToExcludeFromRepairImages.add(SliceNameEnum.OPAQUE_CHIP_PAD);
              }
            }

            retestOffsetToReconstructedImagesMap.put(zOffsetInNanometers, reconstructedImagesAtOffset);
          }

          boolean subtypePassedAtCurrentRetestOffset =
            retestJoints(subtypeToRetest, jointsInSubtype, reconstructedImagesAtOffset);

          // Log the retest data if applicable.
          if (_LOG_FOCUS_CONFIRMATION_DATA)
          {
            String refDes = reconstructionRegion.getComponent().getReferenceDesignator();
            String regionId = String.valueOf(reconstructionRegion.getRegionId());
            String retestFailingPinsStr = new String("true");
            String retestPassingOutliersStr = new String("?");
            String subtypeName = subtypeToRetest.getShortName();
            String jointTypeName = subtypeToRetest.getJointTypeEnum().getName();
            String retestOffsetInMicrometersStr = String.valueOf(zOffsetInNanometers / 1000);
            String passedStr = Boolean.toString(subtypePassedAtCurrentRetestOffset);
            String estimatedZHeightErrorString = String.valueOf(
              getRawEstimatedZHeightError(failingJoint, reconstructedImages, subtypeToRawEstimatedZHeightErrorMap) / 1000);
            _focusConfirmationDataWriter.writeDataLine(Arrays.asList(refDes, regionId, retestFailingPinsStr,
                                                                     retestPassingOutliersStr, jointTypeName, subtypeName,
                                                                     retestOffsetInMicrometersStr, passedStr,
                                                                     estimatedZHeightErrorString));
          }

          // If our subtype is passing, we should save these results and stop re-classifying.
          // Note that repair/tuning images will not be correct if different offsets are chosen for
          // different subtypes. (I.e. we assume a single offset will work for all subtypes in a region).
          if (subtypePassedAtCurrentRetestOffset)
          {
            newlyPassingSubtypes.add(subtypeToRetest);

            /*****
            // Save original (unoffset) repair images at offset 1234567890 if needed.
            if (_config.getBooleanValue(SoftwareConfigEnum.SAVE_FOCUS_CONFIRMATION_REPAIR_IMAGES))
            {
              inspectionEngine.writeRepairImages(reconstructedImages, _ORIGINAL_IMAGE_FLAG);
            }
            *****/

            if (originalSliceImagesReplaced == false)
            {
              if (_LOG_FOCUS_CONFIRMATION_DATA)
              {
                saveFocusConfirmationDebugImages(reconstructionRegion, reconstructedImages, reconstructedImagesAtOffset, false);
              }
              reconstructedImages.replaceAllSlices(reconstructedImagesAtOffset);
              originalSliceImagesReplaced = true;
            }

            updateResultMaps(jointsInSubtype,
                             jointInspectionDataToResultMap,
                             jointInspectionDataToExcludedResultsMap,
                             componentInspectionDataToResultMap,
                             componentInspectionDataToExcludedResultsMap);

            subtypeExoneratedDuringRetest = true;

            if (estimatedZHeightError != 0)
            {
              if (_LOG_FOCUS_CONFIRMATION_DATA)
              {
                System.out.println("fixed " + totalIndictments.getValue() + " total indictments for " +
                                   subtypeToRetest.getJointTypeEnum().getName() + " " +
                                   jointsInSubtype.get(0).getFullyQualifiedPadName() + " " +
                                   subtypeToRetest.getLongName() + " offset " + zOffsetInNanometers);
              }
            }

            break;
          }
          else
          {
            if (_FOCUS_CONFIRMATION_STEP_SIZE_IN_NANOMETERS < 0)
            {
              if (originalSliceImagesReplaced == false)
              {
                if (_LOG_FOCUS_CONFIRMATION_DATA)
                {
                  saveFocusConfirmationDebugImages(reconstructionRegion, reconstructedImages, reconstructedImagesAtOffset, false);
                }
                reconstructedImages.replaceAllSlices(reconstructedImagesAtOffset);
                originalSliceImagesReplaced = true;
              }
              
              updateResultMaps(jointsInSubtype,
                               jointInspectionDataToResultMap,
                               jointInspectionDataToExcludedResultsMap,
                               componentInspectionDataToResultMap,
                               componentInspectionDataToExcludedResultsMap);

              subtypeExoneratedDuringRetest = true;

              if (estimatedZHeightError != 0)
              {
                if (_LOG_FOCUS_CONFIRMATION_DATA)
                {
                  System.out.println("fixed " + totalIndictments.getValue() + " total indictments for " +
                                     subtypeToRetest.getJointTypeEnum().getName() + " " +
                                     jointsInSubtype.get(0).getFullyQualifiedPadName() + " " +
                                     subtypeToRetest.getLongName() + " offset " + zOffsetInNanometers);
                }
              }

              break;
            }
          }
        }

        // Update our 'any subtypes failing' flag.
        anySubtypesStillFailing = anySubtypesStillFailing || (subtypeExoneratedDuringRetest == false);
      }
    }
    finally
    {
      // The following call must alway be made, even if we're not changing any calls.
      // This serves to restore the original results in that case.
      updateRepairImagesAndResults(reconstructionRegion,
                                   anySubtypesStillFailing,
                                   retestOffsetToReconstructedImagesMap,
                                   jointInspectionDataToResultMap,
                                   componentInspectionDataToResultMap,
                                   slicesToExcludeFromRepairImages);

      if (newlyPassingSubtypes.isEmpty() != true)    // If we're changing some calls...
      {

        subtypesWithDefects.removeAll(newlyPassingSubtypes);
        subtypesWithNoDefects.addAll(newlyPassingSubtypes);

        // We're changing this region to passing. If limited surface model was not used
        // to predict the new slice height, let's add the new data to the surface model.
        if (anySubtypesStillFailing == false && doSurfaceModeling && limitedSurfaceModelUsed.getValue() == false)
        {
          for (Subtype subtype : newlyPassingSubtypes)
          {
            Assert.expect(subtypeToInspectableJointsMap != null);
            Assert.expect(reconstructedImagesAtOffset != null);
            List<JointInspectionData> jointsInSubtype = subtypeToInspectableJointsMap.get(subtype);
            _imageAnalysis.setDoSurfaceModeling(true);
            _imageAnalysis.updateSurfaceModel(jointsInSubtype, reconstructedImagesAtOffset, true); //Siew Yeng - XCR-3781
            _imageAnalysis.setDoSurfaceModeling(false);
          }
        }
      }

      for (ReconstructedImages offsetReconstructedImages : retestOffsetToReconstructedImagesMap.values())
        offsetReconstructedImages.decrementReferenceCount();
      retestOffsetToReconstructedImagesMap.clear();

      // Reset our offset index to 0.
      _focusConfirmationOffsetForCurrentThread.set(0);
    }
  }

  /**
   * Retests the specified ReconstructionRegion (if retest is enabled for the region).  This is
   * accomplished by:
   *
   * 1.  Compile lists of all subtypes in the region which do and do not have defective joints/components (exclude
   *     subtypes with only 'excluded' defects like Shorts).
   * 2.  Save original joint and component results.
   * 3.  If enabled, rRetest each passing subtype with a very anomalous looking slice height at the height predicted
   *     by limited surface model, replacing the orginal results with any failure found at the new height.
   * 4.  If enabled, retest each subtype with defects at the focus confirmation slice heights, replacing any
   *     non-excluded failures if all joints now pass.
   *
   * @author Matt Wharton
   * @author John Heumann (refactored into multiple routines, added retest passing outliers)
   */
  void retestRegionIfNeeded(ReconstructedImages reconstructedImages, boolean doSurfaceModeling) throws XrayTesterException
  {
    // If focus confirmation is disabled, we don't need to go any further.
    boolean isFocusConfirmationEnabled = _config.getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_CONFIRMATION);
   
    //Siew Yeng - XCR1745 - Semi-automated mode
    if(TestExecution.getInstance().isSemiAutomatedModeEnabled())
    {
      isFocusConfirmationEnabled = true;
    }
    
    if (isFocusConfirmationEnabled == false)
    {
      return;
    }
     
    //Focus Confirmation Engine shouldn't be used in RFP. @Author Kee, chin Seong
    //if(reconstructedImages.getReconstructionRegion().getComponent().getPadOne().getPadTypeSettings().getGlobalSurfaceModel().equals(GlobalSurfaceModelEnum.USE_RFB))
    //{
    //  System.out.println(reconstructedImages.getReconstructionRegion().getComponent() + 
    ////                     " RFB has been used for this particular reconstructedImages, focus confirmation will be not used !");
    //  return;
    //}
    
    Assert.expect(reconstructedImages != null);
    // Focus Confirmation will be disabled if region use GLOBAL_SURFACE_MODEL. @Author Lim, Seng Yew
    if(reconstructedImages.getReconstructionRegion().getComponent().getPadOne().getPadTypeSettings().getGlobalSurfaceModel().equals(GlobalSurfaceModelEnum.GLOBAL_SURFACE_MODEL))
    {
//      System.out.println(reconstructedImages.getReconstructionRegion().getComponent() + 
//                         " GSM has been selected for this particular component, focus confirmation will be disabled !");
      return;
    }
    
    // If a subtype is marked to skip Focus Confirmation, we will skip it here.
    Assert.expect(reconstructedImages.getReconstructionRegion().getSubtypes().size() == 1);
    Subtype currentSubtype = reconstructedImages.getReconstructionRegion().getSubtypes().iterator().next();
    
    if(TestExecution.getInstance().isSemiAutomatedModeEnabled() == false)
    {
      // Siew Yeng - discussed with wei chin and decided to remove this here but 
      // skip focus confirmation directly at JointTypeEnum retestFailingPins flag
      // bee-hoon.goh - Skip FocusConfirmation for PTH and Pressfit
//      JointTypeEnum jointTypeEnum = reconstructedImages.getReconstructionRegion().getJointTypeEnum();    
//      if (jointTypeEnum.equals(JointTypeEnum.THROUGH_HOLE))
//      {
//        return;
//      }
//      if (jointTypeEnum.equals(JointTypeEnum.PRESSFIT))
//      {
//        return;
//      }
      // bee-hoon.goh - Skip FocusConfirmation for PSH ON subtype
      if (currentSubtype.usePredictiveSliceHeight() || currentSubtype.useAutoPredictiveSliceHeight())
      {
        return;
      }
    }
     
    if (currentSubtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION))
    {
        boolean isFocusConfirmationEnableForSubtype = false;
        Serializable value = currentSubtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION);
        Assert.expect(value instanceof String);
        if (value.equals("Off"))
            isFocusConfirmationEnableForSubtype = false;
        else if (value.equals("On"))
            isFocusConfirmationEnableForSubtype = true;
        else
          Assert.expect(false, "Unexpected Focus Confirmation value.");
        
        //Siew Yeng - XCR1745 - Semi-automated mode
        if(isFocusConfirmationEnableForSubtype == false && TestExecution.getInstance().isSemiAutomatedModeEnabled() == false)
          return;
    }

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    InspectionEngine inspectionEngine = InspectionEngine.getInstance();

    boolean retestFailingPinsEnabledForRegion =
      reconstructionRegion.getJointTypeEnum().isAvailableForRetestFailingPins();
    
    //Siew Yeng - XCR-2015 - Semi-auto mode
    // - force LGA joint type to run focus confirmation when semi-auto mode enabled.
    if(TestExecution.getInstance().isSemiAutomatedModeEnabled() && 
      reconstructionRegion.getJointTypeEnum() == JointTypeEnum.LGA)
      retestFailingPinsEnabledForRegion = true;
      
    // Note that we will only retest passing outliers if
    //   1) EnableFocusConfirmation is true (software.config)
    //   2) UseSurfaceModelDuringFocusConfirmation is true (software.config)
    //   3) RetestPassingOutliersDuringFocusConfirmation is true (software.config)
    // & 4) This joint type is marked availableForRetestPassingOutliers in JointTypeEnum
    boolean retestPassingOutliersEnabledForRegion =
      (reconstructionRegion.getJointTypeEnum().isAvailableForRetestPassingOutliers() &&
      _USE_SURFACE_MODEL_DURING_FOCUS_CONFIRMATION && _RETEST_PASSING_OUTLIERS_DURING_FOCUS_CONFIRMATION);

    if (retestFailingPinsEnabledForRegion || retestPassingOutliersEnabledForRegion)
    {
      // We don't want to allow the same component to be processed by multiple threads at once.
      synchronized(getSynchronizationObject(reconstructionRegion))
      {
        // Surface model may be continuously updated by other threads and regions. To prevent
        // inconsistencies, we cache the difference between the surface model predicted and
        // autofocused z heights on first access.
        Map<Subtype, Integer> subtypeToEstimatedZHeightErrorMap = new HashMap<Subtype, Integer>();
        Map<Subtype, Integer> subtypeToRawEstimatedZHeightErrorMap = new HashMap<Subtype, Integer>();
        Map<Subtype, Integer> subtypeToRetestPassingOffsetMap = new HashMap<Subtype, Integer>();

        // Get lists of defective and good subtypes in this reigon.
        List<Subtype> subtypesWithDefects = new LinkedList<Subtype>();
        List<Subtype> subtypesWithNoDefects = new LinkedList<Subtype>();
        getPassingAndFailingSubtypes(reconstructionRegion, subtypesWithDefects, subtypesWithNoDefects);

        // Cache reconstructed images by offset to avoid unnecessary re-reconstruct requests with multi-subtype regions.
        Map<Integer, ReconstructedImages> retestOffsetToReconstructedImagesMap = new HashMap<Integer, ReconstructedImages>();

        // These data structures keep track of excluded joint/component results and measurements (primarily short calls).
        Map<JointInspectionData, Pair<Collection<JointMeasurement>, Collection<JointIndictment>>> jointInspectionDataToExcludedResultsMap =
          new HashMap<JointInspectionData, Pair<Collection<JointMeasurement>, Collection<JointIndictment>>>();
        Map<ComponentInspectionData, Pair<Collection<ComponentMeasurement>, Collection<ComponentIndictment>>> componentInspectionDataToExcludedResultsMap =
          new HashMap<ComponentInspectionData, Pair<Collection<ComponentMeasurement>, Collection<ComponentIndictment>>>();

        // These data structures keep track of the original joint and component results and measurements.
        Map<JointInspectionData, JointInspectionResult> jointInspectionDataToResultMap =
          new HashMap<JointInspectionData, JointInspectionResult>();
        Map<ComponentInspectionData, ComponentInspectionResult> componentInspectionDataToResultMap =
          new HashMap<ComponentInspectionData, ComponentInspectionResult>();

        // Save the original JointInspectionResults and ComponentInspectionResults.
        // Also, save any 'excluded' joint and component measurements and indictments.
        getJointAndComponentResults(reconstructedImages, jointInspectionDataToResultMap, jointInspectionDataToExcludedResultsMap,
                                    componentInspectionDataToResultMap, componentInspectionDataToExcludedResultsMap,
                                    subtypeToEstimatedZHeightErrorMap, subtypeToRawEstimatedZHeightErrorMap);

        if (_LOG_FOCUS_CONFIRMATION_DATA)
        {
          for (Subtype subtype : reconstructionRegion.getSubtypes())
          {
            String refDes = reconstructionRegion.getComponent().getReferenceDesignator();
            String regionId = String.valueOf(reconstructionRegion.getRegionId());
            String retestFailingPinsStr = Boolean.toString(retestFailingPinsEnabledForRegion);
            String retestPassingOutliersStr = Boolean.toString(retestPassingOutliersEnabledForRegion);
            String subtypeName = subtype.getShortName();
            String jointTypeName = subtype.getJointTypeEnum().getName();
            String offsetInMicrometers = "0";
            String passedStr = Boolean.toString(subtypesWithNoDefects.contains(subtype));
            JointInspectionData failingJoint = reconstructionRegion.getJointInspectionDataList().get(0);
            String estimatedZHeightError = String.valueOf(getRawEstimatedZHeightError(
                failingJoint, reconstructedImages, subtypeToRawEstimatedZHeightErrorMap) / 1000);
            _focusConfirmationDataWriter.writeDataLine(Arrays.asList(refDes,
                                                                     regionId,
                                                                     retestFailingPinsStr,
                                                                     retestPassingOutliersStr,
                                                                     jointTypeName,
                                                                     subtypeName,
                                                                     offsetInMicrometers,
                                                                     passedStr,
                                                                     estimatedZHeightError));
          }
        }

        BooleanRef limitedSurfaceModelUsed = new BooleanRef(false);
        if (retestPassingOutliersEnabledForRegion)
        {
          retestSubtypesWithNoFailingPins(reconstructedImages, reconstructionRegion, inspectionEngine,
                                          subtypesWithNoDefects, subtypesWithDefects, retestOffsetToReconstructedImagesMap,
                                          jointInspectionDataToExcludedResultsMap, componentInspectionDataToExcludedResultsMap,
                                          jointInspectionDataToResultMap, componentInspectionDataToResultMap,
                                          subtypeToEstimatedZHeightErrorMap, subtypeToRawEstimatedZHeightErrorMap,
                                          subtypeToRetestPassingOffsetMap, limitedSurfaceModelUsed);
        }

        if (retestFailingPinsEnabledForRegion)
        {
          retestSubtypesWithFailingPins(reconstructedImages, reconstructionRegion, inspectionEngine,
                                        subtypesWithNoDefects, subtypesWithDefects, retestOffsetToReconstructedImagesMap,
                                        jointInspectionDataToExcludedResultsMap, componentInspectionDataToExcludedResultsMap,
                                        jointInspectionDataToResultMap, componentInspectionDataToResultMap,
                                        subtypeToEstimatedZHeightErrorMap, subtypeToRawEstimatedZHeightErrorMap,
                                        subtypeToRetestPassingOffsetMap, limitedSurfaceModelUsed, doSurfaceModeling);
        }
      }
    }
  }

  /**
   * Dump out plot info into csv file
   * @author Seng Yew Llim
   */
   private void dumpPlotInfoExtra(
      ReconstructedImages reconstructedImages, ReconstructionRegion reconstructionRegion,
      InspectionEngine inspectionEngine,
      List<Subtype> subtypesWithNoDefects,
      List<Subtype> subtypesWithDefects,
      Map<Integer, ReconstructedImages> retestOffsetToReconstructedImagesMap,
      Map<JointInspectionData, Pair<Collection<JointMeasurement>, Collection<JointIndictment>>> jointInspectionDataToExcludedResultsMap,
      Map<ComponentInspectionData, Pair<Collection<ComponentMeasurement>, Collection<ComponentIndictment>>> componentInspectionDataToExcludedResultsMap,
      Map<JointInspectionData, JointInspectionResult> jointInspectionDataToResultMap,
      Map<ComponentInspectionData, ComponentInspectionResult> componentInspectionDataToResultMap,
      Map<Subtype, Integer> subtypeToEstimatedZHeightErrorMap,
      Map<Subtype, Integer> subtypeToRawEstimatedZHeightErrorMap,
      Map<Subtype, Integer> subtypeToRetestPassingOffsetMap) throws XrayTesterException
  {
    // Keep track of whether or not we have any subtypes still failing after we complete our retest sequence.
    boolean anySubtypesStillFailing = false;
    Set<SliceNameEnum> slicesToExcludeFromRepairImages = new HashSet<SliceNameEnum>();
    Map<Subtype, List<JointInspectionData>> subtypeToInspectableJointsMap = null;
    ReconstructedImages reconstructedImagesAtOffset = null;

    try
    {
      subtypeToInspectableJointsMap = reconstructionRegion.getSubtypeToInspectableJointListMap();
      //reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters().get
      int subtypesDefectsCount = 0, jointInspectionDataCount = 0;

      subtypesWithDefects.addAll(subtypesWithNoDefects);

      // Retest only those subtypes which have defects.
      //DumpPlotInfoLogger.getInstance().append("ReferenceDes,PadName,ZoffSetInNanoMeters,Grid Array Diameter Measurement,Millimeters,Mils,NanoMeters");
      for (Subtype subtypeToRetest : subtypesWithDefects)
      {
        List<JointInspectionData> jointsInSubtype = subtypeToInspectableJointsMap.get(subtypeToRetest);

        for (JointInspectionData jointInspectionData : jointsInSubtype)
        {
          //if(jointInspectionData.getPad().getName().matches("H1")){
//          int padSliceZHeight = _zHeightEstimator.getPadSliceZHeight(jointInspectionData, reconstructedImages);
          int padSliceZHeight = 0;
          int estimatedZHeightError = getEstimatedZHeightError(jointInspectionData,
                                                               reconstructedImages,
                                                               subtypeToEstimatedZHeightErrorMap,
                                                               subtypeToRawEstimatedZHeightErrorMap);
          int typicalPadDimension = Math.max(jointInspectionData.getComponent().getPadOne().getWidthInNanoMeters(),jointInspectionData.getComponent().getPadOne().getLengthInNanoMeters());
          int stepSize = 19000*2;
          //int numOfSteps = typicalPadDimension / stepSize;
          int numOfSteps = 2;
          int startZheight = -1 * numOfSteps * stepSize;
          int endZheight = numOfSteps * stepSize;
          int midBallSliceZHeight = 0;
          //Chin-Seong, Kee - Only Midball slice will be get Height when component in is BGA 
          if (jointInspectionData.getInspectionFamilyEnum().equals(InspectionFamilyEnum.GRID_ARRAY) &&
                    (jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.COLLAPSABLE_BGA) ||
                     jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.NON_COLLAPSABLE_BGA) ||
                     jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
              )
          {
              midBallSliceZHeight = reconstructedImages.getReconstructedSlice(SliceNameEnum.MIDBALL).getHeightInNanometers();
              for (int retestOffsetInNanometers=startZheight; retestOffsetInNanometers <= endZheight; retestOffsetInNanometers+=stepSize)
              {                  
                //int retestOffsetInNanometers = 0;
                int zOffsetInNanometers = retestOffsetInNanometers + estimatedZHeightError + padSliceZHeight;
                _focusConfirmationOffsetForCurrentThread.set(zOffsetInNanometers);

                // If we already have the image for this offset in our cache, just use it.  Otherwise, acquire it and add it
                // to the cache.
                reconstructedImagesAtOffset = retestOffsetToReconstructedImagesMap.get(zOffsetInNanometers);
                if (reconstructedImagesAtOffset == null)
                {
                  BooleanRef reReconstructedImagesAvailable = new BooleanRef(true);
                  reconstructedImagesAtOffset =
                  inspectionEngine.getReReconstructedImagesForRegionAtOffset(reconstructionRegion,
                                                                               zOffsetInNanometers,
                                                                               reReconstructedImagesAvailable);

                  // If we didn't get our re-reconstructed images back, we must be in the middle of an abort or
                  // something.  Just return if this is the case.
                  if (reReconstructedImagesAvailable.getValue() == false)
                  {
                    return;
                  }
                  reReconstructedImagesAvailable = null;
                  // Special case kludge. The above call offsets all slices by estimatedZHeightError.
                  // This is correct for single slice regions, and for pth where multiple slices are all at
                  // fixed steps from a  single autofocused slice height.  For chips and pcaps, we want to offset
                  // only the real pad slice, so we restore the original image for the other slice.
//                  InspectionFamilyEnum inspectionFamilyEnum = reconstructionRegion.getInspectionFamilyEnum();
//                  if (inspectionFamilyEnum.equals(InspectionFamilyEnum.CHIP) ||
//                      inspectionFamilyEnum.equals(InspectionFamilyEnum.POLARIZED_CAP))
//                  {
//                    JointInspectionData firstJoint = jointsInSubtype.get(0);
//                    if (ChipMeasurementAlgorithm.testAsOpaque(firstJoint))
//                    {
//                      reconstructedImagesAtOffset.replaceASlice(reconstructedImages, SliceNameEnum.CLEAR_CHIP_PAD);
//                      slicesToExcludeFromRepairImages.add(SliceNameEnum.CLEAR_CHIP_PAD);
//                    }
//                    else
//                    {
//                      reconstructedImagesAtOffset.replaceASlice(reconstructedImages, SliceNameEnum.OPAQUE_CHIP_PAD);
//                      slicesToExcludeFromRepairImages.add(SliceNameEnum.OPAQUE_CHIP_PAD);
//                    }
//                  }

                  retestOffsetToReconstructedImagesMap.put(zOffsetInNanometers, reconstructedImagesAtOffset);
                }

                boolean subtypePassedAtCurrentRetestOffset =
                  retestJoints(subtypeToRetest, jointsInSubtype, reconstructedImagesAtOffset);

                String refDes = reconstructionRegion.getComponent().getReferenceDesignator();
      //          String regionId = String.valueOf(reconstructionRegion.getRegionId());
      //          String subtypeName = subtypeToRetest.getShortName();s
      //          String jointTypeName = subtypeToRetest.getJointTypeEnum().getName();reconstructionRegion.getComponent().getReferenceDesignator()
      //          String retestOffsetInMicrometersStr = String.valueOf(zOffsetInNanometers / 1000);
      //          String passedStr = Boolean.toString(subtypePassedAtCurrentRetestOffset);
      //          String estimatedZHeightErrorString = String.valueOf(
      //            getRawEstimatedZHeightError(failingJoint, reconstructedImages, subtypeToRawEstimatedZHeightErrorMap) / 1000);

//                if (jointInspectionData.getInspectionFamilyEnum().equals(InspectionFamilyEnum.GRID_ARRAY) &&
//                        (jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.COLLAPSABLE_BGA) ||
//                         jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.NON_COLLAPSABLE_BGA) ||
//                         jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
//                   )
//                {
                  JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
                  if (jointInspectionResult.hasJointMeasurement(SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_DIAMETER) == true)
                  {
                    String padName = jointInspectionData.getPad().getName();
                    JointMeasurement jointMeasurement = jointInspectionResult.getJointMeasurement(SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_DIAMETER);
    //                DumpPlotInfoLogger.getInstance().append(refDes+","+padName+","+padSliceZHeight+","+jointMeasurement.getZHeightInNanoMeters()+","+MeasurementEnum.GRID_ARRAY_DIAMETER.toString()+","+jointMeasurement.getMeasurementUnitsEnum().getName()+","+jointMeasurement.getValue());
                    DumpPlotInfoLogger.getInstance().append(refDes+","+padName+","+zOffsetInNanometers+","+MeasurementEnum.GRID_ARRAY_DIAMETER.toString()+","+jointMeasurement.getValue()+
                                               ","+MathUtil.convertMillimetersToMils(jointMeasurement.getValue()) + "," +
                                               MathUtil.convertMillimetersToNanometers(jointMeasurement.getValue()) + "," + midBallSliceZHeight);
                  }
//                }
              }
          }
        }
        subtypeToInspectableJointsMap.clear();
        subtypeToInspectableJointsMap = null;        
        reconstructedImagesAtOffset = null;
      }
    }
    finally
    {
       // The following call must alway be made, even if we're not changing any calls.
       // This serves to restore the original results in that case.
       updateRepairImagesAndResults(reconstructionRegion,
                                           anySubtypesStillFailing,
                                           retestOffsetToReconstructedImagesMap,
                                           jointInspectionDataToResultMap,
                                           componentInspectionDataToResultMap,
                                           slicesToExcludeFromRepairImages);

              // Cleanup and free up memory
       for (ReconstructedImages offsetReconstructedImages : retestOffsetToReconstructedImagesMap.values())
           offsetReconstructedImages.decrementReferenceCount();
       retestOffsetToReconstructedImagesMap.clear();
      // Reset our offset index to 0.
      _focusConfirmationOffsetForCurrentThread.set(0);
    }
  }

  void dumpPlotInfo(ReconstructedImages reconstructedImages) throws XrayTesterException
  {
    boolean isDeveloperDebugMode = _config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE);
    if (isDeveloperDebugMode == false)
    {
      return;
    }
    Assert.expect(reconstructedImages != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    InspectionEngine inspectionEngine = InspectionEngine.getInstance();

    // We don't want to allow the same component to be processed by multiple threads at once.
    synchronized(getSynchronizationObject(reconstructionRegion))
    {
          // Enable dumpPlotInfo for research now
      //DumpPlotInfoLogger.getInstance().setDumpPlotInfo(true);
      // Surface model may be continuously updated by other threads and regions. To prevent
      // inconsistencies, we cache the difference between the surface model predicted and
      // autofocused z heights on first access.
      Map<Subtype, Integer> subtypeToEstimatedZHeightErrorMap = new HashMap<Subtype, Integer>();
      Map<Subtype, Integer> subtypeToRawEstimatedZHeightErrorMap = new HashMap<Subtype, Integer>();
      Map<Subtype, Integer> subtypeToRetestPassingOffsetMap = new HashMap<Subtype, Integer>();

      // Get lists of defective and good subtypes in this reigon.
      List<Subtype> subtypesWithDefects = new LinkedList<Subtype>();
      List<Subtype> subtypesWithNoDefects = new LinkedList<Subtype>();
      getPassingAndFailingSubtypes(reconstructionRegion, subtypesWithDefects, subtypesWithNoDefects);

      // Cache reconstructed images by offset to avoid unnecessary re-reconstruct requests with multi-subtype regions.
      Map<Integer, ReconstructedImages> retestOffsetToReconstructedImagesMap = new HashMap<Integer, ReconstructedImages>();

      // These data structures keep track of excluded joint/component results and measurements (primarily short calls).
      Map<JointInspectionData, Pair<Collection<JointMeasurement>, Collection<JointIndictment>>> jointInspectionDataToExcludedResultsMap =
        new HashMap<JointInspectionData, Pair<Collection<JointMeasurement>, Collection<JointIndictment>>>();
      Map<ComponentInspectionData, Pair<Collection<ComponentMeasurement>, Collection<ComponentIndictment>>> componentInspectionDataToExcludedResultsMap =
        new HashMap<ComponentInspectionData, Pair<Collection<ComponentMeasurement>, Collection<ComponentIndictment>>>();

      // These data structures keep track of the original joint and component results and measurements.
      Map<JointInspectionData, JointInspectionResult> jointInspectionDataToResultMap =
        new HashMap<JointInspectionData, JointInspectionResult>();
      Map<ComponentInspectionData, ComponentInspectionResult> componentInspectionDataToResultMap =
        new HashMap<ComponentInspectionData, ComponentInspectionResult>();

      // Save the original JointInspectionResults and ComponentInspectionResults.
      // Also, save any 'excluded' joint and component measurements and indictments.
      getJointAndComponentResults(reconstructedImages, jointInspectionDataToResultMap, jointInspectionDataToExcludedResultsMap,
                                  componentInspectionDataToResultMap, componentInspectionDataToExcludedResultsMap,
                                  subtypeToEstimatedZHeightErrorMap, subtypeToRawEstimatedZHeightErrorMap);

      dumpPlotInfoExtra(reconstructedImages, reconstructionRegion, inspectionEngine,
                   subtypesWithNoDefects, subtypesWithDefects, retestOffsetToReconstructedImagesMap,
                   jointInspectionDataToExcludedResultsMap, componentInspectionDataToExcludedResultsMap,
                   jointInspectionDataToResultMap, componentInspectionDataToResultMap,
                   subtypeToEstimatedZHeightErrorMap, subtypeToRawEstimatedZHeightErrorMap,
                   subtypeToRetestPassingOffsetMap);

      //DumpPlotInfoLogger.getInstance().setDumpPlotInfo(false);
    }
    
  }

  /**
   * Builds 2 Lists... one of failing and one passing Subtypes for the specified ReconstructionRegion
   *
   * @author Matt Wharton
   * @author John Heumann
   */
  private void getPassingAndFailingSubtypes(ReconstructionRegion reconstructionRegion,
      List<Subtype> subtypesWithDefects, List<Subtype> subtypesWithNoDefects)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(subtypesWithDefects != null);
    Assert.expect(subtypesWithNoDefects != null);
    // Keep track of which subtypes have defects
    Map<Subtype, List<JointInspectionData>> subtypeToInspectableJointsMap =
        reconstructionRegion.getSubtypeToInspectableJointListMap();
    for (Map.Entry<Subtype, List<JointInspectionData>> mapEntry : subtypeToInspectableJointsMap.entrySet())
    {
      Subtype subtype = mapEntry.getKey();
      boolean subtypePassed = true;

      for (JointInspectionData jointInspectionData : mapEntry.getValue())
      {
        JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
        ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
        ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();

        boolean jointFailed = (jointInspectionResult.passed() == false);
        boolean componentFailed = (componentInspectionResult.passed() == false);

        if (jointFailed || componentFailed)
        {
          subtypesWithDefects.add(subtype);
          subtypePassed = false;
          break;
        }
      }
      if (subtypePassed)
      {
        subtypesWithNoDefects.add(subtype);
      }
    }
  }

  /**
   * @author Matt Wharton
   * @author Peter Esbensen
   */
  private Pair<Collection<JointMeasurement>, Collection<JointIndictment>> getExcludedJointMeasurementsAndIndictments(
      JointInspectionData jointInspectionData,
      ReconstructedImages reconstructedImages,
      Map<Subtype, Integer> subtypeToEstimatedZHeightErrorMap,
      Map<Subtype, Integer> subtypeToRawEstimatedZHeightErrorMap)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructedImages != null);

    int absEstimatedZHeightError = Math.abs(getEstimatedZHeightError(jointInspectionData,
                                                                     reconstructedImages,
                                                                     subtypeToEstimatedZHeightErrorMap,
                                                                     subtypeToRawEstimatedZHeightErrorMap));
    boolean excludeResults = true;
    // if the z height is way off, don't exclude anything!
    if ((_USE_SURFACE_MODEL_DURING_FOCUS_CONFIRMATION) &&
        (absEstimatedZHeightError > _MINIMUM_Z_ERROR_FOR_SHORTS_RETEST))
      excludeResults = false;

    Collection<JointIndictment> excludedJointIndictments = new HashSet<JointIndictment>();
    Collection<JointMeasurement> excludedJointMeasurements = new HashSet<JointMeasurement>();

    if (excludeResults)
    {
      // get the excluded joint indictments
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      for (JointIndictment jointIndictment : jointInspectionResult.getIndictments())
      {
        IndictmentEnum indictmentEnum = jointIndictment.getIndictmentEnum();
        if (_indictmentEnumsToExcludeFromRetest.contains(indictmentEnum))
        {
          excludedJointIndictments.add(jointIndictment);
        }
      }

      // get the excluded joint measurements
      for (JointMeasurement jointMeasurement : jointInspectionResult.getAllMeasurements())
      {
        MeasurementEnum measurementEnum = jointMeasurement.getMeasurementEnum();
        if (_measurementEnumsToExcludeFromRetest.contains(measurementEnum))
        {
          excludedJointMeasurements.add(jointMeasurement);
        }
      }
    }

    Pair<Collection<JointMeasurement>, Collection<JointIndictment>> excludedJointResults =
      new Pair<Collection<JointMeasurement>, Collection<JointIndictment>>(excludedJointMeasurements, excludedJointIndictments);

    return excludedJointResults;
  }

  /**
   * @author Matt Wharton
   * @author Peter Esbensen
   */
  private Pair<Collection<ComponentMeasurement>, Collection<ComponentIndictment>>
      getExcludedComponentMeasurementsAndIndictments(ComponentInspectionData componentInspectionData,
                                                     ReconstructedImages reconstructedImages,
                                                     Map<Subtype, Integer> subtypeToEstimatedZHeightErrorMap,
                                                     Map<Subtype, Integer> subtypeToRawEstimatedZHeightErrorMap)
  {
    Assert.expect(componentInspectionData != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    int absEstimatedZHeightError = Math.abs(getEstimatedZHeightError(
                                              reconstructionRegion.getJointInspectionDataList().get(0),
                                              reconstructedImages,
                                              subtypeToEstimatedZHeightErrorMap,
                                              subtypeToRawEstimatedZHeightErrorMap));

    boolean excludeResults = true;
    // if the z height is way off, don't exclude anything!
    if ((_USE_SURFACE_MODEL_DURING_FOCUS_CONFIRMATION) &&
        (absEstimatedZHeightError > _MINIMUM_Z_ERROR_FOR_SHORTS_RETEST))
      excludeResults = false;

    Collection<ComponentMeasurement> excludedComponentMeasurements = new HashSet<ComponentMeasurement>();
    Collection<ComponentIndictment> excludedComponentIndictments = new HashSet<ComponentIndictment>();

    if (excludeResults)
    {
      // get the excluded component measurements
      ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();
      for (ComponentMeasurement componentMeasurement : componentInspectionResult.getAllMeasurements())
      {
        MeasurementEnum measurementEnum = componentMeasurement.getMeasurementEnum();
        if (_measurementEnumsToExcludeFromRetest.contains(measurementEnum))
        {
          excludedComponentMeasurements.add(componentMeasurement);
        }
      }

      // get the excluded component indictments
      for (ComponentIndictment componentIndictment : componentInspectionResult.getIndictments())
      {
        IndictmentEnum indictmentEnum = componentIndictment.getIndictmentEnum();
        if (_indictmentEnumsToExcludeFromRetest.contains(indictmentEnum))
        {
          excludedComponentIndictments.add(componentIndictment);
        }
      }
    }

    // combine them into a pair to return
    Pair<Collection<ComponentMeasurement>, Collection<ComponentIndictment>> excludedComponentResults = new Pair<Collection<ComponentMeasurement>, Collection<ComponentIndictment>>(
      excludedComponentMeasurements, excludedComponentIndictments);

    return excludedComponentResults;
  }

  /**
   * Get an object that can be used to ensure that focus confirmation will not affect componentInspectionResults while
   * another thread is using them.
   *
   * Focus Confirmation will potentially modify jointInspectionResults within the active region (safe, because only one
   * thread is active on a single region), but it will also modify componentInspectionResults (not safe, because the
   * component may be spread across multiple regions which can potentially be processed by multiple threads).
   *
   * To synchronize access to the componentInpsectionResults, all threads should use this method to get the object they
   * will synchronize on.  Like this:
   *
   * synchronize (focusConfirmationEngine.getSynchronizationObject(reconstructionRegion))
   * {
   *  . . .
   *
   *
   * @author Peter Esbensen
   */
  public Component getSynchronizationObject(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    return reconstructionRegion.getComponent();
  }

  /**
   * @author John Heumann
   * @author Khang Wah, Chnee
   * - remove 2.5d images from any contrast enhancement activity
   */
  private void saveFocusConfirmationDebugImages(ReconstructionRegion reconstructionRegion,
                                                ReconstructedImages originalReconstructedImages,
                                                ReconstructedImages newReconstructedImages,
                                                Boolean originalPassed) throws DatastoreException
  {
    Assert.expect(originalReconstructedImages != null);
    Assert.expect(newReconstructedImages != null);

    String regionId = String.valueOf(reconstructionRegion.getRegionId());
    String refDes = reconstructionRegion.getComponent().getReferenceDesignator();
    String prefix;
    InspectionEngine inspectionEngine = InspectionEngine.getInstance();
    if (originalPassed)
    {
      // retest passing prefix
      prefix = Directory.getFocusConfirmationImagesDir(inspectionEngine.getInspectionStartTimeInMillis()) + File.separator + refDes + "_" + regionId + "_" + "rtp";
    }
    else
    {
      // retest failing prefix
      prefix = Directory.getFocusConfirmationImagesDir(inspectionEngine.getInspectionStartTimeInMillis()) + File.separator + refDes + "_" + regionId + "_" + "rtf";
    }


    Map<SliceNameEnum, Image> correctedImages = new TreeMap<SliceNameEnum,Image>();
    for (ReconstructedSlice reconstructedSlice : originalReconstructedImages.getReconstructedSlices())
    {
      correctedImages.put(reconstructedSlice.getSliceNameEnum(), new Image(reconstructedSlice.getImage()));
    }

    ImageEnhancement.autoEnhanceContrastInPlace(ImageManager.getInstance().excludeUnnecessaryImageFromEnhancement(correctedImages).values());

    for (Slice slice : originalReconstructedImages.getReconstructionRegion().getInspectedSlices())
    {
      SliceNameEnum sliceNameEnum = slice.getSliceName();
      String sliceNumber = String.valueOf(sliceNameEnum.getId());

      Image image = correctedImages.remove(sliceNameEnum);
      java.awt.image.BufferedImage buffImg = image.getBufferedImage();
      image.decrementReferenceCount();  // get rid of the memory ASAP

      XrayImageIoUtil.saveJpegImage(buffImg, prefix + "_orig_" + sliceNumber + ".jpg");
    }

    // @optimize PWL : This sort of check should be done earlier, assuming that it actually is entered
    for (Image image : correctedImages.values())
    {
      image.decrementReferenceCount();
    }

    correctedImages.clear();
    for (ReconstructedSlice reconstructedSlice : newReconstructedImages.getReconstructedSlices())
    {
      correctedImages.put(reconstructedSlice.getSliceNameEnum(), new Image(reconstructedSlice.getImage()));
    }
    ImageEnhancement.autoEnhanceContrastInPlace(ImageManager.getInstance().excludeUnnecessaryImageFromEnhancement(correctedImages).values());

    for (Slice slice : newReconstructedImages.getReconstructionRegion().getInspectedSlices())
    {
      SliceNameEnum sliceNameEnum = slice.getSliceName();
      String sliceNumber = String.valueOf(sliceNameEnum.getId());

      Image image = correctedImages.remove(sliceNameEnum);
      java.awt.image.BufferedImage buffImg = image.getBufferedImage();
      image.decrementReferenceCount();  // get rid of the memory ASAP

      XrayImageIoUtil.saveJpegImage(buffImg, prefix + "_new_" + sliceNumber + ".jpg");
    }

    // @optimize PWL : This sort of check should be done earlier, assuming that it actually is entered
    for (Image image : correctedImages.values())
    {
      image.decrementReferenceCount();
    }
  }

  /**
   * @author Siew Yeng
   */
  public Collection<IndictmentEnum> getExcludedIndictmentEnumsToExcludeFromRetest()
  {
    Assert.expect(_indictmentEnumsToExcludeFromRetest != null);
    
    return _indictmentEnumsToExcludeFromRetest;
  }
  
}
