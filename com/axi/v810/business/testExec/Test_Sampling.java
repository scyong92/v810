package com.axi.v810.business.testExec;

import java.io.*;

import com.axi.util.*;
import java.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class Test_Sampling extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_Sampling());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Sampling sampling = Sampling.getInstance();

    // capture original state of config file settings
    PanelSamplingModeEnum origPanelSamplingModeEnum = sampling.getPanelSamplingModeEnum();
    int origSkipEveryNthPanel = sampling.getSkipEveryNthPanel();
    int origTestEveryNthPanel = sampling.getTestEveryNthPanel();

    try
    {
      try
      {
        sampling.setPanelSamplingModeEnum(null);
        Assert.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing
      }

      // test that all panels are tested when there is no sampling
      sampling.setPanelSamplingModeEnum(PanelSamplingModeEnum.NO_SAMPLING);
      Expect.expect(sampling.testPanel());
      Expect.expect(sampling.testPanel());
      Expect.expect(sampling.testPanel());

      // test the testEveryNthPanel
      sampling.setPanelSamplingModeEnum(PanelSamplingModeEnum.TEST_EVERY_NTH_PANEL);

      try
      {
        sampling.setTestEveryNthPanel(-1);
        Assert.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing
      }

      // set to 1 - all panels should be tested
      sampling.setTestEveryNthPanel(1);
      Expect.expect(sampling.testPanel());
      Expect.expect(sampling.testPanel());
      Expect.expect(sampling.testPanel());

      // set to 2 - every other panel should be tested
      sampling.setTestEveryNthPanel(2);
      Expect.expect(sampling.testPanel());
      Expect.expect(sampling.testPanel() == false);
      Expect.expect(sampling.testPanel());
      Expect.expect(sampling.testPanel() == false);
      Expect.expect(sampling.testPanel());

      // set to 3 - every 3rd panel should be tested
      sampling.setTestEveryNthPanel(3);
      Expect.expect(sampling.testPanel());
      Expect.expect(sampling.testPanel() == false);
      Expect.expect(sampling.testPanel() == false);
      Expect.expect(sampling.testPanel());
      Expect.expect(sampling.testPanel() == false);
      Expect.expect(sampling.testPanel() == false);
      Expect.expect(sampling.testPanel());

      // test the skipEveryNthPanel
      sampling.setPanelSamplingModeEnum(PanelSamplingModeEnum.SKIP_EVERY_NTH_PANEL);

      try
      {
        sampling.setSkipEveryNthPanel(-1);
        Assert.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing
      }

       // set to 1 - all panels should be skipped
      sampling.setSkipEveryNthPanel(1);
      Expect.expect(sampling.testPanel() == false);
      Expect.expect(sampling.testPanel() == false);
      Expect.expect(sampling.testPanel() == false);

      // set to 2 - every other panel should be tested
      sampling.setSkipEveryNthPanel(2);
      Expect.expect(sampling.testPanel());
      Expect.expect(sampling.testPanel() == false);
      Expect.expect(sampling.testPanel());
      Expect.expect(sampling.testPanel() == false);
      Expect.expect(sampling.testPanel());

      // set to 3 - every 3rd panel should be skipped
      sampling.setSkipEveryNthPanel(3);
      Expect.expect(sampling.testPanel());
      Expect.expect(sampling.testPanel());
      Expect.expect(sampling.testPanel() == false);
      Expect.expect(sampling.testPanel());
      Expect.expect(sampling.testPanel());
      Expect.expect(sampling.testPanel() == false);
      Expect.expect(sampling.testPanel());
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      // return the config file settings to their original state
      try
      {
        sampling.setPanelSamplingModeEnum(origPanelSamplingModeEnum);
        sampling.setSkipEveryNthPanel(origSkipEveryNthPanel);
        sampling.setTestEveryNthPanel(origTestEveryNthPanel);
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }
}
