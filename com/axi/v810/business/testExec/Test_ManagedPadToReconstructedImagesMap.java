package com.axi.v810.business.testExec;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;

/**
 * @author Peter Esbensen
 */
class Test_ManagedPadToReconstructedImagesMap extends AlgorithmUnitTest
{
  private ImageSetData _imageSet = null;
  private ImageManager _imageManager = ImageManager.getInstance();

  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute (new Test_ManagedPadToReconstructedImagesMap());
  }

  /**
   * @author Peter Esbensen
   */
  private Project loadProjectAndImageSet()
  {
    Project project = getProject("FAMILIES_ALL_RLV");
    Expect.expect(project != null);

    TestProgram testProgram = project.getTestProgram();
    testProgram.clearFilters();

    List<ImageSetData> imageSetDataList = null;
    try
    {
      imageSetDataList = _imageManager.getCompatibleImageSetData(project);
    }
    catch (DatastoreException ex)
    {
      Expect.expect(false);
    }

    for (ImageSetData imageSetData : imageSetDataList)
    {
      if (imageSetData.getImageSetName().equals("normalUnitTestImageSet"))
      {
        _imageSet = imageSetData;
        break;
      }
    }
    Expect.expect(_imageSet != null);

    return project;
  }

  /**
   * @author Peter Esbensen
   */
  private void addAllJointsAndImages(Collection<ReconstructionRegion> reconstructionRegions,
                                     ImageManager imageManager,
                                     TestProgram testProgram,
                                     ManagedPadToReconstructedImageMap managedPadToReconstructedImagesMap)
  {
    Assert.expect(reconstructionRegions != null);
    Assert.expect(imageManager != null);
    Assert.expect(testProgram != null);
    Assert.expect(managedPadToReconstructedImagesMap != null);
    Assert.expect(_imageSet != null);

    try
    {
      for (ReconstructionRegion reconstructionRegion : reconstructionRegions)
      {
        ReconstructedImages reconstructedImages = imageManager.loadInspectionImages(_imageSet,reconstructionRegion);
        for (JointInspectionData jointInspectionData : reconstructionRegion.getJointInspectionDataList())
        {
          managedPadToReconstructedImagesMap.put(jointInspectionData.getPad(), reconstructedImages);
        }
      }
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace();
      Assert.expect(false);
    }
  }

  /**
   * @author Peter Esbensen
   */
  void pullImagesOutAndRemoveAllJoints(Collection<ReconstructionRegion> reconstructionRegions,
      ImageManager imageManager,
      TestProgram testProgram,
      ManagedPadToReconstructedImageMap managedPadToReconstructedImagesMap)
  {
    Assert.expect(reconstructionRegions != null);
    Assert.expect(imageManager != null);
    Assert.expect(testProgram != null);
    Assert.expect(managedPadToReconstructedImagesMap != null);

    try
    {
      for (ReconstructionRegion reconstructionRegion : reconstructionRegions)
      {
        for (JointInspectionData jointInspectionData : reconstructionRegion.getJointInspectionDataList())
        {
          Pad pad = jointInspectionData.getPad();
          ReconstructedImages reconstructedImages = managedPadToReconstructedImagesMap.get(pad);
          Expect.expect(reconstructedImages != null);
          managedPadToReconstructedImagesMap.remove(pad);
        }
      }
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace();
      Assert.expect(false);
    }
  }

  /**
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    final int MAXIMUM_ALLOWABLE_MEMORY_USAGE_IN_BYTES = 20000000; // approx. 20 megs

    // create the object to test
    ManagedPadToReconstructedImageMap managedPadToReconstructedImagesMap = new ManagedPadToReconstructedImageMap(MAXIMUM_ALLOWABLE_MEMORY_USAGE_IN_BYTES);
    Expect.expect(managedPadToReconstructedImagesMap.isEmpty());

    // set up project and testProgram filter
    Project project = loadProjectAndImageSet();
    TestProgram testProgram = project.getTestProgram();
    testProgram.addFilter(project.getPanel().getBoards().get(0).getComponent("U27"));

    ImageManager imageManager = ImageManager.getInstance();

    Collection<ReconstructionRegion> reconstructionRegions = testProgram.getFilteredInspectionRegions();

    // ok, let's go through a series of operations on the object

    addAllJointsAndImages(reconstructionRegions, imageManager, testProgram, managedPadToReconstructedImagesMap);

    Expect.expect(managedPadToReconstructedImagesMap.isEmpty() == false);

    pullImagesOutAndRemoveAllJoints(reconstructionRegions, imageManager, testProgram, managedPadToReconstructedImagesMap);

    Expect.expect(managedPadToReconstructedImagesMap.isEmpty());

    addAllJointsAndImages(reconstructionRegions, imageManager, testProgram, managedPadToReconstructedImagesMap);

    Expect.expect(managedPadToReconstructedImagesMap.isEmpty() == false);

    try
    {
      managedPadToReconstructedImagesMap.clear();
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace();
      Assert.expect(false);
    }

    Expect.expect(managedPadToReconstructedImagesMap.isEmpty());
  }
}
