package com.axi.v810.business.testExec;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class InvalidPanelThicknessBusinessException extends BusinessException
{
  /**
   * @author Bill Darbie
   */
  public InvalidPanelThicknessBusinessException(MathUtilEnum units, double minThicknessInUnits, double maxThicknessInUnits, double actualThicknessInUnits)
  {
    super(new LocalizedString("BUS_INVALID_PANEL_THICKNESS_EXCEPTION_KEY", new Object[]{units, minThicknessInUnits, maxThicknessInUnits, actualThicknessInUnits}));
    Assert.expect(units != null);
  }
}
