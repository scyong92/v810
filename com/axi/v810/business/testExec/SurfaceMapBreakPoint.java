package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class SurfaceMapBreakPoint extends ScanPassBreakPoint 
{  
  private int _regionsThatMustBeMarkedCompletePriorToSurfaceMap = -1;
  private TestSubProgram _testSubProgram;
  
  private static PerformanceLogUtil _performanceLog = PerformanceLogUtil.getInstance();
  private static InspectionEventObservable _inspectionEventObservable;
    
  protected static PspEngine _pspEngine;
  
  private SurfaceMapping _surfaceMapping = SurfaceMapping.getInstance();
  private ScanPass _scanPass;
  
  private FileLoggerAxi _surfaceMapLogger = null;
  
  /**
   * Constructor
   * 
   * @author Cheah Lee Herng 
   */
  public SurfaceMapBreakPoint(TestSubProgram testSubProgram,
                              ScanPass scanPass,
                              double speedFactor,
                              FileLoggerAxi fileLogger)
  {
    super(scanPass, speedFactor);
    
    Assert.expect(testSubProgram != null);
    Assert.expect(scanPass != null);
    Assert.expect(fileLogger != null);
    _scanPass = scanPass;
    _inspectionEventObservable = InspectionEventObservable.getInstance();    
    _pspEngine = PspEngine.getInstance();
    
    _testSubProgram = testSubProgram;
    _regionsThatMustBeMarkedCompletePriorToSurfaceMap = testSubProgram.getAlignmentRegions().size();
    
    // XCR-3780 Additional Surface Map Info log
    _surfaceMapLogger = fileLogger;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void executeAction() throws XrayTesterException 
  {
    if (_testSubProgram.getTestProgram().getProject().getPanel().getPanelSettings().isPanelBasedAlignment() == false)
    {
      if (_testSubProgram.getBoard().isXedOut())
      {
        return;
      }
    }
    // Start log the timestamp for overall optical acquisition
    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_OPTICAL_ACQUISITION_SEQUENCE_FOR_ALL_SUB_PROGRAMS);
    
    // Pause scan path to make way for surface map
    if (_scanPass.isContainScanPassBreak(ScanPassBreakPointEnum.STAGE_SPEED) == false)
      PanelPositioner.getInstance().pauseScanPath();
    
    long timeInMils = System.currentTimeMillis();
    String opticalImageSetName = Directory.getDirName(timeInMils);
    Project project = _testSubProgram.getTestProgram().getProject();
    
    boolean isPspOfflineReconstructionEnabled = Config.getInstance().getBooleanValue(SoftwareConfigEnum.OFFLINE_RECONSTRUCTION);

    // Prepare optical image directory
    if (isPspOfflineReconstructionEnabled == false)
    {
      String opticalImageSetDirectory = Directory.getOpticalImagesDir(project.getName(), opticalImageSetName);
      if (FileUtilAxi.existsDirectory(opticalImageSetDirectory) == false)
      {
         FileUtilAxi.createDirectory(opticalImageSetDirectory);
      }
    }
    else
    {
      String opticalImageSetDirectory = Directory.getOfflineOpticalImagesDir(project.getName());
      if (FileUtilAxi.existsDirectory(opticalImageSetDirectory) == false)
      {
         DirectoryNotFoundDatastoreException directoryNotFoundDatastoreException = new DirectoryNotFoundDatastoreException(opticalImageSetDirectory);
         throw directoryNotFoundDatastoreException;
      }
    }
    
    List<OpticalPointToPointScan> testSubProgramOpticalPointToPointScans = _testSubProgram.getOpticalPointToPointScans();
    fireRuntimeSurfaceMapStartedEvent(testSubProgramOpticalPointToPointScans);
    
    // Before we start running optical image acquisition, we need to sort the points.
    Collections.sort(testSubProgramOpticalPointToPointScans, 
            new OpticalPointToPointScanComparator(true, OpticalPointToPointScanComparatorEnum.PANEL_COORDINATE_X_IN_NANOMETERS));
    Collections.sort(testSubProgramOpticalPointToPointScans, 
            new OpticalPointToPointScanComparator(true, OpticalPointToPointScanComparatorEnum.PANEL_COORDINATE_Y_IN_NANOMETERS));     
    testSubProgramOpticalPointToPointScans = _surfaceMapping.sortOpticalPointToPointScan(testSubProgramOpticalPointToPointScans);

    // Next, position the panel according to TestSubProgram
    if (_testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
        PanelHandler.getInstance().movePanelToRightSide();
    else if (_testSubProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
        PanelHandler.getInstance().movePanelToLeftSide();
    else
        Assert.expect(false, "Invalid PanelLocationInSystemEnum");    
    
    // Log individual TestSubProgram point-to-point scans
    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.OPTICAL_POINT_TO_POINT_SCAN_SIZE, testSubProgramOpticalPointToPointScans.size());
        
    // Start actual acquisition process
    for (OpticalPointToPointScan currentOpticalPointToPointScan : testSubProgramOpticalPointToPointScans)
    {
      if (isPspOfflineReconstructionEnabled == false)
        runNextOpticalPointToPointScan(currentOpticalPointToPointScan, project.getName(), opticalImageSetName);
      else
        runNextOpticalPointToPointScanOffline(currentOpticalPointToPointScan, project.getName(), opticalImageSetName);

      postSurfaceMapOnOpticalImageEvent();
    }
    
    // Modify Focus Instruction with Z-Height from PSP
    if (_testSubProgram.hasEnabledOpticalCameraRectangles() || _testSubProgram.hasOpticalRegionNameToSubProgramReference())
    {
      if (_testSubProgram.hasEnabledOpticalCameraRectangles())
      {
        // Update TPS model in each region
        _testSubProgram.updateModel();

        // Modify reconstruction region focus profile
        _testSubProgram.updateReconstructionRegionsFocusInstructionWithModelData(_surfaceMapLogger);
      }

      if (_testSubProgram.hasOpticalRegionNameToSubProgramReference())
      {
        // Update reconstruction region focus profile with TPS data from reference TestSubProgram
        _testSubProgram.updateReconstructionRegionsFocusInstructionWithReferenceSubProgramModelData(_surfaceMapLogger);
      }
    }
    
    // Make sure that all our modifications to the program have been
    // performed prior to continuing.    
    ImageAcquisitionEngine.getInstance().getReconstructedImagesProducer().waitForPriorIrpCommandsToComplete();
    
    // Clear TestProgram OpticalPointToPointScan list
    if (testSubProgramOpticalPointToPointScans != null)
    {
      testSubProgramOpticalPointToPointScans.clear();
      testSubProgramOpticalPointToPointScans = null;
    }
    
    // Send event to UI to inform that we have completed running Surface Map
    postRunTimeSurfaceMapCompletedEvent();
    
    // Send alignment result
    ImageAcquisitionEngine.getInstance().applyAlignmentResult(_testSubProgram.getTestProgram().getTestSubProgramById(_testSubProgram.getSubProgramRefferenceIdForAlignment()).getAggregateRuntimeAlignmentTransform(), _testSubProgram.getId());
    
    // Finally, log the timestamp for overall optical acquisition
    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_OPTICAL_ACQUISITION_SEQUENCE_FOR_ALL_SUB_PROGRAMS);    
  }

  /**
   * @author Cheah Lee Herng 
   */
  public int getNumberOfRegionsThatMustBeMarkedCompletePriorToExecutingAction() 
  {    
    return _regionsThatMustBeMarkedCompletePriorToSurfaceMap;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public void clearBreakPoint() 
  {
    // Do nothing
  }
  
  private void fireRuntimeSurfaceMapStartedEvent(List<OpticalPointToPointScan> opticalPointToPointScans)
  {
    Assert.expect(opticalPointToPointScans != null);

    SurfaceMapInspectionEvent runtimeSurfaceMapStartedEvent =
        new SurfaceMapInspectionEvent(InspectionEventEnum.RUNTIME_SURFACE_MAP_STARTED);
    runtimeSurfaceMapStartedEvent.setOpticalPointToPointScans(opticalPointToPointScans);
    _inspectionEventObservable.sendEventInfo(runtimeSurfaceMapStartedEvent);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void postSurfaceMapOnOpticalImageEvent()
  {
    SurfaceMapInspectionEvent surfaceMapOnOpticalImageSurfaceMapEvent = 
            new SurfaceMapInspectionEvent(InspectionEventEnum.SURFACE_MAP_ON_OPTICAL_IMAGE);
    _inspectionEventObservable.sendEventInfo(surfaceMapOnOpticalImageSurfaceMapEvent);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void postRunTimeSurfaceMapCompletedEvent()
  {
    SurfaceMapInspectionEvent runTimeSurfaceMapCompletedEvent = 
            new SurfaceMapInspectionEvent(InspectionEventEnum.RUNTIME_SURFACE_MAP_COMPLETED);
    _inspectionEventObservable.sendEventInfo(runTimeSurfaceMapCompletedEvent);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  protected void runNextOpticalPointToPointScan(OpticalPointToPointScan opticalPointToPointScan, String projectName, String opticalImageSetName) throws XrayTesterException
  {    
    Assert.expect(opticalPointToPointScan != null);
    Assert.expect(projectName != null);
    Assert.expect(opticalImageSetName != null);

    // Perform height map image operation
    _pspEngine.performHeightMapOperation(opticalPointToPointScan, projectName, opticalImageSetName);
  }

  /**
   * @author Cheah Lee Herng 
   */
  protected void runNextOpticalPointToPointScanOffline(OpticalPointToPointScan opticalPointToPointScan, String projectName, String opticalImageSetName) throws XrayTesterException
  {
    Assert.expect(opticalPointToPointScan != null);
    Assert.expect(projectName != null);
    Assert.expect(opticalImageSetName != null);      

    // Perform offline height map image operation
    _pspEngine.performOfflineHeightMapOperation(opticalPointToPointScan, projectName, opticalImageSetName);
  }

  /**
   * @author Yong Sheng Chuan
   */
  public ScanPassBreakPointEnum getBreakPointEnum()
  {
    return ScanPassBreakPointEnum.SURFACE_MAP;
  }
}
