package com.axi.v810.business.testExec;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Peter Esbensen
 */
class Test_Alignment extends UnitTest
{
  /**
   * @author Peter Esbensen
   */
  public Test_Alignment()
  {
    // do nothing
  }

  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Alignment());
  }

  /**
   * @author Peter Esbensen
   */
  private void deleteSerializedProject(String panelName)
  {
    Assert.expect(panelName != null);
    try
    {
      String serializedProjectPath = FileName.getProjectSerializedFullPath(panelName);
      if(FileUtil.exists(serializedProjectPath))
        FileUtil.delete(serializedProjectPath);
    }
    catch (CouldNotDeleteFileException ex)
    {
      ex.printStackTrace();
      Expect.expect(false);
    }
  }

  /**
   * @author Peter Esbensen
   */
  private Project loadProject(String panelName)
  {
    Assert.expect(panelName != null);
    Project project = null;
    try
    {
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();
      project = Project.importProjectFromNdfs(panelName, warnings);
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    Assert.expect(project != null);
    return project;
  }

  /**
   * @author Peter Esbensen
   */
  private ReconstructedImages getDummyReconstructedImages(ReconstructionRegion alignmentRegion)
  {
    Assert.expect(alignmentRegion != null);

    IntRectangle panelRectangle = alignmentRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    double nanometersPerPixel = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();

    // System Fiducal (Width, Length) = Panel (Length, Width)
    int width = MathUtil.convertNanoMetersToPixelsUsingCeil(panelRectangle.getWidth(), nanometersPerPixel);
    int height = MathUtil.convertNanoMetersToPixelsUsingCeil(panelRectangle.getHeight(), nanometersPerPixel);
    Image imageBuffer = new Image(width, height);

    // Add some fake features to the image.
    Paint.fillImage(imageBuffer, 255f);
    for (JointInspectionData jointInspectionData : alignmentRegion.getJointInspectionDataList())
    {
      RegionOfInterest jointRoi = null;
      if (ImageAnalysis.isSpecialTwoPinComponent(jointInspectionData.getJointTypeEnum()))
      {
        ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
        jointRoi = componentInspectionData.getOrthogonalComponentRegionOfInterest();
      }
      else
      {
        jointRoi = jointInspectionData.getOrthogonalRegionOfInterestInPixels();
      }

      Paint.fillRegionOfInterest(imageBuffer, jointRoi, 0f);
    }
    for (FiducialInspectionData fiducialInspectionData : alignmentRegion.getFiducialInspectionDataList())
    {
      RegionOfInterest fiducialRoi = fiducialInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels();
      Paint.fillRegionOfInterest(imageBuffer, fiducialRoi, 0f);
    }

    ReconstructedImages images = new ReconstructedImages(alignmentRegion);
    List<Float> arrayZHeight = new ArrayList<Float>();//add by BH
    List<Float> arraySharpness = new ArrayList<Float>();//add by BH
    images.addImage(imageBuffer, SliceNameEnum.PAD, 0, arrayZHeight, arraySharpness);//add by BH
    imageBuffer.decrementReferenceCount();
    return images;
  }

  /**
   * @author Peter Esbensen
   */
  private void generateImageAndSendToAlignment(TestProgram testProgram,
                                               ReconstructionRegion alignmentRegion) throws XrayTesterException
  {
    Assert.expect(alignmentRegion != null);
    Assert.expect(testProgram != null);

    // Make sure we have a good match quality and gray levels for each image.
    Collection<TestSubProgram> testSubPrograms = testProgram.getAllTestSubPrograms();
    for (TestSubProgram testSubProgram : testSubPrograms)
    {
      Collection<AlignmentGroup> alignmentGroups = testSubProgram.getAlignmentGroups();
      for (AlignmentGroup alignmentGroup : alignmentGroups)
      {
        alignmentGroup.setMatchQuality(0d);
        alignmentGroup.setForegroundGrayLevel(0d);
        alignmentGroup.setBackgroundGrayLevel(255d);
      }
    }

    // Try running alignment on the image.
    ReconstructedImages reconstructedImages = getDummyReconstructedImages(alignmentRegion);
    //Khaw Chek Hau - XCR2285: 2D Alignment on v810
    if (testProgram.getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod())
      Alignment.getInstance().alignOn2DImage(reconstructedImages, false);
    else
      Alignment.getInstance().alignOnImage(reconstructedImages, false);
      
    reconstructedImages.decrementReferenceCount();
  }

  /**
   * @author Peter Esbensen
   */
  private void simulateAlignment(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    Collection<TestSubProgram> testSubPrograms = testProgram.getAllTestSubPrograms();

    for (TestSubProgram testSubProgram : testSubPrograms)
    {
      Collection<ReconstructionRegion> alignmentRegions = testSubProgram.getAlignmentRegions();
      for (ReconstructionRegion alignmentRegion : alignmentRegions)
      {
        generateImageAndSendToAlignment(testProgram, alignmentRegion);
      }
    }
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  private void addAlignmentGroups(Project project)
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();
    PanelSettings panelSettings = panel.getPanelSettings();

    // Add the alignment pads to the alignment groups.
    List<AlignmentGroup> alignmentGroups = panelSettings.getAlignmentGroupsForShortPanel();

    // Group 1 (Board: 1  Component: R10  Pads: 1, 2)
    List<Pad> alignmentGroup1Pads = new LinkedList<Pad>();
    alignmentGroup1Pads.addAll(panel.getComponent("1", "r10").getPads());
    AlignmentGroup alignmentGroup1 = alignmentGroups.get(0);
    for (Pad alignmentPad : alignmentGroup1Pads)
    {
      alignmentGroup1.addPad(alignmentPad);
    }

    // Group 2 (Board: 1  Component: R4  Pads: 1, 2   Component: R8  Pads: 1, 2)
    List<Pad> alignmentGroup2Pads = new LinkedList<Pad>();
    alignmentGroup2Pads.addAll(panel.getComponent("1", "r4").getPads());
    alignmentGroup2Pads.addAll(panel.getComponent("1", "r8").getPads());
    AlignmentGroup alignmentGroup2 = alignmentGroups.get(1);
    for (Pad alignmentPad : alignmentGroup2Pads)
    {
      alignmentGroup2.addPad(alignmentPad);
    }

    // Group 3 (Board: 1  Component R3  Pads: 1, 2   Component: R7  Pads: 1, 2)
    List<Pad> alignmentGroup3Pads = new LinkedList<Pad>();
    alignmentGroup3Pads.addAll(panel.getComponent("1", "r3").getPads());
    alignmentGroup3Pads.addAll(panel.getComponent("1", "r7").getPads());
    AlignmentGroup alignmentGroup3 = alignmentGroups.get(2);
    for (Pad alignmentPad : alignmentGroup3Pads)
    {
      alignmentGroup3.addPad(alignmentPad);
    }
  }

  /**
   * @author Matt Wharton
   */
  private void testCorrectPlacementOfAlignmentFeatures(TestProgram testProgram)
  {
    Assert.expect(testProgram != null);

    for (TestSubProgram testSubProgram : testProgram.getAllTestSubPrograms())
    {
      for (ReconstructionRegion alignmentRegion : testSubProgram.getAlignmentRegions())
      {
        for (JointInspectionData jointInspectionData : alignmentRegion.getJointInspectionDataList())
        {
          Pad pad = jointInspectionData.getPad();
          Component component = pad.getComponent();

          // Print out the locations of the individual alignment features.
          if (ImageAnalysis.isSpecialTwoPinComponent(jointInspectionData.getJointTypeEnum()))
          {
            ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
            String padOneFullyQualifiedPadName = componentInspectionData.getPadOneJointInspectionData().getFullyQualifiedPadName();

            if (padOneFullyQualifiedPadName.equals(jointInspectionData.getFullyQualifiedPadName()) == false)
            {
              continue;
            }

            RegionOfInterest alignmentFeatureRoi = componentInspectionData.getOrthogonalComponentRegionOfInterest();

            System.out.println("Component: " + component.getReferenceDesignator());
            System.out.println("Min X: " + alignmentFeatureRoi.getMinX());
            System.out.println("Min Y: " + alignmentFeatureRoi.getMinY());
            System.out.println("Width: " + alignmentFeatureRoi.getWidth());
            System.out.println("Height: " + alignmentFeatureRoi.getHeight());
            System.out.println();
          }
          else
          {
            RegionOfInterest alignmentFeatureRoi = jointInspectionData.getOrthogonalRegionOfInterestInPixels();

            System.out.println("Component: " + component.getReferenceDesignator() + " Pad: " + pad.getName());
            System.out.println("Min X: " + alignmentFeatureRoi.getMinX());
            System.out.println("Min Y: " + alignmentFeatureRoi.getMinY());
            System.out.println("Width: " + alignmentFeatureRoi.getWidth());
            System.out.println("Height: " + alignmentFeatureRoi.getHeight());
            System.out.println();
          }
        }
      }
    }
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String panelName = "nepcon_wide_ew";

    deleteSerializedProject(panelName);

    Project project = loadProject(panelName);

    // Rotate the panel 90 degrees.
    PanelSettings panelSettings = project.getPanel().getPanelSettings();
    panelSettings.setDegreesRotationRelativeToCad(90);

    addAlignmentGroups(project);

    TestProgram testProgram = project.getTestProgram();

    try
    {
      testCorrectPlacementOfAlignmentFeatures(testProgram);

      simulateAlignment(testProgram);
    }
    catch (XrayTesterException hwEx)
    {
      hwEx.printStackTrace();
    }
  }
}
