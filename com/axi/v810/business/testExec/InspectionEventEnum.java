package com.axi.v810.business.testExec;

import com.axi.util.*;

/**
 * @author Peter Esbensen
 */
public class InspectionEventEnum extends com.axi.util.Enum
{
  private static int _index = 0;

  public static final InspectionEventEnum PANEL_LOADING_INTO_MACHINE = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum PANEL_LOADED_INTO_MACHINE = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum PANEL_UNLOADING_FROM_MACHINE = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum PANEL_UNLOADED_FROM_MACHINE = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum IMAGE_ACQUISITION_COMPLETE = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum JOINT_OR_COMPONENT_FAILURE = new InspectionEventEnum(_index++);

  public static final InspectionEventEnum INSPECTION_STARTED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum INSPECTION_COMPLETED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum INSPECTION_PAUSED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum INSPECTION_RESUMED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum BEGIN_INSPECTING_JOINT = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum DONE_INSPECTING_JOINT = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum BEGIN_INSPECTING_IMAGE = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum DONE_INSPECTING_IMAGE = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum POSTING_DIAGNOSTIC = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum MANUAL_ALIGNMENT_STARTED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum MANUAL_ALIGNMENT_COMPLETED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum WAITING_FOR_USER_TO_ALIGN_IMAGE = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum MANUAL_ALIGNMENT_VERIFICATION_STARTED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum MANUAL_ALIGNMENT_VERIFICATION_COMPLETED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum ALIGNED_ON_IMAGE = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum RUNTIME_ALIGNMENT_STARTED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum RUNTIME_ALIGNMENT_COMPLETED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum RUNTIME_SURFACE_MAP_STARTED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum RUNTIME_SURFACE_MAP_COMPLETED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum RUNTIME_ALIGNMENT_SURFACE_MAP_STARTED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum RUNTIME_ALIGNMENT_SURFACE_MAP_COMPLETED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum INITIAL_TUNING_STARTED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum INITIAL_TUNING_COMPLETED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum SUBTYPE_LEARNING_STARTED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum SUBTYPE_LEARNING_COMPLETED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum MESSAGE = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum SURFACE_MAP_ON_OPTICAL_IMAGE = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum ALIGNMENT_SURFACE_MAP_ON_OPTICAL_IMAGE = new InspectionEventEnum(_index++);

  public static final InspectionEventEnum SUBTYPE_DELETING_LEARNED_DATA_STARTED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum SUBTYPE_DELETING_LEARNED_DATA_COMPLETED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum DELETING_LEARNED_DATA_STARTED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum DELETING_LEARNED_DATA_COMPLETED = new InspectionEventEnum(_index++);

  public static final InspectionEventEnum INTERACTIVE_LEARNING_STARTED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum INTERACTIVE_LEARNING_EXPOSED_PAD_USER_INFO = new InspectionEventEnum(_index++);
//  public static final InspectionEventEnum INTERACTIVE_LEARNING_CAPACITOR_USER_INFO = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum INTERACTIVE_LEARNING_COMPLETED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum BEGIN_COLLECT_PRECISION_IMAGE_SET = new InspectionEventEnum(_index++);
  
  public static final InspectionEventEnum OPTICAL_IMAGE_ACQUISITION_COMPLETE = new InspectionEventEnum(_index++);
  
  //Khaw Chek Hau - XCR2285: 2D Alignment on v810
  public static final InspectionEventEnum WAITING_FOR_USER_TO_CROP_IMAGE = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum SAVE_CROP_IMAGE = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum RESET_ZOOM_FACTOR = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum ALIGNED_ON_2D_IMAGE = new InspectionEventEnum(_index++);    
  public static final InspectionEventEnum MANUAL_2D_ALIGNMENT_STARTED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum MANUAL_2D_ALIGNMENT_COMPLETED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum DISABLE_NEXT_BUTTON_BEFORE_IMAGE_CROPPED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum ENABLE_NEXT_BUTTON_AFTER_IMAGE_CROPPED = new InspectionEventEnum(_index++);
  public static final InspectionEventEnum DETERMINE_CROPPED_ALIGNMENT_REGION = new InspectionEventEnum(_index++);
  
  //Khaw Chek Hau - XCR3774: Display load & unload time during production
  public static final InspectionEventEnum INSPECTION_PRODUCTION_RESULT_STATUS = new InspectionEventEnum(_index++);

  /**
   * @author Peter Esbensen
   */
  private InspectionEventEnum(int id)
  {
    super(id);
  }
}
