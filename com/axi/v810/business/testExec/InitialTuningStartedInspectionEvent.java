package com.axi.v810.business.testExec;

import com.axi.util.*;

/**
 * This InspectionEvent is fired by the StatisticalTuningEngine whenever learning is starting.
 *
 * @author Sunit Bhalla
 */
public class InitialTuningStartedInspectionEvent extends InspectionEvent
{
  private int _numberOfSubtypesToLearn = -1;

  /**
   * @author Sunit Bhalla
   */
  public InitialTuningStartedInspectionEvent(int numberOfSubtypesToLearn)
  {
    super(InspectionEventEnum.INITIAL_TUNING_STARTED);

    Assert.expect(numberOfSubtypesToLearn >= 0);

    _numberOfSubtypesToLearn = numberOfSubtypesToLearn;
  }

  /**
   * @author Sunit Bhalla
   */
  public int getNumberOfSubtypesToLearn()
  {
    Assert.expect(_numberOfSubtypesToLearn != -1);

    return _numberOfSubtypesToLearn;
  }

}
