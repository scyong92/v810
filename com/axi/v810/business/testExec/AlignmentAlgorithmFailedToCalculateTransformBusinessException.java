package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * This exception is thrown if the alignment process is unable to calculate an AffineTransform.
 * This should be a rare occurence.
 *
 * @author Matt Wharton
 */
public class AlignmentAlgorithmFailedToCalculateTransformBusinessException extends AlignmentBusinessException
{
  /**
   * @author Matt Wharton
   */
  public AlignmentAlgorithmFailedToCalculateTransformBusinessException(TestSubProgram testSubProgram,
                                                                       AlignmentGroup alignmentGroup1,
                                                                       AlignmentGroup alignmentGroup2,
                                                                       AlignmentGroup alignmentGroup3)
  {
    super(testSubProgram, 
          new LocalizedString("BS_ALIGNMENT_ALGORITHM_FAILED_TO_CALCULATE_TRANSFORM_KEY",
                              new Object[]{testSubProgram.getMagnificationType().toString(),
                                          alignmentGroup1.getName(), 
                                          alignmentGroup2.getName(), 
                                          alignmentGroup3.getName()}));
  }

}
