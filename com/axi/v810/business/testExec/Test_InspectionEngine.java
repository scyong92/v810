package com.axi.v810.business.testExec;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Peter Esbensen
 */
class Test_InspectionEngine extends UnitTest implements Observer
{
  private InspectionEngine _inspectionEngine = InspectionEngine.getInstance();
  private List<AlgorithmDiagnosticMessage> _diagnosticMessages = new ArrayList<AlgorithmDiagnosticMessage>();
  private WorkerThread _eventHandlingWorkerThread = new WorkerThread("Test_InspectionEngine Event Handler Worker");

  public static void main(String[] args)
  {
    UnitTest.execute (new Test_InspectionEngine());
  }

  /**
   * Get diagnostic information being passed up from the ImageAnalysisSubsystem.  Store the diagnostic information
   * in an array of strings so that we can verify the output later.
   *
   * @author Peter Esbensen
   */
  public void update(Observable observable, final Object arg)
  {
    Assert.expect(observable != null);
    Assert.expect(arg != null);

    _eventHandlingWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        if (arg instanceof AlgorithmDiagnosticInspectionEvent)
        {
          AlgorithmDiagnosticInspectionEvent algorithmDiagnosticEvent = (AlgorithmDiagnosticInspectionEvent)arg;
          AlgorithmDiagnosticMessage algDiagMessage = algorithmDiagnosticEvent.getAlgorithmDiagnosticMessage();
          _diagnosticMessages.add(algDiagMessage);
        }
        else if (arg instanceof InspectionEvent)
        {
          // If we got a "paused" event, tell the inspection engine to run to next diagnostic.
          InspectionEvent inspectionEvent = (InspectionEvent)arg;
          InspectionEventEnum inspectionEventEnum = inspectionEvent.getInspectionEventEnum();
          if (inspectionEventEnum.equals(InspectionEventEnum.INSPECTION_PAUSED))
          {
            synchronized (inspectionEvent)
            {
              _inspectionEngine.runToNextDiagnosticEvent();
            }
          }
        }
      }
    });
  }

  /**
   * @author Peter Esbensen
   */
  private void deleteSerializedProject(String projectName)
  {
    Assert.expect(projectName != null);
    try
    {
      String serializedProjectPath = FileName.getProjectSerializedFullPath(projectName);
      if (FileUtil.exists(serializedProjectPath))
        FileUtil.delete(serializedProjectPath);
    }
    catch (CouldNotDeleteFileException ex)
    {
      ex.printStackTrace();
      Expect.expect(false);
    }
  }

  /**
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    InspectionEngine inspectionEngine = InspectionEngine.getInstance();

    String projectName = "families_all_rlv";

    deleteSerializedProject(projectName);

    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    Project project = null;
    try
    {
      project = Project.importProjectFromNdfs(projectName, warnings);
    }
    catch (XrayTesterException xte)
    {
      Expect.expect(false);
      xte.printStackTrace();
    }
    Expect.expect(project != null);

    try
    {
      TestProgram testProgram = project.getTestProgram();

      InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
      inspectionEventObservable.addObserver(this);

      PanelInspectionSettings inspectionSettings = PanelInspectionSettings.getInstance();
      inspectionSettings.clearAllInspectionSettings();
      inspectionSettings.setInspectedSubtypes(project.getPanel());
      inspectionSettings.setAllJointTypesAllAlgorithmsDiagnosticsEnabled(true);

      inspectionEngine.setPanelInspectionSettings(inspectionSettings);

      //inspectionEngine.inspectPanel(panel, new ArrayList(), inspectionSettings);
      //printAndClearDiagnosticMessages();

      TestExecutionTimer testExecTimer = new TestExecutionTimer();
      Board firstBoard = (Board)project.getPanel().getBoards().get(0);
      Pad u2_pin2 = getPad(firstBoard, "U14", "2");
      testProgram.clearFilters();
      testProgram.addFilter(true);
      testProgram.addFilter(u2_pin2);
      inspectionEngine.inspectForTestDevelopment(testProgram, testExecTimer, "IAS RULES!!");

      Pad c18_pin1 = getPad(firstBoard, "C12", "1");
      Subtype subtype = c18_pin1.getPadTypeSettings().getSubtype();
      testProgram.clearFilters();
      testProgram.addFilter(true);
      testProgram.addFilter(subtype);
      inspectionEngine.inspectForTestDevelopment(testProgram, testExecTimer, "HO HO HO!");

      Pad u4_pin1 = getPad(firstBoard, "U3", "1");
      Component component = (Component)u4_pin1.getComponent();
      testProgram.clearFilters();
      testProgram.addFilter(true);
      testProgram.addFilter(component);
      inspectionEngine.inspectForTestDevelopment(testProgram, testExecTimer, "IAS RULES!!");
    }
    catch (XrayTesterException hEx)
    {
      hEx.printStackTrace();
    }
  }

  /**
   * @author Peter Esbensen
   */
  private Pad getPad(Board board, String referenceDesignator, String padName)
  {
    Assert.expect(referenceDesignator != null);
    Assert.expect(padName != null);

    List<Component> components = board.getComponents();
    Iterator<Component> compIt = components.iterator();
    Component component = null;
    while (compIt.hasNext() && (component == null))
    {
      Component candidateComponent = compIt.next();
      if (candidateComponent.getComponentType().getReferenceDesignator().equalsIgnoreCase(referenceDesignator))
        component = candidateComponent;
    }
    Pad pad = component.getPad(padName);
    Assert.expect(pad != null);
    return pad;
  }
}


