package com.axi.v810.business.testExec;

import com.axi.util.*;

/**
 * @author Kok Chun, Tan
 */
public class VelocityMappingParameters
{
  private int _firstVelocity = 0;
  private int _secondVelocity = 0;
  private double _slope = 0;
  private double _offset = 0;
  private double _r2 = 0;
  
  /**
   * @author Kok Chun, Tan
   */
  public VelocityMappingParameters(int firstVelocity, int secondVelocity, double slope, double offset, double r2)
  {
    Assert.expect(firstVelocity >= 0);
    Assert.expect(secondVelocity >= 0);
    
    _firstVelocity = firstVelocity;
    _secondVelocity = secondVelocity;
    _slope = slope;
    _offset = offset;
    _r2 = r2;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public double getSlope()
  {
    return _slope;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public double getOffset()
  {
    return _offset;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public double getR2()
  {
    return _r2;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public int getFirstVelocity()
  {
    return _firstVelocity;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public int getSecondVelocity()
  {
    return _secondVelocity;
  }
}
