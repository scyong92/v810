package com.axi.v810.business.testExec;

import com.axi.util.*;
import java.util.*;

/**
 * @author sham 
 */
public class AlignmentExcludedAreaEnum extends com.axi.util.Enum
{
  private static int _index = 0;
  private static Map<String, AlignmentExcludedAreaEnum> _nameToAlignmentExcludedAreaEnumMap = new HashMap<String, AlignmentExcludedAreaEnum>();

  public static final AlignmentExcludedAreaEnum LEFT = new AlignmentExcludedAreaEnum(++_index, "left");
  public static final AlignmentExcludedAreaEnum RIGHT = new AlignmentExcludedAreaEnum(++_index, "right");
  public static final AlignmentExcludedAreaEnum TOP = new AlignmentExcludedAreaEnum(++_index, "top");
  public static final AlignmentExcludedAreaEnum BOTTOM = new AlignmentExcludedAreaEnum(++_index, "bottom");

  private String _name;

  /**
   * @author sham
   */
  private AlignmentExcludedAreaEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);
    _name = name;
    AlignmentExcludedAreaEnum algEnum = _nameToAlignmentExcludedAreaEnumMap.put(name, this);
    Assert.expect(algEnum == null);
  }

  /**
   * Get all the Enums.
   *
   * @author sham
   */
  static Collection<AlignmentExcludedAreaEnum> getAllEnums()
  {
    return _nameToAlignmentExcludedAreaEnumMap.values();
  }

  /**
   * @author sham
   */
  public String getName()
  {
    return _name;
  }

  /**
   * @author sham
   */
  public String toString()
  {
    return _name;
  }


  /**
   * @author sham
   */
  public static AlignmentExcludedAreaEnum getAlignmentExcludedAreaEnum(String alignmentExcludedAreaName)
  {
    Assert.expect(alignmentExcludedAreaName != null);
    AlignmentExcludedAreaEnum alignmentExcludedAreaEnum = _nameToAlignmentExcludedAreaEnumMap.get(alignmentExcludedAreaName);

    Assert.expect(alignmentExcludedAreaEnum != null);
    return alignmentExcludedAreaEnum;
  }
}
