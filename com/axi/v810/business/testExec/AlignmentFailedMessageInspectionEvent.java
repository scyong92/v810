package com.axi.v810.business.testExec;

import com.axi.util.*;

/**
 * @author Matt Wharton
 */
public class AlignmentFailedMessageInspectionEvent extends MessageInspectionEvent
{
  private AlignmentBusinessException _alignmentException;

  /**
   * @author Matt Wharton
   */
  public AlignmentFailedMessageInspectionEvent(AlignmentBusinessException alignmentException)
  {
    super(alignmentException.getLocalizedString());
    _alignmentException = alignmentException;
  }

  /**
   * @author Matt Wharton
   */
  public AlignmentBusinessException getAlignmentException()
  {
    Assert.expect(_alignmentException != null);

    return _alignmentException;
  }
}
