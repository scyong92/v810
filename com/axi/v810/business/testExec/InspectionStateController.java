package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.testProgram.*;

/**
 * Controls the state of the current inspection.  This handles things like pausing, running to next joint, etc.
 * This also takes care of posting applicable events back to the UI.
 *
 * @author Matt Wharton
 */
public class InspectionStateController
{
  private static InspectionStateController _instance = null;

  private BooleanLock _isPaused = new BooleanLock(false);

  private boolean _pauseRequested = false;
  private boolean _pauseOnNextDiagnostic = false;
  private boolean _pauseOnNextJoint = false;
  private boolean _pauseOnNextSlice = false;
  private boolean _pauseOnNextAlgorithm = false;
  private boolean _pauseOnNextInspectionRegion = false;

  private JointInspectionData _lastJointVisited;
  private SliceNameEnum _lastSliceVisited;
  private AlgorithmEnum _lastAlgorithmVisited;
  private ReconstructionRegion _lastInspectionRegionVisited;

  /**
   * @author Matt Wharton
   */
  private InspectionStateController()
  {
  }

  /**
   * @author Matt Wharton
   */
  public static synchronized InspectionStateController getInstance()
  {
    if (_instance == null)
    {
      _instance = new InspectionStateController();
    }

    return _instance;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void reset()
  {
    _lastJointVisited = null;
    _lastInspectionRegionVisited = null;
  }

  /**
   * Requests to pause the inspection.
   *
   * @author Matt Wharton
   */
  public void requestPauseInspection()
  {
    // Only set a pause request if we're not already paused.
    if (_isPaused.isFalse())
    {
      _pauseRequested = true;
    }
  }

  /**
   * Pauses if a pause has been requested.  Blocks the current thread until the pause is released.
   *
   * NOTE: This should ONLY be invoked from the ImageAnalysis diagnostic thread otherwise deadlocks will
   * occur!
   *
   * @author Matt Wharton
   */
  public synchronized void pauseIfRequired(AlgorithmDiagnosticInspectionEvent algorithmDiagnosticEvent)
  {
    Assert.expect(algorithmDiagnosticEvent != null);
    Assert.expect(_isPaused != null);
    Assert.expect(algorithmDiagnosticEvent.hasAlgorithmDiagnosticMessage());

    // Get the diagnostic message.
    AlgorithmDiagnosticMessage diagnosticMessage = algorithmDiagnosticEvent.getAlgorithmDiagnosticMessage();

    // Do we have a "pending" pause request?
    if (_pauseRequested ||
        _pauseOnNextDiagnostic ||
        _pauseOnNextJoint ||
        _pauseOnNextSlice ||
        _pauseOnNextAlgorithm ||
        _pauseOnNextInspectionRegion)
    {
      // Get the affected ReconstructionRegion.
      ReconstructionRegion inspectionRegion = diagnosticMessage.getInspectionRegion();

      // Determine if we're at the appropriate spot for the requested pause.
      boolean needToPause = false;
      if (_pauseRequested)
      {
        // A pause was requested ASAP, so we need to pause now.
        needToPause = true;
      }
      else if (_pauseOnNextDiagnostic)
      {
        // A pause was requested on the next diagnostic and here we are, so let's pause.
        // let's only pause for graphical diagnostics (or if the message has specifically requested a pause)
        needToPause = diagnosticMessage.hasAnyGraphicalDiagnosticInfo() || diagnosticMessage.getPauseRequested();
      }
      else if (_pauseOnNextJoint)
      {
        // We'll pause if the current joint is different from the last joint visited.
        if (diagnosticMessage.hasJointInspectionData())
        {
          JointInspectionData joint = diagnosticMessage.getJointInspectionData();
          if (joint.equals(_lastJointVisited) == false)
          {
            needToPause = true;
          }
        }
      }
      else if (_pauseOnNextSlice)
      {
        // We'll pause if the current slice is different from the last slice visited.
        if (diagnosticMessage.hasSliceNameEnum())
        {
          SliceNameEnum slice = diagnosticMessage.getSliceNameEnum();
          if (slice.equals(_lastSliceVisited) == false)
          {
            needToPause = true;
          }
        }

        // We'll pause if the current algorithm is different from the last algorithm visited.
        if (diagnosticMessage.hasAlgorithm())
        {
          AlgorithmEnum algorithmEnum = diagnosticMessage.getAlgorithm().getAlgorithmEnum();
          if (algorithmEnum.equals(_lastAlgorithmVisited) == false)
          {
            needToPause = true;
          }
        }

        // We'll pause if the current inspection region is different from the last one visited.
        if (inspectionRegion.equals(_lastInspectionRegionVisited) == false)
        {
          needToPause = true;
        }
      }
      else if (_pauseOnNextAlgorithm)
      {
        // We'll pause if the current algorithm is different than the last algorithm visited
        if (diagnosticMessage.hasAlgorithm())
        {
          AlgorithmEnum algorithmEnum = diagnosticMessage.getAlgorithm().getAlgorithmEnum();
          if (algorithmEnum.equals(_lastAlgorithmVisited) == false)
          {
            needToPause = true;
          }
        }

        // We'll pause if the current inspection region is different from the last one visited.
        if (inspectionRegion.equals(_lastInspectionRegionVisited) == false)
        {
          needToPause = true;
        }
      }
      else if (_pauseOnNextInspectionRegion)
      {
        // We'll pause if the current region is different from the last region visited.
        if (inspectionRegion.equals(_lastInspectionRegionVisited) == false)
        {
          needToPause = true;
        }
      }

      // Pause if we need to.
      if (needToPause)
      {
        // Clear all the "pending" pause requests.
        clearPauseRequests();

        // Fire an event saying that we're paused.
        InspectionEvent pausedInspectionEvent = new InspectionEvent(InspectionEventEnum.INSPECTION_PAUSED);
        synchronized (pausedInspectionEvent)
        {
          InspectionEventObservable.getInstance().sendEventInfo(pausedInspectionEvent);
          _isPaused.setValue(true);
        }
        try
        {
          _isPaused.waitUntilFalse();
        }
        catch (InterruptedException iex)
        {
          // Do nothing ...
        }
      }
    }

    // Update the "last visited" values.
    if (diagnosticMessage.hasJointInspectionData())
    {
      _lastJointVisited = diagnosticMessage.getJointInspectionData();
    }
    if (diagnosticMessage.hasSliceNameEnum())
    {
      _lastSliceVisited = diagnosticMessage.getSliceNameEnum();
    }
    if (diagnosticMessage.hasAlgorithm())
    {
      _lastAlgorithmVisited = diagnosticMessage.getAlgorithm().getAlgorithmEnum();
    }
    _lastInspectionRegionVisited = diagnosticMessage.getInspectionRegion();
  }

  /**
   * Resumes the current inspection.
   *
   * @author Matt Wharton
   */
  public void resumeInspection()
  {
    Assert.expect(_isPaused != null);

    // If the inspection isn't already paused, do nothing.
    if (_isPaused.isTrue())
    {
      _isPaused.setValue(false);

      // Send an event indicating that the inspection has been resumed.
      InspectionEvent resumedInspectionEvent = new InspectionEvent(InspectionEventEnum.INSPECTION_RESUMED);
      InspectionEventObservable.getInstance().sendEventInfo(resumedInspectionEvent);
    }
  }

  /**
   * Requests that we pause on the next diagnostic.
   *
   * @author Matt Wharton
   */
  public void requestPauseOnNextDiagnostic()
  {
    _pauseOnNextDiagnostic = true;
  }

  /**
   * Requests that we pause on the next joint.
   *
   * @author Matt Wharton
   */
  public void requestPauseOnNextJoint()
  {
    _pauseOnNextJoint = true;
  }

  /**
   * Requests that we pause on the next slice.
   *
   * @author Matt Wharton
   */
  public void requestPauseOnNextSlice()
  {
    _pauseOnNextSlice = true;
  }

  /**
   * Requests that we pause on the next algortihm.
   *
   * @author Matt Wharton
   */
  public void requestPauseOnNextAlgorithm()
  {
    _pauseOnNextAlgorithm = true;
  }

  /**
   * Requests that we pause on the next inspection region.
   *
   * @author Matt Wharton
   */
  public void requestPauseOnNextInspectionRegion()
  {
    _pauseOnNextInspectionRegion = true;
  }

  /**
   * Clears all pending pause requests.  Cancels any currently active pause.
   *
   * @author Matt Wharton
   */
  public void clearPauseRequests()
  {
    _isPaused.setValue(false);

    _pauseRequested = false;
    _pauseOnNextDiagnostic = false;
    _pauseOnNextJoint = false;
    _pauseOnNextSlice = false;
    _pauseOnNextAlgorithm = false;
    _pauseOnNextInspectionRegion = false;
  }
}
