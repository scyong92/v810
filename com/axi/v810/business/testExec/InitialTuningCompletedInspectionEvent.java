package com.axi.v810.business.testExec;

/**
 * This InspectionEvent is fired by the StatisticalTuningEngine whenever learning is completed.
 *
 * @author Sunit Bhalla
 */
public class InitialTuningCompletedInspectionEvent extends InspectionEvent
{

  /**
   * @author Sunit Bhalla
   */
  public InitialTuningCompletedInspectionEvent()
  {
    super(InspectionEventEnum.INITIAL_TUNING_COMPLETED);
  }

}
