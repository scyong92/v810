package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.testProgram.*;

/**
 * All alignment exceptions should extend from this class.
 *
 * @author Bill Darbie
 */
public abstract class AlignmentBusinessException extends BusinessException
{
  private TestSubProgram _testSubProgram;
  private String _failAlignmentImagePath = null;
  /**
   * @author Bill Darbie
   */
  public AlignmentBusinessException(TestSubProgram testSubProgram, String failAlignImagePath, LocalizedString localizedString)
  {
    super(localizedString);
    Assert.expect(testSubProgram != null);
    Assert.expect(localizedString != null);
    Assert.expect(failAlignImagePath != null);

    _failAlignmentImagePath = failAlignImagePath;
    _testSubProgram = testSubProgram;
  }

  public AlignmentBusinessException(TestSubProgram testSubProgram, LocalizedString localizedString)
  {
    super(localizedString);
    Assert.expect(testSubProgram != null);
    Assert.expect(localizedString != null);

    _testSubProgram = testSubProgram;
  }

  /**
   * @author Matt Wharton
   */
  public TestSubProgram getTestSubProgram()
  {
    Assert.expect(_testSubProgram != null);

    return _testSubProgram;
  }

  /**
   * @return
   */
  public String getFailAlignmentImagePath()
  {
    return _failAlignmentImagePath;
  }
}
