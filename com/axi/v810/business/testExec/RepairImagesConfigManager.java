package com.axi.v810.business.testExec;

import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * This class handles the config interface for setting/unsetting the number of repair images to be saved in production
 *
 * @author Laura Cormos
 */
public class RepairImagesConfigManager
{
  private static RepairImagesConfigManager _instance;

  private Config _config;

  /**
   * @author Laura Cormos
   */
  public static synchronized RepairImagesConfigManager getInstance()
  {
    if (_instance == null)
      _instance = new RepairImagesConfigManager();

    return _instance;
  }

  /**
   * @author Laura Cormos
   */
  private RepairImagesConfigManager()
  {
    _config = Config.getInstance();
  }

  /**
   * @author Laura Cormos
   */
  public boolean isLimitRepairImagesEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.LIMIT_NUM_REPAIR_IMAGES_TO_SAVE);
  }

  /**
   * @author Laura Cormos
   */
  public void setLimitRepairImagesEnabled(boolean enabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.LIMIT_NUM_REPAIR_IMAGES_TO_SAVE, enabled);
  }

  /**
   * @author Laura Cormos
   */
  public void setNumRepairImagesToSave(int numRepairImagesToSave) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.NUM_REPAIR_IMAGES_TO_SAVE, numRepairImagesToSave);
  }

  /**
   * @author Laura Cormos
   */
  public int getNumRepairImagesToSave()
  {
    return _config.getIntValue(SoftwareConfigEnum.NUM_REPAIR_IMAGES_TO_SAVE);
  }
  
  /**
   * @author Chnee Khang wah, 2011-12-13, 2.5D HIP Development
   */
  public boolean isGenerateMultiAngleImagesEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.GENERATE_MULTI_ANGLE_IMAGES);
  }

  /**
   * @author Chnee Khang wah, 2011-12-13, 2.5D HIP Development
   */
  public void setGenerateMultiAngleImagesEnabled(boolean enabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.GENERATE_MULTI_ANGLE_IMAGES, enabled);
  }

  /**
   * @author Laura Cormos
   */
  public boolean isSaveFocusConfirmationRepairInfoEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.SAVE_FOCUS_CONFIRMATION_REPAIR_IMAGES);
  }

  /**
   * @author Laura Cormos
   */
  public void setSaveFocusConfirmationRepairInfoEnabled(boolean enabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.SAVE_FOCUS_CONFIRMATION_REPAIR_IMAGES, enabled);
  }
}
