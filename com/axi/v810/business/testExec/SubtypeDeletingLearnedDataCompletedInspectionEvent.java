package com.axi.v810.business.testExec;

/**
 * This InspectionEvent is fired by the StatisticalTuningEngine whenever a subtype's learned data is deleted.
 *
 * @author Sunit Bhalla
 */
public class SubtypeDeletingLearnedDataCompletedInspectionEvent extends InspectionEvent
{

  /*
   * @author Sunit Bhalla
   */
  public SubtypeDeletingLearnedDataCompletedInspectionEvent()
  {
    super(InspectionEventEnum.SUBTYPE_DELETING_LEARNED_DATA_COMPLETED);
  }
}
