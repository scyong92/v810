package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAnalysis.*;

/**
 * Specialization of InspectionEvent to represent events related to interactive learning.
 *
 * @author Matt Wharton
 * @author George David
 * @author George Booth
 */
public class InteractiveLearningExposedPadInspectionEvent extends InspectionEvent
{
  private Algorithm _algorithm;
  private List<Image> _imageList;
  private List<String> _stringList;

  /**
   * Constructor.
   *
   * @author George Booth
   */
  public InteractiveLearningExposedPadInspectionEvent(InspectionEventEnum eventEnum, Algorithm algorithm)
  {
    super(eventEnum);
    Assert.expect(eventEnum.equals(InspectionEventEnum.INTERACTIVE_LEARNING_EXPOSED_PAD_USER_INFO));
    Assert.expect(algorithm != null);

    _algorithm = algorithm;
  }

  /**
   * @author George Booth
   */
  public Algorithm getAlgorithm()
  {
    Assert.expect(_algorithm != null);

    return _algorithm;
  }

  /**
   * @author George Booth
   */
  public void setImageData(List<Image> imageList)
  {
    Assert.expect(imageList != null);

    _imageList = imageList;
  }

  /**
   * @author George Booth
   */
  public List<Image> getImageList()
  {
    Assert.expect(_imageList != null);

    return _imageList;
  }

  /**
   * @author George Booth
   */
  public boolean hasImageList()
  {
    return (_imageList != null);
  }

  /**
   * @author George Booth
   */
  public void setStringData(List<String> stringList)
  {
    Assert.expect(stringList != null);

    _stringList = stringList;
  }

  /**
   * @author George Booth
   */
  public List<String> getStringList()
  {
    Assert.expect(_stringList != null);

    return _stringList;
  }

  /**
   * @author George Booth
   */
  public boolean hasStringList()
  {
    return (_stringList != null);
  }

}
