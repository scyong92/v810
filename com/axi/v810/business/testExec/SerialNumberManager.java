package com.axi.v810.business.testExec;

import java.util.*;
import java.util.regex.*;

import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.config.*;

/**
 * Use this class to look up a project name from a serial number or to store
 * new serial number regular expression to project names for future use.
 * Also, use this class to see if a provided serial number should be
 * skipped (i.e. not tested).
 *
 * @author Bill Darbie
 * @author Michael Tutkowski
 */
public class SerialNumberManager
{
  private static SerialNumberManager _instance;

  private SerialNumberToProjectReader _serialNumberToProjectNameReader;
  private SerialNumberToProjectWriter _serialNumberToProjectNameWriter;
  private SerialNumbersListReader _serialNumbersToSkipReader;
  private SerialNumbersListWriter _serialNumbersToSkipWriter;
  private SerialNumbersListReader _validSerialNumbersReader;
  private SerialNumbersListWriter _validSerialNumbersWriter;
  private Config _config;

  /**
   * @author Michael Tutkowski
   */
  public static synchronized SerialNumberManager getInstance()
  {
    if (_instance == null)
    {
      _instance = new SerialNumberManager();
    }

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private SerialNumberManager()
  {
    _serialNumberToProjectNameReader = SerialNumberToProjectReader.getInstance();
    _serialNumberToProjectNameWriter = SerialNumberToProjectWriter.getInstance();
    String serialNumbersToSkipFileName = FileName.getSerialNumbersToSkipConfigFullPath();
    String validSerialNumbersFileName = FileName.getValidSerialNumbersConfigFullPath();
    _serialNumbersToSkipReader = SerialNumbersListReader.getInstance(serialNumbersToSkipFileName);
    _serialNumbersToSkipWriter = SerialNumbersListWriter.getInstance(serialNumbersToSkipFileName);
    _validSerialNumbersReader = SerialNumbersListReader.getInstance(validSerialNumbersFileName);
    _validSerialNumbersWriter = SerialNumbersListWriter.getInstance(validSerialNumbersFileName);
    _config = Config.getInstance();
  }

  /**
   * USE ONLY FOR UNIT TESTING!!
   *
   * @author Bill Darbie
   */
  void setSerialNumberToProjectConfigFileNameForUnitTesting(String fileName)
  {
    Assert.expect(fileName != null);

    _serialNumberToProjectNameReader.setConfigFileNameForUnitTesting(fileName);
    _serialNumberToProjectNameWriter.setConfigFileNameForUnitTesting(fileName);
  }

  /**
   * USE ONLY FOR UNIT TESTING!!
   *
   * @author Michael Tutkowski
   */
  void setSerialNumbersToSkipConfigFileNameForUnitTesting(String fileName)
  {
    Assert.expect(fileName != null);

    _serialNumbersToSkipReader = SerialNumbersListReader.getInstance(fileName);
    _serialNumbersToSkipWriter = SerialNumbersListWriter.getInstance(fileName);
  }

  /**
   * USE ONLY FOR UNIT TESTING!!
   *
   * @author Michael Tutkowski
   */
  void setValidSerialNumbersConfigFileNameForUnitTesting(String fileName)
  {
    Assert.expect(fileName != null);

    _validSerialNumbersReader = SerialNumbersListReader.getInstance(fileName);
    _validSerialNumbersWriter = SerialNumbersListWriter.getInstance(fileName);
  }

  /**
   * Given a serial number, return whether or not a panel or board with
   * this serial number should be tested.
   *
   * @return true if the panel or board with this serial number
   * should be tested; else, false
   *
   * @author Michael Tutkowski
   */
  public boolean shouldSerialNumberBeSkipped(String serialNumber) throws DatastoreException
  {
    Assert.expect(serialNumber != null);

    if (isSkipSerialNumbersEnabled() == false)
      return false;

    return isSerialNumberPresent(serialNumber, _serialNumbersToSkipReader);
  }

  /**
   * Given a serial number, return whether or not a panel or board with
   * this serial number is valid.
   *
   * @return true if a panel or board with this serial number
   * is valid; else, false
   *
   * @author Michael Tutkowski
   */
  public boolean isSerialNumberValid(String serialNumber) throws DatastoreException
  {
    Assert.expect(serialNumber != null);

    return isSerialNumberPresent(serialNumber, _validSerialNumbersReader);
  }

  /**
   * @return true if serialNumber is present in the file fileName; else, return false
   *
   * @author Michael Tutkowski
   */
  private boolean isSerialNumberPresent(String serialNumber, SerialNumbersListReader serialNumbersListReader) throws DatastoreException
  {
    Assert.expect(serialNumber != null);
    Assert.expect(serialNumbersListReader != null);

    Collection collectionOfSerialNumberRegularExpressionData = serialNumbersListReader.getCollectionOfSerialNumberExpressionData();
    boolean match = false;

    // see if the serial number entered matches anything in the file

    Iterator itr = collectionOfSerialNumberRegularExpressionData.iterator();

    while (itr.hasNext())
    {
      SerialNumberRegularExpressionData serialNumberRegularExpression = (SerialNumberRegularExpressionData)itr.next();

      String regEx = serialNumberRegularExpression.getRegularExpression();
      boolean interpretAsRegEx = serialNumberRegularExpression.isInterpretedAsRegularExpression();
      Pattern pattern = null;

      pattern = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
      Matcher matcher = pattern.matcher(serialNumber);

      if (matcher.find())
      {
        match = true;
        break;
      }
    }

    return match;
  }

  /**
   * Given a serial number, return the project name.
   * @return the project name that corresponds to the serial number passed in.  If the
   * serial number does not match, return an empty string.
   *
   * @author Bill Darbie
   */
  public String getProjectNameFromSerialNumber(String serialNumber) throws DatastoreException, HardwareException
  {
    Assert.expect(serialNumber != null);
    Assert.expect(_serialNumberToProjectNameReader != null);

    String projectName = "";
    Collection serialNumberRegularExpressionDataCollection = _serialNumberToProjectNameReader.getSerialNumberRegularExpressionData();

    // see if the serial number entered matches anything in the serial number file
    // iterate over the regular expressions in the same order they were listed in the file

    Iterator it = serialNumberRegularExpressionDataCollection.iterator();

    while (it.hasNext())
    {
      SerialNumberRegularExpressionData serialNumberRegularExpressionData = (SerialNumberRegularExpressionData)it.next();

      String regEx = serialNumberRegularExpressionData.getRegularExpression();
      boolean interpretAsRegEx = serialNumberRegularExpressionData.isInterpretedAsRegularExpression();
      Pattern pattern = null;

      if (interpretAsRegEx == false)
      {
        pattern = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
      }
      else
      {
        pattern = Pattern.compile(regEx);
      }

      Matcher matcher = pattern.matcher(serialNumber);

      if (matcher.find())
      {
        // we found a match, get the panel name
        projectName = serialNumberRegularExpressionData.getProjectName();

        break;
      }
    }

    Assert.expect(projectName != null);

    return projectName;
  }
  
  /**
   * @author Kok Chun, Tan
   * get variation name from serial number matching
   */
  public String getVariationNameFromSerialNumber(String serialNumber) throws DatastoreException, HardwareException
  {
    Assert.expect(serialNumber != null);
    Assert.expect(_serialNumberToProjectNameReader != null);

    String variationName = VariationSettingManager.ALL_LOADED;
    Collection serialNumberRegularExpressionDataCollection = _serialNumberToProjectNameReader.getSerialNumberRegularExpressionData();

    // see if the serial number entered matches anything in the serial number file
    // iterate over the regular expressions in the same order they were listed in the file

    Iterator it = serialNumberRegularExpressionDataCollection.iterator();

    while (it.hasNext())
    {
      SerialNumberRegularExpressionData serialNumberRegularExpressionData = (SerialNumberRegularExpressionData)it.next();

      String regEx = serialNumberRegularExpressionData.getRegularExpression();
      boolean interpretAsRegEx = serialNumberRegularExpressionData.isInterpretedAsRegularExpression();
      Pattern pattern = null;

      if (interpretAsRegEx == false)
      {
        pattern = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
      }
      else
      {
        pattern = Pattern.compile(regEx);
      }

      Matcher matcher = pattern.matcher(serialNumber);

      if (matcher.find())
      {
        // we found a match, get the variation name
        variationName = serialNumberRegularExpressionData.getVariationName();
        break;
      }
    }

    Assert.expect(variationName != null);

    return variationName;
  }

  /**
   * @return a Collection of SerialNumberRegularExpressionData instances related
   * to the the serial-numbers-to-skip file.
   *
   * @author Michael Tutkowski
   */
  public Collection<SerialNumberRegularExpressionData> getSerialNumbersToSkipRegularExpressionData() throws DatastoreException
  {
    Assert.expect(_serialNumbersToSkipReader != null);

    return _serialNumbersToSkipReader.getCollectionOfSerialNumberExpressionData();
  }

  /**
   * @author Michael Tutkowski
   */
  public void saveSerialNumbersToSkipRegularExpressionData(Collection<SerialNumberRegularExpressionData> serialNumberRegularExpressionData) throws
      DatastoreException
  {
    Assert.expect(serialNumberRegularExpressionData != null);
    Assert.expect(_serialNumbersToSkipWriter != null);

    _serialNumbersToSkipWriter.write(serialNumberRegularExpressionData);
  }

  /**
   * @return a Collection of SerialNumberRegularExpressionData instances related
   * to the valid serial numbers file.
   *
   * @author Michael Tutkowski
   */
  public Collection<SerialNumberRegularExpressionData> getValidSerialNumbersRegularExpressionData() throws DatastoreException
  {
    Assert.expect(_validSerialNumbersReader != null);

    return _validSerialNumbersReader.getCollectionOfSerialNumberExpressionData();
  }

  /**
   * @author Michael Tutkowski
   */
  public void saveValidSerialNumbersRegularExpressionData(Collection<SerialNumberRegularExpressionData> serialNumberRegularExpressionData) throws
      DatastoreException
  {
    Assert.expect(serialNumberRegularExpressionData != null);
    Assert.expect(_validSerialNumbersWriter != null);

    _validSerialNumbersWriter.write(serialNumberRegularExpressionData);
  }

  /**
   * @return a Collection of SerialNumberRegularExpressionData.
   * @author Bill Darbie
   */
  public Collection<SerialNumberRegularExpressionData> getSerialNumberToProjectRegularExpressionData() throws DatastoreException
  {
    Assert.expect(_serialNumberToProjectNameReader != null);

    return _serialNumberToProjectNameReader.getSerialNumberRegularExpressionData();
  }

  /**
   * @author Bill Darbie
   */
  public void saveSerialNumberToProjectRegularExpressionData(Collection<SerialNumberRegularExpressionData>
                                                             serialNumberRegularExpressionData) throws DatastoreException
  {
    Assert.expect(serialNumberRegularExpressionData != null);
    Assert.expect(_serialNumberToProjectNameWriter != null);

    _serialNumberToProjectNameWriter.write(serialNumberRegularExpressionData);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isUseSerialNumberToFindProjectEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.USE_SERIAL_NUMBER_TO_FIND_PROJECT);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isUseFirstSerialNumberForProjectLookupOnlyEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.USE_FIRST_SERIAL_NUMBER_FOR_PROJECT_LOOKUP_ONLY);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isUseUniqueSerialNumberPerBoardEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.USE_UNIQUE_SERIAL_NUMBER_PER_BOARD);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isCheckForDuplicateSerialNumbersEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.CHECK_FOR_DUPLICATE_SERIAL_NUMBERS);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isValidateBoardSerialNumbersEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.VALIDATE_BOARD_SERIAL_NUMBERS);
  }


  /**
   * @author Laura Cormos
   */
  public boolean isSkipSerialNumbersEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.SKIP_SERIAL_NUMBERS);
  }

  /**
   * @author Laura Cormos
   */
  public boolean isTestExecPreSelectingProject()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.TEST_EXEC_PRE_SELECT_PROJECT);
  }

  /**
   * @author Laura Cormos
   */
  public boolean isTestExecDisplayingLastCadImageInQueue()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.TEST_EXEC_DISPLAY_LAST_CAD_IMAGE_IN_QUEUE);
  }

  /**
   * @author Laura Cormos
   */
  public boolean areOnlyNewSerialNumbersAllowedWhenCadMatchFails()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.ALLOW_ONLY_NEW_SERIAL_NUMBERS_WHEN_CAD_MATCH_FAILS);
  }

  /**
   * This seemingly barcode scanner related configuration enum is actually used when processing
   * serial numbers in test exec gui. After all, serial numbers are scanned in with a BCR.
   * @author Laura Cormos
   */
  public boolean isBarcodeScannerReadErrorCheckEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.BARCODE_SCANNER_READ_ERROR_CHECK);
  }

  /**
   * @author Laura Cormos
   */
  public void setUseUniqueSerialNumberPerBoard(boolean enabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.USE_UNIQUE_SERIAL_NUMBER_PER_BOARD, enabled);
  }

  /**
   * @author Laura Cormos
   */
  public void setUseFirstSerialNumberForProjectLookupOnly(boolean enabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.USE_FIRST_SERIAL_NUMBER_FOR_PROJECT_LOOKUP_ONLY, enabled);
  }

  /**
   * @author Laura Cormos
   */
  public void setUseSerialNumberToFindProject(boolean enabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.USE_SERIAL_NUMBER_TO_FIND_PROJECT, enabled);
  }

  /**
   * @author Laura Cormos
   */
  public void setValidateBoardSerialNumbers(boolean enabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.VALIDATE_BOARD_SERIAL_NUMBERS, enabled);
  }

  /**
   * @author Laura Cormos
   */
  public void setCheckForDuplicateSerialNumbers(boolean enabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.CHECK_FOR_DUPLICATE_SERIAL_NUMBERS, enabled);
  }

  /**
   * @author Laura Cormos
   */
  public void setSkipSerialNumbers(boolean enabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.SKIP_SERIAL_NUMBERS, enabled);
  }

  /**
   * @author Laura Cormos
   */
  public void setTestExecPreSelectProject(boolean enabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.TEST_EXEC_PRE_SELECT_PROJECT, enabled);
  }


  /**
   * @author Laura Cormos
   */
  public void setTestExecDisplayLastCADImageInQueue(boolean enabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.TEST_EXEC_DISPLAY_LAST_CAD_IMAGE_IN_QUEUE, enabled);
  }

  /**
   * @author Laura Cormos
   */
  public void setAllowOnlyNewSerialNumbersWhenCADMatchFails(boolean enabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.ALLOW_ONLY_NEW_SERIAL_NUMBERS_WHEN_CAD_MATCH_FAILS, enabled);
  }

  /**
   * This seemingly barcode scanner related configuration enum is actually used when processing
   * serial numbers in test exec. After all, serial numbers are scanned in with a BCR.
   * @author Laura Cormos
   */
  public void setBarcodeScannerReadErrorCheck(boolean enabled) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.BARCODE_SCANNER_READ_ERROR_CHECK, enabled);
  }
}
