package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Bill Darbie
 */
public class PanelSamplingModeEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  private static Map<String, PanelSamplingModeEnum> _configFileNameToPanelSamplingModeMap = new HashMap<String, PanelSamplingModeEnum>();

  public static PanelSamplingModeEnum NO_SAMPLING = new PanelSamplingModeEnum(++_index, SoftwareConfigEnum.getPanelSamplingModeNoSampling());
  public static PanelSamplingModeEnum TEST_EVERY_NTH_PANEL = new PanelSamplingModeEnum(++_index, SoftwareConfigEnum.getPanelSamplingModeTestEveryNthPanel());
  public static PanelSamplingModeEnum SKIP_EVERY_NTH_PANEL = new PanelSamplingModeEnum(++_index, SoftwareConfigEnum.getPanelSamplingModeSkipEveryNthPanel());

  private String _configFileString;


  /**
   * @author Bill Darbie
   */
  private PanelSamplingModeEnum(int id, String configFileString)
  {
    super(id);
    Assert.expect(configFileString != null);
    _configFileString = configFileString;
    _configFileNameToPanelSamplingModeMap.put(configFileString, this);
  }

  /**
   * @author Bill Darbie
   */
  String getConfigFileString()
  {
    Assert.expect(_configFileString != null);
    return _configFileString;
  }

  /**
   * @author Bill Darbie
   */
  static PanelSamplingModeEnum getPanelSamplingModeEnum(String configFileString)
  {
    Assert.expect(configFileString != null);
    PanelSamplingModeEnum panelSamplingModeEnum = _configFileNameToPanelSamplingModeMap.get(configFileString);
    Assert.expect(panelSamplingModeEnum != null);
    return panelSamplingModeEnum;
  }
}
