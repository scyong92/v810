package com.axi.v810.business.testExec;

import com.axi.util.*;

/**
 * This InspectionEvent is fired by the StatisticalTuningEngine whenever deleting learned data is started.
 *
 * @author Sunit Bhalla
 */
public class DeletingLearnedDataStartedInspectionEvent extends InspectionEvent
{
  private int _numberOfSubtypesToDeleteLearnedData = -1;

  /**
   * @author Sunit Bhalla
   */
  public DeletingLearnedDataStartedInspectionEvent(int numberOfSubtypesToDeleteLearnedData)
  {
    super(InspectionEventEnum.DELETING_LEARNED_DATA_STARTED);

    Assert.expect(numberOfSubtypesToDeleteLearnedData >= 0);

    _numberOfSubtypesToDeleteLearnedData = numberOfSubtypesToDeleteLearnedData;
  }

  /**
   * @author Sunit Bhalla
   */
  public int getNumberOfSubtypesToDeleteLearnedData()
  {
    Assert.expect(_numberOfSubtypesToDeleteLearnedData != -1);

    return _numberOfSubtypesToDeleteLearnedData;
  }

}
