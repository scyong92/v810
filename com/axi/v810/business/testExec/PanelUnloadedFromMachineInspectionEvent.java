package com.axi.v810.business.testExec;

/**
 * @author Bill Darbie
 */
public class PanelUnloadedFromMachineInspectionEvent extends InspectionEvent
{
  /**
   * @author Bill Darbie
   */
  public PanelUnloadedFromMachineInspectionEvent()
  {
    super(InspectionEventEnum.PANEL_UNLOADED_FROM_MACHINE);
  }
}
