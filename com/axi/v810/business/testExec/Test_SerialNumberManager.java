package com.axi.v810.business.testExec;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Bill Darbie
 */
public class Test_SerialNumberManager extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_SerialNumberManager());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    SerialNumberManager serialNumberManager = SerialNumberManager.getInstance();
    boolean origSkipSerialNumbersEnabled = serialNumberManager.isSkipSerialNumbersEnabled();

    try
    {
      String projectName = serialNumberManager.getProjectNameFromSerialNumber("nonExistantSerialNumber");
      Assert.expect(projectName.equals(""));
      boolean valid = serialNumberManager.isSerialNumberValid("nonValidSerialNumber");
      Assert.expect(valid == false);
      boolean skip = serialNumberManager.shouldSerialNumberBeSkipped("nonSkippedSerialNumber");
      Assert.expect(skip == false);

      String fileName = getTestDataDir() + File.separator + "serialNumberToProject1.config";
      serialNumberManager.setSerialNumberToProjectConfigFileNameForUnitTesting(fileName);

      // try a serial number that has no match
      String projName = serialNumberManager.getProjectNameFromSerialNumber("123");
      Assert.expect(projName.equals(""));

      // 12?4 project1
      projName = serialNumberManager.getProjectNameFromSerialNumber("1234");
      Assert.expect(projName.equals("project1"));

      // 12?4 project1
      projName = serialNumberManager.getProjectNameFromSerialNumber("12d4");
      Assert.expect(projName.equals("project1"));

      // 12?4 project1
      projName = serialNumberManager.getProjectNameFromSerialNumber("1234");
      Assert.expect(projName.equals("project1"));

      // 12?4 project1
      projName = serialNumberManager.getProjectNameFromSerialNumber("12 4");
      Assert.expect(projName.equals("project1"));

      // 1111* project2
      projName = serialNumberManager.getProjectNameFromSerialNumber("1111");
      Assert.expect(projName.equals("project2"));

      // 1111* project2
      projName = serialNumberManager.getProjectNameFromSerialNumber("1111 spaces are ok");
      Assert.expect(projName.equals("project2"));

      // 1111* project2
      projName = serialNumberManager.getProjectNameFromSerialNumber("1111 spaces are ok ");
      Assert.expect(projName.equals("project2"));

      // 1111* project2
      projName = serialNumberManager.getProjectNameFromSerialNumber("1111890");
      Assert.expect(projName.equals("project2"));

      // 77[a|b]89 project3
      projName = serialNumberManager.getProjectNameFromSerialNumber("77a89");
      Assert.expect(projName.equals("project3"));

      // 7.7? project 4
      projName = serialNumberManager.getProjectNameFromSerialNumber("7772");
      Assert.expect(projName.equals(""));

      // 7.7? project 4
      projName = serialNumberManager.getProjectNameFromSerialNumber("7.72");
      Assert.expect(projName.equals("project4"));

      Collection coll = serialNumberManager.getSerialNumberToProjectRegularExpressionData();
      Assert.expect(coll.size() == 4);

      fileName = getTestDataDir() + File.separator + "serialNumbersToSkip1.config";
      serialNumberManager.setSerialNumbersToSkipConfigFileNameForUnitTesting(fileName);

      // try a serial number that has no match
      boolean skipSerialNumber = serialNumberManager.shouldSerialNumberBeSkipped("123");
      Assert.expect(skipSerialNumber == false);


      // now disable skipping
      serialNumberManager.setSkipSerialNumbers(false);

      // 12?4
      skipSerialNumber = serialNumberManager.shouldSerialNumberBeSkipped("1234");
      Assert.expect(skipSerialNumber == false);

      // now enable skipping
      serialNumberManager.setSkipSerialNumbers(true);

      // 12?4
      skipSerialNumber = serialNumberManager.shouldSerialNumberBeSkipped("1234");
      Assert.expect(skipSerialNumber == true);

      // 12?4
      skipSerialNumber = serialNumberManager.shouldSerialNumberBeSkipped("12d4");
      Assert.expect(skipSerialNumber == true);

      // 12?4
      skipSerialNumber = serialNumberManager.shouldSerialNumberBeSkipped("1234");
      Assert.expect(skipSerialNumber == true);

      // 12?4
      skipSerialNumber = serialNumberManager.shouldSerialNumberBeSkipped("12 4");
      Assert.expect(skipSerialNumber == true);

      // 1111*
      skipSerialNumber = serialNumberManager.shouldSerialNumberBeSkipped("1111");
      Assert.expect(skipSerialNumber == true);

      // 1111*
      skipSerialNumber = serialNumberManager.shouldSerialNumberBeSkipped("1111 spaces are ok");
      Assert.expect(skipSerialNumber == true);

      // 1111*
      skipSerialNumber = serialNumberManager.shouldSerialNumberBeSkipped("1111 spaces are ok ");
      Assert.expect(skipSerialNumber == true);

      // 1111*
      skipSerialNumber = serialNumberManager.shouldSerialNumberBeSkipped("1111890");
      Assert.expect(skipSerialNumber == true);

      // 77[a|b]89 project3
      skipSerialNumber = serialNumberManager.shouldSerialNumberBeSkipped("77a89");
      Assert.expect(skipSerialNumber == true);

      // 7.7? project 4
      skipSerialNumber = serialNumberManager.shouldSerialNumberBeSkipped("7772");
      Assert.expect(skipSerialNumber == false);

      // 7.7? project 4
      skipSerialNumber = serialNumberManager.shouldSerialNumberBeSkipped("7.72");
      Assert.expect(skipSerialNumber == true);

      // 123abc
      skipSerialNumber = serialNumberManager.shouldSerialNumberBeSkipped("123abc");
      Assert.expect(skipSerialNumber == true);

      coll = serialNumberManager.getSerialNumbersToSkipRegularExpressionData();
      Assert.expect(coll.size() == 5);

      fileName = getTestDataDir() + File.separator + "validSerialNumbers1.config";
      serialNumberManager.setValidSerialNumbersConfigFileNameForUnitTesting(fileName);

      // try a serial number that has no match
      boolean validSerialNumber = serialNumberManager.isSerialNumberValid("123");
      Assert.expect(validSerialNumber == false);

      // 12?4
      validSerialNumber = serialNumberManager.isSerialNumberValid("1234");
      Assert.expect(validSerialNumber == true);

      // 12?4
      validSerialNumber = serialNumberManager.isSerialNumberValid("12d4");
      Assert.expect(validSerialNumber == true);

      // 12?4
      validSerialNumber = serialNumberManager.isSerialNumberValid("1234");
      Assert.expect(validSerialNumber == true);

      // 12?4
      validSerialNumber = serialNumberManager.isSerialNumberValid("12 4");
      Assert.expect(validSerialNumber == true);

      // 1111*
      validSerialNumber = serialNumberManager.isSerialNumberValid("1111");
      Assert.expect(validSerialNumber == true);

      // 1111*
      validSerialNumber = serialNumberManager.isSerialNumberValid("1111 spaces are ok");
      Assert.expect(validSerialNumber == true);

      // 1111*
      validSerialNumber = serialNumberManager.isSerialNumberValid("1111 spaces are ok ");
      Assert.expect(validSerialNumber == true);

      // 1111*
      validSerialNumber = serialNumberManager.isSerialNumberValid("1111890");
      Assert.expect(validSerialNumber == true);

      // 77[a|b]89 project3
      validSerialNumber = serialNumberManager.isSerialNumberValid("77a89");
      Assert.expect(validSerialNumber == true);

      // 7.7? project 4
      validSerialNumber = serialNumberManager.isSerialNumberValid("7772");
      Assert.expect(validSerialNumber == false);

      // 7.7? project 4
      validSerialNumber = serialNumberManager.isSerialNumberValid("7.72");
      Assert.expect(validSerialNumber == true);

      // 123abc
      validSerialNumber = serialNumberManager.isSerialNumberValid("123abc");
      Assert.expect(validSerialNumber == true);

      coll = serialNumberManager.getValidSerialNumbersRegularExpressionData();
      Assert.expect(coll.size() == 5);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        serialNumberManager.setSkipSerialNumbers(origSkipSerialNumbersEnabled);
      }
      catch (DatastoreException ex)
      {
        ex.printStackTrace();
      }
    }
  }
}
