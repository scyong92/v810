package com.axi.v810.business.testExec;

import com.axi.util.*;

/**
 * This class keeps track of how long various parts of the inspection take to run.
 *
 * @author Bill Darbie
 */
public class TestExecutionTimer
{
  private static TestExecutionTimer _instance;

  // includes everything except panel load and panel unload times
  private TimerUtil _fullInspectionTimer = new TimerUtil();
  private TimerUtil _beatRateTimer = new TimerUtil();

  private TimerUtil _panelLoadTimer = new TimerUtil();
  private TimerUtil _panelUnloadTimer = new TimerUtil();
  // time to gather alignment images and figure out the transform
  private TimerUtil _alignmentTimer = new TimerUtil();
  //
  private TimerUtil _surfaceMappingTimer = new TimerUtil();
  private TimerUtil _jointInspectionTimer = new TimerUtil();
  private TimerUtil _calibrationTimer = new TimerUtil();
  private TimerUtil _imageSaveTimer = new TimerUtil();
  private TimerUtil _xmlWriterTimer = new TimerUtil();
  private TimerUtil _inspectionResultsWriterTimer = new TimerUtil();

  private TimerUtil _irpAndCameraInitializeTimer = new TimerUtil();

  private TimerUtil _scanPassGeneratorTimer = new TimerUtil();
  private TimerUtil _initStageScanPassTimer = new TimerUtil();

  // total imageAcquistion time including stage pauses
  // time to acquire inspection images and alignment images
  // this does NOT include extra re-recontruction requests from the classification code
  private TimerUtil _imageAcquisitionTimer = new TimerUtil();
  // time to scan (includes alignment and inspection scanning and any stage pauses)
  private TimerUtil _stageScanTimer = new TimerUtil();
  // when the IRP's run low on memory this pause happens to stop more projections from coming in from the cameras
  private TimerUtil _stagePausedTimer = new TimerUtil();
  // IRP's are paused when Java gets too low on memory
  private TimerUtil _irpPausedTimer = new TimerUtil();
  private TimerUtil _rereconstructTimer = new TimerUtil();
  private long      _rereconstructCount = 0;
  private TimerUtil _rereconstructIntervalTimer = new TimerUtil();
  private TimerUtil _rereconstructAcquireTimer = new TimerUtil();
  private long      _slices = 0;

  private TimerUtil _motionStartupTimer = new TimerUtil();
  private TimerUtil _allHardwareStartupTimer = new TimerUtil();
  private TimerUtil _xrayOnTimer = new TimerUtil();
  private TimerUtil _xrayOffTimer = new TimerUtil();
  
  // PSP Surface Mapping
  private TimerUtil _opticalImageAcquisitionTimer = new TimerUtil();
  private TimerUtil _fringePatternOnTimer = new TimerUtil();
  private TimerUtil _fringePatternOffTimer = new TimerUtil();

  /**
   * @author Bill Darbie
   */
  public TestExecutionTimer()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  TestExecutionTimer(TestExecutionTimer testExecutionTimer)
  {
    Assert.expect(testExecutionTimer != null);

    _fullInspectionTimer = new TimerUtil(testExecutionTimer._fullInspectionTimer);
    _beatRateTimer = new TimerUtil(testExecutionTimer._beatRateTimer);

    _panelLoadTimer = new TimerUtil(testExecutionTimer._panelLoadTimer);
    _panelUnloadTimer = new TimerUtil(testExecutionTimer._panelUnloadTimer);
    _alignmentTimer = new TimerUtil(testExecutionTimer._alignmentTimer);
    _surfaceMappingTimer = new TimerUtil(testExecutionTimer._surfaceMappingTimer);
    _jointInspectionTimer = new TimerUtil(testExecutionTimer._jointInspectionTimer);
    _calibrationTimer = new TimerUtil(testExecutionTimer._calibrationTimer);
    _imageSaveTimer = new TimerUtil(testExecutionTimer._imageSaveTimer);
    _xmlWriterTimer = new TimerUtil(testExecutionTimer._xmlWriterTimer);
    _inspectionResultsWriterTimer = new TimerUtil(testExecutionTimer._inspectionResultsWriterTimer);

    _irpAndCameraInitializeTimer = new TimerUtil(testExecutionTimer._irpAndCameraInitializeTimer);
    _scanPassGeneratorTimer = new TimerUtil(testExecutionTimer._scanPassGeneratorTimer);
    _initStageScanPassTimer = new TimerUtil(testExecutionTimer._initStageScanPassTimer);

    _imageAcquisitionTimer = new TimerUtil(testExecutionTimer._imageAcquisitionTimer);
    _stageScanTimer = new TimerUtil(testExecutionTimer._stageScanTimer);
    _stagePausedTimer = new TimerUtil(testExecutionTimer._stagePausedTimer);
    _irpPausedTimer = new TimerUtil(testExecutionTimer._irpPausedTimer);
    _rereconstructTimer = new TimerUtil(testExecutionTimer._rereconstructTimer);
    _rereconstructCount = testExecutionTimer._rereconstructCount;
    _rereconstructAcquireTimer = new TimerUtil(testExecutionTimer._rereconstructAcquireTimer);
    _slices = testExecutionTimer._slices;
    _rereconstructIntervalTimer = new TimerUtil(testExecutionTimer._rereconstructIntervalTimer);

    _motionStartupTimer = new TimerUtil(testExecutionTimer._motionStartupTimer);
    _allHardwareStartupTimer = new TimerUtil(testExecutionTimer._allHardwareStartupTimer);
    _xrayOnTimer = new TimerUtil(testExecutionTimer._xrayOnTimer);
    _xrayOffTimer = new TimerUtil(testExecutionTimer._xrayOffTimer);
    
    _opticalImageAcquisitionTimer = new TimerUtil(testExecutionTimer._opticalImageAcquisitionTimer);
    _fringePatternOnTimer = new TimerUtil(testExecutionTimer._fringePatternOnTimer);
    _fringePatternOffTimer = new TimerUtil(testExecutionTimer._fringePatternOffTimer);
  }

  /**
   * @author Bill Darbie
   */
  void reset()
  {
    _fullInspectionTimer.reset();
    //_beatRateTimer.reset(); // this one should NOT be reset

    _panelLoadTimer.reset();
    _panelUnloadTimer.reset();
    _alignmentTimer.reset();
    _surfaceMappingTimer.reset();
    _jointInspectionTimer.reset();
    _calibrationTimer.reset();
    _imageSaveTimer.reset();
    _xmlWriterTimer.reset();
    _inspectionResultsWriterTimer.reset();

    _irpAndCameraInitializeTimer.reset();
    _scanPassGeneratorTimer.reset();
    _initStageScanPassTimer.reset();

    _imageAcquisitionTimer.reset();
    _stageScanTimer.reset();
    _stagePausedTimer.reset();
    _irpPausedTimer.reset();
    _rereconstructTimer.reset();
    _rereconstructCount = 0;
    _rereconstructAcquireTimer.reset();
    _slices = 0;
    _rereconstructIntervalTimer.reset();

    _motionStartupTimer.reset();
    _allHardwareStartupTimer.reset();
    _xrayOnTimer.reset();
    _xrayOffTimer.reset();
    
    _opticalImageAcquisitionTimer.reset();
    _fringePatternOnTimer.reset();
    _fringePatternOffTimer.reset();
  }

  /**
   * @author Bill Darbie
   */
  void stopAllTimers()
  {
    _fullInspectionTimer.stop();
    _beatRateTimer.stop();

    _panelLoadTimer.stop();
    _panelUnloadTimer.stop();
    _alignmentTimer.stop();
    _surfaceMappingTimer.stop();
    _jointInspectionTimer.stop();
    _calibrationTimer.stop();
    _imageSaveTimer.stop();
    _xmlWriterTimer.stop();
    _inspectionResultsWriterTimer.stop();

    _irpAndCameraInitializeTimer.stop();
    _scanPassGeneratorTimer.stop();
    _initStageScanPassTimer.stop();

    _imageAcquisitionTimer.stop();
    _stageScanTimer.stop();
    _stagePausedTimer.stop();
    _irpPausedTimer.stop();
    _rereconstructTimer.stop();
    _rereconstructAcquireTimer.stop();
    _rereconstructIntervalTimer.stop();

    _motionStartupTimer.stop();
    _allHardwareStartupTimer.stop();
    _xrayOnTimer.stop();
    _xrayOffTimer.stop();
    
    _opticalImageAcquisitionTimer.stop();
    _fringePatternOnTimer.stop();
    _fringePatternOffTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public void startFullInspectionTimer()
  {
    _fullInspectionTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopFullInspectionTimer()
  {
    _fullInspectionTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedFullInspectionTimeInMillis()
  {
    return _fullInspectionTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void resetBeatRateTimer()
  {
    _beatRateTimer.reset();
  }

  /**
   * @author Bill Darbie
   */
  public void startBeatRateTimer()
  {
    _beatRateTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopBeatRateTimer()
  {
    _beatRateTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedBeatRateTimeInMillis()
  {
    return _beatRateTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startPanelLoadTimer()
  {
    _panelLoadTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopPanelLoadTimer()
  {
    _panelLoadTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedPanelLoadTimeInMillis()
  {
    return _panelLoadTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  void startPanelUnloadTimer()
  {
    _panelUnloadTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  void stopPanelUnloadTimer()
  {
    _panelUnloadTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedPanelUnloadTimeInMillis()
  {
    return _panelUnloadTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startAlignmentTimer()
  {
    _alignmentTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopAlignmentTimer()
  {
    _alignmentTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedAlignmentTimeInMillis()
  {
    return _alignmentTimer.getElapsedTimeInMillis();
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void startSurfaceMappingTimer()
  {
    _surfaceMappingTimer.start();
  }

  /**
   * @author Kok Chun, Tan
   */
  public void stopSurfaceMappingTimer()
  {
    _surfaceMappingTimer.stop();
  }

  /**
   * @author Kok Chun, Tan
   */
  public long getElapsedSurfaceMappingTimeInMillis()
  {
    return _surfaceMappingTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startJointInspectionTimer()
  {
    _jointInspectionTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopJointInspectionTimer()
  {
    _jointInspectionTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedJointInspectionTimeInMillis()
  {
    return _jointInspectionTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startCalibrationTimer()
  {
    _calibrationTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopCalibrationTimer()
  {
    _calibrationTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedCalibrationTimeInMillis()
  {
    return _calibrationTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startInitStageScanPassTimer()
  {
    _initStageScanPassTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopInitStageScanPassTimer()
  {
    _initStageScanPassTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedInitStageScanPassTimeInMillis()
  {
    return _initStageScanPassTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startImageSaveTimer()
  {
    _imageSaveTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  void stopImageSaveTimer()
  {
    _imageSaveTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedImageSaveTimeInMillis()
  {
    return _imageSaveTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startXmlWriterTimer()
  {
    _xmlWriterTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopXmlWriterTimer()
  {
    _xmlWriterTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedXmlWriterTimeInMillis()
  {
    return _xmlWriterTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startInspectionResultsWriterTimer()
  {
    _inspectionResultsWriterTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopInspectionResultsWriterTimer()
  {
    _inspectionResultsWriterTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedInspectionResultsWriterTimeInMillis()
  {
    return _inspectionResultsWriterTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startStagePausedTimer()
  {
    _stagePausedTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopStagePausedTimer()
  {
    _stagePausedTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedStagePausedTimeInMillis()
  {
    return _stagePausedTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startIrpPausedTimer()
  {
    _irpPausedTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopIrpPausedTimer()
  {
    _irpPausedTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedIrpPausedTimeInMillis()
  {
    return _irpPausedTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Roy Williams
   */
  public void startReReconstructTimer()
  {
    _rereconstructTimer.start();
    _rereconstructCount++;
  }

  /**
   * @author Roy Williams
   */
  public void stopReReconstructTimer()
  {
    _rereconstructTimer.stop();
  }

  /**
   * @author Roy Williams
   */
  public long getElapsedReReconstructTimeInMillis()
  {
    return _rereconstructTimer.getElapsedTimeInMillis();
  }

  /**
    * @author Roy Williams
    */
   public void startReReconstructIntervalTimer()
   {
     _rereconstructIntervalTimer.start();
   }

   /**
    * @author Roy Williams
    */
   public void stopReReconstructIntervalTimer()
   {
     _rereconstructIntervalTimer.stop();
   }

   /**
    * @author Roy Williams
    */
   public long getElapsedReReconstructIntervalTimeInMillis()
   {
     return _rereconstructIntervalTimer.getElapsedTimeInMillis();
   }

   /**
     * @author Roy Williams
     */
    public void startReReconstructAcquireTimer()
    {
      _rereconstructAcquireTimer.start();
    }

    /**
     * @author Roy Williams
     */
    public void stopReReconstructAcquireTimer(int slices)
    {
      _slices += slices;
      _rereconstructAcquireTimer.stop();
    }

    /**
     * @author Roy Williams
     */
    public long getElapsedReReconstructAcquireTimeInMillis()
    {
      return _rereconstructAcquireTimer.getElapsedTimeInMillis();
    }

   /**
    * @author Roy Williams
    */
   public long getReReconstructAcquireSlicesCount()
   {
     return _slices;
   }

  /**
   * @author Roy Williams
   */
  public long getReReconstructCount()
  {
    return _rereconstructCount;
  }

  /**
   * @author Bill Darbie
   */
  public void startIrpAndCameraInitializeTimer()
  {
    _irpAndCameraInitializeTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopIrpAndCameraInitializeTimer()
  {
    _irpAndCameraInitializeTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedIrpAndCameraInitializeTimeInMillis()
  {
    return _irpAndCameraInitializeTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startScanPassGeneratorTimer()
  {
    _scanPassGeneratorTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopScanPassGeneratorTimer()
  {
    _scanPassGeneratorTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedScanPassGeneratorTimeInMillis()
  {
    return _scanPassGeneratorTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startImageAcquisitionTimer()
  {
    _imageAcquisitionTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopImageAcquisitionTimer()
  {
    _imageAcquisitionTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedImageAcquisitionTimeInMillis()
  {
    return _imageAcquisitionTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startStageScanTimer()
  {
    _stageScanTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopStageScanTimer()
  {
    _stageScanTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedStageScanTimeInMillis()
  {
    return _stageScanTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startMotionStartupTimer()
  {
    _motionStartupTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopMotionStartupTimer()
  {
    _motionStartupTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedMotionStartupTimeInMillis()
  {
    return _motionStartupTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startAllHardwareStartupTimer()
  {
    _allHardwareStartupTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopAllHardwareStartupTimer()
  {
    _allHardwareStartupTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedAllHardwareStartupTimeInMillis()
  {
    return _allHardwareStartupTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startXrayOnTimer()
  {
    _xrayOnTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopXrayOnTimer()
  {
    _xrayOnTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedXrayOnTimeInMillis()
  {
    return _xrayOnTimer.getElapsedTimeInMillis();
  }

  /**
   * @author Bill Darbie
   */
  public void startXrayOffTimer()
  {
    _xrayOffTimer.start();
  }

  /**
   * @author Bill Darbie
   */
  public void stopXrayOffTimer()
  {
    _xrayOffTimer.stop();
  }

  /**
   * @author Bill Darbie
   */
  public long getElapsedXrayOffTimeInMillis()
  {
    return _xrayOffTimer.getElapsedTimeInMillis();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void startOpticalImageAcquisitionTimer()
  {
      _opticalImageAcquisitionTimer.start();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void stopOpticalImageAcquisitionTimer()
  {
      _opticalImageAcquisitionTimer.stop();
  }
  
  /**
   * @author Cheah Lee Herng
  */
  public void startFringePatternOnTimer()
  {
      _fringePatternOnTimer.start();
  }

  /**
  * @author Cheah Lee Herng
  */
  public void stopFringePatternOnTimer()
  {
      _fringePatternOnTimer.stop();
  }

  /**
  * @author Cheah Lee Herng
  */
  public void startFringePatternOffTimer()
  {
    _fringePatternOffTimer.start();
  }

  /**
  * @author Cheah Lee Herng
  */
  public void stopFringePatternOffTimer()
  {
    _fringePatternOffTimer.stop();
  }
}
