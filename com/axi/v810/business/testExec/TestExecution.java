package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.hardware.ethernetBasedLightEngine.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.shopFloorSystem.*;

/**
 * This class is in charge of inspecting a panel.  It used the InspectionEngine class to
 * do most of the inspection work.  This class handles the higher level functionality.
 *
 * Note on terminology:
 * This class makes a distinction between the terms Test and Inspection.
 * Test - ALL steps required to run a panel through the machine.  This includes:
 *      - setting the light stack lights
 *      - loading the panel into the machine
 *      - reading in the serial number
 *      - loading the correct project
 *      - determining which boards are x'ed out if any
 *      - inspecting the panel
 *      - unloading the panel
 *
 * @author Bill Darbie
 */
public class TestExecution implements Observer
{
  private static TestExecution _instance;

  private InspectionEngine _inspectionEngine;
  private PanelInspectionSettings _panelInspectionSettings;

  private static String _userId = "";
  private static final int _ALL_FAMILIES = -1;
  private static final int _ALL_SUBTYPES = -1;
  private static boolean _isAbortTesting = false;
  private static final String _BYPASS_MODE_DEFAULT_PROJECT_NAME = "no panel name - bypass mode"; 

  private Config _config;
  private XrayTester _xrayTester;
  private LightStack _lightStack;
  private AbstractXraySource _xraySource;
  private StageViewingLight _stageLight;
  private BarcodeReaderManager _barcodeReaderManager;
  private ManufacturingEquipmentInterface _manufacturingEquipmentInterface;
  private PanelHandler _panelHandler;
  private ImageGeneration _imageGeneration;
  private ImageManager _imageManager;
  private HardwareTaskEngine _hardwareTaskEngine;
  private DigitalIo _digitalIo;
  private EthernetBasedLightEngineProcessor _eblep = EthernetBasedLightEngineProcessor.getInstance();

  private Alignment _alignment;
  private SurfaceMapping _surfaceMapping;
  private Sampling _sampling;
//  private Xout _xout;
  private ScriptRunner _scriptRunner;

  private SerialNumberManager _serialNumberManager;

  // _userAborted stays set until the next inspection is run
  private boolean _userAborted;
  private boolean _exceptionExit = false;
  private boolean _abortExit = false;
  private boolean _panelLoadInProgress = false;
  private boolean _panelWillBeUnloaded = false;
  private boolean _panelHasBeenUnloaded = false;
  private Object _panelUnloadedLock = new Object();

  private HardwareObservable _hardwareObservable;
  private InspectionEventObservable _inspectionEventObservable;
  private TestExecutionTimer _testExecutionTimer;

  private boolean _entryPointIsTestPanel = false;
  private boolean _inspectionInProgress = false;

  private Map<String, List<String>> _serialNumbersUsedInLastPanelTest = new LinkedHashMap<String, List<String>> ();

  private TestResultsEnum _testResultsEnum;
  private boolean _precisionTuningImageAcquisition = false;
  private boolean _production;
  private ImageAcquisitionModeEnum _imageAcquisitionModeEnum;
  private String _panelSerialNumberOrDescription;
  private Map<String, String> _boardNameToSerialNumberMap;
  private ImageSetData _onlineImageSetData = null;

  private XrayTesterException _unloadException;

  private InspectionResultsXMLWriter _resultsXmlWriter;
  private ProductionTuneManager _productionTuneManager;
  private ImageAcquisitionEngine _imageAcquisitionEngine;

  private boolean _imageAcquisitionComplete = false;
  private boolean _atLeastOneInspectionFailure = false;
  private boolean _preparedToUnloadPanel = false;
  private boolean _panelWillBePreparedToUnload = false;

  //private WorkerThread _parallelPanelUnloadWorkerThread = new WorkerThread("parallel panel unloader thread");
  //Siew Yeng - XCR-2178
  //Swee Yee - XCR-2406 Change the hardwareWorkerThread to Instance3 to let the system take action immediately instead of waiting Instance1
  private HardwareWorkerThread _parallelPanelUnloadWorkerThread = HardwareWorkerThread.getInstance3();
  private Object _abortLock = new Object();
  private Object _inspectionInProgressLock = new Object();
  private Object _updateLock = new Object();

  private int _unloadEntryPointCount = 0;

  private boolean _alignmentFailAndUnloadPanel = false;

  private static PerformanceLogUtil _performanceLog = PerformanceLogUtil.getInstance();
  
  private boolean _needToPerformRuntimeManualAlignment = false;
  private HardwareWorkerThread _manualAlignmentForSurfaceMapHardwareWorkerThread = HardwareWorkerThread.getInstance();
  private int _lastPerformedAlignmentTestSubProgramId = -1;

  //Khaw Chek Hau - XCR2654: CAMX Integration
  private AbstractShopFloorSystem _shopFloorSystem = AbstractShopFloorSystem.getInstance();
  
  //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
  private MachineUtilizationReportSystem _machineUtilizationReportSystem = MachineUtilizationReportSystem.getInstance();

   private static boolean _fastSpeedEnabled=false;
   private static TestProgram _fastSpeedEnabled_TestProgram;
   private static boolean _fastSpeedEnabled_IsVirtualLive;
   private static  boolean _fastSpeedEnabled_NeedToEnableSafetyLevelSensorForPanelLoading;
   
   protected static boolean _debug = Config.isDeveloperDebugModeOn();
   private static String _me = "TestExecution";
   
   //@author Kee Chin Seong - PreScan
   private List<String> _preScanBarcodeScanner = new ArrayList<String>();
   private BooleanLock _isPreScanRunning = new BooleanLock(false);
//   private static  TestExecutionTimer _fastSpeedEnabled_testExecutionTimer;
//   private static double _fastSpeedEnabled_zHeightDeltaAboveNominalInNanometers;
//   private static  double _fastSpeedEnabled_zHeightDeltaBelowNominalInNanometers;
//   private static   com.axi.v810.business.imageAcquisition.ImageAcquisitionModeEnum _fastSpeedEnabled_acquisitionMode;
//   private static   double _fastSpeedEnabled_stepOverridePercentage;
//   
//    public void setFastSpeedEnabledAcquireImagesParameters(TestProgram testProgram,
//                          TestExecutionTimer testExecutionTimer,
//                          double zHeightDeltaAboveNominalInNanometers,
//                          double zHeightDeltaBelowNominalInNanometers,
//                          com.axi.v810.business.imageAcquisition.ImageAcquisitionModeEnum acquisitionMode,
//                          double stepOverridePercentage) throws XrayTesterException
//    {
//      _fastSpeedEnabled_TestProgram =testProgram;
//      _fastSpeedEnabled_testExecutionTimer=testExecutionTimer;
//      _fastSpeedEnabled_zHeightDeltaAboveNominalInNanometers=zHeightDeltaAboveNominalInNanometers;
//      _fastSpeedEnabled_zHeightDeltaBelowNominalInNanometers=zHeightDeltaBelowNominalInNanometers;
//      _fastSpeedEnabled_acquisitionMode = acquisitionMode;
//      _fastSpeedEnabled_stepOverridePercentage = stepOverridePercentage;
//    }
   
  //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
  private VOneMachineStatusMonitoringSystem _vOneMachineStatusMonitoringSystem = VOneMachineStatusMonitoringSystem.getInstance();
 
   public void fastSpeedEnabled_LoadPanelIntoMachineWithoutClearingSerialNumbers() throws XrayTesterException
   {
       loadPanelIntoMachineWithoutClearingSerialNumbers(_fastSpeedEnabled_TestProgram, _fastSpeedEnabled_IsVirtualLive, _fastSpeedEnabled_NeedToEnableSafetyLevelSensorForPanelLoading);
   }
  public static boolean getFastSpeedEnable()
  {
    return _fastSpeedEnabled;
  }
  public static void resetFastSpeedEnable()
  {
    _fastSpeedEnabled = false;
  }
  /**
   * @author Bill Darbie
   */
  private TestExecution()
  {
    _inspectionEngine = InspectionEngine.getInstance();
    _panelInspectionSettings = PanelInspectionSettings.getInstance();
    _manufacturingEquipmentInterface = ManufacturingEquipmentInterface.getInstance();

    _hardwareObservable = HardwareObservable.getInstance();
    _inspectionEventObservable = InspectionEventObservable.getInstance();

    _config = Config.getInstance();
    _xrayTester = XrayTester.getInstance();
    _lightStack = LightStack.getInstance();
    _xraySource = AbstractXraySource.getInstance();
    _stageLight = StageViewingLight.getInstance();
    _panelHandler = PanelHandler.getInstance();
    _imageGeneration = ImageGeneration.getInstance();
    _imageManager = ImageManager.getInstance();
    _hardwareTaskEngine = HardwareTaskEngine.getInstance();
    _testExecutionTimer = new TestExecutionTimer();
    _hardwareTaskEngine.setTestExecutionTimer(_testExecutionTimer);
    _digitalIo = DigitalIo.getInstance();

    _alignment = Alignment.getInstance();
    _sampling = Sampling.getInstance();

//    _xout = new Xout(this);
    _scriptRunner = ScriptRunner.getInstance();

    _serialNumberManager = SerialNumberManager.getInstance();
    _barcodeReaderManager = BarcodeReaderManager.getInstance();

    _resultsXmlWriter = InspectionResultsXMLWriter.getInstance();
    _productionTuneManager = ProductionTuneManager.getInstance();
    _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();

    // most InspectionEventObservable's are created in this class and InspectionEngine and used
    // by the GUI, but this class also Observers for the ScaninngCompleteInspectionEvent so it
    // knows when to unload the panel
    _inspectionEventObservable.addObserver(this);
  
  }

  /**
   * @author Bill Darbie
   */
  public static synchronized TestExecution getInstance()
  {
    if (_instance == null)
        _instance = new TestExecution();
    
    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  public void xRayOn() throws XrayTesterException
  {
    try
    {
      clearExitAndAbortFlags();

      _lightStack.hardwareInUseMode();
      if (_xraySource.isStartupRequired())
        _xraySource.startup();
      _xraySource.on();
    }
    catch (XrayTesterException ex)
    {
      handleXrayTesterException(ex);
    }
    catch (RuntimeException rte)
    {
      handleRuntimeException(rte);
    }
    finally
    {
      setLightStackOnExit();
    }
  }

  /**
   * @author Bill Darbie
   */
  public void startup() throws XrayTesterException
  {
    try
    {
      clearExitAndAbortFlags();

      try
      {
        // On the first startup, the light stack call will not work

        // Other times, startup is used to reset from errors
        // Because of this we will attempt to set the light stack AFTER
        // startup, but not before unlike all the other methods
        if (_digitalIo.isStartupRequired() == false)
          _lightStack.standyMode();
      }
      catch (XrayTesterException ex)
      {
        // do nothing
      }
      
      //XCR-2616, XCR-2595 - 24/3/2015
      // try to turn off LED, projector is disconnect if cannot turn off.
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP) && _eblep.isStartupRequired() == false)
      {
        _eblep.isConnected();
      }

      _xrayTester.startup();
      
      _lightStack.standyMode();
    }
    catch (XrayTesterException ex)
    {
      handleXrayTesterException(ex);
    }
    catch (RuntimeException rte)
    {
      handleRuntimeException(rte);
    }
    finally
    {
      setLightStackOnExit();
    }
  }

  /**
   * @author Bill Darbie
   */
  public void shutdown() throws XrayTesterException
  {
    try
    {
      clearExitAndAbortFlags();

      try
      {
        if (_digitalIo.isStartupRequired() == false)
        {
          _lightStack.turnOffAllLights();
          _lightStack.standyMode();
        }
      }
      catch (XrayTesterException ex)
      {
        // since we are trying to shutdown the hardware, lets not stop trying just because the light stack
        // fails!
      }
      _xrayTester.shutdown();
    }
    catch (XrayTesterException ex)
    {
      handleXrayTesterException(ex);
    }
    catch (RuntimeException rte)
    {
      handleRuntimeException(rte);
    }
    finally
    {
      // since we just did a shutdown, we should not do anything with the light stack here
      //setLightStackOnExit();
    }
  }

  /**
   * @author Bill Darbie
   */
  public void startupMotionRelatedHardware() throws XrayTesterException
  {
    try
    {
      clearExitAndAbortFlags();

      try
      {
        // On the first startup, the light stack call will not work

        // Other times, startup is used to reset from errors
        // Because of this we will attempt to set the light stack AFTER
        // startup, but not before unlike all the other methods
        if (_digitalIo.isStartupRequired() == false)
          _lightStack.hardwareInUseMode();
      }
      catch (XrayTesterException ex)
      {
        // do nothing
      }

      _xrayTester.startupMotionRelatedHardware();
      _lightStack.hardwareInUseMode();
    }
    catch (XrayTesterException ex)
    {
      handleXrayTesterException(ex);
    }
    catch (RuntimeException rte)
    {
      handleRuntimeException(rte);
    }
    finally
    {
      setLightStackOnExit();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setEjectPanelIfAlignmentFails(boolean ejectPanelIfAlignmentFails) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.EJECT_PANEL_IF_ALIGNMENT_FAILS, ejectPanelIfAlignmentFails);
  }

  /**
   * @author Bill Darbie
   */
  public boolean shouldPanelBeEjectedIfAlignmentFailsRespectingOverrides()
  {
    if (isPanelEjectOverrideOn())
      return false;

    return _config.getBooleanValue(SoftwareConfigEnum.EJECT_PANEL_IF_ALIGNMENT_FAILS);
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean shouldPanelBeEjectedIfAlignmentFails()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.EJECT_PANEL_IF_ALIGNMENT_FAILS);
  }

  /**
   * @author Bill Darbie
   */
  private boolean isPanelEjectOverrideOn()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.OVERRIDE_PANEL_EJECT_FOR_TEST_EXEC);
  }

  /**
   * @author Matt Wharton
   */
  private boolean isSimulatePanelEjectOn()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.SIMULATE_PANEL_EJECT_FOR_TEST_EXEC);
  }

  /**
   * @author Bill Darbie
   */
  public void setDemoMode(boolean demoMode) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.TEST_EXEC_DEMO_MODE, demoMode);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isDemoModeEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.TEST_EXEC_DEMO_MODE);
  }

  /**
   * @author Bill Darbie
   */
  public void setBypassMode(boolean bypassMode) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.TEST_EXEC_BYPASS_MODE, bypassMode);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isBypassModeEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.TEST_EXEC_BYPASS_MODE);
  }
  
  /**
   * sheng chuan
   */
  public boolean isBypassWithSerialControlModeEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.TEST_EXEC_BYPASS_WITH_SERIAL_MODE);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean isBypassWithResultsEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.TEST_EXEC_BYPASS_WITH_RESULTS);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  public boolean isDisplayProductionPanelLoadingAndUnloadingTimeMode()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.TEST_EXEC_DISPLAY_PRODUCTION_PANEL_LOADING_AND_UNLOADING_TIME);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3774: Display load & unload time during production
   */
  public void setDisplayProductionPanelLoadingAndUnloadingTimeMode(boolean isDisplayProductionPanelLoadingAndUnloadingTime) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.TEST_EXEC_DISPLAY_PRODUCTION_PANEL_LOADING_AND_UNLOADING_TIME, isDisplayProductionPanelLoadingAndUnloadingTime);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public boolean isAlwaysDisplayVariationNameMode()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.TEST_EXEC_ALWAYS_DISPLAY_VARIATION_NAME);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setAlwaysDisplayVariationNameMode(boolean isDisplayVariationName) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.TEST_EXEC_ALWAYS_DISPLAY_VARIATION_NAME, isDisplayVariationName);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public void setSemiAutomatedMode(boolean semiAutomatedMode) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.SEMI_AUTOMATED_MODE, semiAutomatedMode);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public boolean isSemiAutomatedModeEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.SEMI_AUTOMATED_MODE);
  }
  
  /**
   * @author Laura Cormos
   */
  public void setXoutDetectionMode(boolean xoutDetectionMode) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.XOUT_DETECTION_ENABLE, xoutDetectionMode);
  }
  
  /**
   * @author Laura Cormos
   */
  public void setPromptSavingRecipeAfterLearning(boolean promptSavingRecipeAfterLearning) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.PROMPT_SAVING_RECIPE_AFTER_LEARNING, promptSavingRecipeAfterLearning);
  }
    
 /**
    * Project's history log CR
   * @author hsia-fen.tan
   */
  public void setShowHistoryLog(boolean showHistoryLog) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.CREATE_HISTORY_LOG, showHistoryLog);
  }

    public void createHistoryDir(String projectName)throws DatastoreException
  {  
      String dir=Directory.getHistoryFileDir(projectName);
      FileUtilAxi.mkdirs(dir);
  }
    
  /**
   * @author Wei Chin
   */
  public void setXoutDetectionMethod(XoutDetectionMethodEnum xoutDetectionMethod) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.XOUT_DETECTION_METHOD, xoutDetectionMethod.toString());
  }

  /**
   * @author Wei Chin
   */
  public XoutDetectionMethodEnum getXoutDetectionMethod()
  {
    String method = _config.getStringValue(SoftwareConfigEnum.XOUT_DETECTION_METHOD);

    if(method.equals(XoutDetectionMethodEnum.ALIGNMENT_FAILURE.toString()))
      return XoutDetectionMethodEnum.ALIGNMENT_FAILURE;
    else if(method.equals(XoutDetectionMethodEnum.DEFECT_THRESHOLD.toString()))
      return XoutDetectionMethodEnum.DEFECT_THRESHOLD;
    else
      Assert.expect(false, "Invalid XoutDetectionMethod KEY : " + method );

    return null;
  }

  /**
   * @author Laura Cormos
   */
  public boolean isXoutDetectionModeEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.XOUT_DETECTION_ENABLE);
  }

  /**
   * @return 
   * 
   * @authoer Wei Chin
   */
  public boolean isDefectThresholdXoutDetectionModeEnabled()
  {
    return (getXoutDetectionMethod() == XoutDetectionMethodEnum.DEFECT_THRESHOLD);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isPromptSavingRecipeAfterLearning()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.PROMPT_SAVING_RECIPE_AFTER_LEARNING);  
  }
   
   /**
    * Prompt warning message before run alignment if project contains high mag CR
   * @author hsia-fen.tan
   */
    public void setPromptWarningMsgIfRecipeContainHighMag(boolean PromptWarningMsgIfRecipeContainHighMag) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.PROMPT_WARNING_MESSAGE_IF_RECIPE_CONTAIN__HIGH, PromptWarningMsgIfRecipeContainHighMag);
  }
  
   /**
   * @author hsia-fen.tan
   */   
  public boolean isShowHistoryLog()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.CREATE_HISTORY_LOG);
  }
  
  public boolean isDeleteHistoryFile()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.AUTOMATICALLY_DELETE_HISTORY_FILES);
  }  
  
   public boolean isPromptWarningMsgIfRecipeContainHighMag()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.PROMPT_WARNING_MESSAGE_IF_RECIPE_CONTAIN__HIGH);  
  }
  
  /**
   * @author Laura Cormos
   */
  public void setXoutDetectionThreshold(int xoutDetectionThreshold) throws DatastoreException
  {
    Assert.expect(xoutDetectionThreshold >= 0);
    _config.setValue(SoftwareConfigEnum.XOUT_DETECTION_THRESHOLD, xoutDetectionThreshold);
  }

  /**
   * @author Laura Cormos
   */
  public int getXoutDetectionThreshold()
  {
    return _config.getIntValue(SoftwareConfigEnum.XOUT_DETECTION_THRESHOLD);
  }

  /**
   * @author Bill Darbie
   */
  private void checkForValidPanelSize(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    Project project = testProgram.getProject();
    Panel panel = project.getPanel();
    int panelWidth = panel.getWidthAfterAllRotationsInNanoMeters();
    int panelLength = panel.getLengthAfterAllRotationsInNanoMeters();
    int panelThickness = panel.getThicknessInNanometers();

    int minPanelWidth = _panelHandler.getMinimumPanelWidthInNanoMeters();
    int maxPanelWidth = _panelHandler.getMaximumPanelWidthInNanoMeters();
    int minPanelLength = _panelHandler.getMinimumPanelLengthInNanoMeters();
    int maxPanelLength = _panelHandler.getMaximumPanelLengthInNanoMeters();
    int minPanelThickness = _panelHandler.getMinimumPanelThicknessInNanoMeters();
    int maxPanelThickness = _panelHandler.getMaximumPanelThicknessInNanoMeters();

    // check the safety limit on width
    if ((panelWidth < minPanelWidth) || (panelWidth > maxPanelWidth))
    {
      MathUtilEnum units = project.getDisplayUnits();
      double actualWidthInUnits = MathUtil.convertUnits((double)panelWidth, MathUtilEnum.NANOMETERS, units);
      double minWidthInUnits = MathUtil.convertUnits((double)minPanelWidth, MathUtilEnum.NANOMETERS, units);
      double maxWidthInUnits = MathUtil.convertUnits((double)maxPanelWidth, MathUtilEnum.NANOMETERS, units);
      throw new InvalidPanelWidthBusinessException(units, minWidthInUnits, maxWidthInUnits, actualWidthInUnits);
    }

    // check the max safety limit on length
    if ((panelLength < minPanelLength) || (panelLength > maxPanelLength))
    {
      MathUtilEnum units = project.getDisplayUnits();
      double actualLengthInUnits = MathUtil.convertUnits(panelLength, MathUtilEnum.NANOMETERS, units);
      double minLengthInUnits = MathUtil.convertUnits(minPanelLength, MathUtilEnum.NANOMETERS, units);
      double maxLengthInUnits = MathUtil.convertUnits(maxPanelLength, MathUtilEnum.NANOMETERS, units);
      throw new InvalidPanelLengthBusinessException(units, minLengthInUnits, maxLengthInUnits, actualLengthInUnits);
    }

    // check the max safety limit on thickness
    if ((panelThickness < minPanelThickness) || (panelThickness > maxPanelThickness))
    {
      MathUtilEnum units = project.getDisplayUnits();
      double actualThicknessInUnits = MathUtil.convertUnits(panelThickness, MathUtilEnum.NANOMETERS, units);
      double minThicknessInUnits = MathUtil.convertUnits(minPanelThickness, MathUtilEnum.NANOMETERS, units);
      double maxThicknessInUnits = MathUtil.convertUnits(maxPanelThickness, MathUtilEnum.NANOMETERS, units);
      throw new InvalidPanelThicknessBusinessException(units, minThicknessInUnits, maxThicknessInUnits, actualThicknessInUnits);
    }
  }

  /**
   * Load the panel into the xray machine.
   * @author Bill Darbie
   * @author Wei Chin ()
   */
  private void loadPanelIntoMachineWithoutClearingSerialNumbers(TestProgram testProgram, boolean isVirtualLive, boolean needToEnableSafetyLevelSensorForPanelLoading) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    try
    {
      clearExitAndAbortFlags();

      checkForValidPanelSize(testProgram);

      Project project = testProgram.getProject();
      String projName = project.getName();
      Panel panel = project.getPanel();
      int panelWidth = panel.getWidthAfterAllRotationsInNanoMeters();
      int panelLength = panel.getLengthAfterAllRotationsInNanoMeters();
      int panelExtraClearDelayInMiliSeconds = project.getPanelExtraClearDelayInMiliSeconds();

      _panelLoadInProgress = true;
      // if a panel is already loaded in the machine, leave it there and
      // test that one
      if (isPanelLoaded(false, projName))
      {
        Assert.expect(_panelHandler.getLoadedPanelProjectName().equalsIgnoreCase(projName));
        return;
      }

      if (didUserAbort())
        return;
      
      //Siew Yeng - XCR1757 - clear runtime alignment transform flag
      testProgram.clearTestSubProgramsRuntimeManualAlignmentTransform();
      
      getMotionControlHardwareReadyIfNecessary();

      _lightStack.hardwareInUseMode();

      if (didUserAbort())
        return;
//remove due to redundancy - cycle time evaluation -Anthony
//      if(isVirtualLive)
//        checkNeedToEnableSafetyLevelSensorForVirtualLive(testProgram);      
//      else
//        checkNeedToEnableSafetyLevelSensorForVariableMag(testProgram);     
      loadPanelIntoMachine(projName, panelWidth, panelLength, needToEnableSafetyLevelSensorForPanelLoading, panelExtraClearDelayInMiliSeconds);

      // check that it is really loaded, maybe there was an abort or some other reason
      // why it did not load without an exception being thrown
      if (isPanelLoaded(false, projName))
      {
        PanelLoadedIntoMachineInspectionEvent loadedEvent = new PanelLoadedIntoMachineInspectionEvent();
        _inspectionEventObservable.sendEventInfo(loadedEvent);
      }
    }
    catch (XrayTesterException ex)
    {
      handleXrayTesterException(ex);
    }
    catch (RuntimeException rte)
    {
      handleRuntimeException(rte);
    }
    finally
    {
      setLightStackOnExit();

      _panelLoadInProgress = false;
    }
  }

  /*
   * @author Siew Yeng
   */
  public void loadPanelIntoMachine(TestProgram testProgram, boolean isVirtualLive) throws XrayTesterException
  {
    loadPanelIntoMachine(testProgram,isVirtualLive,true);
  }
  
  /**
   * Load the panel into the xray machine.
   * @author Bill Darbie
   * @author Chin Seong, Kee - Just to make sure everytime load panel, the testProgram will be checked
   */
  public void loadPanelIntoMachine(TestProgram testProgram, boolean isVirtualLive, boolean clearSerialNumbers) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    if(clearSerialNumbers)
      _serialNumbersUsedInLastPanelTest.clear();

    boolean needToEnableSafetyLevelSensorForPanelLoading = false;
    if(isVirtualLive)
    {
      //Recheck the testPRogram is it has virtualLive.
      needToEnableSafetyLevelSensorForPanelLoading = checkNeedToEnableSafetyLevelSensorForVirtualLive(testProgram);
    }
    else
    {
      //Recheck the testPRogram is it has High Mag.
      needToEnableSafetyLevelSensorForPanelLoading = checkNeedToEnableSafetyLevelSensorForVariableMag(testProgram);
    }
    loadPanelIntoMachineWithoutClearingSerialNumbers(testProgram, isVirtualLive, needToEnableSafetyLevelSensorForPanelLoading);

    if (didUserAbort())
    {
      abort();
    }
  }

  /**
   * @author Bill Darbie
   * @author Wei Chin
   */
  private void loadPanelIntoMachine(String projectName, 
                                    int panelWidthInNanoMeters, 
                                    int panelLengthInNanoMeters, 
                                    boolean needToEnableSafetyLevelSensorForPanelLoading,
                                    int panelExtraClearDelayInMiliSeconds) throws XrayTesterException
  {
    Assert.expect(projectName != null);
    
          
    PanelLoadingIntoMachineInspectionEvent loadingEvent = new PanelLoadingIntoMachineInspectionEvent();
    _inspectionEventObservable.sendEventInfo(loadingEvent);

    _testExecutionTimer.startPanelLoadTimer();
    // put in standby mode (yellow) because we need to wait for the user to put the panel in the machine
    _lightStack.standyMode();
    TimerUtil timerUtil = new TimerUtil();
    timerUtil.start();
    
    // we have to load the Panel with VM Sensor Checking (Wei Chin)
    //Khaw Chek Hau - XCR2654: CAMX Integration
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
    {
      try
      {
        _shopFloorSystem.loadPanel();
        _shopFloorSystem.equipmentStarved();
      }
      catch (DatastoreException ex)
      {
        DatastoreException datastoreException = (DatastoreException) ex;
        MessageInspectionEvent event = new ShopFloorMessageInspectionEvent(datastoreException);
        InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
        inspectionEventObservable.sendEventInfo(event);
      }
    }

    _panelHandler.loadPanel(projectName, panelWidthInNanoMeters, panelLengthInNanoMeters, needToEnableSafetyLevelSensorForPanelLoading, panelExtraClearDelayInMiliSeconds);
    timerUtil.stop();
    long time = timerUtil.getElapsedTimeInMillis();
//    if (UnitTest.unitTesting() == false)
//      System.out.println("wpd time to loadPanel: " + time);
    if (didUserAbort())
      return;

    // at this point, the load worked without an exception and was not aborted
    _imageManager.deleteOnlineCachedImages();
    _inspectionEngine.dropOnlineImageSetInfo();
    // now go green
    _lightStack.hardwareInUseMode();
    _testExecutionTimer.stopPanelLoadTimer();
  }

  /**
   * Unload the panel from the xray machine.
   * @author Bill Darbie
   */
  private void unloadPanelFromMachineIfAbortIsNotInProgress(TestResultsEnum testResultsEnum) throws XrayTesterException
  {
    Assert.expect(testResultsEnum != null);

//    synchronized (_panelUnloadedLock)
//    {
//      if (_panelHasBeenUnloaded)
//        return;
//      ++_unloadEntryPointCount;
//    try
//    {
//        if (_panelHandler.isPanelLoaded())
//        {
      // use _userAborted here to make sure we do not unload a panel if an abort
      // stopped the inspection
      if (_userAborted)
        return;

      if (_unloadException != null)
        throw _unloadException;

      unloadPanelFromMachine(testResultsEnum);
//        }
//    }
//    finally
//    {
//        --_unloadEntryPointCount;
//        // if this method was the unload entry point, then this method should notify that the panel has been unloaded
//        if (_unloadEntryPointCount == 0)
//        {
//          _panelHasBeenUnloaded = true;
//          _panelUnloadedLock.notifyAll();
//        }
//      }
//    }
  }

  /**
   * Unload the panel from the xray machine.
   * @author Bill Darbie
   */
  public void unloadPanelFromMachine(TestResultsEnum testResultsEnum) throws XrayTesterException
  {
    Assert.expect(testResultsEnum != null);

    // We need to sync at the lowest level and that is unloadPanelFromMachineWithoutClearingAbortOrSettingLightStack().
    // The _panelUnloadedLock is needed here just for the lightStack...
    synchronized (_panelUnloadedLock)
    {
      if (_panelHandler.isPanelLoaded() == false)
      {
        _panelUnloadedLock.notifyAll();
        return;
      }
//      if (_panelHandler.isPanelLoaded())
//      {
      try
      {
//          ++_unloadEntryPointCount;

        clearExitAndAbortFlags();
        
        unloadPanelFromMachineWithoutClearingAbortOrSettingLightStack(testResultsEnum);
        
//        _lightStack.hardwareInUseMode();
      }
      catch (XrayTesterException ex)
      {
        handleXrayTesterException(ex);
      }
      catch (RuntimeException rte)
      {
        handleRuntimeException(rte);
      }
      finally
      {
        setLightStackOnExit();
//          --_unloadEntryPointCount;
//          // if this method was the unload entry point, then this method should notify that the panel has been unloaded
//          if (_unloadEntryPointCount == 0)
//          {
//            _panelHasBeenUnloaded = true;
        _panelUnloadedLock.notifyAll();
//          }
//        }
      }

    }
  }

  /**
   * @author Bill Darbie
   */
  private void unloadPanelFromMachineWithoutClearingAbortOrSettingLightStack(TestResultsEnum testResultsEnum) throws XrayTesterException
  {
    Assert.expect(testResultsEnum != null);

    if (_unloadException != null)
      throw _unloadException;

    synchronized (_panelUnloadedLock)
    {
      if (_panelHandler.isPanelLoaded() == false)
      {
        _panelUnloadedLock.notifyAll();
        return;
      }
      try
      {
//        ++_unloadEntryPointCount;
        _panelWillBeUnloaded = true;

        if (_panelHandler.isPanelLoaded())
        {
          getMotionControlHardwareReadyIfNecessary();

          PanelUnloadingFromMachineInspectionEvent unloadingEvent = new PanelUnloadingFromMachineInspectionEvent();
          _inspectionEventObservable.sendEventInfo(unloadingEvent);
          if (testResultsEnum.equals(TestResultsEnum.PASSED))
            _manufacturingEquipmentInterface.setPanelOutPassed();
          else if (testResultsEnum.equals(TestResultsEnum.FAILED))
            _manufacturingEquipmentInterface.setPanelOutFailed();
          else if (testResultsEnum.equals(TestResultsEnum.NOT_TESTED))
            _manufacturingEquipmentInterface.setPanelOutUntested();
          else
            Assert.expect(false, "Unhandled TestResultsEnum" + testResultsEnum);

          // Make sure a panel still needs to be unloaded
          if (_panelHandler.isPanelLoaded())
          {
            // use standby since we are waiting for the user to unload the panel from the machine
            _lightStack.standyMode();

            _testExecutionTimer.startPanelUnloadTimer();
            
            //Khaw Chek Hau - XCR2654: CAMX Integration
            if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
            {
              try
              {
                _shopFloorSystem.unloadPanel();
                _shopFloorSystem.equipmentUnStarved();
              }
              catch (DatastoreException ex)
              {
                DatastoreException datastoreException = (DatastoreException) ex;
                MessageInspectionEvent event = new ShopFloorMessageInspectionEvent(datastoreException);
                InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
                inspectionEventObservable.sendEventInfo(event);
              }
            }
            // Kok Chun, Tan - XCR-2766 - Panel clear extra delay is not take affect if board is loaded
            if (Project.isCurrentProjectLoaded() && 
                (_panelHandler.getLoadedPanelProjectName().equalsIgnoreCase(Project.getCurrentlyLoadedProject().getName())))
            {
              _panelHandler.unloadPanel(Project.getCurrentlyLoadedProject().getPanelExtraClearDelayInMiliSeconds());
            }
            else
            {
              _panelHandler.unloadPanel();
            }
            
            
            _testExecutionTimer.stopPanelUnloadTimer();
//            if (UnitTest.unitTesting() == false)
//            {
//              long time = _testExecutionTimer.getElapsedPanelUnloadTimeInMillis();
//              System.out.println("wpd panel unload time: " + time);
//            }

            _lightStack.hardwareInUseMode();
          }

        }
      }
      finally
      {
//        --_unloadEntryPointCount;
//        // if this method was the unload entry point, then this method should notify that the panel has been unloaded
//        if (_unloadEntryPointCount == 0)
//        {
          _panelUnloadedLock.notifyAll();
//        }
      }

      PanelUnloadedFromMachineInspectionEvent unloadedEvent = new PanelUnloadedFromMachineInspectionEvent();
      _inspectionEventObservable.sendEventInfo(unloadedEvent);
    }
  }

  /**
   * Simulates the ejection of the panel by repositioning it to the opposite PIP and back.
   *
   * @author Matt Wharton
   */
  private void simulatePanelEject(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(_panelHandler != null);

    if (testProgram.hasLeftProgram() == false)
    {
      _panelHandler.movePanelToLeftSide();
      _panelHandler.movePanelToRightSide();
    }
    else
    {
      _panelHandler.movePanelToRightSide();
      _panelHandler.movePanelToLeftSide();
    }
  }

  /**
   * @return true if a panel is already loaded in the machine
   * @author Bill Darbie
   */
  public boolean isPanelLoaded(boolean hasHighMagnification, String projectName) throws XrayTesterException
  {
    // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
    //  change to throws XrayTesterException
    boolean isPanelLoaded = _panelHandler.isPanelLoaded();
    
    //Check If Need To Eject Panel For Variable Mag if Panel was loaded
    if (isPanelLoaded) 
    {
      if(hasHighMagnification && _panelHandler.getPassedSafetyLevelSensorScan() == false)
      {
         throw new VariableMagSubtypeSettingChangeWithPanelLoadedInTesterBusinessException(projectName);
      }
    }
    return isPanelLoaded;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void waitPreScanFinished() 
  {
    try
    {
        _isPreScanRunning.waitUntilFalse();
    }
    catch(InterruptedException ex)
    {
       //do nothing.
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isPreScanBarCodeScannerRunning()
  {
    return _isPreScanRunning.isTrue();
  }
  
  /*
   * @author Kee Chin Seong
   */
  public List<String> getPreScanSerialNumbers()
  {
     Assert.expect(_preScanBarcodeScanner != null);
     
     return _preScanBarcodeScanner;
  }

  /**
   * @author Bill Darbie
   */
  public List<String> getSerialNumbers() throws XrayTesterException
  {
    //Siew Yeng - XCR1726 - Support for multi-drop barcode reader setup
    if(_barcodeReaderManager.getBarcodeReaderScanMethod().equals(BarcodeReaderScanMethodEnum.SCAN_DURING_BOARD_LOADING.toString()))
    {
      if (_userAborted)
        return new ArrayList<String>();
      
      return _barcodeReaderManager.getSerialNumbersWhenBoardLoading();
    }

    if (_panelHandler.isInLineModeEnabled())
      _manufacturingEquipmentInterface.waitUntilPanelFromUpstreamSystemIsAvailable(0, true);
   
    if (_userAborted)
      return new ArrayList<String>();

    return _barcodeReaderManager.getSerialNumbers();
  }

  /**
   * @author Bill Darbie
   */
  private void panelInspectionBegin(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    boolean needToUseHardware = _imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS) ||
        (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_WHEN_NECESSARY) &&
          _inspectionEngine.needToUseHardwareToGenerateTemporaryOnlineImageSet(testProgram));

    if (needToUseHardware)
    //if(false) //For Speed
      Assert.expect(_panelHandler.isPanelLoaded());

    // this gets called once per panel inspection, no matter how many boards per panel
    inspectionBeginInit(testProgram, needToUseHardware);
    //setInspectionInProgress(true);
  }

  /**
   * This method gets called once at the beginning of a panel inspection.  A panel with
   * multiple boards will only see this called one time.
   * @author Bill Darbie
   */
  private void inspectionBeginInit(TestProgram testProgram, boolean needToUseHardware) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    if (needToUseHardware)
      _lightStack.hardwareInUseMode();

    _testResultsEnum = TestResultsEnum.NOT_TESTED;

    if (_entryPointIsTestPanel)
    {
      // testPanel() already did a _testExecutionTimer.reset()
    }
    else
    {
      _testExecutionTimer.reset();
    }
    _testExecutionTimer.startFullInspectionTimer();

    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.START_FULL_INSPECTION);

//    if (needToUseHardware)
//      _stageLight.off();

    // check if the hardware is ready to run an inspection
    if (needToUseHardware)
      getHardwareStartedUpIfNecessary();
  }

  /**
   * @author Bill Darbie
   */
  private void getMotionControlHardwareReadyIfNecessary() throws XrayTesterException
  {
    if (_xrayTester.isStartupRequiredForMotionRelatedHardware())
    {
      LocalizedString localizedString = new LocalizedString("TESTEXEC_MOTION_HARDWARE_STARTUP_KEY",
                                                             new Object[]{});
      MessageInspectionEvent event = new MessageInspectionEvent(localizedString);
      _inspectionEventObservable.sendEventInfo(event);
      _testExecutionTimer.startMotionStartupTimer();
      _xrayTester.startupMotionRelatedHardware();
      _testExecutionTimer.stopMotionStartupTimer();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void getHardwareStartedUpIfNecessary() throws XrayTesterException
  {
    LocalizedString localizedString = new LocalizedString("TESTEXEC_HARDWARE_STARTUP_KEY",
                                                           new Object[]{});
    MessageInspectionEvent event = new MessageInspectionEvent(localizedString);
    _inspectionEventObservable.sendEventInfo(event);

    _testExecutionTimer.startAllHardwareStartupTimer();
    _xrayTester.startup();
    _testExecutionTimer.stopAllHardwareStartupTimer();
  }

  /**
   * @author Bill Darbie
   * @author Patrick Lacz
   */
  private void panelInspectionEnd()
  {
    _entryPointIsTestPanel = false;

    // now we are done with the entire inspection
    _testExecutionTimer.stopFullInspectionTimer();

    _performanceLog.logMilestone(PerformanceLogMilestoneEnum.END_FULL_INSPECTION);

    if (UnitTest.unitTesting() == false)
    {
      long fullInspectionTimeInMs = _testExecutionTimer.getElapsedFullInspectionTimeInMillis();
      int totalNumberOfJoints = _inspectionEngine.getNumberOfProcessedJoints();
      float overallJointsPerSecond = (float)totalNumberOfJoints / (fullInspectionTimeInMs / 1000.f);
      System.out.println("Overall Inspection Time (" + totalNumberOfJoints + " joints processed over " + fullInspectionTimeInMs +
                         " ms) : " + overallJointsPerSecond + " joints per second.");
      /** @todo PWL It would be nice to indicate how many threads were used to avoid confusion reconciling these numbers with
       * the individual algorithm/inspection family times. */
    }

    if (isInspectionInProgress())
    {
      // copy the Timer class and send it up as an event to the GUI
      TestExecutionTimer timer = new TestExecutionTimer(_testExecutionTimer);
      // Post a message that the inspection is complete.
      InspectionEvent inspectionCompletedEvent = new CompletedInspectionEvent(timer,
                                                                              _inspectionEngine.getNumberOfProcessedJoints(),
                                                                              _inspectionEngine.getNumberOfDefectiveJoints(),
                                                                              _inspectionEngine.getNumberOfDefectiveComponents());
      _inspectionEventObservable.sendEventInfo(inspectionCompletedEvent);
    }
    setInspectionInProgress(false);

    MaskImage.getInstance().clearMaskImageMap();
  }

  /**
   * @author Bill Darbie
   */
  public Map<String, List<String>> getSerialNumbersUsedInLastPanelTest() throws HardwareException
  {
    Assert.expect(_serialNumbersUsedInLastPanelTest != null);

    return _serialNumbersUsedInLastPanelTest;
  }

  /**
   * Test a panel.  In production mode, this means that the panel will be loaded into the machine, the alignment and surface
   * mapping will be run, and the inspection of all boards will be done.  When the test is complete the
   * panel will be unloaded from the machine.
   * @author Bill Darbie
   * @edited Kee Chin Seong
   */
  // production mode always
  public void testPanelForProduction(TestProgram testProgram, String panelSerialNumber, List<Pair<String, String>> boardNameToSerialNumbers) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(panelSerialNumber != null);
    Assert.expect(boardNameToSerialNumbers != null);

    //sheng chuan, move this function before state changed to reset all confirmation task abort status
    clearExitAndAbortFlags();
    //XCR-3797, Failed to cancel CDNA task after start up
    getHardwareStartedUpIfNecessary();
    if (didUserAbort())
      return;
    _hardwareObservable.stateChangedBegin(this, TestExecutionEventEnum.INSPECT_FOR_PRODUCTION);
    _hardwareObservable.setEnabled(false);

    boolean boardSerialNumberSkipped = false;
    boolean runInspection = true;
    
    try
    {
      boolean isCreatingOnlyInspectedSlices = testProgram.isCreateOnlyInspectedSlicesEnabled();
      testProgram.clearFilters();
      testProgram.addFilter(true);
      testProgram.setUseOnlyInspectedRegionsInScanPath(true);
      
      if(testProgram.isRerunningProduction() == false)
      {
        //Khaw Chek Hau - XCR3578: Assert when skip serial number for board based alignment
        for (Board board : testProgram.getProject().getPanel().getBoards())
        {
          board.resetNumberOfFailedJointsAndComponents();
        }
      }
      
      if(_productionTuneManager.isProjectEnabledForProductionTune(testProgram.getProject().getName()))
      {
        //debug("ProductionFlow: Project is enabled: " + testProgram.getProject().getName());
        if(isCreatingOnlyInspectedSlices)
          _imageAcquisitionEngine.setHardwareNeedsToBeReprogrammed();
        testProgram.setCreateOnlyInspectedSlicesFilter(false);
      }
      else
      {
        //debug("ProductionFlow: Project is NOT enabled: " + testProgram.getProject().getName());
        if(isCreatingOnlyInspectedSlices == false)
          _imageAcquisitionEngine.setHardwareNeedsToBeReprogrammed();
         testProgram.setCreateOnlyInspectedSlicesFilter(true);
      }

      _panelWillBeUnloaded = false;
      _preparedToUnloadPanel = false;
      _atLeastOneInspectionFailure = false;
      _imageAcquisitionComplete = false;

      Map<String, String> boardNameToSerialNumberMap = new HashMap<String, String>();
      List<String> serialNumbers = new ArrayList<String> ();
      _serialNumbersUsedInLastPanelTest.clear();
      for (Pair<String, String> pair : boardNameToSerialNumbers)
      {
        serialNumbers.add(pair.getSecond());
        String prev = boardNameToSerialNumberMap.put(pair.getFirst(), pair.getSecond());
        Assert.expect(prev == null);
      }
      _serialNumbersUsedInLastPanelTest.put(testProgram.getProject().getSelectedVariationName(), serialNumbers);
      _testResultsEnum = TestResultsEnum.NOT_TESTED;

      _entryPointIsTestPanel = true;
   
      //Khaw Chek Hau - XCR2662: Production inspection time only display for misalignment board   
      if(testProgram.isRerunningProduction() == false)
      {
        _testExecutionTimer.reset();
        //Khaw Chek Hau - XCR3774: Display load & unload time during production
        _panelHandler.setProductionPanelLoadingTimeInMilis(0);
        _panelHandler.setProductionPanelUnloadingTimeInMilis(0);
      }

      if (didUserAbort())
        return;

      _testExecutionTimer.stopBeatRateTimer();
      if (UnitTest.unitTesting() == false)
      {
        long time = _testExecutionTimer.getElapsedBeatRateTimeInMillis();
        System.out.println("\n\n #######  wpd beat rate time in ms: " + _testExecutionTimer.getElapsedBeatRateTimeInMillis() + "\n\n");
      }
      
      //XCR1726 - Support for multi-drop barcode reader setup
      //Siew Yeng - board already loaded if scan method is scan during board loading
      if(_barcodeReaderManager.getBarcodeReaderAutomaticReaderEnabled() == false ||
        (_barcodeReaderManager.getBarcodeReaderAutomaticReaderEnabled() && 
        _barcodeReaderManager.getBarcodeReaderScanMethod().equals(BarcodeReaderScanMethodEnum.SCAN_BEFORE_BOARD_LOAD.toString())))
      {
        _testExecutionTimer.resetBeatRateTimer();
        _testExecutionTimer.startBeatRateTimer();
      
        boolean needToEnableSafetyLevelSensorForPanelLoading = checkNeedToEnableSafetyLevelSensorForVariableMag(testProgram);
        //_fastSpeedEnabled =true;
        _fastSpeedEnabled =false;
        if( _fastSpeedEnabled)
        {
           _fastSpeedEnabled_TestProgram = testProgram;
           _fastSpeedEnabled_IsVirtualLive = false;
           _fastSpeedEnabled_NeedToEnableSafetyLevelSensorForPanelLoading = needToEnableSafetyLevelSensorForPanelLoading ;
        }
        else
        {
          loadPanelIntoMachineWithoutClearingSerialNumbers(testProgram, false, needToEnableSafetyLevelSensorForPanelLoading);
        }
      }
      
      _lightStack.hardwareInUseMode();

      if (didUserAbort())
        return;

      // check the user supplied script for any returned errors or warnings
      if (_scriptRunner.isPreInspectionScriptEnabled())
      {
        String scriptErrorMessage = _scriptRunner.getErrorMessage();
        if (scriptErrorMessage.equals("") == false)
          throw new PreInspectionScriptErrorBusinessException(_scriptRunner.getPreInspectionScript(), scriptErrorMessage);

        String scriptWarningMessage = _scriptRunner.getWarningMessage();
        if (scriptWarningMessage.equals("") == false)
        {
          String script = _scriptRunner.getPreInspectionScript();
          LocalizedString localizedString = new LocalizedString("TESTEXEC_SCRIPT_WARNING_KEY",
                                                                new Object[] {script, scriptWarningMessage});
          MessageInspectionEvent event = new MessageInspectionEvent(localizedString);
          _inspectionEventObservable.sendEventInfo(event);
        }
      }
      
      //Khaw Chek Hau - XCR2286: Improvement of V810 "SKIP" Function for Panel Based Alignment
      boolean isAllBoardsSkipped = false;
      int totalSkippedBoard = 0;
      
      if (_serialNumberManager.isUseUniqueSerialNumberPerBoardEnabled() == false)
      {
        if( _serialNumberManager.shouldSerialNumberBeSkipped(panelSerialNumber))
        {
          isAllBoardsSkipped = true;
        }
      }
      else
      {
        for(Map.Entry<String,String> entry : boardNameToSerialNumberMap.entrySet())
        {
          String boardSerialNumber = entry.getValue().toString();
          if( _serialNumberManager.shouldSerialNumberBeSkipped(boardSerialNumber))
          {
            totalSkippedBoard ++;
          }
        }
        
        if (boardNameToSerialNumberMap.entrySet().size() == totalSkippedBoard)
        {
          isAllBoardsSkipped = true;  
        }
      }      
      
      runInspection = true;
      boolean samplingMode = false;
      String notInspectedReason = "yes";
      // decide if we should inspect or not
      if (_sampling.testPanel() == false)
      {
        // we are skipping this panel because of panel sampling
        LocalizedString localizedString = new LocalizedString("TESTEXEC_SAMPLING_SKIPPING_PANEL_KEY",
                                                              new Object[] {panelSerialNumber});
        MessageInspectionEvent event = new MessageInspectionEvent(localizedString);
        _inspectionEventObservable.sendEventInfo(event);
        runInspection = false;
        notInspectedReason = "no - panel sampling";
        samplingMode = true;
      }
      else if (_scriptRunner.inspectPanel() == false)
      {
        // we are skipping this panel because of the user defined preInspectionScript told us to
        String script = _scriptRunner.getPreInspectionScript();
        LocalizedString localizedString = new LocalizedString("TESTEXEC_SCRIPT_SKIPPING_PANEL_KEY",
                                                              new Object[] {panelSerialNumber, script});
        MessageInspectionEvent event = new MessageInspectionEvent(localizedString);
        _inspectionEventObservable.sendEventInfo(event);
        runInspection = false;
        notInspectedReason = "no - user defined preInspectionScript";
      }
      //Khaw Chek Hau - XCR2286: Improvement of V810 "SKIP" Function for Panel Based Alignment
      else if (_serialNumberManager.shouldSerialNumberBeSkipped(panelSerialNumber) && isAllBoardsSkipped)
      {
        // this panel should be skipped because its serial number is in the file of serial
        // numbers that should be skipped
        LocalizedString localizedString = new LocalizedString("TESTEXEC_PANEL_SKIPPED_BECAUSE_SERIAL_NUMBER_SKIP_KEY",
                                                              new Object[] {panelSerialNumber});
        MessageInspectionEvent event = new MessageInspectionEvent(localizedString);
        _inspectionEventObservable.sendEventInfo(event);
        runInspection = false;
        notInspectedReason = "no - serial number skip file";
      }
      
      // XCR1597 by Lee Herng - Run production will hang if skip serial number and set multi gain (gain1 and 2) for multiboard
      List<String> skipBoardNames = new ArrayList<String>();
      List<String> runBoardNames = new ArrayList<String>();
      for(Map.Entry<String,String> entry : boardNameToSerialNumberMap.entrySet())
      {
        String boardName = entry.getKey().toString();
        String boardSerialNumber = entry.getValue().toString();
        if( _serialNumberManager.shouldSerialNumberBeSkipped(boardSerialNumber))
        {
          skipBoardNames.add(boardName);
          System.out.println(entry.getValue() + " is Skipped for board " + boardName);
          boardNameToSerialNumberMap.remove(entry);
          boardSerialNumberSkipped = true;
        }
        else
        {
          //Khaw Chek Hau - XCR2286: Improvement of V810 "SKIP" Function for Panel Based Alignment        
          runBoardNames.add(boardName);  
        }
      }
      
      // XCR1597 by Lee Herng - Run production will hang if skip serial number and set multi gain (gain1 and 2) for multiboard
      //Khaw Chek Hau - XCR2286: Improvement of V810 "SKIP" Function for Panel Based Alignment
      if (boardSerialNumberSkipped)
      {        
        testProgram.filterBoard(runBoardNames);
        testProgram.invalidateCheckSum();
        
        testProgram.clearTotalReconstructionRegionToSubProgramMap();
        testProgram.createTotalReconstructionRegionToSubProgramMap();
      }

      //Skip First Board By Kee Chin Seong - Because first board might have been taken out 
      //skip board, so panel serial number has to take next board serial number as 
      //panel barcode.
      //Khaw Chek Hau - XCR2286: Improvement of V810 "SKIP" Function for Panel Based Alignment
      if(_serialNumberManager.shouldSerialNumberBeSkipped(panelSerialNumber))
      {
        for(Map.Entry<String,String> entry : boardNameToSerialNumberMap.entrySet())
        {
          if(_serialNumberManager.shouldSerialNumberBeSkipped(entry.getValue()) == false)
          {
            panelSerialNumber = entry.getValue();
            break;
          }
        }
      }

      if (didUserAbort())
        return;

      if (runInspection)
      {
        // OKAY - NOW LETS INSPECT THE PANEL!
        _unloadException = null;

        if (didUserAbort())
          return;

        // this call will block until the entire inspection is complete including all image analysis

        /** @todo apm -- this is NOT the right place for this, but inserted for testing only */
        String projectName = testProgram.getProject().getName();
//        if (ProductionTuneManager.getInstance().isProjectEnabledForProductionTune(projectName))
//          debug("Production Tuning is ENABLED for: " + projectName);
//        else
//          debug("Production Tuning is NOT enabled for: " + projectName);

        inspectPanelForProduction(testProgram, panelSerialNumber, boardNameToSerialNumberMap);

        _productionTuneManager.productionWasRun(projectName, panelSerialNumber, _inspectionEngine.getNumberOfDefectiveJoints());

        if (_inspectionEngine.getNumberOfDefectiveJoints() == 0 && _inspectionEngine.getNumberOfDefectiveComponents() == 0)
          _testResultsEnum = TestResultsEnum.PASSED;
        else
          _testResultsEnum = TestResultsEnum.FAILED;

        if (_userAborted)
          return;
        
        /*
         * @PreScan in the barcode. - Could be not Delay if needed
         */
        if(_panelHandler.isInLineModeEnabled() == true && 
           _barcodeReaderManager.getBarcodeReaderAutomaticReaderEnabled() == true)
        {
           _parallelPanelUnloadWorkerThread.invokeLater(new Runnable()
           {
              public void run()
              {
                try
                {
                  _isPreScanRunning.setValue(true);
                  _preScanBarcodeScanner.clear();
  
                  _preScanBarcodeScanner = getSerialNumbers();
                  _isPreScanRunning.setValue(false);
                }
                catch (XrayTesterException xte)
                {
                  //Siew Yeng - XCR-3083 - Failed to read barcode if connection is interrupted and then recover back
                  _isPreScanRunning.setValue(false);
                  System.out.println("PRE-SCAN : Exception Caught " + xte.getMessage());
                  _unloadException = xte;
                }
              }
           });
        }

        String currentXMLResultFullPath = _resultsXmlWriter.getCurrentXMLResultPath();
        _scriptRunner.runPostInspectionScript(testProgram.getProject(), panelSerialNumber, boardNameToSerialNumberMap, currentXMLResultFullPath);
        
        // XCR-3680 Defective pins has exceeded the maximum number of error allowed is prompted even the production is aborted
        if (_userAborted == false && _config.getBooleanValue(SoftwareConfigEnum.FALSE_CALL_MONITORING_ENABLED) == true)
        {
           FalseCallMonitoring.getInstance().handleFalseCallTriggering(_inspectionEngine.getNumberOfProcessedJoints(), _inspectionEngine.getNumberOfDefectiveJoints());
        }
      }
      else
      {
        // lets open the xml writer now - we need to open it here because it will report that we have
        // skipped the inspection and why
        long inspectionStartTime = System.currentTimeMillis();
        _resultsXmlWriter.setProductionMode(true);
        _resultsXmlWriter.setSamplingMode(samplingMode);
        _resultsXmlWriter.open(testProgram.getProject(),
                               inspectionStartTime,
                               panelSerialNumber,
                               boardNameToSerialNumberMap,
                               notInspectedReason);
        _resultsXmlWriter.close(inspectionStartTime);
        _resultsXmlWriter.setSamplingMode(false);
      }
      
      //Added by Jack, Add in a delay for False Call Triggering mode before unloading board from machine.
      // This is to make sure this mode has been triggered before panel is unload directly after inspection.
      if (_config.getBooleanValue(SoftwareConfigEnum.FALSE_CALL_MONITORING_ENABLED) == true)
      {
        if (FalseCallMonitoring.getInstance().isFalseCallTriggeringTriggered())
        {
          return;
        }
      }

      if (_userAborted)
        return;

      if (isPanelEjectOverrideOn() == false)
      {      
        if ( _panelHandler.isEnabledKeepOuterBarrierOpenForLoadingAndUnloadingPanel() && 
              PanelHandler.isInLineModeEnabled() && 
             _panelHandler.getPanelLoadingMode().equals(PanelHandlerPanelLoadingModeEnum.PASS_BACK))          
        {
          _panelHandler.setKeepOuterBarrierOpenAfterUnloadingPanel(true); 
        }
        else
        {
          _panelHandler.setKeepOuterBarrierOpenAfterUnloadingPanel(false);
        }
        unloadPanelFromMachineIfAbortIsNotInProgress(_testResultsEnum);
      }
      else if (isSimulatePanelEjectOn())
      {
        simulatePanelEject(testProgram);
      }

      _lightStack.standyMode();
    }
    catch (XrayTesterException ex)
    {
      _panelHandler.setKeepOuterBarrierOpenForLoadingPanel(false);
      if (ex instanceof AlignmentBusinessException)
      {
        AlignmentBusinessException alignmentException = (AlignmentBusinessException)ex;

        // send up a message telling the GUI that we had an alignment failure
        MessageInspectionEvent event = new AlignmentFailedMessageInspectionEvent(alignmentException);
        _inspectionEventObservable.sendEventInfo(event);

        if (shouldPanelBeEjectedIfAlignmentFailsRespectingOverrides() == false)
          handleXrayTesterException(alignmentException);
        else
        {
          try
          {
            // CR1045 fix by LeeHerng - Add in a delay before unloading board from machine.
            // This is to make sure all the scan pass during Alignment are done before
            // triggering point-to-point move.
            Thread.sleep(1000);
            unloadPanelFromMachineWithoutClearingAbortOrSettingLightStack(TestResultsEnum.NOT_TESTED);
          }
          catch (XrayTesterException ex2)
          {
            handleXrayTesterException(ex2);
          }
          catch (InterruptedException ie)
          {
              // Do nothing
          }
        }
      }
      else
        handleXrayTesterException(ex);
    }
    catch (RuntimeException rte)
    {
      _panelHandler.setKeepOuterBarrierOpenForLoadingPanel(false);
      handleRuntimeException(rte);
    }
    finally
    {
      //Khaw Chek Hau - XCR3774: Display load & unload time during production
      InspectionEvent completedProductionResultStatusEvent = new CompletedProductionResultStatusEvent();
      _inspectionEventObservable.sendEventInfo(completedProductionResultStatusEvent);
      
      if (_panelWillBeUnloaded)
      {
        // this method cannot exit until the _panelHasBeenUnloaded variable is set
        // if it did an assert will happen because the thread in the update() method of this
        // class could finish before this method exits
        synchronized (_panelUnloadedLock)
        {
          while ((_panelHandler.isPanelLoaded()) && (_unloadException == null))
          {
            try
            {
              _panelUnloadedLock.wait(200);
            }
            catch (InterruptedException ie)
            {
              // do nothing
            }
          }
        }
      }

      setLightStackOnExit();
      _hardwareObservable.setEnabled(true);

      if(boardSerialNumberSkipped == true)
      {
        boardSerialNumberSkipped = false;
        
        testProgram.clearFilters();
        
        //Chin-seong,Kee - MUST invalidate to make sure IRP download the latest test Program with Skip serial number
        testProgram.invalidateCheckSum();
        
        testProgram.clearTotalReconstructionRegionToSubProgramMap();
        testProgram.createTotalReconstructionRegionToSubProgramMap();
      }

      // since we are in a finally block, we must do this call AFTER the _hardwareObservable.setEnabled(true)
      if (_unloadException !=  null)
        handleXrayTesterException(_unloadException);
    }
    _hardwareObservable.stateChangedEnd(this, TestExecutionEventEnum.INSPECT_FOR_PRODUCTION);
  }

  /**
   * Test a panel while acquiring a precision tuning image set. The panel will not be loaded or unloaded
   * from the system, but a full production mode inspection, including surface model and focus confirmation
   * will be run.  This provides a more complete image set(e.g. predictive slice height components such as
   * pressfits can be included) with more accurate slice heights.
   *
   * @author Bill Darbie
   * @author John Heumann
   */
  public void testPanelForPrecisionTuningImageAcquisition(
      TestProgram testProgram, ImageSetData imageSetData) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(imageSetData != null);

    XrayTesterException lastXrayTesterException = null;
    RuntimeException lastRuntimeException = null;
    clearExitAndAbortFlags();
    //XCR-3797, Failed to cancel CDNA task after start up
    getHardwareStartedUpIfNecessary();
    if (didUserAbort())
      return;
    _hardwareObservable.stateChangedBegin(this, TestExecutionEventEnum.ACQUIRE_OFFLINE_IMAGES);
    _hardwareObservable.setEnabled(false);

    ImageGenerationObservable imageGenerationObservable = ImageGenerationObservable.getInstance();
    try
    {
      imageGenerationObservable.stateChanged(new ImageGenerationEvent(ImageGenerationEventEnum.BEGIN_ACQUIRE_OFFLINE_IMAGES));

      testProgram.addFilter(true);
      testProgram.setUseOnlyInspectedRegionsInScanPath(true);

      // Save all "normal" slices... not just those currently being inspected
      if (testProgram.isCreateOnlyInspectedSlicesEnabled())
        _imageAcquisitionEngine.setHardwareNeedsToBeReprogrammed();
      testProgram.setCreateOnlyInspectedSlicesFilter(false);
      // Don't generate images for the extra slices used for retesting unfocused regions
      testProgram.setSlicesForRetestingUnfocusedRegionsEnabled(false);
      
      if (didUserAbort())
        return;
      
      boolean hasHighMagComponent = containHighMagInspectionInCurrentLoadedProject();
      if (isPanelLoaded(hasHighMagComponent, testProgram.getProject().getName()) == false)
        loadPanelIntoMachine(testProgram, false);
      else
      {
        checkThatCurrentlyLoadedPanelMatchesProjectPanelSize(testProgram);
        checkThatCurrentlyLoadedPanelProjectNameMatchesProjectName(testProgram);
      }

      _panelWillBeUnloaded = false;
      _atLeastOneInspectionFailure = false;
      _imageAcquisitionComplete = false;
      _serialNumbersUsedInLastPanelTest.clear();
      _testResultsEnum = TestResultsEnum.NOT_TESTED;
      _entryPointIsTestPanel = true;
      _testExecutionTimer.reset();

      if (didUserAbort())
        return;

      _testExecutionTimer.stopBeatRateTimer();
      if (UnitTest.unitTesting() == false)
      {
        long time = _testExecutionTimer.getElapsedBeatRateTimeInMillis();
        System.out.println("\n\n #######  wpd beat rate time in ms: " + _testExecutionTimer.getElapsedBeatRateTimeInMillis() + "\n\n");
      }
      _testExecutionTimer.resetBeatRateTimer();
      _testExecutionTimer.startBeatRateTimer();
      _lightStack.hardwareInUseMode();

      if (didUserAbort())
        return;

      // Make sure the panel inspection settings are in the correct state before we start.
      PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
      panelInspectionSettings.clearAllInspectionSettings();
      panelInspectionSettings.setInspectedSubtypes(testProgram.getProject().getPanel());
      _inspectionEngine.setPanelInspectionSettings(panelInspectionSettings);

      _imageGeneration.initializeOfflineImageDirectory(testProgram, imageSetData);

      // OKAY - NOW LETS INSPECT THE PANEL!
      _unloadException = null;

      if (didUserAbort())
        return;

      // this call will block until the entire inspection is complete including all image analysis
      inspectPanelForPrecisionTuningImageAcquisition(testProgram, imageSetData);

      if (_inspectionEngine.getNumberOfDefectiveJoints() == 0)
        _testResultsEnum = TestResultsEnum.PASSED;
      else
        _testResultsEnum = TestResultsEnum.FAILED;

      if (_userAborted)
        return;

      _lightStack.standyMode();
    }
    catch (XrayTesterException ex)
    {
      handleXrayTesterException(ex);
    }
    catch (RuntimeException rte)
    {
      handleRuntimeException(rte);
    }
    catch (Throwable throwable)
    {
      _exceptionExit = true;
      Assert.logException(throwable);
    }
    finally
    {
      if (_userAborted || _exceptionExit)
      {
        try
        {
          _imageManager.deleteImages(imageSetData);
        }
        catch (XrayTesterException ex)
        {
          if (lastXrayTesterException != null || lastRuntimeException != null)
            ex.printStackTrace(); // print this exception, but trhow earlier, probably more serious one
          else
            lastXrayTesterException = ex; // will be thrown at the end of this finally block
        }
        catch (Throwable throwable)
        {
          _exceptionExit = true;
          Assert.logException(throwable);
        }
      }

      testProgram.setSlicesForRetestingUnfocusedRegionsEnabled(true);
      setLightStackOnExit();
      imageGenerationObservable.stateChanged(new ImageGenerationEvent(ImageGenerationEventEnum.END_ACQUIRE_OFFLINE_IMAGES));
      _hardwareObservable.setEnabled(true);

      if (lastXrayTesterException != null)
        throw lastXrayTesterException;
      else if (lastRuntimeException != null)
        throw lastRuntimeException;
    }
    _hardwareObservable.stateChangedEnd(this, TestExecutionEventEnum.ACQUIRE_OFFLINE_IMAGES);
  }

  /**
   * @author Bill Darbie
   */
  public void update(Observable observable, Object arg)
  {
// wpd - with SMEMA on, if a panel that ejects while inspection is still happening,
//       then the door does not close, the quick fix is to comment this out
//
//    synchronized (_updateLock)
//    {
      Assert.expect(arg != null);
      if (isPanelEjectOverrideOn())
        return;

      // The code below was originally written to unload a panel from the machine while classification was
      // still happening.
      //
      // Unfortunately, for panels with a large number of joints, the CPU could be so busy classifying images
      // that our polling to see when the panel clear sensor was tripped would not detect the panel at the
      // door soon enough.  This would result in the panel falling out of the system or hitting the outer barrier
      //
      // To fix this we decided to only allow the parallel panel unload when SMEMA is on, because in that case
      // there is always some hardware that the panel will go out on.
      // I kept the code below alone so it handles both the SMEMA and non-SMEMA case, just in case we ever
      // want to change this later, but the if statement below will short circuit all of this if SMEMA is not
      // enabled.
//      if (_panelHandler.isInLineModeEnabled() == false)
//        return;

      if ((_production) && (_panelWillBeUnloaded == false) && (_unloadException == null) && (!_precisionTuningImageAcquisition)/*Add by Khang Wah - 06-oct-2009*/)
      {
        if (_imageAcquisitionComplete == false && arg instanceof AcquiringImagesCompleteInspectionEvent)
        {
          AcquiringImagesCompleteInspectionEvent completeInspectionEvent = (AcquiringImagesCompleteInspectionEvent)arg;
          if (completeInspectionEvent.didInspectionCompleteNormally())
            _imageAcquisitionComplete = true;
        }
        else if (_imageAcquisitionComplete == false && arg instanceof AcquiringOpticalImagesCompleteInspectionEvent)
        {
          AcquiringOpticalImagesCompleteInspectionEvent completeOpticalInspectionEvent = (AcquiringOpticalImagesCompleteInspectionEvent)arg;
          if (completeOpticalInspectionEvent.didInspectionCompleteNormally())
            _imageAcquisitionComplete = true;
        }
        else if (_atLeastOneInspectionFailure == false && arg instanceof JointOrComponentFailureInspectionEvent)
        {
          _atLeastOneInspectionFailure = true;
        }
        else if (arg instanceof CompletedInspectionEvent)
        {
          // this happens after the AcquiringImagesCompleteInspectionEvent and needs to be ignored
          // so we do not try to unload the panel more than once.
          return;
        }
        else if (arg instanceof PanelUnloadedFromMachineInspectionEvent)
        {
          // We are already done with the unload...
          return;
        }

        // determine if we can unload the panel early (scanning is done, but classification is not finished)
        if (_imageAcquisitionComplete)
        {
          _parallelPanelUnloadWorkerThread.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {    
                if (XrayTester.isS2EXEnabled())//for speed up the process at risk
                {
                  //do nothing
                }
                else
                {
                                
                  if (false)//_panelHandler.isInLineModeEnabled() == false)
                  {
                    // no smema, so unload the panel right now
                    if (isPanelEjectOverrideOn() == false)
                    {
                      // since there is no smema it does not matter what the test results are so
                      // send a NOT_TESTED
                      unloadPanelFromMachineIfAbortIsNotInProgress(TestResultsEnum.NOT_TESTED);
                    }
                  }
                  else
                  {
                    // smema, we can only unload after the scan is done and we see at least one failure
  //                  if (_atLeastOneInspectionFailure == false)
  //                  {
                      // we still do not know if the panel is a passing or failing one, so get ready to unload
                      // the panel out of the machine, but do not unload it yet
                      if (isPanelEjectOverrideOn() == false)
                      {
                        prepareToUnloadPanel();
                      }
  //                  }
  //                  else
  //                  {
  //                    // smema needs to know if the panel passed or failed , so unload the panel right now since
  //                    // we already have at least one failure and the scan is complete
  //                    if (isPanelEjectOverrideOn() == false)
  //                      unloadPanelFromMachineIfAbortIsNotInProgress(TestResultsEnum.FAILED);
  //                  }
                  }
                }
              }
              catch (XrayTesterException xte)
              {
                _unloadException = xte;
              }
            }
          });
        }
//      }
    }
  }

  /**
   * @author Bill Darbie
   * @author Rex Shang
   */
  public void prepareToUnloadPanel() throws XrayTesterException
  {      
    if (isPanelEjectOverrideOn() == false)
    {
      synchronized (_panelUnloadedLock)
      {      

        try
        {
            if (_panelHandler.isPanelLoaded() && _preparedToUnloadPanel == false)
            {
              _panelHandler.prepareToUnloadPanel();
              _preparedToUnloadPanel = true;
            }
        }
        finally
        { 
            _panelUnloadedLock.notifyAll();
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private boolean readyToUnloadPanel()
  {
    boolean readyToUnload = false;

    if (((_panelWillBeUnloaded == false) || (_panelHasBeenUnloaded)) &&
        ((_panelWillBePreparedToUnload == false) || (_preparedToUnloadPanel)) ||
        (_unloadException != null))
      readyToUnload = true;

    return readyToUnload;
  }

  /**
   * Simply send a panel into the machine, and immediately send it back out.
   * @author Bill Darbie
   */
  public void bypassPanel(int panelWidthInNanoMeters, 
                          int panelLengthInNanoMeters,
                          boolean bypassWithSerialMode,
                          List<Pair<String, String>> boardNameToSerialNumberPairs) throws XrayTesterException
  {
    Assert.expect(panelWidthInNanoMeters > 0);

    try
    {
      clearExitAndAbortFlags();
      _lightStack.hardwareInUseMode();

      _testExecutionTimer.reset();
      unloadPanelFromMachineWithoutClearingAbortOrSettingLightStack(TestResultsEnum.NOT_TESTED);

      
      do
      {
        if(_userAborted)
          break;
        
        String projectName = _BYPASS_MODE_DEFAULT_PROJECT_NAME;
        if (Project.isCurrentProjectLoaded())
        {
          // Kok Chun, Tan - XCR - 3044 - make sure loaded panel name and project name are same.
          // either 1 of the condition meet
          // condition 1: global bypass mode enable and global bypass mode with recipe enable
          // condition 2: recipe based bypass mode enable, global bypass mode disable and global bypass mode with recipe disable
          if ((isBypassModeEnabled() && isBypassWithSerialControlModeEnabled()) || 
              (Project.getCurrentlyLoadedProject().isProjectBasedBypassMode() && isBypassModeEnabled()== false && isBypassWithSerialControlModeEnabled()==false))
          {
            projectName = Project.getCurrentlyLoadedProject().getName();
          }
        }
        
        _testExecutionTimer.reset();
        long inspectionStartTime = System.currentTimeMillis();
        loadPanelIntoMachine(projectName, panelWidthInNanoMeters, panelLengthInNanoMeters, false, 0);
        unloadPanelFromMachineWithoutClearingAbortOrSettingLightStack(TestResultsEnum.NOT_TESTED);
        long inspectionEndTime = System.currentTimeMillis();
        
        if (isBypassWithResultsEnabled() && Project.isCurrentProjectLoaded())
        {
          // either 1 of the condition meet
          // condition 1: global bypass mode enable and global bypass mode with recipe enable
          // condition 2: recipe based bypass mode enable, global bypass mode disable and global bypass mode with recipe disable
          if ((isBypassModeEnabled() && isBypassWithSerialControlModeEnabled()) ||
              (Project.getCurrentlyLoadedProject().isProjectBasedBypassMode() && isBypassModeEnabled()== false && isBypassWithSerialControlModeEnabled()==false))
          {
            Project project = Project.getCurrentlyLoadedProject();
            Map<String, String> boardNameToSerialNumberMap = new HashMap<String, String>();
            String panelSerialNumber = boardNameToSerialNumberPairs.get(0).getSecond();

            for (Pair<String, String> pair : boardNameToSerialNumberPairs)
            {
              boardNameToSerialNumberMap.put(pair.getFirst(), pair.getSecond());
            }

            _resultsXmlWriter.setProductionMode(true);
            _resultsXmlWriter.setBypassWithResultsMode(true);
            _resultsXmlWriter.open(project,
              inspectionStartTime,
              panelSerialNumber,
              boardNameToSerialNumberMap,
              "no - bypass mode");
            _resultsXmlWriter.close(inspectionEndTime);
            _resultsXmlWriter.setBypassWithResultsMode(false);
          }
        }
      }while (_userAborted == false && bypassWithSerialMode == false);
    }
    catch(XrayTesterException ex)
    {
      handleXrayTesterException(ex);
    }
    catch (RuntimeException rte)
    {
      handleRuntimeException(rte);
    }
    finally
    {
      setLightStackOnExit();
      _testExecutionTimer.stopPanelLoadTimer();
    }
  }

  /**
   * @return true if a "use hardware when necessary" inspection will actually require the hardware; false otherwise.
   * @author Matt Wharton
   */
  public boolean needToUseHardwareToGenerateTemporaryOnlineImageSet(TestProgram testProgram) throws DatastoreException
  {
    Assert.expect(testProgram != null);
    Assert.expect(_inspectionEngine != null);

    return _inspectionEngine.needToUseHardwareToGenerateTemporaryOnlineImageSet(testProgram);
  }

  /**
   * Run an inspection using the hardware to get images.
   *
   * @author Bill Darbie
   */
  public void inspectPanelForTestDevelopment(TestProgram testProgram,
                                             String description) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(description != null);

    //sheng chuan, move this function before state changed to reset all confirmation task abort status
    clearExitAndAbortFlags();
    _hardwareObservable.stateChangedBegin(this, TestExecutionEventEnum.INSPECT);
    _hardwareObservable.setEnabled(false);

    try
    {
      _production = false;
      _precisionTuningImageAcquisition = false;
      _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS;
      _panelSerialNumberOrDescription = description;
      _boardNameToSerialNumberMap = null;
      inspectPanel(testProgram, null, true);
    }
    finally
    {
      _panelInspectionSettings.clearInspectedSubtypes();
      setLightStackOnExit();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, TestExecutionEventEnum.INSPECT);
  }

  /**
   * Run an inspection using image set data
   * @author Bill Darbie
   */
  public void inspectPanelForTestDevelopmentUsingImageSetData(TestProgram testProgram,
                                                              List<ImageSetData> imageSetDataList,
                                                              String description) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(imageSetDataList != null);
    Assert.expect(description != null);

    //sheng chuan, move this function before state changed to reset all confirmation task abort status
    clearExitAndAbortFlags();
    _hardwareObservable.stateChangedBegin(this, TestExecutionEventEnum.INSPECT_USING_IMAGE_SETS);
    _hardwareObservable.setEnabled(false);

    try
    {
      _production = false;
      _precisionTuningImageAcquisition = false;
      _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_IMAGE_SETS;
      _panelSerialNumberOrDescription = description;
      _boardNameToSerialNumberMap = null;

      // Call inspectPanel for each image set.
      for (ImageSetData imageSetData : imageSetDataList)
      {
        if (_userAborted)
        {
          break;
        }

        inspectPanel(testProgram, imageSetData, false);
      }
    }
    finally
    {
      _panelInspectionSettings.clearInspectedSubtypes();
      setLightStackOnExit();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, TestExecutionEventEnum.INSPECT_USING_IMAGE_SETS);

  }

  /**
   * Run an inspection using image set data
   * @author Bill Darbie
   */
  public void inspectPanelForTestDevelopmentUsingHardwareWhenNecessary(TestProgram testProgram,
                                                                       String description) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(description != null);

    boolean goingToUseHardware = _inspectionEngine.needToUseHardwareToGenerateTemporaryOnlineImageSet(testProgram);
    clearExitAndAbortFlags();
    if (goingToUseHardware)
      _hardwareObservable.stateChangedBegin(this, TestExecutionEventEnum.INSPECT);
    else
      _hardwareObservable.stateChangedBegin(this, TestExecutionEventEnum.INSPECT_USING_IMAGE_SETS);
    _hardwareObservable.setEnabled(false);

    try
    {
      _production = false;
      _precisionTuningImageAcquisition = false;
      _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_HARDWARE_WHEN_NECESSARY;
      _panelSerialNumberOrDescription = description;
      _boardNameToSerialNumberMap = null;
      inspectPanel(testProgram, null, goingToUseHardware);
    }
    finally
    {
      _panelInspectionSettings.clearInspectedSubtypes();
      setLightStackOnExit();
      _hardwareObservable.setEnabled(true);
    }
    if (goingToUseHardware)
      _hardwareObservable.stateChangedEnd(this, TestExecutionEventEnum.INSPECT);
    else
      _hardwareObservable.stateChangedEnd(this, TestExecutionEventEnum.INSPECT_USING_IMAGE_SETS);

  }

  /**
   * Run an inspection using the hardware to get images.
   *
   * @author Bill Darbie
   */
  private void inspectPanelForProduction(TestProgram testProgram,
                                        String panelSerialNumber,
                                        Map<String, String> boardNameToSerialNumberMap) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(panelSerialNumber != null);
    Assert.expect(boardNameToSerialNumberMap != null);

    _production = true;
    _precisionTuningImageAcquisition = false;
    _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS;
    _panelSerialNumberOrDescription = panelSerialNumber;
    _boardNameToSerialNumberMap = boardNameToSerialNumberMap;

    inspectPanel(testProgram, null, true);
    _panelInspectionSettings.clearInspectedSubtypes();
  }

  /**
   * Run an inspection using the hardware to acquire a precision tuning image set.
   *
   * @author Bill Darbie
   * @author John Heumann
   */
  private void inspectPanelForPrecisionTuningImageAcquisition(
      TestProgram testProgram,
      ImageSetData imageSetData) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(imageSetData != null);

    _production = true;
    _precisionTuningImageAcquisition = true;
    _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS;
    _panelSerialNumberOrDescription = imageSetData.getUserDescription();
    _boardNameToSerialNumberMap = null;
    _onlineImageSetData = imageSetData;

    inspectPanel(testProgram, imageSetData, true);
    _panelInspectionSettings.clearInspectedSubtypes();
    _onlineImageSetData = null;
  }

  /**
   * Only run the panel inspection.  The panel will not be loaded into the machine and alignment and surface
   * mapping will not be run if they have been run previously.
   *
   * @param testProgram The TestProgram under test.
   * @param imageSetData The ImageSetData to use if we're inspecting with image sets.  This can be 'null'
   * if we're not doing image set based inspections.
   *
   * @author Bill Darbie
   * @author Matt Wharton
   */
  // test development mode always unless called from testPanel
  private void inspectPanel(TestProgram testProgram, ImageSetData imageSetData, boolean needToUseHardware) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

  //add exception if the land pattern overlaping
    if (ProgramVerificationUtil.isProjectHas2PinDeviceLandPatternOverlapping(testProgram.getProject()))
    {
      String data = ProgramVerificationUtil.getOverlapLandPatternNameInString();
      throw new LandPatternOverlappingException(new LocalizedString("EX_LANDPATTERN_OVERLAP_KEY", new Object[]{ data}));
    }
    try
    {
      if (didUserAbort())
          return;

      if (needToUseHardware)
      {
        checkThatCurrentlyLoadedPanelMatchesProjectPanelSize(testProgram);
        checkThatCurrentlyLoadedPanelProjectNameMatchesProjectName(testProgram);
        checkNeedToEjectPanelAndEnableSafetyLevelSensorForVariableMag();
        checkThatManualAlignmentHasBeenPerformed(testProgram);
      }

      if (_entryPointIsTestPanel == false)
      {
        // this method is the entry point - so we are running in test development mode
        _serialNumbersUsedInLastPanelTest = new LinkedHashMap<String, List<String>>();
      }

      if (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS) == false)
      {
        if (_panelHandler.isPanelLoaded() && _digitalIo.arePanelClampsOpened())
        {
          //this shouldn't happen since panel clamps should be closed before reaching this stage unless the loaded board is leftover board
          _digitalIo.closePanelClamps();
          System.out.println("WARNING: Panel clamps are opened before panel inspection begin, "
            + "somewhere in the code is not handled well or the panel inside the machine is leftover panel"
            + ", please reload it for better inspection.");
        }
      }
      
      panelInspectionBegin(testProgram);

      if (didUserAbort())
        return;

// wpd - put this in when xouts are supported
//      if (_xout.isXedOut(boardProgram))
//      {
//        // board is xed out - do not test it
//        LocalizedString localizedString = new LocalizedString("TESTEXEC_PANEL_SKIPPED_BECAUSE_XED_OUT_KEY",
//                                                              new Object[]{serialNumber});
//        MessageTestStatusEvent event = new MessageTestStatusEvent(localizedString);
//        processInspectionEvent(event);
//        _inspectionLogWriter.writeInspectionLogFileForUntestedBoard(boardProgram);
//      }
//      else
//      {

      if ((_production) && (!_precisionTuningImageAcquisition) &&
          (_serialNumberManager.shouldSerialNumberBeSkipped(_panelSerialNumberOrDescription)))
      {
        //Khaw Chek Hau - XCR3774: Display load & unload time during production
        setInspectionInProgress(true);
        // this panel should be skipped because its serial number is in the file of serial
        // numbers that should be skipped
        LocalizedString localizedString = new LocalizedString("TESTEXEC_PANEL_SKIPPED_BECAUSE_SERIAL_NUMBER_SKIP_KEY",
                                                              new Object[] {_panelSerialNumberOrDescription});
        MessageInspectionEvent event = new MessageInspectionEvent(localizedString);
        _inspectionEventObservable.sendEventInfo(event);
      }
      else
      {
        if (didUserAbort())
          return;

        //Khaw Chek Hau - XCR3774: Display load & unload time during production
        setInspectionInProgress(true);
        if (_production)
        {
          if (_debug)
          {
            System.out.println("new Test Program");
            for (TestSubProgram subProgram : testProgram.getAllTestSubPrograms())
            {
              System.out.println("subProgram:" + subProgram.getId());
              System.out.println("isSubProgramPerformAlignment:" + subProgram.isSubProgramPerformAlignment());
              System.out.println("isAllRegionNotIspectable:" + subProgram.isAllRegionNotIspectable());
              System.out.println("getSubProgramRefferenceIdForAlignment:" + subProgram.getSubProgramRefferenceIdForAlignment());
            }
          }
          if (_precisionTuningImageAcquisition)
          {
            // send event that we're collecting a precision image set so all messages in the tuner are supressed
            Assert.expect(_inspectionEventObservable != null);
            InspectionEvent collectingPrecisionImageSetEvent = new InspectionEvent(InspectionEventEnum.BEGIN_COLLECT_PRECISION_IMAGE_SET);
            _inspectionEventObservable.sendEventInfo(collectingPrecisionImageSetEvent);

            _inspectionEngine.inspectForPrecisionTuningImageAcquisition(testProgram, _testExecutionTimer, _onlineImageSetData);
          }
          else // normal production
          {
            _inspectionEngine.inspectForProduction(testProgram, _testExecutionTimer, _panelSerialNumberOrDescription, _boardNameToSerialNumberMap);
          }
        }
        else
        {
          if (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS))
            _inspectionEngine.inspectForTestDevelopment(testProgram, _testExecutionTimer, _panelSerialNumberOrDescription);
          else if (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_HARDWARE_WHEN_NECESSARY))
            _inspectionEngine.inspectForTestDevelopmentUsingHardwareWhenNecessary(testProgram,
                                                                                  _testExecutionTimer,
                                                                                  _panelSerialNumberOrDescription);
          else if (_imageAcquisitionModeEnum.equals(ImageAcquisitionModeEnum.USE_IMAGE_SETS))
          {
            Assert.expect(imageSetData != null);
            _inspectionEngine.inspectForTestDevelopmentUsingImageSetData(testProgram,
                                                                         _testExecutionTimer,
                                                                         imageSetData,
                                                                         _panelSerialNumberOrDescription);
          }
          else
            Assert.expect(false);
        }
      }
    }
    finally
    {
      panelInspectionEnd();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  public void acquireVerificationImagesForManualAlignment(TestSubProgram subProgram, boolean useRFP) throws XrayTesterException
  {
    Assert.expect(subProgram != null);

    Project project = subProgram.getTestProgram().getProject();
        // remove 'areVerificationImagesValid' flag checking as we want v810 to always generate verification images whenever this function is called.

    // Ying-Huan.Chu [XCR1704]
    
//    boolean panelLoadedIntoMachine = isPanelLoaded();

    // since this is for manual alignment, we require a panel in the machine.  If there is NOT one
    // then the verification images are INVALID by definition
//    boolean areVerificationImagesValid;
//    if (panelLoadedIntoMachine == false)
//      areVerificationImagesValid = false;
//    else
//      areVerificationImagesValid = project.areVerificationImagesValid(subProgram);
//
//    if (areVerificationImagesValid == false)
//    {
//      _hardwareObservable.stateChangedBegin(this, TestExecutionEventEnum.ACQUIRE_VERIFICATION_IMAGES);
//      _hardwareObservable.setEnabled(false);
//    }
    
    //sheng chuan, move this function before state changed to reset all confirmation task abort status
    clearExitAndAbortFlags();
    //XCR-3797, Failed to cancel CDNA task after start up
    getHardwareStartedUpIfNecessary();
    if (didUserAbort())
      return;
    _hardwareObservable.stateChangedBegin(this, TestExecutionEventEnum.ACQUIRE_VERIFICATION_IMAGES);
    _hardwareObservable.setEnabled(false);
    
    try
    {
      _production = false;
      _precisionTuningImageAcquisition = false;
      _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS;
      
      if (didUserAbort())
          return;

      boolean hasHighMagComponent = containHighMagInspectionInCurrentLoadedProject();
      if (isPanelLoaded(hasHighMagComponent, subProgram.getTestProgram().getProject().getName() ) == false)
        loadPanelIntoMachine(subProgram.getTestProgram(), false);
      else
      {
        checkThatCurrentlyLoadedPanelMatchesProjectPanelSize(subProgram.getTestProgram());
        checkThatCurrentlyLoadedPanelProjectNameMatchesProjectName(subProgram.getTestProgram());
      }

      if (didUserAbort())
          return;
      
//      if (areVerificationImagesValid == false)
//      {
        _lightStack.hardwareInUseMode();
        project.acquireVerificationImages(subProgram, _testExecutionTimer, false, useRFP);
//      }
    }
    catch (XrayTesterException ex)
    {
      handleXrayTesterException(ex);
    }
    catch (RuntimeException rte)
    {
      handleRuntimeException(rte);
    }
    finally
    {
      setLightStackOnExit();
//      if (areVerificationImagesValid == false)
        _hardwareObservable.setEnabled(true);
    }
//    if (areVerificationImagesValid == false)
      _hardwareObservable.stateChangedEnd(this, TestExecutionEventEnum.ACQUIRE_VERIFICATION_IMAGES);
  }

  /**
   * @author George A. David
   */
  public void alignAndAcquireVerificationImages(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    //sheng chuan, move this function before state changed to reset all confirmation task abort status
    clearExitAndAbortFlags();
    //XCR-3797, Failed to cancel CDNA task after start up
    getHardwareStartedUpIfNecessary();
    if (didUserAbort())
      return;
    _hardwareObservable.stateChangedBegin(this, TestExecutionEventEnum.ACQUIRE_VERIFICATION_IMAGES);
    _hardwareObservable.setEnabled(false);

    try
    {
      _production = false;
      _precisionTuningImageAcquisition = false;
      _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS;

      if (didUserAbort())
        return;

      boolean hasHighMagComponent = containHighMagInspectionInCurrentLoadedProject();
      if (isPanelLoaded(hasHighMagComponent, testProgram.getProject().getName()) == false)
        loadPanelIntoMachine(testProgram, false);
      else
      {
        checkThatCurrentlyLoadedPanelMatchesProjectPanelSize(testProgram);
        checkThatCurrentlyLoadedPanelProjectNameMatchesProjectName(testProgram);
      }

      _lightStack.hardwareInUseMode();

      if (didUserAbort())
        return;

      Project project = testProgram.getProject();
    
      //reset after aligned and acquire verification images.
      _alignment.setIsRunTimeAlignmentNeededAppliedInVerificationImages(true);
      project.alignAndAcquireVerificationImages(_testExecutionTimer, true);
      _alignment.setIsRunTimeAlignmentNeededAppliedInVerificationImages(false);
    }
    catch (XrayTesterException ex)
    {
      handleXrayTesterException(ex);
    }
    catch (RuntimeException rte)
    {
      handleRuntimeException(rte);
    }
    finally
    {
      setLightStackOnExit();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, TestExecutionEventEnum.ACQUIRE_VERIFICATION_IMAGES);
  }

  /**
   * @author George A. David
   */
  public void acquireAdjustFocusImages(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    //sheng chuan, move this function before state changed to reset all confirmation task abort status
    clearExitAndAbortFlags();
    //XCR-3797, Failed to cancel CDNA task after start up
    getHardwareStartedUpIfNecessary();
    if (didUserAbort())
      return;
    _hardwareObservable.stateChangedBegin(this, TestExecutionEventEnum.ACQUIRE_ADJUST_FOCUS_IMAGES);
    _hardwareObservable.setEnabled(false);

    try
    {
      _production = false;
      _precisionTuningImageAcquisition = false;
      _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS;

      if (didUserAbort())
        return;
      
      boolean hasHighMagComponent = containHighMagInspectionInCurrentLoadedProject();
      if (isPanelLoaded(hasHighMagComponent, testProgram.getProject().getName()) == false)
        loadPanelIntoMachine(testProgram, false);
      else
      {
        checkThatCurrentlyLoadedPanelMatchesProjectPanelSize(testProgram);
        checkThatCurrentlyLoadedPanelProjectNameMatchesProjectName(testProgram);
      }

      if (didUserAbort())
        return;

      _lightStack.hardwareInUseMode();
      _imageGeneration.acquireAdjustFocusImages(testProgram, _testExecutionTimer);
    }
    catch (XrayTesterException ex)
    {
      handleXrayTesterException(ex);
    }
    catch (RuntimeException rte)
    {
      handleRuntimeException(rte);
    }
    finally
    {
      setLightStackOnExit();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, TestExecutionEventEnum.ACQUIRE_VERIFICATION_IMAGES);
  }

  /**
   * @author Scott Richardson
   */
  public void acquireVirtualLiveImages(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    //sheng chuan, move this function before state changed to reset all confirmation task abort status
    clearExitAndAbortFlags();
    //XCR-3797, Failed to cancel CDNA task after start up
    getHardwareStartedUpIfNecessary();
    if (didUserAbort())
    {
      _userAborted = true;
      VirtualLiveImageGenerationSettings.getInstance().setMultipleSelectComponent(false);
      return;
    }
    _hardwareObservable.stateChangedBegin(this, TestExecutionEventEnum.ACQUIRE_VIRTUAL_LIVE_IMAGES);
    _hardwareObservable.setEnabled(false);
    try
    {
      _production = false;
      _precisionTuningImageAcquisition = false;
      _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS;
      
      if (VirtualLiveManager.getInstance().isVirtualLiveWithAlignment())
      {
        checkThatManualAlignmentHasBeenPerformed(testProgram);
      }

      if (didUserAbort())
      {
        _userAborted = true;
        VirtualLiveImageGenerationSettings.getInstance().setMultipleSelectComponent(false);
        return;
      }

      boolean hasHighMagComponent = containHighMagInspectionInTestSubPrograms(testProgram.getTestSubPrograms());
      if (isPanelLoaded(hasHighMagComponent, testProgram.getProject().getName()) == false)
      {
        checkNeedToEnableSafetyLevelSensorForVirtualLive(testProgram);
        loadPanelIntoMachine(testProgram, true);
        
        if (PanelHandler.getInstance().isAborting())
        {
          _userAborted = true;
          VirtualLiveImageGenerationSettings.getInstance().setMultipleSelectComponent(false);
          return;
        }
      }
      else
      {
        checkThatCurrentlyLoadedPanelMatchesProjectPanelSize(testProgram);
        checkThatCurrentlyLoadedPanelProjectNameMatchesProjectName(testProgram);
      }

      if (didUserAbort())
      {
        _userAborted = true;
        VirtualLiveImageGenerationSettings.getInstance().setMultipleSelectComponent(false);
        return;
      }
      _lightStack.hardwareInUseMode();
      Project project = testProgram.getProject();
      project.acquireVirtualLiveImages(_testExecutionTimer, testProgram);
      _lightStack.standyMode();
    }
    catch (XrayTesterException ex)
    {
//      if (ex != null)
//      {       
        VirtualLiveImageGenerationSettings.getInstance().setMultipleSelectComponent(false);
//        VirtualLiveImageGenerationProgressObservable.getInstance().stateChanged(new VirtualLiveImageGenerationEvent(VirtualLiveImageGenerationEventEnum.XRAYEXCEPTION_WHEN_GENERATE_VIRTUALLIVE_IMAGES), ex);
//      }
      _testExecutionTimer.stopAllTimers();
      _exceptionExit = true;
      throw ex;
    }
    catch (RuntimeException rte)
    {
      handleRuntimeException(rte);
      throw rte;
    }
    finally
    {
      setLightStackOnExit();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, TestExecutionEventEnum.ACQUIRE_VERIFICATION_IMAGES);
  }

  /**
   * @author Bill Darbie
   */
  public void runManualAlignment(TestSubProgram subProgram) throws XrayTesterException
  {
    Assert.expect(subProgram != null);
    
    //sheng chuan, move this function before state changed to reset all confirmation task abort status
    clearExitAndAbortFlags();
    _hardwareObservable.stateChangedBegin(this, TestExecutionEventEnum.RUN_MANUAL_ALIGNMENT);
    _hardwareObservable.setEnabled(false);

    try
    {
      _lightStack.hardwareInUseMode();

      if(subProgram.isLowMagnification() != XrayTester.isLowMagnification() )
      {
        ImageAcquisitionEngine.changeMagnification(subProgram.getMagnificationType());
        if(ImagingChainProgramGenerator.getEnableStepSizeOptimization())
          ImagingChainProgramGenerator.forceCalculateMaxStepSizeInNanometers(subProgram.getTestProgram().getProject().getPanel().getThicknessInNanometers(), subProgram.getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers());
      }
      
      _alignment.runManualAlignment(subProgram, _testExecutionTimer);
    }
    catch (XrayTesterException ex)
    {
      handleXrayTesterException(ex);
    }
    catch (RuntimeException rte)
    {
      handleRuntimeException(rte);
    }
    finally
    {
      setLightStackOnExit();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, TestExecutionEventEnum.RUN_MANUAL_ALIGNMENT);

  }

  /**
   * @author Bill Darbie
   */
  private void handleXrayTesterException(XrayTesterException xte) throws XrayTesterException
  {
    Assert.expect(xte != null);

    _testExecutionTimer.stopAllTimers();
    _exceptionExit = true;
    _unloadException = xte;
    throw xte;
  }

  /**
   * @author Bill Darbie
   */
  private void handleRuntimeException(RuntimeException rte)
  {
    Assert.expect(rte != null);

    _testExecutionTimer.stopAllTimers();
    _exceptionExit = true;
    throw rte;
  }

  /**
   * @author Bill Darbie
   */
  private void setLightStackOnExit()
  {
    if (_xrayTester.isHardwareAvailable() == false)
      return;

    if (_exceptionExit)
    {
      try
      {
        if (_digitalIo.isStartupRequired() == false)
		{
          _lightStack.errorMode();
          
          //Khaw Chek Hau - XCR2654: CAMX Integration
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SHOP_FLOOR_SYSTEM))
          {
            try
            {
              _shopFloorSystem.equipmentError();
              _shopFloorSystem.equipmentAlarm();
            }
            catch (DatastoreException ex)
            {
              DatastoreException datastoreException = (DatastoreException) ex;
              MessageInspectionEvent event = new ShopFloorMessageInspectionEvent(datastoreException);
              InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
              inspectionEventObservable.sendEventInfo(event);
            }
          }
          
          //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_MACHINE_UTILIZATION_REPORT) && _machineUtilizationReportSystem.isFaultExist() == false)
          {
            try
            {
              _machineUtilizationReportSystem.initiateMachineUtilizationReport(MachineUtilizationReportEventEnum.MACHINE_ERROR_HAPPEN);  
            }
            catch (DatastoreException ex)
            {
              DatastoreException datastoreException = (DatastoreException) ex;
              MessageInspectionEvent event = new MachineUtilizationReportMessageInspectionEvent(datastoreException);
              InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
              inspectionEventObservable.sendEventInfo(event);
            }
          }
          
          //Khaw Chek Hau - XCR3348: VOne Machine Utilization Feature
          if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.GENERATE_VONE_MACHINE_STATUS_MONITORING_REPORT) && _vOneMachineStatusMonitoringSystem.isFaultExist() == false)
          {
            try
            {
              _vOneMachineStatusMonitoringSystem.writeVOneMachineStatusMonitoringReport(VOneMachineStatusMonitoringEventEnum.MACHINE_ERROR_START);
            }
            catch (DatastoreException ex)
            {
              DatastoreException datastoreException = (DatastoreException) ex;
              MessageInspectionEvent event = new VOneMachineStatusMonitoringMessageInspectionEvent(datastoreException);
              InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
              inspectionEventObservable.sendEventInfo(event);
            }
          }
		}
      }
      catch (XrayTesterException ex)
      {
        // do nothing
      }
    }
    else
    {
      try
      {
        if (_digitalIo.isStartupRequired() == false)
          _lightStack.standyMode();
      }
      catch (XrayTesterException ex)
      {
        // do nothing
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private void setInspectionInProgress(boolean inspectionInProgress)
  {
    synchronized (_inspectionInProgressLock)
    {
      _inspectionInProgress = inspectionInProgress;
      _inspectionInProgressLock.notifyAll();
    }
  }

  /**
   * @author Bill Darbie
   */
  public boolean isInspectionInProgress()
  {
    synchronized (_inspectionInProgressLock)
    {
      return _inspectionInProgress;
    }
  }

  /**
   * If an inspection is paused, this will cause the inspection to continue until the next enabled
   * joint.
   *
   * @author Matt Wharton
   */
  public void runToNextJoint()
  {
    _inspectionEngine.runToNextJoint();
  }

  /**
   * If an inspection is paused, this will cause the inspection to continue until the next enabled
   * diagnostic on the next slice.
   *
   * @author Bill Darbie
   */
  public void runToNextSlice()
  {
    _inspectionEngine.runToNextSlice();
  }

  /**
   * If an inspection is paused, this will cause the inspection to continue until the next enabled
   * diagnostic on the next algorithm.
   *
   * @author Bill Darbie
   */
  public void runToNextAlgorithm()
  {
    _inspectionEngine.runToNextAlgorithm();
  }

  /**
   * @author Bill Darbie
   */
  public void runToNextInspectionRegion()
  {
    _inspectionEngine.runToNextInspectionRegion();
  }

  /**
   * @author Bill Darbie
   */
  public void runToNextDiagnosticEvent()
  {
    _inspectionEngine.runToNextDiagnosticEvent();
  }

  /**
   * @author Bill Darbie
   */
  public void continueInspection()
  {
    _inspectionEngine.continueInspection();
  }

  /**
   * @author Bill Darbie
   */
  public void pauseInspection()
  {
    _inspectionEngine.pauseInspection();
  }

  /**
   * Abort the current action.  This includes aborting an inspection, aborting an
   * panel bypass loop, or aborting a panel load.
   *
   * @author Bill Darbie
   */
  public void abort() throws XrayTesterException
  {
    synchronized (_abortLock)
    {
      // _userAborted stays set until the next inspection is run
      _userAborted = true;
      
      _panelHandler.setKeepOuterBarrierOpenForLoadingPanel(false);
      
      _testResultsEnum = TestResultsEnum.NOT_TESTED;

      _hardwareTaskEngine.cancel();
      _inspectionEngine.abort();
      _xrayTester.abort();
      _imageGeneration.abort();
      
      _preScanBarcodeScanner.clear();
      //always ensure the boolean lock is release
      _isPreScanRunning.setValue(false);

      // if we are in the middle running a panel through the machine in bypass mode, abort it
      if (_panelLoadInProgress)
      {
        _panelHandler.abort();

        // we do not want to set _abortInProgress to false here because
        // the bypassPanel() method will set _abortInProgress to false after it jumps out
        // of its loop where it was waiting for _abortInProgress to be true (which is
        // done at the beginning of this method)
      }   
      if (_panelHandler.isInLineModeEnabled())
        _barcodeReaderManager.abort();
    }
  }

  /**
   * @author Bill Darbie
   */
  public void clearExitAndAbortFlags() throws XrayTesterException
  {
    _userAborted = false;
    _exceptionExit = false;
    _unloadException = null;
    _hardwareTaskEngine.clearAbort();
    //XCR-2994 - Kok Chun, Tan - clear abort flag
    _xrayTester.clearAbortFlag();
  }

  /**
   * @author Bill Darbie
   */
  public boolean didUserAbort()
  {
    synchronized (_abortLock)
    {
      return _userAborted;
    }
  }

  /**
   * @return the TestExecutionTimer class that contains all times of the last run.
   * @author Bill Darbie
   */
  public TestExecutionTimer getTestExecutionTimer()
  {
    Assert.expect(_testExecutionTimer != null);
    return new TestExecutionTimer(_testExecutionTimer);
  }

  /**
   * @author Bill Darbie
   */
  public void checkThatManualAlignmentHasBeenPerformed(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    if (testProgram.getProject().getPanel().getPanelSettings().isManualAlignmentCompleted() == false)
      throw new NoAlignmentBusinessException();
  }

  /**
   * @author Bill Darbie
   */
  public void checkThatCurrentlyLoadedPanelMatchesProjectPanelSize(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    // check that width and length match the panel in the machine
    if (_panelHandler.isPanelLoaded())
    {
      int cadWidth = testProgram.getProject().getPanel().getWidthAfterAllRotationsInNanoMeters();
      int cadLength = testProgram.getProject().getPanel().getLengthAfterAllRotationsInNanoMeters();

      int panelUnderTestWidth = _panelHandler.getLoadedPanelWidthInNanoMeters();
      int panelUnderTestLength = _panelHandler.getLoadedPanelLengthInNanoMeters();

      if ((cadWidth != panelUnderTestWidth) || (cadLength != panelUnderTestLength))
        throw new PanelCadSizeDoesNotMatchPanelSizeInTesterBusinessException(cadWidth,
                                                                             cadLength,
                                                                             panelUnderTestWidth,
                                                                             panelUnderTestLength);
    }

    // check that panel thickness has been set
    int thickness = testProgram.getProject().getPanel().getThicknessInNanometers();
    if (thickness == 0)
      throw new PanelThicknessInvalidBusinessException(thickness);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void checkThatCurrentlyLoadedPanelProjectNameMatchesProjectName(TestProgram testProgram) throws XrayTesterException
  {
      Assert.expect(testProgram != null);

      if (_panelHandler.isPanelLoaded())
      {
          String projectName = testProgram.getProject().getName();
          String loadedPanelProjectName = _panelHandler.getLoadedPanelProjectName();
          if (isProjectNameMatchWithPanelName(projectName, loadedPanelProjectName) == false)
              throw new PanelProjectNameDoesNotMatchProjectNameInTesterBusinessException(projectName, loadedPanelProjectName);
      }
  }
  
  /**
   * @author Eoh Weng Jian
   */
  public void checkThatCurrentlyLoadedPanelProjectNameMatchesProjectName(String projectName) throws XrayTesterException
  {
      Assert.expect(projectName != null);

      if (_panelHandler.isPanelLoaded())
      {
          String loadedPanelProjectName = _panelHandler.getLoadedPanelProjectName();
          if (isProjectNameMatchWithPanelName(projectName, loadedPanelProjectName) == false)
              throw new PanelProjectNameDoesNotMatchProjectNameInTesterBusinessException(projectName, loadedPanelProjectName);
      }
  }

  /**
   * @author Yong Sheng Chuan
   */
  public void checkIsThatCurrentlyLoadedPanelProjectRecipesExist(String projectName) throws XrayTesterException
  {
      Assert.expect(projectName != null);

      boolean isCorrespondingRecipeFound = false;
      for (String names : FileName.getProjectNameList())
      {
        if(isProjectNameMatchWithPanelName(names, projectName))
        {
          isCorrespondingRecipeFound = true;
          break;
        }
      }
      if(isCorrespondingRecipeFound == false)
        throw new PanelProjectNameDoesNotHaveCorrespondingRecipeBusinessException(projectName);
  }
  
  
  /**
   * @return 
   * @author Wei Chin
   */
  public boolean containHighMagInspectionInCurrentLoadedProject()
  {
    //Check If Need To Enabled Level Safety Sensor for Panel Loading    
    if(Project.isCurrentProjectLoaded())
    {
      
      return Project.getCurrentlyLoadedProject().getPanel().getPanelSettings().hasHighMagnificationComponent();
      
    }
    else
    {
      return false;
    }
  }

  
    /**
   * @return 
   * @author Wei Chin
   */
  public boolean containHighMagInspectionInTestSubPrograms(java.util.List <TestSubProgram> testSubprograms)
  {
    for(TestSubProgram testSubProgram : testSubprograms)
    {
      if( testSubProgram.getMagnificationType().equals(MagnificationTypeEnum.HIGH))
        return true;
    }
    return false;
  }
  
  /**
   * @author Anthony Fong
   */
  public void checkNeedToEjectPanelAndEnableSafetyLevelSensorForVariableMag() throws XrayTesterException
  {
    //Check If Need To Enabled Level Safety Sensor for Panel Loading
    TestProgram currentTestProgram = Project.getCurrentlyLoadedProject().getTestProgram();
    
    //Check If Need To Eject Panel For Variable Mag if Panel was loaded
    if (currentTestProgram.getProject().getPanel().getPanelSettings().hasHighMagnificationComponent()  &&
        PanelHandler.getInstance().getPassedSafetyLevelSensorScan() == false)
    {
       String projectName = currentTestProgram.getProject().getName();
       throw new VariableMagSubtypeSettingChangeWithPanelLoadedInTesterBusinessException(projectName);
    }
  }  
  /**
   * @author Chin Seong, Kee
   * This portion of code is to check whether machine should enable the Safety Level Check sensor or not 
   * for panel Loading
   */
  public boolean checkNeedToEnableSafetyLevelSensorForVariableMag(TestProgram testProgram) 
  {
    if(XrayActuator.isInstalled() == false)
      return false;
    
    //previous :: currentTestProgram.hasVariableMagnification()
    if(testProgram.getProject().getPanel().getPanelSettings().hasHighMagnificationComponent() == true)
    {
       return true;
    }
    else
    {
      return false;
    } 
  }

  /**
   * @author Bill Darbie
   */
  public void acquireOfflineImages(TestProgram testProgram, ImageSetData imageSetData) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(imageSetData != null);

    //sheng chuan, move this function before state changed to reset all confirmation task abort status
    clearExitAndAbortFlags();
    //XCR-3797, Failed to cancel CDNA task after start up
    getHardwareStartedUpIfNecessary();
    if (didUserAbort())
      return;
    _hardwareObservable.stateChangedBegin(this, TestExecutionEventEnum.ACQUIRE_OFFLINE_IMAGES);
    _hardwareObservable.setEnabled(false);

    try
    {
      _production = false;
      _precisionTuningImageAcquisition = false;
      _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS;

      if (didUserAbort())
        return;

      boolean hasHighMagComponent = containHighMagInspectionInCurrentLoadedProject();
      if (isPanelLoaded(hasHighMagComponent, testProgram.getProject().getName()) == false)
        loadPanelIntoMachine(testProgram, false);
      else
      {
        checkThatCurrentlyLoadedPanelMatchesProjectPanelSize(testProgram);
        checkThatCurrentlyLoadedPanelProjectNameMatchesProjectName(testProgram);
      }

      if (didUserAbort())
        return;

      if(imageSetData.getImageSetCollectionTypeEnum().equals(ImageSetCollectionTypeEnum.EXPOSURE_LEARNING))
        _imageAcquisitionEngine.setIsGenerateImageForExposureLearning(true);

      _imageAcquisitionModeEnum = ImageAcquisitionModeEnum.USE_HARDWARE_ALWAYS;
      checkThatManualAlignmentHasBeenPerformed(testProgram);

      if (didUserAbort())
        return;

      _lightStack.hardwareInUseMode();
      _imageGeneration.acquireOfflineImages(testProgram, _testExecutionTimer, imageSetData);
    }
    catch (XrayTesterException ex)
    {
      handleXrayTesterException(ex);
    }
    catch (RuntimeException rte)
    {
      handleRuntimeException(rte);
    }
    finally
    {
      setLightStackOnExit();
      _hardwareObservable.setEnabled(true);
    }

    _hardwareObservable.stateChangedEnd(this, TestExecutionEventEnum.ACQUIRE_OFFLINE_IMAGES);
  }

  /**
   * @author Roy Williams
   */
  public void clearProjectInfo()
  {
    _resultsXmlWriter.clearProjectInfo();
  }

  /**
   *
   * @author Cheah Lee Herng
   */
  public boolean isProjectNameMatchWithPanelName(String projectName, String panelName)
  {
      Assert.expect(projectName != null);
      Assert.expect(panelName != null);

      if (projectName.equalsIgnoreCase(panelName))
          return true;
      else
          return false;
  }

  /**
   * @return the _alignmentFailAndUnloadPanel
   * @author Chong Wei Chin
   */
  public boolean isAlignmentFailAndUnloadPanel()
  {
//    if(_alignmentFailAndUnloadPanel)
//    {
//      _alignmentFailAndUnloadPanel = false;
//      return true;
//    }

    return _alignmentFailAndUnloadPanel;
  }

  /**
   * @param alignmentFailAndUnloadPanel the _alignmentFailAndUnloadPanel to set
   * @author Chong Wei Chin
   */
  public void setAlignmentFailAndUnloadPanel(boolean alignmentFailAndUnloadPanel)
  {
    _alignmentFailAndUnloadPanel = alignmentFailAndUnloadPanel;
  }

  /**
   * @author Jack Hwee
   */
  public void doRegionSurfaceMapping(OpticalCameraRectangle opticalCameraRectangle, 
                                     Board board) throws XrayTesterException
  {
    Assert.expect(opticalCameraRectangle != null);
    Assert.expect(board != null);
    
    _surfaceMapping = SurfaceMapping.getInstance();
    _surfaceMapping.doRegionSurfaceMapping(opticalCameraRectangle, board);
  }
  
  /**
   * This function is used in 5.6b as in 5.6b we display optical image in SurfaceMapLiveVideoDialog instead of live video.
   * Thus we have to adjust the current setting, recapture optical image, and repaint SurfaceMapLiveVideoDialog.
   * 
   * @author Ying-Huan.Chu
   */
  public void acquireSingleImage(int currentSetting) throws XrayTesterException
  {
    Assert.expect(currentSetting > 0);
    
    _surfaceMapping = SurfaceMapping.getInstance();
    _surfaceMapping.acquireSingleImage(currentSetting);
  }
  
  /**
   * This function is used in 5.7. In 5.7 we display live video in SurfaceMapLiveVideoDialog and 
   * thus the current setting changes will be taken effect immediately.
   * 
   * @author Ying-Huan.Chu
   */
  public void adjustCurrent(int currentSetting) throws XrayTesterException
  {
    Assert.expect(currentSetting > 0);
    
    _surfaceMapping = SurfaceMapping.getInstance();
    _surfaceMapping.adjustCurrent(currentSetting);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void moveStageToOpticalCameraRectanglePosition(OpticalCameraRectangle opticalCameraRectangle, 
                                                        Board board) throws XrayTesterException
  {
    Assert.expect(opticalCameraRectangle != null);
    Assert.expect(board != null);
    
    _surfaceMapping = SurfaceMapping.getInstance();
    _surfaceMapping.moveStageToOpticalCameraRectanglePosition(opticalCameraRectangle, board);
  }
   
  /**
   * @author sham
   */
  public void checkNeedToEjectPanelAndEnableSafetyLevelSensorForVirtualLive(TestProgram virtualLiveTestProgram) throws XrayTesterException
  {
    TestProgram testProgram = Project.getCurrentlyLoadedProject().getTestProgram();
    
    checkNeedToEnableSafetyLevelSensorForVirtualLive(virtualLiveTestProgram);
    
    //Check If Need To Eject Panel For Variable Mag if Panel was loaded
    if (virtualLiveTestProgram.getAllTestSubPrograms().get(0).getMagnificationType().equals(MagnificationTypeEnum.HIGH) &&
        PanelHandler.getInstance().getPassedSafetyLevelSensorScan() == false)
    {
       
       //PanelHandler.getInstance().unloadPanel();
       //PanelHandler.getInstance().setNeedToEjectPanelForVariableMagDuringStartup(false);
       String projectName = virtualLiveTestProgram.getProject().getName();
       throw new VariableMagSubtypeSettingChangeWithPanelLoadedInTesterBusinessException(projectName);
    }
  }
  
  /**
   * @author sham
   * This portion of code get from checkNeedToEnableSafetyLevelSensorForVariableMag
   * and modify the comparator
   * @author Wei Chin (change to return value)
   */
  public boolean checkNeedToEnableSafetyLevelSensorForVirtualLive(TestProgram testProgram) 
  {
    if(XrayActuator.isInstalled() == false)
      return false;
    
    //here will get the magnification enum set in virtual live subprogram
    if(testProgram.getAllTestSubPrograms().get(0).getMagnificationType().equals(MagnificationTypeEnum.HIGH))
    {
      return true;
    }
    else
    {
      return false;
    } 
  }

  /*
   * @author Phang Siew Yeng
   */
  public void setAlignmentFailedAndRerunAlignment(boolean performRuntimeManualAlignment)
  {
    _needToPerformRuntimeManualAlignment = performRuntimeManualAlignment;
  }

  /*
   * @author Phang Siew Yeng
   */
  public boolean isAlignmentFailAndRerunAlignment()
  {
   return _needToPerformRuntimeManualAlignment;
  }
  
  /**
   * Perform runtime alignment before running Surface Mapping.
   * We pass in OpticalCameraRectangle instead of a list of OpticalCameraRectangles / a OpticalRegion
   * as we have to handle multi-board case and long panel case. 
   * Example 1: only run alignment for 2nd board when only the 2nd board has OpticalCameraRectangle(s).
   * Example 2: only run alignment for the left side of a long panel when only the left side of the long panel has OpticalCameraRectangle(s).
   * 
   * @author Ying-Huan.Chu
   */
  public void performManualAlignmentForSurfaceMappping(TestProgram testProgram, 
                                                       OpticalCameraRectangle opticalCameraRectangle,
                                                       boolean isPanelBasedAlignment) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    Assert.expect(opticalCameraRectangle != null);

    //sheng chuan, move this function before state changed to reset all confirmation task abort status
    clearExitAndAbortFlags();
    //XCR-3797, Failed to cancel CDNA task after start up
    getHardwareStartedUpIfNecessary();
    _hardwareObservable.stateChangedBegin(this, TestExecutionEventEnum.RUN_MANUAL_ALIGNMENT);
    _hardwareObservable.setEnabled(false);

    try
    {
      _lightStack.hardwareInUseMode();

      boolean isLongPanel = testProgram.getProject().getPanel().getPanelSettings().isLongPanel();
      java.util.List<TestSubProgram> testSubPrograms = new ArrayList<>();
      
      // check mixed mag conditions first.
      for (TestSubProgram testSubProgram : testProgram.getAllInspectableAlignmentTestSubPrograms())
      {
        // if the project is a mixed-mag recipe, we should only consider low-mag TestSubProgram.
        if (testProgram.getProject().getPanel().getPanelSettings().hasLowMagnificationComponent() &&
            testProgram.getProject().getPanel().getPanelSettings().hasHighMagnificationComponent())
        {
          if (testSubProgram.getMagnificationType().equals(MagnificationTypeEnum.LOW))
          {
            testSubPrograms.add(testSubProgram);
          }
        }
        else
        {
          testSubPrograms.add(testSubProgram);
        }
      }
      
      // if this is a long panel recipe, check if both left and right alignments should be run.
      java.util.List<TestSubProgram> testSubProgramToRunAlignment = new ArrayList<>();
      if (isLongPanel)
      {
        // we should only include TestSubPrograms that:
        // 1. contain the OpticalCameraRectangle. (higher priority)
        // 2. intersect with the OpticalCameraRectangle. (lower priority)
        for (TestSubProgram subProgram : testSubPrograms)
        {
          if (subProgram.getImageableRegionInNanoMeters().contains(opticalCameraRectangle.getRegion()))
          {
            if (isPanelBasedAlignment)
            {
              testSubProgramToRunAlignment.add(subProgram);
              break;
            }
            else
            {
              // only board(s) that are involved in current Surface Mapping operation will be included.
              if (opticalCameraRectangle.getBoard().equals(subProgram.getBoard()))
              {
                testSubProgramToRunAlignment.add(subProgram);
                break;
              }
            }
          }
        }
        if (testSubProgramToRunAlignment.isEmpty())
        {
          // no TestSubProgram contains the OpticalCameraRectangle.
          // we should find the TestSubProgram that covers the most area of the OpticalCameraRectangle.
          Map<TestSubProgram, java.awt.Rectangle> testSubProgramToIntersectionAreaMap = new LinkedHashMap<>();
          for (TestSubProgram subProgram : testSubPrograms)
          {
            if (subProgram.getImageableRegionInNanoMeters().intersects(opticalCameraRectangle.getRegion()))
            {
              // Checks the area of intersection between the TestSubProgram and OpticalCameraRectangle.
              java.awt.Rectangle intersection = subProgram.getImageableRegionInNanoMeters().getRectangle2D().getBounds().intersection(
                                                opticalCameraRectangle.getRegion().getRectangle2D().getBounds());
              testSubProgramToIntersectionAreaMap.put(subProgram, intersection);
            }
          }
          java.awt.Rectangle largestIntersection = new java.awt.Rectangle(0,0,0,0);
          TestSubProgram testSubProgramWithLargestIntersection = new TestSubProgram(testProgram);
          // Loop through and check which TestSubProgram has the largest intersection area with the OpticalCameraRectangle.
          for (Map.Entry<TestSubProgram, java.awt.Rectangle> entry : testSubProgramToIntersectionAreaMap.entrySet())
          {
            TestSubProgram subProgram = entry.getKey();
            java.awt.Rectangle intersection = entry.getValue();
            // width x height = area
            if ((intersection.getWidth() * intersection.getHeight()) > (largestIntersection.getWidth() * largestIntersection.getHeight()))
            {
              if (isPanelBasedAlignment)
              {
                largestIntersection = intersection;
                testSubProgramWithLargestIntersection = subProgram;
              }
              else
              {
                // check if the boards are equal.
                if (opticalCameraRectangle.getBoard().equals(subProgram.getBoard()))
                {
                  largestIntersection = intersection;
                  testSubProgramWithLargestIntersection = subProgram;
                }
              } 
            }
          }
          testSubProgramToRunAlignment.add(testSubProgramWithLargestIntersection);
        }
      }
      else
      {
        if (isPanelBasedAlignment)
        {
          testSubProgramToRunAlignment.addAll(testSubPrograms);
        }
        else
        {
          for (TestSubProgram subProgram : testSubPrograms)
          {
            // check if the boards are equal.
            if (opticalCameraRectangle.getBoard().equals(subProgram.getBoard()))
            {
              testSubProgramToRunAlignment.add(subProgram);
            }
          }
        }
      }
      
      for (TestSubProgram subProgram : testSubProgramToRunAlignment)
      {
        // we should only include TestSubPrograms that haven't completed runtime alignment.
        if ((_lastPerformedAlignmentTestSubProgramId == subProgram.getId()) == false)
        {
          if (subProgram.isLowMagnification() != XrayTester.isLowMagnification() )
          {
            ImageAcquisitionEngine.changeMagnification(subProgram.getMagnificationType());
            if (ImagingChainProgramGenerator.getEnableStepSizeOptimization())
              ImagingChainProgramGenerator.forceCalculateMaxStepSizeInNanometers(subProgram.getTestProgram().getProject().getPanel().getThicknessInNanometers(), subProgram.getTestProgram().getProject().getPanel().getMeansBoardZOffsetInNanometers());
          }
          try
          {
            _alignment.autoAlignForAlignmentConfirmation(subProgram, _testExecutionTimer);
            _lastPerformedAlignmentTestSubProgramId = subProgram.getId();
          }
          catch (XrayTesterException xex)
          {
            handleXrayTesterException(xex);
          }
        }
      }
    }
    catch (XrayTesterException xex)
    {
      handleXrayTesterException(xex);
    }
    catch (RuntimeException rte)
    {
      handleRuntimeException(rte);
    }
    finally
    {
      setLightStackOnExit();
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, TestExecutionEventEnum.RUN_MANUAL_ALIGNMENT);
  }
  
  /**
   * @author Swee Yee Wong
   */
  private void debug(String message)
  {
    if (_debug)
      System.out.println(_me + ": " + message);
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public void resetLastPerformedAlignmentTestSubProgramId()
  {
    _lastPerformedAlignmentTestSubProgramId = -1;
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public boolean isUseSpecificHistoryLogDir()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.USE_SPECIFIC_HISTORY_LOG_DIR);
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public void setUseSpecificHistoryLogDir(boolean useSpecificHistoryLogDir) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.USE_SPECIFIC_HISTORY_LOG_DIR, useSpecificHistoryLogDir);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3063: Notification Message When Using DRO 
   */
  public void setPromptWarningMsgIfModifyDROLevel(boolean PromptWarningMsgIfModifyDROLevel) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.PROMPT_WARNING_MESSAGE_IF_MODIFY_DRO_LEVEL, PromptWarningMsgIfModifyDROLevel);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3063: Notification Message When Using DRO 
   */
  public boolean isPromptWarningMsgIfModifyDROLevel()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.PROMPT_WARNING_MESSAGE_IF_MODIFY_DRO_LEVEL);  
  }
  
  /**
   * XCR2662: Production inspection time only display for misalignment board
   * @author Khaw Chek Hau
   */
  public void stopFullInspectionTimer()
  {
    Assert.expect(_testExecutionTimer != null);
    _testExecutionTimer.stopFullInspectionTimer();
  }
  
  /**
   * XCR2662: Production inspection time only display for misalignment board
   * @author Khaw Chek Hau
   */
  public void startFullInspectionTimer()
  {
    Assert.expect(_testExecutionTimer != null);
    _testExecutionTimer.startFullInspectionTimer();
  }
  
  /**
   * XCR-3551 - Customize Measurement XML output File
   * @author Janan
   */
  public boolean isGenerateMeasurementsXMLEnabled()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.GENERATE_MEASUREMENTS_XML_FILE);
  }

  /**
   * XCR-3551 - Customize Measurement XML output File
   * @author Janan
   */
  public void setGenerateMeasurementsXML(boolean GenerateMeasurementsXML) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.GENERATE_MEASUREMENTS_XML_FILE, GenerateMeasurementsXML);
  }
  
  /**
   * XCR-3551 - Customize Measurement XML output File
   * @author Janan
   */
  public void setDumpZHeightInMeasurementsXML(boolean dumpZHeight) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.DUMP_ZHEIGHT_IN_MEASUREMENTS_XML_FILE, dumpZHeight);
  }

  /**
   * XCR-3551 - Customize Measurement XML output File
   * @author Janan
   */
  public boolean isDumpZHeightInMeasurementsEnabled(){
    return _config.getBooleanValue(SoftwareConfigEnum.DUMP_ZHEIGHT_IN_MEASUREMENTS_XML_FILE);
  }
  
  /**
   * XCR-3551 - Customize Measurement XML output File
   * @author Janan
   */
  public boolean isCustomizeMeasurementXMLEnable(){
    return _config.getBooleanValue(CustomMeasurementFileConfigEnum.CUSTOMIZE_MEASUREMENT_OUTPUT);
  }
  
  /**
   * XCR-3551 - Customize Measurement XML output File
   * @author Janan
   */
  public boolean isCustomizeJointTypeCheckBoxSelected(String jointTypeName){
    Assert.expect(jointTypeName != null);
    return _config.getBooleanValue(CustomMeasurementFileConfigEnum.getCustomMeasurementFileConfigEnum(jointTypeName));
  }
  
  /**
   * XCR-3551 - Customize Measurement XML output File
   * @author Janan
   */
  public boolean isCustomizeMeasurementTypeCheckBoxSelected(String measurementTypeName){
    Assert.expect(measurementTypeName != null);
    return _config.getBooleanValue(CustomMeasurementFileConfigEnum.getCustomMeasurementFileConfigEnum(measurementTypeName));
  }  
}
