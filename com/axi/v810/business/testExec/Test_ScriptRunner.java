package com.axi.v810.business.testExec;

import java.io.*;

import com.axi.util.*;
import java.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class Test_ScriptRunner extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_ScriptRunner());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    ScriptRunner scriptRunner = ScriptRunner.getInstance();

    // capture original state of config file settings
    String origPreInspectionScript = scriptRunner.getPreInspectionScript();
    String origPostInspectionScript = scriptRunner.getPostInspectionScript();
    boolean origPreScriptEnabled = scriptRunner.isPreInspectionScriptEnabled();
    boolean origPostScriptEnabled = scriptRunner.isPostInspectionScriptEnabled();


    String outputFileName = FileName.getPreInspectionScriptOutputDataFullPath();
    String outputFileNameOrig = FileName.getPreInspectionScriptOutputDataFullPath() + ".orig";

    try
    {
      // get a Project and set up serial numbers
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();
      Project project = Project.importProjectFromNdfs("0027parts", warnings);
      String panelSerialNumber = "panelSerialNumber";
      String currentXMLResultFullPath = Directory.getResultsDir(project.getName());
      Map<String, String> boardNameToSerialNumberMap = new HashMap<String, String>();
      boardNameToSerialNumberMap.put("1", "board1serialNum");

      // set up filenames
      String dataDir = getTestDataDir() + File.separator + "scriptRunner";
      String preScript = dataDir + File.separator + "preScript.bat";
      String postScript = dataDir + File.separator + "postScript.bat";

      String localOutputFile = dataDir + File.separator + FileName.getPreInspectionScriptOutputDataFile();
      FileUtilAxi.copy(localOutputFile, outputFileName);

      // place preInspectionOutput.data into the correct location for this test - normally the
      // preInspectionScript would have created this
      if (FileUtilAxi.exists(outputFileName))
        FileUtilAxi.copy(outputFileName, outputFileNameOrig);

      // enable the scripts to run
      scriptRunner.setPreInspectionScript(preScript);
      scriptRunner.setPostInspectionScript(postScript);
      scriptRunner.setPreInspectionScriptEnabled(true);
      scriptRunner.setPostInspectionScriptEnabled(true);

      // run things
      scriptRunner.runPreInspectionScript(panelSerialNumber,new ArrayList<String>(),false);

      // see if we get back what we expect
      boolean inspect = scriptRunner.inspectPanel();
      Assert.expect(inspect);

      String newSerialNumber = scriptRunner.getPanelSerialNumber();
      Assert.expect(newSerialNumber.equals("newSerialNumber"));

      String warning = scriptRunner.getWarningMessage();
      Assert.expect(warning.equals(" warning message"));

      String error = scriptRunner.getErrorMessage();
      Assert.expect(error.equals(" error message"));

      scriptRunner.runPostInspectionScript(project, panelSerialNumber, boardNameToSerialNumberMap, currentXMLResultFullPath);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      // return the config file settings to their original state
      try
      {
        scriptRunner.setPreInspectionScript(origPreInspectionScript);
        scriptRunner.setPostInspectionScript(origPostInspectionScript);
        scriptRunner.setPreInspectionScriptEnabled(origPreScriptEnabled);
        scriptRunner.setPostInspectionScriptEnabled(origPostScriptEnabled);

        if (FileUtilAxi.exists(outputFileNameOrig))
        {
          FileUtilAxi.copy(outputFileNameOrig, outputFileName);
          FileUtilAxi.delete(outputFileNameOrig);
        }

      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }
}
