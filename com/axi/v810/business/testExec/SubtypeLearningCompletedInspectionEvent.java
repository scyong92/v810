package com.axi.v810.business.testExec;

/**
 * This InspectionEvent is fired by the StatisticalTuningEngine whenever a subtype is learned.
 *
 * @author Sunit Bhalla
 */
public class SubtypeLearningCompletedInspectionEvent extends InspectionEvent
{

  /*
   * @author Sunit Bhalla
   */
  public SubtypeLearningCompletedInspectionEvent()
  {
    super(InspectionEventEnum.SUBTYPE_LEARNING_COMPLETED);
  }
}
