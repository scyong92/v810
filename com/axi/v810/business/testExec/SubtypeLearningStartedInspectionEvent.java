package com.axi.v810.business.testExec;

import com.axi.util.Assert;
import com.axi.v810.business.panelSettings.Subtype;

/**
 * This InspectionEvent is fired by the StatisticalTuningEngine whenever a subtype is starting to be learned.
 *
 * @author Sunit Bhalla
 */
public class SubtypeLearningStartedInspectionEvent extends InspectionEvent
{

  private Subtype _subtype;

  /*
   * @author Sunit Bhalla
   */
  public SubtypeLearningStartedInspectionEvent(Subtype subtype)
  {
    super(InspectionEventEnum.SUBTYPE_LEARNING_STARTED);
    Assert.expect(subtype != null);

    _subtype = subtype;
  }

  /**
   * @author Sunit Bhalla
   */
  public Subtype getSubtype()
  {
    Assert.expect(_subtype != null);

    return _subtype;
  }

}
