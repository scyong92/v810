package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;

/**
 * Specialization of InspectionEvent to represent events related to the interactive learning
 * initializing screen.
 *
 * @author Matt Wharton
 * @author George David
 * @author George Booth
 */
public class InteractiveLearningCompletedInspectionEvent extends InspectionEvent
{
  private List<String> _stringList;

  /**
   * Constructor.
   *
   * @author George Booth
   */
  public InteractiveLearningCompletedInspectionEvent(InspectionEventEnum eventEnum)
  {
    super(eventEnum);
    Assert.expect(eventEnum.equals(InspectionEventEnum.INTERACTIVE_LEARNING_COMPLETED));
  }

  /**
   * @author George Booth
   */
  public void setStringData(List<String> stringList)
  {
    Assert.expect(stringList != null);

    _stringList = stringList;
  }

  /**
   * @author George Booth
   */
  public List<String> getStringList()
  {
    Assert.expect(_stringList != null);

    return _stringList;
  }

  /**
   * @author George Booth
   */
  public boolean hasStringList()
  {
    return (_stringList != null);
  }

}
