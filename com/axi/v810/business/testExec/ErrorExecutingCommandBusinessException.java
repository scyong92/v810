package com.axi.v810.business.testExec;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class ErrorExecutingCommandBusinessException extends BusinessException
{
  /**
   * @author Bill Darbie
   */
  public ErrorExecutingCommandBusinessException(String command)
  {
    super(new LocalizedString("BUS_ERROR_EXECUTING_COMMAND_EXCEPTION_KEY", new Object[]{command}));
    Assert.expect(command != null);
  }
}
