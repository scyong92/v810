package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Khaw Chek Hau
 * @VOne Machine Utilization Feature
 */
public class VOneMachineStatusMonitoringMessageInspectionEvent extends MessageInspectionEvent
{
  private DatastoreException _datastoreException;

  /**
   * @author Khaw Chek Hau
   * @VOne Machine Utilization Feature
   */
  public VOneMachineStatusMonitoringMessageInspectionEvent(DatastoreException datastoreException)
  {
    super(datastoreException.getLocalizedString());
    _datastoreException = datastoreException;
  }

  /**
   * @author Khaw Chek Hau
   * @VOne Machine Utilization Feature
   */
  public DatastoreException getDatastoreException()
  {
    Assert.expect(_datastoreException != null);

    return _datastoreException;
  }
}
