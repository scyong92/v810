package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * Specialization of InspectionEvent to represent events related to alignment.
 *
 * @author Matt Wharton
 * @author George David
 */
public class AlignmentInspectionEvent extends InspectionEvent
{
  private AlignmentGroup _alignmentGroup;
  private Image _alignmentImage;
  private MeasurementRegionDiagnosticInfo _locatedAlignmentFeaturesDiagInfo;
  private MeasurementRegionDiagnosticInfo _locatedContextAlignmentFeaturesDiagInfo;
  private PanelLocationInSystemEnum _panelLocationInSystem;
  private MagnificationTypeEnum _magnificationType;

  private int _testSubProgramId = -1;
  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  public AlignmentInspectionEvent(InspectionEventEnum eventEnum)
  {
    super(eventEnum);

    Assert.expect(eventEnum.equals(InspectionEventEnum.MANUAL_ALIGNMENT_STARTED)
                  || eventEnum.equals(InspectionEventEnum.MANUAL_ALIGNMENT_COMPLETED)
                  || eventEnum.equals(InspectionEventEnum.WAITING_FOR_USER_TO_ALIGN_IMAGE)
                  || eventEnum.equals(InspectionEventEnum.MANUAL_ALIGNMENT_VERIFICATION_STARTED)
                  || eventEnum.equals(InspectionEventEnum.MANUAL_ALIGNMENT_VERIFICATION_COMPLETED)
                  || eventEnum.equals(InspectionEventEnum.ALIGNED_ON_IMAGE)
                  || eventEnum.equals(InspectionEventEnum.RUNTIME_ALIGNMENT_STARTED)
                  || eventEnum.equals(InspectionEventEnum.RUNTIME_ALIGNMENT_COMPLETED)
                  || eventEnum.equals(InspectionEventEnum.WAITING_FOR_USER_TO_CROP_IMAGE)
                  || eventEnum.equals(InspectionEventEnum.SAVE_CROP_IMAGE)
                  || eventEnum.equals(InspectionEventEnum.RESET_ZOOM_FACTOR)
                  || eventEnum.equals(InspectionEventEnum.ALIGNED_ON_2D_IMAGE)
                  || eventEnum.equals(InspectionEventEnum.MANUAL_2D_ALIGNMENT_STARTED)
                  || eventEnum.equals(InspectionEventEnum.MANUAL_2D_ALIGNMENT_COMPLETED)
                  || eventEnum.equals(InspectionEventEnum.ENABLE_NEXT_BUTTON_AFTER_IMAGE_CROPPED)
                  || eventEnum.equals(InspectionEventEnum.DISABLE_NEXT_BUTTON_BEFORE_IMAGE_CROPPED)
                  || eventEnum.equals(InspectionEventEnum.DETERMINE_CROPPED_ALIGNMENT_REGION));
  }

  /**
   * @author George A. David
   */
  public void setAlignmentGroup(AlignmentGroup alignmentGroup)
  {
    Assert.expect(alignmentGroup != null);

    _alignmentGroup = alignmentGroup;
  }

  /**
   * @author George A. David
   */
  public AlignmentGroup getAlignmentGroup()
  {
    Assert.expect(_alignmentGroup != null);

    return _alignmentGroup;
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasAlignmentGroup()
  {
    return (_alignmentGroup != null);
  }

  /**
   * @author Andy Mechtenberg
   */
  public Project getProject()
  {
    return _alignmentGroup.getProject();
  }

  /**
   * @author Matt Wharton
   */
  public void setImage(Image alignmentImage)
  {
    Assert.expect(alignmentImage != null);
    if (_alignmentImage != null)
    {
      _alignmentImage.decrementReferenceCount();
    }

    _alignmentImage = alignmentImage;
    _alignmentImage.incrementReferenceCount();
  }

  /**
   * @author Matt Wharton
   */
  public Image getImage()
  {
    Assert.expect(_alignmentImage != null);

    return _alignmentImage;
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasImage()
  {
    return (_alignmentImage != null);
  }

  /**
   * @author Matt Wharton
   */
  public void setLocatedAlignmentFeaturesDiagnosticInfo(MeasurementRegionDiagnosticInfo locatedAlignmentFeaturesDiagInfo)
  {
    Assert.expect(locatedAlignmentFeaturesDiagInfo != null);

    _locatedAlignmentFeaturesDiagInfo = locatedAlignmentFeaturesDiagInfo;
  }

  /**
   * @author Matt Wharton
   */
  public MeasurementRegionDiagnosticInfo getLocatedAlignmentFeaturesDiagnosticInfo()
  {
    Assert.expect(_locatedAlignmentFeaturesDiagInfo != null);

    return _locatedAlignmentFeaturesDiagInfo;
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasLocatedAlignmentFeaturesDiagnosticInfo()
  {
    return (_locatedAlignmentFeaturesDiagInfo != null);
  }

  /**
   * @author Matt Wharton
   */
  public void setLocatedAlignmentContextFeaturesDiagnosticInfo(MeasurementRegionDiagnosticInfo locatedAlignmentContextFeaturesDiagInfo)
  {
    Assert.expect(locatedAlignmentContextFeaturesDiagInfo != null);

    _locatedContextAlignmentFeaturesDiagInfo = locatedAlignmentContextFeaturesDiagInfo;
  }

  /**
   * @author Matt Wharton
   */
  public MeasurementRegionDiagnosticInfo getLocatedAlignmentContextFeaturesDiagnosticInfo()
  {
    Assert.expect(_locatedContextAlignmentFeaturesDiagInfo != null);

    return _locatedContextAlignmentFeaturesDiagInfo;
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasLocatedAlignmentContextFeaturesDiagnosticInfo()
  {
    return (_locatedContextAlignmentFeaturesDiagInfo != null);
  }

  /**
   * @author Matt Wharton
   */
  public PanelLocationInSystemEnum getPanelLocationInSystem()
  {
    Assert.expect(_panelLocationInSystem != null);

    return _panelLocationInSystem;
  }

  /**
   * @author Matt Wharton
   */
  public void setPanelLocationInSystem(PanelLocationInSystemEnum panelLocationInSystem)
  {
    Assert.expect(panelLocationInSystem != null);

    _panelLocationInSystem = panelLocationInSystem;
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasPanelLocationInSystem()
  {
    return (_panelLocationInSystem != null);
  }

  /**
   * @author Patrick Lacz
   */
  public void incrementReferenceCount()
  {
    if (_alignmentImage != null)
      _alignmentImage.incrementReferenceCount();
  }

  /**
   * @author Patrick Lacz
   */
  public void decrementReferenceCount()
  {
    if (_alignmentImage != null)
      _alignmentImage.decrementReferenceCount();
  }

  /**
   * @return the _magnificationType
   */
  public MagnificationTypeEnum getMagnificationType()
  {
    return _magnificationType;
  }

  /**
   * @param magnificationType the _magnificationType to set
   */
  public void setMagnificationType(MagnificationTypeEnum magnificationType)
  {
    this._magnificationType = magnificationType;
  }

  /**
   * @author Lim, Seng Yew
   */
  public int getTestSubProgramId()
  {
    return _testSubProgramId;
  }

  /**
   * @author Lim, Seng Yew
   */
  public void setTestSubProgramId(int testSubProgramId)
  {
    _testSubProgramId = testSubProgramId;
  }
}
