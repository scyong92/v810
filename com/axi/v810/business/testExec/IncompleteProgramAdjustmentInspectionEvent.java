package com.axi.v810.business.testExec;

import com.axi.util.LocalizedString;

/**
 * @author Laura Cormos
 */
public class IncompleteProgramAdjustmentInspectionEvent extends MessageInspectionEvent
{
  public IncompleteProgramAdjustmentInspectionEvent(LocalizedString messageText)
  {
    super(messageText);
  }
}

