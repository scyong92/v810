package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * This exception is thrown if an alignment group is located too far away from it's expected position.
 *
 * @author Matt Wharton
 */
public class AlignmentGroupLocatedTooFarFromExpectedLocationBusinessException extends AlignmentBusinessException
{
  public AlignmentGroupLocatedTooFarFromExpectedLocationBusinessException(TestSubProgram testSubProgram,
                                                                          AlignmentGroup alignmentGroup)
  {
    super(testSubProgram, 
          new LocalizedString("BS_ALIGNMENT_GROUP_LOCATED_TOO_FAR_FROM_EXPECTED_LOCATION_KEY",
                              new Object[]{testSubProgram.getMagnificationType().toString(), alignmentGroup.getName()}));
  }
}
