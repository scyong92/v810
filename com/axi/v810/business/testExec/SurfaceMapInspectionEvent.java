package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 * @author Cheah Lee Herng
 */
public class SurfaceMapInspectionEvent extends InspectionEvent
{
    List<OpticalPointToPointScan> _opticalPointToPointScans = new ArrayList<OpticalPointToPointScan>();
    
    /**
     * @author Cheah Lee Herng
     */
    public SurfaceMapInspectionEvent(InspectionEventEnum eventEnum)
    {
        super(eventEnum);
        
        Assert.expect(eventEnum.equals(InspectionEventEnum.RUNTIME_SURFACE_MAP_STARTED)
                  || eventEnum.equals(InspectionEventEnum.RUNTIME_SURFACE_MAP_COMPLETED)
                  || eventEnum.equals(InspectionEventEnum.SURFACE_MAP_ON_OPTICAL_IMAGE)
                  || eventEnum.equals(InspectionEventEnum.RUNTIME_ALIGNMENT_SURFACE_MAP_STARTED)
                  || eventEnum.equals(InspectionEventEnum.RUNTIME_ALIGNMENT_SURFACE_MAP_COMPLETED)
                  || eventEnum.equals(InspectionEventEnum.ALIGNMENT_SURFACE_MAP_ON_OPTICAL_IMAGE));
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public void setOpticalPointToPointScans(List<OpticalPointToPointScan> opticalPointToPointScans)
    {
        Assert.expect(opticalPointToPointScans != null);
        _opticalPointToPointScans = opticalPointToPointScans;
    }
    
    /**
     * @author Cheah Lee herng
     */
    public List<OpticalPointToPointScan> getOpticalPointToPointScans()
    {
        Assert.expect(_opticalPointToPointScans != null);
        return _opticalPointToPointScans;
    }
    
    /**
     * @author Cheah Lee Herng     
     */
    public boolean hasOpticalPointToPointScans()
    {
        Assert.expect(_opticalPointToPointScans != null);
        return (_opticalPointToPointScans.size() > 0);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public int getTotalNumberOfPointToPointScans()
    {
        Assert.expect(_opticalPointToPointScans != null);
        return _opticalPointToPointScans.size();
    }
}
