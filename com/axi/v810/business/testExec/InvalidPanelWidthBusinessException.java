package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Bill Darbie
 */
public class InvalidPanelWidthBusinessException extends BusinessException
{
  /**
   * @author Bill Darbie
   */
  public InvalidPanelWidthBusinessException(MathUtilEnum units, double minWidthInUnits, double maxWidthInUnits, double actualWidthInUnits)
  {
    super(new LocalizedString("BUS_INVALID_PANEL_WIDTH_EXCEPTION_KEY", new Object[]{units, minWidthInUnits, maxWidthInUnits, actualWidthInUnits}));
    Assert.expect(units != null);
  }
}
