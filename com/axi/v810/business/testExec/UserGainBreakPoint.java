package com.axi.v810.business.testExec;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 *
 * @author khang-wah.chnee
 */
public class UserGainBreakPoint extends ScanPassBreakPoint
{
  private int _regionsThatMustBeMarkedCompletePriorToHardwareChange = -1;
  private double _userGainValue;
  private ScanPass _scanPass;
  
  /**
   * @author Khang Wah, 2016-05-05, MVEDR 
   */
  public UserGainBreakPoint(ScanPass scanPass,
                            int regionThatMustBeMarkComplete,
                            double userGainValue, 
                            double speedFactor)
  {
    super(scanPass, speedFactor);
    
    Assert.expect(regionThatMustBeMarkComplete >= 0);
    Assert.expect(userGainValue >= 1);
    
    _scanPass = scanPass;
    _regionsThatMustBeMarkedCompletePriorToHardwareChange = regionThatMustBeMarkComplete;
    _userGainValue = userGainValue;
  }
  
  /**
   * @author Khang Wah, 2016-05-05, MVEDR 
   */
  public void executeAction() throws XrayTesterException 
  {
    
    TimerUtil timer = new TimerUtil();
    timer.restart();
    XrayCameraArray.getInstance().setUserDefinedCamerasLight(_userGainValue);
    timer.stop();
    if (_debug)
      System.out.println("Set User Gain : " + _userGainValue);
  }
  
  /**
   * @author Khang Wah, 2016-05-05, MVEDR
   */
  public int getNumberOfRegionsThatMustBeMarkedCompletePriorToExecutingAction() 
  {    
    return _regionsThatMustBeMarkedCompletePriorToHardwareChange;
  }
  
  /**
   * @author Khang Wah, 2016-05-05, MVEDR 
   */
  public void clearBreakPoint() 
  {
    // Do nothing
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public ScanPassBreakPointEnum getBreakPointEnum()
  {
    return ScanPassBreakPointEnum.USER_GAIN;
  }
}
