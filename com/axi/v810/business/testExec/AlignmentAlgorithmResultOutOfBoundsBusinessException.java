package com.axi.v810.business.testExec;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.TestSubProgram;

/**
 * This exception is thrown if a calculated alignment transform is out of acceptable bounds.
 *
 * @author Matt Wharton
 */
public class AlignmentAlgorithmResultOutOfBoundsBusinessException extends AlignmentBusinessException
{
  /**
   * @author Matt Wharton
   */
  public AlignmentAlgorithmResultOutOfBoundsBusinessException(TestSubProgram testSubProgram,
                                                              AlignmentGroup alignmentGroup1,
                                                              AlignmentGroup alignmentGroup2,
                                                              AlignmentGroup alignmentGroup3)
  {
    super(testSubProgram, 
          new LocalizedString("BS_ALIGNMENT_ALGORITHM_RESULT_OUT_OF_BOUNDS_KEY",
                              new Object[]{testSubProgram.getMagnificationType().toString(),
                                          alignmentGroup1.getName(), 
                                          alignmentGroup2.getName(), 
                                          alignmentGroup3.getName()}));
  }
}
