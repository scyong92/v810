package com.axi.v810.business;

import java.io.*;
import java.net.*;
import java.nio.channels.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;

/**
 * Tests the project repository class
 * @author George A. David
 */
public class Test_ProjectDatabase extends UnitTest
{
  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String origRepositoryPath = Directory.getProjectDatabaseDir();
    String tempDir = Directory.getTempDir();
    String repositoryPath = tempDir + File.separator + "database";
    try
    {
      FileUtil.backupFile(FileName.getSoftwareConfigFullPath());

      //create the repository
      if (FileUtil.exists(repositoryPath) == false)
      {
        try
        {
          FileUtil.createDirectory(repositoryPath);
        }
        catch (CouldNotCreateFileException ex)
        {
          ex.printStackTrace();
          Assert.expect(false);
        }
      }

      // set the repository path
      Directory.setProjectDatabaseDir(repositoryPath);

      //test add project
      try
      {
        ProjectDatabase.addProject(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      try
      {
        ProjectDatabase.getProject(null, 0);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

//      String projectName = "projectDatabaseTest";
      String projectName = "families_all_rlv";

      // try retreiving a project that does not exist in the repository
      try
      {
        ProjectDatabase.getProject(projectName, 1);
        Expect.expect(false);
      }
      catch (DatastoreException ex)
      {
        // do nothing
      }

      Expect.expect(ProjectDatabase.getProjectSummaries().isEmpty());

      Project project = Project.importProjectFromNdfs(projectName, new LinkedList<LocalizedString>());
//      project.getTestProgram();

      int origMajorVersion = project.getTestProgramVersion();
      int origMinorVersion = project.getMinorVersion();

      project.setTestProgramVersion(3);
      project.setMinorVersion(0);
      project.save();

      ProjectDatabase.addProject(projectName);
      Expect.expect(ProjectDatabase.doesProjectExist(projectName));

      Expect.expect(ProjectDatabase.doesNewerProjectExist(projectName) == false);
      project.setTestProgramVersion(2);
      project.save();
      Expect.expect(ProjectDatabase.doesNewerProjectExist(projectName));
      project.setTestProgramVersion(3);
      project.save();
      Expect.expect(ProjectDatabase.doesNewerProjectExist(projectName) == false);
      project.setTestProgramVersion(2);
      project.setMinorVersion(9);
      project.save();
      Expect.expect(ProjectDatabase.doesNewerProjectExist(projectName));
      project.setTestProgramVersion(3);
      project.save();
      Expect.expect(ProjectDatabase.doesNewerProjectExist(projectName) == false);
      project.setMinorVersion(0);
      project.save();
      Expect.expect(ProjectDatabase.doesNewerProjectExist(projectName) == false);

      // ok, now change cad/settings, and set the version to what's in the
      // database, we should get an exception
      long origCadCheckSum = project.getCadXmlChecksum();
      long origSettingsCheckSum = project.getSettingsXmlChecksum();
      int origWidth = project.getPanel().getWidthInNanoMeters();
      project.getPanel().setWidthInNanoMeters(origWidth + 100);
      project.setTestProgramVersion(2);
      project.setMinorVersion(0);
      project.save();
      Assert.expect(origCadCheckSum != project.getCadXmlChecksum());
      Assert.expect(origSettingsCheckSum == project.getSettingsXmlChecksum());
      try
      {
        ProjectDatabase.doesNewerProjectExist(projectName);
        Expect.expect(false);
      }
      catch (ProjectDatabaseVersionConflictBusinessException exception)
      {
        // do nothing
      }
      project.getPanel().setWidthInNanoMeters(origWidth);
      project.setTestProgramVersion(2);
      project.setMinorVersion(0);
      project.save();
      Assert.expect(origCadCheckSum == project.getCadXmlChecksum());
      Assert.expect(origSettingsCheckSum == project.getSettingsXmlChecksum());
      Expect.expect(ProjectDatabase.doesNewerProjectExist(projectName) == false);

      Subtype subtype = project.getPanel().getSubtypes().get(0);
      String userComment = subtype.getUserComment();
      subtype.setUserComment(userComment + "test");
      project.getPanel().setWidthInNanoMeters(origWidth + 100);
      project.setTestProgramVersion(2);
      project.setMinorVersion(0);
      project.save();

      project.getPanel().setWidthInNanoMeters(origWidth);
      project.setTestProgramVersion(2);
      project.setMinorVersion(0);
      project.save();
      Assert.expect(origCadCheckSum == project.getCadXmlChecksum());
      Assert.expect(origSettingsCheckSum != project.getSettingsXmlChecksum());
      try
      {
        ProjectDatabase.doesNewerProjectExist(projectName);
        Expect.expect(false);
      }
      catch (ProjectDatabaseVersionConflictBusinessException exception)
      {
        // do nothing
      }
      subtype.setUserComment(userComment);
      project.getPanel().setWidthInNanoMeters(origWidth + 100);
      project.setTestProgramVersion(2);
      project.setMinorVersion(0);
      project.save();

      project.getPanel().setWidthInNanoMeters(origWidth);
      project.setTestProgramVersion(2);
      project.setMinorVersion(0);
      project.save();
      Expect.expect(ProjectDatabase.doesNewerProjectExist(projectName) == false);


      List<ProjectSummary> projectSummaries = ProjectDatabase.getProjectSummaries();
      Expect.expect(projectSummaries.size() == 1);
      ProjectSummary projectSummary = projectSummaries.get(0);
      Expect.expect(projectSummary.getProjectName().equalsIgnoreCase(projectName));
      Expect.expect(projectSummary.getDatabaseVersion() == 1);

      projectName = projectName.toUpperCase();
      ProjectDatabase.addProject(projectName);

      Expect.expect(ProjectDatabase.doesProjectExist(projectName));

      projectSummaries = ProjectDatabase.getProjectSummaries();
      Expect.expect(projectSummaries.size() == 2);
      boolean foundVersion1 = false;
      boolean foundVersion2 = false;
      for (ProjectSummary summary : projectSummaries)
      {
        Expect.expect(summary.getProjectName().equalsIgnoreCase(projectName));
        if (summary.getDatabaseVersion() == 1)
          foundVersion1 = true;
        else if (summary.getDatabaseVersion() == 2)
          foundVersion2 = true;
        else
          Assert.expect(false);
      }
      Expect.expect(foundVersion1);
      Expect.expect(foundVersion2);

      ProjectDatabase.getProject(projectName, 1);

      try
      {
        ProjectDatabase.doesProjectExist(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      Expect.expect(ProjectDatabase.doesProjectExist(projectName));

      // try deleting the project v1
      ProjectDatabase.deleteProject(projectName, 1);
      Expect.expect(ProjectDatabase.doesProjectExist(projectName));

      // try deleting the project v2
      ProjectDatabase.deleteProject(projectName, 2);
      Expect.expect(ProjectDatabase.doesProjectExist(projectName) == false);

      project.setTestProgramVersion(origMajorVersion);
      project.setMinorVersion(origMinorVersion);
      project.save();

      // added by Wei Chin, Chong (08 July 2008) - handling the parenthesis
      String projectNameForRegExTest = "0027parts(1).2";

      Assert.expect(ProjectDatabase.doesProjectExist(projectNameForRegExTest) == false);

      ProjectDatabase.addProject(projectNameForRegExTest);
      ProjectDatabase.getProject(projectNameForRegExTest);
      ProjectDatabase.deleteProject(projectNameForRegExTest, 1);

    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        FileUtil.restoreBackupFile(FileName.getSoftwareConfigFullPath());
      }
      catch (Exception ex1)
      {
        ex1.printStackTrace();
      }

      if (FileUtil.exists(repositoryPath))
      {
        try
        {
          FileUtil.delete(repositoryPath);
        }
        catch (CouldNotDeleteFileException ex)
        {
          ex.printStackTrace();
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
//    debugTest();
    UnitTest.execute(new Test_ProjectDatabase());
//    Pattern pattern = Pattern.compile("((\\\\#|[^#])*).*");
//    Pattern pattern = Pattern.compile("(([^#]|\\\\#)*).*");
//    Matcher matcher = pattern.matcher("test\\#test#test");
//    if(matcher.matches())
//    {
//      System.out.println("matches");
//      System.out.println(matcher.group(0));
//      System.out.println(matcher.group(1));
//    }
//    else
//      System.out.println("does not match");

  }

  /**
   * @author George A. David
   */
  private static void debugTest()
  {
    String dir = "c:/test";
    String images1gb = dir + "/images1gb0.zip";
    String images2gb = dir + "/images2gb.zip";
    String images3gb = dir + "/images3gb.zip";
    String images4gb = dir + "/images4gb.zip";

    String toImages1gb = "t:/images1gb0.zip";
    String toImages2gb = "t:/images2gb.zip";
    String toImages3gb = "t:/images3gb.zip";
    String toImages4gb = "t:/images4gb.zip";


    long totalTimeInMillis = copyFile(images1gb, toImages1gb);
//    totalTimeInMillis += copyFile(images2gb, toImages2gb);
//    totalTimeInMillis += copyFile(images3gb, toImages3gb);
//    totalTimeInMillis += copyFile(images4gb, toImages4gb);
//
//    System.out.println("Time to copy data via share drive: " + totalTimeInMillis / 60000);
//
//    String urlPath = "ftp://anonymous:com@axibuild/";
//    toImages1gb = urlPath + "images1gbftp.zip";
//    toImages2gb = urlPath + "images2gbftp.zip";
//    toImages3gb = urlPath + "images3gbftp.zip";
//    toImages4gb = urlPath + "images4gbftp.zip";
//    totalTimeInMillis = sendFile(images1gb, toImages1gb);
//    totalTimeInMillis += sendFile(images2gb, toImages2gb);
//    totalTimeInMillis += sendFile(images3gb, toImages3gb);
//    totalTimeInMillis += sendFile(images4gb, toImages4gb);
//
//    System.out.println("Time to send data via ftp: " + totalTimeInMillis / 60000);
  }

  /**
   * @author George A. David
   */
  public static long sendFile(String fromPath, String urlPath)
  {
    TimerUtil timer = new TimerUtil();
    timer.start();
    long currentTimeInMillis = 0;
    try
    {
//      URL url = new URL(urlPath);
//      URLConnection con = url.openConnection();
//      BufferedOutputStream out = new BufferedOutputStream(con.getOutputStream());
//      FileInputStream in = new FileInputStream(fromPath);
//      int i = 0;
//      byte[] bytesIn = new byte[10000000];
//      while ((i = in.read(bytesIn)) >= 0)
//      {
//        out.write(bytesIn, 0, i);
//      }
//      out.close();
//      in.close();
//
      URI uri = new URI(urlPath);
      FileOutputStream out = new FileOutputStream(new File(uri));
      FileInputStream in = new FileInputStream(fromPath);
      FileChannel fcIn = in.getChannel();
      FileChannel fcOut = out.getChannel();

      long size1 = new File(fromPath).length();
      long totalBytesTransfered = 0;
      long startingFilePosition = 0;
      long numBytesToTransfer = size1;
      long maxBytesToTransfer = 10000000;
      if(numBytesToTransfer > maxBytesToTransfer)
        numBytesToTransfer = maxBytesToTransfer;
      while(totalBytesTransfered != size1)
      {
        long numBytesTransfered = fcOut.transferFrom(fcIn, startingFilePosition, numBytesToTransfer);
        Assert.expect(numBytesToTransfer == numBytesTransfered);
        totalBytesTransfered += numBytesTransfered;
        if(size1 - totalBytesTransfered < numBytesToTransfer)
          numBytesToTransfer = size1 - totalBytesTransfered;
        startingFilePosition += numBytesTransfered;
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }

    timer.stop();
    currentTimeInMillis = timer.getElapsedTimeInMillis();
    System.out.println("Time to send " + fromPath + ": " + currentTimeInMillis / 60000);
    return currentTimeInMillis;
  }

  /**
   * @author George A. David
   */
  private static long copyFile(String fromPath, String toPath)
  {
    TimerUtil timer = new TimerUtil();
    long currentTimeInMillis = 0;
    try
    {
      timer.start();
      FileUtil.copy(fromPath, toPath);
      timer.stop();
      currentTimeInMillis = timer.getElapsedTimeInMillis();
    }
    catch (Exception exception)
    {
      exception.printStackTrace();
      Assert.expect(false);
    }

    System.out.println("Time to copy " + fromPath + ": " + (double)currentTimeInMillis / 60000.0);
    return currentTimeInMillis;
  }
}
