package com.axi.v810.business;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class ProjectChangeEvent
{
  private Object _source;
  private Object _oldValue;
  private Object _newValue;
  private ProjectChangeEventEnum _projectChangeEventEnum;

  /**
   * @author Bill Darbie
   */
  public ProjectChangeEvent(Object source, Object oldValue, Object newValue, ProjectChangeEventEnum projectChangeEventEnum)
  {
    Assert.expect(source != null);
    // oldValue can be null
    // newValue can be null
    Assert.expect(projectChangeEventEnum != null);

    _source = source;
    _oldValue = oldValue;
    _newValue = newValue;
    _projectChangeEventEnum = projectChangeEventEnum;
  }

  /**
   * @author Bill Darbie
   */
  public Object getSource()
  {
    Assert.expect(_source != null);

    return _source;
  }

  /**
   * @author Bill Darbie
   */
  public Object getOldValue()
  {
    // can return null
    return _oldValue;
  }

  /**
   * @author Bill Darbie
   */
  public Object getNewValue()
  {
    // can return null
    return _newValue;
  }

  /**
   * @author Bill Darbie
   */
  public ProjectChangeEventEnum getProjectChangeEventEnum()
  {
    Assert.expect(_projectChangeEventEnum != null);

    return _projectChangeEventEnum;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isCreated()
  {
    Assert.expect(_projectChangeEventEnum != null);

    if (_projectChangeEventEnum.isCreateOrDestroy() && (_oldValue == null) && (_newValue != null))
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isDestroyed()
  {
    Assert.expect(_projectChangeEventEnum != null);

    if (_projectChangeEventEnum.isCreateOrDestroy() && (_oldValue != null) && (_newValue == null))
      return true;

    return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isRemoved()
  {
    Assert.expect(_projectChangeEventEnum != null);

    if (_projectChangeEventEnum.isAddOrRemove() && (_oldValue != null) && (_newValue == null))
      return true;

    return false;
  }

  /**
   * @author Laura Cormos
   */
  public boolean isAdded()
  {
    Assert.expect(_projectChangeEventEnum != null);

    if (_projectChangeEventEnum.isAddOrRemove() && (_oldValue == null) && (_newValue != null))
      return true;

    return false;
  }
}
