package com.axi.v810.business;

/**
 * @author siew-yeng.phang
 */
public class SerialNumberMappingExceptionEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  
  public static SerialNumberMappingExceptionEnum BARCODE_CONFIG_FOR_RECIPE_AND_CONFIG_USED_IN_PRODUCTION_NOT_SAME = new SerialNumberMappingExceptionEnum(++_index);
  public static SerialNumberMappingExceptionEnum SERIAL_NUMBER_MAPPING_SETTING_NOT_TALLY_WITH_BARCODE_READER_SETUP = new SerialNumberMappingExceptionEnum(++_index);
  public static SerialNumberMappingExceptionEnum SERIAL_NUMBER_MAPPING_SETTING_INVALID = new SerialNumberMappingExceptionEnum(++_index);
  
  /*
   * @author Phang Siew Yeng
   */
  private SerialNumberMappingExceptionEnum(int id)
  {
    super(id);
  }
}
