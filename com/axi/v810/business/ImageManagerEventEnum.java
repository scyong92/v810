package com.axi.v810.business;



/**
 * @author George A. David
 */
public class ImageManagerEventEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static ImageManagerEventEnum INSPECTION_IMAGE_SAVED = new ImageManagerEventEnum(++_index);
  public static ImageManagerEventEnum VERIFICATION_IMAGE_SAVED = new ImageManagerEventEnum(++_index);
  public static ImageManagerEventEnum ADJUST_FOCUS_IMAGE_SAVED = new ImageManagerEventEnum(++_index);
  public static ImageManagerEventEnum VERIFICATION_IMAGE_DELETED = new ImageManagerEventEnum(++_index);

  /**
   * @author George A. David
   */
  private ImageManagerEventEnum(int id)
  {
    super(id);
  }
}
