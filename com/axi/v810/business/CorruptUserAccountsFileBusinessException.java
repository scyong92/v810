package com.axi.v810.business;

import com.axi.util.*;

/**
 * This exception is thrown when the user accounts file cannot be read as a serialized file
 *
 * @author George Booth
 */
public class CorruptUserAccountsFileBusinessException extends BusinessException
{
  /**
   * @author George Booth
   */
  public CorruptUserAccountsFileBusinessException(String userAccountsFilePath)
  {
    super(new LocalizedString("BS_CORRUPT_USER_ACCOUNTS_FILE_EXCEPTION_KEY",
                              new Object[]{userAccountsFilePath}));
    Assert.expect(userAccountsFilePath != null);
  }
}
