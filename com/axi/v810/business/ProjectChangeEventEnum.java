package com.axi.v810.business;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class ProjectChangeEventEnum extends com.axi.util.Enum
{
  private ProjectChangeTypeEnum _projectChangeTypeEnum;

  /**
   * @author Bill Darbie
   */
  protected ProjectChangeEventEnum(int id)
  {
    super(id);
    _projectChangeTypeEnum = ProjectChangeTypeEnum.SET;
  }

  /**
   * @author Bill Darbie
   */
  protected ProjectChangeEventEnum(int id, ProjectChangeTypeEnum projectChangeTypeEnum)
  {
    super(id);
    Assert.expect(projectChangeTypeEnum != null);
    _projectChangeTypeEnum = projectChangeTypeEnum;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isCreateOrDestroy()
  {
    if (_projectChangeTypeEnum.equals(ProjectChangeTypeEnum.CREATE_OR_DESTROY))
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isAddOrRemove()
  {
    if (_projectChangeTypeEnum.equals(ProjectChangeTypeEnum.ADD_OR_REMOVE))
      return true;

    return false;
  }
}
