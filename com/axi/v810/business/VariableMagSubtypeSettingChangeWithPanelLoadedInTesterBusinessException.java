package com.axi.v810.business;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class VariableMagSubtypeSettingChangeWithPanelLoadedInTesterBusinessException extends BusinessException
{
    public VariableMagSubtypeSettingChangeWithPanelLoadedInTesterBusinessException(String projectName)
    {
        super(new LocalizedString("BUS_SUBTYPE_CONTAINED_HIGH_MAG_COMPONENTS_IN_TESTER_EXCEPTION_KEY",
                              new Object[]{projectName}));
    }
}
