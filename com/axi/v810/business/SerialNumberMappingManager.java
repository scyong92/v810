package com.axi.v810.business;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author siew-yeng.phang
 */
public class SerialNumberMappingManager
{
  private static SerialNumberMappingManager _instance;
  
  private BarcodeReaderManager _barcodeReaderManager;
  private SerialNumberMappingSettingsReader _reader;
  private SerialNumberMappingSettingsWriter _writer;
  private List<SerialNumberMappingData> _serialNumberMappingDataList;
  private String _barcodeReaderConfigurationNameUsed;
  private Project _project;
  
  private static final int _DEFAULT_SCAN_SEQUENCE = 1;

  /**
   * @author Phang Siew Yeng
   */
  public static synchronized SerialNumberMappingManager getInstance()
  {
    if (_instance == null)
    {
      _instance = new SerialNumberMappingManager();
    }

    return _instance;
  }

  /**
   * @author Phang Siew Yeng
   */
  SerialNumberMappingManager()
  {
    _barcodeReaderManager = BarcodeReaderManager.getInstance();
    _reader = SerialNumberMappingSettingsReader.getInstance();
    _writer = SerialNumberMappingSettingsWriter.getInstance();

    _project = null;
    _serialNumberMappingDataList = new ArrayList<SerialNumberMappingData>();
  }

  /*
   * @author Phang Siew Yeng
   */
  public void clear()
  {   
    if(_project == null)
      return;
    
    if(isSerialNumberMappingEnabled() == false)
      return;
    
    String projectTempDir = Directory.getProjectTempDir(_project.getName());
    if(FileUtil.existsDirectory(projectTempDir))
    {
      try
      {
        FileUtil.deleteFileOrDirectory(projectTempDir);
      }
      catch (CouldNotDeleteFileException ex)
      {
        
      }
    }
    _project = null;
    _serialNumberMappingDataList.clear();
    _barcodeReaderConfigurationNameUsed = null;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public boolean isSerialNumberMappingFileExists()
  {
    if(isSerialNumberMappingEnabled())
    {
      if (getSerialNumberMappingDataList().isEmpty())
        return false;

      return true;
    }
    else
      return false;
  }

  /**
   * @author Phang Siew Yeng
   */
  public void checkIfSerialNumberMappingAndBarcodeReaderSetupAreTally() throws XrayTesterException
  {
    if(isSerialNumberMappingEnabled() == false)
      return;
    if(_barcodeReaderManager.getBarcodeReaderAutomaticReaderEnabled() == false)
      return;

    if(getBarcodeReaderConfigurationNameUsed() != null)
    {
      if(_barcodeReaderManager.getBarcodeReaderConfigurationNameToUse().equals(getBarcodeReaderConfigurationNameUsed()) == false)
      {
        String[] parameters = {_barcodeReaderManager.getBarcodeReaderConfigurationNameToUse(),
                              getBarcodeReaderConfigurationNameUsed()};
        SerialNumberMappingException snme = SerialNumberMappingException.getBarcodeConfigForRecipeAndConfigUsedInProductionNotSameException(parameters);

        throw snme;
      }

      if(getNoOfScannerNeeded() > _barcodeReaderManager.getBarcodeReaderNumberOfScanners())
      {
        Object[] parameters = {getNoOfScannerNeeded(),
                              _barcodeReaderManager.getBarcodeReaderNumberOfScanners()};
        SerialNumberMappingException snme = SerialNumberMappingException.getSerialNumberMappingSettingNotTallyWithBarcodeReaderSetupException(parameters);
        throw snme;
      }
      
      for(int br = 0 ; br < _barcodeReaderManager.getBarcodeReaderNumberOfScanners() ; br++)
      {
        if(getScanSequences(br+1).isEmpty())
          continue;

        for(int sequence = 0;sequence < getScanSequences(br+1).size() ; sequence++)
        {
          if(getScanSequences(br+1).contains(sequence+1) == false)
          {
            SerialNumberMappingException snme = SerialNumberMappingException.getSerialNumberMappingSettingInvalidException();
            throw snme;
          }
        }
      }
    }
  }

  /*
   * @author Phang Siew Yeng
   */
  public List<Integer> getBarcodeReadersNeeded()
  {
    List<Integer> barcodeReaders = new ArrayList<Integer>();
    for (SerialNumberMappingData data : getSerialNumberMappingDataList())
    {
      if (barcodeReaders.contains(data.getBarcodeReaderId()) == false)
      {
        barcodeReaders.add(data.getBarcodeReaderId());
      }
    }
    return barcodeReaders;
  }
  
  /**
   * Return number of barcode reader needed for serial number mapping
   * @author Phang Siew Yeng
   */
  public int getNoOfScannerNeeded()
  {
    return getBarcodeReadersNeeded().size();
  }

  /*
   * Get current Serial Number Mapping Data.
   * @author Phang Siew Yeng
   */
  public List<SerialNumberMappingData> getSerialNumberMappingDataList()
  {    
    if (_serialNumberMappingDataList.isEmpty())
    {
      _serialNumberMappingDataList = getLastSavedSerialNumberMappingDataList();
    }

    return _serialNumberMappingDataList;
  }

  /*
   * Get default Serial Number Mapping Data.
   * @author Phang Siew Yeng
   */
  public List<SerialNumberMappingData> getDefaultSerialNumberMappingDataList()
  {
    if(_project == null)
      _project = Project.getCurrentlyLoadedProject();

    List<SerialNumberMappingData> serialNumberMappingDataList = new ArrayList();
    int barcodeReaderId = 1;
    List<String> boardNameList = _project.getPanel().getBoardNames();
    for (String boardName: boardNameList)
    {
      serialNumberMappingDataList.add(new SerialNumberMappingData(_project.getName(),boardName,barcodeReaderId,_DEFAULT_SCAN_SEQUENCE));
      if(barcodeReaderId < _barcodeReaderManager.getBarcodeReaderNumberOfScanners())
        barcodeReaderId++;
    }
    return new ArrayList(serialNumberMappingDataList);
  }

  /*
   * Get last saved Serial Number Mapping Data
   * @author Phang Siew Yeng
   */
  public List<SerialNumberMappingData> getLastSavedSerialNumberMappingDataList()
  {
    if (_project == null)
      _project = Project.getCurrentlyLoadedProject();
    
    List<SerialNumberMappingData> serialNumberMappingData;
    try
    {
      serialNumberMappingData = _reader.getSerialNumberMappingData(_project.getName());
      _barcodeReaderConfigurationNameUsed = _reader.getBarcodeReaderConfigurationNameUsed();
    }
    catch (DatastoreException ex)
    {
      serialNumberMappingData = new ArrayList<SerialNumberMappingData>();
    }

    return new ArrayList(serialNumberMappingData);
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void setSerialNumberMappingDataList(List<SerialNumberMappingData> serialNumberMappingData)
  {
    List<SerialNumberMappingData> oldValue = new ArrayList(_serialNumberMappingDataList);
    
    ProjectObservable.getInstance().setEnabled(false);

    try
    {
      _serialNumberMappingDataList = serialNumberMappingData;
    }
    finally
    {
      ProjectObservable.getInstance().setEnabled(true);
    }

    ProjectObservable.getInstance().stateChanged(this, oldValue,
      _serialNumberMappingDataList,
      SerialNumberMappingEventEnum.SERIAL_NUMBER_MAPPING);
  }

  /*
   * @author Phang Siew Yeng
   */
  public void setSerialNumberMappingBarcodeConfigName(String configName)
  {
    String oldValue = _barcodeReaderConfigurationNameUsed;
    ProjectObservable.getInstance().setEnabled(false);
    try
    {
      _barcodeReaderConfigurationNameUsed = configName;
    }
    finally
    {
      ProjectObservable.getInstance().setEnabled(true);
    }

    ProjectObservable.getInstance().stateChanged(this, oldValue,
      _barcodeReaderConfigurationNameUsed,
      SerialNumberMappingEventEnum.SERIAL_NUMBER_MAPPING_BARCODE_CONFIG_NAME);
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public String getBarcodeReaderConfigurationNameUsed()
  {
    if(_barcodeReaderConfigurationNameUsed == null)
      getSerialNumberMappingDataList();
    
    return _barcodeReaderConfigurationNameUsed;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public void write() throws DatastoreException
  {
    if(_project == null)
      return;
    
    if(isSerialNumberMappingEnabled())
      _writer.write(_project.getName());
  }

  /*
   * Get Serial Number Mapping Data.
   * @author Phang Siew Yeng
   */
  public SerialNumberMappingData getSerialNumberMappingData(String boardName)
  {
    for (SerialNumberMappingData data : getSerialNumberMappingDataList())
    {
      if (data.getBoardName().equals(boardName))
      {
        return data;
      }
    }
    return null;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public String getBoardName(int barcodeReaderId, int scanSequence)
  {
    for (SerialNumberMappingData data : getSerialNumberMappingDataList())
    {
      if (data.getBarcodeReaderId() == barcodeReaderId
        && data.getScanSequence() == scanSequence)
      {
        return data.getBoardName();
      }
    }
    return null;
  }
  
  /*
   * @author Phang Siew Yeng
   */
  public List<Integer> getScanSequences(int barcodeReaderId)
  {
    List<Integer> scanSequences = new ArrayList<Integer>();
    
    for (SerialNumberMappingData data : getSerialNumberMappingDataList())
    {
      if (data.getBarcodeReaderId() == barcodeReaderId)
      {
        scanSequences.add(data.getScanSequence());
      }
    }
    return scanSequences;
  }
  
  /**
   * Map returned barcodes with board name
   * @author Phang Siew Yeng
   */
  public List<String> mapSerialNumbers(List<String> serialNumbers)
  {
    if(isSerialNumberMappingEnabled() == false)
      return serialNumbers;
    
    if (_project == null)
      _project = Project.getCurrentlyLoadedProject();

    List<String> mappedSerialNumbers = new ArrayList(serialNumbers);

    List<String> boardList = _project.getPanel().getBoardNames();
    int counter = 0;
    
    for (int i = 0; i < _barcodeReaderManager.getBarcodeReaderNumberOfScanners(); i++)
    {
      for (int scanSeq = 0; scanSeq < getScanSequences(i + 1).size(); scanSeq++)
      {
        String boardName = getBoardName(i + 1, scanSeq + 1);
        int index = boardList.indexOf(boardName);
        mappedSerialNumbers.set(index, serialNumbers.get(counter));
        counter++;
      }
    }
    return mappedSerialNumbers;
  }
  
  /*
   * @author Siew Yeng
   */
  public boolean isSerialNumberMappingEnabled()
  {
    return Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_SERIAL_NUMBER_MAPPING);
  }
}
