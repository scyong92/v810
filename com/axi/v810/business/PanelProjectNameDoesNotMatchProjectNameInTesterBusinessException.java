package com.axi.v810.business;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PanelProjectNameDoesNotMatchProjectNameInTesterBusinessException extends BusinessException
{
    public PanelProjectNameDoesNotMatchProjectNameInTesterBusinessException(String projectName, String panelProjectName)
    {
        super(new LocalizedString("BUS_PANEL_PROJECT_NAME_DOES_NOT_MATCH_PROJECT_NAME_IN_TESTER_EXCEPTION_KEY",
                              new Object[]{projectName, panelProjectName}));
    }
}
