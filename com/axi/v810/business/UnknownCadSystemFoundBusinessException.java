/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.business;

import com.axi.util.*;

/**
 *
 * @author weng-jian.eoh
 */
public class UnknownCadSystemFoundBusinessException extends BusinessException
{
  public UnknownCadSystemFoundBusinessException(String cadName)
  {
    super(new LocalizedString("MM_GUI_UNKNOWN_CAD_SYSTEM_MESSAGE_KEY", new Object[]
      {
        cadName
      }));
  }
}