package com.axi.v810.business.psp;

import com.axi.util.Enum;

/**
 * @author Cheah Lee Herng
 */
public class OpticalLiveVideoEnum extends Enum 
{
    private static int _index = 0;
    public static OpticalLiveVideoEnum DISPLAY_LIVE_VIDEO = new OpticalLiveVideoEnum(++_index);
    public static OpticalLiveVideoEnum CLOSE_LIVE_VIDEO = new OpticalLiveVideoEnum(++_index);
    
    /**
     * FringePatternEnum
     *
     * @author Cheah Lee Herng
    */
    protected OpticalLiveVideoEnum(int id)
    {
        super(id);
    }
}
