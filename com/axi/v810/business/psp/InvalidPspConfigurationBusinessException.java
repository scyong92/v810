package com.axi.v810.business.psp;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * @author Cheah Lee Herng
 */
public class InvalidPspConfigurationBusinessException extends BusinessException
{
  /**
   * @author Cheah Lee Herng 
   */
  public InvalidPspConfigurationBusinessException(String systemType, int moduleVersion, int calibrationVersion)
  {
    super(new LocalizedString("BUS_INVALID_PSP_CONFIGURATION_EXCEPTION_KEY", new Object[]{systemType, moduleVersion, calibrationVersion}));
  }  
}
