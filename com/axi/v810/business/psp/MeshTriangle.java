package com.axi.v810.business.psp;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class MeshTriangle implements Serializable
{
  private java.util.List<OpticalCameraRectangle> _opticalCameraRectangles;
  private boolean _isUsed;
  private double _zHeightInNanometer;
  private boolean _zHeightValid;    
  
  // Track of Board Name to List of components
  private Map<Board, java.util.List<com.axi.v810.business.panelDesc.Component>> _boardToComponentMap; 
  
  /**
   * @author Cheah Lee Herng 
   */
  public MeshTriangle(OpticalCameraRectangle point1OpticalCameraRectangle, 
                      OpticalCameraRectangle point2OpticalCameraRectangle,
                      OpticalCameraRectangle point3OpticalCameraRectangle)
  {
    Assert.expect(point1OpticalCameraRectangle != null);
    Assert.expect(point2OpticalCameraRectangle != null);
    Assert.expect(point3OpticalCameraRectangle != null);        
    
    // Instantiate list and maps
    _opticalCameraRectangles = new ArrayList<OpticalCameraRectangle>();
    _boardToComponentMap = new LinkedHashMap<Board, java.util.List<com.axi.v810.business.panelDesc.Component>>();
    
    addOpticalCameraRectangle(point1OpticalCameraRectangle);
    addOpticalCameraRectangle(point2OpticalCameraRectangle);
    addOpticalCameraRectangle(point3OpticalCameraRectangle);
    
    _isUsed = false;    
    _zHeightInNanometer = -1;
    _zHeightValid = false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public MeshTriangle(int point1PanelCoordinateXInNanometer,
                      int point1PanelCoordinateYInNanometer,
                      int point1RegionWidthInNanometer,
                      int point1RegionHeightInNanometer,
                      int point2PanelCoordinateXInNanometer,
                      int point2PanelCoordinateYInNanometer,
                      int point2RegionWidthInNanometer,
                      int point2RegionHeightInNanometer,
                      int point3PanelCoordinateXInNanometer,
                      int point3PanelCoordinateYInNanometer,
                      int point3RegionWidthInNanometer,
                      int point3RegionHeightInNanometer)
  {
    Assert.expect(point1PanelCoordinateXInNanometer >= 0);
    Assert.expect(point1PanelCoordinateYInNanometer >= 0);
    Assert.expect(point2PanelCoordinateXInNanometer >= 0);
    Assert.expect(point2PanelCoordinateYInNanometer >= 0);
    Assert.expect(point3PanelCoordinateXInNanometer >= 0);
    Assert.expect(point3PanelCoordinateYInNanometer >= 0);
    
    OpticalCameraRectangle point1OpticalCameraRectangle = new OpticalCameraRectangle();
    OpticalCameraRectangle point2OpticalCameraRectangle = new OpticalCameraRectangle();
    OpticalCameraRectangle point3OpticalCameraRectangle = new OpticalCameraRectangle();
    
    point1OpticalCameraRectangle.setRegion(point1PanelCoordinateXInNanometer, 
                                           point1PanelCoordinateYInNanometer, 
                                           point1RegionWidthInNanometer, 
                                           point1RegionHeightInNanometer);
    
    point2OpticalCameraRectangle.setRegion(point2PanelCoordinateXInNanometer, 
                                           point2PanelCoordinateYInNanometer, 
                                           point2RegionWidthInNanometer, 
                                           point2RegionHeightInNanometer);
    
    point3OpticalCameraRectangle.setRegion(point3PanelCoordinateXInNanometer, 
                                           point3PanelCoordinateYInNanometer, 
                                           point3RegionWidthInNanometer, 
                                           point3RegionHeightInNanometer);
    
    // Instantiate list and maps
    _opticalCameraRectangles = new ArrayList<OpticalCameraRectangle>();
    _boardToComponentMap = new LinkedHashMap<Board, java.util.List<com.axi.v810.business.panelDesc.Component>>();
    
    addOpticalCameraRectangle(point1OpticalCameraRectangle);
    addOpticalCameraRectangle(point2OpticalCameraRectangle);
    addOpticalCameraRectangle(point3OpticalCameraRectangle);
    
    _isUsed = false;
    
    _zHeightInNanometer = -1;
    _zHeightValid = false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public OpticalCameraRectangle getPoint1OpticalCameraRectangle()
  {
    return getOpticalCameraRectangles().get(0);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public OpticalCameraRectangle getPoint2OpticalCameraRectangle()
  {
    return getOpticalCameraRectangles().get(1);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public OpticalCameraRectangle getPoint3OpticalCameraRectangle()
  {
    return getOpticalCameraRectangles().get(2);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setPoint3OpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {
    Assert.expect(opticalCameraRectangle != null);
    OpticalCameraRectangle point3OpticalCameraRectangle = getOpticalCameraRectangles().get(2);
    point3OpticalCameraRectangle = opticalCameraRectangle;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public final void addOpticalCameraRectangle(OpticalCameraRectangle opticalCameraRectangle)
  {
    Assert.expect(opticalCameraRectangle != null);
    
    if (_opticalCameraRectangles.contains(opticalCameraRectangle) == false)
      _opticalCameraRectangles.add(opticalCameraRectangle);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setIsUsed(boolean isUsed)
  {
    _isUsed = isUsed;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isUsed()
  {
    return _isUsed;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public java.util.List<OpticalCameraRectangle> getOpticalCameraRectangles()
  {
    Assert.expect(_opticalCameraRectangles != null);
    
    Collections.sort(_opticalCameraRectangles);
    return _opticalCameraRectangles;
  }
  
  /**
   *
   * @author Jack Hwee
   */
  public java.awt.Rectangle getShapeForMeshTriangle(OpticalCameraRectangle point1OpticalCameraRectangle, 
                                                    OpticalCameraRectangle point2OpticalCameraRectangle,
                                                    OpticalCameraRectangle point3OpticalCameraRectangle)
  {
    Assert.expect(point1OpticalCameraRectangle != null);
    Assert.expect(point2OpticalCameraRectangle != null);
    Assert.expect(point3OpticalCameraRectangle != null);
    
    int line1BeginX = point1OpticalCameraRectangle.getRegion().getCenterX();
    int line1EndX = point2OpticalCameraRectangle.getRegion().getCenterX();
    int line1BeginY = point1OpticalCameraRectangle.getRegion().getCenterY();
    int line1EndY =point2OpticalCameraRectangle.getRegion().getCenterY();

    int line2BeginX =point2OpticalCameraRectangle.getRegion().getCenterX();
    int line2EndX = point3OpticalCameraRectangle.getRegion().getCenterX();
    int line2BeginY = point2OpticalCameraRectangle.getRegion().getCenterY();
    int line2EndY = point3OpticalCameraRectangle.getRegion().getCenterY();

    int line3BeginX = point1OpticalCameraRectangle.getRegion().getCenterX();
    int line3EndX = point3OpticalCameraRectangle.getRegion().getCenterX();
    int line3BeginY = point1OpticalCameraRectangle.getRegion().getCenterY();
    int line3EndY = point3OpticalCameraRectangle.getRegion().getCenterY();
    
    java.awt.Shape meshLine1 = new java.awt.geom.Line2D.Double(line1BeginX, line1BeginY, line1EndX, line1EndY);
    java.awt.Shape meshLine2 = new java.awt.geom.Line2D.Double(line2BeginX, line2BeginY, line2EndX, line2EndY);
    java.awt.Shape meshLine3 = new java.awt.geom.Line2D.Double(line3BeginX, line3BeginY, line3EndX, line3EndY);
    java.awt.geom.GeneralPath generalPath = new java.awt.geom.GeneralPath();
    generalPath.append(meshLine1, false);
    generalPath.append(meshLine2, false);
    generalPath.append(meshLine3, false);
    
    return generalPath.getBounds();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean contains(MeshTriangle targetMeshTriangle)
  {
    Assert.expect(targetMeshTriangle != null);
    
    java.awt.Polygon meshTrianglePolygon = new java.awt.Polygon();
    meshTrianglePolygon.addPoint(getPoint1OpticalCameraRectangle().getRegion().getCenterX(),
                                 getPoint1OpticalCameraRectangle().getRegion().getCenterY());
    meshTrianglePolygon.addPoint(getPoint2OpticalCameraRectangle().getRegion().getCenterX(),
                                 getPoint2OpticalCameraRectangle().getRegion().getCenterY());
    meshTrianglePolygon.addPoint(getPoint3OpticalCameraRectangle().getRegion().getCenterX(),
                                 getPoint3OpticalCameraRectangle().getRegion().getCenterY());
    
    java.awt.Polygon targetMeshTrianglePolygon = new java.awt.Polygon();
    targetMeshTrianglePolygon.addPoint(targetMeshTriangle.getPoint1OpticalCameraRectangle().getRegion().getCenterX(),
                                       targetMeshTriangle.getPoint1OpticalCameraRectangle().getRegion().getCenterY());
    targetMeshTrianglePolygon.addPoint(targetMeshTriangle.getPoint2OpticalCameraRectangle().getRegion().getCenterX(),
                                       targetMeshTriangle.getPoint2OpticalCameraRectangle().getRegion().getCenterY());
    targetMeshTrianglePolygon.addPoint(targetMeshTriangle.getPoint3OpticalCameraRectangle().getRegion().getCenterX(),
                                       targetMeshTriangle.getPoint3OpticalCameraRectangle().getRegion().getCenterY());
    
    java.awt.Point p;    
    boolean isPoint1Contained = false;
    boolean isPoint2Contained = false;
    boolean isPoint3Contained = false;
    
    for(int i = 0; i < targetMeshTrianglePolygon.npoints; i++)
    {
      p = new java.awt.Point(targetMeshTrianglePolygon.xpoints[i],targetMeshTrianglePolygon.ypoints[i]);
      
      boolean isPointContainInMeshTriangle = false;
      if (meshTrianglePolygon.contains(p))
        isPointContainInMeshTriangle = true;
      
      boolean isPointAtTheEdgeOfMeshTriangle = false;
      for(int j=0; j < meshTrianglePolygon.npoints; j++)
      {
        if (isPointAtTheEdgeOfMeshTriangle == false)
        {
          if (p.getX() == meshTrianglePolygon.xpoints[i] && 
              p.getY() == meshTrianglePolygon.ypoints[i])
          {
            isPointAtTheEdgeOfMeshTriangle = true;            
          }
        }      
      }
      
      switch(i)
      {
        case 0:
        {
          isPoint1Contained = isPointContainInMeshTriangle | isPointAtTheEdgeOfMeshTriangle;
          break;
        }
        case 1:
        {
          isPoint2Contained = isPointContainInMeshTriangle | isPointAtTheEdgeOfMeshTriangle;
          break;
        }
        case 2:
        {
          isPoint3Contained = isPointContainInMeshTriangle | isPointAtTheEdgeOfMeshTriangle;
          break;
        }
      }
    }
    return isPoint1Contained & isPoint2Contained & isPoint3Contained;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean hasComponent(com.axi.v810.business.panelDesc.Component component, Board board)
  {
    Assert.expect(component != null);
    Assert.expect(board != null);
    Assert.expect(_boardToComponentMap != null);

    if (_boardToComponentMap.containsKey(board))
    {
      java.util.List<com.axi.v810.business.panelDesc.Component> components = _boardToComponentMap.get(board);
      if (components.contains(component))
        return true;
    }
    return false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public java.util.List<com.axi.v810.business.panelDesc.Component> getComponents()
  {
    Assert.expect(_boardToComponentMap != null);
    
    java.util.List<com.axi.v810.business.panelDesc.Component> components = new ArrayList<com.axi.v810.business.panelDesc.Component>();
    for(Map.Entry<Board, java.util.List<com.axi.v810.business.panelDesc.Component>> entry : _boardToComponentMap.entrySet())
    {
      components.addAll(entry.getValue());
    }
    Collections.sort(components, new ComponentMagnificationTypeAndUserGainComparator(true, true));
    return components;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public java.util.List<com.axi.v810.business.panelDesc.Component> getComponents(Board board)
  {
    Assert.expect(board != null);
    Assert.expect(_boardToComponentMap != null);

    java.util.List<com.axi.v810.business.panelDesc.Component> components = new ArrayList<com.axi.v810.business.panelDesc.Component>();
    if (_boardToComponentMap.containsKey(board))
    {
      components.addAll(_boardToComponentMap.get(board));
    }
    Collections.sort(components, new ComponentMagnificationTypeAndUserGainComparator(true, true));
    return components;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void addComponent(Board board, com.axi.v810.business.panelDesc.Component component)
  {
    Assert.expect(board != null);
    Assert.expect(component != null);
    Assert.expect(_boardToComponentMap != null);

    if (_boardToComponentMap.containsKey(board))
    {
      java.util.List<com.axi.v810.business.panelDesc.Component> components = _boardToComponentMap.get(board);
      if (components.contains(component) == false)
        components.add(component);
    }
    else
    {
      java.util.List<com.axi.v810.business.panelDesc.Component> components = new ArrayList<com.axi.v810.business.panelDesc.Component>();
      components.add(component);
      _boardToComponentMap.put(board, components);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void removeComponent(com.axi.v810.business.panelDesc.Component component, Board board)
  {
    Assert.expect(component != null);
    Assert.expect(board != null);

    if (_boardToComponentMap.containsKey(board))
    {
      _boardToComponentMap.get(board).remove(component);
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public java.util.List<Board> getBoards()
  {
    Assert.expect(_boardToComponentMap != null);
    return new ArrayList<Board>(_boardToComponentMap.keySet());
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public Map<Board,java.util.List<com.axi.v810.business.panelDesc.Component>> getBoardToComponentsMap()
  {
    Assert.expect(_boardToComponentMap != null);
    return _boardToComponentMap;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public double getZHeightInNanometer()
  {
    return _zHeightInNanometer;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isZHeightValid()
  {
    return _zHeightValid;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public String getName()
  {
    String name = "";
    java.util.List<OpticalCameraRectangle> opticalCameraRectangles = getOpticalCameraRectangles();
    
    name += opticalCameraRectangles.get(0).getRegion().getMinX() + "_" + opticalCameraRectangles.get(0).getRegion().getMinY() + "_";
    name += opticalCameraRectangles.get(1).getRegion().getMinX() + "_" + opticalCameraRectangles.get(1).getRegion().getMinY() + "_";
    name += opticalCameraRectangles.get(2).getRegion().getMinX() + "_" + opticalCameraRectangles.get(2).getRegion().getMinY();
    
    return name;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasOpticalCameraRectangle(String opticalCameraRectangleName)
  {
    Assert.expect(opticalCameraRectangleName != null);
    
    for(OpticalCameraRectangle opticalCameraRectangle : getOpticalCameraRectangles())
    {
      if (opticalCameraRectangle.getName().equals(opticalCameraRectangleName))
        return true;
    }
    return false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public OpticalCameraRectangle getOpticalCameraRectangle(String opticalCameraRectangleName)
  {
    Assert.expect(opticalCameraRectangleName != null);
    
    OpticalCameraRectangle foundOpticalCameraRectangle = null;
    for(OpticalCameraRectangle opticalCameraRectangle : getOpticalCameraRectangles())
    {
      if (opticalCameraRectangle.getName().equals(opticalCameraRectangleName))
      {
        foundOpticalCameraRectangle = opticalCameraRectangle;
        break;
      }
    }
    return foundOpticalCameraRectangle;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void calculateZHeightInNanometer()
  {    
    List<Double> zHeightInNanometersDouble = new ArrayList<Double>();
    for(OpticalCameraRectangle opticalCameraRectangle : _opticalCameraRectangles)
    {
      if (opticalCameraRectangle.isZHeightValid())
      {        
        zHeightInNanometersDouble.add(opticalCameraRectangle.getZHeightInNanometer());
      }
      
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
      {
        System.out.println("MeshTriangle " + getName() + ": Process OpticalCameraRectangle " + opticalCameraRectangle.getName() + 
                ": Current Z-Height (nm) = " + opticalCameraRectangle.getZHeightInNanometer() +
                ": Z-Height Valid = " + opticalCameraRectangle.isZHeightValid());
      }
    }
    
    if (zHeightInNanometersDouble.isEmpty() == false)
    {     
      float[] zHeightInNanometerFloatArray = new float[zHeightInNanometersDouble.size()];
      for(int i=0; i < zHeightInNanometersDouble.size(); i++)
      {
        zHeightInNanometerFloatArray[i] = zHeightInNanometersDouble.get(i).floatValue();
      }
      
      _zHeightInNanometer = StatisticsUtil.median(zHeightInNanometerFloatArray);
      _zHeightValid = true;
      
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
      {
        System.out.println("MeshTriangle " + getName() + ": Final Z-Height (nm) = " + _zHeightInNanometer);
      }
    }
    else
    {
      _zHeightValid = false;
      
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
      {
        System.out.println("MeshTriangle " + getName() + ": Invalid Z-Height");
      }
    }
  }
  
  /**
   * @author Kok Chun, Tan
   * @param board 
   */
  public void addBoard(Board board)
  {
    for (OpticalCameraRectangle ocr : getOpticalCameraRectangles())
    {
      ocr.setBoard(board);
    }
  }
}