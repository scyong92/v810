package com.axi.v810.business.psp;

import com.axi.util.*;
import com.axi.v810.hardware.*;

import java.util.*;

/**
 * @author Cheah Lee Herng
 */
public class OpticalLiveVideoObservable extends Observable 
{
    private static OpticalLiveVideoObservable _instance;
    
    /**
     * @author Cheah Lee Herng 
     */
    public static synchronized OpticalLiveVideoObservable getInstance()
    {
        if (_instance == null)
        {
          _instance = new OpticalLiveVideoObservable();
        }
        return _instance;
    }    
    
    /**
     * @author Cheah Lee Herng
     */
    public synchronized void sendEvent(OpticalLiveVideoInfo opticalLiveVideoInfo)
    {
        Assert.expect(opticalLiveVideoInfo != null);
        
        setChanged();
        notifyObservers(opticalLiveVideoInfo);
    }
}
