package com.axi.v810.business.psp;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class MeshLine 
{
  private OpticalCameraRectangle _point1OpticalCameraRectangle; // Point 1
  private OpticalCameraRectangle _point2OpticalCameraRectangle; // Point 2
  private long _distanceSquare = -1;
  
  /**
   * @author Cheah Lee Herng
   */
  public MeshLine(OpticalCameraRectangle point1OpticalCameraRectangle, OpticalCameraRectangle point2OpticalCameraRectangle)
  {
    Assert.expect(point1OpticalCameraRectangle != null);
    Assert.expect(point2OpticalCameraRectangle != null);            
    
    PanelCoordinate point1PanelCoordinate = point1OpticalCameraRectangle.getRegion().getCenterCoordinate();
    PanelCoordinate point2PanelCoordinate = point2OpticalCameraRectangle.getRegion().getCenterCoordinate();
    
    // Re-arrange the point location
    if (point1PanelCoordinate.getX() < point2PanelCoordinate.getX())
    {
      _point1OpticalCameraRectangle = point1OpticalCameraRectangle;
      _point2OpticalCameraRectangle = point2OpticalCameraRectangle;
    }
    else if (point1PanelCoordinate.getX() > point2PanelCoordinate.getX())
    {
      _point1OpticalCameraRectangle = point2OpticalCameraRectangle;
      _point2OpticalCameraRectangle = point1OpticalCameraRectangle;
    }
    else  // Have the same X coordinate
    {
      if (point1PanelCoordinate.getY() < point2PanelCoordinate.getY())
      {
        _point1OpticalCameraRectangle = point1OpticalCameraRectangle;
        _point2OpticalCameraRectangle = point2OpticalCameraRectangle;
      }
      else if (point1PanelCoordinate.getY() > point2PanelCoordinate.getY())
      {
        _point1OpticalCameraRectangle = point2OpticalCameraRectangle;
        _point2OpticalCameraRectangle = point1OpticalCameraRectangle;
      }
      else
      {
        _point1OpticalCameraRectangle = point1OpticalCameraRectangle;
        _point2OpticalCameraRectangle = point2OpticalCameraRectangle;
      }
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public long getDistanceSquare()
  {
    Assert.expect(_point1OpticalCameraRectangle != null);
    Assert.expect(_point2OpticalCameraRectangle != null);
    
    if (_distanceSquare == -1)
    {
      PanelCoordinate point1PanelCoordiante = _point1OpticalCameraRectangle.getRegion().getCenterCoordinate();
      PanelCoordinate point2PanelCoordiante = _point2OpticalCameraRectangle.getRegion().getCenterCoordinate();
      
      long deltaX = point1PanelCoordiante.getX() - point2PanelCoordiante.getX();
      long deltaY = point1PanelCoordiante.getY() - point2PanelCoordiante.getY();

      _distanceSquare = (deltaX * deltaX) + (deltaY * deltaY);      
    }
    return _distanceSquare;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public OpticalCameraRectangle getPoint1OpticalCameraRectangle()
  {
    Assert.expect(_point1OpticalCameraRectangle != null);
    return _point1OpticalCameraRectangle;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public OpticalCameraRectangle getPoint2OpticalCameraRectangle()
  {
    Assert.expect(_point2OpticalCameraRectangle != null);
    return _point2OpticalCameraRectangle;
  }
}
