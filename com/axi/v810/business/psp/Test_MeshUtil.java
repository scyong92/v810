package com.axi.v810.business.psp;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Andy Mechtenberg
 */
public class Test_MeshUtil extends UnitTest
{
  /**
   * @author Andy Mechtenberg
   */
  public static void main(String args[])
  {
    UnitTest.execute(new Test_MeshUtil());
  }

  /**
   * @author Keith Lee
   */
  public void test(BufferedReader is, PrintWriter os)
  {        
    List<OpticalCameraRectangle> opticalCameraRectangeList = generateOpticalCameraRectangleList();
    List<MeshTriangle> meshTriangleList = MeshUtil.getInstance().createMesh(opticalCameraRectangeList, MeshUtil.LIMIT_ANGLE);
    
    Expect.expect(meshTriangleList.size() == 18);
  }
  
  private List<OpticalCameraRectangle> generateOpticalCameraRectangleList()
  {
    // These coordinate is extracted from 5DX NDF
    // rect1 627	627	0
    // rect2 1881	627	0
    // rect3 3135	627	0
    // rect4 4389	627	0
    // rect5 627	1881	0
    // rect6 1881	1881	0
    // rect7 3135	1881	0
    // rect8 4389	1881	0
    // rect9 627	3135	0
    // rect10 1881	3135	0
    // rect11 3135	3135	0
    // rect12 4389	3135	0
    // rect13 627	3931	0
    // rect14 1881	3931	0
    // rect15 3135	3931	0
    // rect16 4389	3931	0
    
    List<OpticalCameraRectangle> opticalCameraRectangle = new ArrayList<OpticalCameraRectangle>();
    
    OpticalCameraRectangle rect1 = new OpticalCameraRectangle();
    rect1.setRegion(627, 627, 1, 1);
    opticalCameraRectangle.add(rect1);
    
    OpticalCameraRectangle rect2 = new OpticalCameraRectangle();
    rect2.setRegion(1881, 627, 1, 1);
    opticalCameraRectangle.add(rect2);
    
    OpticalCameraRectangle rect3 = new OpticalCameraRectangle();
    rect3.setRegion(3135, 627, 1, 1);
    opticalCameraRectangle.add(rect3);
    
    OpticalCameraRectangle rect4 = new OpticalCameraRectangle();
    rect4.setRegion(4389, 627, 1, 1);
    opticalCameraRectangle.add(rect4);
    
    OpticalCameraRectangle rect5 = new OpticalCameraRectangle();
    rect5.setRegion(627, 1881, 1, 1);
    opticalCameraRectangle.add(rect5);
    
    OpticalCameraRectangle rect6 = new OpticalCameraRectangle();
    rect6.setRegion(1881, 1881, 1, 1);
    opticalCameraRectangle.add(rect6);
    
    OpticalCameraRectangle rect7 = new OpticalCameraRectangle();
    rect7.setRegion(3135, 1881, 1, 1);
    opticalCameraRectangle.add(rect7);
    
    OpticalCameraRectangle rect8 = new OpticalCameraRectangle();
    rect8.setRegion(4389, 1881, 1, 1);
    opticalCameraRectangle.add(rect8);
    
    OpticalCameraRectangle rect9 = new OpticalCameraRectangle();
    rect9.setRegion(627, 3135, 1, 1);
    opticalCameraRectangle.add(rect9);
    
    OpticalCameraRectangle rect10 = new OpticalCameraRectangle();
    rect10.setRegion(1881, 3135, 1, 1);
    opticalCameraRectangle.add(rect10);
    
    OpticalCameraRectangle rect11 = new OpticalCameraRectangle();
    rect11.setRegion(3135, 3135, 1, 1);
    opticalCameraRectangle.add(rect11);
    
    OpticalCameraRectangle rect12 = new OpticalCameraRectangle();
    rect12.setRegion(4389, 3135, 1, 1);
    opticalCameraRectangle.add(rect12);
    
    OpticalCameraRectangle rect13 = new OpticalCameraRectangle();
    rect13.setRegion(627, 3931, 1, 1);
    opticalCameraRectangle.add(rect13);
    
    OpticalCameraRectangle rect14 = new OpticalCameraRectangle();
    rect14.setRegion(1881, 3931, 1, 1);
    opticalCameraRectangle.add(rect14);
    
    OpticalCameraRectangle rect15 = new OpticalCameraRectangle();
    rect15.setRegion(3135, 3931, 1, 1);
    opticalCameraRectangle.add(rect15);
    
    OpticalCameraRectangle rect16 = new OpticalCameraRectangle();
    rect16.setRegion(4389, 3931, 1, 1);
    opticalCameraRectangle.add(rect16);
    
    return opticalCameraRectangle;
  }
}
