package com.axi.v810.business.psp;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 *
 * @author kok-chun.tan
 */
public class SurroundingOpticalRectangleUtil
{
  private static SurroundingOpticalRectangleUtil _instance = null;
  
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized SurroundingOpticalRectangleUtil getInstance()
  {
    if (_instance == null)
      _instance = new SurroundingOpticalRectangleUtil();
    
    return _instance;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void allocateSurroundingOpticalCameraRectangles(List<ReconstructionRegion> inspectionRegions, List<OpticalCameraRectangle> opticalCameraRectangleList)
  {
    for (com.axi.v810.business.testProgram.ReconstructionRegion inspectionRegion : inspectionRegions)
    {
      allocateSurroundingOpticalCameraRectangles(inspectionRegion, opticalCameraRectangleList);
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private void allocateSurroundingOpticalCameraRectangles(ReconstructionRegion inspectionRegion, List<OpticalCameraRectangle> opticalCameraRectangleList)
  {
    // Initialize variables
    boolean isFirst = true;
    double best = 0.0;
    int bestIndex = -1;

    for (int i = 0; i < opticalCameraRectangleList.size(); i++)
    {
      OpticalCameraRectangle opticalCameraRectangle = opticalCameraRectangleList.get(i);

      double distance = Math.sqrt(getDistanceSquare(inspectionRegion.getRegionRectangleRelativeToPanelInNanoMeters(), opticalCameraRectangle.getRegion()));
      if ((isFirst) || (distance < best))
      {
        isFirst = false;
        bestIndex = i;
        best = distance;
      }
    }

    OpticalCameraRectangle opticalCameraRectangleFound = opticalCameraRectangleList.get(bestIndex);
    inspectionRegion.setReferenceOpticalCameraRectangle(opticalCameraRectangleFound);
  }
  
   /**
   * @author Cheah Lee Herng 
   */
  private long getDistanceSquare(PanelRectangle point1PanelRectangle,
                                 PanelRectangle point2PanelRectangle)
  {
    Assert.expect(point1PanelRectangle != null);
    Assert.expect(point2PanelRectangle != null);
  
    long deltaX = point1PanelRectangle.getCenterX() - point2PanelRectangle.getCenterX();
    long deltaY = point1PanelRectangle.getCenterY() - point2PanelRectangle.getCenterY();

    return (deltaX * deltaX) + (deltaY * deltaY);
  }
}
