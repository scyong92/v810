package com.axi.v810.business.psp;

import com.axi.util.*;
import com.axi.v810.business.autoCal.*;
import com.axi.v810.hardware.*;

/**
 * This object is passed to update calls on Observers of the 
 * OpticalLiveVideoObservable.
 * 
 * @author Cheah Lee Herng
 */
public class OpticalLiveVideoInfo 
{
    private OpticalCameraIdEnum _cameraId;
    private OpticalLiveVideoEnum _opticalLiveVideoEnum;
    private String _actionButtonText;
    private OpticalLiveVideoInteractionInterface _opticalLiveVideoInstance;
    private boolean _displayFringePatternPanel = false;
    private boolean _displayGridPanel = false;
    private boolean _allowSurfaceMapPointSelection = false;
    
    /**
     * @author Cheah Lee Herng
    */
    public OpticalLiveVideoInfo(OpticalLiveVideoEnum opticalLiveVideoEnum, 
                                OpticalCameraIdEnum cameraId, 
                                String actionButtonText,
                                OpticalLiveVideoInteractionInterface opticalLiveVideoInstance,
                                boolean displayFringePatternPanel,
                                boolean displayGridPanel,
                                boolean allowSurfaceMapPointSelection)
    {
        Assert.expect(opticalLiveVideoEnum != null);
        Assert.expect(cameraId != null);
        Assert.expect(actionButtonText != null);
        Assert.expect(opticalLiveVideoInstance != null);
        
        _cameraId = cameraId;
        _opticalLiveVideoEnum = opticalLiveVideoEnum;
        _actionButtonText = actionButtonText;
        _opticalLiveVideoInstance = opticalLiveVideoInstance;
        _displayFringePatternPanel = displayFringePatternPanel;
        _displayGridPanel = displayGridPanel;
        _allowSurfaceMapPointSelection = allowSurfaceMapPointSelection;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public OpticalLiveVideoInfo(OpticalLiveVideoEnum opticalLiveVideoEnum)
    {
        _cameraId = null;
        _opticalLiveVideoEnum = opticalLiveVideoEnum;
        _actionButtonText = "";
        _opticalLiveVideoInstance = null;
        _displayFringePatternPanel = false;
        _displayGridPanel = false;
        _allowSurfaceMapPointSelection = false;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public OpticalCameraIdEnum getCameraId()
    {
        Assert.expect(_cameraId != null);
        return _cameraId;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public OpticalLiveVideoEnum getOpticalLiveVideoEnum()
    {
        Assert.expect(_opticalLiveVideoEnum != null);
        return _opticalLiveVideoEnum;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public String getActionButtonText()
    {
        Assert.expect(_actionButtonText != null);
        return _actionButtonText;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public OpticalLiveVideoInteractionInterface getOpticalLiveVideoInteractionInterface()
    {
        Assert.expect(_opticalLiveVideoInstance != null);
        return _opticalLiveVideoInstance;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public boolean isDisplayFringePatternPanel()
    {
        return _displayFringePatternPanel;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public boolean isDisplayGridPanel()
    {
        return _displayGridPanel;
    }
    
    /**     
     * @author Cheah Lee Herng 
     */
    public boolean isAllowSurfaceMapPointSelection()
    {
      return _allowSurfaceMapPointSelection;
    }
}
