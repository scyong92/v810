package com.axi.v810.business.psp;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class responsible for all the PSP-related settings and configuration.
 * 
 * The information included are:
 * 1) PSP optical fiducial location relative to 
 */
public class PspConfiguration
{
  private static final float DISTANCE_FROM_BELT_TO_OPTICAL_CALIBRATION_JIG_REFERENCE_PLANE_IN_MILIMETERS                          = 0.5f;
  private static final int   DISTNCE_FROM_BELT_TO_REFERENCE_PLANE_IN_MILS                                                         = 30;
  private static final float DISTANCE_FROM_BOTTOM_SURFACE_OF_TOP_STAGE_TO_OPTICAL_CALIBRATION_JIG_REFERENCE_PLANE_IN_MILIMETERS   = 0.682f;
  private static final float DISTANCE_FROM_MIDDLE_OF_OPTICAL_FIDUCIAL_TO_OPTICAL_REF_PLANE_IN_MILIMETERS                          = 0.7704f;  
  
  private static final int OPTICAL_CALIBRATION_JIG_WIDTH_IN_NANOMETERS                                                = 200000000;  // 7.87402inch
  private static final int OPTICAL_CALIBRATION_JIG_HEIGHT_IN_NANOMETERS                                               = 150000000;  // 5.90551inch
  
  private static final int OPTICAL_CALIBRATION_JIG_VERSION_4_WIDTH_IN_NANOMETERS                                      = 207000000;  // 8.07685inch
  private static final int OPTICAL_CALIBRATION_JIG_VERSION_4_HEIGHT_IN_NANOMETERS                                     = 150000000;  // 5.90551inch
  
  private static final int PSP_MODULE_VERSION_2_CAL_1_2_KNOWN_HEIGHT_IN_MICRON                                            = 3000;
  
  // Commented out by Lee Herng 29 June 2015 because we get the value from hardware.config
  // pspOnStageJigObjectPlaneZHeightInMicron key. This value varies for every on-stage jig.
  //private static final int PSP_MODULE_VERSION_2_CAL_3_KNOWN_HEIGHT_IN_MICRON                                            = 4300;
  
  // Optical System Offset Adjustment rail width
  private static final int OPTICAL_SYSTEM_OFFSET_ADJUSTMENT_RAIL_WIDTH_IN_NANOMETERS                                  = 101600000;
  
  // Optical Measurement Confirmation rail width
  private static final int STANDARD_OPTICAL_MEASUREMENT_CONFIRMATION_RAIL_WIDTH_IN_NANOMETERS_VERSION_3               = 101600000;  //  4.00000inch
  private static final int XXL_OPTICAL_MEASUREMENT_CONFIRMATION_RAIL_WIDTH_IN_NANOMETERS_VERSION_3                    = 304800000;  // 12.00000inch
  private static final int S2EX_OPTICAL_MEASUREMENT_CONFIRMATION_RAIL_WIDTH_IN_NANOMETERS_VERSION_3                   = 121920000;  //  4.80000inch
  
  // Optical System Fiducial Adjustment rail width
  private static final int OPTICAL_SYSTEM_FIDUCIAL_ADJUSTMENT_RAIL_WIDTH_IN_NANOMETERS_VERSION_2                      = 304800000;  // 12.00000inch
  
  // XCR-3619 Allow user to adjust AWA width during Optical System Fiducial Adjustment
  //private static final int STANDARD_OPTICAL_SYSTEM_FIDUCIAL_ADJUSTMENT_RAIL_WIDTH_IN_NANOMETERS_VERSION_3             = 122000000;  //  4.80315inch
  //private static final int XXL_OPTICAL_SYSTEM_FIDUCIAL_ADJUSTMENT_RAIL_WIDTH_IN_NANOMETERS_VERSION_3                  = 304800000;  // 12.00000inch
  //private static final int S2EX_OPTICAL_SYSTEM_FIDUCIAL_ADJUSTMENT_RAIL_WIDTH_IN_NANOMETERS_VERSION_3                 = 148978000;  //  5.86528inch
  
  // Optical Calibration Adjustment rail width
  private static final int OPTICAL_CALIBRATION_ADJUSTMENT_RAIL_WIDTH_IN_NANOMETERS_VERSION_2                          = 304800000;  // 12.00000inch
  private static final int STANDARD_OPTICAL_CALIBRATION_ADJUSTMENT_RAIL_WIDTH_IN_NANOMETERS_VERSION_3                 = 101600000;  //  4.00000inch
  private static final int XXL_OPTICAL_CALIBRATION_ADJUSTMENT_RAIL_WIDTH_IN_NANOMETERS_VERSION_3                      = 304800000;  // 12.00000inch
  private static final int S2EX_OPTICAL_CALIBRATION_ADJUSTMENT_RAIL_WIDTH_IN_NANOMETERS_VERSION_3                     = 148978000;  // 5.86528inch
  
  // Optical Fiducial X Location Coordinate from System Fiducial
  private static final int OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_X_IN_NANOMETERS_FOR_CALIBRATION_VERSION_1      = -345440000;
  private static final int OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_X_IN_NANOMETERS_FOR_CALIBRATION_VERSION_2      = -29151000;  
  private static final int OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_X_IN_NANOMETERS_FOR_STD_CALIBRATION_VERSION_3  = -214630000;
  private static final int OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_X_IN_NANOMETERS_FOR_XXL_CALIBRATION_VERSION_3  = -50680000;
  //private static final int OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_X_IN_NANOMETERS_FOR_S2EX_CALIBRATION_VERSION_3 = -13208000;
  private static final int OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_X_IN_NANOMETERS_FOR_S2EX_CALIBRATION_VERSION_3 = -78808000;
  
  // Optical Fiducial Y Location Coordinate from System Fiducial
  private static final int OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_Y_IN_NANOMETERS_FOR_CALIBRATION_VERSION_1      = 179544520;
  private static final int OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_Y_IN_NANOMETERS_FOR_CALIBRATION_VERSION_2      = 352544520;  
  private static final int OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_Y_IN_NANOMETERS_FOR_STD_CALIBRATION_VERSION_3  = 175593701;
  private static final int OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_Y_IN_NANOMETERS_FOR_XXL_CALIBRATION_VERSION_3  = 353350000;
  //private static final int OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_Y_IN_NANOMETERS_FOR_S2EX_CALIBRATION_VERSION_3 = 163322000;
  private static final int OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_Y_IN_NANOMETERS_FOR_S2EX_CALIBRATION_VERSION_3 = 196602000;
  
  // X-Offset value for Optical Calibration version 2 - Standard system
  private static final int STANDARD_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS                          = 7360000;
  private static final int STANDARD_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_MINUS_TWO_PLANE_IN_NANOMETERS                          = 32760000;
  private static final int STANDARD_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS                         = 59160000;
  private static final int STANDARD_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_MINUS_ONE_PLANE_IN_NANOMETERS                          = 14760000;
  private static final int STANDARD_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_FOUR_PLANE_IN_NANOMETERS                          = 43160000;
  private static final int STANDARD_MODULE_VERSION_1_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS  = 14800000;
  private static final int STANDARD_MODULE_VERSION_2_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS  = 14400000;
  private static final int STANDARD_MODULE_VERSION_1_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS  = 16500000;
  private static final int STANDARD_MODULE_VERSION_2_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS  = 19000000;
  
  // X-Offset value for Optical Calibration version 2 - XXL system
  private static final int XXL_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS             = 13360000;
  private static final int XXL_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_MINUS_TWO_PLANE_IN_NANOMETERS             = 32760000;
  private static final int XXL_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS            = 59160000;
  private static final int XXL_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_MINUS_ONE_PLANE_IN_NANOMETERS             = 16760000;
  private static final int XXL_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_FOUR_PLANE_IN_NANOMETERS             = 43160000;
  private static final int XXL_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS      = -18300000;  
  private static final int XXL_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS      = 19300000;
  
  // X-Offset value for Optical Calibration version 3 - Standard system
  private static final int STANDARD_CAL_3_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS        = 13560000;  
  private static final int STANDARD_CAL_3_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS       = 59160000;
  private static final int STANDARD_CAL_3_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS = 1200000;    
  private static final int STANDARD_CAL_3_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS = -1000000;
  
  // X-Offset value for Optical Calibration version 3 - Standard system (for Larger Fov)
  private static final int STANDARD_CAL_3_LARGER_FOV_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS        = 20000000;  
  private static final int STANDARD_CAL_3_LARGER_FOV_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS       = 65000000;
  private static final int STANDARD_CAL_3_LARGER_FOV_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS = 19600000;  
  private static final int STANDARD_CAL_3_LARGER_FOV_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS = -600000; 
  
  // X-Offset value for Optical Calibration version 3 - XXL system
  private static final int XXL_CAL_3_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS             = 14360000;
  private static final int XXL_CAL_3_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS            = 59160000;
  private static final int XXL_CAL_3_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS      = -7500000;   
  private static final int XXL_CAL_3_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS      = 8000000; 
  
  // X-Offset value for Optical Calibration version 3 - XXL system (for Larger Fov)
  private static final int XXL_CAL_3_LARGER_FOV_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS             = 18800000;
  private static final int XXL_CAL_3_LARGER_FOV_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS            = 65000000;
  private static final int XXL_CAL_3_LARGER_FOV_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS      = -7800000;
  private static final int XXL_CAL_3_LARGER_FOV_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS      = 17500000;
  
  // X-Offset value for Optical Calibration version 3 - S2EX system
  private static final int S2EX_CAL_3_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS             = 16160000;  //12360000;
  private static final int S2EX_CAL_3_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS            = 59160000;
  private static final int S2EX_CAL_3_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS      = -13000000;  //10000000;
  private static final int S2EX_CAL_3_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS      = 13000000;  //-500000;
  
  // X-Offset value for Optical Calibration version 4 - Standard system
  private static final int STANDARD_CAL_4_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS        = 13560000;
  private static final int STANDARD_CAL_4_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS       = 59160000;
  private static final int STANDARD_CAL_4_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS = 6000000;
  private static final int STANDARD_CAL_4_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS = 13000000;

  private static PspConfiguration _instance;  
  private static Map<PspModuleVersionEnum, List<PspCalibrationVersionEnum>> _configurationMap;
  private static Map<SystemTypeEnum, List<PspCalibrationVersionEnum>> _systemTypeConfigurationMap;
  
  private static Config _config = Config.getInstance();
  
  private static PspModuleVersionEnum _moduleEnum;
  private static PspCalibrationVersionEnum _calibrationVersionEnum;
  private static boolean _isXXL = false;
  private static boolean _isS2EX = false;
  
  /**
  * @author Cheah Lee Herng
  */
  static
  {
    // Mapping of SystemTypeEnum to list of PspCalibrationVersionEnum
    _systemTypeConfigurationMap = new HashMap<SystemTypeEnum, List<PspCalibrationVersionEnum>>();
    
    // Setup for PSP Module version 1
    List<PspCalibrationVersionEnum> version1CalibrationForSystemTypeList = new ArrayList<PspCalibrationVersionEnum>();
    version1CalibrationForSystemTypeList.add(PspCalibrationVersionEnum.VERSION_1);    
    version1CalibrationForSystemTypeList.add(PspCalibrationVersionEnum.VERSION_3);
    version1CalibrationForSystemTypeList.add(PspCalibrationVersionEnum.VERSION_4);
    _systemTypeConfigurationMap.put(SystemTypeEnum.THROUGHPUT, version1CalibrationForSystemTypeList);
    
    // Setup for PSP Module version 2
    List<PspCalibrationVersionEnum> version2CalibrationForSystemTypeList = new ArrayList<PspCalibrationVersionEnum>();    
    version2CalibrationForSystemTypeList.add(PspCalibrationVersionEnum.VERSION_2);
    version2CalibrationForSystemTypeList.add(PspCalibrationVersionEnum.VERSION_3);
    _systemTypeConfigurationMap.put(SystemTypeEnum.XXL, version2CalibrationForSystemTypeList);
    
    // Setup for PSP Module version 3
    List<PspCalibrationVersionEnum> version3CalibrationForSystemTypeList = new ArrayList<PspCalibrationVersionEnum>();   
    version3CalibrationForSystemTypeList.add(PspCalibrationVersionEnum.VERSION_3);
    _systemTypeConfigurationMap.put(SystemTypeEnum.S2EX, version3CalibrationForSystemTypeList); //Swee Yee
    
    // Mapping of PspModuleVersionEnum to list of PspCalibrationVersionEnum
    _configurationMap = new HashMap<PspModuleVersionEnum, List<PspCalibrationVersionEnum>>();
    
    // Setup for PSP Module version 1
    List<PspCalibrationVersionEnum> version1CalibrationList = new ArrayList<PspCalibrationVersionEnum>();
    version1CalibrationList.add(PspCalibrationVersionEnum.VERSION_1);
    version1CalibrationList.add(PspCalibrationVersionEnum.VERSION_2);
    _configurationMap.put(PspModuleVersionEnum.VERSION_1, version1CalibrationList);
    
    // Setup for PSP Module version 2
    List<PspCalibrationVersionEnum> version2CalibrationList = new ArrayList<PspCalibrationVersionEnum>();
    version2CalibrationList.add(PspCalibrationVersionEnum.VERSION_1);
    version2CalibrationList.add(PspCalibrationVersionEnum.VERSION_2);
    version2CalibrationList.add(PspCalibrationVersionEnum.VERSION_3);
    version2CalibrationList.add(PspCalibrationVersionEnum.VERSION_4);
    _configurationMap.put(PspModuleVersionEnum.VERSION_2, version2CalibrationList);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private PspConfiguration()
  {
    int version = _config.getIntValue(HardwareConfigEnum.PSP_MODULE_VERSION);
    _moduleEnum = PspModuleVersionEnum.getEnum(version);
    
    version = _config.getIntValue(HardwareConfigEnum.PSP_CALIBRATION_VERSION);
    _calibrationVersionEnum = PspCalibrationVersionEnum.getEnum(version);
    
    _isXXL = XrayTester.isXXLBased();
    _isS2EX = XrayTester.isS2EXEnabled();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized PspConfiguration getInstance()
  {
    if (_instance == null)
    {
      _instance = new PspConfiguration();
    }

    return _instance;
  }
  
  /**
   * 
   * 
   * @author Cheah Lee Herng
   */
  public void checkConfiguration() throws XrayTesterException
  {
    if (_config.getBooleanValue(SoftwareConfigEnum.ENABLE_PSP) == false)
      return;
    
    SystemTypeEnum systemTypeEnum = XrayTester.getSystemType();
    PspModuleVersionEnum moduleVersionEnum = PspEngine.getPspModuleVersionEnum();
    PspCalibrationVersionEnum calibrationVersionEnum = PspEngine.getPspCalibrationVersionEnum();
    
    boolean isCorrectSystemTypeConfiguration = false;
    boolean isCorrectConfiguration = false;
    if (_systemTypeConfigurationMap.containsKey(systemTypeEnum))
    {
      List<PspCalibrationVersionEnum> systemTypePspCalibrationVersionEnumList = _systemTypeConfigurationMap.get(systemTypeEnum);
      if (systemTypePspCalibrationVersionEnumList.contains(calibrationVersionEnum))
        isCorrectSystemTypeConfiguration = true;            
    }
    
    if (_configurationMap.containsKey(moduleVersionEnum))
    {
      List<PspCalibrationVersionEnum> pspCalibrationVersionEnumList = _configurationMap.get(moduleVersionEnum);
      if (pspCalibrationVersionEnumList.contains(calibrationVersionEnum))
        isCorrectConfiguration = true;
    }
    
    if (isCorrectConfiguration == false || isCorrectSystemTypeConfiguration == false)
      throw new InvalidPspConfigurationBusinessException(systemTypeEnum.getName(), moduleVersionEnum.getVersion(), calibrationVersionEnum.getVersion());
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getOpticalCalibrationJigWidthInNanometers()
  {
    if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
      return OPTICAL_CALIBRATION_JIG_VERSION_4_WIDTH_IN_NANOMETERS;
    else
      return OPTICAL_CALIBRATION_JIG_WIDTH_IN_NANOMETERS;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getOpticalCalibrationJigHeightInNanometers()
  {
    if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
      return OPTICAL_CALIBRATION_JIG_VERSION_4_HEIGHT_IN_NANOMETERS;
    else
      return OPTICAL_CALIBRATION_JIG_HEIGHT_IN_NANOMETERS;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getOpticalCalibrationKnownHeightInMicron()
  {
    if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3) ||
        _calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
    {
      return _config.getIntValue(HardwareConfigEnum.PSP_ON_STAGE_JIG_OBJECT_PLANE_ZHEIGHT_IN_MICRON);
    }
    else
    {
      return PSP_MODULE_VERSION_2_CAL_1_2_KNOWN_HEIGHT_IN_MICRON;
    }        
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public float getDistanceFromBeltToOpticalCalibrationJigReferencePlaneInMilimeters()
  {
    return DISTANCE_FROM_BELT_TO_OPTICAL_CALIBRATION_JIG_REFERENCE_PLANE_IN_MILIMETERS;
  }    
  
  /**
   * @author Cheah Lee Herng
   */
  public float getDistanceFromBeltToSystemReferencePlaneInMils()
  {
    return DISTNCE_FROM_BELT_TO_REFERENCE_PLANE_IN_MILS;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public float getDistanceFromBottomSurfaceOfTopStageToOpticalCalibrationJigReferencePlaneInMilimeters()
  {
    return DISTANCE_FROM_BOTTOM_SURFACE_OF_TOP_STAGE_TO_OPTICAL_CALIBRATION_JIG_REFERENCE_PLANE_IN_MILIMETERS;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public float getDistanceFromMiddleOpticalFiducialToOpticalReferencePlaneInMilimeters()
  {
    return DISTANCE_FROM_MIDDLE_OF_OPTICAL_FIDUCIAL_TO_OPTICAL_REF_PLANE_IN_MILIMETERS;
  }    
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getOpticalCalibrationRailWidthInNanometers()
  {
    int opticalCalibrationRailWidthInNanometers = 0;
    if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1) || 
        _calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
    {
      // For version 1, we manually load the calibration jig
      opticalCalibrationRailWidthInNanometers = 0;
    }
    else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
    {
      // Only applicable for XXL
      opticalCalibrationRailWidthInNanometers = 
              OPTICAL_CALIBRATION_ADJUSTMENT_RAIL_WIDTH_IN_NANOMETERS_VERSION_2;
    }
    else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
    {
      if (_isXXL)
      {
        opticalCalibrationRailWidthInNanometers = 
                XXL_OPTICAL_CALIBRATION_ADJUSTMENT_RAIL_WIDTH_IN_NANOMETERS_VERSION_3;
      }
      else if (_isS2EX)
      {
        opticalCalibrationRailWidthInNanometers = 
                S2EX_OPTICAL_CALIBRATION_ADJUSTMENT_RAIL_WIDTH_IN_NANOMETERS_VERSION_3;
      }
      else
      {
        opticalCalibrationRailWidthInNanometers = 
                STANDARD_OPTICAL_CALIBRATION_ADJUSTMENT_RAIL_WIDTH_IN_NANOMETERS_VERSION_3;
      }
    }
    else
      Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    
    return opticalCalibrationRailWidthInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getOpticalSystemFiducialAdjustmentRailWidthInNanometers()
  {
    int opticalFiducialAdjustmentRailWidthInNanometers = 0;
    if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1) || 
        _calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
    {
      // For version 1, we manually load the calibration jig
      opticalFiducialAdjustmentRailWidthInNanometers = getOpticalCalibrationJigWidthInNanometers();
    }
    else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
    {
      // Only applicable for XXL
      opticalFiducialAdjustmentRailWidthInNanometers = 
              OPTICAL_SYSTEM_FIDUCIAL_ADJUSTMENT_RAIL_WIDTH_IN_NANOMETERS_VERSION_2;
    }
    else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
    {
      // XCR-3619 Allow user to adjust AWA width during Optical System Fiducial Adjustment
      opticalFiducialAdjustmentRailWidthInNanometers = _config.getIntValue(HardwareConfigEnum.PSP_OPTICAL_FIDUCIAL_ADJUSTMENT_AWA_WIDTH_IN_NANOMETERS);
    }
    else
      Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    
    return opticalFiducialAdjustmentRailWidthInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getOpticalSystemOffsetAdjustmentRailWidthInNanometers()
  {
    return OPTICAL_SYSTEM_OFFSET_ADJUSTMENT_RAIL_WIDTH_IN_NANOMETERS;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getOpticalMeasurementConfirmationRailWidthInNanometers()
  {
    int opticalMeasurementConfirmationRailWidthInNanometers = 0;
    if (_isXXL)
    {
      opticalMeasurementConfirmationRailWidthInNanometers = XXL_OPTICAL_MEASUREMENT_CONFIRMATION_RAIL_WIDTH_IN_NANOMETERS_VERSION_3;
    }
    else if (_isS2EX)
    {
      opticalMeasurementConfirmationRailWidthInNanometers = S2EX_OPTICAL_MEASUREMENT_CONFIRMATION_RAIL_WIDTH_IN_NANOMETERS_VERSION_3;
    }
    else
    {
      opticalMeasurementConfirmationRailWidthInNanometers = STANDARD_OPTICAL_MEASUREMENT_CONFIRMATION_RAIL_WIDTH_IN_NANOMETERS_VERSION_3;
    }
    return opticalMeasurementConfirmationRailWidthInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getOpticalFiducialSystemCoordinateLocationXInNanometers()
  {
    int opticalFiducialXLocationSystemFiducialCoordinateInNanometers = 0;
    if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1) ||
        _calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
    {
      opticalFiducialXLocationSystemFiducialCoordinateInNanometers = 
        OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_X_IN_NANOMETERS_FOR_CALIBRATION_VERSION_1;
    }
    else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
    {
      opticalFiducialXLocationSystemFiducialCoordinateInNanometers = 
        OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_X_IN_NANOMETERS_FOR_CALIBRATION_VERSION_2;
    }
    else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
    {
      if (_isXXL)
      {
        opticalFiducialXLocationSystemFiducialCoordinateInNanometers = 
          OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_X_IN_NANOMETERS_FOR_XXL_CALIBRATION_VERSION_3;
      }
      else if(_isS2EX)
      {
        opticalFiducialXLocationSystemFiducialCoordinateInNanometers = 
          OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_X_IN_NANOMETERS_FOR_S2EX_CALIBRATION_VERSION_3;
      }
      else
      {
        opticalFiducialXLocationSystemFiducialCoordinateInNanometers = 
          OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_X_IN_NANOMETERS_FOR_STD_CALIBRATION_VERSION_3;
      }
    }
    else
      Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    
    return opticalFiducialXLocationSystemFiducialCoordinateInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getOpticalFiducialSystemCoordinateLocationYInNanometers()
  {
    int opticalFiducialYLocationSystemFiducialCoordinateInNanometers = 0;
    if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1) ||
        _calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
    {
      opticalFiducialYLocationSystemFiducialCoordinateInNanometers = 
        OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_Y_IN_NANOMETERS_FOR_CALIBRATION_VERSION_1;
    }
    else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
    {
      opticalFiducialYLocationSystemFiducialCoordinateInNanometers = 
        OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_Y_IN_NANOMETERS_FOR_CALIBRATION_VERSION_2;
    }
    else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
    {
      if (_isXXL)
      {
        opticalFiducialYLocationSystemFiducialCoordinateInNanometers = 
          OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_Y_IN_NANOMETERS_FOR_XXL_CALIBRATION_VERSION_3;
      }
      else if (_isS2EX)
      {
        opticalFiducialYLocationSystemFiducialCoordinateInNanometers = 
          OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_Y_IN_NANOMETERS_FOR_S2EX_CALIBRATION_VERSION_3;
      }
      else
      {
        opticalFiducialYLocationSystemFiducialCoordinateInNanometers = 
          OPTICAL_FIDUCIAL_SYSTEM_COORDINATE_LOCATION_Y_IN_NANOMETERS_FOR_STD_CALIBRATION_VERSION_3;
      }
    }
    else
      Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    
    return opticalFiducialYLocationSystemFiducialCoordinateInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getOpticalFiducialOffsetXOnReferencePlaneInNanometers()
  {
    int opticalFiducialOffsetXOnReferencePlaneInNanometers = 0;
    if (_isXXL)
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        // Not applicable to XXL
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 
                XXL_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 
                XXL_CAL_3_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    }
    else if (_isS2EX)
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        // Not applicable to S2EX
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        // Not applicable to S2EX
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 
                S2EX_CAL_3_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 
          STANDARD_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        // Not applicable to Standard system
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 
                STANDARD_CAL_3_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
      {
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 
                STANDARD_CAL_4_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");        
    }
    
    return opticalFiducialOffsetXOnReferencePlaneInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getLargerFovOpticalFiducialOffsetXOnReferencePlaneInNanometers()
  {
    int opticalFiducialOffsetXOnReferencePlaneInNanometers = 0;
    if (_isXXL)
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        // Not applicable to XXL
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 
                XXL_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 
                XXL_CAL_3_LARGER_FOV_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 
          STANDARD_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        // Not applicable to Standard system
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3) ||
               _calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
      {
        opticalFiducialOffsetXOnReferencePlaneInNanometers = 
                STANDARD_CAL_3_LARGER_FOV_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_REFERENCE_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");        
    }
    
    return opticalFiducialOffsetXOnReferencePlaneInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getOpticalFiducialOffsetXOnPlusThreePlaneInNanometers()
  {
    int opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 0;
    if (_isXXL)
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        // Not applicable to XXL
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 
                XXL_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 
                XXL_CAL_3_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    }
    else if (_isS2EX)
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        // Not applicable to S2EX
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        // Not applicable to S2EX
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 
                S2EX_CAL_3_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 
          STANDARD_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        // Not applicable to Standard system
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 
                STANDARD_CAL_3_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
      {
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 
                STANDARD_CAL_4_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");        
    }
    
    return opticalFiducialOffsetXOnPlusThreePlaneInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getLargerFovOpticalFiducialOffsetXOnPlusThreePlaneInNanometers()
  {
    int opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 0;
    if (_isXXL)
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        // Not applicable to XXL
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 
                XXL_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 
                XXL_CAL_3_LARGER_FOV_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 
          STANDARD_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        // Not applicable to Standard system
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3) ||
               _calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
      {
        opticalFiducialOffsetXOnPlusThreePlaneInNanometers = 
                STANDARD_CAL_3_LARGER_FOV_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_THREE_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");        
    }
    
    return opticalFiducialOffsetXOnPlusThreePlaneInNanometers;
  }
  
  /**   
   * 
   * @author Cheah Lee Herng 
   */
  public int getOpticalFiducialOffsetXOnMinusTwoPlaneInNanometers()
  {
    int opticalFiducialOffsetXOnMinusTwoPlaneInNanometers = 0;
    if (_isXXL || _isS2EX)
    {
      opticalFiducialOffsetXOnMinusTwoPlaneInNanometers = 
        XXL_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_MINUS_TWO_PLANE_IN_NANOMETERS;
    }
    else
    {
      opticalFiducialOffsetXOnMinusTwoPlaneInNanometers = 
        STANDARD_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_MINUS_TWO_PLANE_IN_NANOMETERS;
    }
    
    return opticalFiducialOffsetXOnMinusTwoPlaneInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getOpticalFiducialOffsetXOnMinusOnePlaneInNanometers()
  {
    int opticalFiducialOffsetXOnMinusOnePlaneInNanometers = 0;
    if (_isXXL || _isS2EX)
    {
      opticalFiducialOffsetXOnMinusOnePlaneInNanometers = 
        XXL_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_MINUS_ONE_PLANE_IN_NANOMETERS;
    }
    else
    {
      opticalFiducialOffsetXOnMinusOnePlaneInNanometers = 
        STANDARD_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_MINUS_ONE_PLANE_IN_NANOMETERS;
    }
    
    return opticalFiducialOffsetXOnMinusOnePlaneInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getOpticalFiducialOffsetXOnPlusFourPlaneInNanometers()
  {
    int opticalFiducialOffsetXOnPlusFourPlaneInNanometers = 0;
    if (_isXXL || _isS2EX)
    {
      opticalFiducialOffsetXOnPlusFourPlaneInNanometers = 
        XXL_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_FOUR_PLANE_IN_NANOMETERS;
    }
    else
    {
      opticalFiducialOffsetXOnPlusFourPlaneInNanometers = 
        STANDARD_SYSTEM_OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_PLUS_FOUR_PLANE_IN_NANOMETERS;
    }
    
    return opticalFiducialOffsetXOnPlusFourPlaneInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getFrontModuleOpticalFiducialOffsetYInNanometers()
  {
    int frontModuleOpticalFiducialOffsetYInNanometers = 0;
    if (_isXXL)
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        // Not applicable to XXL
        frontModuleOpticalFiducialOffsetYInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        frontModuleOpticalFiducialOffsetYInNanometers = 
          XXL_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        frontModuleOpticalFiducialOffsetYInNanometers = 
          XXL_CAL_3_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");  
    }
    else if (_isS2EX)
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        // Not applicable to S2EX
        frontModuleOpticalFiducialOffsetYInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        // Not applicable to S2EX
        frontModuleOpticalFiducialOffsetYInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        frontModuleOpticalFiducialOffsetYInNanometers = 
          S2EX_CAL_3_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");  
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        if (_moduleEnum.equals(PspModuleVersionEnum.VERSION_1))
        {
          frontModuleOpticalFiducialOffsetYInNanometers = 
            STANDARD_MODULE_VERSION_1_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
        }
        else if (_moduleEnum.equals(PspModuleVersionEnum.VERSION_2))
        {
          frontModuleOpticalFiducialOffsetYInNanometers = 
            STANDARD_MODULE_VERSION_2_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
        }
        else
          Assert.expect(false, "Invalid PspModuleVersionEnum");
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        // Not applicable to XXL
        frontModuleOpticalFiducialOffsetYInNanometers = 0;                
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        frontModuleOpticalFiducialOffsetYInNanometers = 
          STANDARD_CAL_3_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
      {
        frontModuleOpticalFiducialOffsetYInNanometers = 
          STANDARD_CAL_4_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    }
    
    return frontModuleOpticalFiducialOffsetYInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public int getRearModuleOpticalFiducialOffsetYInNanometers()
  {
    int rearModuleOpticalFiducialOffsetYInNanometers = 0;
    if (_isXXL)
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        // Not applicable to XXL
        rearModuleOpticalFiducialOffsetYInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        rearModuleOpticalFiducialOffsetYInNanometers = 
          XXL_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        rearModuleOpticalFiducialOffsetYInNanometers = 
          XXL_CAL_3_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    }
    else if (_isS2EX)
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        // Not applicable to S2EX
        rearModuleOpticalFiducialOffsetYInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        // Not applicable to S2EX
        rearModuleOpticalFiducialOffsetYInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        rearModuleOpticalFiducialOffsetYInNanometers = 
          S2EX_CAL_3_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        if (_moduleEnum.equals(PspModuleVersionEnum.VERSION_1))
        {
          rearModuleOpticalFiducialOffsetYInNanometers = 
            STANDARD_MODULE_VERSION_1_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
        }
        else if (_moduleEnum.equals(PspModuleVersionEnum.VERSION_2))
        {
          rearModuleOpticalFiducialOffsetYInNanometers = 
            STANDARD_MODULE_VERSION_2_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
        }
        else
          Assert.expect(false, "Invalid PspModuleVersionEnum");
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        // Not applicable to Standard system
        rearModuleOpticalFiducialOffsetYInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        rearModuleOpticalFiducialOffsetYInNanometers =
          STANDARD_CAL_3_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
      {
        rearModuleOpticalFiducialOffsetYInNanometers =
          STANDARD_CAL_4_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    }
    
    return rearModuleOpticalFiducialOffsetYInNanometers;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getLargerFovFrontModuleOpticalFiducialOffsetYInNanometers()
  {
    int frontModuleOpticalFiducialOffsetYInNanometers = 0;
    if (_isXXL)
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        // Not applicable to XXL
        frontModuleOpticalFiducialOffsetYInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        frontModuleOpticalFiducialOffsetYInNanometers = 
          XXL_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        frontModuleOpticalFiducialOffsetYInNanometers = 
          XXL_CAL_3_LARGER_FOV_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");  
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        if (_moduleEnum.equals(PspModuleVersionEnum.VERSION_1))
        {
          frontModuleOpticalFiducialOffsetYInNanometers = 
            STANDARD_MODULE_VERSION_1_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
        }
        else if (_moduleEnum.equals(PspModuleVersionEnum.VERSION_2))
        {
          frontModuleOpticalFiducialOffsetYInNanometers = 
            STANDARD_MODULE_VERSION_2_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
        }
        else
          Assert.expect(false, "Invalid PspModuleVersionEnum");
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        // Not applicable to XXL
        frontModuleOpticalFiducialOffsetYInNanometers = 0;                
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3) ||
               _calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
      {
        frontModuleOpticalFiducialOffsetYInNanometers = 
          STANDARD_CAL_3_LARGER_FOV_SYSTEM_OPTICAL_CAMERA_1_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    }
    System.out.println("frontModuleOpticalFiducialOffsetYInNanometers = " + frontModuleOpticalFiducialOffsetYInNanometers);
    return frontModuleOpticalFiducialOffsetYInNanometers;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public int getLargerFovRearModuleOpticalFiducialOffsetYInNanometers()
  {
    int rearModuleOpticalFiducialOffsetYInNanometers = 0;
    if (_isXXL)
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        // Not applicable to XXL
        rearModuleOpticalFiducialOffsetYInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        rearModuleOpticalFiducialOffsetYInNanometers = 
          XXL_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
      {
        rearModuleOpticalFiducialOffsetYInNanometers = 
          XXL_CAL_3_LARGER_FOV_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_1))
      {
        if (_moduleEnum.equals(PspModuleVersionEnum.VERSION_1))
        {
          rearModuleOpticalFiducialOffsetYInNanometers = 
            STANDARD_MODULE_VERSION_1_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
        }
        else if (_moduleEnum.equals(PspModuleVersionEnum.VERSION_2))
        {
          rearModuleOpticalFiducialOffsetYInNanometers = 
            STANDARD_MODULE_VERSION_2_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
        }
        else
          Assert.expect(false, "Invalid PspModuleVersionEnum");
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_2))
      {
        // Not applicable to Standard system
        rearModuleOpticalFiducialOffsetYInNanometers = 0;
      }
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3) ||
               _calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
      {
        rearModuleOpticalFiducialOffsetYInNanometers =
          STANDARD_CAL_3_LARGER_FOV_SYSTEM_OPTICAL_CAMERA_2_OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS;
      }
      else
        Assert.expect(false, "Invalid PspCalibrationVersionEnum");
    }
    
    return rearModuleOpticalFiducialOffsetYInNanometers;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getFrontOpticalFiducialStageCoordinateLocationXInNanometers()
  {
    if (_isXXL)
    {
      return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_1_STAGE_POSITION_X_NANOMETERS);
    }
    else if (_isS2EX)
    {
      return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_1_STAGE_POSITION_X_NANOMETERS);
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
        return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_X_NANOMETERS);
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
        return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_X_NANOMETERS);
      else
        return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_X_NANOMETERS);
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getFrontOpticalFiducialStageCoordinateLocationYInNanometers()
  {
    if (_isXXL)
    {
      return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS);
    }
    else if (_isS2EX)
    {
      return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_1_STAGE_POSITION_Y_NANOMETERS);
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
        return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_Y_NANOMETERS);
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
        return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS);
      else
        return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_Y_NANOMETERS);
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getRearOpticalFiducialStageCoordinateLocationXInNanometers()
  {
    if (_isXXL)
    {
      return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_2_STAGE_POSITION_X_NANOMETERS);
    }
    else if (_isS2EX)
    {
      return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_2_STAGE_POSITION_X_NANOMETERS);
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
        return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_X_NANOMETERS);
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
        return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_X_NANOMETERS);
      else
        return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_X_NANOMETERS);
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public int getRearOpticalFiducialStageCoordinateLocationYInNanometers()
  {
    if (_isXXL)
    {
      return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS);
    }
    else if (_isS2EX)
    {
      return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_2_STAGE_POSITION_Y_NANOMETERS);
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
        return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_Y_NANOMETERS);
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
        return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS);
      else
        return _config.getIntValue(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_Y_NANOMETERS);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public PspCalibEnum getFrontOpticalFiducialStageCoordinateLocationXCalibEnum()
  {
    if (_isXXL)
    {
      return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_1_STAGE_POSITION_X_NANOMETERS;
    }
    else if (_isS2EX)
    {
      return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_1_STAGE_POSITION_X_NANOMETERS;
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
        return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_X_NANOMETERS;
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
        return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_X_NANOMETERS;
      else
        return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_X_NANOMETERS;
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public PspCalibEnum getFrontOpticalFiducialStageCoordinateLocationYCalibEnum()
  {
    if (_isXXL)
    {
      return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS;
    }
    else if (_isS2EX)
    {
      return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_1_STAGE_POSITION_Y_NANOMETERS;
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
        return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_Y_NANOMETERS;
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
        return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS;
      else
        return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_Y_NANOMETERS;
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public PspCalibEnum getRearOpticalFiducialStageCoordinateLocationXCalibEnum()
  {
    if (_isXXL)
    {
      return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_2_STAGE_POSITION_X_NANOMETERS;
    }
    else if (_isS2EX)
    {
      return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_2_STAGE_POSITION_X_NANOMETERS;
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
        return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_X_NANOMETERS;
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
        return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_X_NANOMETERS;
      else
        return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_X_NANOMETERS;
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public PspCalibEnum getRearOpticalFiducialStageCoordinateLocationYCalibEnum()
  {
    if (_isXXL)
    {
      return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS;
    }
    else if (_isS2EX)
    {
      return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_2_STAGE_POSITION_Y_NANOMETERS;
    }
    else
    {
      if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_4))
        return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_Y_NANOMETERS;
      else if (_calibrationVersionEnum.equals(PspCalibrationVersionEnum.VERSION_3))
        return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS;
      else
        return PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_Y_NANOMETERS;
    }
  }
}
