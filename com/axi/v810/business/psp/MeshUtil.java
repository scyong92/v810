package com.axi.v810.business.psp;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class MeshUtil 
{
  public static final double LIMIT_ANGLE = 3.0;
  
  private static MeshUtil _instance;  
  
  private Comparator<MeshLine> _meshLineComparator = new Comparator<MeshLine>()
  {
    public int compare(MeshLine lhs, MeshLine rhs)
    {
      long lhsDistanceSquare = lhs.getDistanceSquare();
      long rhsDistanceSquare = rhs.getDistanceSquare();
      
      if (lhsDistanceSquare < rhsDistanceSquare)
        return -1;
      else if (lhsDistanceSquare > rhsDistanceSquare)
        return 1;
      else
      {
        OpticalCameraRectangle lhsOpticalCameraRectangle = lhs.getPoint1OpticalCameraRectangle();
        OpticalCameraRectangle rhsOpticalCameraRectangle = rhs.getPoint1OpticalCameraRectangle();
        
        if (lhsOpticalCameraRectangle.getRegion().getCenterX() < rhsOpticalCameraRectangle.getRegion().getCenterX())
          return -1;
        else if (lhsOpticalCameraRectangle.getRegion().getCenterX() > rhsOpticalCameraRectangle.getRegion().getCenterX())
          return 1;
        else
        {
          if (lhsOpticalCameraRectangle.getRegion().getCenterY() < rhsOpticalCameraRectangle.getRegion().getCenterY())
            return -1;
          else if (lhsOpticalCameraRectangle.getRegion().getCenterY() > rhsOpticalCameraRectangle.getRegion().getCenterY())
            return 1;
          else 
            return 0;
        }
      }
    }
  };
  
  /**
   * @author Cheah Lee Herng 
   */
  public static synchronized MeshUtil getInstance()
  {
    if (_instance == null)
      _instance = new MeshUtil();
    return _instance;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private MeshUtil()
  {
    // Do nothing
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public List<MeshTriangle> createMesh(List<OpticalCameraRectangle> opticalCameraRectangles)
  {
    Assert.expect(opticalCameraRectangles != null);    
    return createMesh(opticalCameraRectangles, LIMIT_ANGLE);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public List<MeshTriangle> createMesh(List<OpticalCameraRectangle> opticalCameraRectangles, double limitAngle)
  {
    Assert.expect(opticalCameraRectangles != null);
    
    // Operation Flow:
    // 1. Calculate total line generated
    // 2. Calculate line distance
    // 3. Sort line distance (from shortest to longest)
    // 4. Cull line (remove overlapped line)
    // 5. Make triangle
    // 6. If got mesh limit angle, cull triangle
    
    if (opticalCameraRectangles.isEmpty())
      return new ArrayList<MeshTriangle>();
    
    int totalOpticalCameraRectangle = opticalCameraRectangles.size();
    if (totalOpticalCameraRectangle < 3)    
      return new ArrayList<MeshTriangle>();  
    
    List<MeshLine> meshLines = new ArrayList<MeshLine>();
    int totalLine = ( totalOpticalCameraRectangle * (totalOpticalCameraRectangle - 1) ) / 2;
    
    for(int i=0; i < (totalOpticalCameraRectangle - 1); i++)
    {
      OpticalCameraRectangle opticalCameraRectangle = opticalCameraRectangles.get(i);
      for(int j=i+1; j < totalOpticalCameraRectangle; j++)
      {
        OpticalCameraRectangle neighBourOpticalCameraRectangle = opticalCameraRectangles.get(j);        
        MeshLine meshLine = new MeshLine(opticalCameraRectangle,neighBourOpticalCameraRectangle);        
        if (meshLines.contains(meshLine) == false)
          meshLines.add(meshLine);
      }
    }
    
    // Sort the mesh line
    Collections.sort(meshLines, _meshLineComparator);
    
    // Cull lines to get the list with shortest distance and non-overlapped
    List<MeshLine> newMeshLines = cullMeshLine(meshLines, totalLine);
    
    // Create Mesh Triangle
    List<MeshTriangle> meshTriangles = createMeshTriangle(newMeshLines);
    
    if (limitAngle > 0.0)
    {
      meshTriangles = cullMeshTriangle(meshTriangles, limitAngle);
    }
    
    // Remove overlap Mesh Triangle
    meshTriangles = cullOverlapMeshTriangle(meshTriangles);
    
    return meshTriangles;
  }
  
  /**
   * This function checks and remains those lines that is the shortest and
   * do not intersect with each other.
   * 
   * @author Cheah Lee Herng 
   */
  private List<MeshLine> cullMeshLine(List<MeshLine> meshLines, int origTotalLine)
  {
    Assert.expect(meshLines != null);
    
    int first = 1;
    int i = 0;
    boolean keepLine = true;
    List<MeshLine> newMeshLines = new ArrayList<MeshLine>();
    
    // Add the first line into list
    newMeshLines.add(meshLines.get(0));
    
    for(int second=1; second < origTotalLine; second++)
    {
      keepLine = true;
      i = 0;
            
      while ( (i < first) && keepLine )
      {
        MeshLine line1 = meshLines.get(second);
        MeshLine line2 = newMeshLines.get(i);
        
        if (isLineCross(line1, line2))
          keepLine = false;
        
        i++;
      }
      
      if (keepLine)
      {
        first++;
        newMeshLines.add(meshLines.get(second));
      }
    }
    
    return newMeshLines;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private List<MeshTriangle> createMeshTriangle(List<MeshLine> meshLines)
  {
    Assert.expect(meshLines != null);
    
    List<MeshTriangle> meshTriangles = new ArrayList<MeshTriangle>();    
    
    for(int i=0; i < meshLines.size(); i++)
    {
      // Get point 1
      MeshLine touchingMeshLine1 = meshLines.get(i);     
      OpticalCameraRectangle point1TouchingMeshLine1OpticalCameraRectangle = touchingMeshLine1.getPoint1OpticalCameraRectangle();
      
      List<MeshLine> touchingMeshLines1 = getTouchingMeshLine(meshLines, 0, point1TouchingMeshLine1OpticalCameraRectangle);
      for(int j=0; j < touchingMeshLines1.size(); j++)
      {
        // Get point 2
        MeshLine touchingMeshLine2 = touchingMeshLines1.get(j);
        
        if (touchingMeshLine2.equals(touchingMeshLine1))
          continue;
        
        OpticalCameraRectangle point1TouchingMeshLine2OpticalCameraRectangle = null;
        if (touchingMeshLine2.getPoint1OpticalCameraRectangle().equals(point1TouchingMeshLine1OpticalCameraRectangle))
          point1TouchingMeshLine2OpticalCameraRectangle = touchingMeshLine2.getPoint2OpticalCameraRectangle();
        else
          point1TouchingMeshLine2OpticalCameraRectangle = touchingMeshLine2.getPoint1OpticalCameraRectangle();
        
        List<MeshLine> touchingMeshLines2 = getTouchingMeshLine(meshLines, 0, point1TouchingMeshLine2OpticalCameraRectangle);
        for(int k=0; k < touchingMeshLines2.size(); k++)
        {
          // Get point 3
          MeshLine touchingMeshLine3 = touchingMeshLines2.get(k);
          
          if (touchingMeshLine3.equals(touchingMeshLine2))
            continue;
          
          OpticalCameraRectangle point1TouchingMeshLine3OpticalCameraRectangle = null;
          if (touchingMeshLine3.getPoint1OpticalCameraRectangle().equals(point1TouchingMeshLine2OpticalCameraRectangle))
            point1TouchingMeshLine3OpticalCameraRectangle = touchingMeshLine3.getPoint2OpticalCameraRectangle();
          else
            point1TouchingMeshLine3OpticalCameraRectangle = touchingMeshLine3.getPoint1OpticalCameraRectangle();
          
          List<MeshLine> touchingMeshLines3 = getTouchingMeshLine(meshLines, 0, point1TouchingMeshLine3OpticalCameraRectangle);
          for(int m=0; m < touchingMeshLines3.size(); m++)
          {
            MeshLine touchingMeshLine4 = touchingMeshLines3.get(m);
            
            if (touchingMeshLine4.equals(touchingMeshLine3))
              continue;
            
            // Get point 4
            OpticalCameraRectangle point1TouchingMeshLine4OpticalCameraRectangle = null;
            if (touchingMeshLine4.getPoint1OpticalCameraRectangle().equals(point1TouchingMeshLine3OpticalCameraRectangle))
              point1TouchingMeshLine4OpticalCameraRectangle = touchingMeshLine4.getPoint2OpticalCameraRectangle();
            else
              point1TouchingMeshLine4OpticalCameraRectangle = touchingMeshLine4.getPoint1OpticalCameraRectangle();
            
            // See if we manage to get the connecting point
            if (point1TouchingMeshLine1OpticalCameraRectangle.equals(point1TouchingMeshLine4OpticalCameraRectangle))
            {
              addMeshTriangle(meshTriangles, 
                              point1TouchingMeshLine1OpticalCameraRectangle, 
                              point1TouchingMeshLine2OpticalCameraRectangle, 
                              point1TouchingMeshLine3OpticalCameraRectangle);
            }
          }
        }
      }
    }    
    return meshTriangles;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private List<MeshLine> getTouchingMeshLine(List<MeshLine> meshLines, 
                                             int startIndex, 
                                             OpticalCameraRectangle opticalCameraRectangle)
  {
    Assert.expect(meshLines != null);
    Assert.expect(opticalCameraRectangle != null);
    
    List<MeshLine> touchingMeshLines = new ArrayList<MeshLine>();    
    
    for(int i=startIndex; i < meshLines.size(); i++)
    {
      MeshLine meshLine = meshLines.get(i);
      
      if (meshLine.getPoint1OpticalCameraRectangle().equals(opticalCameraRectangle) ||
          meshLine.getPoint2OpticalCameraRectangle().equals(opticalCameraRectangle))
      {
        if (touchingMeshLines.contains(meshLine) == false)
          touchingMeshLines.add(meshLine);
      }
    }
    
    return touchingMeshLines;
  }
  
  /**
   * I copied the comment from 5DX extractTriangulation function.
   * 
   * ***************************************************************
   * This function adds a triangle to a list if it is not already in the list
     and if it is not degenerate.  Degenerate means the three vertices are in
     a straight line.

     The input parameters are the three views at the corners of the triangles.

     The three points are sorted into a unique order as follows: 

     - the point with the lowest y coordinate is first. If there is a tie for
     the lowest y coordinate, of those, pick the one with the lowest x
     coordinate to be first.

     - the remaining two points are ordered so that the points are listed 
     in counter-clockwise order.

     It is possible that two triangles will share the same first two points.
     For example, ABC and ABD in the diagram below.

                            C --------------- B
                              \         .   /
                                \    D    /
                                  \  |  /
                                    \ /
                                     A

     If so, the one with the larger perimeter should be discarded.
   * 
   * @author Cheah Lee Herng
   */
  private void addMeshTriangle(List<MeshTriangle> meshTriangles,
                               OpticalCameraRectangle opticalCameraRectangle1,
                               OpticalCameraRectangle opticalCameraRectangle2,
                               OpticalCameraRectangle opticalCameraRectangle3)
  {
    Assert.expect(meshTriangles != null);
    Assert.expect(opticalCameraRectangle1 != null);
    Assert.expect(opticalCameraRectangle2 != null);
    Assert.expect(opticalCameraRectangle3 != null);
    
    // check for degenerate triangle (or nearly so)
    if (Math.abs(getCrossProduct(opticalCameraRectangle1, opticalCameraRectangle2, opticalCameraRectangle3)) <= 1.0)
      return;
    
    // Sort OpticalCameraRectangle from lowest X to highest X
    // Then from lowest Y to highest Y
    List<OpticalCameraRectangle> opticalCameraRectangles = new ArrayList<OpticalCameraRectangle>();
    opticalCameraRectangles.add(opticalCameraRectangle1);
    opticalCameraRectangles.add(opticalCameraRectangle2);
    opticalCameraRectangles.add(opticalCameraRectangle3);
    
    Collections.sort(opticalCameraRectangles);
    
    MeshTriangle meshTriangle = new MeshTriangle(opticalCameraRectangles.get(0), opticalCameraRectangles.get(1), opticalCameraRectangles.get(2));
    
    boolean foundTriangle = false;
    for(MeshTriangle currentMeshTriangle : meshTriangles)
    {
      OpticalCameraRectangle currentPoint1OpticalCameraRectangle = currentMeshTriangle.getPoint1OpticalCameraRectangle();
      OpticalCameraRectangle currentPoint2OpticalCameraRectangle = currentMeshTriangle.getPoint2OpticalCameraRectangle();
      OpticalCameraRectangle currentPoint3OpticalCameraRectangle = currentMeshTriangle.getPoint3OpticalCameraRectangle();
      
      OpticalCameraRectangle meshTrianglePoint1OpticalCameraRectangle = meshTriangle.getPoint1OpticalCameraRectangle();
      OpticalCameraRectangle meshTrianglePoint2OpticalCameraRectangle = meshTriangle.getPoint2OpticalCameraRectangle();
      OpticalCameraRectangle meshTrianglePoint3OpticalCameraRectangle = meshTriangle.getPoint3OpticalCameraRectangle();
      
      if (currentPoint1OpticalCameraRectangle.equals(meshTrianglePoint1OpticalCameraRectangle) &&
          currentPoint2OpticalCameraRectangle.equals(meshTrianglePoint2OpticalCameraRectangle) &&
          currentPoint3OpticalCameraRectangle.equals(meshTrianglePoint3OpticalCameraRectangle))
      {
        foundTriangle = true;
        break;
      }
//      else if (currentPoint1OpticalCameraRectangle.equals(meshTrianglePoint1OpticalCameraRectangle) &&
//               currentPoint2OpticalCameraRectangle.equals(meshTrianglePoint2OpticalCameraRectangle) &&
//               currentPoint3OpticalCameraRectangle.equals(meshTrianglePoint3OpticalCameraRectangle) == false)
//      {
//        double delta1 = Math.sqrt(getDistanceSquare(meshTrianglePoint2OpticalCameraRectangle, meshTrianglePoint3OpticalCameraRectangle)) +
//                        Math.sqrt(getDistanceSquare(meshTrianglePoint3OpticalCameraRectangle, meshTrianglePoint1OpticalCameraRectangle));
//        
//        double delta2 = Math.sqrt(getDistanceSquare(meshTrianglePoint2OpticalCameraRectangle, currentPoint3OpticalCameraRectangle)) +
//                        Math.sqrt(getDistanceSquare(currentPoint3OpticalCameraRectangle, meshTrianglePoint1OpticalCameraRectangle));
//        
//        if (delta1 < delta2)
//        {
//          currentMeshTriangle.setPoint3OpticalCameraRectangle(meshTrianglePoint3OpticalCameraRectangle);
//          foundTriangle = true;          
//        }
//      }
    }
    
    if (foundTriangle == false)
    {
      meshTriangles.add(meshTriangle);
    }
  }
  
  /**
   * This function is extracted from 5DX "extractTriangulation" code.
   * It determines if two lines cross. They don't cross if:
   * a) They are colinear but do NOT overlap, or
   * b) They are NOT colinear and
   *    1. one endpoint is colinear with the other line or
   *    2. both endpoints of one line are on the same side of the other line
   * 
   * @author Cheah Lee Herng 
   */
  private boolean isLineCross(MeshLine line1, MeshLine line2)
  {
    Assert.expect(line1 != null);
    Assert.expect(line2 != null);
    
    OpticalCameraRectangle rectangle1 = line1.getPoint1OpticalCameraRectangle();
    OpticalCameraRectangle rectangle2 = line1.getPoint2OpticalCameraRectangle();
    OpticalCameraRectangle rectangle3 = line2.getPoint1OpticalCameraRectangle();
    OpticalCameraRectangle rectangle4 = line2.getPoint2OpticalCameraRectangle();
    
    int ab1 = getSign(getCrossProduct(rectangle1, rectangle2, rectangle3));
    int ab2 = getSign(getCrossProduct(rectangle1, rectangle2, rectangle4));
    int ba1 = getSign(getCrossProduct(rectangle3, rectangle4, rectangle1));
    int ba2 = getSign(getCrossProduct(rectangle3, rectangle4, rectangle2));
    
    // Check if both lines colinear case
    if ((ab1 == 0) && (ab2 == 0))
    {
      if (isWithinLimit(rectangle1.getRegion().getCenterX(), rectangle3.getRegion().getCenterX(), rectangle4.getRegion().getCenterX()) ||
          isWithinLimit(rectangle1.getRegion().getCenterY(), rectangle3.getRegion().getCenterY(), rectangle4.getRegion().getCenterY()) ||
          isWithinLimit(rectangle2.getRegion().getCenterX(), rectangle3.getRegion().getCenterX(), rectangle4.getRegion().getCenterX()) ||
          isWithinLimit(rectangle2.getRegion().getCenterY(), rectangle3.getRegion().getCenterY(), rectangle4.getRegion().getCenterY()) ||
          isWithinLimit(rectangle3.getRegion().getCenterX(), rectangle1.getRegion().getCenterX(), rectangle2.getRegion().getCenterX()) ||
          isWithinLimit(rectangle3.getRegion().getCenterY(), rectangle1.getRegion().getCenterY(), rectangle2.getRegion().getCenterY()) ||
          isWithinLimit(rectangle4.getRegion().getCenterX(), rectangle1.getRegion().getCenterX(), rectangle2.getRegion().getCenterX()) ||
          isWithinLimit(rectangle4.getRegion().getCenterY(), rectangle1.getRegion().getCenterY(), rectangle2.getRegion().getCenterY()))
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else  // Both lines are NOT colinear
    {
      if ((ab1 == 0) || ( ab2 == 0) || (ba1 == 0) || (ba2 == 0))
      {
        return false;
      }
      if ((ab1 == ab2) || (ba1 == ba2))
      {
        return false;
      }
      return true;
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private long getCrossProduct(OpticalCameraRectangle rectangle1, 
                               OpticalCameraRectangle rectangle2, 
                               OpticalCameraRectangle rectangle3)
  {
    Assert.expect(rectangle1 != null);
    Assert.expect(rectangle2 != null);
    Assert.expect(rectangle3 != null);
    
    return getCrossProduct(rectangle1.getRegion(),
                           rectangle2.getRegion(), 
                           rectangle3.getRegion());
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private long getCrossProduct(PanelRectangle point1PanelRectangle,
                               PanelRectangle point2PanelRectangle,
                               PanelRectangle point3PanelRectangle)
  {
    Assert.expect(point1PanelRectangle != null);
    Assert.expect(point2PanelRectangle != null);
    Assert.expect(point3PanelRectangle != null);
    
    long dx1 = point2PanelRectangle.getCenterX() - point1PanelRectangle.getCenterX();
    long dy1 = point2PanelRectangle.getCenterY() - point1PanelRectangle.getCenterY();
    long dx2 = point3PanelRectangle.getCenterX() - point1PanelRectangle.getCenterX();
    long dy2 = point3PanelRectangle.getCenterY() - point1PanelRectangle.getCenterY();
    
    return ( dx1 * dy2 ) - ( dx2 * dy1 );
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private int getSign(long value)
  {
    if (value < 0) return -1;
    if (value > 0) return 1;
    return 0;
  }
  
  /**
   * This function return TRUE if a is strictly between b and c. Note b and c
   * are not necessary in order
   * 
   * @author Cheah Lee Herng 
   */
  private boolean isWithinLimit(int a, int b, int c)
  {
    if (b < c)
      return (b < a) && (a < c);
    
    return ( c < a ) && ( a < b );
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private long getDistanceSquare(PanelRectangle point1PanelRectangle,
                                 PanelRectangle point2PanelRectangle)
  {
    Assert.expect(point1PanelRectangle != null);
    Assert.expect(point2PanelRectangle != null);
    
    long deltaX = point1PanelRectangle.getCenterX() - point2PanelRectangle.getCenterX();
    long deltaY = point1PanelRectangle.getCenterY() - point2PanelRectangle.getCenterY();

    return (deltaX * deltaX) + (deltaY * deltaY);
  }
  
  /**
   * This function checks the angles of a triangle.  If the cosine of some
   * angle is greater than the limit, then the angle itself is close to zero
   * degrees.  In this case the function returns FALSE, else TRUE.  The
   * purpose of this is to reject any long, thin triangles in favor of short,
   * fat ones.
   * 
   * @author Cheah Lee Herng
   */
  private boolean isMeshTriangleAccepted(MeshTriangle meshTriangle, double limitCos)
  {
    Assert.expect(meshTriangle != null);
    
    double cos1 = getCosOfAngle(meshTriangle.getPoint1OpticalCameraRectangle(), 
                                meshTriangle.getPoint2OpticalCameraRectangle(), 
                                meshTriangle.getPoint3OpticalCameraRectangle());
    
    double cos2 = getCosOfAngle(meshTriangle.getPoint2OpticalCameraRectangle(), 
                                meshTriangle.getPoint3OpticalCameraRectangle(), 
                                meshTriangle.getPoint1OpticalCameraRectangle());
    
    double cos3 = getCosOfAngle(meshTriangle.getPoint3OpticalCameraRectangle(), 
                                meshTriangle.getPoint1OpticalCameraRectangle(), 
                                meshTriangle.getPoint2OpticalCameraRectangle());
    
    if ( ( cos1 >= limitCos ) || ( cos2 >= limitCos ) || ( cos3 >= limitCos ) )
    {
      return false;
    }
    return true;
  }
  
  /**
   * This function gets the cosine of the angle defined by three points
   * where p1 is the point at the angle and p2 and p3 are outlying
   * 
   * @author Cheah Lee Herng
   */
  private double getCosOfAngle(OpticalCameraRectangle point1OpticalCameraRectangle, 
                               OpticalCameraRectangle point2OpticalCameraRectangle, 
                               OpticalCameraRectangle point3OpticalCameraRectangle)
  {
    Assert.expect(point1OpticalCameraRectangle != null);
    Assert.expect(point2OpticalCameraRectangle != null);
    Assert.expect(point3OpticalCameraRectangle != null);
    
    PanelCoordinate point1PanelCoordinate = point1OpticalCameraRectangle.getRegion().getCenterCoordinate();
    PanelCoordinate point2PanelCoordinate = point2OpticalCameraRectangle.getRegion().getCenterCoordinate();
    PanelCoordinate point3PanelCoordinate = point3OpticalCameraRectangle.getRegion().getCenterCoordinate();
    
    long dx1 = point2PanelCoordinate.getX() - point1PanelCoordinate.getX();
    long dy1 = point2PanelCoordinate.getY() - point1PanelCoordinate.getY();
    long dx2 = point3PanelCoordinate.getX() - point1PanelCoordinate.getX();
    long dy2 = point3PanelCoordinate.getY() - point1PanelCoordinate.getY();
    long dot = ( dx1 * dx2 ) + ( dy1 * dy2 );
    
    double mag1 = Math.sqrt((dx1 * dx1 ) + ( dy1 * dy1 ));
    double mag2 = Math.sqrt((dx2 * dx2 ) + ( dy2 * dy2 ));
    
    return (dot / (mag1 * mag2));
  }
  
  /**
   * This function removes long skinny triangles from the triangle list, but
   * keeps short fat ones.  It does this by removing any triangle containing
   * an angle of less than about ten degrees.  The limit angle was chosen
   * arbitrarily.
   * 
   * @author Cheah Lee Herng 
   */
  private List<MeshTriangle> cullMeshTriangle(List<MeshTriangle> meshTriangles, double limitAngle)
  {
    Assert.expect(meshTriangles != null);
    
    double limitCos = Math.cos( limitAngle * 3.14159 / 180 );
    List<Integer> indexToRemove = new ArrayList<Integer>();
    List<MeshTriangle> triangleList = new ArrayList<MeshTriangle>();
   
    for(int i=0; i < meshTriangles.size(); i++)
    {
      MeshTriangle meshTriangle = meshTriangles.get(i);
      triangleList.add(meshTriangle);
      if (isMeshTriangleAccepted(meshTriangle, limitCos) == false)
      {
        indexToRemove.add(i);
      }
    }
    
    // Do the actual remove
    for(Integer index : indexToRemove)
    {
      MeshTriangle meshTriangle = triangleList.get(index);
      meshTriangles.remove(meshTriangle);
    }
    triangleList.clear();
    return meshTriangles;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void allocateMeshTriangle(ReconstructionRegion region, List<MeshTriangle> meshTriangles)
  {
    Assert.expect(region != null);
    Assert.expect(meshTriangles != null);
    
    if (meshTriangles.isEmpty())
      return;
    
    boolean isMeshTriangleFound = false;
    
    // Get PanelRectangle of ReconstructionRegion
    PanelRectangle regionPanelRectangle = region.getRegionRectangleRelativeToPanelInNanoMeters();
    
    for(MeshTriangle meshTriangle : meshTriangles)
    {
      OpticalCameraRectangle point1OpticalCameraRectangle = meshTriangle.getPoint1OpticalCameraRectangle();
      OpticalCameraRectangle point2OpticalCameraRectangle = meshTriangle.getPoint2OpticalCameraRectangle();
      OpticalCameraRectangle point3OpticalCameraRectangle = meshTriangle.getPoint3OpticalCameraRectangle();
      
      if ( isWhichSide(point1OpticalCameraRectangle.getRegion(), point2OpticalCameraRectangle.getRegion(), regionPanelRectangle) &&
           isWhichSide(point2OpticalCameraRectangle.getRegion(), point3OpticalCameraRectangle.getRegion(), regionPanelRectangle) &&
           isWhichSide(point3OpticalCameraRectangle.getRegion(), point1OpticalCameraRectangle.getRegion(), regionPanelRectangle))
      {
        meshTriangle.setIsUsed(true);
        region.setMeshTriangle(meshTriangle);
        isMeshTriangleFound = true;
        break;
      }
    }
    
    // If we cannot find any MeshTriangle that encapsulates the region,
    // we will try to find nearest MeshTriangle instead
    if (isMeshTriangleFound == false)
    {    
      boolean isFirst = true;
      double best = 0.0;
      int bestIndex = -1;
      
      for(int i=0; i < meshTriangles.size(); i++)
      {
        MeshTriangle meshTriangle = meshTriangles.get(i);

        OpticalCameraRectangle point1OpticalCameraRectangle = meshTriangle.getPoint1OpticalCameraRectangle();
        OpticalCameraRectangle point2OpticalCameraRectangle = meshTriangle.getPoint2OpticalCameraRectangle();
        OpticalCameraRectangle point3OpticalCameraRectangle = meshTriangle.getPoint3OpticalCameraRectangle();

        double d1 = Math.sqrt(getDistanceSquare(regionPanelRectangle, point1OpticalCameraRectangle.getRegion()));
        double d2 = Math.sqrt(getDistanceSquare(regionPanelRectangle, point2OpticalCameraRectangle.getRegion()));
        double d3 = Math.sqrt(getDistanceSquare(regionPanelRectangle, point3OpticalCameraRectangle.getRegion()));

        double distance = d1 + d2 + d3;

        if ( (isFirst) || (distance < best) )
        {
          isFirst = false;
          bestIndex = i;
          best = distance;
        }
      }
      
      // Mark MeshTriangle as used flag
      MeshTriangle usedMeshTriangle = meshTriangles.get(bestIndex);
      usedMeshTriangle.setIsUsed(true);
      region.setMeshTriangle(usedMeshTriangle);
    }
  }
  
  /**
   * This function calculates which side of line (p1 p2) the point p3 is on
   * by calculating the cross product of the vectors (p1 p2) and (p1 p3).
   * 
   * @author Cheah Lee Herng 
   */
  private boolean isWhichSide(PanelRectangle point1PanelRectangle,
                              PanelRectangle point2PanelRectangle,
                              PanelRectangle point3PanelRectangle)
  {
    Assert.expect(point1PanelRectangle != null);
    Assert.expect(point2PanelRectangle != null);
    Assert.expect(point3PanelRectangle != null);
    
    return ( getCrossProduct(point1PanelRectangle, point2PanelRectangle, point3PanelRectangle) > 0.0 );
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private List<MeshTriangle> cullOverlapMeshTriangle(List<MeshTriangle> meshTriangles)
  {
    Assert.expect(meshTriangles != null);
    
    List<Integer> indexToRemove = new ArrayList<Integer>();
    List<MeshTriangle> triangleList = new ArrayList<MeshTriangle>();
   
    for(int i=0; i < meshTriangles.size(); i++)
    {
      MeshTriangle meshTriangle = meshTriangles.get(i);
      triangleList.add(meshTriangle);
      
      for(MeshTriangle targetMeshTriangle : meshTriangles)
      {
        if (meshTriangle.equals(targetMeshTriangle))
          continue;
        
        if (meshTriangle.contains(targetMeshTriangle))
        {
          indexToRemove.add(i);
          break;
        }
      }
    }
    
    // Do the actual remove
    for(Integer index : indexToRemove)
    {
      MeshTriangle meshTriangle = triangleList.get(index);
      meshTriangles.remove(meshTriangle);
    }
    triangleList.clear();
    return meshTriangles;
  }
}
