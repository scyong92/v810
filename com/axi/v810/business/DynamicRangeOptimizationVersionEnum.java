package com.axi.v810.business;

import com.axi.util.*;
import java.util.*;

/**
 * @author Kok Chun, Tan
 * Version 0 - Does not include Camera 0
 * Version 1 - Include Camera 0
 */
public class DynamicRangeOptimizationVersionEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  private int _version;  
  private static Map<Integer, DynamicRangeOptimizationVersionEnum> _versionNumberToEnumMap = new HashMap<Integer, DynamicRangeOptimizationVersionEnum>();
  private static java.util.List<DynamicRangeOptimizationVersionEnum> _dynamicRangeOptimizationVersionEnumList = new ArrayList<DynamicRangeOptimizationVersionEnum>();

  public static final DynamicRangeOptimizationVersionEnum VERSION_0  = new DynamicRangeOptimizationVersionEnum(++_index);
  public static final DynamicRangeOptimizationVersionEnum VERSION_1  = new DynamicRangeOptimizationVersionEnum(++_index);
  
  /**
   * @author Kok Chun, Tan
   */
  private DynamicRangeOptimizationVersionEnum(int version)
  {
    super(version);
    _version = version;
    
    _versionNumberToEnumMap.put(version, this);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public int getVersion()
  {
    return _version;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static java.util.List<DynamicRangeOptimizationVersionEnum> getAllDynamicRangeOptimizationVersionEnumList()
  {
    if (_dynamicRangeOptimizationVersionEnumList.isEmpty() == false)
      _dynamicRangeOptimizationVersionEnumList.clear();
    
    _dynamicRangeOptimizationVersionEnumList.add(VERSION_0);
    _dynamicRangeOptimizationVersionEnumList.add(VERSION_1);
    
    return _dynamicRangeOptimizationVersionEnumList;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static java.util.List<String> getAllDynamicRangeOptimizationVersionEnumListInString()
  {
    java.util.List<String> dynamicRangeOptimizationVersionEnumListInString = new ArrayList<String>();
    for (DynamicRangeOptimizationVersionEnum droEnum : getAllDynamicRangeOptimizationVersionEnumList())
    {
      dynamicRangeOptimizationVersionEnumListInString.add(String.valueOf(droEnum.getVersion()));
    }
    
    return dynamicRangeOptimizationVersionEnumListInString;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static DynamicRangeOptimizationVersionEnum getDynamicRangeOptimizationVersionEnumFromVersionNumber(int version)
  {
    Assert.expect(version >= 0);
    
    return _versionNumberToEnumMap.get(version);
  }
}
