package com.axi.v810.business;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Bill Darbie
 */
public class Test_SerialNumberToProjectName extends UnitTest
{

  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_SerialNumberToProjectName());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      SerialNumberToProjectName numToName = new SerialNumberToProjectName();
      String fileName = getTestDataDir() + File.separator + "serialNumToProject.config";
      numToName.setConfigFileNameForUnitTesting(fileName);

      // try a serial number that has no match
      String projName = numToName.getProjectNameFromSerialNumber("123");
      Assert.expect(projName.equals(""));

      // 12?4 project1
      projName = numToName.getProjectNameFromSerialNumber("1234");
      Assert.expect(projName.equals("project1"));

      // 12?4 project1
      projName = numToName.getProjectNameFromSerialNumber("12d4");
      Assert.expect(projName.equals("project1"));

      // 12?4 project1
      projName = numToName.getProjectNameFromSerialNumber("1234");
      Assert.expect(projName.equals("project1"));

      // 1111* project2
      projName = numToName.getProjectNameFromSerialNumber("1111");
      Assert.expect(projName.equals("project2"));

      // 1111* project2
      projName = numToName.getProjectNameFromSerialNumber("1111890");
      Assert.expect(projName.equals("project2"));

      // 77[a|b]89 project3
      projName = numToName.getProjectNameFromSerialNumber("77a89");
      Assert.expect(projName.equals("project3"));

      // 7.7? project 4
      projName = numToName.getProjectNameFromSerialNumber("7772");
      Assert.expect(projName.equals(""));

      // 7.7? project 4
      projName = numToName.getProjectNameFromSerialNumber("7.72");
      Assert.expect(projName.equals("project4"));

      Collection<SerialNumberRegularExpressionData> coll = numToName.getSerialNumberRegularExpressionData();
      Assert.expect(coll.size() == 4);
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }

  }
}
