package com.axi.v810.images;

import java.awt.*;
import java.net.*;
import javax.swing.*;
import com.axi.v810.util.ResourceHandler;

/**
  * This class provides common access to images used in the 5DX software.
  * The image files exists in the same directory as this class and this fact
  * is used to tell the ResourceHandler where to look for the images.
  *
  * @see ResourceHandler
  * @author Steve Anonson
  */
public class Image5DX
{
  public static final String AXI = "axi.gif";  // Agilent spark and name
  public static final String AXI_72 = "axi72.gif";  // Larger Agilent logo
  public static final String FRAME_5DX = "frame5DX.gif"; // frame icon for 5DX software
  public static final String FRAME_VIRGO = "Virgo_frame.gif"; // frame icon for 5DX software
  public static final String FRAME_X5000 = "frameIcon.png";//"frame_x5000.gif"; // frame icon for x5000 software
  public static final String AXI_ABOUT = "axi_stack.gif";  // about box
  public static final String AXI_X5000_SPLASH = "splash_v810.gif"; //splash screen
  public static final String AXI_X6000_WIZARD = "wizard_v810.png";//wizard screen
  public static final String AXI_SPARK = "axiSpark.png"; // used a png file because I couldn't create a semi-transparent file with gifs.
  public static final String AXI_ICON = "axi_icon.gif"; // used a png file because I couldn't create a semi-transparent file with gifs.
//  public static final String AXI_ICON = "D:\\home\\george\\Documents\\My Pictures\\axi\\axiLogoIcon.png";

/** @todo wpd - George - can we get rid of the x5000 junk here */
  public static final String HOME_ICON = "homeButton.png";//
  public static final String HOME_DISABLE_ICON = "homeDisableButton.png";
  public static final String TEST_DEVELOPMENT_ICON = "testDevButton.png"; //
  public static final String TEST_DEVELOPMENT_DISABLE_ICON = "testDevDisableButton.png"; 
  public static final String TEST_EXECUTION_RUN_ICON = "testExecButton.png";// 
  public static final String TEST_EXECUTION_RUN_DISABLE_ICON = "testExecDisableButton.png";
  public static final String CONFIG_BUTTON_ICON = "configButton.png";//
  public static final String CONFIG_DISABLE_BUTTON_ICON = "configDisableButton.png";
  public static final String SERVICES_BUTTON_ICON = "servicesButton.png";
  public static final String SERVICES_DISABLE_BUTTON_ICON = "servicesDisableButton.png";
  public static final String VIRTUAL_LIVE_ICON = "virtualLiveButton.png";
  public static final String OPEN_FILE = "openFile.gif";
  public static final String SAVE_FILE = "saveFile.gif";
  public static final String SAVE_DIAGNOSTIC_IMAGE = "saveDiagnosticImage.jpg";
  public static final String SAVE_ALL_FILE = "saveAll.png";
  public static final String SAVE_SCREEN = "saveScreen_16.png";
  public static final String AUTO_DISPLAY = "autoDisplay.png";
  
  //@author Chin-Seong.kee
  public static final String CAD_CREATOR = "testDevButton.png";
  
  // @author Chong, Wei Chin
  public static final String IMPORT_NDF = "import.gif";
  public static final String CLOSE_FILE = "close.gif";
  public static final String LOGOUT_BUTTON = "logout.gif";
  public static final String UNDO_BUTTON = "undo.gif";
  public static final String REDO_BUTTON = "redo.gif";
  public static final String STARTUP_BUTTON = "startup.png";
  public static final String SHUTDOWN_BUTTON = "shutdown.png";
  public static final String SHORTTERM_XRAY_SHUTDOWN_BUTTON = "shutdownXray.png";
  public static final String LOAD_PANEL_BUTTON = "loadPanel.png";
  public static final String UNLOAD_PANEL_BUTTON = "unloadPanel.png";

  public static final String BIG_OPEN_FILE = "bigOpenFile.gif";
  public static final String BIG_CLOSE_FILE = "bigCloseFile.gif";
  public static final String HELP = "help.gif";

  // Main Home Images (temporary)
  public static final String HOME_1 = "main1.gif";

  // Config Images (temporary)
  public static final String CONFIG_1 = "config1.jpg";
  public static final String CONFIG_2 = "config2.jpg";

  // HW Status images
  public static final String HWSTATUS_SMT_COMM = "SMTComm.gif";  // diagram used in HW Status

  // C and D images
  public static final String CD_FRAME_5DX = "frameToolbox.gif";  // frame icon for C and D Toolbox
  public static final String CD_PASS = "cd_pass.gif";
  public static final String CD_FAIL = "cd_fail.gif";
  public static final String CD_ABORT = "cd_abort.gif";
  public static final String CD_STARTUP = "cd_startup.gif";
  public static final String CD_RUNNING = "cd_running.gif";
  public static final String CD_CANCELLED = "cd_cancelled.gif";
  public static final String CD_PARTIAL_RUN = "cd_partialrun.gif";
  public static final String CD_PASSED_TEST = "cd_passedTest.gif";
  public static final String CD_FAILED_TEST = "cd_failedTest.gif";
  public static final String CD_ABORT_TEST = "cd_abortTest.gif";
  public static final String CD_DISABLED_TEST = "cd_disabledTest.gif";
  public static final String CD_ENABLED_TEST = "cd_enabledTest.gif";
  public static final String CD_CANCELLED_TEST = "cd_cancelledTest.gif";
  public static final String CD_PASSED_LEAF_TEST = "cd_passedLeafTest.gif";
  public static final String CD_FAILED_LEAF_TEST = "cd_failedLeafTest.gif";
  public static final String CD_ABORT_LEAF_TEST = "cd_abortLeafTest.gif";
  public static final String CD_CANCELLED_LEAF_TEST = "cd_cancelledLeafTest.gif";
  public static final String CD_ENABLED_LEAF_TEST = "cd_enabledLeafTest.gif";
  public static final String CD_DISABLED_LEAF_TEST = "cd_disabledLeafTest.gif";
  public static final String ERROR_STATE = "error_red_x.png";
  public static final String ON_STATE = "status-online.gif"; //"greenCircle.gif";
  public static final String OFF_STATE = "status-offline.gif"; //"yellowCircle.gif";
  public static final String UNKNOWN_STATE = "unknownState.png";
  public static final String INTERLOCK_SYSTEM_DRAWING = "interlockMachineDrawing.png";
  public static final String KEYLOCK_INTERLOCK_SYSTEM_DRAWING = "keylockInterlockMachineDrawing.png";

  //sheng-chuan.yong
  public static final String XRAY_POWER_ON_STATUS = "xrayPowerOn.gif";
  public static final String XRAY_POWER_OFF_STATUS = "xrayPowerOff.gif";
  
  // AlgoTuner images
  public static final String AT_FRAME_5DX = "at_tuner.gif";  // frame icon for the Tuner
  public static final String AT_VALIDATE_DISABLED_ALGO = "at_validate_disable_algo.png";
  public static final String AT_VIEW_MEASUREMENT_JOINT = "at_view_measurement_joint.png";
  public static final String AT_COPY_THRESHOLDS = "copyThresholds.png";
  public static final String AT_WRITE_LEARNED_DATA = "at_writeLearnedData.gif";
  public static final String AT_SAVE_PANEL_PROGRAM = "at_save.gif";
//  public static final String AT_DELETE_LEARNED_DATA = "at_cut.gif";
  public static final String AT_DELETE_LEARNED_DATA = "delete.gif";
  public static final String AT_SAVE_IMAGES = "at_saveimages.gif";
  public static final String REVIEW_MEASUREMENTS = "reviewMeasurements.png";
  public static final String GENERATE_IMAGE_SET = "createSet.png";
  public static final String SELECT_IMAGE_SET = "selectSet.png";
  public static final String RUN_TESTS = "runTests.png";
  public static final String REVIEW_DEFECTS = "reviewDefects.png";
  public static final String MEASURE = "measure.png";
  public static final String TOGGLE_GRAPHICS = "toggleGraphics.png";
  public static final String TOGGLE_IMAGE_STAY = "toggleImageStay.gif";
  public static final String TOGGLE_IMAGE_FIT_TO_SCREEN = "dc_fitscreen.gif"; //Anthony01005
  public static final String GENERATE_REPORT = "genReport.png";
  public static final String MOVE_DATA = "moveData.png";
  //Lim, Lay Ngor - Post Processing Image Enhancer
  public static final String TOGGLE_POST_PROCESSING_SMALL = "enhanceImage_16.png";
  public static final String TOGGLE_POST_PROCESSING = "enhanceImage.png";
  public static final String TOGGLE_POST_PROCESSING_EXPERT = "enhanceImage_expert_16.png";
//  public static final String APPLY_RESIZE_IMAGE = "Resize_ic.png";
//  public static final String APPLY_BOX_FILTER = "boxfilter_ic_2.png";  
//  public static final String APPLY_CLAHE = "CLAHE_ic_3.png";
//  public static final String APPLY_REMOVE_ARTIFACT_BY_BOX_FILTER = "remove_artifact_boxfilter_ic_3.png";  
  //Using enhancer 1, 2 & 3 image is because not to let the competitor directly know the 
  //post-processing algorithm type that we use.
  public static final String APPLY_CLAHE = "enhancer1_16.png";  
  public static final String APPLY_REMOVE_ARTIFACT = "enhancer2_16.png";  
  public static final String APPLY_FFTBANDPASS_FILTER = "enhancer3_16.png";
  public static final String APPLY_MOTION_BLUR_FILTER = "motionBlurFilter_16.png";
  public static final String APPLY_INVERT_IMAGE = "invertImage_16.png";
  
  //Ying-Huan.Chu
  public static final String TOGGLE_2D_IMAGE = "toggle2DImage.png";
  public static final String PREVIOUS_IMAGE = "vc_uparrow.gif";
  public static final String NEXT_IMAGE = "vc_downarrow.gif";
  public static final String SHARPNESS_PROFILE = "sharpnessProfile.gif";

  public static final String AT_PAUSE = "at_pause.gif";
  public static final String AT_START = "at_start.gif";
  public static final String AT_STOP = "at_stop.gif";
  public static final String AT_RUN_TO_BREAK = "at_runtobreak.gif";
  public static final String AT_RUN_TO_END = "at_runtoend.gif";
  public static final String AT_RUN_TO_JOINT = "at_runtojoint.gif";
  public static final String AT_RUN_TO_INSPECTION_REGION = "at_runtoinspectionregion.gif";
  public static final String AT_RUN_TO_SLICE = "at_runtoslice.png";
  public static final String AT_PLAY_ENABLED_PAUSE_DISABLED = "at_playenabledpause.png";
  public static final String AT_PLAY_DISABLED_PAUSE_ENABLED = "at_playpauseenabled.png";

  public static final String REVIEW_DEFECTS_NO_INSPECTION_IMAGE = "rd_noInspectionImage.jpg";

  //Bee Hoon
  public static final String STITCH_COMPONENT_IMAGES = "imageStitching.gif";
  
  //Siew Yeng - XCR-2761
  public static final String STITCH_ENHANCED_COMPONENT_IMAGES = "enhancedImageStitching.png";

  // Main menu images
  public static final String MM_HELP = "help.gif";
  public static final String MM_STARTUP = "startup.gif";
  public static final String MM_OPEN_PANEL = "open.gif";
  public static final String MM_LOAD_PANEL = "loadpanel.gif";
  public static final String MM_UNLOAD_PANEL = "unloadpanel.gif";
  public static final String MM_ALIGN_AND_MAP = "rulers.gif";
  public static final String MM_CHECK_CAD = "viewVerifier.gif";
  public static final String MM_CADLINK = "MM_testLink.gif";
  public static final String MM_DEFECT_REPORTER = "defectreport.gif";
  public static final String MM_REVIEW_DEFECTS = "reviewdefect.gif";
  public static final String MM_REVIEW_MEASUREMENTS = "MM_reviewmeasure.gif";
  public static final String MM_ALGORITHM_TUNER = "MM_tuner.gif";
  public static final String MM_PAUSE = "at_pause.gif";
  public static final String MM_START = "at_start.gif";
  public static final String MM_STOP = "at_stop.gif";
  public static final String MM_NO_PANEL = "te_nopanel.jpg";//"mm_nopanel.gif";
  public static final String MM_UP_ARROW = "mm_uparrow.gif";
  public static final String MM_DOWN_ARROW = "mm_downarrow.gif";
  public static final String MM_BLANK_ARROW = "mm_blankarrow.gif";
  public static final String MM_HOURGLASS_1 = "hourglass1.gif";
  public static final String MM_HOURGLASS_2 = "hourglass2.gif";
  public static final String MM_HOURGLASS_3 = "hourglass3.gif";
  public static final String MM_HOURGLASS_4 = "hourglass4.gif";
  public static final String MM_HOURGLASS_5 = "hourglass5.gif";
  public static final String MM_HOURGLASS_6 = "hourglass6.gif";
  public static final String MM_HOURGLASS_7 = "hourglass7.gif";
  public static final String MM_HOURGLASS_8 = "hourglass8.gif";
  public static final String MM_HOURGLASS_9 = "hourglass9.gif";
  public static final String MM_HOURGLASS_10 = "hourglass10.gif";

  // test execution images
  public static final String TE_ABORT = "te_abort.png";
  public static final String TE_NO_PANEL = "te_nopanel.jpg";

  // Panel Setup images
  public static final String PS_SEPARATION_BY_OFFSET = "ps_separation_by_offset.gif";
  public static final String PS_SEPARATION_BY_SPACING = "ps_separation_by_spacing.gif";

  // DrawCAD images
  public static final String DC_FRAME_5DX = "at_tuner.gif";  // frame icon for the DrawCAD window
  public static final String DC_VIEW_TOPSIDE = "dc_viewtop.gif";
  public static final String DC_VIEW_BOTTOMSIDE = "dc_viewbottom.gif";
  public static final String DC_VIEW_BOTHSIDES = "dc_viewboth.gif";
  public static final String DC_VIEW_FROMTOP = "dc_viewfromtop.gif";
  public static final String DC_VIEW_FROMBOTTOM = "dc_viewfrombottom.gif";
  public static final String DC_ZOOM_IN = "dc_zoomin.gif";
  public static final String DC_ZOOM_OUT = "dc_zoomout.gif";
  public static final String DC_ZOOM_RECTANGLE = "dc_zoomrect.gif";
  public static final String DC_ZOOM_RESET = "dc_zoomreset.gif";
  public static final String DC_MOVE = "dc_move.gif";
  public static final String DC_GROUPSELECT = "dc_groupselect.gif";
  public static final String DC_VIEW_SURFACEMAP = "dc_surfmap.gif";
  public static final String DC_INCREASE_BRIGHTNESS = "dc_brightness(+)_light.png"; //"dc_brightness(+).png";
  public static final String DC_DECREASE_BRIGHTNESS = "dc_brightness(-)_light.png";// "dc_brightness(-).png";
  public static final String DC_INCREASE_CONTRAST = "dc_contrast(+).png";
  public static final String DC_DECREASE_CONTRAST = "dc_contrast(-).png";
  public static final String DC_ENHANCEMENT = "dc_enhance.png";
  public static final String DC_ENHANCEMENT_INCREASE = "dc_enhance_up.png";
  public static final String DC_ENHANCEMENT_DECREASE = "dc_enhance_down.png";
  public static final String DC_ORIGINAL = "dc_original.png";
  public static final String DC_SELECT_REGION = "dc_select_region.png";
  public static final String DC_UNTESTABLE_AREA = "dc_untestable_area.gif";
  
  //Classifier Tool images
  public static final String CT_ZOOM_IN = "ct_zoomin.gif";
  public static final String CT_FULL_IMAGE = "ct_fullimage.gif";
  public static final String CT_LEFT_ARROW = "ct_leftarrow.gif";
  public static final String CT_RIGHT_ARROW = "ct_rightarrow.gif";
  public static final String CT_UP_ARROW = "ct_uparrow.gif";
  public static final String CT_DOWN_ARROW = "ct_downarrow.gif";
  public static final String CT_THUMBS_UP = "ct_thumbsup.gif";
  public static final String CT_THUMBS_DOWN = "ct_thumbsdown.gif";

  //Edit CAD images
  public static final String EC_SEPARATION_BY_OFFSET = "ec_padarray_by_offset.gif";
  public static final String EC_SEPARATION_BY_SPACING = "ec_padarray_by_space.gif";
  
  //Land Pattern Image
  public static final String LPE_SEPARATION_BY_OFFSET_FROM_SELECTED_PAD_ORIGIN = "ec_padarray_by_offset_from_selected_pad_origin.gif";
  public static final String LPE_SEPARATION_BY_SPACING_FROM_SELECTED_PAD_ORIGIN = "ec_padarray_by_space_from_selected_pad_origin.gif";

  // Verify CAD images
  public static final String VC_UP_ARROW = "vc_uparrow.gif";
  public static final String VC_DOWN_ARROW = "vc_downarrow.gif";

  // Test execution and TDE both use this
  public static final String UP_ARROW = "uparrow.png";
  public static final String DOWN_ARROW = "downarrow.png";

  // toolbar & button icon
  public static final String FIND = "find.png";

  // test coverage report icon
  public static final String TEST_COVERAGE_REPORT = "test_coverage_report_icon.png";
  //start defect packager icon
  public static final String START_DEFECT_PACKAGER = "startDefectPackager.gif";
  //start defect packager icon
  public static final String STOP_DEFECT_PACKAGER = "stopDefectPackager.gif";
  //change system magnification icon
  public static final String CHANGE_SYSTEM_MAGNIFICATION = "changeSystemMagnification.gif";
  // two pin land pattern checking icon - Sham, Khang Shian
  public static final String TWO_PIN_LAND_PATTERN_CHECKING = "two_pin_land_pattern_checking_icon.png";
  // drag optical region icon
  public static final String DRAG_OPTICAL_REGION = "drag_optical_region.png";
  // display pad name icon
  public static final String DISPLAY_PAD_NAME = "display_pad_name.png";
  
  public static final String DISPLAY_NO_LOAD_COMPONENT = "icon_remote_control_m.png";

  // show v810 graphic engine remote controller
  public static final String GRAPHIC_CONTROLLER_NAME = "icon_remote_control_m.png";
  
  // show v810 error handler icon
  public static final String INFO_HANDLER_FRAME_ICON_NAME = "help.gif";
  
  //Kee Chin Seong - Shut down RMS 
  public static final String SHUT_DOWN_REMOTE_MOTION_SERVER = "shutdownRMS.png";
  
  /**
    * This method will find the named image file passed in as a parameter
    * and return an Image instance to the calling routine.  The Image5DX class
    * path is searched for the file.
    *
    * @param imageName - file name of the image to be loaded
    * @author Steve Anonson
    */
  public static Image getImage( String imageName )
  {
    if (imageName == null)
      return null;
    ImageIcon imageIcon = ResourceHandler.getImageIcon(Image5DX.class.getResource(imageName));
    return imageIcon.getImage();
  }

  /**
    * This method will find the named image file passed in as a parameter
    * and return an Image instance to the calling routine.
    *
    * @param imagePath - URL path to the image to be loaded
    * @author Steve Anonson
    */
  public static Image getImage( URL imagePath )
  {
    ImageIcon imageIcon = ResourceHandler.getImageIcon( imagePath );
    return imageIcon.getImage();
  }

  /**
    * This method will find the named image file passed in as a parameter
    * and return an ImageIcon instance to the calling routine.  The Image5DX class
    * path is searched for the file.
    *
    * @param imageName - file name of the image to be loaded
    * @author Steve Anonson
    */
  public static ImageIcon getImageIcon( String imageName )
  {
    if (imageName == null)
      return null;
    return ResourceHandler.getImageIcon(Image5DX.class.getResource(imageName));
  }

  /**
    * This method will find the named image file passed in as a parameter
    * and return an Image instance to the calling routine.
    *
    * @param imagePath - URL path to the image to be loaded
    * @author Steve Anonson
    */
  public static ImageIcon getImageIcon( URL imagePath )
  {
    return ResourceHandler.getImageIcon( imagePath );
  }

}
