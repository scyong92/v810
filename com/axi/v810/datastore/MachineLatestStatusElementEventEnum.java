package com.axi.v810.datastore;

import java.io.*;

/**
 * @author Khaw Chek Hau
 * @XCR3670: V-ONE AXI latest status monitoring log
 */
public class MachineLatestStatusElementEventEnum extends com.axi.util.Enum implements Serializable
{  
  private static int _index = -1;
  private String _machineLatestStatusElementEvent;
  
  public static final MachineLatestStatusElementEventEnum LAST_UPDATE_TIME = new MachineLatestStatusElementEventEnum(++_index, "LastUpdateTime"); 
  public static final MachineLatestStatusElementEventEnum SOFTWARE_VERSION = new MachineLatestStatusElementEventEnum(++_index, "SoftwareVersion"); 
  public static final MachineLatestStatusElementEventEnum CURRENT_ACTIVE_PROGRAM = new MachineLatestStatusElementEventEnum(++_index, "CurrentActiveProgram"); 
  public static final MachineLatestStatusElementEventEnum LOGIN_USER_NAME = new MachineLatestStatusElementEventEnum(++_index, "LoginUserName"); 
  public static final MachineLatestStatusElementEventEnum PRODUCTION_RUNNING_RECIPE = new MachineLatestStatusElementEventEnum(++_index, "Recipe"); 
  public static final MachineLatestStatusElementEventEnum PRODUCTION_RUNNING_VARIATION = new MachineLatestStatusElementEventEnum(++_index, "Variation"); 
  public static final MachineLatestStatusElementEventEnum SERIAL_NUMBER = new MachineLatestStatusElementEventEnum(++_index, "SerialNumber"); 
  public static final MachineLatestStatusElementEventEnum PANEL_PINS_PROCESSED = new MachineLatestStatusElementEventEnum(++_index, "PanelPinsProcessed"); 
  public static final MachineLatestStatusElementEventEnum PANEL_PINS_DEFECTIVE = new MachineLatestStatusElementEventEnum(++_index, "PanelPinsDefective"); 
  public static final MachineLatestStatusElementEventEnum PANEL_COMPONENTS_DEFECTIVE = new MachineLatestStatusElementEventEnum(++_index, "PanelComponentsDefective"); 
  public static final MachineLatestStatusElementEventEnum INSPECTION_TIME = new MachineLatestStatusElementEventEnum(++_index, "InspectionTime"); 
  public static final MachineLatestStatusElementEventEnum INSPECTION_END_TIME = new MachineLatestStatusElementEventEnum(++_index, "InspectionEndTime"); 
  
  /**
   * @author Khaw Chek Hau
   * @XCR3670: V-ONE AXI latest status monitoring log
   */
  private MachineLatestStatusElementEventEnum(int id, String machineLatestStatusElementEvent)
  {
    super(id);
    _machineLatestStatusElementEvent = machineLatestStatusElementEvent;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3670: V-ONE AXI latest status monitoring log
   */
  public String toString()
  {
    return String.valueOf(_machineLatestStatusElementEvent);
  }
}
