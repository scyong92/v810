package com.axi.v810.datastore.userAccounts;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: UserAccountsReader</p>
 *
 * <p>Description: This class reads a UserAccounts file in binary format that was created by
 * UserAccountsWriter.
 *
 * </p>
 * Format:<br>
 *   File revision: int<br>
 *   Number of accounts: int<br>
 * <br>
 *   For each account:<br>
 *     Account Name: string encoded and saved as ints<br>
 *       num of ints: int<br>
 *       array[num] of ints<br>
 *     Account Full Name: encoded string<br>
 *     Account Password: encoded string<br>
 *     Account Type: int<br>
 *     HiddenAccount: boolean <br>
 *     InternalAccount: boolean<br>
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */
public class UserAccountsReader
{
  private UserAccountsCodex _userAccountsCodex;
  private EnumToUniqueIDLookup _enumToUniqueIDLookup;
  private FileInputStream _fileIS;
  private DataInputStream _dataIS;
  private String _accountsFilePath;

  private java.util.ArrayList<UserAccountInfo> _userAccounts;

  /**
   * @author George Booth
   */
  public UserAccountsReader()
  {
    _accountsFilePath = FileName.getUserAccountsFullPath();
    _userAccountsCodex = new UserAccountsCodex();
    _enumToUniqueIDLookup = EnumToUniqueIDLookup.getInstance();
  }

  /**
   * @author George Booth
   */
  public java.util.ArrayList<UserAccountInfo> readUserAccountsFile() throws CorruptUserAccountsFileBusinessException
  {
    _userAccounts = new java.util.ArrayList<UserAccountInfo>();

    if (FileUtilAxi.exists(_accountsFilePath))
    {
      try
      {
        _fileIS = new FileInputStream(_accountsFilePath);

        // Wrap with a DataInputStream so that we can
        // use its read() methods.
        _dataIS = new DataInputStream(_fileIS);

        // version
        int fileVersion = readFileVersion();
        if (fileVersion == UserAccountsManager.CURRENT_VERSION)
        {
          readCurrentAccountsFileVersion();
        }
        else if (fileVersion == UserAccountsManager.PREVIOUS_VERSION)
        {
          readPreviousAccountsFileVersion();
        }
        else
        {
          userAccountsFileCorrupt(null);
        }
        // close streams but watch for exceptions
        try
        {
          close();
        }
        catch (DatastoreException dse)
        {
          dse.printStackTrace();
        }
      }
      catch (FileNotFoundException fnfe)
      {
        userAccountsFileCorrupt(fnfe);
      }
    }
    return _userAccounts;
  }

  /**
   * @author George Booth
   */
  private void readCurrentAccountsFileVersion() throws CorruptUserAccountsFileBusinessException
  {
    // number of user accounts
    int numAccounts = readNumAccounts();
    if (numAccounts < 1)
    {
      userAccountsFileCorrupt(null);
    }

    // get each account
    for (int account = 0; account < numAccounts; account++)
    {
      UserAccountInfo userAccount = new UserAccountInfo();
      String accountName = readEncodedString();
      userAccount.setAccountName(accountName);

      String fullName = readEncodedString();
      userAccount.setFullName(fullName);

      String password = readEncodedString();
      userAccount.setPassword(password);

      UserTypeEnum userTypeEnum = readUserTypeEnum();
      userAccount.setAccountType(userTypeEnum);

      boolean hiddenAccount = readHiddenAccount();
      userAccount.setHiddenAccount(hiddenAccount);

      boolean internalAccount = readInternalAccount();
      userAccount.setInternalAccount(internalAccount);

      _userAccounts.add(userAccount);
    }
    readEndOfFile();
  }

  /**
   * @author George Booth
   */
  private void readPreviousAccountsFileVersion()
  {
    // close streams but watch for exceptions
    try
    {
      close();
    }
    catch (DatastoreException dse)
    {
      dse.printStackTrace();
    }
    Assert.expect(false, "Tried to read unknown previous version of user.accounts");
  }

  /**
   * @author George Booth
   */
  private int readFileVersion() throws CorruptUserAccountsFileBusinessException
  {
    try
    {
      int fileVersion = _dataIS.readInt();
      return fileVersion;
    }
    catch (IOException ioe)
    {
      userAccountsFileCorrupt(ioe);
      return -1;
    }
  }

  /**
   * @author George Booth
   */
  private int readNumAccounts() throws CorruptUserAccountsFileBusinessException
  {
    try
    {
      int numAccounts = _dataIS.readInt();
      return numAccounts;
    }
    catch (IOException ioe)
    {
      userAccountsFileCorrupt(ioe);
      return 0;
    }
  }

  /**
   * @author George Booth
   */
  private String readEncodedString() throws CorruptUserAccountsFileBusinessException
  {
    try
    {
      int stringLength = _dataIS.readInt();
      int[] encodedString = new int[stringLength];
      for (int i = 0; i < stringLength; i++)
      {
        encodedString[i] = _dataIS.readInt();
      }
      return _userAccountsCodex.decode(encodedString);
    }
    catch (IOException ioe)
    {
      userAccountsFileCorrupt(ioe);
      return "";
    }
  }

  /**
   * @author George Booth
   */
  private UserTypeEnum readUserTypeEnum() throws CorruptUserAccountsFileBusinessException
  {
    try
    {
      int userTypeId = _dataIS.readInt();
      // sanity check
      int numberOfUserTypeEnums = com.axi.util.Enum.getNumEnums(UserTypeEnum.class);
      if (userTypeId < 0 || userTypeId >= numberOfUserTypeEnums)
      {
        userAccountsFileCorrupt(null);
      }
      // convert it
      UserTypeEnum userTypeEnum = _enumToUniqueIDLookup.getUserTypeEnum(userTypeId);
      return userTypeEnum;
    }
    catch (IOException ioe)
    {
      userAccountsFileCorrupt(ioe);
      return null;
    }
  }

  /**
   * @author George Booth
   */
  private boolean readHiddenAccount() throws CorruptUserAccountsFileBusinessException
  {
    try
    {
      boolean hiddenAccount = _dataIS.readBoolean();
      return hiddenAccount;
    }
    catch (IOException ioe)
    {
      userAccountsFileCorrupt(ioe);
      return false;
    }
  }

  /**
   * @author George Booth
   */
  private boolean readInternalAccount() throws CorruptUserAccountsFileBusinessException
  {
    try
    {
      boolean internalAccount = _dataIS.readBoolean();
      return internalAccount;
    }
    catch (IOException ioe)
    {
      userAccountsFileCorrupt(ioe);
      return false;
    }
  }

  /**
   * @author George Booth
   */
  private void readEndOfFile() throws CorruptUserAccountsFileBusinessException
  {
    try
    {
      // there should be no more bytes
      int endOfFile = _dataIS.readInt();
      userAccountsFileCorrupt(null);
    }
    catch (EOFException eofe)
    {
      // expected exception
    }
    catch (IOException ioe)
    {
      // unexpected exception
      userAccountsFileCorrupt(null);
    }
  }

  /**
   * @author George Booth
   */
  private void userAccountsFileCorrupt(Throwable cause) throws CorruptUserAccountsFileBusinessException
  {
    // close streams but watch for exceptions
    try
    {
      close();
    }
    catch (DatastoreException dse)
    {
      dse.printStackTrace();
    }
    deleteCorruptFile();
    CorruptUserAccountsFileBusinessException corruptBE = new CorruptUserAccountsFileBusinessException(_accountsFilePath);
    if (cause != null)
    {
      corruptBE.initCause(cause);
    }
    throw corruptBE;
  }

  /**
   * @author George Booth
   */
  private void deleteCorruptFile()
  {
    // delete corrupt file to allow app to restart with default accounts
    try
    {
      FileUtilAxi.delete(_accountsFilePath);
    }
    catch(DatastoreException dse)
    {
      // Could not delete the corrupt file, leave a stacktrace.
      dse.printStackTrace();
    }
  }

  /**
   * @author George Booth
   */
  private void close() throws DatastoreException
  {
    try
    {
      if (_dataIS != null)
        _dataIS.close();
      _dataIS = null;

      if (_fileIS != null)
        _fileIS.close();
      _fileIS = null;
    }
    catch (IOException ioe)
    {
      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_accountsFilePath);
      de.initCause(ioe);
      throw de;
    }
  }

}
