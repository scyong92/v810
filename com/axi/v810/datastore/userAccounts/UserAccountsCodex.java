package com.axi.v810.datastore.userAccounts;

import com.axi.v810.business.CorruptUserAccountsFileBusinessException;
import com.axi.v810.datastore.FileName;

/**
 *
 * <p>Title: UserAccountsCodex</p>
 *
 * <p>Description: This class provides encoding and decoding of string items in the UserAccounts
 * file.  The encoding prevents reading the accounts data with a text editor - only gibberish
 * is seen. </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */
public class UserAccountsCodex
{
  // constants used to encode/decode strings
  final private int _level1 = 60233;
  final private int _level2 = 13;

  public UserAccountsCodex()
  {
    // do nothing
  }

  /**
   * @author George Booth
   */
  public int[] encode(String rawString)
  {
    // The text rawString is converted to integers with each
    // integer value offset by a differing amount.  The results
    // are not decipherable when the saved data is viewed
    // as text.
    int[] encodedString = new int[rawString.length()];
    for (int i = 0; i < rawString.length(); i++)
    {
      char a = rawString.charAt(i);
      int d = (int)a;
      d = d + _level1 + (i * _level2);
      encodedString[i] = d;
    }
    return encodedString;
  }

  /**
   * @author George Booth
   */
  public String decode(int[] encodedString) throws CorruptUserAccountsFileBusinessException
  {
    String rawString = "";
    for (int i = 0; i < encodedString.length; i++)
    {
      int d = encodedString[i] - _level1 - (i * _level2);
      char a = (char)d;
      if (Character.isDefined(a))
      {
        rawString += a;
      }
      else
      {
        // corrupt file
        CorruptUserAccountsFileBusinessException corruptBE =
            new CorruptUserAccountsFileBusinessException(FileName.getUserAccountsFullPath());
        throw corruptBE;
      }
    }
    return rawString;
  }

}
