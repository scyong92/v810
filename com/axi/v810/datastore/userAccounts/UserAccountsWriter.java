package com.axi.v810.datastore.userAccounts;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 *
 * <p>Title: UserAccountsReader</p>
 *
 * <p>Description: This class creates a UserAccounts file in binary format that will be read by
 * UserAccountsReader.
 *
 * </p>
 * Format:<br>
 *   File revision: int<br>
 *   Number of accounts: int<br>
 * <br>
 *   For each account:<br>
 *     Account Name: string encoded and saved as ints<br>
 *       num of ints: int<br>
 *       array[num] of ints<br>
 *     Account Full Name: encoded string<br>
 *     Account Password: encoded string<br>
 *     Account Type: int<br>
 *     HiddenAccount: boolean <br>
 *     InternalAccount: boolean<br>
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */
public class UserAccountsWriter
{
  private UserAccountsCodex _userAccountsCodex;
  private EnumToUniqueIDLookup _enumToUniqueIDLookup;
  private FileOutputStream _fileOS;
  private DataOutputStream _dataOS;

  private String _accountsDir;
  private String _accountsFilePath;

  /**
   * @author George Booth
   */
  public UserAccountsWriter()
  {
    _accountsDir = Directory.getUserAccountsDir();
    _accountsFilePath = FileName.getUserAccountsFullPath();
    _userAccountsCodex = new UserAccountsCodex();
    _enumToUniqueIDLookup = EnumToUniqueIDLookup.getInstance();
  }

  /**
   * @author George Booth
   */
  public void writeUserAccountsFile(java.util.ArrayList<UserAccountInfo> userAccounts) throws DatastoreException
  {
    Assert.expect(userAccounts != null);

    // create config/accounts dir if needed
    if (FileUtilAxi.exists(_accountsDir) == false)
    {
      FileUtilAxi.createDirectory(_accountsDir);
    }

    // write a temp file first
    String tempPathName1 = _accountsFilePath + FileName.getTempFileExtension() + "1";
    boolean exception = false;

    // save the data
    try
    {
      _fileOS = new FileOutputStream(tempPathName1);
      // Wrap the FileOutputStream with a
      // DataOutputStream to obtain its write()
      // methods.
      _dataOS = new DataOutputStream(_fileOS);

      // version
      _dataOS.writeInt(UserAccountsManager.CURRENT_VERSION);

      // number of user accounts
      _dataOS.writeInt(userAccounts.size());

      for (UserAccountInfo userAccount : userAccounts)
      {
        writeEncodedString(userAccount.getAccountName());

        writeEncodedString(userAccount.getFullName());

        writeEncodedString(userAccount.getPassword());

        UserTypeEnum userTypeEnum = userAccount.getAccountType();
        _dataOS.writeInt(_enumToUniqueIDLookup.getUserTypeUniqueID(userTypeEnum));

        _dataOS.writeBoolean(userAccount.isHiddenAccount());

        _dataOS.writeBoolean(userAccount.isInternalAccount());
      }

    }
    catch(IOException ioe)
    {
      exception = true;
      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_accountsFilePath);
      de.initCause(ioe);
      throw de;
    }
    finally
    {
      if (exception == false)
      {
        // close streams
        close();
        // rename temp file
        if (FileUtilAxi.exists(tempPathName1))
          FileUtilAxi.rename(tempPathName1, _accountsFilePath);
      }
      else
      {
        // close streams but watch for exceptions
        try
        {
          close();
        }
        catch (DatastoreException ds1)
        {
          ds1.printStackTrace();
        }

        try
        {
          // delete temp file
          if (FileUtilAxi.exists(tempPathName1))
            FileUtilAxi.delete(tempPathName1);
        }
        catch (DatastoreException ds1)
        {
          ds1.printStackTrace();
        }
      }
    }
  }

  /**
   * @author George Booth
   */
  private void writeEncodedString(String rawString) throws IOException
  {
    int[] encodedString = _userAccountsCodex.encode(rawString);
    _dataOS.writeInt(encodedString.length);
    for (int i = 0; i < encodedString.length; i++)
    {
      _dataOS.writeInt(encodedString[i]);
    }
  }

  /**
   * @author George Booth
   */
  private void close() throws DatastoreException
  {
    try
    {
      if (_dataOS != null)
        _dataOS.close();
      _dataOS = null;
      if (_fileOS != null)
        _fileOS.close();
      _fileOS = null;
    }
    catch (IOException ioe)
    {
      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_accountsFilePath);
      de.initCause(ioe);
      throw de;
    }

  }

}
