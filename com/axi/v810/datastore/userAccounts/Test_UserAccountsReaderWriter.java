package com.axi.v810.datastore.userAccounts;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.FileName;
import com.axi.v810.datastore.Directory;
import com.axi.v810.datastore.DatastoreException;
import com.axi.v810.business.userAccounts.UserAccountsManager;
import com.axi.v810.business.userAccounts.UserAccountInfo;
import com.axi.v810.business.userAccounts.UserTypeEnum;
import com.axi.v810.datastore.EnumToUniqueIDLookup;
import com.axi.v810.datastore.CannotWriteDatastoreException;
import com.axi.v810.business.CorruptUserAccountsFileBusinessException;

/**
 * This class provides a basic test harness for User Accounts on the X6000.
 * @author George Booth
 */
public class Test_UserAccountsReaderWriter extends UnitTest
{
  private UserAccountsManager _userAccountsManager = null;
  private UserAccountsCodex _userAccountsCodex;
  private EnumToUniqueIDLookup _enumToUniqueIDLookup;
  private FileOutputStream _fileOS;
  private DataOutputStream _dataOS;

  private String _accountsDir;
  private String _accountsFilePath;

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @author George Booth
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    _accountsDir = Directory.getUserAccountsDir();
    _accountsFilePath = FileName.getUserAccountsFullPath();
    _userAccountsCodex = new UserAccountsCodex();
    _enumToUniqueIDLookup = EnumToUniqueIDLookup.getInstance();
    _userAccountsManager = UserAccountsManager.getInstance();

    // make sure the accounts directory and file don't exist
    deleteAccountFile();
    if (FileUtilAxi.exists(_accountsDir))
    {
      try
      {
        FileUtilAxi.delete(_accountsDir);
      }
      catch (DatastoreException de)
      {
        System.out.println("delete account dir DatastoreException = " + de.toString());
      }
    }

    //Trivial test to ensure that user accounts can be written and read back

    System.out.println("-- General test to ensure that user accounts can be written and read back");
    // get default accounts
    try
    {
      _userAccountsManager.readUserAccounts();
    }
    catch(Exception ex)
    {
      ex.printStackTrace(os);
    }

    // verify user "1" ia not in accounts
    if (_userAccountsManager.doesUserExist("1"))
    {
      Assert.expect(false, "User \"1\" found in accounts!!");
    }

    // add a new account to cause the user.accounts file to be written
    UserAccountInfo account1 = new UserAccountInfo("1", "First", "1", UserTypeEnum.ADMINISTRATOR);
    try
    {
      _userAccountsManager.addAccount(account1);
    }
    catch(Exception ex)
    {
      ex.printStackTrace(os);
    }

    // read it back
    try
    {
      _userAccountsManager.readUserAccounts();
    }
    catch(Exception ex)
    {
      ex.printStackTrace(os);
    }
    if (_userAccountsManager.doesUserExist("1") == false)
    {
      Assert.expect(false, "New user not found in accounts!!");
    }

    // bad stuff
    testCase1();
  }

  /**
   * @author George Booth
   */
  void testCase1()
  {
    System.out.println("\n--Test invalid usage");
    // file has bad version
    boolean exception = false;
    int version = 12;
    int numAccounts = 3;
    int numAccountsToWrite = 3;
    int userType = 1;
    writeFile(version, numAccounts, numAccountsToWrite, userType, false, false);
    try
    {
      exception = false;
      _userAccountsManager.readUserAccounts();
    }
    catch(Exception ex)
    {
      // exception expected
      exception = true;
      System.out.println(ex.getClass().getCanonicalName());
    }
    if (exception == false)
      System.out.println("Error - Bad version was read OK!!");

    // file has previous version
    version = UserAccountsManager.PREVIOUS_VERSION;
    writeFile(version, numAccounts, numAccountsToWrite, userType, false, false);
    try
    {
      exception = false;
      _userAccountsManager.readUserAccounts();
    }
    catch(Exception ex)
    {
      // exception expected
      exception = true;
      System.out.println(ex.getClass().getCanonicalName());
    }
    if (exception == false)
      System.out.println("Error - Previous version was read OK!!");

    // file has no accounts
    version = UserAccountsManager.CURRENT_VERSION;
    numAccounts = 0;
    numAccountsToWrite = 0;
    writeFile(version, numAccounts, numAccountsToWrite, userType, false, false);
    try
    {
      exception = false;
      _userAccountsManager.readUserAccounts();
    }
    catch(Exception ex)
    {
      // exception expected
      exception = true;
      System.out.println(ex.getClass().getCanonicalName());
    }
    if (exception == false)
      System.out.println("Error - Zero accounts was read OK!!");

    // file has too few accounts
    numAccounts = 4;
    numAccountsToWrite = 3;
    writeFile(version, numAccounts, numAccountsToWrite, userType, false, false);
    try
    {
      exception = false;
      _userAccountsManager.readUserAccounts();
    }
    catch(Exception ex)
    {
      // exception expected
      exception = true;
      System.out.println(ex.getClass().getCanonicalName());
    }
    if (exception == false)
      System.out.println("Error - Too few accounts was read OK!!");

    // file has too many accounts
    numAccounts = 2;
    numAccountsToWrite = 3;
    writeFile(version, numAccounts, numAccountsToWrite, userType, false, false);
    try
    {
      exception = false;
      _userAccountsManager.readUserAccounts();
    }
    catch(Exception ex)
    {
      // exception expected
      exception = true;
      System.out.println(ex.getClass().getCanonicalName());
    }
    if (exception == false)
      System.out.println("Error - Too many accounts was read OK!!");

    // file has bad user type
    numAccounts = 3;
    numAccountsToWrite = 3;
    userType = -1;
    writeFile(version, numAccounts, numAccountsToWrite, userType, false, false);
    try
    {
      exception = false;
      _userAccountsManager.readUserAccounts();
    }
    catch(Exception ex)
    {
      // exception expected
      exception = true;
      System.out.println(ex.getClass().getCanonicalName());
    }
    if (exception == false)
      System.out.println("Error - Bad user type was read OK!!");

    // file has unknown user type
    userType = com.axi.util.Enum.getNumEnums(UserTypeEnum.class) + 1;
    writeFile(version, numAccounts, numAccountsToWrite, userType, false, false);
    try
    {
      exception = false;
      _userAccountsManager.readUserAccounts();
    }
    catch(Exception ex)
    {
      // exception expected
      exception = true;
      System.out.println(ex.getClass().getCanonicalName());
    }
    if (exception == false)
      System.out.println("Error - Unknown user type was read OK!!");

    // file has bad hidden account flag
    userType = 1;
    writeFile(version, numAccounts, numAccountsToWrite, userType, true, false);
    try
    {
      exception = false;
      _userAccountsManager.readUserAccounts();
    }
    catch(Exception ex)
    {
      // exception expected
      exception = true;
      System.out.println(ex.getClass().getCanonicalName());
    }
    if (exception == false)
      System.out.println("Error - Bad hidden account flag was read OK!!");

    // file has bad internal account flag
    writeFile(version, numAccounts, numAccountsToWrite, userType, false, true);
    try
    {
      exception = false;
      _userAccountsManager.readUserAccounts();
    }
    catch(Exception ex)
    {
      // exception expected
      exception = true;
      System.out.println(ex.getClass().getCanonicalName());
    }
    if (exception == false)
      System.out.println("Error - Bad internal account flag was read OK!!");

  }

  void writeFile(int version, int numAccounts, int numAccountsToWrite, int userType, boolean badHidden, boolean badInternal)
  {
//    System.out.println("writeFile(" + version + ", " + numAccounts + ", " + numAccountsToWrite +
//                       ", " + userType + ", " + badHidden + ", " + badInternal + ")");
    deleteAccountFile();
    // write file as instructed
    try
    {
      _fileOS = new FileOutputStream(_accountsFilePath);
      // Wrap the FileOutputStream with a
      // DataOutputStream to obtain its write()
      // methods.
      _dataOS = new DataOutputStream(_fileOS);

      // version
      _dataOS.writeInt(version);

      // number of user accounts
      _dataOS.writeInt(numAccounts);

      for (int i = 0; i < numAccountsToWrite; i++)
      {
//        System.out.println("test writer - account " + i);
        writeEncodedString("account" + i);

        writeEncodedString("fullName" + i);

        writeEncodedString("password" + i);

        _dataOS.writeInt(userType);

        if (badHidden)
          _dataOS.writeInt(1000);
        else
          _dataOS.writeBoolean(false);

        if (badInternal)
          _dataOS.writeInt(1000);
        else
          _dataOS.writeBoolean(false);
      }
      close();
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

  private void deleteAccountFile()
  {
    try
    {
      if (FileUtilAxi.exists(_accountsFilePath))
      {
        FileUtilAxi.delete(_accountsFilePath);
      }
    }
    catch (DatastoreException de)
    {
      System.out.println("delete account file DatastoreException = " + de.toString());
    }
  }

  /**
   * @author George Booth
   */
  private void writeEncodedString(String rawString) throws IOException
  {
    int[] encodedString = _userAccountsCodex.encode(rawString);
    _dataOS.writeInt(encodedString.length);
    for (int i = 0; i < encodedString.length; i++)
    {
      _dataOS.writeInt(encodedString[i]);
    }
  }

  /**
   * @author George Booth
   */
  private void close()
  {
    try
    {
      if (_dataOS != null)
        _dataOS.close();
      _dataOS = null;
      if (_fileOS != null)
        _fileOS.close();
      _fileOS = null;
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

  public static void main(String[] args)
  {
    UnitTest.execute(new Test_UserAccountsReaderWriter());
  }
}
