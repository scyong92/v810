package com.axi.v810.datastore;

import java.io.*;

/**
 * @author Khaw Chek Hau
 * @XCR3670: V-ONE AXI latest status monitoring log
 */
public class MachineLatestStatusCategoryEventEnum extends com.axi.util.Enum implements Serializable
{  
  private static int _index = -1;
  private String _machineLatestStatusCategoryEvent;
  
  public static final MachineLatestStatusCategoryEventEnum MACHINE_INFO = new MachineLatestStatusCategoryEventEnum(++_index, "MachineInfo"); 
  public static final MachineLatestStatusCategoryEventEnum ACTIVE_PROGRAM = new MachineLatestStatusCategoryEventEnum(++_index, "ActiveProgram"); 
  public static final MachineLatestStatusCategoryEventEnum LAST_RUNNING_PRODUCTION = new MachineLatestStatusCategoryEventEnum(++_index, "LastRunningProduction"); 
  
  /**
   * @author Khaw Chek Hau
   * @XCR3670: V-ONE AXI latest status monitoring log
   */
  private MachineLatestStatusCategoryEventEnum(int id, String machineLatestStatusCategoryEvent)
  {
    super(id);
    _machineLatestStatusCategoryEvent = machineLatestStatusCategoryEvent;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3670: V-ONE AXI latest status monitoring log
   */
  public String toString()
  {
    return String.valueOf(_machineLatestStatusCategoryEvent);
  }
}
