package com.axi.v810.datastore;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

public class Test_FileCorruptDatastoreException extends UnitTest
{
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_FileCorruptDatastoreException());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    FileCorruptDatastoreException ex = new FileCorruptDatastoreException("fileName", 100);
    String message = ex.getLocalizedMessage();
    os.println(message);
  }
}
