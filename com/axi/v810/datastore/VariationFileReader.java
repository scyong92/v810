package com.axi.v810.datastore;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Kok Chun, Tan
 */
public class VariationFileReader
{
  private static VariationFileReader _instance = null;

  private LineNumberReader _is = null;
  private String _line = null;
  private String _fileName = null;
  private String _variationName = null;

  private boolean _hasNextLineWasCalled = false;
  private boolean _prevHasNextLineReturnValue = false;
  private transient static ProjectObservable _projectObservable;
  //XCR-3748: Component still remain NOLOAD after import BOM as Tested
  private static final String REFERENCE_DESIGNATOR_REGEX = "^[^a-zA-Z]*([\\sa-zA-Z0-9,!]+).*$";
  
  /**
   * @author Kok Chun, Tan
   */
  static
  {
    _projectObservable = ProjectObservable.getInstance();
  }

  /**
   * @author Kok Chun, Tan
   */
  private VariationFileReader()
  {
    // Do nothing
  }

  /**
   * @author Kok Chun, Tan
   */
  private void init()
  {
    // do nothing
  }

  /**
   * @author Kok Chun, Tan
   */
  public static synchronized VariationFileReader getInstance()
  {
    if (_instance == null)
    {
      _instance = new VariationFileReader();
    }
    return _instance;
  }

  /**
   * @author Kok Chun, Tan
   * component list, should considered under it.
   */
  public boolean readDataFile(Project project, final String addVariationFileName, final String variationName, final boolean isNoLoad) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(addVariationFileName != null);
    Assert.expect(variationName != null);

    boolean isVariationAdded = false;
    try
    {
      _is = ParseUtil.openFile(addVariationFileName);
      _fileName = addVariationFileName;
      _variationName = variationName;

      Map<String, java.util.List<String>> boardTypeToRefDesMap = new LinkedHashMap<String, java.util.List<String>>();
      Panel panel = project.getPanel();
      List<BoardType> boardTypes = panel.getBoardTypes();
      
      for (BoardType boardType : boardTypes)
      {
        String boardTypeName = boardType.getName();
        java.util.List<String> listOfRefDes = new java.util.ArrayList<String>();

        while (hasNextLine())
        {
          // get rid of leading and trailing spaces
          String refDes = getNextLine();
          
          //XCR-3748: Component still remain NOLOAD after import BOM as Tested
          refDes = refDes.replaceAll(REFERENCE_DESIGNATOR_REGEX , "$1");
          
          // Check if we have comma-separated line
          String[] referenceDesignatorArray = refDes.split(",");

          if (referenceDesignatorArray.length > 0)
          {
            // Multi ref des in single line, separated by comma
            for (int i = 0; i < referenceDesignatorArray.length; i++)
            {
              String componentRefDes = referenceDesignatorArray[i].trim().toUpperCase();
              if (panel.hasComponentType(boardTypeName, componentRefDes))
              {
                if (listOfRefDes.contains(componentRefDes) == false)
                {
                  listOfRefDes.add(componentRefDes);
                }
              }
            }
          }
          else
          {
            String componentRefDes = refDes.trim().toUpperCase();
            if (panel.hasComponentType(boardTypeName, componentRefDes))
            {
              if (listOfRefDes.contains(componentRefDes) == false)
              {
                listOfRefDes.add(componentRefDes);
              }
            }
          }
        }
        if (listOfRefDes.isEmpty() == false)
        {
          boardTypeToRefDesMap.put(boardTypeName, listOfRefDes);
        }
      }
      if (boardTypeToRefDesMap.isEmpty() == false)
      {
        isVariationAdded = addVariation(project, _variationName, boardTypeToRefDesMap, isNoLoad);
      }
    }
    finally
    {
      if (_is != null)
      {
        try
        {
          _is.close();
        }
        catch (IOException ioe)
        {
          DatastoreException dex = new DatastoreException(addVariationFileName);
          dex.initCause(ioe);
          throw dex;
        }
      }
    }
    return isVariationAdded;
  }

  /**
   * @author Kok Chun, Tan
   */
  private String getNextLine() throws DatastoreException
  {
    // check that hasNextLine() was already called.  If it was not then
    // call it automatically.
    if (_hasNextLineWasCalled == false)
    {
      if (hasNextLine() == false)
      {
        // there is no next line so throw an exception
        int lineNumber = getLineNumber();
        closeFileDuringExceptionHandling();
        throw new FileCorruptDatastoreException(_fileName, lineNumber + 1);
      }
    }
    _hasNextLineWasCalled = false;

    Assert.expect(_line != null);
    return _line;
  }

  /**
   * @author Kok Chun, Tan
   */
  private boolean hasNextLine() throws DatastoreException
  {
    // if this method was called already, do not parse again until getNextLine() is called
    if (_hasNextLineWasCalled)
    {
      return _prevHasNextLineReturnValue;
    }

    try
    {
      do
      {
        _line = _is.readLine();
        if (_line == null)
        {
          _prevHasNextLineReturnValue = false;
          _hasNextLineWasCalled = false;
          return false;
        }

        // get rid of leading and trailing spaces
        _line = _line.trim();
      }
      while (_line.equals(""));
    }
    catch (IOException ioe)
    {
      closeFileDuringExceptionHandling();

      DatastoreException de = new CannotReadDatastoreException(_fileName);
      de.initCause(ioe);
      throw de;
    }

    _hasNextLineWasCalled = true;
    Assert.expect(_line != null);
    _prevHasNextLineReturnValue = true;
    return true;
  }

  /**
   * @author Kok Chun, Tan
   */
  void closeFileDuringExceptionHandling()
  {
    if (_is != null)
    {
      try
      {
        _is.close();
        _is = null;
      }
      catch (IOException ioe)
      {
        // do nothing
      }
    }
  }

  /**
   * @author Kok Chun, Tan
   */
  int getLineNumber()
  {
    Assert.expect(_is != null);

    int lineNumber = _is.getLineNumber();
    if (_hasNextLineWasCalled)
    {
      --lineNumber;
    }

    return lineNumber;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  private boolean addVariation(Project project, String name, Map<String, List<String>> boardTypeToRefDesMap, boolean isNoLoadFileType) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(name != null);
    
    boolean isVariationAdded = false;
    VariationSetting variation = null;
    List<String> noLoadRefDesList = new ArrayList<String> ();
    List<ComponentTypeSettings> componentTypeSettingsList = new ArrayList<ComponentTypeSettings> ();
    Map<String, List<String>> noLoadBoardTypeToRefDesMap = new LinkedHashMap<String, java.util.List<String>>();
    
    _projectObservable.setEnabled(false);
    try
    {
      if (isNoLoadFileType)
      {
        isVariationAdded = true;
        variation = new VariationSetting(project, name, false, false, boardTypeToRefDesMap);
        project.getPanel().getPanelSettings().addVariationSetting(variation);
      }
      else
      {
        // Kok Chun, Tan - XCR-3263 - support test bom list.
        for (BoardType boardType : project.getPanel().getBoardTypes())
        {
          noLoadRefDesList.clear();
          List<String> refDesList = boardTypeToRefDesMap.get(boardType.getName());
          for (ComponentType componentType : boardType.getComponentTypes())
          {
            if (refDesList.contains(componentType.getReferenceDesignator()))
            {
              componentType.getComponentTypeSettings().setInspected(true);
              componentType.getComponentTypeSettings().setLoaded(true);
              if (componentTypeSettingsList.contains(componentType.getComponentTypeSettings()) == false)
                componentTypeSettingsList.add(componentType.getComponentTypeSettings());
            }
            else
            {              
              if (noLoadRefDesList.contains(componentType.getReferenceDesignator()) == false)
                noLoadRefDesList.add(componentType.getReferenceDesignator());
            }
          }
          if (noLoadRefDesList.isEmpty() == false)
            noLoadBoardTypeToRefDesMap.put(boardType.getName(), noLoadRefDesList);
        }
        if (noLoadBoardTypeToRefDesMap.isEmpty() == false)
        {
          isVariationAdded = true;
          variation = new VariationSetting(project, name, false, false, noLoadBoardTypeToRefDesMap);
          project.getPanel().getPanelSettings().addVariationSetting(variation);
        }
      }
    }
    finally
    {
      _projectObservable.setEnabled(true);
    }
    
    if (isVariationAdded)
      _projectObservable.stateChanged(variation, null, variation, VariationSettingEnum.ADD_OR_REMOVE);
    else if (isNoLoadFileType == false)
      _projectObservable.stateChanged(componentTypeSettingsList, null, null, ComponentTypeSettingsEventEnum.LOADED_LIST);

    return isVariationAdded;
  }
}
