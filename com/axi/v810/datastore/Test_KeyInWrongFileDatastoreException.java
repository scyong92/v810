package com.axi.v810.datastore;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class Test_KeyInWrongFileDatastoreException extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_KeyInWrongFileDatastoreException());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    KeyInWrongFileDatastoreException ex = new KeyInWrongFileDatastoreException("key", "expectedFileName", "actualFileName");
    String message = ex.getLocalizedMessage();
    os.println(message);
  }
}
