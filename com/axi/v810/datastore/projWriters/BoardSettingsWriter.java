package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.ndfReaders.*;

/**
 * @author Bill Darbie
 */
class BoardSettingsWriter
{
  private static BoardSettingsWriter _instance;

  private ProjectWriter _projectWriter;

  /**
   * @author Bill
   */
  static synchronized BoardSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new BoardSettingsWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private BoardSettingsWriter()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  void write(Project project, Map<Board, Integer> boardToBoardInstanceNumberMap) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(boardToBoardInstanceNumberMap != null);

    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();

    List<Board> boards = project.getPanel().getBoards();
    //Punit: Handling Assert for Invalid NDF
    //Refer to recombineSplitBoards() function in MainReader.java to further
    // solve this problem
    for (Board board : boards)
    {
      Integer boardInstanceNumber = boardToBoardInstanceNumberMap.get(board);
      //Assert.expect(boardInstanceNumber != null);      
      if(boardInstanceNumber == null)
        throw new InvalidNdfException();

      if(project.getPanel().hasBoardType(board.getBoardType().getName()) == true)
        writeBoardSettings(project, board, boardInstanceNumber);
    }
  }

  /**
   * @author Bill Darbie
   */
  private void writeBoardSettings(Project project, Board board, int boardInstanceNumber) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(board != null);

    String projName = project.getName();
    String fileName = FileName.getProjectBoardSettingsFullPath(projName, boardInstanceNumber, FileName.getProjectBoardSettingsLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);
      os.println("##############################################################################");
      os.println("# FILE: board_name.X.settings");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
       os.println("##############################################################################");
      BoardSettings boardSettings = board.getBoardSettings();

      boolean inspected = boardSettings.isInspected();
      String inspectedStr = "inspected";
      if (inspected == false)
        inspectedStr = "notInspected";

      int zOffset = boardSettings.getBoard().getBoardSettings().getBoardZOffsetInNanometers();
      String strZOffset = String.valueOf(zOffset);
      
      os.println("# boardName");
      _projectWriter.println(os, board.getName());
      os.println("# inspected");
      os.println(inspectedStr);
      os.println("# z-offset");
      os.println(strZOffset);
      
      os.println("# single|left|right alignmentGroupName numPadsAndFiducials useZheight zHeightInNanometer useCustomizeAlignmentSetting userGain integrationLevel stageSpeed dro");
      os.println("#   boardName refDes padName");
      os.println("#   boardName fiducialName");
      String singleOrLeftOrRight = null;
      for (int i = 0; i < 2; ++i)
      {
        List<AlignmentGroup> alignmentGroups = null;
        if (i == 0)
        {
          if (boardSettings.isLongBoard() || boardSettings.useLeftAlignmentGroup())
          {
            singleOrLeftOrRight = "left";
            alignmentGroups = boardSettings.getLeftAlignmentGroupsForLongPanel();
          }
          else
          {
            singleOrLeftOrRight = "right";
            alignmentGroups = boardSettings.getAlignmentGroupsForShortPanel();
          }
        }
        else
        if (i == 1)
        {
          if (boardSettings.isLongBoard())
          {
            singleOrLeftOrRight = "right";
            alignmentGroups = boardSettings.getRightAlignmentGroupsForLongPanel();
          }
          else
          {
            continue;
          }
        }

        for (AlignmentGroup alignmentGroup : alignmentGroups)
        {
          List<Pad> pads = alignmentGroup.getPads();
          List<Fiducial> fiducials = alignmentGroup.getFiducials();
          int numPadsAndFiducials = pads.size() + fiducials.size();

          os.println(singleOrLeftOrRight + " " + alignmentGroup.getName() + " " + numPadsAndFiducials + 
                     " " + alignmentGroup.useZHeight() + " " + alignmentGroup.getZHeightInNanometer() +
                     " " + alignmentGroup.useCustomizeAlignmentSetting() + " " + alignmentGroup.getUserGainEnum() +
                     " " + alignmentGroup.getSignalCompensationEnum()+ " " + alignmentGroup.getStageSpeedEnum()+
                     " " + alignmentGroup.getDynamicRangeOptimizationLevel());

          for (Pad pad : pads)
          {
            Component component = pad.getComponent();
            String boardName = component.getBoard().getName();
            String refDes = component.getReferenceDesignator();
            String padName = pad.getName();
            _projectWriter.println(os, "  " + boardName + " " + refDes + "  " + padName);
          }

          for (Fiducial fiducial : fiducials)
          {
            String fiducialName = fiducial.getName();
            if (fiducial.isOnBoard())
            {
              String boardName = fiducial.getSideBoard().getBoard().getName();
              _projectWriter.println(os, "  " + boardName + " " + fiducialName);
            }
            else if (fiducial.isOnPanel())
            {
              _projectWriter.println(os, "  " + fiducialName);
            }
            else
            {
              Assert.expect(false);
            }
          }
        }
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
