package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
class CompPackageWriter
{
  private static CompPackageWriter _instance;
  private EnumStringLookup _enumStringLookup;
  private ProjectWriter _projectWriter;
  // Log out any errors that the recipe contained
  private ProjectErrorLogUtil _projectErrorLogUtil;

  /**
   * @author Bill
   */
  static synchronized CompPackageWriter getInstance()
  {
    if (_instance == null)
      _instance = new CompPackageWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private CompPackageWriter()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * @author Bill
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();
    String projName = project.getName();
    String fileName = FileName.getProjectCompPackageFullPath(projName, FileName.getProjectCompPackageLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: package.X.cad");
      os.println("# PURPOSE: To describe each package on the panel.");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");
      os.println("# packageLongName packageShortName");
      os.println("#   landPatternPadName jointTypeName padOrientation customJointHeight");

      Panel panel = project.getPanel();
      List<CompPackage> compPackages = panel.getCompPackages();
      for (CompPackage compPackage : compPackages)
      {
        String compPackageLongName = compPackage.getLongName();
        String compPackageShortName = compPackage.getShortName();
        
        if (compPackage.hasLandPattern() == false)
        {
          _projectErrorLogUtil = ProjectErrorLogUtil.getInstance();
          try {
            _projectErrorLogUtil.log(project.getName(), "Recipe is saved and extra component package is removed: ", compPackageShortName);     
          } catch (XrayTesterException ex) {
            System.out.println("Could not write into recipe error log");
          }
          compPackage.destroy();
          continue;
        }
        
        _projectWriter.println(os, compPackageLongName + " " + compPackageShortName);

        List<PackagePin> packagePins = compPackage.getPackagePins();
        for (PackagePin packagePin : packagePins)
        {
          String orientationStr = "undefined";
          if (packagePin.hasPinOrientationEnumBeenOverridden())
          {
            PinOrientationEnum padOrientationEnum = packagePin.getPinOrientationEnum();
            orientationStr =  _enumStringLookup.getPadOrientationString(padOrientationEnum);
          }

          JointTypeEnum jointTypeEnum = packagePin.getJointTypeEnum();
          String padName = packagePin.getName();

          String jointTypeName = _enumStringLookup.getJointTypeString(jointTypeEnum);
          int customJointHeightInNanos = packagePin.getCustomJointHeightInNanoMeters();

          // padName jointTypeName padOrientation jointTypeName customJointHeightInNanos
          _projectWriter.println(os, "  " + padName + " " + jointTypeName + " " + orientationStr + " " + customJointHeightInNanos);
        }
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
