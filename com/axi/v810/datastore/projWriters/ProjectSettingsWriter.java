package com.axi.v810.datastore.projWriters;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;

/**
 * @author Bill Darbie
 */
class ProjectSettingsWriter
{
  private static ProjectSettingsWriter _instance;
  private EnumStringLookup _enumStringLookup;
  private ProjectWriter _projectWriter;

  /**
   * @author Bill
   */
  static synchronized ProjectSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new ProjectSettingsWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private ProjectSettingsWriter()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * @author Bill
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();
    String projName = project.getName();
    String fileName = FileName.getProjectProjectSettingsFullPath(projName, FileName.getProjectProjectSettingsLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: project.X.settings");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");
      os.println("# project major version");
      os.println(project.getTestProgramVersion());
      os.println("# project minor version");
      os.println(project.getMinorVersion());
      os.println("# test program generation version");
      os.println(project.getProgramGenerationVersion());
      os.println("# project cad xml checksum");
      os.println(project.getCadXmlChecksum());
      os.println("# project setting xml checksum");
      os.println(project.getSettingsXmlChecksum());
      os.println("# last modification time in milliseconds");
      os.println(project.getLastModificationTimeInMillis());
      os.println("# target customer name");
      _projectWriter.println(os, "targetCustomerName:" + project.getTargetCustomerName());
      os.println("# programmer name");
      _projectWriter.println(os, "programmerName:" + project.getProgrammerName());
      os.println("# project display units");
      MathUtilEnum mathUtilEnum = project.getDisplayUnits();
      String units = _enumStringLookup.getMathUtilString(mathUtilEnum);
      os.println(units);
      os.println("# initial thresholds set");
      os.println(project.getInitialThresholdsSetName());
      os.println("# initial Recipe Setting");
      os.println(project.getInitialRecipeSettingName());
      os.println("# softwareVersionUsedAtLastSave ");
      os.println(project.getSoftwareVersionOfProjectLastSave());

      os.println("# system type");
      os.println(XrayTester.getSystemType().getName());

      os.println("# scanpath method");
      os.println(project.getScanPathMethod().getId());
      
      // Add for new scan route generation, XCR1529, New Scan Route, khang-wah.chnee
      os.println("# generate with new scan route method");
      os.println(project.isGenerateByNewScanRoute());
      os.println("# scan route config file name");
      os.println(project.getScanRouteConfigFileName());
      os.println("# enable variable divn");
      os.println(project.isVarDivNEnable());
      os.println("# enable larger tomographic angle");
      os.println(project.isUseLargeTomoAngle());

      // Add for image generation settings, Bee Hoon
      os.println("# generate large view image");
      os.println(project.isEnableLargeImageView());
      os.println("# generate multi angle image");
      os.println(project.isGenerateMultiAngleImage());
      os.println("# generate component image");
      os.println(project.isGenerateComponentImage());
      
      // Add for panelExtraClearDelayInMiliSeconds
      os.println("# panel extra clear delay in miliseconds");
      os.println(project.getPanelExtraClearDelayInMiliSeconds());
      
      os.println("# enlarge reconstruction region");
      os.println(project.isEnlargeReconstructionRegion());
      
      // Add for low magnification used, Ngie Xing
      os.println("# system's low magnification on last project save");
      os.println(project.getProjectLowMagnification());

      // Add for high magnification used, Ngie Xing
      os.println("# system's high magnification on last project save");
      os.println(project.getProjectHighMagnification());
      
      // Add for BGA joint based threshold inspection, Swee Yee
      os.println("# enable joint based threshold inspection for BGA");
      os.println(project.isBGAJointBasedThresholdInspectionEnabled());
      
      // Add for 1x1 reconstruction region for BGA, Swee Yee
      os.println("# enable 1x1 reconstruction region for BGA");
      os.println(project.isBGAInspectionRegionSetTo1X1());
      
      //Set pre-select variation enable value - Kok Chun, Tan
      os.println("# set pre-select variation");
      _projectWriter.println(os, "preSelectVariation:" + project.enablePreSelectVariation());
      
	    // Set thickness table version for this project, Swee Yee
      os.println("# set thickness table version");
      os.println(project.getThicknessTableVersion());

      //Set bypass mode value - Kok Chun, Tan
      os.println("# set bypass mode");
      os.println(project.isProjectBasedBypassMode());
      
      //Khaw Chek Hau - XCR2943: Alignment Fail Due to Too Close To Edge
      //Add for enlarge alignment region
      os.println("# enlarge alignment region");
      os.println(project.isEnlargeAlignmentRegion());
      
      // Kok Chun, Tan - DRO version
      os.println("# dynamic range optimization version");
      os.println(project.getDynamicRangeOptimizationVersion().getVersion());
      
      String notes = project.getNotes();
      _projectWriter.writeMultipleLinesOfText(os, "notesBegin", notes, "notesEnd");
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
