package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Khaw Chek Hau
 * @XCR3554: Package on package (PoP) development
 */
class PackageOnPackageWriter
{
  private static PackageOnPackageWriter _instance;
  private ProjectWriter _projectWriter;
  private EnumStringLookup _enumStringLookup;

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  static synchronized PackageOnPackageWriter getInstance()
  {
    if (_instance == null)
      _instance = new PackageOnPackageWriter();

    return _instance;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  private PackageOnPackageWriter()
  {
    _enumStringLookup = EnumStringLookup.getInstance();       
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();

    writePackageOnPackage(project);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  private void writePackageOnPackage(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    String projName = project.getName();
    String fileName = FileName.getProjectPackageOnPackageFullPath(projName, FileName.getProjectPackageOnPackageLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);
      
      os.println("##############################################################################");
      os.println("# FILE: packageOnPackage.X.settings");
      os.println("# PURPOSE: To describe which components are POP and the details of its component..");
      os.println("#");
      os.println("# NOTE:");
      os.println("#  - Layer 0 represents base layer of the POP component.");
      os.println("#  - The POP ZHeight offset is in nanometers units.");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("##############################################################################");
      os.println("");
      os.println("# referenceDesignator packageOnPackageName LayerNumber Z-HeightOffset");

      List<ComponentType> componentTypes = project.getPanel().getComponentTypes();
      List<CompPackageOnPackage> compPackageOnPackages = project.getPanel().getCompPackageOnPackages();
      for (ComponentType componentType : componentTypes)
      {
        if (componentType.hasCompPackageOnPackage() && compPackageOnPackages.contains(componentType.getCompPackageOnPackage()))
        {
          String refDes = componentType.getReferenceDesignator();
          CompPackageOnPackage compPackageOnPackage = componentType.getCompPackageOnPackage();
          String compPackageOnPackageName = compPackageOnPackage.getPOPName();
          POPLayerIdEnum layerId = componentType.getPOPLayerId();
          String layerIdInString = _enumStringLookup.getLayerIdEnumString(layerId);
          int ZHeightPOPInNanometers = componentType.getPOPZHeightInNanometers();

          _projectWriter.println(os, refDes + " " + compPackageOnPackageName + " " + layerIdInString + " " + ZHeightPOPInNanometers);
        }
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
