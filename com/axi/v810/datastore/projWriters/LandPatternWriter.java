package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
class LandPatternWriter
{
  private static LandPatternWriter _instance;
  private ProjectWriter _projectWriter;

  /**
   * @author Bill
   */
  static synchronized LandPatternWriter getInstance()
  {
    if (_instance == null)
      _instance = new LandPatternWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private LandPatternWriter()
  {
    // do nothing
  }

  /**
   * @author Bill
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();
    String projName = project.getName();
    String fileName = FileName.getProjectLandPatternFullPath(projName, FileName.getProjectLandPatternLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: landPattern.X.cad");
      os.println("# PURPOSE: To describe all the land patterns on the panel");
      os.println("#");
      os.println("# NOTE:");
      os.println("#  - All numbers are in nanometers or degrees.");
      os.println("#  - the xLocation, yLocation numbers indicate the distance from the");
      os.println("#    land patterns reference point to the particular pad center being specified.");
      os.println("#    This reference point is usually the land pattern center, but does");
      os.println("#    not need to be.");
      os.println("#  - the rotation of the pad is with respect to the pad center and");
      os.println("#    can be 0-359.  The rotation is relative to the top of the pad,");
      os.println("#    so if you are looking at the top of the board with x-ray vision");
      os.println("#    a pad on the top will rotatate counter-clockwise,  a pad on");
      os.println("#    the bottom of the board would rotation clockwise when viewed from");
      os.println("#    the top side of the board.");
      os.println("#  - width and length specify the x and y dimensions at 0 degrees rotation");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");
      os.println("#landPatternName1 [userAssignedPadOneNameOrNumber]");
      os.println("#  padNameOrNumber1 xLocation yLocation width length rectangle|circle rotationInDegrees surfaceMount|throughHole [holeXLocation holeYLocation holeWidth holeLength]");
      os.println("#  padNameOrNumber2 xLocation yLocation width length rectangle|circle rotationInDegrees surfaceMount|throughHole [holeXLocation holeYLocation holeWidth holeLength]");

      List<LandPattern> landPatterns = project.getPanel().getAllLandPatterns();
      for (LandPattern landPattern : landPatterns)
      {
        // if there is a user assigned pad one, write it out on the same line and the landPatternName
        String landPatternName = landPattern.getName();
        if (landPattern.isPadOneUserAssigned())
        {
          LandPatternPad landPatternPad = landPattern.getUserAssignedPadOne();
          landPatternName = landPatternName + " " + landPatternPad.getName();
        }

        _projectWriter.println(os, landPatternName);

        List<LandPatternPad> landPatternPads = landPattern.getLandPatternPads();
        for (LandPatternPad landPatternPad : landPatternPads)
        {
          String padName = landPatternPad.getName();
          ComponentCoordinate coord = landPatternPad.getCoordinateInNanoMeters();
          int xInNanos = coord.getX();
          int yInNanos = coord.getY();
          int widthInNanos = landPatternPad.getWidthInNanoMeters();
          int lengthInNanos = landPatternPad.getLengthInNanoMeters();
          String shape = "rectangle";
          ShapeEnum shapeEnum = landPatternPad.getShapeEnum();
          if (shapeEnum.equals(ShapeEnum.CIRCLE))
            shape = "circle";
          String surfaceMountOrThroughHole = "surfaceMount";
          if (landPatternPad.isThroughHolePad())
            surfaceMountOrThroughHole = "throughHole";
          double degreesRotation = landPatternPad.getDegreesRotation();
          // padNameOrNumber2 xLocation yLocation width length rectangle|circle rotationInDegrees surfaceMount|throughHole holeXLocation holeYLocation holeDiameter");
          if (landPatternPad.isSurfaceMountPad())
            _projectWriter.println(os, "  " + padName + " " + xInNanos + " " + yInNanos + " " + widthInNanos + " " +
                       lengthInNanos + " " + shape + " " + degreesRotation + " " + surfaceMountOrThroughHole);
          else
          {
            ThroughHoleLandPatternPad thLandPattern = (ThroughHoleLandPatternPad)landPatternPad;
            ComponentCoordinate panelCoordinate = thLandPattern.getHoleCoordinateInNanoMeters();
            int xHoleInNanos = panelCoordinate.getX();
            int yHoleInNanos = panelCoordinate.getY();
//            int holeDiamInNanos = thLandPattern.getHoleDiameterInNanoMeters();
            //Siew Yeng - XCR-3318 - Oval PTH
            int holeWidthInNanos = thLandPattern.getHoleWidthInNanoMeters();
            int holeLengthInNanos = thLandPattern.getHoleLengthInNanoMeters();

            _projectWriter.println(os, "  " + padName + " " + xInNanos + " " + yInNanos + " " + widthInNanos + " " +
                       lengthInNanos + " " + shape + " " + degreesRotation + " " + surfaceMountOrThroughHole + " " +
                       xHoleInNanos + " " + yHoleInNanos + " " + holeWidthInNanos + " " + holeLengthInNanos);
          }
        }
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
