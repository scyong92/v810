package com.axi.v810.datastore.projWriters;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PanelSurfaceMapSettingsWriter 
{
  public static String _COMMA_DELIMITER = ","; 

  private static PanelSurfaceMapSettingsWriter _instance;

  private ProjectWriter _projectWriter;

  /**
   * @author Cheah Lee Herng
   */
  public static synchronized PanelSurfaceMapSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new PanelSurfaceMapSettingsWriter();

    return _instance;
  }
    
  /**
   * @author Cheah Lee Herng
   */
  private PanelSurfaceMapSettingsWriter()
  {
    // Do nothing
  }
    
  /**
   * @author Cheah Lee Herng
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();
    String projName = project.getName();
    String fileName = FileName.getProjectPanelSurfaceMapSettingsFullPath(projName, FileName.getProjectPanelSurfaceMapSettingsLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: panel_surfaceMap.X.settings");
      os.println("# PURPOSE: to list all surface map points used by this project");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################"); 
      os.println("# Region panelXCoordinateInNanometers panelYCoordinateInNanometers panelCoordinateWidthInNanometer panelCoordinateHeightInNanometer boardName");
      os.println("   # CameraRect cameraRectPanelXCoordinateInNanometers cameraRectPanelYCoordinateInNanometers cameraRectPanelWidthInNanometers cameraRectPanelHeightInNanometers true|false z-height");
      os.println("      # Point pointPanelXCoordinateInNanometers pointPanelYCoordinateInNanometers  yes|no");
      os.println("      # Component refDes");

      PanelSurfaceMapSettings panelSurfaceMapSettings = project.getPanel().getPanelSurfaceMapSettings();
      for (OpticalRegion opticalRegion: panelSurfaceMapSettings.getOpticalRegions())
      {
        PanelRectangle panelRectangle = opticalRegion.getRegion();
        
        String boardName = "";
        for(Board board : opticalRegion.getBoards())
        {
          boardName += board.getName() + _COMMA_DELIMITER;
        }
        int lastIndexOfComma = boardName.lastIndexOf(_COMMA_DELIMITER);
        if (lastIndexOfComma > 0 && lastIndexOfComma == (boardName.length() - 1))
          boardName = boardName.substring(0, boardName.length() - 1);

        _projectWriter.println(os, "Region " + panelRectangle.getMinX() + " " + panelRectangle.getMinY() + " " + panelRectangle.getWidth() + " " + panelRectangle.getHeight() + " " + boardName);

        // OpticalCameraRectangle information
        for(OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getAllOpticalCameraRectangles())
        {
          boolean inspectableBool = opticalCameraRectangle.getInspectableBool();
          double zHeightInNanometer = opticalCameraRectangle.getZHeightInNanometer();
          PanelRectangle opticalCameraRectanglePanelRectangle = opticalCameraRectangle.getRegion();
          _projectWriter.println(os, "   CameraRect " + opticalCameraRectanglePanelRectangle.getMinX() + " " + opticalCameraRectanglePanelRectangle.getMinY() + " " + 
                                      opticalCameraRectanglePanelRectangle.getWidth() + " " + opticalCameraRectanglePanelRectangle.getHeight() + " " + 
                                      inspectableBool + " " + zHeightInNanometer);

          for(PanelCoordinate panelCoordinate : opticalCameraRectangle.getPanelCoordinateList())
          {
            //String xCoordinateInNanometerString = String.valueOf(panelCoordinate.getX());
            //String yCoordinateInNanometerString = String.valueOf(panelCoordinate.getY());
            //boolean isVerified = opticalRegion.getPointPanelPositionVerifiedFlag(xCoordinateInNanometerString + "_" + yCoordinateInNanometerString);
            String verifiedFlagString = "yes"; // This flag is no longer used. Remove it next time.

            // Point PointStageXCoordinatesInNanometers PointStageYCoordinatesInNanometers yes|no
            _projectWriter.println(os, "      Point " + panelCoordinate.getX() + " " + panelCoordinate.getY() + " " + verifiedFlagString);
          }
        }
        for (Component component : opticalRegion.getComponents())
        {
          // Component refDes
          _projectWriter.println(os, "      Component " + component.getReferenceDesignator());
        }
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
      {
        os.close();
      }
    }
  }
}
