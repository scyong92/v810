package com.axi.v810.datastore.projWriters;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.FocusGroup;
import com.axi.v810.business.testProgram.FocusRegion;
import com.axi.v810.business.testProgram.ReconstructionRegion;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 * @author Kee Chin Seong
 * - This class is to read the Focus region in settings,
 *   - Will handle for board based / panel based.
 *   - suppose board based will be created according to board 
 */
public class PanelFocusRegionSettingsWriter 
{
    public static String _COMMA_DELIMITER = ","; 
            
    private static PanelFocusRegionSettingsWriter _instance;

    private ProjectWriter _projectWriter;
    
    /**
     * @author Kee Chin Seong
     */
    public static synchronized PanelFocusRegionSettingsWriter getInstance()
    {
        if (_instance == null)
          _instance = new PanelFocusRegionSettingsWriter();

        return _instance;
    }
    
    /**
     * @author Kee Chin Seong
     */
    private PanelFocusRegionSettingsWriter()
    {
        // Do nothing
    }
    
    /**
     * @author Kee Chin Seong
    */
    void write(Project project) throws DatastoreException
    {
        Assert.expect(project != null);
        if (_projectWriter == null)
          _projectWriter = ProjectWriter.getInstance();
        String projName = project.getName();
        String fileName = FileName.getProjectPanelFocusRegionSettingsFullPath(projName, FileName.getProjectPanelFocusRegionSettingsLatestFileVersion());
        
        PrintWriter os = null;
        try
        {
            os = new PrintWriter(fileName);
            
            os.println("##############################################################################");
            os.println("# FILE: panel_focusRegionSettings.X.settings");
            os.println("# PURPOSE: to list all focus region has modified by this project");
            os.println("#");
            os.println("# NOTE: Hand editing of this file is not supported");
            os.println("#       The format of this file can be changed at any time with new");
            os.println("#       software releases.");
            os.println("#       If you want to write a script that uses information from this file");
            os.println("#       it is highly recommended that you use the cad.id.xml and");
            os.println("#       settings.id.xml files to get your data instead.");
            os.println("##############################################################################"); 
            os.println("# Component BoardName");
            os.println("  # Region regionX regionY regionWidth regionHeight FocusRegionX FocusRegionY FocusRegionWidth FocusRegionHeight");
            

            for(Component comp : project.getComponentToFocusRegionMap().keySet())
            {
                os.println(comp.getReferenceDesignator() + " " + comp.getBoard().getName());
                
                Map<String, List<FocusRegion>> rRegionList = project.getComponentToFocusRegionMap().get(comp);
                
                Iterator it = rRegionList.entrySet().iterator();
                
                while(it.hasNext())
                {
                    Map.Entry pairs = (Map.Entry)it.next();
                    String regionKey = (String)pairs.getKey();
                    
                    String []key = regionKey.split("_");
                    
                    int regionX = Integer.parseInt(key[0]);
                    int regionY = Integer.parseInt(key[1]);
                    int regionWidth = Integer.parseInt(key[2]);
                    int regionHeight = Integer.parseInt(key[3]);
                    
                    List<FocusRegion> regionList = (List<FocusRegion>)pairs.getValue();
                    
                    for(FocusRegion fr : regionList)
                    {    
                       int focusRegionX = (int)fr.getRectangleRelativeToPanelInNanometers().getRectangle2D().getX();
                       int focusRegionY = (int)fr.getRectangleRelativeToPanelInNanometers().getRectangle2D().getY();
                       double focusRegionWidth = fr.getRectangleRelativeToPanelInNanometers().getRectangle2D().getWidth();
                       double focusRegionHeight = fr.getRectangleRelativeToPanelInNanometers().getRectangle2D().getHeight();

                       //ReconstructionRegion X, ReconstructionRegion Y, ReconstructionRegion Width, ReconstructionRegion Height, 
                       //Focus Region X, Focus Region Y, Focus Region Width, Focus Region Height.
                       _projectWriter.println(os, "Region " + regionX + " " + regionY + " " + regionWidth + " " + regionHeight + " " + "FocusRegion"
                                                   + " "  + focusRegionX + " " + focusRegionY + " " + focusRegionWidth + " " + focusRegionHeight);
                    }
                }
            }
        }
        catch (FileNotFoundException ex)
        {
          FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
          dex.initCause(ex);
          throw dex;
        }
        finally
        {
          if (os != null)
          {
            os.close();
          }
        }
    }
}

