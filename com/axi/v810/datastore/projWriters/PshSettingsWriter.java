package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;

/**
 *
 * @author siew-yeng.phang
 */
public class PshSettingsWriter 
{
  private static PshSettingsWriter _instance;

  private ProjectWriter _projectWriter;

  /**
   * @author Siew Yeng
   */
  public static synchronized PshSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new PshSettingsWriter();

    return _instance;
  }

  /**
   * @author Siew Yeng
   */
  private PshSettingsWriter()
  {
    // do nothing
  }

  /**
   * @author Siew Yeng
   */
  public void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();

    String projName = project.getName();
    Panel panel = project.getPanel();
    PshSettings pshSettings = panel.getPshSettings();
    PrintWriter os = null;

    String fileName = FileName.getProjectPshSettingsFullPath(projName, FileName.getProjectPshSettingsLatestFileVersion());
    try
    {
      os = new PrintWriter(fileName);
      os.println("##############################################################################");
      os.println("# FILE: surfaceModel.X.settings");
      os.println("##############################################################################");
      os.println("# selectiveBoardSideEnabled");
      _projectWriter.println(os, "" + pshSettings.isEnableSelectiveBoardSide());
      
      os.println("# Both|Top|Bottom|SameSideWithComponent");
      _projectWriter.println(os, pshSettings.getPshSettingsBoardSideEnum().toString());
      
      os.println("# selectiveJointTypeEnabled");
      _projectWriter.println(os, "" + pshSettings.isEnableSelectiveJointType());
      
      os.println("# disabledJointType");
      for(JointTypeEnum jointType : pshSettings.getDisabledJointType())
      {
        _projectWriter.println(os, jointType.getName());
      }
      
      os.println("# selectiveComponentEnabled");
      _projectWriter.println(os, "" + pshSettings.isEnabledSelectiveComponent());
      
      os.println("# boardName refDes");
      os.println("#   neighbourRefDes");
      for(Map.Entry<ComponentType, List<ComponentType>> mapEntry : pshSettings.getPshComponentTypeToNeighbourComponentTypesMap().entrySet())
      {
        ComponentType pshComp = mapEntry.getKey();
        String boardName = pshComp.getComponents().get(0).getBoard().getName();
        
        _projectWriter.println(os, boardName + " " + pshComp.getReferenceDesignator());
        
        for(ComponentType neighbour : mapEntry.getValue())
        {
          _projectWriter.println(os, "  "+ neighbour.getReferenceDesignator());
        }
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
