package com.axi.v810.datastore.projWriters;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.business.testResults.*;

/**
 * @author Bill Darbie
 */
public class EnumStringLookup
{
  private static EnumStringLookup _instance;

  private Map<PinOrientationEnum, String> _pinOrientationEnumToStringMap = new HashMap<PinOrientationEnum, String>();
  private Map<String, PinOrientationEnum> _stringToPadOrientationEnumMap = new HashMap<String, PinOrientationEnum>();

  private Map<MathUtilEnum, String> _mathUtilEnumToStringMap = new HashMap<MathUtilEnum, String>();
  private Map<String, MathUtilEnum> _stringToMathUtilEnumMap = new HashMap<String, MathUtilEnum>();

  private Map<InspectionFamilyEnum, String> _inspectionFamilyEnumToStringMap = new HashMap<InspectionFamilyEnum, String>();
  private Map<String, InspectionFamilyEnum> _stringToInspectionFamilyEnumMap = new HashMap<String, InspectionFamilyEnum>();

  private Map<JointTypeEnum, String> _jointTypeEnumToStringMap = new HashMap<JointTypeEnum, String>();
  private Map<String, JointTypeEnum> _stringToJointTypeEnumMap = new HashMap<String, JointTypeEnum>();

  private Map<AlgorithmEnum, String> _algorithmEnumToStringMap = new HashMap<AlgorithmEnum, String>();
  private Map<String, AlgorithmEnum> _stringToAlgorithmEnumMap = new HashMap<String, AlgorithmEnum>();

  private Map<AlgorithmSettingEnum, String> _algorithmSettingEnumToStringMap = new HashMap<AlgorithmSettingEnum, String>();
  private Map<String, AlgorithmSettingEnum> _stringToAlgorithmSettingEnumMap = new HashMap<String, AlgorithmSettingEnum>();

  private Set<String> _oldAlgorithmSettingEnumStrings = new HashSet<String>();

  private Map<AlgorithmSettingTypeEnum, String> _algorithmSettingTypeEnumToStringMap = new HashMap<AlgorithmSettingTypeEnum,String>();
  private Map<String, AlgorithmSettingTypeEnum> _stringToAlgorithmSettingTypeEnumMap = new HashMap<String,AlgorithmSettingTypeEnum>();

  private Map<ConditionKeyEnum, String> _conditionKeyEnumToStringMap = new HashMap<ConditionKeyEnum, String>();
  private Map<String, ConditionKeyEnum> _stringToConditionKeyEnumMap = new HashMap<String, ConditionKeyEnum>();

  private Map<ResultFileTypeEnum, String> _resultFileTypeEnumToStringMap = new HashMap<ResultFileTypeEnum, String>();
  private Map<String, ResultFileTypeEnum> _stringToResultFileTypeEnumMap = new HashMap<String, ResultFileTypeEnum>();

  private Map<MeasurementUnitsEnum, String> _measurementUnitsEnumToStringMap = new HashMap<MeasurementUnitsEnum, String>();
  private Map<String, MeasurementUnitsEnum> _stringToMeasurementUnitsEnumMap = new HashMap<String, MeasurementUnitsEnum>();

  private Map<SignalCompensationEnum, String> _signalCompensationEnumToStringMap = new HashMap<SignalCompensationEnum, String>();
  private Map<String, SignalCompensationEnum> _stringToSignalCompensationEnumMap = new HashMap<String, SignalCompensationEnum>();

  private Map<GlobalSurfaceModelEnum, String> _globalSurfaceModelEnumToStringMap = new HashMap<GlobalSurfaceModelEnum, String>();
  private Map<String, GlobalSurfaceModelEnum> _stringToGlobalSurfaceModelEnumMap = new HashMap<String, GlobalSurfaceModelEnum>();

  private Map<UserGainEnum, String> _userGainEnumToStringMap = new HashMap<UserGainEnum, String>();
  private Map<String, UserGainEnum> _userGainEnumMap = new HashMap<String, UserGainEnum>();

  private Map<StageSpeedEnum, String> _stageSpeedEnumToStringMap = new HashMap<StageSpeedEnum, String>();
  private Map<String, StageSpeedEnum> _stageSpeedEnumMap = new HashMap<String, StageSpeedEnum>();
  
  private Map<PspEnum, String> _pspEnumToStringMap = new HashMap<PspEnum, String>();
  private Map<String, PspEnum> _pspEnumMap = new HashMap<String, PspEnum>();

  private Map<MagnificationTypeEnum, String> _magnificationTypeEnumToStringMap = new HashMap<MagnificationTypeEnum, String>();
  private Map<String, MagnificationTypeEnum> _magnificationTypeEnumMap = new HashMap<String, MagnificationTypeEnum>();
  
  //Siew Yeng - XCR_2140 - DRO
  private Map<DynamicRangeOptimizationLevelEnum, String> _dynamicRangeOptimizationLevelEnumToStringMap = new HashMap<DynamicRangeOptimizationLevelEnum, String>();
  private Map<String, DynamicRangeOptimizationLevelEnum> _dynamicRangeOptimizationLevelEnumMap = new HashMap<String, DynamicRangeOptimizationLevelEnum>();
  
  //Khaw Chek Hau - XCR3554: Package on package (PoP) development
  private Map<POPLayerIdEnum, String> _popLayerIdEnumToStringMap = new HashMap<POPLayerIdEnum, String>();
  private Map<String, POPLayerIdEnum> _popLayerIdEnumMap = new HashMap<String, POPLayerIdEnum>();
  
  /**
   * @author Bill Darbie
   */
  public static synchronized EnumStringLookup getInstance()
  {
    if (_instance == null)
      _instance = new EnumStringLookup();

    return _instance;
  }

  /**
   * @author Bill Darbie
   * @author Matt Wharton
   * @author George A. David
   */
  private EnumStringLookup()
  {
    // WARNING - DO NOT CHANGE THESE STRINGS - CHANGING THEM WILL CAUSE ALL PROJECTS
    //           ON CUSTOMER SITES TO BE NON-LOADABLE!!
    // WARNING - IF YOU REMOVE ONE OF THESE ENUMS THE PROJECT READERS MUST BE
    //           MODIFIED TO SUPPORT READING OF PRE-EXISTING PROJECTS! - see Bill Darbie for details
    mapPinOrientationEnumToString(PinOrientationEnum.NOT_APPLICABLE, "doesNotApply");
    mapPinOrientationEnumToString(PinOrientationEnum.COMPONENT_IS_EAST_OF_PIN, "east");
    mapPinOrientationEnumToString(PinOrientationEnum.COMPONENT_IS_NORTH_OF_PIN, "north");
    mapPinOrientationEnumToString(PinOrientationEnum.COMPONENT_IS_SOUTH_OF_PIN, "south");
    mapPinOrientationEnumToString(PinOrientationEnum.COMPONENT_IS_WEST_OF_PIN, "west");
    // Ensure that there is a map entry for each PinOrientationEnum.
    int numberOfPinOrientationEnums = com.axi.util.Enum.getNumEnums(PinOrientationEnum.class);
    Assert.expect(_pinOrientationEnumToStringMap.size() == numberOfPinOrientationEnums, "PinOrientationEnum missing from EnumStringLookup");

    // WARNING - DO NOT CHANGE THESE STRINGS - CHANGING THEM WILL CAUSE ALL PROJECTS
    //           ON CUSTOMER SITES TO BE NON-LOADABLE!!
    // WARNING - IF YOU REMOVE ONE OF THESE ENUMS THE PROJECT READERS MUST BE
    //           MODIFIED TO SUPPORT READING OF PRE-EXISTING PROJECTS! - see Bill Darbie for details
    mapMathUtilEnumToString(MathUtilEnum.INCHES, "inches");
    mapMathUtilEnumToString(MathUtilEnum.METERS, "meters");
    mapMathUtilEnumToString(MathUtilEnum.MICRONS, "microns");
    mapMathUtilEnumToString(MathUtilEnum.MILLIMETERS, "millimeters");
    mapMathUtilEnumToString(MathUtilEnum.MILS, "mils");
    mapMathUtilEnumToString(MathUtilEnum.NANOMETERS, "nanometers");
    // Ensure that there is a map entry for each MathUtilEnum.
    int numberOfMathUtilEnums = com.axi.util.Enum.getNumEnums(MathUtilEnum.class);
    Assert.expect(_mathUtilEnumToStringMap.size() == numberOfMathUtilEnums, "MathUtilEnum missing from EnumStringLookup");

    // WARNING - DO NOT CHANGE THESE STRINGS - CHANGING THEM WILL CAUSE ALL PROJECTS
    //           ON CUSTOMER SITES TO BE NON-LOADABLE!!
    // WARNING - IF YOU REMOVE ONE OF THESE ENUMS THE PROJECT READERS MUST BE
    //           MODIFIED TO SUPPORT READING OF PRE-EXISTING PROJECTS! - see Bill Darbie for details
    mapInspectionFamilyEnumToString(InspectionFamilyEnum.CHIP, "chip");
    mapInspectionFamilyEnumToString(InspectionFamilyEnum.GRID_ARRAY, "gridArray");
    mapInspectionFamilyEnumToString(InspectionFamilyEnum.GULLWING, "gullwing");
    mapInspectionFamilyEnumToString(InspectionFamilyEnum.ADVANCED_GULLWING, "advancedGullwing");
    mapInspectionFamilyEnumToString(InspectionFamilyEnum.LAND_GRID_ARRAY, "landGridArray");
    mapInspectionFamilyEnumToString(InspectionFamilyEnum.THROUGHHOLE, "throughHole");
    mapInspectionFamilyEnumToString(InspectionFamilyEnum.LARGE_PAD, "largePad");
    mapInspectionFamilyEnumToString(InspectionFamilyEnum.CALIBRATION, "calibration");  // this is not used, but left in for backwards-compatibility
    mapInspectionFamilyEnumToString(InspectionFamilyEnum.POLARIZED_CAP, "polarizedCap");
    mapInspectionFamilyEnumToString(InspectionFamilyEnum.PRESSFIT, "pressfit");
    mapInspectionFamilyEnumToString(InspectionFamilyEnum.QUAD_FLAT_NO_LEAD, "quadFlatNoLead");
    mapInspectionFamilyEnumToString(InspectionFamilyEnum.EXPOSED_PAD, "exposedPad");
    mapInspectionFamilyEnumToString(InspectionFamilyEnum.OVAL_THROUGHHOLE, "ovalThroughHole");//Siew Yeng - XCR-3318 - Oval PTH

    // Ensure that there is a map entry for each InspectionFamilyEnum.
    int numberOfInspectionFamilyEnums = com.axi.util.Enum.getNumEnums(InspectionFamilyEnum.class);
    Assert.expect(_inspectionFamilyEnumToStringMap.size() == numberOfInspectionFamilyEnums, "InspectionFamilyEnum missing from EnumStringLookup");

    // WARNING - DO NOT CHANGE THESE STRINGS - CHANGING THEM WILL CAUSE ALL PROJECTS
    //           ON CUSTOMER SITES TO BE NON-LOADABLE!!
    // WARNING - IF YOU REMOVE ONE OF THESE ENUMS THE PROJECT READERS MUST BE
    //           MODIFIED TO SUPPORT READING OF PRE-EXISTING PROJECTS! - see Bill Darbie for details
    mapJointTypeEnumToString(JointTypeEnum.COLLAPSABLE_BGA, "collapsableBGA");
    mapJointTypeEnumToString(JointTypeEnum.NON_COLLAPSABLE_BGA, "nonCollapsableBGA");
    mapJointTypeEnumToString(JointTypeEnum.CGA, "cga");
    mapJointTypeEnumToString(JointTypeEnum.CAPACITOR, "capacitor");
    mapJointTypeEnumToString(JointTypeEnum.SURFACE_MOUNT_CONNECTOR, "surfaceMountConnector");
    mapJointTypeEnumToString(JointTypeEnum.GULLWING, "gullwing");
    mapJointTypeEnumToString(JointTypeEnum.JLEAD, "jlead");
    mapJointTypeEnumToString(JointTypeEnum.SINGLE_PAD, "singlePad");
    mapJointTypeEnumToString(JointTypeEnum.POLARIZED_CAP, "polarizedCap");
    mapJointTypeEnumToString(JointTypeEnum.PRESSFIT, "pressfit");
    mapJointTypeEnumToString(JointTypeEnum.THROUGH_HOLE, "throughHole");
    mapJointTypeEnumToString(JointTypeEnum.OVAL_THROUGH_HOLE, "ovalThroughHole"); //Siew Yeng - XCR-3318 - Oval PTH
    mapJointTypeEnumToString(JointTypeEnum.RESISTOR, "resistor");
    mapJointTypeEnumToString(JointTypeEnum.RF_SHIELD, "rfShield");
    mapJointTypeEnumToString(JointTypeEnum.LEADLESS, "leadLess");
    mapJointTypeEnumToString(JointTypeEnum.SMALL_OUTLINE_LEADLESS, "smallOutlineLeadless");
    mapJointTypeEnumToString(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD, "quadFlatPackNoLead");
    mapJointTypeEnumToString(JointTypeEnum.CHIP_SCALE_PACKAGE, "chipScalePackage");
    mapJointTypeEnumToString(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR, "variableHeightBGAConnector");
    mapJointTypeEnumToString(JointTypeEnum.EXPOSED_PAD, "exposedPad");
    mapJointTypeEnumToString(JointTypeEnum.LGA, "lga");
    // Wei Chin (Tall Cap)
//    mapJointTypeEnumToString(JointTypeEnum.TALL_CAPACITOR, "tallCapacitor");

    //    mapJointTypeEnumToString(JointTypeEnum.CALIBRATION_PAD, "calibrationPad");
    // Ensure that there is a map entry for each JointTypeEnum.
    int numberOfJointTypeEnums = com.axi.util.Enum.getNumEnums(JointTypeEnum.class);
    Assert.expect(_jointTypeEnumToStringMap.size() == numberOfJointTypeEnums, "JointTypeEnum missing from EnumStringLookup");

    // WARNING - DO NOT CHANGE THESE STRINGS - CHANGING THEM WILL CAUSE ALL PROJECTS
    //           ON CUSTOMER SITES TO BE NON-LOADABLE!!
    // WARNING - IF YOU REMOVE ONE OF THESE ENUMS THE PROJECT READERS MUST BE
    //           MODIFIED TO SUPPORT READING OF PRE-EXISTING PROJECTS! - see Bill Darbie for details
    mapAlgorithmEnumToString(AlgorithmEnum.MEASUREMENT, "measurement");
    mapAlgorithmEnumToString(AlgorithmEnum.OPEN, "open");
    mapAlgorithmEnumToString(AlgorithmEnum.SHORT, "short");
    mapAlgorithmEnumToString(AlgorithmEnum.INSUFFICIENT, "insufficient");
    mapAlgorithmEnumToString(AlgorithmEnum.MISALIGNMENT, "misalignment");
    mapAlgorithmEnumToString(AlgorithmEnum.VOIDING, "voiding");
    mapAlgorithmEnumToString(AlgorithmEnum.EXCESS, "excess");

    // Ensure that there is a map entry for each AlgorithmEnum.
    int numberOfAlgorithmEnums = com.axi.util.Enum.getNumEnums(AlgorithmEnum.class);
    Assert.expect(_algorithmEnumToStringMap.size() == numberOfAlgorithmEnums, "AlgorithmEnum missing from EnumStringLookup");

    // WARNING - DO NOT CHANGE THESE STRINGS - CHANGING THEM WILL CAUSE ALL PROJECTS
    //           ON CUSTOMER SITES TO BE NON-LOADABLE!!
    // WARNING - IF YOU REMOVE ONE OF THESE ENUMS THE PROJECT READERS MUST BE
    //           MODIFIED TO SUPPORT READING OF PRE-EXISTING PROJECTS! - see Bill Darbie for details
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_ENABLE_JOINT_BASED_EDGE_DETECTION, "gridArrayEnableJointBasedEdgeDetection");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_PAD, "gridArrayEdgeDetectionPercentPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_LOWERPAD, "gridArrayEdgeDetectionPercentLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_MIDBALL, "gridArrayEdgeDetectionPercentMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_PACKAGE, "gridArrayEdgeDetectionPercentPackage");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_PROFILE_SEARCH_DISTANCE, "gridArrayProfileSearchDistance");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_TECHNIQUE, "gridArrayEdgeDetectionTechnique");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD, "gridArrayNominalDiameterPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_LOWERPAD, "gridArrayNominalDiameterLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD, "gridArrayNominalThicknessPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_LOWERPAD, "gridArrayNominalThicknessLowerPad");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL, "gridArrayNominalDiameterMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL, "gridArrayNominalThicknessMidball");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE, "gridArrayNominalDiameterPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE, "gridArrayNominalThicknessPadPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_1, "gridArrayNominalThicknessUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_2, "gridArrayNominalThicknessUserDefinedSlice2");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_BACKGROUND_REGION_INNER_EDGE_LOCATION, "gridArrayBackgroundRegionInnerEdgeLocation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_BACKGROUND_REGION_OUTER_EDGE_LOCATION, "gridArrayBackgroundRegionOuterEdgeLocation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_FIXED_ECCENTRICITY_AXIS, "gridArrayFixedEccentricityAxis");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_USE_FIXED_ECCENTRICITY_AXIS, "gridArrayUseFixedEccentricityAxis");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_CHECK_ALL_SLICES_FOR_SHORT , "gridArrayCheckAllSlicesForShort"); //lam
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD, "gridArrayOpenMinimumDiameterPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_LOWERPAD, "gridArrayOpenMinimumDiameterLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_MIDBALL, "gridArrayOpenMinimumDiameterMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PACKAGE, "gridArrayOpenMinimumDiameterPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_1, "gridArrayOpenMinimumDiameterUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_2, "gridArrayOpenMinimumDiameterUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_PAD, "gridArrayOpenMaximumDiameterPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_LOWERPAD, "gridArrayOpenMaximumDiameterLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_MIDBALL, "gridArrayOpenMaximumDiameterMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_PACKAGE, "gridArrayOpenMaxumumDiameterPackage");    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_1, "gridArrayOpenMaxumumDiameterUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_2, "gridArrayOpenMaxumumDiameterUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_PAD, "gridArrayOpenMaximumNeighborOutlierPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_LOWERPAD, "gridArrayOpenMaximumNeighborOutlierLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_MIDBALL, "gridArrayOpenMaximumNeighborOutlierMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_PACKAGE, "gridArrayOpenMaximumNeighborOutlierPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_PAD, "gridArrayOpenMinimumNeighborOutlierPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_LOWERPAD, "gridArrayOpenMinimumNeighborOutlierLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_MIDBALL, "gridArrayOpenMinimumNeighborOutlierMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_PACKAGE, "gridArrayOpenMinimumNeighborOutlierPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_THICKNESS, "gridArrayOpenMinimumThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_PAD,
                                    "gridArrayOpenMaximumRegionOutlierPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_LOWERPAD,
                                    "gridArrayOpenMaximumRegionOutlierLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_MIDBALL,
                                    "gridArrayOpenMaximumRegionOutlierMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_PACKAGE,
                                    "gridArrayOpenMaximumRegionOutlierPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1,
                                    "gridArrayOpenMaximumRegionOutlierUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2,
                                    "gridArrayOpenMaximumRegionOutlierUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PAD,
                                    "gridArrayOpenMinumumRegionOutlierPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_LOWERPAD,
                                    "gridArrayOpenMinumumRegionOutlierLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_MIDBALL,
                                    "gridArrayOpenMinumumRegionOutlierMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PACKAGE,
                                    "gridArrayOpenMinumumRegionOutlierPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1,
                                    "gridArrayOpenMinumumRegionOutlierUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2,
                                    "gridArrayOpenMinumumRegionOutlierUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_PERCENT_JOINTS_WITH_INSUFF_DIAMETER,
                                    "gridArrayOpenPercentJointsWithInsuffDiameter");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMIM_DIAMETER_MARGINAL,
                                    "gridArrayOpenMinimumDiameterMarginal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_PAD, "gridArrayOpenMiniumFlatteningPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_PAD, "gridArrayOpenMaxiumFlatteningPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_LOWERPAD, "gridArrayOpenMiniumFlatteningLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_LOWERPAD, "gridArrayOpenMaxiumFlatteningLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_MIDBALL, "gridArrayOpenMiniumFlatteningMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_MIDBALL, "gridArrayOpenMaximumEccentricityMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1, "gridArrayOpenMiniumFlatteningUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1, "gridArrayOpenMaximumEccentricityUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2, "gridArrayOpenMiniumFlatteningUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2, "gridArrayOpenMaximumEccentricityUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_VOID_AREA_LIMIT_FOR_NEIGHBOR_OUTLIER, "gridArrayOpenVoidAreaLimitForNeighborOutlier");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_PAD, "gridArrayOpenMaxiumHipOutlierPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_PAD, "gridArrayOpenMiniumHipOutlierPad");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_EXPECTED, "gridArrayMisalignmentOffsetFromRegionJoints");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_CAD_POSITION, "gridArrayMisalignmentOffsetFromCadPosition");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PAD, "gridArrayInsufficientMinimumPercentageOfNominalThicknessPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PACKAGE, "gridArrayInsufficientMinimumPercentageOfNominalThicknessPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_MIDBALL, "gridArrayInsufficientMinimumPercentageOfNominalThicknessMidball");

     //Chin Seong, Kee - Request to add Excess to the Gullwing family of algorithms
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_EXCESS_MAX_FILLET_THICKNESS_PERCENT_OF_NOMINAL, "gullwingExcessMaxFilletThicknessPercentOfNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_EXCESS_MAX_HEEL_THICKNESS_PERCENT_OF_NOMINAL, "gullwingExcessMaxHeelThicknessPercentOfNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_EXCESS_MAX_TOE_THICKNESS_PERCENT_OF_NOMINAL, "gullwingExcessMaxToeThicknessPercentOfNominal");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_VOID_COMPENSATION_METHOD, "gullwingVoidCompensationMethod");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_VOID_COMPENSATION, "gullwingHeelVoidCompensation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_SMOOTH_SENSITIVITY, "gullwingSmoothSensitivity");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PAD_PROFILE_WIDTH, "gullwingPadProfileWidth");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PAD_PROFILE_LENGTH, "gullwingPadProfileLength");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION, "gullwingBackgroundRegionLocation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET, "gullwingBackgroundRegionLocationOffset");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT, "gullwingHeelEdgeSearchThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE, "gullwingHeelSearchDistanceFromEdge");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_CENTER_OFFSET, "gullwingCenterOffsetFromHeel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PIN_LENGTH, "gullwingPinLength");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT, "gullwingToeEdgeSearchThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH, "gullwingNominalFilletLength");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS, "gullwingNominalFilletThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS, "gullwingNominalHeelThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_TOE_THICKNESS, "gullwingNominalToeThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_POSITION, "gullwingNominalHeelPosition");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL, "gullwingOpenMaxHeelThicknessPercentOfNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, "gullwingOpenMinFilletLengthPercentOfNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, "gullwingOpenMaxFilletLengthPercentOfNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT, "gullwingOpenMaxCenterToHeelThicknessRatio");
    //Siew Yeng - XCR-3532
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER, "gullwingOpenMaxFilletLengthRegionOutlier");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER, "gullwingOpenMinFilletLengthRegionOutlier");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER, "gullwingOpenMaxCenterThicknessRegionOutlier");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER, "gullwingOpenMinCenterThicknessRegionOutlier");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_INSUFFICIENT_MIN_THICKNESS_PERCENT_OF_NOMINAL, "gullwingInsufficientMinFilletThicknessPercentOfNominal");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL, "gullwingMisalignmentMaxHeelPositionDistanceFromNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_JOINT_OFFSET_FROM_CAD, "gullwingMisalignmentMaxJointOffsetFromCad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_OFFSET_FROM_REGION_NEIGHBORS, "gullwingMisalignmentHeelOffsetFromRegionNeighbors");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ACROSS_PROFILE_LOCATION, "advancedGullwingAcrossProfileLocation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ACROSS_PROFILE_WIDTH, "advancedGullwingAcrossProfileWidth");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_CENTER_OFFSET_FROM_EDGE, "advancedGullwingSideFilletCenterOffsetFromEdge");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_EDGE_SEARCH_THICKNESS_PERCENT, "advancedGullwingSideFilletEdgeThicknessSearchThicknessPercent");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_HEEL_SEARCH_DISTANCE_FROM_EDGE, "advancedGullwingSideFilletSearchDistanceFromEdge");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ENABLE_BACKGROUND_REGIONS, "advancedGullwingEnableBackgroundRegions");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_BACKGROUND_REGION_WIDTH, "advancedGullwingBackgroundRegionWidth");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_PAD_CENTER_REFERENCE, "advancedGullwingPadCenterReference");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_HEEL_TO_PAD_CENTER, "advancedGullwingNominalHeelToPadCenter");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MISALIGNMENT_HEEL_TO_PAD_CENTER_DIFFERENCE_FROM_NOMINAL, "advancedGullwingMisalignmentHeelToPadCenterDifferenceFromNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_MISALIGNMENT_MAXIMUM_MASS_SHIFT_ACROSS_PERCENT, "advancedGullwingMisalignmentMaximumMassShiftAcrossPercent");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS, "advancedGullwingOpenMaxCenterToHeelThicknessRatioAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_2_THICKNESS_PERCENT_ACROSS, "advancedGullwingOpenMaxCenterToHeel2ThicknessRatioAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_CONCAVITY_RATIO_ACROSS, "advancedGullwingOpenMinimumConcavityRatioAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_SLOPE, "advancedGullwingOpenMinimumHeelSlope");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM, "advancedGullwingOpenMinimumHeelAndToeSlopeSum");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_SUM_OF_SLOPE_CHANGES, "advancedGullwingOpenMinimumSumOfSlopeChanges");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_TOE_SLOPE, "advancedGullwingOpenMinimumToeSlope");
    //Lim, Lay Ngor - XCR1648:Add Maximum Slope Limit - START
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_SLOPE, "advancedGullwingOpenMaximumHeelSlope");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM, "advancedGullwingOpenMaximumHeelAndToeSlopeSum");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_SUM_OF_SLOPE_CHANGES, "advancedGullwingOpenMaximumSumOfSlopeChanges");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_TOE_SLOPE, "advancedGullwingOpenMaximumToeSlope");
    //Lim Lay Ngor - XCR1648:Add Maximum Slope Limit - END
    //Siew Yeng - XCR-3532
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER, "advancedGullwingOpenMaxFilletLengthRegionOutlier");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER, "advancedGullwingOpenMinFilletLengthRegionOutlier");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER, "advancedGullwingOpenMaxCenterThicknessRegionOutlier");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER, "advancedGullwingOpenMinCenterThicknessRegionOutlier");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.TEMP_SETTING, "temp");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_EFFECTIVE_WIDTH, "LocateEffectiveWidth");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_EFFECTIVE_LENGTH, "LocateEffectiveLength");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_EFFECTIVE_SHIFT_ALONG, "LocateEffectiveShiftAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_EFFECTIVE_SHIFT_ACROSS, "LocateEffectiveShiftAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ALONG, "LocateSearchWindowAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ACROSS, "LocateSearchWindowAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_BORDER_WIDTH, "LocateBorderWidth");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_SNAPBACK_DISTANCE, "LocateSnapbackDistance");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LOCATE_METHOD, "LocateMethod");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_INSPECTION_SLICE, "sharedShortInspectionSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_DETECTION, "sharedShortHighShortDetection");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_PROTRUSION_HIGH_SHORT_MINIMUM_SHORT_THICKNESS, "sharedShortProtrusionMinimumShortThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_PROJECTION_SHORT_MINIMUM_SHORT_THICKNESS, "sharedShortProjectionMinimumShortThickness");//Broken Pin
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_PROTRUSION_HIGH_SHORT_DETECTION, "sharedShortProtrusionHighShortDetection");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_SLICEHEIGHT, "sharedShortHighShortSliceheight");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_MINIMUM_SHORT_THICKNESS, "sharedShortHighShortMinimumThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_THICKNESS, "sharedShortMinimumShortThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_COMPONENT_SIDE_SLICE_MINIMUM_SHORT_THICKNESS, "sharedShortComponentSideSliceMinimumShortThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_PIN_SIDE_SLICE_MINIMUM_SHORT_THICKNESS, "sharedShortPinSideSliceMinimumShortThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION, "sharedShortRegionInnerEdgeLocation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION, "sharedShortRegionOuterEdgeLocation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_REGION_REFERENCE_POSITION, "sharedShortRegionReferencePosition");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_HEAD, "sharedShortEnableTestRegionHead");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_TAIL, "sharedShortEnableTestRegionTail");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_RIGHT, "sharedShortEnableTestRegionRight");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_LEFT, "sharedShortEnableTestRegionLeft");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_MINIMUM_PIXELS_SHORT_ROI_REGION, "sharedShortMinimumPixelsShortRoiRegion");
    //Siew Yeng - XCR-3318 - Oval PTH
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_4_CORNERS, "sharedShortIgnoreTestRegionCorner");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_INNER_CORNER, "sharedShortEnableTestRegionInnerCorner");

    //Lim, Lay Ngor - XCR1743:Benchmark
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_CENTER, "sharedShortEnableTestRegionCenter"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_MULTI_ROI_CENTER_REGION, "sharedShortEnableMultiROICenterRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_MINIMUM_CENTER_SOLDER_BALL_THICKNESS, "sharedShortMinimumCenterSolderBallThickness");    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_CENTER_EFFECTIVE_LENGTH, "sharedShortCenterEffectiveLength");  
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_CENTER_EFFECTIVE_WIDTH, "sharedShortCenterEffectiveWidth");
    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_LENGTH, "sharedShortMinimumShortLength");
    //Lim, Lay Ngor - XCR2100 - Broken Pin
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_BROKEN_PIN, "sharedShortEnableBrokenPin");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_BROKEN_PIN_AREA_SIZE, "sharedShortBrokenPinAreaSize");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_SHORT_BROKEN_PIN_THRESHOLD_RATIO, "sharedShortBrokenPinThresholdRatio");

    // mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICE_OBSOLETE, "throughholeBarrelSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT, "throughholeBarrelSliceHeight");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PROTRUSION_SLICEHEIGHT, "throughholeExtrusionSliceHeight");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSERTION_SLICEHEIGHT, "throughholeInsertionSliceHeight");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT, "throughholeComponentSliceHeight");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT, "throughholePinSliceHeight");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE, "throughholeReferencesFromPlaneOffset");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PIN_DETECTION, "throughholePinDetection");
    //mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_USE_FOR_SURFACE_MODELING_OBSOLETE, "throughholeUseForSurfaceModeling");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PIN_WIDTH, "throughholePinDiameter");
    //Siew Yeng - XCR-3318 - Oval PTH
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIZE_ACROSS, "throughholePinSizeAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIZE_ALONG, "throughholePinSizeAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PIN_SEARCH_DIAMETER_PERCENT_OF_BARREL, "throughholePinSearchDiameter");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PINSIDE_SOLDER_SIGNAL, "throughholeNominalBottomGraylevel");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_SOLDER_SIGNAL, "throughholeNominalBarrelGraylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_2_SOLDER_SIGNAL, "throughholeNominalBarrel2Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_3_SOLDER_SIGNAL, "throughholeNominalBarrel3Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_4_SOLDER_SIGNAL, "throughholeNominalBarrel4Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_5_SOLDER_SIGNAL, "throughholeNominalBarrel5Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_6_SOLDER_SIGNAL, "throughholeNominalBarrel6Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_7_SOLDER_SIGNAL, "throughholeNominalBarrel7Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_8_SOLDER_SIGNAL, "throughholeNominalBarrel8Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_9_SOLDER_SIGNAL, "throughholeNominalBarrel9Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_10_SOLDER_SIGNAL, "throughholeNominalBarrel10Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PROJECTION_SOLDER_SIGNAL, "throughholeNominalProjectionGraylevel");//Broken Pin
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_DIAMETER_TO_TEST, "throughholePercentOfDiameterToTest");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_INNER_EDGE_LOCATION, "throughholeBackgroundRegionInnerEdgeLocation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_OUTER_EDGE_LOCATION, "throughholeBackgroundRegionOuterEdgeLocation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_EXTRUSION_SLICE_OBSOLETE, "throughholeExtrusionSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PROTRUSION_SOLDER_SIGNAL, "throughholeNominalExtrusionSolderSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_INSERTION_SOLDER_SIGNAL, "throughholeNominalInsertionSolderSignal");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_PROTRUSION, "throughholeOpenExtrusionMaxDifferenceFromNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION_PROTRUSION, "throughholeOpenExtrusionMaxDifferenceFromRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_INSERTION, "throughholeOpenInsertionMaxDifferenceFromNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION_INSERTION, "throughholeOpenInsertionMaxDifferenceFromRegion");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_PINSIDE, "throughholeInsufficientPinsideMaxDifferenceFromNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL, "throughholeInsufficientBarrelMaxDifferenceFromNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_2, "throughholeInsufficientBarrelSlice2MaxDifferenceFromNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_3, "throughholeInsufficientBarrelSlice3MaxDifferenceFromNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_4, "throughholeInsufficientBarrelSlice4MaxDifferenceFromNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_5, "throughholeInsufficientBarrelSlice5MaxDifferenceFromNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_6, "throughholeInsufficientBarrelSlice6MaxDifferenceFromNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_7, "throughholeInsufficientBarrelSlice7MaxDifferenceFromNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_8, "throughholeInsufficientBarrelSlice8MaxDifferenceFromNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_9, "throughholeInsufficientBarrelSlice9MaxDifferenceFromNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_10, "throughholeInsufficientBarrelSlice10MaxDifferenceFromNominal");

    //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_EXCESS_DIAMETER_TO_TEST, "throughholePercentOfExcessDiameterToTest");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_PINSIDE, "throughholeMaximumExcessPinsideSolderSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL, "throughholeMaximumExcessBarrelSolderSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_2, "throughholeMaximumExcessBarrelSlice2SolderSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_3, "throughholeMaximumExcessBarrelSlice3SolderSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_4, "throughholeMaximumExcessBarrelSlice4SolderSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_5, "throughholeMaximumExcessBarrelSlice5SolderSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_6, "throughholeMaximumExcessBarrelSlice6SolderSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_7, "throughholeMaximumExcessBarrelSlice7SolderSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_8, "throughholeMaximumExcessBarrelSlice8SolderSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_9, "throughholeMaximumExcessBarrelSlice9SolderSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_10, "throughholeMaximumExcessBarrelSlice10SolderSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_COMPONENTSIDE, "throughholeMaximumExcessComponentsideSolderSignal");    
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_PINSIDE, "throughholeInsufficientPinsideMaxDifferenceFromRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL, "throughholeInsufficientBarrelMaxDifferenceFromRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_2, "throughholeInsufficientBarrel2MaxDifferenceFromRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_3, "throughholeInsufficientBarrel3MaxDifferenceFromRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_4, "throughholeInsufficientBarrel4MaxDifferenceFromRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_5, "throughholeInsufficientBarrel5MaxDifferenceFromRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_6, "throughholeInsufficientBarrel6MaxDifferenceFromRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_7, "throughholeInsufficientBarrel7MaxDifferenceFromRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_8, "throughholeInsufficientBarrel8MaxDifferenceFromRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_9, "throughholeInsufficientBarrel9MaxDifferenceFromRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_10, "throughholeInsufficientBarrel10MaxDifferenceFromRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_WETTING_COVERAGE, "throughholeInsufficientEnableWettingCoverage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE, "throughholeInsufficientWettingCoverage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE_SENSITIVITY, "throughholeInsufficientWettingCoverageSensitivity");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_VOIDING_PERCENTAGE, "throughholeInsufficientVoidingPercentage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DEGREE_COVERAGE, "throughholeInsufficientDegreeCoverage");

    /**
     * @author Lim, Lay Ngor PIPA
     */
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_VOID_VOLUME_PERCENTAGE, "throughholeInsufficientVoidVolumePercentage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_MEASUREMENT, "throughholeInsufficientEnableVoidVolumeMeasurement");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN, "throughholeInsufficientEnableVoidVolumeDirectionFromComponentToPin");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_COMPONENTSIDE, "throughholeInsufficientComponentsideMaxDifferenceFromNominal");    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_COMPONENTSIDE, "throughholeInsufficientComponentsideMaxDifferenceFromRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_COMPONENTSIDE_SOLDER_SIGNAL, "throughholeNominalTopGraylevel");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_PIN_WIDTH, "pressfitPinWidth");
    // mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_BARREL_SLICE_OBSOLETE, "pressfitBarrelSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_CORRECTED_GRAYLEVEL, "pressfitNominalBarrelGraylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_2_CORRECTED_GRAYLEVEL, "pressfitNominalBarrel2Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_3_CORRECTED_GRAYLEVEL, "pressfitNominalBarrel3Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_4_CORRECTED_GRAYLEVEL, "pressfitNominalBarrel4Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_5_CORRECTED_GRAYLEVEL, "pressfitNominalBarrel5Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_6_CORRECTED_GRAYLEVEL, "pressfitNominalBarrel6Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_7_CORRECTED_GRAYLEVEL, "pressfitNominalBarrel7Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_8_CORRECTED_GRAYLEVEL, "pressfitNominalBarrel8Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_9_CORRECTED_GRAYLEVEL, "pressfitNominalBarrel9Graylevel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_10_CORRECTED_GRAYLEVEL, "pressfitNominalBarrel10Graylevel");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL, "pressfitInsufficientBarrelMinimumPercentOfNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_2, "pressfitInsufficientBarrel2MinimumPercentOfNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_3, "pressfitInsufficientBarrel3MinimumPercentOfNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_4, "pressfitInsufficientBarrel4MinimumPercentOfNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_5, "pressfitInsufficientBarrel5MinimumPercentOfNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_6, "pressfitInsufficientBarrel6MinimumPercentOfNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_7, "pressfitInsufficientBarrel7MinimumPercentOfNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_8, "pressfitInsufficientBarrel8MinimumPercentOfNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_9, "pressfitInsufficientBarrel9MinimumPercentOfNominal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_10, "pressfitInsufficientBarrel10MinimumPercentOfNominal");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL, "pressfitInsufficientBarrelMinimumPercentOfRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_2, "pressfitInsufficientBarrel2MinimumPercentOfRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_3, "pressfitInsufficientBarrel3MinimumPercentOfRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_4, "pressfitInsufficientBarrel4MinimumPercentOfRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_5, "pressfitInsufficientBarrel5MinimumPercentOfRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_6, "pressfitInsufficientBarrel6MinimumPercentOfRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_7, "pressfitInsufficientBarrel7MinimumPercentOfRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_8, "pressfitInsufficientBarrel8MinimumPercentOfRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_9, "pressfitInsufficientBarrel9MinimumPercentOfRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_10, "pressfitInsufficientBarrel10MinimumPercentOfRegion");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_PAD_THICKNESS, "chipMeasurementNominalPadThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS, "chipMeasurementNominalPadThicknessForClears");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_BACKGROUND_REGION_SHIFT, "chipMeasurementBackgroundRegionShift");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE, "chipMeasurementBodyThicknessRequiredForOpaqueTest");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH, "chipMeasurementNominalBodyLength");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH, "chipMeasurementNominalBodyLengthClear");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH, "chipMeasurementOpenSignalSearchLength");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_LOCATION_OFFSET, "chipMeasurementOpenSignalSearchLocation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_LOWER_FILLET_LOCATION_OFFSET, "chipMeasurementOpenSignalSpacing");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_SIDE_THICKNESS_PERCENT, "chipMeasurementSideThicknessPercent");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_PAD_PERCENT_TO_MEASURE_FILLET_THICKNESS, "chipMeasurementPadPercentToMeasureFilletThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_TILT_RATIO_MAX, "opaqueComponentTiltedRatioMax");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_EDGE_LOCATION_TECHNIQUE_OPAQUE, "chipMeasurementEdgeLocationTechniqueOpaque");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPDATE_NORMINAL_FILLET_THICKNESS_METHOD, "chipMeasurementUpdateNorminalFilletThicknessMethod");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_CLEAR, "chipMeasurementNominalAlongShiftClear");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_OPAQUE, "chipMeasurementNominalAlongShiftOpaque");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION, "chipMeasurementNominalShiftRotation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_PROFILE_OFFSET_ALONG, "chipMeasurementProfileOffsetAlong");
    //Khaw Chek Hau - XCR2385: Resistor Component Profile Length Across Adjustment
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_ACROSS_PROFILE_SIZE, "chipMeasurementComponentAcrossProfileSize");

    //Lim, Lay Ngor - Clear Tombstone
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_CENTER_OFFSET, "chipMeasurementCenterOffset");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_DETECTION_METHOD, "chipMeasurementUpperFilletDetectionMethod");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_EDGE_SEARCH_THICKNESS_PERCENT, "chipMeasurementUpperFilletEdgeSearchThicknessPercent");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_SEARCH_DISTANCE_FROM_EDGE, "chipMeasurementUpperFilletSearchDistanceFromEdge");
    

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_BODY_LENGTH, "chipOpenMinimumBodyLength");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_BODY_LENGTH, "chipOpenMaximumBodyLength");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_OPAQUE_OPEN_SIGNAL, "chipOpenOpaqueOpenSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_CLEAR_OPEN_SIGNAL, "chipOpenClearOpenSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_FILLET_GAP, "chipOpenMaximumFilletGap");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_BODY_THICKNESS, "chipOpenMinimumBodyThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_CLEAR_MINIMUM_BODY_THICKNESS, "chipOpenClearMinimumBodyThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_BODY_WIDTH, "chipOpenMinimumBodyWidth");
    //Lim, Lay Ngor - Clear Tombstone
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENTAGE, "chipMeasurementMaximumCenterToUpperFilletThicknessPercentage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_SOLDER_AREA_PERCENTAGE, "chipOpenMaximumSolderAreaPercentage");    
    //ShengChuan - Clear Tombstone
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_DETECTION_METHOD, "chipOpenDetectionMethod");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_ROUNDNESS_PERCENTAGE, "chipOpenMaximumRoundnessPercentage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_MATCHING_PERCENTAGE, "chipOpenMinimumMatchingPercentage");
    //Lim, Lay Ngor - Opaque Tombstone
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_ENABLE_OPAQUE_FILLET_WIDTH_DETECTION, "chipOpenEnableOpaqueFilletWidthDetection");//XCR3359
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_FILLET_WIDTH_JOINT_RATIO, "chipOpenMinimumFilletWidthJointRatio");    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_PARTIAL_FILLET_THICKNESS_PERCENTAGE, "chipOpenMinimumPartialFilletThicknessPercentage");     

    // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ALONG, "chipMisalignmentMaxComponentShiftAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ACROSS, "chipMisalignmentMaxComponentShiftAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_ROTATION, "chipMisalignmentMaxComponentRotation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_OPAQUE, "chipMisalignmentMaxMisalignmentAlongDeltaOpaque");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_CLEAR, "chipMisalignmentMaxMisalignmentAlongDeltaClear");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_INSUFFICIENT_MINIMUM_FILLET_THICKNESS, "chipInsufficientMinimumFilletThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.CHIP_EXCESS_MAXIMUM_FILLET_THICKNESS, "chipExcessMaximumFilletThickness");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MEASUREMENT_NOMINAL_PAD_ONE_THICKNESS, "pcapMeasurementNominalPadOneThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MEASUREMENT_NOMINAL_PAD_TWO_THICKNESS, "pcapMeasurementNominalPadTwoThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH, "pcapMeasurementOpenSignalSearchLength");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MEASUREMENT_UPPER_FILLET_OFFSET, "pcapMeasurementOpenSignalSearchLocation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MEASUREMENT_LOWER_FILLET_OFFSET, "pcapMeasurementOpenSignalSpacing");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MEASUREMENT_FILLET_EDGE_PERCENT, "pcapMeasurementFilletEdgePercent");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MEASUREMENT_BACKGROUND_REGION_SHIFT, "pcapBackgroundProfileLocation");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MISALIGNMENT_POLARITY_CHECK_METHODS, "pcapMisalignmentMethods");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MISALIGNMENT_MINIMUM_POLARITY_SIGNAL, "pcapMisalignmentMinimumPolaritySignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_THRESHOLD, "pcapMisalignmentSlugEdgeThreshold");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_LENGTH_ACROSS, "pcapMisalignmentSlugEdgeRegionLengthAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_LENGTH_ALONG, "pcapMisalignmentSlugEdgeRegionLengthAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_POSITION, "pcapMisalignmentSlugEdgeRegionPosition");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_INSUFFICIENT_MINIMUM_FILLET_THICKNESS, "pcapInsufficientMinimumFilletThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_EXCESS_MAXIMUM_FILLET_THICKNESS, "pcapExcessMaximumFilletThickness");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_OPEN_PAD_ONE_MINIMUM_OPEN_SIGNAL, "pcapOpenPadOneOpenSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_OPEN_PAD_TWO_MINIMUM_OPEN_SIGNAL, "pcapOpenPadTwoOpenSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PCAP_OPEN_MINIMUM_SLUG_THICKNESS, "pcapOpenMinimumSlugThickness");

    //Jack
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_METHOD, "gridArrayVoidingChoices");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD, "gridArrayVoidingFloodFillSensitivityThreshold");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD, "gridArrayVoidingBorderThreshold");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PAD, "gridArrayVoidingPercentOfDiameterPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_LOWERPAD, "gridArrayVoidingPercentOfDiameterLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PACKAGE, "gridArrayVoidingPercentOfDiameterPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_MIDBALL, "gridArrayVoidingPercentOfDiameterMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_1, "gridArrayVoidingPercentOfDiameterUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_2, "gridArrayVoidingPercentOfDiameterUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_PAD, "gridArrayVoidingThicknessThresholdPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_LOWERPAD, "gridArrayVoidingThicknessThresholdLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_PACKAGE, "gridArrayVoidingThicknessThresholdPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_MIDBALL, "gridArrayVoidingThicknessThresholdMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_1, "gridArrayVoidingThicknessThresholdUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_2, "gridArrayVoidingThicknessThresholdUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PAD, "gridArrayVoidingFailJointPercentPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_LOWERPAD, "gridArrayVoidingFailJointPercentLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PACKAGE, "gridArrayVoidingFailJointPercentPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_MIDBALL, "gridArrayVoidingFailJointPercentMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_1, "gridArrayVoidingFailJointPercentUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_2, "gridArrayVoidingFailJointPercentUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PAD, "gridArrayVoidingCountToComponentFailPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_LOWERPAD, "gridArrayVoidingCountToComponentFailLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PACKAGE, "gridArrayVoidingCountToComponentFailPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_MIDBALL, "gridArrayVoidingCountToComponentFailMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_1, "gridArrayVoidingCountToComponentFailUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_2, "gridArrayVoidingCountToComponentFailUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_PAD, "gridArrayVoidingFailComponentPercentPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_LOWERPAD, "gridArrayVoidingFailComponentPercentLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_PACKAGE, "gridArrayVoidingFailComponentPercentPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_MIDBALL, "gridArrayVoidingFailComponentPercentMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_1, "gridArrayVoidingFailComponentPercentUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_2, "gridArrayVoidingFailComponentPercentUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_NOISE_REDUCTION, "gridArrayVoidingNoiseReduction");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_NUMBER_OF_IMAGES_TO_SAMPLE, "gridArrayNumberOfImagesToSample");

    //@author Jack Hwee- individual void
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PAD, "gridArrayIndividualVoidingFailJointPercentPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_LOWERPAD, "gridArrayIndividualVoidingFailJointPercentLowerPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PACKAGE, "gridArrayIndividualVoidingFailJointPercentPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_MIDBALL, "gridArrayIndividualVoidingFailJointPercentMidball");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_1, "gridArrayIndividualVoidingFailJointPercentUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_2, "gridArrayIndividualVoidingFailJointPercentUserDefinedSlice2");
    //Siew Yeng - XCR-3515 - Void Diameter
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_PAD, "gridArrayIndividualVoidingFailJointDiameterPercentPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_PACKAGE, "gridArrayIndividualVoidingFailJointDiameterPercentPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_MIDBALL, "gridArrayIndividualVoidingFailJointDiameterPercentMidball");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_WIDTH,"largePadMeasurementOpenInsufficientPadWidth");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_HEIGHT,"largePadMeasurementOpenInsufficientPadHeight");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_IPD_PERCENT,"largePadMeasurementOpenInsufficientIPDPercent");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_BACKGROUND_REGION_SHIFT,"backgroundProfileLocation");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_NUMBER_OF_USER_DEFINED_SLICE, "largePadNumberOfUserDefinedSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_USER_DEFINED_SLICE_1, "largePadUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_USER_DEFINED_SLICE_2, "largePadUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1, "largePadVoidingFailJointPercentPadForUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2, "largePadVoidingFailJointPercentPadForUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1, "largePadIndividualVoidingFailJointPercentPadForUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2, "largePadIndividualVoidingFailJointPercentPadForUserDefinedSlice2");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_REGION_SIZE_PAD, "largePadVoidingBackgroundRegionSizePad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THICKNESS_THRESHOLD_PAD, "largePadVoidingThicknessThresholdPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD, "largePadVoidingFailJointPercentPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_COMPONENT_PERCENT_PAD, "largePadVoidingFailComponentPercentPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NOISE_REDUCTION, "largePadVoidingNoiseReduction");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NOISE_REDUCTION_FOR_FLOODFILL, "largePadVoidingNoiseReductionForFloodfill");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_METHOD, "largePadVoidingUntestedBorderSizeMethod");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE, "largePadVoidingUntestedBorderSize");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_LEFT_SIDE_OFFSET, "largePadVoidingUntestedBorderSizeXWidth");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_BOTTOM_SIDE_OFFSET, "largePadVoidingUntestedBorderSizeYHeight");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_RIGHT_SIDE_OFFSET, "largePadVoidingUntestedBorderSizeXOffset");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_TOP_SIDE_OFFSET, "largePadVoidingUntestedBorderSizeYOffset");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_PERCENTILE, "largePadVoidingBackgroundPercentile");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD, "largePadIndividualVoidingFailJointPercentPad");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_METHOD, "largePadVoidingChoices");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD_FOR_LAYER_ONE, "largePadVoidingFloodFillSensitivityThreshold");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER, "largePadVoidingNumberOfImageLayer");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES, "usingMaskingChoices");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_ACCEPTABLE_GAP_DISTANCE, "largePadIndividualVoidingAcceptableGapDistance");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_ENABLE_SUBREGION, "largePadVoidingEnableSubRegion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VERSION, "largePadVoidingVersion");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_GAUSSIAN_BLUR, "largePadVoidingThresholdOfGaussian");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_TWO, "largePadVoidingSensitivityOfImageLayerTwo");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_THREE, "largePadVoidingSensitivityOfImageLayerThree");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_ADDITIONAL, "largePadVoidingSensitivityOfImageLayerAdditional");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_USING_MULTI_THRESHOLD, "usingMultiThreshold"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_USING_FIT_POLYNOMIAL, "usingPolynomialFitting"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_LOCATOR_METHOD, "usingMaskingLocatorMethod"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_BORDER_SENSIVITY, "largePadVoidingVariablePadSensitivityThreshold");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_VOIDING_SENSIVITY, "largePadVoidingVariablePadVoidingSensitivityThreshold");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ALONG, "largePadOpenMinimumLeadingSlopeAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ALONG, "largePadOpenMinimumTrailingSlopeAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ALONG, "largePadOpenMinimumSlopeSumAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ACROSS, "largePadOpenMinimumLeadingSlopeAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ACROSS, "largePadOpenMinimumTrailingSlopeAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ACROSS, "largePadOpenMinimumSlopeSumAcross");
    //Lim, Lay Ngor - XCR1648:Add Maximum Slope Limit - START        
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ALONG, "largePadOpenMaximumLeadingSlopeAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ALONG, "largePadOpenMaximumTrailingSlopeAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ALONG, "largePadOpenMaximumSlopeSumAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ACROSS, "largePadOpenMaximumLeadingSlopeAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ACROSS, "largePadOpenMaximumTrailingSlopeAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ACROSS, "largePadOpenMaximumSlopeSumAcross");
    //Lim, Lay Ngor - XCR1648:Add Maximum Slope Limit - END    
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_THICKNESS, "largePadInsufficientMinimumThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MAXIMUM_SUM_OF_SLOPE_CHANGES, "largePadInsufficientMaximumSumOfSlopeChanges");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_SMOOTHING_FACTOR, "largePadInsufficientSmoothingFactor");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_HISTOGRAM_AREA_REDUCTION, "largePadInsufficientHistogramAreaReduction");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_METHOD, "largePadInsufficientMethod");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_AREA_METHOD_SENSITIVITY, "largePadInsufficientAreaMethodSensitivity");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD, "largePadMinimumInsufficientArea");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_UNTESTED_BORDER_SIZE, "largePadUntestedBorderSize");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD_FOR_USER_DEFINED_SLICE_1, "largePadMinimumInsufficientAreaForUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD_FOR_USER_DEFINED_SLICE_2, "largePadMinimumInsufficientAreaForUserDefinedSlice2");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_VOID_COMPENSATION, "quadFlatNoLeadHeelVoidCompensation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_PAD_PROFILE_WIDTH, "quadFlatNoLeadPadProfileWidth");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_PAD_PROFILE_LENGTH, "quadFlatNoLeadPadProfileLength");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_REGION_LOCATION, "quadFlatNoLeadBackgroundRegionLocation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET, "quadFlatNoLeadBackgroundRegionLocationOffset");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT, "quadFlatNoLeadHeelEdgeSearchThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE, "quadFlatNoLeadHeelSearchDistanceFromEdge");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_CENTER_OFFSET, "quadFlatNoLeadCenterOffsetFromHeel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT, "quadFlatNoLeadToeEdgeSearchThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_LENGTH, "quadFlatNoLeadNominalFilletLength");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_THICKNESS, "quadFlatNoLeadNominalFilletThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_HEEL_THICKNESS, "quadFlatNoLeadNominalHeelThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_TOE_THICKNESS, "quadFlatNoLeadNominalToeThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_THICKNESS, "quadFlatNoLeadNominalCenterThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_FILLET_LENGTH_TECHNIQUE, "quadFlatNoLeadFilletLengthTechnique");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_FEATURE_LOCATION_TECHNIQUE, "quadFlatNoLeadFeatureLocationTechnique");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_OFFSET, "quadFlatNoLeadToeOffsetFromHeel");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_START_DISTANCE_FROM_HEEL_EDGE, "quadFlatNoLeadUpwardCurvatureStartDistanceFromHeelEdge");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_END_DISTANCE_FROM_HEEL_EDGE, "quadFlatNoLeadUpwardCurvatureEndDistanceFromHeelEdge");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_LOCATION_TECHNIQUE, "quadFlatNoLeadToeLocationTechnique");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_FIXED_DISTANCE, "quadFlatNoLeadToeFixedDistance");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_CENTER_LOCATION_TECHNIQUE, "quadFlatNoLeadCenterLocationTechnique");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_SMOOTHING_KERNEL_LENGTH, "quadFlatNoLeadSmoothingKernelLength");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_TECHNIQUE, "quadFlatNoLeadBackgroundTechnique");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_SOLDER_THICKNESS_LOCATION, "quadFlatNoLeadSolderThicknessLocation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_OF_SOLDER_VOLUME, "quadFlatNoLeadNominalCenterOfSolderVolume");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, "quadFlatNoLeadMinimumFilletLength");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, "quadFlatNoLeadMaximumFilletLength");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL, "quadFlatNoLeadMaximumCenterThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_OPEN_SIGNAL, "quadFlatNoLeadMinimumOpenSignal");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_UPWARD_CURVATURE, "quadFlatNoLeadMinimumUpwardCurvature");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SLOPE, "quadFlatNoLeadMinimumHeelSlope");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_SLOPE_SUM_CHANGES, "quadFlatNoLeadMinimumSlopeSumChanges");
    //Lim, Lay Ngor & Kee Chin Seong - XCR1648:Add Maximum Slope Limit - START    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_SLOPE, "quadFlatNoLeadMaximumHeelSlope");
	mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM, "quadFlatNoLeadMaximumHeelToeSlopeSum");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_SLOPE_SUM_CHANGES, "quadFlatNoLeadMaximumSlopeSumChanges");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE, "quadFlatNoLeadMaximumHeelAcrossLTSlope");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_SLOPE_SUM, "quadFlatNoLeadMaximumHeelAcrossSlopeSum");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_TOE_SLOPE, "quadFlatNoLeadMaximumToeSlope");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_SLOPE, "quadFlatNoLeadMaximumCenterSlope");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE, "quadFlatNoLeadMaximumCenterAcrossLTSlope");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_SLOPE_SUM, "quadFlatNoLeadMaximumCenterAcrossSlopeSum");
    //Lim, Lay Ngor - XCR1648:Add Maximum Slope Limit - END    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE, "quadFlatNoLeadMinimumHeelAcrossLTSlope");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_SLOPE_SUM, "quadFlatNoLeadMinimumHeelAcrossSlopeSum");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_WIDTH, "quadFlatNoLeadMinimumCenterAcrossWidth");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_HEEL_WEIGHT, "quadFlatNoLeadHeelWeight");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_TOE_WEIGHT, "quadFlatNoLeadToeWeight");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_CENTER_WEIGHT, "quadFlatNoLeadCenterWeight");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SHARPNESS, "quadFlatNoLeadMinimumHeelSharpness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_TOE_SLOPE, "quadFlatNoLeadMinimumToeSlope");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_SLOPE, "quadFlatNoLeadMinimumCenterSlope");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM, "quadFlatNoLeadMinimumHeelToeSlopeSum");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_WIDTH, "quadFlatNoLeadMinimumHeelAcrossWidth");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE, "quadFlatNoLeadMinimumCenterAcrossLTSlope");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_SLOPE_SUM, "quadFlatNoLeadMinimumCenterAcrossSlopeSum");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_NEIGHBOR_LENGTH_DIFFERENCE, "quadFlatNoLeadMaximumNeighborLengthDifference");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_TO_TOE_PERCENT, "quadFlatNoLeadMaximumCenterToToePercent");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_TO_HEEL_PERCENT, "quadFlatNoLeadMaximumCenterToHeelPercent");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_ENABLE_MAXIMUM_CENTER_TO_TOE_PERCENT_TEST, "quadFlatNoLeadEnableMaximumCenterToToePercentTest");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_SOLDER_THICKNESS_AT_LOCATION, "quadFlatNoLeadMinimumSolderThicknessAtLocation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL, "quadFlatNoLeadMaximumCenterOfSolderVolumeLocation");
    //Siew Yeng - XCR-3532
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER, "quadFlatNoLeadOpenMaxFilletLengthRegionOutlier");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER, "quadFlatNoLeadOpenMinFilletLengthRegionOutlier");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER, "quadFlatNoLeadOpenMaxCenterThicknessRegionOutlier");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER, "quadFlatNoLeadOpenMinCenterThicknessRegionOutlier");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_FILLET_THICKNESS_PERCENT_OF_NOMINAL, "quadFlatNoLeadMinimumFilletThicknessPercent");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL, "quadFlatNoLeadMinimunHeelThicknessPercent");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL, "quadFlatNoLeadMinimumCenterThicknessPercent");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_TOE_THICKNESS_PERCENT_OF_NOMINAL, "quadFlatNoLeadMinimumToeThicknessPercent");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_VOIDING_MAXIMUM_VOID_PERCENT, "quadFlatNoLeadVoidingMaximumVoidPercent");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_VOIDING_NOISE_REDUCTION, "quadFlatNoLeadVoidingNoiseReduction");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.QFN_INDIVIDUAL_VOIDING_MAXIMUM_VOID_PERCENT, "quadFlatNoLeadVoidingMaximumIndividualVoidPercent");

    //EXPOSED PAD : MEASUREMENT
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ACROSS_MEASUREMENTS, "exposedPadMeasurementNumberOfProfileAcrossMeasurements");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_LOCATION_ACROSS, "exposedPadMeasurementProfileAcrossPosition");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_WIDTH_ACROSS, "exposedPadMeasurementProfileWidthAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_GAP_ACROSS_MEASUREMENTS, "exposedPadMeasurementNumberOfGapAcrossMeasurements");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE_ACROSS, "exposedPadMeasurementBackgroundAcrossMethod");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ALONG_MEASUREMENTS, "exposedPadMeasurementNumberOfProfileAlongMeasurements");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_LOCATION_ALONG, "exposedPadMeasurementProfileAlongPosition");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_WIDTH_ALONG, "exposedPadMeasurementProfileWidthAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_GAP_ALONG_MEASUREMENTS, "exposedPadMeasurementNumberOfGapAlongMeasurements");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE_ALONG, "exposedPadMeasurementBackgroundAlongMethod");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ACROSS, "exposedPadMeasurementBackgroundAcrossProfileLocation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ACROSS, "exposedPadMeasurementCenterSearchDistanceAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_OFFSET_ACROSS, "exposedPadMeasurementGapOffsetAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ALONG, "exposedPadMeasurementBackgroundAlongProfileLocation");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ALONG, "exposedPadMeasurementCenterSearchDistanceAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_OFFSET_ALONG, "exposedPadMeasurementGapOffsetAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_MAXIMUM_EXPECTED_THICKNESS, "exposedPadMeasurementMaximumExpectedThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_CLIFF_SLOPE, "exposedPadMeasurementGapCliffSlope");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_FALLOFF_SLOPE, "exposedPadMeasurementGapFalloffSlope");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_SMOOTHING_LENGTH, "exposedPadSmoothingKernelLength");
    // mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CTR_SEARCH_DIST_OBSOLETE, "exposedPadMeasurementCenterSearchDistance");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE, "exposedPadMeasurementBackgroundMethod");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NOMINAL_BACKGROUND_VALUE, "exposedPadMeasurementNominalBackgroundValue");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CONTRAST_ENHANCEMENT, "exposedPadMeasurementContrastEnhancement");

    // EXPOSED PAD : OPEN
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ACROSS, "exposedPadOpenMaximumGapPercentAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_OUTER_GAP_LENGTH_ACROSS, "exposedPadOpenMaximumOuterGapAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MINIMUM_OUTER_GAP_THICKNESS_ACROSS, "exposedPadOpenMaximumThicknessOuterGapAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ALONG, "exposedPadOpenMaximumGapPercentAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_OUTER_GAP_LENGTH_ALONG, "exposedPadOpenMaximumOuterGapAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MINIMUM_OUTER_GAP_THICKNESS_ALONG, "exposedPadOpenMaximumThicknessOuterGapAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_GAP_LENGTH_ACROSS, "exposedPadOpenMaximumInnerGapAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_GAP_LENGTH_ALONG, "exposedPadOpenMaximumInnerGapAlong");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_ALLOWED_GAP_FAILURES, "exposedPadOpenAllowedGapFailures");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ACROSS, "exposedPadOpenFilledGapThicknessAcross");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ALONG, "exposedPadOpenFilledGapThicknessAlong");

    // EXPOSED PAD : INSUFFICIENT
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_INSUFFICIENT_MAX_VOID_PERCENT, "exposedPadInsufficientMaximumVoidPercent");

    // EXPOSED PAD : VOIDING
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MAXIMUM_VOID_PERCENT, "exposedPadVoidingMaximumVoidPercent");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_SOLDER_THICKNESS, "exposedPadVoidingSolderThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MAXIMUM_GRAY_LEVEL_DIFFERENCE, "exposedPadVoidingGraylevelThreshold");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MINIMUM_VOID_AREA, "exposedPadMeasurementMinimumVoidArea");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_LEVEL_OF_NOISE_REDUCTION, "exposedPadVoidingNoiseReduction");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_VOIDING_TECHNIQUE, "exposedPadVoidingVoidingTechnique");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.EXPOSED_PAD_INDIVIDUAL_VOIDING_MAXIMUM_VOID_PERCENT, "exposedPadIndividualVoidingMaximumVoidPercent");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT, "userDefinedPadSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_BGA_LOWERPAD_SLICEHEIGHT, "userDefinedBgaLowerPadSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_PACKAGE_SLICEHEIGHT, "userDefinedPackageSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_MIDBALL_SLICEHEIGHT, "userDefinedMidballSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_PCAP_SLUG_SLICEHEIGHT, "userDefinedSlugSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_OPAQUE_CHIP_SLICEHEIGHT, "userDefinedOpaqueChipSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_CLEAR_CHIP_SLICEHEIGHT, "userDefinedClearChipSlice");
    // Swee Yee Wong - enable user defined midball to edge offset for grid array
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_DEFINE_OFFSET_FOR_PAD_AND_PACKAGE, "defineOffsetForPadAndPackage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_MIDBALL_TO_EDGE_SLICEHEIGHT, "userDefinedMidballToEdgeOffset");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT, "predictiveSliceHeight");

    // added by wei chin 14-Jul
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_CONTRAST_ENHANCEMENT, "gridArrayContrastEnhancement");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SLICEHEIGHT_WORKING_UNIT, "sliceheightworkingunit");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_NUMBER_OF_SLICE, "numberofbarrelSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS, "throughholeBarrelSliceHeightinThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT_IN_THICKNESS, "throughholeComponentSliceHeightinThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT_IN_THICKNESS, "throughholePinSideSliceHeightinThickness");
    //THROUGHHOLE_PIN_SIDE_SLICEHEIGHT_IN_THICKNESS

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_2, "throughholeBarrelSliceHeightSecondSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_2, "throughholeSecondBarrelSliceHeightinThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_3, "throughholeBarrelSliceHeightThridSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_3, "throughholeThirdBarrelSliceHeightinThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_4, "throughholeBarrelSliceHeightFourthSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_4, "throughholeFourthBarrelSliceHeightinThickness");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_5, "throughholeBarrelSliceHeightSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_5, "throughholeBarrelSliceHeightinThickness5");    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_6, "throughholeBarrelSliceHeightSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_6, "throughholeBarrelSliceHeightinThickness6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_7, "throughholeBarrelSliceHeightSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_7, "throughholeBarrelSliceHeightinThickness7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_8, "throughholeBarrelSliceHeightSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_8, "throughholeBarrelSliceHeightinThickness8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_9, "throughholeBarrelSliceHeightFourthSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_9, "throughholeBarrelSliceHeightinThickness9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_10, "throughholeBarrelSliceHeightSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_10, "throughholeBarrelSliceHeightinThickness10");

    // added by Lee Herng (4 Mar 2011)
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET, "autoFocusMidBoardOffset");

    // Added by Seng Yew on 22-Apr-2011
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_UPDATE, "userDefinedGraylevelUpdate");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM, "userDefinedGraylevelMinimum");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM, "userDefinedGraylevelMaximum");
    
    // Added by Lee Herng on 19-Feb-2012
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION, "userDefinedFocusConfirmation");
    
    // Added by Lee Herng on 22-Mar-2012
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MULTIPASS_INSPECTION, "gridArrayOpenMultipassInspection");
    
    // Added by Lee herng on 5-Apr-2012
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE, "gridArrayNumberOfUserDefinedSlice");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, "gridArrayUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, "gridArrayUserDefinedSlice2");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_1, "gridArrayEdgeDetectionPercentUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_2, "gridArrayEdgeDetectionPercentUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_1, "gridArrayNominalDiameterUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_2, "gridArrayNominalDiameterUserDefinedSlice2");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1, "gridArrayOpenMaximumHIPOutlierUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1, "gridArrayOpenMinimumHIPOutlierUserDefinedSlice1");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2, "gridArrayOpenMaximumHIPOutlierUserDefinedSlice2");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2, "gridArrayOpenMinimumHIPOutlierUserDefinedSlice2");

    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ADVANCED_GULLWING_USER_DEFINED_NUMBER_OF_SLICE, "advancedGullwingNumberofSlice");

    // Added by Wei Chin on 10 Sept 2012
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_ENABLE, "imageEnhancementBFilterEnable");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_ITERATOR, "imageEnhancementBFilterIterator");
    // Added by Siew Yeng - XCR-2388
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_WIDTH, "imageEnhancementBFilterWidth");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_LENGTH, "imageEnhancementBFilterLength");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_ENABLE, "imageEnhancementResizeEnable");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_SCALE, "imageEnhancementResizeScale");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_CLAHE_ENABLE, "imageEnhancementCLAHEEnable");
    // Added by Siew Yeng - XCR-2388
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_CLAHE_BLOCK_SIZE, "imageEnhancementCLAHEBlockSize");
    
    // Added by Khang Wah, 2013-09-11, user defined initial wavelet level
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL, "searchSpeed");
    
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - START
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_ENABLE, "imageEnhancementFFTBandPassFilterEnable");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_LARGESCALE, "imageEnhancementFFTBandPassFilterLargeScale");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SMALLSCALE, "imageEnhancementFFTBandPassFilterSmallScale");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SUPPRESSSTRIPES, "imageEnhancementFFTBandPassFilterSuppressStripes");    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_TOLERANCEOFDIRECTION, "imageEnhancementFFTBandPassFilterToleranceOfDirection");    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SATURATEVALUE, "imageEnhancementFFTBandPassFilterSaturateValue");    
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - END
    
    // Kok Chun, Tan - motion blur
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_ENABLE, "imageEnhancementMotionBlurEnable");   
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_MASK_SCALE, "imageEnhancementMotionBlurMaskScale");   
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_GRAYLEVEL_OFFSET, "imageEnhancementMotionBlurGraylevelOffset");  
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_DIRECTION, "imageEnhancementMotionBlurDirection"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_ITERATION, "imageEnhancementMotionBlurIteration"); 
    
    // Kok Chun, Tan - shading removal
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_ENABLE, "imageEnhancementShadingRemovalEnable");   
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_DIRECTION, "imageEnhancementShadingRemovalDirection");   
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_BLUR_DISTANCE, "imageEnhancementShadingRemovalBlurDistance");  
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_KEEP_OUT_DISTANCE, "imageEnhancementShadingRemovalKeepOutDistance"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_DESIRED_BACKGROUND, "imageEnhancementShadingRemovalDesiredBackground"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_FILTERING_TECHNIQUE, "imageEnhancementShadingRemovalFilteringTechnique"); 
    
	// Added by Lee Herng, 28 Oct 2013, BGA User-Defined Slice 3
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_3, "gridArrayMeasurementNominalThicknessUserDefinedSlice3");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_3, "gridArrayMeasurementEdgeDetectionPercentUserDefinedSlice3");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_3, "gridArrayMeasurementNominalDiameterUserDefinedSlice3");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_3, "gridArrayOpenMinimumDiameterUserDefinedSlice3");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_3, "gridArrayOpenMaximumDiameterUserDefinedSlice3");
    
    //swee-yee.wong
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_4, "gridArrayOpenMinimumDiameterUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_4, "gridArrayOpenMaximumDiameterUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_5, "gridArrayOpenMinimumDiameterUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_5, "gridArrayOpenMaximumDiameterUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_6, "gridArrayOpenMinimumDiameterUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_6, "gridArrayOpenMaximumDiameterUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_7, "gridArrayOpenMinimumDiameterUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_7, "gridArrayOpenMaximumDiameterUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_8, "gridArrayOpenMinimumDiameterUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_8, "gridArrayOpenMaximumDiameterUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_9, "gridArrayOpenMinimumDiameterUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_9, "gridArrayOpenMaximumDiameterUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_10, "gridArrayOpenMinimumDiameterUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_10, "gridArrayOpenMaximumDiameterUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_11, "gridArrayOpenMinimumDiameterUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_11, "gridArrayOpenMaximumDiameterUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_12, "gridArrayOpenMinimumDiameterUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_12, "gridArrayOpenMaximumDiameterUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_13, "gridArrayOpenMinimumDiameterUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_13, "gridArrayOpenMaximumDiameterUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_14, "gridArrayOpenMinimumDiameterUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_14, "gridArrayOpenMaximumDiameterUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_15, "gridArrayOpenMinimumDiameterUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_15, "gridArrayOpenMaximumDiameterUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_16, "gridArrayOpenMinimumDiameterUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_16, "gridArrayOpenMaximumDiameterUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_17, "gridArrayOpenMinimumDiameterUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_17, "gridArrayOpenMaximumDiameterUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_18, "gridArrayOpenMinimumDiameterUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_18, "gridArrayOpenMaximumDiameterUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_19, "gridArrayOpenMinimumDiameterUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_19, "gridArrayOpenMaximumDiameterUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_20, "gridArrayOpenMinimumDiameterUserDefinedSlice20");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_20, "gridArrayOpenMaximumDiameterUserDefinedSlice20");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice3");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice3");
    
    //swee-yee.wong
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20, "gridArrayOpenMinimumNeighborOutlierUserDefinedSlice20");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20, "gridArrayOpenMaximumNeighborOutlierUserDefinedSlice20");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice3");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice3");  
    
    //swee-yee.wong
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice4"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice5"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice6"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice7"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice8"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice9"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice10"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice11"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice12"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice13"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice14"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice15"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice16"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice17"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice18"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice19"); 
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20, "gridArrayOpenMinimumRegionOutlierUserDefinedSlice20");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20, "gridArrayOpenMaximumRegionOutlierUserDefinedSlice20"); 
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3, "gridArrayOpenMinimumEccentricityUserDefinedSlice3");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3, "gridArrayOpenMaximumEccentricityUserDefinedSlice3");
    
    //swee-yee.wong
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4, "gridArrayOpenMinimumEccentricityUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4, "gridArrayOpenMaximumEccentricityUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5, "gridArrayOpenMinimumEccentricityUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5, "gridArrayOpenMaximumEccentricityUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6, "gridArrayOpenMinimumEccentricityUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6, "gridArrayOpenMaximumEccentricityUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7, "gridArrayOpenMinimumEccentricityUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7, "gridArrayOpenMaximumEccentricityUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8, "gridArrayOpenMinimumEccentricityUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8, "gridArrayOpenMaximumEccentricityUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9, "gridArrayOpenMinimumEccentricityUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9, "gridArrayOpenMaximumEccentricityUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10, "gridArrayOpenMinimumEccentricityUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10, "gridArrayOpenMaximumEccentricityUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11, "gridArrayOpenMinimumEccentricityUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11, "gridArrayOpenMaximumEccentricityUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12, "gridArrayOpenMinimumEccentricityUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12, "gridArrayOpenMaximumEccentricityUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13, "gridArrayOpenMinimumEccentricityUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13, "gridArrayOpenMaximumEccentricityUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14, "gridArrayOpenMinimumEccentricityUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14, "gridArrayOpenMaximumEccentricityUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15, "gridArrayOpenMinimumEccentricityUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15, "gridArrayOpenMaximumEccentricityUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16, "gridArrayOpenMinimumEccentricityUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16, "gridArrayOpenMaximumEccentricityUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17, "gridArrayOpenMinimumEccentricityUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17, "gridArrayOpenMaximumEccentricityUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18, "gridArrayOpenMinimumEccentricityUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18, "gridArrayOpenMaximumEccentricityUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19, "gridArrayOpenMinimumEccentricityUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19, "gridArrayOpenMaximumEccentricityUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20, "gridArrayOpenMinimumEccentricityUserDefinedSlice20");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20, "gridArrayOpenMaximumEccentricityUserDefinedSlice20");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3, "gridArrayOpenMaximumHipOutlierUserDefinedSlice3");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3, "gridArrayOpenMinimumHipOutlierUserDefinedSlice3");
    //swee-yee.wong
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4, "gridArrayOpenMaximumHipOutlierUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4, "gridArrayOpenMinimumHipOutlierUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5, "gridArrayOpenMaximumHipOutlierUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5, "gridArrayOpenMinimumHipOutlierUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6, "gridArrayOpenMaximumHipOutlierUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6, "gridArrayOpenMinimumHipOutlierUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7, "gridArrayOpenMaximumHipOutlierUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7, "gridArrayOpenMinimumHipOutlierUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8, "gridArrayOpenMaximumHipOutlierUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8, "gridArrayOpenMinimumHipOutlierUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9, "gridArrayOpenMaximumHipOutlierUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9, "gridArrayOpenMinimumHipOutlierUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10, "gridArrayOpenMaximumHipOutlierUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10, "gridArrayOpenMinimumHipOutlierUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11, "gridArrayOpenMaximumHipOutlierUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11, "gridArrayOpenMinimumHipOutlierUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12, "gridArrayOpenMaximumHipOutlierUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12, "gridArrayOpenMinimumHipOutlierUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13, "gridArrayOpenMaximumHipOutlierUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13, "gridArrayOpenMinimumHipOutlierUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14, "gridArrayOpenMaximumHipOutlierUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14, "gridArrayOpenMinimumHipOutlierUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15, "gridArrayOpenMaximumHipOutlierUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15, "gridArrayOpenMinimumHipOutlierUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16, "gridArrayOpenMaximumHipOutlierUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16, "gridArrayOpenMinimumHipOutlierUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17, "gridArrayOpenMaximumHipOutlierUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17, "gridArrayOpenMinimumHipOutlierUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18, "gridArrayOpenMaximumHipOutlierUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18, "gridArrayOpenMinimumHipOutlierUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19, "gridArrayOpenMaximumHipOutlierUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19, "gridArrayOpenMinimumHipOutlierUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20, "gridArrayOpenMaximumHipOutlierUserDefinedSlice20");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20, "gridArrayOpenMinimumHipOutlierUserDefinedSlice20");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_3, "gridArrayVoidingPercentOfDiameterUserDefinedSlice3");
    //swee-yee.wong
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_4, "gridArrayVoidingPercentOfDiameterUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_5, "gridArrayVoidingPercentOfDiameterUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_6, "gridArrayVoidingPercentOfDiameterUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_7, "gridArrayVoidingPercentOfDiameterUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_8, "gridArrayVoidingPercentOfDiameterUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_9, "gridArrayVoidingPercentOfDiameterUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_10, "gridArrayVoidingPercentOfDiameterUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_11, "gridArrayVoidingPercentOfDiameterUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_12, "gridArrayVoidingPercentOfDiameterUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_13, "gridArrayVoidingPercentOfDiameterUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_14, "gridArrayVoidingPercentOfDiameterUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_15, "gridArrayVoidingPercentOfDiameterUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_16, "gridArrayVoidingPercentOfDiameterUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_17, "gridArrayVoidingPercentOfDiameterUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_18, "gridArrayVoidingPercentOfDiameterUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_19, "gridArrayVoidingPercentOfDiameterUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_20, "gridArrayVoidingPercentOfDiameterUserDefinedSlice20");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_3, "gridArrayVoidingThicknessThresholdUserDefinedSlice3");
    //swee-yee.wong
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_4, "gridArrayVoidingThicknessThresholdUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_5, "gridArrayVoidingThicknessThresholdUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_6, "gridArrayVoidingThicknessThresholdUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_7, "gridArrayVoidingThicknessThresholdUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_8, "gridArrayVoidingThicknessThresholdUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_9, "gridArrayVoidingThicknessThresholdUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_10, "gridArrayVoidingThicknessThresholdUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_11, "gridArrayVoidingThicknessThresholdUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_12, "gridArrayVoidingThicknessThresholdUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_13, "gridArrayVoidingThicknessThresholdUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_14, "gridArrayVoidingThicknessThresholdUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_15, "gridArrayVoidingThicknessThresholdUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_16, "gridArrayVoidingThicknessThresholdUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_17, "gridArrayVoidingThicknessThresholdUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_18, "gridArrayVoidingThicknessThresholdUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_19, "gridArrayVoidingThicknessThresholdUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_20, "gridArrayVoidingThicknessThresholdUserDefinedSlice20");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_3, "gridArrayVoidingFailJointAreaUserDefinedSlice3");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_3, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice3");
    
    //swee-yee.wong
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_4, "gridArrayVoidingFailJointAreaUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_4, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_5, "gridArrayVoidingFailJointAreaUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_5, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_6, "gridArrayVoidingFailJointAreaUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_6, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_7, "gridArrayVoidingFailJointAreaUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_7, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_8, "gridArrayVoidingFailJointAreaUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_8, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_9, "gridArrayVoidingFailJointAreaUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_9, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_10, "gridArrayVoidingFailJointAreaUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_10, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_11, "gridArrayVoidingFailJointAreaUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_11, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_12, "gridArrayVoidingFailJointAreaUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_12, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_13, "gridArrayVoidingFailJointAreaUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_13, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_14, "gridArrayVoidingFailJointAreaUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_14, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_15, "gridArrayVoidingFailJointAreaUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_15, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_16, "gridArrayVoidingFailJointAreaUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_16, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_17, "gridArrayVoidingFailJointAreaUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_17, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_18, "gridArrayVoidingFailJointAreaUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_18, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_19, "gridArrayVoidingFailJointAreaUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_19, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_20, "gridArrayVoidingFailJointAreaUserDefinedSlice20");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_20, "gridArrayIndividualVoidingFailJointAreaUserDefinedSlice20");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_3, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice3");
    //swee-yee.wong
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_4, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_5, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_6, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_7, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_8, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_9, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_10, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_11, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_12, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_13, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_14, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_15, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_16, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_17, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_18, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_19, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_20, "gridArrayVoidingMarginalVoidAreaUserDefinedSlice20");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_3, "gridArrayVoidingFailComponentPercentUserDefinedSlice3");
    //swee-yee.wong
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_4, "gridArrayVoidingFailComponentPercentUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_5, "gridArrayVoidingFailComponentPercentUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_6, "gridArrayVoidingFailComponentPercentUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_7, "gridArrayVoidingFailComponentPercentUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_8, "gridArrayVoidingFailComponentPercentUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_9, "gridArrayVoidingFailComponentPercentUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_10, "gridArrayVoidingFailComponentPercentUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_11, "gridArrayVoidingFailComponentPercentUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_12, "gridArrayVoidingFailComponentPercentUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_13, "gridArrayVoidingFailComponentPercentUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_14, "gridArrayVoidingFailComponentPercentUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_15, "gridArrayVoidingFailComponentPercentUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_16, "gridArrayVoidingFailComponentPercentUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_17, "gridArrayVoidingFailComponentPercentUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_18, "gridArrayVoidingFailComponentPercentUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_19, "gridArrayVoidingFailComponentPercentUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_20, "gridArrayVoidingFailComponentPercentUserDefinedSlice20");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, "gridArrayUserDefinedSlice3");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_4, "gridArrayUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_5, "gridArrayUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_6, "gridArrayUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_7, "gridArrayUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_8, "gridArrayUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_9, "gridArrayUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_10, "gridArrayUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_11, "gridArrayUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_12, "gridArrayUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_13, "gridArrayUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_14, "gridArrayUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_15, "gridArrayUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_16, "gridArrayUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_17, "gridArrayUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_18, "gridArrayUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_19, "gridArrayUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_20, "gridArrayUserDefinedSlice20");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_4, "gridArrayEdgeDetectionPercentUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_5, "gridArrayEdgeDetectionPercentUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_6, "gridArrayEdgeDetectionPercentUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_7, "gridArrayEdgeDetectionPercentUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_8, "gridArrayEdgeDetectionPercentUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_9, "gridArrayEdgeDetectionPercentUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_10, "gridArrayEdgeDetectionPercentUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_11, "gridArrayEdgeDetectionPercentUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_12, "gridArrayEdgeDetectionPercentUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_13, "gridArrayEdgeDetectionPercentUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_14, "gridArrayEdgeDetectionPercentUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_15, "gridArrayEdgeDetectionPercentUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_16, "gridArrayEdgeDetectionPercentUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_17, "gridArrayEdgeDetectionPercentUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_18, "gridArrayEdgeDetectionPercentUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_19, "gridArrayEdgeDetectionPercentUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_20, "gridArrayEdgeDetectionPercentUserDefinedSlice20");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_4, "gridArrayNominalDiameterUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_5, "gridArrayNominalDiameterUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_6, "gridArrayNominalDiameterUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_7, "gridArrayNominalDiameterUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_8, "gridArrayNominalDiameterUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_9, "gridArrayNominalDiameterUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_10, "gridArrayNominalDiameterUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_11, "gridArrayNominalDiameterUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_12, "gridArrayNominalDiameterUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_13, "gridArrayNominalDiameterUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_14, "gridArrayNominalDiameterUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_15, "gridArrayNominalDiameterUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_16, "gridArrayNominalDiameterUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_17, "gridArrayNominalDiameterUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_18, "gridArrayNominalDiameterUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_19, "gridArrayNominalDiameterUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_20, "gridArrayNominalDiameterUserDefinedSlice20");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_4, "gridArrayMeasurementNominalThicknessUserDefinedSlice4");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_5, "gridArrayMeasurementNominalThicknessUserDefinedSlice5");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_6, "gridArrayMeasurementNominalThicknessUserDefinedSlice6");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_7, "gridArrayMeasurementNominalThicknessUserDefinedSlice7");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_8, "gridArrayMeasurementNominalThicknessUserDefinedSlice8");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_9, "gridArrayMeasurementNominalThicknessUserDefinedSlice9");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_10, "gridArrayMeasurementNominalThicknessUserDefinedSlice10");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_11, "gridArrayMeasurementNominalThicknessUserDefinedSlice11");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_12, "gridArrayMeasurementNominalThicknessUserDefinedSlice12");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_13, "gridArrayMeasurementNominalThicknessUserDefinedSlice13");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_14, "gridArrayMeasurementNominalThicknessUserDefinedSlice14");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_15, "gridArrayMeasurementNominalThicknessUserDefinedSlice15");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_16, "gridArrayMeasurementNominalThicknessUserDefinedSlice16");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_17, "gridArrayMeasurementNominalThicknessUserDefinedSlice17");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_18, "gridArrayMeasurementNominalThicknessUserDefinedSlice18");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_19, "gridArrayMeasurementNominalThicknessUserDefinedSlice19");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_20, "gridArrayMeasurementNominalThicknessUserDefinedSlice20");
    
    // Added by Wei Chin
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.BACKGROUND_SENSITIVITY_VALUE, "backgroundSensitivityValue");
    
    // Added by Lee Herng, 2014-08-13, search range limit
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PSP_SEARCH_RANGE_LOW_LIMIT, "searchRangeLowLimit");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PSP_SEARCH_RANGE_HIGH_LIMIT, "searchRangeHighLimit");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PSP_LOCAL_SEARCH, "pspLocalSearch");
    
    // Added by Lee Herng, 2016-08-10, psp Z-offset
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.PSP_Z_OFFSET, "pspZOffset");
    
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.MAXIMUM_NUMBER_PINS_FAILED_MIDBALL_VOID, "maximumNumberPinsFailedMidballVoid"); 
    
    //Added by Wei Chin
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SAVE_DIAGNOSTIC_IMAGES, "saveVoidDiagnosticImage");
    
    //Added by Siew Yeng
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RFILTER_ENABLE, "imageEnhancementRFilterEnable");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SAVE_ENHANCED_IMAGE, "saveEnhancedImage");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.STITCH_COMPONENT_IMAGE_AT_VVTS, "stitchComponentImageAtVVTS");

    //Added By Kee Chin Seong - Circular Voiding Algorithm
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_VOIDING_DETECTION_SENSITIVITY, "circularVoidingDetectionSensitivity");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_VOIDING_REGION_REFERENCE_POSITION, "circularVoidingReferencePosition");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_VOIDING_INNER_EDGE_POSITION, "circularVoidingInnerEdgePosition");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_VOIDING_OUTER_EDGE_POSITION, "circularVoidingOuterEdgePosition");
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.SHARED_VOIDING_GREY_LEVEL_NOMINAL, "circularVoidingGreyLEvelNominal");
    
    //Khaw Chek Hau - XCR3601: Auto distribute the pressfit region by direction of artifact
    mapAlgorithmSettingEnumToString(AlgorithmSettingEnum.ARTIFACT_BASED_REGION_SEPERATION_DIRECTION, "artifactBasedRegionSeperationDirection");

    // create a list of obsolete algorithm setting enum string.  These were used in older versions of the software and
    // therefore may exist in some people's projects.  These obsolete strings should have an associated converter in
    // datastore.projReaders().  The main purpose of this list is to make sure that nobody tries to reuse these strings
    // for future algorithm setting enums.  That would cause problems.  - PE
    _oldAlgorithmSettingEnumStrings.add("throughholeBarrelSlice");
    _oldAlgorithmSettingEnumStrings.add("pressfitBarrelSlice");
    _oldAlgorithmSettingEnumStrings.add("gridArrayVariableHeightConnectorPadSliceOffset");
    _oldAlgorithmSettingEnumStrings.add("exposedPadMeasurementCenterSearchDistance");

    // check to make sure no obsoleted strings are being used
    Set<String> obsoleteStringsBeingUsed = new HashSet<String>(_oldAlgorithmSettingEnumStrings);
    obsoleteStringsBeingUsed.retainAll( _algorithmSettingEnumToStringMap.values());
    Assert.expect(obsoleteStringsBeingUsed.size() == 0);

    // Ensure that there is a map entry for each AlgorithmSettingEnum.
    int numberOfAlgorithmSettingEnums = com.axi.util.Enum.getNumEnums(AlgorithmSettingEnum.class);
    Assert.expect(_algorithmSettingEnumToStringMap.size() == numberOfAlgorithmSettingEnums, "AlgorithmSettingEnum missing from EnumStringLookup");

    // WARNING - DO NOT CHANGE THESE STRINGS - CHANGING THEM WILL CAUSE ALL PROJECTS
    //           ON CUSTOMER SITES TO BE NON-LOADABLE!!
    // WARNING - IF YOU REMOVE ONE OF THESE ENUMS THE PROJECT READERS MUST BE
    //           MODIFIED TO SUPPORT READING OF PRE-EXISTING PROJECTS! - see Bill Darbie for details
    mapAlgorithmSettingTypeEnumToString(AlgorithmSettingTypeEnum.STANDARD, "standard");
    mapAlgorithmSettingTypeEnumToString(AlgorithmSettingTypeEnum.ADDITIONAL, "additional");
    mapAlgorithmSettingTypeEnumToString(AlgorithmSettingTypeEnum.HIDDEN, "internal");
    mapAlgorithmSettingTypeEnumToString(AlgorithmSettingTypeEnum.SLICE_HEIGHT, "sliceheight");
    mapAlgorithmSettingTypeEnumToString(AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL, "sliceheightadditional");

    // Ensure that there is a map entry for each AlgorithmSettingTypeEnum.
    int numberOfAlgorithmSettingTypeEnums = com.axi.util.Enum.getNumEnums(AlgorithmSettingTypeEnum.class);
    Assert.expect(_algorithmSettingTypeEnumToStringMap.size() == numberOfAlgorithmSettingTypeEnums, "AlgorithmSettingTypeEnum missing from EnumStringLookup");


    // WARNING - DO NOT CHANGE THESE STRINGS - CHANGING THEM WILL CAUSE ALL PROJECTS
    //           ON CUSTOMER SITES TO BE NON-LOADABLE!!
    // WARNING - IF YOU REMOVE ONE OF THESE ENUMS THE PROJECT READERS MUST BE
    //           MODIFIED TO SUPPORT READING OF PRE-EXISTING PROJECTS! - see Bill Darbie for details

    mapConditionKeyEnumToString(ConditionKeyEnum.ANY_SERIAL_NUMBER, "anySerialNumber");
    mapConditionKeyEnumToString(ConditionKeyEnum.USER_NAME, "userName");
    mapConditionKeyEnumToString(ConditionKeyEnum.USER_ACCOUNT_TYPE, "userAccountType");
    mapConditionKeyEnumToString(ConditionKeyEnum.PROGRAMMER_NAME, "programmerName");
    mapConditionKeyEnumToString(ConditionKeyEnum.PROJECT_NAME, "projectName");
    mapConditionKeyEnumToString(ConditionKeyEnum.TARGET_CUSTOMER, "targetCustomer");

    // Ensure that there is a map entry for each ConditionKeyEnum.
    int numberOfConditionKeyEnums = com.axi.util.Enum.getNumEnums(ConditionKeyEnum.class);
    Assert.expect(_conditionKeyEnumToStringMap.size() == numberOfConditionKeyEnums, "ConditionKeyEnum missing from EnumStringLookup");

    // WARNING - DO NOT CHANGE THESE STRINGS - CHANGING THEM WILL CAUSE ALL PROJECTS
    //           ON CUSTOMER SITES TO BE NON-LOADABLE!!
    // WARNING - IF YOU REMOVE ONE OF THESE ENUMS THE PROJECT READERS MUST BE
    //           MODIFIED TO SUPPORT READING OF PRE-EXISTING PROJECTS! - see Bill Darbie for details

    mapResultFileTypeEnumToString(ResultFileTypeEnum.DEFECT_IMAGE_FILES, "defectImages");
    mapResultFileTypeEnumToString(ResultFileTypeEnum.XML_CAD_FILE, "CADXML");
    mapResultFileTypeEnumToString(ResultFileTypeEnum.XML_SETTINGS_FILE, "settingsXML");
    mapResultFileTypeEnumToString(ResultFileTypeEnum.XML_INDICTMENTS_FILE, "indictmentsXML");
    mapResultFileTypeEnumToString(ResultFileTypeEnum.XML_INDICTMENTS_FILE_FOR_MRT, "indictmentsXMLForMRT");
    mapResultFileTypeEnumToString(ResultFileTypeEnum.INTERNAL_RESULTS_BINARY_FILE, "measurementsBinary");
    mapResultFileTypeEnumToString(ResultFileTypeEnum.INTERNAL_RESULTS_IMAGE_SET_FILES, "resultImageSetForProductionTuning");
    mapResultFileTypeEnumToString(ResultFileTypeEnum.XML_MEASUREMENTS_FILE, "measurementsXML");

    // Ensure that there is a map entry for each ResultFileTypeEnum.
    int numberOfResultFileTypeEnums = com.axi.util.Enum.getNumEnums(ResultFileTypeEnum.class);
    Assert.expect(_resultFileTypeEnumToStringMap.size() == numberOfResultFileTypeEnums, "ResultFileTypeEnum missing from EnumStringLookup");

    // WARNING - DO NOT CHANGE THESE STRINGS - CHANGING THEM WILL CAUSE ALL PROJECTS
    //           ON CUSTOMER SITES TO BE NON-LOADABLE!!
    // WARNING - IF YOU REMOVE ONE OF THESE ENUMS THE PROJECT READERS MUST BE
    //           MODIFIED TO SUPPORT READING OF PRE-EXISTING PROJECTS! - see Bill Darbie for details

    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.NONE, "");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.MILS, "mils");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.NANOMETERS, "nanometers");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.MILLIMETERS, "millimeters");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PIXELS, "pixels");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT, "percent");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ACROSS, "percent of pad length across");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG, "percent of pad length along");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_HEEL_TOE_PEAK_DISTANCE, "percent of heel-toe peak distance");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_HEEL_TOE_EDGE_DISTANCE, "percent of heel-toe edge distance");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_PIN_LENGTH, "percent of pin length");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_FILLET_LENGTH, "percent of fillet length");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_NOMINAL_TOE_THICKNESS, "percent of nominal toe thickness");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_SIDE_FILLET_LENGTH, "percent of distance between side edges");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE, "percent of interpad distance");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_MAXIMUM_PAD_THICKNESS_ALONG, "percent of maximum pad thickness along");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_MAXIMUM_PAD_THICKNESS_ACROSS, "percent of maximum pad thickness across");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_COMPONENT_BODY_LENGTH, "percent of component body length");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_HEEL_THICKNESS, "percent of heel thickness");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.GRAYLEVEL, "graylevel");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_NOMINAL_GRAYLEVEL, "percent of nominal graylevel");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_AVERAGE_CORRECTED_GRAYLEVEL, "percent of average corrected graylevel");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED, "percent of area voided");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_DIAMETER_VOIDED, "percent of diameter voided");//Siew Yeng - XCR-3515
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM, "percentage through hole, from bottom");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.INTER_QUARTILE_RANGE, "Inter Quartile Range");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_PITCH, "percent of pitch");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.LOWER_TO_UPPER_FILLET_THICKNESS_RATIO, "lower to upper fillet thickness ratio");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_NOMINAL_PAD_ONE_THICKNESS, "percent of nominal pad one thickness");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.DEGREES, "degrees");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_NOMINAL_CENTER_THICKNESS, "percent of nominal center thickness");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_NIMINAL_FILLET_LENGTH, "percent of nominal fillet length");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_PAD_WIDTH_ALONG, "percent of pad width along");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.PERCENT_OF_PAD_WIDTH_ACROSS, "percent of pad width across");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.SQUARE_MILLIMETERS, "square millimeters");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.CUBIC_MILLIMETERS, "cubic millimeters");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.SQUARE_MILS, "square mils");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.CUBIC_MILS, "cubic mils");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.DEFINITE_VOIDING_GREY_LEVEL_VOIDED, "percent voided");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.EXONERATED_VOIDING_GREY_LEVEL_VOIDED, "voided value");
    mapMeasurementUnitsEnumToString(MeasurementUnitsEnum.VOID_DIFFERENCE_FROM_NOMINAL_GREY_LEVEL, "difference");

    // Ensure that there is a map entry for each MeasurementUnitsEnum.
    int numberOfMeasurementUnitsEnums = com.axi.util.Enum.getNumEnums(MeasurementUnitsEnum.class);
    Assert.expect(_measurementUnitsEnumToStringMap.size() == numberOfMeasurementUnitsEnums, "MeasurementUnitsEnum missing from EnumStringLookup");

    // WARNING - DO NOT CHANGE THESE STRINGS - CHANGING THEM WILL CAUSE ALL PROJECTS
    //           ON CUSTOMER SITES TO BE NON-LOADABLE!!
    // WARNING - IF YOU REMOVE ONE OF THESE ENUMS THE PROJECT READERS MUST BE
    //           MODIFIED TO SUPPORT READING OF PRE-EXISTING PROJECTS! - see Bill Darbie for details
    mapSignalCompensationEnumToString(SignalCompensationEnum.DEFAULT_LOW, "defaultLow");
    mapSignalCompensationEnumToString(SignalCompensationEnum.MEDIUM, "medium");
    mapSignalCompensationEnumToString(SignalCompensationEnum.MEDIUM_HIGH, "mediumHigh");
    mapSignalCompensationEnumToString(SignalCompensationEnum.HIGH, "high");
    mapSignalCompensationEnumToString(SignalCompensationEnum.SUPER_HIGH, "superHigh");
    
    // XCR1529, New Scan Route, khang-wah.chnee
    mapSignalCompensationEnumToString(SignalCompensationEnum.IL_1_5, "1.5");
    mapSignalCompensationEnumToString(SignalCompensationEnum.IL_5, "5");
    mapSignalCompensationEnumToString(SignalCompensationEnum.IL_6, "6");
    mapSignalCompensationEnumToString(SignalCompensationEnum.IL_7, "7");

    // Ensure that there is a map entry for each MeasurementUnitsEnum.
    int numberOfSignalCompensationEnums = com.axi.util.Enum.getNumEnums(SignalCompensationEnum.class);
    Assert.expect(_signalCompensationEnumToStringMap.size() == numberOfSignalCompensationEnums, "SignalCompensationEnum missing from EnumStringLookup");

    // WARNING - DO NOT CHANGE THESE STRINGS - CHANGING THEM WILL CAUSE ALL PROJECTS
    //           ON CUSTOMER SITES TO BE NON-LOADABLE!!
    // WARNING - IF YOU REMOVE ONE OF THESE ENUMS THE PROJECT READERS MUST BE
    //           MODIFIED TO SUPPORT READING OF PRE-EXISTING PROJECTS! - see Bill Darbie for details
    mapGlobalSurfaceModelEnumToString(GlobalSurfaceModelEnum.AUTOFOCUS, "autoFocus");
    mapGlobalSurfaceModelEnumToString(GlobalSurfaceModelEnum.GLOBAL_SURFACE_MODEL, "GSM");
    mapGlobalSurfaceModelEnumToString(GlobalSurfaceModelEnum.AUTOSELECT, "autoSelect");
    // Wei Chin (RFB)
    mapGlobalSurfaceModelEnumToString(GlobalSurfaceModelEnum.USE_RFB, "RFB");
    mapGlobalSurfaceModelEnumToString(GlobalSurfaceModelEnum.PSP, "surfaceMap");


    int numberOfGlobalSurfaceModelEnums = com.axi.util.Enum.getNumEnums(GlobalSurfaceModelEnum.class);
    Assert.expect(_globalSurfaceModelEnumToStringMap.size() == numberOfGlobalSurfaceModelEnums, "GlobalSurfaceModelEnum missing from EnumStringLookup");

    // WARNING - DO NOT CHANGE THESE STRINGS - CHANGING THEM WILL CAUSE ALL PROJECTS
    //           ON CUSTOMER SITES TO BE NON-LOADABLE!!
    // WARNING - IF YOU REMOVE ONE OF THESE ENUMS THE PROJECT READERS MUST BE
    //           MODIFIED TO SUPPORT READING OF PRE-EXISTING PROJECTS! - see Bill Darbie for details
    mapUserGainEnumToString(UserGainEnum.ONE, "1.0");
    mapUserGainEnumToString(UserGainEnum.TWO, "2.0");
    mapUserGainEnumToString(UserGainEnum.THREE, "3.0");
    mapUserGainEnumToString(UserGainEnum.FOUR, "4.0");
    mapUserGainEnumToString(UserGainEnum.FIVE, "5.0");
    mapUserGainEnumToString(UserGainEnum.SIX, "6.0");
    mapUserGainEnumToString(UserGainEnum.SEVEN, "7.0");
    mapUserGainEnumToString(UserGainEnum.EIGHT, "8.0");
    mapUserGainEnumToString(UserGainEnum.NINE, "9.0");
    
    mapStageSpeedEnumToString(StageSpeedEnum.ONE, "1.0");
    mapStageSpeedEnumToString(StageSpeedEnum.TWO, "1.5");
    mapStageSpeedEnumToString(StageSpeedEnum.THREE, "2.0");
    mapStageSpeedEnumToString(StageSpeedEnum.FOUR, "2.5");
    mapStageSpeedEnumToString(StageSpeedEnum.FIVE, "3.0");
    mapStageSpeedEnumToString(StageSpeedEnum.SIX, "3.5");
    mapStageSpeedEnumToString(StageSpeedEnum.SEVEN, "4.0");
    mapStageSpeedEnumToString(StageSpeedEnum.EIGHT, "4.5");
    mapStageSpeedEnumToString(StageSpeedEnum.NINE, "5.0");
    mapStageSpeedEnumToString(StageSpeedEnum.TEN, "5.5");
    mapStageSpeedEnumToString(StageSpeedEnum.ELEVEN, "6.0");
    mapStageSpeedEnumToString(StageSpeedEnum.TWELVE, "6.5");
    mapStageSpeedEnumToString(StageSpeedEnum.THIRTEEN, "7.0");
    mapStageSpeedEnumToString(StageSpeedEnum.FOURTEEN, "7.5");
    mapStageSpeedEnumToString(StageSpeedEnum.FIFTEEN, "8.0");
    mapStageSpeedEnumToString(StageSpeedEnum.SIXTEEN, "8.5");
    mapStageSpeedEnumToString(StageSpeedEnum.SEVENTEEN, "9.0");

    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    mapLayerIdEnumToString(POPLayerIdEnum.BASE, "0");
    mapLayerIdEnumToString(POPLayerIdEnum.ONE, "1");
    mapLayerIdEnumToString(POPLayerIdEnum.TWO, "2");
    mapLayerIdEnumToString(POPLayerIdEnum.THREE, "3");
    mapLayerIdEnumToString(POPLayerIdEnum.FOUR, "4");
    mapLayerIdEnumToString(POPLayerIdEnum.FIVE, "5");

    mapPspEnumToString(PspEnum.YES_PSP, "Yes");
    mapPspEnumToString(PspEnum.NO_PSP, "No");

    int numberOfPspEnums = com.axi.util.Enum.getNumEnums(PspEnum.class);
    Assert.expect(_pspEnumToStringMap.size() == numberOfPspEnums, "PspEnum missing from EnumStringLookup");

    // WARNING - DO NOT CHANGE THESE STRINGS - CHANGING THEM WILL CAUSE ALL PROJECTS
    //           ON CUSTOMER SITES TO BE NON-LOADABLE!!
    // WARNING - IF YOU REMOVE ONE OF THESE ENUMS THE PROJECT READERS MUST BE
    //           MODIFIED TO SUPPORT READING OF PRE-EXISTING PROJECTS! - see Bill Darbie for details
    mapMagnificationTypeEnumToString(MagnificationTypeEnum.LOW, "low");
    mapMagnificationTypeEnumToString(MagnificationTypeEnum.HIGH, "high");

    int numberOfMagnificationTypeEnums = com.axi.util.Enum.getNumEnums(MagnificationTypeEnum.class);
    Assert.expect(_magnificationTypeEnumToStringMap.size() == numberOfMagnificationTypeEnums, "MagnificationTypeEnum missing from EnumStringLookup");

    //Siew Yeng - XCR_2140 - DRO
    mapDynamicRangeOptimizationLevelEnumToString(DynamicRangeOptimizationLevelEnum.ONE, "1.0");
    mapDynamicRangeOptimizationLevelEnumToString(DynamicRangeOptimizationLevelEnum.TWO, "2.0");
    mapDynamicRangeOptimizationLevelEnumToString(DynamicRangeOptimizationLevelEnum.THREE, "3.0");
    mapDynamicRangeOptimizationLevelEnumToString(DynamicRangeOptimizationLevelEnum.FOUR, "4.0");
    mapDynamicRangeOptimizationLevelEnumToString(DynamicRangeOptimizationLevelEnum.FIVE, "5.0");
    mapDynamicRangeOptimizationLevelEnumToString(DynamicRangeOptimizationLevelEnum.SIX, "6.0");
    mapDynamicRangeOptimizationLevelEnumToString(DynamicRangeOptimizationLevelEnum.SEVEN, "7.0");
    mapDynamicRangeOptimizationLevelEnumToString(DynamicRangeOptimizationLevelEnum.EIGHT, "8.0");
    mapDynamicRangeOptimizationLevelEnumToString(DynamicRangeOptimizationLevelEnum.NINE, "9.0");
    mapDynamicRangeOptimizationLevelEnumToString(DynamicRangeOptimizationLevelEnum.TEN, "10.0");
    
    int numberOfDynamicRangeOptimizationLevelEnums = com.axi.util.Enum.getNumEnums(DynamicRangeOptimizationLevelEnum.class);
    Assert.expect(_dynamicRangeOptimizationLevelEnumToStringMap.size() == numberOfDynamicRangeOptimizationLevelEnums, "dynamicRangeOptimizationLevelEnum missing from EnumStringLookup");
    
    buildStringToEnumMaps();
  }

  /**
   * @author Bill Darbie
   */
  private void buildStringToEnumMaps()
  {
    for (Map.Entry<PinOrientationEnum, String> entry : _pinOrientationEnumToStringMap.entrySet())
    {
      PinOrientationEnum prev = _stringToPadOrientationEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }

    for (Map.Entry<MathUtilEnum, String> entry : _mathUtilEnumToStringMap.entrySet())
    {
      MathUtilEnum prev = _stringToMathUtilEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }

    for (Map.Entry<InspectionFamilyEnum, String> entry : _inspectionFamilyEnumToStringMap.entrySet())
    {
      InspectionFamilyEnum prev = _stringToInspectionFamilyEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }

    for (Map.Entry<JointTypeEnum, String> entry : _jointTypeEnumToStringMap.entrySet())
    {
      JointTypeEnum prev = _stringToJointTypeEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }

    for (Map.Entry<AlgorithmEnum, String> entry : _algorithmEnumToStringMap.entrySet())
    {
      AlgorithmEnum prev = _stringToAlgorithmEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }

    for (Map.Entry<AlgorithmSettingEnum, String> entry : _algorithmSettingEnumToStringMap.entrySet())
    {
      AlgorithmSettingEnum prev = _stringToAlgorithmSettingEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }

    for (Map.Entry<AlgorithmSettingTypeEnum, String> entry : _algorithmSettingTypeEnumToStringMap.entrySet())
    {
      AlgorithmSettingTypeEnum prev = _stringToAlgorithmSettingTypeEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }

    for (Map.Entry<ConditionKeyEnum, String> entry : _conditionKeyEnumToStringMap.entrySet())
    {
      ConditionKeyEnum prev = _stringToConditionKeyEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }

    for (Map.Entry<ResultFileTypeEnum, String> entry : _resultFileTypeEnumToStringMap.entrySet())
    {
      ResultFileTypeEnum prev = _stringToResultFileTypeEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }

    for (Map.Entry<MeasurementUnitsEnum, String> entry : _measurementUnitsEnumToStringMap.entrySet())
    {
      MeasurementUnitsEnum prev = _stringToMeasurementUnitsEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }

    for (Map.Entry<SignalCompensationEnum, String> entry : _signalCompensationEnumToStringMap.entrySet())
    {
      SignalCompensationEnum prev = _stringToSignalCompensationEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }

    for (Map.Entry<GlobalSurfaceModelEnum, String> entry : _globalSurfaceModelEnumToStringMap.entrySet())
    {
      GlobalSurfaceModelEnum prev = _stringToGlobalSurfaceModelEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }

    for (Map.Entry<UserGainEnum, String> entry : _userGainEnumToStringMap.entrySet())
    {
      UserGainEnum prev = _userGainEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }
    
    for (Map.Entry<StageSpeedEnum, String> entry : _stageSpeedEnumToStringMap.entrySet())
    {
      StageSpeedEnum prev = _stageSpeedEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }

    for (Map.Entry<PspEnum, String> entry : _pspEnumToStringMap.entrySet())
    {
      PspEnum prev = _pspEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }

    for (Map.Entry<MagnificationTypeEnum, String> entry : _magnificationTypeEnumToStringMap.entrySet())
    {
      MagnificationTypeEnum prev = _magnificationTypeEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }
    
    //Siew Yeng - XCR_2140 - DRO
    for (Map.Entry<DynamicRangeOptimizationLevelEnum, String> entry : _dynamicRangeOptimizationLevelEnumToStringMap.entrySet())
    {
      DynamicRangeOptimizationLevelEnum prev = _dynamicRangeOptimizationLevelEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }
    
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    for (Map.Entry<POPLayerIdEnum, String> entry : _popLayerIdEnumToStringMap.entrySet())
    {
      POPLayerIdEnum prev = _popLayerIdEnumMap.put(entry.getValue(), entry.getKey());
      Assert.expect(prev == null);
    }
  }

  /**
   * @author Bill Darbie
   */
  public PinOrientationEnum getPadOrientationEnum(String string)
  {
    Assert.expect(string != null);
    PinOrientationEnum theEnum = _stringToPadOrientationEnumMap.get(string);
    Assert.expect(theEnum != null);
    return theEnum;
  }

  /**
   * @author Bill Darbie
   */
  public String getPadOrientationString(PinOrientationEnum theEnum)
  {
    Assert.expect(theEnum != null);
    String string = _pinOrientationEnumToStringMap.get(theEnum);
    Assert.expect(string != null);
    return string;
  }

  /**
   * @author Bill Darbie
   */
  public MathUtilEnum getMathUtilEnum(String string)
  {
    Assert.expect(string != null);
    MathUtilEnum theEnum = _stringToMathUtilEnumMap.get(string);
    Assert.expect(theEnum != null);
    return theEnum;
  }

  /**
   * @author Bill Darbie
   */
  public String getMathUtilString(MathUtilEnum theEnum)
  {
    Assert.expect(theEnum != null);
    String string = _mathUtilEnumToStringMap.get(theEnum);
    Assert.expect(string != null);
    return string;
  }

  /**
   * @author Bill Darbie
   */
  public InspectionFamilyEnum getInspectionFamilyEnum(String string)
  {
    Assert.expect(string != null);
    InspectionFamilyEnum theEnum = _stringToInspectionFamilyEnumMap.get(string);
    Assert.expect(theEnum != null);
    return theEnum;
  }

  /**
   * @author Bill Darbie
   */
  public String getInspectionFamilyString(InspectionFamilyEnum theEnum)
  {
    Assert.expect(theEnum != null);
    String string = _inspectionFamilyEnumToStringMap.get(theEnum);
    Assert.expect(string != null);
    return string;
  }

  /**
   * @author Bill Darbie
   */
  public JointTypeEnum getJointTypeEnum(String string)
  {
    Assert.expect(string != null);
    JointTypeEnum theEnum = _stringToJointTypeEnumMap.get(string);
    Assert.expect(theEnum != null);
    return theEnum;
  }

  /**
   * @author Bill Darbie
   */
  public String getJointTypeString(JointTypeEnum theEnum)
  {
    Assert.expect(theEnum != null);
    String string = _jointTypeEnumToStringMap.get(theEnum);
    Assert.expect(string != null);
    return string;
  }

  /**
   * @author Bill Darbie
   */
  public AlgorithmEnum getAlgorithmEnum(String string)
  {
    Assert.expect(string != null);
    AlgorithmEnum theEnum = _stringToAlgorithmEnumMap.get(string);
    Assert.expect(theEnum != null);
    return theEnum;
  }

  /**
   * @author Bill Darbie
   */
  public String getAlgorithmString(AlgorithmEnum theEnum)
  {
    Assert.expect(theEnum != null);
    String string = _algorithmEnumToStringMap.get(theEnum);
    Assert.expect(string != null);
    return string;
  }

  /**
   * @author Bill Darbie
   */
  public AlgorithmSettingEnum getAlgorithmSettingEnum(String string)
  {
    Assert.expect(string != null);
    AlgorithmSettingEnum theEnum = _stringToAlgorithmSettingEnumMap.get(string);
    Assert.expect(theEnum != null, string);
    return theEnum;
  }

  /**
   * @author Bill Darbie
   */
  public String getAlgorithmSettingString(AlgorithmSettingEnum theEnum)
  {
    Assert.expect(theEnum != null);
    String string = _algorithmSettingEnumToStringMap.get(theEnum);
    Assert.expect(string != null);
    return string;
  }

   /**
   * @author Laura Cormos
   */
  public AlgorithmSettingTypeEnum getAlgorithmSettingTypeEnum(String string)
  {
    Assert.expect(string != null);
    AlgorithmSettingTypeEnum theEnum = _stringToAlgorithmSettingTypeEnumMap.get(string);
    Assert.expect(theEnum != null);
    return theEnum;
  }

  /**
   * @author Laura Cormos
   */
  public String getAlgorithmSettingTypeString(AlgorithmSettingTypeEnum theEnum)
  {
    Assert.expect(theEnum != null);
    String string = _algorithmSettingTypeEnumToStringMap.get(theEnum);
    Assert.expect(string != null);
    return string;
  }

/**
 * @author Patrick Lacz
 */
  public ConditionKeyEnum getConditionKeyEnum(String string)
  {
    Assert.expect(string != null);
    ConditionKeyEnum theEnum = _stringToConditionKeyEnumMap.get(string);
    Assert.expect(theEnum != null);
    return theEnum;
  }


  /**
   * @author Patrick Lacz
 */
  public String getConditionKeyString(ConditionKeyEnum theEnum)
  {
    Assert.expect(theEnum != null);
    String string = _conditionKeyEnumToStringMap.get(theEnum);
    Assert.expect(string != null);
    return string;
  }


  /**
   * @author Patrick Lacz
 */
  public ResultFileTypeEnum getResultFileTypeEnum(String string)
  {
    Assert.expect(string != null);
    ResultFileTypeEnum theEnum = _stringToResultFileTypeEnumMap.get(string);
    Assert.expect(theEnum != null);
    return theEnum;
  }


  /**
   * @author Patrick Lacz
 */
  public String getResultFileTypeString(ResultFileTypeEnum theEnum)
  {
    Assert.expect(theEnum != null);
    String string = _resultFileTypeEnumToStringMap.get(theEnum);
    Assert.expect(string != null);
    return string;
  }

  /**
   * @author Laura Cormos
   */
  public String getMeasurementUnitsEnumString(MeasurementUnitsEnum theEnum)
  {
    Assert.expect(theEnum != null);
    String string = _measurementUnitsEnumToStringMap.get(theEnum);
    Assert.expect(string != null);
    return string;
  }

  /**
   * @author Laura Cormos
   */
  public MeasurementUnitsEnum getMeasurementUnitsEnum(String string)
  {
    Assert.expect(string != null);
    MeasurementUnitsEnum theEnum = _stringToMeasurementUnitsEnumMap.get(string);
    Assert.expect(theEnum != null);
    return theEnum;
  }

  /**
   * @author George A. David
   */
  public String getSignalCompensationEnumString(SignalCompensationEnum signalCompensation)
  {
    Assert.expect(signalCompensation != null);
    String string = _signalCompensationEnumToStringMap.get(signalCompensation);
    Assert.expect(string != null);
    return string;
  }

  /**
   * @author Cheah, Lee Herng
   */
  public String getGlobalSurfaceModelEnumString(GlobalSurfaceModelEnum globalSurfaceModel)
  {
    Assert.expect(globalSurfaceModel != null);
    String string = _globalSurfaceModelEnumToStringMap.get(globalSurfaceModel);
    Assert.expect(string != null);
    return string;
  }

  /**
   * @author George A. David
   */
  public SignalCompensationEnum getSignalCompensationEnum(String string)
  {
    Assert.expect(string != null);
    SignalCompensationEnum signalCompensation = _stringToSignalCompensationEnumMap.get(string);
    Assert.expect(signalCompensation != null);

    return signalCompensation;
  }

  /**
   * @author Cheah, Lee Herng
   */
  public GlobalSurfaceModelEnum getGlobalSurfaceModelEnum(String string)
  {
    Assert.expect(string != null);
    GlobalSurfaceModelEnum globalSurfaceModel = _stringToGlobalSurfaceModelEnumMap.get(string);
    Assert.expect(globalSurfaceModel != null);

    return globalSurfaceModel;
  }

  /**
   * Maps the specified PinOrientationEnum to the specified String.  Ensures (asserts) no attempts are made to insert
   * duplicate PinOrientationEnums.
   *
   * @author Matt Wharton
   */
  private void mapPinOrientationEnumToString(PinOrientationEnum pinOrientationEnum, String string)
  {
    Assert.expect(pinOrientationEnum != null);
    Assert.expect(string != null);
    Assert.expect(_pinOrientationEnumToStringMap != null);

    String previousEntry = _pinOrientationEnumToStringMap.put(pinOrientationEnum, string);
    Assert.expect(previousEntry == null);
  }

  /**
   * Maps the specified MathUtilEnum to the specified String.  Ensures (asserts) no attempts are made to insert
   * duplicate MathUtilEnums.
   *
   * @author Matt Wharton
   */
  private void mapMathUtilEnumToString(MathUtilEnum mathUtilEnum, String string)
  {
    Assert.expect(mathUtilEnum != null);
    Assert.expect(string != null);
    Assert.expect(_mathUtilEnumToStringMap != null);

    String previousEntry = _mathUtilEnumToStringMap.put(mathUtilEnum, string);
    Assert.expect(previousEntry == null);
  }

  /**
   * Maps the specified InspectionFamilyEnum to the specified String.  Ensures (asserts) no attempts are made to insert
   * duplicate InspectionFamilyEnums.
   *
   * @author Matt Wharton
   */
  private void mapInspectionFamilyEnumToString(InspectionFamilyEnum inspectionFamilyEnum, String string)
  {
    Assert.expect(inspectionFamilyEnum != null);
    Assert.expect(string != null);
    Assert.expect(_inspectionFamilyEnumToStringMap != null);

    String previousEntry = _inspectionFamilyEnumToStringMap.put(inspectionFamilyEnum, string);
    Assert.expect(previousEntry == null);
  }

  /**
   * Maps the specified JointTypeEnum to the specified String.  Ensures (asserts) no attempts are made to insert
   * duplicate JointTypeEnums.
   *
   * @author Matt Wharton
   */
  private void mapJointTypeEnumToString(JointTypeEnum jointTypeEnum, String string)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(string != null);
    Assert.expect(_jointTypeEnumToStringMap != null);

    String previousEntry = _jointTypeEnumToStringMap.put(jointTypeEnum, string);
    Assert.expect(previousEntry == null);
  }

  /**
   * Maps the specified AlgorithmEnum to the specified String.  Ensures (asserts) no attempts are made to insert
   * duplicate AlgorithmEnums.
   *
   * @author Matt Wharton
   */
  private void mapAlgorithmEnumToString(AlgorithmEnum algorithmEnum, String string)
  {
    Assert.expect(algorithmEnum != null);
    Assert.expect(string != null);
    Assert.expect(_algorithmEnumToStringMap != null);

    String previousEntry = _algorithmEnumToStringMap.put(algorithmEnum, string);
    Assert.expect(previousEntry == null);
  }

  /**
   * Maps the specified AlgorithmSettingEnum to the specified String.  Ensures (asserts) no attempts are made to insert
   * duplicate AlgorithmSettingEnums.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  private void mapAlgorithmSettingEnumToString(AlgorithmSettingEnum algorithmSettingEnum, String string)
  {
    Assert.expect(algorithmSettingEnum != null);
    Assert.expect(string != null);
    Assert.expect(_algorithmSettingEnumToStringMap != null);

    // make sure this isn't trying to use a String that has been obsoleted
    Assert.expect(_oldAlgorithmSettingEnumStrings.contains(string) == false);

    // add the string to the map, checking to make sure it hasn't been added already
    String previousEntry = _algorithmSettingEnumToStringMap.put(algorithmSettingEnum, string);
    Assert.expect(previousEntry == null);
  }

  /**
   * @author Peter Esbensen
   */
  public boolean algorithmSettingEnumStringIsObsolete(String string)
  {
    Assert.expect(string != null);
    Assert.expect(_oldAlgorithmSettingEnumStrings != null);

    return _oldAlgorithmSettingEnumStrings.contains(string);
  }

  /**
   * Maps the specified AlgorithmSettingTypeEnum to the specified String.  Ensures (asserts) no attempts are made to insert
   * duplicate AlgorithmSettingTypeEnums. This mapping is used by the settings XML writer.
   *
   * @author Laura Cormos
   */
  private void mapAlgorithmSettingTypeEnumToString(AlgorithmSettingTypeEnum algorithmSettingTypeEnum, String string)
  {
    Assert.expect(algorithmSettingTypeEnum != null);
    Assert.expect(string != null);
    Assert.expect(_algorithmSettingTypeEnumToStringMap != null);

    String previousEntry = _algorithmSettingTypeEnumToStringMap.put(algorithmSettingTypeEnum, string);
    Assert.expect(previousEntry == null);
  }

  /**
   * Maps the specified ConditionKeyEnum to the specified String.  Ensures (asserts) no attempts are made to insert
   * duplicate ConditionKeyEnums.
   *
   * @author Patrick Lacz
   */
  private void mapConditionKeyEnumToString(ConditionKeyEnum theEnum, String string)
  {
    Assert.expect(theEnum != null);
    Assert.expect(string != null);
    Assert.expect(_conditionKeyEnumToStringMap != null);

    String previousEntry = _conditionKeyEnumToStringMap.put(theEnum, string);
    Assert.expect(previousEntry == null);
  }

  /**
   * Maps the specified ResultFileTypeEnum to the specified String.  Ensures (asserts) no attempts are made to insert
   * duplicate ResultFileTypeEnums.
   *
   * @author Patrick Lacz
   */
  private void mapResultFileTypeEnumToString(ResultFileTypeEnum resultFileTypeEnum, String string)
  {
    Assert.expect(resultFileTypeEnum != null);
    Assert.expect(string != null);
    Assert.expect(_resultFileTypeEnumToStringMap != null);

    String previousEntry = _resultFileTypeEnumToStringMap.put(resultFileTypeEnum, string);
    Assert.expect(previousEntry == null);
  }

  /**
   * Maps the specified MeasurementUnitsEnum to the specified String.  Ensures (asserts) no attempts are made to insert
   * duplicate MeasurementUnitsEnums.
   * @author Laura Cormos
   */
  private void mapMeasurementUnitsEnumToString(MeasurementUnitsEnum measurementUnitsEnum, String string)
  {
    Assert.expect(measurementUnitsEnum != null);
    Assert.expect(string != null);
    Assert.expect(_measurementUnitsEnumToStringMap != null);

    String previousEntry = _measurementUnitsEnumToStringMap.put(measurementUnitsEnum, string);
    Assert.expect(previousEntry == null);
  }

  /**
   * Maps the specified SignalCompensationEnum to the specified String.
   * Ensures (asserts) no attempts are made to insert duplicate SignalCompensationEnum
   * @author George A. David
   */
  private void mapSignalCompensationEnumToString(SignalCompensationEnum signalCompensationEnum, String string)
  {
    Assert.expect(signalCompensationEnum != null);
    Assert.expect(string != null);
    Assert.expect(_signalCompensationEnumToStringMap != null);

    String previousEntry = _signalCompensationEnumToStringMap.put(signalCompensationEnum, string);
    Assert.expect(previousEntry == null);
  }

  /**
   * @author Cheah, Lee Herng
   */
  private void mapGlobalSurfaceModelEnumToString(GlobalSurfaceModelEnum globalSurfaceModelEnum, String string)
  {
    Assert.expect(globalSurfaceModelEnum != null);
    Assert.expect(string != null);
    Assert.expect(_globalSurfaceModelEnumToStringMap != null);

    String previousEntry = _globalSurfaceModelEnumToStringMap.put(globalSurfaceModelEnum, string);
    Assert.expect(previousEntry == null);
  }

  /**
   * @author Cheah, Lee Herng
   */
  public boolean isAlgorithmSettingEnumExist(String string)
  {
      boolean istAlgorithmSettingEnumExist = false;

      AlgorithmSettingEnum theEnum = _stringToAlgorithmSettingEnumMap.get(string);
      if (theEnum == null)
          istAlgorithmSettingEnumExist = false;
      else
          istAlgorithmSettingEnumExist = true;

      return istAlgorithmSettingEnumExist;
  }

  /**
   * @author sham
   */
  private void mapUserGainEnumToString(UserGainEnum userGainEnum, String string)
  {
    Assert.expect(userGainEnum != null);
    Assert.expect(string != null);
    Assert.expect(_userGainEnumToStringMap != null);

    String previousEntry = _userGainEnumToStringMap.put(userGainEnum, string);
    Assert.expect(previousEntry == null);
  }

  /**
   * @author sham
   */
  public UserGainEnum getUserGainEnum(String string)
  {
    Assert.expect(string != null);
    UserGainEnum userGain = _userGainEnumMap.get(string);
    Assert.expect(userGain != null);

    return userGain;
  }

  /**
   * @author Sham
   */
  public String getUserGainEnumString(UserGainEnum userGain)
  {
    Assert.expect(userGain != null);
    String string = _userGainEnumToStringMap.get(userGain);
    Assert.expect(string != null);
    return string;
  }
  
  /**
   * @author sheng chuan
   */
  public StageSpeedEnum getstageSpeedEnum(String string)
  {
    Assert.expect(string != null);
    StageSpeedEnum stageSpeed = _stageSpeedEnumMap.get(string);
    Assert.expect(stageSpeed != null);

    return stageSpeed;
  }

  /**
   * @author sheng chuan
   */
  public String getstageSpeedEnumString(StageSpeedEnum stageSpeed)
  {
    Assert.expect(stageSpeed != null);
    String string = _stageSpeedEnumToStringMap.get(stageSpeed);
    Assert.expect(string != null);
    return string;
  }
  
  /**
   * @author sheng chuan
   */
  private void mapStageSpeedEnumToString(StageSpeedEnum stageSpeedEnum, String string)
  {
    Assert.expect(stageSpeedEnum != null);
    Assert.expect(string != null);
    Assert.expect(_userGainEnumToStringMap != null);

    String previousEntry = _stageSpeedEnumToStringMap.put(stageSpeedEnum, string);
    Assert.expect(previousEntry == null);
  }
  
  /**
   * @author sham
   */
  private void mapPspEnumToString(PspEnum pspEnum, String string)
  {
    Assert.expect(pspEnum != null);
    Assert.expect(string != null);
    Assert.expect(_pspEnumToStringMap != null);

    String previousEntry = _pspEnumToStringMap.put(pspEnum, string);
    Assert.expect(previousEntry == null);
  }

  /**
   * @author sham
   */
  public PspEnum getPspEnum(String string)
  {
    Assert.expect(string != null);
    PspEnum psp = _pspEnumMap.get(string);
    Assert.expect(psp != null);

    return psp;
  }

  /**
   * @author Sham
   */
  public String getPspEnumString(PspEnum psp)
  {
    Assert.expect(psp != null);
    String string = _pspEnumToStringMap.get(psp);
    Assert.expect(string != null);
    return string;
  }

/**
 * author sham
 */
  private void mapMagnificationTypeEnumToString(MagnificationTypeEnum magnificationTypeEnum, String string)
  {
    Assert.expect(magnificationTypeEnum != null);
    Assert.expect(string != null);
    Assert.expect(_magnificationTypeEnumToStringMap != null);

    String previousEntry = _magnificationTypeEnumToStringMap.put(magnificationTypeEnum, string);
    Assert.expect(previousEntry == null);
  }

   /**
   * @author sham
   */
  public MagnificationTypeEnum getMagnificationTypeEnum(String string)
  {
    Assert.expect(string != null);
    MagnificationTypeEnum magnificationType = _magnificationTypeEnumMap.get(string);
    Assert.expect(magnificationType != null);

    return magnificationType;
  }

  /**
   * @author Sham
   */
  public String getMagnificationTypeEnumString(MagnificationTypeEnum magnificationType)
  {
    Assert.expect(magnificationType != null);
    String string = _magnificationTypeEnumToStringMap.get(magnificationType);
    Assert.expect(string != null);
    return string;
  }
  
  /**
   * @author Siew Yeng
   */
  private void mapDynamicRangeOptimizationLevelEnumToString(DynamicRangeOptimizationLevelEnum droLevelEnum, String string)
  {
    Assert.expect(droLevelEnum != null);
    Assert.expect(string != null);
    Assert.expect(_dynamicRangeOptimizationLevelEnumToStringMap != null);

    String previousEntry = _dynamicRangeOptimizationLevelEnumToStringMap.put(droLevelEnum, string);
    Assert.expect(previousEntry == null);
  }

  /**
   * @author Siew Yeng
   */
  public DynamicRangeOptimizationLevelEnum getDynamicRangeOptimizationLevelEnum(String string)
  {
    Assert.expect(string != null);
    DynamicRangeOptimizationLevelEnum droLevelEnum = _dynamicRangeOptimizationLevelEnumMap.get(string);
    Assert.expect(droLevelEnum != null);

    return droLevelEnum;
  }

  /**
   * @author Sham
   */
  public String getDynamicRangeOptimizationLevelEnumString(DynamicRangeOptimizationLevelEnum droLevelEnum)
  {
    Assert.expect(droLevelEnum != null);
    String string = _dynamicRangeOptimizationLevelEnumToStringMap.get(droLevelEnum);
    Assert.expect(string != null);
    return string;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public POPLayerIdEnum getLayerIdEnum(String string)
  {
    Assert.expect(string != null);
    POPLayerIdEnum popLayerId = _popLayerIdEnumMap.get(string);
    Assert.expect(popLayerId != null);

    return popLayerId;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public String getLayerIdEnumString(POPLayerIdEnum popLayerId)
  {
    Assert.expect(popLayerId != null);
    String string = _popLayerIdEnumToStringMap.get(popLayerId);
    Assert.expect(string != null);
    return string;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  private void mapLayerIdEnumToString(POPLayerIdEnum popLayerIdEnum, String string)
  {
    Assert.expect(popLayerIdEnum != null);
    Assert.expect(string != null);
    Assert.expect(_popLayerIdEnumToStringMap != null);

    String previousEntry = _popLayerIdEnumToStringMap.put(popLayerIdEnum, string);
    Assert.expect(previousEntry == null);
  }
}
