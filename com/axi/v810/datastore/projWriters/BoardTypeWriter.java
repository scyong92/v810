package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
class BoardTypeWriter
{
  private static BoardTypeWriter _instance;
  private ProjectWriter _projectWriter;

  /**
   * @author Bill
   */
  static synchronized BoardTypeWriter getInstance()
  {
    if (_instance == null)
      _instance = new BoardTypeWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private BoardTypeWriter()
  {
    // do nothing
  }

  /**
   * @author Bill
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();

    int boardTypeNumber = 0;
    List<BoardType> boardTypes = project.getPanel().getBoardTypes();
    for (BoardType boardType : boardTypes)
    {
      ++boardTypeNumber;
      writeBoardType(project, boardType, boardTypeNumber);
    }
  }

  /**
   * @author Bill Darbie
   */
  private void writeBoardType(Project project, BoardType boardType, int boardTypeNumber) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(boardType != null);

    String projName = project.getName();
    String fileName = FileName.getProjectBoardTypeFullPath(projName, boardTypeNumber, FileName.getProjectBoardTypeLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: boardType_name.X.cad");
      os.println("# PURPOSE: To describe where each component is located for a board type.");
      os.println("#          There will be one copy of this file per board type, not");
      os.println("#          per board instance.");
      os.println("#");
      os.println("# NOTE:");
      os.println("#  - All numbers are in nanometers or degrees.");
      os.println("#  - the xLocation, yLocation numbers indicate the distance from");
      os.println("#    the top side of the boards bottom left corner to the reference point");
      os.println("#    of the components land pattern.");
      os.println("#  - The rotation specifies a rotation around the land pattern reference point");
      os.println("#    which is not necessary the component center.");
      os.println("#    The rotation is counter-clockwise relative");
      os.println("#    to the component top. It can be 0-359.  When looking");
      os.println("#    at the top of a board with x-ray vision, a component on the");
      os.println("#    top will rotate counter-clockwise, a component on the bottom");
      os.println("#    of the board will rotate clockwise.");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");
      os.println("# boardType name");
      _projectWriter.println(os, boardType.getName());
      os.println("# board width in nanometers");
      os.println(boardType.getWidthInNanoMeters());
      os.println("# board length in nanometers");
      os.println(boardType.getLengthInNanoMeters());
      os.println("");
      os.println("# referenceDesignator side1|side2 xLocationInNanometers yLocationInNanoMeters rotationInDegrees landPattern packageLongName");

      SideBoardType sideBoardType = null;
      String side = null;
      for (int i = 0; i < 2; ++i)
      {
        if (i == 0)
        {
          if (boardType.sideBoardType1Exists())
          {
            sideBoardType = boardType.getSideBoardType1();
            side = "side1";
          }
          else
            continue;
        }
        else
        {
          if (boardType.sideBoardType2Exists())
          {
            sideBoardType = boardType.getSideBoardType2();
            side = "side2";
          }
          else
            continue;
        }

        List<ComponentType> componentTypes = sideBoardType.getComponentTypes();
        for (ComponentType componentType : componentTypes)
        {
          String refDes = componentType.getReferenceDesignator();
          BoardCoordinate boardCoordinate = componentType.getCoordinateInNanoMeters();
          int xInNanoMeters = boardCoordinate.getX();
          int yInNanoMeters = boardCoordinate.getY();
          double degreesRotation = componentType.getDegreesRotationRelativeToBoard();
          LandPattern landPattern = componentType.getLandPattern();
          CompPackage compPackage = componentType.getCompPackage();
          String landPatternName = landPattern.getName();
          String compPackageLongName = compPackage.getLongName();

          // referenceDesignator side1|side2 xLocationInNanometers yLocationInNanoMeters rotationInDegrees landPattern packageLongName");
          _projectWriter.println(os, refDes + " " + side + " " + xInNanoMeters + " " + yInNanoMeters + " " +
                     degreesRotation + " " + landPatternName + " " + compPackageLongName);
        }

      }

      os.println("");
      os.println("# fiducial: fiducialName side1|side2 xLocation yLocation width length circle|rectangle");
      for (int i = 0; i < 2; ++i)
      {
        if (i == 0)
        {
          if (boardType.sideBoardType1Exists())
          {
            sideBoardType = boardType.getSideBoardType1();
            side = "side1";
          }
          else
            continue;
        }
        else
        {
          if (boardType.sideBoardType2Exists())
          {
            sideBoardType = boardType.getSideBoardType2();
            side = "side2";
          }
          else
            continue;
        }

        List<FiducialType> fiducialTypes = sideBoardType.getFiducialTypes();
        for (FiducialType fiducialType : fiducialTypes)
        {
          String name = fiducialType.getName();
          PanelCoordinate coord = fiducialType.getCoordinateInNanoMeters();
          int xInNanos = coord.getX();
          int yInNanos = coord.getY();
          int lengthInNanos = fiducialType.getLengthInNanoMeters();
          int widthInNanos = fiducialType.getWidthInNanoMeters();
          ShapeEnum shapeEnum = fiducialType.getShapeEnum();
          String shape = "";
          if (shapeEnum.equals(ShapeEnum.CIRCLE))
            shape = "circle";
          else if (shapeEnum.equals(ShapeEnum.RECTANGLE))
            shape = "rectangle";
          else
            Assert.expect(false);

          // fiducialName side1|side2 xLocation yLocation width length circle|rectangle
          _projectWriter.println(os, "fiducial: " + name + " " + side + " " + xInNanos + " " + yInNanos + " " +
                     widthInNanos + " " + lengthInNanos + " " + shape);

        }
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
