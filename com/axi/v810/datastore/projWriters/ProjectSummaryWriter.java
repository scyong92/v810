package com.axi.v810.datastore.projWriters;

import java.io.*;

import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import java.util.*;
import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 * This class will write out information that is found in other project files.
 * This summary file is special because it is meant to be read without having
 * to load the entire project into memory.
 *
 * @author Bill Darbie
 */
class ProjectSummaryWriter
{
  private static ProjectSummaryWriter _instance;
  private ProjectWriter _projectWriter;

  /**
   * @author Bill
   */
  static synchronized ProjectSummaryWriter getInstance()
  {
    if (_instance == null)
      _instance = new ProjectSummaryWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private ProjectSummaryWriter()
  {
    // do nothing
  }

  /**
   * @author Bill
   * @authoe Chnee Khang Wah, XCR1374
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();
    String projName = project.getName();
    String fileName = FileName.getProjectSummaryFullPath(projName, FileName.getProjectSummaryLatestFileVersion());

    //XCR1374, KhangWah, 2011-09-12, gather information of inspected joint and untestable joint.
    project.getPanel().gatherJointsCountDetail(); 
    
    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: project.X.summary");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");
      os.println("# project version");
      os.println(project.getVersion());

      os.println("# last modification time in milliseconds");
      os.println(project.getLastModificationTimeInMillis());
      os.println("# target customer name");
      _projectWriter.println(os, "targetCustomerName:" + project.getTargetCustomerName());
      os.println("# programmer name");
      _projectWriter.println(os, "programmerName:" + project.getProgrammerName());

      os.println("# project notes");
      String notes = project.getNotes();
      _projectWriter.writeMultipleLinesOfText(os, "notesBegin", notes, "notesEnd");

      Panel panel = project.getPanel();
      os.println("# number of board types");
      os.println(panel.getBoardTypes().size());
      os.println("# number of boards");
      os.println(panel.getBoards().size());
      os.println("# board names");
      for(Board board : panel.getBoards())
      {
        os.println(board.getName());
      }
      os.println("# number of components");
      os.println(panel.getNumComponents());
      os.println("# number of joints");
      os.println(panel.getNumJoints());
      os.println("# number of inspected joints");
//      os.println(panel.getNumInspectedJoints());
      os.println(panel.getNumInspectedJointsFast()); //XCR1374, KhangWah, 2011-09-12
      os.println("# number of untestable joints"); //XCR1374, KhangWah, 2011-09-12
      os.println(panel.getNumUnTestableJointsFast()); //XCR1374, KhangWah, 2011-09-12
      os.println("# cad xml checksum");
      os.println(project.getCadXmlChecksum());
      os.println("# settings xml checksum");
      os.println(project.getSettingsXmlChecksum());
      os.println("# system type");
      os.println(XrayTester.getSystemType().getName());
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
