package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class PanelAlignmentSurfaceMapSettingsWriter 
{
  public static String _COMMA_DELIMITER = ",";

  private static PanelAlignmentSurfaceMapSettingsWriter _instance;

  private ProjectWriter _projectWriter;

  /**
   * @author Cheah Lee Herng
   */
  public static synchronized PanelAlignmentSurfaceMapSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new PanelAlignmentSurfaceMapSettingsWriter();

    return _instance;
  }

  /**
   * @author Cheah Lee Herng
   */
  private PanelAlignmentSurfaceMapSettingsWriter()
  {
    // Do nothing
  }

  /**
   * @author Cheah Lee Herng
  */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();
    String projName = project.getName();
    String fileName = FileName.getProjectPanelAlignmentSurfaceMapSettingsFullPath(projName, FileName.getProjectPanelAlignmentSurfaceMapSettingsLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: panel_alignment_surfaceMap.X.settings");
      os.println("# PURPOSE: to list all surface map points used by this project");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################"); 
      os.println("# alignmentGroupName yes|no");
      os.println("#    Region pointPanelXCoordinateInNanometers pointPanelYCoordinateInNanometers width height scaleX shearY shearX scaleY translateX translateY");
      os.println("#      CameraRect cameraRectPanelXCoordinateInNanometers cameraRectPanelYCoordinateInNanometers widthInNanometers heightInNanometers cameraRectXCoordinateInPixel cameraRectYCoordinateInPixel widthInPixel heightInNanometer");
      os.println("#        pointSurfaceType pointPanelXCoordinateInNanometers pointPanelYCoordinateInNanometers pointPanelXCoordinateInPixel pointPanelYCoordinateInPixel yes|no");
//            os.println("#        Component refDes");

      PanelAlignmentSurfaceMapSettings panelAlignmentSurfaceMapSettings = project.getPanel().getPanelAlignmentSurfaceMapSettings();
      for(Map.Entry<String, String> alignmentGroupNameEntry : panelAlignmentSurfaceMapSettings.getAlignmentGroupNameToRegionPositionNameMap().entrySet())
      {
        String alignmentGroupName = alignmentGroupNameEntry.getKey();
        String regionPositionName = alignmentGroupNameEntry.getValue();

        OpticalRegion opticalRegion = panelAlignmentSurfaceMapSettings.getOpticalRegion(regionPositionName);
        PanelRectangle panelRectangle = opticalRegion.getRegion();

        boolean setToUsePsp = opticalRegion.isSetToUsePspForAlignment();
        String setToUsePspString = "";
        if (setToUsePsp)
          setToUsePspString = "yes";
        else
          setToUsePspString = "no";

        _projectWriter.println(os, alignmentGroupName + " " + setToUsePspString);

        double scaleX = opticalRegion.getAffineTransform().getScaleX();
        double shearY = opticalRegion.getAffineTransform().getShearY();
        double shearX = opticalRegion.getAffineTransform().getShearX();
        double scaleY = opticalRegion.getAffineTransform().getScaleY();
        double translateX = opticalRegion.getAffineTransform().getTranslateX();
        double translateY = opticalRegion.getAffineTransform().getTranslateY();

        String boardName = "";
        for(Board board : opticalRegion.getBoards())
        {
          boardName += board.getName() + _COMMA_DELIMITER;
        }
        int lastIndexOfComma = boardName.lastIndexOf(_COMMA_DELIMITER);
        if (lastIndexOfComma > 0 && lastIndexOfComma == (boardName.length() - 1))
          boardName.substring(0, boardName.length() - 1);

        _projectWriter.println(os, "   Region " + panelRectangle.getMinX() + " " + panelRectangle.getMinY() + " " + panelRectangle.getWidth() + " " + panelRectangle.getHeight() + " " + boardName
                + " "  + scaleX + " " + shearY + " " + shearX + " " + scaleY + " " + translateX + " " + translateY );

        // OpticalCameraRectangle information
        for(OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getAllOpticalCameraRectangles())
        {
          boolean inspectableBool = opticalCameraRectangle.getInspectableBool();
          double zHeightInNanometer = opticalCameraRectangle.getZHeightInNanometer();
          PanelRectangle opticalCameraRectanglePanelRectangle = opticalCameraRectangle.getRegion();
          _projectWriter.println(os, "      CameraRect " + opticalCameraRectanglePanelRectangle.getMinX() + " " + opticalCameraRectanglePanelRectangle.getMinY() + " " + 
                                  opticalCameraRectanglePanelRectangle.getWidth() + " " + opticalCameraRectanglePanelRectangle.getHeight() + " " + 
                                  inspectableBool + " " + zHeightInNanometer);

          for(PanelCoordinate panelCoordinate : opticalCameraRectangle.getPanelCoordinateList())
          {
            //String xCoordinateInNanometerString = String.valueOf(panelCoordinate.getX());
            //String yCoordinateInNanometerString = String.valueOf(panelCoordinate.getY());
            //boolean isVerified = opticalRegion.getPointPanelPositionVerifiedFlag(xCoordinateInNanometerString + "_" + yCoordinateInNanometerString);
            String verifiedFlagString = "yes"; // This flag is no longer used. Remove it next time.

            // Point PointStageXCoordinatesInNanometers PointStageYCoordinatesInNanometers yes|no
            _projectWriter.println(os, "      Point " + panelCoordinate.getX() + " " + panelCoordinate.getY() + " " + verifiedFlagString);
          }

//                    for(Component component : opticalRegion.getComponents())
//                    {
//                        // Component refDes
//                        _projectWriter.println(os, "        Component " + component.getReferenceDesignator());
//                    }
        }
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
      {
        os.close();
      }
    }
  }
}
