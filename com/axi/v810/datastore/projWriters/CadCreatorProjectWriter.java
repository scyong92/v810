package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.util.*;

/**
 * This class will write an entire project out to disk.
 *
 * @author Bill Darbie
 */
public class CadCreatorProjectWriter
{
  private static CadCreatorProjectWriter _instance;
  private boolean _displayTimes = false;

  private PanelWriter _panelWriter;
  private BoardTypeWriter _boardTypeWriter;
  private LandPatternWriter _landPatternWriter;
  private CompPackageWriter _compPackageWriter;

  private CadCreatorProjectSettingsWriter _projectSettingsWriter;
  private PanelSettingsWriter _panelSettingsWriter;
  private BoardSettingsWriter _boardSettingsWriter;
  private SubtypeSettingsWriter _subtypeSettingsWriter;
  private SubtypeAdvanceSettingsWriter _subtypeAdvanceSettingsWriter;
  private BoardTypeSettingsWriter _boardTypeSettingsWriter;
  private CompPackageSettingsWriter _compPackageSettingsWriter;

  private List<Pair<String, String>> backedUpFileList = new ArrayList<Pair<String, String>>();
  /**
   * @author Bill Darbie
   */
  public static synchronized CadCreatorProjectWriter getInstance()
  {
    if (_instance == null)
      _instance = new CadCreatorProjectWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private CadCreatorProjectWriter()
  {
    _panelWriter = PanelWriter.getInstance();
    _boardTypeWriter = BoardTypeWriter.getInstance();
    _compPackageWriter = CompPackageWriter.getInstance();
    _landPatternWriter = LandPatternWriter.getInstance();

    _projectSettingsWriter = CadCreatorProjectSettingsWriter.getInstance();
    _panelSettingsWriter = PanelSettingsWriter.getInstance();
    _boardSettingsWriter = BoardSettingsWriter.getInstance();
    _subtypeSettingsWriter = SubtypeSettingsWriter.getInstance();
    _subtypeAdvanceSettingsWriter = SubtypeAdvanceSettingsWriter.getInstance();
    _boardTypeSettingsWriter = BoardTypeSettingsWriter.getInstance();
    _compPackageSettingsWriter = CompPackageSettingsWriter.getInstance();
  }

  /**
   * Save the project to disk.
   *
   * @author Bill Darbie
   */
  public void save(Project project, boolean isFastSave) throws DatastoreException
  {
    Assert.expect(project != null);

    String projName = project.getName();
    String tempDir = "";
    List<String> dontMoveDirOrFiles = new LinkedList<String>();
    String projDir = "";
    boolean exception = false;

    String projectsDir = Directory.getProjectsDir();
    if (FileUtilAxi.exists(projectsDir) == false)
      FileUtilAxi.mkdirs(projectsDir);
    // backup all files in the current project directory if it exists
    projDir = Directory.getProjectDir(projName);
    if (FileUtilAxi.exists(projDir))
    {
      tempDir = FileUtilAxi.createTempDir(projDir);
      Collection<String> allSubDirs = FileUtilAxi.listAllSubDirectoriesFullPathInDirectory(projDir);
      dontMoveDirOrFiles.addAll(allSubDirs);
      try
      {
        TimerUtil timer = new TimerUtil();
        timer.start();
        FileUtilAxi.copyDirectoryRecursively(projDir, tempDir, dontMoveDirOrFiles);
        timer.stop();
        if ((_displayTimes) && (UnitTest.unitTesting() == false))
          System.out.println("wpd: time to copy project directory (in case the write fails), in millis:" + timer.getElapsedTimeInMillis());
      }
      catch (DatastoreException dex)
      {
        // copy back whatever files went into the temp directory before throwing the original exception
        try
        {
          if (FileUtilAxi.exists(tempDir))
          {
            FileUtilAxi.moveDirectoryContents(tempDir, projDir);
            FileUtilAxi.delete(tempDir);
          }
        }
        catch (DatastoreException dex2)
        {
          // if this failed don't worry about it - we did the best we could
        }
        throw dex;
      }
    }

    try
    {
      //assignAll(project);
      
      // now write out the new files
      write(project);

      // save the serialized file last since its time stamp must be the most recent!
      if(isFastSave)
      {
        if(FileUtilAxi.exists(FileName.getProjectSerializedFullPath(projName)))
          FileUtilAxi.delete(FileName.getProjectSerializedFullPath(projName));
      }
      else
        saveProjectAsSerializedFile(project);
    }
    catch (DatastoreException dex)
    {
      exception = true;
      throw dex;
    }
    catch (RuntimeException rex)
    {
      exception = true;
      throw rex;
    }
    finally
    {
      if (exception)
      {
        // remove the current directory contents and return the original directory
        // back to its previous state
        try
        {
          if (FileUtilAxi.exists(projDir))
          {
            List<String> dontDeleteFilesOrDirectories = new ArrayList<String>();
            dontDeleteFilesOrDirectories.add(tempDir);
            FileUtilAxi.deleteDirectoryContents(projDir, dontDeleteFilesOrDirectories);
            FileUtilAxi.moveDirectoryContents(tempDir, projDir);
            FileUtilAxi.delete(tempDir);
          }
        }
        catch (DatastoreException dex2)
        {
          dex2.printStackTrace();
        }
      }
    }

    MemoryUtil.garbageCollect();
  }

  /**
   * Save project information to the .project File
   * @author Bill Darbie
   */
  public void saveProjectAsSerializedFile(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    String fileName = FileName.getProjectSerializedFullPath(project.getName());

    // save file to a temporary name first, in case the program is
    // killed during the save
    File tempPanelFile = new File(fileName + FileName.getTempFileExtension());

    if (tempPanelFile.exists() && (tempPanelFile.canWrite() == false))
      throw new CannotWriteDatastoreException(tempPanelFile.getAbsolutePath());

    ObjectOutput os = null;
    try
    {
      FileOutputStream fos = new FileOutputStream(tempPanelFile);
      BufferedOutputStream bos = new BufferedOutputStream(fos);
      os = new ObjectOutputStream(bos);

      // wpd - timing
      long startTime = System.currentTimeMillis();
      os.writeObject(project);
      long stopTime = System.currentTimeMillis();
      long time = stopTime - startTime;
      if (_displayTimes && (UnitTest.unitTesting() == false))
      {
        System.out.println("time to save serialized file in milliseconds: " + time);
      }
    }
    catch(IOException ioe)
    {
      ioe.printStackTrace();
      // remove the partially written file, but must close the stream first
      try
      {
        if (os != null)
          os.close();
      }
      catch(IOException fioe)
      {
        // do nothing
      }
      tempPanelFile.delete();

      DatastoreException dex = new CannotWriteDatastoreException(fileName);
      dex.initCause(ioe);
    }
    finally
    {
      try
      {
        if (os != null)
          os.close();
      }
      catch(IOException ioe)
      {
        ioe.printStackTrace();
      }
    }

    // now move the temporary file to the real file name
    File destFile = new File(fileName);
    destFile.delete();
    tempPanelFile.renameTo(destFile);
  }
  
  private void assignAll(Project project) throws DatastoreException
  {
    // find ComponentTypes that should really be Fiducials
    // assign component packages
    TimerUtil timer = new TimerUtil();
    timer.reset();
    timer.start();
    assignCompPackages(project);
    timer.stop();
    long time = timer.getElapsedTimeInMillis();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
      System.out.println("wpd - Time to assign component packages (and subtypes) in millis: " + time);

    // assign pad orientations, pitch and interpad distances
    timer.reset();
    timer.start();
    assignPinOrientationsPitchAndInterPadDistancesAndPadOneAndWidthLengthAndCoord(project);
    timer.stop();
    time = timer.getElapsedTimeInMillis();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
      System.out.println("Time to assign pin orientations, pitch and interpad distances, and Land Pattern coords in millis: " + time);

    // assign AlignmentGroups
    timer.reset();
    timer.start();
    assignAlignmentGroups(project);
    timer.stop();
    time = timer.getElapsedTimeInMillis();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
      System.out.println("Time to assign alignment groups in millis: " + time);
  }
  
  private void assignPinOrientationsPitchAndInterPadDistancesAndPadOneAndWidthLengthAndCoord(Project project)
  {
    for (LandPattern landPattern : project.getPanel().getAllLandPatterns())
      landPattern.assignAll();
  }

  /**
   * @author Bill Darbie
   */
  private void assignCompPackages(Project project) throws DatastoreException
  {
    // load the rules for joint type assignment first
    JointTypeEnumAssignment.getInstance().load();
    for (ComponentType componentType : project.getPanel().getComponentTypes())
    {
      componentType.assignOrModifyCompPackageAndSubtypeAndJointTypeEnums();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void assignAlignmentGroups(Project project)
  {
    project.getPanel().getPanelSettings().assignAlignmentGroups();

    for(Board board : project.getPanel().getBoards())
    {
      board.getBoardSettings().assignAlignmentGroups();
    }
  }


  /**
   * @author Bill Darbie
   */
  private void write(Project project) throws DatastoreException
  {
    String projName = project.getName();
    String dirName = Directory.getProjectDir(projName);
    // create the directory if it does not exist
    if (FileUtilAxi.exists(dirName) == false)
      FileUtilAxi.mkdirs(dirName);

    if (project != null)
    {
      project.setName(projName);
      // on a serialized load not all of the test program gets pulled in
      // because not all of it was save, so let's initialize it
      com.axi.v810.business.testGen.ProgramGeneration.getInstance().intializeTestProgramAfterProjectLoad(project.getTestProgram());
    }
    
    if(project.hasSoftwareVersionOfProjectLastSave() == false)
       project.setSoftwareVersionOfProjectLastSave(Version.getVersionNumber());
    else
    {
        // Update project file creation version.
        if (Double.parseDouble(project.getSoftwareVersionOfProjectLastSave()) < Double.parseDouble(Version.getVersionNumber()))
        {
          project.setSoftwareVersionOfProjectLastSave(Version.getVersionNumber());
        }
    }

    // write out the xml file first, because writing the xml files generates the checksums,
    // which will get written out in the project text files
    TimerUtil timer = new TimerUtil();
    // the settings xml file has to be written out before the cad xml file
    
    Map<Board, Integer> boardToBoardInstanceNumberMap = _panelWriter.write(project);
    _boardTypeWriter.write(project);
    _compPackageWriter.write(project);
    _landPatternWriter.write(project);

    _projectSettingsWriter.write(project);
    _panelSettingsWriter.write(project);
    _boardSettingsWriter.write(project, boardToBoardInstanceNumberMap);
    _subtypeSettingsWriter.write(project);
    _subtypeAdvanceSettingsWriter.write(project);
    _boardTypeSettingsWriter.write(project);
    _compPackageSettingsWriter.write(project);
    
    timer.stop();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
      System.out.println("wpd: time to write out text files, in millis:" + timer.getElapsedTimeInMillis());
  }

  /**
   * Change any # to \#.  This method should be called for any strings that need to be written out that
   * may have a # in them.
   * @author Bill Darbie
   */
  void println(PrintWriter os, String line)
  {
    Assert.expect(os != null);
    Assert.expect(line != null);

    line = line.replaceAll("#", "\\\\#");
    os.println(line);
  }

  /**
   * Take a text line that can contain one or more \n in it and write it out to
   * a file so it can be parsed by ProjectReader.readMultipleLinesOfText.
   * @author Bill Darbie
   */
  void writeMultipleLinesOfText(PrintWriter os, String header, String text, String footer)
  {
    Assert.expect(os != null);
    Assert.expect(header != null);
    Assert.expect(text != null);
    Assert.expect(footer != null);

    List<String> textList = new ArrayList<String>();
    int numNewLines = 0;
    if (text.equals("") == false)
    {
      int beginIndex = 0;
      int endIndex = 0;
      do
      {
        if (endIndex == 0)
          --endIndex;
        endIndex = text.indexOf("\n", beginIndex);
        String str = null;
        if (endIndex == -1)
          str = text.substring(beginIndex, text.length());
        else
          str = text.substring(beginIndex, endIndex);
        // replace # with \#
        str = str.replaceAll("#", "\\#");
        textList.add(str);
        beginIndex = endIndex + 1;
        ++numNewLines;
      }
      while (endIndex != -1);
    }
    os.println(header);
    os.println(numNewLines);
    for (int i = 0; i < numNewLines; ++i)
      println(os, "\"" + textList.get(i) + "\"");
    os.println(footer);
  }

  /*
   * Get rid off project files that were created with older version of our application.
   * @author Rex Shang
   */
  private void removeOlderVersionProjectFiles(Project project) throws DatastoreException
  {
    String projName = project.getName();
    List<String> filesToRemove = new ArrayList<String>();
    int previousVersion = 0;

    Panel panel = project.getPanel();
    List<BoardType> boardTypes = panel.getBoardTypes();
    List<Board> boards = panel.getBoards();

    // project.X.summary
    previousVersion = FileName.getProjectSummaryLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectSummaryFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // panel.X.cad
    previousVersion = FileName.getProjectPanelLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectPanelFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // boardType_X.X.cad
    previousVersion = FileName.getProjectBoardTypeLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      for (int boardTypeNumber = 1; boardTypeNumber <= boardTypes.size(); boardTypeNumber++)
      {
        String oldFile = FileName.getProjectBoardTypeFullPath(projName, boardTypeNumber, previousVersion);
        if (FileUtilAxi.exists(oldFile))
        {
          filesToRemove.add(oldFile);
        }
      }
      previousVersion--;
    }

    // package.X.cad
    previousVersion = FileName.getProjectCompPackageLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectCompPackageFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // landPattern.X.cad
    previousVersion = FileName.getProjectLandPatternLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectLandPatternFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // project.X.settings
    previousVersion = FileName.getProjectProjectSettingsLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectProjectSettingsFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // panel.X.settings
    previousVersion = FileName.getProjectPanelSettingsLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectPanelSettingsFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // board_X.X.settings
    previousVersion = FileName.getProjectBoardSettingsLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      for (int boardInstanceNumber = 1; boardInstanceNumber <= boards.size(); boardInstanceNumber++)
      {
        String oldFile = FileName.getProjectBoardSettingsFullPath(projName, boardInstanceNumber, previousVersion);
        if (FileUtilAxi.exists(oldFile))
        {
          filesToRemove.add(oldFile);
        }
      }
      previousVersion--;
    }

    // subtype.X.settings
    //Khaw Chek Hau - XCR2514 : Threshold value changed by itself after using software 5.6
    //Don't delete the old subtypeSetting file as it will cause assert when try to open newer version recipe
//    previousVersion = FileName.getProjectSubtypeSettingsLatestFileVersion() - 1;
//    while (previousVersion > 0)
//    {
//      String oldFile = FileName.getProjectSubtypeSettingsFullPath(projName, previousVersion);
//      if (FileUtilAxi.exists(oldFile))
//      {
//        filesToRemove.add(oldFile);
//      }
//      previousVersion--;
//    }

    // boardType_X.X.settings
    previousVersion = FileName.getProjectBoardTypeSettingsLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      for (int boardTypeNumber = 1; boardTypeNumber <= boardTypes.size(); boardTypeNumber++)
      {
        String oldFile = FileName.getProjectBoardTypeSettingsFullPath(projName, boardTypeNumber, previousVersion);
        if (FileUtilAxi.exists(oldFile))
        {
          filesToRemove.add(oldFile);
        }
      }
      previousVersion--;
    }

    // componentNoLoad.X.settings
    previousVersion = FileName.getProjectComponentNoLoadSettingsLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectComponentNoLoadSettingsFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // alignmentTransform.X.settings
    previousVersion = FileName.getProjectAlignmentTransfromSettingsLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectAlignmentTransformSettingsFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // package.X.settings
    previousVersion = FileName.getProjectCompPackageLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectCompPackageSettingFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // Do the backup.
    FileUtilAxi.delete(filesToRemove);
  }

}
