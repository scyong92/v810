package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.v810.datastore.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class ComponentNoLoadSettingsWriter
{
  private static ComponentNoLoadSettingsWriter _instance;

  private ProjectWriter _projectWriter;
  private Pattern _fileNamePattern = Pattern.compile("^(componentNoLoad.)(\\d)(\\.)(\\d+)(.settings)$");

  /**
   * @author Bill
   */
  public static synchronized ComponentNoLoadSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new ComponentNoLoadSettingsWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private ComponentNoLoadSettingsWriter()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   * @author Kok Chun, Tan
   */
  public void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();

    String projName = project.getName();
    Panel panel = project.getPanel();
    List<VariationSetting> noLoadSettingVariationList = panel.getPanelSettings().getAllVariationSettings();
    Map<String, List<String>> boardTypeToComponentsMap = new LinkedHashMap<String, java.util.List<String>>();
    VariationSetting currentVariation = null;
    PrintWriter os = null;

    if (project.isUpdateComponentNoLoadSetting())
    {
      //find out current enable variation
      for (VariationSetting variation : noLoadSettingVariationList)
      {
        if (variation.isEnable())
        {
          currentVariation = variation;
          break;
        }
      }
      
      // check current no load components
      for (BoardType boardType : panel.getBoardTypes())
      {
        List<String> refDesList = new ArrayList<String>();
        for (ComponentType componentType : boardType.getComponentTypes())
        {
          if (componentType.getComponentTypeSettings().isLoaded() == false)
          {
            String refDes = componentType.getReferenceDesignator();
            refDesList.add(refDes);
          }
        }
        if (refDesList.isEmpty() == false)
        {
          boardTypeToComponentsMap.put(boardType.getName(), refDesList);
        }
      }

      //update variation list if the map is not empty
      if (boardTypeToComponentsMap.isEmpty() == false)
      {
        //if have variation enabled, then update the variation.
        if (currentVariation != null)
        {
          currentVariation.getBoardTypeToRefDesMap().clear();
          currentVariation.addBoardTypeToRefDesMap(boardTypeToComponentsMap);
        }
        //if no variation enabled, then create a new variation.
        else
        {
          String name = VariationSettingManager.getInstance().getDefaultVariationName();
          VariationSetting variation = new VariationSetting(project, name, true, true, boardTypeToComponentsMap);
          project.getPanel().getPanelSettings().addVariationSetting(variation);
          VariationSettingManager.getInstance().setupVariationSetting(true, project, boardTypeToComponentsMap);
          project.setEnabledVariationName(variation.getName());
          project.setSelectedVariationName(variation.getName());
        }
      }
      //delete the variation if the map is empty
      else
      {
        if (currentVariation != null)
        {
          panel.getPanelSettings().getAllVariationSettings().remove(currentVariation);
          project.setEnabledVariationName(VariationSettingManager.ALL_LOADED);
          project.setSelectedVariationName(VariationSettingManager.ALL_LOADED);
        }
      }
    }
    
    noLoadSettingVariationList = panel.getPanelSettings().getAllVariationSettings();
    for (int i = 0; i < noLoadSettingVariationList.size(); i++)
    {
      String fileName = FileName.getProjectComponentNoLoadSettingsFullPath(projName, FileName.getProjectComponentNoLoadSettingsLatestFileVersion(), i + 1);
      try
      {
        VariationSetting variation = noLoadSettingVariationList.get(i);
        os = new PrintWriter(fileName);
        os.println("##############################################################################");
        os.println("# FILE: componentNoLoad.X.X.settings");
        os.println("##############################################################################");
        os.println("# components listed here are not loaded");
        os.println("# variationName: variationName");
        os.println("#  isEnable: isEnable");
        os.println("#   boardTypeName: boardTypeName");
        os.println("#    refDes1");
        os.println("#    refDes2");

        _projectWriter.println(os, "variationName: " + variation.getName());
        _projectWriter.println(os, " isEnable: " + variation.isEnable());
        _projectWriter.println(os, " isViewableInProduction: " + variation.isViewableInProduction());
        for (Map.Entry<String, List<String>> boardType : variation.getBoardTypeToRefDesMap().entrySet())
        {
          _projectWriter.println(os, "  boardTypeName: " + boardType.getKey());
          for (String refDes : boardType.getValue())
          {
            _projectWriter.println(os, "   " + refDes);
          }
        }
      }
      catch (FileNotFoundException ex)
      {
        FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
        dex.initCause(ex);
        throw dex;
      }
      finally
      {
        if (os != null)
          os.close();
      }
    }
    
    // if no variation, we need to generate 1 empty componentNoLoad.X.1.settings file
    if (noLoadSettingVariationList.isEmpty())
    {
      String fileName = FileName.getProjectComponentNoLoadSettingsFullPath(projName, FileName.getProjectComponentNoLoadSettingsLatestFileVersion(), 1);
      try
      {
        os = new PrintWriter(fileName);
        os.println("##############################################################################");
        os.println("# FILE: componentNoLoad.X.X.settings");
        os.println("##############################################################################");
        os.println("# components listed here are not loaded");
        os.println("# variationName: variationName");
        os.println("#  isEnable: isEnable");
        os.println("#   boardTypeName: boardTypeName");
        os.println("#    refDes1");
        os.println("#    refDes2");
      }
      catch (FileNotFoundException ex)
      {
        FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
        dex.initCause(ex);
        throw dex;
      }
      finally
      {
        if (os != null)
          os.close();
      }
    }

    String projDir = Directory.getProjectDir(project.getName());
    for (String file : FileUtilAxi.listAllFilesInDirectory(projDir))
    {
      Matcher matcher = _fileNamePattern.matcher(file);
      if (matcher.matches())
      {
        //componentNoLoad.X.1.Settings always not delete.
        if (Integer.parseInt(matcher.group(4)) > noLoadSettingVariationList.size() && Integer.parseInt(matcher.group(4)) > 1)
        {
          FileUtilAxi.delete(FileName.getProjectComponentNoLoadSettingsFullPath(project.getName(), file));
        }
      }
    }
  }
}
