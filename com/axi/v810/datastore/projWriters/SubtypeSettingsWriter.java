package com.axi.v810.datastore.projWriters;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 * @author Bill Darbie
 */
class SubtypeSettingsWriter
{
  private static SubtypeSettingsWriter _instance;

  private ProjectWriter _projectWriter;
  private EnumStringLookup _enumStringLookup;

  /**
   * @author Bill
   */
  static synchronized SubtypeSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new SubtypeSettingsWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private SubtypeSettingsWriter()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * @author Bill Darbie
   * @author George Booth
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();

    String projName = project.getName();
    String fileName = FileName.getProjectSubtypeSettingsFullPath(projName, FileName.getProjectSubtypeSettingsLatestFileVersion());
    String fileNameSubtypeSettingsVersion4 = FileName.getProjectSubtypeSettingsFullPath(projName, 4);
    
    //Khaw Chek Hau - XCR2514 : Threshold value changed by itself after using software 5.6
    String softwareVersion = Version.getVersion();
    if (softwareVersion.startsWith("5.6c"))
      writeIntoSubtypeSettingsFile(project, fileNameSubtypeSettingsVersion4);

    writeIntoSubtypeSettingsFile(project, fileName);
  }

  /**
   * @author Bill Darbie
   */
  private String getAlgorithmName(AlgorithmEnum algorithmEnum)
  {
    Assert.expect(algorithmEnum != null);

    String algorithmName = _enumStringLookup.getAlgorithmString(algorithmEnum);
    return algorithmName;
  }

  /**
   * @author Bill Darbie
   */
  private String getAlgorithmSettingName(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);

    String algorithmSettingName = _enumStringLookup.getAlgorithmSettingString(algorithmSettingEnum);
    return algorithmSettingName;
  }

  /**
   * @author Bill Darbie
   */
  private String getTypeString(Serializable value)
  {
    Assert.expect(value != null);

    String typeStr = null;
    if (value instanceof Integer)
      typeStr = "int";
    else if (value instanceof String)
      typeStr = "string";
    else if (value instanceof Float)
      typeStr = "float";
    else
      Assert.expect(false);

    return typeStr;
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR2514 : Threshold value changed by itself after using software 5.6
   */
  private void writeIntoSubtypeSettingsFile(Project project, String fileName) throws DatastoreException
  {    
    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: subtype.X.settings");
      os.println("# PURPOSE: to list all subtypes used by this project");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");
      os.println("# subtypeNameLongName subtypeShortName inspectionFamilyName jointTypeEnum allAlgorithmsExceptShortsLearned shortsAlgorithmLearned expectedImageAlgorithmLearned expectedImageTemplateLearned userComment importedPackageLibraryPath parentSubtype");//Shengchuan - Clear Tombstone
      os.println("#   algorithmName algorithmVersion on|off");
      os.println("#     # tuned algorithm settings");
      os.println("#     algorithmSettingName tunedValueType tunedValue importedFromLibrary modifiedByUser"); // added importedFromLibrary and modifiedByUser --> the value would be true or false
      os.println("#     # commented algorithm settings");
      os.println("#     algorithmSettingName userComment");

      for (Subtype subtype : project.getPanel().getSubtypes())
      {
        String longSubtypeName = subtype.getLongName();
        String subtypeShortName = subtype.getShortName();
        InspectionFamilyEnum inspectionFamilyEnum = subtype.getInspectionFamilyEnum();
        String inspectionFamilyName = _enumStringLookup.getInspectionFamilyString(inspectionFamilyEnum);
        JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
        String jointTypeEnumStr = _enumStringLookup.getJointTypeString(jointTypeEnum);
        String importedPackageLibraryPath = subtype.getImportedPackageLibraryPath();

        String allAlgsExceptShortLearned = "false";
        if (subtype.isJointSpecificDataLearned())
          allAlgsExceptShortLearned = "true";
        String isShortAlgorithmLearned = "false";
        if (subtype.isJointSpecificDataLearned())
          isShortAlgorithmLearned = "true";
        String isExpectedImageAlgorithmLearned = "false";
        if (subtype.isExpectedImageDataLearned())
          isExpectedImageAlgorithmLearned = "true";
        //ShengChuan - Clear Tombstone
        String isExpectedImageTemplateAlgorithmLearned = "false";
        if (subtype.isExpectedImageTemplateLearned())
          isExpectedImageTemplateAlgorithmLearned = "true";
        

        String userComment = subtype.getUserComment();
        // subtypeNameLongName subtypeShortName inspectionFamilyName jointTypeEnum allAlgorithmsExceptShortsLearned shortsAlgorithmLearned expectedImageAlgorithmLearned userComment importedFromLibrary modifiedByUser
        _projectWriter.println(os, longSubtypeName + " " +
                               subtypeShortName + " " +
                               inspectionFamilyName + " " +
                               jointTypeEnumStr + " " +
                               allAlgsExceptShortLearned + " " +
                               isShortAlgorithmLearned + " " +
                               isExpectedImageAlgorithmLearned + " " +
                               isExpectedImageTemplateAlgorithmLearned + " " +//ShengChuan - Clear Tombstone
                               "\"" + userComment + "\"" + " " +
                               "\"" +importedPackageLibraryPath + "\"");
        List<AlgorithmSettingEnum> tunedAlgSettingEnums = subtype.getTunedAlgorithmSettingEnums();
        List<AlgorithmSettingEnum> importedAlgSettingEnums = subtype.getImportedAlgorithmSettingEnums();

        InspectionFamily inspectionFamily = InspectionFamily.getInstance(inspectionFamilyEnum);
        List<Algorithm> subtypeAlgorithms = subtype.getEnabledAlgorithms();
        for (Algorithm algorithm : inspectionFamily.getAlgorithms())
        {
          AlgorithmEnum algorithmEnum = algorithm.getAlgorithmEnum();
          String algorithmName = getAlgorithmName(algorithmEnum);
          int algVersion = subtype.getAlgorithmVersion(algorithmEnum);
          String onOrOff = "on";
          if (subtypeAlgorithms.contains(algorithm) == false)
            onOrOff = "off";
          // algorithmName algorithmVersion on|off
          _projectWriter.println(os, "  " + algorithmName + " " + algVersion + " " + onOrOff);

          // print out all imported AlgorithmSetting lines
          os.println("    # imported algorithm settings");
          for (AlgorithmSettingEnum importedAlgSettingEnum : importedAlgSettingEnums)
          {
            if (algorithm.doesAlgorithmSettingExist(importedAlgSettingEnum))
            {
              String algorithmSettingName = getAlgorithmSettingName(importedAlgSettingEnum);
              Serializable importedValue = subtype.getImportedAlgorithmSettingValue(importedAlgSettingEnum);
              boolean isImportedFromLibrary = true;
              String typeStr = getTypeString(importedValue);
              // algorithmSettingName tunedValueType tunedValue importedFromLibrary
              _projectWriter.println(os, "    " + algorithmSettingName + " " + typeStr + " \"" + importedValue + "\"" + " " + isImportedFromLibrary);
            }
          }

          // print out all tuned AlgorithmSetting lines
          os.println("    # tuned algorithm settings");
          for (AlgorithmSettingEnum tunedAlgorithmSettingEnum : tunedAlgSettingEnums)
          {
            if (algorithm.doesAlgorithmSettingExist(tunedAlgorithmSettingEnum))
            {
              String algorithmSettingName = getAlgorithmSettingName(tunedAlgorithmSettingEnum);
              Serializable tunedValue = subtype.getAlgorithmSettingValue(tunedAlgorithmSettingEnum);
              boolean isImportedFromLibrary = false;
              String typeStr = getTypeString(tunedValue);
              // algorithmSettingName tunedValueType tunedValue importedFromLibrary
              _projectWriter.println(os, "    " + algorithmSettingName + " " + typeStr + " \"" + tunedValue + "\""  + " " + isImportedFromLibrary);
            }
          }

          // print out all AlgorithmSetting lines with user Comments
          os.println("    # commented algorithm settings");
          for (AlgorithmSettingEnum commentedAlgorithmSettingEnum : subtype.getCommentedAlgorithmSettingEnums())
          {
            if (algorithm.doesAlgorithmSettingExist(commentedAlgorithmSettingEnum))
            {
              String algorithmSettingName = getAlgorithmSettingName(commentedAlgorithmSettingEnum);
              String comment = subtype.getAlgorithmSettingUserComment(commentedAlgorithmSettingEnum);
              // algorithmSettingName userComment
              _projectWriter.println(os, "    " + algorithmSettingName + " \"" + comment + "\"");
            }
          }
        }
        
        //Siew Yeng - XCR-2764 - Sub-subtype feature
        if(subtype.hasSubSubtypes())
        {
          for(Subtype subSubtype : subtype.getSubSubtypes())
          {
            _projectWriter.println(os, subSubtype.getLongName() + " " +
                             subSubtype.getShortName() + " " +
                             inspectionFamilyName + " " +
                             jointTypeEnumStr + " " +
                             allAlgsExceptShortLearned + " " +
                             isShortAlgorithmLearned + " " +
                             isExpectedImageAlgorithmLearned + " " +
                             isExpectedImageTemplateAlgorithmLearned + " " +//ShengChuan - Clear Tombstone
                             "\"" + userComment + "\"" + " " +
                             "\"" +importedPackageLibraryPath + "\"" + " " +
                            subtype.getLongName());

            List<AlgorithmEnum> enabledAlgorithms = subSubtype.getEnabledAlgorithmEnums();
            for(AlgorithmEnum algoEnum : subSubtype.getAlgorithmEnums())
            {
              Algorithm algo = InspectionFamily.getAlgorithm(inspectionFamily.getInspectionFamilyEnum(), algoEnum);
              String onOrOff = "on";
              if (enabledAlgorithms.contains(algoEnum) == false)
                onOrOff = "off";
              _projectWriter.println(os, "  " + getAlgorithmName(algoEnum) + " " + subSubtype.getAlgorithmVersion(algoEnum) + " " + onOrOff);

              os.println("    # tuned algorithm settings");
              for (AlgorithmSettingEnum tunedAlgoSettingEnum : subSubtype.getTunedAlgorithmSettingEnums())
              {
                if (algo.doesAlgorithmSettingExist(tunedAlgoSettingEnum))
                {
                  String algorithmSettingName = getAlgorithmSettingName(tunedAlgoSettingEnum);
                  Serializable tunedValue = subSubtype.getAlgorithmSettingValue(tunedAlgoSettingEnum);
                  boolean isImportedFromLibrary = false;
                  String typeStr = getTypeString(tunedValue);
                  // algorithmSettingName tunedValueType tunedValue importedFromLibrary
                  _projectWriter.println(os, "    " + algorithmSettingName + " " + typeStr + " \"" + tunedValue + "\""  + " " + isImportedFromLibrary);
                }
              }
            }
          }
        }
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
