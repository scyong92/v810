package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.util.*;

/**
 * This class is the writer for package setting file.
 * Modification to package setting format should modified this class
 *
 * @author Poh Kheng
 */
class CompPackageSettingsWriter
{
  private static CompPackageSettingsWriter _instance;
  private ProjectWriter _projectWriter;

  /**
   * @author Poh Kheng
   */
  static synchronized CompPackageSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new CompPackageSettingsWriter();

    return _instance;
  }

  /**
   * @author Poh Kheng
   */
  private CompPackageSettingsWriter()
  {
    // do nothing
  }

  /**
   * This would write out the pacakge setting file.
   *
   * @author Poh Kheng
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();
    String projName = project.getName();
    String fileName = FileName.getProjectCompPackageSettingFullPath(projName, FileName.getProjectCompPackageSettingLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: package.X.settings");
      os.println("# PURPOSE: to list all subtypes used by this project.");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");
      os.println("# packageLongName packageShortName importedPackageLibraryPath libraryCompPackageCheckSum libraryLandPatternName isImported subtypeThresholdCheckSum");

      Panel panel = project.getPanel();
      List<CompPackage> compPackages = panel.getCompPackages();
      for (CompPackage compPackage : compPackages)
      {
        if(compPackage.hasSourceLibrary())
        {
          String compPackageLongName = compPackage.getLongName();
          String compPackageShortName = compPackage.getShortName();
          String compPackageLibraryPath = compPackage.getSourceLibrary();
          long compPackageCheckSum = compPackage.getLibraryCheckSum();
          String libraryLandPatternName = "null";
          if(compPackage.hasLibraryLandPatternName())
           libraryLandPatternName = compPackage.getLibraryLandpatternName();
          boolean imported = compPackage.isImported();
          long subtypeThresholdsCheckSum = compPackage.hasPreviousSubtypeThresholdsCheckSum() ? compPackage.getPreviousSubtypeThresholdsCheckSum() : 0;

          _projectWriter.println(os, compPackageLongName + " " + compPackageShortName + " \"" +
                  compPackageLibraryPath + "\"" + " " + compPackageCheckSum + " " + libraryLandPatternName + " " +
                  imported + " " + subtypeThresholdsCheckSum);
        }
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
