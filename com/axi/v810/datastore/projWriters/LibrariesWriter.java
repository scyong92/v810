package com.axi.v810.datastore.projWriters;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.v810.datastore.*;

/**
 * @author Wei Chin, Chong
 * @version 1.0
 */
public class LibrariesWriter
{
  private static LibrariesWriter _instance;

  /**
   * @author Wei Chin, Chong
   */
  private LibrariesWriter()
  {
    //do nothing
  }

  /**
   * @return LibrariesWriter
   *
   * @author Wei Chin, Chong
   */
  public static synchronized LibrariesWriter getInstance()
  {
    if (_instance == null)
      _instance = new LibrariesWriter();

    return _instance;
  }

  /**
   * @param libraryList LibraryManager
   * @throws DatastoreException
   *
   * @author Wei Chin, Chong
   */
  public void write(LibraryManager libraryList) throws DatastoreException
  {
    Assert.expect(libraryList != null);

    String fileName = FileName.getLibraryListFullPath();

    PrintWriter os = null;
    if(FileUtil.exists(fileName) == false)
    {
      try
      {
        FileUtil.createEmtpyFile(fileName);
      }
      catch(IOException ioe)
      {
        CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(fileName);
        dex.initCause(ioe);
        throw dex;
      }
    }
    try
    {
      os = new PrintWriter(fileName);
      for(String library: libraryList.getLibraries())
        os.println(library);

      os.println(LibraryManager.START_OF_SELECTED_LIBRARIES);

      for(String libraryPath : libraryList.getSelectedLibraries())
        os.println(libraryPath);
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
