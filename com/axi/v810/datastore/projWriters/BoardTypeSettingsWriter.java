package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;

/**
 * @author Bill Darbie
 */
class BoardTypeSettingsWriter
{
  private static BoardTypeSettingsWriter _instance;

  private ProjectWriter _projectWriter;
  private EnumStringLookup _enumStringLookup;

  /**
   * @author Bill
   */
  static synchronized BoardTypeSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new BoardTypeSettingsWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private BoardTypeSettingsWriter()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();

    int boardTypeNumber = 0;
    List<BoardType> boardTypes = project.getPanel().getBoardTypes();
    for (BoardType boardType : boardTypes)
    {
      ++boardTypeNumber;
      writeBoardTypeSettings(project, boardType, boardTypeNumber);
    }
  }

//  /**
//   * @author George A. David
//   */
//  private boolean requiresPadSpecificData(ComponentTypeSettings compTypeSettings)
//  {
//    boolean requiresPadData = true;
//    for(PadType padType : compTypeSettings.getComponentType().getPadTypes())
//    {
//      PadTypeSettings padTypeSettings = padType.getPadTypeSettings();
//      if(padTypeSettings.getSignalCompensation().equals(SignalCompensationEnum.DEFAULT_LOW) == false ||
//         padTypeSettings.allowShadingCompensation() == false)
//      {
//        requiresPadData = false;
//        break;
//      }
//    }
//
//
//
//    return requiresPadData;
//  }

  /**
   * @author Bill Darbie
   */
  private void writeBoardTypeSettings(Project project, BoardType boardType, int boardTypeNumber) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(boardType != null);

    String projName = project.getName();
    String fileName = FileName.getProjectBoardTypeSettingsFullPath(projName, boardTypeNumber, FileName.getProjectBoardTypeSettingsLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);
      os.println("##############################################################################");
      os.println("# FILE: boardType_name.X.settings");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");
      os.println("# refDes inspected|notInspected singleSubtype subtypeName");
      os.println("# refDes inspected|notInspected multipleSubtypes");
      os.println("#   padName1 subTypeName signalCompensation shadingCompensationAllowed customArtifactCompensationState globalSurfaceModel userGain");
      os.println("#   padName2 subTypeName signalCompensation shadingCompensationAllowed customArtifactCompensationState globalSurfaceModel userGain");
      os.println("# refDes");
      os.println("#   padName1 testable|notTestable inspected|notInspected subTypeName signalCompensation shadingCompensationAllowed artifactCompensationState globalSurfaceModel userGain");
      os.println("#   padName2 testable|notTestable inspected|notInspected subTypeName signalCompensation shadingCompensationAllowed artifactCompensationState globalSurfaceModel userGain");
      for (ComponentType componentType : boardType.getComponentTypes())
      {
        String refDes = componentType.getReferenceDesignator();
        ComponentTypeSettings compTypeSettings = componentType.getComponentTypeSettings();
        boolean allInspected = compTypeSettings.areAllPadTypesInspectedFlagsSet();
        boolean allNotInspected = compTypeSettings.areAllPadTypesInspectedFlagNotSet();
        boolean allTestable = compTypeSettings.areAllPadTypesTestable();
        boolean allGlobalSurfaceModelAtDefaultSetting = compTypeSettings.areAllPadTypesAtDefaultGlobalSurfaceModel();

        boolean needToWritePadSpecificData = allTestable == false || allGlobalSurfaceModelAtDefaultSetting == false || (allInspected == false && allNotInspected == false);

        String inspectedStr = null;
        if (allTestable)
        {
          if (allInspected)
            inspectedStr = "inspected";
          else if (allNotInspected)
            inspectedStr = "notInspected";
        }

        if (compTypeSettings.usesOneSubtype() && needToWritePadSpecificData == false)
        {
          Subtype subtype = compTypeSettings.getSubtype();
          String subtypeName = subtype.getLongName();

          // refDes inspected|notInspected singleSubtype subtype
          _projectWriter.println(os, refDes + " " + inspectedStr + " singleSubtype " + subtypeName);
        }
        else
        {
          if (inspectedStr == null)
          {
            // refDes
            _projectWriter.println(os, refDes);
          }
          else
          {
            // refDes inspected|notInspected multipleSubtypes
            _projectWriter.println(os, refDes + " " + inspectedStr + " multipleSubtypes");
          }

          for (PadType padType : componentType.getPadTypes())
          {
            String padName = padType.getName();
            Subtype subtype = padType.getPadTypeSettings().getSubtype();
            String subtypeName = subtype.getLongName();
            String globalSurfaceModelStr = _enumStringLookup.getGlobalSurfaceModelEnumString(padType.getPadTypeSettings().getGlobalSurfaceModel());

            if (inspectedStr == null)
            {
              String testableStr = "testable";
              PadTypeSettings padTypeSettings = padType.getPadTypeSettings();
              if (padTypeSettings.isTestable() == false)
                testableStr = "notTestable";
              String inspectStr = "inspected";
              if (padTypeSettings.isInspectedFlagSet() == false)
                inspectStr = "notInspected";
              //   padName inspected|notInspected subtype  globalSurfaceModel 
              _projectWriter.println(os,"  " + padName + " " + testableStr + " " + inspectStr + " " + subtypeName + " " +  globalSurfaceModelStr);
            }
            else
            {
              //   padName subtype  globalSurfaceModel 
              _projectWriter.println(os, "  " + padName + " " + subtypeName   + " " + globalSurfaceModelStr);
            }
          }
        }
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
