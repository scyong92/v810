package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;

/**
 * @author sham
 */
public class SubtypeAdvanceSettingsWriter
{
 private static SubtypeAdvanceSettingsWriter _instance;

  private ProjectWriter _projectWriter;
  private EnumStringLookup _enumStringLookup;

  /**
   * @author Bill
   */
  static synchronized SubtypeAdvanceSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new SubtypeAdvanceSettingsWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private SubtypeAdvanceSettingsWriter()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * @author Bill Darbie
   * @author George Booth
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();

    String projName = project.getName();
    String fileName = FileName.getProjectSubtypeAdvanceSettingsFullPath(projName, FileName.getProjectSubtypeAdvanceSettingsLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: subtypeAdvance.X.settings");
      os.println("# PURPOSE: to list all subtypes's advance settings used by this project");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");
      os.println("# subtypeNameLongName signalCompensation artifactCompensationState isInterferenceCompensatable userGain magnificationType canUseVariableMagnification droLevel stageSpeed");


      for (Subtype subtype : project.getPanel().getSubtypes())
      {
        String longSubtypeName = subtype.getLongName();
        String signalCompensationString = _enumStringLookup.getSignalCompensationEnumString(subtype.getSubtypeAdvanceSettings().getSignalCompensation());
        String artifactCompensationStateString = getArtifactCompensationStateEnumString(subtype.getSubtypeAdvanceSettings().getArtifactCompensationState());
        String isInterferenceCompensatableString = getTrueFalseString(subtype.getSubtypeAdvanceSettings().isIsInterferenceCompensatable());
        String userGainString = _enumStringLookup.getUserGainEnumString(subtype.getSubtypeAdvanceSettings().getUserGain());
        String stageSpeedString = _enumStringLookup.getstageSpeedEnumString(subtype.getSubtypeAdvanceSettings().getStageSpeedList().iterator().next());
        String magnificationTypeString = _enumStringLookup.getMagnificationTypeEnumString(subtype.getSubtypeAdvanceSettings().getMagnificationType());
        String canUseVariableMagnification = getTrueFalseString(subtype.getSubtypeAdvanceSettings().canUseVariableMagnification());
        String droLevel = _enumStringLookup.getDynamicRangeOptimizationLevelEnumString(subtype.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel());
        //subtypeNameLongName signalCompensation artifactCompensationState isInterferenceCompensatable userGain magnificationType
        _projectWriter.println(os,longSubtypeName + " "
                + signalCompensationString + " "
                + artifactCompensationStateString + " "
                + isInterferenceCompensatableString + " "
                + userGainString + " "
                + magnificationTypeString + " "
                + canUseVariableMagnification + " "
                + droLevel + " "
                + stageSpeedString);
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if(os != null)
      {
        os.close();
      }
    }
  }

/**
 * @author sham
 */
  private String getArtifactCompensationStateEnumString(ArtifactCompensationStateEnum artifactCompensationState)
  {
    Assert.expect(artifactCompensationState != null);
    
    String artifactCompensationStateString = "";
    if(artifactCompensationState.equals(ArtifactCompensationStateEnum.COMPENSATED))
    {
      artifactCompensationStateString = "compensated";
    }
    else if(artifactCompensationState.equals(ArtifactCompensationStateEnum.NOT_COMPENSATED))
    {
      artifactCompensationStateString = "notCompensated";
    }
    else if(artifactCompensationState.equals(ArtifactCompensationStateEnum.NOT_APPLICABLE))
    {
      artifactCompensationStateString = "notCompensated";
    }
    else
    {
      Assert.expect(false);
    }

    return artifactCompensationStateString;
  }
  
  /**
  * @author sham
  */
  private String getTrueFalseString(boolean trueOrFalse)
  {
    Assert.expect(trueOrFalse == true || trueOrFalse == false);

    String trueOrFalseString = "";
    if(trueOrFalse)
    {
      trueOrFalseString = "true";
    }
    else
    {
      trueOrFalseString = "false";
    }

    return trueOrFalseString;
  }
}
