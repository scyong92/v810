package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;
import com.axi.util.*;
import com.axi.v810.datastore.config.Config;
import com.axi.v810.datastore.config.SoftwareConfigEnum;
import com.axi.v810.hardware.XrayTester;

/**
 * @author Bill Darbie
 */
class PanelWriter
{
  private static PanelWriter _instance;
  private ProjectWriter _projectWriter;

  /**
   * @author Bill
   */
  static synchronized PanelWriter getInstance()
  {
    if (_instance == null)
      _instance = new PanelWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private PanelWriter()
  {
    // do nothing
  }

  /**
   * @author Bill
   */
  Map<Board, Integer> write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    Map<Board, Integer> boardToBoardInstanceNumberMap = new HashMap<Board, Integer>();

    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();
    String projName = project.getName();
    String fileName = FileName.getProjectPanelFullPath(projName, FileName.getProjectPanelLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: panel.X.cad");
      os.println("# PURPOSE: To describe where each board is located on the panel or");
      os.println("#          carrier and enter some panel information.");
      os.println("#");
      os.println("#");
      os.println("# NOTE:");
      os.println("#  - All numbers are in nanometers or degrees.");
      os.println("#  - the panel xLocation, yLocation numbers indicate the distance from the");
      os.println("#    top side of the panels bottom left corner to the top side of the");
      os.println("#    carriers bottom left corner.");
      os.println("#  - the board xLocation, yLocation numbers indicate the distance from the");
      os.println("#    top side of the panels bottom left corner to the top side of the");
      os.println("#    boards bottom left corner.");
      os.println("#  - the board rotation specifies a rotation around the boards top side bottom");
      os.println("#    left corner.  It must be 0, 90, 180, or 270.");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");
      Panel panel = project.getPanel();
      os.println("# panel width in nanometers");
      os.println(panel.getWidthInNanoMeters());
      os.println("# panel length in nanometers");
      os.println(panel.getLengthInNanoMeters());
      os.println("# panel thickness in nanometers");
      //Chin-Seong,Kee Add 10 mils for every panel when the "flag" is turn on";
      if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_FOCUS_OPTIMIZATION) == true)
        os.println(panel.getThicknessInNanometers() - XrayTester.getSystemPanelThicknessOffset());
      else
        os.println(panel.getThicknessInNanometers());
      os.println("");
      os.println("# boardInstanceNumber boardInstanceName boardType flippedLeftToRight|notFlippedLeftToRight flippedTopToBottom|notFlippedTopToBottom xLocationInNanometers yLocationInNanoMeters rotationInDegrees");
      List<BoardType> boardTypes = panel.getBoardTypes();
      int boardInstanceNumber = 0;
      for (BoardType boardType : boardTypes)
      {
        String boardTypeName = boardType.getName();
        List<Board> boards = boardType.getBoards();
        for (Board board : boards)
        {
          ++boardInstanceNumber;
          boardToBoardInstanceNumberMap.put(board, boardInstanceNumber);

          String boardName = board.getName();
          String leftToRightFlippedOrNot = "notFlippedLeftToRight";
          if (board.isLeftToRightFlip())
            leftToRightFlippedOrNot = "flippedLeftToRight";
          String topToBottomFlippedOrNot = "notFlippedTopToBottom";
          if (board.isTopToBottomFlip())
            topToBottomFlippedOrNot = "flippedTopToBottom";
          PanelCoordinate panelCoordinate = board.getCoordinateInNanoMeters();
          int xInNanoMeters = panelCoordinate.getX();
          int yInNanoMeters = panelCoordinate.getY();

          int degreesRotation = board.getDegreesRotationRelativeToPanel();
          //  boardInstanceName boardType flippedLeftToRight|notFlippedLeftToRight
          //  flippedTopToBottom|notFlippedTopToBottom xLocation yLocation xShift yShift rotation
          _projectWriter.println(os, boardInstanceNumber + " " + boardName + " " + boardTypeName + " " + leftToRightFlippedOrNot + " " +
                     topToBottomFlippedOrNot + " " + xInNanoMeters + " " + yInNanoMeters + " " +
                     degreesRotation);
        }
      }

      os.println("");
      os.println("# fiducialName side1|side2 xLocationInNanometers yLocationInNanoMeters widthInNanoMeters lengthInNanoMeters circle|rectangle");
      List<Fiducial> fiducials = panel.getFiducials();
      for (Fiducial fiducial : fiducials)
      {
        String sideBoard = "side2";
        if (fiducial.isPanelSide1())
          sideBoard = "side1";
        FiducialType fiducialType = fiducial.getFiducialType();
        String name = fiducialType.getName();
        PanelCoordinate coord = fiducialType.getCoordinateInNanoMeters();
        int xInNanos = coord.getX();
        int yInNanos = coord.getY();
        int lengthInNanos = fiducialType.getLengthInNanoMeters();
        int widthInNanos = fiducialType.getWidthInNanoMeters();
        ShapeEnum shapeEnum = fiducialType.getShapeEnum();
        String shape = "";
        if (shapeEnum.equals(ShapeEnum.CIRCLE))
          shape = "circle";
        else if (shapeEnum.equals(ShapeEnum.RECTANGLE))
          shape = "rectangle";
        else
          Assert.expect(false);

        //fiducialName side1|side2 xLocationInNanometers yLocationInNanoMeters widthInNanoMeters lengthInNanoMeters circle|rectangle
        _projectWriter.println(os, name + " " + sideBoard + " " + xInNanos + " " + yInNanos + " " +
                   widthInNanos + " " + lengthInNanos + " " + shape);

      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }

    return boardToBoardInstanceNumberMap;
  }
}
