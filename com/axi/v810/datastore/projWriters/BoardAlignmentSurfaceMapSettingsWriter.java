package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class BoardAlignmentSurfaceMapSettingsWriter 
{
  private static BoardAlignmentSurfaceMapSettingsWriter _instance;

  private ProjectWriter _projectWriter;

  /**
   * @author Cheah Lee Herng
   */
  public static synchronized BoardAlignmentSurfaceMapSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new BoardAlignmentSurfaceMapSettingsWriter();

    return _instance;
  }
    
  /**
   * @author Cheah Lee Herng
   */
  private BoardAlignmentSurfaceMapSettingsWriter()
  {
    // Do nothing
  }

  /**
   * @author Cheah Lee Herng
  */
  void write(Project project, Map<Board, Integer> boardToBoardInstanceNumberMap) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(boardToBoardInstanceNumberMap != null);

    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();

    List<Board> boards = project.getPanel().getBoards();
    for (Board board : boards)
    {
      Integer boardInstanceNumber = boardToBoardInstanceNumberMap.get(board);
      Assert.expect(boardInstanceNumber != null);
      writeBoardSurfaceMapSettings(project, board, boardInstanceNumber);
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  void writeBoardSurfaceMapSettings(Project project, Board board, int boardInstanceNumber) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(board != null);

    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();

    String projName = project.getName();
    String fileName = FileName.getProjectBoardAlignmentSurfaceMapSettingsFullPath(projName, boardInstanceNumber, FileName.getProjectAlignmentSurfaceMapSettingsLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: board_alignment_surfaceMap_X.X.settings");
      os.println("# PURPOSE: to list all surface map points used in alignment groups by this project");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");
      os.println("# alignmentGroupName yes|no");
      os.println("#    boardName Region pointPanelXCoordinateInNanometers pointPanelYCoordinateInNanometers width height scaleX shearY shearX scaleY translateX translateY");
      os.println("#      CameraRect cameraRectPanelXCoordinateInNanometers cameraRectPanelYCoordinateInNanometers widthInNanometers heightInNanometers cameraRectXCoordinateInPixel cameraRectYCoordinateInPixel widthInPixel heightInNanometer");
      os.println("#        pointSurfaceType pointPanelXCoordinateInNanometers pointPanelYCoordinateInNanometers pointPanelXCoordinateInPixel pointPanelYCoordinateInPixel yes|no");
//          os.println("#        Component refDes");

      BoardAlignmentSurfaceMapSettings boardAlignmentSurfaceMapSettings = board.getBoardAlignmentSurfaceMapSettings();
      for(Map.Entry<String, String> alignmentGroupNameEntry : boardAlignmentSurfaceMapSettings.getAlignmentGroupNameToRegionPositionNameMap().entrySet())
      {
        String alignmentGroupName = alignmentGroupNameEntry.getKey();
        String regionPositionName = alignmentGroupNameEntry.getValue();

        OpticalRegion opticalRegion = boardAlignmentSurfaceMapSettings.getOpticalRegion(regionPositionName);
        PanelRectangle panelRectangle = opticalRegion.getRegion();

        boolean setToUsePsp = opticalRegion.isSetToUsePspForAlignment();
        String setToUsePspString = "";
        if (setToUsePsp)
          setToUsePspString = "yes";
        else
          setToUsePspString = "no";

        _projectWriter.println(os, alignmentGroupName + " " + setToUsePspString);

        double scaleX = opticalRegion.getAffineTransform().getScaleX();
        double shearY = opticalRegion.getAffineTransform().getShearY();
        double shearX = opticalRegion.getAffineTransform().getShearX();
        double scaleY = opticalRegion.getAffineTransform().getScaleY();
        double translateX = opticalRegion.getAffineTransform().getTranslateX();
        double translateY = opticalRegion.getAffineTransform().getTranslateY();

        _projectWriter.println(os, board.getName() + " Region " + panelRectangle.getMinX() + " " + panelRectangle.getMinY() + " " + panelRectangle.getWidth() + " " + panelRectangle.getHeight() + " " 
                + scaleX + " " + shearY + " " + shearX + " " + scaleY + " " + translateX + " " + translateY );

        // OpticalCameraRectangle information
        for(OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getOpticalCameraRectangles(board))
        {
          PanelRectangle opticalCameraRectanglePanelRectangle = opticalCameraRectangle.getRegion();
          boolean inspectableBool = opticalCameraRectangle.getInspectableBool();
          double zHeightInNanometer = opticalCameraRectangle.getZHeightInNanometer();
          _projectWriter.println(os, "     CameraRect " + opticalCameraRectanglePanelRectangle.getMinX() + " " + opticalCameraRectanglePanelRectangle.getMinY() + " " + 
                                  opticalCameraRectanglePanelRectangle.getWidth() + " " + opticalCameraRectanglePanelRectangle.getHeight() + " " + 
                                  inspectableBool + " " + zHeightInNanometer);

          for(PanelCoordinate panelCoordinate : opticalCameraRectangle.getPanelCoordinateList())
          {
            //String xCoordinateInNanometerString = String.valueOf(panelCoordinate.getX());
            //String yCoordinateInNanometerString = String.valueOf(panelCoordinate.getY());
            //boolean isVerified = opticalRegion.getPointPanelPositionVerifiedFlag(xCoordinateInNanometerString + "_" + yCoordinateInNanometerString);
            String verifiedFlagString = "yes"; // This flag is no longer used. Remove it next time.

            // Point PointStageXCoordinatesInNanometers PointStageYCoordinatesInNanometers yes|no
            _projectWriter.println(os, "      Point " + panelCoordinate.getX() + " " + panelCoordinate.getY() + " " + verifiedFlagString);
          }

//                    for(Component component : opticalRegion.getComponents())
//                    {
//                        // Component refDes
//                        _projectWriter.println(os, "     Component " + component.getReferenceDesignator());
//                    }
        }
      }                         
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
