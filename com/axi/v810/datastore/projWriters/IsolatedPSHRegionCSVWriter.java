package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.testProgram.*;

/**
 *
 * @author khang-wah.chnee
 */
public class IsolatedPSHRegionCSVWriter
{
  private static IsolatedPSHRegionCSVWriter _instance;
  
  private ProjectWriter _projectWriter;
  private EnumStringLookup _enumStringLookup;
  
  /**
   * @author Chnee Khang Wah
   */
  static synchronized IsolatedPSHRegionCSVWriter getInstance()
  {
    if (_instance == null)
      _instance = new IsolatedPSHRegionCSVWriter();

    return _instance;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  private IsolatedPSHRegionCSVWriter()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }
  
  /**
   * @author Chnee Khang Wah
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();

    String projName = project.getName();
    String fileName = FileName.getProjectIsolatedPSHRegionCSVFullPath(projName);

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: isolatedPSHRegion.csv");
      os.println("# PURPOSE: to list all isolated PSH region in this project");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("##############################################################################");
     // os.println("# subtypeNameLongName signalCompensation artifactCompensationState isInterferenceCompensatable userGain magnificationType canUseVariableMagnification");
      os.println("# testSubProgram, refDes, subtypeNameLongName, userGain, stageSpeed, artifactCompensationState, signalCompensation(IL),  magnificationType, DRO");
        
      Collection<String> refDegs = new ArrayList<String>();
      
      for(TestSubProgram testSubProgram : project.getTestProgram().getFilteredScanPathTestSubPrograms())
      {
        String testSubProgramId = Integer.toString(testSubProgram.getId());
        
        for(ReconstructionRegion region : testSubProgram.getIsolatedGlobalSurfaceRegion())
        {
          Subtype subtype = region.getComponent().getPadOne().getSubtype();
          
          String refDes = region.getComponent().getReferenceDesignator();
          String longSubtypeName = subtype.getLongName();
          String signalCompensationString = _enumStringLookup.getSignalCompensationEnumString(subtype.getSubtypeAdvanceSettings().getSignalCompensation());
          String artifactCompensationStateString = getArtifactCompensationStateEnumString(subtype.getSubtypeAdvanceSettings().getArtifactCompensationState());
          String userGainString = _enumStringLookup.getUserGainEnumString(subtype.getSubtypeAdvanceSettings().getUserGain());
          String magnificationTypeString = _enumStringLookup.getMagnificationTypeEnumString(subtype.getSubtypeAdvanceSettings().getMagnificationType());
          Collection<String> stageSpeedString = new ArrayList<String>();
          for (StageSpeedEnum speedEnum : subtype.getSubtypeAdvanceSettings().getStageSpeedList())
          {
            stageSpeedString.add(speedEnum.toString());
          }
          String stageSpeed = StringUtil.join(stageSpeedString, ',');
          String droLevel = _enumStringLookup.getDynamicRangeOptimizationLevelEnumString(subtype.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel());
        
          if(refDegs.contains(refDes))
            continue;
          
          refDegs.add(refDes);
          
          //subtypeNameLongName signalCompensation artifactCompensationState isInterferenceCompensatable userGain magnificationType
          _projectWriter.println(os, testSubProgramId + ", "
                + refDes + ", "
                + longSubtypeName + ", "
                + userGainString + ", "
                + stageSpeed + ", "
                + artifactCompensationStateString + ", "
                + signalCompensationString + ", "
                + magnificationTypeString +", "
                + droLevel);
        }
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if(os != null)
      {
        os.close();
      }
    }
  }
  
 /**
 * @author sham
 */
  private String getArtifactCompensationStateEnumString(ArtifactCompensationStateEnum artifactCompensationState)
  {
    Assert.expect(artifactCompensationState != null);
    
    String artifactCompensationStateString = "";
    if(artifactCompensationState.equals(ArtifactCompensationStateEnum.COMPENSATED))
    {
      artifactCompensationStateString = "compensated";
    }
    else if(artifactCompensationState.equals(ArtifactCompensationStateEnum.NOT_COMPENSATED))
    {
      artifactCompensationStateString = "notCompensated";
    }
    else if(artifactCompensationState.equals(ArtifactCompensationStateEnum.NOT_APPLICABLE))
    {
      artifactCompensationStateString = "notCompensated";
    }
    else
    {
      Assert.expect(false);
    }

    return artifactCompensationStateString;
  }
  
  /**
  * @author sham
  */
  private String getTrueFalseString(boolean trueOrFalse)
  {
    Assert.expect(trueOrFalse == true || trueOrFalse == false);

    String trueOrFalseString = "";
    if(trueOrFalse)
    {
      trueOrFalseString = "true";
    }
    else
    {
      trueOrFalseString = "false";
    }

    return trueOrFalseString;
  }
}
