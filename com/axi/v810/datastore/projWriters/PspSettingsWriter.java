/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.projWriters;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelSettings.*;
/**
 *
 * @author Administrator
 */
public class PspSettingsWriter
{
  public static String _COMMA_DELIMITER = ","; 

  private static PspSettingsWriter _instance;

  private ProjectWriter _projectWriter;
  
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized PspSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new PspSettingsWriter();

    return _instance;
  }
    
  /**
   * @author Cheah Lee Herng
   */
  private PspSettingsWriter()
  {
    // Do nothing
  }
  
   /**
   * @author Cheah Lee Herng
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();
    String projName = project.getName();
    String fileName = FileName.getProjectPspSettingsFullPath(projName, FileName.getProjectPspSettingsLatestFileVersion());
    PspSettings pspSettings =  project.getPanel().getPspSettings();

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: psp.X.settings");
      os.println("# PURPOSE: to list all surface map points used by this project");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");      
      os.println("# autoPopulateSpacing");
      _projectWriter.println(os, Integer.toString(pspSettings.getAutoPopulateSpacingInNanometers()));
      os.println("# isRemoveComponentOnTopOfBoard");
      _projectWriter.println(os, String.valueOf(pspSettings.isRemoveRectanglesOnTopOfComponent()));
      os.println("# isExtendRectanglesToEdge");
      _projectWriter.println(os, String.valueOf(pspSettings.isExtendRectanglesToEdgeOfBoard()));
      os.println("# isShowMeshTriangle");
      _projectWriter.println(os, String.valueOf(pspSettings.isShowMeshTriangle()));
      os.println("# isApplyToAllBoard");
      _projectWriter.println(os, String.valueOf(pspSettings.isApplyToAllBoard()));
      os.println("# isAutomaticallyIncludeAllComponent");
      _projectWriter.println(os, String.valueOf(pspSettings.isAutomaticallyIncludeAllComponents()));
      os.println("# isUseMeshTriangle");
      _projectWriter.println(os, String.valueOf(pspSettings.isUseMeshTriangle()));
      os.println("# currentSetting");
      _projectWriter.println(os, String.valueOf(pspSettings.getCurrentSetting()));
      //Ying-Huan.Chu - temporary commented Larger FOV feature for 5.7 release.
      //os.println("# isLargerFov");
      //_projectWriter.println(os, String.valueOf(pspSettings.isLargerFOV()));
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
      {
        os.close();
      }
    }
  }
  
}
