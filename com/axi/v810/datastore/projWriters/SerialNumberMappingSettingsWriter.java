package com.axi.v810.datastore.projWriters;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;

/**
 * @author siew-yeng.phang
 */
public class SerialNumberMappingSettingsWriter
{
  private static SerialNumberMappingSettingsWriter _instance;
  private String _fileName = "";

  /**
   * @author Phang Siew Yeng
   */
  public static synchronized SerialNumberMappingSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new SerialNumberMappingSettingsWriter();

    return _instance;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  private SerialNumberMappingSettingsWriter()
  {

  }
  
  /**
   * @author Phang Siew Yeng
   */
  public void write(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);

    _fileName = FileName.getSerialNumberMappingSettingsFullPath(projectName);

    FileWriter fw = null;
    try
    {
      fw = new FileWriter(_fileName);
    }
    catch(IOException ioe)
    {
      CannotOpenFileDatastoreException ex = new CannotOpenFileDatastoreException(_fileName);
      ex.initCause(ioe);
      throw ex;
    }
    PrintWriter os = new PrintWriter(fw);

    // ok - now write out the file
    os.println("#######################################################################");
    os.println("# FILE: serialNumberMapping.settings");
    os.println("# PURPOSE: To store all serial number sequence mapping with ");
    os.println("#          barcode readers and board name.");
    os.println("#          To store barcode reader configuration used in production.");
    os.println("#");
    os.println("########################################################################");
    os.println("# barcodeReaderConfigurationNameToUse");
    os.println(SerialNumberMappingManager.getInstance().getBarcodeReaderConfigurationNameUsed());
    os.println("# boardName barcodeReaderId scanSequence");

    for(SerialNumberMappingData data: SerialNumberMappingManager.getInstance().getSerialNumberMappingDataList())
    {
      os.println(data.getBoardName()+" "+data.getBarcodeReaderId()+" "+data.getScanSequence());
    }
    os.println(""); 
    os.close();
  }
}
