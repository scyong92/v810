package com.axi.v810.datastore.projWriters;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class Test_ProjectWriter extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ProjectWriter());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    List<String> projectNames = new ArrayList<String>();

    try
    {
      // clean out the project directory so it is in a known state
      // good small panel to try
      projectNames.add("smallPanel");
      // same ref des on different boards using same land pattern and component package
      projectNames.add("hp5dx_1024");
      // 2 sides, bottom side rotated differnt than top
      projectNames.add("families_all_rlv");
      // 2 boardTypes, 6 instances
      projectNames.add("242_hetero");

      for (String projectName : projectNames)
      {
        String fullPathProjectName = Directory.getProjectDir(projectName);
        if (FileUtilAxi.exists(fullPathProjectName))
        {
          // Move any pre-existing directory contents to a temp directory.
          String tempDir = Directory.getTempDir() + File.separator + projectName;
          if (FileUtil.exists(tempDir) == false)
          {
            FileUtil.createDirectory(tempDir);
          }
          else
          {
            FileUtilAxi.deleteDirectoryContents(tempDir);
          }

          FileUtilAxi.moveDirectoryContents(fullPathProjectName, tempDir);
        }

        // import some ndfs
        List<LocalizedString> warnings = new ArrayList<LocalizedString>();
        Project project = Project.importProjectFromNdfs(projectName, warnings);
        // write them out in the new format
        project.setNotes("notes\n\ntest");
        ProjectWriter projectWriter = ProjectWriter.getInstance();
        projectWriter.save(project,true);

        // now load the serialized project file
        project = null;
        ProjectReader projectReader = ProjectReader.getInstance();
        project = projectReader.load(projectName);
        project.setName(projectName);
        ProjectSummaryReader.getInstance().read(projectName);

        // make sure a save/load works after a serialized load
        // generate a test program so we are testing if it gets serialized properly as well
        project.getTestProgram();
        project.save();
        project.getProjectState().resetEnabledStateForUnitTest();
        project = projectReader.load(projectName);
        project.setName(projectName);
        ProjectSummaryReader.getInstance().read(projectName);

        // try doing a few things to make sure we do not get any asserts
        Panel panel = project.getPanel();
        int numJoints = panel.getNumJoints();
        Assert.expect(numJoints > 0);

        // now remove the .project file and make sure a project file (text file) load will work
        String projectFile = FileName.getProjectSerializedFullPath(projectName);
        FileUtilAxi.delete(projectFile);
        project.getProjectState().resetEnabledStateForUnitTest();
        project = null;
        project = projectReader.load(projectName);
        project.setName(projectName);
        ProjectSummaryReader.getInstance().read(projectName);

        // make sure a save/load works after a project load (text files)
        project.save();
        projectFile = FileName.getProjectSerializedFullPath(projectName);
        FileUtilAxi.delete(projectFile);
        project.getProjectState().resetEnabledStateForUnitTest();
        project = null;
        project = projectReader.load(projectName);
        project.setName(projectName);
        ProjectSummaryReader.getInstance().read(projectName);

        // now set things up so we exercise all text parsing paths possible
        if (projectName.equalsIgnoreCase("smallPanel"))
        {
          // create a panel fiducial on the top side
          Fiducial fid = panel.createFiducial("fiducial1", true);
          FiducialType fidType = fid.getFiducialType();
          fidType.setCoordinateInNanoMeters(new PanelCoordinate(10, 10));

          // create a panel fiducial on the bottom side
          fid = panel.createFiducial("fiducial2", false);
          fidType = fid.getFiducialType();
          fidType.setCoordinateInNanoMeters(new PanelCoordinate(10, 10));

          // add a fiducial to the alignment group
          AlignmentGroup alignmentGroup = panel.getPanelSettings().getAlignmentGroupsForShortPanel().get(1);
          alignmentGroup.addFiducial(fid);

          // set board to not be inspected
          List<Board> boards = panel.getBoards();
          Board board = boards.get(0);
          BoardSettings boardSettings = board.getBoardSettings();
          boardSettings.setInspected(false);

          // add a board fiducial
          BoardType boardType = board.getBoardType();
          SideBoardType sideBoardType = boardType.getSideBoardType1();
          fidType = sideBoardType.createFiducialType("boardFid1", new BoardCoordinate(20,20));

          // set one component to not be inspected
          List<Component> components = board.getComponents();
          Component component = components.get(0);
          component.getComponentType().getComponentTypeSettings().setInspected(false);

          // set one pad to not be inspected
          component = components.get(1);
          PadType padType = component.getComponentType().getPadTypes().get(0);
          padType.getPadTypeSettings().setInspected(false);

          // set one pad to not be testable
          component = components.get(2);
          padType = component.getComponentType().getPadTypes().get(0);
//          padType.getPadTypeSettings().setTestable(false);
          padType.getPads().get(0).getPadSettings().setTestable(false);

          board = boards.get(1);
          components = board.getComponents();
          // set a component to be no load
          component = components.get(0);
          component.getComponentTypeSettings().setLoaded(false);

          // set some project notes
          project.setNotes("here are\n\nsome\nnotes  .");

          // set subtype comments
          List<Subtype> subtypes = panel.getInspectedSubtypes();
          Subtype subtype = subtypes.get(0);
          subtype.setUserComment("subtype user\ncomment");
          AlgorithmSetting algSetting = subtype.getAlgorithms().get(0).getAlgorithmSettings(subtype.getJointTypeEnum()).get(0);
          subtype.setAlgorithmSettingUserComment(algSetting.getAlgorithmSettingEnum(), "algorithm setting user\ncomment");

          // set a tuned value
          subtype.setTunedValue(algSetting.getAlgorithmSettingEnum(), "2");

          // remove all pads from a landpattern
          LandPattern landPattern = panel.getAllLandPatterns().get(0);
          for (LandPatternPad landPatternPad : landPattern.getLandPatternPads())
            landPatternPad.destroy();

          // add a manual transform
          panel = project.getPanel();
          PanelSettings panelSettings = panel.getPanelSettings();
          AffineTransform trans = AffineTransform.getTranslateInstance(10, 20);
          panelSettings.setRightManualAlignmentTransform(trans);

          // add alignment groups
          List<Pad> topSidePads = new ArrayList<Pad>();
          List<Pad> pads = panel.getPads();
          for (Pad pad : pads)
          {
            if (pad.isTopSide())
              topSidePads.add(pad);
          }

          Assert.expect(topSidePads.size() > 6);
          Iterator<Pad> it = topSidePads.iterator();
          AlignmentGroup group = new AlignmentGroup(panelSettings);
          group.setName("11");
          group.addPad(it.next());
          panelSettings.addAlignmentGroup(group);
          group = new AlignmentGroup(panelSettings);
          group.setName("12");
          group.addPad(it.next());
          group.addPad(it.next());
          panelSettings.addAlignmentGroup(group);

          // save the project to disk
          project.save();

          // now make sure we can parse the project files
          projectFile = FileName.getProjectSerializedFullPath(projectName);
          FileUtilAxi.delete(projectFile);
          project.getProjectState().resetEnabledStateForUnitTest();
          project = null;
          project = projectReader.load(projectName);
          project.setName(projectName);
          ProjectSummaryReader.getInstance().read(projectName);
        }

        // check that components are converted to fiducials properly
        if (projectName.equalsIgnoreCase("families_all_rlv"))
        {
          for (Fiducial fiducial : project.getPanel().getFiducials())
          {
            System.out.println("panel fiducial for project " + projectName + " : " + fiducial.getName());
          }

          for (Board board : project.getPanel().getBoards())
          {
            for (Fiducial fiducial : board.getFiducials())
            {
              System.out.println("board fiducial for project " + projectName + " and board " + board.getName() + " : " +
                                 fiducial.getName());
            }
          }
        }
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      // Restore the project directories to their original state.
      for (String projectName : projectNames)
      {
        String fullPathProjectName = Directory.getProjectDir(projectName);
        if (FileUtilAxi.exists(fullPathProjectName))
        {
          // Delete the project directory and move the original contents back.
          String tempDir = Directory.getTempDir() + File.separator + projectName;
          if (FileUtil.exists(tempDir))
          {
            if (FileUtil.exists(fullPathProjectName))
            {
              try
              {
                FileUtilAxi.deleteDirectoryContents(fullPathProjectName);
                FileUtilAxi.moveDirectoryContents(tempDir, fullPathProjectName);
              }
              catch (DatastoreException dex)
              {
                // Oops...an error occured while restoring the directory.
                dex.printStackTrace();
              }
            }
          }
        }
      }
    }
  }
}
