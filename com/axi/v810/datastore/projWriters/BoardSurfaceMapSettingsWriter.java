package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.ndfReaders.*;
import com.axi.v810.util.*;

/**
 * This class reads in surfaceMap.X.settings
 * 
 * @author Cheah Lee Herng
 */
public class BoardSurfaceMapSettingsWriter 
{
    private static BoardSurfaceMapSettingsWriter _instance;

    private ProjectWriter _projectWriter;
    
    /**
     * @author Cheah Lee Herng
     */
    public static synchronized BoardSurfaceMapSettingsWriter getInstance()
    {
        if (_instance == null)
          _instance = new BoardSurfaceMapSettingsWriter();

        return _instance;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private BoardSurfaceMapSettingsWriter()
    {
        // Do nothing
    }
    
    /**
     * @author Cheah Lee Herng
    */
    void write(Project project, Map<Board, Integer> boardToBoardInstanceNumberMap) throws DatastoreException
    {
        Assert.expect(project != null);
        Assert.expect(boardToBoardInstanceNumberMap != null);
        
        if (_projectWriter == null)
            _projectWriter = ProjectWriter.getInstance();
        
        List<Board> boards = project.getPanel().getBoards();
      //Punit: Handling Assert for Invalid NDF
      for (Board board : boards)
      {
        Integer boardInstanceNumber = boardToBoardInstanceNumberMap.get(board);
        //Assert.expect(boardInstanceNumber != null);      
        if(boardInstanceNumber == null)
           throw new InvalidNdfException();
        
        if(project.getPanel().hasBoardType(board.getBoardType().getName()) == true)
          writeBoardSurfaceMapSettings(project, board, boardInstanceNumber);
      }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    void writeBoardSurfaceMapSettings(Project project, Board board, int boardInstanceNumber) throws DatastoreException
    {
        Assert.expect(project != null);
        Assert.expect(board != null);
        
        if (_projectWriter == null)
          _projectWriter = ProjectWriter.getInstance();

        String projName = project.getName();
        String fileName = FileName.getProjectBoardSurfaceMapSettingsFullPath(projName, boardInstanceNumber, FileName.getProjectBoardSurfaceMapSettingsLatestFileVersion());
        
        PrintWriter os = null;
        try
        {
          os = new PrintWriter(fileName);
          
          os.println("##############################################################################");
          os.println("# FILE: board_surfaceMap.X.settings");
          os.println("# PURPOSE: to list all surface map points used by this project");
          os.println("#");
          os.println("# NOTE: Hand editing of this file is not supported");
          os.println("#       The format of this file can be changed at any time with new");
          os.println("#       software releases.");
          os.println("#       If you want to write a script that uses information from this file");
          os.println("#       it is highly recommended that you use the cad.id.xml and");
          os.println("#       settings.id.xml files to get your data instead.");
          os.println("##############################################################################");  
          os.println("# Region panelXCoordinateInNanometers panelYCoordinateInNanometers panelCoordinateWidthInNanometer panelCoordinateHeightInNanometer");
          os.println("   # CameraRect cameraRectPanelXCoordinateInNanometers cameraRectPanelYCoordinateInNanometers cameraRectPanelWidthInNanometers cameraRectPanelHeightInNanometers true|false z-height");
      os.println("      # Point pointPanelXCoordinateInNanometers pointPanelYCoordinateInNanometers  yes|no");
      os.println("      # Component refDes");
          
          BoardSurfaceMapSettings boardSurfaceMapSettings = board.getBoardSurfaceMapSettings();
          if (boardSurfaceMapSettings.isDataAvailable())
          {   
           for (OpticalRegion opticalRegion : boardSurfaceMapSettings.getOpticalRegionPositionNameToOpticalRegionMap().values())
            {
              PanelRectangle panelRectangle = opticalRegion.getRegion();

              _projectWriter.println(os, "Region " + panelRectangle.getMinX() + " " + panelRectangle.getMinY() + " " + panelRectangle.getWidth() + " " + panelRectangle.getHeight());
              
              // OpticalCameraRectangle information
              for(OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getOpticalCameraRectangles(board))
              {
                boolean inspectableBool = opticalCameraRectangle.getInspectableBool();
                double zHeightInNanometer = opticalCameraRectangle.getZHeightInNanometer();
                PanelRectangle opticalCameraRectanglePanelRectangle = opticalCameraRectangle.getRegion();
                _projectWriter.println(os, "   CameraRect " + opticalCameraRectanglePanelRectangle.getMinX() + " " + opticalCameraRectanglePanelRectangle.getMinY() + " " + 
                                            opticalCameraRectanglePanelRectangle.getWidth() + " " + opticalCameraRectanglePanelRectangle.getHeight() + " " + 
                                            inspectableBool + " " + zHeightInNanometer);

                for(PanelCoordinate panelCoordinate : opticalCameraRectangle.getPanelCoordinateList())
                {
                  //String xCoordinateInNanometerString = String.valueOf(panelCoordinate.getX());
                  //String yCoordinateInNanometerString = String.valueOf(panelCoordinate.getY());
                  //boolean isVerified = opticalRegion.getPointPanelPositionVerifiedFlag(xCoordinateInNanometerString + "_" + yCoordinateInNanometerString);
                  String verifiedFlagString = "yes"; // This flag is no longer used. Remove it next time.

                  // Point PointStageXCoordinatesInNanometers PointStageYCoordinatesInNanometers yes|no
                  _projectWriter.println(os, "      Point " + panelCoordinate.getX() + " " + panelCoordinate.getY() + " " + verifiedFlagString);
                }
              }
              for (Component component : opticalRegion.getComponents(board))
              {
                // Component refDes
                _projectWriter.println(os, "      Component " + component.getReferenceDesignator());
              }
            }
          }                              
        }
        catch (FileNotFoundException ex)
        {
          FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
          dex.initCause(ex);
          throw dex;
        }
        finally
        {
          if (os != null)
            os.close();
        }
    }
}
