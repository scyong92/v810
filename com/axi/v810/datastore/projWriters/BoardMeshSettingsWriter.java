package com.axi.v810.datastore.projWriters;

import java.util.*;
import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.ndfReaders.*;

/**
 * This class reads in board_mesh.X.settings
 * 
 * @author Cheah Lee Herng
 */
public class BoardMeshSettingsWriter 
{
  private static BoardMeshSettingsWriter _instance;
  private ProjectWriter _projectWriter;

  /**
   * @author Cheah Lee Herng 
   */
  public static synchronized BoardMeshSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new BoardMeshSettingsWriter();

    return _instance;
  }

  /**
   * @author Cheah Lee Herng
   */
  public BoardMeshSettingsWriter()
  {
    // Do nothing
  }

  /**
   * @author Cheah Lee Herng 
   */
  void write(Project project, Map<Board, Integer> boardToBoardInstanceNumberMap) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(boardToBoardInstanceNumberMap != null);

    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();

    List<Board> boards = project.getPanel().getBoards();
    //Punit: Handling Assert for Invalid NDF
    for (Board board : boards)
    {
      Integer boardInstanceNumber = boardToBoardInstanceNumberMap.get(board);
      //Assert.expect(boardInstanceNumber != null);
      if(boardInstanceNumber == null)
        throw new InvalidNdfException();
      
      if(project.getPanel().hasBoardType(board.getBoardType().getName()) == true)
        writeBoardMeshSettings(project, board, boardInstanceNumber);
    }
  }

  /**
   * @author Cheah Lee Herng 
   */
  void writeBoardMeshSettings(Project project, Board board, int boardInstanceNumber) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(board != null);

    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();

    String projName = project.getName();
    String fileName = FileName.getProjectMeshSettingsFullPath(projName, boardInstanceNumber, FileName.getProjectMeshSettingsLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: board_mesh.X.settings");
      os.println("# PURPOSE: to list all triangle mesh points used by this project");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");
      os.println("# point1PanelXCoordinateInNanometers point1PanelYCoordinateInNanometers point1RegionWidthInNanometers point1RegionHeightInNanometers "
                   + "point2PanelXCoordinateInNanometers point2PanelYCoordinateInNanometers point2RegionWidthInNanometers point2RegionHeightInNanometers "
                   + "point3PanelXCoordinateInNanometers point3PanelYCoordinateInNanometers point3RegionWidthInNanometers point3RegionHeightInNanometers");

      BoardMeshSettings boardMeshSettings = board.getBoardMeshSettings();
      for(MeshTriangle meshTriangle : boardMeshSettings.getMeshTriangleList())
      {
        _projectWriter.println(os, meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getMinX() + " " + 
                                     meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getMinY() + " " + 
                                     meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getWidth() + " " +
                                     meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getHeight() + " " +
                                     meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getMinX() + " " + 
                                     meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getMinY() + " " +                  
                                     meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getWidth() + " " +
                                     meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getHeight() + " " +                  
                                     meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getMinX() + " " + 
                                     meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getMinY() + " " + 
                                     meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getWidth() + " " +
                                     meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getHeight());
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
        os.close();
    }
  }
}
