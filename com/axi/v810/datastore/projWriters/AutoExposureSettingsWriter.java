package com.axi.v810.datastore.projWriters;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import java.io.*;
import java.util.*;

/**
 * @author sheng-chuan.yong
 */
public class AutoExposureSettingsWriter
{

  private static AutoExposureSettingsWriter _instance;

  private ProjectWriter _projectWriter;
  private EnumStringLookup _enumStringLookup;

  /**
   * @author Bill
   */
  static synchronized AutoExposureSettingsWriter getInstance()
  {
    if (_instance == null)
    {
      _instance = new AutoExposureSettingsWriter();
    }

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private AutoExposureSettingsWriter()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * @author Bill Darbie
   * @author George Booth
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    if (_projectWriter == null)
    {
      _projectWriter = ProjectWriter.getInstance();
    }

    String projName = project.getName();
    String fileName = FileName.getProjectAutoExposureSettingsFullPath(projName, FileName.getProjectAutoExposureSettingsLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: autoExposure.X.settings");
      os.println("# PURPOSE: to list all subtypes's Exposure settings used by this project");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");
      os.println("# subtypeNameLongName SpeedCombination CameraSelection");

      for (Subtype subtype : project.getPanel().getSubtypes())
      {
        String longSubtypeName = subtype.getLongName();
        Collection<String> speedString = new ArrayList<String>();
        for (StageSpeedEnum speedEnum : subtype.getSubtypeAdvanceSettings().getStageSpeedList())
        {
          speedString.add(speedEnum.toString());
        }

        Collection<String> enabledCameras = new ArrayList<String>();
        for (Integer cameraId : subtype.getSubtypeAdvanceSettings().getCameraEnabledIDList())
        {
          enabledCameras.add(cameraId.toString());
        }

        String stageSpeedString = StringUtil.join(speedString, ',');
        String enabledCameraString = StringUtil.join(enabledCameras, ',');
        _projectWriter.println(os, longSubtypeName + " "
          + stageSpeedString + " "
          + enabledCameraString);
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
      {
        os.close();
      }
    }
  }
}
