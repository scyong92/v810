package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.testResults.*;
import com.axi.v810.util.*;

/**
 * This class will write an entire project out to disk.
 *
 * @author Bill Darbie
 */
public class ProjectWriter
{
  private static ProjectWriter _instance;
  private boolean _displayTimes = false;

  private ProjectSummaryWriter _projectSummaryWriter;
  private PanelWriter _panelWriter;
  private BoardTypeWriter _boardTypeWriter;
  private LandPatternWriter _landPatternWriter;
  private CompPackageWriter _compPackageWriter;

  private ProjectSettingsWriter _projectSettingsWriter;
  private PanelSettingsWriter _panelSettingsWriter;
  private BoardSettingsWriter _boardSettingsWriter;
  private SubtypeSettingsWriter _subtypeSettingsWriter;
  private SubtypeAdvanceSettingsWriter _subtypeAdvanceSettingsWriter;
  private BoardTypeSettingsWriter _boardTypeSettingsWriter;
  private ComponentNoLoadSettingsWriter _componentNoLoadSettingsWriter;
  private AlignmentTransformSettingsWriter _alignmentTransformSettingsWriter;
  private CompPackageSettingsWriter _compPackageSettingsWriter;
  private PanelSurfaceMapSettingsWriter _panelSurfaceMapSettingsWriter;
  //private PanelAlignmentSurfaceMapSettingsWriter _panelAlignmentSurfaceMapSettingsWriter;
  private BoardSurfaceMapSettingsWriter _boardSurfaceMapSettingsWriter;
  //private BoardAlignmentSurfaceMapSettingsWriter _boardAlignmentSurfaceMapSettingsWriter;
  private PanelMeshSettingsWriter _panelMeshSettingsWriter;
  private BoardMeshSettingsWriter _boardMeshSettingsWriter;
  private PspSettingsWriter _pspSettingsWriter;
  private AutoExposureSettingsWriter _autoExposureSettingWriter;

  //Kee Chin Seong - Focus Region Setting Writer
  private PanelFocusRegionSettingsWriter _panelFocusRegionSettingsWriter;
  
  //Siew Yeng - XCR-3781
  private PshSettingsWriter _pshSettingsWriter;

  private ProjectSettingsXMLWriter _projectSettingsXMLWriter;
  private ProjectCadXMLWriter _projectCadXMLWriter;
  private ImageManager _imageManager;

  private List<Pair<String, String>> backedUpFileList = new ArrayList<Pair<String, String>>();
  
  private IsolatedPSHRegionCSVWriter _isolatedPSHRegionCSVWriter;
  
  //Khaw Chek Hau - XCR3554: Package on package (PoP) development
  private PackageOnPackageWriter _packageOnPackageWriter;
  
  /**
   * @author Bill Darbie
   */
  public static synchronized ProjectWriter getInstance()
  {
    if (_instance == null)
      _instance = new ProjectWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private ProjectWriter()
  {
    _projectSummaryWriter = ProjectSummaryWriter.getInstance();
    _panelWriter = PanelWriter.getInstance();
    _boardTypeWriter = BoardTypeWriter.getInstance();
    _compPackageWriter = CompPackageWriter.getInstance();
    _landPatternWriter = LandPatternWriter.getInstance();

    _projectSettingsWriter = ProjectSettingsWriter.getInstance();
    _panelSettingsWriter = PanelSettingsWriter.getInstance();
    _boardSettingsWriter = BoardSettingsWriter.getInstance();
    _subtypeSettingsWriter = SubtypeSettingsWriter.getInstance();
    _subtypeAdvanceSettingsWriter = SubtypeAdvanceSettingsWriter.getInstance();
    _boardTypeSettingsWriter = BoardTypeSettingsWriter.getInstance();
    _componentNoLoadSettingsWriter = ComponentNoLoadSettingsWriter.getInstance();
    _alignmentTransformSettingsWriter = AlignmentTransformSettingsWriter.getInstance();
    _compPackageSettingsWriter = CompPackageSettingsWriter.getInstance();
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    _packageOnPackageWriter = PackageOnPackageWriter.getInstance();
    
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP))
    {
        _panelSurfaceMapSettingsWriter = PanelSurfaceMapSettingsWriter.getInstance();
        //_panelAlignmentSurfaceMapSettingsWriter = PanelAlignmentSurfaceMapSettingsWriter.getInstance();
        _boardSurfaceMapSettingsWriter = BoardSurfaceMapSettingsWriter.getInstance();
        //_boardAlignmentSurfaceMapSettingsWriter = BoardAlignmentSurfaceMapSettingsWriter.getInstance();
        _panelMeshSettingsWriter = PanelMeshSettingsWriter.getInstance();
        _boardMeshSettingsWriter = BoardMeshSettingsWriter.getInstance();
        _pspSettingsWriter = PspSettingsWriter.getInstance();
    }
    _autoExposureSettingWriter = AutoExposureSettingsWriter.getInstance();
    //Focus Region Writer
    _panelFocusRegionSettingsWriter = PanelFocusRegionSettingsWriter.getInstance();
    
    //Siew Yeng - XCR-3781 - surface model settings
    _pshSettingsWriter = PshSettingsWriter.getInstance();

    _projectSettingsXMLWriter = ProjectSettingsXMLWriter.getInstance();
    _projectCadXMLWriter = ProjectCadXMLWriter.getInstance();
    _imageManager = ImageManager.getInstance();
    
    _isolatedPSHRegionCSVWriter = IsolatedPSHRegionCSVWriter.getInstance();
  }

  /**
   * Save the project to disk.
   *
   * @author Bill Darbie
   */
  public void save(Project project, boolean isFastSave) throws DatastoreException
  {
    Assert.expect(project != null);

    String projName = project.getName();
    String tempDir = "";
    List<String> dontMoveDirOrFiles = new LinkedList<String>();
    String projDir = "";
    boolean exception = false;
    String projectCreationVersionNumber = project.getSoftwareVersionOfProjectLastSave();
    boolean needToBackupOlderVersionProjectFiles = false;
    
    if (VersionUtil.isOlderVersion(projectCreationVersionNumber))
      needToBackupOlderVersionProjectFiles = true;

    String projectsDir = Directory.getProjectsDir();
    if (FileUtilAxi.exists(projectsDir) == false)
      FileUtilAxi.mkdirs(projectsDir);
    // backup all files in the current project directory if it exists
    projDir = Directory.getProjectDir(projName);
    if (FileUtilAxi.exists(projDir))
    {
      tempDir = FileUtilAxi.createTempDir(projDir);
      Collection<String> allSubDirs = FileUtilAxi.listAllSubDirectoriesFullPathInDirectory(projDir);
      dontMoveDirOrFiles.addAll(allSubDirs);
      try
      {
        TimerUtil timer = new TimerUtil();
        timer.start();
        FileUtilAxi.copyDirectoryRecursively(projDir, tempDir, dontMoveDirOrFiles);
        timer.stop();
        if ((_displayTimes) && (UnitTest.unitTesting() == false))
          System.out.println("wpd: time to copy project directory (in case the write fails), in millis:" + timer.getElapsedTimeInMillis());
      }
      catch (DatastoreException dex)
      {
        // copy back whatever files went into the temp directory before throwing the original exception
        try
        {
          if (FileUtilAxi.exists(tempDir))
          {
            FileUtilAxi.moveDirectoryContents(tempDir, projDir);
            FileUtilAxi.delete(tempDir);
          }
        }
        catch (DatastoreException dex2)
        {
          // if this failed don't worry about it - we did the best we could
        }
        throw dex;
      }
    }

    try
    {
      // now write out the new files
      write(project);

      // save the serialized file last since its time stamp must be the most recent!
      if(isFastSave)
      {
        if(FileUtilAxi.exists(FileName.getProjectSerializedFullPath(projName)))
          FileUtilAxi.delete(FileName.getProjectSerializedFullPath(projName));
      }
      else
        saveProjectAsSerializedFile(project);
    }
    catch (DatastoreException dex)
    {
      exception = true;
      throw dex;
    }
    catch (RuntimeException rex)
    {
      exception = true;
      throw rex;
    }
    finally
    {
      if (exception)
      {
        // remove the current directory contents and return the original directory
        // back to its previous state
        try
        {
          if (FileUtilAxi.exists(projDir))
          {
            List<String> dontDeleteFilesOrDirectories = new ArrayList<String>();
            dontDeleteFilesOrDirectories.add(tempDir);
            FileUtilAxi.deleteDirectoryContents(projDir, dontDeleteFilesOrDirectories);
            FileUtilAxi.moveDirectoryContents(tempDir, projDir);
            FileUtilAxi.delete(tempDir);
          }
        }
        catch (DatastoreException dex2)
        {
          dex2.printStackTrace();
        }
      }
      if (FileUtilAxi.exists(tempDir))
      {
        if (needToBackupOlderVersionProjectFiles && exception == false)
        {
          // Now we should make the files in temporary directory to be our backup.
          String oldProjectFilesBackupDirectoryName = projDir + File.separator + projectCreationVersionNumber + FileName.getBackupExtension();
          if (FileUtilAxi.exists(oldProjectFilesBackupDirectoryName))
          {
            // Backup directory already exist, just move files into it.
            FileUtilAxi.copyDirectory(tempDir, oldProjectFilesBackupDirectoryName);
            FileUtilAxi.delete(tempDir);
          }
          else
          {
            FileUtilAxi.rename(tempDir, oldProjectFilesBackupDirectoryName);
          }

          // Clean up the current project directory.
          removeOlderVersionProjectFiles(project);
        }
        else
        {
          FileUtilAxi.delete(tempDir);
        }
      }
    }

    MemoryUtil.garbageCollect();
  }

  /**
   * Save project information to the .project File
   * @author Bill Darbie
   * @edited By Kee Chin Seong - Catching Stack overflow to prevent the ObjectOutput throws exception
   */
  public void saveProjectAsSerializedFile(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    String fileName = FileName.getProjectSerializedFullPath(project.getName());

    // save file to a temporary name first, in case the program is
    // killed during the save
    File tempPanelFile = new File(fileName + FileName.getTempFileExtension());

    if (tempPanelFile.exists() && (tempPanelFile.canWrite() == false))
      throw new CannotWriteDatastoreException(tempPanelFile.getAbsolutePath());

    ObjectOutput os = null;
    try
    {
      FileOutputStream fos = new FileOutputStream(tempPanelFile);
      BufferedOutputStream bos = new BufferedOutputStream(fos);
      os = new ObjectOutputStream(bos);

      // wpd - timing
      long startTime = System.currentTimeMillis();
      os.writeObject(project);
      long stopTime = System.currentTimeMillis();
      long time = stopTime - startTime;
      if (_displayTimes && (UnitTest.unitTesting() == false))
      {
        System.out.println("time to save serialized file in milliseconds: " + time);
      }
    }
    catch(IOException ioe)
    {
      ioe.printStackTrace();
      // remove the partially written file, but must close the stream first
      try
      {
        if (os != null)
          os.close();
      }
      catch(IOException fioe)
      {
        // do nothing
      }
      tempPanelFile.delete();

      DatastoreException dex = new CannotWriteDatastoreException(fileName);
      dex.initCause(ioe);
    }
    catch (StackOverflowError soe)
    {
      // XCR-3120 Intermittent software crash when load recipe
      // for now ignore this error, java has a bug about this already
      // BugID: 4152790  It has been open since June 1998.
      // if we can figure this out in the future, we can stop catching this error.
      // There is similar code in ProjectWriter as well.
      if (UnitTest.unitTesting() == false)
      {
        System.out.println("WARNING: StackOverflowError while while saving Project serialized file for : " + project.getName());
      }
    }
    finally
    {
      try
      {
        if (os != null)
          os.close();
      }
      catch(IOException ioe)
      {
        ioe.printStackTrace();
      }
    }

    // now move the temporary file to the real file name
    File destFile = new File(fileName);
    destFile.delete();
    tempPanelFile.renameTo(destFile);
  }

  /**
   * Write Isolated PSH region list to .csv file
   * @author Chnee Khang Wah
   */
  public void writeIsolatedRegionList(Project project) throws DatastoreException
  {
    _isolatedPSHRegionCSVWriter.write(project);
  }
  
  /**
   * @author Bill Darbie
   */
  private void write(Project project) throws DatastoreException
  {
    String projName = project.getName();
    String dirName = Directory.getProjectDir(projName);
    // create the directory if it does not exist
    if (FileUtilAxi.exists(dirName) == false)
      FileUtilAxi.mkdirs(dirName);

    // Update project file creation version.
    if (VersionUtil.isOlderVersion(project.getSoftwareVersionOfProjectLastSave()))
    {
      project.setSoftwareVersionOfProjectLastSave(Version.getVersionNumber());
    }

    // write out the xml file first, because writing the xml files generates the checksums,
    // which will get written out in the project text files
    TimerUtil timer = new TimerUtil();
    // the settings xml file has to be written out before the cad xml file
    timer.reset();
    timer.start();
    _projectSettingsXMLWriter.write(project, true);
    timer.stop();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
      System.out.println("wpd: time to write out settings.xml file, in millis:" + timer.getElapsedTimeInMillis());

    timer.reset();
    timer.start();
    _projectCadXMLWriter.write(project, true);
    timer.stop();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
      System.out.println("wpd: time to write out cad.xml file, in millis:" + timer.getElapsedTimeInMillis());

    timer.reset();
    timer.start();
    _projectSummaryWriter.write(project);
    Map<Board, Integer> boardToBoardInstanceNumberMap = _panelWriter.write(project);
    _boardTypeWriter.write(project);
    //Khaw Chek Hau - XCR3554: Package on package (PoP) development
    _packageOnPackageWriter.write(project);
    _compPackageWriter.write(project);
    _landPatternWriter.write(project);

    _projectSettingsWriter.write(project);
    _panelSettingsWriter.write(project);
    _boardSettingsWriter.write(project, boardToBoardInstanceNumberMap);
    _subtypeSettingsWriter.write(project);
    _subtypeAdvanceSettingsWriter.write(project);
    _boardTypeSettingsWriter.write(project);
    _componentNoLoadSettingsWriter.write(project);
    _alignmentTransformSettingsWriter.write(project);
    _compPackageSettingsWriter.write(project);
    
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP))
    {
      // Modified by Jack 28 March 2013 - To handle missing PanelSurfaceMapSettings and BoardSurfaceMapSettings
      if (project.getPanel().hasPspSettings() == false)
        project.getPanel().setPspSettings(new PspSettings());
      
      if (project.getPanel().hasPanelSurfaceMapSettings() == false)
        project.getPanel().setPanelSurfaceMapSettings(new PanelSurfaceMapSettings());
      
      if (project.getPanel().hasPanelAlignmentSurfaceMapSettings() == false)
        project.getPanel().setPanelAlignmentSurfaceMapSettings(new PanelAlignmentSurfaceMapSettings());
      
      if (project.getPanel().hasPanelMeshSettings() == false)
        project.getPanel().setPanelMeshSettings(new PanelMeshSettings());
      
      for(Board board : project.getPanel().getBoards())
      {
        if (board.hasBoardSurfaceMapSettings() == false)
          board.setBoardSurfaceMapSettings(new BoardSurfaceMapSettings(board));
        
        if (board.hasBoardAlignmentSurfaceMapSettings() == false)
          board.setBoardAlignmentSurfaceMapSettings(new BoardAlignmentSurfaceMapSettings(board));
        
        if (board.hasBoardMeshSettings() == false)
          board.setBoardMeshSettings(new BoardMeshSettings(board));
      }
      
      _panelSurfaceMapSettingsWriter.write(project);
      //_panelAlignmentSurfaceMapSettingsWriter.write(project);
      _boardSurfaceMapSettingsWriter.write(project, boardToBoardInstanceNumberMap);
      //_boardAlignmentSurfaceMapSettingsWriter.write(project, boardToBoardInstanceNumberMap);
      _panelMeshSettingsWriter.write(project);
      _boardMeshSettingsWriter.write(project, boardToBoardInstanceNumberMap);
      _pspSettingsWriter.write(project);
    }
    _autoExposureSettingWriter.write(project);
    _panelFocusRegionSettingsWriter.write(project);
    
    //Siew Yeng - XCR-3839 - Import NDF crash
    if(project.getPanel().hasPshSettings() == false)
      project.getPanel().setPshSettings(new PshSettings(project.getPanel())); //Siew Yeng - XCR-3855 - fix crash when switch to PSH Settings
    
    _pshSettingsWriter.write(project);
    
    //Siew Yeng - XCR1725 - Serial Number Mapping
    SerialNumberMappingManager.getInstance().write();

    timer.stop();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
      System.out.println("wpd: time to write out text files, in millis:" + timer.getElapsedTimeInMillis());

    timer.reset();
    timer.start();
    if (project.isTestProgramValidIgnoringAlignmentRegions())
    {
      _imageManager.saveImageInfoToProjectDirectory(project);
    }

    timer.stop();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
      System.out.println("wpd: time to save image names to project directory, in millis:" + timer.getElapsedTimeInMillis());
  }

  /**
   * Change any # to \#.  This method should be called for any strings that need to be written out that
   * may have a # in them.
   * @author Bill Darbie
   */
  void println(PrintWriter os, String line)
  {
    Assert.expect(os != null);
    Assert.expect(line != null);

    line = line.replaceAll("#", "\\\\#");
    os.println(line);
  }

  /**
   * Take a text line that can contain one or more \n in it and write it out to
   * a file so it can be parsed by ProjectReader.readMultipleLinesOfText.
   * @author Bill Darbie
   */
  void writeMultipleLinesOfText(PrintWriter os, String header, String text, String footer)
  {
    Assert.expect(os != null);
    Assert.expect(header != null);
    Assert.expect(text != null);
    Assert.expect(footer != null);

    List<String> textList = new ArrayList<String>();
    int numNewLines = 0;
    if (text.equals("") == false)
    {
      int beginIndex = 0;
      int endIndex = 0;
      do
      {
        if (endIndex == 0)
          --endIndex;
        endIndex = text.indexOf("\n", beginIndex);
        String str = null;
        if (endIndex == -1)
          str = text.substring(beginIndex, text.length());
        else
          str = text.substring(beginIndex, endIndex);
        // replace # with \#
        str = str.replaceAll("#", "\\#");
        textList.add(str);
        beginIndex = endIndex + 1;
        ++numNewLines;
      }
      while (endIndex != -1);
    }
    os.println(header);
    os.println(numNewLines);
    for (int i = 0; i < numNewLines; ++i)
      println(os, "\"" + textList.get(i) + "\"");
    os.println(footer);
  }

  /**
   * Back up older project files to a directory so the reader won't automatictally
   * open it down the road.  This will stop older appliction from opening a project
   * that is modified by newer application .
   * @author Rex Shang
   */
  private void backupOlderVersionProjectFiles(Project project) throws DatastoreException
  {
    String projName = project.getName();
    String dirName = Directory.getProjectDir(projName);

    String oldProjectFilesBackupDirectoryName = dirName + File.separator + project.getSoftwareVersionOfProjectLastSave() + FileName.getBackupExtension();
    if (FileUtilAxi.exists(oldProjectFilesBackupDirectoryName) == false)
    {
      FileUtilAxi.mkdirs(oldProjectFilesBackupDirectoryName);
    }

    List<String> filesToBackup = new ArrayList<String>();
    int previousVersion = 0;

    Panel panel = project.getPanel();
    List<BoardType> boardTypes = panel.getBoardTypes();
    List<Board> boards = panel.getBoards();

    // project.X.summary
    previousVersion = FileName.getProjectSummaryLatestFileVersion();
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectSummaryFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToBackup.add(oldFile);
      }
      previousVersion--;
    }

    // panel.X.cad
    previousVersion = FileName.getProjectPanelLatestFileVersion();
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectPanelFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToBackup.add(oldFile);
      }
      previousVersion--;
    }

    // boardType_X.X.cad
    previousVersion = FileName.getProjectBoardTypeLatestFileVersion();
    while (previousVersion > 0)
    {
      for (int boardTypeNumber = 1; boardTypeNumber <= boardTypes.size(); boardTypeNumber++)
      {
        String oldFile = FileName.getProjectBoardTypeFullPath(projName, boardTypeNumber, previousVersion);
        if (FileUtilAxi.exists(oldFile))
        {
          filesToBackup.add(oldFile);
        }
      }
      previousVersion--;
    }

    // package.X.cad
    previousVersion = FileName.getProjectCompPackageLatestFileVersion();
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectCompPackageFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToBackup.add(oldFile);
      }
      previousVersion--;
    }

    // landPattern.X.cad
    previousVersion = FileName.getProjectLandPatternLatestFileVersion();
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectLandPatternFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToBackup.add(oldFile);
      }
      previousVersion--;
    }

    // project.X.settings
    previousVersion = FileName.getProjectProjectSettingsLatestFileVersion();
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectProjectSettingsFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToBackup.add(oldFile);
      }
      previousVersion--;
    }

    // panel.X.settings
    previousVersion = FileName.getProjectPanelSettingsLatestFileVersion();
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectPanelSettingsFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToBackup.add(oldFile);
      }
      previousVersion--;
    }

    // board_X.X.settings
    previousVersion = FileName.getProjectBoardSettingsLatestFileVersion();
    while (previousVersion > 0)
    {
      for (int boardInstanceNumber = 1; boardInstanceNumber <= boards.size(); boardInstanceNumber++)
      {
        String oldFile = FileName.getProjectBoardSettingsFullPath(projName, boardInstanceNumber, previousVersion);
        if (FileUtilAxi.exists(oldFile))
        {
          filesToBackup.add(oldFile);
        }
      }
      previousVersion--;
    }

    // subtype.X.settings
    previousVersion = FileName.getProjectSubtypeSettingsLatestFileVersion();
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectSubtypeSettingsFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToBackup.add(oldFile);
      }
      previousVersion--;
    }

    // boardType_X.X.settings
    previousVersion = FileName.getProjectBoardTypeSettingsLatestFileVersion();
    while (previousVersion > 0)
    {
      for (int boardTypeNumber = 1; boardTypeNumber <= boardTypes.size(); boardTypeNumber++)
      {
        String oldFile = FileName.getProjectBoardTypeSettingsFullPath(projName, boardTypeNumber, previousVersion);
        if (FileUtilAxi.exists(oldFile))
        {
          filesToBackup.add(oldFile);
        }
      }
      previousVersion--;
    }

    // componentNoLoad.X.settings
    previousVersion = FileName.getProjectComponentNoLoadSettingsLatestFileVersion();
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectComponentNoLoadSettingsFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToBackup.add(oldFile);
      }
      previousVersion--;
    }

    // alignmentTransform.X.settings
    previousVersion = FileName.getProjectAlignmentTransfromSettingsLatestFileVersion();
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectAlignmentTransformSettingsFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToBackup.add(oldFile);
      }
      previousVersion--;
    }

    // package.X.settings
    previousVersion = FileName.getProjectCompPackageLatestFileVersion();
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectCompPackageSettingFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToBackup.add(oldFile);
      }
      previousVersion--;
    }

    // Do the backup.
    backedUpFileList.clear();
    for (String oldFileName : filesToBackup)
    {
      String newFileName = oldProjectFilesBackupDirectoryName + File.separator + FileUtilAxi.getNameWithoutPath(oldFileName);
      FileUtilAxi.rename(oldFileName, newFileName);
      backedUpFileList.add(new Pair(oldFileName, newFileName));
//      System.out.println("rename " + oldFileName + " to " + newFileName);
    }
  }

  /*
   * Get rid off project files that were created with older version of our application.
   * @author Rex Shang
   */
  private void removeOlderVersionProjectFiles(Project project) throws DatastoreException
  {
    String projName = project.getName();
    List<String> filesToRemove = new ArrayList<String>();
    int previousVersion = 0;

    Panel panel = project.getPanel();
    List<BoardType> boardTypes = panel.getBoardTypes();
    List<Board> boards = panel.getBoards();

    // project.X.summary
    previousVersion = FileName.getProjectSummaryLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectSummaryFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // panel.X.cad
    previousVersion = FileName.getProjectPanelLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectPanelFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // boardType_X.X.cad
    previousVersion = FileName.getProjectBoardTypeLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      for (int boardTypeNumber = 1; boardTypeNumber <= boardTypes.size(); boardTypeNumber++)
      {
        String oldFile = FileName.getProjectBoardTypeFullPath(projName, boardTypeNumber, previousVersion);
        if (FileUtilAxi.exists(oldFile))
        {
          filesToRemove.add(oldFile);
        }
      }
      previousVersion--;
    }

    // package.X.cad
    previousVersion = FileName.getProjectCompPackageLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectCompPackageFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // landPattern.X.cad
    previousVersion = FileName.getProjectLandPatternLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectLandPatternFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // project.X.settings
    previousVersion = FileName.getProjectProjectSettingsLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectProjectSettingsFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // panel.X.settings
    previousVersion = FileName.getProjectPanelSettingsLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectPanelSettingsFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // board_X.X.settings
    previousVersion = FileName.getProjectBoardSettingsLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      for (int boardInstanceNumber = 1; boardInstanceNumber <= boards.size(); boardInstanceNumber++)
      {
        String oldFile = FileName.getProjectBoardSettingsFullPath(projName, boardInstanceNumber, previousVersion);
        if (FileUtilAxi.exists(oldFile))
        {
          filesToRemove.add(oldFile);
        }
      }
      previousVersion--;
    }

    // subtype.X.settings
    //Khaw Chek Hau - XCR2514 : Threshold value changed by itself after using software 5.6
    //Don't delete the old subtypeSetting file as it will cause assert when try to open newer version recipe
//    previousVersion = FileName.getProjectSubtypeSettingsLatestFileVersion() - 1;
//    while (previousVersion > 0)
//    {
//      String oldFile = FileName.getProjectSubtypeSettingsFullPath(projName, previousVersion);
//      if (FileUtilAxi.exists(oldFile))
//      {
//        filesToRemove.add(oldFile);
//      }
//      previousVersion--;
//    }

    // boardType_X.X.settings
    previousVersion = FileName.getProjectBoardTypeSettingsLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      for (int boardTypeNumber = 1; boardTypeNumber <= boardTypes.size(); boardTypeNumber++)
      {
        String oldFile = FileName.getProjectBoardTypeSettingsFullPath(projName, boardTypeNumber, previousVersion);
        if (FileUtilAxi.exists(oldFile))
        {
          filesToRemove.add(oldFile);
        }
      }
      previousVersion--;
    }

    // componentNoLoad.X.X.settings
    previousVersion = FileName.getProjectComponentNoLoadSettingsLatestFileVersion() - 1;
    while (previousVersion > 1)
    {
      int minorVersion = 1;
      while (minorVersion >= 1)
      {
        String oldFile = FileName.getProjectComponentNoLoadSettingsFullPath(projName, previousVersion, minorVersion);
        if (FileUtilAxi.exists(oldFile))
        {
          ++minorVersion;
          filesToRemove.add(oldFile);
        }
        else
        {
          minorVersion = 0;
        }
      }
      previousVersion--;
    }

    // alignmentTransform.X.settings
    previousVersion = FileName.getProjectAlignmentTransfromSettingsLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectAlignmentTransformSettingsFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // package.X.settings
    previousVersion = FileName.getProjectCompPackageLatestFileVersion() - 1;
    while (previousVersion > 0)
    {
      String oldFile = FileName.getProjectCompPackageSettingFullPath(projName, previousVersion);
      if (FileUtilAxi.exists(oldFile))
      {
        filesToRemove.add(oldFile);
      }
      previousVersion--;
    }

    // Do the backup.
    FileUtilAxi.delete(filesToRemove);
  }
  
  public static void reintializeDueToSystemTypeChange()
  {
    _instance = null;
  }

}
