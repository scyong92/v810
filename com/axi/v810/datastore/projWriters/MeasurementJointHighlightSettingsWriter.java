/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.projWriters;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.datastore.*;
import java.util.*;

/**
 * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
 *
 * @author weng-jian.eoh
 */
public class MeasurementJointHighlightSettingsWriter
{

  private String _filePath;

  public MeasurementJointHighlightSettingsWriter(String projectName)
  {
    _filePath = FileName.getProjectMeasurementJointHighlightSettingFullPath(projectName);
  }

  public void saveSettings(List<String> data) throws DatastoreException, BusinessException
  {
    Assert.expect(data != null);

    writeSettingsToFile(data);
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  private void writeSettingsToFile(List<String> data) throws DatastoreException, BusinessException
  {

    Assert.expect(data != null);

    FileWriterUtil fileWriterUtil = new FileWriterUtil(_filePath, false);
    try
    {
      fileWriterUtil.open();
      for (String parameter : data)
      {
        fileWriterUtil.writeln(parameter);
      }
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(_filePath);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      fileWriterUtil.close();
    }
  }
}
