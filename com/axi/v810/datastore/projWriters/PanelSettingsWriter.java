package com.axi.v810.datastore.projWriters;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.testProgram.*;

/**
 * @author Bill Darbie
 */
class PanelSettingsWriter
{
  private static PanelSettingsWriter _instance;
  private ProjectWriter _projectWriter;
  private EnumToUniqueIDLookup _enumToIdLookup;


  /**
   * @author Bill
   */
  static synchronized PanelSettingsWriter getInstance()
  {
    if (_instance == null)
    {
      _instance = new PanelSettingsWriter();
    }

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private PanelSettingsWriter()
  {
    _enumToIdLookup = EnumToUniqueIDLookup.getInstance();
  }

  /**
   * @author Bill
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();
    String projName = project.getName();
    String fileName = FileName.getProjectPanelSettingsFullPath(projName, FileName.getProjectPanelSettingsLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: panel.X.settings");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");
      PanelSettings panelSettings = project.getPanel().getPanelSettings();
      os.println("# user was warned about panel thickness importance");
      os.println(panelSettings.wasUserWarnedOfThicknessImportance());
      os.println("# panel rotation in degrees");
      os.println(panelSettings.getDegreesRotationRelativeToCad());
      os.println("# isLeftToRightFlipped");
      os.println(panelSettings.isLeftToRightFlip());
      os.println("# isTopToBottomFlipped");
      os.println(panelSettings.isTopToBottomFlip());
      os.println("# isPanelBasedAlignment");
      os.println(panelSettings.isPanelBasedAlignment());
      
      os.println("# splitLocation in Nanometer");
      os.println(panelSettings.getSplitLocationInNanometer());
      
      os.println("# use new thickness table");
      os.println(panelSettings.isUseNewThicknessTable());
      
      os.println("# bypass alignment transform within acceptable limit");
      os.println(panelSettings.isBypassAlignmentTranformWithinAcceptableLimit());
      
      os.println("# unfocused regions slices to be fixed list");
      Map<String, Set<SliceNameEnum>> unfocusedRegionSlicesToBeFixedMap = panelSettings.getUnfocusedRegionSlicesToBeFixed();
      os.println(unfocusedRegionSlicesToBeFixedMap.size());
      for (Map.Entry<String, Set<SliceNameEnum>> entry : unfocusedRegionSlicesToBeFixedMap.entrySet())
      {
        String regionName = entry.getKey();
        Set<SliceNameEnum> sliceNameEnumSet = entry.getValue();
        os.println(regionName);
        os.println(sliceNameEnumSet.size());
        for (SliceNameEnum sliceNameEnum : sliceNameEnumSet)
        {
          int sliceId = _enumToIdLookup.getSliceNameUniqueID(sliceNameEnum);
          os.println(sliceId);
        }
      }

      os.println("# unfocused region slices to Z offset list");
      Map<String, Set<Pair<SliceNameEnum, Integer>>> unfocusedRegionSlicesToZoffsetInNanosMap = panelSettings.getUnfocusedRegionSlicesToZoffsetInNanos();
      os.println(unfocusedRegionSlicesToZoffsetInNanosMap.size());
      for (Map.Entry<String, Set<Pair<SliceNameEnum, Integer>>> entry : unfocusedRegionSlicesToZoffsetInNanosMap.entrySet())
      {
        String regionName = entry.getKey();
        Set<Pair<SliceNameEnum, Integer>> sliceToZoffsetPairSet = entry.getValue();
        int numSlices = sliceToZoffsetPairSet.size();
        os.println(regionName);
        os.println(numSlices);
        for (Pair<SliceNameEnum, Integer> sliceToZoffsetPair : sliceToZoffsetPairSet)
        {
          SliceNameEnum sliceNameEnum = sliceToZoffsetPair.getFirst();
          int sliceId = _enumToIdLookup.getSliceNameUniqueID(sliceNameEnum);
          int zOffset = sliceToZoffsetPair.getSecond();
          os.println(sliceId);
          os.println(zOffset);
        }
      }

      os.println("# unfocused region names to retest list");
      Set<String> unfocusedRegionNamesToRetestSet = panelSettings.getUnfocusedRegionsNamesToRetest();
      os.println(unfocusedRegionNamesToRetestSet.size());
      for (String unfocusedRegionName : unfocusedRegionNamesToRetestSet)
        os.println(unfocusedRegionName);

      os.println("# no test region names and pad types");
      Set<String> noTestRegionNames = panelSettings.getNoTestRegionsNames();
      os.println(noTestRegionNames.size());
      for (String noTestRegionName : noTestRegionNames)
      {
        os.println(noTestRegionName);
        List<PadType> padTypes = panelSettings.getNoTestPadTypes(noTestRegionName);
        os.println(padTypes.size());
        for (PadType padType : padTypes)
        {
          ComponentType componentType = padType.getComponentType();
          BoardType boardType = componentType.getSideBoardType().getBoardType();

          String boardTypeName = boardType.getName();
          String refDes = componentType.getReferenceDesignator();
          String padName = padType.getName();
          os.println(boardTypeName);
          os.println(refDes);
          os.println(padName);
        }
      }

//     os.println("# single|left|right alignmentGroupName numPadsAndFiducials");
      os.println("# single|left|right alignmentGroupName numPadsAndFiducials useZheight zHeightInNanometer useCustomizeAlignmentSetting userGain integrationLevel stageSpeed dro magnification");
      os.println("#   boardName refDes padName");
      os.println("#   boardName fiducialName");
      String singleOrLeftOrRight = null;
      for (int i = 0; i < 2; ++i)
      {
        List<AlignmentGroup> alignmentGroups = null;
        if (i == 0)
        {
          if (panelSettings.isLongPanel())
          {
            singleOrLeftOrRight = "left";
            alignmentGroups = panelSettings.getLeftAlignmentGroupsForLongPanel();
          }
          else
          {
            singleOrLeftOrRight = "single";
            alignmentGroups = panelSettings.getAlignmentGroupsForShortPanel();
          }
        }
        else
        if (i == 1)
        {
          if (panelSettings.isLongPanel())
          {
            singleOrLeftOrRight = "right";
            alignmentGroups = panelSettings.getRightAlignmentGroupsForLongPanel();
          }
          else
          {
            continue;
          }
        }

        for (AlignmentGroup alignmentGroup : alignmentGroups)
        {
          List<Pad> pads = alignmentGroup.getPads();
          List<Fiducial> fiducials = alignmentGroup.getFiducials();
          int numPadsAndFiducials = pads.size() + fiducials.size();
          boolean useZheight = alignmentGroup.useZHeight();
          boolean useCustomizeAlignmentSetting = alignmentGroup.useCustomizeAlignmentSetting();
          int zHeightInNanometer = alignmentGroup.getZHeightInNanometer();

          os.println(singleOrLeftOrRight + " " + alignmentGroup.getName() + " " + numPadsAndFiducials + 
                     " " + useZheight + " " + zHeightInNanometer +
                     " " + useCustomizeAlignmentSetting + " " + alignmentGroup.getUserGainEnum() +
                     " " + alignmentGroup.getSignalCompensationEnum() + " " + alignmentGroup.getStageSpeedEnum() + 
                     " " + alignmentGroup.getDynamicRangeOptimizationLevel());

          for (Pad pad : pads)
          {
            Component component = pad.getComponent();
            String boardName = component.getBoard().getName();
            String refDes = component.getReferenceDesignator();
            String padName = pad.getName();
            _projectWriter.println(os, "  " + boardName + " " + refDes + "  " + padName);
          }

          for (Fiducial fiducial : fiducials)
          {
            String fiducialName = fiducial.getName();
            if (fiducial.isOnBoard())
            {
              String boardName = fiducial.getSideBoard().getBoard().getName();
              _projectWriter.println(os, "  " + boardName + " " + fiducialName);
            }
            else if (fiducial.isOnPanel())
            {
              _projectWriter.println(os, "  " + fiducialName);
            }
            else
            {
              Assert.expect(false);
            }
          }
        }
      }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
      {
        os.close();
      }
    }
  }
}
