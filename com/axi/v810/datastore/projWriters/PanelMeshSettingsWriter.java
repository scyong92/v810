package com.axi.v810.datastore.projWriters;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.datastore.*;

/**
 * @author Cheah Lee Herng
 */
public class PanelMeshSettingsWriter 
{
  private static PanelMeshSettingsWriter _instance;
  private ProjectWriter _projectWriter;
  
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized PanelMeshSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new PanelMeshSettingsWriter();

    return _instance;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public PanelMeshSettingsWriter()
  {
    // Do nothing
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();
    String projName = project.getName();
    String fileName = FileName.getProjectPanelMeshSettingsFullPath(projName, FileName.getProjectPanelMeshSettingsLatestFileVersion());
    
    PrintWriter os = null;
    try
    {
        os = new PrintWriter(fileName);

        os.println("##############################################################################");
        os.println("# FILE: panel_mesh.X.settings");
        os.println("# PURPOSE: to list all triangle mesh points used by this project");
        os.println("#");
        os.println("# NOTE: Hand editing of this file is not supported");
        os.println("#       The format of this file can be changed at any time with new");
        os.println("#       software releases.");
        os.println("#       If you want to write a script that uses information from this file");
        os.println("#       it is highly recommended that you use the cad.id.xml and");
        os.println("#       settings.id.xml files to get your data instead.");
        os.println("##############################################################################");
        os.println("# point1PanelXCoordinateInNanometers point1PanelYCoordinateInNanometers point1RegionWidthInNanometers point1RegionHeightInNanometers "
                   + "point2PanelXCoordinateInNanometers point2PanelYCoordinateInNanometers point2RegionWidthInNanometers point2RegionHeightInNanometers "
                   + "point3PanelXCoordinateInNanometers point3PanelYCoordinateInNanometers point3RegionWidthInNanometers point3RegionHeightInNanometers");
        
        PanelMeshSettings panelMeshSettings = project.getPanel().getPanelMeshSettings();
        for(MeshTriangle meshTriangle : panelMeshSettings.getMeshTriangleList())
        {
          _projectWriter.println(os, meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getMinX() + " " + 
                                     meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getMinY() + " " + 
                                     meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getWidth() + " " +
                                     meshTriangle.getPoint1OpticalCameraRectangle().getRegion().getHeight() + " " +
                                     meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getMinX() + " " + 
                                     meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getMinY() + " " +                  
                                     meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getWidth() + " " +
                                     meshTriangle.getPoint2OpticalCameraRectangle().getRegion().getHeight() + " " +                  
                                     meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getMinX() + " " + 
                                     meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getMinY() + " " + 
                                     meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getWidth() + " " +
                                     meshTriangle.getPoint3OpticalCameraRectangle().getRegion().getHeight());
        }
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
      {
        os.close();
      }
    }
  }
}
