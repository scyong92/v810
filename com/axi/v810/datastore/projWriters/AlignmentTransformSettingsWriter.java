package com.axi.v810.datastore.projWriters;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;

/**
 * This writer is different from the other project writers because it will get called every time a new
 * alignment transform is created.  This happens both when a manual alignment is done and when a panel
 * is inspected.  So the file that this Writer creates will be writen more often than the other project files.
 *
 * @author Bill Darbie
 */
public class AlignmentTransformSettingsWriter
{
  private static AlignmentTransformSettingsWriter _instance;
  private ProjectWriter _projectWriter;

  /**
   * @author Bill
   */
  public static synchronized AlignmentTransformSettingsWriter getInstance()
  {
    if (_instance == null)
      _instance = new AlignmentTransformSettingsWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private AlignmentTransformSettingsWriter()
  {
    // do nothing
  }

  /**
   * @author Bill
   */
  public void write(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    if (_projectWriter == null)
      _projectWriter = ProjectWriter.getInstance();
    String projName = project.getName();
    String fileName = FileName.getProjectAlignmentTransformSettingsFullPath(projName, FileName.getProjectAlignmentTransfromSettingsLatestFileVersion());

    PrintWriter os = null;
    try
    {
      os = new PrintWriter(fileName);

      os.println("##############################################################################");
      os.println("# FILE: alignmentTransformpanel.X.settings");
      os.println("#");
      os.println("# NOTE: Hand editing of this file is not supported");
      os.println("#       The format of this file can be changed at any time with new");
      os.println("#       software releases.");
      os.println("#       If you want to write a script that uses information from this file");
      os.println("#       it is highly recommended that you use the cad.id.xml and");
      os.println("#       settings.id.xml files to get your data instead.");
      os.println("##############################################################################");

      PanelSettings panelSettings = project.getPanel().getPanelSettings();

      os.println("# alignmentGroupName [match quality] [foreground gray level] [background gray level]");

      if( panelSettings.isPanelBasedAlignment() )
      {
        for (int i = 0; i < 2; ++i)
        {
          List<AlignmentGroup> alignmentGroups = null;
          if (i == 0)
          {
            if (panelSettings.isLongPanel())
            {
              alignmentGroups = panelSettings.getLeftAlignmentGroupsForLongPanel();
            }
            else
            {
              alignmentGroups = panelSettings.getAlignmentGroupsForShortPanel();
            }
          }
          else if (i == 1)
          {
            if (panelSettings.isLongPanel())
            {
              alignmentGroups = panelSettings.getRightAlignmentGroupsForLongPanel();
            }
            else
            {
              continue;
            }
          }

          if (alignmentGroups.isEmpty())
            continue;

          for (AlignmentGroup alignmentGroup : alignmentGroups)
          {
            if (alignmentGroup.hasMatchQuality())
            {
              double matchQuality = alignmentGroup.getMatchQuality();
              double foregroundGrayLevel = -1;
              double backgroundGrayLevel = -1;              
              if(alignmentGroup.hasForegroundGrayLevel() && alignmentGroup.hasBackgroundGrayLevel())
              {
                foregroundGrayLevel = alignmentGroup.getForegroundGrayLevel();
                backgroundGrayLevel = alignmentGroup.getBackgroundGrayLevel();                
              }
              
              if(alignmentGroup.hasForegroundGrayLevelInHighMag() && alignmentGroup.hasBackgroundGrayLevelInHighMag())
                os.println(alignmentGroup.getName() + " " + 
                  matchQuality + " " +
                  foregroundGrayLevel + " " + 
                  backgroundGrayLevel + " " + 
                  alignmentGroup.getForegroundGrayLevelInHighMag() + " " + 
                  alignmentGroup.getBackgroundGrayLevelInHighMag());
              else if(alignmentGroup.hasForegroundGrayLevel() && alignmentGroup.hasBackgroundGrayLevel())
                os.println(alignmentGroup.getName() + " " + matchQuality + " " + foregroundGrayLevel + " " + backgroundGrayLevel);
            }
          }
        }

        os.println("matrixData");
        if (panelSettings.leftManualAlignmentTransformExists())
        {
          os.println("leftManualAlignmentTransformMatrix");
          AffineTransform trans = panelSettings.getLeftManualAlignmentTransform();
          writeTransform(os, trans);
        }

        if (panelSettings.rightManualAlignmentTransformExists())
        {
          os.println("rightManualAlignmentTransformMatrix");
          AffineTransform trans = panelSettings.getRightManualAlignmentTransform();
          writeTransform(os, trans);
        }

        if (panelSettings.leftRuntimeAlignmentTransformExists())
        {
          os.println("leftRuntimeAlignmentTransformMatrix");
          AffineTransform trans = panelSettings.getLeftRuntimeAlignmentTransform();
          writeTransform(os, trans);
        }

        if (panelSettings.rightRuntimeAlignmentTransformExists())
        {
          os.println("rightRuntimeAlignmentTransformMatrix");
          AffineTransform trans = panelSettings.getRightRuntimeAlignmentTransform();
          writeTransform(os, trans);
        }
        if (panelSettings.leftManualAlignmentTransformForHighMagExists())
        {
          os.println("leftManualAlignmentTransformMatrixForHighMag");
          AffineTransform trans = panelSettings.getLeftManualAlignmentTransformForHighMag();
          writeTransform(os, trans);
        }

        if (panelSettings.rightManualAlignmentTransformForHighMagExists())
        {
          os.println("rightManualAlignmentTransformMatrixForHighMag");
          AffineTransform trans = panelSettings.getRightManualAlignmentTransformForHighMag();
          writeTransform(os, trans);
        }

        if (panelSettings.leftRuntimeAlignmentTransformForHighMagExists())
        {
          os.println("leftRuntimeAlignmentTransformMatrixForHighMag");
          AffineTransform trans = panelSettings.getLeftRuntimeAlignmentTransformForHighMag();
          writeTransform(os, trans);
        }

        if (panelSettings.rightRuntimeAlignmentTransformForHighMagExists())
        {
          os.println("rightRuntimeAlignmentTransformMatrixForHighMag");
          AffineTransform trans = panelSettings.getRightRuntimeAlignmentTransformForHighMag();
          writeTransform(os, trans);
        }
        
        if (panelSettings.leftManualAlignmentTransformExists())
        {
          os.println("leftLastSavedViewCadRuntimeAlignmentTransformMatrix");
          AffineTransform trans = panelSettings.getLeftLastSavedViewCadRuntimeAlignmentTransform();
          writeTransform(os, trans);
        }
        if (panelSettings.rightManualAlignmentTransformExists())
        {
          os.println("rightLastSavedViewCadRuntimeAlignmentTransformMatrix");
          AffineTransform trans = panelSettings.getRightLastSavedViewCadRuntimeAlignmentTransform();
          writeTransform(os, trans);
        }
        
        if (panelSettings.leftManualAlignmentTransformForHighMagExists())
        {
          os.println("leftLastSavedViewCadRuntimeAlignmentTransformMatrixForHighMag");
          AffineTransform trans = panelSettings.getLeftLastSavedRuntimeAlignmentTransformForHighMag();
          writeTransform(os, trans);
        }
        if (panelSettings.rightManualAlignmentTransformForHighMagExists())
        {
          os.println("rightLastSavedViewCadRuntimeAlignmentTransformMatrixForHighMag");
          AffineTransform trans = panelSettings.getRightLastSavedViewCadRuntimeAlignmentTransformForHighMag();
          writeTransform(os, trans);
        }
      }
      else
      {
        for(Board board : project.getPanel().getBoards())
        {
          List<AlignmentGroup> alignmentGroups = new ArrayList<AlignmentGroup>();

          os.println("boardData");
          if (board.getBoardSettings().isLongBoard())
          {
            alignmentGroups.addAll(board.getBoardSettings().getLeftAlignmentGroupsForLongPanel());
          }
          else
          {
            alignmentGroups.addAll(board.getBoardSettings().getAlignmentGroupsForShortPanel());
          }
          
          if (board.getBoardSettings().isLongBoard())
            alignmentGroups.addAll(board.getBoardSettings().getRightAlignmentGroupsForLongPanel());
          
          for (AlignmentGroup alignmentGroup : alignmentGroups)
          {
            if (alignmentGroup.hasMatchQuality())
            {
              double matchQuality = alignmentGroup.getMatchQuality();
              
              double foregroundGrayLevel = -1;
              double backgroundGrayLevel = -1;              
              if(alignmentGroup.hasForegroundGrayLevel() && alignmentGroup.hasBackgroundGrayLevel())
              {
                foregroundGrayLevel = alignmentGroup.getForegroundGrayLevel();
                backgroundGrayLevel = alignmentGroup.getBackgroundGrayLevel();                
              }
              if(alignmentGroup.hasForegroundGrayLevelInHighMag() && alignmentGroup.hasBackgroundGrayLevelInHighMag())
                os.println(alignmentGroup.getName() + " " + 
                  matchQuality + " " + 
                  foregroundGrayLevel + " " + 
                  backgroundGrayLevel + " " + 
                  alignmentGroup.getForegroundGrayLevelInHighMag() + " " + 
                  alignmentGroup.getBackgroundGrayLevelInHighMag());
              else if(alignmentGroup.hasForegroundGrayLevel() && alignmentGroup.hasBackgroundGrayLevel())
                os.println(alignmentGroup.getName() + " " + matchQuality + " " + foregroundGrayLevel + " " + backgroundGrayLevel);
            }
          }

          os.println("matrixData");
          if (board.getBoardSettings().leftManualAlignmentTransformExists())
          {
            os.println("leftManualAlignmentTransformMatrix");
            AffineTransform trans = board.getBoardSettings().getLeftManualAlignmentTransform();
            writeTransform(os, trans);
          }

          if (board.getBoardSettings().rightManualAlignmentTransformExists())
          {
            os.println("rightManualAlignmentTransformMatrix");
            AffineTransform trans = board.getBoardSettings().getRightManualAlignmentTransform();
            writeTransform(os, trans);
          }

          if (board.getBoardSettings().leftRuntimeAlignmentTransformExists())
          {
            os.println("leftRuntimeAlignmentTransformMatrix");
            AffineTransform trans = board.getBoardSettings().getLeftRuntimeAlignmentTransform();
            writeTransform(os, trans);
          }

          if (board.getBoardSettings().rightRuntimeAlignmentTransformExists())
          {
            os.println("rightRuntimeAlignmentTransformMatrix");
            AffineTransform trans = board.getBoardSettings().getRightRuntimeAlignmentTransform();
            writeTransform(os, trans);
          }
          
          if (board.getBoardSettings().leftManualAlignmentTransformForHighMagExists())
          {
            os.println("leftManualAlignmentTransformMatrixForHighMag");
            AffineTransform trans = board.getBoardSettings().getLeftManualAlignmentTransformForHighMag();
            writeTransform(os, trans);
          }

          if (board.getBoardSettings().rightManualAlignmentTransformForHighMagExists())
          {
            os.println("rightManualAlignmentTransformMatrixForHighMag");
            AffineTransform trans = board.getBoardSettings().getRightManualAlignmentTransformForHighMag();
            writeTransform(os, trans);
          }

          if (board.getBoardSettings().leftRuntimeAlignmentTransformForHighMagExists())
          {
            os.println("leftRuntimeAlignmentTransformMatrixForHighMag");
            AffineTransform trans = board.getBoardSettings().getLeftRuntimeAlignmentTransformForHighMag();
            writeTransform(os, trans);
          }

          if (board.getBoardSettings().rightRuntimeAlignmentTransformForHighMagExists())
          {
            os.println("rightRuntimeAlignmentTransformMatrixForHighMag");
            AffineTransform trans = board.getBoardSettings().getRightRuntimeAlignmentTransformForHighMag();
            writeTransform(os, trans);
          }
          
          if (board.getBoardSettings().leftManualAlignmentTransformExists())
          {
            os.println("leftLastSavedViewCadRuntimeAlignmentTransformMatrix");
            AffineTransform trans = board.getBoardSettings().getLeftLastSavedViewCadRuntimeAlignmentTransform();
            writeTransform(os, trans);
          }
          if (board.getBoardSettings().rightManualAlignmentTransformExists())
          {
            os.println("rightLastSavedViewCadRuntimeAlignmentTransformMatrix");
            AffineTransform trans = board.getBoardSettings().getRightLastSavedViewCadRuntimeAlignmentTransform();
            writeTransform(os, trans);
          }
          
          if (board.getBoardSettings().leftManualAlignmentTransformForHighMagExists())
          {
            os.println("leftLastSavedViewCadRuntimeAlignmentTransformMatrixForHighMag");
            AffineTransform trans = board.getBoardSettings().getLeftLastSavedViewCadRuntimeAlignmentTransformForHighMag();
            writeTransform(os, trans);
          }
          if (board.getBoardSettings().rightManualAlignmentTransformForHighMagExists())
          {
            os.println("rightLastSavedViewCadRuntimeAlignmentTransformMatrixForHighMag");
            AffineTransform trans = board.getBoardSettings().getRightLastSavedViewCadRuntimeAlignmentTransformForHighMag();
            writeTransform(os, trans);
          }
          
          alignmentGroups.clear();
          alignmentGroups = null;
        }
      }
      //Khaw Chek Hau - XCR2285: 2D Alignment on v810
      os.println("alignmentMode");
      if (panelSettings.isUsing2DAlignmentMethod())
        os.println("2D");
      else
        os.println("3D");
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      if (os != null)
      {
        os.close();
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private void writeTransform(PrintWriter os, AffineTransform trans)
  {
    Assert.expect(os != null);
    Assert.expect(trans != null);

    double[] matrix = new double[6];
    trans.getMatrix(matrix);
    for (int i = 0; i < matrix.length; ++i)
    {
      os.println(matrix[i]);
    }
  }
}
