package com.axi.v810.datastore.testResults;

import java.io.*;
import java.util.*;
import java.text.*;
import java.util.concurrent.atomic.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.AlgorithmEnum;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
   * @author Chin-Seong, Kee
   */
public class TestCoverageReportTxtWriter 
{
  private boolean _exceptionThrown = false;
  private Project _project;
  private String _projectFilePath;
  private String _currentDateTime;
  private PrintWriter _os;

  private AtomicInteger _numberOfTopSideLoadedJoints = new AtomicInteger(0);
  private AtomicInteger _numberOfBtmSideLoadedJoints = new AtomicInteger(0);
  private AtomicInteger _numberOfTopSideLoadedComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfBtmSideLoadedComponents = new AtomicInteger(0);
  
  private AtomicInteger _numberOfTopSideTestedComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfBtmSideTestedComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfTopSideTestedJoints = new AtomicInteger(0);
  private AtomicInteger _numberOfBtmSideTestedJoints = new AtomicInteger(0);

  private AtomicInteger _numberOfTopSideUntestableComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfBtmSideUntestableComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfTopSideUntestableJoints = new AtomicInteger(0);
  private AtomicInteger _numberOfBtmSideUntestableJoints = new AtomicInteger(0);
  
  private AtomicInteger _numberOfTotalTopSideUnloadedComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfTotalBtmSideUnloadedComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfTotalTopSideUnloadedJoints = new AtomicInteger(0);
  private AtomicInteger _numberOfTotalBtmSideUnloadedJoints = new AtomicInteger(0);

  private AtomicInteger _numberOfTotalTopSideNoTestComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfTotalBtmSideNoTestComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfTotalTopSideNoTestJoints = new AtomicInteger(0);
  private AtomicInteger _numberOfTotalBtmSideNoTestJoints = new AtomicInteger(0);

  private Map<CompPackage, Integer> _jointTypeToPackageMap = new HashMap<CompPackage,Integer>();
  private Map<CompPackage, Integer> _noLoadJointToPackageMap = new HashMap<CompPackage,Integer>();
  private Map<CompPackage, Integer> _testedJointToPackageMap = new HashMap<CompPackage,Integer>();
  private Map<CompPackage, Integer> _untestableJointToPackageMap = new HashMap<CompPackage,Integer>();
  private Map<CompPackage, Integer> _noTestJointToPackageMap = new HashMap<CompPackage,Integer>();

  DecimalFormat twoDecimalPlaces = new DecimalFormat("###.##");

  private final String _MIXED_JOINT_TYPE = "Mixed";

  /**
   * @author Chin-Seong, Kee
   */
  public TestCoverageReportTxtWriter()
  {
    _numberOfTopSideLoadedJoints.set(0);
    _numberOfTopSideLoadedComponents.set(0);
    _numberOfBtmSideLoadedJoints.set(0);
    _numberOfBtmSideLoadedComponents.set(0);

    _numberOfTopSideTestedComponents.set(0);
    _numberOfBtmSideTestedComponents.set(0);
    _numberOfTopSideTestedJoints.set(0);
    _numberOfBtmSideTestedJoints.set(0);

    _numberOfTotalTopSideUnloadedComponents.set(0);
    _numberOfTotalBtmSideUnloadedJoints.set(0);
    _numberOfTotalBtmSideUnloadedComponents.set(0);
    _numberOfTotalTopSideUnloadedJoints.set(0);

    _numberOfTopSideUntestableComponents.set(0);
    _numberOfBtmSideUntestableComponents.set(0);
    _numberOfTopSideUntestableJoints.set(0);
    _numberOfBtmSideUntestableJoints.set(0);

    _numberOfTotalTopSideNoTestComponents.set(0);
    _numberOfTotalBtmSideNoTestComponents.set(0);
    _numberOfTotalTopSideNoTestJoints.set(0);
    _numberOfTotalBtmSideNoTestJoints.set(0);
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void open(Project project, String _projectTxtFilePath) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(_projectTxtFilePath != null);

    _project = project;
    _projectFilePath = _projectTxtFilePath;

    
    DateFormat myformat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss zzz");
    myformat.setTimeZone(TimeZone.getDefault());
    Date date = new Date();
    _currentDateTime = myformat.format(date);

    calculateComponentLevel();
    calculateJointLevel();
    calculatePackageToComponent();

    try
    {
      _os = new PrintWriter(_projectFilePath);
      writeTopElement();
    }
    catch (FileNotFoundException ex)
    {
      _exceptionThrown = true;
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_projectFilePath);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      // if the file still exists and an exception was thrown, delete it
      if (_exceptionThrown && FileUtil.exists(_projectFilePath))
      {
        // close the file in case an exception was thrown before the file was closed
        if (_os != null)
          _os.close();
        try
        {
          FileUtilAxi.delete(_projectFilePath);
        }
        catch (CannotDeleteFileDatastoreException ex)
        {
          if (_exceptionThrown)
            ex.printStackTrace();
          else
            throw ex;
        }
      }
    }
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTopElement()
  {
      _os.println("Title   : V810 Test Coverage Report\n");
      _os.println("Date    : " + _currentDateTime + "\n");
      _os.println("Project : " + _project.getName() + "\n");
      _os.println("---------------------------------------------------------------");
      _os.println("|-------------------------------------------------------------|");

      writeTotalOfComponentAndJointLevel();
      writeTotalOfTestedComponentAndJointLevel();
      writeTotalOfUnloadedComponentAndJointLevel();
      writeTotalOfNoTestComponentAndJointLevel();
      writeTotalOfUntestableComponentAndJointLevel();
      writeTotalCoverageOfComponentAndJointLevel();
      
      _os.println("---------------------------------------------------------------");
      _os.println("");

      writeTestCoverageBreakdown();
      writeNoLoadPackageList();
      writeNoTestPackageList();
      writeUntestablePackageList();
      writeComponentTestedAlgorithmSettingsLevel();
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTotalOfComponentAndJointLevel()
  {
      _os.println(String.format("%-6s","|") + String.format("%-34s", "Component Count") + String.format("%-22s","Joint Count") + "|");
      _os.println(String.format("%-6s","|") + "Top      : " 
                  + String.format("%-23d", (_numberOfTopSideLoadedComponents.get() + _numberOfTotalTopSideUnloadedComponents.get()))
                  + "Top      : " +
                  String.format("%-11d", (_numberOfTopSideLoadedJoints.get() + _numberOfTotalTopSideUnloadedJoints.get())) + "|");
      _os.println(String.format("%-6s","|") + "Bottom   : " 
                  + String.format("%-23d", (_numberOfBtmSideLoadedComponents.get() + _numberOfTotalBtmSideUnloadedComponents.get()))
                  + "Bottom   : " +
                  String.format("%-11d", (_numberOfBtmSideLoadedJoints.get() + _numberOfTotalBtmSideUnloadedJoints.get())) + "|");
      _os.println(String.format("%-1s","|") + "-------------------------------------------------------------|");
      _os.println(String.format("%-6s","|") + "Total    : " + String.format("%-23d",(_numberOfTopSideLoadedComponents.get() +
                  _numberOfTotalTopSideUnloadedComponents.get()) + (_numberOfBtmSideLoadedComponents.get() +
                  _numberOfTotalBtmSideUnloadedComponents.get())) + "Total    : " +
                  String.format("%-11d", (_numberOfBtmSideLoadedJoints.get() + _numberOfTotalBtmSideUnloadedJoints.get()) +
                  (_numberOfTopSideLoadedJoints.get() + _numberOfTotalTopSideUnloadedJoints.get())) + "|" );
      _os.println(String.format("%-1s","|") + "-------------------------------------------------------------|");
      _os.println(String.format("%-62s","|") + "|");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTotalOfTestedComponentAndJointLevel()
  {
      _os.println(String.format("%-6s","|") + String.format("%-34s", "Component Tested") + String.format("%-22s", "Joint Tested") + "|");
      _os.println(String.format("%-6s","|") + "Top      : " + String.format("%-23d", _numberOfTopSideTestedComponents.get())
                  + "Top      : " +
                  String.format("%-11d", _numberOfTopSideTestedJoints.get()) + "|");
      _os.println(String.format("%-6s","|") + "Bottom   : " + String.format("%-23d", _numberOfBtmSideTestedComponents.get())
                  + "Bottom   : " +
                   String.format("%-11d", _numberOfBtmSideTestedJoints.get()) + "|");
      _os.println(String.format("%-1s","|") + "-------------------------------------------------------------|");
      _os.println(String.format("%-6s","|") + "Total    : " + String.format("%-23d",_project.getPanel().getNumTestedComponents())
                  + "Total    : " +
                  String.format("%-11d", _project.getPanel().getNumInspectedJoints()) + "|" );
      _os.println(String.format("%-1s","|") + "-------------------------------------------------------------|");
      _os.println(String.format("%-62s","|") + "|");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTotalCoverageOfComponentAndJointLevel()
  {
      _os.println(String.format("%-6s","|") + String.format("%-34s", "Component Coverage") + String.format("%-22s", "Joint Coverage") + "|");
      _os.println(String.format("%-6s","|") + "Top      : " + String.format("%-5s", twoDecimalPlaces.format((double)((double)_numberOfTopSideTestedComponents.get()/
                  (double)_numberOfTopSideLoadedComponents.get()) * 100)) + String.format("%-18s", "%") +
                  "Top      : " +
                  String.format("%-5s",  twoDecimalPlaces.format((double)((double)_numberOfTopSideTestedJoints.get()/
                  (double)_numberOfTopSideLoadedJoints.get()) * 100)) + String.format("%-6s", "%") + "|");
      _os.println(String.format("%-6s","|") + "Bottom   : " + String.format("%-5s", twoDecimalPlaces.format((double)((double)_numberOfBtmSideTestedComponents.get()/
                  (double)_numberOfBtmSideLoadedComponents.get()) * 100)) + String.format("%-18s", "%") +
                  "Bottom   : " +
                  String.format("%-5s", twoDecimalPlaces.format((double)((double)_numberOfBtmSideTestedJoints.get()/
                  (double)_numberOfBtmSideLoadedJoints.get()) * 100)) + String.format("%-6s", "%") + "|");
      _os.println(String.format("%-1s","|") + "-------------------------------------------------------------|");
      _os.println(String.format("%-6s","|") + "Total    : " + String.format("%-5s", twoDecimalPlaces.format((double)((double)_project.getPanel().getNumTestedComponents()/
                  (double)_project.getPanel().getNumLoadedComponents()) * 100)) + String.format("%-18s", "%") +
                  "Total    : " +
                  String.format("%-5s", twoDecimalPlaces.format((double)((double)_project.getPanel().getNumInspectedJoints()/
                  (double)_project.getPanel().getNumJoints()) * 100)) + String.format("%-6s", "%") + "|");
      _os.println(String.format("%-1s","|") + "-------------------------------------------------------------|");
      _os.println(String.format("%-62s","|") + "|");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTotalOfUnloadedComponentAndJointLevel()
  {
      _os.println(String.format("%-6s","|") + String.format("%-34s", "No Load Component") + String.format("%-22s", "No Load Joint") + "|");
      _os.println(String.format("%-6s","|") + "Top      : " + String.format("%-23d", _numberOfTotalTopSideUnloadedComponents.get())
                  + "Top      : " + String.format("%-11d", _numberOfTotalTopSideUnloadedJoints.get()) + "|");
      _os.println(String.format("%-6s","|") + "Bottom   : " + String.format("%-23d", _numberOfTotalBtmSideUnloadedComponents.get())
                 + "Bottom   : " + String.format("%-11d", _numberOfTotalBtmSideUnloadedJoints.get()) + "|");
      _os.println(String.format("%-1s","|") + "-------------------------------------------------------------|");
      _os.println(String.format("%-6s","|") + "Total    : " + String.format("%-23d",_numberOfTotalTopSideUnloadedComponents.get()
                  + _numberOfTotalBtmSideUnloadedComponents.get()) + "Total    : "
                  + String.format("%-11d", _numberOfTotalTopSideUnloadedJoints.get() + _numberOfTotalBtmSideUnloadedJoints.get()) + "|" );
      _os.println(String.format("%-1s","|") + "-------------------------------------------------------------|");
      _os.println(String.format("%-62s","|") + "|");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTotalOfNoTestComponentAndJointLevel()
  {
      _os.println(String.format("%-6s","|") + String.format("%-34s", "No Test Component") + String.format("%-22s", "No Test Joint") + "|");
      _os.println(String.format("%-6s","|") + "Top      : " + String.format("%-23d", _numberOfTotalTopSideNoTestComponents.get())
                  + "Top      : " + String.format("%-11d", _numberOfTotalTopSideNoTestJoints.get()) + "|");
      _os.println(String.format("%-6s","|") + "Bottom   : " + String.format("%-23d", _numberOfTotalBtmSideNoTestComponents.get())
                  + "Bottom   : " + String.format("%-11d", _numberOfTotalBtmSideNoTestJoints.get()) + "|");
      _os.println(String.format("%-1s","|") + "-------------------------------------------------------------|");
      _os.println(String.format("%-6s","|") + "Total    : " + String.format("%-23d",_numberOfTotalTopSideNoTestComponents.get()
                  + _numberOfTotalBtmSideNoTestComponents.get()) + "Total    : "
                  + String.format("%-11d", _numberOfTotalTopSideNoTestJoints.get() + _numberOfTotalBtmSideNoTestJoints.get()) + "|" );
      _os.println(String.format("%-1s","|") + "-------------------------------------------------------------|");
      _os.println(String.format("%-62s","|") + "|");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTotalOfUntestableComponentAndJointLevel()
  {
      _os.println(String.format("%-6s","|") + String.format("%-34s", "Untestable Component") + String.format("%-22s", "Untestable Joint") + "|");
      _os.println(String.format("%-6s","|") + "Top      : " + String.format("%-23d", _numberOfTopSideUntestableComponents.get())
                  + "Top      : " + String.format("%-11d", _numberOfTopSideUntestableJoints.get()) + "|");
      _os.println(String.format("%-6s","|") + "Bottom   : " + String.format("%-23d", _numberOfBtmSideUntestableComponents.get())
                  + "Bottom   : " + String.format("%-11d", _numberOfBtmSideUntestableJoints.get()) + "|");
      _os.println(String.format("%-1s","|") + "-------------------------------------------------------------|");
      _os.println(String.format("%-6s","|") + "Total    : " + String.format("%-23d",_numberOfTopSideUntestableComponents.get()
                  + _numberOfBtmSideUntestableComponents.get()) + "Total    : "
                  + String.format("%-11d", _numberOfTopSideUntestableJoints.get() + _numberOfBtmSideUntestableJoints.get()) + "|" );
      _os.println(String.format("%-1s","|") + "-------------------------------------------------------------|");
      _os.println(String.format("%-62s","|") + "|");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTestCoverageBreakdown()
  {
      _os.println("-------------------------------------------------------------------------------------" +
                  "-------------------------------------------------------------------------------------" +
                  "---------------------------------------------------------------------------------------------------------");
      _os.println(String.format("%-40s", "Package Lists") + String.format("%-40s", "JointType") +
                  String.format("%-40s", "Num of Package") + String.format("%-41s","Pins per Package") +
                  String.format("%-35s", "Total Pins") +  String.format("%-41s", "Total Pin Tested") +
                  String.format("%-42s", "Total No Load Pin") +  String.format("%-42s", "Total No Test Pin") +
                  String.format("%-45s", "Total Untestable Pin") + String.format("%-47s","Package (Pin-Coverage)"));
      _os.println("-------------------------------------------------------------------------------------" +
                  "-------------------------------------------------------------------------------------" +
                  "---------------------------------------------------------------------------------------------------------");
      for (Board board : _project.getPanel().getBoards()){
             for(CompPackage compPackage : board.getCompPackages()){
                 _os.println(String.format("%-40s", compPackage.getShortName()) +
                             String.format("%-45s", (compPackage.usesOneJointTypeEnum() == true) ? compPackage.getJointTypeEnum().getName() : _MIXED_JOINT_TYPE)+
                             String.format("%-40s", _jointTypeToPackageMap.get(compPackage)) +
                             String.format("%-40s", compPackage.getLandPattern().getNumberOfLandPatternPads()) +
                             String.format("%-38s", (_jointTypeToPackageMap.get(compPackage) * compPackage.getLandPattern().getNumberOfLandPatternPads())) +
                             String.format("%-42s", _testedJointToPackageMap.get(compPackage)) +
                             String.format("%-42s", _noLoadJointToPackageMap.get(compPackage)) +
                             String.format("%-42s", _noTestJointToPackageMap.get(compPackage)) +
                             String.format("%-45s", _untestableJointToPackageMap.get(compPackage)) +
                             String.format("%-25s", twoDecimalPlaces.format((double)((double)_testedJointToPackageMap.get(compPackage) /
                             ((double)_jointTypeToPackageMap.get(compPackage) * (double)compPackage.getLandPattern().getNumberOfLandPatternPads())* 100))) +
                             String.format("%-18s", "%"));
             }
      }
      _os.println("-------------------------------------------------------------------------------------" +
                  "-------------------------------------------------------------------------------------" +
                  "---------------------------------------------------------------------------------------------------------");
      _os.println("");
  }

  /**
   * @author Chin-Seong, Kee
  */
  private void writeNoLoadPackageList()
  {
      _os.println(String.format("%-50s", "NO LOAD LIST"));
      _os.println("------------------------------------------------------------------------------------------------------------" +
                  "-----------------------------------------------------------------------------------------------------");
      _os.println(String.format("%-50s", "Package") + String.format("%-50s", "JointType") +
                                String.format("%-50s", "Component") + String.format("%-50s", "Joint"));
      _os.println("------------------------------------------------------------------------------------------------------------" +
                  "-----------------------------------------------------------------------------------------------------");

      int totalPad = 0;
      
      if(_numberOfTotalTopSideUnloadedComponents.get() + _numberOfTotalBtmSideUnloadedComponents.get() > 0){
          for (Board board : _project.getPanel().getBoards()){
              for(Component component : board.getComponents()){
                 if(component.getComponentType().isLoaded() == false){
                     for(Pad pad : component.getPads())
                        totalPad ++ ;
                     _os.println(String.format("%-50s", component.getCompPackage().getShortName()) +
                                 String.format("%-50s", (component.getCompPackage().usesOneJointTypeEnum() == true) ?
                                                         component.getCompPackage().getJointTypeEnum().getName() : _MIXED_JOINT_TYPE) +
                                 String.format("%-50s", component.getComponentType().toString()) +
                                 String.format("%-50s", totalPad));
                     totalPad = 0;
                 }
              }
          }
      }
      else
          _os.println("N\\A");

      _os.println("------------------------------------------------------------------------------------------------------------" +
                  "-----------------------------------------------------------------------------------------------------");
      _os.println("");
  }

/**
   * @author Chin-Seong, Kee
   */
  private void writeNoTestPackageList()
  {
      _os.println(String.format("%-50s", "NO TEST LIST"));
      _os.println("------------------------------------------------------------------------------------------------------------" +
                  "-----------------------------------------------------------------------------------------------------");
      _os.println(String.format("%-50s", "Package") + String.format("%-50s", "JointType") +
                                String.format("%-50s", "Component") + String.format("%-50s", "Pad Name"));
      _os.println("------------------------------------------------------------------------------------------------------------" +
                  "-----------------------------------------------------------------------------------------------------");

      int totalPad = 0;
      
      if(_numberOfTotalTopSideNoTestComponents.get() + _numberOfTotalBtmSideNoTestComponents.get() > 0){
          for (Board board : _project.getPanel().getBoards()){
              for(Component component : board.getComponents()){
                 if(component.getComponentType().isLoaded() == true && component.getComponentType().isInspected() == false
                    && component.getComponentType().isTestable() == true){
                     for(Pad pad : component.getPads()){
                         _os.println(String.format("%-50s", component.getCompPackage().getShortName()) +
                                     String.format("%-50s", (component.getCompPackage().usesOneJointTypeEnum() == true) ?
                                                             component.getCompPackage().getJointTypeEnum().getName() : _MIXED_JOINT_TYPE) +
                                     String.format("%-50s", component.getComponentType().toString()) +
                                     String.format("%-50s", pad.getName()));
                     }
                     totalPad = 0;
                 }
              }
          }
      }
      else
        _os.println("N\\A");

      _os.println("------------------------------------------------------------------------------------------------------------" +
                  "-----------------------------------------------------------------------------------------------------");
      _os.println("");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeUntestablePackageList()
  {
      _os.println(String.format("%-50s", "UNTESTABLE LIST"));
      _os.println("------------------------------------------------------------------------------------------------------------" +
                  "-----------------------------------------------------------------------------------------------------");
      _os.println(String.format("%-50s", "Package") + String.format("%-50s", "JointType") +
                                String.format("%-50s", "Component") + String.format("%-50s", "Pad Name") + String.format("%-50s", "Reason") + String.format("%-50s", "BoardName"));
      _os.println("------------------------------------------------------------------------------------------------------------" +
                  "-----------------------------------------------------------------------------------------------------");

      if(_numberOfBtmSideUntestableComponents.get() + _numberOfTopSideUntestableComponents.get() > 0)
      {
          for (Board board : _project.getPanel().getBoards()){
              for(Component component : board.getComponents()){
                 if(component.getComponentType().isTestable() == false){
                     for(Pad pad : component.getPads()){
                       if(pad.getPadSettings().isTestable() == false)
                       {
                         _os.println(String.format("%-50s", component.getCompPackage().getShortName()) +
                            String.format("%-50s", (component.getCompPackage().usesOneJointTypeEnum() == true) ?
                                                    component.getCompPackage().getJointTypeEnum().getName() : _MIXED_JOINT_TYPE) +
                            String.format("%-50s", component.getComponentType().toString()) +
                            String.format("%-50s", pad.getName()) +
                            String.format("%-50s", pad.getPadSettings().getUntestableReason()) +
                            String.format("%-50s", pad.getBoardAndComponentAndPadName().split(" ")[0]));
                       }
                     }
                 }
              }
          }
      }
      else
          _os.println("N\\A");

      _os.println("------------------------------------------------------------------------------------------------------------" +
                  "-----------------------------------------------------------------------------------------------------");
      _os.println("");
  }

 /**
 * @author Chin-Seong,Kee
 */
  private void writeComponentTestedAlgorithmSettingsLevel()
  {
     _os.println("-------------------------------------------------------------------------------------" +
                  "-------------------------------------------------------------------------------------" +
                  "---------------------------------------------------------------------------------------------------------");
     _os.println(String.format("%-40s", "RefDes") + String.format("%-40s", "JointType") +
                 String.format("%-20s", "Side of Card") +
                 String.format("%-15s", AlgorithmEnum.EXCESS) +
                 String.format("%-15s", AlgorithmEnum.INSUFFICIENT) + String.format("%-15s", AlgorithmEnum.MISALIGNMENT) +
                 String.format("%-15s", AlgorithmEnum.OPEN) + String.format("%-15s", AlgorithmEnum.SHORT) +
                 String.format("%-15s", AlgorithmEnum.VOIDING));
     _os.println("-------------------------------------------------------------------------------------" +
                  "-------------------------------------------------------------------------------------" +
                  "---------------------------------------------------------------------------------------------------------");
      for (Board board : _project.getPanel().getBoards()){
           for(Component component : board.getComponents()){
                for(Pad pad : component.getPads()){
                    List<AlgorithmEnum> algo = pad.getSubtype().getAlgorithmEnums();                    
                    //Bee Hoon, XCR 1649 - Exclude not tested/no load component in RefDes 
                    if(component.isInspected()){
                      _os.println(String.format("%-40s", component.getReferenceDesignator()) +
                                  String.format("%-45s", pad.getJointTypeEnum())+
                                  String.format("%-20s", pad.isTopSide() ? "T" : "B") +
                                  String.format("%-15s", algo.contains(AlgorithmEnum.EXCESS) ? "Y" : "N") +
                                  String.format("%-16s", algo.contains(AlgorithmEnum.INSUFFICIENT) ? "Y" : "N") +
                                  String.format("%-12s", algo.contains(AlgorithmEnum.MISALIGNMENT) ? "Y" : "N") +
                                  String.format("%-14s", algo.contains(AlgorithmEnum.OPEN) ? "Y" : "N") +
                                  String.format("%-14s", algo.contains(AlgorithmEnum.SHORT) ? "Y" : "N") +
                                  String.format("%-12s", algo.contains(AlgorithmEnum.VOIDING) ? "Y" : "N"));
                    }
                    else if(component.isInspected() == false && component.isLoaded())
                    {
                      //Siew Yeng
                      //XCR1724 - Add No Test component to the test coverage list but with all NNNNN
                      _os.println(String.format("%-40s", component.getReferenceDesignator()) +
                                  String.format("%-45s", pad.getJointTypeEnum())+
                                  String.format("%-20s", pad.isTopSide() ? "T" : "B") +
                                  String.format("%-15s", "N") +
                                  String.format("%-16s", "N") +
                                  String.format("%-12s", "N") +
                                  String.format("%-14s", "N") +
                                  String.format("%-14s", "N") +
                                  String.format("%-12s", "N"));
                    }
                    break;
                }
           }
      }

     _os.println("-------------------------------------------------------------------------------------" +
                 "-------------------------------------------------------------------------------------" +
                 "---------------------------------------------------------------------------------------------------------");
     _os.println("");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void calculateComponentLevel()
  {
      for (Board board : _project.getPanel().getBoards()){
           for(Component component : board.getComponents()){
               if(component.getComponentType().isLoaded()){
                   if(component.isTopSide()){
                       _numberOfTopSideLoadedComponents.incrementAndGet();
                       if(component.getComponentType().isTestable() == true){
                           if(component.getComponentType().isInspected() == true)
                               _numberOfTopSideTestedComponents.incrementAndGet();
                           else
                               _numberOfTotalTopSideNoTestComponents.incrementAndGet();
                       }
                       else
                           _numberOfTopSideUntestableComponents.incrementAndGet();
                   }
                   else if (component.isBottomSide()){
                       _numberOfBtmSideLoadedComponents.incrementAndGet();
                       if(component.getComponentType().isTestable() == true){
                           if(component.getComponentType().isInspected() == true)
                               _numberOfBtmSideTestedComponents.incrementAndGet();
                           else
                               _numberOfTotalBtmSideNoTestComponents.incrementAndGet();
                       }
                       else
                          _numberOfBtmSideUntestableComponents.incrementAndGet();
                   }
               }
               else{
                   if(component.isTopSide())
                      _numberOfTotalTopSideUnloadedComponents.incrementAndGet();
                   else
                      _numberOfTotalBtmSideUnloadedComponents.incrementAndGet();
               }
           }
      }
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void calculateJointLevel()
  {
      for (Board board : _project.getPanel().getBoards()){
           for(Component component : board.getComponents()){
               if(component.getComponentType().isLoaded()){
                   for(Pad pad : component.getPads()){
                       if(pad.isTopSide()){
                           _numberOfTopSideLoadedJoints.incrementAndGet();
                           if(pad.getPadSettings().isTestable() == true){
                               if(pad.getPadType().isInspected() == true)
                                   _numberOfTopSideTestedJoints.incrementAndGet();
                               else
                                   _numberOfTotalTopSideNoTestJoints.incrementAndGet();
                           }
                           else
                               _numberOfTopSideUntestableJoints.incrementAndGet();

                       }
                       else{
                           _numberOfBtmSideLoadedJoints.incrementAndGet();
                            if(pad.getPadSettings().isTestable() == true){
                                if(pad.getPadType().isInspected() == true)
                                   _numberOfBtmSideTestedJoints.incrementAndGet();
                                else
                                   _numberOfTotalBtmSideNoTestJoints.incrementAndGet();
                            }
                            else
                               _numberOfBtmSideUntestableJoints.incrementAndGet();
                       }
                   }
               }
               else{
                   for(Pad pad : component.getPads()){
                       if(pad.isTopSide())
                          _numberOfTotalTopSideUnloadedJoints.incrementAndGet();
                       else
                          _numberOfTotalBtmSideUnloadedJoints.incrementAndGet();
                   }
               }
           }
      }
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void calculatePackageToComponent()
  {
      int compToCompPackageCounter = 0;
      int compUnloadedToCompPackageCounter = 0;
      int compUntestableToCompPackageCounter = 0;
      int compTestedToCompPackageCounter = 0;
      int compNoTestedToCompPackageCounter = 0;

      for(Board board : _project.getPanel().getBoards())
      {
          for(CompPackage compPackage : board.getCompPackages())
          {
               for(Component component : board.getComponents())
               {
                   if(compPackage.getShortName().compareTo(component.getCompPackage().getShortName()) == 0)
                   {
                       compToCompPackageCounter ++;
                       if(component.getComponentType().isLoaded()){
                           if(component.getComponentType().isTestable()){
                                if(component.getComponentType().isInspected())
                                    compTestedToCompPackageCounter += calculateNumberOfPin(component);
                                else
                                    compNoTestedToCompPackageCounter += calculateNumberOfPin(component);;
                           }
                           else
                              compUntestableToCompPackageCounter += calculateNumberOfPin(component);;
                       }
                       else
                           compUnloadedToCompPackageCounter += calculateNumberOfPin(component);;
                   }
               }
               _jointTypeToPackageMap.put(compPackage, compToCompPackageCounter);
               _noLoadJointToPackageMap.put(compPackage, compUnloadedToCompPackageCounter);
               _testedJointToPackageMap.put(compPackage, compTestedToCompPackageCounter);
               _untestableJointToPackageMap.put(compPackage, compUntestableToCompPackageCounter);
               _noTestJointToPackageMap.put(compPackage, compNoTestedToCompPackageCounter);

               compToCompPackageCounter = 0;
               compUnloadedToCompPackageCounter = 0;
               compUntestableToCompPackageCounter = 0;
               compTestedToCompPackageCounter = 0;
               compNoTestedToCompPackageCounter = 0;
          }
      }
  }

  /**
   * @author Chin-Seong, Kee
   */
  private int calculateNumberOfPin(Component component)
  {
      int padCounter = 0;

      for(Pad pad : component.getPads())
      {
          padCounter ++;
      }
      return padCounter;
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void close()
  {
    if (_os != null)
      _os.close();
  }
}
