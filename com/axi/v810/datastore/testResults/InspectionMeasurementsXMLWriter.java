package com.axi.v810.datastore.testResults;

import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
public class InspectionMeasurementsXMLWriter
{
  private static InspectionMeasurementsXMLWriter _instance;
  private boolean _printSpaces = false;
  private boolean _exceptionThrown = false;
  private Project _project;
  private PrintWriter _os;
  private String _cadFilePath;
  private String _cadFileInResultsDirName;
  private String _cadFileInResultsDirFullPath;
  private String _settingsFilePath;
  private String _settingsFileInResultsDirName;
  private String _settingsFileInResultsDirFullPath;
  private String _cadXmlSchemaFilePath;
  private String _cadXmlSchemaInResultsDirFilePath;
  private String _measurementsXmlSchemaFilePath;
  private String _measuremntsXmlSchemaInResultsDirFilePath;
  private String _settingsXmlSchemaFilePath;
  private String _settingsXmlSchemaInResultsDirFilePath;
  private String _inspectionResultsDir;
  private String _projName;
  private BooleanRef _productionMode; //if false, this is a demo mode
  private int _panelNumberOfIndictments;
  private int _panelNumberProcessedComponents;
  private int _panelNumberDefectiveComponents;
  private int _panelNumberProcessedJoints;
  private int _panelNumberDefectiveJoints;
  private String _panelSerialNumber;
  private long _inspRunTimeStamp;
  private String _startDateTime;
  private String _endDateTime;
  private int _indent = 0;
  String _measurementsFilePath;
  String _tempMeasurementsFilePath;
  private int _numOfSpaces = XMLWriterStringUtil.NUM_INDENT_SPACES;
  // keep this for future reference, if comments are added to this file
  private String _beginCommentTag = XMLWriterStringUtil.BEGIN_COMMENT_TAG;
  private String _endCommentTag = XMLWriterStringUtil.END_COMMENT_TAG;
  private String _gt = XMLWriterStringUtil.GREATER_THEN_TAG;
  private String _dbQuote = XMLWriterStringUtil.ESCAPED_DOUBLE_QUOTE;
  private String _elemEnd  = XMLWriterStringUtil.ELEMENT_END_TAG;
  private String _dbQuoteGt = _dbQuote + _gt;
  private String _dbQtElemEnd = _dbQuote + _elemEnd;
  private Map<Board, BoardMeasurementsXMLWriter> _boardToBoardXMLWriterMap = new HashMap<Board,BoardMeasurementsXMLWriter>();
  private Map<String, String> _boardNameToBoardSerialNumMap = new HashMap<String, String>();
  private Map<Board, String> _boardToBoardResultsFilePathMap = new HashMap<Board,String>();
  private String _isPanelTestedReason;

  /**
   * @author Laura Cormos
   */
  public static synchronized InspectionMeasurementsXMLWriter getInstance()
  {
    if (_instance == null)
      _instance = new InspectionMeasurementsXMLWriter();

    return _instance;
  }

  /**
   * @author Laura Cormos
   */
  public InspectionMeasurementsXMLWriter()
  {
    // do nothing
  }

  /**
   * @author Laura Cormos
   */
  public void open(Project project,
                   long inspRunTimeStamp,
                   String panelSerialNumber,
                   Map<String, String> boardNameToBoardSerialNumMap,
                   String isPanelTestedReason) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(inspRunTimeStamp > 0);
    Assert.expect(boardNameToBoardSerialNumMap != null);
    Assert.expect(panelSerialNumber != null);
    Assert.expect(isPanelTestedReason != null);

    _inspRunTimeStamp = inspRunTimeStamp;
    DateFormat myformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz");
    _startDateTime = myformat.format(new java.util.Date(_inspRunTimeStamp));
    _project = project;
    _projName = project.getName();
    _boardNameToBoardSerialNumMap = boardNameToBoardSerialNumMap;
    _panelSerialNumber = panelSerialNumber;
    _isPanelTestedReason = isPanelTestedReason;

    if (_boardToBoardXMLWriterMap == null)
      _boardToBoardXMLWriterMap = new HashMap<Board, BoardMeasurementsXMLWriter>();
    if (_boardToBoardResultsFilePathMap == null)
      _boardToBoardResultsFilePathMap = new HashMap<Board, String>();

    // figure out all the file paths and file names
    _inspectionResultsDir = Directory.getInspectionResultsDir(_projName, _inspRunTimeStamp);
    _measurementsFilePath = FileName.getXMLMeasurementsFileFullPath(_projName, _inspRunTimeStamp);
    _tempMeasurementsFilePath = FileName.getXMLTempMeasurementsFileFullPath(_projName, _inspRunTimeStamp);

    _cadFilePath = FileName.getCadFileNameFullPath(_projName, _project.getCadXmlChecksum());
    _cadFileInResultsDirName = FileName.getCadFileName(_project.getCadXmlChecksum());
    _cadFileInResultsDirFullPath = FileName.getCadFileNameInResultsDirFullPath(_projName,
                                                                               _project.getCadXmlChecksum());

    _settingsFilePath = FileName.getSettingsFileNameFullPath(_projName, _project.getSettingsXmlChecksum());
    _settingsFileInResultsDirName = FileName.getSettingsFileName(_project.getSettingsXmlChecksum());
    _settingsFileInResultsDirFullPath = FileName.getSettingsFileNameInResultsDirFullPath(_projName,
                                                                                         _project.getSettingsXmlChecksum());

    _cadXmlSchemaFilePath = FileName.getCadXmlSchemaFileNameFullPath();
    _cadXmlSchemaInResultsDirFilePath = FileName.getCadXmlSchemaInResultsDirFullPath(_projName, _inspRunTimeStamp);

    _measurementsXmlSchemaFilePath = FileName.getResultsXmlSchemaFileNameFullPath();
    _measuremntsXmlSchemaInResultsDirFilePath = FileName.getResultsXmlSchemaInResultsDirFullPath(_projName, _inspRunTimeStamp);

    _settingsXmlSchemaFilePath = FileName.getSettingsXmlSchemaFileNameFullPath();
    _settingsXmlSchemaInResultsDirFilePath = FileName.getSettingsXmlSchemaInResultsDirFullPath(_projName, _inspRunTimeStamp);

    if (FileUtilAxi.exists(_inspectionResultsDir) == false)
      FileUtilAxi.mkdirs(_inspectionResultsDir);

    try
    {
      _os = new PrintWriter(_tempMeasurementsFilePath);
      // write the header and any comments
      _os.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
      if (_printSpaces)
        _os.println("");

      writeTopLevelElementBeginning();
    }
    catch (FileNotFoundException ex)
    {
      _exceptionThrown = true;
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_tempMeasurementsFilePath);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      // if the file still exists and an exception was thrown, delete it
      if (_exceptionThrown && FileUtil.exists(_tempMeasurementsFilePath))
      {
        try
        {
          // close the file in case an exception was thrown before the file was closed
          if (_os != null)
            closePrintWriter();

          FileUtilAxi.delete(_tempMeasurementsFilePath);
        }
        catch (CannotDeleteFileDatastoreException ex)
        {
          if (_exceptionThrown)
            ex.printStackTrace();
        }
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  public void close(long endDateTime) throws DatastoreException
  {
    try
    {
      // close each of the opened board XML writers and get the summary from each
      if (_boardToBoardXMLWriterMap != null)
      {
        for (BoardMeasurementsXMLWriter boardXMLWriter : _boardToBoardXMLWriterMap.values())
        {
          BoardResultsSummary boardSummary = boardXMLWriter.close();
          boardXMLWriter.CheckXOutBoard();
          if(!boardXMLWriter.isXoutBoard()){
              _panelNumberOfIndictments += boardSummary.getNumberOfIndictments();
              _panelNumberProcessedComponents += boardSummary.getNumberProcessedComponents();
              _panelNumberDefectiveComponents += boardSummary.getNumberDefectiveComponents();
              _panelNumberProcessedJoints += boardSummary.getNumberProcessedJoints();
              _panelNumberDefectiveJoints += boardSummary.getNumberDefectiveJoints();
          }
        }
      }
      DateFormat myformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz");
      _endDateTime = myformat.format(new java.util.Date(endDateTime));

      writePanelElement();

      if (_os != null)
        closePrintWriter();

      String tempConcatFile = FileName.getConcatenationIntermediateFileFullPath(_inspectionResultsDir);

      // if there are any board files, concatenate them
      if (_boardToBoardResultsFilePathMap != null && _boardToBoardResultsFilePathMap.keySet().size() > 0)
      {
        String tempFile;
        List<String> filePaths = new ArrayList<String>();
        filePaths.addAll(_boardToBoardResultsFilePathMap.values());
        String boardResultsFilePath = filePaths.get(0);
        for (int i = 1; i < filePaths.size(); i++)
        {
          tempFile = FileUtil.getTempFileName(_measurementsFilePath + i);
          try
          {
            FileUtilAxi.concat(boardResultsFilePath, filePaths.get(i), tempFile);
            if (FileUtilAxi.exists(boardResultsFilePath))
              FileUtilAxi.delete(boardResultsFilePath);
            if (FileUtilAxi.exists(filePaths.get(i)))
              FileUtilAxi.delete(filePaths.get(i));
          }
          catch (CannotDeleteFileDatastoreException ex)
          {
            _exceptionThrown = true;
            throw ex;
          }
          catch (FileNotFoundDatastoreException ex)
          {
            _exceptionThrown = true;
            throw ex;
          }
          catch (CannotReadDatastoreException ex)
          {
            _exceptionThrown = true;
            throw ex;
          }
          finally
          {
            if (_exceptionThrown)
            {
              if (FileUtilAxi.exists(tempFile))
                FileUtilAxi.delete(tempFile);
            }
          }

          boardResultsFilePath = tempFile;
        }

        // now concatenate the top part of the final indictments.xml file with the file containting all the board results
        FileUtilAxi.concat(_tempMeasurementsFilePath, boardResultsFilePath, tempConcatFile);
        if (FileUtilAxi.exists(boardResultsFilePath))
          FileUtilAxi.delete(boardResultsFilePath);
      }
      else // otherwise, just rename the exising file to the intermediate file
      {
        if (FileUtilAxi.exists(_tempMeasurementsFilePath))
          FileUtilAxi.rename(_tempMeasurementsFilePath, tempConcatFile);
        else
        {
          FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_tempMeasurementsFilePath);
          throw dex;
        }
      }

      // finish writing out the bottom part of the indictments.xml
      _os = new PrintWriter(_tempMeasurementsFilePath);
      writeTopLevelElementEnding();

      if (_os != null)
        closePrintWriter();

      // one last concatenation for the very last element closing
      FileUtilAxi.concat(tempConcatFile, _tempMeasurementsFilePath, _measurementsFilePath);

      try
      {
        // clean up all leftover temp files
        if (FileUtilAxi.exists(_tempMeasurementsFilePath))
          FileUtilAxi.delete(_tempMeasurementsFilePath);
        if (FileUtilAxi.exists(tempConcatFile))
          FileUtilAxi.delete(tempConcatFile);
        // copy the cad and settings xml files into the results directory
        if (FileUtil.exists(_cadFileInResultsDirFullPath) == false)
          FileUtilAxi.copy(_cadFilePath, _cadFileInResultsDirFullPath);
        if (FileUtil.exists(_settingsFileInResultsDirFullPath) == false)
          FileUtilAxi.copy(_settingsFilePath, _settingsFileInResultsDirFullPath);
        // copy the schema files in the inspectionRun directory
        FileUtilAxi.copy(_cadXmlSchemaFilePath, _cadXmlSchemaInResultsDirFilePath);
        FileUtilAxi.copy(_measurementsXmlSchemaFilePath, _measuremntsXmlSchemaInResultsDirFilePath);
        FileUtilAxi.copy(_settingsXmlSchemaFilePath, _settingsXmlSchemaInResultsDirFilePath);
      }
      catch (CannotDeleteFileDatastoreException ex)
      {
        _exceptionThrown = true;
        throw ex;
      }
      catch (CannotCopyFileDatastoreException ex)
      {
        _exceptionThrown = true;
        throw ex;
      }
      catch (FileNotFoundDatastoreException ex)
      {
        _exceptionThrown = true;
        throw ex;
      }
    }
    catch (FileNotFoundException ex)
    {
      _exceptionThrown = true;
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_tempMeasurementsFilePath);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      // close the file in case an exception was thrown before the file was closed
      if (_os != null)
        closePrintWriter();

      //clear up board writers internal structures
      _boardToBoardXMLWriterMap = null;
      _boardToBoardResultsFilePathMap = null;
      _project = null;

      // reset counters
      _panelNumberOfIndictments = 0;
      _panelNumberProcessedComponents = 0;
      _panelNumberDefectiveComponents = 0;
      _panelNumberProcessedJoints = 0;
      _panelNumberDefectiveJoints = 0;

      // reset exception flag
      _exceptionThrown = false;
    }
  }

  /**
   * @author Laura Cormos
   * @author Patrick Lacz
   */
  public void writeJointInspectionResult(JointInspectionResult jointInspResult) throws DatastoreException
  {
    Assert.expect(jointInspResult != null);

    Board board = jointInspResult.getJointInspectionData().getBoard();
    BoardMeasurementsXMLWriter boardXMLWriter = _boardToBoardXMLWriterMap.get(board);
    if (boardXMLWriter == null)
    {
      // this is the first time a joint from this board is coming through. Initialize the board xml writer
      boardXMLWriter = initializeBoardXmlWriter(board);
    }
    boardXMLWriter.writeJointInspectionResult(jointInspResult);
  }


  /**
   * @author Laura Cormos
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  public void writeComponentInspectionResult(ComponentInspectionResult componentInspectionResult) throws DatastoreException
  {
    Assert.expect(componentInspectionResult != null);

    Board board = componentInspectionResult.getComponentInspectionData().getComponent().getBoard();
    BoardMeasurementsXMLWriter boardXMLWriter = _boardToBoardXMLWriterMap.get(board);
    if (boardXMLWriter == null)
    {
      // this is the first time a component from this board is coming through. Initialize the board xml writer
      boardXMLWriter = initializeBoardXmlWriter(board);
    }
    boardXMLWriter.writeComponentInspectionResult(componentInspectionResult);
  }

  /**
   * @author Peter Esbensen
   */
  private BoardMeasurementsXMLWriter initializeBoardXmlWriter(Board board) throws DatastoreException
  {
    Assert.expect(board != null);
    BoardMeasurementsXMLWriter boardXMLWriter = new BoardMeasurementsXMLWriter();
    _boardToBoardXMLWriterMap.put(board, boardXMLWriter);
    String boardXMLresultsFilePath = FileName.getXMLBoardMeasurementsFileFullPath(board, _projName, _inspRunTimeStamp);
    _boardToBoardResultsFilePathMap.put(board, boardXMLresultsFilePath);
    boardXMLWriter.open(board, boardXMLresultsFilePath, _boardNameToBoardSerialNumMap.get(board.getName()), _indent, _printSpaces);
    return boardXMLWriter;
  }

  /**
   * @author Laura Cormos
   */
  private void writeTopLevelElementBeginning() throws DatastoreException
  {
    Assert.expect(_os != null);
    Assert.expect(_indent >= 0);

    StringBuilder spaces = new StringBuilder("");
    _os.println("<inspectionResults");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    _os.println(spaces + "schemaName=" + _dbQuote + FileName.getInspectionIndictmentsXmlSchemaFileName() + _dbQuote);
    _os.println(spaces + "dateAndTime=" + _dbQuote + _startDateTime + _dbQuote);
    if (isProductionMode())
      _os.println(spaces + "mode=" + _dbQuote + "test execution" + _dbQuoteGt);
    else
      _os.println(spaces + "mode=" + _dbQuote + "demo" + _dbQuoteGt);

    ++_indent;
    writeSettingsFileElement();
    writeCadFileElement();
    writeXrayTesterElement();
    writeProjectElement();

  }

  /**
   * @author Laura Cormos
   */
  private void writeTopLevelElementEnding()
  {
    Assert.expect(_os != null);

    _os.println("</inspectionResults>");
  }

  /**
   * @author Laura Cormos
   */
  private void writeSettingsFileElement()
  {
    Assert.expect(_os != null);
    Assert.expect(_indent >= 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (_indent * _numOfSpaces); i++)
        spaces.append(" ");
    _os.println(spaces + "<settingsFile");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    Assert.expect(_settingsFilePath != null);
    _os.println(spaces + "name=" + _dbQuote + _settingsFileInResultsDirName + _dbQtElemEnd);
  }

  /**
   * @author Laura Cormos
   */
  private void writeCadFileElement()
  {
    Assert.expect(_os != null);
    Assert.expect(_indent >= 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (_indent * _numOfSpaces); i++)
        spaces.append(" ");
    _os.println(spaces + "<cadFile");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    Assert.expect(_settingsFilePath != null);
    _os.println(spaces + "name=" + _dbQuote + _cadFileInResultsDirName + _dbQtElemEnd);
  }

  /**
   * @author Laura Cormos
   */
  private void writeXrayTesterElement()
  {
//    <xrayTester serialNumber="300" controllerHostName="mtdlaura" controllerIPAddress="192.168.1.1" />
    Assert.expect(_os != null);
    Assert.expect(_indent >= 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (_indent * _numOfSpaces); i++)
        spaces.append(" ");
    _os.println(spaces + "<xrayTester");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    Assert.expect(_settingsFilePath != null);

    _os.println(spaces + "serialNumber=" + _dbQuote + XrayTester.getInstance().getSerialNumber() + _dbQuote);
    try
    {
      String hostIP = InetAddress.getLocalHost().toString();
      String hostname = InetAddress.getLocalHost().getHostName();
      String IP = hostIP.substring(hostname.length() + 1);
      _os.println(spaces + "controllerHostName=" + _dbQuote + hostname + _dbQuote);
      _os.println(spaces + "controllerIPAddress=" + _dbQuote + IP + _dbQtElemEnd);
    }
    catch (UnknownHostException ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Laura Cormos
   */
  private void writeProjectElement()
  {
    Assert.expect(_os != null);
    Assert.expect(_indent >= 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (_indent * _numOfSpaces); i++)
        spaces.append(" ");
    _os.println(spaces + "<project");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    _os.println(spaces + "name=" + _dbQuote + XMLWriterStringUtil.replaceIllegalStringChars(_projName) + _dbQuote);
    _os.println(spaces + "version=" + _dbQuote + _project.getVersion() + _dbQuote);
    _os.println(spaces + "softwareVersionDuringProjectCreation=" + _dbQuote + _project.getSoftwareVersionOfProjectLastSave() + _dbQuote);
    _os.println(spaces + "softwareVersionDuringFileCreation=" + _dbQuote + Version.getVersionNumber() + _dbQtElemEnd);
  }

  /**
   * Writes the panel summary for the indictments xml file
   * <panel serialNumber="3 ab2" indictments="30" components="123" testedComponents="121"
   *       defectiveComponents="0" joints="5" testedJoints="5" defectiveJoints="5">
   * </panel>
   *
   * @author Laura Cormos
   */
  private void writePanelElement() throws DatastoreException
  {
    Assert.expect(_indent >= 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (_indent * _numOfSpaces); i++)
        spaces.append(" ");
    _os.println(spaces + "<panel");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    Panel panel = _project.getPanel();

    _os.println(spaces + "tested=" + _dbQuote + _isPanelTestedReason + _dbQuote);
    _os.println(spaces + "serialNumber=" + _dbQuote + XMLWriterStringUtil.replaceIllegalStringChars(_panelSerialNumber) + _dbQuote);
    _os.println(spaces + "indictments=" + _dbQuote + _panelNumberOfIndictments + _dbQuote);
    _os.println(spaces + "components=" + _dbQuote + panel.getNumComponents() + _dbQuote);
    _os.println(spaces + "testedComponents=" + _dbQuote + _panelNumberProcessedComponents + _dbQuote);
    _os.println(spaces + "defectiveComponents=" + _dbQuote + _panelNumberDefectiveComponents + _dbQuote);
    _os.println(spaces + "joints=" + _dbQuote + panel.getNumJoints() + _dbQuote);
    _os.println(spaces + "testedJoints=" + _dbQuote + _panelNumberProcessedJoints + _dbQuote);
    _os.println(spaces + "defectiveJoints=" + _dbQuote + _panelNumberDefectiveJoints + _dbQuoteGt);

    ++_indent;
    writeInspStartTimeElement(_indent);
    writeInspEndTimeElement(_indent);

    String endElemSpaces = new String("");
    if (_printSpaces)
      endElemSpaces = spaces.substring(0, spaces.length() - _numOfSpaces);
    _os.println(endElemSpaces + "</panel>");
  }

  /**
   * @author Laura Cormos
   */
  private void writeInspStartTimeElement(int indent)
  {
    Assert.expect(indent >= 0);
//  <inspectionStartTime timeZone="MST" year="2006" month="01" day="23" hour="12" minute="23" second="01" />

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    _os.println(spaces + "<inspectionStartTime");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    _os.println(spaces + "timeZone=" + _dbQuote + _startDateTime.substring(20) + _dbQuote);
    _os.println(spaces + "year=" + _dbQuote + _startDateTime.substring(0, 4) + _dbQuote);
    _os.println(spaces + "month=" + _dbQuote + _startDateTime.substring(5, 7) + _dbQuote);
    _os.println(spaces + "day=" + _dbQuote + _startDateTime.substring(8, 10) + _dbQuote);
    _os.println(spaces + "hour=" + _dbQuote + _startDateTime.substring(11, 13) + _dbQuote);
    _os.println(spaces + "minute=" + _dbQuote + _startDateTime.substring(14, 16) + _dbQuote);
    _os.println(spaces + "second=" + _dbQuote + _startDateTime.substring(17, 19) + _dbQtElemEnd);

  }

  /**
   * Writes inspection end time element
   * <inspectionStopTime timeZone="MST" year="2006" month="01" day="23" hour="12" minute="23" second="01" />
   *
   * @author Laura Cormos
   */
  private void writeInspEndTimeElement(int indent)
  {
    Assert.expect(indent >= 0);
    Assert.expect(_endDateTime != null);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    _os.println(spaces + "<inspectionStopTime");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    _os.println(spaces + "timeZone=" + _dbQuote + _startDateTime.substring(20) + _dbQuote);
    _os.println(spaces + "year=" + _dbQuote + _endDateTime.substring(0, 4) + _dbQuote);
    _os.println(spaces + "month=" + _dbQuote + _endDateTime.substring(5, 7) + _dbQuote);
    _os.println(spaces + "day=" + _dbQuote + _endDateTime.substring(8, 10) + _dbQuote);
    _os.println(spaces + "hour=" + _dbQuote + _endDateTime.substring(11, 13) + _dbQuote);
    _os.println(spaces + "minute=" + _dbQuote + _endDateTime.substring(14, 16) + _dbQuote);
    _os.println(spaces + "second=" + _dbQuote + _endDateTime.substring(17, 19) + _dbQtElemEnd);

  }

  /**
   * @author Laura Cormos
   */
  public void setXedOut(Board board)
  {
    Assert.expect(board != null);

    BoardMeasurementsXMLWriter boardXMLWriter = _boardToBoardXMLWriterMap.get(board);
    if (boardXMLWriter != null)
      boardXMLWriter.setXoutBoard();
    else
      Assert.expect(false, "Received Xout command for board \"" + board.getName() + "\" before receiving any joint results!! Something is not right!");
  }

  /**
   * @author Laura Cormos
   */
  public void setProductionMode(boolean productionMode)
  {
    if (_productionMode == null)
      _productionMode = new BooleanRef(productionMode);
    else
      _productionMode.setValue(productionMode);
  }

  /**
   * @author Bill Darbie
   */
  private boolean isProductionMode()
  {
    Assert.expect(_productionMode != null);
    return _productionMode.getValue();
  }

  /**
   * @author Laura Cormos
   */
  Project getProject()
  {
    Assert.expect(_project != null);
    return _project;
  }

  /**
   * @author Laura Cormos
   */
  void closePrintWriter() throws DatastoreException
  {
    Assert.expect(_os != null);

    try
    {
      _os.flush();
      _os.close();
      _os = null;
    }
    catch (IllegalStateException ex)
    {
      // this can happen if the network connection is suddenly severed
      CannotWriteDatastoreException dex = new CannotWriteDatastoreException(_tempMeasurementsFilePath);
      dex.initCause(ex);
      throw dex;
    }
  }


  //Chin Seong :: Xed-Out Process the XedOut Measure file
  void decideBoardXoutStatus(BoardMeasurementsXMLWriter boardXMLWriter)
  {
    Assert.expect(boardXMLWriter != null);
    //Chin Seong :: Add XedOut for the board
    if(boardXMLWriter.getBoard().isXedOut()) //boardXMLWriter.getBoard().isXedOut() &&
       boardXMLWriter.setXoutBoard();
  }
}
