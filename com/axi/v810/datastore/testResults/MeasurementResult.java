package com.axi.v810.datastore.testResults;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;

/**
 * @author Bill Darbie
 */
public class MeasurementResult
{
  private MeasurementEnum _measurementEnum;
  private AlgorithmEnum _algorithmEnum;
  private String _componentName;
  private String _padName;
  private String _subtypeShortName;
  private String _subtypeLongName;
  private SliceNameEnum _sliceNameEnum;
  private float _value;
  private MeasurementUnitsEnum _measurementUnitsEnum;

  /**
   * @author Bill Darbie
   */
  void setMeasurementEnum(MeasurementEnum measurementEnum)
  {
    Assert.expect(measurementEnum != null);
    _measurementEnum = measurementEnum;
  }

  /**
   * @author Bill Darbie
   */
  public MeasurementEnum getMeasurementEnum()
  {
    Assert.expect(_measurementEnum != null);
    return _measurementEnum;
  }

  /**
   * @author Bill Darbie
   */
  void setAlgorithmEnum(AlgorithmEnum algorithmEnum)
  {
    Assert.expect(algorithmEnum != null);
    _algorithmEnum = algorithmEnum;
  }

  /**
   * @author Bill Darbie
   */
  public AlgorithmEnum getAlgorithmEnum()
  {
    Assert.expect(_algorithmEnum != null);
    return _algorithmEnum;
  }

  /**
   * @author Bill Darbie
   */
  void setSubtypeShortName(String subtypeShortName)
  {
    Assert.expect(subtypeShortName != null);
    _subtypeShortName = subtypeShortName;
  }

  /**
   * @author Bill Darbie
   */
  public String getSubtypeShortName()
  {
    Assert.expect(_subtypeShortName != null);
    return _subtypeShortName;
  }

  /**
    * @author Laura Cormos
    */
   void setSubtypeLongName(String subtypeLongName)
   {
     Assert.expect(subtypeLongName != null);
     _subtypeLongName = subtypeLongName;
   }

   /**
    * @author Laura Cormos
    */
   public String getSubtypeLongName()
   {
     Assert.expect(_subtypeLongName != null);
     return _subtypeLongName;
  }

  /**
   * @author Bill Darbie
   */
  void setSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    _sliceNameEnum = sliceNameEnum;
  }

  /**
   * @author Bill Darbie
   */
  public SliceNameEnum getSliceNameEnum()
  {
    Assert.expect(_sliceNameEnum != null);
    return _sliceNameEnum;
  }

  /**
   * @author Bill Darbie
   */
  void setValue(float value)
  {
    _value = value;
  }
  
  void setComponentName(String componentName)
  {
     Assert.expect(componentName != null);
    _componentName = componentName;
  }
  
  
  public String getComponentName()
  {
    Assert.expect(_componentName != null);
    
    return _componentName;
  }
  
  void setPadName(String padName)
  {
    Assert.expect(padName != null);
    _padName = padName;
  }
  
  public String getPadName()
  {
    Assert.expect(_padName != null);
    return _padName;
  }
  /**
   * @author Bill Darbie
   */
  public float getValue()
  {
    return _value;
  }

  /**
   * @author Bill Darbie
   */
  void setMeasurementUnitEnum(MeasurementUnitsEnum measurementUnitsEnum)
  {
    Assert.expect(measurementUnitsEnum != null);
    _measurementUnitsEnum = measurementUnitsEnum;
  }

  /**
   * @author Bill Darbie
   */
  public MeasurementUnitsEnum getMeasurementUnitsEnum()
  {
    Assert.expect(_measurementUnitsEnum != null);
    return _measurementUnitsEnum;
  }
}
