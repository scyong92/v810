package com.axi.v810.datastore.testResults;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class Test_InspectionResultsWriter extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_InspectionResultsWriter());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {

    String projName = "families_all_rlv";
    String resultsDir = Directory.getResultsDir(projName);
    try
    {
      if (FileUtilAxi.exists(resultsDir))
        FileUtilAxi.deleteDirectoryContents(resultsDir);

      InspectionResultsWriter inspectionResultsWriter = new InspectionResultsWriter();
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();
      Project project = Project.importProjectFromNdfs(projName, warnings);

      // test the writer
      inspectionResultsWriter.open(project, 0, "panelSerialNumber", new HashMap<String, String>(), "", "", false);
      inspectionResultsWriter.close(10);

      List<Long> times = InspectionResultsReader.getInspectionStartTimesInMillis(projName);
      for (Long time : times)
        System.out.println(time);

      // test the reader
      InspectionResultsReader inspectionResultsReader = new InspectionResultsReader();
      inspectionResultsReader.open(projName);
      List<JointName> jointNames = inspectionResultsReader.getJointNames();
      for (JointName jointName : jointNames)
      {
        System.out.println(jointName);
        JointResults jointResults = inspectionResultsReader.getJointResult(jointName);
      }
      List<ComponentName> compNames = inspectionResultsReader.getComponentNames();
      for (ComponentName compName : compNames)
      {
        System.out.println(compName);
        ComponentResults compResults = inspectionResultsReader.getComponentResult(compName);
      }
      inspectionResultsReader.close();

//      if (FileUtilAxi.exists(resultsDir))
//        FileUtilAxi.deleteDirectoryContents(resultsDir);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
