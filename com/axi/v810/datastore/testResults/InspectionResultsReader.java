package com.axi.v810.datastore.testResults;

import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class reads in all results information into a binary file.  The format
 * is the following:
 *
 * Header section
 *   Header Record
 *     header record version (int)
 *     software version used to create this file (double)
 *     imageSetName (String)
 *     were images collected online (boolean)
 *     version of the test program (int)
 *     inspection start time (long)
 *     inspection stop time (long)
 *
 *   numPadsAndComponents (in order that they appear later in the file)
 *     boardName refDes padName (string) OR  boardName refDes (string)
 *     jointOrComponent (boolean)  joint = true, component = false
 *     pass (boolean)
 *     uniqueId (int)
 *     number of bytes into file starting at the data section to get to that record (long)
 *
 *   numSubtypes (int)
 *     subtype name (string)
 *     numMeasurements (int)
 *       measurementEnumId (int)
 *         sliceEnumId (int)
 *
 * Data section
 *   Panel Record
 *     record version number (int)
 *     panel serial number (string)
 *
 *   Board Record
 *     record version number (int)
 *     numBoards (int)
 *     board name (string)
 *       xed out (boolean)
 *       serial number (string)
 *
 *   Component Record
 *     record version number (int)
 *     uniqueId (int)
 *     pass or fail (boolean)
 *     topSide (boolean)
 *     numMeasurements (int)
 *     measurement name (int)
 *       algorithm name (int)
 *       subtype short name (string)
 *       subtype long name (string)
 *       slice name (int )
 *       value (float)
 *       units (int)
 *     numIndictments (int)
 *     indictment name (int)
 *     algorithm name (int)
 *       numFailingMeasurements (int)
 *       list of failing measurement index
 *       numRelatedMeasurements (int)
 *       list of related measurement index
 *
 *   Joint Record
 *     record version number (int)
 *     uniqueId (int)
 *     pass or fail (boolean)
 *     topSide (boolean)
 *     subtype short name (string)
 *     subtype long name (string)
 *     joint type (int)
 *     numMeasurements (int)
 *     measurement name (int)
 *       algorithm name (int)
 *       slice name (int)
 *       value (float)
 *       units (int)
 *     numIndictments (int)
 *     indictment name (int)
 *     algorithm name (int)
 *       numFailingMeasurements (int)
 *         failing measurement index (int)
 *       numRelatedMeasurements (int)
 *         related measurement index (int)
 *
 * @author Bill Darbie
 */
public class InspectionResultsReader
{
  private MappedByteBuffer _mmByteBuffer;

  private FileChannel _fileChannel;
  private String _fileName;
  private Map<String, Pair<Integer, Long>> _jointNameToIdAndFilePositionMap = new HashMap<String, Pair<Integer, Long>>();
  private Map<String, Pair<Integer, Long>> _componentNameToIdAndFilePositionMap = new HashMap<String, Pair<Integer, Long>>();
  private Set<String> _failedPadNameSet = new HashSet<String>();
  private Set<String> _failedCompNameSet = new HashSet<String>();
  private long _headerSizeInBytes;
  private EnumToUniqueIDLookup _enumToIdLookup;

  private static int numBytesForInteger = Integer.SIZE / 8;
  private static int numBytesForLong = Long.SIZE / 8;
  private static int numBytesForFloat = Float.SIZE / 8;
  private static int numBytesForDouble = Double.SIZE / 8;
  private static int numBytesForByte = Byte.SIZE / 8;

  private String _softwareVersion;
  private int _testProgramVersion = -1;
  private long _inspectionStartTimeInMillis = -1;
  private long _inspectionStopTimeInMillis = -1;
  private String _panelSerialNumber;
  private Map<Long, String> _positionToNameMap;
  private String _imageSetName;
  private String _imageSetUserDescription;
  private boolean _imagesCollectedOnline;

  private List<JointName> _jointNames = new ArrayList<JointName>();
  private List<ComponentName> _componentNames = new ArrayList<ComponentName>();
  private List<JointName> _failingJointNames = new ArrayList<JointName>();
  private List<ComponentName> _failingCompNames = new ArrayList<ComponentName>();

  private Map<String, BoardResults> _boardNameToBoardResultsMap = new HashMap<String, BoardResults>();

  private List<MeasurementResult> _componentMeasurementResults = new ArrayList<MeasurementResult>();

  private Map<String, List<Pair<MeasurementEnum, SliceNameEnum>>> _subtypeToMeasurementEnumAndSliceNameEnumPairListMap =
      new HashMap<String,List<Pair<MeasurementEnum, SliceNameEnum>>>();

  /**
   * @author Bill Darbie
   */
  public InspectionResultsReader()
  {
    _enumToIdLookup = EnumToUniqueIDLookup.getInstance();
  }

  /**
   * @return the List of inspection start times stored on disk.
   * @author Bill Darbie
   */
  public static List<Long> getInspectionStartTimesInMillis(Project project)
  {
    Assert.expect(project != null);
    return getInspectionStartTimesInMillis(project.getName());
  }

  /**
   * @return the List of inspection start times stored on disk.  The List will be ordered from
   * must recent inspection run to the oldest.
   * @author Bill Darbie
   */
  public static List<Long> getInspectionStartTimesInMillis(String projName)
  {
    Assert.expect(projName != null);

    return Directory.getInspectionResultsStartTimesInMillis(projName);
  }

  /**
   * Open the last run results file.
   * @author Bill Darbie
   */
  public void open(Project project) throws DatastoreException
  {
    Assert.expect(project != null);
    open(project.getName());
  }

  /**
   * Open the last run results file.
   * @author Bill Darbie
   */
  public void open(String projName) throws DatastoreException
  {
    Assert.expect(projName != null);
    List<Long> times = getInspectionStartTimesInMillis(projName);
    Assert.expect(times.isEmpty() == false);
    open(projName, times.get(0));
  }

  /**
   * Open the results file on disk that cooresponds to the start time passed in.
   * @author Bill Darbie
   */
  public void open(Project project, long startTimeInMillis) throws DatastoreException
  {
    Assert.expect(project != null);
    open(project.getName(), startTimeInMillis);
  }

  /**
   * Open the results file on disk that cooresponds to the start time passed in.
   * @author Bill Darbie
   */
  public void open(String projName, long startTimeInMillis) throws DatastoreException
  {
    try
    {
      init();

      _fileName = FileName.getResultsFileFullPath(projName, startTimeInMillis);
      RandomAccessFile raf = new RandomAccessFile(_fileName, "r");
      _fileChannel = raf.getChannel();
      _mmByteBuffer = _fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, _fileChannel.size());
      //loadByteBuffer();
    }
    catch (FileNotFoundException fnfe)
    {
      FileNotFoundDatastoreException de = new FileNotFoundDatastoreException(_fileName);
      de.initCause(fnfe);
      throw de;
    }
    catch (IOException ioe)
    {
      CannotOpenFileDatastoreException de = new CannotOpenFileDatastoreException(_fileName);
      de.initCause(ioe);
      throw de;
    }

    readHeader();
    readBoards();

    buildComponentToMeasurementsSetMapAndMeasurementToSliceNameMap();

//    if (UnitTest.unitTesting() == false)
//      System.out.println("wpd total time to read entire file: " + timer.getElapsedTimeInMillis());
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    _jointNameToIdAndFilePositionMap.clear();
    _componentNameToIdAndFilePositionMap.clear();
    _componentMeasurementResults.clear();
    _subtypeToMeasurementEnumAndSliceNameEnumPairListMap.clear();
    _failedPadNameSet.clear();
    _failedCompNameSet.clear();
    _jointNames.clear();
    _componentNames.clear();
    _failingJointNames.clear();
    _failingCompNames.clear();
    _boardNameToBoardResultsMap.clear();
    _positionToNameMap = null;
  }

  /**
   * @author Bill Darbie
   */
  private void buildNameLists()
  {
    if (_positionToNameMap == null)
    {
      _positionToNameMap = new TreeMap<Long, String>();
      for (Map.Entry<String, Pair<Integer, Long>> entry : _jointNameToIdAndFilePositionMap.entrySet())
      {
        String name = entry.getKey();
        Pair<Integer, Long> pair = entry.getValue();
        Object prev = _positionToNameMap.put(pair.getSecond(), name);
        Assert.expect(prev == null);
      }

      for (Map.Entry<String, Pair<Integer, Long>> entry : _componentNameToIdAndFilePositionMap.entrySet())
      {
        String name = entry.getKey();
        Pair<Integer, Long> pair = entry.getValue();
        Object prev = _positionToNameMap.put(pair.getSecond(), name);
        Assert.expect(prev == null);
      }

      for (String name : _positionToNameMap.values())
      {
        int firstSpace = name.indexOf(" ");
        int secondSpace = name.indexOf(" ", firstSpace + 1);

        if (secondSpace == -1)
        {
          // we have a component name
          String boardName = name.substring(0, firstSpace);
          String refDes = name.substring(firstSpace + 1, name.length());
          ComponentName componentName = new ComponentName(boardName, refDes);
          _componentNames.add(componentName);
          if (_failedCompNameSet.contains(name))
            _failingCompNames.add(componentName);
        }
        else
        {
          // we have a joint name
          String boardName = name.substring(0, firstSpace);
          String refDes = name.substring(firstSpace + 1, secondSpace);
          String padName = name.substring(secondSpace + 1, name.length());
          JointName jointName = new JointName(boardName, refDes, padName);
          _jointNames.add(jointName);
          if (_failedPadNameSet.contains(name))
            _failingJointNames.add(jointName);
        }
      }
      _failedPadNameSet.clear();
      _failedCompNameSet.clear();
    }
  }

  /**
   * @return a List of names of Joint records stored in the file.  Reading the records out in the same
   * order as this list will give you the fastest results.
   * @author Bill Darbie
   */
  public List<JointName> getJointNames()
  {
    buildNameLists();
    return _jointNames;
  }

  /**
   * @return a List of names of failing Joint records stored in the file. Reading the records out in the same
   * order as this list will give you the fastest results.
   * @author Bill Darbie
   */
  public List<JointName> getFailingJointNames()
  {
    buildNameLists();
    return _failingJointNames;
  }

  /**
   * @return a List of names of Component records stored in the file. Reading the records out in the same
   * order as this list will give you the fastest results.
   * @author Bill Darbie
   */
  public List<ComponentName> getComponentNames()
  {
    buildNameLists();
    return _componentNames;
  }

  /**
   * @return a List of names of failing Component records stored in the file. Reading the records out in the same
   * order as this list will give you the fastest results.
   * @author Bill Darbie
   */
  public List<ComponentName> getFailingComponentNames()
  {
    buildNameLists();
    return _failingCompNames;
  }

  /**
   * @author Bill Darbie
   */
  private void readHeader() throws DatastoreException
  {
    int recordVersion = readInt();
    if (recordVersion != 2)
      throw new FileCorruptDatastoreException(_fileName);

    _softwareVersion = String.valueOf(readDouble());
    _imageSetName = readString();
    _imageSetUserDescription = readString();
    _imagesCollectedOnline = readBoolean();
    _testProgramVersion = readInt();
    _inspectionStartTimeInMillis = readLong();
    _inspectionStopTimeInMillis = readLong();
    _panelSerialNumber = readString();

    int numPadsAndComponents = readInt();
    for (int i = 0; i < numPadsAndComponents; ++i)
    {
      String name = readString();
      boolean joint = readBoolean();
      boolean passed = readBoolean();
      int id = readInt();
      long offset = readLong();

      if (joint)
      {
        Object prev = _jointNameToIdAndFilePositionMap.put(name, new Pair<Integer, Long>(id, offset));
        Assert.expect(prev == null);
        if (passed == false)
        {
          boolean added = _failedPadNameSet.add(name);
          Assert.expect(added);
        }
      }
      else
      {
        Object prev = _componentNameToIdAndFilePositionMap.put(name, new Pair<Integer, Long>(id, offset));
        Assert.expect(prev == null);
        if (passed == false)
        {
          boolean added = _failedCompNameSet.add(name);
          Assert.expect(added);
        }
      }
    }

    int numSubtypeRecords = readInt();
    for (int i = 1; i <= numSubtypeRecords; i++)
    {
      String subtypeName = readString();
      int numMeasAndSlicePairs = readInt();
      List<Pair<MeasurementEnum, SliceNameEnum>> measAndSlicePairList = new ArrayList<Pair<MeasurementEnum,SliceNameEnum>>();
      for (int j = 1; j <= numMeasAndSlicePairs; j++)
      {
        int measEnumId = readInt();
        int sliceEnumId = readInt();
        measAndSlicePairList.add(new Pair<MeasurementEnum,SliceNameEnum>(_enumToIdLookup.getMeasurementEnum(measEnumId),
                                                                         _enumToIdLookup.getSliceNameEnum(sliceEnumId)));
      }
      _subtypeToMeasurementEnumAndSliceNameEnumPairListMap.put(subtypeName, measAndSlicePairList);
    }

    _headerSizeInBytes = _mmByteBuffer.position();
  }

  /**
   * @author Bill Darbie
   */
  public String getSoftwareVersion()
  {
    //XCR-3847, Unable to view inspection result under fine tuning measurement/ result tab
    Assert.expect(_softwareVersion != null);
    return _softwareVersion;
  }

  /**
   * @author Bill Darbie
   */
  public int getTestProgramVersion()
  {
    Assert.expect(_testProgramVersion != -1);
    return _testProgramVersion;
  }

  /**
   * @author Bill Darbie
   */
  private void readBoards() throws DatastoreException
  {
    int boardRecordVersion = readInt();
    if (boardRecordVersion != 1)
      throw new FileCorruptDatastoreException(_fileName);

    int numBoards = readInt();
    for (int i = 0; i < numBoards; ++i)
    {
      BoardResults boardResults = new BoardResults();

      String boardName = readString();
      String boardSerialNumber = readString();

      boardResults.setBoardName(boardName);
      boardResults.setSerialNumber("");
      // for 1.0 release, no xout support so hard code false
      boardResults.setXout(false);
      boardResults.setSerialNumber(boardSerialNumber);
      Object prev = _boardNameToBoardResultsMap.put(boardName, boardResults);
      Assert.expect(prev == null);
    }
  }

  /**
   * @author Bill Darbie
   */
  public PanelResults getPanelResults() throws DatastoreException
  {
    PanelResults panelResults = new PanelResults();
    panelResults.setSerialNumberOrDescription(_panelSerialNumber);
    panelResults.setInspectionStartTimeInMillis(_inspectionStartTimeInMillis);
    panelResults.setInspectionStopTimeInMillis(_inspectionStopTimeInMillis);
    panelResults.setImageSetName(_imageSetName);
    panelResults.setUserDescription(_imageSetUserDescription);
    panelResults.setImagesCollectedOnline(_imagesCollectedOnline);

//    ImageSetInfoReader imageSetInfoReader = new ImageSetInfoReader();
//    String imageSetFilePath = null;
//    if (_imagesCollectedOnline)
//    {
//      imageSetFilePath = FileName.getImageSetInfoFullPathForOnlineTestDevelopment(_imageSetName);
//    }
//    else
//    {
//      imageSetFilePath = FileName.getImageSetInfoFullPath(projectName, _imageSetName);
//    }
//    imageSetInfoReader.parseFile(imageSetFilePath);
//    panelResults.setUserDescription(imageSetInfoReader.getImageSetData().getUserDescription());
    return panelResults;
  }

  /**
   * @author Bill Darbie
   */
  public Map<String, BoardResults> getBoardResults()
  {
    return _boardNameToBoardResultsMap;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isJointResultAvailable(Pad pad)
  {
    String jointName = getName(pad);
    return _componentNameToIdAndFilePositionMap.containsKey(jointName);
  }

  /**
   * @author Bill Darbie
   */
  public JointResults getJointResult(Pad pad) throws DatastoreException
  {
    Assert.expect(pad != null);

    JointName jointName = new JointName(pad.getComponent().getBoard().getName(), pad.getComponent().getReferenceDesignator(), pad.getName());
    return getJointResult(jointName);
  }

  /**
   * @author Bill Darbie
   */
  public JointResults getJointResult(JointName jointName) throws DatastoreException
  {
    Assert.expect(jointName != null);
    String boardAndComponentAndPadName = jointName.getBoardName() + " " + jointName.getReferenceDesignator() + " " + jointName.getPadName();

    Pair<Integer, Long> pair = _jointNameToIdAndFilePositionMap.get(boardAndComponentAndPadName);
    if(pair == null)
    {
      System.err.println("boardAndComponentAndPadName : " + boardAndComponentAndPadName + " Not found!");
      throw new FileCorruptDatastoreException(_fileName);
    }
//    Assert.expect(pair != null);

    int id = pair.getFirst();
    long offset = pair.getSecond();

    setPosition(offset);
    int recordVersionNumber = readInt();
    if (recordVersionNumber != 1)
      throw new FileCorruptDatastoreException(_fileName);

    int uniqueId = readInt();
    if (uniqueId != id)
      throw new FileCorruptDatastoreException(_fileName);

    JointResults jointResults = new JointResults();
    jointResults.setJointName(jointName);

    boolean pass = readBoolean();
    jointResults.setPassed(pass);
    boolean isTopSide = readBoolean();
    jointResults.setTopSide(isTopSide);

    String subtypeShortName = readString();
    jointResults.setSubtypeShortName(subtypeShortName);
    String subtypeLongName = readString();
    jointResults.setSubtypeLongName(subtypeLongName);

    JointTypeEnum jointTypeEnum = _enumToIdLookup.getJointTypeEnum(readInt());
    jointResults.setJointTypeEnum(jointTypeEnum);

    List<MeasurementResult> measurementResults = new ArrayList<MeasurementResult>();
    int numMeasurements = readInt();
    for (int i = 0; i < numMeasurements; ++i)
    {
      MeasurementEnum measurementEnum = _enumToIdLookup.getMeasurementEnum(readInt());
      AlgorithmEnum algEnum = _enumToIdLookup.getAlgorithmEnum(readInt());
      SliceNameEnum sliceNameEnum = _enumToIdLookup.getSliceNameEnum(readInt());
      float value = readFloat();
      MeasurementUnitsEnum measurementUnitsEnum = _enumToIdLookup.getMeasurementUnitsEnum(readInt());

      MeasurementResult measurementResult = new MeasurementResult();
      measurementResult.setMeasurementEnum(measurementEnum);
      measurementResult.setAlgorithmEnum(algEnum);
      measurementResult.setSliceNameEnum(sliceNameEnum);
      measurementResult.setValue(value);
      measurementResult.setMeasurementUnitEnum(measurementUnitsEnum);
      measurementResult.setSubtypeShortName(subtypeShortName);
      measurementResult.setSubtypeLongName(subtypeLongName);
      measurementResult.setPadName(jointName.getPadName());
      measurementResult.setComponentName(jointName.getReferenceDesignator());

      measurementResults.add(measurementResult);
      jointResults.addMeasurementResult(measurementResult);
    }

    int numIndictments = readInt();
    for (int i = 0; i < numIndictments; ++i)
    {
      IndictmentEnum indictmentEnum = _enumToIdLookup.getIndictmentEnum(readInt());
      AlgorithmEnum algoEnum = _enumToIdLookup.getAlgorithmEnum(readInt());

      IndictmentResult indictmentResult = new IndictmentResult();
      indictmentResult.setIndictmentEnum(indictmentEnum);
      indictmentResult.setAlgorithmEnum(algoEnum);

      int numFailingMeasurements = readInt();
      for (int j = 0; j < numFailingMeasurements; ++j)
      {
        int failingMeasurementIndex = readInt();
        indictmentResult.addFailingMeasurement(measurementResults.get(failingMeasurementIndex));
      }

      int numRelatedMeasurements = readInt();
      for (int j = 0; j < numRelatedMeasurements; ++j)
      {
        int relatedMeasurementIndex = readInt();
        indictmentResult.addRelatedMeasurement(measurementResults.get(relatedMeasurementIndex));
      }

      jointResults.addIndictmentResult(indictmentResult);
    }

    return jointResults;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isComponentResultAvailable(Component component)
  {
    String compName = getName(component);
    return _componentNameToIdAndFilePositionMap.containsKey(compName);
  }

  /**
   * @author Bill Darbie
   */
  public ComponentResults getComponentResult(Component component) throws DatastoreException
  {
    Assert.expect(component != null);

    ComponentName compName = new ComponentName(component.getBoard().getName(), component.getReferenceDesignator());
    return getComponentResult(compName);
  }

  /**
   * @author Bill Darbie
   */
  private String getName(Component component)
  {
    Assert.expect(component != null);
    ComponentName compName = new ComponentName(component.getBoard().getName(), component.getReferenceDesignator());
    return getName(compName);
  }

  /**
   * @author Bill Darbie
   */
  private String getName(JointName jointName)
  {
    String boardAndReferenceDesignatorAndPadName = jointName.getBoardName() + " " + jointName.getReferenceDesignator() + " " + jointName.getPadName();
    return boardAndReferenceDesignatorAndPadName;
  }

  /**
   * @author Bill Darbie
   */
  private String getName(Pad pad)
  {
    Assert.expect(pad != null);
    JointName jointName = new JointName(pad.getComponent().getBoard().getName(),
                                        pad.getComponent().getReferenceDesignator(),
                                        pad.getName());
    return getName(jointName);
  }

  /**
   * @author Bill Darbie
   */
  private String getName(ComponentName componentName)
  {
    String boardAndReferenceDesignator = componentName.getBoardName() + " " + componentName.getReferenceDesignator();
    return boardAndReferenceDesignator;
  }

  /**
   * @author Bill Darbie
   */
  public ComponentResults getComponentResult(ComponentName componentName) throws DatastoreException
  {
    Assert.expect(componentName != null);
    String boardAndReferenceDesignator = getName(componentName);

    Pair<Integer, Long> pair = _componentNameToIdAndFilePositionMap.get(boardAndReferenceDesignator);
    if (pair == null)
      throw new FileCorruptDatastoreException(_fileName);

    int id = pair.getFirst();
    long offset = pair.getSecond();

    setPosition(offset);
    int recordVersionNumber = readInt();
    if (recordVersionNumber != 1)
      throw new FileCorruptDatastoreException(_fileName);
    int uniqueId = readInt();
    if (uniqueId != id)
      throw new FileCorruptDatastoreException(_fileName);

    ComponentResults compResults = new ComponentResults();
    compResults.setComponentName(componentName);

    boolean pass = readBoolean();
    compResults.setPassed(pass);
    boolean isTopSide = readBoolean();
    compResults.setTopSide(isTopSide);

    List<MeasurementResult> measurementResults = new ArrayList<MeasurementResult>();
    int numMeasurements = readInt();
    for (int i = 0; i < numMeasurements; ++i)
    {
      MeasurementEnum measurementEnum = _enumToIdLookup.getMeasurementEnum(readInt());
      AlgorithmEnum algoEnum = _enumToIdLookup.getAlgorithmEnum(readInt());
      String subtypeShortName = readString();
      String subtypeLongName = readString();
      SliceNameEnum sliceNameEnum = _enumToIdLookup.getSliceNameEnum(readInt());
      float value = readFloat();
      MeasurementUnitsEnum measurementUnitsEnum = _enumToIdLookup.getMeasurementUnitsEnum(readInt());

      MeasurementResult measurementResult = new MeasurementResult();
      measurementResult.setMeasurementEnum(measurementEnum);
      measurementResult.setAlgorithmEnum(algoEnum);
      measurementResult.setSliceNameEnum(sliceNameEnum);
      measurementResult.setValue(value);
      measurementResult.setMeasurementUnitEnum(measurementUnitsEnum);
      measurementResult.setSubtypeShortName(subtypeShortName);
      measurementResult.setSubtypeLongName(subtypeLongName);
      measurementResult.setComponentName(componentName.getReferenceDesignator());
      measurementResult.setPadName(StringLocalizer.keyToString("RMGUI_COMPONENT_LEVEL_KEY"));

      measurementResults.add(measurementResult);
      compResults.addMeasurementResult(measurementResult);
    }

    int numIndictments = readInt();
    for (int i = 0; i < numIndictments; ++i)
    {
      IndictmentEnum indictmentEnum = _enumToIdLookup.getIndictmentEnum(readInt());
      AlgorithmEnum algoEnum = _enumToIdLookup.getAlgorithmEnum(readInt());

      IndictmentResult indictmentResult = new IndictmentResult();
      indictmentResult.setIndictmentEnum(indictmentEnum);
      indictmentResult.setAlgorithmEnum(algoEnum);

      int numFailingMeasurements = readInt();
      for (int j = 0; j < numFailingMeasurements; ++j)
      {
        int failingMeasurementIndex = readInt();
        indictmentResult.addFailingMeasurement(measurementResults.get(failingMeasurementIndex));
      }

      int numRelatedMeasurements = readInt();
      for (int j = 0; j < numRelatedMeasurements; ++j)
      {
        int relatedMeasurementIndex = readInt();
        indictmentResult.addRelatedMeasurement(measurementResults.get(relatedMeasurementIndex));
      }
      compResults.addIndictmentResult(indictmentResult);
    }

    return compResults;
  }

  /**
   * @author Bill Darbie
   */
  public void close()
  {
    if (_fileChannel == null)
      return;

    try
    {
      init();
      _mmByteBuffer.force();
      _fileChannel.close();
      try
      {
        // Ying-Huan.Chu
        unmap(_fileChannel, _mmByteBuffer);
      }
      catch (Exception ex)
      {
        System.out.println("ERROR: MappedByteBuffer Unmap() failed.");
      }
      _fileChannel = null;
      // the only way to release the memory mapped file is to null out the _byteBuffer so do no forget to do this!
      _mmByteBuffer.clear();
      _mmByteBuffer = null;
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void setPosition(long filePositionAfterHeader) throws DatastoreException
  {
    long position = filePositionAfterHeader + _headerSizeInBytes;

    Assert.expect(position <= Integer.MAX_VALUE);
    _mmByteBuffer.position((int)position);
  }

  /**
   * @author Bill Darbie
   */
  private int readInt() throws DatastoreException
  {
    if (_mmByteBuffer.remaining() < numBytesForInteger)
      throw new FileCorruptDatastoreException(_fileName);
    return _mmByteBuffer.getInt();
  }

  /**
   * @author Bill Darbie
   */
  private double readDouble() throws DatastoreException
  {
    if (_mmByteBuffer.remaining() < numBytesForDouble)
      throw new FileCorruptDatastoreException(_fileName);
    return _mmByteBuffer.getDouble();
  }

  /**
   * @author Bill Darbie
   */
  private float readFloat() throws DatastoreException
  {
    if (_mmByteBuffer.remaining() < numBytesForFloat)
      throw new FileCorruptDatastoreException(_fileName);
    return _mmByteBuffer.getFloat();
  }

  /**
   * @author Bill Darbie
   */
  private long readLong() throws DatastoreException
  {
    if (_mmByteBuffer.remaining() < numBytesForLong)
      throw new FileCorruptDatastoreException(_fileName);
    return _mmByteBuffer.getLong();
  }

  /**
   * @author Bill Darbie
   */
  private boolean readBoolean() throws DatastoreException
  {
    if (_mmByteBuffer.remaining() < numBytesForByte)
      throw new FileCorruptDatastoreException(_fileName);
    byte bite = _mmByteBuffer.get();
    if (bite == 0)
      return false;
    return true;
  }

  /**
   * @author Bill Darbie
   */
  private String readString() throws DatastoreException
  {
    int numBytes = readInt();
    byte[] bytes = new byte[numBytes];
    if (_mmByteBuffer.remaining() < numBytes)
      throw new FileCorruptDatastoreException(_fileName);
    _mmByteBuffer.get(bytes);
    return new String(bytes);
  }

  /**
   * @author Laura Cormos
   */
  private void buildComponentToMeasurementsSetMapAndMeasurementToSliceNameMap() throws DatastoreException
  {
    // for every component saved in the results file do the following
    for (ComponentName componentName : getComponentNames())
    {
      // construct the componentResult object
      ComponentResults componentResults = getComponentResult(componentName);
      // get a list of all measurements used for this joint
      List<MeasurementResult> allMeasurements = componentResults.getAllMeasurementResults();
      // construct a list of all measurement results for all componenets for later querries
      _componentMeasurementResults.addAll(allMeasurements);
//      // make a set of measurement enums for all measurement results in this component
//      Set<MeasurementEnum> allMeasurementEnums = new HashSet<MeasurementEnum>();
//      for (MeasurementResult measurementResult : allMeasurements)
//      {
//        allMeasurementEnums.add(measurementResult.getMeasurementEnum());
//      }
//      // store all measurement enums found so far for the same subtype
//      _componentNameToMeasurementsSetMap.put(componentName.getReferenceDesignator(), allMeasurementEnums);
    }
  }

  /**
   * @author Laura Cormos
   */
  public Set<MeasurementEnum> getMeasurementsForSubtype(Subtype subtype)
  {
    Assert.expect(subtype != null);

    if (_subtypeToMeasurementEnumAndSliceNameEnumPairListMap.containsKey(subtype.getLongName()) == false)
      return new HashSet<MeasurementEnum>();

    Set<MeasurementEnum> allMeasurementEnums = new HashSet<MeasurementEnum>();
    List <Pair<MeasurementEnum, SliceNameEnum>> pairList = _subtypeToMeasurementEnumAndSliceNameEnumPairListMap.get(subtype.getLongName());
    if (pairList != null)
    {
      for (Pair<MeasurementEnum, SliceNameEnum> pair : pairList)
      {
        allMeasurementEnums.add(pair.getFirst());
      }
    }
    return allMeasurementEnums;
  }

  /**
   * @author Laura Cormos
   */
  public Set<SliceNameEnum> getSlicesForSubtype(Subtype subtype)
  {
    Assert.expect(subtype != null);

    if (_subtypeToMeasurementEnumAndSliceNameEnumPairListMap.containsKey(subtype.getLongName()) == false)
      return new HashSet<SliceNameEnum>();

    Set<SliceNameEnum> allSliceNameEnums = new HashSet<SliceNameEnum>();
    for (Pair<MeasurementEnum, SliceNameEnum> pair : _subtypeToMeasurementEnumAndSliceNameEnumPairListMap.get(subtype.getLongName()))
    {
      allSliceNameEnums.add(pair.getSecond());
    }
    return allSliceNameEnums;
  }

  /**
   * @author Laura Cormos
   */
  public Set<SliceNameEnum> getSliceNameEnumForSubtypeAndMeasurementEnum(Subtype subtype, MeasurementEnum measurementEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(measurementEnum != null);

    Set<SliceNameEnum> sliceNameEnums = new HashSet<SliceNameEnum>();
    List <Pair<MeasurementEnum, SliceNameEnum>> pairList = _subtypeToMeasurementEnumAndSliceNameEnumPairListMap.get(subtype.getLongName());
    if (pairList != null)
    {
      for (Pair<MeasurementEnum, SliceNameEnum> pair : pairList)
      {
        MeasurementEnum measEnum = pair.getFirst();
        if (measEnum.equals(measurementEnum))
          sliceNameEnums.add(pair.getSecond());
      }
    }

    return sliceNameEnums;
  }

  /**
   * @author Laura Cormos
   */
  public Set<MeasurementEnum> getMeasurementEnumForSubtypeAndSliceNameEnum(Subtype subtype, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    Set<MeasurementEnum> measurementEnums = new HashSet<MeasurementEnum>();
    List <Pair<MeasurementEnum, SliceNameEnum>> pairList = _subtypeToMeasurementEnumAndSliceNameEnumPairListMap.get(subtype.getLongName());
    if (pairList != null)
    {
      for (Pair<MeasurementEnum, SliceNameEnum> pair : pairList)
      {
        SliceNameEnum sliceNameEnumToTest = pair.getSecond();
        if (sliceNameEnumToTest.equals(sliceNameEnum))
          measurementEnums.add(pair.getFirst());
      }
    }

    return measurementEnums;
  }

  /**
    * @author Laura Cormos
    *
    * remove - not needed anymore
    */
   public Set<SliceNameEnum> getSliceNameEnumForComponentAndMeasurementEnum(Component component, MeasurementEnum measurementEnum)
   {
     Assert.expect(component != null);
     Assert.expect(measurementEnum != null);

     Set<SliceNameEnum> sliceNameEnums = new HashSet<SliceNameEnum>();
     for (MeasurementResult measurementResult : _componentMeasurementResults)
     {
       if (measurementResult.getMeasurementEnum().equals(measurementEnum))
         sliceNameEnums.add(measurementResult.getSliceNameEnum());
     }

     return sliceNameEnums;
    }
  
  /**
   * @author Ying-Huan.Chu // XCR1604 Fix
   * @description To unmap the MappedByteBuffer so that the Inspection Results can be released for deletion.
   */
  private static void unmap(FileChannel fc, MappedByteBuffer bb) throws Exception 
  {
    Class<?> fcClass = fc.getClass();
    java.lang.reflect.Method unmapMethod = fcClass.getDeclaredMethod("unmap",new Class[]{java.nio.MappedByteBuffer.class});
    unmapMethod.setAccessible(true);
    unmapMethod.invoke(null,new Object[]{bb});
  }
   
  /**
   * @author Jack Hwee
   */
  public String getImageSetUserDescription() 
  {
    Assert.expect(_imageSetUserDescription != null);
    return _imageSetUserDescription;
  }
}
