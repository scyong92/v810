package com.axi.v810.datastore.testResults;

import java.io.*;
import java.util.*;
import java.util.zip.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
public class ProjectSettingsXMLWriter
{
  private static ProjectSettingsXMLWriter _instance;
  private boolean _printSpaces = true;
  private boolean _exceptionThrown = false;
  private Adler32 _checksumUtil = new Adler32();
  private Project _project;
  private PrintWriter _os;
  private String _projName;
  private int _numOfSpaces = XMLWriterStringUtil.NUM_INDENT_SPACES;
  private String _beginCommentTag = XMLWriterStringUtil.BEGIN_COMMENT_TAG;
  private String _endCommentTag = XMLWriterStringUtil.END_COMMENT_TAG;
  private String _greaterThanTag = XMLWriterStringUtil.GREATER_THEN_TAG;
  private String _dbQuote = XMLWriterStringUtil.ESCAPED_DOUBLE_QUOTE;
  private String _elemEndTag = XMLWriterStringUtil.ELEMENT_END_TAG;
  private String _dbQuoteGtTag = _dbQuote + _greaterThanTag;
  private String _dbQtElemEndTag = _dbQuote + _elemEndTag;
  private EnumStringLookup _enumStringLookup;
  private Subtype _currentSubtype;

  /**
   * @author Laura Cormos
   */
  public static synchronized ProjectSettingsXMLWriter getInstance()
  {
    if (_instance == null)
      _instance = new ProjectSettingsXMLWriter();

    return _instance;
  }

  /**
   * @author Laura Cormos
   */
  public ProjectSettingsXMLWriter()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * @author Laura Cormos
   */
  private void printAndUpdateChecksum(String line)
  {
    Assert.expect(line != null);
    _checksumUtil.update(line.getBytes());
    _os.println(line);
  }

  /**
   * @author Bill Darbie
   */
  private void clear()
  {
    _project = null;
    _currentSubtype = null;
    _os = null;
  }

  /**
   * @author Laura Cormos
   */
  public void write(Project project, boolean printSpaces) throws DatastoreException
  {
    Assert.expect(project != null);

    _project = project;
    _printSpaces = printSpaces;
    _projName = project.getName();

    String projDirNameForProj = Directory.getProjectDir(_projName);
    // first write out the file to a temp location in case there are problems and because we won't know the name of the
    // file until it is finished - checksum
    String tempSettingsFileNamePath = projDirNameForProj + File.separator +
                                      FileUtil.getTempFileName(_projName + FileName.getSettingsFileExtension() +
                                      FileName.getXmlFileExtension());
    if (FileUtilAxi.exists(projDirNameForProj) == false)
      FileUtilAxi.mkdir(projDirNameForProj);
    int indent = 0;
    _checksumUtil.reset();

    try
    {
      _os = new PrintWriter(tempSettingsFileNamePath);
      // write the header and any comments
      printAndUpdateChecksum(XMLWriterStringUtil.XML_TAG);
      if (_printSpaces)
        printAndUpdateChecksum("");
      writeTopLevelElement(indent);

      long checksum = _checksumUtil.getValue();
      // get the checksum for the previous version of the cad.xml file, if any, so any previous copies of
      // this file can be deleted.
      long previousChecksum = _project.getSettingsXmlChecksum();
      _project.setSettingsXmlChecksum(checksum);

      // have to close the file before I can attempt to rename it
      if (_os != null)
        _os.close();

      try
      {
        // rename the temp settings.xml into the settings.checksum.xml file
        FileUtil.rename(tempSettingsFileNamePath, FileName.getSettingsFileNameFullPath(_projName, checksum));
      }
      catch (CouldNotDeleteFileException ex)
      {
        _exceptionThrown = true;
        CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(tempSettingsFileNamePath);
        dex.initCause(ex);
        throw dex;
      }
      catch (CouldNotRenameFileException ex)
      {
        _exceptionThrown = true;
        CannotWriteDatastoreException dex = new CannotWriteDatastoreException(FileName.getCadFileNameFullPath(_projName,
            checksum));
        dex.initCause(ex);
        throw dex;
      }             
      catch (FileNotFoundException ex)
      {
        _exceptionThrown = true;
        FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(tempSettingsFileNamePath);
        dex.initCause(ex);
        throw dex; // unable to return a valid value
      }
           
      // if a previous settings.previousChecksum.xml exists, delete it only if is different than the one we just created
      if (checksum != previousChecksum)
      {
        String previousSettingsFile = FileName.getSettingsFileNameFullPath(_projName, previousChecksum);
        if (FileUtil.exists(previousSettingsFile))
          try
          {
            FileUtil.delete(previousSettingsFile);
          }
          catch (CouldNotDeleteFileException ex)
          {
            _exceptionThrown = true;
            CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(previousSettingsFile);
            dex.initCause(ex);
            throw dex;
          }
      }
    }
    catch (FileNotFoundException ex)
    {
      _exceptionThrown = true;
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(tempSettingsFileNamePath);
      dex.initCause(ex);
      throw dex;
    }
    catch (RuntimeException ex)
    {
      _exceptionThrown = true;
      throw ex;
    }
    finally
    {
      // don't hold on to project - allow the garbage collector to get it
      clear();

      // if the temp file still exists, delete it
      if (FileUtil.exists(tempSettingsFileNamePath))
      {
        if (_exceptionThrown)
        {
          if (_os != null)
            _os.close();
        }
        try
        {
          FileUtil.delete(tempSettingsFileNamePath);
        }
        catch (CouldNotDeleteFileException ex)
        {
          if (_exceptionThrown)
          {
            ex.printStackTrace();
          }
          else
          {
            CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(tempSettingsFileNamePath);
            dex.initCause(ex);
            throw dex;
          }
        }
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void writeTopLevelElement(int indent)
  {
    Assert.expect(_os != null);
    Assert.expect(indent >= 0);

    StringBuilder spaces = new StringBuilder();
    printAndUpdateChecksum("<projectSettings");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    printAndUpdateChecksum(spaces + "schemaName=" + _dbQuote + FileName.getSettingsXmlSchemaFileName() + _dbQuoteGtTag);
    indent++;
    writeProjectElement(indent);
    writePanelElement(indent);
    printAndUpdateChecksum("</projectSettings>");
  }

  /**
   * @author Laura Cormos
   */
  private void writeProjectElement(int indent)
  {
    Assert.expect(indent >= 0);
    StringBuilder spaces = new StringBuilder();
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<project");
    String endElemSpaces = spaces.toString();

    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    printAndUpdateChecksum(spaces + "name=" + _dbQuote + _projName + _dbQuote);
    printAndUpdateChecksum(spaces + "version=" + _dbQuote + _project.getVersion() + _dbQuote);
    printAndUpdateChecksum(spaces + "softwareVersionDuringProjectCreation=" + _dbQuote +
                           _project.getSoftwareVersionOfProjectLastSave() + _dbQuote);
    printAndUpdateChecksum(spaces + "softwareVersionDuringFileCreation=" + _dbQuote +
                           _project.getSoftwareVersionOfProjectLastSave() + _dbQuote);
    printAndUpdateChecksum(spaces + "projectDatabaseVersion=" + _dbQuote + _project.getDatabaseVersion() + _dbQuote);
    printAndUpdateChecksum(spaces + "projectNotes=" + _dbQuote + _project.getNotes() + _dbQtElemEndTag);

//    indent++;
//    printAndUpdateChecksum(endElemSpaces + "</project>");
  }

  /**
   * @author Laura Cormos
   */
  private void writePanelElement(int indent)
  {
    Assert.expect(indent >= 0);
    StringBuilder spaces = new StringBuilder();
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<panel");
    String endElemSpaces = spaces.toString();

    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    Panel panel = _project.getPanel();
    printAndUpdateChecksum(spaces + "rotation=" + _dbQuote + panel.getDegreesRotationAfterAllRotations() + _dbQuote);
    if (panel.isFlipped())
      printAndUpdateChecksum(spaces + "flipped=" + _dbQuote + "true" + _dbQuoteGtTag);
    else
      printAndUpdateChecksum(spaces + "flipped=" + _dbQuote + "false" + _dbQuoteGtTag);

    indent++;
    writeAlignmentElement(indent);
    List<BoardType> boardTypes = panel.getBoardTypes();
    for (BoardType boardType :  boardTypes)
      writeBoardTypeElement(boardType, indent);
    List<Subtype> subtypes = panel.getSubtypes();
    for (Subtype subtype : subtypes)
      writeSubtypeElement(subtype, indent);

    printAndUpdateChecksum(endElemSpaces + "</panel>");
  }

  /**
   * @author Laura Cormos
   */
  private void writeAlignmentFiducialElement(Fiducial fiducial, int indent)
  {
    Assert.expect(fiducial != null);
    Assert.expect(indent >= 0);

    StringBuilder spaces = new StringBuilder();
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<alignmentFiducial");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    if (fiducial.isOnBoard())
      printAndUpdateChecksum(spaces + "name=" + _dbQuote + fiducial.getSideBoard().getBoard().getName() +
                             " " + fiducial.getName() + _dbQtElemEndTag);
    else
      printAndUpdateChecksum(spaces + "name=" + _dbQuote + fiducial.getName() + _dbQtElemEndTag);
  }

  /**
   * @author Laura Cormos
   */
  private void writeBoardTypeElement(BoardType boardType, int indent)
  {
    Assert.expect(boardType != null);
    Assert.expect(indent >= 0);

    StringBuilder spaces = new StringBuilder();
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<boardType");
    String endElemSpaces = spaces.toString();

    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    printAndUpdateChecksum(spaces + "name=" + _dbQuote + boardType.getName() + _dbQuoteGtTag);

    ++indent;
    List<Board> boards = boardType.getBoards();
    for (Board board : boards)
      writeBoardElement(board, indent);

    printAndUpdateChecksum(endElemSpaces + "</boardType>");
  }

  /**
   * @author Laura Cormos
   */
  private void writeSubtypeElement(Subtype subtype, int indent)
  {
    Assert.expect(subtype != null);
    Assert.expect(indent >= 0);

    _currentSubtype = subtype;
    StringBuilder spaces = new StringBuilder();
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<subtype");
    String endElemSpaces = spaces.toString();

    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    printAndUpdateChecksum(spaces + "name=" + _dbQuote + subtype.getShortName() + _dbQuote);
    printAndUpdateChecksum(spaces + "userComment=" + _dbQuote + subtype.getUserComment() + _dbQuote);
    printAndUpdateChecksum(spaces + "jointType=" + _dbQuote + _enumStringLookup.getJointTypeString(subtype.getJointTypeEnum())
                           + _dbQuoteGtTag);

    ++indent;
    List<Algorithm> algorithms = subtype.getAlgorithms();
    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    for (Algorithm algorithm : algorithms)
      writeAlgorithmElement(jointTypeEnum, algorithm, indent);

    printAndUpdateChecksum(endElemSpaces + "</subtype>");
  }

  /**
   * @author Laura Cormos
   */
  private void writeAlgorithmElement(JointTypeEnum jointTypeEnum, Algorithm algorithm, int indent)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithm != null);
    Assert.expect(indent >= 0);

    StringBuilder spaces = new StringBuilder();
   if (_printSpaces)
     for (int i = 0; i < (indent * _numOfSpaces); i++)
       spaces.append(" ");
   printAndUpdateChecksum(spaces + "<algorithm");
   String endElemSpaces = spaces.toString();

   // attribute indent
   if (_printSpaces)
     spaces.append("  ");
   printAndUpdateChecksum(spaces + "name=" + _dbQuote + _enumStringLookup.getAlgorithmString(algorithm.getAlgorithmEnum())
                          + _dbQuote);
   printAndUpdateChecksum(spaces + "version=" + _dbQuote + algorithm.getVersion() + _dbQuoteGtTag);

   ++indent;
   List<AlgorithmSetting> algorithmSettings = algorithm.getAlgorithmSettings(jointTypeEnum);
   for (AlgorithmSetting algorithmSetting : algorithmSettings)
     writeAlgorithmSettingElement(algorithmSetting, algorithm.getVersion(), indent);

   printAndUpdateChecksum(endElemSpaces + "</algorithm>");
  }

  /**
   * @author Laura Cormos
   */
  private void writeAlgorithmSettingElement(AlgorithmSetting algorithmSetting, int algorithmVersion, int indent)
  {
    Assert.expect(algorithmSetting != null);
    Assert.expect(indent >= 0);

    StringBuilder spaces = new StringBuilder();
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<algorithmSetting");

    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    AlgorithmSettingEnum settingEnum = algorithmSetting.getAlgorithmSettingEnum();
    printAndUpdateChecksum(spaces + "name=" + _dbQuote + _enumStringLookup.getAlgorithmSettingString(settingEnum) +
                           _dbQuote);
    if (algorithmSetting.hasMaximumValue(algorithmVersion))
      printAndUpdateChecksum(spaces + "highValue=" + _dbQuote + algorithmSetting.getMaximumValue(algorithmVersion) +
                             _dbQuote);
    if (algorithmSetting.hasMinimumValue(algorithmVersion))
      printAndUpdateChecksum(spaces + "lowValue=" + _dbQuote + algorithmSetting.getMinimumValue(algorithmVersion) +
                             _dbQuote);
    printAndUpdateChecksum(spaces + "defaultValue=" + _dbQuote + algorithmSetting.getDefaultValue(algorithmVersion) +
                           _dbQuote);
    printAndUpdateChecksum(spaces + "tunedValue=" + _dbQuote + _currentSubtype.getAlgorithmSettingValue(settingEnum) +
                           _dbQuote);
    printAndUpdateChecksum(spaces + "units=" + _dbQuote +
                           _enumStringLookup.getMeasurementUnitsEnumString(algorithmSetting.getUnits(algorithmVersion)) +
                           _dbQuote);
    printAndUpdateChecksum(spaces + "type=" + _dbQuote +
                           _enumStringLookup.getAlgorithmSettingTypeString(algorithmSetting.getAlgorithmSettingTypeEnum(algorithmVersion)) +
                           _dbQuote);
    printAndUpdateChecksum(spaces + "comment=" + _dbQuote + _currentSubtype.getAlgorithmSettingUserComment(settingEnum) +
                           _dbQtElemEndTag);
  }
  /**
   * @author Laura Cormos
   */
  private void writeBoardElement(Board board, int indent)
  {
    Assert.expect(board != null);
    Assert.expect(indent >= 0);

    StringBuilder spaces = new StringBuilder();
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<board");
    String endElemSpaces = spaces.toString();

    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    printAndUpdateChecksum(spaces + "name=" + _dbQuote + board.getName() + _dbQuote);
    if (board.isInspected())
      printAndUpdateChecksum(spaces + "inspected=" + _dbQuote + "true" + _dbQuoteGtTag);
    else
      printAndUpdateChecksum(spaces + "inspected=" + _dbQuote + "false" + _dbQuoteGtTag);

    indent++;
    List<Component> components = board.getComponents();
    for (Component component : components)
      writeComponentElement(component, indent);

    printAndUpdateChecksum(endElemSpaces + "</board>");
  }

  /**
   * @author Laura Cormos
   */
  private void writeComponentElement(Component component, int indent)
  {
    Assert.expect(component != null);
    Assert.expect(indent >= 0);

    StringBuilder spaces = new StringBuilder();
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<component");
    String endElemSpaces = spaces.toString();

    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    printAndUpdateChecksum(spaces + "referenceDesignator=" + _dbQuote + component.getReferenceDesignator() + _dbQuote);
    if (component.isLoaded())
      printAndUpdateChecksum(spaces + "loaded=" + _dbQuote + "true" + _dbQuote);
    else
      printAndUpdateChecksum(spaces + "loaded=" + _dbQuote + "false" + _dbQuote);
    if (component.isInspected())
      printAndUpdateChecksum(spaces + "inspected=" + _dbQuote + "true" + _dbQuoteGtTag);
    else
      printAndUpdateChecksum(spaces + "inspected=" + _dbQuote + "false" + _dbQuoteGtTag);

    indent++;
    List<Pad> pads = component.getPads();
    for (Pad pad : pads)
      writePadElement(pad, indent);

    printAndUpdateChecksum(endElemSpaces + "</component>");
  }

  /**
  * @author Laura Cormos
  */
 private void writePadElement(Pad pad, int indent)
 {
   Assert.expect(pad != null);
   Assert.expect(indent >= 0);

   StringBuilder spaces = new StringBuilder();
   if (_printSpaces)
     for (int i = 0; i < (indent * _numOfSpaces); i++)
       spaces.append(" ");
   printAndUpdateChecksum(spaces + "<pad");
   // attribute indent
   if (_printSpaces)
     spaces.append("  ");
   printAndUpdateChecksum(spaces + "name=" + _dbQuote + pad.getName() + _dbQuote);
   if (pad.isInspected())
     printAndUpdateChecksum(spaces + "inspected=" + _dbQuote + "true" + _dbQuote);
   else
     printAndUpdateChecksum(spaces + "inspected=" + _dbQuote + "false" + _dbQuote);
   printAndUpdateChecksum(spaces + "jointType=" + _dbQuote + _enumStringLookup.getJointTypeString(pad.getJointTypeEnum()) + _dbQuote);
   printAndUpdateChecksum(spaces + "subtype=" + _dbQuote + pad.getSubtype().getShortName() + _dbQtElemEndTag);
 }

  /**
   * @author Laura Cormos
   */
  private void writeAlignmentPadElement(Pad pad, int indent)
  {
    Assert.expect(pad != null);
    Assert.expect(indent >= 0);

    StringBuilder spaces = new StringBuilder();
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<alignmentPad");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    printAndUpdateChecksum(spaces + "name=" + _dbQuote + pad.getBoardAndComponentAndPadName() + _dbQtElemEndTag);
  }

  /**
   * @author Laura Cormos
   */
  private void writeAlignmentElement(int indent)
  {
    Assert.expect(indent >= 0);

    StringBuilder spaces = new StringBuilder();
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<alignment>");
    String endElemSpaces = spaces.toString();

    PanelSettings panelSettings = _project.getPanel().getPanelSettings();
    List<AlignmentGroup> alignmentGroups = new ArrayList<AlignmentGroup>();
    if (panelSettings.isLongPanel())
    {
      if (panelSettings.hasRightAlignmentGroups())
        alignmentGroups.addAll(panelSettings.getRightAlignmentGroupsForLongPanel());
      if (panelSettings.hasLeftAlignmentGroups())
        alignmentGroups.addAll(panelSettings.getLeftAlignmentGroupsForLongPanel());
    }
    else if (panelSettings.hasAlignmentGroups())
      alignmentGroups = panelSettings.getAlignmentGroupsForShortPanel();

    ++indent;
    for (AlignmentGroup alignmentGroup : alignmentGroups)
      writeAlignmentGroupElement(alignmentGroup, indent);

    printAndUpdateChecksum(endElemSpaces + "</alignment>");
  }

  /**
   * @author Laura Cormos
   */
  private void writeAlignmentGroupElement(AlignmentGroup alignmentGroup, int indent)
  {
    Assert.expect(alignmentGroup != null);
    Assert.expect(indent >= 0);

    StringBuilder spaces = new StringBuilder();
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<alignmentGroup");
    String endElemSpaces = spaces.toString();

    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    printAndUpdateChecksum(spaces + "name=" + _dbQuote + alignmentGroup.getName() + _dbQuoteGtTag);

    List<Fiducial> fiducials = alignmentGroup.getFiducials();
    List<Pad> pads = alignmentGroup.getPads();

    ++indent;
    for (Pad pad : pads)
      writeAlignmentPadElement(pad, indent);
    for (Fiducial fiducial : fiducials)
      writeAlignmentFiducialElement(fiducial, indent);

    printAndUpdateChecksum(endElemSpaces + "</alignmentGroup>");
  }
}
