package com.axi.v810.datastore.testResults;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class BoardResults
{
  private String _boardName;
  private boolean _xOut;
  private String _serialNumber;

  /**
   * @author Bill Darbie
   */
  void setBoardName(String boardName)
  {
    Assert.expect(boardName != null);
    _boardName = boardName;
  }

  /**
   * @author Laura Cormos
   */
  public String getBoardName()
  {
    Assert.expect(_boardName != null);
    return _boardName;
  }

  /**
   * @author Bill Darbie
   */
  void setXout(boolean xOut)
  {
    _xOut = xOut;
  }

  /**
   * @author Laura Cormos
   */
  public boolean isXout()
  {
    return _xOut;
  }

  /**
   * @author Bill Darbie
   */
  void setSerialNumber(String serialNumber)
  {
    Assert.expect(serialNumber != null);
    _serialNumber = serialNumber;
  }

  /**
   * @author Laura Cormos
   */
  public String getSerialNumber()
  {
    Assert.expect(_serialNumber != null);
    return _serialNumber;
  }
}
