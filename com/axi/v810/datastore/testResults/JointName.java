package com.axi.v810.datastore.testResults;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class JointName
{
  private String _boardName;
  private String _refDes;
  private String _padName;

  /**
   * @author Bill Darbie
   */
  JointName(String boardName, String refDes, String padName)
  {
    Assert.expect(boardName != null);
    Assert.expect(refDes != null);
    Assert.expect(padName != null);

    _boardName = boardName;
    _refDes = refDes;
    _padName = padName;
  }

  /**
   * @author Bill Darbie
   */
  public String getBoardName()
  {
    Assert.expect(_boardName != null);
    return _boardName;
  }

  /**
   * @author Bill Darbie
   */
  public String getReferenceDesignator()
  {
    Assert.expect(_refDes != null);
    return _refDes;
  }

  /**
   * @author Bill Darbie
   */
  public String getPadName()
  {
    Assert.expect(_padName != null);
    return _padName;
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getBoardName() + " " + getReferenceDesignator() + " " + getPadName();
  }
}
