package com.axi.v810.datastore.testResults;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.testResults.*;

/**
 * @author Bill Darbie
 */
public class IndictmentResult
{
  private IndictmentEnum _indictmentEnum;
  private AlgorithmEnum _algorithmEnum;

  private List<MeasurementResult> _failingMeasurements;
  private List<MeasurementResult> _relatedMeasurements;

  /**
   * @author Rex Shang
   */
  IndictmentResult()
  {
    _failingMeasurements = new ArrayList<MeasurementResult>();
    _relatedMeasurements = new ArrayList<MeasurementResult>();
  }

  /**
   * @author Bill Darbie
   */
  void setIndictmentEnum(IndictmentEnum indictmentEnum)
  {
    Assert.expect(indictmentEnum != null);
    _indictmentEnum = indictmentEnum;
  }

  /**
   * @author Bill Darbie
   */
  public IndictmentEnum getIndictmentEnum()
  {
    Assert.expect(_indictmentEnum != null);
    return _indictmentEnum;
  }

  /**
   * @author Bill Darbie
   */
  void setAlgorithmEnum(AlgorithmEnum algorithmEnum)
  {
    Assert.expect(algorithmEnum != null);
    _algorithmEnum = algorithmEnum;
  }

  /**
   * @author Bill Darbie
   */
  public AlgorithmEnum getAlgorithmEnum()
  {
    Assert.expect(_algorithmEnum != null);
    return _algorithmEnum;
  }

  /**
   * @author Bill Darbie
   */
  void addFailingMeasurement(MeasurementResult measurementResult)
  {
    Assert.expect(measurementResult != null);
    _failingMeasurements.add(measurementResult);
  }

  /**
   * @author Bill Darbie
   */
  public List<MeasurementResult> getFailingMeasurementResults()
  {
    return _failingMeasurements;
  }

  /**
   * @author Bill Darbie
   */
  void addRelatedMeasurement(MeasurementResult measurementResult)
  {
    Assert.expect(measurementResult != null);
    _relatedMeasurements.add(measurementResult);
  }

  /**
   * @author Bill Darbie
   */
  public List<MeasurementResult> getRelatedMeasurementResults()
  {
    return _relatedMeasurements;
  }
}
