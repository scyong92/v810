package com.axi.v810.datastore.testResults;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class ComponentName
{
  private String _boardName;
  private String _refDes;

  /**
   * @author Bill Darbie
   */
  ComponentName(String boardName, String refDes)
  {
    Assert.expect(boardName != null);
    Assert.expect(refDes != null);

    _boardName = boardName;
    _refDes = refDes;
  }

  /**
   * @author Bill Darbie
   */
  public String getBoardName()
  {
    Assert.expect(_boardName != null);
    return _boardName;
  }

  /**
   * @author Bill Darbie
   */
  public String getReferenceDesignator()
  {
    Assert.expect(_refDes != null);
    return _refDes;
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return getBoardName() + " " + getReferenceDesignator();
  }
}
