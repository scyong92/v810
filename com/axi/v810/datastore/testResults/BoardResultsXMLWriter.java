package com.axi.v810.datastore.testResults;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
public class BoardResultsXMLWriter
{
  private boolean _printSpaces = true;
  private final String _space = " ";
  private boolean _exceptionThrown = false;
  private Board _board;
  private String _serialNumber;
  private String _boardFilePath;
  private PrintWriter _os;
  private boolean _isXedOut = false;
  private int _totalNumberOfIndictments = 0;
  private int _numberOfProcessedComponents = 0;
  private int _numberOfDefectiveComponents = 0;
  private int _numberOfProcessedJoints = 0;
  private int _numberOfDefectiveJoints = 0;
  private JointInspectionResult _currentJoint;
  private ComponentInspectionResult _currentComponent;
  private int _indent;
  private int _numOfSpaces = XMLWriterStringUtil.NUM_INDENT_SPACES;
  // kept for future reference if comments are added
  private String _gt = XMLWriterStringUtil.GREATER_THEN_TAG;
  private String _dbQuote = XMLWriterStringUtil.ESCAPED_DOUBLE_QUOTE;
  private String _elemEnd  = XMLWriterStringUtil.ELEMENT_END_TAG;
  private String _dbQuoteGt = _dbQuote + _gt;
  private String _dbQtElemEnd = _dbQuote + _elemEnd;
  private List<Component> _defectiveComponents = new ArrayList<Component>();
  private Map<Component, List<JointInspectionResult>> _componentToFailedJointsListMap =
      new HashMap<Component,List<JointInspectionResult>>();
  private Map<Component, Integer> _componentToNumProcessedJointsMap = new HashMap<Component,Integer>();
  private Map<Component, Integer> _componentToTotalNumInspectableJointsMap = new HashMap<Component,Integer>();
  private Map<JointInspectionResult, FailingImageInfo> _jointInspectionResultToFailingJointImageInfoMap =
      new HashMap<JointInspectionResult,FailingImageInfo>();
  private Map<ComponentInspectionResult, FailingImageInfo> _compInspectionResultToFailingJointImageInfoMap =
      new HashMap<ComponentInspectionResult,FailingImageInfo>();
  
  private String _xoutReason = "";

  /**
   * @author Laura Cormos
   */
  public BoardResultsXMLWriter()
  {
    // do nothing
  }

  /**
   * @author Laura Cormos
   */
  public void open(Board board, String boardXMLFilePath, String serialNumber, int indent, boolean printSpaces) throws DatastoreException
  {
    Assert.expect(board != null);
    Assert.expect(boardXMLFilePath != null);
    Assert.expect(serialNumber != null);
    Assert.expect(indent > 0);

    _board = board;
    _indent = indent;
    _printSpaces = printSpaces;
    _boardFilePath = boardXMLFilePath;
    _serialNumber = serialNumber;      //Chin Seong :: Xed-Out Process the XedOut Result file

    try
    {
      _os = new PrintWriter(_boardFilePath);
      writeBoardElementBeginning(serialNumber);
    }
    catch (FileNotFoundException ex)
    {
      _exceptionThrown = true;
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_boardFilePath);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      // if the file still exists and an exception was thrown, delete it
      if (_exceptionThrown && FileUtil.exists(_boardFilePath))
      {
        // close the file in case an exception was thrown before the file was closed
        if (_os != null)
          _os.close();
        try
        {
          FileUtilAxi.delete(_boardFilePath);
        }
        catch (CannotDeleteFileDatastoreException ex)
        {
          if (_exceptionThrown)
            ex.printStackTrace();
          else
            throw ex;
        }
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  public BoardResultsSummary getBoardResultsSummary()
  {
    return new BoardResultsSummary(_totalNumberOfIndictments,
                                  _numberOfProcessedComponents, _numberOfDefectiveComponents,
                                  _numberOfProcessedJoints, _numberOfDefectiveJoints);
  }

  /**
   * @author Laura Cormos
   */
  public void close() throws DatastoreException
  {
    writeBoardElementEnding();

    if (_os != null)
      _os.close();

    // CR1018 Memory Leak fix by LeeHerng - Clear off hash map
    clearMaps();
    
    //Chin Seong :: Xed-Out Process the XedOut Result file
    processXedOutBoardImage();
    processXedOutBoardResultFile();
    
    
//    return new BoardResultsSummary(_totalNumberOfIndictments,
//                                   _numberOfProcessedComponents, _numberOfDefectiveComponents,
//                                   _numberOfProcessedJoints, _numberOfDefectiveJoints);
  }

  /**
   * 
   * @throws DatastoreException 
   * 
   * @author ?? 
   */
  private void processXedOutBoardImage() throws DatastoreException
  {
    if (_isXedOut)
    {
      List<Component> components = _board.getComponents();
      for (Component component : components)
      {
        List<JointInspectionResult> failedJoints = _componentToFailedJointsListMap.get(component);
        if (null != failedJoints && failedJoints.isEmpty() == false)
        {
          for (JointInspectionResult failedJoint : failedJoints)
          {
            List<Pair<String, String>> imagePathAndSlices = _jointInspectionResultToFailingJointImageInfoMap.get(failedJoint).getRepairImagePathsAndSliceNames();
            if (null != imagePathAndSlices && imagePathAndSlices.isEmpty() == false)
            {
              for (Pair<String, String> imagePathAndSlice : imagePathAndSlices)
              {
                deleteXedOutBoardFailedImages(imagePathAndSlice.getFirst().toString());
              }
            }
            List<Pair<Integer, Pair<String, String>>> additionalImagesList = InspectionResultsXMLWriter.getInstance().getAdditionalImagesForJoint(failedJoint);
            if (null != additionalImagesList && additionalImagesList.isEmpty() == false)
            {
              for (Pair<Integer, Pair<String, String>> offsetAndimagePathAndSlice : additionalImagesList)
              {
                deleteXedOutBoardFailedImages(offsetAndimagePathAndSlice.getFirst().toString());
              }
            }
          }
        }
      }
    }
  }

  /**
   * @param imagesPath
   * @throws DatastoreException 
   * no author?
   * 
   */
  private void deleteXedOutBoardFailedImages(String imagesPath) throws DatastoreException
  {
      Assert.expect(imagesPath != null);

      try
      {
        if (FileUtilAxi.exists(imagesPath))
          FileUtilAxi.delete(imagesPath);
      }
      catch (CannotDeleteFileDatastoreException ex)
      {
        if (_exceptionThrown)
          ex.printStackTrace();
        else
          throw ex;
      }
  }

  //Chin Seong :: Xed-Out Process the XedOut Result file
  private void processXedOutBoardResultFile() throws DatastoreException
  {
      if(_isXedOut)
      {
         try
         {
           if (FileUtilAxi.exists(_boardFilePath))
            FileUtilAxi.delete(_boardFilePath);
         }
         catch (CannotDeleteFileDatastoreException ex)
         {
           if (_exceptionThrown)
             ex.printStackTrace();
           else
             throw ex;
         }
        try
        {
            _os = new PrintWriter(_boardFilePath);
            writeBoardElementBeginning(_serialNumber);
        }
        catch (FileNotFoundException ex)
        {
            _exceptionThrown = true;
            FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_boardFilePath);
            dex.initCause(ex);
            throw dex;
        }
        finally{
            writeBoardElementEnding();
            if (_os != null)
                _os.close();
        }
      }
  }
  /**
   * @author Laura Cormos
   */
  public void writeJointInspectionResult(JointInspectionResult jointInspResult,
                                         List<Pair<String,String>> repairImagePathsAndSliceNames,
                                         int imageWidth, int imageHeight)
  {
    Assert.expect(jointInspResult != null);
    Assert.expect(repairImagePathsAndSliceNames != null);

    _numberOfProcessedJoints++;

    boolean jointPassed = jointInspResult.passed();
    Component component = jointInspResult.getJointInspectionData().getPad().getComponent();
    Integer totalNumJoints = _componentToTotalNumInspectableJointsMap.get(component);
    if (totalNumJoints != null)
    {
      _componentToNumProcessedJointsMap.put(component, _componentToNumProcessedJointsMap.get(component) + 1 );
    }
    else // this happens only once per component the first time we see a joint from that component
    {
      _componentToTotalNumInspectableJointsMap.put(component, component.getComponentType().getInspectedPadTypes().size());
      _componentToNumProcessedJointsMap.put(component, 1);
      _numberOfProcessedComponents++;
    }

    // only process failing joints
    if (jointPassed == false)
    {
      _numberOfDefectiveJoints++;
      // if this component did not have any failing joints before or this is the first time its joints are processed
      // and this joint is defective, add the component to the list of defective components and increment the count
      if (_defectiveComponents.contains(component) == false)
      {
        _numberOfDefectiveComponents++;
        _defectiveComponents.add(component);
      }

      List<JointInspectionResult> failedJointsListForComponent = _componentToFailedJointsListMap.get(component);
      if (failedJointsListForComponent == null)
        failedJointsListForComponent = new ArrayList<JointInspectionResult>();

      failedJointsListForComponent.add(jointInspResult);
      _componentToFailedJointsListMap.put(component, failedJointsListForComponent);
      _jointInspectionResultToFailingJointImageInfoMap.put(jointInspResult,
                                                           new FailingImageInfo(repairImagePathsAndSliceNames,
                                                                                imageWidth,
                                                                                imageHeight));
    }
  }

  /**
   * @author Laura Cormos
   */
  public void writeComponentInspectionResult(ComponentInspectionResult componentInspectionResult,
                                             List<Pair<String,String>> repairImagePathsAndSliceNames,
                                             int imageWidth, int imageHeight )
  {
    Assert.expect(componentInspectionResult != null);
    Component componentToWrite = componentInspectionResult.getComponentInspectionData().getComponent();

    List<JointInspectionResult> failedJoints = _componentToFailedJointsListMap.get(componentToWrite);

    _compInspectionResultToFailingJointImageInfoMap.put(componentInspectionResult,
                                                        new FailingImageInfo(repairImagePathsAndSliceNames,
                                                                                  imageWidth,
                                                                                  imageHeight));
    // write out this component element only if there are failing joints or component level defects
    if ((failedJoints != null && failedJoints.size() > 0) || componentInspectionResult.getIndictments().size() > 0)
    {
      if (failedJoints == null || failedJoints.isEmpty())
        _numberOfDefectiveComponents++;
      writeComponentElement(componentToWrite, componentInspectionResult);
    }
  }

  /**
   * Writes out the beginning of the board element of the indictments.xml file
   *   <board name="1" boardTypeName="type_1" tested="true" serialNumber="32 l;.a41">
   *
   * @author Laura Cormos
   */
  private void writeBoardElementBeginning(String serialNumber)
  {
    Assert.expect(serialNumber != null);
    Assert.expect(_os != null);
    Assert.expect(_indent >= 0);

     StringBuilder spaces = new StringBuilder("");
     if (_printSpaces)
       for (int i = 0; i < (_indent * _numOfSpaces); i++)
         spaces.append(_space);;

     _os.println(spaces + "<board");
     // attribute indent
     if (_printSpaces)
       spaces.append("  ");

    _os.println(spaces + "name=" + _dbQuote + XMLWriterStringUtil.replaceIllegalStringChars(_board.getName()) + _dbQuote);
    _os.println(spaces + "boardTypeName=" + _dbQuote + XMLWriterStringUtil.replaceIllegalStringChars(_board.getBoardType().getName())
                 + _dbQuote);
     if (_board.isInspected() && !_isXedOut)
       _os.println(spaces + "tested=" + _dbQuote + "true" + _dbQuote);
     else
     {
       _os.println(spaces + "tested=" + _dbQuote + "false" + _dbQuote); //Chin Seong - temporary is Hard coded
       _os.println(spaces + "reason=" +  _dbQuote + _board.getXoutReason() + _dbQuote); //added in the xedout reason
     }
     _os.println(spaces + "serialNumber=" + _dbQuote + XMLWriterStringUtil.replaceIllegalStringChars(serialNumber) + _dbQuoteGt);

     _indent++; // set up the indent for the next nested element
   }

   /**
    * Writes out the end of the board element in the indictments.xml file
    *    <summary XedOut="false" indictments="5" components="5" testedComponents="5" defectiveComponents="5"
    *                                            joints="5" testedJoints="5" defectiveJoints="5"/>
    *  </board>
    *
    * @author Laura Cormos
    */
   private void writeBoardElementEnding()
   {
     Assert.expect(_os != null);
     Assert.expect(_indent >= 0);

     StringBuilder spaces = new StringBuilder("");
     if (_printSpaces)
       spaces = buildSpaceString();

     _os.println(spaces + "<summary");
     // attribute indent
     if (_printSpaces)
       spaces.append("  ");

     if (_isXedOut)
        _os.println(spaces + "XedOut=" + _dbQuote + "true" + _dbQtElemEnd);
     else
     {
        _os.println(spaces + "XedOut=" + _dbQuote + "false" + _dbQuote);
        _os.println(spaces + "indictments=" + _dbQuote + _totalNumberOfIndictments + _dbQuote);
        _os.println(spaces + "components=" + _dbQuote + _board.getNumComponents() + _dbQuote);
        _os.println(spaces + "testedComponents=" + _dbQuote + _numberOfProcessedComponents + _dbQuote);
        _os.println(spaces + "defectiveComponents=" + _dbQuote + _numberOfDefectiveComponents + _dbQuote);
        _os.println(spaces + "joints=" + _dbQuote + _board.getNumJoints() + _dbQuote);
        _os.println(spaces + "testedJoints=" + _dbQuote + _numberOfProcessedJoints + _dbQuote);
        _os.println(spaces + "defectiveJoints=" + _dbQuote + _numberOfDefectiveJoints + _dbQtElemEnd);
     }

     _indent = 1; // for the end of the board element
     StringBuilder endElemSpaces = new StringBuilder("");
     if (_printSpaces)
       spaces = buildSpaceString();
    _os.println(endElemSpaces + "</board>");
   }

   /**
    * Writes the component element and all its failing joint elements for the indictments.xml file
    *  <component refDes="C1" landPattern="CP002" failureType="joint">
    *  </component>
    * @author Laura Cormos
    *
    */
   private void writeComponentElement(Component component, ComponentInspectionResult componentInspectionResult)
   {
     Assert.expect(component != null);
     Assert.expect(componentInspectionResult != null);
     Assert.expect(_indent > 0);

     _currentComponent = componentInspectionResult;

     List<JointInspectionResult> failedJoints = _componentToFailedJointsListMap.get(component);

     StringBuilder spaces = new StringBuilder("");
     if (_printSpaces)
       spaces = buildSpaceString();

     _os.println(spaces + "<component");
     // attribute indent
     if (_printSpaces)
       spaces.append("  ");

     _os.println(spaces + "refDes=" + _dbQuote + XMLWriterStringUtil.replaceIllegalStringChars(component.getReferenceDesignator())
                 + _dbQuote);
     _os.println(spaces + "landPattern=" + _dbQuote + XMLWriterStringUtil.replaceIllegalStringChars(component.getLandPattern().getName())
                 + _dbQuote);
     if (failedJoints != null && (componentInspectionResult != null && componentInspectionResult.getIndictments().size() > 0))
       _os.println(spaces + "failureType=" + _dbQuote + "both" + _dbQuoteGt);
     else if (failedJoints == null)
       _os.println(spaces + "failureType=" + _dbQuote + "component" + _dbQuoteGt);
     else
       _os.println(spaces + "failureType=" + _dbQuote + "joint" + _dbQuoteGt);

     ++_indent;

     if (componentInspectionResult != null && componentInspectionResult.getIndictments().size() > 0)
       writeComponentIndictmentElement(componentInspectionResult);

     if (failedJoints != null)
       for (JointInspectionResult failedJoint : failedJoints)
         writeJointIndictmentElement(failedJoint);

    _os.println(spaces + "</component>");

    //decrement indent for the next element
    --_indent;
  }

  /**
   * Writes the componentindictment element for a component level defect call
   *  <componentIndictment jointType="capacitor" subtype="1206">
        <imageSize width="702" length="225"/>
        <componentDescriptionInImageCoords x="112" y="112" width="38" length="142" rotation="0.0"/>
        <allSliceImages>
          <failingImage sliceName="Pad" imageFileName="top20421600x5181600_0.jpg"/>
        </allSliceImages>
        <failingAlgorithm name="Open">
          <indictment priority="1" name="Open" sliceName="Pad" imageFileName="top20421600x5181600_0.jpg">
            <measurement name="fillet length % of nominal" value="88818.90625" units="percent"/>
            <measurement name="center to heel thickness ratio difference from nominal" value="0.3999999761581421" units="percent"/>
          </indictment>
        </failingAlgorithm>
      </componentIndictment>
   *
   * @author Laura Cormos
   */
  private void writeComponentIndictmentElement(ComponentInspectionResult componentInspectionResult)
  {
    Assert.expect(componentInspectionResult != null);
    Assert.expect(_indent > 0);

    ComponentInspectionData componentInspectionData = componentInspectionResult.getComponentInspectionData();
    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<componentIndictment");
    String endElemSpaces = spaces.toString();

    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    Map<JointTypeEnum, Set<Subtype>> jointTypeToSubtypeListMap = new HashMap<JointTypeEnum,Set<Subtype>>();
    Collection<ComponentIndictment> indictments = componentInspectionResult.getIndictments();
    for (ComponentIndictment componentIndictment : indictments)
    {
      JointTypeEnum jointTypeEnum = componentIndictment.getJointTypeEnum();
      //Subtype subtype = componentIndictment.getSubtype();
      //Siew Yeng - XCR-2168
      Set<Subtype> subtypeSet;
      if (jointTypeToSubtypeListMap.containsKey(jointTypeEnum))
        subtypeSet = jointTypeToSubtypeListMap.get(jointTypeEnum);
      else
        subtypeSet = new HashSet<Subtype>();
      subtypeSet.addAll(componentIndictment.getSubtypes());
      jointTypeToSubtypeListMap.put(jointTypeEnum, subtypeSet);
    }
    List<JointTypeEnum> jointTypeEnums = componentInspectionData.getComponent().getJointTypeEnums();

    if (jointTypeEnums.size() == 1)
    {
      _os.println(spaces + "jointType=" + _dbQuote + jointTypeEnums.get(0).getName() + _dbQuote);
      Set<Subtype> subtypeSet = jointTypeToSubtypeListMap.get(jointTypeEnums.get(0));
      if (subtypeSet.size() == 1)
        _os.println(spaces + "subtype=" + _dbQuote + XMLWriterStringUtil.replaceIllegalStringChars(subtypeSet.iterator().next().getShortName())
                    + _dbQuoteGt);
      else
        _os.println(spaces + "subtype=" + _dbQuote + StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY") + _dbQuoteGt);
    }
    else
    {
      _os.println(spaces + "jointType=" + _dbQuote + StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY") + _dbQuote);
      _os.println(spaces + "subtype=" + _dbQuote + StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY") + _dbQuoteGt);
    }
    ++_indent;

    // write out component level images element and ROI element only if the component fits in a single reconstructed
    // region AND the component has component level indictments
    if (componentFitsInOneRegion(componentInspectionResult) && componentInspectionResult.getIndictments().isEmpty() == false)
    {
      writeComponentDescriptionElement(componentInspectionResult);
      writeImageSizeElement(componentInspectionResult);
    }
    writeAllSliceImagesElement(componentInspectionResult);

    // write the actual indictments
    Map<Algorithm, List<ComponentIndictment>> algorithmToComponentIndictmentListMap = new HashMap<Algorithm,List<ComponentIndictment>>();
    for (ComponentIndictment indictment : indictments)
    {
      Algorithm indictmentAlgorithm = indictment.getAlgorithm();
      List<ComponentIndictment> indictmentsForAlgorithm = algorithmToComponentIndictmentListMap.get(indictmentAlgorithm);
      if (indictmentsForAlgorithm == null)
      {
        indictmentsForAlgorithm = new ArrayList<ComponentIndictment>();
        algorithmToComponentIndictmentListMap.put(indictmentAlgorithm, indictmentsForAlgorithm);
      }
      indictmentsForAlgorithm.add(indictment);
    }
    Set<Map.Entry<Algorithm, List<ComponentIndictment>>> algorithmMapEntrySet = algorithmToComponentIndictmentListMap.entrySet();
    for (Map.Entry<Algorithm, List<ComponentIndictment>> entry : algorithmMapEntrySet)
      writeComponentFailingAlgorithmElement(entry.getKey(), entry.getValue());

    _os.println(endElemSpaces + "</componentIndictment>");

    --_indent;
  }

  /**
   * Write the joint element for the indictments.xml file
   *  <jointIndictment padName="1" nodeName="GND" subtype="CP002_15" jointType="capacitor">
   *  </jointIndictment>
   * @author Laura Cormos
   */
  private void writeJointIndictmentElement(JointInspectionResult joint)
  {
    Assert.expect(joint != null);
    Assert.expect(_indent > 0);

    _currentJoint = joint;
    JointInspectionData jointInspectionData = joint.getJointInspectionData();
    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<jointIndictment");
    String endElemSpaces = spaces.toString();

    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    _os.println(spaces + "padName=" + _dbQuote +
                XMLWriterStringUtil.replaceIllegalStringChars(jointInspectionData.getPad().getName()) + _dbQuote);
    if (jointInspectionData.getPad().getNodeName().length() > 0)
      _os.println(spaces + "nodeName=" + _dbQuote +
                  XMLWriterStringUtil.replaceIllegalStringChars(jointInspectionData.getPad().getNodeName()) + _dbQuote);
    _os.println(spaces + "subtype=" + _dbQuote +
                XMLWriterStringUtil.replaceIllegalStringChars(jointInspectionData.getSubtype().getShortName()) + _dbQuote);
    _os.println(spaces + "jointType=" + _dbQuote + joint.getJointInspectionData().getJointTypeEnum().getName() + _dbQuoteGt);

    ++_indent;

    writePadDescriptionElement(joint);
    writeImageSizeElement(joint);
    writeAllSliceImagesElement(joint);

    Collection<JointIndictment> indictments = joint.getIndictments();
    Map<Algorithm, List<JointIndictment>> algorithmToJointIndictmentListMap = new HashMap<Algorithm, List<JointIndictment>>();
    for (JointIndictment indictment : indictments)
    {
      Algorithm indictmentAlgorithm = indictment.getAlgorithm();
      List<JointIndictment> indictmentsForAlgorithm = algorithmToJointIndictmentListMap.get(indictmentAlgorithm);
      if (indictmentsForAlgorithm == null)
      {
        indictmentsForAlgorithm = new ArrayList<JointIndictment>();
        algorithmToJointIndictmentListMap.put(indictmentAlgorithm, indictmentsForAlgorithm);
      }
      indictmentsForAlgorithm.add(indictment);
    }
    Set<Map.Entry<Algorithm, List<JointIndictment>>> algorithmMapEntrySet = algorithmToJointIndictmentListMap.entrySet();
    for (Map.Entry<Algorithm, List<JointIndictment>> entry : algorithmMapEntrySet)
      writeFailingAlgorithmElement(entry.getKey(), entry.getValue());

    _os.println(endElemSpaces + "</jointIndictment>");

    --_indent;
  }

  /**
   * Writes out the imageSize element for a joint element
   *  <imageSize width="345" length="345" />
   *
   * @author Laura Cormos
   */
  private void writeImageSizeElement(JointInspectionResult joint)
  {
    Assert.expect(joint != null);
    Assert.expect(_indent > 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<imageSize");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    FailingImageInfo jointInfo = _jointInspectionResultToFailingJointImageInfoMap.get(joint);
    _os.println(spaces + "width=" + _dbQuote + jointInfo.getImageWidth() + _dbQuote);
    _os.println(spaces + "length=" + _dbQuote + jointInfo.getImageHeight() + _dbQtElemEnd);
  }

  /**
     * Writes out the imageSize element for a component element
     *  <imageSize width="345" length="345" />
     *
     * @author Laura Cormos
     */
    private void writeImageSizeElement(ComponentInspectionResult component)
    {
      Assert.expect(component != null);
      Assert.expect(_compInspectionResultToFailingJointImageInfoMap != null);
      Assert.expect(_indent > 0);

      StringBuilder spaces = new StringBuilder("");
      if (_printSpaces)
        spaces = buildSpaceString();

      _os.println(spaces + "<imageSize");
      // attribute indent
      if (_printSpaces)
        spaces.append("  ");

      FailingImageInfo componentImageInfo = _compInspectionResultToFailingJointImageInfoMap.get(component);
      _os.println(spaces + "width=" + _dbQuote + componentImageInfo.getImageWidth() + _dbQuote);
      _os.println(spaces + "length=" + _dbQuote + componentImageInfo.getImageHeight() + _dbQtElemEnd);
  }

  /**
   * Writes the element that contains the names of all images associated with this joint and the image elements too
   *  <allSliceImages>
   *   </allSliceImages>
   * @author Laura Cormos
   */
  private void writeAllSliceImagesElement(JointInspectionResult joint)
  {
    Assert.expect(joint != null);
    Assert.expect(_indent >= 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<allSliceImages>");

    ++_indent;
    for (Pair<String, String> imagePathAndSlice : _jointInspectionResultToFailingJointImageInfoMap.get(joint).getRepairImagePathsAndSliceNames())
      writeFailingImageElement(imagePathAndSlice, 0); // the focus offset for slices tested only once is always 0

    List<Pair<Integer,Pair<String, String>>> additionalImagesList = InspectionResultsXMLWriter.getInstance().getAdditionalImagesForJoint(joint);
    if (additionalImagesList.isEmpty() == false)
    {
      for (Pair<Integer, Pair<String, String>> offsetAndimagePathAndSlice : additionalImagesList)
        writeFailingImageElement(offsetAndimagePathAndSlice.getSecond(), offsetAndimagePathAndSlice.getFirst());
    }

    _os.println(spaces + "</allSliceImages>");

    --_indent;
  }

  /**
   * Writes the failing image element as part of the element containing all the failing image slices
   * <failingImage
   *    sliceName="Clear Chip Pad"
   *    imageFileName="top53385600x60035600_0.jpg"
   *    focusOffset="0" />
   *
   * @author Laura Cormos
   */
  private void writeFailingImageElement(Pair<String, String> imageAndSlice, int focusOffset)
  {
    Assert.expect(imageAndSlice != null);
    Assert.expect(_indent > 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<failingImage");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    _os.println(spaces + "sliceName=" + _dbQuote + imageAndSlice.getSecond() + _dbQuote);
    String imagePath = imageAndSlice.getFirst();
    File temp = new File(imagePath);
    _os.println(spaces + "focusOffset=" + _dbQuote + focusOffset + _dbQuote);
    _os.println(spaces + "imageFileName=" + _dbQuote + temp.getName() + _dbQtElemEnd);
  }

  /**
   * Writes the failingAlgorithm element for this joint
   *    <failingAlgorithm name="short">
   *      <indictment priority="1" name="insufficient" sliceName="Clear Chip Pad" imageFileName="top53385600x60035600_0.jpg">
   *        <measurement name="slkdfj" value="545" units="mil" />
   *        <measurement name="lksjdf" value="454" units="mils" />
   *        <measurement name="lakdfj" value="454" units="mils" />
   *      </indictment>
   *    </failingAlgorithm>
   *
   * @author Laura Cormos
   */
  private void writeFailingAlgorithmElement(Algorithm indictmentAlgorithm, List<JointIndictment> jointIndictments)
  {
    Assert.expect(indictmentAlgorithm != null);
    Assert.expect(jointIndictments != null);
    Assert.expect(_indent > 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<failingAlgorithm");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    _os.println(spaces + "name=" + _dbQuote + indictmentAlgorithm.getAlgorithmEnum().getName() + _dbQuoteGt);

    ++_indent;

    for (JointIndictment indictment : jointIndictments)
      writeIndictmentElement(indictment);

    _os.println(spaces + "</failingAlgorithm>");

    --_indent;
  }

  /**
   * Writes the failingAlgorithm element for this joint
   *    <failingAlgorithm name="short">
   *      <indictment priority="1" name="insufficient" sliceName="sdfh" imageFileName="sdgsg">
   *        <measurement name="slkdfj" value="545" units="mil" />
   *        <measurement name="lksjdf" value="454" units="mils" />
   *        <measurement name="lakdfj" value="454" units="mils" />
   *      </indictment>
   *    </failingAlgorithm>
   *
   * @author Laura Cormos
   */
  private void writeComponentFailingAlgorithmElement(Algorithm indictmentAlgorithm, List<ComponentIndictment> componentIndictments)
  {
    Assert.expect(indictmentAlgorithm != null);
    Assert.expect(componentIndictments != null);
    Assert.expect(_indent > 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<failingAlgorithm");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    _os.println(spaces + "name=" + _dbQuote + indictmentAlgorithm.getAlgorithmEnum().getName() + _dbQuoteGt);

    ++_indent;

    for (ComponentIndictment indictment : componentIndictments)
      writeIndictmentElement(indictment);

    _os.println(spaces + "</failingAlgorithm>");

    --_indent;
  }

  /**
   * Writes the indictment element for the current indictment algorithm
   *  <indictment priority="1" name="insufficient" sliceName="sdfh" imageFileName="sdgsg">
   *    <measurement name="slkdfj" value="545" units="mil" />
   *    <measurement name="lksjdf" value="454" units="mils" />
   *    <measurement name="lakdfj" value="454" units="mils" />
   *  </indictment>
   *
   * @author Laura Cormos
   */
  private void writeIndictmentElement(JointIndictment indictment)
  {
    Assert.expect(indictment != null);
    Assert.expect(_indent > 0);

    ++_totalNumberOfIndictments;

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<indictment");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    Collection<JointMeasurement> failingMeasurements =  indictment.getFailingMeasurements();
    // assuming that all measurements for an indictment are from the same slice, I'm using one of the measurements to
    // get the slice's name below
    JointMeasurement measurement = failingMeasurements.iterator().next();
    /** @todo future LC uncomment below when indictment priority is ready */
//    _os.println(spaces + "priority=" + _dbQuote + indictment.getPriority() + _dbQuote);
    _os.println(spaces + "priority=" + _dbQuote + "5" + _dbQuote);
    _os.println(spaces + "name=" + _dbQuote + indictment.getIndictmentName() + _dbQuote);

    String imageSliceEnumName = "";
    // Kee Chin Seong - due to XCR 1801, equalIgnoreCase need to be Exact same slice name,
    //                  the sliceName has to be processed for the "extra information"
    if(Config.getInstance().getBooleanValue(com.axi.v810.datastore.config.SoftwareConfigEnum.ADD_EXTRA_INFO_ON_SLICE_NAME_IN_OUTPUT_FILE) == true && 
       (measurement.getPad().getJointTypeEnum().equals(JointTypeEnum.PRESSFIT) ||
        measurement.getPad().getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE) ||
        measurement.getPad().getJointTypeEnum().equals(JointTypeEnum.OVAL_THROUGH_HOLE)) && //Siew Yeng - XCR-3318 - Oval PTH
        measurement.getSliceNameEnum().isMultiAngleSlice() == false)
    {
      imageSliceEnumName = UserDefinedSliceheights.getSliceNameForResult(measurement.getSliceNameEnum(), _currentJoint.getJointInspectionData().getSubtype());  
    }
    else
    {
      imageSliceEnumName = measurement.getSliceNameEnum().getName();
    }
    
    for (Pair<String, String> imagePathAndSliceNamePair : _jointInspectionResultToFailingJointImageInfoMap.get(_currentJoint).getRepairImagePathsAndSliceNames())
    {
      // XCR1801 - changed from 'startsWith' to 'equalsIgnoreCase' by Ying-Huan.Chu
      // Siew Yeng - XCR-2687 - fix multipe slice name at result xml
      // Chin Seong, Kee - XCR-2634
      // XCR - 2683, Chin Seong, Save Enhanced Image  
      String sliceName = "", imageName = "";
      boolean isUsingEnhancedImage = false;
      List<Pair<Integer,Pair<String, String>>> additionalImagesList = InspectionResultsXMLWriter.getInstance().getAdditionalImagesForJoint(_currentJoint);
      if (additionalImagesList.isEmpty() == false)
      {
        for (Pair<Integer, Pair<String, String>> offsetAndimagePathAndSlice : additionalImagesList)
        {
           if(offsetAndimagePathAndSlice.getSecond().getSecond().equals("Enhanced " + imageSliceEnumName))
           {
              sliceName = offsetAndimagePathAndSlice.getSecond().getSecond();
              imageName = offsetAndimagePathAndSlice.getSecond().getFirst();
              isUsingEnhancedImage = true;
           }
        }
      }
      if (imageSliceEnumName.equalsIgnoreCase(imagePathAndSliceNamePair.getSecond()))
      {
        _os.println(spaces + "sliceName=" + _dbQuote + ((isUsingEnhancedImage == true) ? sliceName : imageSliceEnumName) + _dbQuote);
        
        String imagePath = imagePathAndSliceNamePair.getFirst();
        File temp = new File(imagePath);
        _os.println(spaces + "imageFileName=" + _dbQuote + ((isUsingEnhancedImage == true) ? sliceName : temp.getName()) + _dbQuoteGt);
        break;
      }
    }

    ++_indent;
    for (JointMeasurement jointMeasurement : failingMeasurements)
      writeIndictmentMeasElement(jointMeasurement);

    _os.println(spaces + "</indictment>");

    --_indent;
  }

  /**
   * Writes the indictment element for the current indictment algorithm
   *  <indictment priority="1" name="insufficient" sliceName="sdfh" imageFileName="sdgsg">
   *    <measurement name="slkdfj" value="545" units="mil" />
   *    <measurement name="lksjdf" value="454" units="mils" />
   *    <measurement name="lakdfj" value="454" units="mils" />
   *  </indictment>
   *
   * @author Laura Cormos
   */
  private void writeIndictmentElement(ComponentIndictment indictment)
  {
    Assert.expect(indictment != null);
    Assert.expect(_indent > 0);

    ++_totalNumberOfIndictments;

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<indictment");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    Collection<ComponentMeasurement> failingMeasurements = indictment.getFailingMeasurements();
    // assuming that all measurements for an indictment are from the same slice,
    // I'm using one of the measurements to get the slice's name below
    ComponentMeasurement measurement = failingMeasurements.iterator().next();

    /** @todo future LC uncomment below when indictment priority is ready */
    //_os.println(spaces + "priority=" + _dbQuote + indictment.getPriority() + _dbQuote);
    _os.println(spaces + "priority=" + _dbQuote + "5" + _dbQuote);
    _os.println(spaces + "name=" + _dbQuote + indictment.getIndictmentName() + _dbQuote);
    _os.println(spaces + "sliceName=" + _dbQuote + measurement.getSliceNameEnum().getName() + _dbQuote);

    String imagePath = null;
    List<Pair<String, String>> imagePathAndSliceNamePairList =
      _compInspectionResultToFailingJointImageInfoMap.get(_currentComponent).getRepairImagePathsAndSliceNames();
    for (Pair<String, String> imagePathAndSliceNamePair : imagePathAndSliceNamePairList)
    {
      if (imagePathAndSliceNamePair.getSecond().equals(measurement.getSliceNameEnum().getName()))
      {
        imagePath = imagePathAndSliceNamePair.getFirst();
        break;
      }
    }
    // for a large components (e.g. grid array) it is possible that pad 1 is a different joint type (with fewer slices) than others
    // so when indictments come in for the other slices, no image will be available. In this case, put in the empty string
    if (imagePath != null)
    {
      File temp = new File(imagePath);
      _os.println(spaces + "imageFileName=" + _dbQuote + temp.getName() + _dbQuoteGt);
    }
    else
      _os.println(spaces + "imageFileName=" + _dbQuote + "" + _dbQuoteGt);

    ++_indent;
    for (ComponentMeasurement componentMeasurement : failingMeasurements)
      writeIndictmentMeasElement(componentMeasurement);

    _os.println(spaces + "</indictment>");

    --_indent;
  }

  /**
   * Writes the measuremnt element for the current indictment
   *    <measurement name="slkdfj" value="545" units="mil" />
   *
   * @author Laura Cormos
   */
  private void writeIndictmentMeasElement(JointMeasurement jointMeasurement)
  {
    Assert.expect(jointMeasurement != null);
    Assert.expect(_indent > 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<measurement");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    _os.println(spaces + "name=" + _dbQuote + jointMeasurement.getMeasurementName() + _dbQuote);
    _os.println(spaces + "value=" + _dbQuote + jointMeasurement.getValue() + _dbQuote);
    _os.println(spaces + "units=" + _dbQuote + jointMeasurement.getMeasurementUnitsEnum().getName() + _dbQtElemEnd);
  }

  /**
    * Writes the measuremnt element for the current indictment
    *    <measurement name="slkdfj" value="545" units="mil" />
    *
    * @author Laura Cormos
    */
   private void writeIndictmentMeasElement(ComponentMeasurement componentMeasurement)
   {
     Assert.expect(componentMeasurement != null);
     Assert.expect(_indent > 0);

     StringBuilder spaces = new StringBuilder("");
     if (_printSpaces)
       spaces = buildSpaceString();

     _os.println(spaces + "<measurement");
     // attribute indent
     if (_printSpaces)
       spaces.append("  ");

     _os.println(spaces + "name=" + _dbQuote + componentMeasurement.getMeasurementName() + _dbQuote);
     _os.println(spaces + "value=" + _dbQuote + componentMeasurement.getValue() + _dbQuote);
     _os.println(spaces + "units=" + _dbQuote + componentMeasurement.getMeasurementUnitsEnum().getName() + _dbQtElemEnd);
  }

  /**
   * Writes the region of interest element for the failing joint
   *  <padDescriptionInImageCoords x="52452" y="4252" width="542" length="52" rotation="55.0"/>
   *
   * @author Laura Cormos
   */
  private void writePadDescriptionElement(JointInspectionResult joint)
  {
   Assert.expect(joint != null);
    Assert.expect(_indent > 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<padDescriptionInImageCoords");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    JointInspectionData jointInspectionData = joint.getJointInspectionData();
    //Lim, Lay Ngor - pass in true value for ableToPerformResize if want the resized pad roi. 
    //Turn enableRotation to false because VVTS need to receive the pad roi without rotation apply.
    RegionOfInterest roi = jointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels(false, false);
    _os.println(spaces + "x=" + _dbQuote + roi.getCenterX() + _dbQuote);
    _os.println(spaces + "y=" + _dbQuote + roi.getCenterY() + _dbQuote);
    _os.println(spaces + "width=" + _dbQuote + roi.getWidth() + _dbQuote);
    _os.println(spaces + "length=" + _dbQuote + roi.getHeight() + _dbQuote);
    if (jointInspectionData.getPad().isTopSide())
      _os.println(spaces + "rotation=" + _dbQuote + jointInspectionData.getPad().getDegreesRotationAfterAllRotations() +
                  _dbQtElemEnd);
    else
      _os.println(spaces + "rotation=" + _dbQuote +
                  MathUtil.getDegreesWithin0To359(jointInspectionData.getPad().getDegreesRotationAfterAllRotations() - 180) +
                  _dbQtElemEnd);
  }


  /**
   * Writes the region of interest element for the failing joint
   *  <padDescriptionInImageCoords x="52452" y="4252" width="542" length="52" rotation="55.0"/>
   *
   * @author Laura Cormos
   */
  private void writeComponentDescriptionElement(ComponentInspectionResult component)
  {
    Assert.expect(component != null);
    Assert.expect(_indent > 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<componentDescriptionInImageCoords");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    ComponentInspectionData componentInspectionData = component.getComponentInspectionData();
    RegionOfInterest roi = componentInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels(false);
    _os.println(spaces + "x=" + _dbQuote + roi.getCenterX() + _dbQuote);
    _os.println(spaces + "y=" + _dbQuote + roi.getCenterY() + _dbQuote);
    _os.println(spaces + "width=" + _dbQuote + roi.getWidth() + _dbQuote);
    _os.println(spaces + "length=" + _dbQuote + roi.getHeight() + _dbQuote);
    if (componentInspectionData.getComponent().isTopSide())
      _os.println(spaces + "rotation=" + _dbQuote + componentInspectionData.getComponent().getDegreesRotationAfterAllRotations() +
                  _dbQtElemEnd);
    else
      _os.println(spaces + "rotation=" + _dbQuote +
                  MathUtil.getDegreesWithin0To359(componentInspectionData.getComponent().getDegreesRotationAfterAllRotations() - 180) +
                  _dbQtElemEnd);
  }

   /**
   * Writes the element that contains the names of all images associated with this joint and the image elements too
   *  <allSliceImages>
   *   </allSliceImages>
   * @author Laura Cormos
   */
  private void writeAllSliceImagesElement(ComponentInspectionResult component)
  {
    Assert.expect(component != null);
    Assert.expect(_indent >= 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<allSliceImages>");

    ++_indent;
    for (Pair<String, String> imagePathAndSlice : _compInspectionResultToFailingJointImageInfoMap.get(component).getRepairImagePathsAndSliceNames())
      writeFailingImageElement(imagePathAndSlice, 0); // the focus offset for slices tested only once is always 0

    List<Pair<Integer,Pair<String, String>>> additionalImagesList = InspectionResultsXMLWriter.getInstance().getAdditionalImagesForComponent(component);
    if (additionalImagesList.isEmpty() == false)
    {
      for (Pair<Integer, Pair<String, String>> offsetAndimagePathAndSlice : additionalImagesList)
        writeFailingImageElement(offsetAndimagePathAndSlice.getSecond(), offsetAndimagePathAndSlice.getFirst());
    }

    _os.println(spaces + "</allSliceImages>");

    --_indent;
  }

  /**
   * @author Laura Cormos
   */
  public void setXoutBoard()
  {
    _isXedOut = true;
    //Siew Yeng - XCR-2384 - set board xout when skip serial number
    if(getBoard().isBoardAlignmentExceptionCaught() == false) //XCR-2505 - add this to avoid alignment exception become xout
      getBoard().forceXout();
  }
  
    /*
   * @author Kee Chin Seong - this is to write out the reason 
   *                          why the board is xedout
   */
  public String getXoutReason()
  {
    return _xoutReason;
  }
  
  /*
   * @author Kee Chin Seong - this is to write out the reason 
   *                          why the board is xedout
   */
  public void setXoutReason(String xOutReason)
  {
    _xoutReason = xOutReason;
  }

  /**
   * @author Laura Cormos
   */
  private StringBuilder buildSpaceString()
  {
    StringBuilder spaces = new StringBuilder("");

    for (int i = 0; i < (_indent * _numOfSpaces); i++)
      spaces.append(_space);

    return spaces;
  }

  /**
   * If any of the other pads for the given component (other than pad 1) has a different reconstruction
   * region than pad 1, then the given component spans multiple regions
   *
   * @author Laura Cormos
   */
  private boolean componentFitsInOneRegion(ComponentInspectionResult compInspResult)
  {
    Assert.expect(compInspResult != null);

    TestProgram testProgram = InspectionResultsXMLWriter.getInstance().getProject().getTestProgram();
    ComponentInspectionData compInspData = compInspResult.getComponentInspectionData();
    ReconstructionRegion reconstructionRegion = compInspData.getPadOneJointInspectionData().getInspectionRegion();

    for (Pad pad : compInspData.getComponent().getPads())
    {
      if(pad.isInspected() == false)
        continue;
      
      JointInspectionData jointInspectionData = testProgram.getJointInspectionData(pad);
      if (reconstructionRegion != jointInspectionData.getInspectionRegion())
        return false;
    }
    return true;
  }

  /**
   * @author Laura Cormos
   */
  private class FailingImageInfo
  {
    private List<Pair<String, String>> _repairImagePaths;
    private int _imageWidth = 0;
    private int _imageHeight = 0;

    FailingImageInfo(List<Pair<String,String>> repairImagePathsAndSliceNames, int imageWidth, int imageHeight)
    {
      Assert.expect(repairImagePathsAndSliceNames != null);

      _repairImagePaths = repairImagePathsAndSliceNames;
      _imageWidth = imageWidth;
      _imageHeight = imageHeight;
    }

    /**
     * @author Laura Cormos
     */
    List<Pair<String, String>> getRepairImagePathsAndSliceNames()
    {
      Assert.expect(_repairImagePaths != null);
      return _repairImagePaths;
    }

    /**
     * @author Laura Cormos
     */
    int getImageWidth()
    {
      return _imageWidth;
    }

    /**
     * @author Laura Cormos
     */
    int getImageHeight()
    {
      return _imageHeight;
    }
  }

  /**
   * @author Kee Chin Seong
   */
  public boolean isXoutBoard()
  {
      return _isXedOut;
  }

  /**
   * @author Kee Chin Seong
   */
  public void CheckXOutBoard()
  {
    if (TestExecution.getInstance().isXoutDetectionModeEnabled())
    {
      if (InspectionResultsXMLWriter.getInstance().getProject().getPanel().getBoards().size() > 1)
      {
        decideBoardXoutStatus();
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void decideBoardXoutStatus()
  {
    InspectionResultsXMLWriter.getInstance().decideBoardXoutStatus(this);
  }

  /**
   * @author Laura Cormos
   */
  Board getBoard()
  {
    Assert.expect(_board != null);
    return _board;
  }

  /**
   * @author Cheah Lee Herng
   */
  private void clearMaps()
  {
      if (_componentToFailedJointsListMap != null)
          _componentToFailedJointsListMap.clear();

      if (_componentToNumProcessedJointsMap != null)
          _componentToNumProcessedJointsMap.clear();

      if (_componentToTotalNumInspectableJointsMap != null)
          _componentToTotalNumInspectableJointsMap.clear();

      if (_jointInspectionResultToFailingJointImageInfoMap != null)
          _jointInspectionResultToFailingJointImageInfoMap.clear();
      
      if (_compInspectionResultToFailingJointImageInfoMap != null)
          _compInspectionResultToFailingJointImageInfoMap.clear();
  }
}

