package com.axi.v810.datastore.testResults;

import com.axi.util.*;

/**
 * Container class for all the summary info for any board after an inspection run
 * @author Laura Cormos
 */

public class BoardResultsSummary
{
  private int _numberOfIndictments = -1;
  private int _numberOfProcessedComponents = -1;
  private int _numberOfDefectiveComponents = -1;
  private int _numberOfProcessedJoints = -1;
  private int _numberOfDefectiveJoints = -1;

  /**
   * @author Laura Cormos
   */
  BoardResultsSummary(int numberOfIndictments,
                      int numberOfProcessedComponents, int numberOfDefectiveComponents,
                      int numberOfProcessedJoints, int numberOfDefectiveJoints)
  {
    _numberOfIndictments = numberOfIndictments;
    _numberOfProcessedComponents = numberOfProcessedComponents;
    _numberOfDefectiveComponents = numberOfDefectiveComponents;
    _numberOfProcessedJoints = numberOfProcessedJoints;
    _numberOfDefectiveJoints = numberOfDefectiveJoints;
  }

  /**
   * @author Laura Cormos
   */
  int getNumberOfIndictments()
  {
    Assert.expect(_numberOfIndictments != -1);
    return _numberOfIndictments;
  }

  /**
   * @author Laura Cormos
   */
  int getNumberProcessedComponents()
  {
    Assert.expect(_numberOfProcessedComponents != -1);
    return _numberOfProcessedComponents;
  }

  /**
   * @author Laura Cormos
   */
  int getNumberDefectiveComponents()
  {
    Assert.expect(_numberOfDefectiveComponents != -1);
    return _numberOfDefectiveComponents;
  }

  /**
   * @author Laura Cormos
   */
  int getNumberProcessedJoints()
  {
    Assert.expect(_numberOfProcessedJoints != -1);
    return _numberOfProcessedJoints;
  }

  /**
   * @author Laura Cormos
   */
  int getNumberDefectiveJoints()
  {
    Assert.expect(_numberOfDefectiveJoints != -1);
    return _numberOfDefectiveJoints;
  }
}
