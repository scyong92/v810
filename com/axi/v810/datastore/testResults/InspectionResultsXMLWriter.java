package com.axi.v810.datastore.testResults;

import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.AlgorithmSettingEnum;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Laura Cormos
 */
public class InspectionResultsXMLWriter
{
  private static InspectionResultsXMLWriter _instance;
  private boolean _printSpaces = false;
  private boolean _exceptionThrown = false;
  private Project _project;
  private PrintWriter _os;
  private String _cadFilePath;
  private String _cadFileInResultsDirName;
  private String _cadFileInResultsDirFullPath;
  private String _settingsFilePath;
  private String _settingsFileInResultsDirName;
  private String _settingsFileInResultsDirFullPath;
  private String _cadXmlSchemaFilePath;
  private String _cadXmlSchemaInResultsDirFilePath;
  private String _resultsXmlSchemaFilePath;
  private String _resultsXmlSchemaInResultsDirFilePath;
  private String _settingsXmlSchemaFilePath;
  private String _settingsXmlSchemaInResultsDirFilePath;
  private String _inspectionResultsDir;
  private String _projName;
  private BooleanRef _productionMode; //if false, this is a demo mode
  private int _panelNumberOfIndictments;
  private int _panelNumberProcessedComponents;
  private int _panelNumberDefectiveComponents;
  private int _panelNumberProcessedJoints;
  private int _panelNumberDefectiveJoints;
  private String _panelSerialNumber;
  private long _inspRunTimeStamp;
  private String _startDateTime;
  private String _endDateTime;
  private int _indent = 0;
  String _resultsFilePath;
  String _tempResultsFilePath;
  private int _numOfSpaces = XMLWriterStringUtil.NUM_INDENT_SPACES;
  // keep this for future reference, if comments are added to this file
  private String _beginCommentTag = XMLWriterStringUtil.BEGIN_COMMENT_TAG;
  private String _endCommentTag = XMLWriterStringUtil.END_COMMENT_TAG;
  private String _gt = XMLWriterStringUtil.GREATER_THEN_TAG;
  private String _dbQuote = XMLWriterStringUtil.ESCAPED_DOUBLE_QUOTE;
  private String _elemEnd  = XMLWriterStringUtil.ELEMENT_END_TAG;
  private String _aliase = XMLWriterStringUtil._ALIASE_STRING;
  private String _dbQuoteGt = _dbQuote + _gt;
  private String _dbQtElemEnd = _dbQuote + _elemEnd;
  private Map<Board, BoardResultsXMLWriter> _boardToBoardXMLWriterMap = new HashMap<Board, BoardResultsXMLWriter>();
  private Map<String, String> _boardNameToBoardSerialNumMap = new HashMap<String, String>();
  private Map<Board, String> _boardToBoardResultsFilePathMap = new HashMap<Board,String>();
  private String _isPanelTestedReason;

  private Map<JointInspectionResult, List<Pair<Integer,Pair<String, String>>>> _jointResultToRepairImageNameAndSliceNameOffsetListMap =
      new HashMap<JointInspectionResult,List<Pair<Integer,Pair<String,String>>>>();
  private Map<ComponentInspectionResult, List<Pair<Integer,Pair<String, String>>>> _componentResultToRepairImageNameAndSliceNameOffsetListMap =
      new HashMap<ComponentInspectionResult,List<Pair<Integer,Pair<String,String>>>>();

  private Map<Board, String> _boardToTempIndictmentFileName = new HashMap<Board,String>();
  private Map<BoardResultsXMLWriter, BoardResultsSummary> _boardWriterToBoardSummary = new HashMap<BoardResultsXMLWriter,BoardResultsSummary>();
  private boolean _bypassWithResultsMode = false;
  private boolean _samplingMode = false;

  /**
   * @author Laura Cormos
   */
  public static synchronized InspectionResultsXMLWriter getInstance()
  {
    if (_instance == null)
      _instance = new InspectionResultsXMLWriter();

    return _instance;
  }

  /**
   * @author Laura Cormos
   */
  public InspectionResultsXMLWriter()
  {
    // do nothing
  }

  /**
   * @author Laura Cormos
   */
  public void open(Project project,
                   long inspRunTimeStamp,
                   String panelSerialNumber,
                   Map<String, String> boardNameToBoardSerialNumMap,
                   String isPanelTestedReason) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(inspRunTimeStamp > 0);
    Assert.expect(boardNameToBoardSerialNumMap != null);
    Assert.expect(panelSerialNumber != null);
    Assert.expect(isPanelTestedReason != null);

    _inspRunTimeStamp = inspRunTimeStamp;
    DateFormat myformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz");
    myformat.setTimeZone(TimeZone.getDefault());
    _startDateTime = myformat.format(new java.util.Date(_inspRunTimeStamp));
    _project = project;
    _projName = project.getName();
    _boardNameToBoardSerialNumMap = boardNameToBoardSerialNumMap;
    _panelSerialNumber = panelSerialNumber;
    _isPanelTestedReason = isPanelTestedReason;

    // create internal structures
    if (_boardToBoardXMLWriterMap == null)
      _boardToBoardXMLWriterMap = new HashMap<Board, BoardResultsXMLWriter>();
    if (_boardToBoardResultsFilePathMap == null)
      _boardToBoardResultsFilePathMap = new HashMap<Board, String>();
    if (_jointResultToRepairImageNameAndSliceNameOffsetListMap == null)
      _jointResultToRepairImageNameAndSliceNameOffsetListMap = new HashMap<JointInspectionResult,List<Pair<Integer,Pair<String,String>>>>();
    if (_componentResultToRepairImageNameAndSliceNameOffsetListMap == null)
      _componentResultToRepairImageNameAndSliceNameOffsetListMap = new HashMap<ComponentInspectionResult,List<Pair<Integer,Pair<String,String>>>>();
    // give each board on this panel a unique name to be used when writing out temporary board indictments xml files
    int i = 1;
    for (Board board : _project.getPanel().getBoards())
    {
      _boardToTempIndictmentFileName.put(board, Integer.toString(i));
      i++;
    }

    // figure out all the file paths and file names
    _inspectionResultsDir = Directory.getInspectionResultsDir(_projName, _inspRunTimeStamp);
    _resultsFilePath = FileName.getXMLResultsFileFullPath(_projName, _inspRunTimeStamp);
    _tempResultsFilePath = FileName.getXMLTempResultsFileFullPath(_projName, _inspRunTimeStamp);

    _cadFilePath = FileName.getCadFileNameFullPath(_projName, _project.getCadXmlChecksum());
    _cadFileInResultsDirName = FileName.getCadFileName(_project.getCadXmlChecksum());
    _cadFileInResultsDirFullPath = FileName.getCadFileNameInResultsDirFullPath(_projName,
                                                                               _project.getCadXmlChecksum());

    _settingsFilePath = FileName.getSettingsFileNameFullPath(_projName, _project.getSettingsXmlChecksum());
    _settingsFileInResultsDirName = FileName.getSettingsFileName(_project.getSettingsXmlChecksum());
    _settingsFileInResultsDirFullPath = FileName.getSettingsFileNameInResultsDirFullPath(_projName,
                                                                                         _project.getSettingsXmlChecksum());

    _cadXmlSchemaFilePath = FileName.getCadXmlSchemaFileNameFullPath();
    _cadXmlSchemaInResultsDirFilePath = FileName.getCadXmlSchemaInResultsDirFullPath(_projName, _inspRunTimeStamp);

    _resultsXmlSchemaFilePath = FileName.getResultsXmlSchemaFileNameFullPath();
    _resultsXmlSchemaInResultsDirFilePath = FileName.getResultsXmlSchemaInResultsDirFullPath(_projName, _inspRunTimeStamp);

    _settingsXmlSchemaFilePath = FileName.getSettingsXmlSchemaFileNameFullPath();
    _settingsXmlSchemaInResultsDirFilePath = FileName.getSettingsXmlSchemaInResultsDirFullPath(_projName, _inspRunTimeStamp);

    if (FileUtilAxi.exists(_inspectionResultsDir) == false)
      FileUtilAxi.mkdirs(_inspectionResultsDir);

    try
    {
      _os = new PrintWriter(_tempResultsFilePath);
      // write the header and any comments
      _os.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
      if (_printSpaces)
        _os.println("");

      _os.println(_beginCommentTag + " All focus offsets are in nanometers " + _endCommentTag);

      writeTopLevelElementBeginning();
    }
    catch (FileNotFoundException ex)
    {
      _exceptionThrown = true;
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_tempResultsFilePath);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      // if the file still exists and an exception was thrown, delete it
      if (_exceptionThrown && FileUtil.exists(_tempResultsFilePath))
      {
        try
        {
          // close the file in case an exception was thrown before the file was closed
          if (_os != null)
            closePrintWriter();

          FileUtilAxi.delete(_tempResultsFilePath);
        }
        catch (CannotDeleteFileDatastoreException ex)
        {
          if (_exceptionThrown)
            ex.printStackTrace();
        }
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  public void close(long endDateTime) throws DatastoreException
  {
    try
    {
      if (_boardToBoardXMLWriterMap == null)
      {
        // there isn't an open file, just be done.
        return;
      }

      TestProgram testProgram = InspectionResultsXMLWriter.getInstance().getProject().getTestProgram();

      //Kee Chin Seong - set X out for those serial number Skipped
      //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
      //Skip setting X out for remaining boards if manual realignment performed
      if(testProgram.isRealignPerformed() == false || _project.getTestProgram().getProject().getPanel().getPanelSettings().isPanelBasedAlignment())
        finishRemainingBoards();

      for (BoardResultsXMLWriter boardXMLWriter : _boardToBoardXMLWriterMap.values())
      {
        BoardResultsSummary boardSummary = boardXMLWriter.getBoardResultsSummary();
        boardXMLWriter.CheckXOutBoard();
        if(!boardXMLWriter.isXoutBoard()){
            _panelNumberOfIndictments += boardSummary.getNumberOfIndictments();
            _panelNumberProcessedComponents += boardSummary.getNumberProcessedComponents();
            _panelNumberDefectiveComponents += boardSummary.getNumberDefectiveComponents();
            _panelNumberProcessedJoints += boardSummary.getNumberProcessedJoints();
            _panelNumberDefectiveJoints += boardSummary.getNumberDefectiveJoints();
        }
        boardXMLWriter.close();
      }

      DateFormat myformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz");
      _endDateTime = myformat.format(new java.util.Date(endDateTime));

      writePanelElement();

      if (_os != null)
        closePrintWriter();

      String tempConcatFile = FileName.getConcatenationIntermediateFileFullPath(_inspectionResultsDir);

      // if there are any board files, concatenate them
      //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
      if (_boardToBoardResultsFilePathMap != null && _boardToBoardResultsFilePathMap.keySet().size() > 0 
            && (testProgram.isRealignPerformed() == false || testProgram.getProject().getPanel().getPanelSettings().isPanelBasedAlignment()))
      {
        String tempFile;
        List<String> filePaths = new ArrayList<String>();
        filePaths.addAll(_boardToBoardResultsFilePathMap.values());
        String boardResultsFilePath = filePaths.get(0);
        for (int i = 1; i < filePaths.size(); i++)
        {
          tempFile = FileUtil.getTempFileName(_resultsFilePath + i);
          try
          {
            FileUtilAxi.concat(boardResultsFilePath, filePaths.get(i), tempFile);
            if (FileUtilAxi.exists(boardResultsFilePath)) 
              FileUtilAxi.delete(boardResultsFilePath);
            if (FileUtilAxi.exists(filePaths.get(i))) 
              FileUtilAxi.delete(filePaths.get(i));
          }
          catch (CannotDeleteFileDatastoreException ex)
          {
            _exceptionThrown = true;
            throw ex;
          }
          catch (FileNotFoundDatastoreException ex)
          {
            _exceptionThrown = true;
            throw ex;
          }
          catch (CannotReadDatastoreException ex)
          {
            _exceptionThrown = true;
            throw ex;
          }
          finally
          {
            if (_exceptionThrown)
            {
              if (FileUtilAxi.exists(tempFile))
                FileUtilAxi.delete(tempFile);
            }
          }

          boardResultsFilePath = tempFile;
        }

        // now concatenate the top part of the final indictments.xml file with the file containting all the board results
        FileUtilAxi.concat(_tempResultsFilePath, boardResultsFilePath, tempConcatFile);
        if (FileUtilAxi.exists(boardResultsFilePath))
          FileUtilAxi.delete(boardResultsFilePath);
      }
      else // otherwise, just rename the exising file to the intermediate file
      {
        if (FileUtilAxi.exists(_tempResultsFilePath))
          FileUtilAxi.rename(_tempResultsFilePath, tempConcatFile);
        else
        {
          FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_tempResultsFilePath);
          throw dex;
        }
      }

      // finish writing out the bottom part of the indictments.xml
      _os = new PrintWriter(_tempResultsFilePath);
      writeTopLevelElementEnding();

      if (_os != null)
        closePrintWriter();

      // one last concatenation for the very last element closing
      FileUtilAxi.concat(tempConcatFile, _tempResultsFilePath, _resultsFilePath);

      try
      {
        // clean up all leftover temp files
        if (FileUtilAxi.exists(_tempResultsFilePath))
          FileUtilAxi.delete(_tempResultsFilePath);
        if (FileUtilAxi.exists(tempConcatFile))
          FileUtilAxi.delete(tempConcatFile);
        // copy the cad and settings xml files into the results directory
        if (FileUtil.exists(_cadFileInResultsDirFullPath) == false)
          FileUtilAxi.copy(_cadFilePath, _cadFileInResultsDirFullPath);
        if (FileUtil.exists(_settingsFileInResultsDirFullPath) == false)
          FileUtilAxi.copy(_settingsFilePath, _settingsFileInResultsDirFullPath);
        // copy the schema files in the inspectionRun directory
        FileUtilAxi.copy(_cadXmlSchemaFilePath, _cadXmlSchemaInResultsDirFilePath);
        FileUtilAxi.copy(_resultsXmlSchemaFilePath, _resultsXmlSchemaInResultsDirFilePath);
        FileUtilAxi.copy(_settingsXmlSchemaFilePath, _settingsXmlSchemaInResultsDirFilePath);
      }
      catch (CannotDeleteFileDatastoreException ex)
      {
        _exceptionThrown = true;
        throw ex;
      }
      catch (CannotCopyFileDatastoreException ex)
      {
        _exceptionThrown = true;
        throw ex;
      }
      catch (FileNotFoundDatastoreException ex)
      {
        _exceptionThrown = true;
        throw ex;
      }
    }
    catch (FileNotFoundException ex)
    {
      _exceptionThrown = true;
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_tempResultsFilePath);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      TestProgram testProgram = InspectionResultsXMLWriter.getInstance().getProject().getTestProgram();  
      
      // close the file in case an exception was thrown before the file was closed
      if (_os != null)
        closePrintWriter();

      //clear up internal structures
      //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
      //Don't do the clearing if manual realignment is performed
      if(testProgram.isRealignPerformed() == false || testProgram.getProject().getPanel().getPanelSettings().isPanelBasedAlignment())
      {        
        _boardToBoardXMLWriterMap.clear();
        _boardToBoardXMLWriterMap = null;
        _boardToBoardResultsFilePathMap.clear();
        _boardToBoardResultsFilePathMap = null;
        _boardWriterToBoardSummary.clear();
        _jointResultToRepairImageNameAndSliceNameOffsetListMap.clear();
        _jointResultToRepairImageNameAndSliceNameOffsetListMap = null;
        _componentResultToRepairImageNameAndSliceNameOffsetListMap.clear();
        _componentResultToRepairImageNameAndSliceNameOffsetListMap = null;
        _boardToTempIndictmentFileName.clear();      

        _project = null;
      }

      // reset counters
      _panelNumberOfIndictments = 0;
      _panelNumberProcessedComponents = 0;
      _panelNumberDefectiveComponents = 0;
      _panelNumberProcessedJoints = 0;
      _panelNumberDefectiveJoints = 0;

      // reset exception flag
      _exceptionThrown = false;
    }
  }

  /**
   * This method is called only for failed joints that have been re-tested
   *
   * @author Laura Cormos
   * @author Kee Chin Seong - Adding the Width Height to make the image name unique
   */
  public void addSliceOffsetToJoint(JointInspectionResult jointInspResult, ReconstructedImages images, int offset)
  {
    Assert.expect(jointInspResult != null);
    Assert.expect(images != null);

    List<Pair<Integer,Pair<String,String>>> imageNameAndSliceNameOffsetList;
    if (_jointResultToRepairImageNameAndSliceNameOffsetListMap.containsKey(jointInspResult))
      imageNameAndSliceNameOffsetList = _jointResultToRepairImageNameAndSliceNameOffsetListMap.get(jointInspResult);
    else
      imageNameAndSliceNameOffsetList = new ArrayList<Pair<Integer,Pair<String,String>>>();

    ReconstructionRegion reconstructionRegion = images.getReconstructionRegion();
    PanelRectangle panelRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    for (ReconstructedSlice reconImageSlice : images.getReconstructedSlices())
    {
      String imageName = FileName.getResultImageName(reconstructionRegion.isTopSide(),
                                                     panelRect.getMinX(),
                                                     panelRect.getMinY(),
                                                     panelRect.getWidth(),
                                                     panelRect.getHeight(),
                                                     reconImageSlice.getSliceNameEnum().getId(),
                                                     offset);
      String imageSliceEnumName = reconImageSlice.getSliceNameEnum().getName();
      imageNameAndSliceNameOffsetList.add(new Pair<Integer,Pair<String, String>>(offset,
                                                                                 new Pair<String, String>(imageName, imageSliceEnumName)));
    }

    _jointResultToRepairImageNameAndSliceNameOffsetListMap.put(jointInspResult, imageNameAndSliceNameOffsetList);
  }

  /**
   * This method is called only for joints that have set save diagnostic repair image
   *
   * @author Wei Chin
   * @author Kee Chin Seong - Adding the Width Height to make the image name unique
   */
  public void addDiagnosticSliceToJoint(JointInspectionResult jointInspResult, ReconstructedImages images)
  {
    Assert.expect(jointInspResult != null);
    Assert.expect(images != null);

    List<Pair<Integer,Pair<String,String>>> imageNameAndSliceNameOffsetList;
    if (_jointResultToRepairImageNameAndSliceNameOffsetListMap.containsKey(jointInspResult))
      imageNameAndSliceNameOffsetList = _jointResultToRepairImageNameAndSliceNameOffsetListMap.get(jointInspResult);
    else
      imageNameAndSliceNameOffsetList = new ArrayList<Pair<Integer,Pair<String,String>>>();

    ReconstructionRegion reconstructionRegion = images.getReconstructionRegion();
    PanelRectangle panelRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    if(images.hasReconstructedSlice(SliceNameEnum.DIAGNOSTIC_PAD_SLICE))
    {
      String imageName = FileName.getResultImageName(reconstructionRegion.isTopSide(),
                                                     panelRect.getMinX(),
                                                     panelRect.getMinY(),
                                                     panelRect.getWidth(),
                                                     panelRect.getHeight(),
                                                     SliceNameEnum.DIAGNOSTIC_PAD_SLICE.getId(),
                                                     0);
      String imageSliceEnumName = SliceNameEnum.DIAGNOSTIC_PAD_SLICE.getName();
      imageNameAndSliceNameOffsetList.add(new Pair<Integer,Pair<String, String>>(0,
                                                                                 new Pair<String, String>(imageName, imageSliceEnumName)));
    }
    if(images.hasReconstructedSlice(SliceNameEnum.DIAGNOSTIC_PACKAGE_SLICE))
    {
      String imageName = FileName.getResultImageName(reconstructionRegion.isTopSide(),
                                                     panelRect.getMinX(),
                                                     panelRect.getMinY(),
                                                     panelRect.getWidth(),
                                                     panelRect.getHeight(),
                                                     SliceNameEnum.DIAGNOSTIC_PACKAGE_SLICE.getId(),
                                                     0);
      String imageSliceEnumName = SliceNameEnum.DIAGNOSTIC_PACKAGE_SLICE.getName();
      imageNameAndSliceNameOffsetList.add(new Pair<Integer,Pair<String, String>>(0,
                                                                                 new Pair<String, String>(imageName, imageSliceEnumName)));
    }
    if(images.hasReconstructedSlice(SliceNameEnum.DIAGNOSTIC_MIDBALL_SLICE))
    {
      String imageName = FileName.getResultImageName(reconstructionRegion.isTopSide(),
                                                     panelRect.getMinX(),
                                                     panelRect.getMinY(),
                                                     panelRect.getWidth(),
                                                     panelRect.getHeight(),
                                                     SliceNameEnum.DIAGNOSTIC_MIDBALL_SLICE.getId(),
                                                     0);
      String imageSliceEnumName = SliceNameEnum.DIAGNOSTIC_MIDBALL_SLICE.getName();
      imageNameAndSliceNameOffsetList.add(new Pair<Integer,Pair<String, String>>(0,
                                                                                 new Pair<String, String>(imageName, imageSliceEnumName)));
    }
    if(images.hasReconstructedSlice(SliceNameEnum.DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
      String imageName = FileName.getResultImageName(reconstructionRegion.isTopSide(),
                                                     panelRect.getMinX(),
                                                     panelRect.getMinY(),
                                                     panelRect.getWidth(),
                                                     panelRect.getHeight(),
                                                     SliceNameEnum.DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_1.getId(),
                                                     0);
      String imageSliceEnumName = SliceNameEnum.DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_1.getName();
      imageNameAndSliceNameOffsetList.add(new Pair<Integer,Pair<String, String>>(0,
                                                                                 new Pair<String, String>(imageName, imageSliceEnumName)));
    }
    if(images.hasReconstructedSlice(SliceNameEnum.DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
      String imageName = FileName.getResultImageName(reconstructionRegion.isTopSide(),
                                                     panelRect.getMinX(),
                                                     panelRect.getMinY(),
                                                     panelRect.getWidth(),
                                                     panelRect.getHeight(),
                                                     SliceNameEnum.DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_2.getId(),
                                                     0);
      String imageSliceEnumName = SliceNameEnum.DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_2.getName();
      imageNameAndSliceNameOffsetList.add(new Pair<Integer,Pair<String, String>>(0,
                                                                                 new Pair<String, String>(imageName, imageSliceEnumName)));
    }
    if(images.hasReconstructedSlice(SliceNameEnum.DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
      String imageName = FileName.getResultImageName(reconstructionRegion.isTopSide(),
                                                     panelRect.getMinX(),
                                                     panelRect.getMinY(),
                                                     panelRect.getWidth(),
                                                     panelRect.getHeight(),
                                                     SliceNameEnum.DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_3.getId(),
                                                     0);
      String imageSliceEnumName = SliceNameEnum.DIAGNOSTIC_GRID_ARRAY_USER_DEFINED_SLICE_3.getName();
      imageNameAndSliceNameOffsetList.add(new Pair<Integer,Pair<String, String>>(0,
                                                                                 new Pair<String, String>(imageName, imageSliceEnumName)));
    }
    if(images.hasReconstructedSlice(SliceNameEnum.DIAGNOSTIC_USER_DEFINED_SLICE_1))
    {
      String imageName = FileName.getResultImageName(reconstructionRegion.isTopSide(),
                                                     panelRect.getMinX(),
                                                     panelRect.getMinY(),
                                                     panelRect.getWidth(),
                                                     panelRect.getHeight(),
                                                     SliceNameEnum.DIAGNOSTIC_USER_DEFINED_SLICE_1.getId(),
                                                     0);
      String imageSliceEnumName = SliceNameEnum.DIAGNOSTIC_USER_DEFINED_SLICE_1.getName();
      imageNameAndSliceNameOffsetList.add(new Pair<Integer,Pair<String, String>>(0,
                                                                                 new Pair<String, String>(imageName, imageSliceEnumName)));
    }
    if(images.hasReconstructedSlice(SliceNameEnum.DIAGNOSTIC_USER_DEFINED_SLICE_2))
    {
      String imageName = FileName.getResultImageName(reconstructionRegion.isTopSide(),
                                                     panelRect.getMinX(),
                                                     panelRect.getMinY(),
                                                     panelRect.getWidth(),
                                                     panelRect.getHeight(),
                                                     SliceNameEnum.DIAGNOSTIC_USER_DEFINED_SLICE_2.getId(),
                                                     0);
      String imageSliceEnumName = SliceNameEnum.DIAGNOSTIC_USER_DEFINED_SLICE_2.getName();
      imageNameAndSliceNameOffsetList.add(new Pair<Integer,Pair<String, String>>(0,
                                                                                 new Pair<String, String>(imageName, imageSliceEnumName)));
    }
    if(images.hasReconstructedSlice(SliceNameEnum.DIAGNOSTIC_FOREIGN_INCLUSION))
    {
      String imageName = FileName.getResultImageName(reconstructionRegion.isTopSide(),
                                                     panelRect.getMinX(),
                                                     panelRect.getMinY(),
                                                     panelRect.getWidth(),
                                                     panelRect.getHeight(),                                                     
                                                     SliceNameEnum.DIAGNOSTIC_FOREIGN_INCLUSION.getId(),
                                                     0);
      String imageSliceEnumName = SliceNameEnum.DIAGNOSTIC_FOREIGN_INCLUSION.getName();
      imageNameAndSliceNameOffsetList.add(new Pair<Integer,Pair<String, String>>(0,
                                                                                 new Pair<String, String>(imageName, imageSliceEnumName)));
    }
    //XCR-3264, Diagnostic Image for Tombstone is not able to send to VVTS 
    if(images.hasReconstructedSlice(SliceNameEnum.DIAGNOSTIC_CLEAR_CHIP_SLICE))
    {
      String imageName = FileName.getResultImageName(reconstructionRegion.isTopSide(),
                                                     panelRect.getMinX(),
                                                     panelRect.getMinY(),
                                                     panelRect.getWidth(),
                                                     panelRect.getHeight(),                                                     
                                                     SliceNameEnum.DIAGNOSTIC_CLEAR_CHIP_SLICE.getId(),
                                                     0);
      String imageSliceEnumName = SliceNameEnum.DIAGNOSTIC_CLEAR_CHIP_SLICE.getName();
      imageNameAndSliceNameOffsetList.add(new Pair<Integer,Pair<String, String>>(0,
                                                                                 new Pair<String, String>(imageName, imageSliceEnumName)));
    }

    _jointResultToRepairImageNameAndSliceNameOffsetListMap.put(jointInspResult, imageNameAndSliceNameOffsetList);
  }

  /**
   * This method is called only for joints that have set save enhanced image
   * @author Siew Yeng
   */
  public void addEnhancedImageSliceToJoint(JointInspectionResult jointInspResult, ReconstructedImages images)
  {
    Assert.expect(jointInspResult != null);
    Assert.expect(images != null);

    List<Pair<Integer,Pair<String,String>>> imageNameAndSliceNameOffsetList;
    if (_jointResultToRepairImageNameAndSliceNameOffsetListMap.containsKey(jointInspResult))
      imageNameAndSliceNameOffsetList = _jointResultToRepairImageNameAndSliceNameOffsetListMap.get(jointInspResult);
    else
      imageNameAndSliceNameOffsetList = new ArrayList<Pair<Integer,Pair<String,String>>>();

    ReconstructionRegion reconstructionRegion = images.getReconstructionRegion();
    PanelRectangle panelRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    
    for(ReconstructedSlice reconstructedSlice : images.getInspectedReconstructedSlices())
    {
      //Siew Yeng - XCR-2848 - add only inspected slice image
      SliceNameEnum enhancedImageSliceNameEnum = EnumToUniqueIDLookup.getInstance().getEnhancedImageSliceNameEnum(reconstructedSlice.getSliceNameEnum());
      if(images.hasReconstructedSlice(enhancedImageSliceNameEnum))
      {
        String imageName = FileName.getResultImageName(reconstructionRegion.isTopSide(),
                                                     panelRect.getMinX(),
                                                     panelRect.getMinY(),
                                                     panelRect.getWidth(),
                                                     panelRect.getHeight(),
                                                     enhancedImageSliceNameEnum.getId(),
                                                     0);
        String imageSliceEnumName = enhancedImageSliceNameEnum.getName();
        imageNameAndSliceNameOffsetList.add(new Pair<Integer,Pair<String, String>>(0,
                                                                                 new Pair<String, String>(imageName, imageSliceEnumName)));
      }
    }
    
    _jointResultToRepairImageNameAndSliceNameOffsetListMap.put(jointInspResult, imageNameAndSliceNameOffsetList);
  }

  /**
   * @author Laura Cormos
   * @author Patrick Lacz
   * @author Kee Chin Seong - Adding the Width Height to make the image name unique
   */
  public void writeJointInspectionResult(JointInspectionResult jointInspResult,
                                         ReconstructionRegion reconstructionRegion) throws DatastoreException
  {
    Assert.expect(jointInspResult != null);

    boolean jointPassed = jointInspResult.passed();
    Board board = jointInspResult.getJointInspectionData().getBoard();

    List<Pair<String, String>> repairImagePathsAndSliceNames = new ArrayList<Pair<String,String>>();
    String targetResultImagePath = null;
    String imageSliceEnumName = null;
    // if the joint failed, get the repair images paths since an image usually contains multiple joints
    if (jointPassed == false)
    {
      PanelRectangle panelRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
      String resultsImageDir = Directory.getInspectionResultsDir(_projName, _inspRunTimeStamp);

      for (Slice slice : reconstructionRegion.getInspectedAndMultiAngleSlices())
      {
        SliceNameEnum sliceNameEnum = slice.getSliceName();

        String targetResultImageName = FileName.getResultImageName(reconstructionRegion.isTopSide(),
                                                                   panelRect.getMinX(),
                                                                   panelRect.getMinY(),
                                                                   panelRect.getWidth(),
                                                                   panelRect.getHeight(),
                                                                   sliceNameEnum.getId());
        targetResultImagePath = resultsImageDir + File.separator + targetResultImageName;
        
        if(Config.getInstance().getBooleanValue(SoftwareConfigEnum.ADD_EXTRA_INFO_ON_SLICE_NAME_IN_OUTPUT_FILE) == true
           && (reconstructionRegion.getJointTypeEnum().equals(JointTypeEnum.PRESSFIT) ||
               reconstructionRegion.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE) ||
               reconstructionRegion.getJointTypeEnum().equals(JointTypeEnum.OVAL_THROUGH_HOLE)) && //Siew Yeng - XCR-3318 - Oval PTH
           sliceNameEnum.isMultiAngleSlice() == false)
        {
              imageSliceEnumName = UserDefinedSliceheights.getSliceNameForResult(sliceNameEnum, jointInspResult.getJointInspectionData().getSubtype());
        }
        else
          imageSliceEnumName = sliceNameEnum.getName();
        
        //If it is broken pin, add extra slice for its indicment
        boolean isBrokenPin = false;
        if (sliceNameEnum == SliceNameEnum.CAMERA_0)
        {
          Collection<JointMeasurement> jointMeasurementList = jointInspResult.getAllMeasurements();
          if(jointMeasurementList != null && jointMeasurementList.size() > 0)
          {
            for(JointMeasurement jointMeasurement : jointMeasurementList)
            {
              if (jointMeasurement.getMeasurementEnum() == MeasurementEnum.SHORT_BROKEN_PIN_AREA_SIZE)
              {
                //This broken pin slice shall share same SliceNameEnum number as pro_00
                isBrokenPin = true;
                break;
              }
            }
          }
        }
        
        if (isBrokenPin)
        {
          String newImageSliceEnumName = SliceNameEnum.FOREIGN_INCLUSION.getName();
          repairImagePathsAndSliceNames.add(new Pair<String, String>(targetResultImagePath, newImageSliceEnumName));
        }

        //If for broken pin case, this is to remain the original pro_00 slice
        repairImagePathsAndSliceNames.add(new Pair<String, String>(targetResultImagePath, imageSliceEnumName));
      }
    }

    ImageRectangle regionImageRectangle = reconstructionRegion.getRegionRectangleInPixels();
    int imageWidth = regionImageRectangle.getWidth();
    int imageHeight = regionImageRectangle.getHeight();

    BoardResultsXMLWriter boardXMLWriter = _boardToBoardXMLWriterMap.get(board);
    if (boardXMLWriter == null)
    {
      // this is the first time a joint from this board is coming through. Initialize the board xml writer
      boardXMLWriter = initializeBoardXmlWriter(board);
    }
    boardXMLWriter.writeJointInspectionResult(jointInspResult, repairImagePathsAndSliceNames, imageWidth, imageHeight);
  }


  /**
   * This method is called only for failed components that have been re-tested
   *
   * @author Laura Cormos
   * @author Kee Chin Seong - Adding the Width Height to make the image name unique
   */
  public void addSliceOffsetToComponent(ComponentInspectionResult compInspResult, ReconstructedImages images, int offset)
  {
    Assert.expect(compInspResult != null);
    Assert.expect(images != null);

    List<Pair<Integer,Pair<String,String>>> imageNameAndSliceNameOffsetList;
    if (_componentResultToRepairImageNameAndSliceNameOffsetListMap.containsKey(compInspResult))
      imageNameAndSliceNameOffsetList = _componentResultToRepairImageNameAndSliceNameOffsetListMap.get(compInspResult);
    else
      imageNameAndSliceNameOffsetList = new ArrayList<Pair<Integer,Pair<String,String>>>();

    ReconstructionRegion reconstructionRegion = images.getReconstructionRegion();
    PanelRectangle panelRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    for (ReconstructedSlice reconImageSlice : images.getReconstructedSlices())
    {
      String imageName = FileName.getResultImageName(reconstructionRegion.isTopSide(),
                                                     panelRect.getMinX(),
                                                     panelRect.getMinY(),
                                                     panelRect.getWidth(),
                                                     panelRect.getHeight(),
                                                     reconImageSlice.getSliceNameEnum().getId(),
                                                     offset);
      String imageSliceEnumName = reconImageSlice.getSliceNameEnum().getName();
      imageNameAndSliceNameOffsetList.add(new Pair<Integer, Pair<String, String>>(offset,
                                                                                  new Pair<String,String>(imageName,imageSliceEnumName)));
    }

    _componentResultToRepairImageNameAndSliceNameOffsetListMap.put(compInspResult, imageNameAndSliceNameOffsetList);
  }

  /**
   * @author Laura Cormos
   * @author Peter Esbensen
   * @author Patrick Lacz
   * @Edited by Siew Yeng
   * @author Kee Chin Seong - Adding the Width Height to make the image name unique
   */
  public void writeComponentInspectionResult(ComponentInspectionResult componentInspectionResult,
                                             List<ReconstructionRegion> reconstructionRegionList) throws DatastoreException
  {
    Assert.expect(componentInspectionResult != null);
    Assert.expect(reconstructionRegionList != null);

    Board board = componentInspectionResult.getComponentInspectionData().getComponent().getBoard();

    List<Pair<String, String>> repairImagePathsAndSliceNames = new ArrayList<Pair<String,String>>();
    String imagePath = null;
    String imageSliceEnumName = null;
    // if the component failed, get the repair image path
    if (componentInspectionResult.passed() == false)
    {
      //Siew Yeng - XCR-2141 - save all region image when fail component
      for(ReconstructionRegion reconstructionRegion : reconstructionRegionList)
      {
        PanelRectangle panelRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
        String resultsImageDir = Directory.getInspectionResultsDir(_projName, _inspRunTimeStamp);
        for (Slice slice : reconstructionRegion.getInspectedAndMultiAngleSlices())
        {
          SliceNameEnum sliceNameEnum = slice.getSliceName();

          String imageName = FileName.getResultImageName(reconstructionRegion.isTopSide(),
                                                         panelRect.getMinX(),
                                                         panelRect.getMinY(),
                                                         panelRect.getWidth(),
                                                         panelRect.getHeight(),
                                                         sliceNameEnum.getId());
          imagePath = resultsImageDir + File.separator + imageName;
          imageSliceEnumName = sliceNameEnum.getName();
          repairImagePathsAndSliceNames.add(new Pair<String, String>(imagePath, imageSliceEnumName));
        }
      }
    }

    ImageRectangle regionImageRectangle = reconstructionRegionList.get(0).getRegionRectangleInPixels();
    int imageWidth = regionImageRectangle.getWidth();
    int imageHeight = regionImageRectangle.getHeight();

    BoardResultsXMLWriter boardXMLWriter = _boardToBoardXMLWriterMap.get(board);
    if (boardXMLWriter == null)
    {
      // this is the first time a component from this board is coming through. Initialize the board xml writer
      boardXMLWriter = initializeBoardXmlWriter(board);
    }
    boardXMLWriter.writeComponentInspectionResult(componentInspectionResult, repairImagePathsAndSliceNames, imageWidth, imageHeight);
  }

  /**
   * @author Peter Esbensen
   */
  private BoardResultsXMLWriter initializeBoardXmlWriter(Board board) throws DatastoreException
  {
    Assert.expect(board != null);
    BoardResultsXMLWriter boardXMLWriter = new BoardResultsXMLWriter();
    _boardToBoardXMLWriterMap.put(board, boardXMLWriter);
    String boardXMLresultsFilePath = FileName.getXMLBoardResultsFileFullPath(_boardToTempIndictmentFileName.get(board),
                                                                             _projName, _inspRunTimeStamp);
    _boardToBoardResultsFilePathMap.put(board, boardXMLresultsFilePath);
    boardXMLWriter.open(board, boardXMLresultsFilePath, _boardNameToBoardSerialNumMap.get(board.getName()), _indent, _printSpaces);
    return boardXMLWriter;
  }
  
  /*
   * @author Kee Chin Seong - For X-out board Skip Serial Number
   */
  public void finishRemainingBoards() throws DatastoreException
  {
    Map<String, String> boardNameToBoardSerialNumMap = new HashMap<String, String>(_boardNameToBoardSerialNumMap);    
    for(String board : boardNameToBoardSerialNumMap.keySet())
    {
      BoardResultsXMLWriter boardXMLWriter = _boardToBoardXMLWriterMap.get(_project.getPanel().getBoard(board));
      if (boardXMLWriter == null)
      {
        // this is the first time a component from this board is coming through. Initialize the board xml writer
        boardXMLWriter = initializeBoardXmlWriter(_project.getPanel().getBoard(board));
        boolean isXout = _project.getPanel().getBoard(board).isXedOut();
        boardXMLWriter.setXoutBoard();
        if (isXout == false)
        {
          if (isBypassWithResultsMode())
          {
            boardXMLWriter.getBoard().setXoutReason(StringLocalizer.keyToString("SKIP_DUE_TO_BYPASS_MODE_KEY"));
          }
          else if (isSamplingMode())
          {
            boardXMLWriter.getBoard().setXoutReason(StringLocalizer.keyToString("SKIP_DUE_TO_SAMPLING_MODE_KEY"));
          }
          else
          {
            boardXMLWriter.getBoard().setXoutReason(StringLocalizer.keyToString("XOUT_DUE_TO_SKIP_SERIAL_NUMBER_KEY"));
          }
        }
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  private void writeTopLevelElementBeginning() throws DatastoreException
  {
    Assert.expect(_os != null);
    Assert.expect(_indent >= 0);

    StringBuilder spaces = new StringBuilder("");
    _os.println("<inspectionResults");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    _os.println(spaces + "schemaName=" + _dbQuote + FileName.getInspectionIndictmentsXmlSchemaFileName() + _dbQuote);
    _os.println(spaces + "dateAndTime=" + _dbQuote + _startDateTime + _dbQuote);
    _os.println(spaces + "loginName=" + _dbQuote + UserAccountsManager.getCurrentUserLoginName() + _dbQuote);
    if (isProductionMode())
      _os.println(spaces + "mode=" + _dbQuote + "test execution" + _dbQuoteGt);
    else
      _os.println(spaces + "mode=" + _dbQuote + "demo" + _dbQuoteGt);

    ++_indent;
    writeSettingsFileElement();
    writeCadFileElement();
    writeXrayTesterElement();
    writeProjectElement();

  }

  /**
   * @author Laura Cormos
   */
  private void writeTopLevelElementEnding()
  {
    Assert.expect(_os != null);

    _os.println("</inspectionResults>");
  }

  /**
   * @author Laura Cormos
   */
  private void writeSettingsFileElement()
  {
    Assert.expect(_os != null);
    Assert.expect(_indent >= 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (_indent * _numOfSpaces); i++)
        spaces.append(" ");
    _os.println(spaces + "<settingsFile");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    Assert.expect(_settingsFilePath != null);
    _os.println(spaces + "name=" + _dbQuote + _settingsFileInResultsDirName + _dbQtElemEnd);
  }

  /**
   * @author Laura Cormos
   */
  private void writeCadFileElement()
  {
    Assert.expect(_os != null);
    Assert.expect(_indent >= 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (_indent * _numOfSpaces); i++)
        spaces.append(" ");
    _os.println(spaces + "<cadFile");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    Assert.expect(_settingsFilePath != null);
    _os.println(spaces + "name=" + _dbQuote + _cadFileInResultsDirName + _dbQtElemEnd);
  }

  /**
   * @author Laura Cormos
   */
  private void writeXrayTesterElement()
  {
//    <xrayTester serialNumber="300" controllerHostName="mtdlaura" controllerIPAddress="192.168.1.1" />
    Assert.expect(_os != null);
    Assert.expect(_indent >= 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (_indent * _numOfSpaces); i++)
        spaces.append(" ");
    _os.println(spaces + "<xrayTester");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    Assert.expect(_settingsFilePath != null);

    _os.println(spaces + "serialNumber=" + _dbQuote + XrayTester.getInstance().getSerialNumber() + _dbQuote);
    try
    {
      String hostIP = InetAddress.getLocalHost().toString();
      String hostname = InetAddress.getLocalHost().getHostName();
      String IP = hostIP.substring(hostname.length() + 1);
      _os.println(spaces + "controllerHostName=" + _dbQuote + hostname + _dbQuote);
      _os.println(spaces + "controllerIPAddress=" + _dbQuote + IP + _dbQtElemEnd);
    }
    catch (UnknownHostException ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Laura Cormos
   */
  private void writeProjectElement()
  {
    Assert.expect(_os != null);
    Assert.expect(_indent >= 0);

    String variationName = _project.getSelectedVariationName();
    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (_indent * _numOfSpaces); i++)
        spaces.append(" ");
    _os.println(spaces + "<project");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.TEST_EXEC_ALWAYS_DISPLAY_VARIATION_NAME))
    {
      if (variationName.equals(VariationSettingManager.ALL_LOADED) == false)
        _os.println(spaces + "name=" + _dbQuote + XMLWriterStringUtil.replaceIllegalStringChars(_projName) + _aliase + variationName + _dbQuote);
      else
        _os.println(spaces + "name=" + _dbQuote + XMLWriterStringUtil.replaceIllegalStringChars(_projName) + _dbQuote);
    }
    else
      _os.println(spaces + "name=" + _dbQuote + XMLWriterStringUtil.replaceIllegalStringChars(_projName) + _dbQuote);
    
    _os.println(spaces + "version=" + _dbQuote + _project.getVersion() + _dbQuote);
    _os.println(spaces + "softwareVersionDuringProjectCreation=" + _dbQuote + _project.getSoftwareVersionOfProjectLastSave() + _dbQuote);
    _os.println(spaces + "softwareVersionDuringFileCreation=" + _dbQuote + Version.getVersionNumber() + _dbQtElemEnd);
  }

  /**
   * Writes the panel summary for the indictments xml file
   * <panel serialNumber="3 ab2" indictments="30" components="123" testedComponents="121"
   *       defectiveComponents="0" joints="5" testedJoints="5" defectiveJoints="5">
   * </panel>
   *
   * @author Laura Cormos
   */
  private void writePanelElement() throws DatastoreException
  {
    Assert.expect(_indent >= 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (_indent * _numOfSpaces); i++)
        spaces.append(" ");
    _os.println(spaces + "<panel");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    Panel panel = _project.getPanel();

    _os.println(spaces + "tested=" + _dbQuote + _isPanelTestedReason + _dbQuote);
    _os.println(spaces + "serialNumber=" + _dbQuote + XMLWriterStringUtil.replaceIllegalStringChars(_panelSerialNumber) + _dbQuote);
    _os.println(spaces + "indictments=" + _dbQuote + _panelNumberOfIndictments + _dbQuote);
    _os.println(spaces + "components=" + _dbQuote + panel.getNumComponents() + _dbQuote);
    _os.println(spaces + "testedComponents=" + _dbQuote + _panelNumberProcessedComponents + _dbQuote);
    _os.println(spaces + "defectiveComponents=" + _dbQuote + _panelNumberDefectiveComponents + _dbQuote);
    _os.println(spaces + "joints=" + _dbQuote + panel.getNumJoints() + _dbQuote);
    _os.println(spaces + "testedJoints=" + _dbQuote + _panelNumberProcessedJoints + _dbQuote);
    _os.println(spaces + "defectiveJoints=" + _dbQuote + _panelNumberDefectiveJoints + _dbQuoteGt);

    ++_indent;
    writeInspStartTimeElement(_indent);
    writeInspEndTimeElement(_indent);

    String endElemSpaces = new String("");
    if (_printSpaces)
      endElemSpaces = spaces.substring(0, spaces.length() - _numOfSpaces);
    _os.println(endElemSpaces + "</panel>");
  }

  /**
   * @author Laura Cormos
   */
  private void writeInspStartTimeElement(int indent)
  {
    Assert.expect(indent >= 0);
//  <inspectionStartTime timeZone="MST" year="2006" month="01" day="23" hour="12" minute="23" second="01" />

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    _os.println(spaces + "<inspectionStartTime");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    _os.println(spaces + "timeZone=" + _dbQuote + _startDateTime.substring(20) + _dbQuote);
    _os.println(spaces + "year=" + _dbQuote + _startDateTime.substring(0, 4) + _dbQuote);
    _os.println(spaces + "month=" + _dbQuote + _startDateTime.substring(5, 7) + _dbQuote);
    _os.println(spaces + "day=" + _dbQuote + _startDateTime.substring(8, 10) + _dbQuote);
    _os.println(spaces + "hour=" + _dbQuote + _startDateTime.substring(11, 13) + _dbQuote);
    _os.println(spaces + "minute=" + _dbQuote + _startDateTime.substring(14, 16) + _dbQuote);
    _os.println(spaces + "second=" + _dbQuote + _startDateTime.substring(17, 19) + _dbQtElemEnd);

  }

  /**
   * Writes inspection end time element
   * <inspectionStopTime timeZone="MST" year="2006" month="01" day="23" hour="12" minute="23" second="01" />
   *
   * @author Laura Cormos
   */
  private void writeInspEndTimeElement(int indent)
  {
    Assert.expect(indent >= 0);
    Assert.expect(_endDateTime != null);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    _os.println(spaces + "<inspectionStopTime");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    _os.println(spaces + "timeZone=" + _dbQuote + _startDateTime.substring(20) + _dbQuote);
    _os.println(spaces + "year=" + _dbQuote + _endDateTime.substring(0, 4) + _dbQuote);
    _os.println(spaces + "month=" + _dbQuote + _endDateTime.substring(5, 7) + _dbQuote);
    _os.println(spaces + "day=" + _dbQuote + _endDateTime.substring(8, 10) + _dbQuote);
    _os.println(spaces + "hour=" + _dbQuote + _endDateTime.substring(11, 13) + _dbQuote);
    _os.println(spaces + "minute=" + _dbQuote + _endDateTime.substring(14, 16) + _dbQuote);
    _os.println(spaces + "second=" + _dbQuote + _endDateTime.substring(17, 19) + _dbQtElemEnd);

  }

  /**
   * @author Laura Cormos
   */
  public void setXedOut(Board board)
  {
    Assert.expect(board != null);

    BoardResultsXMLWriter boardXMLWriter = _boardToBoardXMLWriterMap.get(board);
    if (boardXMLWriter != null)
      boardXMLWriter.setXoutBoard();
    else
      Assert.expect(false, "Received Xout command for board \"" + board.getName() + "\" before receiving any joint results!! Something is not right!");
  }

  /**
   * @author Laura Cormos
   */
  public void setProductionMode(boolean productionMode)
  {
    if (_productionMode == null)
      _productionMode = new BooleanRef(productionMode);
    else
      _productionMode.setValue(productionMode);
  }

  /**
   * @author Bill Darbie
   */
  private boolean isProductionMode()
  {
    Assert.expect(_productionMode != null);
    return _productionMode.getValue();
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setBypassWithResultsMode(boolean bypassWithResultsMode)
  {
    _bypassWithResultsMode = bypassWithResultsMode;
  }

  /**
   * @author Kok Chun, Tan
   */
  private boolean isBypassWithResultsMode()
  {
    return _bypassWithResultsMode;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setSamplingMode(boolean samplingMode)
  {
    _samplingMode = samplingMode;
  }

  /**
   * @author Kok Chun, Tan
   */
  private boolean isSamplingMode()
  {
    return _samplingMode;
  }

  /**
   * @author Laura Cormos
   */
  List<Pair<Integer,Pair<String, String>>> getAdditionalImagesForJoint(JointInspectionResult jointInspResult)
  {
    Assert.expect(jointInspResult != null);
    Assert.expect(_jointResultToRepairImageNameAndSliceNameOffsetListMap != null);

    List<Pair<Integer,Pair<String, String>>> listToReturn = _jointResultToRepairImageNameAndSliceNameOffsetListMap.get(jointInspResult);
    if (listToReturn == null)
      return new ArrayList<Pair<Integer,Pair<String,String>>>();

    return listToReturn;
  }

  /**
   * @author Laura Cormos
   */
  List<Pair<Integer,Pair<String, String>>> getAdditionalImagesForComponent(ComponentInspectionResult compInspResult)
  {
    Assert.expect(compInspResult != null);
    Assert.expect(_componentResultToRepairImageNameAndSliceNameOffsetListMap != null);

    List<Pair<Integer,Pair<String, String>>> listToReturn = _componentResultToRepairImageNameAndSliceNameOffsetListMap.get(compInspResult);
    if (listToReturn == null)
      return new ArrayList<Pair<Integer,Pair<String,String>>>();

    return listToReturn;
  }

  /**
   * @author Laura Cormos
   */
  Project getProject()
  {
    Assert.expect(_project != null);
    return _project;
  }

  /**
   * @author Laura Cormos
   */
  void closePrintWriter() throws DatastoreException
  {
    Assert.expect(_os != null);

    try
    {
      _os.flush();
      _os.close();
      _os = null;
    }
    catch (IllegalStateException ex)
    {
      // this can happen if the network connection is suddenly severed
      CannotWriteDatastoreException dex = new CannotWriteDatastoreException(_tempResultsFilePath);
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * @author Roy Williams
   */
  public void clearProjectInfo()
  {
    _project = null;
  }

  /**
   * @author Laura Cormos
   */
  void decideBoardXoutStatus(BoardResultsXMLWriter boardXMLWriter)
  {
    Assert.expect(boardXMLWriter != null);
    //if (boardIsXedout(boardXMLWriter))
    //Chin Seong :: Add XedOut for the board
    if(boardXMLWriter.getBoard().isXedOut()) //boardXMLWriter.getBoard().isXedOut() &&
      boardXMLWriter.setXoutBoard();

   // setXedOut(board);
  }

  /**
   * @author Laura Cormos
   */
  private boolean boardIsXedout(BoardResultsXMLWriter currentBoardXMLWriter)
  {
    Assert.expect(currentBoardXMLWriter != null);
    
    BoardResultsSummary currentBoardSummary = currentBoardXMLWriter.getBoardResultsSummary();
    int xoutThresholdValue = TestExecution.getInstance().getXoutDetectionThreshold(); // customer setting in config
    int numTotalInspectedJoints = currentBoardXMLWriter.getBoard().getNumInspectedJoints();
    int minNumFailingJointsForXout = numTotalInspectedJoints * xoutThresholdValue / 100; // threshold % of total inspected joints

    if (currentBoardSummary.getNumberDefectiveJoints() <= minNumFailingJointsForXout)
      return false;

    // this board IS a candidate for being xed out, as it's over the threshold.  Now, check with all boards of the same
    // boardType to make sure if they're all over the limit, this board is NOT Xed out.
    for (BoardResultsXMLWriter boardXMLWriter : _boardToBoardXMLWriterMap.values())
    {
      BoardResultsSummary boardSummaryUnderConsideration = boardXMLWriter.getBoardResultsSummary();
      BoardType boardType = boardXMLWriter.getBoard().getBoardType();
      if (currentBoardXMLWriter.getBoard().getBoardType().equals(boardType))
      {
        if (boardSummaryUnderConsideration.getNumberDefectiveJoints() < minNumFailingJointsForXout)
          return true;  // at least one board was UNDER the limit, so the currentBoard is considered Xed out
      }
    }

    return false;    
  }
  
   /**
   * @author Jack Hwee
   */
  public String getCurrentXMLResultPath()
  {
    return _resultsFilePath;
  }
}
