package com.axi.v810.datastore.testResults;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Bill Darbie
 */
public class PanelResults
{
  // serial number for production runs, description for test development runs
  private String _serialNumberOrDescription;
  private long _inspectionStartTimeInMillis = -1;
  private long _inspectionStopTimeInMillis = -1;
  private String _imageSetName;
  private BooleanRef _imagesCollectedOnline;
  private String _userDescription;
  private String _informationFlag = "";

  /**
   * @author Bill Darbie
   */
  void setSerialNumberOrDescription(String serialNumberOrDescription)
  {
    Assert.expect(serialNumberOrDescription != null);
    _serialNumberOrDescription = StringUtil.convertAcsiiStringToUnicodeString(serialNumberOrDescription);
  }

  /**
   * @author Laura Cormos
   */
  public String getSerialNumberOrDescription()
  {
    Assert.expect(_serialNumberOrDescription != null);
    return _serialNumberOrDescription;
  }

  /**
   * @author George Booth
   */
  void setUserDescription(String userDescription)
  {
    Assert.expect(userDescription != null);
    _userDescription = userDescription;
  }

  /**
   * @author George Booth
   */
  public String getImageSetDescription()
  {
    String imageSetDescription = "";
    if (_imageSetName != null && _imageSetName.length() > 0)
    {
      imageSetDescription = StringLocalizer.keyToString("RDGUI_IMAGE_SET_PREFIX_KEY") + " \"";
      try
      {
        // format the date and time for the image set in a short format
        long timeInMils = StringUtil.convertStringToTimeInMilliSecondsUsingDate(_imageSetName);
        
        // XCR-2837 Assert When Viewing Result in Fine Tuning - Cheah Lee Herng
        // Below is temporary assert so that it can display the image set name that cause negative time conversion.
        if (timeInMils < 0)
        {
          Assert.expect(false, "PanelResults - Time cannot be negative - ImageSetName: " + _imageSetName);
        }
        
        imageSetDescription += StringUtil.convertMilliSecondsToTimeAndDate(timeInMils);
        if (_userDescription.length() > 0)
        {
          imageSetDescription += " (" + _userDescription + ")\"";
        }
        else
        {
          imageSetDescription += "\"";
        }
      }
      catch(BadFormatException bfe)
      {
        // nothing the user can do to correct a badly formatted image set name
//        return "";
        Assert.expect(false, "Image Set name is not in date-time format");
      }
    }
    return imageSetDescription;
  }

  /**
   * @author Bill Darbie
   */
  void setInspectionStartTimeInMillis(long inspectionStartTimeInMillis)
  {
    _inspectionStartTimeInMillis = inspectionStartTimeInMillis;
  }

  /**
   * @author Bill Darbie
   */
  public long getInspectionStartTimeInMillis()
  {
    Assert.expect(_inspectionStartTimeInMillis != -1);
    return _inspectionStartTimeInMillis;
  }

  /**
   * @author Bill Darbie
   */
  void setInspectionStopTimeInMillis(long inspectionStopTimeInMillis)
  {
    _inspectionStopTimeInMillis = inspectionStopTimeInMillis;
  }

  /**
   * @author Bill Darbie
   */
  public long getInspectionStopTimeInMillis()
  {
    Assert.expect(_inspectionStopTimeInMillis != -1);
    return _inspectionStopTimeInMillis;
  }

  /**
   * @author Bill Darbie
   */
  public void setImageSetName(String imageSetName)
  {
    Assert.expect(imageSetName != null);
    _imageSetName = imageSetName;
  }

  /**
   * @author Bill Darbie
   */
  public String getImageSetName()
  {
    Assert.expect(_imageSetName != null);
    return _imageSetName;
  }

  /**
   * @author George Booth
   */
  public void setInformationFlag(String informationFlag)
  {
    Assert.expect(informationFlag != null);
    _informationFlag = informationFlag;
  }

  /**
   * @author George Booth
   */
  public String getInformationFlag()
  {
    Assert.expect(_informationFlag != null);
    return _informationFlag;
  }

  /**
   * @author Bill Darbie
   */
  public void setImagesCollectedOnline(boolean imagesGatheredOnline)
  {
    if (_imagesCollectedOnline == null)
      _imagesCollectedOnline = new BooleanRef(imagesGatheredOnline);
    else
      _imagesCollectedOnline.setValue(imagesGatheredOnline);
  }

  /**
   * @author Bill Darbie
   */
  public boolean wereImagesCollectedOnline()
  {
    Assert.expect(_imagesCollectedOnline != null);
    return _imagesCollectedOnline.getValue();
  }
}
