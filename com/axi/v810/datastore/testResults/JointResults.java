package com.axi.v810.datastore.testResults;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * @author Bill Darbie
 */
public class JointResults
{
  private JointName _jointName;
  private JointTypeEnum _jointTypeEnum;
  private String _subtypeShortName;
  private String _subtypeLongName;
  private boolean _pass;
  private boolean _isTopSide;

  private List<MeasurementResult> _allMeasurements = new ArrayList<MeasurementResult>();
  private List<MeasurementResult> _failingMeasurements = new ArrayList<MeasurementResult>();
  private List<MeasurementResult> _relatedMeasurements = new ArrayList<MeasurementResult>();

  private List<IndictmentResult> _indictmentResults = new ArrayList<IndictmentResult>();

  /**
   * @author Bill Darbie
   */
  void setJointName(JointName jointName)
  {
    Assert.expect(jointName != null);
    _jointName = jointName;
  }

  /**
   * @author Bill Darbie
   */
  public JointName getJointName()
  {
    Assert.expect(_jointName != null);
    return _jointName;
  }

  /**
   * @author Bill Darbie
   */
  void setJointTypeEnum(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);
    _jointTypeEnum = jointTypeEnum;
  }

  /**
   * @author Bill Darbie
   */
  public JointTypeEnum getJointTypeEnum()
  {
    Assert.expect(_jointTypeEnum != null);
    return _jointTypeEnum;
  }

  /**
   * @author Bill Darbie
   */
  void setSubtypeShortName(String subtypeShortName)
  {
    Assert.expect(subtypeShortName != null);
    _subtypeShortName = subtypeShortName;
  }

  /**
   * @author Bill Darbie
   */
  public String getSubtypeShortName()
  {
    Assert.expect(_subtypeShortName != null);
    return _subtypeShortName;
  }

  /**
   * @author Laura Cormos
   */
  void setSubtypeLongName(String subtypeLongName)
  {
    Assert.expect(subtypeLongName != null);
    _subtypeLongName = subtypeLongName;
  }

  /**
   * @author Laura Cormos
   */
  public String getSubtypeLongName()
  {
    Assert.expect(_subtypeLongName != null);
    return _subtypeLongName;
  }

  /**
   * @author Bill Darbie
   */
  void setPassed(boolean passed)
  {
    _pass = passed;
  }

  /**
   * @author Bill Darbie
   */
  public boolean passed()
  {
    return _pass;
  }

  /**
   * @author Bill Darbie
   */
  void setTopSide(boolean topSide)
  {
    _isTopSide = topSide;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isTopSide()
  {
    return _isTopSide;
  }

  /**
   * @author Bill Darbie
   */
  void addMeasurementResult(MeasurementResult measurementResult)
  {
    Assert.expect(measurementResult != null);
    _allMeasurements.add(measurementResult);
  }

  /**
   * @author Bill Darbie
   */
  public List<MeasurementResult> getAllMeasurementResults()
  {
    return _allMeasurements;
  }

  /**
   * @author Bill Darbie
   */
  void addFailingMeasurement(MeasurementResult measurementResult)
  {
    Assert.expect(measurementResult != null);
    _failingMeasurements.add(measurementResult);
  }

  /**
   * @author Bill Darbie
   */
  public List<MeasurementResult> getFailingMeasurementResults()
  {
    return _failingMeasurements;
  }

  /**
   * @author Bill Darbie
   */
  void addRelatedMeasurement(MeasurementResult measurementResult)
  {
    Assert.expect(measurementResult != null);
    _relatedMeasurements.add(measurementResult);
  }

  /**
   * @author Bill Darbie
   */
  public List<MeasurementResult> getRelatedMeasurementResults()
  {
    return _relatedMeasurements;
  }

  /**
   * @author Bill Darbie
   */
  void addIndictmentResult(IndictmentResult indictmentResult)
  {
    Assert.expect(indictmentResult != null);
    _indictmentResults.add(indictmentResult);
  }

  /**
   * @author Bill Darbie
   */
  public List<IndictmentResult> getIndictmentResults()
  {
    return _indictmentResults;
  }
}
