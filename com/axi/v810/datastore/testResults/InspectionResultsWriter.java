package com.axi.v810.datastore.testResults;

import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class writes out all results information into a binary file.  The format
 * is the following:
 *
 * Header section
 *   Header Record
 *     header record version (int)
 *     software version used to create this file (double)
 *     imageSetName (String)
 *     imageSetUserDescription (String)
 *     were images gathered online (boolean)
 *     version of the test program (int)
 *     inspection start time (long)
 *     inspection stop time (long)
 *
 *   numPadsAndComponents (in order that they appear later in the file)
 *     boardName refDes padName (string) OR  boardName refDes (string)
 *     jointOrComponent (boolean)  joint = true, component = false
 *     pass (boolean)
 *     uniqueId (int)
 *     number of bytes into file starting at the data section to get to that record (long)
 *
 *   numSubtypes (int)
 *     subtype name (string)
 *     numMeasurements (int)
 *       measurementEnumId (int)
 *         sliceEnumId (int)
 *
 * Data section
 *   Panel Record
 *     record version number (int)
 *     panel serial number (string)
 *
 *   Board Record
 *     record version number (int)
 *     numBoards (int)
 *     board name (string)
 *       xed out (boolean)
 *       serial number (string)
 *
 *   Component Record
 *     record version number (int)
 *     uniqueId (int)
 *     pass or fail (boolean)
 *     topSide (boolean)
 *     numMeasurements (int)
 *     measurement name (int)
 *       algorithm name (int)
 *       subtype short name (string)
 *       subtype long name (string)
 *       slice name (int )
 *       value (float)
 *       units (int)
 *     numIndictments (int)
 *     indictment name (int)
 *     algorithm name (int)
 *       numFailingMeasurements (int)
 *       list of failing measurement index
 *       numRelatedMeasurements (int)
 *       list of related measurement index
 *
 *   Joint Record
 *     record version number (int)
 *     uniqueId (int)
 *     pass or fail (boolean)
 *     topSide (boolean)
 *     subtype short name (string)
 *     subtype long name (string)
 *     joint type (int)
 *     numMeasurements (int)
 *     measurement name (int)
 *       algorithm name (int)
 *       slice name (int)
 *       value (float)
 *       units (int)
 *     numIndictments (int)
 *     indictment name (int)
 *     algorithm name (int)
 *       numFailingMeasurements (int)
 *         failing measurement index (int)
 *       numRelatedMeasurements (int)
 *         related measurement index (int)
 *
 * @author Bill Darbie
 */
public class InspectionResultsWriter
{
  private FileChannel _fileChannel;
  private int _bufSize = 8192;
  private ByteBuffer _byteBuffer = ByteBuffer.allocate(_bufSize);

  private Project _project;
  private String _panelSerialNumber;
  private Map<String, String> _boardNameToSerialNumberMap;
  private String _imageSetName;
  private String _imageSetUserDescription;
  private boolean _imagesCollectedOnline;
  private String _fileName;
  private String _tempFileName1;
  private String _tempFileName2;
  private String _currFileName;
  private long _inspectionStartTime;
  private long _inspectionStopTime;
  private Map<Long, Pair<String, Integer>> _filePositionToPadOrComponentNameAndIdMap = new TreeMap<Long, Pair<String, Integer>>();
  private Map<Long, Pair<Boolean, Boolean>> _filePositionToJointAndPassMap = new TreeMap<Long, Pair<Boolean, Boolean>>();
  private EnumToUniqueIDLookup _enumToIdLookup;
  private int _recordId = 0;
  private List<JointMeasurement> _jointMeasurements = new ArrayList<JointMeasurement>();
  private Map<Long, Integer> _jointMeasurementIdToIndex = new HashMap<Long, Integer>();
  private List<ComponentMeasurement> _componentMeasurements = new ArrayList<ComponentMeasurement>();
  private Map<Long, Integer> _componentMeasurementIdToIndex = new HashMap<Long, Integer>();
  private Map<String, List<Pair<MeasurementEnum, SliceNameEnum>>> _subtypeToMeasurementEnumAndSliceNameEnumPairListMap =
      new HashMap<String,List<Pair<MeasurementEnum, SliceNameEnum>>>();

  private static int _numBytesForInteger = Integer.SIZE / 8;
  private static int _numBytesForLong = Long.SIZE / 8;
  private static int _numBytesForFloat = Float.SIZE / 8;
  private static int _numBytesForDouble = Double.SIZE / 8;
  private static int _numBytesForByte = Byte.SIZE / 8;

  private TimerUtil _timerUtil = new TimerUtil();
  private Set<String> _padNameSet = new HashSet<String>();

  /**
   * @author Bill Darbie
   */
  public InspectionResultsWriter()
  {
    _enumToIdLookup = EnumToUniqueIDLookup.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    _filePositionToPadOrComponentNameAndIdMap.clear();
    _filePositionToJointAndPassMap.clear();
    _jointMeasurements.clear();

    _jointMeasurementIdToIndex.clear();
    _componentMeasurements.clear();
    _componentMeasurementIdToIndex.clear();

    clearSubtypeMeasurementSliceEnumDataStructures();

    _recordId = 0;
  }

  /**
   * @author Bill Darbie
   */
  public void open(Project project,
                   long inspectionStartTime,
                   String panelSerialNumber,
                   Map<String, String> boardNameToSerialNumberMap,
                   String imageSetName,
                   String imageSetUserDescription,
                   boolean imagesCollectedOnline) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(panelSerialNumber != null);
    Assert.expect(boardNameToSerialNumberMap != null);
    Assert.expect(imageSetName != null);

    _timerUtil.reset();
    _timerUtil.start();

    init();

    _project = project;
    _inspectionStartTime = inspectionStartTime;

    if(Localization.getLocale().equals(Locale.ENGLISH))
      _panelSerialNumber = panelSerialNumber;
    else
      _panelSerialNumber = StringUtil.convertUnicodeStringToAsciiString(panelSerialNumber);
    _boardNameToSerialNumberMap = boardNameToSerialNumberMap;
    _imageSetName = imageSetName;
    _imageSetUserDescription = imageSetUserDescription;
    _imagesCollectedOnline = imagesCollectedOnline;

    try
    {
      _byteBuffer.clear();

      String projName = project.getName();
      // create  the directory
      String dir = Directory.getInspectionResultsDir(projName, inspectionStartTime);
      FileUtilAxi.mkdirs(dir);
      // get the fileNames
      _fileName = FileName.getResultsFileFullPath(projName, inspectionStartTime);
      _tempFileName1 = _fileName + FileName.getTempFileExtension() + "1";
      _tempFileName2 = _fileName + FileName.getTempFileExtension() + "2";
      _currFileName = _tempFileName1;

      // set up the channel
      FileOutputStream fos = new FileOutputStream(_currFileName);
      _fileChannel = fos.getChannel();
    }
    catch (FileNotFoundException ex)
    {
      CannotCreateFileDatastoreException de = new CannotCreateFileDatastoreException(_currFileName);
      de.initCause(ex);
      throw de;
    }

    try
    {
      writeBoards();
    }
    catch (DatastoreException dex)
    {
      close();

      if (FileUtilAxi.exists(_currFileName))
        FileUtilAxi.delete(_currFileName);

      throw dex;
    }

    _timerUtil.stop();
    // now let the user of this API call writeJoint() and writeComponent() as the data comes in
  }

  /**
   * @author Bill Darbie
   */
  public void close(long inspectionStopTime) throws DatastoreException
  {
    if (_currFileName == null)
      return;

    _timerUtil.start();
    _inspectionStopTime = inspectionStopTime;

    // if _fileChannel is null then close was already called
    if (_fileChannel == null)
      return;
    try
    {
      // first close the temporary file that has been written
      flush();
      close();

      // now open a new file and write out the header to it.
      try
      {
        _currFileName = _tempFileName2;
        FileOutputStream fos = new FileOutputStream(_currFileName);
        _fileChannel = fos.getChannel();
      }
      catch (FileNotFoundException ex)
      {
        FileNotFoundDatastoreException de = new FileNotFoundDatastoreException(_currFileName);
        de.initCause(ex);
        throw de;
      }

      writeHeader();
      flush();
      close();

      // now append the temporary file to the header
      TimerUtil timerUtil = new TimerUtil();
      timerUtil.start();
      FileUtilAxi.concat(_tempFileName2, _tempFileName1, _fileName);
      timerUtil.stop();
//      if (UnitTest.unitTesting() == false)
//        System.out.println("wpd total concat time in milliseconds to write binary results file: " + timerUtil.getElapsedTimeInMillis());
    }
    finally
    {
      close();
      _project = null;
      _jointMeasurements.clear();
      _componentMeasurements.clear();

      if (FileUtilAxi.exists(_tempFileName1))
        FileUtilAxi.delete(_tempFileName1);

      if (FileUtilAxi.exists(_tempFileName2))
        FileUtilAxi.delete(_tempFileName2);

      init();

      _timerUtil.stop();
//      if (UnitTest.unitTesting() == false)
//        System.out.println("wpd total time in milliseconds to write binary results file: " + _timerUtil.getElapsedTimeInMillis());

      _fileName = null;
      _tempFileName1 = null;
      _tempFileName2 = null;
      _currFileName = null;
      //sham XCR 1449 - add for temporary fix result crash
      _padNameSet.clear();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void close()
  {
    try
    {
      if (_fileChannel != null)
        _fileChannel.close();
      _fileChannel = null;
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void buildMeasurementDataStructures(JointInspectionResult jointInspectionResult)
  {
    Assert.expect(jointInspectionResult != null);

    // build up a map from algorithmEnum to JointMeasurements

    clearMeasurementDataStructures();

    int index = -1;
    for (JointMeasurement measurement : jointInspectionResult.getAllMeasurements())
    {
      ++index;
      _jointMeasurements.add(measurement);
      long id = measurement.getId();
      _jointMeasurementIdToIndex.put(id, index);
    }
  }

  /**
   * @author Bill Darbie
   */
  private void clearMeasurementDataStructures()
  {
    _jointMeasurements.clear();
    _jointMeasurementIdToIndex.clear();

    _componentMeasurements.clear();
    _componentMeasurementIdToIndex.clear();
  }

  /**
   * @author Bill Darbie
   */
  private void buildMeasurementDataStructures(ComponentInspectionResult componentInspectionResult)
  {
    Assert.expect(componentInspectionResult != null);

    // build up a map from algorithmEnum to JointMeasurements

    clearMeasurementDataStructures();

    int index = -1;
    for (ComponentMeasurement measurement : componentInspectionResult.getAllMeasurements())
    {
      ++index;
      _componentMeasurements.add(measurement);
      long id = measurement.getId();
      _componentMeasurementIdToIndex.put(id, index);
    }
  }

  /**
    * @author Laura Cormos
    */
   private void buildSubtypeMeasurementSliceEnumDataStructure(JointInspectionResult jointInspectionResult)
   {
     Assert.expect(jointInspectionResult != null);

     // build a map of Subtype name to list of Measurement/Slice pairs for the subtype
     String subtypeName = jointInspectionResult.getJointInspectionData().getPad().getSubtype().getLongName();

     List<Pair<MeasurementEnum, SliceNameEnum>> measSlicePairList = _subtypeToMeasurementEnumAndSliceNameEnumPairListMap.get(subtypeName);
     if (measSlicePairList == null)
     {
       measSlicePairList = new ArrayList<Pair<MeasurementEnum, SliceNameEnum>>();
       _subtypeToMeasurementEnumAndSliceNameEnumPairListMap.put(subtypeName, measSlicePairList);
     }

     for (JointMeasurement measurement : jointInspectionResult.getAllMeasurements())
     {
       MeasurementEnum measEnum = measurement.getMeasurementEnum();
       SliceNameEnum sliceEnum = measurement.getSliceNameEnum();
       Pair<MeasurementEnum, SliceNameEnum> newEntry = new Pair<MeasurementEnum, SliceNameEnum>(measEnum, sliceEnum);
       if (measSlicePairList.contains(newEntry) == false)
         measSlicePairList.add(newEntry);
     }
   }

   /**
    * @author Laura Cormos
    */
   private void buildSubtypeMeasurementSliceEnumDataStructure(ComponentInspectionResult componentInspectionResult)
   {
     Assert.expect(componentInspectionResult != null);

     // build a map of Subtype name to list of Measurement/Slice pairs for the subtype
     List<Pair<MeasurementEnum, SliceNameEnum>> measSlicePairList;
     for (ComponentMeasurement measurement : componentInspectionResult.getAllMeasurements())
     {
       MeasurementEnum measEnum = measurement.getMeasurementEnum();
       SliceNameEnum sliceEnum = measurement.getSliceNameEnum();
       Pair<MeasurementEnum, SliceNameEnum> newEntry = new Pair<MeasurementEnum, SliceNameEnum>(measEnum, sliceEnum);
       
       //Siew Yeng - XCR-2535 - Inconsistent display of Component Voiding Area at Measurement Type for mix Subtype
       for(Subtype subtype : measurement.getSubtypes())
       {
         String subtypeName = subtype.getLongName();

         measSlicePairList = _subtypeToMeasurementEnumAndSliceNameEnumPairListMap.get(subtypeName);
         if (measSlicePairList == null)
         {
           measSlicePairList = new ArrayList<Pair<MeasurementEnum, SliceNameEnum>>();
           _subtypeToMeasurementEnumAndSliceNameEnumPairListMap.put(subtypeName, measSlicePairList);
         }
         if (measSlicePairList.contains(newEntry) == false)
           measSlicePairList.add(newEntry);
       }
     }
   }

   /**
    * @author Laura Cormos
    */
   private void clearSubtypeMeasurementSliceEnumDataStructures()
   {
     _subtypeToMeasurementEnumAndSliceNameEnumPairListMap.clear();
   }

  /**
   * @author Bill Darbie
   */
  public synchronized void writeJoint(JointInspectionResult jointInspectionResult) throws DatastoreException
  {
    Assert.expect(jointInspectionResult != null);

    // if _fileChannel is null then close was already called when IOException happen when disk is full
    if (_fileChannel == null)
      return;

    _timerUtil.start();
    try
    {
      buildMeasurementDataStructures(jointInspectionResult);
      buildSubtypeMeasurementSliceEnumDataStructure(jointInspectionResult);

      // before anything is written, get the current file position
      long position = getPosition();
      ++_recordId;

      boolean passed = jointInspectionResult.passed();

      // update the _padToFilePositionMap
      JointInspectionData jointInspectionData = jointInspectionResult.getJointInspectionData();
      Pad pad = jointInspectionData.getPad();
      Component component = pad.getComponent();
      String boardName = component.getSideBoard().getBoard().getName();
      String refDes = component.getReferenceDesignator();
      String padName = pad.getName();
      String name = boardName + " " + refDes + " " + padName;
      Object prev = _filePositionToPadOrComponentNameAndIdMap.put(position, new Pair<String, Integer>(name, _recordId));
      Assert.expect(prev == null);
      prev = _filePositionToJointAndPassMap.put(position, new Pair<Boolean, Boolean>(true, passed));
      Assert.expect(prev == null);

      // record version number
      writeInt(1);
      // uniqueId
      writeInt(_recordId);
      // passed
      writeBoolean(passed);
      // topSide
      writeBoolean(pad.isTopSide());
      // subtype name
      writeString(pad.getSubtype().getShortName());
      writeString(pad.getSubtype().getLongName());
      // jointTypeEnum
      JointTypeEnum jointTypeEnum = pad.getJointTypeEnum();
      writeInt(_enumToIdLookup.getJointTypeUniqueID(jointTypeEnum));

      // number of measurements
      writeInt(_jointMeasurements.size());
      for (JointMeasurement jointMeasurement : _jointMeasurements)
      {
        // measurementNameEnum
        writeInt(_enumToIdLookup.getMeasurementUniqueID(jointMeasurement.getMeasurementEnum()));
        // algorithmNameEnum
        writeInt(_enumToIdLookup.getAlgorithmUniqueID(jointMeasurement.getAlgorithm().getAlgorithmEnum()));
        // sliceNameEnum
        writeInt(_enumToIdLookup.getSliceNameUniqueID(jointMeasurement.getSliceNameEnum()));
        // value
        writeFloat(jointMeasurement.getValue());
        // unitsEnum
        writeInt(_enumToIdLookup.getMeasurementUnitsUniqueID(jointMeasurement.getMeasurementUnitsEnum()));
      }

      Collection<JointIndictment> jointIndictments = jointInspectionResult.getIndictments();
      int size = jointIndictments.size();
      // number of indictments
      writeInt(size);
      for (JointIndictment jointIndictment : jointIndictments)
      {
        // indictmentNameEnum
        writeInt(_enumToIdLookup.getIndictmentUniqueID(jointIndictment.getIndictmentEnum()));
        // algorithmNameEnum
        writeInt(_enumToIdLookup.getAlgorithmUniqueID(jointIndictment.getAlgorithm().getAlgorithmEnum()));

        Collection<JointMeasurement> failingJointMeasurements = jointIndictment.getFailingMeasurements();
        // numFailingMeasurements
        writeInt(failingJointMeasurements.size());
        for (JointMeasurement jointMeasurement : failingJointMeasurements)
        {
          int index = _jointMeasurementIdToIndex.get(jointMeasurement.getId());
          // index to failing joint measurements
          writeInt(index);
        }
        Collection<JointMeasurement> relatedMeasurements = jointIndictment.getRelatedMeasurements();
        // numRelatedMeasurements
        writeInt(relatedMeasurements.size());
        for (JointMeasurement jointMeasurement : relatedMeasurements)
        {
          int index = _jointMeasurementIdToIndex.get(jointMeasurement.getId());
          // index to failing joint measurements
          writeInt(index);
        }
      }
    }
    finally
    {
      clearMeasurementDataStructures();

      _timerUtil.stop();
    }
  }

  /**
   * @author Bill Darbie
   */
  public synchronized void writeComponent(ComponentInspectionResult componentInspectionResult) throws DatastoreException
  {
    Assert.expect(componentInspectionResult != null);

    // if _fileChannel is null then close was already called when IOException happen when disk is full
    if (_fileChannel == null)
      return;
    _timerUtil.start();
    try
    {
      buildMeasurementDataStructures(componentInspectionResult);
      buildSubtypeMeasurementSliceEnumDataStructure(componentInspectionResult);

      // get the position before any writes for this record are done
      long position = getPosition();
      ++_recordId;

      boolean passed = componentInspectionResult.passed();

      // update the _padToFilePositionMap
      ComponentInspectionData componentInspectionData = componentInspectionResult.getComponentInspectionData();
      Component component = componentInspectionData.getComponent();
      String boardName = component.getBoard().getName();
      String refDes = component.getReferenceDesignator();
      String name = boardName + " " + refDes;
      Object prev;
      if(_padNameSet.add(name))
      {
        prev = _filePositionToPadOrComponentNameAndIdMap.put(position, new Pair<String, Integer>(name, _recordId));
        Assert.expect(prev == null);
      }
      prev = _filePositionToJointAndPassMap.put(position, new Pair<Boolean, Boolean>(false, passed));
      Assert.expect(prev == null);

      // record version number
      writeInt(1);
      // uniqueId
      writeInt(_recordId);
      // passed
      writeBoolean(passed);
      // topSide
      writeBoolean(component.isTopSide());

      // numMeasurements
      writeInt(countTotalComponentMeasurements());
      for (ComponentMeasurement componentMeasurement : _componentMeasurements)
      {
        //Siew Yeng - XCR-2535 - Inconsistent display of Component Voiding Area at Measurement Type for mix Subtype
        for(Subtype subtype: componentMeasurement.getSubtypes())
        {
          // measurementNameEnum
          writeInt(_enumToIdLookup.getMeasurementUniqueID(componentMeasurement.getMeasurementEnum()));
          // algorithmNameEnum
          writeInt(_enumToIdLookup.getAlgorithmUniqueID(componentMeasurement.getAlgorithm().getAlgorithmEnum()));
          // subtype name
          writeString(subtype.getShortName());
          writeString(subtype.getLongName());
          // sliceNameEnum
          writeInt(_enumToIdLookup.getSliceNameUniqueID(componentMeasurement.getSliceNameEnum()));
          // value
          writeFloat(componentMeasurement.getValue());
          // unitsEnum
          writeInt(_enumToIdLookup.getMeasurementUnitsUniqueID(componentMeasurement.getMeasurementUnitsEnum()));
        }
      }

      Collection<ComponentIndictment> componentIndictments = componentInspectionResult.getIndictments();
      int size = componentIndictments.size();
      // number of indictments
      writeInt(size);
      for (ComponentIndictment componentIndictment : componentIndictments)
      {
        // indictmentNameEnum
        writeInt(_enumToIdLookup.getIndictmentUniqueID(componentIndictment.getIndictmentEnum()));
        // algorithmNameEnum
        writeInt(_enumToIdLookup.getAlgorithmUniqueID(componentIndictment.getAlgorithm().getAlgorithmEnum()));

        Collection<ComponentMeasurement> failingJointMeasurements = componentIndictment.getFailingMeasurements();
        // numFailingMeasurements
        writeInt(failingJointMeasurements.size());
        for (ComponentMeasurement componentMeasurement : failingJointMeasurements)
        {
          int index = _componentMeasurementIdToIndex.get(componentMeasurement.getId());
          // index to failing joint measurements
          writeInt(index);
        }
        Collection<ComponentMeasurement> relatedMeasurements = componentIndictment.getRelatedMeasurements();
        // numRelatedMeasurements
        writeInt(relatedMeasurements.size());
        for (ComponentMeasurement componentMeasurement : relatedMeasurements)
        {
          int index = _componentMeasurementIdToIndex.get(componentMeasurement.getId());
          // index to failing joint measurements
          writeInt(index);
        }
      }
    }
    finally
    {
      clearMeasurementDataStructures();
      _timerUtil.stop();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void writeHeader() throws DatastoreException
  {
    // write the header record version
    writeInt(2);
    // software version number
    writeString(Version.getVersionNumber());
    // image set name
    writeString(_imageSetName);
    // image set user description
    writeString(_imageSetUserDescription);
    // images collected online
    writeBoolean(_imagesCollectedOnline);
    // test program version
    writeInt(_project.getTestProgramVersion());

    writeLong(_inspectionStartTime);
    writeLong(_inspectionStopTime);

    // panel serial number
    //writeString(_panelSerialNumber);
    
    if (_project.getPanel().isMultiBoardPanel())
      writeBoardsSerialNumber();
    else
      writeString(_panelSerialNumber);

    // create the header that maps Pad to file offset
    // numPadsAndComponents
    writeInt(_filePositionToPadOrComponentNameAndIdMap.size());
    Set<String> padNameSet = new HashSet<String>();
    for (Map.Entry<Long, Pair<String, Integer>> entry : _filePositionToPadOrComponentNameAndIdMap.entrySet())
    {
      long position = entry.getKey();
      Pair<String, Integer> nameRecordPair = entry.getValue();
      String name = nameRecordPair.getFirst();
      int recordId = nameRecordPair.getSecond();
      Pair<Boolean, Boolean> jointPassPair = _filePositionToJointAndPassMap.get(position);
      boolean joint = jointPassPair.getFirst();
      boolean passed = jointPassPair.getSecond();

      //      Assert.expect(padNameSet.add(name), "Joint information for same pad cannot be stored twice for pad: " + name);
      if(padNameSet.add(name))
      {
        writeString(name);
        // jointOrComponent (boolean)  joint = true, component = false
        writeBoolean(joint);
        // pass
        writeBoolean(passed);
        // uniqueId (int)
        writeInt(recordId);
        // number of bytes into file starting at the data section to get to that record
        writeLong(position);
      }
    }

    // add the mapping of Subtypes to measurementEnumID/SliceEnumID pairs
    //   numSubtypes (int)
    writeInt(_subtypeToMeasurementEnumAndSliceNameEnumPairListMap.size());

    for (Map.Entry<String, List<Pair<MeasurementEnum, SliceNameEnum>>> entry :
         _subtypeToMeasurementEnumAndSliceNameEnumPairListMap.entrySet())
    {
      String subtypeName = entry.getKey();
      // subtype name (string)
      writeString(subtypeName);
      // numMeasurements (int)
      writeInt(entry.getValue().size());
      for (Pair<MeasurementEnum, SliceNameEnum> pair : entry.getValue())
      {
        // measurementEnumId (int)
        // sliceEnumId (int)
        writeInt(_enumToIdLookup.getMeasurementUniqueID(pair.getFirst()));
        writeInt(_enumToIdLookup.getSliceNameUniqueID(pair.getSecond()));
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private void writeBoards() throws DatastoreException
  {
    // board record version
    writeInt(1);

    List<Board> boards = _project.getPanel().getBoards();
    // number of boards
    writeInt(boards.size());
    for (Board board : boards)
    {
      String name = board.getName();
      writeString(name);
      // board serial number
      String serialNumber = _boardNameToSerialNumberMap.get(name);
      if (serialNumber == null)
        serialNumber = "";
      writeString(serialNumber);
    }
  }
  
   /**
   * @author Jack Hwee
   */
  private void writeBoardsSerialNumber() throws DatastoreException
  {
    List<Board> boards = _project.getPanel().getBoards();
    List<String> boardsSerialNumberList = new ArrayList<>();
    List<String> boardsWithoutSerialNumberList = new ArrayList<String>();
    
    for (Board board : boards)
    {
      String name = board.getName();
      // board serial number
      String serialNumber = _boardNameToSerialNumberMap.get(name);
      if (serialNumber == null)
      {
        serialNumber = ""; 
      }
      
      if (serialNumber.equals(""))
        boardsWithoutSerialNumberList.add(name);
      
      boardsSerialNumberList.add(serialNumber);
    }
    // XCR-2305 Result details is incomplete when run test on Multi Board recipe image set   
    if (boardsWithoutSerialNumberList.size() == boards.size())
    {
      boardsSerialNumberList.clear();
      boardsSerialNumberList.add(_panelSerialNumber);
    }
    
    writeString(boardsSerialNumberList.toString());
    boardsSerialNumberList.clear();
  }

  /**
   * @author Bill Darbie
   */
  private void flush() throws DatastoreException
  {
    try
    {
      if (_fileChannel != null)
      {
        _byteBuffer.flip();
        _fileChannel.write(_byteBuffer);
        _byteBuffer.clear();
      }
    }
    catch (IOException ioe)
    {
      close();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }
  }

  /**
   * @author Bill Darbie
   */
  private long getPosition() throws DatastoreException
  {
    long position = 0;
    try
    {
      Assert.expect(_fileChannel != null);
      Assert.expect(_byteBuffer != null);
      position = _fileChannel.position() + (long)_byteBuffer.position();
    }
    catch (IOException ioe)
    {
      close();

      CannotWriteDatastoreException de = new CannotWriteDatastoreException(_currFileName);
      de.initCause(ioe);
      throw de;
    }

    return position;
  }

  /**
   * @author Bill Darbie
   */
  private void writeInt(int value) throws DatastoreException
  {
    if (_byteBuffer.remaining() < _numBytesForInteger)
    {
      flush();
      Assert.expect(_byteBuffer.remaining() > _numBytesForInteger);
    }

    _byteBuffer.putInt(value);
  }

  /**
   * @author Bill Darbie
   */
  private void writeLong(long value) throws DatastoreException
  {
    if (_byteBuffer.remaining() < _numBytesForLong)
    {
      flush();
      Assert.expect(_byteBuffer.remaining() > _numBytesForLong);
    }

    _byteBuffer.putLong(value);
  }

  /**
   * @author Bill Darbie
   */
  private void writeFloat(float value) throws DatastoreException
  {
    if (_byteBuffer.remaining() < _numBytesForFloat)
    {
      flush();
      Assert.expect(_byteBuffer.remaining() > _numBytesForFloat);
    }

    _byteBuffer.putFloat(value);
  }

  /**
   * @author Bill Darbie
   */
  private void writeDouble(double value) throws DatastoreException
  {
    if (_byteBuffer.remaining() < _numBytesForDouble)
    {
      flush();
      Assert.expect(_byteBuffer.remaining() > _numBytesForDouble);
    }

    _byteBuffer.putDouble(value);
  }

  /**
   * @author Bill Darbie
   */
  private void writeBoolean(boolean value) throws DatastoreException
  {
    if (_byteBuffer.remaining() < _numBytesForByte)
    {
      flush();
      Assert.expect(_byteBuffer.remaining() > _numBytesForByte);
    }

    if (value)
      _byteBuffer.put((byte)1);
    else
      _byteBuffer.put((byte)0);
  }

  /**
   * @author Bill Darbie
   */
  private void writeString(String string) throws DatastoreException
  {
    Assert.expect(string != null);

    byte[] bytes = string.getBytes();
    int numBytes = bytes.length;
    writeInt(numBytes);
    if (_byteBuffer.remaining() < numBytes)
    {
      flush();
      Assert.expect(_byteBuffer.remaining() > numBytes);
    }

    _byteBuffer.put(bytes);
  }
  
  /**
   * @author Siew Yeng 
   */
  private int countTotalComponentMeasurements()
  {
    int total = 0;
    for(ComponentMeasurement componentMeasurement : _componentMeasurements)
    {
      total += componentMeasurement.getSubtypes().size();
    }
    return total;
  }
}
