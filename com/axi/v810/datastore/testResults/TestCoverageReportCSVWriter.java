package com.axi.v810.datastore.testResults;

import java.io.*;
import java.util.*;
import java.text.*;
import java.util.concurrent.atomic.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.AlgorithmEnum;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
   * @author Chin-Seong, Kee
*/
public class TestCoverageReportCSVWriter
{
  private boolean _exceptionThrown = false;
  private Project _project;
  private String _projectFilePath;
  private String _currentDateTime;
  private PrintWriter _os;

  private AtomicInteger _numberOfTopSideLoadedJoints = new AtomicInteger(0);
  private AtomicInteger _numberOfBtmSideLoadedJoints = new AtomicInteger(0);
  private AtomicInteger _numberOfTopSideLoadedComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfBtmSideLoadedComponents = new AtomicInteger(0);

  private AtomicInteger _numberOfTopSideTestedComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfBtmSideTestedComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfTopSideTestedJoints = new AtomicInteger(0);
  private AtomicInteger _numberOfBtmSideTestedJoints = new AtomicInteger(0);

  private AtomicInteger _numberOfTopSideUntestableComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfBtmSideUntestableComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfTopSideUntestableJoints = new AtomicInteger(0);
  private AtomicInteger _numberOfBtmSideUntestableJoints = new AtomicInteger(0);

  private AtomicInteger _numberOfTotalTopSideUnloadedComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfTotalBtmSideUnloadedComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfTotalTopSideUnloadedJoints = new AtomicInteger(0);
  private AtomicInteger _numberOfTotalBtmSideUnloadedJoints = new AtomicInteger(0);

  private AtomicInteger _numberOfTotalTopSideNoTestComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfTotalBtmSideNoTestComponents = new AtomicInteger(0);
  private AtomicInteger _numberOfTotalTopSideNoTestJoints = new AtomicInteger(0);
  private AtomicInteger _numberOfTotalBtmSideNoTestJoints = new AtomicInteger(0);

  private Map<CompPackage, Integer> _jointTypeToPackageMap = new HashMap<CompPackage,Integer>();
  private Map<CompPackage, Integer> _noLoadJointToPackageMap = new HashMap<CompPackage,Integer>();
  private Map<CompPackage, Integer> _testedJointToPackageMap = new HashMap<CompPackage,Integer>();
  private Map<CompPackage, Integer> _untestableJointToPackageMap = new HashMap<CompPackage,Integer>();
  private Map<CompPackage, Integer> _noTestJointToPackageMap = new HashMap<CompPackage,Integer>();

  private String _dbQuote = ",";

  DecimalFormat twoDecimalPlaces = new DecimalFormat("###.##");

  private final String _MIXED_JOINT_TYPE = "Mixed";

  /**
   * @author Chin-Seong, Kee
   */
  public TestCoverageReportCSVWriter()
  {
    _numberOfTopSideLoadedJoints.set(0);
    _numberOfTopSideLoadedComponents.set(0);
    _numberOfBtmSideLoadedJoints.set(0);
    _numberOfBtmSideLoadedComponents.set(0);

    _numberOfTopSideTestedComponents.set(0);
    _numberOfBtmSideTestedComponents.set(0);
    _numberOfTopSideTestedJoints.set(0);
    _numberOfBtmSideTestedJoints.set(0);

    _numberOfTotalTopSideUnloadedComponents.set(0);
    _numberOfTotalBtmSideUnloadedJoints.set(0);
    _numberOfTotalBtmSideUnloadedComponents.set(0);
    _numberOfTotalTopSideUnloadedJoints.set(0);

    _numberOfTopSideUntestableComponents.set(0);
    _numberOfBtmSideUntestableComponents.set(0);
    _numberOfTopSideUntestableJoints.set(0);
    _numberOfBtmSideUntestableJoints.set(0);

    _numberOfTotalTopSideNoTestComponents.set(0);
    _numberOfTotalBtmSideNoTestComponents.set(0);
    _numberOfTotalTopSideNoTestJoints.set(0);
    _numberOfTotalBtmSideNoTestJoints.set(0);
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void open(Project project, String projectCSVFilePath) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(projectCSVFilePath != null);

    _project = project;
    _projectFilePath = projectCSVFilePath;


    DateFormat myformat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss zzz");
    myformat.setTimeZone(TimeZone.getDefault());
    Date date = new Date();
    _currentDateTime = myformat.format(date);

    calculateComponentLevel();
    calculateJointLevel();
    calculatePackageToComponent();

    try
    {
      _os = new PrintWriter(_projectFilePath);
      writeTopElement();
    }
    catch (FileNotFoundException ex)
    {
      _exceptionThrown = true;
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_projectFilePath);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      // if the file still exists and an exception was thrown, delete it
      if (_exceptionThrown && FileUtil.exists(_projectFilePath))
      {
        // close the file in case an exception was thrown before the file was closed
        if (_os != null)
          _os.close();
        try
        {
          FileUtilAxi.delete(_projectFilePath);
        }
        catch (CannotDeleteFileDatastoreException ex)
        {
          if (_exceptionThrown)
            ex.printStackTrace();
          else
            throw ex;
        }
      }
    }
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTopElement()
  {
      _os.println("Title" + _dbQuote + "V810 Test Coverage Report");
      _os.println("Date" + _dbQuote + _currentDateTime);
      _os.println("Project" + _dbQuote + _project.getName());
      _os.println("");

      writeTotalOfComponentAndJointLevel();
      writeTotalOfTestedComponentAndJointLevel();
      writeTotalOfUnloadedComponentAndJointLevel();
      writeTotalOfNoTestComponentAndJointLevel();
      writeTotalOfUntestableComponentAndJointLevel();
      writeTotalCoverageOfComponentAndJointLevel();
      writeTestCoverageBreakdown();
      writeNoLoadPackageList();
      writeNoTestPackageList();
      writeUntestablePackageList();
      writeComponentTestedAlgorithmSettingsLevel();
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTotalOfComponentAndJointLevel()
  {
      _os.println("Component Count" + _dbQuote + _dbQuote + _dbQuote + "Joint Count");
      _os.println("Top" + _dbQuote + (_numberOfTopSideLoadedComponents.get() + _numberOfTotalTopSideUnloadedComponents.get()) +
                  _dbQuote + "Top" + _dbQuote + (_numberOfTopSideLoadedJoints.get() + _numberOfTotalTopSideUnloadedJoints.get()));
      _os.println("Bottom" + _dbQuote + (_numberOfBtmSideLoadedComponents.get() + _numberOfTotalBtmSideUnloadedComponents.get()) +
                  _dbQuote + "Bottom" + _dbQuote + (_numberOfBtmSideLoadedJoints.get() + _numberOfTotalBtmSideUnloadedJoints.get()));
      _os.println("Total" + _dbQuote + ((_numberOfTopSideLoadedComponents.get() +
                  _numberOfTotalTopSideUnloadedComponents.get()) + (_numberOfBtmSideLoadedComponents.get() +
                  _numberOfTotalBtmSideUnloadedComponents.get())) + _dbQuote + "Total" + _dbQuote +
                  ((_numberOfBtmSideLoadedJoints.get() + _numberOfTotalBtmSideUnloadedJoints.get()) +
                  (_numberOfTopSideLoadedJoints.get() + _numberOfTotalTopSideUnloadedJoints.get())));
      _os.println("");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTotalOfTestedComponentAndJointLevel()
  {
      _os.println("Component Tested" + _dbQuote + _dbQuote + _dbQuote + "Joint Tested");
      _os.println("Top" + _dbQuote + _numberOfTopSideTestedComponents.get() + _dbQuote +
                  "Top" + _dbQuote + _numberOfTopSideTestedJoints.get());
      _os.println("Bottom" + _dbQuote + _numberOfBtmSideTestedComponents.get() + _dbQuote +
                  "Bottom" + _dbQuote + _numberOfBtmSideTestedJoints.get());
      _os.println("Total" + _dbQuote + _project.getPanel().getNumTestedComponents() + _dbQuote +
                  "Total" + _dbQuote + _project.getPanel().getNumInspectedJoints());
      _os.println("");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTotalOfUnloadedComponentAndJointLevel()
  {
      _os.println("No Load Component" + _dbQuote + _dbQuote + _dbQuote + "No Load Joint");
      _os.println("Top" + _dbQuote + _numberOfTotalTopSideUnloadedComponents.get() + _dbQuote +
                  "Top" + _dbQuote + _numberOfTotalTopSideUnloadedJoints.get());
      _os.println("Bottom" + _dbQuote + _numberOfTotalBtmSideUnloadedComponents.get() + _dbQuote +
                 "Bottom" + _dbQuote + _numberOfTotalBtmSideUnloadedJoints.get());
      _os.println("Total" + _dbQuote + (_numberOfTotalTopSideUnloadedComponents.get()
                  + _numberOfTotalBtmSideUnloadedComponents.get()) + _dbQuote + "Total" + _dbQuote +
                  + (_numberOfTotalTopSideUnloadedJoints.get() + _numberOfTotalBtmSideUnloadedJoints.get()));
      _os.println("");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTotalCoverageOfComponentAndJointLevel()
  {
      _os.println("Component Coverage" + _dbQuote + _dbQuote + _dbQuote + "Joint Coverage");
      _os.println("Top" + _dbQuote + twoDecimalPlaces.format((double)((double)_numberOfTopSideTestedComponents.get()/
                  (double)_numberOfTopSideLoadedComponents.get()) * 100) + "%" + _dbQuote +
                  "Top" + _dbQuote +
                  twoDecimalPlaces.format((double)((double)_numberOfTopSideTestedJoints.get()/
                  (double)_numberOfTopSideLoadedJoints.get()) * 100) + "%");
      _os.println("Bottom" + _dbQuote +twoDecimalPlaces.format((double)((double)_numberOfBtmSideTestedComponents.get()/
                  (double)_numberOfBtmSideLoadedComponents.get()) * 100) + "%" + _dbQuote +
                  "Bottom" + _dbQuote +
                  twoDecimalPlaces.format((double)((double)_numberOfBtmSideTestedJoints.get()/
                  (double)_numberOfBtmSideLoadedJoints.get()) * 100) + "%");
      _os.println("Total" + _dbQuote + twoDecimalPlaces.format((double)((double)_project.getPanel().getNumTestedComponents()/
                  (double)_project.getPanel().getNumLoadedComponents()) * 100) + "%" + _dbQuote +
                  "Total" + _dbQuote +
                 twoDecimalPlaces.format((double)((double)_project.getPanel().getNumInspectedJoints()/
                  (double)_project.getPanel().getNumJoints()) * 100) + "%");
      _os.println("");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTotalOfNoTestComponentAndJointLevel()
  {
      _os.println("No Test Component" + _dbQuote + _dbQuote + _dbQuote + "No Test Joint");
      _os.println("Top" + _dbQuote + _numberOfTotalTopSideNoTestComponents.get() + _dbQuote +
                  "Top" + _dbQuote + _numberOfTotalTopSideNoTestJoints.get());
      _os.println("Bottom" + _dbQuote + _numberOfTotalBtmSideNoTestComponents.get() + _dbQuote +
                  "Bottom" + _dbQuote + _numberOfTotalBtmSideNoTestJoints.get());
      _os.println("Total" + _dbQuote + (_numberOfTotalTopSideNoTestComponents.get()+
                  _numberOfTotalBtmSideNoTestComponents.get()) + _dbQuote + "Total" + _dbQuote +
                  (_numberOfTotalTopSideNoTestJoints.get() + _numberOfTotalBtmSideNoTestJoints.get()));
      _os.println("");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTotalOfUntestableComponentAndJointLevel()
  {
      _os.println("Untestable Component" + _dbQuote + _dbQuote + _dbQuote + "Untestable Joint");
      _os.println("Top" + _dbQuote + _numberOfTopSideUntestableComponents.get() + _dbQuote +
                  "Top" + _dbQuote + _numberOfTopSideUntestableJoints.get());
      _os.println("Bottom" + _dbQuote + _numberOfBtmSideUntestableComponents.get() + _dbQuote +
                  "Bottom" + _dbQuote +_numberOfBtmSideUntestableJoints.get());
      _os.println("Total" + _dbQuote + (_numberOfTopSideUntestableComponents.get() +
                  _numberOfBtmSideUntestableComponents.get()) + _dbQuote + "Total" + _dbQuote +
                  (_numberOfTopSideUntestableJoints.get() + _numberOfBtmSideUntestableJoints.get()));
      _os.println("");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeNoLoadPackageList()
  {
      _os.println("NO LOAD LIST");
      _os.println("Package" + _dbQuote + "JointType" + _dbQuote +
                  "Component" + _dbQuote + "No. Of Joint");

      int totalPad = 0;
      
      if(_numberOfTotalTopSideUnloadedComponents.get() + _numberOfTotalBtmSideUnloadedComponents.get() > 0){
          for (Board board : _project.getPanel().getBoards()){
              for(Component component : board.getComponents()){
                 if(component.getComponentType().isLoaded() == false){
                     for(Pad pad : component.getPads())
                        totalPad ++ ;

                     _os.println(component.getCompPackage().getShortName() + _dbQuote +
                                 ((component.getCompPackage().usesOneJointTypeEnum() == true) ?
                                  component.getCompPackage().getJointTypeEnum().getName() : _MIXED_JOINT_TYPE) + _dbQuote +
                                 component.getComponentType().toString() + _dbQuote +
                                 totalPad);

                     totalPad = 0;
                 }
              }
          }
      }
      else
          _os.println("");

      _os.println("");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeTestCoverageBreakdown()
  {
      _os.println("Package Lists" + _dbQuote + "JointType" + _dbQuote +
                  "Num of Package" + _dbQuote + "Pins per Package" + _dbQuote +
                  "Total Pins" +  _dbQuote + "Total Pin Tested" + _dbQuote +
                  "Total No Load Pin" +  _dbQuote + "Total No Test Pin" + _dbQuote +
                  "Total Untestable Pin" + _dbQuote + "Package (Pin-Coverage)");

      for (Board board : _project.getPanel().getBoards()){
             for(CompPackage compPackage : board.getCompPackages()){
                 _os.println(compPackage.getShortName() + _dbQuote +
                             ((compPackage.usesOneJointTypeEnum() == true) ? compPackage.getJointTypeEnum().getName() : _MIXED_JOINT_TYPE) + _dbQuote +
                             _jointTypeToPackageMap.get(compPackage) + _dbQuote +
                             compPackage.getLandPattern().getNumberOfLandPatternPads() + _dbQuote +
                             (_jointTypeToPackageMap.get(compPackage) * compPackage.getLandPattern().getNumberOfLandPatternPads()) + _dbQuote +
                             _testedJointToPackageMap.get(compPackage) + _dbQuote +
                             _noLoadJointToPackageMap.get(compPackage) + _dbQuote +
                             _noTestJointToPackageMap.get(compPackage) + _dbQuote +
                             _untestableJointToPackageMap.get(compPackage) + _dbQuote +
                             twoDecimalPlaces.format((double)((double)_testedJointToPackageMap.get(compPackage) /
                             ((double)_jointTypeToPackageMap.get(compPackage) * (double)compPackage.getLandPattern().getNumberOfLandPatternPads())* 100)) +
                             String.format("%-18s", "%"));
             }
      }
      
      _os.println("");
  }

/**
   * @author Chin-Seong, Kee
   */
  private void writeNoTestPackageList()
  {
      _os.println("NO TEST LIST");
      _os.println("Package" + _dbQuote + "JointType" + _dbQuote +
                  "Component" + _dbQuote + "Pad Name");

      
      if(_numberOfTotalTopSideNoTestComponents.get() + _numberOfTotalBtmSideNoTestComponents.get() > 0){
          for (Board board : _project.getPanel().getBoards()){
              for(Component component : board.getComponents()){
                 if(component.getComponentType().isLoaded() == true && component.getComponentType().isInspected() == false
                    && component.getComponentType().isTestable() == true){
                     for(Pad pad : component.getPads()){
                         _os.println(component.getCompPackage().getShortName() + _dbQuote +
                                     ((component.getCompPackage().usesOneJointTypeEnum() == true) ?
                                      component.getCompPackage().getJointTypeEnum().getName() : _MIXED_JOINT_TYPE) + _dbQuote +
                                     component.getComponentType().toString() + _dbQuote +
                                     pad.getName());
                     }

                 }
              }
          }
      }
      else
        _os.println("");

      _os.println("");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void writeUntestablePackageList()
  {
      _os.println("UNTESTABLE LIST");
      _os.println("Package" + _dbQuote + "JointType" + _dbQuote +
                  "Component" + _dbQuote + "Pad Name" + _dbQuote + "Reason" + _dbQuote + "BoardName");

      if(_numberOfBtmSideUntestableComponents.get() + _numberOfTopSideUntestableComponents.get() > 0)
      {
          for (Board board : _project.getPanel().getBoards()){
              for(Component component : board.getComponents()){
                 if(component.getComponentType().isTestable() == false){
                     for(Pad pad : component.getPads()){
                       if(pad.getPadSettings().isTestable() == false)
                       {
                          _os.println(component.getCompPackage().getShortName() + _dbQuote +
                                   ((component.getCompPackage().usesOneJointTypeEnum() == true) ?
                                    component.getCompPackage().getJointTypeEnum().getName() : _MIXED_JOINT_TYPE) + _dbQuote +
                                   component.getComponentType().toString() + _dbQuote +
                                   pad.getName() + _dbQuote + "\"" +
                                   pad.getPadSettings().getUntestableReason() + "\"" +
                                   pad.getBoardAndComponentAndPadName().split(" ")[0]);
                       }
                     }
                 }
              }
          }
      }
      else
          _os.println("");
      
      _os.println("");
  }

   /**
 * @author Chin-Seong,Kee
 */
  private void writeComponentTestedAlgorithmSettingsLevel()
  {
     _os.print("COMPONENT-ALGORITHM BREAKDOWN");
     _os.println("RefDes" + _dbQuote + "JointType" + _dbQuote +
                 "Side of Card" + _dbQuote +
                 AlgorithmEnum.EXCESS + _dbQuote +
                 AlgorithmEnum.INSUFFICIENT + _dbQuote + AlgorithmEnum.MISALIGNMENT + _dbQuote +
                 AlgorithmEnum.OPEN + _dbQuote + AlgorithmEnum.SHORT + _dbQuote +
                 AlgorithmEnum.VOIDING);
      for (Board board : _project.getPanel().getBoards()){
           for(Component component : board.getComponents()){
                for(Pad pad : component.getPads()){
                    List<AlgorithmEnum> algo = pad.getSubtype().getAlgorithmEnums();                   
                    //Bee Hoon, XCR 1649 - Exclude not tested/no load component in RefDes 
                    if(component.isInspected()){
                      _os.println(component.getReferenceDesignator() + _dbQuote +
                                pad.getJointTypeEnum()+ _dbQuote +
                                (pad.isTopSide() ? "T" : "B") + _dbQuote +
                                (algo.contains(AlgorithmEnum.EXCESS) ? "Y" : "N") + _dbQuote +
                                (algo.contains(AlgorithmEnum.INSUFFICIENT) ? "Y" : "N") + _dbQuote +
                                (algo.contains(AlgorithmEnum.MISALIGNMENT) ? "Y" : "N") + _dbQuote +
                                (algo.contains(AlgorithmEnum.OPEN) ? "Y" : "N") + _dbQuote +
                                (algo.contains(AlgorithmEnum.SHORT) ? "Y" : "N") + _dbQuote +
                                (algo.contains(AlgorithmEnum.VOIDING) ? "Y" : "N"));
                    }
                    else if(component.isInspected() == false && component.isLoaded())
                    {
                      //Siew Yeng
                      //XCR1724 - Add No Test component to the test coverage list but with all NNNNN
                      _os.println(component.getReferenceDesignator() + _dbQuote +
                                pad.getJointTypeEnum()+ _dbQuote +
                                (pad.isTopSide() ? "T" : "B") + _dbQuote +
                                "N" + _dbQuote +
                                "N" + _dbQuote +
                                "N" + _dbQuote +
                                "N" + _dbQuote +
                                "N" + _dbQuote +
                                "N");
                    }
                    break;
                }
           }
      }
     _os.println("");
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void calculateComponentLevel()
  {
      for (Board board : _project.getPanel().getBoards()){
           for(Component component : board.getComponents()){
               if(component.getComponentType().isLoaded()){
                   if(component.isTopSide()){
                       _numberOfTopSideLoadedComponents.incrementAndGet();
                       if(component.getComponentType().isTestable() == true){
                           if(component.getComponentType().isInspected() == true)
                               _numberOfTopSideTestedComponents.incrementAndGet();
                           else
                               _numberOfTotalTopSideNoTestComponents.incrementAndGet();
                       }
                       else
                           _numberOfTopSideUntestableComponents.incrementAndGet();
                   }
                   else if (component.isBottomSide()){
                       _numberOfBtmSideLoadedComponents.incrementAndGet();
                       if(component.getComponentType().isTestable() == true){
                           if(component.getComponentType().isInspected() == true)
                               _numberOfBtmSideTestedComponents.incrementAndGet();
                           else
                               _numberOfTotalBtmSideNoTestComponents.incrementAndGet();
                       }
                       else
                          _numberOfBtmSideUntestableComponents.incrementAndGet();
                   }
               }
               else{
                   if(component.isTopSide())
                      _numberOfTotalTopSideUnloadedComponents.incrementAndGet();
                   else
                      _numberOfTotalBtmSideUnloadedComponents.incrementAndGet();
               }
           }
      }
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void calculateJointLevel()
  {
      for (Board board : _project.getPanel().getBoards()){
           for(Component component : board.getComponents()){
               if(component.getComponentType().isLoaded()){
                   for(Pad pad : component.getPads()){
                       if(pad.isTopSide()){
                           _numberOfTopSideLoadedJoints.incrementAndGet();
                           if(pad.getPadSettings().isTestable() == true){
                               if(pad.getPadType().isInspected() == true)
                                   _numberOfTopSideTestedJoints.incrementAndGet();
                               else
                                   _numberOfTotalTopSideNoTestJoints.incrementAndGet();
                           }
                           else
                               _numberOfTopSideUntestableJoints.incrementAndGet();

                       }
                       else{
                           _numberOfBtmSideLoadedJoints.incrementAndGet();
                           if(pad.getPadSettings().isTestable() == true){
                                if(pad.getPadType().isInspected() == true)
                                   _numberOfBtmSideTestedJoints.incrementAndGet();
                                else
                                   _numberOfTotalBtmSideNoTestJoints.incrementAndGet();
                            }
                            else
                               _numberOfBtmSideUntestableJoints.incrementAndGet();
                       }
                   }
               }
               else{
                   for(Pad pad : component.getPads()){
                       if(pad.isTopSide())
                          _numberOfTotalTopSideUnloadedJoints.incrementAndGet();
                       else
                          _numberOfTotalBtmSideUnloadedJoints.incrementAndGet();
                   }
               }
           }
      }
  }

  /**
   * @author Chin-Seong, Kee
   */
  private void calculatePackageToComponent()
  {
      int compToCompPackageCounter = 0;
      int compUnloadedToCompPackageCounter = 0;
      int compUntestableToCompPackageCounter = 0;
      int compTestedToCompPackageCounter = 0;
      int compNoTestedToCompPackageCounter = 0;

      for(Board board : _project.getPanel().getBoards())
      {
          for(CompPackage compPackage : board.getCompPackages())
          {
               for(Component component : board.getComponents())
               {
                   if(compPackage.getShortName().compareTo(component.getCompPackage().getShortName()) == 0)
                   {
                       compToCompPackageCounter ++;
                       if(component.getComponentType().isLoaded()){
                           if(component.getComponentType().isTestable()){
                                if(component.getComponentType().isInspected())
                                    compTestedToCompPackageCounter += calculateNumberOfPin(component);
                                else
                                    compNoTestedToCompPackageCounter += calculateNumberOfPin(component);;
                           }
                           else
                              compUntestableToCompPackageCounter += calculateNumberOfPin(component);;
                       }
                       else
                           compUnloadedToCompPackageCounter += calculateNumberOfPin(component);;
                   }
               }
               _jointTypeToPackageMap.put(compPackage, compToCompPackageCounter);
               _noLoadJointToPackageMap.put(compPackage, compUnloadedToCompPackageCounter);
               _testedJointToPackageMap.put(compPackage, compTestedToCompPackageCounter);
               _untestableJointToPackageMap.put(compPackage, compUntestableToCompPackageCounter);
               _noTestJointToPackageMap.put(compPackage, compNoTestedToCompPackageCounter);

               compToCompPackageCounter = 0;
               compUnloadedToCompPackageCounter = 0;
               compUntestableToCompPackageCounter = 0;
               compTestedToCompPackageCounter = 0;
               compNoTestedToCompPackageCounter = 0;
          }
      }
  }

  /**
   * @author Chin-Seong, Kee
   */
  private int calculateNumberOfPin(Component component)
  {
      int padCounter = 0;

      for(Pad pad : component.getPads())
      {
          padCounter ++;
      }
      return padCounter;
  }

  /**
   * @author Chin-Seong, Kee
   */
  public void close()
  {
    if (_os != null)
      _os.close();
  }
}
