package com.axi.v810.datastore.testResults;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * This class reads in results file header information from a binary file.  The format
 * is the following:
 *
 * Header section
 *   Header Record
 *     header record version (int) = 2
 *     software version used to create this file (double)
 *     imageSetName (String)
 *     were images collected online (boolean)
 *     version of the test program (int)
 *     inspection start time (long)
 *     inspection stop time (long)
 *     panel serial number (string)
 *
 *
 * @author Bill Darbie
 * @author George Booth
 */
public class InspectionResultsHeaderReader
{
  private FileInputStream _fileIS;
  private DataInputStream _dataIS;
  private String _filePath;

  private String _softwareVersion;
  private int _testProgramVersion = -1;
  private long _inspectionStartTimeInMillis = -1;
  private long _inspectionStopTimeInMillis = -1;
  private String _panelSerialNumber;
  private String _imageSetName;
  private String _imageSetUserDescription;
  private boolean _imagesCollectedOnline;

  /**
   * @author George Booth
   */
  public InspectionResultsHeaderReader()
  {
    // do nothing
  }

  /**
   * Open the results file on disk that cooresponds to the start time passed in.
   * @author George Booth
   */
  public void open(String projName, long startTimeInMillis) throws DatastoreException
  {
    try
    {
      _filePath = FileName.getResultsFileFullPath(projName, startTimeInMillis);
      _fileIS = new FileInputStream(_filePath);

      // Wrap with a DataInputStream so that we can
      // use its read() methods.
      _dataIS = new DataInputStream(_fileIS);
    }
    catch (FileNotFoundException fnfe)
    {
      FileNotFoundDatastoreException de = new FileNotFoundDatastoreException(_filePath);
      de.initCause(fnfe);
      throw de;
    }
//    catch (IOException ioe)
//    {
//      System.out.println("ioe = " + ioe.toString());
//      CannotOpenFileDatastoreException de = new CannotOpenFileDatastoreException(_filePath);
//      de.initCause(ioe);
//      throw de;
//    };

    TimerUtil timer = new TimerUtil();
    timer.start();

    readHeader();
  }

  /**
   * @author George Booth
   */
  private void readHeader() throws DatastoreException
  {
    int recordVersion = readInt();
    if (recordVersion != 2)
      throw new FileCorruptDatastoreException(_filePath);
    
    _softwareVersion = String.valueOf(readDouble());
    _imageSetName = readString();
    _imageSetUserDescription = readString();
    _imagesCollectedOnline = readBoolean();
    _testProgramVersion = readInt();
    _inspectionStartTimeInMillis = readLong();
    _inspectionStopTimeInMillis = readLong();
    _panelSerialNumber = readString();
  }

  /**
   * @author George Booth
   */
  public PanelResults getPanelResults() throws DatastoreException
  {
    PanelResults panelResults = new PanelResults();
    panelResults.setSerialNumberOrDescription(_panelSerialNumber);
    panelResults.setInspectionStartTimeInMillis(_inspectionStartTimeInMillis);
    panelResults.setInspectionStopTimeInMillis(_inspectionStopTimeInMillis);
    panelResults.setImageSetName(_imageSetName);
    panelResults.setUserDescription(_imageSetUserDescription);
    panelResults.setImagesCollectedOnline(_imagesCollectedOnline);
    return panelResults;
  }

  /**
   * @author George Booth
   */
  private int readInt() throws DatastoreException
  {
    try
    {
      return _dataIS.readInt();
    }
    catch(IOException ioe)
    {
      throw new FileCorruptDatastoreException(_filePath);
    }
  }

  /**
   * @author George Booth
   */
  private double readDouble() throws DatastoreException
  {
    try
    {
      return _dataIS.readDouble();
    }
    catch(IOException ioe)
    {
      throw new FileCorruptDatastoreException(_filePath);
    }
  }

  /**
   * @author George Booth
   */
  private long readLong() throws DatastoreException
  {
    try
    {
      return _dataIS.readLong();
    }
    catch(IOException ioe)
    {
      throw new FileCorruptDatastoreException(_filePath);
    }
  }

  /**
   * @author George Booth
   */
  private boolean readBoolean() throws DatastoreException
  {
    try
    {
      return _dataIS.readBoolean();
    }
    catch(IOException ioe)
    {
      throw new FileCorruptDatastoreException(_filePath);
    }
  }

  /**
   * @author George Booth
   */
  private String readString() throws DatastoreException
  {
    int numBytes = readInt();
    byte[] bytes = new byte[numBytes];
    try
    {
      int num = _dataIS.read(bytes);
    }
    catch(IOException ioe)
    {
      throw new FileCorruptDatastoreException(_filePath);
    }
    return new String(bytes);
  }

  /**
   * @author George Booth
   */
  public void close()
  {
    try
    {
      if (_dataIS != null)
        _dataIS.close();
      _dataIS = null;

      if (_fileIS != null)
        _fileIS.close();
      _fileIS = null;
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

}
