package com.axi.v810.datastore.testResults;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
public class Test_ProjectCadXMLWriter extends UnitTest
{
  private Project _project;

  public static void main(String[] argv)
  {
    UnitTest.execute(new Test_ProjectCadXMLWriter());
  }

  /**
   * @author Laura Cormos
   */
  public Test_ProjectCadXMLWriter()
  {
    // do nothing...
  }

  /**
   * @author Laura Cormos
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    ProjectCadXMLWriter cadXMLwriter = ProjectCadXMLWriter.getInstance();

    _project = loadFamiliesAllRlvProject();

    addCadToProject();

    try
    {
      cadXMLwriter.write(_project, true);
    }
    catch (DatastoreException ex)
    {
      ex.printStackTrace();
    }

    try
    {
      BufferedReader cadFile = new BufferedReader(new FileReader(FileName.getCadFileNameFullPath(_project.getName(),
          _project.getCadXmlChecksum())));
      String line;
      while ((line = cadFile.readLine()) != null)
      {
        /** @todo PE stop filtering this Settings line once the algorithms stabilize */
        if (line.contains("settings") == false)
          System.out.println(line);
      }
      cadFile.close();
    }
    catch (FileNotFoundException ex)
    {
      ex.printStackTrace();
    }
    catch (IOException ioex)
    {
      ioex.printStackTrace();
    }
  }

  /**
   * @author Laura Cormos
   */
  private static Project loadFamiliesAllRlvProject()
  {
    Project dummyProject = null;
    try
    {
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();
      dummyProject = Project.importProjectFromNdfs("families_all_rlv", warnings);
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace();
    }
    return dummyProject;
  }

  /**
   * @author Laura Cormos
   */
  private void addCadToProject()
  {
    Assert.expect(_project != null);
    Panel panel = _project.getPanel();
    List<Board> boards = panel.getBoards();
    for (Board board : boards)
    {
      SideBoardType sideBoardType1 = board.getBoardType().getSideBoardType1();
      SideBoardType sideBoardType2 = board.getBoardType().getSideBoardType2();
      sideBoardType1.createFiducialType("side1fid1", new BoardCoordinate(100, 100));
      sideBoardType2.createFiducialType("side2fid1", new BoardCoordinate(100, 100));
    }
    panel.createFiducial("paneltopfid1", true);
    panel.createFiducial("panelbottomfid1", false);
  }
}
