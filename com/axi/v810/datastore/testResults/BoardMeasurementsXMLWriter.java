package com.axi.v810.datastore.testResults;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Laura Cormos
 */
public class BoardMeasurementsXMLWriter
{
  private boolean _printSpaces = true;
  private final String _space = " ";
  private boolean _exceptionThrown = false;
  private Board _board;
  private String _serialNumber;
  private String _boardFilePath;
  private PrintWriter _os;
  private boolean _isXedOut = false;
  private int _totalNumberOfIndictments = 0;
  private int _numberOfProcessedComponents = 0;
  private int _numberOfDefectiveComponents = 0;
  private int _numberOfProcessedJoints = 0;
  private int _numberOfDefectiveJoints = 0;
  private JointInspectionResult _currentJoint;
  private ComponentInspectionResult _currentComponent;
  private int _indent;
  private int _numOfSpaces = XMLWriterStringUtil.NUM_INDENT_SPACES;
  // kept for future reference if comments are added
  private String _gt = XMLWriterStringUtil.GREATER_THEN_TAG;
  private String _dbQuote = XMLWriterStringUtil.ESCAPED_DOUBLE_QUOTE;
  private String _elemEnd  = XMLWriterStringUtil.ELEMENT_END_TAG;
  private String _dbQuoteGt = _dbQuote + _gt;
  private String _dbQtElemEnd = _dbQuote + _elemEnd;
  private List<Component> _defectiveComponents = new ArrayList<Component>();
  private Map<Component, List<JointInspectionResult>> _componentToFailedJointsListMap =
      new HashMap<Component,List<JointInspectionResult>>();
  private Map<Component, List<JointInspectionResult>> _componentToPassedJointsListMap =
      new HashMap<Component,List<JointInspectionResult>>();
  private Map<Component, Integer> _componentToNumProcessedJointsMap = new HashMap<Component,Integer>();
  private Map<Component, Integer> _componentToTotalNumInspectableJointsMap = new HashMap<Component,Integer>();
  private Map<JointInspectionResult, Set<JointMeasurement>> _jointInspResultToFailingMeasSetMap =
      new HashMap<JointInspectionResult,Set<JointMeasurement>>();
  private Map<ComponentInspectionResult, Set<ComponentMeasurement>> _componentInspResultToFailingMeasSetMap =
      new HashMap<ComponentInspectionResult,Set<ComponentMeasurement>>();
  
  //Janan XCR-3551: Customize Measurement XML output
  private boolean _hasComponentMeasurement = false;
  private boolean _isCustomMeasurementXMLSelected = Config.getInstance().getBooleanValue(CustomMeasurementFileConfigEnum.CUSTOMIZE_MEASUREMENT_OUTPUT);
  private List<ComponentMeasurement> _selectedComponentMeasurement = new ArrayList<ComponentMeasurement>();
  private List<JointMeasurement> _selectedJointMeasurement = new ArrayList<JointMeasurement>();
  /**
   * @author Laura Cormos
   */
  public BoardMeasurementsXMLWriter()
  {
    // do nothing
  }

  /**
   * @author Laura Cormos
   */
  public void open(Board board, String boardXMLFilePath, String serialNumber, int indent, boolean printSpaces) throws DatastoreException
  {
    Assert.expect(board != null);
    Assert.expect(boardXMLFilePath != null);
    Assert.expect(serialNumber != null);
    Assert.expect(indent > 0);

    _board = board;
    _indent = indent;
    _printSpaces = printSpaces;
    _boardFilePath = boardXMLFilePath;
    _serialNumber = serialNumber;

    try
    {
      _os = new PrintWriter(_boardFilePath);
      writeBoardElementBeginning(serialNumber);
    }
    catch (FileNotFoundException ex)
    {
      _exceptionThrown = true;
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_boardFilePath);
      dex.initCause(ex);
      throw dex;
    }
    finally
    {
      // if the file still exists and an exception was thrown, delete it
      if (_exceptionThrown && FileUtil.exists(_boardFilePath))
      {
        // close the file in case an exception was thrown before the file was closed
        if (_os != null)
          _os.close();
        try
        {
          FileUtilAxi.delete(_boardFilePath);
        }
        catch (CannotDeleteFileDatastoreException ex)
        {
          if (_exceptionThrown)
            ex.printStackTrace();
          else
            throw ex;
        }
      }
    }
  }

  /**
   * @author Laura Cormos
   */
  public BoardResultsSummary close() throws DatastoreException
  {
    writeBoardElementEnding();

    if (_os != null)
      _os.close();

    //Chin Seong :: Xed-Out Process the XedOut Measure file
    processXedOutBoardResultFile();

    return new BoardResultsSummary(_totalNumberOfIndictments,
                                   _numberOfProcessedComponents, _numberOfDefectiveComponents,
                                   _numberOfProcessedJoints, _numberOfDefectiveJoints);
  }

    //Chin Seong :: Xed-Out Process the XedOut Measure file
  private void processXedOutBoardResultFile() throws DatastoreException
  {
      if(_isXedOut)
      {
         try
         {
           if (FileUtilAxi.exists(_boardFilePath))
            FileUtilAxi.delete(_boardFilePath);
         }
         catch (CannotDeleteFileDatastoreException ex)
         {
           if (_exceptionThrown)
             ex.printStackTrace();
           else
             throw ex;
         }
        try
        {
            _os = new PrintWriter(_boardFilePath);
            writeBoardElementBeginning(_serialNumber);
        }
        catch (FileNotFoundException ex)
        {
            _exceptionThrown = true;
            FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_boardFilePath);
            dex.initCause(ex);
            throw dex;
        }
        finally{
            writeBoardElementEnding();
            if (_os != null)
                _os.close();
        }
      }
  }

  /**
   * @author Laura Cormos
   */
  public void writeJointInspectionResult(JointInspectionResult jointInspResult)
  {
    Assert.expect(jointInspResult != null);

    _numberOfProcessedJoints++;

    boolean jointPassed = jointInspResult.passed();
    Component component = jointInspResult.getJointInspectionData().getPad().getComponent();
    Integer totalNumJoints = _componentToTotalNumInspectableJointsMap.get(component);
    if (totalNumJoints != null)
    {
      _componentToNumProcessedJointsMap.put(component, _componentToNumProcessedJointsMap.get(component) + 1 );
    }
    else // this happens only once per component the first time we see a joint from that component
    {
      _componentToTotalNumInspectableJointsMap.put(component, component.getComponentType().getInspectedPadTypes().size());
      _componentToNumProcessedJointsMap.put(component, 1);
      _numberOfProcessedComponents++;
    }

    if (jointPassed == false)
    {
      _numberOfDefectiveJoints++;
      // if this component did not have any failing joints before or this is the first time its joints are processed
      // and this joint is defective, add the component to the list of defective components and increment the count
      if (_defectiveComponents.contains(component) == false)
      {
        _numberOfDefectiveComponents++;
        _defectiveComponents.add(component);
      }

      List<JointInspectionResult> failedJointsListForComponent = _componentToFailedJointsListMap.get(component);
      if (failedJointsListForComponent == null)
        failedJointsListForComponent = new ArrayList<JointInspectionResult>();

      failedJointsListForComponent.add(jointInspResult);
      _componentToFailedJointsListMap.put(component, failedJointsListForComponent);

      // create set of failing measurements for this joint to use later when writing out the actual measurements
      Set<JointMeasurement> failedMeasurements = new HashSet<JointMeasurement>();
      for (JointIndictment jointIndictment : jointInspResult.getIndictments())
      {
        failedMeasurements.addAll(jointIndictment.getFailingMeasurements());
      }
      _jointInspResultToFailingMeasSetMap.put(jointInspResult, failedMeasurements);
    }
    else
    {
      List<JointInspectionResult> passingJointsListForComponent = _componentToPassedJointsListMap.get(component);
      if (passingJointsListForComponent == null)
        passingJointsListForComponent = new ArrayList<JointInspectionResult>();

      passingJointsListForComponent.add(jointInspResult);
      _componentToPassedJointsListMap.put(component, passingJointsListForComponent);
    }
  }

  /**
   * @author Laura Cormos
   */
  public void writeComponentInspectionResult(ComponentInspectionResult componentInspectionResult)
  {
    Assert.expect(componentInspectionResult != null);

    Component componentToWrite = componentInspectionResult.getComponentInspectionData().getComponent();

    List<JointInspectionResult> failedJoints = _componentToFailedJointsListMap.get(componentToWrite);

    // increment failing component count only if there are failing joints or component level defects
    if ((failedJoints != null && failedJoints.size() > 0) || componentInspectionResult.getIndictments().size() > 0)
    {
      if (failedJoints == null || failedJoints.isEmpty())
        _numberOfDefectiveComponents++;
    }
	  //Janan XCR 3551- Custom Measurement XML output
    if (_isCustomMeasurementXMLSelected)
    {
      if (CustomMeasurementSetting.getInstance().getCustomSettingJointType().contains(componentInspectionResult.getComponentInspectionData().getComponent().getJointTypeEnums().get(0).getName()))
      {
        List<JointInspectionResult> failJoints = _componentToFailedJointsListMap.get(componentInspectionResult.getComponentInspectionData().getComponent());
        List<JointInspectionResult> passJoints = _componentToPassedJointsListMap.get(componentInspectionResult.getComponentInspectionData().getComponent());
        List<JointInspectionResult> joints = new ArrayList<JointInspectionResult>();

        if (failJoints != null)
        {
          joints.addAll(failJoints);
        }
        
        if (passJoints != null)
        {
          joints.addAll(passJoints);
        }

        List<JointMeasurement> jointMeasurement = new ArrayList<JointMeasurement>();        
        for(JointInspectionResult jointInspectionResult : joints)
        {
          jointMeasurement.addAll(jointInspectionResult.getAllMeasurements());
        }

        List<ComponentMeasurement> compMeasurement = new ArrayList<ComponentMeasurement>(componentInspectionResult.getAllMeasurements());
        for (ComponentMeasurement componentMeasuremement : compMeasurement)
        {
          if (CustomMeasurementSetting.getInstance().getCustomSettingMeasurement().contains(componentMeasuremement.getMeasurementName()))
          {
            writeComponentElement(componentInspectionResult);
            _hasComponentMeasurement = true;
            break;
          }
        }
        
        if (_hasComponentMeasurement == false)
        {
          for (JointMeasurement jointmeasurement : jointMeasurement)
          {
            if (CustomMeasurementSetting.getInstance().getCustomSettingMeasurement().contains(jointmeasurement.getMeasurementName()))
            {
              writeComponentElement(componentInspectionResult);
              break;
            }
          }
        }
        
        _hasComponentMeasurement = false;
      }
    }
    else //default measurement xml output
    {      
      writeComponentElement(componentInspectionResult);
    }
  }
  
  /**
   * Writes out the beginning of the board element of the indictments.xml file
   *   <board name="1" boardTypeName="type_1" tested="true" serialNumber="32 l;.a41">
   *
   * @author Laura Cormos
   */
  private void writeBoardElementBeginning(String serialNumber)
  {
    Assert.expect(serialNumber != null);
    Assert.expect(_os != null);
    Assert.expect(_indent >= 0);

     StringBuilder spaces = new StringBuilder("");
     if (_printSpaces)
       for (int i = 0; i < (_indent * _numOfSpaces); i++)
         spaces.append(_space);;

     _os.println(spaces + "<board");
     // attribute indent
     if (_printSpaces)
       spaces.append("  ");

     _os.println(spaces + "name=" + _dbQuote + XMLWriterStringUtil.replaceIllegalStringChars(_board.getName()) + _dbQuote);
     _os.println(spaces + "boardTypeName=" + _dbQuote + XMLWriterStringUtil.replaceIllegalStringChars(_board.getBoardType().getName())
                 + _dbQuote);
     if (_board.isInspected() && !_isXedOut)
       _os.println(spaces + "tested=" + _dbQuote + "true" + _dbQuote);
     else
       _os.println(spaces + "tested=" + _dbQuote + "false - XedOut" + _dbQuote); //Chin Seong - temporary is Hard coded
     _os.println(spaces + "serialNumber=" + _dbQuote + XMLWriterStringUtil.replaceIllegalStringChars(serialNumber) + _dbQuoteGt);

     _indent++; // set up the indent for the next nested element
   }

   /**
    * Writes out the end of the board element in the indictments.xml file
    *    <summary XedOut="false" indictments="5" components="5" testedComponents="5" defectiveComponents="5"
    *                                            joints="5" testedJoints="5" defectiveJoints="5"/>
    *  </board>
    *
    * @author Laura Cormos
    */
   private void writeBoardElementEnding()
   {
     Assert.expect(_os != null);
     Assert.expect(_indent >= 0);

     StringBuilder spaces = new StringBuilder("");
     if (_printSpaces)
       spaces = buildSpaceString();

     _os.println(spaces + "<summary");
     // attribute indent
     if (_printSpaces)
       spaces.append("  ");

     if (_isXedOut)
       _os.println(spaces + "XedOut=" + _dbQuote + "true" + _dbQtElemEnd);
     else{
        _os.println(spaces + "XedOut=" + _dbQuote + "false" + _dbQuote);
        _os.println(spaces + "indictments=" + _dbQuote + _totalNumberOfIndictments + _dbQuote);
        _os.println(spaces + "components=" + _dbQuote + _board.getNumComponents() + _dbQuote);
        _os.println(spaces + "testedComponents=" + _dbQuote + _numberOfProcessedComponents + _dbQuote);
        _os.println(spaces + "defectiveComponents=" + _dbQuote + _numberOfDefectiveComponents + _dbQuote);
        _os.println(spaces + "joints=" + _dbQuote + _board.getNumJoints() + _dbQuote);
        _os.println(spaces + "testedJoints=" + _dbQuote + _numberOfProcessedJoints + _dbQuote);
        _os.println(spaces + "defectiveJoints=" + _dbQuote + _numberOfDefectiveJoints + _dbQtElemEnd);
     }

     _indent = 1; // for the end of the board element
     StringBuilder endElemSpaces = new StringBuilder("");
     if (_printSpaces)
       spaces = buildSpaceString();
    _os.println(endElemSpaces + "</board>");
   }

   /**
    * Writes the component element and all its failing joint elements for the indictments.xml file
    *  <component refDes="C1" landPattern="CP002" failureType="joint">
    *  </component>
    * @author Laura Cormos
    *
    */
   private void writeComponentElement(ComponentInspectionResult componentInspectionResult)
   {
     Assert.expect(componentInspectionResult != null);
     Assert.expect(_indent > 0);

     Component component = componentInspectionResult.getComponentInspectionData().getComponent();
     _currentComponent = componentInspectionResult;

     List<JointInspectionResult> failedJoints = _componentToFailedJointsListMap.get(component);
     List<JointInspectionResult> passedJoints = _componentToPassedJointsListMap.get(component);

     StringBuilder spaces = new StringBuilder("");
     if (_printSpaces)
       spaces = buildSpaceString();

     _os.println(spaces + "<component");
     // attribute indent
     if (_printSpaces)
       spaces.append("  ");

     _os.println(spaces + "refDes=" + _dbQuote +
                 XMLWriterStringUtil.replaceIllegalStringChars(component.getReferenceDesignator()) + _dbQuote);
     _os.println(spaces + "landPattern=" + _dbQuote +
                 XMLWriterStringUtil.replaceIllegalStringChars(component.getLandPattern().getName()) + _dbQuote);
     _os.println(spaces + "x=" + _dbQuote +
                 component.getCoordinateRelativeToPanelInNanoMeters().getX() + _dbQuote);
     _os.println(spaces + "y=" + _dbQuote +
                 component.getCoordinateRelativeToPanelInNanoMeters().getY() + _dbQuote);

     if(component.getSideBoard().isSideBoard1())
     {
       _os.println(spaces + "boardSide=" + _dbQuote +
                 StringLocalizer.keyToString("CAD_SIDE1_KEY") + _dbQuoteGt);
     }
     else
     {
       _os.println(spaces + "boardSide=" + _dbQuote +
                 StringLocalizer.keyToString("CAD_SIDE2_KEY") + _dbQuoteGt);
     }

     ++_indent;

     // create set of failing measurements for this component to use later when writing out the actual measurements
      Set<ComponentMeasurement> failedMeasurements = new HashSet<ComponentMeasurement>();
      for (ComponentIndictment componentIndictment : componentInspectionResult.getIndictments())
      {
        failedMeasurements.addAll(componentIndictment.getFailingMeasurements());
      }
      _componentInspResultToFailingMeasSetMap.put(componentInspectionResult, failedMeasurements);

     // write all the component measurements by algorithm
     Collection<ComponentMeasurement> componentMeasurements = componentInspectionResult.getAllMeasurements();
     Map<Algorithm, List<ComponentMeasurement>> algorithmToComponentMeasurementListMap = new HashMap<Algorithm, List<ComponentMeasurement>>();
     for (ComponentMeasurement componentMeas : componentMeasurements)
     {
       Algorithm algorithm = componentMeas.getAlgorithm();
       List<ComponentMeasurement> compMeasurementsForAlgorithm = algorithmToComponentMeasurementListMap.get(algorithm);
       if (compMeasurementsForAlgorithm == null)
         compMeasurementsForAlgorithm = new ArrayList<ComponentMeasurement>();

       compMeasurementsForAlgorithm.add(componentMeas);
       algorithmToComponentMeasurementListMap.put(algorithm, compMeasurementsForAlgorithm);
// Comment for fixing CR 1019 - Pull out the Measurement extra Infomation added by WC
// Comment by Wei Chin
//       componentMeas.setZHeightInNanoMeters(componentInspectionResult.getReconstructedSlice(componentMeas.getSliceNameEnum()).getHeightInNanometers());
     }
     Set<Map.Entry<Algorithm, List<ComponentMeasurement>>> algorithmMapEntrySet = algorithmToComponentMeasurementListMap.entrySet();
     for (Map.Entry<Algorithm, List<ComponentMeasurement>> entry : algorithmMapEntrySet)
     {
       //XCR -3551 customize measurement XML - Janan
       if (_isCustomMeasurementXMLSelected)      
       {
         _selectedComponentMeasurement.clear();
         for (ComponentMeasurement componentMeasurement : entry.getValue())
         {
           if (CustomMeasurementSetting.getInstance().getCustomSettingMeasurement().contains(componentMeasurement.getMeasurementName()))
           {
             _selectedComponentMeasurement.add(componentMeasurement);
           }
         }

         if (_selectedComponentMeasurement.isEmpty() == false)
         {
           writeComponentAlgorithmElement(entry.getKey(), _selectedComponentMeasurement);
         }
       }
       else //default measurement xml output
       {
         writeComponentAlgorithmElement(entry.getKey(), entry.getValue());
       }
     }
 
     // write all the joint elements now
     List<JointInspectionResult> allJoints = new ArrayList<JointInspectionResult>();

     if (failedJoints != null)
       allJoints.addAll(failedJoints);

     if (passedJoints != null)
       allJoints.addAll(passedJoints);

     Collections.sort(allJoints, new JointInspectionResultComparator(true));

     for (JointInspectionResult jointInspectionResult : allJoints)
     {
       if (_isCustomMeasurementXMLSelected)     //XCR -3551 customize measurement xml - Janan
       {
         List<JointMeasurement> jointMeasurement = new ArrayList<JointMeasurement>(jointInspectionResult.getAllMeasurements());
         for (JointMeasurement jointmeasurement : jointMeasurement)
         {
           if (CustomMeasurementSetting.getInstance().getCustomSettingMeasurement().contains(jointmeasurement.getMeasurementName()))
           {
             writeJointElement(jointInspectionResult);
             break;
           }
         }
       }
       else  //default measurement xml output
       {
         writeJointElement(jointInspectionResult);
       }
     }

    _os.println(spaces + "</component>");

    //decrement indent for the next element
    --_indent;
  }

  /**
   * Write the joint element for the indictments.xml file
   *  <joint padName="1" nodeName="GND" subtype="CP002_15" jointType="capacitor">
   *  </joint>
   * @author Laura Cormos
   */
  private void writeJointElement(JointInspectionResult joint)
  {
    Assert.expect(joint != null);
    Assert.expect(_indent > 0);

    _currentJoint = joint;
    JointInspectionData jointInspectionData = joint.getJointInspectionData();
    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<joint");
    String endElemSpaces = spaces.toString();

    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    _os.println(spaces + "padName=" + _dbQuote +
                XMLWriterStringUtil.replaceIllegalStringChars(jointInspectionData.getPad().getName()) + _dbQuote);
    if (jointInspectionData.getPad().getNodeName().length() > 0)
      _os.println(spaces + "nodeName=" + _dbQuote + jointInspectionData.getPad().getNodeName() + _dbQuote);
    _os.println(spaces + "subtype=" + _dbQuote +
                XMLWriterStringUtil.replaceIllegalStringChars(jointInspectionData.getSubtype().getShortName()) + _dbQuote);
    _os.println(spaces + "jointType=" + _dbQuote + joint.getJointInspectionData().getJointTypeEnum().getName() + _dbQuote);

    _os.println(spaces + "x=" + _dbQuote +
                    jointInspectionData.getInspectionRegion().getCenterCoordinateRelativeToPanelInNanoMeters().getX() + _dbQuote);

    _os.println(spaces + "y=" + _dbQuote +
                    jointInspectionData.getInspectionRegion().getCenterCoordinateRelativeToPanelInNanoMeters().getY() + _dbQuote);

    _os.println(spaces + "reconstructionRegionId=" + _dbQuote +
                jointInspectionData.getInspectionRegion().getRegionId() + _dbQuote);

    _os.println(spaces + "regionName=" + _dbQuote +
                jointInspectionData.getInspectionRegion().getName() + _dbQuoteGt);

    ++_indent;


    Collection<JointMeasurement> jointMeasurements = joint.getAllMeasurements();
    Map<Algorithm, List<JointMeasurement>> algorithmToJointMeasurementsListMap = new HashMap<Algorithm, List<JointMeasurement>>();
    for (JointMeasurement jointMeas : jointMeasurements)
    {
      Algorithm algorithm = jointMeas.getAlgorithm();
      List<JointMeasurement> measurementsForAlgorithm = algorithmToJointMeasurementsListMap.get(algorithm);
      if (measurementsForAlgorithm == null)
      {
        measurementsForAlgorithm = new ArrayList<JointMeasurement>();
        algorithmToJointMeasurementsListMap.put(algorithm, measurementsForAlgorithm);
      }
      measurementsForAlgorithm.add(jointMeas);
// Comment for fixing CR 1019 - Pull out the Measurement extra Infomation added by WC
// Comment by Wei Chin
//      jointMeas.setZHeightInNanoMeters(joint.getReconstructedSlice(jointMeas.getSliceNameEnum()).getHeightInNanometers());
    }
    Set<Map.Entry<Algorithm, List<JointMeasurement>>> algorithmMapEntrySet = algorithmToJointMeasurementsListMap.entrySet();
    for (Map.Entry<Algorithm, List<JointMeasurement>> entry : algorithmMapEntrySet)
    {
      _selectedJointMeasurement.clear();
      if (_isCustomMeasurementXMLSelected)     //XCR -3551 customize measurement xml - Janan
      {
        for (JointMeasurement jointMeasurement : entry.getValue())
        {
          if (CustomMeasurementSetting.getInstance().getCustomSettingMeasurement().contains(jointMeasurement.getMeasurementName()))
          {
            _selectedJointMeasurement.add(jointMeasurement);
          }
        }

        if (_selectedJointMeasurement.isEmpty() == false)
        {
          writeJointAlgorithmElement(entry.getKey(), _selectedJointMeasurement);
        }
      }
      else  //default measurement xml output
      {
        writeJointAlgorithmElement(entry.getKey(), entry.getValue());
      }
    }
    _os.println(endElemSpaces + "</joint>");

    --_indent;
  }

  /**
   * Writes the algorithm element for this component
   *    <algorithm name="short">
   *        <measurement name="slkdfj" value="545" units="mil" />
   *        <measurement name="lksjdf" value="454" units="mils" />
   *        <measurement name="lakdfj" value="454" units="mils" />
   *    </algorithm>
   *
   * @author Laura Cormos
   */
  private void writeComponentAlgorithmElement(Algorithm algorithm,
                                              List<ComponentMeasurement> componentMeasurements)
  {
    Assert.expect(algorithm != null);
    Assert.expect(componentMeasurements != null);
    Assert.expect(_indent > 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<algorithm");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    _os.println(spaces + "name=" + _dbQuote + algorithm.getAlgorithmEnum().getName() + _dbQuoteGt);

    ++_indent;

    for (ComponentMeasurement componentMeasurement : componentMeasurements)
      writeMeasurementElement(componentMeasurement);

    _os.println(spaces + "</algorithm>");

    --_indent;
  }

   /**
   * Writes the failingAlgorithm element for this joint
   *    <algorithm name="short">
   *        <measurement name="slkdfj" value="545" units="mil" />
   *        <measurement name="lksjdf" value="454" units="mils" />
   *        <measurement name="lakdfj" value="454" units="mils" />
   *    </algorithm>
   *
   * @author Laura Cormos
   */
  private void writeJointAlgorithmElement(Algorithm algorithm,
                                          List<JointMeasurement> jointMeasurements)
  {
    Assert.expect(algorithm != null);
    Assert.expect(jointMeasurements != null);
    Assert.expect(_indent > 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<algorithm");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    _os.println(spaces + "name=" + _dbQuote + algorithm.getAlgorithmEnum().getName() + _dbQuoteGt);

    ++_indent;

    for (JointMeasurement jointMeasurement : jointMeasurements)
      writeMeasurementElement(jointMeasurement);

    _os.println(spaces + "</algorithm>");

    --_indent;
  }

  /**
   * Writes the measuremnt element for the current indictment
   *    <measurement name="slkdfj" value="545" units="mil" />
   *
   * @author Laura Cormos
   */
  private void writeMeasurementElement(JointMeasurement jointMeasurement)
  {
    Assert.expect(jointMeasurement != null);
    Assert.expect(_indent > 0);

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      spaces = buildSpaceString();

    _os.println(spaces + "<measurement");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");

    _os.println(spaces + "name=" + _dbQuote + jointMeasurement.getMeasurementName() + _dbQuote);
    _os.println(spaces + "value=" + _dbQuote + jointMeasurement.getValue() + _dbQuote);
    _os.println(spaces + "units=" + _dbQuote + jointMeasurement.getMeasurementUnitsEnum().getName() + _dbQuote);
    _os.println(spaces + "sliceName=" + _dbQuote + jointMeasurement.getSliceNameEnum().getName() + _dbQuote);
    _os.println(spaces + "sliceId=" + _dbQuote + jointMeasurement.getSliceNameEnum().getId() + _dbQuote);
    Integer zHeight = InspectionMeasurementsXMLWriter.getInstance().getProject().getTestProgram().getReconstructedSliceZHeightByJointMeasurement(jointMeasurement);
    if (zHeight != null)
      _os.println(spaces + "zHeightInNanoMeters=" + _dbQuote + zHeight + _dbQuote);
    //    _os.println(spaces + "zHeightInNanoMeters=" + _dbQuote + jointMeasurement.getZHeightInNanoMeters() + _dbQuote);

    String status = "passed";

    Set<JointMeasurement> failedMeasurements = _jointInspResultToFailingMeasSetMap.get(_currentJoint);
    if (failedMeasurements != null && failedMeasurements.contains(jointMeasurement))
      status = "failed";
    _os.println(spaces + "status=" + _dbQuote + status + _dbQtElemEnd);
  }

  /**
    * Writes the measuremnt element for the current indictment
    *    <measurement name="slkdfj" value="545" units="mil" />
    *
    * @author Laura Cormos
    */
   private void writeMeasurementElement(ComponentMeasurement componentMeasurement)
   {
     Assert.expect(componentMeasurement != null);
     Assert.expect(_indent > 0);

     StringBuilder spaces = new StringBuilder("");
     if (_printSpaces)
       spaces = buildSpaceString();

     _os.println(spaces + "<measurement");
     // attribute indent
     if (_printSpaces)
       spaces.append("  ");

     _os.println(spaces + "name=" + _dbQuote + componentMeasurement.getMeasurementName() + _dbQuote);
     _os.println(spaces + "value=" + _dbQuote + componentMeasurement.getValue() + _dbQuote);
     _os.println(spaces + "units=" + _dbQuote + componentMeasurement.getMeasurementUnitsEnum().getName() + _dbQuote);
     _os.println(spaces + "sliceName=" + _dbQuote + componentMeasurement.getSliceNameEnum().getName() + _dbQuote);
     _os.println(spaces + "sliceId=" + _dbQuote + componentMeasurement.getSliceNameEnum().getId() + _dbQuote);
//     _os.println(spaces + "zHeightInNanoMeters=" + _dbQuote + componentMeasurement.getZHeightInNanoMeters() + _dbQuote);

     String status = "passed";

     Set<ComponentMeasurement> failedMeasurements = _componentInspResultToFailingMeasSetMap.get(_currentComponent);
     if (failedMeasurements != null && failedMeasurements.contains(componentMeasurement))
       status = "failed";
     _os.println(spaces + "status=" + _dbQuote + status + _dbQtElemEnd);
  }

  /**
   * @author Laura Cormos
   */
  public void setXoutBoard()
  {
    _isXedOut = true;
  }

  /**
   * @author Laura Cormos
   */
  private StringBuilder buildSpaceString()
  {
    StringBuilder spaces = new StringBuilder("");

    for (int i = 0; i < (_indent * _numOfSpaces); i++)
      spaces.append(_space);

    return spaces;
  }

  /**
   * @author Kee Chin Seong
   */
  public boolean isXoutBoard()
  {
      return _isXedOut;
  }

  /**
   * @author Kee Chin Seong
   */
  public void CheckXOutBoard()
  {
    //Chin Seong :: Xed-Out Process the XedOut Measure file
    if (TestExecution.getInstance().isXoutDetectionModeEnabled())
    {
      if (InspectionMeasurementsXMLWriter.getInstance().getProject().getPanel().getBoards().size() > 1)
      {
        decideBoardXoutStatus();
      }
    }
  }

  //Chin Seong :: To Set XedOut for the board
  private void decideBoardXoutStatus()
  {
    InspectionMeasurementsXMLWriter.getInstance().decideBoardXoutStatus(this);
  }

  //Chin Seong :: To Set XedOut for the board
  Board getBoard()
  {
    Assert.expect(_board != null);
    return _board;
  }
}

