package com.axi.v810.datastore.testResults;

import java.io.*;
import java.util.*;
import java.util.zip.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Laura Cormos
 */
public class ProjectCadXMLWriter
{
  private static ProjectCadXMLWriter _instance;
  private boolean _printSpaces = true;
  private boolean _exceptionThrown = false;
  private Adler32 _checksumUtil = new Adler32();
  private Project _project;
  private PrintWriter _os;
  private String _settingsFileName;
  private String _projName;
  private int _numOfSpaces = XMLWriterStringUtil.NUM_INDENT_SPACES;
  private String _beginCommentTag = XMLWriterStringUtil.BEGIN_COMMENT_TAG;
  private String _endCommentTag = XMLWriterStringUtil.END_COMMENT_TAG;
  private String _greaterThanTag = XMLWriterStringUtil.GREATER_THEN_TAG;
  private String _dbQuote = XMLWriterStringUtil.ESCAPED_DOUBLE_QUOTE;
  private String _elemEndTag  = XMLWriterStringUtil.ELEMENT_END_TAG;


  private String _dbQuoteGtTag = _dbQuote + _greaterThanTag;
  private String _dbQtElemEndTag = _dbQuote + _elemEndTag;

  /**
   * @author Laura Cormos
   */
  public static synchronized ProjectCadXMLWriter getInstance()
  {
    if (_instance == null)
      _instance = new ProjectCadXMLWriter();

    return _instance;
  }

  /**
   * @author Laura Cormos
   */
  public ProjectCadXMLWriter()
  {
    // do nothing
  }

  /**
   * @author Laura Cormos
   */
  private void printAndUpdateChecksum(String line)
  {
    Assert.expect(line != null);
    _checksumUtil.update(line.getBytes());
    _os.println(line);
  }

  /**
   * @author Bill Darbie
   */
  private void clear()
  {
    _project = null;
    _os = null;
  }

  /**
   * @author Laura Cormos
   */
  public void write(Project project, boolean printSpaces) throws DatastoreException
  {
    Assert.expect(project != null);
    _project = project;
    _printSpaces = printSpaces;
    _projName = project.getName();

    String projDirNameForProj = Directory.getProjectDir(_projName);
    // first write out the file to a temp location in case there are problems
//    String tempCadFileNamePath = projDirNameForProj + File.separator + "temp" + FileName.getCadFileExtension() + FileName.getXmlFileExtension();
    String tempCadFileNamePath = projDirNameForProj + File.separator +
                                 FileUtil.getTempFileName(_projName + FileName.getCadFileExtension() + FileName.getXmlFileExtension());
    _settingsFileName = FileName.getSettingsFileName(_project.getSettingsXmlChecksum());
    if (FileUtilAxi.exists(projDirNameForProj) == false)
      FileUtilAxi.mkdir(projDirNameForProj);
    int indent = 0;
    _checksumUtil.reset();

    try
    {
      _os = new PrintWriter(tempCadFileNamePath);
      // write the header and any comments
      printAndUpdateChecksum("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
      if (_printSpaces)
        printAndUpdateChecksum("");
      printAndUpdateChecksum(_beginCommentTag + " All rotations are in degrees " + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + " Units are in nanometers  " + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + " ALL x, y values are relative to the panel's origin" + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + " The origin is defined to be the lower left corner of the panel side as is physically viewed after all rotations " + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + " A panel's origin is at its lower left corner " + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + " A board's origin's x, y coordinate is at its lower left corner " + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + " A panel or board's width is along the x axis" + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + " A panel or board's length is along the y axis" + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + " A component's x, y coordinate is that of its center " + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + " A component's width is the dimension along the x axis at 0 degrees rotation " + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + " A component's length is the dimension along the y axis at 0 degrees rotation " + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + " A component's rotation has to be applied about its rotation point which is defined by an x, y coordinate relative to the panel's origin " + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + "    a component's rotation point is not necessarily its center point " + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + " A pad's x, y coordinate is that of its center " + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + " A pad's width is the dimension along the x axis at 0 degrees rotation " + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + " A pad's length is the dimension along the y axis at 0 degrees rotation " + _endCommentTag);
      printAndUpdateChecksum(_beginCommentTag + "    a pad is always rotated about its center point " + _endCommentTag);
      if (_printSpaces)
        printAndUpdateChecksum("");
      writeTopLevelElement(indent);

      long checksum = _checksumUtil.getValue();
      // get the checksum for the previous version of the cad.xml file, if any, so any previous copies of
      // this file can be deleted.
      long previousChecksum = _project.getCadXmlChecksum();
      _project.setCadXmlChecksum(checksum);

      // have to close the file before I can attempt to rename it
      if (_os != null)
        _os.close();

      try
      {
        // rename the temp cad.xml into the cad.checksum.xml file
        FileUtil.rename(tempCadFileNamePath, FileName.getCadFileNameFullPath(_projName, checksum));
      }
      catch (CouldNotDeleteFileException ex)
      {
        _exceptionThrown = true;
        CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(tempCadFileNamePath);
        dex.initCause(ex);
        throw dex;
      }
      catch (CouldNotRenameFileException ex)
      {
        _exceptionThrown = true;
        CannotWriteDatastoreException dex = new CannotWriteDatastoreException(FileName.getCadFileNameFullPath(_projName, checksum));
        dex.initCause(ex);
        throw dex;
      }        
      catch (FileNotFoundException ex)
      {
        FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(tempCadFileNamePath);
        dex.initCause(ex);
        throw dex;
      }
     
      // if a previous cad.previousChecksum.xml exists, delete it only if is different than the one we just created
      if (checksum != previousChecksum)
      {
        String previousCadFile = FileName.getCadFileNameFullPath(_projName, previousChecksum);
        if (FileUtil.exists(previousCadFile))
          try
          {
            FileUtil.delete(previousCadFile);
          }
          catch (CouldNotDeleteFileException ex)
          {
            _exceptionThrown = true;
            CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(previousCadFile);
            dex.initCause(ex);
            throw dex;
          }
      }
    }
    catch (FileNotFoundException ex)
    {
      _exceptionThrown = true;
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(tempCadFileNamePath);
      dex.initCause(ex);
      throw dex;
    }
    catch (RuntimeException ex)
    {
      _exceptionThrown = true;
      throw ex;
    }
    finally
    {
      clear();

      // if the temp file still exists, delete it
      if (FileUtil.exists(tempCadFileNamePath))
      {
        if (_exceptionThrown)
        {
          if (_os != null)
            _os.close();
        }
        try
        {
          FileUtil.delete(tempCadFileNamePath);
        }
        catch (CouldNotDeleteFileException ex)
        {
          if (_exceptionThrown)
          {
            ex.printStackTrace();
          }
          else
          {
            CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(tempCadFileNamePath);
            dex.initCause(ex);
            throw dex;
          }
        }
      }
    }
  }
  /**
   * @author Laura Cormos
   */
  private void writeTopLevelElement(int indent)
  {
    Assert.expect(indent >= 0);
    StringBuilder spaces = new StringBuilder("");
    printAndUpdateChecksum("<projectCad");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    printAndUpdateChecksum(spaces + "schemaName=" + _dbQuote + FileName.getCadXmlSchemaFileName() + _dbQuoteGtTag);
    indent++;
    writeSettingsFileElement(indent);
    writeProjectElement(indent);
    printAndUpdateChecksum("</projectCad>");
  }

  /**
   * @author Laura Cormos
   */
  private void writeSettingsFileElement(int indent)
  {
    Assert.expect(indent >= 0);
    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<settingsFile");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    Assert.expect(_settingsFileName != null);
    // do not use the settings file name for the cad checksum,
    // otherwise this will cause the cad checksum to be changed if
    // a setting is changed and this is undesired behavior
    _os.println(spaces + "name=" + _dbQuote + _settingsFileName + _dbQtElemEndTag);
  }

  /**
   * @author Laura Cormos
   */
  private void writeProjectElement(int indent)
  {
    Assert.expect(indent >= 0);
    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<project");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    printAndUpdateChecksum(spaces + "name=" + _dbQuote + _projName + _dbQuote);
    printAndUpdateChecksum(spaces + "version=" + _dbQuote + _project.getVersion() + _dbQuote);
    printAndUpdateChecksum(spaces + "softwareVersionDuringProjectCreation=" + _dbQuote + _project.getSoftwareVersionOfProjectLastSave() + _dbQuoteGtTag);
    indent++;
    writePanelElement(indent);
    String endElemSpaces = new String("");
    if (_printSpaces)
      endElemSpaces = spaces.substring(0, spaces.length() - _numOfSpaces);
    printAndUpdateChecksum(endElemSpaces + "</project>");
  }

  /**
   * @author Laura Cormos
   */
  private void writePanelElement(int indent)
  {
    Assert.expect(indent >= 0);
    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<panel");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    Panel panel = _project.getPanel();
    printAndUpdateChecksum(spaces + "width=" + _dbQuote + panel.getWidthAfterAllRotationsInNanoMeters() + _dbQuote);
    printAndUpdateChecksum(spaces + "length=" + _dbQuote + panel.getLengthAfterAllRotationsInNanoMeters() + _dbQuoteGtTag);
    indent++;
    List<Fiducial> panelFiducials = panel.getFiducials();
    for (Fiducial fiducial : panelFiducials)
      writeFiducialElement(fiducial, indent);
    List<Board> boards = panel.getBoards();
    for (Board board : boards)
      writeBoardElement(board, indent);
    String endElemSpaces = new String("");
    if (_printSpaces)
      endElemSpaces = spaces.substring(0, spaces.length() - _numOfSpaces);
    printAndUpdateChecksum(endElemSpaces + "</panel>");
  }

  /**
   * @author Laura Cormos
   */
  private void writeFiducialElement(Fiducial fiducial, int indent)
  {
    Assert.expect(indent >= 0);
    Assert.expect(fiducial != null);
    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<fiducial");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    printAndUpdateChecksum(spaces + "name=" + _dbQuote + fiducial.getName() + _dbQuote);
    if (fiducial.isTopSide())
      printAndUpdateChecksum(spaces + "topOrBottom=" + _dbQuote + "top" + _dbQuote);
    else
      printAndUpdateChecksum(spaces + "topOrBottom=" + _dbQuote + "bottom" + _dbQuote);
    PanelCoordinate fidPanelCoord = fiducial.getPanelCoordinateInNanoMeters();
    printAndUpdateChecksum(spaces + "x=" + _dbQuote + fidPanelCoord.getX() + _dbQuote);
    printAndUpdateChecksum(spaces + "y=" + _dbQuote + fidPanelCoord.getY() + _dbQuote);
    printAndUpdateChecksum(spaces + "width=" + _dbQuote + fiducial.getWidthAfterAllRotationsInNanoMeters() + _dbQuote);
    printAndUpdateChecksum(spaces + "length=" + _dbQuote + fiducial.getLengthAfterAllRotationsInNanoMeters() + _dbQuote);
    ShapeEnum fidShape = fiducial.getShapeEnum();
    if (fidShape.equals(ShapeEnum.CIRCLE))
      printAndUpdateChecksum(spaces + "shape=" + _dbQuote + "circle" + _dbQtElemEndTag);
    else if (fidShape.equals(ShapeEnum.RECTANGLE))
      printAndUpdateChecksum(spaces + "shape=" + _dbQuote + "rectangle" + _dbQtElemEndTag);
    else
      Assert.expect(false, "Unknown fiducial shape '" + fidShape.toString() + "' for fiducial: " + fiducial.getName());
  }

  /**
    * @author Laura Cormos
    */
   private void writeBoardElement(Board board, int indent)
   {
     Assert.expect(indent >= 0);
     Assert.expect(board != null);
     StringBuilder spaces = new StringBuilder("");
     if (_printSpaces)
       for (int i = 0; i < (indent * _numOfSpaces); i++)
         spaces.append(" ");
     printAndUpdateChecksum(spaces + "<board");
     // attribute indent
     if (_printSpaces)
       spaces.append("  ");
     printAndUpdateChecksum(spaces + "name=" + _dbQuote + board.getName() + _dbQuote);
     printAndUpdateChecksum(spaces + "boardTypeName=" + _dbQuote + board.getBoardType().getName() + _dbQuote);
     PanelCoordinate boardPanelCoord = board.getLowerLeftCoordinateRelativeToPanelInNanoMeters();
     printAndUpdateChecksum(spaces + "x=" + _dbQuote + boardPanelCoord.getX() + _dbQuote);
     printAndUpdateChecksum(spaces + "y=" + _dbQuote + boardPanelCoord.getY() + _dbQuote);
     printAndUpdateChecksum(spaces + "width=" + _dbQuote + board.getWidthAfterAllRotationsInNanoMeters() + _dbQuote);
     printAndUpdateChecksum(spaces + "length=" + _dbQuote + board.getLengthAfterAllRotationsInNanoMeters() + _dbQuoteGtTag);
     indent++;
     List<Fiducial> boardFiducials = board.getFiducials();
     for (Fiducial fiducial : boardFiducials)
       writeFiducialElement(fiducial, indent);
     List<Component> components = board.getComponents();
     for (Component component : components)
       writeComponentElement(component, indent);
     String endElemSpaces = new String("");
     if (_printSpaces)
       endElemSpaces = spaces.substring(0, spaces.length() - _numOfSpaces);
    printAndUpdateChecksum(endElemSpaces + "</board>");
   }

   /**
    * @author Laura Cormos
    */
   private void writeComponentElement(Component component, int indent)
   {
     Assert.expect(component != null);
     Assert.expect(indent >= 0);

     PanelCoordinate compCenterPanelCoord = component.getCenterCoordinateRelativeToPanelInNanoMeters();
     PanelCoordinate compRotationPanelCoord = component.getCoordinateRelativeToPanelInNanoMeters();
     boolean top = component.isTopSide();
     double totalRotation = component.getDegreesRotationAfterAllRotations();
     double relativeRotation = component.getDegreesRotationRelativeToBoard();

     StringBuilder spaces = new StringBuilder("");
     if (_printSpaces)
       for (int i = 0; i < (indent * _numOfSpaces); i++)
         spaces.append(" ");
     printAndUpdateChecksum(spaces + "<component");
     // attribute indent
     if (_printSpaces)
       spaces.append("  ");
     printAndUpdateChecksum(spaces + "referenceDesignator=" + _dbQuote + component.getReferenceDesignator() + _dbQuote);
     printAndUpdateChecksum(spaces + "landPatternName=" + _dbQuote + component.getLandPattern().getName() + _dbQuote);
     printAndUpdateChecksum(spaces + "topOrBottom=" + _dbQuote + calculateTopOrBottom(top) + _dbQuote);
     printAndUpdateChecksum(spaces + "x=" + _dbQuote + compCenterPanelCoord.getX() + _dbQuote);
     printAndUpdateChecksum(spaces + "y=" + _dbQuote + compCenterPanelCoord.getY() + _dbQuote);
     printAndUpdateChecksum(spaces + "width=" + _dbQuote + component.getWidthInNanoMeters() + _dbQuote);
     printAndUpdateChecksum(spaces + "length=" + _dbQuote + component.getLengthInNanoMeters() + _dbQuote);
     printAndUpdateChecksum(spaces + "rotationX=" + _dbQuote + compRotationPanelCoord.getX() + _dbQuote);
     printAndUpdateChecksum(spaces + "rotationY=" + _dbQuote + compRotationPanelCoord.getY() + _dbQuote);
     printAndUpdateChecksum(spaces + "rotation=" + _dbQuote + calculateRotation(totalRotation, relativeRotation, top, false) + _dbQuote);
     printAndUpdateChecksum(spaces + "numPins=" + _dbQuote + component.getPads().size() + _dbQuoteGtTag);
     indent++;
     List<Pad> pads = component.getPads();
     for (Pad pad : pads)
       writePadElement(pad, indent);
     String endElemSpaces = new String("");
     if (_printSpaces)
       endElemSpaces = spaces.substring(0, spaces.length() - _numOfSpaces);
    printAndUpdateChecksum(endElemSpaces + "</component>");
  }

  /**
   * @author Laura Cormos
   */
  private void writePadElement(Pad pad, int indent)
  {
    Assert.expect(indent >= 0);
    Assert.expect(pad != null);
    double totalRotation = pad.getDegreesRotationAfterAllRotations();
    double relativeRotation = pad.getDegreesRotation();
    boolean top = pad.getComponent().isTopSide();

    StringBuilder spaces = new StringBuilder("");
    if (_printSpaces)
      for (int i = 0; i < (indent * _numOfSpaces); i++)
        spaces.append(" ");
    printAndUpdateChecksum(spaces + "<pad");
    // attribute indent
    if (_printSpaces)
      spaces.append("  ");
    printAndUpdateChecksum(spaces + "name=" + _dbQuote + pad.getName() + _dbQuote);
    PanelCoordinate padPanelCoord = pad.getCenterCoordinateRelativeToPanelInNanoMeters();
    printAndUpdateChecksum(spaces + "x=" + _dbQuote + padPanelCoord.getX() + _dbQuote);
    printAndUpdateChecksum(spaces + "y=" + _dbQuote + padPanelCoord.getY() + _dbQuote);
    printAndUpdateChecksum(spaces + "width=" + _dbQuote + pad.getWidthInNanoMeters() + _dbQuote);
    printAndUpdateChecksum(spaces + "length=" + _dbQuote + pad.getLengthInNanoMeters() + _dbQuote);
//    printAndUpdateChecksum(spaces + "totalrotation=" + _dbQuote + totalRotation + _dbQuote);
//    printAndUpdateChecksum(spaces + "relativerotation=" + _dbQuote + relativeRotation + _dbQuote);
    printAndUpdateChecksum(spaces + "rotation=" + _dbQuote + calculateRotation(totalRotation, relativeRotation, top, true) + _dbQuote);
    ShapeEnum padShape = pad.getShapeEnum();
    if (padShape.equals(ShapeEnum.CIRCLE))
      printAndUpdateChecksum(spaces + "shape=" + _dbQuote + "circle" + _dbQuote);
    else if (padShape.equals(ShapeEnum.RECTANGLE))
      printAndUpdateChecksum(spaces + "shape=" + _dbQuote + "rectangle" + _dbQuote);
    else
      Assert.expect(false, "Unknown pad shape for pad: " + pad.getComponent().getReferenceDesignator() + "-" + pad.getName());
    if (pad.isSurfaceMountPad())
      printAndUpdateChecksum(spaces + "throughHoleOrSurfaceMount=" + _dbQuote + "surfaceMount" + _dbQuote);
    else if (pad.isThroughHolePad())
    {
      printAndUpdateChecksum(spaces + "throughHoleOrSurfaceMount=" + _dbQuote + "throughHole" + _dbQuote);
      printAndUpdateChecksum(spaces + "holeX=" + _dbQuote +
                 pad.getPadType().getThroughHoleLandPatternPad().getHoleCoordinateInNanoMeters().getX() + _dbQuote);
      printAndUpdateChecksum(spaces + "holeY=" + _dbQuote +
                 pad.getPadType().getThroughHoleLandPatternPad().getHoleCoordinateInNanoMeters().getY() + _dbQuote);
//      printAndUpdateChecksum(spaces + "holeDiameter=" + _dbQuote +
//                 pad.getPadType().getThroughHoleLandPatternPad().getHoleDiameterInNanoMeters() + _dbQuote);
      //Siew Yeng - XCR-3318 - Oval PTH
      printAndUpdateChecksum(spaces + "holeWidth=" + _dbQuote +
                 pad.getPadType().getThroughHoleLandPatternPad().getHoleWidthInNanoMeters()+ _dbQuote);
      printAndUpdateChecksum(spaces + "holeLength=" + _dbQuote +
                 pad.getPadType().getThroughHoleLandPatternPad().getHoleLengthInNanoMeters()+ _dbQuote);
    }
    else
      Assert.expect(false, "Unknown pad type");
    printAndUpdateChecksum(spaces + "jointType=" + _dbQuote + pad.getPackagePin().getJointTypeEnum() + _dbQuote);
    printAndUpdateChecksum(spaces + "orientation=" + _dbQuote + pad.getPinOrientationInDegreesAfterAllRotations() + _dbQuote);
    printAndUpdateChecksum(spaces + "nodeName=" + _dbQuote + pad.getNodeName() + _dbQtElemEndTag);
  }

  /**
   * @author Laura Cormos
   */
  private String calculateTopOrBottom(boolean top)
  {
    if (top)
      return "top";
    else
      return "bottom";
  }

  /**
   * Calculates the proper rotation for a component or pad after all rotations have been applied as viewed from
   * that side of the board (basically mirrored left-to-right)
   * @author Laura Cormos
   */
  private double calculateRotation(double totalRotation, double relativeRotation, boolean top, boolean pad)
  {
    // because the totalRotation and relativeRotation data returned from project description are all as viewed from
    // the x-ray source perspective, the rotation for bottom components and pads needs to be mirrored in a left-to-right way.
    // for pads the relativeRotation does not matter since it's already included in the totalRotation.
    if (top)
      return totalRotation;
    else
    {
      if (pad)
        return Math.abs(totalRotation - 180);
      else
        return MathUtil.getDegreesWithin0To359(totalRotation - 180 + 2*relativeRotation);
    }

  }

//  /**
//   * @author Laura Cormos
//   */
//  private int calculateXCoord(int xCoordRelativeToPanel, boolean top)
//  {
//    if (top)
//      return xCoordRelativeToPanel;
//    else
//      return (_project.getPanel().getWidthAfterAllRotationsInNanoMeters() - xCoordRelativeToPanel);
//  }
}
