package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when there is not enough hard drive space
 * to transfer the project offline
 *
 * @author George A. David
 */
public class InsufficientSpaceToTransferProjectOfflineDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param projectName the name of the project that cannot be added to the repository
   * @param offlinePath the path of the offline workstations that does not have enough disk space
   * @author George A. David
   */
  public InsufficientSpaceToTransferProjectOfflineDatastoreException(String projectName, String offlinePath)
  {
    super(new LocalizedString("DS_ERROR_INSUFFICIENT_SPACE_TO_TRANSFER_PROJECT_OFFLINE_KEY", new Object[]{projectName, offlinePath}));
    Assert.expect(projectName != null);
    Assert.expect(offlinePath != null);
  }
}
