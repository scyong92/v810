package com.axi.v810.datastore;

import com.axi.util.Assert;
import com.axi.util.LocalizedString;

/**
 * This exception is thrown when a properties file has a menu item shortcut that is a
 * duplicate of another item under that menu.
 * @author George Booth
 */
public class InvalidMenuItemShortcutInPropertiesDatastoreException extends DatastoreException
{
  /**
   * @author George Booth
   */
  public InvalidMenuItemShortcutInPropertiesDatastoreException(
      String menu1, String shortcut, String menu2, String menu3)
  {
    super(new LocalizedString("GUI_ERROR_INVALID_MENU_ITEM_SHORTCUT_KEY",
                              new Object[]{menu1, shortcut, menu2, menu3,
                              FileName.getSoftwareConfigFullPath()}));

    Assert.expect(menu1 != null);
    Assert.expect(shortcut != null);
    Assert.expect(menu2 != null);
    Assert.expect(menu3 != null);
  }
}
