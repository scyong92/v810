package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when an invalid value is found in a config file
 * for a particular key
 * @author Bill Darbie
 */
public class InvalidValueDatastoreException extends DatastoreException
{
  /**
   * @param key is the key that is missing
   * @param configFileName is the config file that the key belongs in
   * @author Bill Darbie
   */
  public InvalidValueDatastoreException(String key, String invalidValue, String configFileName)
  {
    super(new LocalizedString("DS_ERROR_CONFIG_INVALID_VALUE_KEY", new Object[]{key, invalidValue, configFileName}));
    Assert.expect(key != null);
    Assert.expect(invalidValue != null);
    Assert.expect(configFileName != null);
  }
}
