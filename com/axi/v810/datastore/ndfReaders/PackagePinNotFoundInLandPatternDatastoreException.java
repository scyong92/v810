package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This exception is thrown when a one-to-one mapping of pins to pad does not exits.
 * @author Eugene Kim-Leighton
 */
public class PackagePinNotFoundInLandPatternDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public PackagePinNotFoundInLandPatternDatastoreException(String referenceDesignator,
                                                           String packageName,
                                                           String pinName,
                                                           String landPatternName)

  {
    super(new LocalizedString("DS_NDF_ERROR_PACKAGE_PIN_NOT_FOUND_IN_LAND_PATTERN_KEY",
                              new Object[]{referenceDesignator,
                                           packageName,
                                           pinName,
                                           landPatternName}));

    Assert.expect(referenceDesignator != null);
    Assert.expect(packageName != null);
    Assert.expect(pinName != null);
    Assert.expect(landPatternName != null);
  }

  /**
   * @author Eugene Kim-Leighton
   */
  public PackagePinNotFoundInLandPatternDatastoreException(String fileName,
                                                           int lineNumber,
                                                           String packageName,
                                                           String pinName,
                                                           String landPatternName)

  {
    super(new LocalizedString("DS_ERROR_PACKAGE_PIN_NOT_FOUND_IN_LAND_PATTERN_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           packageName,
                                           pinName,
                                           landPatternName}));

    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(packageName != null);
    Assert.expect(pinName != null);
    Assert.expect(landPatternName != null);
  }
}
