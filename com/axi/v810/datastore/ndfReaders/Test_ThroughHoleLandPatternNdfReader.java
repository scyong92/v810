package com.axi.v810.datastore.ndfReaders;

import java.io.*;

import com.axi.util.*;

/**
 * Tests the ThroughHoldLandPatternNdfReader class.
 * @author George A. David
 */
class Test_ThroughHoleLandPatternNdfReader extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ThroughHoleLandPatternNdfReader());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String panelName = "smallPanel";
    String padgeomNdf = "padgeom.ndf";
    String dataDir = getTestDataDir();
    String padgeomNdfsDataDir = dataDir + File.separator + "throughHoleNdfs";
    String[] cornerCaseFiles = new String[]{
                                            //general testing
                                            "good.ndf",
                                            "extraSpaces.ndf",
                                            "extraData.ndf",
                                            "comment.ndf",
                                            "garbageLine.ndf",
                                            "empty.ndf",


                                            // testing the units attribute
                                            // .units: <value>
                                            "noUnit.ndf",
                                            "duplicateUnits.ndf",

                                            "missingUnitsValue.ndf",
                                            "invalidUnitsValue.ndf",


                                            // testing the checksum attribute
                                            "duplicateChecksum.ndf",


                                            //tesiting an unknown attribute
                                            "unknownAttribute.ndf",


                                            // testing data line
                                            // @<land pattern name> <pad number> <x hole coordinate> <y hole coordinate> <hole diameter> <pad shape> <x pad coordinate> <y pad coordinate> <pad width> <pad length>
                                            "missingLandPatternName.ndf",

                                            "missingPadNumber.ndf",
                                            "negativePadNumber.ndf",
                                            "duplicatePadNumber.ndf",
//                                            "padNumberIsDoubleValue.ndf",
//                                            "padNumberStartsWithLetter.ndf",
//                                            "padNumberEndsWithLetter.ndf",

                                            "missingXHoleCoordinate.ndf",
                                            "xHoleCoordinateBeginsWithLetter.ndf",
                                            "xHoleCoordinateEndsWithLetter.ndf",
                                            "xHoleCoordinateContainsTwoDecimalPoints.ndf",

                                            "missingYHoleCoordinate.ndf",
                                            "yHoleCoordinateBeginsWithLetter.ndf",
                                            "yHoleCoordinateEndsWithLetter.ndf",
                                            "yHoleCoordinateContainsTwoDecimalPoints.ndf",

                                            "missingHoleDiameter.ndf",
                                            "holeDiameterBeginsWithLetter.ndf",
                                            "holeDiameterEndsWithLetter.ndf",
                                            "negativeHoleDiameter.ndf",
                                            "zeroHoleDiameter.ndf",
                                            "holeDiameterContainsTwoDecimalPoints.ndf",

                                            "missingPadShape.ndf",
                                            "badPadShape.ndf",

                                            "missingXPadCoordinate.ndf",
                                            "xPadCoordinateBeginsWithLetter.ndf",
                                            "xPadCoordinateEndsWithLetter.ndf",
                                            "xPadCoordinateContainsTwoDecimalPoints.ndf",

                                            "missingYPadCoordinate.ndf",
                                            "yPadCoordinateBeginsWithLetter.ndf",
                                            "yPadCoordinateEndsWithLetter.ndf",
                                            "yPadCoordinateContainsTwoDecimalPoints.ndf",

                                            "missingPadWidth.ndf",
                                            "negativePadWidth.ndf",
                                            "padWidthBeginsWithLetter.ndf",
                                            "padWidthEndsWithLetter.ndf",
                                            "padWidthContainsTwoDecimalPoints.ndf",
                                            "zeroPadWidth.ndf",

                                            "missingPadLength.ndf",
                                            "zeroPadLength.ndf",
                                            "negativePadLength.ndf",
                                            "padLengthBeginsWithLetter.ndf",
                                            "padLengthEndsWithLetter.ndf",
                                            "padLengthContainsTwoDecimalPoints.ndf"
                                           };

    Test_ReaderUtil.testParser(panelName, padgeomNdf, padgeomNdfsDataDir, cornerCaseFiles);
 }
}

