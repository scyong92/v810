package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when we could not find a boolean value where one was required.
 * @author Vincent Wong
 */
public class MissingTokenBooleanDatastoreException extends DatastoreException
{
  /**
   * @author Vincent Wong
   */
  public MissingTokenBooleanDatastoreException(String fileName,
                                               int lineNumber,
                                               String token)
  {
    super(new LocalizedString("DS_NDF_ERROR_TOKEN_MISSING_TRUE_OR_FALSE_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           token}));

    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(token != null);
  }
}
