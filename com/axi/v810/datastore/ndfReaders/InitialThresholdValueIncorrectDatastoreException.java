package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * @author George A. David
 */
public class InitialThresholdValueIncorrectDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public InitialThresholdValueIncorrectDatastoreException(String thresholdFile, int lineNumber, String algorithmFamilyName, String algorithmName, String thresholdName)
  {
    super(new LocalizedString("DS_ERROR_INITIAL_THRESHOLD_VALUE_INCORRECT_KEY",
          new Object[]{thresholdFile, new Integer(lineNumber), algorithmFamilyName, algorithmName, thresholdName}));
    Assert.expect(thresholdFile != null);
    Assert.expect(algorithmFamilyName != null);
    Assert.expect(algorithmName != null);
    Assert.expect(thresholdName != null);
  }
}
