package com.axi.v810.datastore.ndfReaders;

import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This class reads the rptcad.rtf file to determine the maping of component id's to reference
 * designators.
 *
 * @author Bill Darbie
 */
public class ReportCadRtfReader
{
// wpd - I think I can remove this for virgo, comment out for now to be safe, then remove
//       once I am sure
//  private static ReportCadRtfReader _instance;
//  private String _fileName;
//  private FileChannel _fileChannel;
//  private MappedByteBuffer _buffer;
//  private CDataByteBufferUtil _bufferUtil;
//  private int _componentRecordOffset;
//  private int _componentOffsetOffset;
//  private int _compNameTableOffset;
//  private int _numComponents;
//  private int _numComponentNames; // this can be larger than _numComponents
//
//  private int _pinRecordOffset;
//  private int _pinOffsetOffset;
//  private int _pinNameTableOffset;
//  private int _alphaPinName;
//  private int _numPins;
//  private int _numPinNames;
//
//  private int[] _compNameIndex;
//  private int[] _compNameOffset;
//
//  private int[] _pinNameIndex;
//  private int[] _pinComponentIndex;
//  private int[] _pinNameOffset;
//  private Map _nameToComponentId;
//  private Map _pinIndexToPinNameMap;
//  private Map _compIndexToPinRecordMap;  // this is a map of a map:  (compID --> (PinName --> PinID))
//  private boolean _locationDataUsed;
//
//  /**
//   * @author Bill Darbie
//   */
//  public static synchronized ReportCadRtfReader getInstance()
//  {
//    if (_instance == null)
//      _instance = new ReportCadRtfReader();
//
//    return _instance;
//  }
//
//  /**
//   * @author Bill Darbie
//   */
//  private ReportCadRtfReader()
//  {
//    // do nothing
//  }
//
//  /**
//   * Parse the parts of the rptcad.rtf file for the given panel to get the component ID to reference
//   * designator mapping.
//   * @author Bill Darbie
//   */
//  public void parse(String reportCadFileName) throws DatastoreException
//  {
//    Assert.expect(reportCadFileName != null);
//    _fileName = reportCadFileName;
//
//    try
//    {
//      openStream();
//      parseHeaderTypeStructure();
//      parseComponentIndex();
//      parseComponentRecords();
//      parseComponentNames();
//
//
//      parsePinIndex();
//      parsePinRecords();
//      parsePinNames();
//    }
//    finally
//    {
//      closeStream();
//    }
//  }
//
//  /**
//   * return an array that maps component ID (Integer) to reference designators (String)
//   *
//   * @author Bill Darbie
//   */
//  public Map getNameToComponentIdMap()
//  {
//    Assert.expect(_nameToComponentId != null);
//
//    return _nameToComponentId;
//  }
//
//  /**
//   * return an array that maps component ID (Integer) to a map of pin name(String) to sorted joint number (Integer)
//   *
//   * @author Andy Mechtenberg
//   */
//  public Map getCompIDToPinRecordMap()
//  {
//    Assert.expect(_compIndexToPinRecordMap != null);
//
//    return _compIndexToPinRecordMap;
//  }
//
//  /*
//   * @author Bill Darbie
//   */
//  private void parseHeaderTypeStructure()
//  {
//    Assert.expect(_buffer != null);
//    Assert.expect(_bufferUtil != null);
//
//    // parse through the data.  Most of this data I do not care about.  Only the
//    // data that I set to member variables of this class are the ones I need
//    String id = _bufferUtil.getPascalPackedArray(8);
//    for(int i = 0; i < 3; ++i)
//    {
//      int checkSumHolder = _bufferUtil.getLong();
//    }
//    short formatVersion = _buffer.getShort();
//    short cadDataVersion = _buffer.getShort();
//    short locationDataUsed = _bufferUtil.getUnsignedChar();
//    if (locationDataUsed == 0)
//      _locationDataUsed = false;
//    else
//      _locationDataUsed = true;
//    _alphaPinName = (int) _bufferUtil.getUnsignedChar();
//    short flagBits = _buffer.getShort();
//
//    String boardName = _bufferUtil.getPascalString(80);
//    String derivedBoardName = _bufferUtil.getPascalString(80);
//
//    int boardWidth = _bufferUtil.getLong();
//    int boardHeight = _bufferUtil.getLong();
//    int boardThickness = _bufferUtil.getLong();
//
//    int numLayers = _bufferUtil.getLong();
//    _numComponents = (int)_bufferUtil.getLong();
//    int totalNumPins = _bufferUtil.getLong();
//    _numPins = (int)_bufferUtil.getLong();
//
//    int layerTableOffset = _bufferUtil.getLong();
//    _componentRecordOffset = (int)_bufferUtil.getLong();
//    _pinRecordOffset = (int)_bufferUtil.getLong();
//
//    int extensionHeaderOffset = _bufferUtil.getLong();
//    for(int i = 0; i < 3; ++i)
//    {
//       int reservedSpace = _bufferUtil.getLong();
//    }
//
//    int numLayerNames = _bufferUtil.getLong();
//    int layerNameIndexTableOffset = _bufferUtil.getLong();
//    int layerNameTableOffset = _bufferUtil.getLong();
//
//    int numPkgNames = _bufferUtil.getLong();
//    int pkgNameIndexTableOffset = _bufferUtil.getLong();
//    int pkgNameTableOffset = _bufferUtil.getLong();
//
//    _numComponentNames = (int)_bufferUtil.getLong();
//    // it is OK to have more names then components.  I think when a component is
//    // not tested you get this situation.
//    Assert.expect(_numComponentNames >= _numComponents);
//
//    _componentOffsetOffset = (int)_bufferUtil.getLong();
//    _compNameTableOffset = (int)_bufferUtil.getLong();
//
//    _numPinNames = (int)_bufferUtil.getLong();
//    _pinOffsetOffset = (int)_bufferUtil.getLong();
//    _pinNameTableOffset = (int)_bufferUtil.getLong();
//
//    int numJointTpNames = _bufferUtil.getLong();
//    int jointTpNameIndexTableOffset = _bufferUtil.getLong();
//    int jointTpNameTableOffset = _bufferUtil.getLong();
//  }
//
//  /**
//   * @author Bill Darbie
//   */
//  private void parseComponentRecords()
//  {
//    Assert.expect(_buffer != null);
//    Assert.expect(_bufferUtil != null);
//
//    // go to where the component records are and parse out the _componentNameIndex data
//    _buffer.position(_componentRecordOffset);
//
//    // now parse the component record type structure
//    _compNameIndex = new int[_numComponents];
//    for(int i = 0; i < _numComponents; ++i)
//    {
//      short layerTableIndex = _buffer.getShort();
//      int packageNameIndex = _bufferUtil.getLong();
//      int componentNameIndex = (int)_bufferUtil.getLong();
//      Assert.expect(componentNameIndex > 0);
//      Assert.expect(componentNameIndex <= _numComponentNames);
//
//      _compNameIndex[i] = componentNameIndex - 1;
//      int jointTypeNameIndex = _bufferUtil.getLong();
//      int firstPinSJN = _bufferUtil.getLong();
//      short totalNumberPins = _buffer.getShort();
//      short numberActualPins = _buffer.getShort();
//      if (_locationDataUsed)
//      {
//        int componentXLoc = _bufferUtil.getLong();
//        int componentYLoc = _bufferUtil.getLong();
//        int componentWidth = _bufferUtil.getLong();
//        int componentHeight = _bufferUtil.getLong();
//        short flagBits = _bufferUtil.getUnsignedChar();
//      }
//    }
//  }
//
//  /**
//   * @author Andy Mechtenberg
//   */
//  private void parsePinRecords()
//  {
//    Assert.expect(_buffer != null);
//    Assert.expect(_bufferUtil != null);
//
//    // go to where the pin records are and parse out the _offset1 data
//    _buffer.position(_pinRecordOffset);
//
//    // now parse the component record type structure
//    _pinNameIndex = new int[_numPins];
//    _pinComponentIndex = new int[_numPins];
//    for(int i = 0; i < _numPins; ++i)
//    {
//      short componentIndex = _buffer.getShort();
//      Assert.expect(componentIndex > 0);
//      Assert.expect(componentIndex <= _numComponents);
//
//      _pinComponentIndex[i] = componentIndex;
//      int pinNameIndex = (int)_bufferUtil.getLong();
//      _pinNameIndex[i] = pinNameIndex - 1;
//
//      short orientation = _buffer.getShort();
//      short subtype = _bufferUtil.getUnsignedChar();
//      if (_locationDataUsed)
//      {
//        int pinXLoc = _bufferUtil.getLong();
//        int pinYLoc = _bufferUtil.getLong();
//        int pinWidth = _bufferUtil.getLong();
//        int pinHeight = _bufferUtil.getLong();
//        short flagBits = _bufferUtil.getUnsignedChar();
//      }
//    }
//  }
//
//  /**
//   * @author Bill Darbie
//   */
//  private void parseComponentIndex()
//  {
//    Assert.expect(_buffer != null);
//    Assert.expect(_bufferUtil != null);
//
//    // go to the _componentOffsetOffset location and read out _compNameOffset values
//    _buffer.position(_componentOffsetOffset);
//    _compNameOffset = new int[_numComponentNames];
//    for(int i = 0; i < _numComponentNames; ++i)
//    {
//      _compNameOffset[i] = (int)_bufferUtil.getLong();
//    }
//  }
//
//  /**
//   * @author Andy Mechtenberg
//   */
//  private void parsePinIndex()
//  {
//    Assert.expect(_buffer != null);
//    Assert.expect(_bufferUtil != null);
//
//    // go to the _pinOffsetOffset location and read out _pinNameOffset values
//    _buffer.position(_pinOffsetOffset);
//    _pinNameOffset = new int[_numPinNames];
//    for(int i = 0; i < _numPinNames; ++i)
//    {
//      _pinNameOffset[i] = (int)_bufferUtil.getLong();
//    }
//  }
//
//  /**
//   * @author Bill Darbie
//   */
//  private void parseComponentNames()
//  {
//    Assert.expect(_buffer != null);
//    Assert.expect(_bufferUtil != null);
//
//    _buffer.position(_compNameTableOffset);
//    _nameToComponentId= new HashMap();
//
//    for(int compID = 0; compID < _numComponents; ++compID)
//    {
//      int index1 = _compNameIndex[compID];
//      int index2 = _compNameOffset[index1];
//      int index3 = _compNameTableOffset + index2;
//      _buffer.position(index3);
//      String name = _bufferUtil.getPascalString(128);
//      Integer id = new Integer(compID + 1);  // all indexes are 1 based, but this for loop is 0 based, so add 1
//      if (_nameToComponentId.put(name, id) != null)
//        Assert.expect(false);
//    }
//  }
//
//  /**
//   * @author Erica Wheatcroft
//   */
//  private void parsePinNames()
//  {
//    Assert.expect(_buffer != null);
//    Assert.expect(_bufferUtil != null);
//
//    _buffer.position(_pinNameTableOffset);
//    if(_alphaPinName == 0 )
//    {
//      // alpha pin names are not being used
//      _compIndexToPinRecordMap= new HashMap();
//      for (int sjn = 0; sjn < _numPins; ++sjn)
//      {
//        Integer sjnInteger = new Integer(sjn);
//        int compID = _pinComponentIndex[sjn];
//        Integer compInteger = new Integer(compID);
//        // add back the 1 we took out when it really was in index.  Now that it's the pin number, restore back to normal
//        Integer legacyPinNumber = new Integer(_pinNameIndex[sjn] + 1);
//
//        // now we have all three elements to make our datastructure: compID, legacyPinNumber and sjn
//        Map legacyPinNumberToPinIDMap =(Map)_compIndexToPinRecordMap.get(compInteger);
//        if (legacyPinNumberToPinIDMap == null)  // new component index, create the map
//          legacyPinNumberToPinIDMap = new HashMap();
//
//        legacyPinNumberToPinIDMap.put( legacyPinNumber.toString(), sjnInteger);
//        _compIndexToPinRecordMap.put(compInteger, legacyPinNumberToPinIDMap);
//      }
//    }
//    else
//    {
//      // alpha pin names are being used
//      _pinIndexToPinNameMap = new HashMap();
//
//      for(int pinID = 0; pinID < _numPins; ++pinID)
//      {
//        int index1 = _pinNameIndex[pinID];
//        int index2 = _pinNameOffset[index1];
//        int index3 = _pinNameTableOffset + index2;
//        _buffer.position(index3);
//        String name = _bufferUtil.getPascalString(128);
//        // all indexes are 1 based, but this for loop is 0 based, so add 1
//        Integer id = new Integer(pinID + 1);
//        if (_pinIndexToPinNameMap.put(id, name) != null)
//          Assert.expect(false);
//      }
//      createComponentIndexToPinRecordMap();
//    }
//  }
//
//  /**
//   * @author Erica Wheatcroft
//   * @author Bill Darbie
//   */
//  private void createComponentIndexToPinRecordMap()
//  {
//    Assert.expect(_buffer != null);
//    Assert.expect(_bufferUtil != null);
//
//    _buffer.position(_pinNameTableOffset);
//    _compIndexToPinRecordMap= new HashMap();
//
//    int count = 1;
//    for (int sjn = 0; sjn < _numPins; ++sjn)
//    {
//      Integer sjnInteger = new Integer(sjn);
//      int compID = _pinComponentIndex[sjn];
//
//      Integer compInteger = new Integer(compID);
//      String pinName = _pinIndexToPinNameMap.get(new Integer(sjn + 1)).toString();
//
//      // the new data structure will be a map compId -> pin Name -> sjn
//      Map legacyPinNameToPinIDMap =(Map)_compIndexToPinRecordMap.get(compInteger);
//
//      // new component index, create the map
//      if (legacyPinNameToPinIDMap == null)
//        legacyPinNameToPinIDMap = new HashMap();
//
//      legacyPinNameToPinIDMap.put(pinName, sjnInteger);
//      _compIndexToPinRecordMap.put(compInteger, legacyPinNameToPinIDMap);
//
//    }
//  }
//
//  /**
//   * @author Bill Darbie
//   */
//  private void openStream() throws DatastoreException
//  {
//    try
//    {
//      File file = new File(_fileName);
//      FileInputStream fis = new FileInputStream(_fileName);
//      _fileChannel = fis.getChannel();
//      _buffer = _fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, file.length());
//      _buffer.order(ByteOrder.LITTLE_ENDIAN);
//      _bufferUtil = new CDataByteBufferUtil(_buffer);
//    }
//    catch(FileNotFoundException e)
//    {
//      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(_fileName);
//      dex.initCause(e);
//      throw dex;
//    }
//    catch(IOException ioe)
//    {
//      DatastoreException dex = new DatastoreException(_fileName);
//      dex.initCause(ioe);
//      throw dex;
//    }
//  }
//
//  /**
//   * @author Bill Darbie
//   */
//  private void closeStream()
//  {
//    try
//    {
//      if (_fileChannel != null)
//      _fileChannel.close();
//    }
//    catch(IOException e)
//    {
//      e.printStackTrace();
//    }
//    _buffer = null;
//    _bufferUtil = null;
//  }
}
