package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This exception is thrown if an invalid Pad name is found in the pins.ndf file.
 * @author Bill Darbie
 */
public class InvalidPadNumberDatastoreException extends DatastoreException
{
  /**
   * @author Kristin Casterton
   */
  public InvalidPadNumberDatastoreException(String fileName,
                                            int lineNumber,
                                            String padNumber,
                                            String throughHoleLandPatternNdfFileName,
                                            String surfaceMountLandPatternNdfFileName,
                                            String landPatternName)
  {
    super(new LocalizedString("DS_ERROR_INVALID_PAD_NUMBER_KEY",
          new Object[]{fileName,
                       new Integer(lineNumber),
                       padNumber,
                       throughHoleLandPatternNdfFileName,
                       surfaceMountLandPatternNdfFileName,
                       landPatternName}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(padNumber != null);
    Assert.expect(throughHoleLandPatternNdfFileName != null);
    Assert.expect(surfaceMountLandPatternNdfFileName != null);
    Assert.expect(landPatternName != null);
  }
}
