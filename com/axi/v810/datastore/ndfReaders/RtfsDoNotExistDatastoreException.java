package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when no rtf file exist and the code is expecting them to be there.
 * @author Bill Darbie
 */
public class RtfsDoNotExistDatastoreException extends DatastoreException
{
  /*
   * @author Bill Darbie
   */
  public RtfsDoNotExistDatastoreException(String panelName)
  {
    super(new LocalizedString("DS_ERROR_RTFS_DO_NOT_EXIST_KEY", new Object[]{panelName}));
    Assert.expect(panelName != null);
  }
}
