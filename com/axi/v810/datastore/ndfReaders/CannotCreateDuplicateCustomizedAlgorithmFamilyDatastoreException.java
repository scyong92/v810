package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class CannotCreateDuplicateCustomizedAlgorithmFamilyDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public CannotCreateDuplicateCustomizedAlgorithmFamilyDatastoreException(String name)
  {
    super(new LocalizedString("DS_ERROR_CANNOT_CREATE_DUPLICATE_CUSTOMIZED_ALGORITHM_FAMILY_KEY",
          new Object[]{name}));
    Assert.expect(name != null);
  }
}
