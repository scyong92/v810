package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when an invalid Ndf is encountered
 * @author Punit
 */
public class InvalidNdfException extends DatastoreException
{
  public InvalidNdfException()
  {
    super(new LocalizedString("DS_INVALID_NDF_WARNING_KEY", null));
  }
}