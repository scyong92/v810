package com.axi.v810.datastore.ndfReaders;

import java.io.*;
import java.util.*;

import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.util.*;

class Test_ComponentNdfReader extends UnitTest
{

  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ComponentNdfReader());
  }

  /**
   * @author Keith Lee
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String panelName = "smallPanel";
    String componentNdf = "pads_asc_A2" + File.separator + "componen.ndf";
    String dataDir = getTestDataDir();
    String componentNdfsDataDir = dataDir + File.separator + "componentNdfs";
    String[] cornerCaseFiles = new String[]{
                                             "good.ndf",
                                            "empty.ndf",
                                            "extraSpaces.ndf",
                                            "extraData.ndf",
                                            "garbageLine.ndf",
                                            "comments.ndf",
                                            "duplicateRefDes.ndf",
                                            "duplicateRefDes1.ndf",
                                            "missingRefDes.ndf",
                                            "missingTestStatus.ndf",
                                            "missingPackageName.ndf",
                                            "missingTechnology.ndf",
                                            "missingSubtype.ndf",
                                            "missingSubtypeComponentFalse.ndf",
                                            "corruptRefDes.ndf",
                                            "corruptTestStatus.ndf",
                                            "corruptPackage.ndf",
                                            "corruptTechnology.ndf",
                                            "corruptSubtype.ndf",
                                            "corruptSubtype1.ndf",
                                            "corruptSubtype2.ndf",
                                            "corruptSubtype3.ndf",
                                            "negativeSubtype.ndf",
                                            "outOfRangeSubtype.ndf",
                                            "hasBoardId.ndf",
                                            "hasBoardId1.ndf",
                                            "hasBoardId2.ndf",
                                            "hasUnit.ndf"
                                            };
    Test_ReaderUtil.testParser(panelName, componentNdf, componentNdfsDataDir, cornerCaseFiles);
 }
}

