package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class throws an exception if there a pin located outside of the pad region.
 * A pin's center must be located inside of the pad.
 *
 * @author Eugene Kim-Leighton
 */
public class PinOutsidePadRegionDatastoreException extends DatastoreException
{
  public PinOutsidePadRegionDatastoreException(String referenceDesignator,
                                               String packageFileName,
                                               String packageName,
                                               String pinName,
                                               String landPatternFileName,
                                               String landPatternName)
  {
    super(new LocalizedString("DS_NDF_ERROR_PIN_OUTSIDE_PAD_REGION_KEY",
                              new Object[]{referenceDesignator,
                                           packageFileName,
                                           packageName,
                                           pinName,
                                           landPatternFileName,
                                           landPatternName}));
    Assert.expect(referenceDesignator != null);
    Assert.expect(packageFileName != null);
    Assert.expect(packageName != null);
    Assert.expect(pinName != null);
    Assert.expect(landPatternFileName != null);
    Assert.expect(landPatternName != null);
  }
}
