package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when a file is not found.
 * @author Bill Darbie
 */
public class RtfsAreOlderThanNdfsDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param panelName is the name of the panel that has older rtf files than ndf files
   * @author Bill Darbie
   */
  public RtfsAreOlderThanNdfsDatastoreException(String panelName)
  {
    super(new LocalizedString("DS_ERROR_RTFS_OLDER_THAN_NDFS_KEY", new Object[]{panelName}));
    Assert.expect(panelName != null);
  }
}
