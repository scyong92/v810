package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This exception occurs when a subtype has been reference but does not exist in any component ndf file.
 * @author Erica Wheatcroft
 */
public class BadSubtypeReferenceDatastoreException extends DatastoreException
{
  /**
   * @author Erica Wheatcroft
   */
  public BadSubtypeReferenceDatastoreException(String fileName,
                                               int lineNumber,
                                               String algorithmFamilyName,
                                               String subTypeNumber)

  {
    super(new LocalizedString("DS_ERROR_BAD_SUBTYPE_REFERENCE_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           algorithmFamilyName,
                                           subTypeNumber}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(algorithmFamilyName != null);
    Assert.expect(subTypeNumber != null);
  }
}
