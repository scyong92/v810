package com.axi.v810.datastore.ndfReaders;

import java.io.*;

import com.axi.util.*;

/**
 * @author George A. David
 */
class Test_BoardNdfReader extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_BoardNdfReader());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String panelName = "smallPanel";
    String boardNdf = "board_a1" + File.separator + "board.ndf";
    String dataDir = getTestDataDir();
    String boardNdfsDataDir = dataDir + File.separator + "boardNdfs";
    String[] cornerCaseFiles = new String[]{
                                            // general test cases
                                            "good.ndf",
                                            "extraSpaces.ndf",
                                            "extraData.ndf",
                                            "comment.ndf",
                                            "garbageLine.ndf",
                                            "empty.ndf",


                                            // testing the dataline
                                            //"@<ref designator> <x coordinate> <y coordinate> <degrees rotation> <land pattern name>"
                                            "noDataLines.ndf",
                                            "missingComponent.ndf",
                                            "missingReferenceDesignator.ndf",
                                            "duplicateReferenceDesignator.ndf",
                                            "invalidReferenceDesignator.ndf",

                                            "missingXCoordinate.ndf",
                                            "negativeXCoordinate.ndf",
                                            "xCoordinateBeginsWithLetter.ndf",
                                            "xCoordinateEndsWithLetter.ndf",

                                            "missingYCoordinate.ndf",
                                            "negativeYCoordinate.ndf",
                                            "yCoordinateBeginsWithLetter.ndf",
                                            "yCoordinateEndsWithLetter.ndf",

                                            "missingDegreesRotation.ndf",
                                            "negativeDegreesRotation.ndf",
                                            "degreesRotationBeginsWithLetter.ndf",
                                            "degreesRotationEndsWithLetter.ndf",
                                            "degreesRotationOutOfRange.ndf",
                                            "degreesRotationContainsTwoDecimalPoints.ndf",

                                            "missingLandPatterName.ndf",
                                            "badLandPattern.ndf",


                                            // testing the alignment pads attribute line
                                            //.alignment_pads: <alignment group number> <ref. des.> <pin name> [<ref designator> <pin name> ..]
                                            "missingAlignmentGroupNumber.ndf",
                                            "invalidAlignmentGroupNumber.ndf",
                                            "alignmentGroupNumberBeginsWithLetter.ndf",
                                            "alignmentGroupNumberEndsWithLetter.ndf",
                                            "negativeAlignmentGroupNumber.ndf",
                                            "duplicateAlignmentGroupNumber.ndf",

                                            "missingAlignmentGroupReferenceDesignator.ndf",
                                            "invalidAlignmentGroupReferenceDesignator.ndf",
                                            "alignmentGroupComponentOnWrongSide.ndf",

                                            "missingAlignmentGroupPinNumber.ndf",
                                            "invalidAlignmentGroupPinNumber.ndf",
                                            "tooManyAlignmentPads.ndf",


                                            // testing the dimensions attribute line
                                            // .dimensions: <board width> <board length> <pad to pad thickness>
                                            "duplicateDimensionsAttribute.ndf",
                                            "missingDimensions.ndf",

                                            "missingBoardWidth.ndf",
                                            "boardWidthBeginsWithLetter.ndf",
                                            "boardWidthEndsWithLetter.ndf",
                                            "negativeBoardWidth.ndf",
                                            "zeroBoardWidth.ndf",

                                            "missingBoardLength.ndf",
                                            "boardLengthBeginsWithLetter.ndf",
                                            "boardLengthEndsWithLetter.ndf",
                                            "negativeBoardLength.ndf",
                                            "zeroBoardLength.ndf",

                                            "missingPadToPadThickness.ndf",
                                            "negativePadToPadThickness.ndf",
                                            "padToPadThicknessBeginsWithLetter.ndf",
                                            "padToPadThicknessEndsWithLetter.ndf",
                                            "zeroPadToPadThickness.ndf",


                                            // testing the board id attribute line
                                            // .board_id: <board name>
                                            "duplicateBoardIdAttribute.ndf",

                                            "missingBoardName.ndf",


                                            // testing the default pitch attribute line
                                            // .default_pitch: <pitch>
                                            "duplicateDefaultPitchAttribute.ndf",

                                            "missingDefaultPitchValue.ndf",
                                            "defaultPitchBeginsWithLetter.ndf",
                                            "defaultPitchEndsWithLetter.ndf",
                                            "negativeDefaultPitch.ndf",
                                            "zeroDefaultPitch.ndf",


                                            // testing the default surface map density attribute line
                                            // .surface_map_density: <density>
                                            "duplicateSurfaceMapDensityAttribute.ndf",

                                            "missingSurfaceMapDensity.ndf",
                                            "negativeSurfaceMapDensity.ndf",
                                            "surfaceMapDensityBeginsWithLetter.ndf",
                                            "surfaceMapDensityEndsWithLetter.ndf",
                                            "zeroSurfaceMapDensity.ndf",

                                            // testing the units attribute line
                                            // .units: <value>
                                            "duplicateUnitsAttribute.ndf",
                                            "missingUnitsAttribute.ndf",

                                            "missingUnitsToken.ndf",
                                            "invalidUnitsValue.ndf",


                                            // testing the thickness pads attribute line
                                            // .thickness_pads: <focus height> <camera index> <snap margin> <fov> <technique> <ref designator> <pin name> [<ref designator> <pin name> ..]
                                            "goodThicknessPads.ndf",

                                            "missingFocusHeight.ndf",
                                            "focusHeightBeginsWithLetter.ndf",
                                            "focusHeightEndsWithLetter.ndf",

                                            "missingCameraIndex.ndf",
                                            "negativeCameraIndex.ndf",
                                            "cameraIndexBeginsWithLetter.ndf",
                                            "cameraIndexEndsWithLetter.ndf",
                                            "cameraIndexIsDouble.ndf",

                                            "missingSnapMargin.ndf",
                                            "negativeSnapMargin.ndf",
                                            "snapMarginBeginsWithLetter.ndf",
                                            "snapMarginEndsWithLetter.ndf",
                                            "snapMarginContainsTwoDecimalPoints.ndf",

                                            "missingFieldOfView.ndf",
                                            "fieldOfViewBeginsWithLetter.ndf",
                                            "fieldOfViewEndsWithLetter.ndf",
                                            "negativeFieldOfView.ndf",

                                            "missingAutoFocusTechnique.ndf",
                                            "invalidAutoFocusTechnique.ndf",

                                            "missingThicknessPadsRefDes.ndf",
                                            "invalidThicknessPadsRefDes.ndf",

                                            "missingThicknessPadsPinNumber.ndf",
                                            "invalidThicknessPadsPinNumber.ndf",


                                            // testing the align1 attribute line
                                            //.align1: <ref. des.> <pin name>
                                            "goodAlignX.ndf",
                                            "alignmentPads1AlreadyDefined.ndf",
                                            "alignmentPads1DefinedLater.ndf",
                                            "duplicateAlign1.ndf",

                                            "missingAlign1ReferenceDesignator.ndf",
                                            "invalidAlign1ReferenceDesignator.ndf",

                                            "missingAlign1PinNumber.ndf",
                                            "invalidAlign1PinNumber.ndf",


                                            // testing the align2 attribute line
                                            //.align2: <ref. des.> <pin name>
                                            "alignmentPads2AlreadyDefined.ndf",
                                            "alignmentPads2DefinedLater.ndf",
                                            "duplicateAlign2.ndf",

                                            "missingAlign2ReferenceDesignator.ndf",
                                            "invalidAlign2ReferenceDesignator.ndf",

                                            "missingAlign2PinNumber.ndf",
                                            "invalidAlign2PinNumber.ndf",


                                            // testing the align3 attribute line
                                            //.align3: <ref. des.> <pin name>
                                            "alignmentPads3AlreadyDefined.ndf",
                                            "duplicateAlign3.ndf",

                                            "missingAlign3ReferenceDesignator.ndf",
                                            "invalidAlign3ReferenceDesignator.ndf",

                                            "missingAlign3PinNumber.ndf",
                                            "invalidAlign3PinNumber.ndf",
                                           };

    Test_ReaderUtil.testParser(panelName, boardNdf, boardNdfsDataDir, cornerCaseFiles);
  }
}
