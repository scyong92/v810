package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when we find something besides a number where a number was required.
 * @author Bill Darbie
 */
public class InvalidThresholdNameDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public InvalidThresholdNameDatastoreException(String fileName,
                                                int lineNumber,
                                                String actual)
  {
    super(new LocalizedString("DS_ERROR_INVALID_THRESHOLD_NAME_KEY", new Object[]{fileName, new Integer(lineNumber), actual}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(actual != null);
  }
}
