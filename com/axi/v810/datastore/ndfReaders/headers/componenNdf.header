#####################################################################################
# FILE: componen.ndf
# PURPOSE: The component files list all components on a board side,
#     their test status (tested,
#     not tested, sampled) the package name, the technology (surface mount or
#     through-hole) and the subtype to be used to test them.  Algorithm family
#     (joint type) information that the subtype refers to is in the package file.
#     There is one component file for each board side for each unique board on
#     the panel.  The files reside in a subdirectory for that board side.
#
#   There are no attributes used in the component files.
#
#   Data lines:
#    description of entry  syntax of entry     explanation
#    --------------------   ---------------    -----------
#    @<ref. des.>          alphanumeric
#
#     <test status>        TRUE|FALSE|PARTIAL  if all components in a view are
#                                              PARTIAL, the view is tested on a
#                                              sampling basis specified by
#                                              .COMPONENT_SAMPLE_PERCENT in the
#                                              panel file or globally
#                                              (configured in System Access,
#                                              Configure, Sampling Mode
#                                              Configuration.)
#
#     <package name>       alphanumeric        must match name in package file
#
#     <technology>         SM|TH               surface mount or through-hole
#
#     <subtype number>     integer (0-255)     if there is no entry, 0 is
#                                              assumed.
#
#####################################################################################