#####################################################################################
# FILE: package.ndf
# PURPOSE: File contains a list of pin locations and sizes for each package type.
#    It also specifies the pitch and algorithm family (joint type) to use for
#    testing.
#
#  Optional attributes:
#    .UNIT: inches | mils | millimeters
#      If this attribute is missing, mils is assumed.
#
#  Data lines:
#   description of entry   syntax of entry    explanation
#    --------------------   ---------------   -----------
#    @<package name>        <alphanumeric>    must match the name in the
#                                             component file.
#
#     <pitch>               <float>           pin width plus interpin distance
#                                             (IPD)
#
#     <composition>         <PLASTIC|CERAMIC> this field is ignored
#
#     <algorithm family>    <BGA2|CHIP|etc.>  list of valid families found in
#                                             famalgnm.dat.
#
#     <pin number>          <integer>         the pin number
#
#     <x location>          <float>           from package reference point
#                                             (centroid) to pin centroid
#
#     <y location>          <float>	      from package reference point
#                                             centroid) to pin centroid
#
#     <shape>               <R|C>             Rectangular or Circular (obround)
#
#     <pin x size>          <float>           width of the pin
#
#     <pin y size>          <float>           length of the pin
#
#     <orientation>         <0|90|180|270>    Optional pin orientation field.
#                                             Uses values inferred from
#                                             package definition if absent.
#

#####################################################################################
.UNIT: mils

# packageId  pitch  composition  algFamily  PinNum   pinXloc   pinYloc  pinShape   pinXsize   pinYsize   pinOrientation
# invalid x location
# @SM00137_C 68.75 PLASTIC Chip 2 -32 0 R 3 44 180
  @SM00137_C 68.75 PLASTIC Chip 2 32 0.2.4 R 3 44 180
@SM00137_C 68.75 PLASTIC Chip 1 32 0 R 3 44 0

@SM00135_C 87.5 PLASTIC Chip 2 -50 0 R 4 56 180
@SM00135_C 87.5 PLASTIC Chip 1 50 0 R 4 56 0

@SM00079_L 182.5 PLASTIC Gullwing 2 -80 0 R 5 116.8 180
@SM00079_L 182.5 PLASTIC Gullwing 1 80 0 R 5 116.8 0

@1206_R 75 PLASTIC Res 2 67.5 0 R 6 48 0
@1206_R 75 PLASTIC Res 1 -67.5 0 R 6 48 180

@6032_C 112.5 PLASTIC Chip 2 -120 0 R 12 72 180
@6032_C 112.5 PLASTIC Chip 1 120 0 R 12 72 0

@1206_C 75 PLASTIC Chip 2 67.5 0 R 6 48 0
@1206_C 75 PLASTIC Chip 1 -67.5 0 R 6 48 180


@26PINCONN_JJ 57.75 PLASTIC PTH 2 50 50 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 3 100 0 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 4 150 50 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 5 200 0 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 6 250 50 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 7 300 0 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 8 350 50 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 9 400 0 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 10 450 50 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 11 500 0 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 12 550 50 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 13 600 0 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 14 650 50 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 15 700 0 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 16 750 50 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 17 800 0 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 18 850 50 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 19 900 0 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 20 950 50 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 21 1000 0 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 22 1050 50 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 23 1100 0 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 24 1150 50 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 25 1200 0 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 26 1250 50 C 28 28 0
@26PINCONN_JJ 57.75 PLASTIC PTH 1 0 0 C 28 28 0

@TO_213AA_QQ 112.5 PLASTIC PTH 1 0 0 C 52 52 0
@TO_213AA_QQ 112.5 PLASTIC PTH 2 0 200 C 52 52 0
@TO_213AA_QQ 212.5 PLASTIC PTH 4 380 100 C 100 100 0
@TO_213AA_QQ 212.5 PLASTIC PTH 3 -580 100 C 100 100 0

@6032_G 112.5 PLASTIC Gullwing 2 -120 0 R 12 72 180
@6032_G 112.5 PLASTIC Gullwing 1 120 0 R 12 72 0
