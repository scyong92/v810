#####################################################################################
# FILE: panel.ndf
# PURPOSE: The panel ndf contains a list of panel dimensions and board
#          offsets.  It also controls the numbering of boards.
# SYNTAX:
#  Required Attributes:
#    .DIMENSIONS: <panel width> <panel length> <thickness>
#
#  Optional Attributes:
#    .UNIT: inches | mils | millimeters
#       If this attribute is missing, mils is assumed.
#
#    .SOFTWARE_REVISION: <80|81|82|83|90|...>
#       This lists which version of software was used to develop the initial set
#       of ndfs for this panel.
#
#    .SUBPANEL_ID: <panel name>
#       No spaces allowed. If missing no name is used.
#
#    .ROTATION: 0 | 90 | 180 | 270
#       If missing 0 is default.
#
#    .MATERIALS: default | cu_6337 | <custom setting>
#       This setting controls which solder thickness settings are used.  The
#       custom setting name given needs to match the name of the files that have
#       these settings (for example 5dx/rXX/calib/cu_6337.400 would contain
#       the settings for the 400 field of view for the cu_6337 set up.)
#       If this line is missing then the pre-6.0 solder thickness settings are
#       used.  In general this it is recommended that the .MATERIALS line is
#       included.  The default and cu_6337 settings are identical.
#
#    .STAGE_SPEED: <1 through 20>
#       This setting controls how the stage moves.  There is no consistent
#       pattern to what bigger/smaller numbers mean (bigger is generally faster
#       in the same decade).  Only 11-20 are available for Series 3.
#       1-10 are the options for all other systems.
#
#    .LASER_MAP_STAGE_SPEED: <1 through 20>
#       This setting controls how the stage moves during surface mapping.  See
#       STAGE_SPEED above.
#
#    .SURFACE_MAP_ALIGNMENT_VIEWS: <true | false>
#       This is usually omitted if not set to true.  If true the system will go
#       to the map point nearest each alignment view and base the align height for
#       that point on that reading.
#
#    .COMPONENT_SAMPLE_PERCENT: <integer>
#       This is usually omitted if set to 100. A view is classified PARTIAL if the
#       test status of each component in that view is PARTIAL (see componen.ndf).
#       PARTIAL test views are tested N percent, where N is specified by the
#       component sampling percentage.
#
#    .DUAL_PANEL_TEST_CAD: <true | false>
#       This is usually omitted if not set to true.  If true and the system is
#       set to SINGLE PANEL mode (<%RELROOT%>/CALIB/Machine.cfg), the system will
#       only test the first half of the defined boards.  If this is false or not
#       present and the system is in SINGLE PANEL mode, it will attempt to test
#       all boards.  Valid only on Series 3 machines (and beyond?)
#
#    .PANEL_LOAD_TRAILING_EDGE_DETECT: <true | false>
#       This is usually omitted if not set to true. If true the system will begin
#       closing the outer barriers as soon as the panel is no longer sensed by
#       the panel clear sensors.  Otherwise it will wait until the panel is
#       sensed by the Panel In Place Sensor on load or the downstream conveyor on
#       unload.  Valid only for loaderless systems (Series 2L and 3).
#
#    .CONTAINS_DIVIDED_BRD: <true | false>
#       This is usually omitted if not set to true.  Used for Awaretest.
#       Awaretest normally does not support multi-board panels.  This token is to
#       let Awaretest know that this is actually a single board.
#
#    .DEFECT_ANALYZER: <true | false>
#       This is usually omitted if not set to true. If true, views with failing
#       joints will be retested.  A joint must fail each test to be called as a
#       defect.
#
#    .ACCEPT_ILLEGAL_IDENTIFIERS: <true | false>
#       This is usually omitted if not set to true.  If true, the compiler will
#       allow the use of suspect characters in names of all NDF files (these are
#       flagged as warnings in compilation report).  If false or missing, these
#       characters generate errors upon compile.  The following characters are
#       not allowed: ""#',./:;<>@\`|";  Furthermore, a dash ("-") is also
#       illegal in reference designators and board ids. Not explicitly checked
#       for are spaces " " since files are spaced delimited.
#
#    .AWARETEST_ON: <true | false>
#       This is usually omitted if not set to true.  If true an rtf file called
#       complist.txt is generated.  This data is sent to the development 3070
#       using the info in at Config.ini.
#
#    .PAUSE_ON_AUTO_ALIGN: <true | false>
#       This is usually omitted if not set to true.  If true system will stop at
#       each alignment point during any automatic alignment procedure (Align,
#       Automatic, Align and Map, Panel Test, Automatic Test) waiting for
#       acknowledgement.  (User hits any key).
#
#    .FIDUCIAL: <number> <board side> <x location> <y location> <shape>
#        <x size> <y size> <orientation>
#       <number> <1|2|3> This is a label for the particular fiducial.  The
#       system will align on point 1 first, then 2, then 3.
#       <board side> <top|bottom|btm> Which side of the panel the fidicual is on.
#       <x location> <float> The x position of the fiducial relative to the panel
#       origin (the bottom left of the panel)
#       <y location> <float> The y position of the fiducial relative to the panel
#       origin (the bottom left of the panel)
#       <shape> <R|C|H> R stands for rectangular and C stands for obround (Circle).
#         H stands for an inverted circle.
#       <x size> the width of the fiducial
#       <y size> the length of the fiducial
#       <orientation> <0|90|180|270> the orientation of the fiducial relative to
#       the panel.
#
#       Fiducials in the panel file are used for alignment.  If FIDUCIALs are
#       used there must be 3 lines - one for each fiducial that will be used to
#       align the panel.  If there are board based alignment points (board.ndf),
#       they take precedence, and the FIDUCIAL lines are ignored.  Note that
#       Panel-based alignment (use of .FIDUCIAL lines) requires that the 400 FOV
#       is used during a test otherwise there will be a run-time error.
#
#   There are a number of optional attributes that informational only.  These are
#   essentially comments that can be added:
#     .DESCRIPTION: <string>
#     .ASSEMBLY_NAME: <string>
#     .ASSEMBLY_NUMBER: <string>
#
#
#  Data lines:
#    description of entry     syntax of entry  explanation
#    --------------------     ---------------  -----------
#    @<board side name>       <alphanumeric>   No spaces allowed.  The name of
#                                              this side of the board.
#
#     <board orientation>     <top|bottom|bot> Top means that this board will be
#                                              facing up in the 5DX. Bot and
#                                              bottom mean that this board will
#                                              be facing down in the machine.
#
#     <inspect the board>     <true|false>     If false do not inspect the board.
#
#     <board location (x)>    <float>          The x distance between the bottom
#                                              left corner of panel and the
#                                              bottom left corner of the board.
#
#     <board location (y)>    <float>          The y distance between the bottom
#                                              left corner of panel and the
#                                              bottom left corner of the board.
#
#     <board rotation>        <0|90|180|270>   Rotation relative to the panel.
#
#     <other board side name> <name|none>      None means don't test this side
#                                              of the board.  No spaces allowed
#                                              if a name is given.
#
#     <board location (x)>    <float>          This is the x origin on the second
#                                              side of the board.  Note that the
#                                              origin on the top side and bottom
#                                              side of panels and boards are at
#                                              two different corners.  The origin
#                                              is always the lower left corner of
#                                              the board as viewed from above for
#                                              the side you are concerned with.
#                                              If none was specified for <other
#                                              board side name> then this setting
#                                              is not required and if it is
#                                              present it will be ignored.
#
#     <board location (y)>    <float>          This is the y origin on the second
#                                              side of the board.  See the
#                                              explanation of the x origin above.
#
#     <board rotation>        <0|90|180|270>   Relative to the panel.  Be aware
#                                              that second side rotation may not
#                                              the same as the front side
#                                              rotation because you are viewing
#                                              the board from the other side.
#
#     <panel stop>            <L|R>            Left or Right. Which
#                                              panel-in-place sensor to use
#                                              for testing this board.  Valid
#                                              only for long, split panels
#                                              (.CONTAINS_DIVIDED_BRD: TRUE)
#                                              Otherwise this field is left blank.
#
#  One line is used for each board on the panel.  The order of the boards
#  determines the number being used by the system for each board.  The numbers
#  start at 1 and increment up from there for each line.  The system will test
#  the boards in this order.
#
################################################################################
# PANEL.NDF
# 5DX Panel Definition
# Panel Name : Zorro_Panel
.SUBPANEL_ID: Zorro_Panel
.UNIT: mils
.ROTATION: 0
.STAGE_SPEED: 1
#
#
# X Y Board Thickness
.DIMENSIONS:  11000 6000 62
#
.COMPONENT_SAMPLE_PERCENT: 100
.SURFACE_MAP_ALIGNMENT_VIEWS: FALSE
.MATERIALS: 6337_cu or not
.PANEL_LOAD_TRAILING_EDGE_DETECT: FALSE
.ACCEPT_ILLEGAL_IDENTIFIERS: FALSE
.AWARETEST_ON: FALSE
.DEFECT_ANALYZER: FALSE
.DUAL_PANEL_TEST_CAD: FALSE
#
#
# P Board Side Test? Location Rotation Secondary Bd Location Rotation
#BOARD 1
@BOARD_A1 TOP TRUE 0 3000 0 BOARD_B1 6083.15 3000 0 
#BOARD 2
@pads_asc_A2 TOP TRUE 0 0 0 pads_asc_B2 7700 0 0 
#BOARD 3
@pads_asc_A2 TOP TRUE 5000 0 0 pads_asc_B2 2700 0 0 
#BOARD 4
@BOARD_A1 BOTTOM TRUE 500 3000 0 BOARD_B1 5583.15 3000 0 

.CHECKSUM: 1231496430
