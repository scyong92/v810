#####################################################################################
# FILE: board.ndf
# PURPOSE: The board.ndf contains a list of component (footprint) locations and
#    orientations.  It may also contain data for alignment points and board
#    thickness measurements.  There are two board files for each unique board on
#    the panel, one for each board side (only one if single-sided).  The files
#    reside in a subdirectory for that board side.
# SYNTAX:
#  Required Attributes:
#    .DIMENSIONS: <board width> <board length> <thickness>
#
#  Optional Attributes:
#    .UNIT: inches | mils | millimeters
#       If this attribute is missing, mils is assumed.
#
#    .BOARD_ID: <board name>
#        This is strictly informational, actual board names come from panel file.
#
#    .DEFAULT_PITCH: <float>
#        Any packages that have a listed pitch of 0 (package.ndf) are assigned
#        this pitch.  Not recommended, pitches should be specified for all
#        all packages in the package file.
#
#    .SURFACE_MAP_DENSITY: <float>
#        A default surface map is created to allow compilation before an actual
#        surface map file can be developed. This value determines the density
#        (spacing) of the points.  This seems to only be an issue if no valid
#        triangles can be constructed from the points.
#
#    .ALIGNX: <ref.des> <pin number> (X = 1, 2 or 3)
#        This is an old form to specify alignment points and is no longer used.
#
#    .ALIGNMENT_PADS: |1|2|3| <ref.des> <pin number> ... <ref.des.> <pin number>
#        Each alignment point can use up to 6 pads.  Component must be on board
#        side listed, and must be tested (status set to TRUE in componen.ndf).
#        Alignment points can be on both board sides (set 1 and 3 could be on
#        top, set 2 on the bottom.  All pads for one set must be on the same
#        board side.
#
#    .THICKNESS_PADS: <delta z> <camera index> <snap margin> <fov> <technique>
#        <ref.des> <pin number>...<ref.des> <pin number>
#        <delta z> is the difference in mils between where sharpest image found
#        and desired definition of board surface.
#        <camera index> legitimate values are 0-31 not the brightnessXcycles.
#        <snap margin> is the distance in mils in addition to listed pads to use
#        for tested area.
#        <fov> fov to use for measurement.  Must be a loaded FOV (included in
#        test)
#        <technique> one of the techniques in XMAPMETH.DAT, currently:
#        PROFILE_DERIV_MAX, PROFILE_DERIV_VARIANCE, GRADIENT_PERCENT,
#        GRADIENT_VARIANCE or SOBEL_GRADIENT.  SOBEL_GRADIENT is the preferred
#        technique.
#        <ref. des.> <pin number>  Specified pins should all be in same row of
#        the same device.  No more than 3-4 pads should be used.  Note there can
#        be multiple  .THICNESS_PADS lines.
#        This attribute is used to automatically determine board thickness on
#        each assembly tested, for cases where board thickness varies from
#        assembly to assembly.  .THICKNESS_PADS lines are only valid in the
#        board file for the bottom side board.
#
#  Data lines:
#    description of entry   syntax of entry    explanation
#    --------------------   ---------------    -----------
#     @<ref. des.>          <alphanumeric>
#
#     <x location>          <float>            x distance from the board origin
#                                              to the component reference point
#                                              (normally the component centroid).
#
#     <y location>          <float>            y distance from the board origin
#                                              to the component reference point
#                                              (normally the component centroid).
#
#     <orientation>         <integer degrees>  orientation relative to the
#                                              defined orientation in the landpat
#                                              or padgeom file.
#
#     <footprint name>      <alphanumeric>     must match the name in landpat or
#                                              padgeom file.
#
#####################################################################################
.BOARD_ID: BOARD_A1
.UNIT: mils
.UNIT: mils
#   X   Y   Board Thickness
.DIMENSIONS:  4916.85 1841 62
#
.Alignment_Pads: 1  L4 1
.Alignment_Pads: 2  C3 1 C3 2

#
#COMPONENT LOCATION DATA LINES (Default referenced to package center)
#Reference Designator   XLoc   YLoc   rotation   Footprint ID
@C3 791.95 1673 90 SM00137
@L4 421.95 543 90 SM00079

.CHECKSUM: 4227686791
