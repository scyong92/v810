package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown if the snap margin for a .FOV: attribute line in fov_data.ndf differs
 * from other attribute lines where the Field Of View and Image Resolution are the same.
 * @author Tony Turner
 */
public class SnapMarginDiffersDatastoreException extends DatastoreException
{

 /**
  * @author Tony Turner
  */
  public SnapMarginDiffersDatastoreException(String fileName,
                                             int lineNumber,
                                             int fieldOfView,
                                             int resolution,
                                             double expectedSnapMarginValueForCurrentPitch,
                                             double actualSnapMarginValueForCurrentPitch)
  {
    super(new LocalizedString("DS_ERROR_SNAP_MARGIN_DIFFERS_FROM_MATCHING_ATTRIBUTE_LINES_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           new Integer(fieldOfView),
                                           new Integer(resolution),
                                           new Double(expectedSnapMarginValueForCurrentPitch),
                                           new Double(actualSnapMarginValueForCurrentPitch)}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(fieldOfView > 0);
    Assert.expect(resolution > 0);
    Assert.expect(expectedSnapMarginValueForCurrentPitch > 0.0);
    Assert.expect(actualSnapMarginValueForCurrentPitch > 0.0);
  }

}
