package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * Throw this exception when a pin is referenced in some file that does not exist in the package.ndf file.
 * @author Bill Darbie
 */
public class BadPinReferenceDatastoreException extends DatastoreException
{

  /**
   * @author George A. David
   */
  public BadPinReferenceDatastoreException(String fileName,
                                           int lineNumber,
                                           String expectedSyntax,
                                           String invalidField,
                                           String referenceDesignator,
                                           String pinName,
                                           String packageName,
                                           String packageFileName)
  {
    super(new LocalizedString("DS_NDF_ERROR_BAD_PIN_REFERENCE_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           expectedSyntax,
                                           invalidField,
                                           referenceDesignator,
                                           pinName,
                                           packageName,
                                           packageFileName}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidField != null);
    Assert.expect(referenceDesignator != null);
    Assert.expect(pinName != null);
    Assert.expect(packageName != null);
    Assert.expect(packageFileName != null);
  }

  /**
   * @author Bill Darbie
   */
  public BadPinReferenceDatastoreException(String fileName,
                                           int lineNumber,
                                           String pinName,
                                           String referenceDesignator,
                                           String packageFileName,
                                           String landPatternFileName)

  {
    super(new LocalizedString("DS_ERROR_BAD_PIN_REFERENCE_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           pinName,
                                           referenceDesignator,
                                           packageFileName,
                                           landPatternFileName}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(pinName != null);
    Assert.expect(referenceDesignator != null);
    Assert.expect(packageFileName != null);
    Assert.expect(landPatternFileName != null);
  }
}
