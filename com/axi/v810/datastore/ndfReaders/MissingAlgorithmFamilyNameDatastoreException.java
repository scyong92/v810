package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * If the algorithm family name is missing, this exception should be thrown.
 * @author Bill Darbie
 */
public class MissingAlgorithmFamilyNameDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public MissingAlgorithmFamilyNameDatastoreException(String fileName,
                                                      int lineNumber)
                         
  {
    super(new LocalizedString("DS_ERROR_MISSING_ALGORITHM_FAMILY_NAME_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber)}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
  }
}
