package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * @author Andy Mechtenberg
 */
class InvalidBoardRotationDatastoreException extends DatastoreException
{
  /**
   * @param fileName is the name of the panel file that is corrupt.
   * @author Andy Mechtenberg
   */
  InvalidBoardRotationDatastoreException(String fileName,
                                         int lineNumber,
                                         String expectedSyntax,
                                         String invalidField,
                                         String boardOrientation)
  {
    super(new LocalizedString("DS_NDF_ERROR_INVALID_BOARD_ROTATION_KEY", new Object[]{fileName,
                                                                                      new Integer(lineNumber),
                                                                                      expectedSyntax,
                                                                                      invalidField,
                                                                                      boardOrientation}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidField != null);
    Assert.expect(boardOrientation != null);
  }
}
