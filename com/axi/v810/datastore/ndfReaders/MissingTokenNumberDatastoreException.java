package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when we could not find a number where one was required.
 * @author Vincent Wong
 */
public class MissingTokenNumberDatastoreException extends DatastoreException
{
  /**
   * @author Vincent Wong
   */
  public MissingTokenNumberDatastoreException(String fileName,
                                              int lineNumber,
                                              String token)
  {
    super(new LocalizedString("DS_ERROR_TOKEN_MISSING_A_NUMBER_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           token}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(token != null);
  }
}
