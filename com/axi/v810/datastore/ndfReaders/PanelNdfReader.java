package com.axi.v810.datastore.ndfReaders;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Parses in the panel.ndf file.
 * This class creates the following objects:
 *   Project
 *     Panel
 *     awareTestOn
 *     notes
 *     softwareVersionCreation
 *
 *   Panel
 *     Project
 *     PanelSettings
 *     degreesRotationRelativeToCad
 *     Boards
 *     widthInNanoMeters
 *     lengthInNanoMeters
 *     padToPadThicknessInNanometers
 *
 *   PanelSettings
 *
 *   SideBoard
 *     topSide
 *     Board
 *     CoordinateInNanoMeters
 *     SideBoardSettings
 *     legacyName
 *     SideBoardType
 *
 *   SideBoardType
 *     BoardType
 *     SideBoard
 *
 *   Board
 *     name
 *     Panel
 *     legacyNumber
 *     bottomSideBoard
 *     topSideBoard
 *     legacyPrimarySideBoardOnTop
 *     BoardSettings
 *     BoardType
 *     flipped
 *     legacyRtfBoardName
 *
 *   BoardType
 *     name
 *     topSideBoardType
 *     bottomSideBoardType
 *     Panel
 *     Boards
 *
 * This class requires the following to already be available to it from MainReader:
 *   doSameSideBoardInstancesExist
 *   getSameSideBoardInstances
 *
 * This class puts the following into MainReader:
 *   addSideBoardNameToSameSideBoardInstancesMap
 *   addSideBoardToDegreesRotationMap
 *   addBoardToLegacyRtfBoardNameMap(board, legacyRtfBoardName);
 *   addSideBoardToLegacyCoordinateInMils
 *
 * @author Keith Lee
 */
class PanelNdfReader extends NdfReader
{
  private static final boolean _DONT_RETURN_DELIMITERS = false;

  //"@<board side name> <board orientation> <inspect the board> <board x location> <board y location> <board rotation>
  // <other board side name> <other board x location> <other board y location> <other board rotation> <panel stop>
  private static final String _EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_PANEL_EXPECTED_SYNTAX_KEY");
  private static final String _BOARD_SIDE_NAME_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_SIDE_NAME_KEY"); //"<board side name>";
  private static final String _BOARD_ORIENTATION_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_ORIENTATION_KEY"); //"<board orientation>";
  private static final String _INSPECT_THE_BOARD_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_INSPECT_THE_BOARD_KEY"); //"<inspect the board>";
  private static final String _BOARD_X_LOCATION_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_X_LOCATION_KEY"); //"<board x location>";
  private static final String _BOARD_Y_LOCATION_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_Y_LOCATION_KEY"); //"<board y location>";
  private static final String _BOARD_ROTATION_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_ROTATION_KEY"); //"<board rotation>";
  private static final String _OTHER_BOARD_SIDE_NAME_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_OTHER_BOARD_SIDE_NAME_KEY"); //"<other board side name>";
  private static final String _OTHER_BOARD_X_LOCATION_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_OTHER_BOARD_X_LOCATION_KEY"); //"<other board x location>";
  private static final String _OTHER_BOARD_Y_LOCATION_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_OTHER_BOARD_Y_LOCATION_KEY"); //"<other board y location>";
  private static final String _OTHER_BOARD_ROTATION_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_OTHER_BOARD_ROTATION_KEY"); //"<other board rotation>";
  private static final String _PANEL_STOP_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PANEL_STOP_KEY"); //"<panel stop>";

  //.fiducial: <number> <side> <x location> <y location> <shape> <x size> <y size> <rotation>
  private static final String _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_PANEL_ATTRIBUTE_FIDUCIAL_KEY");
  private static final String _FIDUCIAL_NUMBER_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_NUMBER_KEY");
  private static final String _FIDUCIAL_SIDE_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_SIDE_KEY");
  private static final String _FIDUCIAL_X_LOCATION_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_X_LOCATION_KEY");
  private static final String _FIDUCIAL_Y_LOCATION_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_Y_LOCATION_KEY");
  private static final String _FIDUCIAL_SHAPE_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_SHAPE_KEY");
  private static final String _FIDUCIAL_X_SIZE_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_X_SIZE_KEY");
  private static final String _FIDUCIAL_Y_SIZE_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_Y_SIZE_KEY");
  private static final String _FIDUCIAL_ROTATION_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_ROTATION_KEY");

  //.dimensions: <panel width> <panel length> <pad to pad thickness>
  private static final String _ATTRIBUTE_LINE_DIMENSIONS_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_PANEL_ATTRIBUTE_DIMENSIONS_EXPECTED_SYNTAX_KEY");
  private static final String _PANEL_WIDTH_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PANEL_WIDTH_KEY");
  private static final String _PANEL_LENGTH_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PANEL_LENGTH_KEY");
  private static final String _PAD_TO_PAD_THICKNESS_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PAD_TO_PAD_THICKNESS_KEY");

  //.component_sample_percent: <sample percent>
  private static final String _ATTRIBUTE_LINE_COMPONENT_SAMPLE_PERCENT_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_PANEL_ATTRIBUTE_COMPONENT_SAMPLE_PERCENT_KEY");
  private static final String _SAMPLE_PERCENT_NUMBER_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_SAMPLE_PERCENT_KEY");

  //.subpanel_id: <panel name>
  private static final String _ATTRIBUTE_LINE_SUBPANEL_ID_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_PANEL_ATTRIBUTE_SUBPANEL_ID_KEY");
  private static final String _PANEL_NAME_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PANEL_NAME_KEY");

  //.software_revision <version number>
  private static final String _ATTRIBUTE_LINE_SOFTWARE_REVISION_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_SOFTWARE_REVISION_EXPECTED_SYNTAX_KEY");
  private static final String _VERSION_NUMBER_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_VERSION_NUMBER_KEY");

  //.rotation: <degrees rotation>
  private static final String _ATTRIBUTE_LINE_ROTATION_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_PANEL_ROTATION_EXPECTED_SYNTAX_KEY");
  private static final String _DEGREES_ROTATION_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_DEGREES_ROTATION_KEY");

  //.materials <material>
  private static final String _ATTRIBUTE_LINE_MATERIALS_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_PANEL_MATERIALS_EXPECTED_SYNTAX_KEY");
  private static final String _MATERIAL_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_MATERIAL_KEY");

  //.stage_speed: <1 through 20>
  private static final String _ATTRIBUTE_LINE_STAGE_SPEED_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_PANEL_ATTRIBUTE_STAGE_SPEED_KEY");
  private static final String _STAGE_SPEED_NUMBER_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_STAGE_SPEED_NUMBER_KEY");

  //.laser_map_stage_speed: <1 through 20>
  private static final String _ATTRIBUTE_LINE_LASER_MAP_STAGE_SPEED_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_PANEL_ATTRIBUTE_LASER_MAP_STAGE_SPEED_KEY");
  private static final String _LASER_MAP_STAGE_SPEED_NUMBER_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_STAGE_SPEED_NUMBER_KEY");

  private static final String _TOP_SIDE_BOARD = "TOP";
  private static final String _BOTTOM_SIDE_BOARD = "BOTTOM";
  private static final String _BOT_SIDE_BOARD = "BOT";
  private static final String _BTM_SIDE_BOARD = "BTM";
  private static final String _NO_SECONDARY_BOARD = "None";
  private static final String _NIL_SECONDARY_BOARD = "Nil";

  private static final String _SUBPANEL_ID = ".SUBPANEL_ID:";
  private static final String _DIMENSIONS = ".DIMENSIONS:";
  private static final String _MATERIALS = ".MATERIALS:";
  private static final String _COMPONENT_SAMPLE_PERCENT = ".COMPONENT_SAMPLE_PERCENT:";
  private static final String _SURFACE_MAP_ALIGNMENT_VIEWS = ".SURFACE_MAP_ALIGNMENT_VIEWS:";
  private static final String _PAUSE_ON_AUTO_ALIGN = ".PAUSE_ON_AUTO_ALIGN:";
  private static final String _FIDUCIAL = ".FIDUCIAL:";
  private static final String _ROTATION = ".ROTATION:";
  private static final String _STAGE_SPEED = ".STAGE_SPEED:";
  private static final String _DEFECT_ANALYZER = ".DEFECT_ANALYZER:";
  private static final String _LASER_MAP_STAGE_SPEED = ".LASER_MAP_STAGE_SPEED:";
  private static final String _DUAL_PANEL_TEST_CAD = ".DUAL_PANEL_TEST_CAD:";
  private static final String _PANEL_LOAD_TRAILING_EDGE_DETECT = ".PANEL_LOAD_TRAILING_EDGE_DETECT:";
  private static final String _CONTAINS_DIVIDED_BRD = ".CONTAINS_DIVIDED_BRD:";
  private static final String _ACCEPT_ILLEGAL_IDENTIFIERS = ".ACCEPT_ILLEGAL_IDENTIFIERS:";
  private static final String _DESCRIPTION = ".DESCRIPTION:";
  private static final String _ASSEMBLY_NAME = ".ASSEMBLY_NAME:";
  private static final String _ASSEMBLY_NUMBER = ".ASSEMBLY_NUMBER:";
  private static final String _AWARETEST_ON = ".AWARETEST_ON:";
  private static final String _SOFTWARE_REVISION = ".SOFTWARE_REVISION:";
  // unused/deprecated attribute lines
  private static final String _NUMBER_OF_FIDUCIALS = ".NUMBER_OF_FIDUCIALS:";
  private static final String _PRODUCT = ".PRODUCT:";
  private static final String _NUMBER_OF_LOADED_SIDES = ".NUMBER_OF_LOADED_SIDES:";
  private static final String _NUMBER_OF_BOARDS = ".NUMBER_OF_BOARDS:";
  private static final String _NUMBER_OF_SUBPANEL_TOOLING_HOLES = ".NUMBER_OF_SUBPANEL_TOOLING_HOLES:";

  private static final int _MIN_SAMPLING_RATE = 0;
  private static final int _MAX_SAMPLING_RATE = 100;

  private static final int _MIN_STAGE_SPEED = 1;
  private static final int _MAX_STAGE_SPEED = 20;

  private static final int _MIN_FIDUCIAL_DEGREES_ROTATION = 0;
  private static final int _MAX_FIDUCIAL_DEGREES_ROTATION = 359;

  private static final int _FIDUCIAL_ONE = 1;
  private static final int _FIDUCIAL_TWO = 2;
  private static final int _FIDUCIAL_THREE = 3;

  private static final String _RIGHT_SENSOR_POSITION = "Right";
  private static final String _LEFT_SENSOR_POSITION = "Left";

  private static final String _INVERTED_CIRCULAR_SHAPE = "H";
  private static final String _LEGACY_FALSE = "F";
  private static final String _LEGACY_TOP_SIDE_BOARD = "T";
  private static final String _LEGACY_BOTTOM_SIDE_BOARD = "B";

  // default values (if corresponding attribute line is mising)
  private static final boolean _DEFAULT_AWARE_TEST_ON = false;
  private static final int _DEFAULT_COMPONENT_SAMPLING_RATE = _MAX_SAMPLING_RATE;
  private static final boolean _DEFAULT_CONTAINS_DIVIDED_BOARD = false;
  private static final boolean _DEFAULT_DUAL_PANEL_TEST = false;
  private static final boolean _DEFAULT_DETECT_TRAILING_EDGE = false;
  private static final boolean _DEFAULT_DEFECT_ANALYZER = false;
  private static final boolean _DEFAULT_PAUSE_ON_AUTO_ALIGN = false;
  private static final int _DEFAULT_ROTATION = 0;
  private static final String _DEFAULT_SOLDER_MATERIALS_VALUE = "6337_Cu";
  private static final String _DEFAULT_SOLDER_MATERIALS_KEYWORD = "Default";
  private static final boolean _DEFAULT_USE_OLD_SOLDER_THICKNESS_TABLES = true;
  private static final boolean _DEFAULT_SURFACE_MAP_ALIGNMENT_VIEWS = false;
  private static final double _DEFAULT_SOFTWARE_REVISION_CREATION = 0.0;
  private static final String _DEFAULT_ASSEMBLY_NAME = "";
  private static final String _DEFAULT_ASSEMBLY_VERSION = "";
  private static final String _DEFAULT_DESCRIPTION = "";
  private static final String _DEFAULT_CUSTOMER_CONTACT = "";
  private static final String _DEFAULT_CUSTOMER_NAME = "";
  private static final String _DEFAULT_CUSTOMER_PHONE = "";
  private static final String _DEFAULT_THREHSOLDS_NAME = "";
  private static final String _DEFAULT_MACHINE_MODEL_TARGET = "";
  private static final String _DEFAULT_MACHINE_SERIES_TARGET = "";
  private static final String _DEFAULT_PROGRAMMER_NAME = "";
  private static final String _DEFAULT_PROJECT_NAME = "";
  private static final String _DEFAULT_TEST_LINK_VERSION = "";

  // assumed values (if corresponding attribute line does not specifiy a value)
  private static final boolean _ASSUMED_SURFACE_MAP_ALIGNMENT_VIEWS = true;
  private static final boolean _ASSUMED_PAUSE_ON_AUTO_ALIGN = false;
  private static final boolean _ASSUMED_DETECT_TRAILING_EDGE = true;
  private static final boolean _ASSUMED_AWARE_TEST_ON = true;
  private static final boolean _ASSUMED_CONTAINS_DIVIDED_BOARD = true;
  private static final boolean _ASSUMED_DUAL_PANEL_TEST = true;
  private static final boolean _ASSUMED_DEFECT_ANALYZER = true;
  private static final boolean _ASSUMED_ACCEPT_ILLEGAL_IDENTIFIERS = true;

  private Project _project = null;
  private Panel _panel = null;
  private PanelSettings _panelSettings = null;
  private PanelSurfaceMapSettings _panelSurfaceMapSettings = null;
  private PanelAlignmentSurfaceMapSettings _panelAlignmentSurfaceMapSettings = null;
  private PanelMeshSettings _panelMeshSettings = null;
  private double _panelWidthInMils = 0.0;
  private PspSettings _pspSettings = null;

  // required line that can be set/specified multiple times per panel.ndf
  private int _validRequiredDataLines = 0;
  private int _sensorPositionsSpecified = 0;

  // lines that can be set/specified once per panel.ndf
  //_checkSumFound inherited from NdfReader
  //_unitLineFound inherited from NdfReader
  private boolean _validRequiredAttributeLineDimensions = false;
  private boolean _validAttributeLineSubPanelID = false;
  private boolean _validAttributeLineMaterials = false;
  private boolean _validAttributeLineRotation = false;
  private boolean _validAttributeLineStageSpeed = false;
  //.COMPONENT_SAMPLE_PERCENT: required for sampling components tagged as PARTIAL in componen.ndf
  private boolean _validAttributeLineComponentSamplePercent = false;
  private boolean _validAttributeLineSurfaceMapAlignmentViews = false;
  private boolean _validAttributeLinePauseOnAutoAlign = false;
  private boolean _validAttributeLineDefectAnalyzer = false;
  private boolean _validAttributeLineLaserMapStageSpeed = false;
  private boolean _validAttributeLineDualPanelTestCad = false;
  private boolean _validAttributeLinePanelLoadTrailingEdgeDetect = false;
  private boolean _validAttributeLineContainsDividedBoard = false;
  private boolean _validAttributeLineAcceptIllegalIdentifiers = false;
  private boolean _validAttributeLineAwareTestOn = false;
  private boolean _validAttributeLineSoftwareRevision = false;
  private boolean _validAttributeLineAssemblyName = false;
  private boolean _validAttributeLineAssemblyNumber = false;
  private boolean _validAttributeLineDescription = false;
  // for fiducials, either none or all three must be set
  private boolean _validAttributeLineFiducial = false;
  private boolean _validFiducialOne = false;
  private boolean _validFiducialTwo = false;
  private boolean _validFiducialThree = false;

  private int _unitAttributeLinesFound = 0;
  private int _materialsAttributeLinesFound = 0;
  private int _softwareRevisionAttributeLinesFound = 0;

  private boolean _materialsAttributeLineTokenExist = true;
  private boolean _containsDividedPanel = false;

  // legacy board number variables
  private int _numInspectedBoards = 0;
  private int _numNonInspectedBoards = 0;
  //board name variables
  private String _line;
  private StringTokenizer _tokenizer;
  private String _primarySideBoardTypeName = null;
  private boolean _isPrimaryOnTop = false;
  boolean _boardInspected = false;
  private int _primarySideBoardXCoordinateInNanoMeters = 0;
  private int _primarySideBoardYCoordinateInNanoMeters = 0;
  private int _primarySideBoardDegreesRotation = 0;
  private boolean _secondarySideBoardExists = false;
  private String _secondarySideBoardTypeName = null;
  private int _secondarySideBoardXCoordinateInNanoMeters = 0;
  private int _secondarySideBoardYCoordinateInNanoMeters = 0;
  private int _secondarySideBoardDegreesRotation = 0;
  private Map<SideBoard, DoubleCoordinate> _sideBoardToLegacyCoordinateInMilsMap = new HashMap<SideBoard, DoubleCoordinate>();
  private Map<String, SideBoardType> _sideBoardTypeNameToSideBoardTypeMap = new HashMap<String, SideBoardType>();
  private Map<Board, Boolean> _boardToLegacyIsPrimarySideBoardOnTopMap = new HashMap<Board, Boolean>();
  private Map<SideBoardType, BoardType> _sideBoardTypeToBoardTypeMap = new HashMap<SideBoardType, BoardType>();
  private Map<BoardType, Board> _boardTypeToOriginalBoardMap = new HashMap<BoardType, Board>();
  private Set<BoardType> _possibleSplitBoardTypeSet = new HashSet<BoardType>();
  private int _boardTypeId = 0;
  private int _boardNameId = 0;
  private SideBoard _topSideBoard;
  private SideBoard _bottomSideBoard;
  private int _degreesRotation;

  /**
   * @param mainReader the MainReader associated with the panel programa
   * @author Keith Lee
   */
  PanelNdfReader(MainReader mainReader)
  {
    super(mainReader);
  }

  /**
   * @author Keith Lee
   */
  protected void initializeBeforeParsing()
  {
    // set panel defaults
    _project = new Project(false);
    _panel = new Panel();
    _panel.disableChecksForNdfParsers();
    _project.setPanel(_panel);
    _panel.setProject(_project);
    _project.setReadyForProduction(false);
    _project.setNotes("");
    _project.setSystemType(XrayTester.getSystemType());

    _panelSettings = new PanelSettings();
    _panelSettings.setPanel(_panel);
    _panel.setPanelSettings(_panelSettings);
    
    if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_PSP))
    {
        _panelSurfaceMapSettings = new PanelSurfaceMapSettings();
        _panel.setPanelSurfaceMapSettings(_panelSurfaceMapSettings);
        _panelAlignmentSurfaceMapSettings = new PanelAlignmentSurfaceMapSettings();
        _panel.setPanelAlignmentSurfaceMapSettings(_panelAlignmentSurfaceMapSettings);
        _pspSettings = new PspSettings();
        _panel.setPspSettings(_pspSettings);
        _panelMeshSettings = new PanelMeshSettings();
        _panel.setPanelMeshSettings(_panelMeshSettings);
    }
    
    _parsedUnits = null;
    _panelWidthInMils = 0.0;

    _validRequiredDataLines = 0;
    _sensorPositionsSpecified = 0;
    _validAttributeLineSubPanelID = false;
    _checkSumFound = false;
    _unitLineFound = false;
    _validAttributeLineRotation = false;
    _validAttributeLineStageSpeed = false;
    _validRequiredAttributeLineDimensions = false;
    _validAttributeLineMaterials = false;
    _validAttributeLineComponentSamplePercent = false;
    _validAttributeLineSurfaceMapAlignmentViews = false;
    _validAttributeLinePauseOnAutoAlign = false;
    _validAttributeLineDefectAnalyzer = false;
    _validAttributeLineLaserMapStageSpeed = false;
    _validAttributeLineDualPanelTestCad = false;
    _validAttributeLinePanelLoadTrailingEdgeDetect = false;
    _validAttributeLineContainsDividedBoard = false;
    _validAttributeLineAcceptIllegalIdentifiers = false;
    _validAttributeLineAwareTestOn = false;
    _validAttributeLineSoftwareRevision = false;
    _validAttributeLineFiducial = false;
    _validAttributeLineAssemblyName = false;
    _validAttributeLineAssemblyNumber = false;
    _validAttributeLineDescription = false;
    _validFiducialOne = false;
    _validFiducialTwo = false;
    _validFiducialThree = false;

    _containsDividedPanel = false;

    _unitAttributeLinesFound = 0;
    _materialsAttributeLinesFound = 0;
    _softwareRevisionAttributeLinesFound = 0;

    _numInspectedBoards = 0;
    _numNonInspectedBoards = 0;
    String _primarySideBoardName = null;
    _isPrimaryOnTop = false;
    _boardInspected = false;
    _primarySideBoardXCoordinateInNanoMeters = 0;
    _primarySideBoardYCoordinateInNanoMeters = 0;
    _primarySideBoardDegreesRotation = 0;
    _secondarySideBoardExists = false;
    _secondarySideBoardTypeName = null;
    _secondarySideBoardXCoordinateInNanoMeters = 0;
    _secondarySideBoardYCoordinateInNanoMeters = 0;
    _secondarySideBoardDegreesRotation = 0;
    _boardTypeId = 0;
    _boardNameId = 0;
  }

  /**
   * @author Keith Lee
   */
  protected void resetAfterParsing()
  {
    // cannot set _panel to null here. getProject is the only way to get Panel
    // instance, but that is called after clear() is called.
    // Also, cannot set _primarySideBoards to null because that data is needed after the clear is called.

    _panelSettings = null;
    _sideBoardToLegacyCoordinateInMilsMap.clear();
    _sideBoardTypeToBoardTypeMap.clear();
    _boardTypeToOriginalBoardMap.clear();
    _possibleSplitBoardTypeSet.clear();

    _sideBoardTypeNameToSideBoardTypeMap.clear();
    _boardToLegacyIsPrimarySideBoardOnTopMap.clear();
    _sideBoardToLegacyCoordinateInMilsMap.clear();
    _boardTypeId = 0;
    _boardNameId = 0;
    _topSideBoard = null;
    _bottomSideBoard = null;
  }

  /**
   * @author Keith Lee
   */
  Project getProject()
  {
    Assert.expect(_project != null);

    return _project;
  }

  /**
   * @author Bill Darbie
   */
  void clearProject()
  {
    _project = null;
    _panel = null;
  }

  /**
   * @author Keith Lee
   */
  protected void postProcessData() throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);

    if ((_validAttributeLineContainsDividedBoard) && (_containsDividedPanel))
    {
      _mainReader.setSplitBoardTypeSet(_possibleSplitBoardTypeSet);
    }

    //if a required line is missing
    if (_validRequiredAttributeLineDimensions == false)
      throw new MissingTokenDatastoreException(_currentFile, _DIMENSIONS);

    if (_validRequiredDataLines == 0)
      throw new MissingRequiredLineDatastoreException(_currentFile, _EXPECTED_SYNTAX);

    // make sure that sensor positions are specified for all boards or none
    if ( (_sensorPositionsSpecified > 0) &&
        ((_validRequiredDataLines == _sensorPositionsSpecified) == false) )
    {
      throw new ExpectedAllBoardsToSpecifySensorPositionsDatastoreException(_currentFile);
    }

    // if .ROTATION: is not specified in the panel.ndf
    if (_validAttributeLineRotation == false)
    {
      _panelSettings.setDegreesRotationRelativeToCad(_DEFAULT_ROTATION);
    }

    // if .AWARETEST_ON: is not specified in the panel.ndf
    if (_validAttributeLineAwareTestOn == false)
    {
      // do nothing
    }

    // if .FIDUCIAL: is not specified in the panel.ndf
    if (_validAttributeLineFiducial == false)
    {
      // do nothing
    }

    // now it is okay to set degrees rotation, since that requires width and length be set first
    _panelSettings.setDegreesRotationRelativeToCad(_degreesRotation);
  }

  /**
   * parses a data line in a panel.ndf file that follows the following format
   * "@<primary> <orientation> <test boolean> <x location> <y location> <rotation> <secondary> <x location> <y rotation> <rotation> [ <sensor position> ]"
   *
   * @param line - the line of panel data in the panel.ndf to parse.
   * @author Keith Lee
   */
  protected void parseDataLine(String line) throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);
    Assert.expect(line != null);

    _line = line;
    parseInLine();
    populateClassesWithData();
  }

  /**
   * Parse in one data line
   * @author Andy Mechtenberg
   */
  private void parseInLine() throws DatastoreException
  {
    _tokenizer = new StringTokenizer(_line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    _primarySideBoardTypeName = parsePrimarySideBoardName();
    _isPrimaryOnTop = parsePrimarySideBoardOrientation();
    _boardInspected = parseTestBoard();

    // if board is not inspected pre 8.0, give warning, skip rest of line and do not create board
    if ( (_boardInspected == false) &&
        (_mainReader.ndfsAreWellFormed() == false) )
    {
      Object[] args = new Object[]{_currentFile,
                                   new Integer(_is.getLineNumber())};
      _warnings.add(new LocalizedString("DS_NDF_WARNING_NON_INSPECTED_BOARD_KEY", args));
      return;
    }

    _primarySideBoardXCoordinateInNanoMeters = parsePrimarySideBoardXLocation();
    _primarySideBoardYCoordinateInNanoMeters = parsePrimarySideBoardYLocation();
    _primarySideBoardDegreesRotation = parsePrimarySideBoardRotation();
    _secondarySideBoardTypeName = parseSecondarySideBoardName();

    // if no second side board, and in compatible mode
    if ((_secondarySideBoardExists == false) && (_mainReader.ndfsAreWellFormed() == false))
    {
      String ignoreToken = ParseUtil.getNextToken(_tokenizer); // secondary X
      ignoreToken = ParseUtil.getNextToken(_tokenizer); // secondary Y
      ignoreToken = ParseUtil.getNextToken(_tokenizer); // secondary theta
    }
    else  // either in strict mode, or there really is a second board
    {
      _secondarySideBoardXCoordinateInNanoMeters = parseSecondarySideBoardXLocation();
      _secondarySideBoardYCoordinateInNanoMeters = parseSecondarySideBoardYLocation();
      _secondarySideBoardDegreesRotation = parseSecondarySideBoardRotation();
    }
    // parse in sensor positition, but do not use it.
    parseSensorPosition();

    checkForExtraDataAtEndOfLine(_tokenizer, _EXPECTED_SYNTAX);
  }

  /**
   * Take the data read in on a single line and populate the appropriate classes
   * with it.
   * @author Andy Mechtenberg
   */
  private void populateClassesWithData() throws DatastoreException
  {
    SideBoard primarySideBoard = null;
    SideBoard secondarySideBoard = null;

    double primarySideBoardLegacyXCoordinateInMils = 0.0;
    double primarySideBoardLegacyYCoordinateInMils = 0.0;
    double secondarySideBoardLegacyXCoordinateInMils = 0.0;
    double secondarySideBoardLegacyYCoordinateInMils = 0.0;

    List<SideBoard> primarySideBoardList = null;
    List<SideBoard> secondarySideBoardList = null;

    // get the SideBoardType object or create it if it does not already exist.
    SideBoardType primarySideBoardType = getSideBoardType(_primarySideBoardTypeName);
    SideBoardType secondarySideBoardType = null;

    // the primary side board MUST exist, so create it here.
    // the secondary side board is optional (the panel may be one-sided)
    primarySideBoard = new SideBoard();
    primarySideBoardType.addSideBoard(primarySideBoard);

    if (_secondarySideBoardExists)
    {
      secondarySideBoardType = getSideBoardType(_secondarySideBoardTypeName);
      secondarySideBoard = new SideBoard();
      secondarySideBoardType.addSideBoard(secondarySideBoard);
    }

    // the legacy code needs these coordinates in mils so it can calculate the RTF board directory name
    primarySideBoardLegacyXCoordinateInMils = MathUtil.convertUnits(_primarySideBoardXCoordinateInNanoMeters, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
    primarySideBoardLegacyYCoordinateInMils = MathUtil.convertUnits(_primarySideBoardYCoordinateInNanoMeters, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
    secondarySideBoardLegacyXCoordinateInMils = MathUtil.convertUnits(_secondarySideBoardXCoordinateInNanoMeters, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);
    secondarySideBoardLegacyYCoordinateInMils = MathUtil.convertUnits(_secondarySideBoardYCoordinateInNanoMeters, MathUtilEnum.NANOMETERS, MathUtilEnum.MILS);

    // get primary and secondary board from map and create Coordinates

    PanelCoordinate primarySideBoardCoordinateInNanoMeters =
        new PanelCoordinate(_primarySideBoardXCoordinateInNanoMeters, _primarySideBoardYCoordinateInNanoMeters);
    DoubleCoordinate primarySideBoardLegacyCoordinateInMils =
        new DoubleCoordinate(primarySideBoardLegacyXCoordinateInMils, primarySideBoardLegacyYCoordinateInMils);
    PanelCoordinate secondarySideBoardCoordinateInNanoMeters =
        new PanelCoordinate(_secondarySideBoardXCoordinateInNanoMeters, _secondarySideBoardYCoordinateInNanoMeters);
    DoubleCoordinate secondarySideBoardLegacyCoordinateInMils =
        new DoubleCoordinate(secondarySideBoardLegacyXCoordinateInMils, secondarySideBoardLegacyYCoordinateInMils);

    if (_secondarySideBoardExists)
    {
      if (_mainReader.doSameSideBoardInstancesExist(_secondarySideBoardTypeName))
        secondarySideBoardList = _mainReader.getSameSideBoardInstances(_secondarySideBoardTypeName);
    }

    BoardType boardType = getBoardType(primarySideBoard, secondarySideBoard, primarySideBoardType, secondarySideBoardType);
    boardType.setPanel(_panel);

    // create new Board
    Board board = new Board();
    ++_boardNameId;
    boardType.addBoard(board);
    board.setName((new Integer(_boardNameId)).toString());

    // check for possible split panel
    // cadlink split boards end with _A _B
    // testlink split boards end with _A1 _A2 or _B1 _B2
    if ((_primarySideBoardTypeName.endsWith("_A")) || (_primarySideBoardTypeName.endsWith("_B")) ||
        (_primarySideBoardTypeName.endsWith("_A1")) || (_primarySideBoardTypeName.endsWith("_B1")) ||
        (_primarySideBoardTypeName.endsWith("_A2")) || (_primarySideBoardTypeName.endsWith("_B2")))
    {
      _possibleSplitBoardTypeSet.add(boardType);
    }

    if (primarySideBoard != null)
    {
      addSideBoardToLegacyCoordinateInMilsMap(primarySideBoard, primarySideBoardLegacyCoordinateInMils);
      _mainReader.addSideBoardToDegreesRotationMap(primarySideBoard, _primarySideBoardDegreesRotation);
      _mainReader.addSideBoardToCoordinateInNanoMetersMap(primarySideBoard, primarySideBoardCoordinateInNanoMeters);
    }
    if (secondarySideBoard != null)
    {
      addSideBoardToLegacyCoordinateInMilsMap(secondarySideBoard, secondarySideBoardLegacyCoordinateInMils);
      _mainReader.addSideBoardToDegreesRotationMap(secondarySideBoard, _secondarySideBoardDegreesRotation);
      _mainReader.addSideBoardToCoordinateInNanoMetersMap(secondarySideBoard, secondarySideBoardCoordinateInNanoMeters);
    }

    // if primary side board is not already mapped, check that secondary is not mapped
    // then set maps for primary and secondary side boards
    if (_mainReader.doSameSideBoardInstancesExist(_primarySideBoardTypeName) == false)
    {
      primarySideBoardList = new ArrayList<SideBoard>();
      _mainReader.addSideBoardNameToSameSideBoardInstancesMap(_primarySideBoardTypeName, primarySideBoardList);
    }
    else
      primarySideBoardList = _mainReader.getSameSideBoardInstances(_primarySideBoardTypeName);

    if (_secondarySideBoardExists && (_mainReader.doSameSideBoardInstancesExist(_secondarySideBoardTypeName) == false) && secondarySideBoard != null)
    {
      if (_secondarySideBoardTypeName.equalsIgnoreCase(_primarySideBoardTypeName))
      {
        secondarySideBoardList = primarySideBoardList;
      }
      else
      {
        secondarySideBoardList = new ArrayList<SideBoard>();
        _mainReader.addSideBoardNameToSameSideBoardInstancesMap(_secondarySideBoardTypeName, secondarySideBoardList);
      }
    }

    // set primary side board, add to list and map
    primarySideBoardList.add(primarySideBoard);

    // set secondary side board, add to list and map if needed
    if (secondarySideBoard != null)
    {
      secondarySideBoardList.add(secondarySideBoard);
    }

    // add PanelCoordinate/Rotation pairs and place side boards on correct side of board
    _topSideBoard = null;
    _bottomSideBoard = null;
    if (_isPrimaryOnTop)
    {
      _topSideBoard = primarySideBoard;
      board.setSideBoard1(primarySideBoard);
      addBoardToLegacyPrimarySideBoardOnTopMap(board, true);
      if (secondarySideBoard != null)
      {
        _bottomSideBoard = secondarySideBoard;
        board.setSideBoard2(secondarySideBoard);
      }
    }
    else
    {
      _bottomSideBoard = primarySideBoard;
      board.setSideBoard2(primarySideBoard);
      addBoardToLegacyPrimarySideBoardOnTopMap(board, false);
      if (secondarySideBoard != null)
      {
        _topSideBoard = secondarySideBoard;
        board.setSideBoard1(secondarySideBoard);
      }
    }

    // create board settings and add variables to board
    BoardSettings boardSettings = new BoardSettings(board);
    boardSettings.setInspected(_boardInspected);
    board.setBoardSettings(boardSettings);
    
    BoardSurfaceMapSettings boardSurfaceMapSettings = new BoardSurfaceMapSettings(board);
    board.setBoardSurfaceMapSettings(boardSurfaceMapSettings);
    
    BoardAlignmentSurfaceMapSettings boardAlignmentSurfaceMapSettings = new BoardAlignmentSurfaceMapSettings(board);
    board.setBoardAlignmentSurfaceMapSettings(boardAlignmentSurfaceMapSettings);

    // set the board.setFlipped() correctly here
    if (_boardTypeToOriginalBoardMap.containsKey(boardType) == false)
    {
      Object previous = _boardTypeToOriginalBoardMap.put(boardType, board);
      Assert.expect(previous == null);
    }
    else
    {
      Board origBoard = (Board)_boardTypeToOriginalBoardMap.get(boardType);
      SideBoardType topSideBoardType = null;
      SideBoardType origTopSideBoardType = null;
      SideBoardType bottomSideBoardType = null;
      SideBoardType origBottomSideBoardType = null;
      if (_topSideBoard != null)
        topSideBoardType = _topSideBoard.getSideBoardType();
      if (origBoard.sideBoard1Exists())
        origTopSideBoardType = origBoard.getSideBoard1().getSideBoardType();

      if (_bottomSideBoard != null)
        bottomSideBoardType = _bottomSideBoard.getSideBoardType();
      if (origBoard.sideBoard2Exists())
        origBottomSideBoardType = origBoard.getSideBoard2().getSideBoardType();

      if ((topSideBoardType == origTopSideBoardType) && (bottomSideBoardType == origBottomSideBoardType))
      {
        // do nothing
      }
      else if ((topSideBoardType == origBottomSideBoardType) && (bottomSideBoardType == origTopSideBoardType))
      {
        board.flipSideBoardsForNdfReader();
      }
      else
        Assert.expect(false);
    }

    _validRequiredDataLines++;
  }

  /**
   * "@<board side name> <board orientation> <inspect the board> <board x location> <board y location> <board rotation>
   * <other board side name> <other board x location> <other board y location> <other board rotation> <panel stop>
   * @author Keith Lee
   */
  private String parsePrimarySideBoardName() throws DatastoreException
  {
    String primarySideBoardName = ParseUtil.getNextToken(_tokenizer);
    if (primarySideBoardName == null)
    {
      // must have name to have a side.
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _BOARD_SIDE_NAME_FIELD);
    }
    return primarySideBoardName;
  }

  /**
   * "@<board side name> <board orientation> <inspect the board> <board x location> <board y location> <board rotation>
   * <other board side name> <other board x location> <other board y location> <other board rotation> <panel stop>
   * @author Keith Lee
   */
  private boolean parsePrimarySideBoardOrientation() throws DatastoreException
  {
    String primaryTopOrBottomStr = ParseUtil.getNextToken(_tokenizer);
    boolean isPrimaryOnTop = false;
    if (primaryTopOrBottomStr == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _BOARD_ORIENTATION_FIELD);
    }
    // primary board orientation (TOP or BOTTOM/BOT/BTM)
    if (primaryTopOrBottomStr.equalsIgnoreCase(_TOP_SIDE_BOARD))
    {
      isPrimaryOnTop = true;
    }
    else if ( primaryTopOrBottomStr.equalsIgnoreCase(_BOTTOM_SIDE_BOARD) ||
             primaryTopOrBottomStr.equalsIgnoreCase(_BOT_SIDE_BOARD) ||
             primaryTopOrBottomStr.equalsIgnoreCase(_BTM_SIDE_BOARD) )
    {
      isPrimaryOnTop = false;
    }
    else
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new InvalidBoardSideDatastoreException(_currentFile,
                                                     _is.getLineNumber(),
                                                     _EXPECTED_SYNTAX,
                                                     _BOARD_ORIENTATION_FIELD,
                                                     primaryTopOrBottomStr);
      }
      else
      {
        String tempToken = primaryTopOrBottomStr.toUpperCase();
        if (tempToken.startsWith(_LEGACY_TOP_SIDE_BOARD))
        {
          isPrimaryOnTop = true;
        }
        else if (tempToken.startsWith(_LEGACY_BOTTOM_SIDE_BOARD))
        {
          isPrimaryOnTop = false;
        }
        else
        {
          throw new InvalidBoardSideDatastoreException(_currentFile,
                                                     _is.getLineNumber(),
                                                     _EXPECTED_SYNTAX,
                                                     _BOARD_ORIENTATION_FIELD,
                                                     primaryTopOrBottomStr);
        }
        String defaultSide;
        if (isPrimaryOnTop)
          defaultSide = "TOP";
        else
          defaultSide = "BOTTOM";
        _warnings.add(new LocalizedString("DS_NDF_WARNING_INVALID_BOARD_SIDE_KEY",
                                          new Object[]
                                          {_currentFile,
                                          new Integer(_is.getLineNumber()),
                                          _EXPECTED_SYNTAX,
                                          _BOARD_ORIENTATION_FIELD,
                                          primaryTopOrBottomStr,
                                          defaultSide}));

      }
    }
    return isPrimaryOnTop;
  }

  /**
   * @author Keith Lee
   */
  private boolean parseTestBoard() throws DatastoreException
  {
    String testBoardStr = ParseUtil.getNextToken(_tokenizer);

    boolean isBoardTested = false;

    if (testBoardStr == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _INSPECT_THE_BOARD_FIELD);
    }
    // primary board inspect (TRUE or FALSE)
    // the panel.ndf file only lets the user choose to inspect a board, not a side of a board
    try
    {
      isBoardTested = StringUtil.convertStringToBoolean(testBoardStr);
    }
    catch(BadFormatException e)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new InvalidTestStatusDatastoreException(_currentFile,
                                                      _is.getLineNumber(),
                                                      _EXPECTED_SYNTAX,
                                                      _INSPECT_THE_BOARD_FIELD,
                                                      testBoardStr);
      }
      else
      {
        // the compiler checks for "true". if not true, assumes false.
        isBoardTested = false;
        _warnings.add(new LocalizedString("DS_NDF_WARNING_INVALID_TEST_STATUS_KEY",
                      new Object[]{_currentFile,
                                   new Integer(_is.getLineNumber()),
                                   _EXPECTED_SYNTAX,
                                   _INSPECT_THE_BOARD_FIELD,
                                   testBoardStr}));
      }
    }

    return isBoardTested;
  }

  /**
   * "@<board side name> <board orientation> <inspect the board> <board x location> <board y location> <board rotation>
   * <other board side name> <other board x location> <other board y location> <other board rotation> <panel stop>
   * @author Keith Lee
   */
  private int parsePrimarySideBoardXLocation() throws DatastoreException
  {
    String primarySideBoardXLocationStr = ParseUtil.getNextToken(_tokenizer);
    int primarySideBoardXLocationInNanoMeters = 0;

    if (primarySideBoardXLocationStr == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _EXPECTED_SYNTAX,
                                                  _BOARD_X_LOCATION_FIELD);
      }
      else
      {
        primarySideBoardXLocationStr = "0";
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      _EXPECTED_SYNTAX,
                                      _BOARD_X_LOCATION_FIELD,
                                      primarySideBoardXLocationStr};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_KEY", args));
      }
    }

    double primarySideBoardXLocation = parsePositiveDouble(primarySideBoardXLocationStr, _EXPECTED_SYNTAX, _BOARD_X_LOCATION_FIELD, _parsedUnits);
    primarySideBoardXLocation = MathUtil.convertUnits(primarySideBoardXLocation, _parsedUnits, _DATASTORE_UNITS);

    // the double is in nanometers -- convert to an integer
    try
    {
      primarySideBoardXLocationInNanoMeters = MathUtil.convertDoubleToInt(primarySideBoardXLocation);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _EXPECTED_SYNTAX,
                                                                                    _BOARD_X_LOCATION_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }
    return primarySideBoardXLocationInNanoMeters;
  }

  /**
   * "@<board side name> <board orientation> <inspect the board> <board x location> <board y location> <board rotation>
   * <other board side name> <other board x location> <other board y location> <other board rotation> <panel stop>
   * @author Keith Lee
   */
  private int parsePrimarySideBoardYLocation() throws DatastoreException
  {
    String primarySideBoardYLocationStr = ParseUtil.getNextToken(_tokenizer);
    int primarySideBoardYLocationInNanoMeters = 0;

    if (primarySideBoardYLocationStr == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _EXPECTED_SYNTAX,
                                                  _BOARD_Y_LOCATION_FIELD);
      }
      else
      {
        primarySideBoardYLocationStr = "0";
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      _EXPECTED_SYNTAX,
                                      _BOARD_Y_LOCATION_FIELD,
                                      primarySideBoardYLocationStr};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_KEY", args));
      }
    }

    double primarySideBoardYLocation = parsePositiveDouble(primarySideBoardYLocationStr, _EXPECTED_SYNTAX, _BOARD_Y_LOCATION_FIELD, _parsedUnits);
    primarySideBoardYLocation = MathUtil.convertUnits(primarySideBoardYLocation, _parsedUnits, _DATASTORE_UNITS);

    // the double is in nanometers -- convert to an integer
    try
    {
      primarySideBoardYLocationInNanoMeters = MathUtil.convertDoubleToInt(primarySideBoardYLocation);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _EXPECTED_SYNTAX,
                                                                                    _BOARD_Y_LOCATION_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }
    return primarySideBoardYLocationInNanoMeters;
  }

  /**
   * "@<board side name> <board orientation> <inspect the board> <board x location> <board y location> <board rotation>
   * <other board side name> <other board x location> <other board y location> <other board rotation> <panel stop>
   * @author Keith Lee
   */
  private int parsePrimarySideBoardRotation() throws DatastoreException
  {
    String primarySideBoardRotationStr = ParseUtil.getNextToken(_tokenizer);
    int primarySideBoardRotation = -1;

    if (primarySideBoardRotationStr == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _EXPECTED_SYNTAX,
                                                  _BOARD_ROTATION_FIELD);

      }
      else
      {
        primarySideBoardRotationStr = "0";
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      _EXPECTED_SYNTAX,
                                      _BOARD_ROTATION_FIELD,
                                      primarySideBoardRotationStr};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_KEY", args));

      }
    }
    // primary board rotation (0, 90, 180 or 270)
    Exception ex = null;
    try
    {
      primarySideBoardRotation = StringUtil.convertStringToPositiveInt(primarySideBoardRotationStr);
    }
    catch (BadFormatException bfex)
    {
      ex = bfex;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        InvalidBoardRotationDatastoreException dex = new InvalidBoardRotationDatastoreException(_currentFile,
                                                                                                _is.getLineNumber(),
                                                                                                _EXPECTED_SYNTAX,
                                                                                                _BOARD_ROTATION_FIELD,
                                                                                                primarySideBoardRotationStr);
        dex.initCause(ex);
        throw dex;
      }
      else
      {
        try
        {
          double tempDoubleValue = StringUtil.convertStringToDouble(primarySideBoardRotationStr);
          tempDoubleValue = Math.floor(tempDoubleValue);
          try
          {
            primarySideBoardRotation = MathUtil.convertDoubleToInt(tempDoubleValue);
          }
          catch(ValueOutOfRangeException voore)
          {
            ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile, _is.getLineNumber(), voore);
            dex.initCause(voore);
            throw dex;
          }
        }
        catch(BadFormatException bfe)
        {
          primarySideBoardRotation = 0;
          // warn the user that we expected a number
          Object[] args = new Object[]
                          {_currentFile,
                          new Integer(_is.getLineNumber()),
                          _EXPECTED_SYNTAX,
                          _BOARD_ROTATION_FIELD,
                          primarySideBoardRotationStr,
                          new Double(primarySideBoardRotation)};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_WHOLE_NUMBER_KEY", args));
        }
      }
    }
    if (DegreesEnum.isValid(primarySideBoardRotation) == false)
    {
      throw new InvalidBoardRotationDatastoreException(_currentFile,
                                                       _is.getLineNumber(),
                                                       _EXPECTED_SYNTAX,
                                                       _BOARD_ROTATION_FIELD,
                                                       primarySideBoardRotationStr);
    }

    return primarySideBoardRotation;
  }

  /**
   * "@<board side name> <board orientation> <inspect the board> <board x location> <board y location> <board rotation>
   * <other board side name> <other board x location> <other board y location> <other board rotation> <panel stop>
   * @author Keith Lee
   */
  private String parseSecondarySideBoardName() throws DatastoreException
  {
    String secondarySideBoardName = ParseUtil.getNextToken(_tokenizer);
    // secondary board's name (if not null or (not NONE or NIL) )
    if ( (secondarySideBoardName != null) &&
        (secondarySideBoardName.equalsIgnoreCase(_NO_SECONDARY_BOARD) == false) &&
        (secondarySideBoardName.equalsIgnoreCase(_NIL_SECONDARY_BOARD) == false) )
    {
      _secondarySideBoardExists = true;
    }
    else
      _secondarySideBoardExists = false;

    return secondarySideBoardName;
  }

  /**
   * "@<board side name> <board orientation> <inspect the board> <board x location> <board y location> <board rotation>
   * <other board side name> <other board x location> <other board y location> <other board rotation> <panel stop>
   * @author Keith Lee
   */
  private int parseSecondarySideBoardXLocation() throws DatastoreException
  {
    String secondarySideBoardXLocationStr = ParseUtil.getNextToken(_tokenizer);
    int secondarySideBoardXLocationInNanoMeters = 0;

    if (secondarySideBoardXLocationStr == null)
    {
      if (_secondarySideBoardExists)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new UnexpectedEolDatastoreException(_currentFile,
              _is.getLineNumber(),
              _EXPECTED_SYNTAX,
              _OTHER_BOARD_X_LOCATION_FIELD);
        }
        else
        {
          secondarySideBoardXLocationStr = "0";
          Object[] args = new Object[]
                          {_currentFile,
                          new Integer(_is.getLineNumber()),
                          _EXPECTED_SYNTAX,
                          _BOARD_X_LOCATION_FIELD,
                          secondarySideBoardXLocationStr};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_KEY", args));
        }
      }
      else
        return 0; // secondary side board doesn't exist, and we had no token, get the heck out.
    }

    double secondarySideBoardXLocation = parsePositiveDouble(secondarySideBoardXLocationStr, _EXPECTED_SYNTAX, _OTHER_BOARD_X_LOCATION_FIELD, _parsedUnits);
    secondarySideBoardXLocation = MathUtil.convertUnits(secondarySideBoardXLocation, _parsedUnits, _DATASTORE_UNITS);

    // the double is in nanometers -- convert to an integer
    try
    {
      secondarySideBoardXLocationInNanoMeters = MathUtil.convertDoubleToInt(secondarySideBoardXLocation);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _EXPECTED_SYNTAX,
                                                                                    _OTHER_BOARD_X_LOCATION_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }
    return secondarySideBoardXLocationInNanoMeters;
  }

  /**
   * "@<board side name> <board orientation> <inspect the board> <board x location> <board y location> <board rotation>
   * <other board side name> <other board x location> <other board y location> <other board rotation> <panel stop>
   * @author Keith Lee
   */
  private int parseSecondarySideBoardYLocation() throws DatastoreException
  {
    String secondarySideBoardYLocationStr = ParseUtil.getNextToken(_tokenizer);
    int secondarySideBoardYLocationInNanoMeters = 0;

    if (secondarySideBoardYLocationStr == null)
    {
      if (_secondarySideBoardExists)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new UnexpectedEolDatastoreException(_currentFile,
              _is.getLineNumber(),
              _EXPECTED_SYNTAX,
              _OTHER_BOARD_Y_LOCATION_FIELD);
        }
        else
        {
          secondarySideBoardYLocationStr = "0";
          Object[] args = new Object[]
                          {_currentFile,
                          new Integer(_is.getLineNumber()),
                          _EXPECTED_SYNTAX,
                          _BOARD_Y_LOCATION_FIELD,
                          secondarySideBoardYLocationStr};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_KEY", args));
        }
      }
      else
        return 0;  // secondary side board doesn't exist, and we had no token, get the heck out.
    }

    double secondarySideBoardYLocation = parsePositiveDouble(secondarySideBoardYLocationStr, _EXPECTED_SYNTAX, _OTHER_BOARD_Y_LOCATION_FIELD, _parsedUnits);
    secondarySideBoardYLocation = MathUtil.convertUnits(secondarySideBoardYLocation, _parsedUnits, _DATASTORE_UNITS);

    // the double is in nanometers -- convert to an integer
    try
    {
      secondarySideBoardYLocationInNanoMeters = MathUtil.convertDoubleToInt(secondarySideBoardYLocation);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _EXPECTED_SYNTAX,
                                                                                    _OTHER_BOARD_Y_LOCATION_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }
    return secondarySideBoardYLocationInNanoMeters;
  }

  /**
   * "@<board side name> <board orientation> <inspect the board> <board x location> <board y location> <board rotation>
   * <other board side name> <other board x location> <other board y location> <other board rotation> <panel stop>
   * @author Keith Lee
   */
  private int parseSecondarySideBoardRotation() throws DatastoreException
  {
    String secondarySideBoardRotationStr = ParseUtil.getNextToken(_tokenizer);
    int secondarySideBoardRotation = -1;

    if (secondarySideBoardRotationStr == null)
    {
      if (_secondarySideBoardExists)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new UnexpectedEolDatastoreException(_currentFile,
              _is.getLineNumber(),
              _EXPECTED_SYNTAX,
              _OTHER_BOARD_ROTATION_FIELD);

        }
        else
        {
          secondarySideBoardRotationStr = "0";
          Object[] args = new Object[]
                          {_currentFile,
                          new Integer(_is.getLineNumber()),
                          _EXPECTED_SYNTAX,
                          _OTHER_BOARD_ROTATION_FIELD,
                          secondarySideBoardRotationStr};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_KEY", args));
        }
      }
      else
        return 0;  // secondary side board doesn't exist, and we had no token, get the heck out.
    }
    // primary board rotation (0, 90, 180 or 270)
    Exception ex = null;
    try
    {
      secondarySideBoardRotation = StringUtil.convertStringToPositiveInt(secondarySideBoardRotationStr);
    }
    catch (BadFormatException bfe)
    {
      ex = bfe;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        InvalidBoardRotationDatastoreException dex = new InvalidBoardRotationDatastoreException(_currentFile,
                                                                                                _is.getLineNumber(),
                                                                                                _EXPECTED_SYNTAX,
                                                                                                _OTHER_BOARD_ROTATION_FIELD,
                                                                                                secondarySideBoardRotationStr);
        dex.initCause(ex);
        throw dex;
      }
      else
      {
        try
        {
          double tempDoubleValue = StringUtil.convertStringToDouble(secondarySideBoardRotationStr);
          tempDoubleValue = Math.floor(tempDoubleValue);
          try
          {
            secondarySideBoardRotation = MathUtil.convertDoubleToInt(tempDoubleValue);
          }
          catch(ValueOutOfRangeException voore)
          {
            ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile, _is.getLineNumber(), voore);
            dex.initCause(voore);
            throw dex;
          }
        }
        catch(BadFormatException bfe)
        {
          secondarySideBoardRotation = 0;
          // warn the user that we expected a number
          Object[] args = new Object[]
                          {_currentFile,
                          new Integer(_is.getLineNumber()),
                          _EXPECTED_SYNTAX,
                          _BOARD_ROTATION_FIELD,
                          secondarySideBoardRotationStr,
                          new Double(secondarySideBoardRotation)};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_WHOLE_NUMBER_KEY", args));
        }
      }
    }
    if (DegreesEnum.isValid(secondarySideBoardRotation) == false)
    {
      throw new InvalidBoardRotationDatastoreException(_currentFile,
                                                       _is.getLineNumber(),
                                                       _EXPECTED_SYNTAX,
                                                       _OTHER_BOARD_ROTATION_FIELD,
                                                       secondarySideBoardRotationStr);
    }
    return secondarySideBoardRotation;
  }

  /**
   * "@<board side name> <board orientation> <inspect the board> <board x location> <board y location> <board rotation>
   * <other board side name> <other board x location> <other board y location> <other board rotation> <panel stop>
   * @author Keith Lee
   */
  private void parseSensorPosition() throws DatastoreException
  {
    String sensorPositionStr = ParseUtil.getNextToken(_tokenizer);
    if (sensorPositionStr == null)
    {
      // do nothing
    }
    else
    {
      if (sensorPositionStr.equalsIgnoreCase(_RIGHT_SENSOR_POSITION))
      {
        _sensorPositionsSpecified++;
      }
      else if (sensorPositionStr.equalsIgnoreCase(_LEFT_SENSOR_POSITION))
      {
        _sensorPositionsSpecified++;
      }
      else
      {
        throw new InvalidSensorPositionDatastoreException(_currentFile,
                                                          _is.getLineNumber(),
                                                          _EXPECTED_SYNTAX,
                                                          _PANEL_STOP_FIELD,
                                                          sensorPositionStr);
      }
    }
  }

  /**
   * parses an attribute line in a panel.ndf file that follows one of the following formats
   *
   * REQUIRED to be in panel.ndf
   * .dimensions: <x> <y> <thickness>
   *
   * OPTIONAL
   * Already parsed by parseDataLineRequiredByOtherLines
   * .unit: <unit>
   * .software_revision: <revision>
   *
   * .subpanel_id: <name>
   * .rotation: <theta>
   * .materials: <material>
   * .stage_speed: <speed>
   * .laser_map_stage_speed: <speed>
   * .surface_map_alignment_views: <boolean>
   * .component_sample_percent: <integer percent>
   * .dual_panel_test_cad: <boolean>
   * .panel_load_trailing_edge_detect: <boolean>
   * .contains_divided_brd: <boolean>
   * .defect_analyzer: <boolean>
   * .accept_illegal_identifiers: <boolean>
   * .awaretest_on: <boolean>
   * .pause_on_auto_align: <boolean>
   * .fiducial <#> <side> <x location> <y location> <shape> <z size> <y size> <rotation>
   * .description: <string>
   * .assembly_name: <string>
   * .assembly_number: <string>
   * .checksum: <number>
   *
   * DEPRECATED attribue lines:  (lines we recognize, but do not parse and give a warning)
   * .NUMBER_OF_FIDUCIALS:
   * .PRODUCT:
   * .NUMBER_OF_LOADED_SIDES:
   * .NUMBER_OF_BOARDS:
   * .NUMBER_OF_SUBPANEL_TOOLING_HOLES:
   * @param line - the line of panel attributes in the panel.ndf to parse.
   * @author Keith Lee
   */
  protected void parseAttributeLine(String line) throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);
    Assert.expect(line != null);

    String tempLine = null;
    tempLine = line.toUpperCase();

    if ( tempLine.startsWith(_DIMENSIONS))
    {
      if (_validRequiredAttributeLineDimensions == false)
      {
        line = line.substring(_DIMENSIONS.length());
        parseAttributeLineDimensions(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _DIMENSIONS);
        else
          duplicateTokenWarning(_DIMENSIONS);
      }
    }
    else if (tempLine.startsWith(NdfReader.getUnitToken()))
    {
      // .UNIT: was read in during parseRequiredLines(). check that this is the
      // only occurance of this attribute line in the file.
      _unitAttributeLinesFound++;
      if (_unitAttributeLinesFound > 1)
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), getUnitToken());
        else
        {
          duplicateTokenWarning(NdfReader.getUnitToken());
        }
      }
    }
    else if (tempLine.startsWith(_SOFTWARE_REVISION))
    {
      // .SOFTWARE_REVISION: was read in during parseRequiredLines(). check that
      // this is the only occurance of this attribute line in the file.
      _softwareRevisionAttributeLinesFound++;
      if (_softwareRevisionAttributeLinesFound > 1)
      {
        // if _softwareRevisionAttributeLinesFound > 1, then we already know
        throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _SOFTWARE_REVISION);
      }
    }
    else if (tempLine.startsWith(_SUBPANEL_ID))
    {
      if (_validAttributeLineSubPanelID == false)
      {
        line = line.substring(_SUBPANEL_ID.length());
        parseAttributeLineSubPanelID(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _SUBPANEL_ID);
        else
          duplicateTokenWarning(_SUBPANEL_ID);
      }
    }
    else if (tempLine.startsWith(_ROTATION))
    {
      if (_validAttributeLineRotation == false)
      {
        line = line.substring(_ROTATION.length());
        parseAttributeLineRotation(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _ROTATION);
        else
          duplicateTokenWarning(_ROTATION);
      }
    }
    else if (tempLine.startsWith(_MATERIALS))
    {
      // if .MATERIALS: is missing it's token, function will return and
      // validFileCheck will set to default. but since function returns before
      // _validAttributeLineMaterials is set, we must check that it only occurs
      // once.
      if ((_validAttributeLineMaterials == false) && (_materialsAttributeLinesFound == 0))
      {
        line = line.substring(_MATERIALS.length());
        parseAttributeLineMaterials(line);
        _materialsAttributeLinesFound++;
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _MATERIALS);
        else
          duplicateTokenWarning(_MATERIALS);
      }
    }
    else if (tempLine.startsWith(_STAGE_SPEED))
    {
      if (_validAttributeLineStageSpeed == false)
      {
        line = line.substring(_STAGE_SPEED.length());
        parseAttributeLineStageSpeed(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _STAGE_SPEED);
        else
          duplicateTokenWarning(_STAGE_SPEED);
      }
    }
    else if (tempLine.startsWith(_LASER_MAP_STAGE_SPEED))
    {
      if (_validAttributeLineLaserMapStageSpeed == false)
      {
        line = line.substring(_LASER_MAP_STAGE_SPEED.length());
        parseAttributeLineLaserMapStageSpeed(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _LASER_MAP_STAGE_SPEED);
        else
          duplicateTokenWarning(_LASER_MAP_STAGE_SPEED);
      }
    }
    else if (tempLine.startsWith(_SURFACE_MAP_ALIGNMENT_VIEWS))
    {
      if (_validAttributeLineSurfaceMapAlignmentViews == false)
      {
        line = line.substring(_SURFACE_MAP_ALIGNMENT_VIEWS.length());
        parseAttributeLineSurfaceMapAlignmentViews(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _SURFACE_MAP_ALIGNMENT_VIEWS);
        else
          duplicateTokenWarning(_SURFACE_MAP_ALIGNMENT_VIEWS);
      }
    }
    else if (tempLine.startsWith(_COMPONENT_SAMPLE_PERCENT))
    {
      if (_validAttributeLineComponentSamplePercent == false)
      {
        line = line.substring(_COMPONENT_SAMPLE_PERCENT.length());
        parseAttributeLineComponentSamplePercent(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _COMPONENT_SAMPLE_PERCENT);
        else
          duplicateTokenWarning(_COMPONENT_SAMPLE_PERCENT);
      }
    }
    else if (tempLine.startsWith(_DUAL_PANEL_TEST_CAD))
    {
      if (_validAttributeLineDualPanelTestCad == false)
      {
        line = line.substring(_DUAL_PANEL_TEST_CAD.length());
        parseAttributeLineDualPanelTestCad(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _DUAL_PANEL_TEST_CAD);
        else
          duplicateTokenWarning(_DUAL_PANEL_TEST_CAD);
      }
    }
    else if (tempLine.startsWith(_PANEL_LOAD_TRAILING_EDGE_DETECT))
    {
      if (_validAttributeLinePanelLoadTrailingEdgeDetect == false)
      {
        line = line.substring(_PANEL_LOAD_TRAILING_EDGE_DETECT.length());
        parseAttributeLinePanelLoadTrailingEdgeDetect(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _PANEL_LOAD_TRAILING_EDGE_DETECT);
        else
          duplicateTokenWarning(_PANEL_LOAD_TRAILING_EDGE_DETECT);
      }
    }
    else if (tempLine.startsWith(_CONTAINS_DIVIDED_BRD))
    {
      if (_validAttributeLineContainsDividedBoard == false)
      {
        line = line.substring(_CONTAINS_DIVIDED_BRD.length());
        parseAttributeLineContainsDividedBrd(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _CONTAINS_DIVIDED_BRD);
        else
          duplicateTokenWarning(_CONTAINS_DIVIDED_BRD);
      }
    }
    else if (tempLine.startsWith(_DEFECT_ANALYZER))
    {
      if (_validAttributeLineDefectAnalyzer == false)
      {
        line = line.substring(_DEFECT_ANALYZER.length());
        parseAttributeLineDefectAnalyzer(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _DEFECT_ANALYZER);
        else
          duplicateTokenWarning(_DEFECT_ANALYZER);
      }
    }
    else if (tempLine.startsWith(_ACCEPT_ILLEGAL_IDENTIFIERS))
    {
      if (_validAttributeLineAcceptIllegalIdentifiers == false)
      {
        line = line.substring(_ACCEPT_ILLEGAL_IDENTIFIERS.length());
        parseAttributeLineAcceptIllegalIdentifiers(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _ACCEPT_ILLEGAL_IDENTIFIERS);
        else
          duplicateTokenWarning(_ACCEPT_ILLEGAL_IDENTIFIERS);
      }
    }
    else if (tempLine.startsWith(_AWARETEST_ON))
    {
      if (_validAttributeLineAwareTestOn == false)
      {
        line = line.substring(_AWARETEST_ON.length());
        parseAttributeLineAwareTestOn(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _AWARETEST_ON);
        else
          duplicateTokenWarning(_AWARETEST_ON);
      }
    }
    else if (tempLine.startsWith(_PAUSE_ON_AUTO_ALIGN))
    {
      if (_validAttributeLinePauseOnAutoAlign == false)
      {
        line = line.substring(_PAUSE_ON_AUTO_ALIGN.length());
        parseAttributeLinePauseOnAutoAlign(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _PAUSE_ON_AUTO_ALIGN);
        else
          duplicateTokenWarning(_PAUSE_ON_AUTO_ALIGN);
      }
    }
    else if (tempLine.startsWith(_FIDUCIAL))
    {
      line = line.substring(_FIDUCIAL.length());
      parseAttributeLineFiducial(line);
    }
    else if (tempLine.startsWith(_DESCRIPTION))
    {
      if (_validAttributeLineDescription == false)
      {
        line = line.substring(_DESCRIPTION.length());
        parseAttributeLineDescription(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _DESCRIPTION);
        }
        else
        {
          duplicateTokenWarning(_DESCRIPTION);
        }
      }
    }
    else if (tempLine.startsWith(_ASSEMBLY_NAME))
    {
      if (_validAttributeLineAssemblyName == false)
      {
        line = line.substring(_ASSEMBLY_NAME.length());
        parseAttributeLineAssemblyName(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _ASSEMBLY_NAME);
        }
        else
        {
          duplicateTokenWarning(_ASSEMBLY_NAME);
        }
      }
    }
    else if (tempLine.startsWith(_ASSEMBLY_NUMBER))
    {
      if (_validAttributeLineAssemblyNumber == false)
      {
        line = line.substring(_ASSEMBLY_NUMBER.length());
        parseAttributeLineAssemblyNumber(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _ASSEMBLY_NUMBER);
        else
          duplicateTokenWarning(_ASSEMBLY_NUMBER);
      }
    }
    else if (tempLine.startsWith(CHECKSUM))
    {
      line = line.substring(CHECKSUM.length());
      parseAttributeLineCheckSum(line);
    }
    else if (tempLine.startsWith(_NUMBER_OF_FIDUCIALS))
      deprecatedTokenWarning(_NUMBER_OF_FIDUCIALS);
    else if (tempLine.startsWith(_PRODUCT))
      deprecatedTokenWarning(_PRODUCT);
    else if (tempLine.startsWith(_NUMBER_OF_LOADED_SIDES))
      deprecatedTokenWarning(_NUMBER_OF_LOADED_SIDES);
    else if (tempLine.startsWith(_NUMBER_OF_BOARDS))
      deprecatedTokenWarning(_NUMBER_OF_BOARDS);
    else if (tempLine.startsWith(_NUMBER_OF_SUBPANEL_TOOLING_HOLES))
      deprecatedTokenWarning(_NUMBER_OF_SUBPANEL_TOOLING_HOLES);
    else
    {
      int index = line.indexOf(':');
      String token = line;
      if (index != -1)
        token = line.substring(0, index + 1);

      if (_mainReader.ndfsAreWellFormed())
      {
        // invalid or unknown attribute line
        String expected = _SUBPANEL_ID + " " +
                          NdfReader.getUnitToken() + " " +
                          CHECKSUM + " " +
                          _DIMENSIONS + " " +
                          _MATERIALS + " " +
                          _COMPONENT_SAMPLE_PERCENT + " " +
                          _SURFACE_MAP_ALIGNMENT_VIEWS + " " +
                          _PAUSE_ON_AUTO_ALIGN + " " +
                          _FIDUCIAL + " " +
                          _ROTATION + " " +
                          _STAGE_SPEED + " " +
                          _DEFECT_ANALYZER + " " +
                          _LASER_MAP_STAGE_SPEED + " " +
                          _DUAL_PANEL_TEST_CAD + " " +
                          _PANEL_LOAD_TRAILING_EDGE_DETECT + " " +
                          _CONTAINS_DIVIDED_BRD + " " +
                          _ACCEPT_ILLEGAL_IDENTIFIERS + " " +
                          _DESCRIPTION + " " +
                          _ASSEMBLY_NAME + " " +
                          _ASSEMBLY_NUMBER + " " +
                          _AWARETEST_ON + " or " +
                          _SOFTWARE_REVISION;
        throw new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), expected, token);
      }
      else
      {
        unrecognizedTokenWarning(token);
      }
    }
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .subpanel_id: <name>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineSubPanelID(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // panel name
    // this is strictly informational. the actual panel name is passed into
    // MainReader.load() via Panel.load() and can also be found in the
    // dir_id.dat file found in the same directory as the panel.ndf file

    if (_mainReader.ndfsAreWellFormed())
    {
      StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
      String token = ParseUtil.getNextToken(st);
      if (token == null)
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _ATTRIBUTE_LINE_SUBPANEL_ID_EXPECTED_SYNTAX,
                                                  _PANEL_NAME_FIELD);
      }
      checkForExtraDataAtEndOfLine(st, _ATTRIBUTE_LINE_SUBPANEL_ID_EXPECTED_SYNTAX);
    }

    _validAttributeLineSubPanelID = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .dimensions: <width> <length> <thickness>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineDimensions(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // panel width, panel length and board thickness from pad level to pad level
    //values must be valid (cannot be null or <= 0)
    int panelWidthInNanoMeters = 0;
    int panelLengthInNanoMeters = 0;
    int padToPadThicknessInNanoMeters = 0;
    double tempDoubleValue = 0.0;

    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                               _is.getLineNumber(),
                                               _ATTRIBUTE_LINE_DIMENSIONS_EXPECTED_SYNTAX,
                                               _PANEL_WIDTH_FIELD);
    }
    Exception ex = null;
    try
    {
      tempDoubleValue = StringUtil.convertStringToPositiveDouble(token);
      // to fix CR08931 i removed the round of tempDoubleValue after it has been converted from parsedUnits to MILS.
      // this was causing a rounding problem and so when we built up the rtfLegacyBoardName, certain panel programs were
      // building wrong board name directories causing a fov_xref.rtf file not found error.
      _panelWidthInMils = MathUtil.convertUnits(tempDoubleValue, _parsedUnits, MathUtilEnum.MILS);
      tempDoubleValue = MathUtil.convertUnits(tempDoubleValue, _parsedUnits, _DATASTORE_UNITS);
      try
      {
        panelWidthInNanoMeters = MathUtil.convertDoubleToInt(tempDoubleValue);
      }
      catch(ValueOutOfRangeException voore)
      {
        ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile, _is.getLineNumber(), voore);
        dex.initCause(voore);
        throw dex;
      }
      if (panelWidthInNanoMeters == 0)
      {
        throw new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                  _is.getLineNumber(),
                                                                  _ATTRIBUTE_LINE_DIMENSIONS_EXPECTED_SYNTAX,
                                                                  _PANEL_WIDTH_FIELD,
                                                                  token);
      }
    }
    catch(BadFormatException bfe)
    {
      ex = bfe;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      ExpectedPositiveNonZeroNumberDatastoreException dex = new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                                                                _is.getLineNumber(),
                                                                                                                _ATTRIBUTE_LINE_DIMENSIONS_EXPECTED_SYNTAX,
                                                                                                                _PANEL_WIDTH_FIELD,
                                                                                                                token);
      dex.initCause(ex);
      throw dex;
    }


    // now go for length
    token = ParseUtil.getNextToken(st);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                               _is.getLineNumber(),
                                               _ATTRIBUTE_LINE_DIMENSIONS_EXPECTED_SYNTAX,
                                               _PANEL_LENGTH_FIELD);
    }
    try
    {
      tempDoubleValue = StringUtil.convertStringToPositiveDouble(token);
      tempDoubleValue = MathUtil.convertUnits(tempDoubleValue, _parsedUnits, _DATASTORE_UNITS);
      try
      {
        panelLengthInNanoMeters = MathUtil.convertDoubleToInt(tempDoubleValue);
      }
      catch(ValueOutOfRangeException voore)
      {
        ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile, _is.getLineNumber(), voore);
        dex.initCause(voore);
        throw dex;
      }
      if (panelLengthInNanoMeters == 0)
      {
        throw new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                  _is.getLineNumber(),
                                                                  _ATTRIBUTE_LINE_DIMENSIONS_EXPECTED_SYNTAX,
                                                                  _PANEL_LENGTH_FIELD,
                                                                  token);
      }
    }
    catch (BadFormatException bfe)
    {
      ex = bfe;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      ExpectedPositiveNonZeroNumberDatastoreException dex = new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                                                                _is.getLineNumber(),
                                                                                                                _ATTRIBUTE_LINE_DIMENSIONS_EXPECTED_SYNTAX,
                                                                                                                _PANEL_LENGTH_FIELD,
                                                                                                                token);
      dex.initCause(ex);
      throw dex;
    }

    // last one, thickness
    token = ParseUtil.getNextToken(st);
    if ((token != null) && (token.equals("<>")))
      token = "0";
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                               _is.getLineNumber(),
                                               _ATTRIBUTE_LINE_DIMENSIONS_EXPECTED_SYNTAX,
                                               _PAD_TO_PAD_THICKNESS_FIELD);
    }
    ex = null;
    try
    {
      tempDoubleValue = StringUtil.convertStringToPositiveDouble(token);
      tempDoubleValue = MathUtil.convertUnits(tempDoubleValue, _parsedUnits, _DATASTORE_UNITS);
      try
      {
        padToPadThicknessInNanoMeters = MathUtil.convertDoubleToInt(tempDoubleValue);
      }
      catch(ValueOutOfRangeException voore)
      {
        ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile, _is.getLineNumber(), voore);
        dex.initCause(voore);
        throw dex;
      }
      if (padToPadThicknessInNanoMeters < 0)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                  _is.getLineNumber(),
                                                                  _ATTRIBUTE_LINE_DIMENSIONS_EXPECTED_SYNTAX,
                                                                  _PAD_TO_PAD_THICKNESS_FIELD,
                                                                  token);
        }
        else
        {
          // a value of 1 nanometer will be used instead
          _warnings.add(new LocalizedString("DS_NDF_WARNING_PANEL_THICKNESS_IS_ZERO_KEY",
                        new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber())}));
          padToPadThicknessInNanoMeters = 1;
        }
      }
    }
    catch(BadFormatException fex)
    {
      ex = fex;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      ExpectedPositiveNonZeroNumberDatastoreException dex = new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                                                                _is.getLineNumber(),
                                                                                                                _ATTRIBUTE_LINE_DIMENSIONS_EXPECTED_SYNTAX,
                                                                                                                _PAD_TO_PAD_THICKNESS_FIELD,
                                                                                                                token);
      dex.initCause(ex);
      throw dex;
    }

    checkForExtraDataAtEndOfLine(st, _ATTRIBUTE_LINE_DIMENSIONS_EXPECTED_SYNTAX);

    _panel.setWidthInNanoMeters(panelWidthInNanoMeters);
    _panel.setLengthInNanoMeters(panelLengthInNanoMeters);
    _panel.setThicknessInNanoMeters(padToPadThicknessInNanoMeters);

    _validRequiredAttributeLineDimensions = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .materials: <material>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineMaterials(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    String solderMaterials = null;

    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _ATTRIBUTE_LINE_MATERIALS_EXPECTED_SYNTAX,
                                                  _MATERIAL_FIELD);
      }
      else
      {
        token = _DEFAULT_SOLDER_MATERIALS_VALUE;
        Object[] args = new Object[]
                        {_currentFile,
                        new Integer(_is.getLineNumber()),
                        _ATTRIBUTE_LINE_MATERIALS_EXPECTED_SYNTAX,
                        _MATERIAL_FIELD,
                        token};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_KEY", args));
      }
    }

    solderMaterials = token;

    checkForExtraDataAtEndOfLine(st, _ATTRIBUTE_LINE_MATERIALS_EXPECTED_SYNTAX);

    // check that the materials is the same as the default
    if ( (solderMaterials.equalsIgnoreCase(_DEFAULT_SOLDER_MATERIALS_VALUE) == false) &&
        (solderMaterials.equalsIgnoreCase(_DEFAULT_SOLDER_MATERIALS_KEYWORD) == false) )
    {
// for genesis this warning is not needed anymore
//      // create warning that solder material is not default
//      Object[] args = new Object[]{ _currentFile,
//                                    new Integer(_is.getLineNumber()),
//                                    solderMaterials,
//                                    Directory.getLegacyConfigDir() };
//      _warnings.add(new LocalizedString("DS_NDF_WARNING_NON_DEFAULT_SOLDER_MATERIAL_KEY", args));
    }

    _materialsAttributeLineTokenExist = true;
    _validAttributeLineMaterials = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .rotation: <theta>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineRotation(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // panel rotation
    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _ATTRIBUTE_LINE_ROTATION_EXPECTED_SYNTAX,
                                                  _DEGREES_ROTATION_FIELD);
      }
      else
      {
        token = "0";
        // if not specified at all, Zero degrees will be used
        Object[] args = new Object[]
                {_currentFile,
                new Integer(_is.getLineNumber()),
                _ATTRIBUTE_LINE_ROTATION_EXPECTED_SYNTAX,
                _DEGREES_ROTATION_FIELD,
                token};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_KEY", args));
      }
    }
    int panelDegreesRotation = 0;
    Exception ex = null;
    try
    {
      panelDegreesRotation = StringUtil.convertStringToPositiveInt(token);
      if (DegreesEnum.isValid(panelDegreesRotation) == false)
      {
        throw new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), EXPECTED_0_90_180_OR_270_DEGREES, token);
      }
    }
    catch(BadFormatException e)
    {
      ex = e;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), EXPECTED_0_90_180_OR_270_DEGREES, token);
        dex.initCause(ex);
        throw dex;
      }
      else
      {
        try
        {
          double tempDoubleValue = StringUtil.convertStringToDouble(token);
          tempDoubleValue = Math.floor(tempDoubleValue);
          try
          {
            panelDegreesRotation = MathUtil.convertDoubleToInt(tempDoubleValue);
          }
          catch(ValueOutOfRangeException voore)
          {
            ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile, _is.getLineNumber(), voore);
            dex.initCause(voore);
            throw dex;
          }
          if (DegreesEnum.isValid(panelDegreesRotation) == false)
          {
            throw new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), EXPECTED_0_90_180_OR_270_DEGREES, token);
          }
        }
        catch(BadFormatException bfe)
        {
          panelDegreesRotation = 0;
          if (DegreesEnum.isValid(panelDegreesRotation) == false)
          {
            FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), EXPECTED_0_90_180_OR_270_DEGREES, token);
            dex.initCause(bfe);
            throw dex;
          }
        }
      }
    }

    checkForExtraDataAtEndOfLine(st, _ATTRIBUTE_LINE_ROTATION_EXPECTED_SYNTAX);

    // save degrees rotation and call the set method for it after width and length are set
    // otherwise there will be an assert
    _degreesRotation = panelDegreesRotation;
    _validAttributeLineRotation = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .stage_speed: <speed>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineStageSpeed(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // 5dx stage speed
    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);

    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _ATTRIBUTE_LINE_STAGE_SPEED_EXPECTED_SYNTAX,
                                                  _STAGE_SPEED_NUMBER_FIELD);
      }
      else
      {
        // if not specified a default of 2 will be used
        Object[] args = new Object[]
                        {_currentFile,
                        new Integer(_is.getLineNumber()),
                        _ATTRIBUTE_LINE_STAGE_SPEED_EXPECTED_SYNTAX,
                        _STAGE_SPEED_NUMBER_FIELD,
                        new Integer(2)};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_KEY", args));
      }
    }
    else
    {
      int motionProfile = parseInt(token, _ATTRIBUTE_LINE_STAGE_SPEED_EXPECTED_SYNTAX, _STAGE_SPEED_NUMBER_FIELD, _MIN_STAGE_SPEED, _MAX_STAGE_SPEED);
    }

    checkForExtraDataAtEndOfLine(st, _ATTRIBUTE_LINE_STAGE_SPEED_EXPECTED_SYNTAX);

    _validAttributeLineStageSpeed = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .component_sample_percent: <sample percent>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineComponentSamplePercent(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // component sampling rate
    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    int componentSamplingRateInPercent;

    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _ATTRIBUTE_LINE_COMPONENT_SAMPLE_PERCENT_EXPECTED_SYNTAX,
                                                  _SAMPLE_PERCENT_NUMBER_FIELD);
      }
      else
      {
        // the default is 100 percent.  Shocking.
        componentSamplingRateInPercent = _DEFAULT_COMPONENT_SAMPLING_RATE;
        Object[] args = new Object[]
                        {_currentFile,
                        new Integer(_is.getLineNumber()),
                        _ATTRIBUTE_LINE_COMPONENT_SAMPLE_PERCENT_EXPECTED_SYNTAX,
                        _SAMPLE_PERCENT_NUMBER_FIELD,
                        new Integer(componentSamplingRateInPercent)};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_KEY", args));
      }
    }
    else
      componentSamplingRateInPercent = parseInt(token, _ATTRIBUTE_LINE_COMPONENT_SAMPLE_PERCENT_EXPECTED_SYNTAX, _SAMPLE_PERCENT_NUMBER_FIELD, _MIN_SAMPLING_RATE, _MAX_SAMPLING_RATE);

    checkForExtraDataAtEndOfLine(st, _ATTRIBUTE_LINE_COMPONENT_SAMPLE_PERCENT_EXPECTED_SYNTAX);

    _validAttributeLineComponentSamplePercent = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .surface_map_alignment_views: <boolean>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineSurfaceMapAlignmentViews(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // surface map alignment views
    boolean surfaceMapAlignmentViews;
    // I can get away with not localizing this, because everything here is a token
    String expected_syntax = _SURFACE_MAP_ALIGNMENT_VIEWS + " " + EXPECTED_BOOLEAN;

    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new MissingTokenBooleanDatastoreException(_currentFile, _is.getLineNumber(), _SURFACE_MAP_ALIGNMENT_VIEWS);
      }
      else
      {
        _warnings.add(new LocalizedString("DS_NDF_WARNING_TOKEN_MISSING_TRUE_OR_FALSE_USE_DEFAULT_KEY",
                      new Object[] { _currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _SURFACE_MAP_ALIGNMENT_VIEWS,
                                     new Boolean(_ASSUMED_SURFACE_MAP_ALIGNMENT_VIEWS)}));
        surfaceMapAlignmentViews = _ASSUMED_SURFACE_MAP_ALIGNMENT_VIEWS;
      }
    }
    else
    {
      try
      {
        surfaceMapAlignmentViews = StringUtil.convertStringToBoolean(token);
      }
      catch(BadFormatException e)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), EXPECTED_BOOLEAN, token);
          dex.initCause(e);
          throw dex;
        }
        else
        {
          String tempToken = token.toUpperCase();
          if (tempToken.startsWith(_LEGACY_FALSE))
            surfaceMapAlignmentViews = false;
          else
            surfaceMapAlignmentViews = true;
          _warnings.add(new LocalizedString("DS_NDF_WARNING_TRUE_OR_FALSE_CORRUPT_KEY",
                        new Object[] { _currentFile,
                                       new Integer(_is.getLineNumber()),
                                       expected_syntax,
                                       token,
                                       new Boolean(surfaceMapAlignmentViews) }));
        }
      }
    }

    checkForExtraDataAtEndOfLine(st, expected_syntax);

    _validAttributeLineSurfaceMapAlignmentViews = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .pause_on_auto_align: <boolean>
   *
   * @author Keith Lee
   */
  private void parseAttributeLinePauseOnAutoAlign(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // pause on auto align
    boolean pauseOnAutoAlign;
    String expected_syntax = _PAUSE_ON_AUTO_ALIGN + " " + EXPECTED_BOOLEAN;

    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new MissingTokenBooleanDatastoreException(_currentFile, _is.getLineNumber(), _PAUSE_ON_AUTO_ALIGN);
      }
      else
      {
        _warnings.add(new LocalizedString("DS_NDF_WARNING_TOKEN_MISSING_TRUE_OR_FALSE_USE_DEFAULT_KEY",
                      new Object[] { _currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _PAUSE_ON_AUTO_ALIGN,
                                     new Boolean(_ASSUMED_PAUSE_ON_AUTO_ALIGN) }));

        pauseOnAutoAlign = _ASSUMED_PAUSE_ON_AUTO_ALIGN;
      }
    }
    else
    {
      try
      {
        pauseOnAutoAlign = StringUtil.convertStringToBoolean(token);
      }
      catch(BadFormatException e)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), EXPECTED_BOOLEAN, token);
          dex.initCause(e);
          throw dex;
        }
        else
        {
          String tempToken = token.toUpperCase();
          if (tempToken.startsWith(_LEGACY_FALSE))
            pauseOnAutoAlign = false;
          else
            pauseOnAutoAlign = true;
          _warnings.add(new LocalizedString("DS_NDF_WARNING_TRUE_OR_FALSE_CORRUPT_KEY",
                        new Object[] { _currentFile,
                                       new Integer(_is.getLineNumber()),
                                       expected_syntax,
                                       token,
                                       new Boolean(pauseOnAutoAlign)}));

        }
      }
    }

    checkForExtraDataAtEndOfLine(st, expected_syntax);

    _validAttributeLinePauseOnAutoAlign = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .defect_analyzer: <boolean>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineDefectAnalyzer(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // defect analyzer
    boolean defectAnalyzer;
    String expected_syntax = _DEFECT_ANALYZER + " " + EXPECTED_BOOLEAN;

    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new MissingTokenBooleanDatastoreException(_currentFile, _is.getLineNumber(), _DEFECT_ANALYZER);
      }
      else
      {
        _warnings.add(new LocalizedString("DS_NDF_WARNING_TOKEN_MISSING_TRUE_OR_FALSE_USE_DEFAULT_KEY",
                      new Object[] { _currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _DEFECT_ANALYZER,
                                     new Boolean(_ASSUMED_DEFECT_ANALYZER) }));

        defectAnalyzer = _ASSUMED_DEFECT_ANALYZER;
      }
    }
    else
    {
      try
      {
        defectAnalyzer = StringUtil.convertStringToBoolean(token);
      }
      catch(BadFormatException e)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), EXPECTED_BOOLEAN, token);
          dex.initCause(e);
          throw dex;
        }
        else
        {
          String tempToken = token.toUpperCase();
          if (tempToken.startsWith(_LEGACY_FALSE))
            defectAnalyzer = false;
          else
            defectAnalyzer = true;
          _warnings.add(new LocalizedString("DS_NDF_WARNING_TRUE_OR_FALSE_CORRUPT_KEY",
                        new Object[] { _currentFile,
                                       new Integer(_is.getLineNumber()),
                                       expected_syntax,
                                       token,
                                       new Boolean(defectAnalyzer)}));
        }
      }
    }

    checkForExtraDataAtEndOfLine(st, expected_syntax);

    _validAttributeLineDefectAnalyzer = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .laser_map_stage_speed: <speed>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineLaserMapStageSpeed(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // 5dx laser map stage speed
    // the legacy code implementation is weird. if there is no token for the
    // attribute line, the laser map stage speed is 0. if there is a token, the
    // value must be between 1 and 20.

    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _ATTRIBUTE_LINE_LASER_MAP_STAGE_SPEED_EXPECTED_SYNTAX,
                                                  _LASER_MAP_STAGE_SPEED_NUMBER_FIELD);
      }
      else
      {
        // if not specified a default of 1 will be used
        Object[] args = new Object[]
                        {_currentFile,
                        new Integer(_is.getLineNumber()),
                        _ATTRIBUTE_LINE_LASER_MAP_STAGE_SPEED_EXPECTED_SYNTAX,
                        _LASER_MAP_STAGE_SPEED_NUMBER_FIELD,
                        new Integer(1)};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_KEY", args));

      }
    }
    else
    {
      int motionProfile = parseInt(token, _ATTRIBUTE_LINE_LASER_MAP_STAGE_SPEED_EXPECTED_SYNTAX, _LASER_MAP_STAGE_SPEED_NUMBER_FIELD, _MIN_STAGE_SPEED, _MAX_STAGE_SPEED);
    }

    checkForExtraDataAtEndOfLine(st, _ATTRIBUTE_LINE_LASER_MAP_STAGE_SPEED_EXPECTED_SYNTAX);

    _validAttributeLineLaserMapStageSpeed = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .dual_panel_test_cad: <boolean>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineDualPanelTestCad(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // dual panel test
    boolean dualPanelTest;
    String expected_syntax = _DUAL_PANEL_TEST_CAD + " " + EXPECTED_BOOLEAN;


    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new MissingTokenBooleanDatastoreException(_currentFile, _is.getLineNumber(), _DUAL_PANEL_TEST_CAD);
      }
      else
      {
        _warnings.add(new LocalizedString("DS_NDF_WARNING_TOKEN_MISSING_TRUE_OR_FALSE_USE_DEFAULT_KEY",
                      new Object[] { _currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _DUAL_PANEL_TEST_CAD,
                                     new Boolean(_ASSUMED_DUAL_PANEL_TEST) }));

        dualPanelTest = _ASSUMED_DUAL_PANEL_TEST;
      }
    }
    else
    {
      try
      {
        dualPanelTest = StringUtil.convertStringToBoolean(token);
      }
      catch(BadFormatException e)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), EXPECTED_BOOLEAN, token);
          dex.initCause(e);
          throw dex;
        }
        else
        {
          String tempToken = token.toUpperCase();
          if (tempToken.startsWith(_LEGACY_FALSE))
            dualPanelTest = false;
          else
            dualPanelTest = true;
          _warnings.add(new LocalizedString("DS_NDF_WARNING_TRUE_OR_FALSE_CORRUPT_KEY",
                        new Object[] { _currentFile,
                                       new Integer(_is.getLineNumber()),
                                       expected_syntax,
                                       token,
                                       new Boolean(dualPanelTest)}));
        }
      }
    }

    checkForExtraDataAtEndOfLine(st, expected_syntax);

    _validAttributeLineDualPanelTestCad = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .panel_load_trailing_edge_detect: <boolean>
   *
   * @author Keith Lee
   */
  private void parseAttributeLinePanelLoadTrailingEdgeDetect(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // panel load trailing edge
    boolean detectTrailingEdge;
    String expected_syntax = _PANEL_LOAD_TRAILING_EDGE_DETECT + " " + EXPECTED_BOOLEAN;

    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new MissingTokenBooleanDatastoreException(_currentFile, _is.getLineNumber(), _PANEL_LOAD_TRAILING_EDGE_DETECT);
      }
      else
      {
        _warnings.add(new LocalizedString("DS_NDF_WARNING_TOKEN_MISSING_TRUE_OR_FALSE_USE_DEFAULT_KEY",
                      new Object[] { _currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _PANEL_LOAD_TRAILING_EDGE_DETECT,
                                     new Boolean(_ASSUMED_DETECT_TRAILING_EDGE) }));

        detectTrailingEdge = _ASSUMED_DETECT_TRAILING_EDGE;
      }
    }
    else
    {
      try
      {
        detectTrailingEdge = StringUtil.convertStringToBoolean(token);
      }
      catch(BadFormatException e)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), EXPECTED_BOOLEAN, token);
          dex.initCause(e);
          throw dex;
        }
        else
        {
          String tempToken = token.toUpperCase();
          if (tempToken.startsWith(_LEGACY_FALSE))
            detectTrailingEdge = false;
          else
            detectTrailingEdge = true;
          _warnings.add(new LocalizedString("DS_NDF_WARNING_TRUE_OR_FALSE_CORRUPT_KEY",
                        new Object[] { _currentFile,
                                       new Integer(_is.getLineNumber()),
                                       expected_syntax,
                                       token,
                                       new Boolean(detectTrailingEdge)}));
        }
      }
    }

    checkForExtraDataAtEndOfLine(st, expected_syntax);

    _validAttributeLinePanelLoadTrailingEdgeDetect = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .contains_divided_brd: <boolean>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineContainsDividedBrd(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // contains divided board
    String expected_syntax = _CONTAINS_DIVIDED_BRD + " " + EXPECTED_BOOLEAN;

    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new MissingTokenBooleanDatastoreException(_currentFile, _is.getLineNumber(), _CONTAINS_DIVIDED_BRD);
      }
      else
      {
        _warnings.add(new LocalizedString("DS_NDF_WARNING_TOKEN_MISSING_TRUE_OR_FALSE_USE_DEFAULT_KEY",
                      new Object[] { _currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _CONTAINS_DIVIDED_BRD,
                                     new Boolean(_ASSUMED_CONTAINS_DIVIDED_BOARD) }));

       _containsDividedPanel = _ASSUMED_CONTAINS_DIVIDED_BOARD;
      }
    }
    else
    {
      try
      {
        _containsDividedPanel = StringUtil.convertStringToBoolean(token);
      }
      catch(BadFormatException e)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), EXPECTED_BOOLEAN, token);
          dex.initCause(e);
          throw dex;
        }
        else
        {
          String tempToken = token.toUpperCase();
          if (tempToken.startsWith(_LEGACY_FALSE))
            _containsDividedPanel = false;
          else
            _containsDividedPanel = true;
          _warnings.add(new LocalizedString("DS_NDF_WARNING_TRUE_OR_FALSE_CORRUPT_KEY",
                        new Object[] { _currentFile,
                                       new Integer(_is.getLineNumber()),
                                       expected_syntax,
                                       token,
                                       new Boolean(_containsDividedPanel)}));

        }
      }
    }

    checkForExtraDataAtEndOfLine(st, expected_syntax);

    _validAttributeLineContainsDividedBoard = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .accept_illegal_identifiers: <boolean>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineAcceptIllegalIdentifiers(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // this attribute line is used by legacy code only, but we will still read it.
    // accept illegal identifiers
    boolean acceptIllegalIdentifiers;
    String expected_syntax = _ACCEPT_ILLEGAL_IDENTIFIERS + " " + EXPECTED_BOOLEAN;

    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new MissingTokenBooleanDatastoreException(_currentFile, _is.getLineNumber(), _ACCEPT_ILLEGAL_IDENTIFIERS);
      }
      else
      {
        _warnings.add(new LocalizedString("DS_NDF_WARNING_TOKEN_MISSING_TRUE_OR_FALSE_USE_DEFAULT_KEY",
                      new Object[] { _currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _ACCEPT_ILLEGAL_IDENTIFIERS,
                                     new Boolean(_ASSUMED_ACCEPT_ILLEGAL_IDENTIFIERS) }));

        acceptIllegalIdentifiers = _ASSUMED_ACCEPT_ILLEGAL_IDENTIFIERS;
      }
    }
    else
    {
      try
      {
        acceptIllegalIdentifiers = StringUtil.convertStringToBoolean(token);
      }
      catch(BadFormatException e)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), EXPECTED_BOOLEAN, token);
          dex.initCause(e);
          throw dex;
        }
        else
        {
          String tempToken = token.toUpperCase();
          if (tempToken.startsWith(_LEGACY_FALSE))
            acceptIllegalIdentifiers = false;
          else
            acceptIllegalIdentifiers = true;
          _warnings.add(new LocalizedString("DS_NDF_WARNING_TRUE_OR_FALSE_CORRUPT_KEY",
                        new Object[] { _currentFile,
                                       new Integer(_is.getLineNumber()),
                                       expected_syntax,
                                       token,
                                       new Boolean(acceptIllegalIdentifiers)}));
        }
      }
    }

    checkForExtraDataAtEndOfLine(st, expected_syntax);

    _validAttributeLineAcceptIllegalIdentifiers = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .description: <string>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineDescription(String line)
  {
    Assert.expect(line != null);

    // description
    String description = line;

    int commentLineSymbolIndex = description.indexOf(NdfReader.getCommentChar());
    int semiColonIndex = description.indexOf(';');
    int colonIndex = description.indexOf(':');

    int index = -1;
    // choose the smallest of the 3 indicies greater than -1, if there is one
    if (commentLineSymbolIndex >= 0)
    {
      if (semiColonIndex >= 0)
      {
        index = Math.min(commentLineSymbolIndex, semiColonIndex);
        if (colonIndex >= 0)
          index = Math.min(index, colonIndex);
      }
      else if (colonIndex >= 0)
        index = Math.min(commentLineSymbolIndex, colonIndex);
      else
        index = commentLineSymbolIndex;
    }
    else if (semiColonIndex >= 0)
    {
      if (colonIndex >= 0)
        index = Math.min(semiColonIndex, colonIndex);
      else
        index = semiColonIndex;
    }
    else if (colonIndex >= 0)
      index = colonIndex;

    // if there is a comment, remove it
    if (index >= 0)
      description = description.substring(0, index);

    // remove all leading and trailing white space
    description = description.trim();

    _project.setNotes(description);
    _validAttributeLineDescription = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .assembly_name: <string>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineAssemblyName(String line)
  {
    Assert.expect(line != null);

    // assembly name
    String assemblyName = line;

    int commentLineSymbolIndex = assemblyName.indexOf(NdfReader.getCommentChar());
    int semiColonIndex = assemblyName.indexOf(';');
    int colonIndex = assemblyName.indexOf(':');

    int index = -1;
    // choose the smallest of the 3 indicies greater than -1, if there is one
    if (commentLineSymbolIndex >= 0)
    {
      if (semiColonIndex >= 0)
      {
        index = Math.min(commentLineSymbolIndex, semiColonIndex);
        if (colonIndex >= 0)
          index = Math.min(index, colonIndex);
      }
      else if (colonIndex >= 0)
        index = Math.min(commentLineSymbolIndex, colonIndex);
      else
        index = commentLineSymbolIndex;
    }
    else if (semiColonIndex >= 0)
    {
      if (colonIndex >= 0)
        index = Math.min(semiColonIndex, colonIndex);
      else
        index = semiColonIndex;
    }
    else if (colonIndex >= 0)
      index = colonIndex;

    // if there is a comment, remove it
    if (index >= 0)
      assemblyName = assemblyName.substring(0, index);

    // remove all leading and trailing white space
    assemblyName = assemblyName.trim();

    _validAttributeLineAssemblyName = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .assembly_number: <string>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineAssemblyNumber(String line)
  {
    Assert.expect(line != null);

    // assembly number
    String assemblyNumber = line;

    int commentLineSymbolIndex = assemblyNumber.indexOf(NdfReader.getCommentChar());
    int semiColonIndex = assemblyNumber.indexOf(';');
    int colonIndex = assemblyNumber.indexOf(':');

    int index = -1;
    // choose the smallest of the 3 indicies greater than -1, if there is one
    if (commentLineSymbolIndex >= 0)
    {
      if (semiColonIndex >= 0)
      {
        index = Math.min(commentLineSymbolIndex, semiColonIndex);
        if (colonIndex >= 0)
          index = Math.min(index, colonIndex);
      }
      else if (colonIndex >= 0)
        index = Math.min(commentLineSymbolIndex, colonIndex);
      else
        index = commentLineSymbolIndex;
    }
    else if (semiColonIndex >= 0)
    {
      if (colonIndex >= 0)
        index = Math.min(semiColonIndex, colonIndex);
      else
        index = semiColonIndex;
    }
    else if (colonIndex >= 0)
      index = colonIndex;

    // if there is a comment, remove it
    if (index >= 0)
      assemblyNumber = assemblyNumber.substring(0, index);

    // remove all leading and trailing white space
    assemblyNumber = assemblyNumber.trim();

    _validAttributeLineAssemblyNumber = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .awaretest_on: <boolean>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineAwareTestOn(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // aware test on
    boolean awareTestOn;
    String expected_syntax = _AWARETEST_ON + " " + EXPECTED_BOOLEAN;

    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new MissingTokenBooleanDatastoreException(_currentFile, _is.getLineNumber(), _AWARETEST_ON);
      }
      else
      {
        _warnings.add(new LocalizedString("DS_NDF_WARNING_TOKEN_MISSING_TRUE_OR_FALSE_USE_DEFAULT_KEY",
                      new Object[] { _currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _AWARETEST_ON,
                                     new Boolean(_ASSUMED_AWARE_TEST_ON) }));

        awareTestOn = _ASSUMED_AWARE_TEST_ON;
      }
    }
    else
    {
      try
      {
        awareTestOn = StringUtil.convertStringToBoolean(token);
      }
      catch(BadFormatException e)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), EXPECTED_BOOLEAN, token);
          dex.initCause(e);
          throw dex;
        }
        else
        {
          String tempToken = token.toUpperCase();
          if (tempToken.startsWith(_LEGACY_FALSE))
            awareTestOn = false;
          else
            awareTestOn = true;
          _warnings.add(new LocalizedString("DS_NDF_WARNING_TRUE_OR_FALSE_CORRUPT_KEY",
                        new Object[] { _currentFile,
                                       new Integer(_is.getLineNumber()),
                                       expected_syntax,
                                       token,
                                       new Boolean(awareTestOn)}));
        }
      }
    }

    checkForExtraDataAtEndOfLine(st, expected_syntax);

    _validAttributeLineAwareTestOn = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .software_revision: <revision>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineSoftwareRevision(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // software revision
    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _ATTRIBUTE_LINE_SOFTWARE_REVISION_EXPECTED_SYNTAX,
                                                _VERSION_NUMBER_FIELD);
    }
    double softwareRevision = 0;
    // A valid software revision is either a number (positive double) or "XX"
    // "XX" is typically used by developers here at Agilent.  Any software revision that is XX
    // should be interpreted as the latest software version, as specified by Version.java
    // Added on 1/22/2002 by APM
    if (token.equalsIgnoreCase("xx"))
    {
      token = "1000"; // use release 100.0 on genesis
    }
    Exception ex = null;
    try
    {
      softwareRevision = StringUtil.convertStringToPositiveDouble(token);
    }
    catch(BadFormatException bfe)
    {
      ex = bfe;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      ExpectedPositiveNumberDatastoreException dex = new ExpectedPositiveNumberDatastoreException(_currentFile,
                                                                                                   _is.getLineNumber(),
                                                                                                   _ATTRIBUTE_LINE_SOFTWARE_REVISION_EXPECTED_SYNTAX,
                                                                                                   _VERSION_NUMBER_FIELD,
                                                                                                   token);
      dex.initCause(ex);
      throw dex;
    }
    _mainReader.setSoftwareRevision(softwareRevision);
    _project.setSoftwareVersionOfProjectLastSave(Version.getVersionNumber());

    // this uses _mainReader.ndfsAreWellFormed(), this check has to be done after setting the software version
    checkForExtraDataAtEndOfLine(st, _ATTRIBUTE_LINE_SOFTWARE_REVISION_EXPECTED_SYNTAX);

    _validAttributeLineSoftwareRevision = true;
  }

  /**
   * parses an attribute line in a panel.ndf file that follows the following format
   * .fiducial <#> <side> <x location> <y location> <shape> <z size> <y size> <rotation>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineFiducial(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    String fiducialName = null;
    String boardSideStr = null;
    String xLocationStr = null;
    String yLocationStr = null;
    String shapeStr = null;
    String xSizeStr = null;
    String ySizeStr = null;
    String rotationStr = null;
    int fiducialNumber = 0;
    boolean topSide = false;
    int fiducialXCoordinateInNanoMeters = 0;
    int fiducialYCoordinateInNanoMeters = 0;
    int fiducialWidthInNanoMeters = 0;
    int fiducialLengthInNanoMeters = 0;
    int fiducialDegreesRotation = 0;

    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    // fiducial number
    fiducialName = ParseUtil.getNextToken(st);
    if (fiducialName == null)
    {
      //no need to check for _mainReader.ndfsAreWellFormed()
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                                _FIDUCIAL_NUMBER_FIELD);
    }

    fiducialNumber = parseInt(fiducialName, _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX, _FIDUCIAL_NUMBER_FIELD, _FIDUCIAL_ONE, _FIDUCIAL_THREE);

    boolean duplicateWarning = false;
    switch(fiducialNumber)
    {
      case _FIDUCIAL_ONE:
        if (_validFiducialOne)
        {
          if (_mainReader.ndfsAreWellFormed())
            throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _FIDUCIAL + " " + fiducialNumber);
          else
          {
            duplicateWarning = true;
            _warnings.add(new LocalizedString("DS_NDF_WARNING_DUPLICATE_TOKEN_KEY",
                                          new Object[]
                                          {_currentFile,
                                          new Integer(_is.getLineNumber()),
                                          _FIDUCIAL + " " + fiducialNumber}));
          }
        }
        _validFiducialOne = true;
        break;
      case _FIDUCIAL_TWO:
        if (_validFiducialTwo)
        {
          if (_mainReader.ndfsAreWellFormed())
            throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _FIDUCIAL + " " + fiducialNumber);
          else
          {
            duplicateWarning = true;
            _warnings.add(new LocalizedString("DS_NDF_WARNING_DUPLICATE_TOKEN_KEY",
                                          new Object[]
                                          {_currentFile,
                                          new Integer(_is.getLineNumber()),
                                          _FIDUCIAL + " " + fiducialNumber}));
          }
        }
        _validFiducialTwo = true;
        break;
      case _FIDUCIAL_THREE:
        if (_validFiducialThree)
        {
          if (_mainReader.ndfsAreWellFormed())
            throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _FIDUCIAL + " " + fiducialNumber);
          else
          {
            duplicateWarning = true;
            _warnings.add(new LocalizedString("DS_NDF_WARNING_DUPLICATE_TOKEN_KEY",
                                          new Object[]
                                          {_currentFile,
                                          new Integer(_is.getLineNumber()),
                                          _FIDUCIAL + " " + fiducialNumber}));
          }
        }
        _validFiducialThree = true;
        break;
      default:
        throw new ValueOutOfRangeDatastoreException(_currentFile,
                                                    _is.getLineNumber(),
                                                    _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                                    _FIDUCIAL_NUMBER_FIELD,
                                                    fiducialNumber,
                                                    _FIDUCIAL_ONE,
                                                    _FIDUCIAL_THREE);

    }

    // board side
    boardSideStr = ParseUtil.getNextToken(st);
    if (boardSideStr == null)
    {
      //no need to check for _mainReader.ndfsAreWellFormed()
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                                _FIDUCIAL_SIDE_FIELD);
    }

    if (boardSideStr.equalsIgnoreCase(_TOP_SIDE_BOARD))
    {
      topSide = true;
    }
    else if ( boardSideStr.equalsIgnoreCase(_BOTTOM_SIDE_BOARD) ||
             boardSideStr.equalsIgnoreCase(_BOT_SIDE_BOARD) ||
             boardSideStr.equalsIgnoreCase(_BTM_SIDE_BOARD) )
    {
      topSide = false;
    }
    else
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new InvalidBoardSideDatastoreException(_currentFile,
                                                     _is.getLineNumber(),
                                                     _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                                     _FIDUCIAL_SIDE_FIELD,
                                                     boardSideStr);
      }
      else
      {
        String tempToken = boardSideStr.toUpperCase();
        if (tempToken.startsWith(_LEGACY_TOP_SIDE_BOARD))
        {
          topSide = true;
        }
        else if (tempToken.startsWith(_LEGACY_BOTTOM_SIDE_BOARD))
        {
          topSide = false;
        }
        else
        {
          throw new InvalidBoardSideDatastoreException(_currentFile,
                                                     _is.getLineNumber(),
                                                     _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                                     _FIDUCIAL_SIDE_FIELD,
                                                     boardSideStr);
        }

        String defaultSide;
        if (topSide)
          defaultSide = "TOP";
        else
          defaultSide = "BOTTOM";
        _warnings.add(new LocalizedString("DS_NDF_WARNING_INVALID_BOARD_SIDE_KEY",
                                          new Object[]
                                          {_currentFile,
                                          new Integer(_is.getLineNumber()),
                                          _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                          _FIDUCIAL_SIDE_FIELD,
                                          boardSideStr,
                                          defaultSide}));

      }
    }

    // x location
    xLocationStr = ParseUtil.getNextToken(st);
    if (xLocationStr == null)
    {
      //no need to check for _mainReader.ndfsAreWellFormed()
      String expected = EXPECTED_POSITIVE_NUMBER;
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                                _FIDUCIAL_X_LOCATION_FIELD);
    }
    double fiducialXLocation = parseDouble(xLocationStr, _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX, _FIDUCIAL_X_LOCATION_FIELD, _parsedUnits);
    fiducialXLocation = MathUtil.convertUnits(fiducialXLocation, _parsedUnits, _DATASTORE_UNITS);

    // the double is in nanometers -- convert to an integer
    try
    {
      fiducialXCoordinateInNanoMeters = MathUtil.convertDoubleToInt(fiducialXLocation);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                                                                    _FIDUCIAL_X_LOCATION_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }

    // y location
    yLocationStr = ParseUtil.getNextToken(st);
    if (yLocationStr == null)
    {
      //no need to check for _mainReader.ndfsAreWellFormed()
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                                _FIDUCIAL_Y_LOCATION_FIELD);
    }

    double fiducialYLocation = parseDouble(yLocationStr, _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX, _FIDUCIAL_Y_LOCATION_FIELD, _parsedUnits);
    fiducialYLocation = MathUtil.convertUnits(fiducialYLocation, _parsedUnits, _DATASTORE_UNITS);

    // the double is in nanometers -- convert to an integer
    try
    {
      fiducialYCoordinateInNanoMeters = MathUtil.convertDoubleToInt(fiducialYLocation);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                                                                    _FIDUCIAL_Y_LOCATION_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }

    // shape
    shapeStr = ParseUtil.getNextToken(st);
    if (shapeStr == null)
    {
      // no need to check for _mainReader.ndfsAreWellFormed()
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                                _FIDUCIAL_SHAPE_FIELD);
    }

    // x size
    xSizeStr = ParseUtil.getNextToken(st);
    if (xSizeStr == null)
    {
      // fiducial width must be valid.
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                                _FIDUCIAL_X_SIZE_FIELD);
    }

    double fiducialXSize = parsePositiveNonZeroDouble(xSizeStr, _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX, _FIDUCIAL_X_SIZE_FIELD, _parsedUnits);
    fiducialXSize = MathUtil.convertUnits(fiducialXSize, _parsedUnits, _DATASTORE_UNITS);
    // the double is in nanometers -- convert to an integer
    try
    {
      fiducialWidthInNanoMeters = MathUtil.convertDoubleToInt(fiducialXSize);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                                                                    _FIDUCIAL_X_SIZE_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }

    // y size
    ySizeStr = ParseUtil.getNextToken(st);
    if (ySizeStr == null)
    {
      // fiducial width must be valid.
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                                _FIDUCIAL_Y_SIZE_FIELD);
    }

    double fiducialYSize = parsePositiveNonZeroDouble(ySizeStr, _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX, _FIDUCIAL_Y_SIZE_FIELD, _parsedUnits);
    fiducialYSize = MathUtil.convertUnits(fiducialYSize, _parsedUnits, _DATASTORE_UNITS);
    // the double is in nanometers -- convert to an integer
    try
    {
      fiducialLengthInNanoMeters = MathUtil.convertDoubleToInt(fiducialYSize);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                                                                    _FIDUCIAL_Y_SIZE_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }

    // rotation
    rotationStr = ParseUtil.getNextToken(st);
    if (rotationStr == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                                                  _FIDUCIAL_ROTATION_FIELD);
      }
      else
      {
        fiducialDegreesRotation = 0;
        // if not specified at all, Zero degrees will be used
        Object[] args = new Object[]
                {_currentFile,
                new Integer(_is.getLineNumber()),
                _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX,
                _FIDUCIAL_ROTATION_FIELD,
                rotationStr};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_KEY", args));

      }
    }
    else
      fiducialDegreesRotation = parseInt(rotationStr, _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX, _FIDUCIAL_ROTATION_FIELD, _MIN_FIDUCIAL_DEGREES_ROTATION, _MAX_FIDUCIAL_DEGREES_ROTATION);

    checkForExtraDataAtEndOfLine(st, _ATTRIBUTE_LINE_FIDUCIAL_EXPECTED_SYNTAX);

    if (duplicateWarning == false)
    {
      Fiducial fiducial = new Fiducial();
      FiducialType fiducialType = new FiducialType();

      fiducialType.setWidthInNanoMeters(fiducialWidthInNanoMeters);
      fiducialType.setLengthInNanoMeters(fiducialLengthInNanoMeters);
      fiducialType.setCoordinateInNanoMeters(new PanelCoordinate(fiducialXCoordinateInNanoMeters,
                                             fiducialYCoordinateInNanoMeters));
      fiducialType.setShapeEnum(ShapeEnum.CIRCLE);
      fiducial.setFiducialType(fiducialType);
      fiducial.setPanel(_panel, topSide);
      fiducialType.setName(fiducialName);
    }

    _validAttributeLineFiducial = true;
  }

  /**
   * @author Keith Lee
   */
  protected void parseLinesRequiredByOtherLines() throws DatastoreException
  {
    String line = ParseUtil.readNextLine(_is, _currentFile);
    String tempLine = null;

    // read in first .SOFTWARE_REVISION: attribute line
    tempLine = null;
    boolean softwareRevisionAttributeLineFound = false;
    while ((softwareRevisionAttributeLineFound == false) && (line != null))
    {
      tempLine = line.toUpperCase();
      if (tempLine.length() == 0)
      {
        // skip: this is a blank line
      }
      else if (tempLine.startsWith(_SOFTWARE_REVISION))
      {
        // .SOFTWARE_REVISION:
        line = line.substring(_SOFTWARE_REVISION.length());
        parseAttributeLineSoftwareRevision(line);
        softwareRevisionAttributeLineFound = true;
      }
      else
      {
        // skip: this will be read in later
      }
      line = ParseUtil.readNextLine(_is, _currentFile);
    }

    //if .SOFTWARE_REVISION: is not specified in the panel.ndf
    //(this means we are running pre 8.0 software.)
    if (_validAttributeLineSoftwareRevision == false)
    {
      _project.setSoftwareVersionOfProjectLastSave(Version.getVersionNumber());
      //_mainReader automatically assumes 0.0 unless explicitly specified otherwise by panel.ndf
      //_mainReader.setSoftwareRevision(_DEFAULT_SOFTWARE_REVISION_CREATION);
    }

    // reset file to beginning and re-mark for next attribute line
    try
    {
      _is.reset();
      _is.mark((int)_file.length() + 1);
    }
    catch(IOException e)
    {
      DatastoreException dex = new DatastoreException(_currentFile);
      dex.initCause(e);
      throw dex;
    }

    // read in first .UNIT: attribute line
    line = ParseUtil.readNextLine(_is, _currentFile);
    boolean unitAttributeLineFound = false;
    while ((unitAttributeLineFound == false) && (line != null))
    {
      tempLine = line.toUpperCase();
      if (tempLine.length() == 0)
      {
        // skip: this is a blank line
      }
      else if (tempLine.startsWith(NdfReader.getUnitToken()))
      {
        // .UNIT:
        line = line.substring(NdfReader.getUnitToken().length());
        parseAttributeLineUnit(line);
        unitAttributeLineFound = true;
      }
      else
      {
        // skip: this will be read in later during parseFile()
      }
      line = ParseUtil.readNextLine(_is, _currentFile);
    }

    // if no .UNIT: attribute line, set _parsedUnits to default value (MILS)
    if (_unitLineFound == false)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new MissingTokenDatastoreException(_currentFile, NdfReader.getUnitToken());
      }
      else
      {
        //give a warning that .UNIT arrtibute is missing and that mils are used as default
        _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_UNIT_ASSUMING_MILS_KEY",
        new Object[]{_currentFile}));
        _parsedUnits = _DEFAULT_NDF_UNITS;
      }
    }

    _project.setDisplayUnits(_parsedUnits);
  }

  /**
   * @return the SideBoardType object for the given name if it already exists.  If it does not
   * exist already, create the object and return it.
   *
   * @author Bill Darbie
   */
  private SideBoardType getSideBoardType(String sideBoardTypeName)
  {
    Assert.expect(sideBoardTypeName != null);

    String sideBoardTypeNameUpper = sideBoardTypeName.toUpperCase();
    SideBoardType sideBoardType = (SideBoardType)_sideBoardTypeNameToSideBoardTypeMap.get(sideBoardTypeNameUpper);
    if (sideBoardType == null)
    {
      sideBoardType = new SideBoardType();
      sideBoardType.setSideBoardTypeSettings(new SideBoardTypeSettings());

      _sideBoardTypeNameToSideBoardTypeMap.put(sideBoardTypeNameUpper, sideBoardType);
      _mainReader.addSideBoardTypeToLegacyNameMap(sideBoardType, sideBoardTypeName);
    }

    Assert.expect(sideBoardType != null);
    return sideBoardType;
  }

  /**
   * @return the BoardType object for the given primary and secondary names if it already exists.  If it does not
   * exist already, create the object and return it.
   *
   * @author Bill Darbie
   */
  private BoardType getBoardType(SideBoard primarySideBoard,
                                 SideBoard secondarySideBoard,
                                 SideBoardType primarySideBoardType,
                                 SideBoardType secondarySideBoardType)
  {
    Assert.expect(primarySideBoard != null);
    // secondarySideBoard can be null if the board is single sided
    //Assert.expect(secondarySideBoard != null);
    Assert.expect(primarySideBoardType != null);
    // secondarySideBoardType can be null if the board is single sided
    //Assert.expect(secondarySideBoardType != null);

    BoardType boardType = null;
    BoardType primaryBoardType = (BoardType)_sideBoardTypeToBoardTypeMap.get(primarySideBoardType);
    BoardType secondaryBoardType = null;
    if (secondarySideBoardType != null)
      secondaryBoardType = (BoardType)_sideBoardTypeToBoardTypeMap.get(secondarySideBoardType);

    if ((primaryBoardType == null) && (secondaryBoardType == null))
    {
      // no previous BoardType exists, so create it, and assign it to both SideBoards
      boardType = new BoardType();
      boardType.setPanel(_panel);
      ++_boardTypeId;
      boardType.setName("boardType" + new Integer(_boardTypeId));

      _sideBoardTypeToBoardTypeMap.put(primarySideBoardType, boardType);
      if (secondarySideBoardType != null)
        _sideBoardTypeToBoardTypeMap.put(secondarySideBoardType, boardType);

      if (_isPrimaryOnTop)
      {
        boardType.setSideBoardType1(primarySideBoardType);
        if (secondarySideBoardType != null)
          boardType.setSideBoardType2(secondarySideBoardType);
      }
      else
      {
        if (secondarySideBoardType != null)
          boardType.setSideBoardType1(secondarySideBoardType);
        boardType.setSideBoardType2(primarySideBoardType);
      }
    }
    else if ((primaryBoardType == null) && (secondaryBoardType != null))
    {
      // The primarySideBoardType did not have a BoardType associated with it
      // but the secondaryBoardType did.
      // This means that the secondarySideBoard is being used by more than one Board
      // We need to make another SideBoardType for the secondaryBoard since two
      // SideBoardTypes cannot be shared by more than one BoardType
      boardType = new BoardType();
      boardType.setPanel(_panel);
      ++_boardTypeId;
      boardType.setName("boardType" + new Integer(_boardTypeId));
      SideBoardType newSecondarySideBoardType = new SideBoardType();

      newSecondarySideBoardType.setSideBoardTypeSettings(new SideBoardTypeSettings());
      secondarySideBoard.setSideBoardType(newSecondarySideBoardType);
      _mainReader.addSideBoardTypeToLegacyNameMap(newSecondarySideBoardType, _secondarySideBoardTypeName);
      _sideBoardTypeToBoardTypeMap.put(primarySideBoardType, boardType);
      _sideBoardTypeToBoardTypeMap.put(newSecondarySideBoardType, boardType);

      if (_isPrimaryOnTop)
      {
        boardType.setSideBoardType1(primarySideBoardType);
        boardType.setSideBoardType2(newSecondarySideBoardType);
      }
      else
      {
        boardType.setSideBoardType1(newSecondarySideBoardType);
        boardType.setSideBoardType2(primarySideBoardType);
      }
    }
    else if ((primaryBoardType != null) && (secondaryBoardType == null))
    {
      // the secondarySideBoardType did not have a BoardType associated with it
      // but the primaryBoardType did.
      // This means that the primarySideBoard is being used by more than one Board

      if ((primarySideBoardType == null) ||
          (secondarySideBoardType != null) ||
          (primaryBoardType.sideBoardType1Exists() && primaryBoardType.sideBoardType2Exists()))
      {
        // the current board only has one side, but the previous one had two sides.

        // We need to make another SideBoardType for the primaryBoard since two
        // SideBoardTypes cannot be shared by more than one BoardType
        boardType = new BoardType();
        boardType.setPanel(_panel);
        ++_boardTypeId;
        boardType.setName("boardType" + new Integer(_boardTypeId));
        SideBoardType newPrimarySideBoardType = new SideBoardType();
        newPrimarySideBoardType.setSideBoardTypeSettings(new SideBoardTypeSettings());
        primarySideBoard.setSideBoardType(newPrimarySideBoardType);
        _mainReader.addSideBoardTypeToLegacyNameMap(newPrimarySideBoardType, _primarySideBoardTypeName);
        _sideBoardTypeToBoardTypeMap.put(newPrimarySideBoardType, boardType);

        if (_isPrimaryOnTop)
        {
          boardType.setSideBoardType1(newPrimarySideBoardType);
          if (secondarySideBoardType != null)
            boardType.setSideBoardType2(secondarySideBoardType);
        }
        else
        {
          if (secondarySideBoardType != null)
            boardType.setSideBoardType1(secondarySideBoardType);
          boardType.setSideBoardType2(newPrimarySideBoardType);
        }
      }
      else
      {
        // This means that we have two Board instances sharing a BoardType
        // return the BoardType already created previously
        boardType = primaryBoardType;
      }
    }
    else if ((primaryBoardType != null) && (secondaryBoardType != null))
    {
      if (primaryBoardType != secondaryBoardType)
      {
        // The primaryBoardType and secondaryBoardType both exist but do
        // not match.
        // This means that both the primarySideBoard and the secondarySideBoard
        // are being used by more than one Board
        // We need to make another SideBoardType for the primaryBoard and another SideBoardType
        // for the secondaryBoard since two
        // SideBoardTypes cannot be shared by more than one BoardType
        boardType = new BoardType();
        boardType.setPanel(_panel);
        ++_boardTypeId;
        boardType.setName("boardType" + new Integer(_boardTypeId));
        SideBoardType newPrimarySideBoardType = new SideBoardType();
        newPrimarySideBoardType.setSideBoardTypeSettings(new SideBoardTypeSettings());
        primarySideBoard.setSideBoardType(newPrimarySideBoardType);
        SideBoardType newSecondarySideBoardType = new SideBoardType();
        newSecondarySideBoardType.setSideBoardTypeSettings(new SideBoardTypeSettings());
        secondarySideBoard.setSideBoardType(newSecondarySideBoardType);
        _sideBoardTypeToBoardTypeMap.put(newPrimarySideBoardType, boardType);
        _sideBoardTypeToBoardTypeMap.put(newSecondarySideBoardType, boardType);
        _mainReader.addSideBoardTypeToLegacyNameMap(newPrimarySideBoardType, _primarySideBoardTypeName);
        _mainReader.addSideBoardTypeToLegacyNameMap(newSecondarySideBoardType, _secondarySideBoardTypeName);

        if (_isPrimaryOnTop)
        {
          boardType.setSideBoardType1(newPrimarySideBoardType);
          boardType.setSideBoardType2(newSecondarySideBoardType);
        }
        else
        {
          boardType.setSideBoardType1(newSecondarySideBoardType);
          boardType.setSideBoardType2(newPrimarySideBoardType);
        }
      }
      else
      {
        // The primaryBoardType and secondaryBoardType both exist and are the same instance
        // This means that we have two Board instances sharing a BoardType
        // return the BoardType already created previously
        boardType = primaryBoardType;
      }
    }

    Assert.expect(boardType != null);
    return boardType;
  }


  /**
   * @author Bill Darbie
   */
  private void addBoardToLegacyPrimarySideBoardOnTopMap(Board board, boolean legacyPrimarySideBoardOnTop)
  {
    Assert.expect(board != null);

    Object previous = _boardToLegacyIsPrimarySideBoardOnTopMap.put(board, new Boolean(legacyPrimarySideBoardOnTop));
    Assert.expect(previous == null);
  }

  /**
   * The first SideBoard listed in the panel is considered the primary side board (independent of whether
   * it is top or bottom side)
   * @author Bill Darbie
   */
  private boolean isLegacyPrimarySideBoardOnTop(Board board)
  {
    Assert.expect(board != null);

    Boolean primaryOnTop = (Boolean)_boardToLegacyIsPrimarySideBoardOnTopMap.get(board);
    Assert.expect(primaryOnTop != null);

    return primaryOnTop.booleanValue();
  }

  /**
   * @author Bill Darbie
   */
  private void addSideBoardToLegacyCoordinateInMilsMap(SideBoard sideBoard, DoubleCoordinate legacyCoordinateInMils)
  {
    Assert.expect(sideBoard != null);
    Assert.expect(legacyCoordinateInMils != null);

    Object previous = _sideBoardToLegacyCoordinateInMilsMap.put(sideBoard, legacyCoordinateInMils);
    Assert.expect(previous == null);
  }

  /**
   * @author Bill Darbie
   */
  private DoubleCoordinate getLegacyCoordinateInMils(SideBoard sideBoard)
  {
    Assert.expect(sideBoard != null);

    DoubleCoordinate legacyCoordinateInMils = (DoubleCoordinate)_sideBoardToLegacyCoordinateInMilsMap.get(sideBoard);
    Assert.expect(legacyCoordinateInMils != null);

    return legacyCoordinateInMils;
  }

}
