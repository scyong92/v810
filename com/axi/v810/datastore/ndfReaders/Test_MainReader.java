package com.axi.v810.datastore.ndfReaders;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
class Test_MainReader extends UnitTest
{
  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_MainReader());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testPackageCases();
    testLandPatternCases();
    testComponentCases();
    testBoardNdfCases();
    testTopAndBottomBoardRotation();

    testMethods();
  }

  /**
   * @author Andy Mechtenberg
   */
  private void testPackageCases()
  {
    String panelName = "smallPanel";
    String targetNdf = "package.ndf";
    String dataDir = getTestDataDir();
    String testNdfsDataDir = dataDir + File.separator + "mainReaderNdfs";

    // to test the package stuff, I need to FALSE out component Q1
    // backup the componen.ndf file -- it should be the good one, so we can restore it at the end.
    String componenNdfFullPath = Directory.getLegacyNdfPanelDir(panelName) + File.separator + "pads_asc_a2" + File.separator + "componen.ndf";;
    String testComponenNdf = testNdfsDataDir + File.separator + "component.q1.false.ndf";
    try
    {
      FileUtilAxi.backupFile(componenNdfFullPath );
      // copy over the new one
      FileUtil.copy(testComponenNdf, componenNdfFullPath);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    String[] cornerCaseFiles = new String[]
                               {
                               "package.extra.pin.ndf",
                               "package.smallpitch.ndf"
                               };
    Test_ReaderUtil.testParser(panelName, targetNdf, testNdfsDataDir, cornerCaseFiles);

    // now restore the componen.ndf from backup
    try
    {
      FileUtilAxi.restoreBackupFile(componenNdfFullPath);
    }
    catch(DatastoreException ex)
    {
      ex.printStackTrace();
    }

  }

  /**
   * @author Andy Mechtenberg
   */
  private void testLandPatternCases()
  {
    String dataDir = getTestDataDir();
    String panelName = "smallPanel";
    String targetNdf = "landpat.ndf";
    String testNdfsDataDir = dataDir + File.separator + "mainReaderNdfs";
    String[] cornerCaseFiles = new String[]
                               {
                               "landpat.extra.pad.ndf",
                               "landpat.samename.ndf",
                               "landpat.newname.ndf"
                               };
    Test_ReaderUtil.testParser(panelName, targetNdf, testNdfsDataDir, cornerCaseFiles);

    // now we need to update the padgeom.ndf file with a pattern the same name as one in the landpat file.
    // so, we'll backup padgeom, copy a new one and run a test case.
    // to test the package stuff, I need to FALSE out component Q1
    // backup the componen.ndf file -- it should be the good one, so we can restore it at the end.
    String padgeomNdfFullPath = Directory.getLegacyNdfPanelDir(panelName) + File.separator + "padgeom.ndf";
    String testPadgeomNdf = testNdfsDataDir + File.separator + "padgeom.newname.ndf"; ;
    try
    {
      FileUtilAxi.backupFile(padgeomNdfFullPath);
      // copy over the new one
      FileUtil.copy(testPadgeomNdf, padgeomNdfFullPath);
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }

    cornerCaseFiles = new String[]
                      {
                      "landpat.newname.ndf"
                      };
    Test_ReaderUtil.testParser(panelName, targetNdf, testNdfsDataDir, cornerCaseFiles);

    // now restore the componen.ndf from backup
    try
    {
      FileUtilAxi.restoreBackupFile(padgeomNdfFullPath);
    }
    catch(DatastoreException ex)
    {
      ex.printStackTrace();
    }

  }

  /**
   * @author Andy Mechtenberg
   */
  private void testComponentCases()
  {
    String dataDir = getTestDataDir();
    String panelName = "smallPanel";
    String targetNdf = "pads_asc_a2" + File.separator + "componen.ndf";
    String testNdfsDataDir = dataDir + File.separator + "mainReaderNdfs";
    String[] cornerCaseFiles = new String[]
                               {
                               "component.invalidsubtype.ndf",
                               };

    // backup the subtypes ndf file -- we want to delete it
    String ndfRoot = dataDir + File.separator + "ndf";
    String panelDir = Directory.getLegacyNdfPanelDir(panelName);
    String subtypesFile = panelDir + File.separator + "subtypes.ndf";
    try
    {
      FileUtilAxi.backupFile(subtypesFile);
      FileUtilAxi.delete(subtypesFile);
    }
    catch(DatastoreException dex)
    {
      Assert.expect(false);
    }

    Test_ReaderUtil.testParser(panelName, targetNdf, testNdfsDataDir, cornerCaseFiles);
    // restore the subtypes ndf file
    try
    {
      FileUtilAxi.restoreBackupFile(subtypesFile);
    }
    catch(DatastoreException ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  private void testBoardNdfCases()
  {
    String dataDir = getTestDataDir();
    String panelName = "smallPanel";
    String targetNdf = "board_a1" + File.separator + "board.ndf";
    String testNdfsDataDir = dataDir + File.separator + "mainReaderNdfs";
    String[] cornerCaseFiles = new String[]
                               {
                               "board.differentthickness.ndf",
                               "board.differentwidth.ndf",
                               "board.differentlength.ndf",
                               };
    Test_ReaderUtil.testParser(panelName, targetNdf, testNdfsDataDir, cornerCaseFiles);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void testMethods()
  {
    MainReader mainReader = new MainReader();

    // addComponent/getComponent/doesComponentExist
    Expect.expect(mainReader.getComponents().size() == 0);
    try
    {
      mainReader.addComponent(null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    try
    {
      mainReader.getComponent(null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    try
    {
      mainReader.doesComponentExist(null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    String referenceDesignator = "U1";
    Component component = new Component();
    ComponentType componentType = new ComponentType();
    Panel panel = new Panel();
    panel.disableChecksForNdfParsers();
    componentType.setPanel(panel);
    component.setComponentType(componentType);

    componentType.setReferenceDesignator(referenceDesignator);
    Expect.expect(mainReader.doesComponentExist(referenceDesignator) == false);
    try
    {
      mainReader.getComponent(referenceDesignator);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    mainReader.addComponent(component);
    Expect.expect(mainReader.doesComponentExist(referenceDesignator) == true);
    Expect.expect(mainReader.getComponent(referenceDesignator) == component);
    Expect.expect(mainReader.getComponents().size() == 1);

    // addThroughHoleLandPattern/getTopSideThroughHoleLandPattern/doesTopSideThroughHoleLandPatternExist
    Expect.expect(mainReader.getThroughHoleLandPatterns().size() == 0);
    try
    {
      mainReader.addThroughHoleLandPattern(null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    try
    {
      LandPattern lp = new LandPattern();
      mainReader.addThroughHoleLandPattern(lp);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }

    try
    {
      mainReader.getThroughHoleLandPattern(null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    try
    {
      mainReader.doesThroughHoleLandPatternExist(null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    String landPatternName = "LPName";
    LandPattern landPattern = new LandPattern();
    panel.disableChecksForNdfParsers();
    landPattern.setPanel(panel);
    landPattern.setName(landPatternName);
    Expect.expect(mainReader.doesThroughHoleLandPatternExist(landPatternName) == false);
    try
    {
      mainReader.getThroughHoleLandPattern(landPatternName);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    mainReader.addThroughHoleLandPattern(landPattern);
    Expect.expect(mainReader.doesThroughHoleLandPatternExist(landPatternName) == true);
    Expect.expect(mainReader.getThroughHoleLandPattern(landPatternName) == landPattern);
    Expect.expect(mainReader.getThroughHoleLandPatterns().size() == 1);
    Expect.expect(mainReader.getThroughHoleLandPatterns().contains(landPattern));

    // addSurfaceMountLandPattern/getSurfaceMountLandPattern/doesSurfaceMountLandPatternExist
    Expect.expect(mainReader.getSurfaceMountLandPatterns().size() == 0);
    try
    {
      mainReader.addSurfaceMountLandPattern(null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    try
    {
      LandPattern lp = new LandPattern();
      mainReader.addSurfaceMountLandPattern(lp);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }

    try
    {
      mainReader.getSurfaceMountLandPattern(null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    try
    {
      mainReader.doesSurfaceMountLandPatternExist(null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    String surfaceMountLandPatternName = "SurfaceMountLPName";
    LandPattern surfaceMountLandPattern = new LandPattern();
    surfaceMountLandPattern.setPanel(panel);
    surfaceMountLandPattern.setName(surfaceMountLandPatternName);
    Expect.expect(mainReader.doesSurfaceMountLandPatternExist(surfaceMountLandPatternName) == false);
    try
    {
      mainReader.getSurfaceMountLandPattern(surfaceMountLandPatternName);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    mainReader.addSurfaceMountLandPattern(surfaceMountLandPattern);
    Expect.expect(mainReader.doesSurfaceMountLandPatternExist(surfaceMountLandPatternName) == true);
    Expect.expect(mainReader.getSurfaceMountLandPattern(surfaceMountLandPatternName) == surfaceMountLandPattern);
    Expect.expect(mainReader.getSurfaceMountLandPatterns().size() == 1);
    Expect.expect(mainReader.getSurfaceMountLandPatterns().contains(surfaceMountLandPattern));

// wpd virgo
//    // addCompPackage/getCompPackage/doesCompPackageExist
//    Expect.expect(mainReader.getCompPackages().size() == 0);
//    try
//    {
//      mainReader.addCompPackage(null);
//      Expect.expect(false);
//    }
//    catch (AssertException ex)
//    {
//      // do nothing
//    }
//    try
//    {
//      CompPackage cp = new CompPackage();
//      mainReader.addCompPackage(cp);
//      Expect.expect(false);
//    }
//    catch (AssertException ex)
//    {
//      // do nothing
//    }
//
//    try
//    {
//      mainReader.getCompPackage(null);
//      Expect.expect(false);
//    }
//    catch (AssertException ex)
//    {
//      // do nothing
//    }
//    try
//    {
//      mainReader.doesCompPackageExist(null);
//      Expect.expect(false);
//    }
//    catch (AssertException ex)
//    {
//      // do nothing
//    }
//    String compPackageName = "Comp Package Name";
//    CompPackage compPackage = new CompPackage();
//    compPackage.setName(compPackageName);
//    Expect.expect(mainReader.doesCompPackageExist(compPackageName) == false);
//    try
//    {
//      mainReader.getCompPackage(compPackageName);
//      Expect.expect(false);
//    }
//    catch (AssertException ex)
//    {
//      // do nothing
//    }
//    mainReader.addCompPackage(compPackage);
//    Expect.expect(mainReader.doesCompPackageExist(compPackageName) == true);
//    Expect.expect(mainReader.getCompPackage(compPackageName) == compPackage);
//    Expect.expect(mainReader.getCompPackages().size() == 1);
//    Expect.expect(mainReader.getCompPackages().contains(compPackage));

    // addLandPatternPadsToLandPattern and getLandPatternPads
    try
    {
      mainReader.addLandPatternToPadNameToLandPatternPadMapMap(null, null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    try
    {
      mainReader.addLandPatternToPadNameToLandPatternPadMapMap(landPattern, null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    try
    {
      mainReader.addLandPatternToPadNameToLandPatternPadMapMap(null, new HashMap<String, LandPatternPad>());
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    try
    {
      mainReader.getPadNameToLandPatternPadMap(null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    try
    {
      mainReader.getPadNameToLandPatternPadMap(landPattern);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    Map<String, LandPatternPad> landPatternPads = new HashMap<String, LandPatternPad>();
    mainReader.addLandPatternToPadNameToLandPatternPadMapMap(landPattern, landPatternPads);
    Expect.expect(mainReader.getPadNameToLandPatternPadMap(landPattern) == landPatternPads);

    //addSideBoardNameToSideBoardList getSideBoardListFromSideBoardName
    try
    {
      mainReader.getSameSideBoardInstances(null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    try
    {
      mainReader.addSideBoardNameToSameSideBoardInstancesMap(null, null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    try
    {
      mainReader.addSideBoardNameToSameSideBoardInstancesMap("name", null);
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    try
    {
      mainReader.addSideBoardNameToSameSideBoardInstancesMap(null, new ArrayList<SideBoard>());
      Expect.expect(false);
    }
    catch (AssertException ex)
    {
      // do nothing
    }
    try
    {
      mainReader.getSameSideBoardInstances("bogus name");
      Expect.expect(false);
    }
    catch(AssertException ex)
    {
      // do nothing
    }
    String sideBoardName = "sideboard";
    List<SideBoard> boardList = new ArrayList<SideBoard>();
    mainReader.addSideBoardNameToSameSideBoardInstancesMap(sideBoardName, boardList);
    try
    {
      mainReader.addSideBoardNameToSameSideBoardInstancesMap(sideBoardName, boardList);
      Expect.expect(false);
    }
    catch(AssertException ex)
    {
      // do nothing
    }
    Expect.expect(mainReader.getSameSideBoardInstances(sideBoardName) == boardList);

    // software revision
    Expect.expect(mainReader.getSoftwareRevision() == 0.0);
    try
    {
      mainReader.setSoftwareRevision(-1.0);
      Expect.expect(false);
    }
    catch(AssertException ex)
    {
      // do nothing
    }
    double softwareRevision = 3.2;
    mainReader.setSoftwareRevision(softwareRevision);
    Expect.expect(mainReader.getSoftwareRevision() == softwareRevision);

// wpd virgo
//    // addInspectedCustomAlgorithmFamily getInspectedCustomAlgorithmFamilies
//    Expect.expect(mainReader.getInspectedCustomAlgorithmFamilies().size() == 0);
//    try
//    {
//      mainReader.addInspectedCustomAlgorithmFamily(null);
//      Expect.expect(false);
//    }
//    catch(AssertException ex)
//    {
//      // do nothing
//    }
//    GenericAlgorithmFamily genericFam = new GenericAlgorithmFamily();
//    genericFam.setAlgorithmFamilyEnum(AlgorithmFamilyEnum.CHIP);
//    CustomAlgorithmFamily custFam = new CustomAlgorithmFamily(genericFam);
//    mainReader.addInspectedCustomAlgorithmFamily(custFam);
//    Expect.expect(mainReader.getInspectedCustomAlgorithmFamilies().size() == 1);
//    Expect.expect(mainReader.getInspectedCustomAlgorithmFamilies().contains(custFam));

    // currentSideBoardName
    try
    {
      mainReader.getCurrentSideBoardName();
      Expect.expect(false);
    }
    catch(AssertException ex)
    {
      // do nothing
    }
    try
    {
      mainReader.setCurrentSideBoardName(null);
      Expect.expect(false);
    }
    catch(AssertException ex)
    {
      // do nothing
    }
    String currentSideBoardName = "whatever";
    mainReader.setCurrentSideBoardName(currentSideBoardName);
    Expect.expect(mainReader.getCurrentSideBoardName() == currentSideBoardName);
  }

  /**
   * @author Bill Darbie
   */
  private void testTopAndBottomBoardRotation()
  {
    MainReader mainReader = new MainReader();
    List<LocalizedString> warnings = new ArrayList<LocalizedString>();

    Pattern panelPattern = Pattern.compile(".*panel\\.(0|90|180|270)\\.(0|90|180|270|notop)\\.ndf$");

    List<String> panelNdfNames = new ArrayList<String>();
    panelNdfNames.add("panel.0.0.ndf");
    panelNdfNames.add("panel.0.90.ndf");
    panelNdfNames.add("panel.0.180.ndf");
    panelNdfNames.add("panel.0.270.ndf");

    panelNdfNames.add("panel.90.0.ndf");
    panelNdfNames.add("panel.90.90.ndf");
    panelNdfNames.add("panel.90.180.ndf");
    panelNdfNames.add("panel.90.270.ndf");

    panelNdfNames.add("panel.180.0.ndf");
    panelNdfNames.add("panel.180.90.ndf");
    panelNdfNames.add("panel.180.180.ndf");
    panelNdfNames.add("panel.180.270.ndf");

    panelNdfNames.add("panel.270.0.ndf");
    panelNdfNames.add("panel.270.90.ndf");
    panelNdfNames.add("panel.270.180.ndf");
    panelNdfNames.add("panel.270.270.ndf");

    panelNdfNames.add("panel.0.notop.ndf");
    panelNdfNames.add("panel.90.notop.ndf");
    panelNdfNames.add("panel.180.notop.ndf");
    panelNdfNames.add("panel.270.notop.ndf");

    try
    {
      String dataDir = getTestDataDir();
      String dir = dataDir + File.separator + "rotateBottomSideToMatchTopSide" + File.separator;

      String projectName = "2parts";
      String topBoardName = "pads_asc_a1";
      String bottomBoardName = "pads_asc_b1";
      String testPanelNdf = FileName.getLegacyPanelNdfFullPath(projectName);
      String testTopBoardNdf = FileName.getLegacyBoardNdfFullPath(projectName, topBoardName);
      String testBottomBoardNdf = FileName.getLegacyBoardNdfFullPath(projectName, bottomBoardName);

      for (String panelNdfName : panelNdfNames)
      {
        String panelNdf = dir + panelNdfName;
        System.out.println("");
        System.out.println("panel: " + panelNdfName);

        Matcher matcher = panelPattern.matcher(panelNdf);
        if (matcher.matches() == false)
          Assert.expect(false);

        String firstStr = matcher.group(1);
        String secondStr = matcher.group(2);

        String topRotation = null;
        String bottomRotation = null;
        boolean notop = false;
        if (secondStr.equals("notop") == false)
        {
          topRotation = firstStr;
          bottomRotation = secondStr;
          notop = false;
        }
        else
        {
          topRotation = "0";
          bottomRotation = firstStr;
          notop = true;
        }

        String topBoardNdf = dir + "topBoard" + File.separator + "board." + topRotation + ".ndf";
        String bottomBoardNdf = dir + "bottomBoard" + File.separator + "board." + bottomRotation + ".ndf";

        FileUtilAxi.copy(panelNdf, testPanelNdf);
        FileUtilAxi.copy(topBoardNdf, testTopBoardNdf);
        FileUtilAxi.copy(bottomBoardNdf, testBottomBoardNdf);

        Project project = Project.importProjectFromNdfs("2parts", warnings);
        if (notop == false)
        {
          Board board1 = project.getPanel().getBoard("1");
          Component topComp1 = board1.getComponents().get(0);
          Component topComp2 = board1.getComponents().get(1);

          PanelCoordinate coord1 = topComp1.getCenterCoordinateRelativeToPanelInNanoMeters();
          PanelCoordinate coord2 = topComp2.getCenterCoordinateRelativeToPanelInNanoMeters();
          System.out.println("top component 1 coord: " + coord1.getX() + ", " + coord1.getY());
          System.out.println("top component 2 coord: " + coord2.getX() + ", " + coord2.getY());
        }

        Board board2 = null;
        if (notop == false)
          board2 = project.getPanel().getBoard("2");
        else
          board2 = project.getPanel().getBoards().get(0);
        Component bottomComp1 = board2.getComponents().get(0);
        Component bottomComp2 = null;
        if (notop == false)
          bottomComp2 = board2.getComponents().get(1);

        PanelCoordinate coord1 = bottomComp1.getCenterCoordinateRelativeToPanelInNanoMeters();
        System.out.println("bottom component 1 coord: " + coord1.getX() + ", " + coord1.getY());
        if (notop == false)
        {
          PanelCoordinate coord2 = bottomComp2.getCenterCoordinateRelativeToPanelInNanoMeters();
          System.out.println("bottom component 2 coord: " + coord2.getX() + ", " + coord2.getY());
        }
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      String dataDir = getTestDataDir();
      String dir = dataDir + File.separator + "rotateBottomSideToMatchTopSide" + File.separator;
      String panelNdf = dir + "panel.0.0.ndf";
      String topBoardNdf = dir + "topBoard" + File.separator + "board.0.ndf";
      String bottomBoardNdf = dir + "bottomBoard" + File.separator + "board.0.ndf";

      String projectName = "2parts";
      String topBoardName = "pads_asc_a1";
      String bottomBoardName = "pads_asc_b1";
      String testPanelNdf = FileName.getLegacyPanelNdfFullPath(projectName);
      String testTopBoardNdf = FileName.getLegacyBoardNdfFullPath(projectName, topBoardName);
      String testBottomBoardNdf = FileName.getLegacyBoardNdfFullPath(projectName, bottomBoardName);

      try
      {
        FileUtilAxi.copy(panelNdf, testPanelNdf);
        FileUtilAxi.copy(topBoardNdf, testTopBoardNdf);
        FileUtilAxi.copy(bottomBoardNdf, testBottomBoardNdf);
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }


    // load 2parts
    // for all 20 cases:
    //    copy correct files
    //    load project
    //    check for 4 components location
  }
}
