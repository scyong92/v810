package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * If the auto focus technique is invalid, this exception should be thrown.
 *
 * author George A. David
 */
class InvalidAutoFocusTechniqueNameDatastoreException extends DatastoreException
{

  /**
   * @author George A. David
   */
  InvalidAutoFocusTechniqueNameDatastoreException(String fileName,
                                                  int lineNumber,
                                                  String expectedSyntax,
                                                  String invalidFieldName,
                                                  String invalidFieldData)

  {
    super(new LocalizedString("DS_NDF_ERROR_INVALID_AUTO_FOCUS_TECHNIQUE_NAME_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           expectedSyntax,
                                           invalidFieldName,
                                           invalidFieldData}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidFieldName != null);
    Assert.expect(invalidFieldData != null);
  }
}
