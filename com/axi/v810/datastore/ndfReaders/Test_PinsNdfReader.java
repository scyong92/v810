package com.axi.v810.datastore.ndfReaders;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Keith Lee
 */
class Test_PinsNdfReader extends UnitTest
{
  // VARIABLES
  private static MainReader _mainReader = null;
  private static PinsNdfReader _pinsNdfReader = null;
  private static List<LocalizedString> _warnings = new ArrayList<LocalizedString>();

  private static BufferedReader _is = null;
  private static PrintWriter _os = null;

  /**
   * @author Keith Lee
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_PinsNdfReader());
  }

  /**
   * @author Keith Lee
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    _is = is;
    _os = os;

    _mainReader = new MainReader();
    _pinsNdfReader = new PinsNdfReader(_mainReader);

    testOtherCornerCases();
    testValidFile();
  }

  /**
   * @author Keith Lee
   */
  private void testOtherCornerCases()
  {
    boolean exception = false;

    try
    {
      _pinsNdfReader.parseAttributeLine("");
    }
    catch(AssertException e)
    {
      exception = true;
    }
    catch(DatastoreException e)
    {
      // this exception should not happen
      e.printStackTrace();
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      _pinsNdfReader.parseDataLine("@C1 1 A1");
    }
    catch(AssertException e)
    {
      exception = true;
    }
    catch(DatastoreException e)
    {
      // this exception should not happen
      e.printStackTrace();
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      _pinsNdfReader.postProcessData();
    }
    catch(AssertException e)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      _pinsNdfReader.parseFile(null, _warnings);
    }
    catch(AssertException e)
    {
      exception = true;
    }
    catch(DatastoreException e)
    {
      // this exception should not happen
      e.printStackTrace();
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      _pinsNdfReader.parseFile("", null);
    }
    catch(AssertException e)
    {
      exception = true;
    }
    catch(DatastoreException e)
    {
      // this exception should not happen
      e.printStackTrace();
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      _pinsNdfReader.parseFile("", _warnings);
    }
    catch(FileNotFoundDatastoreException e)
    {
      exception = true;
    }
    catch(DatastoreException e)
    {
      // this exception should not happen
      e.printStackTrace();
    }
    Expect.expect(exception);
    exception = false;
  }

  /**
   * @author Keith Lee
   */
  private void testValidFile()
  {
    String softwareRevision = null;
    String panelName = null;
    String boardName = null;

    String panelFilename = null;
    String surfaceMountLandPatternFilename = null;
    String throughHoleLandPatternFilename = null;
    String packageFilename = null;
    String componentFilename = null;
    String pinsFilename = null;
    String componentName = null;

    Project project = null;
    PanelNdfReader panelNdfReader = null;
    ThroughHoleLandPatternNdfReader throughHoleLandPatternNdfReader = null;
    ComponentNdfReader componentNdfReader = null;

    try
    {
      softwareRevision = _is.readLine();
      panelName = _is.readLine();
      boardName = _is.readLine();
      panelFilename = FileName.getLegacyPanelNdfFullPath(panelName);
      surfaceMountLandPatternFilename = FileName.getLegacySurfaceMountLandPatternNdfFullPath(panelName);
      throughHoleLandPatternFilename = FileName.getLegacyThroughHoleLandPatternNdfFullPath(panelName);
      packageFilename = FileName.getLegacyPackageNdfFullPath(panelName);
      componentFilename = FileName.getLegacyComponentNdfFullPath(panelName, boardName);
      _os.println("panel: " + panelName);
      _os.println("board: " + boardName);
      pinsFilename = _is.readLine();
      while(pinsFilename != null)
      {
        _mainReader = new MainReader();
        if(softwareRevision.equals("") == false)
          _mainReader.setSoftwareRevision(Integer.parseInt(softwareRevision));
        panelNdfReader = new PanelNdfReader(_mainReader);
        throughHoleLandPatternNdfReader = new ThroughHoleLandPatternNdfReader(_mainReader);
        componentNdfReader = new ComponentNdfReader(_mainReader);
        _pinsNdfReader = new PinsNdfReader(_mainReader);

        try
        {
          panelNdfReader.parseFile(panelFilename, _warnings);
          project = panelNdfReader.getProject();
          Panel panel = project.getPanel();
          PanelSettings panelSettings = panel.getPanelSettings();
          _mainReader.setProject(project);

          throughHoleLandPatternNdfReader.parseFile(throughHoleLandPatternFilename, _warnings);
          componentNdfReader.parseFile(componentFilename, _warnings);
        }
        catch(Exception e)
        {
          // this exception should not happen
          e.printStackTrace();
        }

        _warnings.clear();
        try
        {
          _os.println("pins.ndf: " + pinsFilename);
          _pinsNdfReader.parseFile(pinsFilename, _warnings);
          _os.println("Warnings: " + _warnings.size());
          componentName = _is.readLine();
          while((componentName != null) && (componentName.equals("")== false))
          {
            Component component = _mainReader.getComponent(componentName);
            Expect.expect(component != null);
            _os.println("Component: " + component.getReferenceDesignator());
            List<Pad> pads = component.getPads();
            for (Pad pad : pads)
            {
              _os.println("\tPad: " + pad.getName() +  " : " + pad.getName());
            }
            componentName = _is.readLine();
          }
        }
        catch(DatastoreException e)
        {
          _os.println("DatastoreException");
        }
        pinsFilename = _is.readLine();
      }
    }
    catch(IOException e)
    {
      // this exception should not happen
      e.printStackTrace();
    }
  }
}
