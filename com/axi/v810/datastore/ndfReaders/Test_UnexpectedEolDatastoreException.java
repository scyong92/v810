package com.axi.v810.datastore.ndfReaders;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

public class Test_UnexpectedEolDatastoreException extends UnitTest
{
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_UnexpectedEolDatastoreException());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    UnexpectedEolDatastoreException ex = new UnexpectedEolDatastoreException("fileName", 100, "expected");
    String message = ex.getLocalizedMessage();
    os.println(message);
  }
}
