package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This exception is thrown when an invalid board side is found in the panel ndf file
 * @author Andy Mechtenberg
 */
public class InvalidBoardSideDatastoreException extends DatastoreException
{
  /**
   * @author Andy Mechtenberg
   */
  public InvalidBoardSideDatastoreException(String fileName, 
                                            int lineNumber,
                                            String expectedSyntax,
                                            String invalidFieldName,
                                            String invalidFieldData)
  {
    super(new LocalizedString("DS_NDF_ERROR_INVALID_BOARD_SIDE_KEY", new Object[]{fileName,
                                                                     new Integer(lineNumber),
                                                                     expectedSyntax,
                                                                     invalidFieldName,
                                                                     invalidFieldData}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidFieldName != null);
    Assert.expect(invalidFieldData != null);
  }
}
