package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;
/**
 * This class get thrown when duplicate components are found.
 * @author George A. David
 */
public class DuplicateComponentDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public DuplicateComponentDatastoreException(String referenceDesignator,
                                              String topSideBoardFileName,
                                              String bottomSideBoardFileName)
  {
    super(new LocalizedString("DS_NDF_ERROR_DUPLICATE_COMPONENT_ON_BOTH_BOARD_SIDES_KEY",
          new Object[]{topSideBoardFileName,
                       bottomSideBoardFileName,
                       referenceDesignator}));
    Assert.expect(referenceDesignator != null);
    Assert.expect(topSideBoardFileName != null);
    Assert.expect(bottomSideBoardFileName != null);
  }

  /**
   * @author George A. David
   */
  public DuplicateComponentDatastoreException(String fileName,
                                              int lineNumber,
                                              String componentName)
  {
    super(new LocalizedString("DS_NDF_ERROR_DUPLICATE_COMPONENT_IN_SAME_FILE_KEY",
          new Object[]{fileName,
                       new Integer(lineNumber),
                       componentName}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(componentName != null);
  }
}
