package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when an 8.0 program
 * contains a pin that uses an invalid/unrecognized algorithm family
 * @author George A. David
 */
public class InvalidAlgorithmFamilyDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public InvalidAlgorithmFamilyDatastoreException(String componentFile,
                                                  String packageName,
                                                  String invalidAlgorithmFamilyName,
                                                  String packageFile,
                                                  String referenceDesignator)
  {
    super(new LocalizedString("DS_NDF_ERROR_INVALID_ALGORITHM_FAMILY_KEY",
          new Object[]{componentFile,
                       packageName,
                       invalidAlgorithmFamilyName,
                       packageFile,
                       referenceDesignator}));
    Assert.expect(componentFile != null);
    Assert.expect(packageName != null);
    Assert.expect(invalidAlgorithmFamilyName != null);
    Assert.expect(packageFile != null);
    Assert.expect(referenceDesignator != null);
  }

  /**
   * @author George A. David
   */
  public InvalidAlgorithmFamilyDatastoreException(String packageFile,
                                                  int lineNumber,
                                                  String pinName,
                                                  String packageName,
                                                  String invalidAlgorithmFamilyName)
  {
    super(new LocalizedString("DS_NDF_ERROR_PIN_USES_INVALID_ALGORITHM_FAMILY_KEY",
          new Object[]{packageFile,
                       new Integer(lineNumber),
                       pinName,
                       packageName,
                       invalidAlgorithmFamilyName}));

    Assert.expect(packageFile != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(pinName != null);
    Assert.expect(packageName != null);
    Assert.expect(invalidAlgorithmFamilyName != null);
  }
}
