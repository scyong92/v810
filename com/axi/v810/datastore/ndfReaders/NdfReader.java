package com.axi.v810.datastore.ndfReaders;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This is the base class for all NdfReader classes.  It provides the general purpose
 * parse method.  The parse method calls several abstract methods which must be
 * provided by any class that inherits from this one. This class provides get methods
 * for common methods that most if not all of the readers need to use
 *
 * @author Keith Lee
 */
public abstract class NdfReader
{
  public static final String CHECKSUM = ".CHECKSUM:";

  private static final int _MIN_LEGACY_SUBTYPE = 0;
  private static final int _MAX_LEGACY_SUBTYPE = 255;
  private static final String _UNIT_TOKEN = ".UNIT:";
  private static final String _INCHES_TOKEN = "INCHES";
  private static final String _MILS_TOKEN = "MILS";
  private static final String _MILLIMETERS_TOKEN = "MILLIMETERS";
  private static final String _RECTANGULAR_SHAPE = "R";
  private static final String _CIRCULAR_SHAPE = "C";

  private static final char _COMMENT = '#';
  private static final char _ALTERNATE_COMMENT = '*';
  private static final char _DATA_LINE_SYMBOL = '@';
  private static final char _ATTRIBUTE_LINE_SYMBOL = '.';
  private static final String _DELIMITERS = " \t\n\r\f";
  private static final boolean _DONT_RETURN_DELIMITERS = false;
  private static final char CNTRL_Z = '\u001A';
  private static final String _NO_DELIMITERS = "";
  private static final String _SUBTYPE_COMMENT_TOKEN = _ATTRIBUTE_LINE_SYMBOL + "SUBTYPE_COMMENT" + ":";

  // _5dxNdfConversionMethod = 0, uses land pattern name to create subtype name in V810
  // _5dxNdfConversionMethod = 1, uses subtype name & subtype ID to create subtype name in V810
  private static char _5dxNdfConversionMethod = 0;

  // .checksum: <value>
  private static final String _CHECKSUM_ATTRIBUTE_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_CHECKSUM_EXPECTED_SYNTAX_KEY");
  private static final String _CHECKSUM_VALUE_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_CHECKSUM_VALUE_KEY");


  // the following todos have not been fixed because these variables are still being used
  // by readers that were not included in our datastore clean up.
//  /** @todo actfix - this string is not localizable, in that it is an english phrase */
  static final String EXPECTED_POSITIVE_NUMBER = "a positive number";
  /** @todo actfix - this string is not localizable, in that it is an english phrase */
  static final String EXPECTED_POSITIVE_NON_ZERO_NUMBER = "a positive non-zero number";

  static final String EXPECTED_BOOLEAN = "<true | false>";
  static final String EXPECTED_0_90_180_OR_270_DEGREES = DegreesEnum.ZERO + ", " +
                                                         DegreesEnum.NINETY + ", " +
                                                         DegreesEnum.ONE_EIGHTY + "," +
                                                         DegreesEnum.TWO_SEVENTY;

  static final String EXPECTED_SYMBOLS = _DATA_LINE_SYMBOL + ", " +
                                         _ATTRIBUTE_LINE_SYMBOL + ", " +
                                         _COMMENT + ", " +
                                         _ALTERNATE_COMMENT;
  static final String EXPECTED_SHAPE = _RECTANGULAR_SHAPE + ", " + _CIRCULAR_SHAPE;

  protected LineNumberReader _is;
  protected File _file;
  protected String _currentFile;
  protected List<LocalizedString> _warnings = new ArrayList<LocalizedString>();
  protected MainReader _mainReader;
  protected MathUtilEnum _parsedUnits;
  protected static final MathUtilEnum _DATASTORE_UNITS = MathUtilEnum.NANOMETERS;
  protected static final MathUtilEnum _DEFAULT_NDF_UNITS = MathUtilEnum.MILS;
  protected boolean _unitLineFound = false;
  protected boolean _checkSumFound = false;
  protected static boolean _is5dxProject = false;
  protected static boolean _is5dxNdf = false;
  protected static boolean _validRequiredDataLineForNoBoard = true;
  protected static Map<String, String> _5dxProjectNameTo5dxHashNameMap = new HashMap<String, String>();

  // all of the following abstract  methods are used by the parseFile() method.
  // Refer to that method to see exactly when and how they are used
  // initializeBeforeParsing()
  //   called before any parsing is done
  // parseLinesRequiredByOtherLines()
  //   used to parse in any lines that need to be read in before other lines are read in.
  //   the .UNIT line is an example of a line that could be read in here
  // parseDataLine(String line)
  //   parse in a single data line (begins with '@')
  // parseAttributeLine(String line)
  //   parse in a single attribute line (begins with '.')
  // postProcessData()
  //   after all parsing is done any post processing of the data is done here
  // resetAfterParsing();
  //   called last to allow variables to be cleared for garbage collection
  protected abstract void initializeBeforeParsing();
  protected abstract void parseLinesRequiredByOtherLines() throws DatastoreException;
  protected abstract void parseDataLine(String line) throws DatastoreException;
  protected abstract void parseAttributeLine(String line) throws DatastoreException;
  protected abstract void postProcessData() throws DatastoreException;
  protected abstract void resetAfterParsing();


  /**
   * @param mainReader the MainReader associated with the panel program.
   * @author Keith Lee
   */
  NdfReader(MainReader mainReader)
  {
    Assert.expect(mainReader != null);

    _mainReader = mainReader;
  }

  /**
   * @author Bill Darbie
   */
  static String getDelimiters()
  {
    Assert.expect(_DELIMITERS != null);

    return _DELIMITERS;
  }

  /**
   * @author Bill Darbie
   */
  protected static int getMinLegacySubtype()
  {
    return _MIN_LEGACY_SUBTYPE;
  }

  /**
   * @author Bill Darbie
   */
  protected static final int getMaxLegacySubtype()
  {
    return _MAX_LEGACY_SUBTYPE;
  }

  /**
   * @author Bill Darbie
   */
  protected static final String getUnitToken()
  {
    Assert.expect(_UNIT_TOKEN != null);

    return _UNIT_TOKEN;
  }

  /**
   * @author George A. David
   */
  public static String getSubtypeCommentToken()
  {
    Assert.expect(_SUBTYPE_COMMENT_TOKEN != null);

    return _SUBTYPE_COMMENT_TOKEN;
  }

  /**
   * @author Bill Darbie
   */
  static MathUtilEnum getDatastoreUnits()
  {
    Assert.expect(_DATASTORE_UNITS != null);

    return _DATASTORE_UNITS;
  }

  /**
   * @author Bill Darbie
   */
  static MathUtilEnum getDefaultNdfUnits()
  {
    Assert.expect(_DEFAULT_NDF_UNITS != null);

    return _DEFAULT_NDF_UNITS;
  }

  /**
   * @author Bill Darbie
   */
  public static char getDataLineSymbol()
  {
    return _DATA_LINE_SYMBOL;
  }

  /**
   * @author Bill Darbie
   */
  static char getAttributeLineSymbol()
  {
    return _ATTRIBUTE_LINE_SYMBOL;
  }

  /**
   * @author Bill Darbie
   */
  static char getCommentChar()
  {
    return _COMMENT;
  }

  /**
   * @author Bill Darbie
   */
  static String getRectangularShapeToken()
  {
    Assert.expect(_RECTANGULAR_SHAPE != null);

    return _RECTANGULAR_SHAPE;
  }

  /**
   * @author Bill Darbie
   */
  static String getCircularShapeToken()
  {
    Assert.expect(_CIRCULAR_SHAPE != null);

    return _CIRCULAR_SHAPE;
  }

  /**
   * parses the ndf file.
   *
   * @param filename the full name of the ndf file to be parsed in.
   * @param warnings list of LocalizedStrings containing warnings about problems in the ndf file.
   * @author Keith Lee
   */
  public void parseFile(String filename, List<LocalizedString> warnings) throws DatastoreException
  {
    try
    {
      Assert.expect(filename != null);
      Assert.expect(warnings != null);

      _currentFile = filename;
      _file = new File(_currentFile);
      _is = ParseUtil.openFile(filename);
      _warnings.clear();

      // initialize any variables before beginning the parsing of the file
      init();
      initializeBeforeParsing();

      try
      {
        // mark the beginning of the file so the _is.reset() method will bring
        // us back to the beginning of the file any time.
        // Note that we must set the read ahead limit to be 1 larger than the file
        // so the _is.reset() method is guaranteed to work
        long readAheadLimit = _file.length() + 1;
        Assert.expect(readAheadLimit <= Integer.MAX_VALUE);
        _is.mark((int)readAheadLimit);

        // now parse lines that are required to be in the ndf file
        parseLinesRequiredByOtherLines();
        _is.reset();
      }
      catch(IOException ioe)
      {
        DatastoreException dex = new DatastoreException(_currentFile);
        dex.initCause(ioe);
        throw dex;
      }

      String line = ParseUtil.readNextLine(_is, _currentFile);
      while(line != null)
      {
        // get rid of any CNTRL_Z
        line = line.replace(CNTRL_Z, ' ');

        // remove any comments
        if (_mainReader.ndfsAreWellFormed())
        {
          // pre 8.0 # is actually allowed as a valid character in entries so
          // we cannot blindly rip out comments,
          // 8.0 and beyond we can
          int commentIndex = line.indexOf(_COMMENT);
          if (commentIndex != -1)
            line = line.substring(0, commentIndex);
        }

        // trim out leading and trailing white space
        line = line.trim();
        if (line.length() > 0)
        {
          char firstChar = line.charAt(0);
          if (firstChar == _DATA_LINE_SYMBOL)
          {
            // line starts with an @
            // rip out the @
            line = line.substring(1);
            parseDataLine(line);
            setValidRequiredDataLineForNoBoard(false);  //  XCR1163 Jack Hwee - fix to load single-sided panel ndf.
          }
          else if (firstChar == _ATTRIBUTE_LINE_SYMBOL)
          {
            // line starts with a .
            parseAttributeLine(line);
          }
          else if (firstChar == _COMMENT)
          {
            // skip: this is a comment line
          }
          else if ((_mainReader.ndfsAreWellFormed() == false) &&  (firstChar == _ALTERNATE_COMMENT))
          {
            // skip: this is a comment line also for pre 8.0 ndf files
          }
          else
          {
            // something unexpected was found - give an error
            if (_mainReader.ndfsAreWellFormed())
            {
              throw new InvalidStartOfLineDatastoreException(_currentFile,
                                                             _is.getLineNumber(),
                                                             EXPECTED_SYMBOLS,
                                                             String.valueOf(firstChar));
            }
            else
            {
              setValidRequiredDataLineForNoBoard(false); //  XCR1163 Jack Hwee - fix to load single-sided panel ndf.
              unrecognizedLineWarning(line);
            }
          }
        }

        line = ParseUtil.readNextLine(_is, _currentFile);
      }

      postProcessData();

    }
    finally
    {
      // do this whether or not an exception occurs
      warnings.addAll(_warnings);
      try
      {
        if (_is != null)
          _is.close();
      }
      catch(IOException ex)
      {
        ex.printStackTrace();
      }

      reset();
      resetAfterParsing();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void init()
  {
    _parsedUnits = null;

    _checkSumFound = false;
    _unitLineFound = false;
    _validRequiredDataLineForNoBoard = true;   //Jack Hwee - fix to load single-sided panel ndf.

  }

  /**
   * This method clears all variables common to all classes which extend NdfReader.
   * @author Keith Lee
   */
  private void reset()
  {
    _is = null;
    _file = null;
    _currentFile = null;
    _warnings.clear();
//    _mainReader = null;
    _parsedUnits = null;
  }

  /**
   * Since comments can be added to the end of a valid data and attribute lines,
   * this method checks to see that the end token passed in is indeed a comment.
   * @return true if the token is a comment or the token equals null.
   * @author Keith Lee
   */
  protected boolean isValidComment(String token)
  {
    boolean validComment = false;

    if (token == null)
      validComment = true;
    else if (token.length() > 0)
    {
      String tok = token.trim();
      if ( (tok.charAt(0) == _COMMENT) ||
          (tok.charAt(0) == ';') ||
          (tok.charAt(0) == ':') )
      {
        validComment = true;
      }
    }

    return validComment;
  }

  /**
   * Creates a standard duplicate line warning. This is to only be used with
   * lines that use the standard duplicate line warning. Some lines, such as
   * .TOP_SIDE_REFERENCE: in SurfaceMapNdfReader, will not use this function,
   * but instead create their own warning.
   * @author Keith Lee
   */
  protected void duplicateTokenWarning(String token)
  {
    Assert.expect(token != null);

    Object[] args = new Object[]{_currentFile, new Integer(_is.getLineNumber()), token};
    _warnings.add(new LocalizedString("DS_NDF_WARNING_DUPLICATE_TOKEN_KEY", args));
  }

  /**
   * Creates a standard deprecated (obsolete) attribute line warning.
   * @author Keith Lee
   */
  protected void deprecatedTokenWarning(String token)
  {
    Assert.expect(token != null);

    Object[] args = new Object[]{_currentFile, new Integer(_is.getLineNumber()), token};
    _warnings.add(new LocalizedString("DS_NDF_WARNING_DEPRECATED_TOKEN_KEY", args));
  }

  /**
   * Creates a standard unrecognized token warning.
   * @author Keith Lee
   */
  protected void unrecognizedTokenWarning(String token)
  {
    Assert.expect(token != null);

    Object[] args = new Object[]{_currentFile, new Integer(_is.getLineNumber()), token};
    _warnings.add(new LocalizedString("DS_NDF_WARNING_UNRECOGNIZED_TOKEN_KEY", args));
  }

  /**
   * Creates a standard unrecognized line warning.
   * @author Bill Darbie
   */
  private void unrecognizedLineWarning(String line)
  {
    Assert.expect(line != null);

    Object[] args = new Object[]{_currentFile,
                                 new Integer(_is.getLineNumber()),
                                 EXPECTED_SYMBOLS,
                                 String.valueOf(line.charAt(0))};
    _warnings.add(new LocalizedString("DS_NDF_WARNING_INVALID_START_OF_LINE_KEY", args));
  }

  /**
   * parses an attribute line in a ndf file that follows the following format
   * .unit: <unit>
   *
   * @param line the <unit> portion of the attribute line.
   * @author Keith Lee
   */
  protected void parseAttributeLineUnit(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // since everything in a .UNIT line is a token, there is no need to localize it.  We don't accept the
    // the french work for MILS, so we'll hardcode the expected syntax here
    String unitsField = " <" + _INCHES_TOKEN + " | " + _MILS_TOKEN + " | " + _MILLIMETERS_TOKEN + ">";
    String expectedSyntax = _UNIT_TOKEN +  unitsField;

    StringTokenizer st = new StringTokenizer(line, _DELIMITERS, _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile, _is.getLineNumber(), expectedSyntax, unitsField);
    }
    else if (token.equalsIgnoreCase(_MILS_TOKEN))
    {
      _parsedUnits = MathUtilEnum.MILS;
    }
    else if (token.equalsIgnoreCase(_INCHES_TOKEN))
    {
      _parsedUnits = MathUtilEnum.INCHES;
    }
    else if (token.equalsIgnoreCase(_MILLIMETERS_TOKEN))
    {
      _parsedUnits = MathUtilEnum.MILLIMETERS;
    }
    else
    {
      throw new InvalidUnitsDatastoreException(_currentFile,
                                               _is.getLineNumber(),
                                               expectedSyntax,
                                               unitsField,
                                               token);
    }

    token = ParseUtil.getNextToken(st);
    if ((_mainReader.ndfsAreWellFormed()) && (isValidComment(token) == false))
    {
      throw new ExpectedEolDatastoreException(_currentFile, _is.getLineNumber(), expectedSyntax, token);
    }

    _unitLineFound = true;
  }

  /**
   * parses an attribute line in a ndf file that follows the following format
   * .checksum: <value>
   *
   * @author Keith Lee
   */
  protected void parseAttributeLineCheckSum(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    if (_checkSumFound)
    {
      throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), CHECKSUM);
    }


    long checkSum = 0;

    StringTokenizer st = new StringTokenizer(line, _DELIMITERS, _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _CHECKSUM_ATTRIBUTE_EXPECTED_SYNTAX,
                                                _CHECKSUM_VALUE_FIELD);
    }
    try
    {
      checkSum = StringUtil.convertStringToLong(token);
    }
    catch(BadFormatException e)
    {
      DatastoreException dex = new ExpectedNumberDatastoreException(_currentFile,
                                                                    _is.getLineNumber(),
                                                                    _CHECKSUM_ATTRIBUTE_EXPECTED_SYNTAX,
                                                                    _CHECKSUM_VALUE_FIELD,
                                                                    token);
      dex.initCause(e);
      throw dex;
    }
    token = ParseUtil.getNextToken(st);
    if (isValidComment(token) == false)
    {
      throw new ExpectedEolDatastoreException(_currentFile,
                                              _is.getLineNumber(),
                                              _CHECKSUM_ATTRIBUTE_EXPECTED_SYNTAX,
                                              token);
    }

    _checkSumFound = true;
  }

  /**
   * This function first attempts to convert the entire token to a positive double. If it fails, and the ndfs are
   * well formed, then an exception is thrown. Otherwise, it attempts to convert as much of the
   * token as possible to a positive double.
   *
   * @param tokenToParse the token to parse
   * @param expectedSyntax the expected syntax of the line the tokenToParse variable was extracted from
   * @param fieldBeingParsed the current field of the line expected syntax being parsed
   * @author George A. David
   */
  protected double parsePositiveNonZeroDouble(String tokenToParse, String expectedSyntax, String fieldBeingParsed) throws DatastoreException
  {
    Assert.expect(tokenToParse != null);
    Assert.expect(expectedSyntax != null);
    Assert.expect(fieldBeingParsed != null);

    double doubleValue = 0.0;

    Exception ex = null;
    try
    {
      doubleValue = StringUtil.convertStringToPositiveDouble(tokenToParse);
      if (doubleValue == 0.0)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                    _is.getLineNumber(),
                                                                    expectedSyntax,
                                                                    fieldBeingParsed,
                                                                    tokenToParse);
        }
        else
        {
          doubleValue = 1.0;
          //warn the user that we expected a positive non zero number and that we
          // will set it to 1.
          Object[] args = new Object[]{ _currentFile,
                                        new Integer(_is.getLineNumber()),
                                        expectedSyntax,
                                        fieldBeingParsed,
                                        tokenToParse,
                                        new Double(doubleValue)};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_NUMBER_KEY", args));
        }
      }
    }
    catch(BadFormatException bfe)
    {
      ex = bfe;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        ExpectedPositiveNonZeroNumberDatastoreException dex = new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                                                                  _is.getLineNumber(),
                                                                                                                  expectedSyntax,
                                                                                                                  fieldBeingParsed,
                                                                                                                  tokenToParse);
        dex.initCause(ex);
        throw dex;
      }
      else
      {
        // the legacy compiler used the c++ atoi() function, which converted as
        // much of the string as possible to a float, and that is what we will do here.
        ex = null;
        try
        {
          doubleValue = StringUtil.legacyConvertStringToPositiveDouble(tokenToParse);
          if (doubleValue == 0.0)
          {
            doubleValue = 1.0;
          }
        }
        catch(BadFormatException anotherBfe)
        {
          ex = anotherBfe;
        }
        catch (ValueOutOfRangeException vex)
        {
          ex = vex;
        }
        if (ex != null)
        {
          // the legacy compiler used the c++ built-in function atoi() to convert
          // a string to an integer. if the string did not begin with a positive or
          // negative integer, the value returned is 0. In this case, 0 is not valid,
          // so we will return 1.
          doubleValue = 1.0;
        }
        // warn the user that we expected a positive non zero number
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      expectedSyntax,
                                      fieldBeingParsed,
                                      tokenToParse,
                                      new Double(doubleValue)};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_NUMBER_KEY", args));
      }
    }


    return doubleValue;
  }

  /**
   * The function first attempts to convert the entire token to a positive double. If it fails, and the ndfs are
   * well formed, then an exception is thrown. Otherwise, it attempts to convert as much of the
   * token as possible to a positive double.
   *
   * @param tokenToParse the token to parse
   * @param expectedSyntax the expected syntax of the line the tokenToParse variable was extracted from
   * @param fieldBeingParsed the current field of the line expected syntax being parsed
   * @param units the units of the current ndf file being parsed.
   * @author George A. David
   */
  protected double parsePositiveNonZeroDouble(String tokenToParse, String expectedSyntax, String fieldBeingParsed, MathUtilEnum units) throws DatastoreException
  {
    Assert.expect(tokenToParse != null);
    Assert.expect(expectedSyntax != null);
    Assert.expect(fieldBeingParsed != null);
    Assert.expect(units != null);

    double doubleValue = 0.0;

    Exception ex = null;
    try
    {
      doubleValue = StringUtil.convertStringToPositiveDouble(tokenToParse);
      if (doubleValue == 0.0)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                    _is.getLineNumber(),
                                                                    expectedSyntax,
                                                                    fieldBeingParsed,
                                                                    tokenToParse);
        }
        else
        {
          //warn the user that we expected a positive non zero number
          //and we will set the value to 1.0 mils
          doubleValue = 1.0;
          Object[] args = new Object[]{ _currentFile,
                                        new Integer(_is.getLineNumber()),
                                        expectedSyntax,
                                        fieldBeingParsed,
                                        tokenToParse,
                                        new Double(doubleValue),
                                        MathUtilEnum.MILS};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_NUMBER_UNITS_KEY", args));
          doubleValue = MathUtil.convertUnits(doubleValue, MathUtilEnum.MILS, units);
        }
      }
    }
    catch(BadFormatException bfe)
    {
      ex = bfe;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        ExpectedPositiveNonZeroNumberDatastoreException dex = new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                                                                  _is.getLineNumber(),
                                                                                                                  expectedSyntax,
                                                                                                                  fieldBeingParsed,
                                                                                                                  tokenToParse);
        dex.initCause(ex);
        throw dex;
      }
      else
      {
        // the legacy compiler used the c++ atoi() function, which converted as
        // much of the string as possible to a float, and that is what we will do here.
        MathUtilEnum unitsUsed = units;
        double valueUsed = 0.0;
        ex = null;
        try
        {
          doubleValue = StringUtil.legacyConvertStringToPositiveDouble(tokenToParse);
          valueUsed = doubleValue;
          if (doubleValue == 0.0)
          {
            valueUsed = 1.0;
            doubleValue = MathUtil.convertUnits(valueUsed, MathUtilEnum.MILS, units);
            unitsUsed = MathUtilEnum.MILS;
          }
        }
        catch(BadFormatException anotherBfe)
        {
          ex = anotherBfe;
        }
        catch (ValueOutOfRangeException vex)
        {
          ex = vex;
        }
        if (ex != null)
        {
          // the legacy compiler used the c++ built-in function atoi() to convert
          // a string to an integer. if the string did not begin with a positive or
          // negative integer, then the function returns 0. O is not valid in this case
          // so we will use 1.0 mils.
          valueUsed = 1.0;
          doubleValue = MathUtil.convertUnits(valueUsed, MathUtilEnum.MILS, units);
          unitsUsed = MathUtilEnum.MILS;
        }
        // warn the user that we expected a positive non zero number
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      expectedSyntax,
                                      fieldBeingParsed,
                                      tokenToParse,
                                      new Double(valueUsed),
                                      unitsUsed};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_NUMBER_UNITS_KEY", args));
      }
    }


    return doubleValue;
  }

  /**
   * The first attempts to convert the entire token to a positive double. If it fails, and the ndfs are
   * well formed, then an exception is thrown. Otherwise, it attempts to convert as much of the
   * token as possible to a positive double.
   *
   * @param tokenToParse the token to parse
   * @param expectedSyntax the expected syntax of the line the tokenToParse variable was extracted from
   * @param fieldBeingParsed the current field of the line expected syntax being parsed
   * @author George A. David
   */
  protected double parsePositiveDouble(String tokenToParse, String expectedSyntax, String fieldBeingParsed) throws DatastoreException
  {
    Assert.expect(tokenToParse != null);
    Assert.expect(expectedSyntax != null);
    Assert.expect(fieldBeingParsed != null);

    double doubleValue = 0.0;

    Exception ex = null;
    try
    {
      doubleValue = StringUtil.convertStringToPositiveDouble(tokenToParse);
    }
    catch(BadFormatException bfe)
    {
      ex = bfe;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        ExpectedPositiveNumberDatastoreException dex = new ExpectedPositiveNumberDatastoreException(_currentFile,
                                                                                                    _is.getLineNumber(),
                                                                                                    expectedSyntax,
                                                                                                    fieldBeingParsed,
                                                                                                    tokenToParse);
        dex.initCause(ex);
        throw dex;
      }
      else
      {
        // the legacy compiler used the c++ atof() function, which converted as
        // much of the string as possible to a float, and that is what we will do here.
        ex = null;
        try
        {
          doubleValue = StringUtil.legacyConvertStringToPositiveDouble(tokenToParse);
        }
        catch(BadFormatException anotherBfe)
        {
          ex = anotherBfe;
        }
        catch (ValueOutOfRangeException vex)
        {
          ex = vex;
        }
        if (ex != null)
        {
          // the legacy compiler used the c++ built-in function atof() to convert
          // a string to an integer. if the string did not begin with a positive or
          // negative integer, it would return 0.0
          doubleValue = 0.0;
        }
        // warn the user that we expected a number
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      expectedSyntax,
                                      fieldBeingParsed,
                                      tokenToParse,
                                      new Double(doubleValue)};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NUMBER_KEY", args));
      }
    }

    return doubleValue;
  }

  /**
   * The function first attempts to convert the entire token to a positive double. If it fails, and the ndfs are
   * well formed, then an exception is thrown. Otherwise, it attempts to convert as much of the
   * token as possible to a positive double.
   *
   * @param tokenToParse the token to parse
   * @param expectedSyntax the expected syntax of the line the tokenToParse variable was extracted from
   * @param fieldBeingParsed the current field of the line expected syntax being parsed
   * @param units the units of the ndf file currently being parsed
   * @author George A. David
   */
  protected double parsePositiveDouble(String tokenToParse, String expectedSyntax, String fieldBeingParsed, MathUtilEnum units) throws DatastoreException
  {
    Assert.expect(tokenToParse != null);
    Assert.expect(expectedSyntax != null);
    Assert.expect(fieldBeingParsed != null);
    Assert.expect(units != null);

    double doubleValue = 0.0;

    Exception ex = null;
    try
    {
      doubleValue = StringUtil.convertStringToPositiveDouble(tokenToParse);
    }
    catch(BadFormatException bfe)
    {
      ex = bfe;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        ExpectedPositiveNumberDatastoreException dex = new ExpectedPositiveNumberDatastoreException(_currentFile,
                                                                                                    _is.getLineNumber(),
                                                                                                    expectedSyntax,
                                                                                                    fieldBeingParsed,
                                                                                                    tokenToParse);
        dex.initCause(ex);
        throw dex;
      }
      else
      {
        // the legacy compiler used the c++ atof() function, which converted as
        // much of the string as possible to a float, and that is what we will do here.
        ex = null;
        try
        {
          doubleValue = StringUtil.legacyConvertStringToPositiveDouble(tokenToParse);
        }
        catch(BadFormatException anotherBfe)
        {
          ex = anotherBfe;
        }
        catch (ValueOutOfRangeException vex)
        {
          ex = vex;
        }
        if (ex != null)
        {
          // the legacy compiler used the c++ built-in function atof() to convert
          // a string to an integer. if the string did not begin with a positive or
          // negative integer, it would return 0.0
          doubleValue = 0.0;
        }
        // warn the user that we expected a number
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      expectedSyntax,
                                      fieldBeingParsed,
                                      tokenToParse,
                                      new Double(doubleValue),
                                      units};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NUMBER_UNITS_KEY", args));
      }
    }

    return doubleValue;
  }

  /**
   * The function first attempts to convert the entire token to a double. If it fails, and the ndfs are
   * well formed, then an exception is thrown. Otherwise, it attempts to convert as much of the
   * token as possible to a double.
   *
   * @param tokenToParse the token to parse
   * @param expectedSyntax the expected syntax of the line the tokenToParse variable was extracted from
   * @param fieldBeingParsed the current field of the line expected syntax being parsed
   * @author George A. David
   */
  protected double parseDouble(String tokenToParse, String expectedSyntax, String fieldBeingParsed) throws DatastoreException
  {
    Assert.expect(tokenToParse != null);
    Assert.expect(expectedSyntax != null);
    Assert.expect(fieldBeingParsed != null);


    double doubleValue = 0.0;

    try
    {
      doubleValue = StringUtil.convertStringToDouble(tokenToParse);
    }
    catch(BadFormatException bfe)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        ExpectedNumberDatastoreException dex = new ExpectedNumberDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    expectedSyntax,
                                                                                    fieldBeingParsed,
                                                                                    tokenToParse);
        dex.initCause(bfe);
        throw dex;
      }
      else
      {
        // the legacy compiler used the c++ atof() function, which converted as
        // much of the string as possible to a float, and that is what we will do here.
        try
        {
          doubleValue = StringUtil.legacyConvertStringToDouble(tokenToParse);
        }
        catch(BadFormatException anotherBfe)
        {
          // the legacy compiler used the c++ built-in function atof() to convert
          // a string to an integer. if the string did not begin with a positive or
          // negative integer, it would return 0.0
          doubleValue = 0.0;
        }
        // warn the user that we expected a number
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      expectedSyntax,
                                      fieldBeingParsed,
                                      tokenToParse,
                                      new Double(doubleValue)};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_NUMBER_KEY", args));
      }
    }

    return doubleValue;
  }

  /**
   * The function first attempts to convert the entire token to a double. If it fails, and the ndfs are
   * well formed, then an exception is thrown. Otherwise, it attempts to convert as much of the
   * token as possible to a double. If the value is not within the specified range, then an
   * exception is thrown.
   *
   * @param tokenToParse the token to parse
   * @param expectedSyntax the expected syntax of the line the tokenToParse variable was extracted from
   * @param fieldBeingParsed the current field of the line expected syntax being parsed
   * @param minValue the minimum value allowed for the current filed being parsed
   * @param maxValue the maximum value allowed for the current filed being parsed
   * @author George A. David
   */
  protected double parseDouble(String tokenToParse, String expectedSyntax, String fieldBeingParsed, double minValue, double maxValue) throws DatastoreException
  {
    Assert.expect(tokenToParse != null);
    Assert.expect(expectedSyntax != null);
    Assert.expect(fieldBeingParsed != null);
    Assert.expect(minValue <= maxValue);


    double doubleValue = 0.0;

    try
    {
      doubleValue = StringUtil.convertStringToDouble(tokenToParse);
    }
    catch(BadFormatException bfe)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        ExpectedNumberDatastoreException dex = new ExpectedNumberDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    expectedSyntax,
                                                                                    fieldBeingParsed,
                                                                                    tokenToParse);
        dex.initCause(bfe);
        throw dex;
      }
      else
      {
        // the legacy compiler used the c++ atof() function, which converted as
        // much of the string as possible to a float, and that is what we will do here.
        try
        {
          doubleValue = StringUtil.legacyConvertStringToDouble(tokenToParse);
        }
        catch(BadFormatException anotherBfe)
        {
          // the legacy compiler used the c++ built-in function atof() to convert
          // a string to an integer. If the string was not an integer, than 0 was
          // returned. We will use the minimum value.
          doubleValue = minValue;
        }
        // warn the user that we expected a number
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      expectedSyntax,
                                      fieldBeingParsed,
                                      tokenToParse,
                                      new Double(doubleValue)};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_NUMBER_KEY", args));
      }
    }

    // check to see if it's in the range
    if (doubleValue > maxValue || doubleValue < minValue)
    {
      throw new ValueOutOfRangeDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  expectedSyntax,
                                                  fieldBeingParsed,
                                                  doubleValue,
                                                  minValue,
                                                  maxValue);
    }

    return doubleValue;
  }

  /**
   * The function first attempts to convert the entire token to a double. If it fails, and the ndfs are
   * well formed, then an exception is thrown. Otherwise, it attempts to convert as much of the
   * token as possible to a double.
   *
   * @param tokenToParse the token to parse
   * @param expectedSyntax the expected syntax of the line the tokenToParse variable was extracted from
   * @param fieldBeingParsed the current field of the line expected syntax being parsed
   * @param units the units of the ndf file being parsed
   * @author George A. David
   */
  protected double parseDouble(String tokenToParse, String expectedSyntax, String fieldBeingParsed, MathUtilEnum units) throws DatastoreException
  {
    Assert.expect(tokenToParse != null);
    Assert.expect(expectedSyntax != null);
    Assert.expect(fieldBeingParsed != null);
    Assert.expect(units != null);


    double doubleValue = 0.0;

    try
    {
      doubleValue = StringUtil.convertStringToDouble(tokenToParse);
    }
    catch(BadFormatException bfe)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        ExpectedNumberDatastoreException dex = new ExpectedNumberDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    expectedSyntax,
                                                                                    fieldBeingParsed,
                                                                                    tokenToParse);
        dex.initCause(bfe);
        throw dex;
      }
      else
      {
        // the legacy compiler used the c++ atof() function, which converted as
        // much of the string as possible to a float, and that is what we will do here.
        try
        {
          doubleValue = StringUtil.legacyConvertStringToDouble(tokenToParse);
        }
        catch(BadFormatException anotherBfe)
        {
          // the legacy compiler used the c++ built-in function atof() to convert
          // a string to an integer. if the string did not begin with a positive or
          // negative integer, it would return 0.0
          doubleValue = 0.0;
        }
        // warn the user that we expected a number
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      expectedSyntax,
                                      fieldBeingParsed,
                                      tokenToParse,
                                      new Double(doubleValue),
                                      units};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_NUMBER_UNITS_KEY", args));
      }
    }

    return doubleValue;
  }

  /**
   * The fucntion first attempts to convert the entire token to a positive integer. If it fails, and the ndfs are
   * well formed, then an exception is thrown. Otherwise, it attempts to convert as much of the
   * token as possible to a positive integer.
   *
   * @param tokenToParse the token to parse
   * @param expectedSyntax the expected syntax of the line the tokenToParse variable was extracted from
   * @param fieldBeingParsed the current field of the line expected syntax being parsed
   * @author George A. David
   */
  protected int parsePositiveInt(String tokenToParse, String expectedSyntax, String fieldBeingParsed) throws DatastoreException
  {
    Assert.expect(tokenToParse != null);
    Assert.expect(expectedSyntax != null);
    Assert.expect(fieldBeingParsed != null);

    int intValue = 0;

    Exception ex = null;
    try
    {
      intValue = StringUtil.convertStringToPositiveStrictInt(tokenToParse);
    }
    catch(BadFormatException bfe)
    {
      ex = bfe;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        ExpectedPositiveWholeNumberDatastoreException dex = new ExpectedPositiveWholeNumberDatastoreException(_currentFile,
                                                                                                              _is.getLineNumber(),
                                                                                                              expectedSyntax,
                                                                                                              fieldBeingParsed,
                                                                                                              tokenToParse);
        dex.initCause(ex);
        throw dex;
      }
      else
      {
        // the legacy compiler used the c++ atoi() function, which converted as
        // much of the string as possible to a float, and that is what we will do here.
        ex = null;
        try
        {
          intValue = StringUtil.legacyConvertStringToPositiveInt(tokenToParse);
        }
        catch(BadFormatException anotherBfe)
        {
          ex = anotherBfe;
        }
        catch (ValueOutOfRangeException vex)
        {
          ex = vex;
        }
        if (ex != null)
        {
          // the legacy compiler used the c++ built-in function atoi() to convert
          // a string to an integer. if the string did not begin with a positive or
          // negative integer, it would return 0
          intValue = 0;
        }
        // warn the user that we expected a number
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      expectedSyntax,
                                      fieldBeingParsed,
                                      tokenToParse,
                                      new Double(intValue)};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_WHOLE_NUMBER_KEY", args));
      }
    }

    return intValue;
  }

  /**
   * The function first attempts to convert the entire token to an integer. If it fails, and the ndfs are
   * well formed, then an exception is thrown. Otherwise, it attempts to convert as much of the
   * token as possible to an integer.
   *
   * @param tokenToParse the token to parse
   * @param expectedSyntax the expected syntax of the line the tokenToParse variable was extracted from
   * @param fieldBeingParsed the current field of the line expected syntax being parsed
   * @author George A. David
   */
  protected int parseInt(String tokenToParse, String expectedSyntax, String fieldBeingParsed) throws DatastoreException
  {
    Assert.expect(tokenToParse != null);
    Assert.expect(expectedSyntax != null);
    Assert.expect(fieldBeingParsed != null);


    int intValue = 0;

    try
    {
      intValue = StringUtil.convertStringToStrictInt(tokenToParse);
    }
    catch(BadFormatException bfe)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        ExpectedWholeNumberDatastoreException dex = new ExpectedWholeNumberDatastoreException(_currentFile,
                                                                                              _is.getLineNumber(),
                                                                                              expectedSyntax,
                                                                                              fieldBeingParsed,
                                                                                              tokenToParse);
        dex.initCause(bfe);
        throw dex;
      }
      else
      {
        // the legacy compiler used the c++ atoi() function, which converted as
        // much of the string as possible to a float, and that is what we will do here.
        try
        {
          intValue = StringUtil.legacyConvertStringToInt(tokenToParse);
        }
        catch(BadFormatException anotherBfe)
        {
          // the legacy compiler used the c++ built-in function atoi() to convert
          // a string to an integer. if the string did not begin with a positive or
          // negative integer, it would return 0
          intValue = 0;
        }
        // warn the user that we expected a number
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      expectedSyntax,
                                      fieldBeingParsed,
                                      tokenToParse,
                                      new Double(intValue)};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_WHOLE_NUMBER_KEY", args));
      }
    }

    return intValue;
  }

  /**
   * The fucntion first attempts to convert the entire token to an integer. If it fails, and the ndfs are
   * well formed, then an exception is thrown. Otherwise, it attempts to convert as much of the
   * token as possible to an integer. If the integer is not within a specified range, then an
   * exception is thrown.
   *
   * @param tokenToParse the token to parse
   * @param expectedSyntax the expected syntax of the line the tokenToParse variable was extracted from
   * @param fieldBeingParsed the current field of the line expected syntax being parsed
   * @param minValue the minimum value of the current field being parsed
   * @param maxValue the maximum value of the current field being parsed
   * @author George A. David
   */
  protected int parseInt(String tokenToParse, String expectedSyntax, String fieldBeingParsed, int minValue, int maxValue) throws DatastoreException
  {
    Assert.expect(tokenToParse != null);
    Assert.expect(expectedSyntax != null);
    Assert.expect(fieldBeingParsed != null);
    Assert.expect(minValue <= maxValue);

    int intValue = 0;
    boolean validInt = true;

    try
    {
      intValue = StringUtil.convertStringToStrictInt(tokenToParse);
    }
    catch(BadFormatException bfe)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        ExpectedWholeNumberDatastoreException dex = new ExpectedWholeNumberDatastoreException(_currentFile,
                                                                                              _is.getLineNumber(),
                                                                                              expectedSyntax,
                                                                                              fieldBeingParsed,
                                                                                              tokenToParse);
        dex.initCause(bfe);
        throw dex;
      }
      else
      {
        // the legacy compiler used the c++ atoi() function, which converted as
        // much of the string as possible to a float, and that is what we will do here.
        try
        {
          intValue = StringUtil.legacyConvertStringToInt(tokenToParse);
        }
        catch(BadFormatException anotherBfe)
        {
          // the legacy compiler used the c++ built-in function atoi() to convert
          // a string to an integer. if the string did not begin with a positive or
          // negative integer, it would return 0
          intValue = 0;
        }
        // warn the user that we expected a number
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      expectedSyntax,
                                      fieldBeingParsed,
                                      tokenToParse,
                                      new Double(intValue)};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_WHOLE_NUMBER_KEY", args));
        validInt = false;
      }
    }

    if (validInt)
    {
      // check to see if it's in the range
      if (intValue > maxValue || intValue < minValue)
      {
        throw new ValueOutOfRangeDatastoreException(_currentFile,
                                                    _is.getLineNumber(),
                                                    expectedSyntax,
                                                    fieldBeingParsed,
                                                    intValue,
                                                    minValue,
                                                    maxValue);
      }
    }
    return intValue;
  }

  /**
   * The function first attempts to convert the entire token to a positive integer. If it fails, and the ndfs are
   * well formed, then an exception is thrown. Otherwise, it attempts to convert as much of the
   * token as possible to an integer.
   *
   * @param tokenToParse the token to parse
   * @param expectedSyntax the expected syntax of the line the tokenToParse variable was extracted from
   * @param fieldBeingParsed the current field of the line expected syntax being parsed
   * @author George A. David
   */
  protected int parsePositiveNonZeroInt(String tokenToParse, String expectedSyntax, String fieldBeingParsed) throws DatastoreException
  {
    Assert.expect(tokenToParse != null);
    Assert.expect(expectedSyntax != null);
    Assert.expect(fieldBeingParsed != null);

    int integer = 1;

    Exception ex = null;
    try
    {
      integer = StringUtil.convertStringToPositiveStrictInt(tokenToParse);
      if (integer == 0)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new ExpectedPositiveNonZeroWholeNumberDatastoreException(_currentFile,
                                                                         _is.getLineNumber(),
                                                                         expectedSyntax,
                                                                         fieldBeingParsed,
                                                                         tokenToParse);
        }
        else
        {
          //warn the user that we expected a positive non zero number
          // and set the value to 1.
          integer = 1;
          Object[] args = new Object[]{ _currentFile,
                                        new Integer(_is.getLineNumber()),
                                        expectedSyntax,
                                        fieldBeingParsed,
                                        tokenToParse,
                                        new Integer(integer)};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_WHOLE_NUMBER_KEY", args));
        }
      }
    }
    catch(BadFormatException bfe)
    {
      ex = bfe;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        ExpectedPositiveNonZeroWholeNumberDatastoreException dex = new ExpectedPositiveNonZeroWholeNumberDatastoreException(_currentFile,
                                                                                                                            _is.getLineNumber(),
                                                                                                                            expectedSyntax,
                                                                                                                            fieldBeingParsed,
                                                                                                                            tokenToParse);
        dex.initCause(ex);
        throw dex;
      }
      else
      {
        // the legacy compiler used the c++ atoi() function, which converted as
        // much of the string as possible to a float, and that is what we will do here.
        ex = null;
        try
        {
          integer = StringUtil.legacyConvertStringToPositiveInt(tokenToParse);
          if (integer == 0)
          {
            integer = 1;
          }
        }
        catch(BadFormatException anotherBfe)
        {
          ex = anotherBfe;
        }
        catch (ValueOutOfRangeException vex)
        {
          ex = vex;
        }
        if (ex != null)
        {
          // the legacy compiler used the c++ built-in function atoi() to convert
          // a string to an integer. if the string did not begin with a positive or
          // negative integer.
          integer = 1;
        }
        // warn the user that we expected a positive non zero number
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      expectedSyntax,
                                      fieldBeingParsed,
                                      tokenToParse,
                                      new Integer(integer)};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_WHOLE_NUMBER_KEY", args));
      }
    }


    return integer;
  }

  /**
   * The function first attempts to convert the entire token to a positive integer. If it fails, and the ndfs are
   * well formed, then an exception is thrown. Otherwise, it attempts to convert as much of the
   * token as possible to an integer.
   *
   * @param tokenToParse the token to parse
   * @param expectedSyntax the expected syntax of the line the tokenToParse variable was extracted from
   * @param fieldBeingParsed the current field of the line expected syntax being parsed
   * @param units the units of the ndf currently being parsed
   * @author George A. David
   */
  protected int parsePositiveNonZeroInt(String tokenToParse, String expectedSyntax, String fieldBeingParsed, MathUtilEnum units) throws DatastoreException
  {
    Assert.expect(tokenToParse != null);
    Assert.expect(expectedSyntax != null);
    Assert.expect(fieldBeingParsed != null);
    Assert.expect(units != null);

    int integer = 1;

    Exception ex = null;
    try
    {
      integer = StringUtil.convertStringToPositiveStrictInt(tokenToParse);
      if (integer == 0)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new ExpectedPositiveNonZeroWholeNumberDatastoreException(_currentFile,
                                                                         _is.getLineNumber(),
                                                                         expectedSyntax,
                                                                         fieldBeingParsed,
                                                                         tokenToParse);
        }
        else
        {
          //warn the user that we expected a positive non zero number
          //and we will use a value of 1 mil
          double value = 1.0;
          Object[] args = new Object[]{ _currentFile,
                                        new Integer(_is.getLineNumber()),
                                        expectedSyntax,
                                        fieldBeingParsed,
                                        tokenToParse,
                                        new Double(value),
                                        MathUtilEnum.MILS};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_WHOLE_NUMBER_UNITS_KEY", args));
          value = MathUtil.convertUnits(value, MathUtilEnum.MILS, units);
          try
          {
            integer = MathUtil.convertDoubleToInt(value);
          }
          catch(ValueOutOfRangeException voore)
          {
            throw new ValueOutOfRangeDatastoreException(_currentFile,
                                                        _is.getLineNumber(),
                                                        expectedSyntax,
                                                        fieldBeingParsed,
                                                        voore);
          }
        }
      }
    }
    catch(BadFormatException bfe)
    {
      ex = bfe;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        ExpectedPositiveNonZeroWholeNumberDatastoreException dex = new ExpectedPositiveNonZeroWholeNumberDatastoreException(_currentFile,
                                                                                                                            _is.getLineNumber(),
                                                                                                                            expectedSyntax,
                                                                                                                            fieldBeingParsed,
                                                                                                                            tokenToParse);
        dex.initCause(ex);
        throw dex;
      }
      else
      {
        MathUtilEnum unitsUsed = units;
        int valueUsed = 0;
        // the legacy compiler used the c++ atoi() function, which converted as
        // much of the string as possible to a float, and that is what we will do here.
        ex = null;
        try
        {
          integer = StringUtil.legacyConvertStringToPositiveInt(tokenToParse);
          valueUsed = integer;
          if (integer == 0)
          {
            // a value of zero is not allowed, use a value of 1 mil
            double value = MathUtil.convertUnits(1.0, MathUtilEnum.MILS, units);
            try
            {
              integer = MathUtil.convertDoubleToInt(value);
            }
            catch(ValueOutOfRangeException voore)
            {
              throw new ValueOutOfRangeDatastoreException(_currentFile,
                                                          _is.getLineNumber(),
                                                          expectedSyntax,
                                                          fieldBeingParsed,
                                                          voore);
            }
            valueUsed = 1;
            unitsUsed = MathUtilEnum.MILS;
          }
        }
        catch(BadFormatException anotherBfe)
        {
          ex = anotherBfe;
        }
        catch (ValueOutOfRangeException vex)
        {
          ex = vex;
        }
        if (ex != null)
        {
          // the legacy compiler used the c++ built-in function atoi() to convert
          // a string to an integer. if the string did not begin with a positive or
          // negative integer, the compiler would use zero, however, a value of
          // zero is not allowed, use a value of 1 mil
          double value = MathUtil.convertUnits(1.0, MathUtilEnum.MILS, units);
          try
          {
            integer = MathUtil.convertDoubleToInt(value);
          }
          catch(ValueOutOfRangeException voore)
          {
            throw new ValueOutOfRangeDatastoreException(_currentFile,
                                                        _is.getLineNumber(),
                                                        expectedSyntax,
                                                        fieldBeingParsed,
                                                        voore);
          }
          valueUsed = 1;
          unitsUsed = MathUtilEnum.MILS;
        }
        // warn the user that we expected a positive non zero number
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      expectedSyntax,
                                      fieldBeingParsed,
                                      tokenToParse,
                                      new Integer(valueUsed),
                                      unitsUsed};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_WHOLE_NUMBER_UNITS_KEY", args));
      }
    }


    return integer;
  }

  /**
   * @author Bill Darbie
   */
  protected void checkForExtraDataAtEndOfLine(StringTokenizer tokenizer, String expectedSyntax) throws DatastoreException
  {
    Assert.expect(tokenizer != null);
    Assert.expect(expectedSyntax != null);

    String restOfLine = ParseUtil.getNextToken(tokenizer, _NO_DELIMITERS);
    if (restOfLine != null)
    {
      if (isValidComment(restOfLine) == false)
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new ExpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  expectedSyntax,
                                                  restOfLine);
        else
        {
          // warn that extra data on this line is going to be ignored
          Object[] args = new Object[]{_currentFile,
                                       new Integer(_is.getLineNumber()),
                                       expectedSyntax,
                                       restOfLine};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_EOL_KEY", args));
        }
      }
    }
  }

  /**
   * @author Jack Hwee
   */
  public static void set5dxProjectFlag (boolean flag)
  {
      _is5dxProject = flag;
  }

  /**
   * @author Jack Hwee
   */
  public static boolean is5dxProjectFlag ()
  {
      return _is5dxProject;
  }

   /**
   * @author Jack Hwee
   */
  public static void set5dxNdfFlag (boolean flag)
  {
      _is5dxNdf = flag;
  }

  /**
   * @author Jack Hwee
   */
  public static boolean is5dxNdfFlag ()
  {
      return _is5dxNdf;
  }

   /**
   *   XCR1163
   * @author Jack Hwee
   *  fix to load single-sided panel ndf.
   */
  public void setValidRequiredDataLineForNoBoard (boolean flag)
  {
      _validRequiredDataLineForNoBoard = flag;
  }

   /**
   * @author Jack Hwee
   */
  public boolean isValidRequiredDataLineForNoBoard ()
  {
      return _validRequiredDataLineForNoBoard;
  }

  /**
   * @author Cheah Lee Herng
   */
  public static void set5dxProjectNameTo5dxHashNameMap(Map<String, String> fivedxProjectNameTo5dxHashNameMap)
  {
      Assert.expect(fivedxProjectNameTo5dxHashNameMap != null);
      Assert.expect(_5dxProjectNameTo5dxHashNameMap != null);
      
      _5dxProjectNameTo5dxHashNameMap.clear();
      _5dxProjectNameTo5dxHashNameMap = fivedxProjectNameTo5dxHashNameMap;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String get5dxHashName(String fivedxProjectName)
  {
      Assert.expect(fivedxProjectName != null);
      Assert.expect(_5dxProjectNameTo5dxHashNameMap != null);
      
      String fivedxHashName = null;
      if (_5dxProjectNameTo5dxHashNameMap.containsKey(fivedxProjectName))
      {
          fivedxHashName = _5dxProjectNameTo5dxHashNameMap.get(fivedxProjectName);
      }
      return fivedxHashName;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static void clear5dxProjectNamesToProjectHashNameMap()
  {
      if (_5dxProjectNameTo5dxHashNameMap != null)
      {
          _5dxProjectNameTo5dxHashNameMap.clear();
      }
  }

 /**
  * @author Bee Hoon
  */  
  public static void set5dxNdfWithDiffSubtype(char isImport5dxNdfWithDiffSubtype)
  {
    _5dxNdfConversionMethod = isImport5dxNdfWithDiffSubtype;
  }

 /**
  * @author Bee Hoon
  */    
  public static char get5dxNdfWithDiffSubtype()
  {
    return _5dxNdfConversionMethod;
  }
}
