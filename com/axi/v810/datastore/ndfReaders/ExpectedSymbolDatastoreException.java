package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when a symbol (like @) is not found where it was expected
 * @author Bill Darbie
 */
public class ExpectedSymbolDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public ExpectedSymbolDatastoreException(String fileName, String symbol)
  {
    super(new LocalizedString("DS_ERROR_EXPECTED_SYMBOL_KEY", new Object[]{fileName, symbol}));
    Assert.expect(fileName != null);
    Assert.expect(symbol != null);
  }
}
