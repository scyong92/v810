package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * Throw this exception when a component that is not set to be inspected is referenced
 * in some other file.  In most cases the component must be set to be tested if it is referenced
 * in other places.
 * @author Bill Darbie
 */
public class ComponentReferencedButNotInspectedDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public ComponentReferencedButNotInspectedDatastoreException(String fileName,
                                                              int lineNumber,
                                                              String referenceDesignator,
                                                              String boardFileName,
                                                              String componentFileName)

  {
    super(new LocalizedString("DS_ERROR_COMPONENT_REFERENCED_THAT_IS_NOT_BEING_INSPECTED_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           referenceDesignator,
                                           boardFileName,
                                           componentFileName}));

    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(referenceDesignator != null);
    Assert.expect(boardFileName != null);
    Assert.expect(componentFileName != null);
  }
}
