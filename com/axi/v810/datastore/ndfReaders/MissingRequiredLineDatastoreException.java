package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when a required line is missing from an ndf file
 * @author Bill Darbie
 */
public class MissingRequiredLineDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public MissingRequiredLineDatastoreException(String fileName, String expectedSyntax)
  {
    super(new LocalizedString("DS_NDF_ERROR_MISSING_REQUIRED_LINE_KEY",
                              new Object[]{fileName,
                                           expectedSyntax}));
    Assert.expect(fileName != null);
    Assert.expect(expectedSyntax != null);
  }
}
