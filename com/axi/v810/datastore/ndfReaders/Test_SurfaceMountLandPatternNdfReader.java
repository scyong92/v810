package com.axi.v810.datastore.ndfReaders;

import java.io.*;

import com.axi.util.*;

/**
 * @author Jeff Ryer
 */
class Test_SurfaceMountLandPatternNdfReader extends UnitTest
{
  /**
   * @author Jeff Ryer
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_SurfaceMountLandPatternNdfReader());
  }

  /**
   * @author Jeff Ryer
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String panelName = "smallPanel";
    String landPatternNdf = "landpat.ndf";
    String dataDir = getTestDataDir();
    String landPatternNdfsDataDir = dataDir + File.separator + "landPatternNdfs";
    String[] cornerCaseFiles = new String[]{"good.ndf",
                                            "extraSpaces.ndf",
                                            "extraData.ndf",
                                            "empty.ndf",
                                            "noUnit.ndf",
                                            "duplicateUnits.ndf",
                                            "duplicateChecksum.ndf",
                                            "unknownAttribute.ndf",
                                            "comment.ndf",
                                            "garbageLine.ndf",
                                            "missingLandPatternName.ndf",
                                            "missingPadNumber.ndf",
                                            "negativePadNumber.ndf",
                                            "duplicatePadNumber.ndf",
//                                            "padNumberIsDoubleValue.ndf",
//                                            "padNumberStartsWithLetter.ndf",
//                                            "padNumberEndsWithLetter.ndf",
                                            "missingPadShape.ndf",
                                            "badPadShape.ndf",
                                            "missingXPadCoordinate.ndf",
                                            "xPadCoordinateBeginsWithLetter.ndf",
                                            "xPadCoordinateEndsWithLetter.ndf",
                                            "xPadCoordinateContainsTwoDecimalPoints.ndf",
                                            "missingYPadCoordinate.ndf",
                                            "yPadCoordinateBeginsWithLetter.ndf",
                                            "yPadCoordinateEndsWithLetter.ndf",
                                            "yPadCoordinateContainsTwoDecimalPoints.ndf",
                                            "missingPadWidth.ndf",
                                            "negativePadWidth.ndf",
                                            "padWidthBeginsWithLetter.ndf",
                                            "padWidthEndsWithLetter.ndf",
                                            "padWidthContainsTwoDecimalPoints.ndf",
                                            "zeroPadWidth.ndf",
                                            "missingPadLength.ndf",
                                            "zeroPadLength.ndf",
                                            "negativePadLength.ndf",
                                            "padLengthBeginsWithLetter.ndf",
                                            "padLengthEndsWithLetter.ndf",
                                            "padLengthContainsTwoDecimalPoints.ndf",
                                            "extraPad.ndf"};

    Test_ReaderUtil.testParser(panelName, landPatternNdf, landPatternNdfsDataDir, cornerCaseFiles);
 }
}

