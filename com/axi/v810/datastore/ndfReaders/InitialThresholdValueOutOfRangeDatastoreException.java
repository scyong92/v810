package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * @author George A. David
 */
public class InitialThresholdValueOutOfRangeDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public InitialThresholdValueOutOfRangeDatastoreException(String thresholdFile, int lineNumber, String algorithmFamilyName, String algorithmName, String thresholdName, Object thresholdValue, Object minValue, Object maxValue)
  {
    super(new LocalizedString("DS_ERROR_INITIAL_THRESHOLD_VALUE_OUT_OF_RANGE_KEY",
          new Object[]{thresholdFile, new Integer(lineNumber), algorithmFamilyName, algorithmName, thresholdName, thresholdValue, minValue, maxValue}));
    Assert.expect(thresholdFile != null);
    Assert.expect(algorithmFamilyName != null);
    Assert.expect(algorithmName != null);
    Assert.expect(thresholdName != null);
    Assert.expect(thresholdValue != null);
    Assert.expect(minValue != null);
    Assert.expect(maxValue != null);
  }
}
