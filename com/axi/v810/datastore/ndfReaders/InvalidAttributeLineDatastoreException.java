package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown if the board thickness of the top and
 * bottom boards do not match.
 * @author Keith Lee
 */
public class InvalidAttributeLineDatastoreException extends DatastoreException
{
 /**
  * @param fileName - the full path to the file
  * @param lineNumber - the line number where the invalid attriubte line is found
  * @param attributeLineIdentifier - the attribute line identifier (i.e. .MINIMUM_SNAP_MARGIN:)
  * @author Keith Lee
  */
  public InvalidAttributeLineDatastoreException(String fileName, int lineNumber, String attributeLineIdentifier)
  {
    //The {2} identifier is no longer used. Please remove line {1} in {0}.
    super(new LocalizedString("DS_ERROR_READ_NDF_INVALID_ATTRIBUTE_LINE_KEY",
          new Object[]{ fileName, new Integer(lineNumber), attributeLineIdentifier }));
    Assert.expect(fileName != null);
    Assert.expect(attributeLineIdentifier != null);
  }

}
