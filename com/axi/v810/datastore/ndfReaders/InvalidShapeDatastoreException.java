package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when we find something besides a circle or rectangle for a shape
 * @author Bill Darbie
 */
public class InvalidShapeDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public InvalidShapeDatastoreException(String fileName,
                                        int lineNumber,
                                        String expectedSyntax,
                                        String invalidItem,
                                        String actualValue)
  {
    super(new LocalizedString("DS_NDF_ERROR_INVALID_SHAPE_KEY",
          new Object[]{fileName,
                       new Integer(lineNumber),
                       expectedSyntax,
                       invalidItem,
                       actualValue}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidItem != null);
    Assert.expect(actualValue != null);
  }
}
