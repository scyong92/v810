package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * If the package name is invalid, this exception should be thrown.
 * @author Bill Darbie
 */
public class InvalidPackageNameDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public InvalidPackageNameDatastoreException(String fileName,
                                              int lineNumber,
                                              String badPackageName,
                                              String packageFileName)

  {
    super(new LocalizedString("DS_NDF_ERROR_INVALID_PACKAGE_NAME_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           badPackageName,
                                           packageFileName}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(badPackageName != null);
    Assert.expect(packageFileName != null);
  }
}
