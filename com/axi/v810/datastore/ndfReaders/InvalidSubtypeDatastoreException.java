package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * If an invalid subtype is specified this exception should be thrown.
 * @author Bill Darbie
 */
public class InvalidSubtypeDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public InvalidSubtypeDatastoreException(String fileName,
                                          int lineNumber,
                                          String subTypeNumber)

  {
    super(new LocalizedString("DS_ERROR_INVALID_SUBTYPE_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           subTypeNumber}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(subTypeNumber != null);
  }
}
