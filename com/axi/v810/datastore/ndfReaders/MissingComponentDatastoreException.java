package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class is thrown when some component is specified in the component ndf file,
 * but not the board ndf file.
 *
 * @author George A. David
 */
public class MissingComponentDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public MissingComponentDatastoreException(String boardFileName,
                                            String componentFileName,
                                            String expectedSyntax,
                                            String referenceDesignator)

  {
    super(new LocalizedString("DS_NDF_ERROR_MISSING_COMPONENT_KEY",
          new Object[]{boardFileName,
                       componentFileName,
                       expectedSyntax,
                       referenceDesignator}));

    Assert.expect(boardFileName != null);
    Assert.expect(componentFileName != null);
    Assert.expect(expectedSyntax != null);
    Assert.expect(referenceDesignator != null);
  }
}
