package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when an alignment view specifies an invalid pin.
 * @author Bill Darbie
 */
public class InvalidAlignmentPinDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public InvalidAlignmentPinDatastoreException(String fileName,
                                               int lineNumber,
                                               String referenceDesignator,
                                               String pinName,
                                               String packageName,
                                               String packageFileName)
  {
    super(new LocalizedString("DS_ERROR_ALIGNMENT_VIEW_INVALID_PIN_KEY",
          new Object[]{fileName,
                       new Integer(lineNumber),
                       referenceDesignator,
                       pinName,
                       packageName,
                       packageFileName}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(referenceDesignator != null);
    Assert.expect(pinName != null);
    Assert.expect(packageName != null);
    Assert.expect(packageFileName != null);
  }
}
