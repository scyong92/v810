package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * Throw this exception when a component is referenced in some file was not set up in the board.ndf
 * and the component.ndf files as it needs to be.
 * @author Bill Darbie
 */
public class BadComponentReferenceDatastoreException extends DatastoreException
{
  /**
   * Use this contructor when you have a line number available.
   * @author Bill Darbie
   */
  public BadComponentReferenceDatastoreException(String fileName,
                                                 int lineNumber,
                                                 String expectedSyntax,
                                                 String invalidField,
                                                 String referenceDesignator,
                                                 String boardFileName,
                                                 String componentFileName)

  {
    super(new LocalizedString("DS_NDF_ERROR_BAD_COMPONENT_REFERENCE_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           expectedSyntax,
                                           invalidField,
                                           referenceDesignator,
                                           boardFileName,
                                           componentFileName}));

    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidField != null);
    Assert.expect(referenceDesignator != null);
    Assert.expect(boardFileName != null);
    Assert.expect(componentFileName != null);
  }
}
