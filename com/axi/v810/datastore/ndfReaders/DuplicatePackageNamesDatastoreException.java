package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import java.io.*;

import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * Throw this exception if the same package has been defined more than once.
 * @author Bill Darbie
 */
public class DuplicatePackageNamesDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public DuplicatePackageNamesDatastoreException(String fileName,
                                                 int lineNumber,
                                                 String packageName)
  {
    super(new LocalizedString("DS_ERROR_DUPLICATE_PACKAGE_KEY",
                              new Object[]{ fileName, new Integer(lineNumber), packageName}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(packageName != null);
  }
}
