package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when a line starst with an unexpected character
 * @author George A. David
 */
public class InvalidStartOfLineDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public InvalidStartOfLineDatastoreException(String fileName,
                                              int lineNumber,
                                              String expectedSymbols,
                                              String actualSymbol)
  {
    super(new LocalizedString("DS_NDF_ERROR_INVALID_START_OF_LINE_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           expectedSymbols,
                                           actualSymbol}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSymbols != null);
    Assert.expect(actualSymbol != null);
  }
}
