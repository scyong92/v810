package com.axi.v810.datastore.ndfReaders;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class should be used by all NdfReader test classes to test that the readers are
 * working properly.
 *
 * @author Bill Darbie
 */
class Test_ReaderUtil
{
  private static ProjectObservable _datastoreObservable = ProjectObservable.getInstance();

  /**
   * @author Bill Darbie
   */
  static void testParser(String projectName, String targetNdf, String cornerCaseNdfDataDir, String[] cornerCaseFiles)
  {
    Assert.expect(projectName != null);
    Assert.expect(cornerCaseNdfDataDir != null);
    Assert.expect(cornerCaseFiles != null);

    String ndfPanelDir = Directory.getLegacyNdfPanelDir(projectName);

    String strictPanelName = ndfPanelDir + File.separator + "panel_strict.ndf";
    String compatiblePanelName = ndfPanelDir + File.separator + "panel_compatible.ndf";
    String panelNdfName = ndfPanelDir + File.separator + "panel.ndf";
    String targetNdfFullPathName = ndfPanelDir + File.separator + targetNdf;

    boolean isTestingPanelNdf = false;
    if (targetNdf.equalsIgnoreCase("panel.ndf"))
      isTestingPanelNdf = true;

    try
    {
      // backup the target ndf file -- it should be the good one, so we can restore it at the end.
      FileUtil.backupFile(targetNdfFullPathName);

      // now try all error conditions (both strict and compatible)
      for(int i = 0; i < cornerCaseFiles.length; ++i)
      {
        String file = cornerCaseNdfDataDir + File.separator + cornerCaseFiles[i];
        if (isTestingPanelNdf)  // when testing panel.ndf, it's up to the test case to handle the strict/compatible stuff
        {
          deleteSerializedProjectFiles();
          System.out.println("\n#### Testing: " + UnitTest.stripOffViewName(file));
          FileUtil.copy(file, targetNdfFullPathName);
          loadProject(projectName);

          // if this is not true, some method in datastore did a _datastoreObservable.setEnabled(false)
          // without doing a cooresponding _datastoreObservable.setEnabled(true)
          Assert.expect(_datastoreObservable.isEnabled());

        }
        else  // if not testing panel.ndf, do this strict/compatible stuff
        {
          deleteSerializedProjectFiles();
          System.out.println("\n#### Testing in STRICT mode: " + UnitTest.stripOffViewName(file));
          FileUtil.copy(strictPanelName, panelNdfName);
          FileUtil.copy(file, targetNdfFullPathName);
          loadProject(projectName);

          deleteSerializedProjectFiles();
          System.out.println("#### Testing in COMPATIBLE mode: " + UnitTest.stripOffViewName(file));
          FileUtil.copy(compatiblePanelName, panelNdfName);
          loadProject(projectName);

          // if this is not true, some method in datastore did a _datastoreObservable.setEnabled(false)
          // without doing a cooresponding _datastoreObservable.setEnabled(true)
          Assert.expect(_datastoreObservable.isEnabled());
        }
      }
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      // restore the target ndf file
      try
      {
        FileUtilAxi.restoreBackupFile(targetNdfFullPathName);
      }
      catch (DatastoreException ex)
      {
        ex.printStackTrace();
      }
      deleteSerializedProjectFiles();
    }

    System.out.println("#### DONE testing");
  }

  /**
   * @author Bill Darbie
   */
  private static void deleteSerializedProjectFiles()
  {
    String projectsDir = Directory.getProjectsDir();

    File file = new File(projectsDir);
    String[] serializedFiles = file.list(new FilenameFilter()
    {
      public boolean accept(File dir, String name)
      {
        Assert.expect(dir != null);
        Assert.expect(name != null);

        String projectExt = FileName.getProjectFileExtension();
        if (name.endsWith(projectExt))
          return true;

        return false;
      }
    });

    for(int i = 0; i < serializedFiles.length; ++i)
    {
      try
      {
        FileUtil.delete(projectsDir + File.separator + serializedFiles[i]);
      }
      catch(CouldNotDeleteFileException ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private static void loadProject(String projectName)
  {
    Assert.expect(projectName != null);

    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    try
    {
      Project project = Project.importProjectFromNdfs(projectName, warnings);
      printWarnings(warnings);
    }
    catch(DatastoreException dex)
    {
      // now display warnings
      printWarnings(warnings);

      String message = dex.getLocalizedMessage();
      message = UnitTest.stripOffViewName(message);
      System.out.println("ERROR: " + message);

    }
    catch(AssertException ae)
    {
      if (ae.getLocalizedMessage().trim().length() == 0)
      {
        ae.printStackTrace();
      }
      else
      {
        System.out.println("ASSERT: " + ae.getLocalizedMessage());
        //wpd remove
//        ae.printStackTrace();
      }

      printWarnings(warnings);
    }
  }

  /**
   * @author Andy Mechtenberg
   */
  static void printWarnings(List<LocalizedString> warnings)
  {
    for (LocalizedString ls : warnings)
    {
      String message = StringLocalizer.keyToString(ls);
      message = UnitTest.stripOffViewName(message);
      System.out.println("WARNING: " + message);
    }
  }
}
