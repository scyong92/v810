package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This exception will be thrown if a land pattern is defined in fileName1 but not in fileName2.
 * @author Eugene Kim-Leighton
 */
public class BadLandPatternReferenceDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public BadLandPatternReferenceDatastoreException(String currentFileName,
                                                   int lineNumber,
                                                   String expectedSyntax,
                                                   String invalidField,
                                                   String landPatternName,
                                                   String landPatternFileName)
  {
    super(new LocalizedString("DS_NDF_ERROR_BAD_LAND_PATTERN_REFERENCE_KEY",
                              new Object[]{currentFileName,
                                           new Integer(lineNumber),
                                           expectedSyntax,
                                           invalidField,
                                           landPatternName,
                                           landPatternFileName}));

    Assert.expect(currentFileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidField != null);
    Assert.expect(landPatternName != null);
    Assert.expect(landPatternFileName != null);
  }

  /**
   * @author Eugene Kim-Leighton
   */
  public BadLandPatternReferenceDatastoreException(String fileName1, int lineNumber, String landPatternName, String fileName2)
  {
    super(new LocalizedString("DS_ERROR_BAD_LAND_PATTERN_REFERENCE_KEY",
                              new Object[]{fileName1,
                                           new Integer(lineNumber),
                                           landPatternName,
                                           fileName2}));

    Assert.expect(fileName1 != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(landPatternName != null);
    Assert.expect(fileName2 != null);
  }
}
