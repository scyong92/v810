package com.axi.v810.datastore.ndfReaders;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * Parses in the board.ndf file.
 *
 * This class creates the following Datastore objects:
 *   AlignmentGroup
 *     name
 *     Pads
 *
 *   Pad
 *     Component
 *     PadType
 *     id
 *
 *   PadSettings
 *     Pad
 *     inspected
 *
 *   PadType
 *     LandPatternPad
 *     PackagePin
 *     PadTypeSettings
 *     ComponentType
 *     Pad
 *     id
 *
 *   PadTypeSettings
 *     inspected
 *     PadType
 *
 *
 * This class sets the following variables:
 *
 *   ComponentType
 *     LandPattern
 *     CoordinateInNanoMeters
 *     degreesRotation
 *     PadType
 *
 *   SideBoard
 *     Components
 *
 *   SideBoardSettings
 *     AlignmentGroup
 *
 *   LandPattern
 *     ComponentTypes
 *
 *
 * This class requires the following to already be available to it from MainReader:
 *  ThroughHoleLandPattern  (from ThroughHoleLandPatternNdfReader)
 *  SurfaceMountLandPattern (from SurfaceMountLandPatternNdfReader)
 *  Panel name
 *  Component (from ComponentNdfReader)
 *
 * This class puts the following into MainReader:
 *   _mainReader.addSideBoardTypeToWidthInNanoMetersMap()
 *   _mainReader.addSideBoardTypeToLengthInNanoMetersMap()
 *
 * @author George A. David
 * @author Bill Darbie
 *
 */
class BoardNdfReader extends NdfReader
{
  private static final boolean _DONT_RETURN_DELIMITERS = false;
  private static final String _NO_DELIMITERS = "";
  private static final String _ALIGN = ".ALIGN:";
  private static final String _ALIGN1 = ".ALIGN1:";
  private static final String _ALIGN2 = ".ALIGN2:";
  private static final String _ALIGN3 = ".ALIGN3:";
  private static final String _ALIGNMENT_PADS = ".ALIGNMENT_PADS:";
  private static final String _BOARD_ID = ".BOARD_ID:";
  private static final String _DEFAULT_PITCH = ".DEFAULT_PITCH:";
  private static final String _DEFAULT_SURFACE_MAP_DENSITY = ".DEFAULT_SURFACE_MAP_DENSITY:";
  private static final String _SURFACE_MAP_DENSITY = ".SURFACE_MAP_DENSITY:";
  private static final String _DIMENSIONS = ".DIMENSIONS:";
  private static final String _THICKNESS_PADS = ".THICKNESS_PADS:";

  // "@<ref designator> <x location> <y location> <rotation> <land pattern id>"
  private static final String _EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_EXPECTED_SYNTAX_KEY");
  private static final String _REFERENCE_DESIGNATOR_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_REFERENCE_DESIGNATOR_KEY");
  private static final String _X_COORDINATE_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_X_COORDINATE_KEY");
  private static final String _Y_COORDINATE_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_Y_COORDINATE_KEY");
  private static final String _DEGREES_ROTATION_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_DEGREES_ROTATION_KEY");
  private static final String _LAND_PATTERN_NAME_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_LAND_PATTERN_NAME_KEY");

  // .default_pitch: <pitch>
  private static final String _DEFAULT_PITCH_ATTRIBUTE_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_ATTRIBUTE_DEFAULT_PITCH_EXPECTED_SYNTAX_KEY");
  private static final String _DEFAULT_PITCH_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_DEFAULT_PITCH_KEY");

  // .dimensions: <board width> <board length> <pad to pad thickness>
  private static final String _DIMENSIONS_ATTRIBUTE_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_ATTRIBUTE_DIMENSIONS_EXPECTED_SYNTAX_KEY");
  private static final String _BOARD_WIDTH_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_WIDTH_KEY");
  private static final String _BOARD_LENGTH_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_LENGTH_KEY");
  private static final String _PAD_TO_PAD_THICKNESS_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PAD_TO_PAD_THICKNESS_KEY");

  // .board_id: <name>
  private static final String _BOARD_ID_ATTRIBUTE_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_ATTRIBUTE_BOARD_ID_EXPECTED_SYNTAX_KEY");
  private static final String _BOARD_NAME_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_NAME_KEY");

  // .surface_map_density: <density>
  private static final String _DEFAULT_SURFACE_MAP_DENSITY_ATTRIBUTE_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_ATTRIBUTE_DEFAULT_SURFACE_MAP_DENSITY_EXPECTED_SYNTAX_KEY");
  private static final String _DENSITY_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_DENSITY_KEY");

  // .thickness_pads: <focus height> <camera index> <snap margin> <fov> <technique> <ref designator> <pin #> [<ref designator> <pin #> ..]
  private static final String _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_ATTRIBUTE_THICKNESS_PADS_EXPECTED_SYNTAX_KEY");
  private static final String _FOCUS_HEIGHT_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_FOCUS_HEIGHT_KEY");
  private static final String _CAMERA_INDEX_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_CAMERA_INDEX_KEY");
  private static final String _SNAP_MARGIN_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_SNAP_MARGIN_KEY");
  private static final String _FOV_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_FOV_KEY");
  private static final String _AUTO_FOCUS_TECHNIQUE_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_AUTO_FOCUS_TECHNIQUE_KEY");
  private static final String _PIN_NAME_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PIN_NAME_KEY");

  //.alignment_pads: <alignment group number> <ref. des.> <pin number> [<ref designator> <pin #> ..]
  private static final String _ALIGNMENT_PADS_ATTRIBUTE_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_ATTRIBUTE_ALIGNMENT_PADS_EXPECTED_SYNTAX_KEY");
  private static final String _ALIGNMENT_GROUP_NUMBER_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_ALIGNMENT_GROUP_NUMBER_KEY");

  // .align1: <ref designator> <pin number>
  private static final String _ALIGN_1_ATTRIBUTE_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_ATTRIBUTE_ALIGN_1_EXPECTED_SYNTAX_KEY");
  // .align2: <ref designator> <pin number>
  private static final String _ALIGN_2_ATTRIBUTE_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_ATTRIBUTE_ALIGN_2_EXPECTED_SYNTAX_KEY");
  // .align3: <ref designator> <pin number>
  private static final String _ALIGN_3_ATTRIBUTE_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_ATTRIBUTE_ALIGN_3_EXPECTED_SYNTAX_KEY");

  private static final double _DEFAULT_SURFACE_MAP_DENSITY_IN_MILS = 1500.0;
  private static final MathUtilEnum _DEFAULT_SURFACE_MAP_DENSITY_UNITS = MathUtilEnum.MILS;

  private static final int _ALIGNMENT_POINT_MIN_INDEX = 1;
  private static final int _ALIGNMENT_POINT_MAX_INDEX = 3;
  private static final int _ALIGNMENT_POINT_MAX_PADS = 6;
  private static final MathUtilEnum _THICKNESS_PAD_UNITS = MathUtilEnum.MILS;

  // contains a List of all the SideBoard objects that are:
  // - the same board type
  // - the same board side
  private List<SideBoard> _sameSideBoardInstances = null;
  private List<SideBoardType> _sameSideBoardTypes = null;

  // required line that can be set/specified multiple times per board.ndf
  private boolean _validRequiredDataLine = false;
  private boolean _validLine = true;

  // lines that can be set/specified only once per board.ndf
  //_checkSumFound inherited from NdfReader
  //_unitLineFound inherited from NdfReader
  private boolean _validAttributeLineDimensions = false;
  private boolean _validAttributeLineBoardID = false;
  //.DEFAULT_PITCH: required if package.ndf has packages with negative pitches
  private boolean _validAttributeLineDefaultPitch = false;
  private boolean _validAttributeLineDefaultSurfaceMapDensity = false;
  // see validFileCheck() for a summary of the logic used for determining the three (3) alignment groups.
  // this logic is why variables are named _use instead of _valid
  private boolean _useAttributeLineAlign1 = false;
  private boolean _useAttributeLineAlign2 = false;
  private boolean _useAttributeLineAlign3 = false;
  private boolean _useAttributeLineAlignmentPads1 = false;
  private boolean _useAttributeLineAlignmentPads2 = false;
  private boolean _useAttributeLineAlignmentPads3 = false;
  private boolean _parseRemainingAttributeLinesAlignmentPads = true;

  private boolean _unitAttributeLineFound = false;

  // BoardThicknessPointSetting and AlignmentGroup variables
  private List _boardThicknessPointSettingList = new ArrayList(); // list of BoardThicknessPointSettings
  //map from legacy group number Integer to List. List contents alternate between Strings of component name and pin number.
  private Map<Integer, List<String>> _legacyAlignmentGroupNumberToAlignmentGroupInfoListMap = new TreeMap<Integer, List<String>>();
  //map from legacy group number Integer to line number Integer for that legacy alignment group
  private Map<Integer, Integer> _legacyAlignmentGroupNumberToLineNumberMap = new HashMap<Integer, Integer>();
  private List<List<String>> _alignXinfoList = new ArrayList<List<String>>();

  // keeps track of component names in order to find duplicates.
  private Set<String> _uniqueComponentNames = new HashSet<String>();

  private StringTokenizer _tokenizer = null;
  private String _line = null;

  private String _referenceDesignator = null;
  private int _xCoordinateInNanoMeters = 0;
  private int _yCoordinateInNanoMeters = 0;
  private int _degreesRotation = 0;
  private String _landPatternName = null;
  private Panel _panel;

  /**
   * Constructor for the BoardNdfReader.
   *
   * @param mainReader - the MainReader associated with the panel program.
   * @author Keith Lee
   */
  BoardNdfReader(MainReader mainReader)
  {
    super(mainReader);
  }

  /**
   * @author Keith Lee
   */
  void setSameSideBoardInstances(List<SideBoard> sameSideBoardInstances)
  {
    Assert.expect(sameSideBoardInstances != null);
    Assert.expect(sameSideBoardInstances.size() > 0);

    _sameSideBoardInstances = sameSideBoardInstances;

    // there may be more than one SideBoardType for the SideBoards passed in
    // this can only happen if a SideBoard is shared with more than one Board
    // type in the ndfs.  In this corner case, we need to populate more than
    // one SideBoardType with the same ComponentTypes because the Component coordinates
    // will likely be different between the two SideBoardTypes when the top and
    // bottom SideBoards are corrected so they can share the same rotation, coordinate
    // and width and height.
    Set<SideBoardType> sideBoardTypes = new HashSet<SideBoardType>();
    for (SideBoard sideBoard : _sameSideBoardInstances)
    {
      SideBoardType sideBoardType = sideBoard.getSideBoardType();
      sideBoardTypes.add(sideBoardType);
    }
    _sameSideBoardTypes = new ArrayList<SideBoardType>(sideBoardTypes);
  }

  /**
   * @author Keith Lee
   */
  protected void initializeBeforeParsing()
  {
    // list of side boards can only be set via setSideBoards()
    Assert.expect(_sameSideBoardInstances != null);

    _parsedUnits = null;

    _validRequiredDataLine = false;
    _checkSumFound = false;
    _unitLineFound = false;
    _validAttributeLineBoardID = false;
    _validAttributeLineDefaultPitch = false;
    _validAttributeLineDefaultSurfaceMapDensity = false;
    _validAttributeLineDimensions = false;
    _useAttributeLineAlignmentPads1 = false;
    _useAttributeLineAlignmentPads2 = false;
    _useAttributeLineAlignmentPads3 = false;
    _useAttributeLineAlign1 = false;
    _useAttributeLineAlign2 = false;
    _useAttributeLineAlign3 = false;
    _parseRemainingAttributeLinesAlignmentPads = true;
    _unitAttributeLineFound = false;

    _boardThicknessPointSettingList.clear();
    _legacyAlignmentGroupNumberToAlignmentGroupInfoListMap.clear();
    _legacyAlignmentGroupNumberToLineNumberMap.clear();
    _alignXinfoList.clear();

    _uniqueComponentNames.clear();

    _tokenizer = null;
    _line = null;
    _referenceDesignator = null;
    _xCoordinateInNanoMeters = 0;
    _yCoordinateInNanoMeters = 0;
    _degreesRotation = 0;
    _landPatternName = null;
    _panel = null;
    _validLine = true;
  }

  /**
   * @author Keith Lee
   */
  protected void parseLinesRequiredByOtherLines() throws DatastoreException
  {
    // read in first .UNIT: attribute line
    String line = ParseUtil.readNextLine(_is, _currentFile);
    String upperCaseLine = null;
    boolean unitAttributeLineFound = false;
    while ((unitAttributeLineFound == false) && (line != null))
    {
      upperCaseLine = line.toUpperCase();
      if (upperCaseLine.length() == 0)
      {
        // skip: this is a blank line
      }
      else if (upperCaseLine.startsWith(NdfReader.getUnitToken()))
      {
        // .UNIT:
        line = line.substring(NdfReader.getUnitToken().length());
        parseAttributeLineUnit(line);
        unitAttributeLineFound = true;
      }
      else
      {
        // skip: this will be read in later during parseFile()
      }
      line = ParseUtil.readNextLine(_is, _currentFile);
    }

    // if no .UNIT: attribute line, set _parsedUnits to default value (MILS)
    if (_unitLineFound == false)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new MissingTokenDatastoreException(_currentFile, NdfReader.getUnitToken());
      }
      else
      {
        //give a warning that .UNIT arrtibute is missing and that mils are used as default
        _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_UNIT_ASSUMING_MILS_KEY",
                                          new Object[]{_currentFile}));
        _parsedUnits = _DEFAULT_NDF_UNITS;
      }
    }
  }

  /**
   * parses a data line in a board.ndf file that follows the following format
   * "@<ref designator> <x location> <y location> <rotation> <land pattern id>"
   *
   * @param line the line of board data in the board.ndf to parse.
   * @author George A. David
   */
  protected void parseDataLine(String line) throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);
    Assert.expect(line != null);

    _line = line;
    _validLine = true;

    parseInLine();
    if (_validLine)
    {
      populateClassesWithData();
    }
  }

  /**
   * @<reference designator> <x coordinate> <y coordinate> <degrees rotation> <land pattern name>
   * @author George A. David
   */
  private void parseInLine() throws DatastoreException
  {
    _tokenizer = new StringTokenizer(_line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    _referenceDesignator = parseReferenceDesignator();
    if (_validLine)
    {
      if (_uniqueComponentNames.add(_referenceDesignator) == false)
      {
        // the same reference designator is used twice
        throw new DuplicateComponentDatastoreException(_currentFile,
                                                       _is.getLineNumber(),
                                                       _referenceDesignator);
      }
      _xCoordinateInNanoMeters = parseXCoordinate();
      if (_validLine)
      {
        _yCoordinateInNanoMeters = parseYCoordinate();
        if (_validLine)
        {
          _degreesRotation = parseDegreesRotation();
          if (_validLine)
          {
            _landPatternName = parseLandPatternName();
            if (_validLine)
            {
              checkForExtraDataAtEndOfLine(_tokenizer, _EXPECTED_SYNTAX);
            }
          }
        }
      }
    }
  }

  /**
   * After each line has been parsed this method is called.  There is one reference
   * designator per line.
   *
   * @author George A. David
   */
  private void populateClassesWithData() throws DatastoreException
  {
    if (_mainReader.doesComponentExist(_referenceDesignator) == false)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new BadComponentReferenceDatastoreException(_currentFile,
                                                          _is.getLineNumber(),
                                                          _EXPECTED_SYNTAX,
                                                          _REFERENCE_DESIGNATOR_FIELD,
                                                          _referenceDesignator,
                                                          _currentFile,
                                                          FileName.getLegacyComponentNdfFullPath(_mainReader.getPanelName(),
                                                                                                 _mainReader.getCurrentSideBoardName()));

      }
      else
      {
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _EXPECTED_SYNTAX,
                                     _REFERENCE_DESIGNATOR_FIELD,
                                     _referenceDesignator,
                                     _currentFile,
                                     FileName.getLegacyComponentNdfFullPath(_mainReader.getPanelName(),
                                                                            _mainReader.getCurrentSideBoardName())};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_BAD_COMPONENT_REFERENCE_KEY", args));
      }
    }
    else
    {
      Component component = _mainReader.getComponent(_referenceDesignator);
      ComponentType componentType = component.getComponentType();

      // get land pattern type from component
      PadTypeEnum landPatternType = _mainReader.getPadTypeEnum(component);

      // get the LandPattern object since we now know its name
      LandPattern landPattern = null;
      boolean landPatternExists = false;
      if (landPatternType.equals(PadTypeEnum.THROUGH_HOLE))
      {
        landPatternExists = _mainReader.doesThroughHoleLandPatternExist(_landPatternName);
        if (landPatternExists)
        {
          landPattern = _mainReader.getThroughHoleLandPattern(_landPatternName);
        }
      }
      else if (landPatternType.equals(PadTypeEnum.SURFACE_MOUNT))
      {
        landPatternExists = _mainReader.doesSurfaceMountLandPatternExist(_landPatternName);
        if (landPatternExists)
        {
          landPattern = _mainReader.getSurfaceMountLandPattern(_landPatternName);
        }
      }
      else
      {
        //This assert should not happen. LandPatternType should only be mapped
        //to Through Hole (TH) or SurfaceMount (SM).
        Assert.expect(false);
      }

      if (landPatternExists == false)
      {
        // invalid land pattern
        String landPatternFileName = "";

        if (landPatternType.equals(PadTypeEnum.THROUGH_HOLE))
          landPatternFileName = FileName.getLegacyThroughHoleLandPatternNdfFullPath(_mainReader.getPanelName());
        else if (landPatternType.equals(PadTypeEnum.SURFACE_MOUNT))
          landPatternFileName = FileName.getLegacySurfaceMountLandPatternNdfFullPath(_mainReader.getPanelName());
        else
          Assert.expect(false);

        if (_mainReader.ndfsAreWellFormed() || _mainReader.isComponentInspected(component))
        {
          throw new BadLandPatternReferenceDatastoreException(_currentFile,
                                                              _is.getLineNumber(),
                                                              _EXPECTED_SYNTAX,
                                                              _LAND_PATTERN_NAME_FIELD,
                                                              _landPatternName,
                                                              landPatternFileName);
        }
        else
        {
          //create missing component land pattern id warning
          Object[] args = new Object[]{ _currentFile,
                                        new Integer(_is.getLineNumber()),
                                        _EXPECTED_SYNTAX,
                                        _LAND_PATTERN_NAME_FIELD,
                                        _landPatternName,
                                        landPatternFileName};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_BAD_LAND_PATTERN_REFERENCE_KEY", args));
        }
      }
      else
      {
        // we have a valid LandPattern
        // the LandPattern already has a List of LandPatternPads (from SurfaceMountLandPatternNdfReader
        // and ThroughHoleLandPatternNdfReader
        // Make a Pad object for each LandPatternPad and add them to Component
        // also make PackagePin objects for each LandPatternPad and add them to CompPackage
        Map<String, LandPatternPad> padNameToLandPatternPadMap = _mainReader.getPadNameToLandPatternPadMap(landPattern);
        landPattern.addComponentType(componentType);
        if (_panel == null)
          _panel = _mainReader.getProject().getPanel();

        if (_panel.doesLandPatternExist(landPattern.getName()) == false)
          _panel.addLandPattern(landPattern);

        ComponentTypeSettings componentTypeSettings = component.getComponentTypeSettings();
        List<PadType> padTypesForCurrentComponentType = componentType.getPadTypes();
        int numPadTypes = padTypesForCurrentComponentType.size();
        Collection<LandPatternPad> landPatternPads = padNameToLandPatternPadMap.values();
        int numLandPatternPads = landPatternPads.size();

        int i = -1;
        for (LandPatternPad landPatternPad : landPatternPads)
        {
          ++i;
          Pad pad = new Pad();
          pad.setNodeName("");

          // Set up the PadSettings
          PadSettings padSettings = new PadSettings();
          pad.setPadSettings(padSettings);
          padSettings.setPad(pad);
          padSettings.setTestable(true);

          // at this point we need to create a PadType and Pad for each line
          // in the board ndf file.  Later we will create more Pads using the same
          // PadType
          PadType padType = new PadType();
          padType.addPad(pad);
          pad.setPadType(padType);
          padType.setLandPatternPad(landPatternPad);
          PadTypeSettings padTypeSettings = new PadTypeSettings();
          padType.setPadTypeSettings(padTypeSettings);
          padTypeSettings.setInspected(true);
//          padTypeSettings.setTestable(true);
          component.addPad(pad);
          componentType.addPadType(padType);
        }

        BoardCoordinate componentCoordinateInNanoMeters = new BoardCoordinate(_xCoordinateInNanoMeters, _yCoordinateInNanoMeters);
        componentType.setLandPattern(landPattern);

        componentType.setCoordinateInNanoMeters(componentCoordinateInNanoMeters);
        componentType.setDegreesRotationRelativeToBoard(_degreesRotation);

        // set if the component is inspected or not.  This could not be done any earlier
        // because this method uses the Pads which were just added above
        if (_mainReader.isComponentInspected(component))
          component.getComponentTypeSettings().setInspected(true);
        else
          component.getComponentTypeSettings().setLoaded(false);

        // For each component read in, put a copy of the Component on every instance of the SideBoard object
        // that it belongs on
        i = 0;
        for (SideBoard sideBoard : _sameSideBoardInstances)
        {
          ++i;
          if (i == 1)
          {
            sideBoard.addComponent(component);
            SideBoardType sideBoardType = sideBoard.getSideBoardType();
            sideBoardType.addComponentType(componentType);
          }
          else
          {
            // all the rest of the side boards use copies
            Component componentCopy = new Component(component);
            sideBoard.addComponent(componentCopy);
          }
        }
      }
      
      if(component.has5dxPackageName())
        componentType.getCompPackage().setShortName(component.getFivedxPackageName());
    }
    _validRequiredDataLine = true;
  }

  /**
   * @<reference designator> <x coordinate> <y coordinate> <degrees rotation> <land pattern name>
   * @author George A. David
   */
  private String parseReferenceDesignator() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _EXPECTED_SYNTAX,
                                                  _REFERENCE_DESIGNATOR_FIELD);
      }
      else
      {
        // the compiler will ignore this line if the reference designator is missing.
        _validLine = false;
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _EXPECTED_SYNTAX,
                                     _REFERENCE_DESIGNATOR_FIELD};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_FIELD_LINE_IGNORED_KEY", args));
      }
    }
    return token;
  }

  /**
   * @<reference designator> <x coordinate> <y coordinate> <degrees rotation> <land pattern name>
   * @author George A. David
   */
  private int parseXCoordinate() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _EXPECTED_SYNTAX,
                                                  _X_COORDINATE_FIELD);
      }
      else
      {
        // the compiler will ignore this line.
        _validLine = false;
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _EXPECTED_SYNTAX,
                                     _X_COORDINATE_FIELD};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_FIELD_LINE_IGNORED_KEY", args));
      }
    }
    int xCoordinate = 0;

    if (_validLine)
    {
      double xCoordinateDouble = parseDouble(token,
                                             _EXPECTED_SYNTAX,
                                             _X_COORDINATE_FIELD,
                                             _parsedUnits);

      if (xCoordinateDouble < 0)
      {
        // It was decided that the compiler should not error on this and so Java should not either. TestLink will enforce it for
        // brand new programs, and for legacy programs it will allow it.
        // George A. David

        //create invalid coordinate location warning
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _EXPECTED_SYNTAX,
                                     _X_COORDINATE_FIELD,
                                     token};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_NEGATIVE_COMPONENT_COORDINATE_KEY", args));
      }

      xCoordinateDouble = MathUtil.convertUnits(xCoordinateDouble, _parsedUnits, _DATASTORE_UNITS);
      try
      {
        xCoordinate = MathUtil.convertDoubleToInt(xCoordinateDouble);
      }
      catch(ValueOutOfRangeException voore)
      {
        ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
            _is.getLineNumber(),
            _EXPECTED_SYNTAX,
            _X_COORDINATE_FIELD,
            voore);
        dex.initCause(voore);
        throw dex;
      }
    }

    return xCoordinate;
  }

  /**
   * @<reference designator> <x coordinate> <y coordinate> <degrees rotation> <land pattern name>
   * @author George A. David
   */
  private int parseYCoordinate() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _EXPECTED_SYNTAX,
                                                  _Y_COORDINATE_FIELD);
      }
      else
      {
        // the compiler will ignore this line
        _validLine = false;
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _EXPECTED_SYNTAX,
                                     _Y_COORDINATE_FIELD};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_FIELD_LINE_IGNORED_KEY", args));
      }
    }

    int yCoordinate = 0;

    if (_validLine)
    {
      double yCoordinateDouble = parseDouble(token,
                                             _EXPECTED_SYNTAX,
                                             _Y_COORDINATE_FIELD,
                                             _parsedUnits);
      if (yCoordinateDouble < 0)
      {
        // It was decided that the compiler should not error on this and so Java should not either. TestLink will enforce it for
        // brand new programs, and for legacy programs it will allow it.
        // George A. David

        //create invalid coordinate location warning
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _EXPECTED_SYNTAX,
                                     _Y_COORDINATE_FIELD,
                                     token};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_NEGATIVE_COMPONENT_COORDINATE_KEY", args));
      }

      yCoordinateDouble = MathUtil.convertUnits(yCoordinateDouble, _parsedUnits, _DATASTORE_UNITS);
      try
      {
        yCoordinate = MathUtil.convertDoubleToInt(yCoordinateDouble);
      }
      catch(ValueOutOfRangeException voore)
      {
        ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                      _is.getLineNumber(),
                                                                                      _EXPECTED_SYNTAX,
                                                                                      _Y_COORDINATE_FIELD,
                                                                                      voore);
        dex.initCause(voore);
        throw dex;
      }
    }

    return yCoordinate;
  }

  /**
   * @<reference designator> <x coordinate> <y coordinate> <degrees rotation> <land pattern name>
   *
   * @author George A. David
   */
  private int parseDegreesRotation() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _EXPECTED_SYNTAX,
                                                  _DEGREES_ROTATION_FIELD);
      }
      else
      {
        // the compiler will ignore this line
        _validLine = false;
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _EXPECTED_SYNTAX,
                                     _DEGREES_ROTATION_FIELD};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_FIELD_LINE_IGNORED_KEY", args));
      }
    }

    int degreesRotation = 0;

    if (_validLine)
    {
      double degreesRotationDouble = parseDouble(token, _EXPECTED_SYNTAX, _DEGREES_ROTATION_FIELD);
      degreesRotation = (int)Math.rint(degreesRotationDouble);
    }

    return degreesRotation;
  }

  /**
   * @<reference designator> <x coordinate> <y coordinate> <degrees rotation> <land pattern name>
   * @author George A. David
   */
  private String parseLandPatternName() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _EXPECTED_SYNTAX,
                                                  _LAND_PATTERN_NAME_FIELD);
      }
      else
      {
        _validLine = false;
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _EXPECTED_SYNTAX,
                                     _LAND_PATTERN_NAME_FIELD};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_FIELD_LINE_IGNORED_KEY", args));
      }
    }
    return token;
  }

  /**
   * parses an attribute line in a board.ndf file that follows one of the following formats
   * .board_id: <name>
   * .unit: <unit>
   * .default_pitch: <pitch>
   * .dimensions: <x> <y> <thickness>
   * .alignment_pads: <#> <ref designator> <pin #> [<ref designator> <pin #> ..]
   * .surface_map_density: <density>
   * .thickness_pads: <delta z> <fg index> <margin> <fov> <technique> <ref designator> <pin #> [<ref designator> <pin #> ..]
   *
   * @param line the line of board attributes in the board.ndf to parse.
   * @author Keith Lee
   * @author George A. David
   */
  protected void parseAttributeLine(String line) throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);
    Assert.expect(line != null);

    _line = line;
    String upperCaseLine = null;
    upperCaseLine = line.toUpperCase();
    _validLine = true;

    if (upperCaseLine.startsWith(NdfReader.getUnitToken()))
    {
      parseUnits();
    }
    else if (upperCaseLine.startsWith(_DIMENSIONS))
    {
      parseDimensions();
    }
    else if (upperCaseLine.startsWith(_BOARD_ID))
    {
      parseBoardId();

    }
    else if (upperCaseLine.startsWith(_DEFAULT_PITCH))
    {
      parseDefaultPitch();
    }
    // see validFileCheck() for a summary of the logic used for determining the 3 alignment groups.
    // the three alignment groups are determined by .ALIGNMENT_PADS: and .ALIGNX: {X = 1|2|3}
    else if (upperCaseLine.startsWith(_ALIGNMENT_PADS))
    {
      parseAlignmentGroup();
    }
    else if (upperCaseLine.startsWith(_ALIGN1))
    {
      parseAlign1();
    }
    else if (upperCaseLine.startsWith(_ALIGN2))
    {
      parseAlign2();
    }
    else if (upperCaseLine.startsWith(_ALIGN3))
    {
      parseAlign3();
    }
    else if (upperCaseLine.startsWith(_SURFACE_MAP_DENSITY))
    {
      parseDefaultSurfaceMapDensity();
    }
    else if (upperCaseLine.startsWith(_THICKNESS_PADS))
    {
      parseThicknessPads();
    }
    else if (upperCaseLine.startsWith(CHECKSUM))
    {
      _line = _line.substring(CHECKSUM.length());
      parseAttributeLineCheckSum(_line);
    }
    else if (upperCaseLine.startsWith(_DEFAULT_SURFACE_MAP_DENSITY))
    {
      deprecatedTokenWarning(_DEFAULT_SURFACE_MAP_DENSITY);
    }
    else if ( upperCaseLine.startsWith(_ALIGN))
    {
      deprecatedTokenWarning(_ALIGN);
    }
    else
    {
      int index = line.indexOf(":");
      String token = line;
      if (index != -1)
        token = line.substring(0, index + 1);

      if (_mainReader.ndfsAreWellFormed())
      {
        // invalid or unknown attribute line
        String validAttributes = _BOARD_ID + " " +
                          NdfReader.getUnitToken() + " " +
                          CHECKSUM + " " +
                          _DIMENSIONS + " " +
                          _DEFAULT_PITCH + " " +
                          _ALIGNMENT_PADS + " " +
                          _SURFACE_MAP_DENSITY + " " +
                          _THICKNESS_PADS;
        throw new InvalidAttributeDatastoreException(_currentFile,
                                                     _is.getLineNumber(),
                                                     token,
                                                     validAttributes);
      }
      else
      {
        unrecognizedTokenWarning(token);
      }
    }
  }

  /**
   * .UNIT: <units>
   * @author George A. David
   */
  private void parseUnits() throws DatastoreException
  {
    // .UNIT: was read in during parseRequiredLines(). check that this is the
    // only occurance of this attribute line in the file.
    if (_unitAttributeLineFound)
    {
      if (_mainReader.ndfsAreWellFormed())
        throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), NdfReader.getUnitToken());
      else
        duplicateTokenWarning(NdfReader.getUnitToken());
    }

    _unitAttributeLineFound = true;
  }

  /**
   * parses an attribute line in a board.ndf file that follows the following format
   * .board_id: <name>
   *
   * @author George A. David
   */
  private void parseBoardId() throws DatastoreException
  {
    Assert.expect(_line != null);

    if (_validAttributeLineBoardID == false)
    {
      _line = _line.substring(_BOARD_ID.length());

      // board name
      // this is strictly informational. the actual board name comes from panel.ndf
      // and can also be found in the dir_id.dat file found in the same directory
      // as the board.ndf file

      _tokenizer = new StringTokenizer(_line, getDelimiters(), _DONT_RETURN_DELIMITERS);
      String token = ParseUtil.getNextToken(_tokenizer);
      if (token == null)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new UnexpectedEolDatastoreException(_currentFile,
                                                    _is.getLineNumber(),
                                                    _BOARD_ID_ATTRIBUTE_EXPECTED_SYNTAX,
                                                    _BOARD_NAME_FIELD);
        }
        else
        {
          _validLine = false;
          Object[] args = new Object[]{_currentFile,
                                       new Integer(_is.getLineNumber()),
                                       _BOARD_ID_ATTRIBUTE_EXPECTED_SYNTAX,
                                       _BOARD_NAME_FIELD};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_FIELD_LINE_IGNORED_KEY", args));
        }
      }
      checkForExtraDataAtEndOfLine(_tokenizer, _BOARD_ID_ATTRIBUTE_EXPECTED_SYNTAX);

      _validAttributeLineBoardID = true;
    }
    else
    {
      if (_mainReader.ndfsAreWellFormed())
        throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _BOARD_ID);
      else
        duplicateTokenWarning(_BOARD_ID);
    }
  }

  /**
   * parses an attribute line in a board.ndf file that follows the following format
   * .default_pitch: <pitch>
   *
   * @author George A. David
   */
  private void parseDefaultPitch() throws DatastoreException
  {
    Assert.expect(_line != null);
    boolean userWarned = false;

    if (_validAttributeLineDefaultPitch == false)
    {
      _line = _line.substring(_DEFAULT_PITCH.length());
      // default board pitch (distance between pins, overrides those with negative pitch in package.ndf)
      int defaultPadPitchInNanoMeters = 0;
      double defaultPadPitchDouble = 0.0;

      _tokenizer = new StringTokenizer(_line, getDelimiters(), _DONT_RETURN_DELIMITERS);
      String token = ParseUtil.getNextToken(_tokenizer);
      if (token == null)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new UnexpectedEolDatastoreException(_currentFile,
                                                    _is.getLineNumber(),
                                                    _DEFAULT_PITCH_ATTRIBUTE_EXPECTED_SYNTAX,
                                                    _DEFAULT_PITCH_FIELD);
        }
        else
        {
          // the default pitch is mising, warn the user
          // and assume a value of 100 mils because that is what the compiler did.
          defaultPadPitchDouble = 100.0;
          Object[] args = new Object[]{_currentFile,
                                       new Integer(_is.getLineNumber()),
                                       _DEFAULT_PITCH_ATTRIBUTE_EXPECTED_SYNTAX,
                                       _DEFAULT_PITCH_FIELD,
                                       new Double(defaultPadPitchDouble),
                                       MathUtilEnum.MILS};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_UNITS_KEY", args));
          defaultPadPitchDouble = MathUtil.convertUnits(defaultPadPitchDouble, MathUtilEnum.MILS, _DATASTORE_UNITS);
        }
      }
      else
      {
        defaultPadPitchDouble = parseDouble(token,
                                            _DEFAULT_PITCH_ATTRIBUTE_EXPECTED_SYNTAX,
                                            _DEFAULT_PITCH_FIELD,
                                            _parsedUnits);

        if (defaultPadPitchDouble < 0 || MathUtil.fuzzyEquals(defaultPadPitchDouble, 0.0))
        {
          if (_mainReader.ndfsAreWellFormed())
          {
            throw new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                      _is.getLineNumber(),
                                                                      _DEFAULT_PITCH_ATTRIBUTE_EXPECTED_SYNTAX,
                                                                      _DEFAULT_PITCH_FIELD,
                                                                      token);
          }
          else
          {
            //warn the user that we expected a positive non zero number
            //and assume a value of 100.0 mils
            defaultPadPitchDouble = 100.0;
            Object[] args = new Object[]{ _currentFile,
                                          new Integer(_is.getLineNumber()),
                                          _DEFAULT_PITCH_ATTRIBUTE_EXPECTED_SYNTAX,
                                          _DEFAULT_PITCH_FIELD,
                                          token,
                                          new Double(defaultPadPitchDouble),
                                          MathUtilEnum.MILS};
            _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_NUMBER_UNITS_KEY", args));
            defaultPadPitchDouble = MathUtil.convertUnits(defaultPadPitchDouble, MathUtilEnum.MILS, _DATASTORE_UNITS);
          }
        }
        else
        {
          defaultPadPitchDouble = MathUtil.convertUnits(defaultPadPitchDouble, _parsedUnits, _DATASTORE_UNITS);
        }
      }
      try
      {
        defaultPadPitchInNanoMeters = MathUtil.convertDoubleToInt(defaultPadPitchDouble);
      }
      catch(ValueOutOfRangeException voore)
      {
        ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                      _is.getLineNumber(),
                                                                                      _DEFAULT_PITCH_ATTRIBUTE_EXPECTED_SYNTAX,
                                                                                      _DEFAULT_PITCH_FIELD,
                                                                                      voore);
        dex.initCause(voore);
        throw dex;
      }

      checkForExtraDataAtEndOfLine(_tokenizer, _DEFAULT_PITCH_ATTRIBUTE_EXPECTED_SYNTAX);

      _validAttributeLineDefaultPitch = true;
    }
    else
    {
      if (_mainReader.ndfsAreWellFormed())
        throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _DEFAULT_PITCH);
      else
        duplicateTokenWarning(_DEFAULT_PITCH);
    }
  }

  /**
   * parses an attribute line in a board.ndf file that follows the following format
   * .dimensions: <board width> <board length> <pad to pad thickness>
   *
   * @author George A. David
   */
  private void parseDimensions() throws DatastoreException
  {
    Assert.expect(_line != null);

    if (_validAttributeLineDimensions == false)
    {
      _line = _line.substring(_DIMENSIONS.length());

      // board width, board length and board thickness from pad level to pad level
      _tokenizer = new StringTokenizer(_line, getDelimiters(), _DONT_RETURN_DELIMITERS);
      int boardWidthInNanoMeters = parseBoardWidth();
      int boardLengthInNanoMeters = parseBoardLength();
      int padToPadThicknessInNanoMeters = parsePadToPadThickness();
      checkForExtraDataAtEndOfLine(_tokenizer, _DIMENSIONS_ATTRIBUTE_EXPECTED_SYNTAX);

      // thickness checks are done back in mainReader, because the "Board" overrides the panel, and down in here we don't know
      // which "Board" we are -- as we only parse each SideBoard once, we need to put them together in mainReader to form boards.
      Iterator it = _sameSideBoardTypes.iterator();
      while (it.hasNext())
      {
        SideBoardType sideBoardType = (SideBoardType)it.next();
        _mainReader.addSideBoardTypeToWidthInNanoMetersMap(sideBoardType, boardWidthInNanoMeters);
        _mainReader.addSideBoardTypeToLengthInNanoMetersMap(sideBoardType, boardLengthInNanoMeters);
      }
      _validAttributeLineDimensions = true;
    }
    else
    {
      if (_mainReader.ndfsAreWellFormed())
        throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _DIMENSIONS);
      else
        duplicateTokenWarning(_DIMENSIONS);
    }
  }

  /**
   * .dimensions: <board width> <board length> <pad to pad thickness>
   *
   * @author George A. David
   */
  private int parseBoardWidth() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    int boardWidth = -1;
    double boardWidthDouble = -1.0;

    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _DIMENSIONS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                  _BOARD_WIDTH_FIELD);
      }
      else
      {
        // if the width is not specified, assume 1 mil and
        // warn the user
        boardWidthDouble = 1.0;
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _DIMENSIONS_ATTRIBUTE_EXPECTED_SYNTAX,
                                     _BOARD_WIDTH_FIELD,
                                     new Double(boardWidthDouble),
                                     MathUtilEnum.MILS};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_UNITS_KEY", args));
        boardWidthDouble = MathUtil.convertUnits(boardWidthDouble, MathUtilEnum.MILS, _DATASTORE_UNITS);
      }
    }
    else
    {

      boardWidthDouble = parsePositiveNonZeroDouble(token,
                                                    _DIMENSIONS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                    _BOARD_WIDTH_FIELD,
                                                    _parsedUnits);
      boardWidthDouble = MathUtil.convertUnits(boardWidthDouble, _parsedUnits, _DATASTORE_UNITS);
    }

    try
    {
      boardWidth = MathUtil.convertDoubleToInt(boardWidthDouble);
    }
    catch(ValueOutOfRangeException voore)
    {
      double actualValue = MathUtil.convertUnits(voore.getActualValue(), _DATASTORE_UNITS, _parsedUnits);
      double maxValue = MathUtil.convertUnits(voore.getMaxValue(), _DATASTORE_UNITS, _parsedUnits);
      double minValue = MathUtil.convertUnits(voore.getMinValue(), _DATASTORE_UNITS, _parsedUnits);
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _DIMENSIONS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                                                    _BOARD_WIDTH_FIELD,
                                                                                    actualValue,
                                                                                    minValue,
                                                                                    maxValue);
      dex.initCause(voore);
      throw dex;
    }

    return boardWidth;
  }

  /**
   * .dimensions: <board width> <board length> <pad to pad thickness>
   *
   * @author George A. David
   */
  private int parseBoardLength() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    double boardLengthDouble = -1.0;
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _DIMENSIONS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                  _BOARD_LENGTH_FIELD);
      }
      else
      {
        // if not specified, warn the user and
        // assume a value of 1 mil
        boardLengthDouble = 1.0;
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _DIMENSIONS_ATTRIBUTE_EXPECTED_SYNTAX,
                                     _BOARD_LENGTH_FIELD,
                                     new Double(boardLengthDouble),
                                     MathUtilEnum.MILS};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_UNITS_KEY", args));
        boardLengthDouble = MathUtil.convertUnits(boardLengthDouble, MathUtilEnum.MILS, _DATASTORE_UNITS);
      }
    }
    else
    {
      boardLengthDouble = parsePositiveNonZeroDouble(token,
                                                     _DIMENSIONS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                     _BOARD_LENGTH_FIELD,
                                                     _parsedUnits);
      boardLengthDouble = MathUtil.convertUnits(boardLengthDouble, _parsedUnits, _DATASTORE_UNITS);
    }

    int boardLength = 0;
    try
    {
      boardLength = MathUtil.convertDoubleToInt(boardLengthDouble);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _DIMENSIONS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                                                    _BOARD_LENGTH_FIELD, voore);
      dex.initCause(voore);
      throw dex;
    }

    return boardLength;
  }

  /**
   * .dimensions: <board width> <board length> <pad to pad thickness>
   *
   * @author George A. David
   */
  private int parsePadToPadThickness() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    double padToPadThicknessDouble = -1.0;
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _DIMENSIONS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                  _PAD_TO_PAD_THICKNESS_FIELD);
      }
      else
      {
        // if not specified, warn the user and assume a value of 1 mil
        padToPadThicknessDouble = 1.0;
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _DIMENSIONS_ATTRIBUTE_EXPECTED_SYNTAX,
                                     _PAD_TO_PAD_THICKNESS_FIELD,
                                     new Double(padToPadThicknessDouble),
                                     MathUtilEnum.MILS};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_UNITS_KEY", args));
        padToPadThicknessDouble = MathUtil.convertUnits(padToPadThicknessDouble, MathUtilEnum.MILS, _DATASTORE_UNITS);
      }
    }
    else
    {
      padToPadThicknessDouble = parsePositiveNonZeroDouble(token,
                                                           _DIMENSIONS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                           _PAD_TO_PAD_THICKNESS_FIELD,
                                                           _parsedUnits);

      padToPadThicknessDouble = MathUtil.convertUnits(padToPadThicknessDouble, _parsedUnits, _DATASTORE_UNITS);
    }

    int padToPadThickness = 0;
    try
    {
      padToPadThickness = MathUtil.convertDoubleToInt(padToPadThicknessDouble);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _DIMENSIONS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                                                    _PAD_TO_PAD_THICKNESS_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }
    return padToPadThickness;
  }

  /**
   * parses an attribute line in a board.ndf file that follows the following format
   * .alignment_pads: <alignment group number> <ref. des.> <pin number> [<ref designator> <pin #> ..]
   *
   * @author George A. David
   */
  private void parseAlignmentGroup() throws DatastoreException
  {
    Assert.expect(_line != null);

    _validLine = true;
    if (_parseRemainingAttributeLinesAlignmentPads)
    {
      _line = _line.substring(_ALIGNMENT_PADS.length());
      // NOTE: since alignment groups are dependent on data lines, temp alignment group info is created here.
      // actual alignment groups are created and added in validFileCheck(). see validFileCheck() for a summary
      // of the logic used for determining the three (3) alignment groups.

      // board alignment pads

      List<String> alignmentGroupInfoList = new ArrayList<String>();


      _tokenizer = new StringTokenizer(_line, getDelimiters(), _DONT_RETURN_DELIMITERS);
      int alignmentGroupNumber = parseAlignmentGroupNumber();

      if (_validLine)
      {
        String referenceDesignator = parseAlignmentGroupReferenceDesignator();
        if (_validLine)
        {
          String pinName = parseAlignmentGroupPinName();
          if (_validLine)
          {
            //should always be able to add component reference designator and pin name to alignment group info list
            Assert.expect(alignmentGroupInfoList.add(referenceDesignator));
            Assert.expect(alignmentGroupInfoList.add(pinName));

            int padCount = 1;
            while (_tokenizer.hasMoreTokens() &&
                  (padCount < _ALIGNMENT_POINT_MAX_PADS) &&
                  _validLine)
            {
              referenceDesignator = parseAlignmentGroupReferenceDesignator();
              if (_validLine)
              {
                pinName = parseAlignmentGroupPinName();

                if (_validLine)
                {
                  //should always be able to add component reference designator and pin number to alignment group info list
                  Assert.expect(alignmentGroupInfoList.add(referenceDesignator));
                  Assert.expect(alignmentGroupInfoList.add(pinName));

                  padCount++;
                }
              }
            }

            if (_validLine)
            {
              if (_tokenizer.hasMoreTokens())
              {
                // at this point, we have too much information
                // we need to determine if it's just a comment,
                // or too many pads.
                String restOfLine = ParseUtil.getNextToken(_tokenizer, _NO_DELIMITERS);
                _tokenizer = new StringTokenizer(restOfLine, getDelimiters(), _DONT_RETURN_DELIMITERS);
                String token = ParseUtil.getNextToken(_tokenizer);
                if (_mainReader.doesComponentExist(token))
                {
                  // ok, so it seems like the user tried to specify too many pads.
                  if (_mainReader.ndfsAreWellFormed())
                  {
                    throw new TooManyAlignmentPadsDatastoreException(_currentFile,
                                                                     _is.getLineNumber(),
                                                                     _ALIGNMENT_POINT_MAX_PADS);
                  }
                  else
                  {
                    //create warning
                    Object[] args = new Object[]{_currentFile,
                                                 new Integer(_is.getLineNumber()),
                                                 new Integer(_ALIGNMENT_POINT_MAX_PADS) };
                    _warnings.add(new LocalizedString("DS_NDF_WARNING_TOO_MANY_ALIGNMENT_PADS_KEY", args));
                  }
                }
                else
                {
                  // ok, we now know the user did not specify more pads,
                  // let's determine if the data is a valid comment.
                  if (isValidComment(restOfLine) == false)
                  {
                    //must end line with valid comment post 8.0
                    if (_mainReader.ndfsAreWellFormed())
                    {
                      throw new ExpectedEolDatastoreException(_currentFile,
                                                              _is.getLineNumber(),
                                                              _ALIGNMENT_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                              restOfLine);
                    }
                    //pre 8.0, validFileCheck() will generate warning
                    else
                    {
                      // warn that extra data on this line is going to be ignored
                      Object[] args = new Object[]{_currentFile,
                                                   new Integer(_is.getLineNumber()),
                                                   _ALIGNMENT_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                   restOfLine};
                      _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_EOL_KEY", args));
                    }
                  }
                }
              }
              //map legacy alignment group number to alignment group info list and line number
              Object previous = _legacyAlignmentGroupNumberToAlignmentGroupInfoListMap.put(new Integer(alignmentGroupNumber),
                                                                                           alignmentGroupInfoList);
              if (_mainReader.ndfsAreWellFormed())
              {
                Assert.expect(previous == null); //check earlier if alignment group info already defined by previous line
                previous = _legacyAlignmentGroupNumberToLineNumberMap.put(new Integer(alignmentGroupNumber), new Integer(_is.getLineNumber()));
                Assert.expect(previous == null); //if legacy group number to alignment group info did not Assert, neither should this
              }
              else
              {
                if (previous == null)
                {
                  previous = _legacyAlignmentGroupNumberToLineNumberMap.put(new Integer(alignmentGroupNumber), new Integer(_is.getLineNumber()));
                  Assert.expect(previous == null);//if legacy group number to alignment group info not previously mapped, neither should this
                }
                else
                {
                  previous = _legacyAlignmentGroupNumberToLineNumberMap.put(new Integer(alignmentGroupNumber), new Integer(_is.getLineNumber()));
                  Assert.expect(previous != null);//if legacy group number to alignment group info previously mapped, so should this
                }
              }
              switch(alignmentGroupNumber)
              {
                case 1:
                  _useAttributeLineAlignmentPads1 = true;
                  break;
                case 2:
                  _useAttributeLineAlignmentPads2 = true;
                  break;
                case 3:
                  _useAttributeLineAlignmentPads3 = true;
                  break;
                default:
                  Assert.expect(false);
              }
            }
          }
        }
      }
    }
  }

  /**
   * .alignment_pads: <alignment group number> <ref. des.> <pin number> [<ref designator> <pin #> ..]
   *
   * @author George A. David
   */
  private int parseAlignmentGroupNumber() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    int alignmentGroupNumber = 0;

    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _ALIGNMENT_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                  _ALIGNMENT_GROUP_NUMBER_FIELD);
      }
      else
      {
        //generate a warning; stop parsing line and all future alignment lines.
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _ALIGNMENT_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                     _ALIGNMENT_GROUP_NUMBER_FIELD};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_ALIGNMENT_GROUP_NUMBER_KEY", args));
        _parseRemainingAttributeLinesAlignmentPads = false;
        _validLine = false;
      }
    }
    else
    {
      alignmentGroupNumber = parseInt(token,
                                      _ALIGNMENT_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                      _ALIGNMENT_GROUP_NUMBER_FIELD);

      if (alignmentGroupNumber < _ALIGNMENT_POINT_MIN_INDEX ||
         alignmentGroupNumber > _ALIGNMENT_POINT_MAX_INDEX)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new ValueOutOfRangeDatastoreException(_currentFile,
                                                      _is.getLineNumber(),
                                                      _ALIGNMENT_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                      _ALIGNMENT_GROUP_NUMBER_FIELD,
                                                      alignmentGroupNumber,
                                                      _ALIGNMENT_POINT_MIN_INDEX,
                                                      _ALIGNMENT_POINT_MAX_INDEX);
        }
        else
        {
          // warn the user and ignore this line and all future alignment lines
          _parseRemainingAttributeLinesAlignmentPads = false;
          _validLine = false;
          Object[] args = new Object[]{_currentFile,
                                       new Integer(_is.getLineNumber()),
                                       _ALIGNMENT_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                       _ALIGNMENT_GROUP_NUMBER_FIELD,
                                       new Integer(alignmentGroupNumber),
                                       new Integer(_ALIGNMENT_POINT_MIN_INDEX),
                                       new Integer(_ALIGNMENT_POINT_MAX_INDEX)};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_INVALID_ALIGNMENT_GROUP_NUMBER_KEY", args));

        }
      }
      else
      {
        //check to see if alignment group info specified in multiple places
        switch(alignmentGroupNumber)
        {
          case 1:
            if (_useAttributeLineAlignmentPads1 || _useAttributeLineAlign1)
            {
              if (_mainReader.ndfsAreWellFormed())
              {
                throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _ALIGNMENT_PADS + " 1");
              }
              else
              {
                Integer lineNumber = (Integer)_legacyAlignmentGroupNumberToLineNumberMap.get(new Integer(alignmentGroupNumber));
                Assert.expect(lineNumber != null);
                Object[] args = new Object[]{ _currentFile,
                                              new Integer(alignmentGroupNumber),
                                              lineNumber,
                                              new Integer(_is.getLineNumber())};
                _warnings.add(new LocalizedString("DS_NDF_WARNING_DUPLICATE_ALIGNMENT_VIEW_KEY", args));
              }
            }
            break;
          case 2:
            if (_useAttributeLineAlignmentPads2 || _useAttributeLineAlign2)
            {
              if (_mainReader.ndfsAreWellFormed())
              {
                throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _ALIGNMENT_PADS + " 2");
              }
              else
              {
                Integer lineNumber = (Integer)_legacyAlignmentGroupNumberToLineNumberMap.get(new Integer(alignmentGroupNumber));
                Assert.expect(lineNumber != null);
                Object[] args = new Object[]{ _currentFile,
                                              new Integer(alignmentGroupNumber),
                                              lineNumber,
                                              new Integer(_is.getLineNumber())};
                _warnings.add(new LocalizedString("DS_NDF_WARNING_DUPLICATE_ALIGNMENT_VIEW_KEY", args));
              }
            }
            break;
          case 3:
            if (_useAttributeLineAlignmentPads3 || _useAttributeLineAlign3)
            {
              if (_mainReader.ndfsAreWellFormed())
              {
                throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _ALIGNMENT_PADS + " 3");
              }
              else
              {
                Integer lineNumber = (Integer)_legacyAlignmentGroupNumberToLineNumberMap.get(new Integer(alignmentGroupNumber));
                Assert.expect(lineNumber != null);
                Object[] args = new Object[]{ _currentFile,
                                              new Integer(alignmentGroupNumber),
                                              lineNumber,
                                              new Integer(_is.getLineNumber())};
                _warnings.add(new LocalizedString("DS_NDF_WARNING_DUPLICATE_ALIGNMENT_VIEW_KEY", args));
              }
            }
            break;
          default:
            Assert.expect(false);
        }
      }
    }
    return alignmentGroupNumber;
  }

  /**
   * .alignment_pads: <alignment group number> <ref. des.> <pin number> [<ref designator> <pin #> ..]
   *
   * @author George A. David
   */
  private String parseAlignmentGroupReferenceDesignator() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _ALIGNMENT_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                  _REFERENCE_DESIGNATOR_FIELD);
      }
      else
      {
        _validLine = false;
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _ALIGNMENT_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                     _REFERENCE_DESIGNATOR_FIELD};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_FIELD_LINE_IGNORED_KEY", args));
      }
    }
    return token;
  }

  /**
   * .alignment_pads: <alignment group number> <ref. des.> <pin number> [<ref designator> <pin #> ..]
   *
   * @author George A. David
   */
  private String parseAlignmentGroupPinName() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);

    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _ALIGNMENT_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                  _PIN_NAME_FIELD);
      }
      else
      {
        _validLine = false;
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _ALIGNMENT_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                     _PIN_NAME_FIELD};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_FIELD_LINE_IGNORED_KEY", args));
      }
    }

    return token;
  }

  /**
   * .alignX: <ref designator> <pin number>
   * @author George A. David
   */
  private void parseAlignX(int indexNumber, String expectedSyntax) throws DatastoreException
  {
    Assert.expect(indexNumber >= _ALIGNMENT_POINT_MIN_INDEX);
    Assert.expect(indexNumber <= _ALIGNMENT_POINT_MAX_INDEX);
    Assert.expect(expectedSyntax != null);
    Assert.expect(_mainReader.ndfsAreWellFormed() == false);

    _validLine = true;

    // no .ALIGNMENT_PADS: for alignment group one(1) yet
    _line = _line.substring(_ALIGN1.length());
    // NOTE: since alignment groups are dependent on data lines, temp alignment group info is created here.
    // actual alignment groups are created and added in validFileCheck(). see validFileCheck() for a summary
    // of the logic used for determining the three (3) alignment groups.

    List<String> alignmentGroupInfoList = new ArrayList<String>();

    _tokenizer = new StringTokenizer(_line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String referenceDesignator = parseAlignXreferenceDesignator(expectedSyntax);
    if (_validLine)
    {
      String pinName = parseAlignXpinName(expectedSyntax);
      if (_validLine)
      {
        String token = ParseUtil.getNextToken(_tokenizer);
        checkForExtraDataAtEndOfLine(_tokenizer, expectedSyntax);

        //should always be able to add component reference designator and pin number to alignment group info list
        Assert.expect(alignmentGroupInfoList.add(referenceDesignator));
        Assert.expect(alignmentGroupInfoList.add(pinName));
        //map legacy alignment group number to alignment group info list and line number
        Object previous = _legacyAlignmentGroupNumberToAlignmentGroupInfoListMap.put(new Integer(indexNumber),
                                                                                                 alignmentGroupInfoList);
        Assert.expect(_alignXinfoList.add(alignmentGroupInfoList));
        Assert.expect(previous == null);//should not be parsing if already defined by previous line
        previous = _legacyAlignmentGroupNumberToLineNumberMap.put(new Integer(indexNumber), new Integer(_is.getLineNumber()));
        Assert.expect(previous == null);//if legacy group number to alignment group info did not Assert, neither should this
        switch(indexNumber)
        {
          case 1:
            _useAttributeLineAlign1 = true;
            break;
          case 2:
            _useAttributeLineAlign2 = true;
            break;
          case 3:
            _useAttributeLineAlign3 = true;
            break;
          default:
            Assert.expect(false);
        }
      }
    }
  }

  /**
   * .align1: <ref designator> <pin number>
   * @author George A. David
   */
  private void parseAlign1() throws DatastoreException
  {
    int indexNumber = 1;

    if (_mainReader.ndfsAreWellFormed())
    {
      throw new AlignXunsupportedDatastoreException(_currentFile,
                                                    _is.getLineNumber(),
                                                    indexNumber);
    }

    if (_useAttributeLineAlign1 == false)
    {
      if (_useAttributeLineAlignmentPads1 == false)
      {
        parseAlignX(indexNumber, _ALIGN_1_ATTRIBUTE_EXPECTED_SYNTAX);
      }
      else
      {
        // warning : .ALIGNX: and .ALIGNMENT_PADS: for alignment group one
        // defined with .ALIGNX: and .ALIGNMENT_PADS: - using .ALIGNMENT_PADS:
        Integer lineNumber = (Integer)_legacyAlignmentGroupNumberToLineNumberMap.get(new Integer(indexNumber));
        Assert.expect(lineNumber != null);
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(indexNumber),
                                      _ALIGN1};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_ALIGN_AND_ALIGN_GROUP_MIXED_KEY", args));
      }
    }
    else
    {
        duplicateTokenWarning(_ALIGN1);
    }
  }

  /**
   * .align2: <ref designator> <pin number>
   * @author George A. David
   */
  private void parseAlign2() throws DatastoreException
  {
    int indexNumber = 2;
    if (_mainReader.ndfsAreWellFormed())
    {
      throw new AlignXunsupportedDatastoreException(_currentFile,
                                                    _is.getLineNumber(),
                                                    indexNumber);
    }

    if (_useAttributeLineAlign2 == false)
    {
      if (_useAttributeLineAlignmentPads2 == false)
      {
        parseAlignX(indexNumber, _ALIGN_2_ATTRIBUTE_EXPECTED_SYNTAX);
      }
      else
      {
        // warning : .ALIGNX: and .ALIGNMENT_PADS: for alignment group one
        // defined with .ALIGNX: and .ALIGNMENT_PADS: - using .ALIGNMENT_PADS:
        Integer lineNumber = (Integer)_legacyAlignmentGroupNumberToLineNumberMap.get(new Integer(indexNumber));
        Assert.expect(lineNumber != null);
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(indexNumber),
                                      _ALIGN2};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_ALIGN_AND_ALIGN_GROUP_MIXED_KEY", args));
      }
    }
    else
    {
        duplicateTokenWarning(_ALIGN2);
    }
  }

  /**
   * .align3: <ref designator> <pin number>
   * @author George A. David
   */
  private void parseAlign3() throws DatastoreException
  {
    int indexNumber = 3;
    if (_mainReader.ndfsAreWellFormed())
    {
      throw new AlignXunsupportedDatastoreException(_currentFile,
                                                    _is.getLineNumber(),
                                                    indexNumber);
    }

    if (_useAttributeLineAlign3 == false)
    {
      if (_useAttributeLineAlignmentPads3 == false)
      {
        parseAlignX(indexNumber, _ALIGN_3_ATTRIBUTE_EXPECTED_SYNTAX);
      }
      else
      {
        // warning : .ALIGNX: and .ALIGNMENT_PADS: for alignment group one
        // defined with .ALIGNX: and .ALIGNMENT_PADS: - using .ALIGNMENT_PADS:
        Integer lineNumber = (Integer)_legacyAlignmentGroupNumberToLineNumberMap.get(new Integer(indexNumber));
        Assert.expect(lineNumber != null);
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(indexNumber),
                                      _ALIGN3};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_ALIGN_AND_ALIGN_GROUP_MIXED_KEY", args));
      }
    }
    else
    {
        duplicateTokenWarning(_ALIGN3);
    }
  }

  /**
   * .alignx: <reference designator> <pin number>
   *
   * @author George A. David
   */
  private String parseAlignXreferenceDesignator(String expectedSyntax) throws DatastoreException
  {
    Assert.expect(expectedSyntax != null);
    Assert.expect(_mainReader.ndfsAreWellFormed() == false);

    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      _validLine = false;
      Object[] args = new Object[]{_currentFile,
                                   new Integer(_is.getLineNumber()),
                                   expectedSyntax,
                                   _REFERENCE_DESIGNATOR_FIELD};
      _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_FIELD_LINE_IGNORED_KEY", args));
    }
    return token;
  }

  /**
   * .alignx: <reference designator> <pin number>
   *
   * @author George A. David
   */
  private String parseAlignXpinName(String expectedSyntax) throws DatastoreException
  {
    Assert.expect(expectedSyntax != null);
    Assert.expect(_mainReader.ndfsAreWellFormed() == false);

    String token = ParseUtil.getNextToken(_tokenizer);

    if (token == null)
    {
      _validLine = false;
      Object[] args = new Object[]{_currentFile,
                                   new Integer(_is.getLineNumber()),
                                   expectedSyntax,
                                   _PIN_NAME_FIELD};
      _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_FIELD_LINE_IGNORED_KEY", args));
    }

    return token;
  }

  /**
   * parses an attribute line in a board.ndf file that follows the following format
   * .surface_map_density: <density>
   *
   * @author George A. David
   */
  private void parseDefaultSurfaceMapDensity() throws DatastoreException
  {
    Assert.expect(_line != null);

    if (_validAttributeLineDefaultSurfaceMapDensity == false)
    {
      _line = _line.substring(_SURFACE_MAP_DENSITY.length());

      // board surface map density
      double surfaceMapDensityDouble= 0.0;
      int surfaceMapDensityInNanoMeters = 0;

      _tokenizer = new StringTokenizer(_line, getDelimiters(), _DONT_RETURN_DELIMITERS);
      String token = ParseUtil.getNextToken(_tokenizer);
      if (token == null)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new UnexpectedEolDatastoreException(_currentFile,
                                                    _is.getLineNumber(),
                                                    _DEFAULT_SURFACE_MAP_DENSITY_ATTRIBUTE_EXPECTED_SYNTAX,
                                                    _DENSITY_FIELD);
        }
        else
        {
          // assume the default which is 1500 mils
          surfaceMapDensityDouble = _DEFAULT_SURFACE_MAP_DENSITY_IN_MILS;
          surfaceMapDensityDouble = MathUtil.convertUnits(surfaceMapDensityDouble, _DEFAULT_SURFACE_MAP_DENSITY_UNITS, _DATASTORE_UNITS);
          Object[] args = new Object[]{_currentFile,
                                       new Integer(_is.getLineNumber()),
                                       _DEFAULT_SURFACE_MAP_DENSITY_ATTRIBUTE_EXPECTED_SYNTAX,
                                       _DENSITY_FIELD,
                                       new Double(_DEFAULT_SURFACE_MAP_DENSITY_IN_MILS),
                                       MathUtilEnum.MILS};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_UNITS_KEY", args));
        }
      }
      else
      {
        Exception ex = null;
        try
        {
          surfaceMapDensityDouble = StringUtil.convertStringToPositiveDouble(token);
          if (MathUtil.fuzzyEquals(surfaceMapDensityDouble, 0.0))
          {
            if (_mainReader.ndfsAreWellFormed())
            {
              throw new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                        _is.getLineNumber(),
                                                                        _DEFAULT_SURFACE_MAP_DENSITY_ATTRIBUTE_EXPECTED_SYNTAX,
                                                                        _DENSITY_FIELD,
                                                                        token);
            }
            else
            {
              //warn the user that we expected a positive non zero number
              //and we will set the value to 1500.0 mils because this is what the compiler did
              surfaceMapDensityDouble = _DEFAULT_SURFACE_MAP_DENSITY_IN_MILS;
              Object[] args = new Object[]{ _currentFile,
                                            new Integer(_is.getLineNumber()),
                                            _DEFAULT_SURFACE_MAP_DENSITY_ATTRIBUTE_EXPECTED_SYNTAX,
                                            _DENSITY_FIELD,
                                            token,
                                            new Double(surfaceMapDensityDouble),
                                            MathUtilEnum.MILS};
              _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_NUMBER_UNITS_KEY", args));
              surfaceMapDensityDouble = MathUtil.convertUnits(surfaceMapDensityDouble, MathUtilEnum.MILS, _parsedUnits);
            }
          }
        }
        catch(BadFormatException bfe)
        {
          ex = bfe;
        }
        catch (ValueOutOfRangeException vex)
        {
          ex = vex;
        }
        if (ex != null)
        {
          if (_mainReader.ndfsAreWellFormed())
          {
            ExpectedPositiveNonZeroNumberDatastoreException dex = new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                                                                      _is.getLineNumber(),
                                                                                                                      _DEFAULT_SURFACE_MAP_DENSITY_ATTRIBUTE_EXPECTED_SYNTAX,
                                                                                                                      _DENSITY_FIELD,
                                                                                                                      token);
            dex.initCause(ex);
            throw dex;
          }
          else
          {
            // the legacy compiler used the c++ atoi() function, which converted as
            // much of the string as possible to a float, and that is what we will do here.
            MathUtilEnum unitsUsed = _parsedUnits;
            double valueUsed = 0.0;
            ex = null;
            try
            {
              surfaceMapDensityDouble = StringUtil.legacyConvertStringToPositiveDouble(token);
              valueUsed = surfaceMapDensityDouble;
              if (MathUtil.fuzzyEquals(surfaceMapDensityDouble, 0.0))
              {
                // set the value to 1500 mils because this is what the compiler does
                valueUsed = _DEFAULT_SURFACE_MAP_DENSITY_IN_MILS;
                surfaceMapDensityDouble = MathUtil.convertUnits(valueUsed, MathUtilEnum.MILS, _parsedUnits);
                unitsUsed = MathUtilEnum.MILS;
              }
            }
            catch(BadFormatException anotherBfe)
            {
              ex = anotherBfe;
            }
            catch (ValueOutOfRangeException vex)
            {
              ex = vex;
            }
            if (ex != null)
            {
              // the legacy compiler used the c++ built-in function atoi() to convert
              // a string to an integer. if the string did not begin with a positive or
              // negative integer. set the value to 1500 mils because this is what the compiler does
              valueUsed = _DEFAULT_SURFACE_MAP_DENSITY_IN_MILS;
              surfaceMapDensityDouble = MathUtil.convertUnits(valueUsed, MathUtilEnum.MILS, _parsedUnits);
              unitsUsed = MathUtilEnum.MILS;
            }
            // warn the user that we expected a positive non zero number
            Object[] args = new Object[]{ _currentFile,
                                          new Integer(_is.getLineNumber()),
                                          _DEFAULT_SURFACE_MAP_DENSITY_ATTRIBUTE_EXPECTED_SYNTAX,
                                          _DENSITY_FIELD,
                                          token,
                                          new Double(valueUsed),
                                          unitsUsed};
            _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_NUMBER_UNITS_KEY", args));
          }
        }

        surfaceMapDensityDouble = MathUtil.convertUnits(surfaceMapDensityDouble, _parsedUnits, _DATASTORE_UNITS);
      }

      try
      {
        surfaceMapDensityInNanoMeters = MathUtil.convertDoubleToInt(surfaceMapDensityDouble);
      }
      catch(ValueOutOfRangeException voore)
      {
        ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                      _is.getLineNumber(),
                                                                                      _DEFAULT_SURFACE_MAP_DENSITY_ATTRIBUTE_EXPECTED_SYNTAX,
                                                                                      _DENSITY_FIELD,
                                                                                      voore);
        dex.initCause(voore);
        throw dex;
      }
      //no need to check for _mainReader.ndfsAreWellFormed().
      //surface map density cannot be negative or zero.
      if (surfaceMapDensityInNanoMeters == 0)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                    _is.getLineNumber(),
                                                                    _DEFAULT_SURFACE_MAP_DENSITY_ATTRIBUTE_EXPECTED_SYNTAX,
                                                                    _DENSITY_FIELD,
                                                                    token);
        }
        else
        {
          // warn the user that a positive non zero number was expected
          // assume a value of 1500 mils because this is what the compiler does
          surfaceMapDensityDouble = _DEFAULT_SURFACE_MAP_DENSITY_IN_MILS;
          Object[] args = new Object[]{ _currentFile,
                                        new Integer(_is.getLineNumber()),
                                        _DEFAULT_SURFACE_MAP_DENSITY_ATTRIBUTE_EXPECTED_SYNTAX,
                                        _DENSITY_FIELD,
                                        token,
                                        new Double(surfaceMapDensityDouble),
                                        MathUtilEnum.MILS};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_NUMBER_UNITS_KEY", args));
          surfaceMapDensityDouble = MathUtil.convertUnits(surfaceMapDensityDouble, MathUtilEnum.MILS, _DATASTORE_UNITS);
          try
          {
            surfaceMapDensityInNanoMeters = MathUtil.convertDoubleToInt(surfaceMapDensityDouble);
          }
          catch(ValueOutOfRangeException voore)
          {
            ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                          _is.getLineNumber(),
                                                                                          _DEFAULT_SURFACE_MAP_DENSITY_ATTRIBUTE_EXPECTED_SYNTAX,
                                                                                          _DENSITY_FIELD,
                                                                                          voore);
            dex.initCause(voore);
            throw dex;
          }
        }
      }

      checkForExtraDataAtEndOfLine(_tokenizer, _DEFAULT_SURFACE_MAP_DENSITY_ATTRIBUTE_EXPECTED_SYNTAX);

      _validAttributeLineDefaultSurfaceMapDensity = true;
    }
    else
    {
      if (_mainReader.ndfsAreWellFormed())
        throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _SURFACE_MAP_DENSITY);
      else
        duplicateTokenWarning(_SURFACE_MAP_DENSITY);
    }
  }

  /**
   * parses an attribute line in a board.ndf file that follows the following format
   * .thickness_pads: <focus height> <camera index> <snap margin> <fov> <technique> <ref designator> <pin #> [<ref designator> <pin #> ..]
   *
   * @author George A. David
   */
  private void parseThicknessPads() throws DatastoreException
  {
    Assert.expect(_line != null);

    _line = _line.substring(_THICKNESS_PADS.length());

    // I'm going to change it so that we iterate through all the sideboards.
    // If all side boards are topside only then we need to throw an error
    // and not read in the thickness pads. If one of the sideboards is a bottom side board,
    // then we will need to read in the thickness pads. -> Erica Wheatcroft 4/15/02

    Iterator sideBoardIt = _sameSideBoardInstances.iterator();
    boolean readThicknessPads = false;
    while (sideBoardIt.hasNext() )
    {
      SideBoard currentSideBoard = ( SideBoard ) sideBoardIt.next();
      if ( currentSideBoard.isTopSide() == false )
        readThicknessPads = true;
    }

    if ( readThicknessPads )
    {
      // NOTE: since board thickness point settings are dependent on data lines,
      // temp board thickness point settings are created here. actual ones are
      // created and added in validFileCheck().
      _tokenizer = new StringTokenizer(_line, getDelimiters(), _DONT_RETURN_DELIMITERS);
      // begin parsing thickness pads
      int focusHeightInNanoMeters = parseFocusHeight();
      int cameraIndex = parseCameraIndex();
      int snapMargin = parseSnapMargin();
      int fieldOfView = parseFieldOfView();
      parseAutoFocusTechnique();

      parseThicknessPoints();
      checkForExtraDataAtEndOfLine(_tokenizer, _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX);

    }
  }

  /**
   * .thickness_pads: <focus height> <camera index> <snap margin> <fov> <auto focus technique> <ref designator> <pin #> [<ref designator> <pin #> ..]
   * @author George A. David
   */
  private int parseFocusHeight() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                _FOCUS_HEIGHT_FIELD);
    }

    int focusHeight = 0;
    double focusHeightDouble = parseDouble(token, _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX, _FOCUS_HEIGHT_FIELD);

    // units for this attribute line is mils. convert to _desiredUnits
    focusHeightDouble = MathUtil.convertUnits(focusHeightDouble, _THICKNESS_PAD_UNITS, _DATASTORE_UNITS);
    try
    {
      focusHeight = MathUtil.convertDoubleToInt(focusHeightDouble);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                                                    _FOCUS_HEIGHT_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }

    return focusHeight;
  }

  /**
   * .thickness_pads: <focus height> <camera index> <snap margin> <fov> <auto focus technique> <ref designator> <pin #> [<ref designator> <pin #> ..]
   *
   * @author George A. David
   */
  private int parseCameraIndex() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                _CAMERA_INDEX_FIELD);
    }
    int cameraIndex = parseInt(token,
                               _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                               _CAMERA_INDEX_FIELD);
    return cameraIndex;
  }

  /**
   * .thickness_pads: <focus height> <camera index> <snap margin> <fov> <auto focus technique> <ref designator> <pin #> [<ref designator> <pin #> ..]
   *
   * @author George A. David
   */
  private int parseSnapMargin() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                _SNAP_MARGIN_FIELD);
    }
    int snapMargin = 0;
    double snapMarginDouble = parsePositiveDouble(token,
                                                  _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                  _SNAP_MARGIN_FIELD,
                                                  MathUtilEnum.MILS);
    return snapMargin;
  }

  /**
   * .thickness_pads: <focus height> <camera index> <snap margin> <fov> <auto focus technique> <ref designator> <pin #> [<ref designator> <pin #> ..]
   *
   * @author George A. David
   */
  private int parseFieldOfView() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                _FOV_FIELD);
    }

    int fieldOfView = 0;
    double fieldOfViewDouble = 0.0;
    Exception ex = null;
    try
    {
      fieldOfViewDouble = StringUtil.convertStringToPositiveDouble(token);
    }
    catch(BadFormatException bfe)
    {
      ex = bfe;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        ExpectedPositiveNonZeroNumberDatastoreException dex = new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                                                                  _is.getLineNumber(),
                                                                                                                  _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                                                                                  _FOV_FIELD,
                                                                                                                  token);
        dex.initCause(ex);
        throw dex;
      }
      else
      {
        // the legacy compiler used the c++ atoi() function, which converted as
        // much of the string as possible to a float, and that is what we will do here.
        ex = null;
        try
        {
          fieldOfViewDouble = StringUtil.legacyConvertStringToPositiveDouble(token);
        }
        catch(BadFormatException anotherBfe)
        {
          ex = anotherBfe;
        }
        catch (ValueOutOfRangeException vex)
        {
          ex = vex;
        }
        if (ex != null)
        {
          // the legacy compiler used the c++ built-in function atoi() to convert
          // a string to an integer. if the string did not begin with a positive or
          // negative integer.
          fieldOfViewDouble = 0.0;
        }
        // warn the user that we expected a positive non zero number
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                      _FOV_FIELD,
                                      token,
                                      new Double(fieldOfViewDouble)};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_NUMBER_KEY", args));
      }
    }
    try
    {
      fieldOfView = MathUtil.convertDoubleToInt(fieldOfViewDouble);
    }
    catch(ValueOutOfRangeException voore)
    {
      DatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                     _is.getLineNumber(),
                                                                     _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                                     _FOV_FIELD,
                                                                     voore);
      dex.initCause(voore);
      throw dex;
    }

    if (fieldOfView <= 0)
    {
      throw new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                _is.getLineNumber(),
                                                                _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                                _FOV_FIELD,
                                                                token);
    }

    return fieldOfView;
  }

  /**
   * .thickness_pads: <focus height> <camera index> <snap margin> <fov> <auto focus technique> <ref designator> <pin #> [<ref designator> <pin #> ..]
   *
   * @author George A. David
   */
  private void parseAutoFocusTechnique() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                _AUTO_FOCUS_TECHNIQUE_FIELD);
    }
  }

  /**
   * .thickness_pads: <focus height> <camera index> <snap margin> <fov> <auto focus technique> <ref designator> <pin #> [<ref designator> <pin #> ..]
   *
   * @author George A. David
   */
  private void parseThicknessPoints() throws DatastoreException
  {
    if (_tokenizer.hasMoreTokens() == false)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                  _REFERENCE_DESIGNATOR_FIELD);
      }
      else
      {
        // the compiler will ignore this line if the reference designator is missing.
        _validLine = false;
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                     _REFERENCE_DESIGNATOR_FIELD};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_FIELD_LINE_IGNORED_KEY", args));
      }
    }
    else
    {
      while (_tokenizer.hasMoreTokens() && _validLine)
      {
        String referenceDesignator = ParseUtil.getNextToken(_tokenizer);
        //check that component is at least in componen.ndf
        if (_mainReader.doesComponentExist(referenceDesignator) == false)
        {
          if (_mainReader.ndfsAreWellFormed())
          {
            throw new BadComponentReferenceDatastoreException(_currentFile,
                                                              _is.getLineNumber(),
                                                              _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                              _REFERENCE_DESIGNATOR_FIELD,
                                                              referenceDesignator,
                                                              _currentFile,
                                                              FileName.getLegacyComponentNdfFullPath(_mainReader.getPanelName(),
                                                              _mainReader.getCurrentSideBoardName()));
          }
          else
          {
            //create warning component not found for thickness pad
            Object[] args = new Object[]{_currentFile,
                                         new Integer(_is.getLineNumber()),
                                         _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                         _REFERENCE_DESIGNATOR_FIELD,
                                         referenceDesignator,
                                         _currentFile,
                                         FileName.getLegacyComponentNdfFullPath(_mainReader.getPanelName(),
                                         _mainReader.getCurrentSideBoardName())};
            _warnings.add(new LocalizedString("DS_NDF_WARNING_BAD_COMPONENT_REFERENCE_KEY", args));
            // grab the next token and throw it away. the compiler assumes it is a pin number.
            ParseUtil.getNextToken(_tokenizer);
          }
        }
        else
        {
          String pinName = null;
          if (_tokenizer.hasMoreTokens())
          {
            pinName = parsePinName();
          }
          else
          {
            if (_mainReader.ndfsAreWellFormed())
            {
              throw new UnexpectedEolDatastoreException(_currentFile,
                                                        _is.getLineNumber(),
                                                        _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                        _PIN_NAME_FIELD);
            }
            else
            {
              // if not specified, assume 0
              _validLine = false;
              Object[] args = new Object[]{_currentFile,
                                           new Integer(_is.getLineNumber()),
                                           _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                           _PIN_NAME_FIELD};
              _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_FIELD_LINE_IGNORED_KEY", args));
            }
          }
          if (_validLine)
          {
            Component component = _mainReader.getComponent(referenceDesignator);
          }
        }
      }
    }
  }

  /**
   * .thickness_pads: <focus height> <camera index> <snap margin> <fov> <auto focus technique> <ref designator> <pin #> [<ref designator> <pin #> ..]
   *
   * @author George A. David
   */
  private String parsePinName() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);

    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                                  _PIN_NAME_FIELD);
      }
      else
      {
        // if not specified, assume 0
        token = "0";
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _THICKNESS_PADS_ATTRIBUTE_EXPECTED_SYNTAX,
                                     _PIN_NAME_FIELD,
                                     token};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_KEY", args));
      }
    }

    return token;
  }


  /**
   * @author Keith Lee
   */
  protected void postProcessData() throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);

    // if a required line is missing
    if (_validAttributeLineDimensions == false)
    {
       if (isValidRequiredDataLineForNoBoard () == false) //  XCR1163 Jack Hwee - fix to load single-sided panel ndf.
       {
      throw new MissingTokenDatastoreException(_currentFile, _DIMENSIONS);
       }
    }

    if (_validRequiredDataLine == false)
    {
       if (isValidRequiredDataLineForNoBoard () == false)  //  XCR1163 Jack Hwee - fix to load single-sided panel ndf.
       {
      throw new MissingRequiredLineDatastoreException(_currentFile, _EXPECTED_SYNTAX);
       }
    }

    // check that all components in componen.ndf are included in board.ndf
    Collection<Component> componentCollection = _mainReader.getComponents();
    for (Component component : componentCollection)
    {
      Assert.expect(_sameSideBoardInstances.size() > 0);
      SideBoard sideBoard = (SideBoard)_sameSideBoardInstances.get(0);
      if (sideBoard.hasComponent(component.getReferenceDesignator()) == false)
      {
        if (_mainReader.ndfsAreWellFormed() || _mainReader.isComponentInspected(component))
        {
          String panelName = _mainReader.getPanelName();
          String sideBoardName = _mainReader.getCurrentSideBoardName();
          String componentFile = FileName.getLegacyComponentNdfFullPath(panelName, sideBoardName);
          throw new MissingComponentDatastoreException(_currentFile,
                                                       componentFile,
                                                       _EXPECTED_SYNTAX,
                                                       component.getReferenceDesignator());
        }
        else
        {
          String panelName = _mainReader.getPanelName();
          String sideBoardName = _mainReader.getCurrentSideBoardName();
          String componentFile = FileName.getLegacyComponentNdfFullPath(panelName, sideBoardName);
          _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_COMPONENT_KEY",
                         new Object[]{_currentFile,
                                      componentFile,
                                      _EXPECTED_SYNTAX,
                                      component.getReferenceDesignator()}));
        }
      }
    }

    Iterator it = _sameSideBoardTypes.iterator();
    while (it.hasNext())
    {
      SideBoardType sideBoardType = (SideBoardType)it.next();
      _mainReader.addSideBoardTypeToLegacyBoardNdfUnits(sideBoardType, _parsedUnits);
    }
  }

  /**
   * @author Keith Lee
   */
  protected void resetAfterParsing()
  {
    initializeBeforeParsing();

    _sameSideBoardInstances = null;
    _sameSideBoardTypes = null;
    _boardThicknessPointSettingList.clear();
    _legacyAlignmentGroupNumberToAlignmentGroupInfoListMap.clear();
    _legacyAlignmentGroupNumberToLineNumberMap.clear();
    _alignXinfoList.clear();
 }
}

