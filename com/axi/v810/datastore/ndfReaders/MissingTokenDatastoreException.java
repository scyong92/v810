package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when a token is missing.
 * @author Vincent Wong
 */
public class MissingTokenDatastoreException extends DatastoreException
{
  /**
   * @param fileName is the file that the token belongs in
   * @param token is the token that is missing
   * @author Vincent Wong
   */
  public MissingTokenDatastoreException(String fileName, String token)
  {
    super(new LocalizedString("DS_NDF_ERROR_MISSING_TOKEN_KEY", new Object[] {fileName, token}));

    Assert.expect(fileName != null);
    Assert.expect(token != null);
  }
}
