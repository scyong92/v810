package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when an alignment point specified for one board side references a component on a different board side.
 * @author Bill Darbie
 */
public class InvalidAlignmentPointNotOnCurrentSideDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public InvalidAlignmentPointNotOnCurrentSideDatastoreException(String fileName,
                                                                 int lineNumber,
                                                                 String referenceDesignator)
  {
    super(new LocalizedString("DS_ERROR_INVALID_ALIGNMENT_POINT_NOT_ON_CURRENT_SIDE_KEY",
          new Object[]{fileName,
                       new Integer(lineNumber),
                       referenceDesignator}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(referenceDesignator != null);
  }
}
