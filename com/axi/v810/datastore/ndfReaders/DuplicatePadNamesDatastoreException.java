package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import java.io.*;

import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * Throw this exception if the same pad has been defined more than once.
 * @author Bill Darbie
 */
public class DuplicatePadNamesDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public DuplicatePadNamesDatastoreException(String fileName,
                                             int lineNumber,
                                             String landPatternName,
                                             String duplicatePadName)
  {
    super(new LocalizedString("DS_NDF_ERROR_DUPLICATE_PAD_KEY",
                              new Object[]{ fileName, new Integer(lineNumber), landPatternName, duplicatePadName }));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(landPatternName != null);
    Assert.expect(duplicatePadName != null);
  }
}
