package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when a duplicate token is found in an ndf file.
 * @author Bill Darbie
 */
public class DuplicateTokenDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public DuplicateTokenDatastoreException(String fileName, int lineNum, String duplicateToken)
  {
    super(new LocalizedString("DS_NDF_ERROR_DUPLICATE_TOKEN_KEY", new Object[]{fileName, new Integer(lineNum), duplicateToken}));
    Assert.expect(fileName != null);
    Assert.expect(lineNum > 0);
    Assert.expect(duplicateToken != null);
  }
}
