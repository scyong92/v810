package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class is thrown when some component information is missing
 * @author George A. David
 */
public class MissingComponentInformationDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public MissingComponentInformationDatastoreException(String boardFile,
                                                       int lineNumber,
                                                       String referenceDesignator,
                                                       String componentFile)
  {
    super(new LocalizedString("DS_ERROR_MISSING_COMPONENT_INFORMATION_KEY",
          new Object[]{boardFile,
                       new Integer(lineNumber),
                       referenceDesignator,
                       componentFile}));

    Assert.expect(boardFile != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(referenceDesignator != null);
    Assert.expect(componentFile != null);
  }
}
