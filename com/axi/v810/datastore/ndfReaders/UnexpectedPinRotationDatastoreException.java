package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * This class gets thrown when a a pin is found at an invalid orientation.
 * @author Tony Turner
 */
public class UnexpectedPinRotationDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param fileName is the name of the package file that the bad pin orientation was found in
   * @param lineNumber is the line number in the file where the bad pin orientation was found
   * @param pinOrientation is the orientation angle in degrees
   * @author Tony Turner
   */
  public UnexpectedPinRotationDatastoreException(String fileName,
                                                 int lineNumber,
                                                 String expectedSyntax,
                                                 String invalidField,
                                                 String pinOrientation)
  {
    super(new LocalizedString("DS_NDF_ERROR_UNEXPECTED_PIN_ROTATION_KEY", new Object[]{fileName,
                                                                                          new Integer(lineNumber),
                                                                                          expectedSyntax,
                                                                                          invalidField,
                                                                                          pinOrientation}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidField != null);
    Assert.expect(pinOrientation != null);
  }

  /**
   * Construct the class.
   * @param fileName is the name of the package file that the bad pin orientation was found in
   * @param lineNumber is the line number in the file where the bad pin orientation was found
   * @param pinOrientation is the orientation angle in degrees
   * @author Tony Turner
   */
  public UnexpectedPinRotationDatastoreException(String fileName, int lineNumber, String pinOrientation)
  {
    super(new LocalizedString("DS_ERROR_UNEXPECTED_PIN_ORIENTATION_KEY", new Object[]{fileName,
                                                                                      new Integer(lineNumber),
                                                                                      pinOrientation}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(pinOrientation != null);
  }
}
