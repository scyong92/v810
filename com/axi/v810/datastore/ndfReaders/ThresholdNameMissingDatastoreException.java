package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when we find something besides a valid threshold name.
 * @author Bill Darbie
 */
public class ThresholdNameMissingDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public ThresholdNameMissingDatastoreException(String fileName,
                                                int lineNumber)
  {
    super(new LocalizedString("DS_ERROR_THRESHOLD_NAME_MISSING_KEY", new Object[]{fileName, new Integer(lineNumber)}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
  }
}
