package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when an invalid attribute is encountered
 * @author George A. David
 */
public class InvalidAttributeDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public InvalidAttributeDatastoreException(String fileName,
                                            int lineNumber,
                                            String invalidAttribute,
                                            String validAttributes)
  {
    super(new LocalizedString("DS_NDF_ERROR_INVALID_ATTRIBUTE_KEY",
          new Object[] {fileName,
                        new Integer(lineNumber),
                        invalidAttribute,
                        validAttributes}));

    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(invalidAttribute != null);
    Assert.expect(validAttributes != null);
  }
}
