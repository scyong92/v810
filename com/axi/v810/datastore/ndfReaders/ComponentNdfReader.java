package com.axi.v810.datastore.ndfReaders;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * Parses in the componen.ndf file
 *
 * This class creates the following Datastore objects:
 *   Component
 *     CompPackage
 *     ComponentType
 *
 *   ComponentType
 *     referenceDesignator
 *     CompPackage
 *     CompTypeSettings
 *
 * This class sets variables in these objects:
 *
 *   ComponentTypeSettings
 *     isInspected
 *     isLoaded
 *
 *   Pad
 *     PackagePin
 *
 *   CompPackage
 *     ComponentTypes
 *
 * This class requires the following to already be available to it from MainReader:
 *   none
 *
 * This class puts the following into MainReader:
 *   Component
 *   Component to PadTypeEnum
 *   CustomAlgorithmFamily
 *
 * @author Andy Mechtenberg
 */
class ComponentNdfReader extends NdfReader
{
  private static final boolean _DONT_RETURN_DELIMITERS = false;
  private static final String _NO_DELIMITERS = "";

  //"@<ref. des.> <test status> <package name> <technology> <subtype number>"
  private static final String _EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_COMPONENT_EXPECTED_SYNTAX_KEY");
  private static final String _REFERENCE_DESIGNATOR_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_REFERENCE_DESIGNATOR_KEY"); //"<ref. des.>";
  private static final String _TEST_STATUS_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_TEST_STATUS_KEY"); //"<test status>";
  private static final String _PACKAGE_NAME_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PACKAGE_NAME_KEY"); //"<package name>";
  private static final String _TECHNOLOGY_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_TECHNOLOGY_KEY"); //"<technology>";
  private static final String _SUBTYPE_NUMBER_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_SUBTYPE_NUMBER_KEY"); //"<subtype number>";
  private static final String _BOARD_NAME_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_NAME_KEY"); //"<board name>";
  private static final String _BOARD_ID_EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_BOARD_ATTRIBUTE_BOARD_ID_EXPECTED_SYNTAX_KEY"); //".BOARD_ID <board name>"

  private static final String _TRUE = "True";
  private static final String _FALSE = "False";
  private static final String _PARTIAL = "Partial";
  private static final String _SURFACE_MOUNT = "SM";
  private static final String _THROUGH_HOLE = "TH";
  private static final String _BOARD_ID = ".BOARD_ID:";

  private static final String _DEFAULT_CUSTOMIZED_NAME = "";
  private static final String _DEFAULT_USER_COMMENT = "";

  // lines that can be set/specified only once per componen.ndf
  //_checkSumFound inherited from NdfReader
  // required line that can be set/specified multiple times per componen.ndf
  private boolean _validRequiredDataLine = false;
  private boolean _validAttributeLineBoardID = false;

  private int _numUnitLines = 0;

  private StringTokenizer _tokenizer;
  private String _line;

  private String _referenceDesignator;
  private boolean _inspectComponent;
  private String _packageName;
  private PadTypeEnum _padTypeEnum;
  private int _subtypeNumber;

  private Panel _panel;


  /**
   * Constructor for the ComponentNdfReader.
   *
   * @author Keith Lee
   */
  ComponentNdfReader(MainReader mainReader)
  {
    super(mainReader);
  }

  /**
   * @author Bill Darbie
   */
  void setProject(Project project)
  {
    Assert.expect(project != null);
    _panel = project.getPanel();
  }

  /**
   * @author Keith Lee
   */
  protected void initializeBeforeParsing()
  {
    _checkSumFound = false;
    _numUnitLines = 0;
    _validAttributeLineBoardID = false;
    _validRequiredDataLine = false;
  }

  /**
   * @author Keith Lee
   */
  protected void resetAfterParsing()
  {
    // do nothing
  }

  /**
   * Parses a data line in a componen.ndf file that follows the following format
   * "@<ref designator> <test status> <package name> <technology> [<subtype>]"
   *
   * @author Andy Mechtenberg
   */
  protected void parseDataLine(String line) throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);
    Assert.expect(line != null);
    _line = line;

    parseInLine();
    populateClassesWithData();
  }

  /**
   * Parse in one data line
   * @author Andy Mechtenberg
   */
  private void parseInLine() throws DatastoreException
  {
    // parse in each token from the line
    _tokenizer = new StringTokenizer(_line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    _referenceDesignator = parseReferenceDesignator();
    _inspectComponent = parseTestStatus();
    _packageName = parsePackageName();
    _padTypeEnum = parsePadTypeEnum();
    _subtypeNumber = parseSubtypeNumber();
    checkForExtraDataAtEndOfLine(_tokenizer, _EXPECTED_SYNTAX);
  }


  /**
   * Take the data read in on a single line and populate the appropriate classes
   * with it.
   * @author Andy Mechtenberg
   */
  private void populateClassesWithData() throws DatastoreException
  {
    boolean validCompPackageAlgorithmFamily = false;

    // create the new component
    Component component = new Component();
    ComponentType componentType = new ComponentType();
    componentType.setPanel(_panel);

    component.setComponentType(componentType);
    componentType.setReferenceDesignator(_referenceDesignator);

    /**
     * @author Jack Hwee
     */
    if ((NdfReader.is5dxNdfFlag()) || (NdfReader.is5dxProjectFlag()))
    component.setFivedxPackageName(_packageName);

    // this mapping is needed by BoardNdfReader
    _mainReader.addComponentToPadTypeEnumMap(component, _padTypeEnum);

    ComponentTypeSettings componentTypeSettings = new ComponentTypeSettings();
    componentType.setComponentTypeSettings(componentTypeSettings);

    // check that we do not have a duplicate component
    if (_mainReader.doesComponentExist(_referenceDesignator) == true)
    {
      // we have a duplicate component
      throw new DuplicateComponentDatastoreException(_currentFile, _is.getLineNumber(), _referenceDesignator);
    }
    
    if (NdfReader.get5dxNdfWithDiffSubtype() == '1') 
    {
      componentType.set5dxNewSubtypeName(_packageName + "_" + _subtypeNumber);
    }

    componentTypeSettings.setLoaded(true);
    // set inspect mode
    // we cannot set inspection of a component now, this method requires that all PadTypes already
    // exist since they are really what gets set to inspect or not inspect.  The list
    // of Components that are inspected is saved in _mainReader and will be used later
    // to make the componentTypeSettings.setInspected() call
    _mainReader.addComponent(component);
    if (_inspectComponent)
      _mainReader.addInspectedComponent(component);

    _validRequiredDataLine = true;
  }

  /**
   * Called after all parsing is done.
   * @author Keith Lee
   */
  protected void postProcessData() throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);

    // since there are no required specific lines, we check for ANY valid data lines

    if (_validRequiredDataLine == false)
    {
      if (isValidRequiredDataLineForNoBoard () == false)  //   XCR1163 Jack Hwee - fix to load single-sided panel ndf.
      {
      throw new MissingRequiredLineDatastoreException(_currentFile, _EXPECTED_SYNTAX);
      }
    }
  }

  /**
   * Parses an attribute line in a componen.ndf file.
   * Sometimes a .UNIT line is put in -- no harm
   * Also, you can get a .BOARD_ID attribute line -- again, this is optional and essentially worthless
   * Also, there is a .CHECKSUM
   *
   * @param line - the line of package attributes in the componen.ndf to parse.
   * @author Keith Lee
   */
  protected void parseAttributeLine(String line) throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);
    Assert.expect(line != null);

    String upperCaseLine = line.toUpperCase();
    if (upperCaseLine.startsWith(CHECKSUM))
    {
      line = line.substring(CHECKSUM.length());
      parseAttributeLineCheckSum(line);
    }
    else if (upperCaseLine.startsWith(NdfReader.getUnitToken()))
    {
      // .UNIT: is not needed since no values in this value are unit dependent. but it is often
      // found in code, so we will only generate a warning if it occurs more than once.
      _numUnitLines++;
      if (_numUnitLines > 1)
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), NdfReader.getUnitToken());
        else
          duplicateTokenWarning(NdfReader.getUnitToken());
      }
    }
    else if (upperCaseLine.startsWith(_BOARD_ID))
    {
      // .BOARD_ID is optional
      if (_validAttributeLineBoardID == false)
      {
        line = line.substring(_BOARD_ID.length());
        parseAttributeLineBoardId(line);
      }
      else
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), _BOARD_ID);
        else
          duplicateTokenWarning(_BOARD_ID);
      }
    }
    else
    {
      String token = line;
      int index = line.indexOf(":");
      if (index != -1)
        token = line.substring(0, index + 1);

      if (_mainReader.ndfsAreWellFormed())
      {
        // invalid or unknown attribute line
        String expected = CHECKSUM;
        throw new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), expected, token);
      }
      else
      {
        unrecognizedTokenWarning(token);
      }
    }
  }

  /**
   * parses an attribute line in a componen.ndf file that follows the following format
   * .board_id: <name>
   *
   * @author Keith Lee
   */
  private void parseAttributeLineBoardId(String line) throws DatastoreException
  {
    Assert.expect(line != null);

    // board name
    // this is strictly informational. the actual board name comes from panel.ndf
    // and can also be found in the dir_id.dat file found in the same directory
    // as the componen.ndf file

    if (_mainReader.ndfsAreWellFormed())
    {
      StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
      String token = ParseUtil.getNextToken(st);
      if (token == null)
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _BOARD_ID_EXPECTED_SYNTAX,
                                                  _BOARD_NAME_FIELD);
      }

      token = ParseUtil.getNextToken(st);
      if (isValidComment(token) == false)
      {
        String actual = token;
        throw new ExpectedEolDatastoreException(_currentFile, _is.getLineNumber(), _BOARD_ID_EXPECTED_SYNTAX, actual);
      }
    }

    _validAttributeLineBoardID = true;
  }

  /**
   * @author Keith Lee
   */
  protected void parseLinesRequiredByOtherLines()
  {
    // do nothing. only data lines should be in this file.
  }

  /**
   * "@<ref designator> <test status> <package name> <technology> [<subtype>]"
   *
   * @author Andy Mechtenberg
   */
  private String parseReferenceDesignator() throws DatastoreException
  {
    String refDes = ParseUtil.getNextToken(_tokenizer);

    if (refDes == null)
    {
      // no need to check for _mainReader.ndfsAreWellFormed(). if no reference
      // designator, then no component type.
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _REFERENCE_DESIGNATOR_FIELD);

    }
    return refDes;
  }

  /**
   * "@<ref designator> <test status> <package name> <technology> [<subtype>]"
   *
   * @author Andy Mechtenberg
   */
  private boolean parseTestStatus() throws DatastoreException
  {
    String testStatusStr = ParseUtil.getNextToken(_tokenizer);
    if (testStatusStr == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _TEST_STATUS_FIELD);
    }
    boolean inspectComponent = false;

    // component inspect value
    if (testStatusStr.equalsIgnoreCase(_TRUE))
    {
      inspectComponent = true;
    }
    else if (testStatusStr.equalsIgnoreCase(_FALSE))
    {
      inspectComponent = false;
    }
    else if (testStatusStr.equalsIgnoreCase(_PARTIAL))
    {
      inspectComponent = true;
    }
    else  // not set to "true" "false" or "partial"
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new InvalidComponentTestStatusDatastoreException(_currentFile,
                                                               _is.getLineNumber(),
                                                               _EXPECTED_SYNTAX,
                                                               _TEST_STATUS_FIELD,
                                                               testStatusStr);
      }
      else
      {
        // legacy does not explictly check for false. it only checks for true or
        // partial. if not one of those, then it assumes false.
        inspectComponent = false;
        _warnings.add(new LocalizedString("DS_NDF_WARNING_INVALID_COMPONENT_TEST_STATUS_KEY",
                      new Object[]{_currentFile,
                                   new Integer(_is.getLineNumber()),
                                   _EXPECTED_SYNTAX,
                                   _TEST_STATUS_FIELD,
                                   testStatusStr}));
      }
    }

    return inspectComponent;
  }

  /**
   * "@<ref designator> <test status> <package name> <technology> [<subtype>]"
   *
   * @author Andy Mechtenberg
   */
  private String parsePackageName() throws DatastoreException
  {
    String packageName = ParseUtil.getNextToken(_tokenizer);

    if (packageName == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _PACKAGE_NAME_FIELD);
    }
    return packageName;
  }

  /**
   * "@<ref designator> <test status> <package name> <technology> [<subtype>]"
   *
   * @author Andy Mechtenberg
   */
  private PadTypeEnum parsePadTypeEnum() throws DatastoreException
  {
    String technologyStr = ParseUtil.getNextToken(_tokenizer);

    if (technologyStr == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _TECHNOLOGY_FIELD);
    }

    PadTypeEnum padTypeEnum;
    if (technologyStr.equalsIgnoreCase(_SURFACE_MOUNT))
    {
      padTypeEnum = PadTypeEnum.SURFACE_MOUNT;
    }
    else if (technologyStr.equalsIgnoreCase(_THROUGH_HOLE))
    {
      padTypeEnum = PadTypeEnum.THROUGH_HOLE;
    }
    else
    {
      // no need to check for _mainReader.ndfsAreWellFormed(). must have a
      // valid component type.
      throw new InvalidTechnologyDatastoreException(_currentFile,
                                                    _is.getLineNumber(),
                                                    _EXPECTED_SYNTAX,
                                                    _TECHNOLOGY_FIELD,
                                                    technologyStr);
    }

    return padTypeEnum;
  }

  /**
   * "@<ref designator> <test status> <package name> <technology> [<subtype>]"
   *
   * @author Andy Mechtenberg
   */
  private int parseSubtypeNumber() throws DatastoreException
  {
    String subtypeStr = ParseUtil.getNextToken(_tokenizer);
    int subtype = 0;

    //component subtype is optional pre 8.0, required post 8.0
    if (subtypeStr == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _EXPECTED_SYNTAX,
                                                  _SUBTYPE_NUMBER_FIELD);
      }
      else
      {
        // Warning reads:  A subtype was not specified for component <2> on line <1> in file <0>.  Valid subtype values
        //                 range from 0 to 255.  The default subtype value <3> will be used.
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _referenceDesignator,
                                     new Integer(0)};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_SUBTYPE_KEY", args));


        subtypeStr = "0";
      }
    }

    subtype = parseInt(subtypeStr, _EXPECTED_SYNTAX, _SUBTYPE_NUMBER_FIELD, getMinLegacySubtype(), getMaxLegacySubtype());

    return subtype;
  }
}
