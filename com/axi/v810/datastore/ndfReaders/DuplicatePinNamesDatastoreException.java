package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import java.io.*;

import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * Throw this exception if the same pin has been defined more than once.
 * @author Bill Darbie
 */
public class DuplicatePinNamesDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public DuplicatePinNamesDatastoreException(String fileName,
                                             int lineNumber,
                                             String pinName)
  {
    super(new LocalizedString("DS_NDF_ERROR_DUPLICATE_PIN_KEY",
                              new Object[]{ fileName, new Integer(lineNumber), pinName}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(pinName != null);
  }
}
