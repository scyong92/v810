package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * If the algorithm family name is invalid, this exception should be thrown.
 * @author Bill Darbie
 */
class InvalidAlgorithmFamilyNameDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  InvalidAlgorithmFamilyNameDatastoreException(String fileName,
                                               int lineNumber,
                                               String badName)

  {
    super(new LocalizedString("DS_ERROR_INVALID_ALGORITHM_FAMILY_NAME_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           badName}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(badName != null);
  }

  /**
   * @author Bill Darbie
   */
  InvalidAlgorithmFamilyNameDatastoreException(String fileName,
                                               int lineNumber,
                                               String expectedSyntax,
                                               String invalidFieldName,
                                               String invalidFieldData)

  {
    super(new LocalizedString("DS_NDF_ERROR_INVALID_ALGORITHM_FAMILY_NAME_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           expectedSyntax,
                                           invalidFieldName,
                                           invalidFieldData}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidFieldName != null);
    Assert.expect(invalidFieldData != null);
  }
}
