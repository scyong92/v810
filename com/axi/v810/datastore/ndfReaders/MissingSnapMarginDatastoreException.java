package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * If the snap margin setting is missing exception should be thrown.
 * @author Bill Darbie
 */
public class MissingSnapMarginDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public MissingSnapMarginDatastoreException(String fileName,
                                             int lineNumber)

  {
    super(new LocalizedString("DS_ERROR_MISSING_SNAP_MARGIN_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber)}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
  }
}
