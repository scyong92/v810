package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when the .align1:, .align2:, or .align3: attribute is used.
 * @author George A. David
 */
public class AlignXunsupportedDatastoreException extends DatastoreException
{
  /**
   * @author George a. David
   */
  public AlignXunsupportedDatastoreException(String fileName,
                                             int lineNumber,
                                             int indexNumber)
  {
    super(new LocalizedString("DS_NDF_ERROR_ALIGN_X_UNSUPPORTED_KEY", new Object[] {fileName,
                                                                          new Integer(lineNumber),
                                                                          new Integer(indexNumber)}));

    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(indexNumber >= 1);
    Assert.expect(indexNumber <= 3);
  }
}
