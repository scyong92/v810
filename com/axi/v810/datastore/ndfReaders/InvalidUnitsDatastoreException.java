package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when an invalid unit is encountered.
 * @author George A. David
 */
public class InvalidUnitsDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public InvalidUnitsDatastoreException(String fileName,
                                        int lineNumber,
                                        String expectedSyntax,
                                        String invalidField,
                                        String actualValue)
  {
    super(new LocalizedString("DS_NDF_ERROR_INVALID_UNITS_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           expectedSyntax,
                                           invalidField,
                                           actualValue}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidField != null);
    Assert.expect(actualValue != null);
  }
}
