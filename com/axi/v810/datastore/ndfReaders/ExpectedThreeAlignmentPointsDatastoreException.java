package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when one or two alignment points are specified.  Either 0 or 3 are required.
 * @author Bill Darbie
 */
public class ExpectedThreeAlignmentPointsDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public ExpectedThreeAlignmentPointsDatastoreException(String fileName, String expectedSyntax)
  {
    super(new LocalizedString("DS_NDF_ERROR_EXPECTED_THREE_ALIGNMENT_POINTS_KEY", new Object[]{fileName, expectedSyntax}));
    Assert.expect(fileName != null);
    Assert.expect(expectedSyntax != null);
  }
}
