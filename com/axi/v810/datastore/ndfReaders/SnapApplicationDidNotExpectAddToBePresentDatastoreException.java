package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown if the snap application for a .FOV: attribute line in fov_data.ndf
 * was found to be set to "ADD", whereas the snap application field was not set in other
 * attribute lines where the Field Of View and Image Resolution are the same.
 * @author Tony Turner
 */
public class SnapApplicationDidNotExpectAddToBePresentDatastoreException extends DatastoreException
{

 /**
  * @author Tony Turner
  */
  public SnapApplicationDidNotExpectAddToBePresentDatastoreException(String fileName,
                                                                     int lineNumber,
                                                                     int fieldOfView,
                                                                     int resolution)
  {
    super(new LocalizedString("DS_ERROR_SNAP_APPLICATION_DID_NOT_EXPECT_ADD_TO_BE_PRESENT_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           new Integer(fieldOfView),
                                           new Integer(resolution)}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(fieldOfView > 0);
    Assert.expect(resolution > 0);
  }

}
