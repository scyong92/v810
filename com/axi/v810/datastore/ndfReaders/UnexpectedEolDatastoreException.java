package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when an end of line unexpectedly happens.
 * @author Keith Lee
 */
public class UnexpectedEolDatastoreException extends DatastoreException
{
  /**
   * @param fileName is the name of the file that is corrupt.
   * @param lineNumber is the line number in the file where the eol occurred.
   * @param expectedSyntax is the String representing what was expected instead of the eol.
   * @param missingItem is the item that was expected, but is missing
   * @author Bill Darbie
   */
  public UnexpectedEolDatastoreException(String fileName,
                                         int lineNumber,
                                         String expectedSyntax,
                                         String missingItem)
  {
    super(new LocalizedString("DS_NDF_ERROR_LINE_MISSING_DATA_KEY", new Object[]{fileName,
                                                                                 new Integer(lineNumber),
                                                                                 expectedSyntax,
                                                                                 missingItem}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(missingItem != null);
  }

  /**
   * Make sure the expected parameter passed in is NOT an english hard-coded string.  Only proper nouns should be passed in
   * so we can support localization properly!
   * @param fileName is the name of the file that is corrupt.
   * @param lineNumber is the line number in the file where the eol occurred.
   * @param expected is the String representing what was expected instead of the eol.
   * @author Keith Lee
   */
  public UnexpectedEolDatastoreException(String fileName, int lineNumber, String expected)
  {
    super(new LocalizedString("DS_ERROR_FILE_UNEXPECTED_EOL_KEY", new Object[]{fileName, new Integer(lineNumber), expected}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expected != null);
  }

  /**
   * Use the previous constuctor when you can.
   * @author Bill Darbie
   */
  public UnexpectedEolDatastoreException(String fileName, int lineNumber)
  {
    super(new LocalizedString("DS_ERROR_FILE_UNEXPECTED_EOL_NO_EXPECT_KEY", new Object[]{fileName, new Integer(lineNumber)}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
  }
}
