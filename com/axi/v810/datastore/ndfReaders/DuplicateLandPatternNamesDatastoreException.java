package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import java.io.*;

import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * This is thrown if there is a SurfaceMount and ThroughHole LandPattern with the same name.
 * @author Keith Lee
 */
public class DuplicateLandPatternNamesDatastoreException extends DatastoreException
{
  /**
   * @param duplicateLandPatternName - the name shared by the through hole and surface mount land patterns.
   * @author Keith Lee
   */
  public DuplicateLandPatternNamesDatastoreException(String duplicateLandPatternName,
                                                     String surfaceMountLandPatternFileName,
                                                     String throughHoleLandPatternFileName)
  {
    super(new LocalizedString("DS_NDF_ERROR_DUPLICATE_LAND_PATTERN_NAMES_KEY",
                              new Object[]{ duplicateLandPatternName, surfaceMountLandPatternFileName, throughHoleLandPatternFileName}));
    Assert.expect(duplicateLandPatternName != null);
    Assert.expect(surfaceMountLandPatternFileName != null);
    Assert.expect(throughHoleLandPatternFileName != null);
  }
}
