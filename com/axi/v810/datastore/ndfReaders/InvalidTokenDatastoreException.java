package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when a token is invalid
 * @author Bill
 */
public class InvalidTokenDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public InvalidTokenDatastoreException(String fileName, int lineNumber, String actual, String expected)
  {
    super(new LocalizedString("DS_ERROR_INVALID_TOKEN_KEY", new Object[] {fileName,
                                                                          new Integer(lineNumber),
                                                                          actual,
                                                                          expected}));

    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(actual != null);
    Assert.expect(expected != null);
  }
}
