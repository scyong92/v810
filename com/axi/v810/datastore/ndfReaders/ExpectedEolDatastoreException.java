package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when an end of line unexpectedly does not happen.
 * @author Keith Lee
 */
public class ExpectedEolDatastoreException extends DatastoreException
{
  /**
   * @param fileName is the name of the file that is corrupt.
   * @param lineNum is the line number in the file where the eol did not occur.
   * @param actual is the String representing what has found instead of the eol.
   * @author Keith Lee
   */
  public ExpectedEolDatastoreException(String fileName, int lineNum, String expectedSyntax, String actual)
  {
    super(new LocalizedString("DS_NDF_ERROR_EXPECTED_EOL_KEY", new Object[]{fileName,
                                                                            new Integer(lineNum),
                                                                            expectedSyntax,
                                                                            actual}));
    Assert.expect(fileName != null);
    Assert.expect(lineNum > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(actual != null);
  }

  /**
   * @param fileName is the name of the file that is corrupt.
   * @param lineNum is the line number in the file where the eol did not occur.
   * @param actual is the String representing what has found instead of the eol.
   * @author Keith Lee
   */
  public ExpectedEolDatastoreException(String fileName, int lineNum, String actual)
  {
    super(new LocalizedString("DS_ERROR_FILE_EXPECTED_EOL_KEY", new Object[]{fileName,
                                                                             new Integer(lineNum),
                                                                             actual}));
    Assert.expect(fileName != null);
    Assert.expect(lineNum > 0);
    Assert.expect(actual != null);
  }
}
