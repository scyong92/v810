package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This exception will get thrown whenever we have encountered a invalid attribute value in a ndf.
 * @author Erica Wheatcroft
 */
public class InvalidAttributeValueDatastoreException extends DatastoreException
{
  /**
   * @author Erica Wheatcroft
   */
  public InvalidAttributeValueDatastoreException(String fileName,
                                                 int lineNumber,
                                                 String attributeName,
                                                 String expectedValue,
                                                 String actualValue)
  {
    super(new LocalizedString("DS_ERROR_INVALID_ATTRIBUTE_VALUE_KEY",
                              new Object[] {fileName,
                                            new Integer(lineNumber),
                                            attributeName,
                                            expectedValue,
                                            actualValue }));

    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(attributeName != null);
    Assert.expect(expectedValue != null);
    Assert.expect(actualValue != null);
  }
}
