package com.axi.v810.datastore.ndfReaders;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * Parses in the padgeom.ndf file
 *
 * This class creates the following objects:
 *   LandPattern
 *     name
 *     ThroughHoleLandPatternPads
 *
 *   ThroughHoleLandPatternPad - 1 for each Pad of every LandPattern instance
 *     name
 *     coordinates
 *     shape
 *     width
 *
 * This class sets the following variables:
 *   none
 *
 * This class requires the following to already be available to it from MainReader:
 *   none
 *
 * This class puts the following into MainReader:
 *   ThroughHoleLandPattern list
 *   LandPattern to PadNameToPadMap Map
 *   LandPattern to PadNameToLandPatternPad Map
 *
 * @author George A. David
 * @author Bill Darbie
 */
class ThroughHoleLandPatternNdfReader extends NdfReader
{
  private static final boolean _DONT_RETURN_DELIMITERS = false;
  private static final String _LAND_PATTERN_NAME_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_LAND_PATTERN_NAME_KEY");
  private static final String _PAD_NAME_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PAD_NAME_KEY");
  private static final String _X_HOLE_COORDINATE_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_X_HOLE_COORDINATE_KEY");
  private static final String _Y_HOLE_COORDINATE_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_Y_HOLE_COORDINATE_KEY");
  private static final String _HOLE_DIAMETER_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_HOLE_DIAMETER_KEY");
  private static final String _PAD_SHAPE_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PAD_SHAPE_KEY");
  private static final String _X_PAD_COORDINATE_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PAD_X_LOCATION_KEY");
  private static final String _Y_PAD_COORDINATE_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PAD_Y_LOCATION_KEY");
  private static final String _PAD_WIDTH_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PAD_WIDTH_KEY");
  private static final String _PAD_LENGTH_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PAD_LENGTH_KEY");
  private static final String _EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_PAD_GEOM_EXPECTED_SYNTAX_KEY");

  private LandPattern _landPattern = null;
  private Set<String> _landPatternPadSet = null;
  private Map<LandPattern, Set<String>> _landPatternPadSetMap = new HashMap<LandPattern, Set<String>>();
  private Map<String, LandPatternPad> _padNameToLandPatternPadMap = null;

  private int _unitAttributeLinesFound = 0;
  private Panel _panel = null;

  private StringTokenizer _tokenizer = null;
  private String _line = null;

  private String _landPatternName = null;
  private String _landPatternPadName = null;
  private int _xHoleCoordinateInNanoMeters = -1;
  private int _yHoleCoordinateInNanoMeters = -1;
  //private int _holeDiameterInNanoMeters = -1;
  //Siew Yeng - XCR-3318 - Oval PTH
  private int _holeWidthInNanoMeters = -1;
  private int _holeLengthInNanoMeters = -1;
  private ShapeEnum _padShape = null;
  private int _xPadCoordinateInNanoMeters = -1;
  private int _yPadCoordinateInNanoMeters = 1;
  private int _padWidthInNanoMeters = -1;
  private int _padLengthInNanoMeters = -1;

  /**
   * @author George A. David
   */
  ThroughHoleLandPatternNdfReader(MainReader mainReader)
  {
    super(mainReader);
  }

  /**
   * @author Keith Lee
   */
  protected void initializeBeforeParsing()
  {
    _parsedUnits = null;
    _landPattern = null;
    _landPatternPadSet = null;
    _landPatternPadSetMap.clear();
    _padNameToLandPatternPadMap = null;
    _unitAttributeLinesFound = 0;
    _line = null;
    _tokenizer = null;
    _landPatternPadName = null;
    _xHoleCoordinateInNanoMeters = 0;
    _yHoleCoordinateInNanoMeters = 0;
//    _holeDiameterInNanoMeters = -1;
    //Siew Yeng - XCR-3318 - Oval PTH
    _holeWidthInNanoMeters = -1;
    _holeLengthInNanoMeters = -1;
    _padShape = null;
    _xPadCoordinateInNanoMeters = 0;
    _yPadCoordinateInNanoMeters = 0;
    _padWidthInNanoMeters = 0;
    _padLengthInNanoMeters = 0;

    // variables from NdfReader
    _checkSumFound = false;
    _unitLineFound = false;
  }

  /**
   * @author Keith Lee
   */
  protected void resetAfterParsing()
  {
    initializeBeforeParsing();
    _panel = null;
  }


  /**
   * @author Keith Lee
   */
  protected void parseLinesRequiredByOtherLines() throws DatastoreException
  {
    // read in first .UNIT: attribute line
    String line = ParseUtil.readNextLine(_is, _currentFile);
    String upperCaseLine = null;
    boolean unitAttributeLineFound = false;
    while ((unitAttributeLineFound == false) && (line != null))
    {
      upperCaseLine = line.toUpperCase();
      if (upperCaseLine.length() == 0)
      {
        // skip: this is a blank line
      }
      else if (upperCaseLine.startsWith(getUnitToken()))
      {
        // .UNIT:
        line = line.substring(getUnitToken().length());
        parseAttributeLineUnit(line);
        unitAttributeLineFound = true;
      }
      else
      {
        // skip: this will be read in later during parseFile()
      }
      line = ParseUtil.readNextLine(_is, _currentFile);
    }

    // if no .UNIT: attribute line, set _parsedUnits to default value (MILS)
    if (_unitLineFound == false)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new MissingTokenDatastoreException(_currentFile, NdfReader.getUnitToken());
      }
      else
      {
        //give a warning that .UNIT arrtibute is missing and that mils are used as default
        _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_UNIT_ASSUMING_MILS_KEY",
        new Object[]{_currentFile}));
        _parsedUnits = _DEFAULT_NDF_UNITS;
      }
    }
  }

  /**
   * parses a data line in a padgeom.ndf file that follows the following format
   * "@<land pattern name> <pad number> <x hole coordinate> <y hole coordinate> <hole diameter> <pad shape> <x pad coordinate> <y pad coordinate> <pad width> <pad length>"
   *
   * @author George A. David
   */
  protected void parseDataLine(String line) throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);
    Assert.expect(line != null);

    _line = line;

    parseInLine();
    populateClassesWithData();
  }

  /**
   * Parses in one data line.
   *
   * @author George A. David
   */
  private void parseInLine() throws DatastoreException
  {
    boolean parsedLandPatternPadLengthInNanoMetersToken = false;

    _tokenizer = new StringTokenizer(_line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    _landPatternName = parseLandPatternName();
    _landPatternPadName = parseLandPatternPadName();
    _xHoleCoordinateInNanoMeters = parseXholeCoordinate();
    _yHoleCoordinateInNanoMeters = parseYholeCoordinate();
//    _holeDiameterInNanoMeters = parseHoleDiameter();
    //Siew Yeng - XCR-3318 - Oval PTH
    _holeWidthInNanoMeters = _holeLengthInNanoMeters = parseHoleDiameter();
    _padShape = parsePadShape();
    _xPadCoordinateInNanoMeters = parseXpadCoordinate();
    _yPadCoordinateInNanoMeters = parseYpadCoordinate();
    _padWidthInNanoMeters = parsePadWidth();
    _padLengthInNanoMeters = parsePadLength();
    checkForExtraDataAtEndOfLine(_tokenizer, _EXPECTED_SYNTAX);
  }

  /**
   * "@<land pattern name> <pad number> <x hole coordinate> <y hole coordinate> <hole diameter> <pad shape> <x pad coordinate> <y pad coordinate> <pad width> <pad length>"
   * @author George A. David
   */
  private String parseLandPatternName() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _LAND_PATTERN_NAME_FIELD);
    }

    return token;
  }

  /**
   * "@<land pattern name> <pad number> <x hole coordinate> <y hole coordinate> <hole diameter> <pad shape> <x pad coordinate> <y pad coordinate> <pad width> <pad length>"
   * @author George A. David
   */
  private String parseLandPatternPadName() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _PAD_NAME_FIELD);
    }

    return token;
  }

  /**
   * "@<land pattern name> <pad number> <x hole coordinate> <y hole coordinate> <hole diameter> <pad shape> <x pad coordinate> <y pad coordinate> <pad width> <pad length>"
   * @author George A. David
   */
  private int parseXholeCoordinate() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _X_HOLE_COORDINATE_FIELD);
    }

    int xHoleCoordinate = 0;
    double xHoleCoordinateDouble = parseDouble(token,
                                               _EXPECTED_SYNTAX,
                                               _X_HOLE_COORDINATE_FIELD,
                                               _parsedUnits);

    xHoleCoordinateDouble = MathUtil.convertUnits(xHoleCoordinateDouble, _parsedUnits, _DATASTORE_UNITS);
    try
    {
      xHoleCoordinate = MathUtil.convertDoubleToInt(xHoleCoordinateDouble);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _EXPECTED_SYNTAX,
                                                                                    _X_HOLE_COORDINATE_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }

    return xHoleCoordinate;
  }

  /**
   * "@<land pattern name> <pad number> <x hole coordinate> <y hole coordinate> <hole diameter> <pad shape> <x pad coordinate> <y pad coordinate> <pad width> <pad length>"
   * @author George A. David
   */
  private int parseYholeCoordinate() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _Y_HOLE_COORDINATE_FIELD);
    }

    int yHoleCoordinate = 0;
    double yHoleCoordinateDouble = parseDouble(token,
                                               _EXPECTED_SYNTAX,
                                               _Y_HOLE_COORDINATE_FIELD,
                                               _parsedUnits);

    yHoleCoordinateDouble = MathUtil.convertUnits(yHoleCoordinateDouble, _parsedUnits, _DATASTORE_UNITS);
    try
    {
      yHoleCoordinate = MathUtil.convertDoubleToInt(yHoleCoordinateDouble);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _EXPECTED_SYNTAX,
                                                                                    _Y_HOLE_COORDINATE_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }

    return yHoleCoordinate;
  }

  /**
   * "@<land pattern name> <pad number> <x hole coordinate> <y hole coordinate> <hole diameter> <pad shape> <x pad coordinate> <y pad coordinate> <pad width> <pad length>"
   * @author George A. David
   */
  private int parseHoleDiameter() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _HOLE_DIAMETER_FIELD);
    }

    int holeDiameter = 0;
    double holeDiameterDouble = parseDouble(token,
                                            _EXPECTED_SYNTAX,
                                            _HOLE_DIAMETER_FIELD,
                                            _parsedUnits);

    holeDiameterDouble = MathUtil.convertUnits(holeDiameterDouble, _parsedUnits, _DATASTORE_UNITS);
    try
    {
      holeDiameter = MathUtil.convertDoubleToInt(holeDiameterDouble);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _EXPECTED_SYNTAX,
                                                                                    _HOLE_DIAMETER_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }

    if (holeDiameter <= 0)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                  _is.getLineNumber(),
                                                                  _EXPECTED_SYNTAX,
                                                                  _HOLE_DIAMETER_FIELD,
                                                                  token);
      }
      else
      {
        // leave diameter as 0 - we key off this to make the the pad a SurfaceMountLandPatternPad instead of a
        // through hole one.
        holeDiameter = 0;
      }
    }

    Assert.expect(holeDiameter >= 0);
    return holeDiameter;
  }

  /**
   * "@<land pattern name> <pad number> <x hole coordinate> <y hole coordinate> <hole diameter> <pad shape> <x pad coordinate> <y pad coordinate> <pad width> <pad length>"
   * @author George A. David
   */
  private ShapeEnum parsePadShape() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _PAD_SHAPE_FIELD);
    }

    ShapeEnum padShape = null;

    // land pattern pad shape
    if (token.equalsIgnoreCase(getRectangularShapeToken()))
    {
      padShape = ShapeEnum.RECTANGLE;
    }
    else if (token.equalsIgnoreCase(getCircularShapeToken()))
    {
      padShape = ShapeEnum.CIRCLE;
    }
    else
    {
      String expected = EXPECTED_SHAPE;
      throw new InvalidShapeDatastoreException(_currentFile,
                                               _is.getLineNumber(),
                                               _EXPECTED_SYNTAX,
                                               _PAD_SHAPE_FIELD,
                                               token);
    }

    Assert.expect(padShape != null);

    return padShape;
  }

  /**
   * "@<land pattern name> <pad number> <x hole coordinate> <y hole coordinate> <hole diameter> <pad shape> <x pad coordinate> <y pad coordinate> <pad width> <pad length>"
   * @author George A David
   */
  private int parseXpadCoordinate() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _EXPECTED_SYNTAX,
                                                  _X_PAD_COORDINATE_FIELD);
      }
      else
      {
        // the legacy compiler uses atof or atoi which converts a missing/empty string to 0.
        token = "0";
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _EXPECTED_SYNTAX,
                                     _X_PAD_COORDINATE_FIELD,
                                     token,
                                     _parsedUnits};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_UNITS_KEY", args));
      }
    }

    int xPadCoordinate = 0;
    double xPadCoordinateDouble = parseDouble(token,
                                              _EXPECTED_SYNTAX,
                                              _X_PAD_COORDINATE_FIELD,
                                              _parsedUnits);

    xPadCoordinateDouble = MathUtil.convertUnits(xPadCoordinateDouble, _parsedUnits, _DATASTORE_UNITS);
    try
    {
      xPadCoordinate = MathUtil.convertDoubleToInt(xPadCoordinateDouble);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _EXPECTED_SYNTAX,
                                                                                    _X_PAD_COORDINATE_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }

    return xPadCoordinate;
  }

  /**
   * "@<land pattern name> <pad number> <x hole coordinate> <y hole coordinate> <hole diameter> <pad shape> <x pad coordinate> <y pad coordinate> <pad width> <pad length>"
   * @author George A. David
   */
  private int parseYpadCoordinate() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _EXPECTED_SYNTAX,
                                                  _Y_PAD_COORDINATE_FIELD);
      }
      else
      {
        // the legacy compiler uses atof or atoi which converts a missing/empty string to 0.
        token = "0";
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _EXPECTED_SYNTAX,
                                     _Y_PAD_COORDINATE_FIELD,
                                     token,
                                     _parsedUnits};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_UNITS_KEY", args));
      }
    }

    int yPadCoordinate = 0;
    double yPadCoordinateDouble = parseDouble(token,
                                              _EXPECTED_SYNTAX,
                                              _Y_PAD_COORDINATE_FIELD,
                                              _parsedUnits);

    yPadCoordinateDouble = MathUtil.convertUnits(yPadCoordinateDouble, _parsedUnits, _DATASTORE_UNITS);
    try
    {
      yPadCoordinate = MathUtil.convertDoubleToInt(yPadCoordinateDouble);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _EXPECTED_SYNTAX,
                                                                                    _Y_PAD_COORDINATE_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }

    return yPadCoordinate;
  }

  /**
   * "@<land pattern name> <pad number> <x hole coordinate> <y hole coordinate> <hole diameter> <pad shape> <x pad coordinate> <y pad coordinate> <pad width> <pad length>"
   * @author Keith Lee
   */
  private int parsePadWidth() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);

    int padWidth = 0;
    double padWidthDouble = -1.0;

    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _EXPECTED_SYNTAX,
                                                  _PAD_WIDTH_FIELD);
      }
      else
      {
        // the legacy compiler uses atof or atoi which converts a missing/empty string to 0, but
        // zero is not a valid pad width, so we will set it to 1 mil and warn the user
        padWidthDouble = 1.0;
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _EXPECTED_SYNTAX,
                                     _PAD_WIDTH_FIELD,
                                     new Double(padWidthDouble),
                                     MathUtilEnum.MILS};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_UNITS_KEY", args));
        padWidthDouble = MathUtil.convertUnits(padWidthDouble, MathUtilEnum.MILS, _DATASTORE_UNITS);
      }
    }
    else
    {

      padWidthDouble = parsePositiveNonZeroDouble(token,
                                                  _EXPECTED_SYNTAX,
                                                  _PAD_WIDTH_FIELD,
                                                  _parsedUnits);

      padWidthDouble = MathUtil.convertUnits(padWidthDouble, _parsedUnits, _DATASTORE_UNITS);
    }
    try
    {
      padWidth = MathUtil.convertDoubleToInt(padWidthDouble);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _EXPECTED_SYNTAX,
                                                                                    _PAD_WIDTH_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }


    return padWidth;
  }

  /**
   * "@<land pattern name> <pad number> <x hole coordinate> <y hole coordinate> <hole diameter> <pad shape> <x pad coordinate> <y pad coordinate> <pad width> <pad length>"
   * @author George A. David
   */
  private int parsePadLength() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);

    boolean padLengthIsMissing = false;

    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _EXPECTED_SYNTAX,
                                                  _PAD_LENGTH_FIELD);
      }
      else
      {
        padLengthIsMissing = true;
      }
    }

    int padLength = 0;
    if (padLengthIsMissing == false)
    {
      double padLengthDouble = 0.0;

      Exception ex = null;
      try
      {
        padLengthDouble = StringUtil.convertStringToPositiveDouble(token);
      }
      catch(BadFormatException bfe)
      {
        ex = bfe;
      }
      catch (ValueOutOfRangeException vex)
      {
        ex = vex;
      }
      if (ex != null)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          ExpectedPositiveNonZeroNumberDatastoreException dex = new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                                                                    _is.getLineNumber(),
                                                                                                                    _EXPECTED_SYNTAX,
                                                                                                                    _PAD_LENGTH_FIELD,
                                                                                                                    token);
          dex.initCause(ex);
          throw dex;
        }
        else
        {
          // the legacy compiler used the c++ atof() function, which converted as
          // much of the string as possible to a float, and that is what we will do here.
          try
          {
            padLengthDouble = StringUtil.legacyConvertStringToDouble(token);
            // only warn if the value is greater than zero. If its 0 or negative,
            // we will warn later on in this function. This is to avoid giving the user two warnings.
            if (padLengthDouble > 0)
            {
              // at this point, we have converted as much as the string as possible to a double,
              // warn the user that we expected a positive non-zero number and will use what
              // we were able to convert.
              Object[] args = new Object[]{ _currentFile,
                                            new Integer(_is.getLineNumber()),
                                            _EXPECTED_SYNTAX,
                                            _PAD_LENGTH_FIELD,
                                            token,
                                            new Double(padLengthDouble),
                                            _parsedUnits};
              _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_NUMBER_UNITS_KEY", args));
            }
          }
          catch(BadFormatException anotherBfe)
          {
            // the legacy compiler used the c++ built-in function atof() to convert
            // a string to a double. if the string did not begin with a positive or
            // negative double, it would return 0 and the compiler would use this
            // as a valid value. We don't need to warn the user at this point because
            // we will warn later on for a 0 pad length value. This way we avoid
            // reporting two warnings on the same problem.
            padLengthDouble = 0.0;
          }

        }
      }

      padLengthDouble = MathUtil.convertUnits(padLengthDouble, _parsedUnits, _DATASTORE_UNITS);
      try
      {
        padLength = MathUtil.convertDoubleToInt(padLengthDouble);
      }
      catch(ValueOutOfRangeException voore)
      {
        ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                      _is.getLineNumber(),
                                                                                      _EXPECTED_SYNTAX,
                                                                                      _PAD_LENGTH_FIELD,
                                                                                      voore);
        dex.initCause(voore);
        throw dex;
      }

      if (padLength == 0)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                    _is.getLineNumber(),
                                                                    _EXPECTED_SYNTAX,
                                                                    _PAD_LENGTH_FIELD,
                                                                    token);
        }
        else
        {
          padLength = _padWidthInNanoMeters;
          // warn the user that we expected a positive non-zero number and that
          // we will use the pad width value for the pad length
          Object[] args = new Object[]{ _currentFile,
                                        new Integer(_is.getLineNumber()),
                                        _EXPECTED_SYNTAX,
                                        _PAD_LENGTH_FIELD,
                                        token,
                                        new Double(MathUtil.convertUnits(padLength, _DATASTORE_UNITS, _parsedUnits)),
                                        _parsedUnits};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_NUMBER_UNITS_KEY", args));
        }
      }
      else if (padLength < 0)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                    _is.getLineNumber(),
                                                                    _EXPECTED_SYNTAX,
                                                                    _PAD_LENGTH_FIELD,
                                                                    token);
        }
        else
        {
          padLength = Math.abs(padLength);
          // warn the user that we expected a positive non-zero number
          // and that we will user the absolute value of the pad length
          Object[] args = new Object[]{ _currentFile,
                                        new Integer(_is.getLineNumber()),
                                        _EXPECTED_SYNTAX,
                                        _PAD_LENGTH_FIELD,
                                        token,
                                        new Double(MathUtil.convertUnits(padLength, _DATASTORE_UNITS, _parsedUnits)),
                                        _parsedUnits};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_NUMBER_UNITS_KEY", args));
        }
      }
    }
    else
    {
      // if the pad length field is missing, the legacy compiler will use
      // the pad width value
      padLength = _padWidthInNanoMeters;
      // warn the user that the pad length field was missing
      Object[] args = new Object[]{ _currentFile,
                                    new Integer(_is.getLineNumber()),
                                    _EXPECTED_SYNTAX,
                                    _PAD_LENGTH_FIELD,
                                    new Double(MathUtil.convertUnits(padLength, _DATASTORE_UNITS, _parsedUnits)),
                                    _parsedUnits};
      _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_UNITS_KEY", args));
    }

    return padLength;
  }

  /**
   * @author George A. David
   */
  private void populateClassesWithData() throws DatastoreException
  {
    // check to see if the previous pad process uses the same land pattern as the current one.
    // This way we don't need to waste time accessing maps.
    if ( (_landPattern == null) || (_landPattern.getName().equalsIgnoreCase(_landPatternName) == false) )
    {
      if (_mainReader.doesThroughHoleLandPatternExist(_landPatternName) == false)
      {
        // the land pattern does not exist, create a the land pattern
        _landPattern = new LandPattern();
        _landPattern.setPanel(_panel);
        _landPattern.setName(_landPatternName);
        _landPatternPadSet = new HashSet<String>();
        _padNameToLandPatternPadMap = new TreeMap<String, LandPatternPad>();
        _mainReader.addThroughHoleLandPattern(_landPattern);
        _landPatternPadSetMap.put(_landPattern, _landPatternPadSet);
        _mainReader.addLandPatternToPadNameToLandPatternPadMapMap(_landPattern, _padNameToLandPatternPadMap);
      }
      else
      {
        // the land pattern exists, retrieve it
        _landPattern = _mainReader.getThroughHoleLandPattern(_landPatternName);
        _landPatternPadSet = _landPatternPadSetMap.get(_landPattern);
        _padNameToLandPatternPadMap = _mainReader.getPadNameToLandPatternPadMap(_landPattern);
        // this should not happen. checked mapping of land pattern id and land pattern above.
        Assert.expect(_landPatternPadSet != null);
        Assert.expect(_padNameToLandPatternPadMap != null);
      }
    }

    LandPatternPad landPatternPad;
    if (_holeWidthInNanoMeters > 0 && _holeLengthInNanoMeters > 0)
    {
      ThroughHoleLandPatternPad throughHoleLandPatternPad = new ThroughHoleLandPatternPad();
      throughHoleLandPatternPad.setLandPattern(_landPattern);
      throughHoleLandPatternPad.setName(_landPatternPadName);
      ComponentCoordinate holeCoordinateInNanoMeters = new ComponentCoordinate(_xHoleCoordinateInNanoMeters,
                                                                               _yHoleCoordinateInNanoMeters);
      throughHoleLandPatternPad.setHoleCoordinateInNanoMeters(holeCoordinateInNanoMeters);
      throughHoleLandPatternPad.setShapeEnum(_padShape);

      ComponentCoordinate padCoordinateInNanoMeters = new ComponentCoordinate(_xPadCoordinateInNanoMeters,
                                                                              _yPadCoordinateInNanoMeters);
      throughHoleLandPatternPad.setCoordinateInNanoMeters(padCoordinateInNanoMeters);
      throughHoleLandPatternPad.setLengthInNanoMeters(_padLengthInNanoMeters);
      throughHoleLandPatternPad.setWidthInNanoMeters(_padWidthInNanoMeters);

//      throughHoleLandPatternPad.setHoleDiameterInNanoMeters(_holeDiameterInNanoMeters);
      //Siew Yeng - XCR-3318 - Oval PTH
      throughHoleLandPatternPad.setHoleWidthInNanoMeters(_holeWidthInNanoMeters);
      throughHoleLandPatternPad.setHoleLengthInNanoMeters(_holeLengthInNanoMeters);

      throughHoleLandPatternPad.setDegreesRotationWithoutAffectingPinOrientation(0);
      landPatternPad = throughHoleLandPatternPad;
    }
    else
    {
      // if the hole diameter is 0, then this pad is really a surface mount pad
      SurfaceMountLandPatternPad surfaceMountLandPatternPad = new SurfaceMountLandPatternPad();

      surfaceMountLandPatternPad.setLandPattern(_landPattern);
      surfaceMountLandPatternPad.setName(_landPatternPadName);
      surfaceMountLandPatternPad.setShapeEnum(_padShape);

      ComponentCoordinate padCoordinateInNanoMeters = new ComponentCoordinate(_xPadCoordinateInNanoMeters,
                                                                              _yPadCoordinateInNanoMeters);
      surfaceMountLandPatternPad.setCoordinateInNanoMeters(padCoordinateInNanoMeters);
      surfaceMountLandPatternPad.setLengthInNanoMeters(_padLengthInNanoMeters);
      surfaceMountLandPatternPad.setWidthInNanoMeters(_padWidthInNanoMeters);
      surfaceMountLandPatternPad.setDegreesRotationWithoutAffectingPinOrientation(0);
      landPatternPad = surfaceMountLandPatternPad;
    }

    boolean addedToSet = _landPatternPadSet.add(_landPatternPadName);
    if (addedToSet == false)
    {
      // this is the 2nd time we have seen the same pad, give a warning or an error
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new DuplicatePadNamesDatastoreException(_currentFile,
                                                      _is.getLineNumber(),
                                                      _landPatternName,
                                                      _landPatternPadName);
      }
      else
      {
        // warn the user that a duplicate pad was found and the second pad definition will be used.
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _landPatternName,
                                     _landPatternPadName};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_DUPLICATE_PAD_KEY", args));
      }
    }
    else
    {
      _padNameToLandPatternPadMap.put(_landPatternPadName, landPatternPad);
    }
  }


  /**
   * parses an attribute line in a padgeom.ndf file that follows one of the following formats
   * .unit: <unit>
   *
   * @param line the line of through hole land pattern atributes in the padgeom.ndf to parse.
   * @author Keith Lee
   */
  protected void parseAttributeLine(String line) throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);
    Assert.expect(line != null);

    String upperCaseLine = line.toUpperCase();
    if (upperCaseLine.startsWith(getUnitToken()))
    {
      // .UNIT: was read in during parseRequiredLines(). check that this is the
      // only occurance of this attribute line in the file.
      _unitAttributeLinesFound++;
      if (_unitAttributeLinesFound > 1)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), getUnitToken());
        }
        else
        {
          duplicateTokenWarning(getUnitToken());
        }
      }
    }
    else if (upperCaseLine.startsWith(CHECKSUM))
    {
      line = line.substring(CHECKSUM.length());
      parseAttributeLineCheckSum(line);
    }
    else
    {
      int index = line.indexOf(':');
      String token = line;
      if (index != -1)
        token = line.substring(0, index + 1);

      if (_mainReader.ndfsAreWellFormed())
      {
        // invalid or unknown attribute line
        String validAttributes = ".CHECKSUM .UNITS";
        throw new InvalidAttributeDatastoreException(_currentFile,
                                                     _is.getLineNumber(),
                                                     token,
                                                     validAttributes);
      }
      else
      {
        unrecognizedTokenWarning(token);
      }
    }
  }

  /**
   * padgeom.ndf does not have to have data. Board can have only surface mount parts.
   * @author Keith Lee
   */
  protected void postProcessData() throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);
  }

  /**
   * @author George A. David
   */
  void setProject(Project project)
  {
    Assert.expect(project != null);

    _panel = project.getPanel();
  }
}
