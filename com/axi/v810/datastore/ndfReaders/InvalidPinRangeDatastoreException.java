package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when an invalid range of pins is specified.
 * @author Bill Darbie
 */
public class InvalidPinRangeDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public InvalidPinRangeDatastoreException(String fileName,
                                           int lineNumber,
                                           int minRange,
                                           int maxRange)
  {
    super(new LocalizedString("DS_ERROR_INVALID_PIN_RANGE_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           new Integer(minRange),
                                           new Integer(maxRange)}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
  }
}
