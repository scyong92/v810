package com.axi.v810.datastore.ndfReaders;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * Parses in the landpat.ndf file.
 *
 * This class creates the following objects:
 *   LandPattern
 *     name
 *     SurfaceMountLandPatternPads
 *
 *   SurfaceMountLandPatternPad - 1 for each Pad of every LandPattern instance
 *     name
 *     coordinates
 *     shape
 *     width
 *     length
 *     rotation
 *
 * This class sets the following variables:
 *   none
 *
 * This class requires the following to already be available to it from MainReader:
 *   none
 *
 * This class puts the following into MainReader:
 *   SurfaceMountLandPattern list
 *   LandPattern to PadNameToPadMap Map
 *   LandPattern to PadNameToLandPatternPad Map
 *
 * @author Jeff Ryer
 * @author Bill Darbie*
 */
class SurfaceMountLandPatternNdfReader extends NdfReader
{
  private static final boolean _DONT_RETURN_DELIMITERS = false;
  private static final String _NO_DELIMITERS = "";
  private static final String _EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_LAND_PATTERN_EXPECTED_SYNTAX_KEY");
  // "@<land pattern name> <pad number> <x loaction> <y location> <shape> <pad width> <pad length>"
  private static final String _LAND_PATTERN_NAME_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_LAND_PATTERN_NAME_KEY"); // "<land pattern name>"
  private static final String _PAD_NAME_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PAD_NAME_KEY"); // "<pad name>"
  private static final String _PAD_X_LOCATION_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PAD_X_LOCATION_KEY"); // "<pad x location>"
  private static final String _PAD_Y_LOCATION_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PAD_Y_LOCATION_KEY"); // "<pad y location>"
  private static final String _PAD_SHAPE_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PAD_SHAPE_KEY"); // "<pad shape>"
  private static final String _PAD_WIDTH_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PAD_WIDTH_KEY"); // "<pad width>"
  private static final String _PAD_LENGTH_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PAD_LENGTH_KEY"); // "<pad length>"

  private String _landPatternName = null;
  private String _landPatternPadName = null;
  private int _padXlocationInNanos = -1;
  private int _padYlocationInNanos = -1;
  private ShapeEnum _padShape = null;
  private int _padWidthInNanos = -1;
  private int _padLengthInNanos = -1;
  private String _line = null;
  private StringTokenizer _tokenizer = null;
  private LandPattern _landPattern = null;
  private Set<String> _landPatternPadSet = null;
  private Map<LandPattern, Set<String>> _landPatternToPadSetMap = new HashMap<LandPattern, Set<String>>();
  private Map<String, LandPatternPad> _padNameToLandPatternPadMap = null;

  private boolean _atLeastOneValidLineFound = false;
  private int _unitAttributeLinesFound = 0;
  private Panel _panel;

  /**
   * @author Jeff Ryer
   */
  SurfaceMountLandPatternNdfReader(MainReader mainReader)
  {
    super(mainReader);
  }

  /**
   * @author Jeff Ryer
   */
  protected void initializeBeforeParsing()
  {
    _landPatternName = null;
    _landPatternPadName = null;
    _padXlocationInNanos = -1;
    _padYlocationInNanos = -1;
    _padShape = null;
    _padWidthInNanos = -1;
    _padLengthInNanos = -1;
    _line = null;
    _tokenizer = null;
    _landPattern = null;
    _landPatternPadSet = null;
    _landPatternToPadSetMap.clear();
    _padNameToLandPatternPadMap = null;
    _unitAttributeLinesFound = 0;
  }

  /**
   * @author Jeff Ryer
   */
  protected void resetAfterParsing()
  {
    initializeBeforeParsing();
    _panel = null;
  }

  /**
   * parses a data line in a landpat.ndf file that follows the following format
   * "@<land pattern id> <pad #> <pad x location> <pad y location> <pad shape> <pad width> <pad length>"
   *
   * @param line the line of surface mount land pattern data in the landpat.ndf to parse.
   * @author Jeff Ryer
   */
  protected void parseDataLine(String line) throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);
    Assert.expect(_panel != null);
    Assert.expect(line != null);
    _line = line;

    parseInLine();
    populateClassesWithData();
  }

  /**
   * Parse in one data line
   * @author Jeff Ryer
   */
  private void parseInLine() throws DatastoreException
  {
    // parse in each token from the line
    _tokenizer = new StringTokenizer(_line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    _landPatternName = parseLandPatternName();
    _landPatternPadName = parseLandPatternPadName();
    _padXlocationInNanos = parsePadXlocation();
    _padYlocationInNanos = parsePadYlocation();
    _padShape = parsePadShape();
    _padWidthInNanos = parsePadWidth();
    _padLengthInNanos = parsePadLength();
    checkForExtraDataAtEndOfLine(_tokenizer, _EXPECTED_SYNTAX);
  }

  /**
   * "@<land pattern name> <pad number> <x loaction> <y location> <shape> <pad width> <pad length>"
   * @author Jeff Ryer
   */
  private String parseLandPatternName() throws DatastoreException
  {
    String landPatternName = ParseUtil.getNextToken(_tokenizer);
    if (landPatternName == null)
    {
      // no need to check for _mainReader.ndfsAreWellFormed(). if no land
      // pattern name, then there is no pad shape.
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _LAND_PATTERN_NAME_FIELD);
    }

    return landPatternName;
  }

  /**
   * "@<land pattern name> <pad number> <x loaction> <y location> <shape> <pad width> <pad length>"
   * @author Jeff Ryer
   */
  private String parseLandPatternPadName() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);

    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _PAD_NAME_FIELD);
    }

    return token;
  }

  /**
   * "@<land pattern name> <pad number> <x loaction> <y location> <shape> <pad width> <pad length>"
   * @author Jeff Ryer
   */
  private int parsePadXlocation() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      // no need to check for _mainReader.ndfsAreWellFormed(). if no x
      // location, then no pad shape.
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _PAD_X_LOCATION_FIELD);
    }
    int padXlocation = 0;
    double padXlocationDouble = parseDouble(token, _EXPECTED_SYNTAX, _PAD_X_LOCATION_FIELD);
    padXlocationDouble = MathUtil.convertUnits(padXlocationDouble, _parsedUnits, _DATASTORE_UNITS);
    try
    {
      padXlocation = MathUtil.convertDoubleToInt(padXlocationDouble);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _EXPECTED_SYNTAX,
                                                                                    _PAD_X_LOCATION_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }
    return padXlocation;
  }

  /**
   * "@<land pattern name> <pad number> <x loaction> <y location> <shape> <pad width> <pad length>"
   * @author Jeff Ryer
   */
  private int parsePadYlocation() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      // no need to check for _mainReader.ndfsAreWellFormed(). if no x
      // location, then no pad shape.
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _PAD_Y_LOCATION_FIELD);
    }
    int padYlocation = 0;
    double padYlocationDouble = parseDouble(token, _EXPECTED_SYNTAX, _PAD_Y_LOCATION_FIELD);
    padYlocationDouble = MathUtil.convertUnits(padYlocationDouble, _parsedUnits, _DATASTORE_UNITS);
    try
    {
      padYlocation = MathUtil.convertDoubleToInt(padYlocationDouble);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _EXPECTED_SYNTAX,
                                                                                    _PAD_Y_LOCATION_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }
    return padYlocation;
  }

  /**
   * "@<land pattern name> <pad number> <x loaction> <y location> <shape> <pad width> <pad length>"
   * @author Jeff Ryer
   */
  private ShapeEnum parsePadShape() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    if (token == null)
    {
      throw new UnexpectedEolDatastoreException(_currentFile,
                                                _is.getLineNumber(),
                                                _EXPECTED_SYNTAX,
                                                _PAD_SHAPE_FIELD);
    }
    ShapeEnum padShape = null;
    if (token.equalsIgnoreCase(getRectangularShapeToken()))
    {
      padShape = ShapeEnum.RECTANGLE;
    }
    else if (token.equalsIgnoreCase(getCircularShapeToken()))
    {
      padShape = ShapeEnum.CIRCLE;
    }
    else
    {
      throw new InvalidShapeDatastoreException(_currentFile,
                                               _is.getLineNumber(),
                                               _EXPECTED_SYNTAX,
                                               _PAD_SHAPE_FIELD,
                                               token);
    }
    return padShape;
  }

  /**
   * @return pad width. In compatibility mode: if the value is negative, return abs(padWidth);
   *                                           if the value is "0", return 1.
   * "@<land pattern name> <pad number> <x loaction> <y location> <shape> <pad width> <pad length>"
   * @author Jeff Ryer
   */
  private int parsePadWidth() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    int padWidth = 0;
    double padWidthDouble = 0.0;
    if (token == null)
    // the compiler let's this go, so we only throw an exception if well-formed, else warning and set value to 1
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        // must have dimension and it must be valid, since dimension cannot be 0.
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _EXPECTED_SYNTAX,
                                                  _PAD_WIDTH_FIELD);
      }
      else
      {
        // the legacy compiler uses atof or atoi which converts a missing/empty string to 0, but
        // zero is not a valid pad width, so we will set it to 1 mil and warn the user
        padWidthDouble = 1.0;
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _EXPECTED_SYNTAX,
                                     _PAD_WIDTH_FIELD,
                                     new Double(padWidthDouble),
                                     MathUtilEnum.MILS};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_UNITS_KEY", args));
        padWidthDouble = MathUtil.convertUnits(padWidthDouble, MathUtilEnum.MILS, _DATASTORE_UNITS);
      }
    }
    else
    {
      padWidthDouble = parsePositiveNonZeroDouble(token, _EXPECTED_SYNTAX, _PAD_WIDTH_FIELD);
      padWidthDouble = MathUtil.convertUnits(padWidthDouble, _parsedUnits, _DATASTORE_UNITS);
    }

    try
    {
      padWidth = MathUtil.convertDoubleToInt(padWidthDouble);
    }
    catch(ValueOutOfRangeException voore)
    {
      ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                    _is.getLineNumber(),
                                                                                    _EXPECTED_SYNTAX,
                                                                                    _PAD_WIDTH_FIELD,
                                                                                    voore);
      dex.initCause(voore);
      throw dex;
    }
    return padWidth;
  }

  /**
   * @return pad length. In compatibility mode: if the value is negative, return abs(padLength);
   *                                            if the value is missing, return the pad width.
   * "@<land pattern name> <pad number> <x loaction> <y location> <shape> <pad width> <pad length>"
   * @author Jeff Ryer
   */
  private int parsePadLength() throws DatastoreException
  {
    String token = ParseUtil.getNextToken(_tokenizer);
    int padLength = 0;
    if (token == null)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new UnexpectedEolDatastoreException(_currentFile,
                                                  _is.getLineNumber(),
                                                  _EXPECTED_SYNTAX,
                                                  _PAD_LENGTH_FIELD);
      }
      else
      {
        // if the pad length field is missing, the legacy compiler will use
        // the pad width value
        padLength = _padWidthInNanos;
        // warn the user that the pad length field was missing
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      _EXPECTED_SYNTAX,
                                      _PAD_LENGTH_FIELD,
                                      new Double(MathUtil.convertUnits(padLength, _DATASTORE_UNITS, _parsedUnits)),
                                      _parsedUnits};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_LINE_MISSING_DATA_UNITS_KEY", args));
      }
    }
    else
    {
      double padLengthDouble = 0.0;
      Exception ex = null;
      try
      {
        padLengthDouble = StringUtil.convertStringToPositiveDouble(token);
      }
      catch(BadFormatException bfe)
      {
        ex = bfe;
      }
      catch (ValueOutOfRangeException vex)
      {
        ex = vex;
      }
      if (ex != null)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          ExpectedPositiveNonZeroNumberDatastoreException dex = new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                                                                    _is.getLineNumber(),
                                                                                                                    _EXPECTED_SYNTAX,
                                                                                                                    _PAD_LENGTH_FIELD,
                                                                                                                    token);
          dex.initCause(ex);
          throw dex;
        }
        else
        {
          // the legacy compiler used the c++ atof() function, which converted as
          // much of the string as possible to a float, and that is what we will do here.
          try
          {
            padLengthDouble = StringUtil.legacyConvertStringToDouble(token);
            // only warn if the value is greater than zero. If its 0 or negative,
            // we will warn later on in this function. This is to avoid giving the user two warnings.
            if (padLengthDouble > 0)
            {
              // warn the user that we expected a positive non-zero number, but will use the legacy-converted value instead
              Object[] args = new Object[]{ _currentFile,
                                            new Integer(_is.getLineNumber()),
                                            _EXPECTED_SYNTAX,
                                            _PAD_LENGTH_FIELD,
                                            token,
                                            new Double(padLengthDouble),
                                            _parsedUnits};
              _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_NUMBER_UNITS_KEY", args));
            }
          }
          catch(BadFormatException anotherBfe)
          {
            // the legacy compiler used the c++ build in function atof() to convert
            // a string to an integer. if the string did not begin with a positive or
            // negative integer, it would return 0 and the compiler would use this
            // as a valid value. We don't need to warn the user at this point because
            // we will warn later on for a 0 pad length value. This way we avoid
            // reporting two warnings on the same problem.
            padLengthDouble = 0.0;
          }

        }
      }

      padLengthDouble = MathUtil.convertUnits(padLengthDouble, _parsedUnits, _DATASTORE_UNITS);
      try
      {
        padLength = MathUtil.convertDoubleToInt(padLengthDouble);
      }
      catch(ValueOutOfRangeException voore)
      {
        ValueOutOfRangeDatastoreException dex = new ValueOutOfRangeDatastoreException(_currentFile,
                                                                                      _is.getLineNumber(),
                                                                                      _EXPECTED_SYNTAX,
                                                                                      _PAD_LENGTH_FIELD,
                                                                                      voore);
        dex.initCause(voore);
        throw dex;
      }

      if (padLength == 0)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                    _is.getLineNumber(),
                                                                    _EXPECTED_SYNTAX,
                                                                    _PAD_LENGTH_FIELD,
                                                                    token);
        }
        else
        {
          padLength = _padWidthInNanos;
          // warn the user that we expected a positive non-zero number, but will use the pad width
          Object[] args = new Object[]{ _currentFile,
                                        new Integer(_is.getLineNumber()),
                                        _EXPECTED_SYNTAX,
                                        _PAD_LENGTH_FIELD,
                                        token,
                                        new Double(MathUtil.convertUnits(padLength, _DATASTORE_UNITS, _parsedUnits)),
                                        _parsedUnits};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_NUMBER_UNITS_KEY", args));
        }
      }
      else if (padLength < 0)
      {
        if (_mainReader.ndfsAreWellFormed())
        {
          throw new ExpectedPositiveNonZeroNumberDatastoreException(_currentFile,
                                                                    _is.getLineNumber(),
                                                                    _EXPECTED_SYNTAX,
                                                                    _PAD_LENGTH_FIELD,
                                                                    token);
        }
        else
        {
          padLength = Math.abs(padLength);
          // warn the user that we expected a positive non-zero number, but will use the absolute value instead
          Object[] args = new Object[]{ _currentFile,
                                        new Integer(_is.getLineNumber()),
                                        _EXPECTED_SYNTAX,
                                        _PAD_LENGTH_FIELD,
                                        token,
                                        new Double(MathUtil.convertUnits(padLength, _DATASTORE_UNITS, _parsedUnits)),
                                        _parsedUnits};
          _warnings.add(new LocalizedString("DS_NDF_WARNING_EXPECTED_A_POSITIVE_NON_ZERO_NUMBER_UNITS_KEY", args));
        }
      }
    }
    return padLength;
  }

  /**
   * parses an attribute line in a landpat.ndf file that has the following format
   * .unit: <unit>
   *
   * @param line is the line of surface mount land pattern atributes in the landpat.ndf to parse.
   * @author Jeff Ryer
   */
  protected void parseAttributeLine(String line) throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);
    Assert.expect(line != null);

    String tempLine = null;

    tempLine = line.toUpperCase();
    if (tempLine.startsWith(getUnitToken()))
    {
      // .UNIT: was read in during parseRequiredLines(). check that this is the
      // only occurance of this attribute line in the file.
      _unitAttributeLinesFound++;
      if (_unitAttributeLinesFound > 1)
      {
        if (_mainReader.ndfsAreWellFormed())
          throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), getUnitToken());
        else
          duplicateTokenWarning(getUnitToken());
      }
    }
    else if (tempLine.startsWith(CHECKSUM))
    {
      line = line.substring(CHECKSUM.length());
      parseAttributeLineCheckSum(line);
    }
    else
    {
      int index = line.indexOf(':');
      String token = line;
      if (index != -1)
        token = line.substring(0, index + 1);

      if (_mainReader.ndfsAreWellFormed())
      {
        // invalid or unknown attribute line
        String validAttributes = ".CHECKSUM .UNITS";
        throw new InvalidAttributeDatastoreException(_currentFile,
                                                     _is.getLineNumber(),
                                                     token,
                                                     validAttributes);
      }
      else
      {
        unrecognizedTokenWarning(token);
      }
    }
  }

  /**
   * @author Jeff Ryer
   */
  protected void parseLinesRequiredByOtherLines() throws DatastoreException
  {
    // read in first .UNIT: attribute line
    String line = ParseUtil.readNextLine(_is, _currentFile);
    String tempLine = null;
    boolean unitAttributeLineFound = false;
    while ((unitAttributeLineFound == false) && (line != null))
    {
      tempLine = line.toUpperCase();
      if (tempLine.length() == 0)
      {
        // skip: this is a blank line
      }
      else if (tempLine.startsWith(NdfReader.getUnitToken()))
      {
        // .UNIT:
        line = line.substring(NdfReader.getUnitToken().length());
        parseAttributeLineUnit(line);
        unitAttributeLineFound = true;
      }
      else
      {
        // skip: this will be read in later during parseFile()
      }
      line = ParseUtil.readNextLine(_is, _currentFile);
    }

    // if no .UNIT: attribute line, set _parsedUnits to default value (MILS)
    if (_unitLineFound == false)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new MissingTokenDatastoreException(_currentFile, NdfReader.getUnitToken());
      }
      else
      {
        //give a warning that .UNIT arrtibute is missing and that mils are used as default
        _warnings.add(new LocalizedString("DS_NDF_WARNING_MISSING_UNIT_ASSUMING_MILS_KEY",
            new Object[]{_currentFile}));
        _parsedUnits = _DEFAULT_NDF_UNITS;
      }
    }
  }

  /**
   * Take the data read in on a single line and populate the appropriate classes
   * with it.
   * @author Jeff Ryer
   */
  private void populateClassesWithData() throws DatastoreException
  {
    //get land pattern if current one is null or parsed id doesnt match current one
    if ( (_landPattern == null) ||
        (_landPattern.getName().equalsIgnoreCase(_landPatternName) == false) )
    {
      if (_mainReader.doesSurfaceMountLandPatternExist(_landPatternName) == false)
      {
        _landPattern = new LandPattern();
        _landPattern.setPanel(_panel);
        _landPattern.setName(_landPatternName);
        _landPatternPadSet = new HashSet<String>();
        _padNameToLandPatternPadMap = new TreeMap<String, LandPatternPad>();
        // store LandPattern so
        _mainReader.addSurfaceMountLandPattern(_landPattern);
        _landPatternToPadSetMap.put(_landPattern, _landPatternPadSet);
        _mainReader.addLandPatternToPadNameToLandPatternPadMapMap(_landPattern, _padNameToLandPatternPadMap);
      }
      else
      {
        _landPattern = _mainReader.getSurfaceMountLandPattern(_landPatternName);
        _landPatternPadSet = _landPatternToPadSetMap.get(_landPattern);
        _padNameToLandPatternPadMap = _mainReader.getPadNameToLandPatternPadMap(_landPattern);
        // this should not happen. checked mapping of land pattern id and land pattern above.
        Assert.expect(_landPatternPadSet != null);
        Assert.expect(_padNameToLandPatternPadMap != null);
      }
    }

    SurfaceMountLandPatternPad surfaceMountLandPatternPad = new SurfaceMountLandPatternPad();
    surfaceMountLandPatternPad.setLandPattern(_landPattern);
    surfaceMountLandPatternPad.setName(_landPatternPadName);
    ComponentCoordinate padCoordinateInNanoMeters = new ComponentCoordinate(_padXlocationInNanos, _padYlocationInNanos);
    surfaceMountLandPatternPad.setCoordinateInNanoMeters(padCoordinateInNanoMeters);
    surfaceMountLandPatternPad.setShapeEnum(_padShape);
    surfaceMountLandPatternPad.setLengthInNanoMeters(_padLengthInNanos);
    surfaceMountLandPatternPad.setWidthInNanoMeters(_padWidthInNanos);
    surfaceMountLandPatternPad.setDegreesRotationWithoutAffectingPinOrientation(0);

    boolean addedToSet = _landPatternPadSet.add(_landPatternPadName);
    if (addedToSet == false)
    {
      if (_mainReader.ndfsAreWellFormed())
      {
        throw new DuplicatePadNamesDatastoreException(_currentFile,
                                                      _is.getLineNumber(),
                                                      _landPattern.getName(),
                                                      _landPatternPadName);
      }
      else
      {
        _padNameToLandPatternPadMap.put(_landPatternPadName, surfaceMountLandPatternPad);
        // create duplicate pad warning
        // Warn the user that two pads for the land pattern are using the same pad number and that the first
        // pad definition will be ignored.
        Object[] args = new Object[]{ _currentFile,
                                      new Integer(_is.getLineNumber()),
                                      _landPattern.getName(),
                                      _landPatternPadName};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_DUPLICATE_PAD_KEY", args));
      }
    }
    else
    {
      _padNameToLandPatternPadMap.put(_landPatternPadName, surfaceMountLandPatternPad);
    }
    _atLeastOneValidLineFound = true;
  }

  /**
   * called after all parsing is done
   * @author Jeff Ryer
   */
  protected void postProcessData() throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);

    // landpat.ndf does not have to have data. Board can have only through hole parts.
  }

  /**
   * @author George A. David
   */
  void setProject(Project project)
  {
    Assert.expect(project != null);

    _panel = project.getPanel();
  }
}
