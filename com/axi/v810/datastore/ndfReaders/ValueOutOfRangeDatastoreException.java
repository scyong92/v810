package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when an value that is out of range is encountered.
 * @author Bill Darbie
 */
public class ValueOutOfRangeDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public ValueOutOfRangeDatastoreException(String fileName,
                                           int lineNumber,
                                           String expectedSyntax,
                                           String invalidItem,
                                           double actualValue,
                                           double minValue,
                                           double maxValue)
  {
    super(new LocalizedString("DS_NDF_ERROR_VALUE_OUT_OF_RANGE_EXCEPTION_KEY", new Object[]{fileName,
                                                                                            new Integer(lineNumber),
                                                                                            expectedSyntax,
                                                                                            invalidItem,
                                                                                            new Double(actualValue),
                                                                                            new Double(minValue),
                                                                                            new Double(maxValue)}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidItem != null);
  }

  /**
   * @author Bill Darbie
   */
  public ValueOutOfRangeDatastoreException(String fileName,
                                           int lineNumber,
                                           double actualValue,
                                           double minValue,
                                           double maxValue)
  {
    super(new LocalizedString("DS_ERROR_VALUE_OUT_OF_RANGE_EXCEPTION_KEY", new Object[]{fileName,
                                                                                        new Integer(lineNumber),
                                                                                        new Double(actualValue),
                                                                                        new Double(minValue),
                                                                                        new Double(maxValue)}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
  }

  /**
   * @author Bill Darbie
   */
  public ValueOutOfRangeDatastoreException(String fileName,
                                           int lineNumber,
                                           String expectedSyntax,
                                           String invalidItem,
                                           ValueOutOfRangeException valueOutOfRangeException)
  {
    super(new LocalizedString("DS_NDF_ERROR_VALUE_OUT_OF_RANGE_EXCEPTION_KEY",
                               new Object[]{fileName,
                                            new Integer(lineNumber),
                                            expectedSyntax,
                                            invalidItem,
                                            new Double(valueOutOfRangeException.getActualValue()),
                                            new Double(valueOutOfRangeException.getMinValue()),
                                            new Double(valueOutOfRangeException.getMaxValue())}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidItem != null);
    Assert.expect(valueOutOfRangeException != null);
  }

  /**
   * @author Bill Darbie
   */
  public ValueOutOfRangeDatastoreException(String fileName,
                                           int lineNumber,
                                           ValueOutOfRangeException valueOutOfRangeException)
  {
    super(new LocalizedString("DS_ERROR_VALUE_OUT_OF_RANGE_EXCEPTION_KEY", new Object[]{fileName,
                                                                                        new Integer(lineNumber),
                                                                                        new Double(valueOutOfRangeException.getActualValue()),
                                                                                        new Double(valueOutOfRangeException.getMinValue()),
                                                                                        new Double(valueOutOfRangeException.getMaxValue())}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(valueOutOfRangeException != null);
  }

}
