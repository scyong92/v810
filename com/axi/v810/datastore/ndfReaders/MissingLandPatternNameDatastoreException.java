package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * If the land pattern name is missing, this exception should be thrown.
 * @author Bill Darbie
 */
public class MissingLandPatternNameDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public MissingLandPatternNameDatastoreException(String fileName,
                                                 int lineNumber)

  {
    super(new LocalizedString("DS_ERROR_MISSING_LAND_PATTERN_NAME_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber)}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
  }
}
