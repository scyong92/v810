package com.axi.v810.datastore.ndfReaders;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * Reads in all the ndf files (and some rtf files) for a particular panel
 * program and populates the datastore/panelDesc and datastore/panelSettings
 * classes with the data.
 *
 * Also acts as a repository for temporary data to be collected and validated.  A lot of the
 * individual readers will store data here, that will later be used by other readers or MainReader
 * itself to make sure no contrary data is being read in.
 *
 * This class creates the following Datastore objects:
 *
 * This class sets the following variables:
 *
 *   PanelSettings
 *     AlgorithmFamilyLibrary
 *
 *   Project
 *     name
 *
 *   AlgorithmFamily
 *     used
 *
 *   LandPattern
 *     setName (if surfac emount and through hole names were the same)
 *
 *   BoardType
 *     widthInNanoMeters
 *     lengthInNanoMeters
 *
 *
 * @author Keith Lee
 * @author Erica Wheatcroft
 * @author Bill Darbie
 */
public class MainReader
{
  private String _currentFile = "";
  private String _currentSideBoardName = null;
  private Map<String, Component> _referenceDesignatorToComponentMap = new HashMap<String, Component>();
  private Map<String, LandPattern> _landPatternNameToThroughHoleLandPatternMap = new HashMap<String, LandPattern>();
  private Map<String, LandPattern> _landPatternNameToSurfaceMountLandPatternMap = new HashMap<String, LandPattern>();
  private Map<LandPattern, Map<String, LandPatternPad>> _landPatternToPadNameToLandPatternPadMapMap = new HashMap<LandPattern, Map<String, LandPatternPad>>();
  private Map<Component, PadTypeEnum> _componentToPadTypeEnumMap = new HashMap<Component, PadTypeEnum>();
  private Map<CompPackage, Map<String, PackagePin>> _compPackageToPackagePinNameToPackagePinMapMap = new HashMap<CompPackage, Map<String, PackagePin>>();
  private Map<String, List<SideBoard>> _sideBoardNameToSameSideBoardInstancesMap = new HashMap<String, List<SideBoard>>();
  private Set<PackagePin> _packagePinsThatNeedDegreesRotationCalculated = new HashSet<PackagePin>();
  private Map<Board, String> _boardToLegacyRtfBoardNameMap = new HashMap<Board, String>();

  private Map<SideBoardType, MathUtilEnum> _sideBoardTypeToLegacyBoardNdfUnits = new HashMap<SideBoardType, MathUtilEnum>();
  private Map<SideBoardType, String> _sideBoardTypeToLegacyNameMap = new HashMap<SideBoardType, String>();
  private Map<SideBoardType, Integer> _sideBoardTypeToWidthInNanoMetersMap = new HashMap<SideBoardType, Integer>();
  private Map<SideBoardType, Integer> _sideBoardTypeToLengthInNanoMetersMap = new HashMap<SideBoardType, Integer>();
  private Map<SideBoard, PanelCoordinate> _sideBoardToCoordinateInNanoMetersMap = new HashMap<SideBoard, PanelCoordinate>();
  private Map<SideBoard, Integer> _sideBoardToDegreesRotationsMap = new HashMap<SideBoard, Integer>();
  private Set<Component> _inspectedComponentSet = new HashSet<Component>();
  private Set<BoardType> _possibleSplitBoardTypeSet = new HashSet<BoardType>();

  private PanelNdfReader _panelNdfReader;
  private BoardNdfReader _boardNdfReader;
  private ComponentNdfReader _componentNdfReader;
  private ThroughHoleLandPatternNdfReader _throughHoleLandPatternNdfReader;
  private SurfaceMountLandPatternNdfReader _surfaceMountLandPatternNdfReader;
  private PinsNdfReader _pinsNdfReader;
  private List<LocalizedString> _warnings = new ArrayList<LocalizedString>();

  private Project _project;
  private double _softwareRevision = 0.0;
  private static final double _MIN_WELL_FORMED_SOFTWARE_REVISION = 80.0;

  private FileWriterUtilAxi _fileWriterUtil;
  private boolean _displayTimes = false;

  /**
   * @author Andy Mechtenberg
   */
  public MainReader()
  {
    // do nothing
  }

  /**
   * Load in a project from ndf files on disk and save it to the native file format.
   *
   * @author Bill Darbie
   */
  public Project importProjectFromNdfs(String projectName, List<LocalizedString> warnings) throws DatastoreException
  {
    Assert.expect(projectName != null);
    Assert.expect(warnings != null);

    if ((_displayTimes) && (UnitTest.unitTesting() == false))
    {
      System.out.println("\n#####################################################################################");
      System.out.println("BEGIN LOAD of project: " + projectName);
      System.out.println("\n");
    }

    TimerUtil timer = new TimerUtil();

   timer.start();
    Project project = loadLegacyNdfData(projectName, warnings);
    timer.stop();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
    {
      long totalTime = timer.getElapsedTimeInMillis();
      System.out.println("\nwpd - MainReader.importProjectFromNdfs() total time to load in millis: " + totalTime + "\n");
//      int numPins = project.getPanel().getNumJoints();
//      System.out.println("wpd - number of pins: " + numPins);
//      System.out.println("wpd - pins / sec: " + (double)numPins / totalTime * 1000.0);
//      System.out.println("wpd - TOTAL LOAD TIME in milliseconds: " + totalTime);
    }

    MemoryUtil.garbageCollect();

    Assert.expect(project != null);
    return project;
  }

  /**
   * Load in a panel program from NDFs
   * @author Bill Darbie
   */
  private Project loadLegacyNdfData(String projectName, List<LocalizedString> warnings) throws DatastoreException
  {
    Assert.expect(projectName != null);
    Assert.expect(warnings != null);

    Project project = read(projectName, warnings);
    writeWarningLogFile(projectName, warnings);

    return project;
  }

  /**
   * This will write out a file in the panel directory with a name of projectName.log.  It will
   * contain all warnings that were encountered when parsing the ndf files.
   *
   * @author Bill Darbie
   */
  private void writeWarningLogFile(String projectName, List<LocalizedString> warnings) throws DatastoreException
  {
    Assert.expect(projectName != null);
    Assert.expect(warnings != null);

    String _separator = StringUtil.getLineSeparator();

    int maxLineLength = 70;

    //Ngie Xing, XCR-2033, add in check for 5dx hashed project name
    // This will decide if we load from hash code or project name
    String projectStringToLoadFrom = null;
    
    // read in the panel information
    if (NdfReader.is5dxProjectFlag())
    {
      String legacyPanelNdfFullPath = FileName.getLegacyPanelNdfFullPath(projectName);
      if (FileUtil.existsDirectory(legacyPanelNdfFullPath) == false)
      {
        // It must be users have manually extract the 5dx zip file because
        // now the folder is in the hash code form. We need to load that from
        // the hash code folder instead.
        String projectHashName = NdfReader.get5dxHashName(projectName);
        Assert.expect(projectHashName != null);
        projectStringToLoadFrom = projectHashName;
      }
      else
      {
        projectStringToLoadFrom = projectName;
      }
    }
    else
    {
      projectStringToLoadFrom = projectName;
    }
    String warningLogFileFullPath = FileName.getLoadWarningsLogFullPath(projectStringToLoadFrom);
    
    if (warnings.isEmpty())
    {
      try
      {
        if (FileUtil.exists(warningLogFileFullPath))
          FileUtil.delete(warningLogFileFullPath);
      }
      catch(CouldNotDeleteFileException ex)
      {
        CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(warningLogFileFullPath);
        dex.initCause(ex);
        throw dex;
      }
      return;
    }

    String headerString  = StringLocalizer.keyToString(new LocalizedString("DS_LOG_WARNINGS_HEADER_KEY",
                                                             new Object[]{warningLogFileFullPath}));
    headerString = StringUtil.format(headerString, maxLineLength);
    headerString = "################################################################################" + _separator + headerString;
    headerString += _separator + "################################################################################" + _separator;

    List<String> data = new ArrayList<String>();
    int warningCount = 0;
    for (LocalizedString localString : warnings)
    {
      ++warningCount;
      String message = StringLocalizer.keyToString(localString);
      message = "WARNING " + warningCount + ": " + message;
      // remove the new line if one is already there
      if (message.endsWith("\n"))
        message = message.substring(0, message.length() - 1);

      // format so that each line is 70 characters
      message = StringUtil.format(message, maxLineLength);
      // replace all new lines with new line and two spaces to get some indentation
      message = StringUtil.replace(message, _separator, _separator + "  ");
      message += _separator;

      data.add(message);
    }

    _fileWriterUtil = new FileWriterUtilAxi(warningLogFileFullPath, false);
    _fileWriterUtil.open();
    _fileWriterUtil.writeln(headerString);
    _fileWriterUtil.writeln(data);
    _fileWriterUtil.close();
  }


  /**
   * Reads and loads the panel program specified by the String projectName
   *
   * @author Keith Lee
   */
  private Project read(String projectName, List<LocalizedString> warnings) throws DatastoreException
  {
    Assert.expect(projectName != null);
    Assert.expect(warnings != null);
    warnings.clear();

    // this is used to determine if a file exists (only for files that can be
    // skipped or have multiple locations)
    File file = null;
    
    // This will decide if we load from hash code or project name
    String projectStringToLoadFrom = null;
    
    initializeBeforeReading();

    try
    {
      // read in the panel information
      if (NdfReader.is5dxProjectFlag())
      {
          String legacyPanelNdfFullPath = FileName.getLegacyPanelNdfFullPath(projectName);
          if (FileUtil.existsDirectory(legacyPanelNdfFullPath) == false)
          {
              // It must be users have manually extract the 5dx zip file because
              // now the folder is in the hash code form. We need to load that from
              // the hash code folder instead.
              String projectHashName = NdfReader.get5dxHashName(projectName);
              Assert.expect(projectHashName != null);
              
              projectStringToLoadFrom = projectHashName;
          }
          else
          {
              projectStringToLoadFrom = projectName;
          }
      }
      else
      {
          projectStringToLoadFrom = projectName;
      }
      
      _currentFile = FileName.getLegacyPanelNdfFullPath(projectStringToLoadFrom);
      _panelNdfReader.parseFile(_currentFile, _warnings);
      _project = _panelNdfReader.getProject();
      _panelNdfReader.clearProject();
      Panel panel = _project.getPanel();
      _project.setName(projectName);
      _project.setProgramGenerationVersion(1); /** @todo wpd - G$ - put your call here */

      // read in the pad geometry file (if it exists)
      _currentFile = FileName.getLegacyThroughHoleLandPatternNdfFullPath(projectStringToLoadFrom);
      file = new File(_currentFile);
      if (file.exists())
      {
        _throughHoleLandPatternNdfReader.setProject(_project);
        _throughHoleLandPatternNdfReader.parseFile(_currentFile, _warnings);
      }

      // read in the land pattern file (if it exists)
      _currentFile = FileName.getLegacySurfaceMountLandPatternNdfFullPath(projectStringToLoadFrom);
      file = new File(_currentFile);
      if (file.exists())
      {
        _surfaceMountLandPatternNdfReader.setProject(_project);
        _surfaceMountLandPatternNdfReader.parseFile(_currentFile, _warnings);
      }

      // read in the board files
      // since the boards have different side boards which share ndf files,
      // each set of board specific ndf files should be read in only once,
      // then propagated to the identical side boards
      // from PanelNdfReader
      for (List<SideBoard> sameSideBoardInstances : _sideBoardNameToSameSideBoardInstancesMap.values())
      {
        // this is passing in a List of all SideBoard's that are part of the same SideBoardType!!
        readSameSideBoard(projectStringToLoadFrom, sameSideBoardInstances);
      }

      nameBoardInstances();
      validPanelCheck();

      _project.getPanel().enableChecksAfterNdfParsersAreComplete();

      assignAll();
      
      // check have any no load components
      if (_inspectedComponentSet.isEmpty() == false)
      {
        Map<String, java.util.List<String>> boardTypeToRefDesMap = new LinkedHashMap<>();
        VariationSetting variation = new VariationSetting(_project, VariationSettingManager.getInstance().getDefaultVariationName(), true, true, boardTypeToRefDesMap);
        _project.getPanel().getPanelSettings().addVariationSetting(variation);
      }
    }
    finally
    {
      panelCleanUp();

      warnings.addAll(_warnings);
    }

    Project project = _project;
    _project = null;
    Assert.expect(project != null);
    return project;
  }

  /**
   * This method will adjust ComponentType coordinates so they are relative to the lower left corner
   * of the board when viewed from the top.  This means we will have a single origin - the same physical
   * point for both top and bottom side components.
   *
   * @author Bill Darbie
   */
  private void createSingleOriginFromTwo5dxOrigins()
  {
    for (BoardType boardType : _project.getPanel().getBoardTypes())
    {
      if (boardType.sideBoardType2Exists())
      {
        // convert side 2 component coordinates to be based on the same origin as side 1 component coordinates
        SideBoardType sideBoardType = boardType.getSideBoardType2();
        int width = boardType.getWidthInNanoMeters();
        for (ComponentType componentType : sideBoardType.getComponentTypes())
        {
          BoardCoordinate boardCoord = componentType.getCoordinateInNanoMeters();
          int x = boardCoord.getX();
          int y = boardCoord.getY();
          componentType.setCoordinateInNanoMeters(new BoardCoordinate(width - x, y));
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private void assignAll() throws DatastoreException
  {
    // find ComponentTypes that should really be Fiducials
    // assign component packages
    TimerUtil timer = new TimerUtil();
    timer.reset();
    timer.start();
    assignCompPackages();
    timer.stop();
    long time = timer.getElapsedTimeInMillis();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
      System.out.println("wpd - Time to assign component packages (and subtypes) in millis: " + time);

    // assign pad orientations, pitch and interpad distances
    timer.reset();
    timer.start();
    assignPinOrientationsPitchAndInterPadDistancesAndPadOneAndWidthLengthAndCoord();
    timer.stop();
    time = timer.getElapsedTimeInMillis();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
      System.out.println("Time to assign pin orientations, pitch and interpad distances, and Land Pattern coords in millis: " + time);

    // assign AlignmentGroups
    timer.reset();
    timer.start();
    assignAlignmentGroups();
    timer.stop();
    time = timer.getElapsedTimeInMillis();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
      System.out.println("Time to assign alignment groups in millis: " + time);

    // assign pad testability
    timer.reset();
    timer.start();
    assignPadTestability();
    timer.stop();
    time = timer.getElapsedTimeInMillis();
    if ((_displayTimes) && (UnitTest.unitTesting() == false))
      System.out.println("Time to assign pad testability: " + time);
  }

  /**
   * @author Bill Darbie
   */
  private void createFiducialsFromComponents()
  {
    for (ComponentType componentType : _project.getPanel().getComponentTypes())
    {
      String refDes = componentType.getReferenceDesignator();
      refDes = refDes.toUpperCase();
      if (refDes.startsWith("FID"))
      {
        int numPads = componentType.getLandPattern().getLandPatternPads().size();

        if (numPads == 1)
        {
          if (componentType.getLandPattern().getLandPatternPads().get(0).isSurfaceMountPad())
            changeComponentTypeToFiducial(componentType);
        }
        else if (numPads == 2)
        {
          List<LandPatternPad> landPatternPads = componentType.getLandPattern().getLandPatternPads();
          if ((landPatternPads.get(0).isSurfaceMountPad()) && (landPatternPads.get(1).isSurfaceMountPad()))
            changeComponentTypeToFiducial(componentType);
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private void changeComponentTypeToFiducial(ComponentType componentType)
  {
    Assert.expect(componentType != null);

    int numPads = componentType.getLandPattern().getLandPatternPads().size();
    if (numPads == 1)
    {
      // gather all information needed from the ComponentType
      String refDes = componentType.getReferenceDesignator();
      BoardCoordinate coord = componentType.getCoordinateInNanoMeters();
      int width = componentType.getWidthInNanoMeters();
      int length = componentType.getLengthInNanoMeters();
      ShapeEnum shapeEnum = componentType.getLandPattern().getLandPatternPads().get(0).getShapeEnum();

      // find a unique name
      String fidName = refDes;
      int i = 1;
      Panel panel = _project.getPanel();
      while (panel.isFiducialNameDuplicate(fidName))
      {
        fidName = refDes + "_" + Integer.toString(i);
        ++i;
      }

      // create the new FiducialType and Fiducials
      SideBoardType sideBoardType = componentType.getSideBoardType();
      FiducialType fidType = sideBoardType.createFiducialType(fidName, coord);
      fidType.setWidthInNanoMeters(width);
      fidType.setLengthInNanoMeters(length);
      fidType.setShapeEnum(shapeEnum);
    }
    else if (numPads == 2)
    {
      String refDes = componentType.getReferenceDesignator();
      BoardCoordinate boardCoord = componentType.getCoordinateInNanoMeters();
      List<LandPatternPad> landPatternPads = componentType.getLandPattern().getLandPatternPads();
      LandPatternPad landPatternPad1 = landPatternPads.get(0);
      LandPatternPad landPatternPad2 = landPatternPads.get(1);
      ComponentCoordinate compCoord1 = landPatternPad1.getCenterCoordinateInNanoMeters();
      ComponentCoordinate compCoord2 = landPatternPad2.getCenterCoordinateInNanoMeters();

      int width1 = landPatternPad1.getWidthInNanoMeters();
      int length1 = landPatternPad1.getLengthInNanoMeters();
      int width2 = landPatternPad2.getWidthInNanoMeters();
      int length2 = landPatternPad2.getLengthInNanoMeters();

      int boardX = boardCoord.getX();
      int boardY = boardCoord.getY();
      int compX1 = compCoord1.getX();
      int compY1 = compCoord1.getY();
      int compX2 = compCoord2.getX();
      int compY2 = compCoord2.getY();

      BoardCoordinate boardCoord1 = new BoardCoordinate(boardX + compX1, boardY + compY1);
      BoardCoordinate boardCoord2 = new BoardCoordinate(boardX + compX2, boardY + compY2);

      ShapeEnum shapeEnum1 = landPatternPad1.getShapeEnum();
      ShapeEnum shapeEnum2 = landPatternPad2.getShapeEnum();

      // find unique names
      String fid1Name = refDes;
      int i = 1;
      Panel panel = _project.getPanel();
      while (panel.isFiducialNameDuplicate(fid1Name))
      {
        fid1Name = refDes + "_" + Integer.toString(i);
        ++i;
      }
      String fid2Name = refDes + "_" + Integer.toString(i);
      while (panel.isFiducialNameDuplicate(fid2Name))
      {
        fid2Name = refDes + "_" + Integer.toString(i);
        ++i;
      }

      // create the new FiducialType and Fiducials
      SideBoardType sideBoardType = componentType.getSideBoardType();

      FiducialType fidType1 = sideBoardType.createFiducialType(fid1Name, boardCoord1);
      fidType1.setWidthInNanoMeters(width1);
      fidType1.setLengthInNanoMeters(length1);
      fidType1.setShapeEnum(shapeEnum1);

      FiducialType fidType2 = sideBoardType.createFiducialType(fid2Name, boardCoord2);
      fidType2.setWidthInNanoMeters(width2);
      fidType2.setLengthInNanoMeters(length2);
      fidType2.setShapeEnum(shapeEnum2);
    }
    else
      Assert.expect(false);

    // destroy the ComponentType
    componentType.destroy();
  }

  /**
   * @author George A. David
   */
  private void assignPadTestability()
  {
//    for (Pad pad : _project.getPanel().getPadsUnsorted())
//      pad.getPadType().getPadTypeSettings().setTestable(ProgramGeneration.isPadTestable(pad));
  }

  /**
   * @author Bill Darbie
   */
  private void assignPinOrientationsPitchAndInterPadDistancesAndPadOneAndWidthLengthAndCoord()
  {
    for (LandPattern landPattern : _project.getPanel().getAllLandPatterns())
      landPattern.assignAll();
  }

  /**
   * @author Bill Darbie
   */
  private void assignCompPackages() throws DatastoreException
  {
    // load the rules for joint type assignment first
    JointTypeEnumAssignment.getInstance().load();
    for (ComponentType componentType : _project.getPanel().getComponentTypes())
    {
      componentType.assignOrModifyCompPackageAndSubtypeAndJointTypeEnums();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void assignAlignmentGroups()
  {
    _project.getPanel().getPanelSettings().assignAlignmentGroups();

    for(Board board : _project.getPanel().getBoards())
    {
      board.getBoardSettings().assignAlignmentGroups();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void nameBoardInstances()
  {
    // first create a mapping from BoardType to a List of Boards
    Map<BoardType, List<Board>> boardTypeToBoardInstancesMap = new HashMap<BoardType, List<Board>>();
    for (Board board : _project.getPanel().getBoards())
    {
      BoardType boardType = board.getBoardType();
      List<Board> boardInstances = boardTypeToBoardInstancesMap.get(boardType);
      if (boardInstances == null)
      {
        boardInstances = new ArrayList<Board>();
        boardInstances.add(board);
        boardTypeToBoardInstancesMap.put(boardType, boardInstances);
      }
      else
      {
        boardInstances.add(board);
      }
    }
  }

  /**
   * Parse in all information for all SideBoard instances that are for the same Board type
   * @author Keith Lee
   */
  private void readSameSideBoard(String panelName, List<SideBoard> sameSideBoardInstances) throws DatastoreException
  {
    Assert.expect(panelName != null);
    Assert.expect(sameSideBoardInstances != null);
    Assert.expect(sameSideBoardInstances.isEmpty() == false);

    SideBoard sideBoard = sameSideBoardInstances.get(0);
    _currentSideBoardName = getLegacyName(sideBoard.getSideBoardType());

    // read in the component file
    _currentFile = FileName.getLegacyComponentNdfFullPath(panelName, _currentSideBoardName);
    _componentNdfReader.setProject(_project);
    _componentNdfReader.parseFile(_currentFile, _warnings);

    // read in the board file
    _currentFile = FileName.getLegacyBoardNdfFullPath(panelName, _currentSideBoardName);
    _boardNdfReader.setSameSideBoardInstances(sameSideBoardInstances);
    _boardNdfReader.parseFile(_currentFile, _warnings);

    // read in the pins.ndf file if it exists
    _currentFile = FileName.getLegacyPinsNdfFullPath(panelName, _currentSideBoardName);
    File pinsFile = new File(_currentFile);
    if (pinsFile.exists())
    {
      // on 8.2 and later releases we do not need to read the pins.ndf file anymore since alphanumerics
      // will be in all the ndfs starting with that release
      if (_softwareRevision < 82.0)
      {
        _pinsNdfReader.setSameSideBoardInstances(sameSideBoardInstances);
        _pinsNdfReader.parseFile(_currentFile, _warnings);
      }
    }

    boardCleanUp();
  }

  /**
   * The ndfs allowed two side boards on the same board to have different
   * rotation and coordinates.  Fix that here.
   * @author Bill Darbie
   */
  private void combineNdfSideBoardRotationAndCoordinatesIntoBoardRotationAndCoordinates()
  {
    Set<BoardType> boardTypeSet = new HashSet<BoardType>();

    // iterate over every Board
    for (Board board : _project.getPanel().getBoards())
    {
      AffineTransform trans = AffineTransform.getScaleInstance(1.0, 1.0);
      AffineTransform preTrans = AffineTransform.getScaleInstance(1.0, 1.0);
      int componentTypeRotation = 0;

      BoardType boardType = board.getBoardType();
      int sideBoard2DegreesRotation = 0;

      if (boardTypeSet.contains(boardType) == false)
      {
        boardTypeSet.add(boardType);

        // this is the first time we have seen this BoardType, so
        // do what is necessary to get the top SideBoard and bottom SideBoard aligned
        // so they can share the same coordinate, rotation, width and length
        SideBoardType sideBoardType2 = null;
        int sideBoard2Width = 0;
        int sideBoard2Length = 0;
        if (boardType.sideBoardType2Exists())
        {
          sideBoardType2 = boardType.getSideBoardType2();
          SideBoard sideBoard2 = board.getSideBoard2();
          sideBoard2DegreesRotation = getDegreesRotation(sideBoard2);
          sideBoard2Width = getWidthInNanoMeters(sideBoardType2);
          sideBoard2Length = getLengthInNanoMeters(sideBoardType2);

          // now we want to apply the bottom SideBoard rotation about the bottom right
          // corner of the SideBoard

          // first take the ComponentTypeCoordinates and flip them right to left
          // in order to get the data so it is relative to the bottom left corner
          // instead of the bottom right corner
          AffineTransform leftToRightFlipTrans = AffineTransform.getScaleInstance(-1, 1);
          leftToRightFlipTrans.preConcatenate(AffineTransform.getTranslateInstance(sideBoard2Width, 0));
          translateComponentTypeCoords(sideBoardType2, leftToRightFlipTrans, 0);

          // translate the bottom SideBoard ComponentType coordinates
          // to get the bottom right corner of the SideBoard at the origin
          preTrans = AffineTransform.getTranslateInstance( -sideBoard2Width, 0);
          trans.preConcatenate(preTrans);
        }

        if (boardType.sideBoardType1Exists())
        {
          // rotation and coordinate should match the top side
          SideBoard sideBoard1 = board.getSideBoard1();
          int side1DegreesRotation = getDegreesRotation(sideBoard1);
          PanelCoordinate coord = getCoordinateInNanoMeters(sideBoard1);

          board.setCoordinateInNanoMeters(coord);
          board.setDegreesRotationRelativeToPanelFromNdfReaders(side1DegreesRotation);
        }

        if ((boardType.sideBoardType1Exists()) && (boardType.sideBoardType2Exists() == false))
        {
          // do nothing
        }
        else if ((boardType.sideBoardType1Exists()) && (boardType.sideBoardType2Exists()))
        {
          // now rotate the ComponentType coordinates around the origin at the degrees specified
          // by the bottom side ndfs
          // convert the rotation to be as viewed from the top instead of the bottom
          int degreesRotation = 360 - sideBoard2DegreesRotation;
          componentTypeRotation += degreesRotation;
          preTrans = AffineTransform.getRotateInstance(Math.toRadians(degreesRotation));
          trans.preConcatenate(preTrans);

          // figure out new width and length (after the previous rotation)
          if ((sideBoard2DegreesRotation == 90) || (sideBoard2DegreesRotation == 270))
          {
            int temp = sideBoard2Width;
            sideBoard2Width = sideBoard2Length;
            sideBoard2Length = temp;
          }
          else if ((sideBoard2DegreesRotation != 0) && (sideBoard2DegreesRotation != 180))
            Assert.expect(false);

          // now translate ComponentType coordinates to get the bottom SideBoard aligned under
          // its top SideBoard.  To do this we need to rotate the bottom SideBoard ComponentType
          // coordinates by the negative amount of the top SideBoard rotation

          // first get the correct corner of the bottom SideBoard at the origin
          SideBoard sideBoard1 = board.getSideBoard1();
          int sideBoard1DegreesRotation = getDegreesRotation(sideBoard1);
          if (sideBoard1DegreesRotation == 0)
          {
            // need to get the bottom left corner of the bottom SideBoard to the origin
            // for the next rotation
            if (sideBoard2DegreesRotation == 0)
              preTrans = AffineTransform.getTranslateInstance(sideBoard2Width, 0);
            else if (sideBoard2DegreesRotation == 90)
              preTrans = AffineTransform.getTranslateInstance(0, 0);
            else if (sideBoard2DegreesRotation == 180)
              preTrans = AffineTransform.getTranslateInstance(0, sideBoard2Length);
            else if (sideBoard2DegreesRotation == 270)
              preTrans = AffineTransform.getTranslateInstance(sideBoard2Width, sideBoard2Length);
            else
              Assert.expect(false);
          }
          else if (sideBoard1DegreesRotation == 90)
          {
            // need to get the bottom right corner of the bottom SideBoard to the origin
            // for the next rotation
            if (sideBoard2DegreesRotation == 0)
              preTrans = AffineTransform.getTranslateInstance(0, 0);
            else if (sideBoard2DegreesRotation == 90)
              preTrans = AffineTransform.getTranslateInstance( -sideBoard2Width, 0);
            else if (sideBoard2DegreesRotation == 180)
              preTrans = AffineTransform.getTranslateInstance( -sideBoard2Width, sideBoard2Length);
            else if (sideBoard2DegreesRotation == 270)
              preTrans = AffineTransform.getTranslateInstance(0, sideBoard2Length);
            else
              Assert.expect(false);
          }
          else if (sideBoard1DegreesRotation == 180)
          {
            // need to get the top right corner of the bottom SideBoard to the origin
            // for the next rotation
            if (sideBoard2DegreesRotation == 0)
              preTrans = AffineTransform.getTranslateInstance(0, -sideBoard2Length);
            else if (sideBoard2DegreesRotation == 90)
              preTrans = AffineTransform.getTranslateInstance( -sideBoard2Width, -sideBoard2Length);
            else if (sideBoard2DegreesRotation == 180)
              preTrans = AffineTransform.getTranslateInstance( -sideBoard2Width, 0);
            else if (sideBoard2DegreesRotation == 270)
              preTrans = AffineTransform.getTranslateInstance(0, 0);
            else
              Assert.expect(false);
          }
          else if (sideBoard1DegreesRotation == 270)
          {
            // need to get the top left of the bottom SideBoard to the origin
            // for the next rotation
            if (sideBoard2DegreesRotation == 0)
            preTrans = AffineTransform.getTranslateInstance(sideBoard2Width, -sideBoard2Length);
            else if (sideBoard2DegreesRotation == 90)
              preTrans = AffineTransform.getTranslateInstance(0, -sideBoard2Length);
            else if (sideBoard2DegreesRotation == 180)
            {
              preTrans = AffineTransform.getTranslateInstance(0, 0);
            }
            else if (sideBoard2DegreesRotation == 270)
              preTrans = AffineTransform.getTranslateInstance(sideBoard2Width, 0);
            else
              Assert.expect(false);
          }
          else
            Assert.expect(false);
          trans.preConcatenate(preTrans);

          // now rotate the ComponentType coordinates to get the bottom SideBoard aligned under
          // its top SideBoard
          degreesRotation =  -sideBoard1DegreesRotation;
          componentTypeRotation += degreesRotation;

          preTrans = AffineTransform.getRotateInstance(Math.toRadians(degreesRotation));
          trans.preConcatenate(preTrans);

          // figure out new width and length (after the previous rotation)
          if ((sideBoard1DegreesRotation == 90) || (sideBoard1DegreesRotation == 270))
          {
            int temp = sideBoard2Width;
            sideBoard2Width = sideBoard2Length;
            sideBoard2Length = temp;
          }
          else if ((sideBoard1DegreesRotation != 0) && (sideBoard1DegreesRotation != 180))
            Assert.expect(false);

          // OK - now apply the transform
          translateComponentTypeCoords(sideBoardType2, trans, componentTypeRotation);
        }
        else if ((boardType.sideBoardType1Exists() == false) && (boardType.sideBoardType2Exists()))
        {
          // rotation should be oldNdfRotation - 90
          // coordinate should be bottom side coordinate converted

          // apply -90 to the ndf rotation to the ComponentType coordinates.  This needs to be done because
          // this gets the bottomSideBoard to be in the 1st quadrant.  The first quadrant is the
          // only quadrant where a rotation about the bottom left corner can be applied to get
          // the board to any other quadrant
          // Note that the rotation is about the bottom right corner
          int degreesRotation = -90;
          componentTypeRotation += degreesRotation;
          preTrans = AffineTransform.getRotateInstance(Math.toRadians(degreesRotation));
          trans.preConcatenate(preTrans);

          // figure out new width and length (after the previous rotation)
          degreesRotation = MathUtil.getDegreesWithin0To359(degreesRotation);

          if ((degreesRotation == 90) || (degreesRotation == 270))
          {
            int temp = sideBoard2Width;
            sideBoard2Width = sideBoard2Length;
            sideBoard2Length = temp;
          }
          else if ((degreesRotation == 0) && (degreesRotation == 180))
          {
            // do nothing
          }
          else
            Assert.expect(false);

          // OK - now apply the transform
          translateComponentTypeCoords(sideBoardType2, trans, componentTypeRotation);

          // now set the new width, and length
          if ((degreesRotation == 90) || (degreesRotation == 270))
          {
            int temp = boardType.getWidthInNanoMeters();
            boardType.setWidthInNanoMeters(boardType.getLengthInNanoMeters());
            boardType.setLengthInNanoMeters(temp);
          }
          else if ((degreesRotation == 0) && (degreesRotation == 180))
          {
            // do nothing
          }
          else
            Assert.expect(false);

          // set the boards coordinate
          // xCoord = oldX - newBoardLength
          SideBoard sideBoard2 = board.getSideBoard2();
          PanelCoordinate oldCoord = getCoordinateInNanoMeters(sideBoard2);
          int x = oldCoord.getX();
          int y = oldCoord.getY();
          if (sideBoard2DegreesRotation == 0)
          {
            x += sideBoard2Length;
          }
          else if (sideBoard2DegreesRotation == 90)
          {
            x -= sideBoard2Width;
          }
          else if (sideBoard2DegreesRotation == 180)
          {
            x -= sideBoard2Length;
          }
          else if (sideBoard2DegreesRotation == 270)
          {
            x += sideBoard2Width;
          }
          else
            Assert.expect(false);
          board.setCoordinateInNanoMeters(new PanelCoordinate(x, y));

        }
        else
          Assert.expect(false);

        if (boardType.sideBoardType2Exists())
        {
          // now apply another left-to-right flip to get the bottom side data back
          // to be relative to the bottom right corner when viewed from the top

          AffineTransform leftToRightFlipTrans = AffineTransform.getScaleInstance(-1, 1);
          leftToRightFlipTrans.preConcatenate(AffineTransform.getTranslateInstance(sideBoard2Width, 0));
          translateComponentTypeCoords(sideBoardType2, leftToRightFlipTrans, 0);
        }
      }
      else
      {
        // we have seen this BoardType before, so set the
        // coordinate and rotation in the Board instance correctly here
        if (boardType.sideBoardType1Exists())
        {
          // we have only a top side or a top and bottom side

          // rotation and coordinate should match the top side
          SideBoard sideBoard1 = board.getSideBoard1();
          int degreesRotation = getDegreesRotation(sideBoard1);
          PanelCoordinate coord = getCoordinateInNanoMeters(sideBoard1);

          board.setCoordinateInNanoMeters(coord);
          board.setDegreesRotationRelativeToPanelFromNdfReaders(degreesRotation);
        }
        else if (boardType.sideBoardType2Exists())
        {
          // we have only a bottom side

          // apply -90 to the ndf rotation
          SideBoard sideBoard2 = board.getSideBoard2();
          sideBoard2DegreesRotation = getDegreesRotation(sideBoard2);
          board.setDegreesRotationRelativeToPanelFromNdfReaders(sideBoard2DegreesRotation - 90);

          // set the boards coordinate to be the same as the original bottom side coordinate
          SideBoard sideBoard = board.getSideBoard2();
          // set the boards coordinate
          // xCoord = oldX - newBoardLength
          // yCoord = oldY
          int length = sideBoard.getBoard().getLengthInNanoMeters();
          PanelCoordinate oldCoord = getCoordinateInNanoMeters(sideBoard);
          PanelCoordinate newCoord = new PanelCoordinate(oldCoord.getX() - length, oldCoord.getY());
          board.setCoordinateInNanoMeters(newCoord);
        }
        else
          Assert.expect(false);
      }
    }

    // ok - now iterate over the boards one more time to adjust rotation of boards that
    // only have a bottom side
    for (Board board : _project.getPanel().getBoards())
    {
      BoardType boardType = board.getBoardType();

      if ((boardType.sideBoardType1Exists() == false) && (boardType.sideBoardType2Exists()))
      {
        // set top side rotation of any boards using this SideBoardType to be 90 - oldNdfRotation
        SideBoard sideBoard2 = board.getSideBoard2();
        int oldNdfRotation = getDegreesRotation(sideBoard2);
        int newRotation = 90 - oldNdfRotation;
        newRotation = MathUtil.getDegreesWithin0To359(newRotation);
        board.setDegreesRotationRelativeToPanelFromNdfReaders(newRotation);
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private void translateComponentTypeCoords(SideBoardType sideBoardType,
                                            AffineTransform trans,
                                            int componentTypeRotation)
  {
    Assert.expect(sideBoardType != null);
    Assert.expect(trans != null);

    // since the component is on the bottom take 360 - the rotation
    componentTypeRotation = 360 - componentTypeRotation;

    for (ComponentType componentType : sideBoardType.getComponentTypes())
    {
      double degrees = componentType.getDegreesRotationRelativeToBoard() + componentTypeRotation;
      degrees = MathUtil.getDegreesWithin0To359(degrees);

      componentType.setDegreesRotationRelativeToBoard(degrees);
      BoardCoordinate coord = componentType.getCoordinateInNanoMeters();
      Point2D oldPoint = new Point2D.Double(coord.getX(), coord.getY());
      Point2D newPoint = new Point2D.Double(0, 0);
      trans.transform(oldPoint, newPoint);
      coord.setLocation((int)Math.round(newPoint.getX()), (int)Math.round(newPoint.getY()));
      componentType.setCoordinateInNanoMeters(coord);
    }
  }

  /**
   * null all references to temporary objects so the garbage collector can clean things up
   * @author Bill Darbie
   */
  private void panelCleanUp()
  {
    //board stuff
    _referenceDesignatorToComponentMap.clear();

    // panel stuff
    _landPatternNameToThroughHoleLandPatternMap.clear();
    _landPatternNameToSurfaceMountLandPatternMap.clear();
    _landPatternToPadNameToLandPatternPadMapMap.clear();
    _componentToPadTypeEnumMap.clear();
    _compPackageToPackagePinNameToPackagePinMapMap.clear();
    _sideBoardNameToSameSideBoardInstancesMap.clear();
    _inspectedComponentSet.clear();
    _packagePinsThatNeedDegreesRotationCalculated.clear();
    _boardToLegacyRtfBoardNameMap.clear();
    _sideBoardTypeToWidthInNanoMetersMap.clear();
    _sideBoardTypeToLengthInNanoMetersMap.clear();
    _sideBoardTypeToLegacyBoardNdfUnits.clear();
    _sideBoardTypeToLegacyNameMap.clear();
    _possibleSplitBoardTypeSet.clear();

    _sideBoardToDegreesRotationsMap.clear();
    _sideBoardToCoordinateInNanoMetersMap.clear();
  }

  /**
   * null all references to temporary objects so the garbage collector can clean things up.
   * also create new instances of board specific NdfReaders.
   * @author Keith Lee
   */
  private void boardCleanUp()
  {
    _referenceDesignatorToComponentMap.clear();

    _componentNdfReader = new ComponentNdfReader(this);
    _pinsNdfReader = new PinsNdfReader(this);
    _boardNdfReader = new BoardNdfReader(this);

  }

  /**
   * initializes the variables for reading and loading a panel program
   *
   * @author Keith Lee
   */
  private void initializeBeforeReading()
  {
    _currentFile = null;
    _warnings.clear();
    boardCleanUp();
    panelCleanUp();

    if (_panelNdfReader == null)
      _panelNdfReader = new PanelNdfReader(this);

    if (_boardNdfReader == null)
      _boardNdfReader = new BoardNdfReader(this);

    if (_componentNdfReader == null)
      _componentNdfReader = new ComponentNdfReader(this);

    if (_throughHoleLandPatternNdfReader == null)
      _throughHoleLandPatternNdfReader = new ThroughHoleLandPatternNdfReader(this);

    if (_surfaceMountLandPatternNdfReader == null)
      _surfaceMountLandPatternNdfReader = new SurfaceMountLandPatternNdfReader(this);

    if (_pinsNdfReader == null)
      _pinsNdfReader = new PinsNdfReader(this);

    _softwareRevision = 0;
  }

  /**
   * check for duplicate land pattern names (surface mount and through hole land pattern with the same name)
   * @author Bill Darbie
   */
  private void checkForDuplicateLandPatternNames() throws DatastoreException
  {
    Collection<LandPattern> surfaceMount = new HashSet<LandPattern>(getSurfaceMountLandPatterns());
    Collection<LandPattern> throughHole = new HashSet<LandPattern>(getThroughHoleLandPatterns());
    Iterator landPatternIt = null;
    //iterate over smaller Collection to save time
    boolean surfaceMountCollectionSmaller;
    if (surfaceMount.size() <= throughHole.size())
    {
      landPatternIt = surfaceMount.iterator();
      surfaceMountCollectionSmaller = true;
    }
    else
    {
      landPatternIt = throughHole.iterator();
      surfaceMountCollectionSmaller = false;
    }

    while (landPatternIt.hasNext())
    {
      LandPattern landPattern = (LandPattern)landPatternIt.next();
      LandPattern duplicateLandPattern = null;
      String name = landPattern.getName();
      boolean landPatternExists = false;
      if (surfaceMountCollectionSmaller)
      {
        landPatternExists = doesThroughHoleLandPatternExist(name);
        if (landPatternExists)
        {
          duplicateLandPattern = getThroughHoleLandPattern(name);
        }
      }
      else
      {
        landPatternExists = doesSurfaceMountLandPatternExist(name);
        if (landPatternExists)
        {
          duplicateLandPattern = getSurfaceMountLandPattern(name);
        }
      }
      if (landPatternExists)
      {
        if (ndfsAreWellFormed())
        {
          //error : duplicate land pattern names
          throw new DuplicateLandPatternNamesDatastoreException(name,
                                                                FileName.getLegacySurfaceMountLandPatternNdfFullPath(_project.getName()),
                                                                FileName.getLegacyThroughHoleLandPatternNdfFullPath(_project.getName()));
        }
        else
        {
          //add _SM and _TH to the end of the names (name_SM and name_TH)
          String newSurfaceMountName = name + "_SM";
          String newThroughHoleName = name + "_TH";
          boolean surfaceMountExists = doesSurfaceMountLandPatternExist(newSurfaceMountName);
          boolean throughHoleExists = doesThroughHoleLandPatternExist(newThroughHoleName);

          //name_TH or name_SM already exist. add a number on the end of the
          //name until we find a unique pair (i.e. name_SM1 and name_TH1)
          if (surfaceMountExists || throughHoleExists)
          {
            int i = 0;
            while (surfaceMountExists && throughHoleExists)
            {
              ++i;
              surfaceMountExists = doesSurfaceMountLandPatternExist(newSurfaceMountName + i);
              throughHoleExists = doesThroughHoleLandPatternExist(newThroughHoleName + i);
            }
            newSurfaceMountName += i;
            newThroughHoleName += i;
          }

          // change duplicate surface mount and through hole land patterns used by
          // Components in each SideBoard
          String origLandPatternName = landPattern.getName();
          for (SideBoard sideBoard : getAllSideBoards())
          {
            SideBoardType sideBoardType = sideBoard.getSideBoardType();
            for (ComponentType componentType : sideBoardType.getComponentTypes())
            {
              LandPattern componentLandPattern = componentType.getLandPattern();
              if (componentLandPattern.getName().equalsIgnoreCase(name))
              {
                if (componentLandPattern.getPadTypeEnum().equals(PadTypeEnum.SURFACE_MOUNT))
                  componentLandPattern.setName(newSurfaceMountName);
                else if (componentLandPattern.getPadTypeEnum().equals(PadTypeEnum.THROUGH_HOLE))
                  componentLandPattern.setName(newThroughHoleName);
                else
                  Assert.expect(false, name + " land pattern is not surface mount or through hole in SideBoard " + getLegacyName(sideBoardType));

                  // warning : renaming land patterns
                  Object[] args = new Object[]{ name,
                                                newSurfaceMountName,
                                                newThroughHoleName,
                                                FileName.getLegacySurfaceMountLandPatternNdfFullPath(_project.getName()),
                                                FileName.getLegacyThroughHoleLandPatternNdfFullPath(_project.getName()),
                                                FileName.getLegacyBoardNdfFullPath(_project.getName(), getLegacyName(sideBoardType))};

                _warnings.add(new LocalizedString("DS_NDF_WARNING_DUPLICATE_LAND_PATTERN_NAMES_KEY", args));
              }
            }
          }

          // change through hole and surface mount land patterns in Collections
          // landPattern is surface mount; duplicateLandPattern is top side through hole
          if (surfaceMountCollectionSmaller)
          {
            // change surface mount
            setNewSurfaceMountLandPatternName(origLandPatternName, newSurfaceMountName);
            landPattern.setName(newSurfaceMountName);

            // change top side through hole
            setNewThroughHoleLandPatternName(origLandPatternName, newThroughHoleName);
            duplicateLandPattern.setName(newThroughHoleName);
          }
          else //landPattern is top side through hole; duplicateLandPattern is surface mount
          {
            //change surface mount
            duplicateLandPattern.setName(newSurfaceMountName);
            //change top side through hole
            landPattern.setName(newThroughHoleName);
          }
        }
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  private void checkThatSide1AndSide2WidthAndLengthMatch() throws DatastoreException
  {
    Set<BoardType> boardTypeSet = new HashSet<BoardType>();

    // check that top side board and bottom side board dimensions match (width, length)
    List<Board> boards = _project.getPanel().getBoards();
    for (Board board : boards)
    {
      BoardType boardType = board.getBoardType();

      int boardWidth = 0;
      int boardLength = 0;
      if ((boardType.sideBoardType1Exists() == false) || (boardType.sideBoardType2Exists() == false))
      {
        // we have a one sided board, so set its width and length - no checking needed.
        // just set the BoardType's width and length to the proper setting
        SideBoardType sideBoardType = null;
        if (boardType.sideBoardType1Exists())
          sideBoardType = boardType.getSideBoardType1();
        else
          sideBoardType = boardType.getSideBoardType2();

        boardWidth = getWidthInNanoMeters(sideBoardType);
        boardLength = getLengthInNanoMeters(sideBoardType);
      }
      else if (boardType.sideBoardType1Exists() && boardType.sideBoardType2Exists())
      {
        // we have a two sided board

        SideBoardType sideBoardType1 = boardType.getSideBoardType1();
        SideBoardType sideBoardType2 = boardType.getSideBoardType2();
        SideBoard sideBoard1 = board.getSideBoard1();
        SideBoard sideBoard2 = board.getSideBoard2();

        // set boardWidth and boardLength to the top side values as a starting point
        boardWidth = getWidthInNanoMeters(sideBoardType1);
        boardLength = getLengthInNanoMeters(sideBoardType1);

        // check the width and length
        DoubleRef sideBoard1Width = new DoubleRef(getWidthInNanoMeters(sideBoardType1));
        DoubleRef sideBoard1Length = new DoubleRef(getLengthInNanoMeters(sideBoardType1));
        DoubleRef sideBoard2Width = new DoubleRef(getWidthInNanoMeters(sideBoardType2));
        DoubleRef sideBoard2Length = new DoubleRef(getLengthInNanoMeters(sideBoardType2));

        GeomUtil.rotateWidthAndLengthOfRectangleAroundOrigin(sideBoard1Width,
                                                             sideBoard1Length,
                                                             getDegreesRotation(sideBoard1));
        GeomUtil.rotateWidthAndLengthOfRectangleAroundOrigin(sideBoard2Width,
                                                             sideBoard2Length,
                                                             getDegreesRotation(sideBoard2));

        if (sideBoard1Width.fuzzyEquals(sideBoard2Width) == false || sideBoard1Length.fuzzyEquals(sideBoard2Length) == false)
        {
          MathUtilEnum side1Units = getLegacyBoardNdfUnits(sideBoardType1);
          MathUtilEnum side2Units = getLegacyBoardNdfUnits(sideBoardType2);

          double side1WidthInUnits = MathUtil.convertUnits(sideBoard1Width.getValue(),
                                                         MathUtilEnum.NANOMETERS,
                                                         side1Units);
          double side1LengthInUnits = MathUtil.convertUnits(sideBoard1Length.getValue(),
                                                          MathUtilEnum.NANOMETERS,
                                                          side1Units);
          double side2WidthInUnits = MathUtil.convertUnits(sideBoard2Width.getValue(),
                                                            MathUtilEnum.NANOMETERS,
                                                            side1Units);
          double side2LengthInUnits = MathUtil.convertUnits(sideBoard2Length.getValue(),
                                                             MathUtilEnum.NANOMETERS,
                                                             side1Units);

          if (ndfsAreWellFormed())
          {
            throw new DifferentBoardDimensionsDatastoreException(new Double(side1WidthInUnits),
                                                                 new Double(side1LengthInUnits),
                                                                 side1Units.toString(),
                                                                 FileName.getLegacyBoardNdfFullPath(_project.getName(),
                                                                                                    getLegacyName(sideBoardType1)),
                                                                 new Double(side2WidthInUnits),
                                                                 new Double(side2LengthInUnits),
                                                                 side2Units.toString(),
                                                                 FileName.getLegacyBoardNdfFullPath(_project.getName(),
                                                                 getLegacyName(sideBoardType2)));
          }
          else
          {
            Object[] args = new Object[] { new Double(side1WidthInUnits),
                                           new Double(side1LengthInUnits),
                                           side1Units.toString(),
                                           FileName.getLegacyBoardNdfFullPath(_project.getName(),
                                                                              getLegacyName(sideBoardType1)),
                                           new Double(side2WidthInUnits),
                                           new Double(side2LengthInUnits),
                                           side2Units.toString(),
                                           FileName.getLegacyBoardNdfFullPath(_project.getName(),
                                                                              getLegacyName(sideBoardType2))};
            _warnings.add(new LocalizedString("DS_NDF_WARNING_DIFFERENT_BOARD_DIMENSIONS_KEY", args));

            // in this case set the width and height to the largest side board values
            double boardWidthDouble = sideBoard2Width.getValue();
            if (sideBoard1Width.getValue() > sideBoard2Width.getValue())
              boardWidthDouble = sideBoard1Width.getValue();
            double boardLengthDouble = sideBoard2Length.getValue();
            if (sideBoard1Length.getValue() > sideBoard2Length.getValue())
              boardLengthDouble = sideBoard1Length.getValue();

            DoubleRef width = new DoubleRef(boardWidthDouble);
            DoubleRef length = new DoubleRef(boardLengthDouble);

            // now rotate the width and length back to its original rotation
            GeomUtil.rotateWidthAndLengthOfRectangleAroundOrigin(width,
                                                                 length,
                                                                 -getDegreesRotation(sideBoard1));
            boardWidth = (int)width.getValue();
            boardLength = (int)length.getValue();
          }
        }
      }

      if (boardTypeSet.contains(boardType) == false)
      {
        // ok - set the board with the proper width, length, and degrees rotation
        boardTypeSet.add(boardType);
        boardType.setWidthInNanoMeters(boardWidth);
        boardType.setLengthInNanoMeters(boardLength);
      }
    }
  }

  /**
   * Performs a variety of checks, including:
   * <li> check for a surface mount land pattern name the same as a through hole land pattern
   * <li> check the thickness of the primary and secondary board to make sure they are the same
   * <li> check that top side board and bottom side board dimensions match (width, length)
   * <li> remove any components in the list that are NOT tested AND cannot be changed to tested. (variety of reasons)
   *
   * @author Keith Lee
   */
  private void validPanelCheck() throws DatastoreException
  {
    checkForDuplicateLandPatternNames();
    checkThatSide1AndSide2WidthAndLengthMatch();
    // the ndfs allowed two side boards on the same board to have different
    // rotation and coordinates.  Fix that here
    combineNdfSideBoardRotationAndCoordinatesIntoBoardRotationAndCoordinates();

    // assign a single origin on the top side lower left corner for both top and bottom side boards
    createSingleOriginFromTwo5dxOrigins();
    createFiducialsFromComponents();
    recombineSplitBoards();
    checkForDuplicateComponentNamesOnAllBoards();
  }

  /**
   * @author Bill Darbie
   */
  private void recombineSplitBoards()
  {
    Set<Pair<Board, Board>> splitBoardPairSet = new HashSet<Pair<Board, Board>>();
    Set<Board> boardSet = new HashSet<Board>();

    for (BoardType possibleSplitBoardType1 : _possibleSplitBoardTypeSet)
    {
      Set<BoardType> possibleSplitBoardTypeSet2 = new HashSet<BoardType>(_possibleSplitBoardTypeSet);
      possibleSplitBoardTypeSet2.remove(possibleSplitBoardType1);

      // determine if the Boards that belong to the BoardTypes overlap
      // if they do then we need to recombine them
      BoardType boardType1 = possibleSplitBoardType1;
      for (BoardType boardType2 : possibleSplitBoardTypeSet2)
      {
        for (Board board1 : boardType1.getBoards())
        {
          PanelCoordinate panelCoord1 = board1.getCadOriginRelativeToPanelInNanometers();
          int xCoord1 = panelCoord1.getX();
          int yCoord1 = panelCoord1.getY();
          int length1 = board1.getLengthAfterAllRotationsInNanoMeters();
          for (Board board2 : boardType2.getBoards())
          {
            PanelCoordinate panelCoord2 = board2.getCadOriginRelativeToPanelInNanometers();
            int xCoord2 = panelCoord2.getX();
            int yCoord2 = panelCoord2.getY();
            int length2 = board2.getLengthAfterAllRotationsInNanoMeters();

            if (MathUtil.fuzzyEquals(xCoord1, xCoord2))
            {
              if (yCoord1 < yCoord2)
              {
                if ((yCoord1 + length1) > yCoord2)
                {
                  // the 2 boards overlap
                  if (boardSet.add(board1)) // makes sure we do not add same pair more than once
                  {
                    if (boardSet.add(board2)) // makes sure we do not add same pair more than once
                    {
                      Pair<Board, Board> pair = new Pair<Board, Board>(board1, board2);
                      splitBoardPairSet.add(pair);
                    }
                  }
                }
              }
              else
              {
                if ((yCoord2 + length2) > yCoord1)
                {
                  // the 2 boards overlap
                  if (boardSet.add(board1)) // makes sure we do not add same pair more than once
                  {
                    if (boardSet.add(board2)) // makes sure we do not add same pair more than once
                    {
                      Pair<Board, Board> pair = new Pair<Board, Board>(board2, board1);
                      splitBoardPairSet.add(pair);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    recombineSplitBoards(splitBoardPairSet);
  }

  /**
   * @author Bill Darbie
   */
  private void recombineSplitBoards(Set<Pair<Board, Board>> splitBoardPairSet)
  {
    Map<BoardType, Integer> boardTypeToWidthMap = new HashMap<BoardType, Integer>();
    Map<BoardType, Integer> boardTypeToLengthMap = new HashMap<BoardType, Integer>();
    Set<BoardType> boardTypeSet = new HashSet<BoardType>();
    for (Pair<Board, Board> splitBoardPair : splitBoardPairSet)
    {
      Board board1 = splitBoardPair.getFirst();
      Board board2 = splitBoardPair.getSecond();

      // combine board2 into board1

      // we always want to move to the same boardType in case we have pairs that share so
      // to be consistent always have board1 be the one that has the first name when sorted
      // by the AlphanumericComparator
      AlphaNumericComparator comparator = new AlphaNumericComparator();
      if (comparator.compare(board1.getBoardType().getName(), board2.getBoardType().getName()) > 0)
      {
        Board tempBoard = board1;
        board1 = board2;
        board2 = tempBoard;
      }

      BoardType boardType1 = board1.getBoardType();
      BoardType boardType2 = board2.getBoardType();
      int length1 = board1.getLengthInNanoMeters();
      int length2 = board2.getLengthInNanoMeters();
      int width1 = board1.getWidthInNanoMeters();
      int width2 = board2.getWidthInNanoMeters();
      PanelCoordinate panelCoord1 = board1.getCadOriginRelativeToPanelInNanometers();
      PanelCoordinate panelCoord2 = board2.getCadOriginRelativeToPanelInNanometers();
      int x1 = panelCoord1.getX();
      int y1 = panelCoord1.getY();
      int x2 = panelCoord2.getX();
      int y2 = panelCoord2.getY();
      int deltaX = x2 - x1;
      int deltaY = y2 - y1;
      java.awt.geom.Rectangle2D rect1 = board1.getShapeRelativeToPanelInNanoMeters().getBounds2D();
      java.awt.geom.Rectangle2D rect2 = board2.getShapeRelativeToPanelInNanoMeters().getBounds2D();

      boolean flipX = false;
      boolean flipY = false;
      boolean swapXandY = false;
      // figure out where the origin of the boards are and make appropriate adjustments to things
      PanelCoordinate panelCadCoord1 = board1.getCadOriginRelativeToPanelInNanometers();
      int cadX = panelCadCoord1.getX();
      int cadY = panelCadCoord1.getY();
      PanelCoordinate panelLowerLeftCoord1 = board1.getLowerLeftCoordinateRelativeToPanelInNanoMeters();
      int lowerLeftX = panelLowerLeftCoord1.getX();
      int lowerLeftY = panelLowerLeftCoord1.getY();
      if (MathUtil.fuzzyEquals(cadX, lowerLeftX) && MathUtil.fuzzyEquals(cadY, lowerLeftY))
      {
        // boards x,y is at lower left - 0
        flipX = false;
        flipY = false;
        swapXandY = false;
      }
      else if ((MathUtil.fuzzyEquals(cadX, lowerLeftX) == false) && MathUtil.fuzzyEquals(cadY, lowerLeftY))
      {
        // boards x,y is at lower right - 90
        flipX = true;
        flipY = false;
        swapXandY = true;
      }
      else if (MathUtil.fuzzyEquals(cadX, lowerLeftX) && (MathUtil.fuzzyEquals(cadY, lowerLeftY) == false))
      {
        // boards x,y is at upper left - 270
        flipX = false;
        flipY = true;
        swapXandY = true;
      }
      else if ((MathUtil.fuzzyEquals(cadX, lowerLeftX) == false) && (MathUtil.fuzzyEquals(cadY, lowerLeftY) == false))
      {
        // boards x,y is at upper right - 180
        flipX = true;
        flipY = true;
        swapXandY = false;
      }
      else
        Assert.expect(false);

      if (flipX)
        deltaX = -deltaX;
      if (flipY)
        deltaY = -deltaY;

      if (swapXandY)
      {
        // swap x1 and y1
        int temp = x1;
        x1 = y1;
        y1 = temp;

        // swap x2 and y2
        temp = x2;
        x2 = y2;
        y2 = temp;

        // swap deltaX and deltaY
        temp = deltaX;
        deltaX = deltaY;
        deltaY = temp;

        // swap length1 with width1
        temp = length1;
        length1 = width1;
        width1 = temp;

        // swap length2 with width2
        temp = length2;
        length2 = width2;
        width2 = temp;
      }

      boolean alreadyProcessedBoardType = false;
      if (boardTypeSet.add(boardType1) == false)
        alreadyProcessedBoardType = true;

      boolean alreadyProcessedBoardType2 = false;
      if (boardTypeSet.add(boardType2) == false)
        alreadyProcessedBoardType2 = true;
      Assert.expect(alreadyProcessedBoardType == alreadyProcessedBoardType2);

      // set board1 to the correct name
      String name1 = board1.getName();
      String name2 = board2.getName();

      if (comparator.compare(name1, name2) > 0)
      {
        // swap the names because we are going to get rid of board1 but want to keep its name
        // find a unique temporary name to use
        // first board names
        String tempName = name1 + "_temp";
        int i = 0;
        while (board1.getPanel().isBoardNameDuplicate(tempName))
        {
          ++i;
          tempName = tempName + "_" + Integer.toString(i);
        }
        board1.setName(tempName);
        board2.setName(name1);
        board1.setName(name2);
      }

      if (((swapXandY == false) && (((flipY == false) && (y2 < y1)) || (flipY && (y1 < y2)))) ||
          ((swapXandY == true)  && (((flipY == false) && (x2 < x1)) || (flipY && (x1 < x2)))))
      {
        // board2's y coordinate is lower than board1, so we must adjust board1 coordinate and all board1 components
        PanelCoordinate coord1 = board1.getCoordinateInNanoMeters();
        PanelCoordinate coord2 = board2.getCoordinateInNanoMeters();
        board1.setCoordinateInNanoMeters(new PanelCoordinate(coord1.getX(), coord2.getY()));
      }

      if (alreadyProcessedBoardType == false)
      {
        // set the length of BoardType1 to the new combined length
        java.awt.geom.Rectangle2D unionRect = new java.awt.geom.Rectangle2D.Double();
        java.awt.geom.Rectangle2D.union(rect1, rect2, unionRect);
        int newLength = (int)unionRect.getHeight();

        if ((flipX == false) && (flipY == false))
        {
          // boards x,y is at lower left
          Integer prev = boardTypeToLengthMap.put(boardType1, newLength);
          Assert.expect(prev == null);
        }
        else if ((flipX == false) && (flipY))
        {
          // boards x,y is at upper left
          Integer prev = boardTypeToWidthMap.put(boardType1, newLength);
          Assert.expect(prev == null);
        }
        else if ((flipX) && (flipY == false))
        {
          // boards x,y is at lower right
          Integer prev = boardTypeToWidthMap.put(boardType1, newLength);
          Assert.expect(prev == null);
        }
        else if ((flipX) && (flipY))
        {
          // boards x,y is at upper right
          Integer prev = boardTypeToLengthMap.put(boardType1, newLength);
          Assert.expect(prev == null);
        }
        else
          Assert.expect(false);

        if (((swapXandY == false) && ((flipY == false) && (y2 < y1)) || (flipY && (y1 < y2))) ||
            ((swapXandY == true)  && ((flipY == false) && (x2 < x1)) || (flipY && (x1 < x2))))
        {
          for (ComponentType compType1 : boardType1.getComponentTypes())
          {
            // adjust the components coordinate to be relative to board1 instead of board2
            BoardCoordinate boardCoord1 = compType1.getCoordinateInNanoMeters();
            BoardCoordinate newBoardCoord1 = null;
            if (swapXandY == false)
              newBoardCoord1 = new BoardCoordinate(boardCoord1.getX(), boardCoord1.getY() - deltaY);
            else
              newBoardCoord1 = new BoardCoordinate(boardCoord1.getX() - deltaX, boardCoord1.getY());
            compType1.setCoordinateInNanoMeters(newBoardCoord1);
          }
        }

        if (((swapXandY == false) && ((flipY == false) && (y2 < y1)) || (flipY && (y1 < y2))))
          deltaY = 0;
        if (((swapXandY == true)  && ((flipY == false) && (x2 < x1)) || (flipY && (x1 < x2))))
          deltaX = 0;

        // To move Components over from board2 to board1 do the following:
        // ComponentType2 - change SideBoardType2 to SideBoardType1
        // Component2 - change SideBoard2 to SideBoard1
        // Fiducial - change SideBoard2 to SideBoard1
        // remove Board2 from Panel
        //
        // check for duplicate component names and fix if necessary
        // side 1 components

        // ComponentType2 - add to SideBoardType which will change SideBoardType2 to SideBoardType1
        for (ComponentType compType2 : boardType2.getComponentTypes())
        {
          // adjust the components coordinate to be relative to board1 instead of board2
          BoardCoordinate boardCoord2 = compType2.getCoordinateInNanoMeters();
          BoardCoordinate newBoardCoord1 = new BoardCoordinate(boardCoord2.getX() + deltaX, boardCoord2.getY() + deltaY);
          compType2.setCoordinateInNanoMeters(newBoardCoord1);

          // check for duplicate names and fix any found
          String refDes2 = compType2.getReferenceDesignator();
          if (boardType1.isComponentTypeReferenceDesignatorDuplicate(refDes2))
          {
            // we have a duplicate reference designator, rename it to something unique
            String uniqueName = refDes2 + "_recombine";
            int i = 0;
            while (boardType1.isComponentTypeReferenceDesignatorDuplicate(uniqueName))
            {
              ++i;
              uniqueName = uniqueName + "_" + Integer.toString(i);
            }
            compType2.setReferenceDesignator(uniqueName);
          }

          // now add ComponentType2 to BoardType1
          if (compType2.isOnSideBoard1())
            boardType1.getSideBoardType1().addComponentType(compType2);
          else
          {
            if (boardType1.sideBoardType2Exists() == false)
              boardType1.getBoards().get(0).createSecondSideBoard();
            boardType1.getSideBoardType2().addComponentType(compType2);
          }
        }
      }

      // Component2 - add to Board which will change SideBoard2 to SideBoard1
      for (Component comp2 : board2.getComponents())
      {
        if (comp2.isOnSideBoard1())
          board1.getSideBoard1().addComponent(comp2);
        else
          board1.getSideBoard2().addComponent(comp2);
      }

      // Fiducial - change SideBoard2 to SideBoard1
      if (board2.sideBoard1Exists())
      {
        for (Fiducial fiducial2 : board2.getSideBoard1().getFiducials())
        {
          if (alreadyProcessedBoardType == false)
          {
            // adjust the components coordinate to be relative to board1 instead of board2
            FiducialType fidType2 = fiducial2.getFiducialType();
            PanelCoordinate panelCoord = fidType2.getCoordinateInNanoMeters();
            PanelCoordinate newPanelCoord = new PanelCoordinate(panelCoord.getX() + deltaX, panelCoord.getY() + deltaY);
            fidType2.setCoordinateInNanoMeters(newPanelCoord);

            // check for duplicate names and fix any found
            String fidName2 = fidType2.getName();
            if (board1.getSideBoard1().getBoard().isFiducialNameDuplicate(fidName2))
            {
              // we have a duplicate reference designator, rename it to something unique
              String uniqueName = fidName2 + "_recombine";
              int i = 0;
              while (fiducial2.getSideBoard().getBoard().isFiducialNameDuplicate(uniqueName))
              {
                ++i;
                uniqueName = uniqueName + "_" + Integer.toString(i);
              }
              fidType2.setName(uniqueName);
            }

            board1.getSideBoard1().getSideBoardType().addFiducialType(fidType2);
          }
          fiducial2.setSideBoard(board1.getSideBoard1());
        }
      }
      if (board2.sideBoard2Exists())
      {
        for (Fiducial fiducial2 : board2.getSideBoard2().getFiducials())
        {
          // adjust the components coordinate to be relative to board1 instead of board2
          FiducialType fidType2 = fiducial2.getFiducialType();
          PanelCoordinate panelCoord = fidType2.getCoordinateInNanoMeters();
          PanelCoordinate newPanelCoord = new PanelCoordinate(panelCoord.getX() + deltaX, panelCoord.getY() + deltaY);
          fidType2.setCoordinateInNanoMeters(newPanelCoord);

          // check for duplicate names and fix any found
          String fidName2 = fidType2.getName();
          if (board1.getSideBoard1().getBoard().isFiducialNameDuplicate(fidName2))
          {
            // we have a duplicate reference designator, rename it to something unique
            String uniqueName = fidName2 + "_recombine";
            int i = 0;
            while (fiducial2.getSideBoard().getBoard().isFiducialNameDuplicate(uniqueName))
            {
              ++i;
              uniqueName = uniqueName + "_" + Integer.toString(i);
            }
            fidType2.setName(uniqueName);
          }
          fiducial2.setSideBoard(board1.getSideBoard2());

          if (alreadyProcessedBoardType == false)
            board1.getSideBoard2().getSideBoardType().addFiducialType(fiducial2.getFiducialType());
        }
      }

      // remove Board2 from Panel
      _project.getPanel().removeBoard(board2);
      if (alreadyProcessedBoardType == false)
        _project.getPanel().removeBoardType(board2.getBoardType());
    }

    // now set the new board length or width
    for (Map.Entry<BoardType, Integer> entry : boardTypeToWidthMap.entrySet())
    {
      BoardType boardType = entry.getKey();
      Integer width = entry.getValue();
      boardType.setWidthInNanoMeters(width);
    }

    for (Map.Entry<BoardType, Integer> entry : boardTypeToLengthMap.entrySet())
    {
      BoardType boardType = entry.getKey();
      Integer length = entry.getValue();
      boardType.setLengthInNanoMeters(length);
    }
  }

  /**
   * @author Bill Darbie
   */
  private void checkForDuplicateComponentNamesOnAllBoards() throws DatastoreException
  {
    for (Board board : _project.getPanel().getBoards())
    {
      if (board.sideBoard1Exists() && board.sideBoard2Exists())
      {
        SideBoard sideBoard1 = board.getSideBoard1();
        SideBoard sideBoard2 = board.getSideBoard2();
        for (Component component : sideBoard1.getComponents())
        {
          if (sideBoard2.hasComponent(component.getReferenceDesignator()))
          {
            throw new DuplicateComponentDatastoreException(component.getReferenceDesignator(),
                                                           FileName.getLegacyBoardNdfFullPath(_project.getName(), getLegacyName(sideBoard1.getSideBoardType())),
                                                           FileName.getLegacyBoardNdfFullPath(_project.getName(), getLegacyName(sideBoard2.getSideBoardType())));
          }
        }
      }
    }
  }

  /**
   * Called by BoardNdfReader and ComponentNdfReader
   * Add the component to a Map of all reference designators to Components
   *
   * @author Andy Mechtenberg
   */
  void addComponent(Component component)
  {
    Assert.expect(component != null);

    //convert to upper case
    String referenceDesignator = component.getReferenceDesignator();
    String key = referenceDesignator.toUpperCase();

    Object previousKey = _referenceDesignatorToComponentMap.put(key, component);
    Assert.expect(previousKey == null);
  }

  /**
   * Called by ComponentNdfReader and BoardNdfReader
   *
   * @author Bill Darbie
   */
  boolean doesComponentExist(String referenceDesignator)
  {
    Assert.expect(referenceDesignator != null);

    //convert to upper case
    String key = referenceDesignator.toUpperCase();
    return _referenceDesignatorToComponentMap.containsKey(key);
  }

  /**
   * Called by BoardNdfReader
   *
   * Returns the component mapped to the component reference designator.
   *
   * @param referenceDesignator The component reference designator to find an associated land pattern id for
   * @return the land pattern id mapped to the reference designator. If a land pattern id cannot be found, null is returned.
   * @author Keith Lee
   */
  Component getComponent(String referenceDesignator)
  {
    Assert.expect(referenceDesignator != null);

    //convert to upper case
    String key = referenceDesignator.toUpperCase();
    Component component = (Component)_referenceDesignatorToComponentMap.get(key);

    Assert.expect(component != null);
    return component;
  }

  /**
   * Called by BoardNdfReader
   * @return a Collection of Component objects for all boards in the panel
   * @author Keith Lee
   */
  Collection<Component> getComponents()
  {
    Assert.expect(_referenceDesignatorToComponentMap != null);

    return _referenceDesignatorToComponentMap.values();
  }

  /**
   * Called by ThroughHoleLandPatternNdfReader
   * Adds the top side land pattern
   *
   * @author Keith Lee
   */
  void addThroughHoleLandPattern(LandPattern throughHoleLandPattern)
  {
    Assert.expect(throughHoleLandPattern != null);

    String landPatternName = throughHoleLandPattern.getName();

    //convert to upper case
    String key = landPatternName.toUpperCase();

    Object previousKey = _landPatternNameToThroughHoleLandPatternMap.put(key, throughHoleLandPattern);
    Assert.expect(previousKey == null);
  }

  /**
   * @author Bill Darbie
   */
  void setNewThroughHoleLandPatternName(String oldName, String newName)
  {
    Assert.expect(oldName != null);
    Assert.expect(newName != null);

    //convert to upper case
    String oldKey = oldName.toUpperCase();
    String newKey = newName.toUpperCase();

    LandPattern prev = _landPatternNameToThroughHoleLandPatternMap.remove(oldKey);
    Assert.expect(prev != null);
    prev = _landPatternNameToThroughHoleLandPatternMap.put(newKey, prev);
  }

  /**
   * Called by ThroughHoleLandPatternNdfReader, BoardNdfReader, and MainReader
   * @return true if the top side land pattern name exists
   * @param landPatternName the name in question
   * @author George A. David
   */
  boolean doesThroughHoleLandPatternExist(String landPatternName)
  {
    Assert.expect(landPatternName != null);

    //convert to upper case
    String key = landPatternName.toUpperCase();

    return _landPatternNameToThroughHoleLandPatternMap.containsKey(key);
  }

  /**
   * Called by ThroughHoleLandPatternNdfReader, BoardNdfReader, and MainReader
   * Returns the actual land pattern of through hole pads mapped to the land pattern id.
   *
   * @param landPatternName The name of the top side land pattern to get
   * @return the top side land pattern of through hole pads mapped to the land pattern id. If a land pattern cannot be found, null is returned.
   * @author Keith Lee
   * @author George A. David
   */
  LandPattern getThroughHoleLandPattern(String landPatternName)
  {
    Assert.expect(landPatternName != null);

    //convert to upper case
    String key = landPatternName.toUpperCase();

    LandPattern landPattern = (LandPattern)_landPatternNameToThroughHoleLandPatternMap.get(key);

    Assert.expect(landPattern != null);

    return landPattern;
  }

  /**
   * Called by MainReader and ThroughHoleLandPatternNdfReader
   * @return a Collection of LandPattern objects for the top side through hole land patterns
   * @author Keith Lee
   */
  Collection<LandPattern> getThroughHoleLandPatterns()
  {
    Assert.expect(_landPatternNameToThroughHoleLandPatternMap != null);

    return _landPatternNameToThroughHoleLandPatternMap.values();
  }

  /**
   * Called by SurfaceMountLandPatternNdfReader
   * Maps the land pattern ID for components to the actual land pattern of surface mount pads.
   *
   * @param landPattern The land pattern of surface mount pads to map.
   * @author Keith Lee
   */
  void addSurfaceMountLandPattern(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);

    String landPatternName = landPattern.getName();
    //convert to upper case
    String key = landPatternName.toUpperCase();

    Object previousKey = _landPatternNameToSurfaceMountLandPatternMap.put(key, landPattern);
    Assert.expect(previousKey == null);
  }

  /**
   * @author Bill Darbie
   */
  void setNewSurfaceMountLandPatternName(String oldName, String newName)
  {
    Assert.expect(oldName != null);
    Assert.expect(newName != null);

    //convert to upper case
    String oldKey = oldName.toUpperCase();
    String newKey = newName.toUpperCase();

    LandPattern prev = _landPatternNameToSurfaceMountLandPatternMap.remove(oldKey);
    Assert.expect(prev != null);
    prev = _landPatternNameToSurfaceMountLandPatternMap.put(newKey, prev);
  }

  /**
   * @author Bill Darbie
   */
  void addInspectedComponent(Component component)
  {
    Assert.expect(component != null);
    boolean added = _inspectedComponentSet.add(component);
    Assert.expect(added);
  }

  boolean isComponentInspected(Component component)
  {
    Assert.expect(component != null);

    return _inspectedComponentSet.contains(component);
  }

  /**
   * Called by MainReader and SurfaceMountLandPatternNdfReader
   * Returns the actual land pattern of surface mount pads mapped to the land pattern id.
   *
   * @param landPatternName The name of the land pattern to get
   * @return the land pattern of surface mount pads mapped to the land pattern id.
   * @author Keith Lee
   */
  LandPattern getSurfaceMountLandPattern(String landPatternName)
  {
    Assert.expect(landPatternName != null);

    //convert to upper case
    String key = landPatternName.toUpperCase();

    LandPattern landPattern = (LandPattern)_landPatternNameToSurfaceMountLandPatternMap.get(key);
    Assert.expect(landPattern != null);
    return landPattern;
  }

  /**
   * Called by MainReader and SurfaceMountLandPatternNdfReader and BoardNdfReader
   * @return true if the surface mount land pattern exists
   * @param landPatternName the name of the surface mount land pattern
   * @author George A. David
   */
  boolean doesSurfaceMountLandPatternExist(String landPatternName)
  {
    Assert.expect(landPatternName != null);

    //convert to upper case
    String key = landPatternName.toUpperCase();

    return _landPatternNameToSurfaceMountLandPatternMap.containsKey(key);
  }

  /**
   * Called by MainReader and SurfaceMountLandPatternNdfReader
   * @return a Collection of LandPattern object (all surface mount)
   * @author Keith Lee
   */
  Collection<LandPattern> getSurfaceMountLandPatterns()
  {
    Assert.expect(_landPatternNameToSurfaceMountLandPatternMap != null);

    return _landPatternNameToSurfaceMountLandPatternMap.values();
  }

  /**
   * @author Bill Darbie
   */
  void addComponentToPadTypeEnumMap(Component component, PadTypeEnum padTypeEnum)
  {
    Assert.expect(component != null);
    Assert.expect(padTypeEnum != null);

    Object previousValue = _componentToPadTypeEnumMap.put(component, padTypeEnum);
    Assert.expect(previousValue == null);
  }

  /**
   * @author Bill Darbie
   */
  PadTypeEnum getPadTypeEnum(Component component)
  {
    Assert.expect(component != null);

    PadTypeEnum padTypeEnum = (PadTypeEnum)_componentToPadTypeEnumMap.get(component);
    Assert.expect(padTypeEnum != null);
    return padTypeEnum;
  }

  /**
   * For a particular ComponentPackage, add a mapping of all of its PackagePin names to the
   * PackagePin objects
   *
   * @author Keith Lee
   */
  void addCompPackageToPackagePinNameToPackagePinMapMap(CompPackage compPackage, Map<String, PackagePin> packagePinNameToPackagePinMap)
  {
    Assert.expect(compPackage != null);
    Assert.expect(packagePinNameToPackagePinMap != null);

    Object previousKey = _compPackageToPackagePinNameToPackagePinMapMap.put(compPackage, packagePinNameToPackagePinMap);
    Assert.expect(previousKey == null);
  }

  /**
   * @return a mapping of all of its PackagePin names to the PackagePin objects for the CompPackage passed in
   *
   * @author Keith Lee
   */
  Map<String, PackagePin> getPackagePinNameToPackagePinMap(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);

    Map<String, PackagePin> packagePinNameToPackagePinMap = _compPackageToPackagePinNameToPackagePinMapMap.get(compPackage);

    Assert.expect(packagePinNameToPackagePinMap != null);
    return packagePinNameToPackagePinMap;
  }

  /**
   * Adds land pattern pads to the land pattern.
   * This is necessary to ensure land pattern pads are added to LandPattern in the correct order.
   *
   * @param landPattern The land pattern to map
   * @param padNameToLandPatternPadMap The Map of landPatternPads
   * @author Keith Lee
   */
  void addLandPatternToPadNameToLandPatternPadMapMap(LandPattern landPattern, Map<String, LandPatternPad> padNameToLandPatternPadMap)
  {
    Assert.expect(landPattern != null);
    Assert.expect(padNameToLandPatternPadMap != null);

    Object previousKey = _landPatternToPadNameToLandPatternPadMapMap.put(landPattern, padNameToLandPatternPadMap);
    Assert.expect(previousKey == null);
  }

  /**
   * Returns the set of land pattern pads mapped to the land pattern.
   *
   * @param landPattern The land pattern.
   * @return the set of land pattern pads mapped to the land pattern. If a set of land pattern pads cannot be found, null is returned.
   * @author Keith Lee
   */
  Map<String, LandPatternPad> getPadNameToLandPatternPadMap(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);

    Map<String, LandPatternPad> padNameToLandPatternPadMap = _landPatternToPadNameToLandPatternPadMapMap.get(landPattern);
    Assert.expect(padNameToLandPatternPadMap != null);
    return padNameToLandPatternPadMap;
  }

  /**
   * Called by PanelNdfReader.
   * @author Keith Lee
   */
  void addSideBoardNameToSameSideBoardInstancesMap(String sideBoardName, List<SideBoard> sameSideBoardInstances)
  {
    Assert.expect(sideBoardName != null);
    Assert.expect(sameSideBoardInstances != null);

    //convert to upper case
    String sideBoardNameUppercase = sideBoardName.toUpperCase();

    Object previousKey = _sideBoardNameToSameSideBoardInstancesMap.put(sideBoardNameUppercase, sameSideBoardInstances);
    Assert.expect(previousKey == null);
  }

  /**
   * Called by PanelNdfReader and MainReader
   * @return a List of sideboard objects that correspond to the passed in name
   * @param sideBoardName a sideboard name
   * @author Keith Lee
   */
  List<SideBoard> getSameSideBoardInstances(String sideBoardName)
  {
    Assert.expect(sideBoardName != null);

    //convert to upper case
    String sideBoardNameUppercase = sideBoardName.toUpperCase();

    List<SideBoard> sameSideBoardInstances = _sideBoardNameToSameSideBoardInstancesMap.get(sideBoardNameUppercase);

    Assert.expect(sameSideBoardInstances != null);
    return sameSideBoardInstances;
  }

  /**
   * @return true if there exists a sideboard of the passed name
   * @param sideBoardName a sideboard name
   * @author Keith Lee
   */
  boolean doSameSideBoardInstancesExist(String sideBoardName)
  {
    Assert.expect(sideBoardName != null);

    //convert to upper case
    String sideBoardNameUppercase = sideBoardName.toUpperCase();

    return _sideBoardNameToSameSideBoardInstancesMap.containsKey(sideBoardNameUppercase);
  }


  /**
   * This is used when checking for duplicate land pattern names.
   * @return A List of SideBoard objects for all boards in the panel
   * @author Keith Lee
   */
  private List<SideBoard> getAllSideBoards()
  {
    List<SideBoard> allSideBoards = new ArrayList<SideBoard>();

    for (List<SideBoard> sideBoards : _sideBoardNameToSameSideBoardInstancesMap.values())
    {
      allSideBoards.addAll(sideBoards);
    }

    return allSideBoards;
  }

  /**
   * @return the current panel name being parsed
   * @author George A. David
   */
  String getPanelName()
  {
    Assert.expect(_project != null);
    return _project.getName();
  }

  /**
   * @author Bill Darbie
   */
  Project getProject()
  {
    Assert.expect(_project != null);

    return _project;
  }

  /**
   * @return true if panel.ndf specified a software revision for the panel program
   * that is greater than or equal to 80.
   * Otherwise, returns false.
   * @author Keith Lee
   */
  public boolean ndfsAreWellFormed()
  {
    if (_softwareRevision >= _MIN_WELL_FORMED_SOFTWARE_REVISION)
      return true;
    else
      return false;
  }

  /**
   * Sets the software revision for the panel program. If ndfs are well formed,
   * software revision must be set greater than or equal to 80.
   * @param softwareRevision the current software revision number
   * @author Keith Lee
   */
  void setSoftwareRevision(double softwareRevision)
  {
    Assert.expect(softwareRevision >= 0.0);

    _softwareRevision = softwareRevision;
  }

  /**
   * @return the double value of the software revision for the panel program.
   * @author Keith Lee
   */
  public double getSoftwareRevision()
  {
    Assert.expect(_softwareRevision >= 0.0);

    return _softwareRevision;
  }

  /**
   * @return the current side board being parsed
   * @author George A. David
   */
  String getCurrentSideBoardName()
  {
    Assert.expect(_currentSideBoardName != null);

    return _currentSideBoardName;
  }

  /**
   * This function is for use with UnitTests only!!
   * We check that Components specified as inspected in componen.ndf are also specified in board.ndf.
   * This List would be cleared out under normal circumstances, but when doing UnitTests we need a way
   * to manually clear out the List of Components.
   * @author Keith Lee
   */
  void clearComponents()
  {
    Assert.expect(UnitTest.unitTesting());
    _referenceDesignatorToComponentMap.clear();
  }

  /**
   * For UnitTesting ONLY!
   * @author Keith Lee
   */
  public void setProject(Project project)
  {
    Assert.expect(UnitTest.unitTesting());
    Assert.expect(project != null);

    _project = project;
  }

  /**
   * This function is to be used only for unit testing!!!
   * @param sideBoardName The name of the side board
   * @author George A. DAvid
   */
  void setCurrentSideBoardName(String sideBoardName)
  {
    Assert.expect(UnitTest.unitTesting());
    Assert.expect(sideBoardName != null);

    _currentSideBoardName = sideBoardName;
  }

  /**
   * @author Bill Darbie
   */
  void addBoardToLegacyRtfBoardNameMap(Board board, String legacyRtfBoardName)
  {
    Assert.expect(board != null);
    Assert.expect(legacyRtfBoardName != null);
    Object previous = _boardToLegacyRtfBoardNameMap.put(board, legacyRtfBoardName);
    Assert.expect(previous == null);
  }

  /**
   * @author Bill Darbie
   */
  private String getLegacyRtfBoardName(Board board)
  {
    Assert.expect(board != null);

    String legacyName = (String)_boardToLegacyRtfBoardNameMap.get(board);
    Assert.expect(legacyName != null);

    return legacyName;
  }

  /**
   * @author Bill Darbie
   */
  void addSideBoardTypeToWidthInNanoMetersMap(SideBoardType sideBoardType, int widthInNanoMeters)
  {
    Assert.expect(sideBoardType != null);

    Object previous = (Integer)_sideBoardTypeToWidthInNanoMetersMap.put(sideBoardType, new Integer(widthInNanoMeters));
    Assert.expect(previous == null);
  }

  /**
   * @author Bill Darbie
   */
  private int getWidthInNanoMeters(SideBoardType sideBoardType)
  {
    Assert.expect(sideBoardType != null);

    Integer integer = _sideBoardTypeToWidthInNanoMetersMap.get(sideBoardType);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @author Bill Darbie
   */
  void addSideBoardTypeToLengthInNanoMetersMap(SideBoardType sideBoardType, int lengthInNanoMeters)
  {
    Assert.expect(sideBoardType != null);

    Object previous = _sideBoardTypeToLengthInNanoMetersMap.put(sideBoardType, new Integer(lengthInNanoMeters));
    Assert.expect(previous == null);
  }

  /**
   * @author Bill Darbie
   */
  private int getLengthInNanoMeters(SideBoardType sideBoardType)
  {
    Assert.expect(sideBoardType != null);

    Integer integer = _sideBoardTypeToLengthInNanoMetersMap.get(sideBoardType);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @author Bill Darbie
   */
  void addSideBoardToDegreesRotationMap(SideBoard sideBoard, int degreesRotation)
  {
    Assert.expect(sideBoard != null);

    Object previous = (Integer)_sideBoardToDegreesRotationsMap.put(sideBoard, new Integer(degreesRotation));
    Assert.expect(previous == null);
  }

  /**
   * @author Bill Darbie
   */
  int getDegreesRotation(SideBoard sideBoard)
  {
    Assert.expect(sideBoard != null);

    Integer integer = (Integer)_sideBoardToDegreesRotationsMap.get(sideBoard);
    Assert.expect(integer != null);
    return integer.intValue();
  }

  /**
   * @author Bill Darbie
   */
  void addSideBoardToCoordinateInNanoMetersMap(SideBoard sideBoard, PanelCoordinate coordinate)
  {
    Assert.expect(sideBoard != null);
    Assert.expect(coordinate != null);

    Object previous = _sideBoardToCoordinateInNanoMetersMap.put(sideBoard, coordinate);
    Assert.expect(previous == null);
  }

  /**
   * @author Bill Darbie
   */
  private PanelCoordinate getCoordinateInNanoMeters(SideBoard sideBoard)
  {
    Assert.expect(sideBoard != null);

    PanelCoordinate coordinate = (PanelCoordinate)_sideBoardToCoordinateInNanoMetersMap.get(sideBoard);
    Assert.expect(coordinate != null);

    return coordinate;
  }

  /**
   * Called by PanelNdfReader
   * @author Bill Darbie
   */
  void addSideBoardTypeToLegacyNameMap(SideBoardType sideBoardType, String legacyName)
  {
    Assert.expect(sideBoardType != null);
    Assert.expect(legacyName != null);

    Object previous = _sideBoardTypeToLegacyNameMap.put(sideBoardType, legacyName);
    Assert.expect(previous == null);
  }

  /**
   * @author Bill Darbie
   */
  public String getLegacyName(SideBoardType sideBoardType)
  {
    Assert.expect(sideBoardType != null);

    String legacyName = (String)_sideBoardTypeToLegacyNameMap.get(sideBoardType);
    Assert.expect(legacyName != null);

    return legacyName;
  }

  /**
   * @author Bill Darbie
   */
  void addSideBoardTypeToLegacyBoardNdfUnits(SideBoardType sideBoardType, MathUtilEnum mathUtilEnum)
  {
    Assert.expect(sideBoardType != null);
    Assert.expect(mathUtilEnum != null);

    Object previous = _sideBoardTypeToLegacyBoardNdfUnits.put(sideBoardType, mathUtilEnum);
    Assert.expect(previous == null);
  }

  /**
   * @author Bill Darbie
   */
  private MathUtilEnum getLegacyBoardNdfUnits(SideBoardType sideBoardType)
  {
    Assert.expect(sideBoardType != null);

    MathUtilEnum mathUtilEnum = (MathUtilEnum)_sideBoardTypeToLegacyBoardNdfUnits.get(sideBoardType);
    Assert.expect(mathUtilEnum != null);

    return mathUtilEnum;
  }

  /**
   * @author Bill Darbie
   */
  void setSplitBoardTypeSet(Set<BoardType> possibleSplitBoardTypeSet)
  {
    Assert.expect(possibleSplitBoardTypeSet != null);
    _possibleSplitBoardTypeSet = new HashSet<BoardType>(possibleSplitBoardTypeSet);
  }
}
