package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class is thrown when the number of pads do not equals the number of pins.
 * @author George A. David
 */
public class NumberOfPinsAndPadsNotEqualDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public NumberOfPinsAndPadsNotEqualDatastoreException(String panelFileName,
                                                       String componentFileName,
                                                       String boardFileName,
                                                       String landPatternFileName,
                                                       String landPatternName,
                                                       int numberOfPads,
                                                       String packageName,
                                                       int numberOfPins,
                                                       String referenceDesignator)
  {
    super(new LocalizedString("DS_NDF_ERROR_NUMBER_OF_PINS_AND_PADS_NOT_EQUAL_COMPONENT_NOT_KNOWN_KEY",
                              new Object[]{panelFileName,
                                           componentFileName,
                                           boardFileName,
                                           landPatternFileName,
                                           landPatternName,
                                           new Integer(numberOfPads),
                                           packageName,
                                           new Integer(numberOfPins),
                                           referenceDesignator}));
    Assert.expect(panelFileName != null);
    Assert.expect(landPatternFileName != null);
    Assert.expect(landPatternName != null);
    Assert.expect(numberOfPads > 0);
    Assert.expect(packageName != null);
    Assert.expect(numberOfPins > 0);
    Assert.expect(referenceDesignator != null);
  }
}
