package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when some boards specify a sensor positions, but not all.  It should be all or none.
 * @author Bill Darbie
 */
public class ExpectedAllBoardsToSpecifySensorPositionsDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public ExpectedAllBoardsToSpecifySensorPositionsDatastoreException(String fileName)
  {
    super(new LocalizedString("DS_NDF_ERROR_EXPECTED_ALL_BOARDS_TO_SPECIFY_A_SENSOR_POSITION_KEY", new Object[]{fileName}));
    Assert.expect(fileName != null);
  }
}
