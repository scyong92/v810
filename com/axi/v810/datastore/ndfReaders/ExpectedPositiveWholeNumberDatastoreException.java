package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when we find something besides a positive whole number.
 * @author George A. David
 */
public class ExpectedPositiveWholeNumberDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public ExpectedPositiveWholeNumberDatastoreException(String fileName,
                                                  int lineNumber,
                                                  String expectedSyntax,
                                                  String invalidField,
                                                  String actualValue)
  {
    super(new LocalizedString("DS_NDF_ERROR_EXPECTED_A_POSITIVE_WHOLE_NUMBER_KEY",
          new Object[]{fileName,
                       new Integer(lineNumber),
                       expectedSyntax,
                       invalidField,
                       actualValue}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidField != null);
    Assert.expect(actualValue != null);
  }

}
