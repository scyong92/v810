package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when we find something besides left or right for sensor position in panel.ndf
 * @author Andy Mechtenberg
 */
public class InvalidSensorPositionDatastoreException extends DatastoreException
{
  /**
   * @author Andy Mechtenberg
   */
  public InvalidSensorPositionDatastoreException(String fileName,
                                                 int lineNumber,
                                                 String expectedSyntax,
                                                 String invalidItem,
                                                 String actualValue)
  {
    super(new LocalizedString("DS_NDF_ERROR_INVALID_SENSOR_POSITION_KEY",
          new Object[]{fileName,
                       new Integer(lineNumber),
                       expectedSyntax,
                       invalidItem,
                       actualValue}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidItem != null);
    Assert.expect(actualValue != null);
  }
}
