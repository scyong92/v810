package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;
/**
 * This class gets thrown when the dimensions of the board do not match after rotation.
 * @author George A. David
 */
public class DifferentBoardDimensionsDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public DifferentBoardDimensionsDatastoreException(Double topWidth,
                                                    Double topLength,
                                                    String topBoardUnits,
                                                    String topBoardFileName,
                                                    Double bottomWidth,
                                                    Double bottomLength,
                                                    String bottomBoardUnits,
                                                    String bottomBoardFileName)

  {
    super(new LocalizedString("DS_NDF_ERROR_DIFFERENT_BOARD_DIMENSIONS_KEY",
          new Object[]{topWidth,
                       topLength,
                       topBoardUnits,
                       topBoardFileName,
                       bottomWidth,
                       bottomLength,
                       bottomBoardUnits,
                       bottomBoardFileName}));

    Assert.expect(topWidth != null);
    Assert.expect(topLength != null);
    Assert.expect(topBoardUnits != null);
    Assert.expect(topBoardFileName != null);
    Assert.expect(bottomWidth != null);
    Assert.expect(bottomLength != null);
    Assert.expect(bottomBoardUnits != null);
    Assert.expect(bottomBoardFileName != null);
  }
}
