package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * If the snap margin setting is invalid then this exception should be thrown.
 * @author Bill Darbie
 */
public class InvalidSnapMarginDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public InvalidSnapMarginDatastoreException(String fileName,
                                             int lineNumber,
                                             String badSnapMargin)

  {
    super(new LocalizedString("DS_ERROR_INVALID_SNAP_MARGIN_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           badSnapMargin}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(badSnapMargin != null);
  }
}
