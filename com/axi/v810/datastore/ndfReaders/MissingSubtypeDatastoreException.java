package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * If the subtype is not specified this exception should be thrown.
 * @author Bill Darbie
 */
public class MissingSubtypeDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public MissingSubtypeDatastoreException(String fileName,
                                          int lineNumber,
                                          String referenceDesignator,
                                          int defaultSubTypeNumber)

  {
    super(new LocalizedString("DS_ERROR_MISSING_SUBTYPE_KEY",
                              new Object[]{fileName,
                                           new Integer(lineNumber),
                                           referenceDesignator,
                                           new Integer(defaultSubTypeNumber)}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(referenceDesignator != null);
    Assert.expect(defaultSubTypeNumber >= 0);
  }
}
