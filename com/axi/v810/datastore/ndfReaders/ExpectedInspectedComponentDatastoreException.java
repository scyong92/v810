package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * This class gets thrown when we find something besides
 * an inspected component where an inspected component is required.
 * @author George A. David
 */
public class ExpectedInspectedComponentDatastoreException extends DatastoreException
{
  /**
   * @author George A. David
   */
  public ExpectedInspectedComponentDatastoreException(String fileName,
                                                      int lineNumber,
                                                      String expectedSyntax,
                                                      String invalidField,
                                                      String actualValue)
  {
    super(new LocalizedString("DS_NDF_ERROR_EXPECTED_INSPECTED_COMPONENT_KEY",
          new Object[]{fileName,
                       new Integer(lineNumber),
                       expectedSyntax,
                       invalidField,
                       actualValue}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidField != null);
    Assert.expect(actualValue != null);
  }

}
