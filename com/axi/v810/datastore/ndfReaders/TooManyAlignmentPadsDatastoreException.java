package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

 /**
  * This exception will be thrown from the BoardNDFReader when there are found to be too many
  * pins in one alignment group
  * @author Kristin Casterton
  */
public class TooManyAlignmentPadsDatastoreException extends DatastoreException
{
  public TooManyAlignmentPadsDatastoreException(String fileName, int lineNumber, int maxAlignmentPads)
  {
    super(new LocalizedString("DS_NDF_ERROR_TOO_MANY_ALIGNMENT_PADS_KEY",
                              new Object[]{fileName, new Integer(lineNumber), new Integer(maxAlignmentPads)}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(maxAlignmentPads > 0);
  }
}
