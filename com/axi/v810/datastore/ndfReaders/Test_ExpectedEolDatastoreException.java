package com.axi.v810.datastore.ndfReaders;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

public class Test_ExpectedEolDatastoreException extends UnitTest
{
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ExpectedEolDatastoreException());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    ExpectedEolDatastoreException ex = new ExpectedEolDatastoreException("fileName", 100, "actual");
    String message = ex.getLocalizedMessage();
    os.println(message);
  }
}
