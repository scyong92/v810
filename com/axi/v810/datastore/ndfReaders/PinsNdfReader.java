package com.axi.v810.datastore.ndfReaders;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.util.*;

/**
 * Parses the pins.ndf file.
 *
 * This class creates the following Datastore objects:
 *   none
 *
 * This class sets the following variables:
 *   LandPatterPad
 *     name
 *
 * This class requires the following to already be available to it from MainReader:
 *   none
 *
 * This class puts the following into MainReader:
 *   none
 *
 * @author Keith Lee
 */
class PinsNdfReader extends NdfReader
{
  private static final String _EXPECTED_SYNTAX = StringLocalizer.keyToString("DS_SYNTAX_NDF_PINS_EXPECTED_SYNTAX_KEY");
  private static final String _PIN_NUMBER_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PIN_NUMBER_KEY");
  private static final String _PIN_NAME_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_PIN_NAME_KEY");
  private static final String _REFERENCE_DESIGNATOR_FIELD = StringLocalizer.keyToString("DS_SYNTAX_NDF_REFERENCE_DESIGNATOR_KEY");

  private static final boolean _DONT_RETURN_DELIMITERS = false;
  private static final String _NAME_DELIMITERS = "\t\n\r\f";

  private static final String _EXPECTED_ATTRIBUTES = CHECKSUM;
  /** @todo wpdfix - exception parameters cannot be english phrases */
  private static final String _EXPECTED_PIN_NAME = "a pin name";
  /** @todo wpdfix - exception parameters cannot be english phrases */
  private static final String _EXPECTED_REFERENCE_DESIGNATOR = "a reference designator";
  /** @todo wpdfix - exception parameters cannot be english phrases */
  private static final String _EXPECTED_VALID_PIN_NUMBER = "a valid pin number";
  /** @todo wpdfix - exception parameters cannot be english phrases */
  private static final String _EXPECTED_VALID_REFERENCE_DESIGNATOR = "a valid component name";

  private List<SideBoard> _sameSideBoardInstances;
  private Set<LandPatternPad> _landPatternPadSet = new HashSet<LandPatternPad>();
  private Map<Component, Map<String, String>> _compToOrigPadNameToTempPadNameMap = new HashMap<Component, Map<String, String>>();

  //_checkSumFound inherited from NdfReader

  /**
   * Constructor for the PinsNdfReader.
   *
   * @param mainReader - the MainReader associated with the panel program.
   * @author Keith Lee
   */
  PinsNdfReader(MainReader mainReader)
  {
    super(mainReader);
  }

  /**
   * @author Keith Lee
   */
  protected void initializeBeforeParsing()
  {
    _checkSumFound = false;
    _landPatternPadSet.clear();
    _compToOrigPadNameToTempPadNameMap.clear();
  }

  /**
   * @author Keith Lee
   */
  protected void resetAfterParsing()
  {
    initializeBeforeParsing();
  }

  /**
   * @author Keith Lee
   */
  protected void postProcessData()
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);

    // do nothing
  }

  /**
   * parses a data line in a pins.ndf file that follows the following format
   * "@<ref designator <pin #> <pin name> <node name>"
   *
   * @param line the line of alphanumeric pin data in the pins.ndf to parse.
   * @author Keith Lee
   */
  protected void parseDataLine(String line) throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);
    Assert.expect(line != null);

    String referenceDesignator = null;
    int padNumber = 0;
    String padName = null;

    StringTokenizer st = new StringTokenizer(line, getDelimiters(), _DONT_RETURN_DELIMITERS);
    String token = ParseUtil.getNextToken(st);
    referenceDesignator = parseDataLineReferenceDesignator(token);

    token = ParseUtil.getNextToken(st);
    padNumber = parseDataLinePinNumber(token);
    padName = parseDataLinePinName(st);

    // check that component name is valid
    for (SideBoard sideBoard : _sameSideBoardInstances)
    {
      if (sideBoard.hasComponent(referenceDesignator) == false)
      {
        Object[] args = new Object[]{_currentFile,
                                     new Integer(_is.getLineNumber()),
                                     _EXPECTED_SYNTAX,
                                     _REFERENCE_DESIGNATOR_FIELD,
                                     referenceDesignator,
                                     _currentFile};
        _warnings.add(new LocalizedString("DS_NDF_WARNING_CORRUPT_COMPONENT_REFERENCE_KEY", args));
        return;
      }

      Component component = sideBoard.getComponent(referenceDesignator);
      LandPattern landPattern = component.getLandPattern();

      // get the Pad using the old padNumber
      String legacyPadNumber = (new Integer(padNumber)).toString();
      Pad pad = null;

      // maybe the pad name has already been changed
      // first check to see if we are using a temporary name, if so use that
      Map<String, String> origNameToTempNameMap = _compToOrigPadNameToTempPadNameMap.get(component);
      if (origNameToTempNameMap != null)
      {
        String tempName = origNameToTempNameMap.get(legacyPadNumber);
        if (tempName != null)
          legacyPadNumber = tempName;
      }

      if (component.hasPad(legacyPadNumber))
      {
        pad = component.getPad(legacyPadNumber);
      }
      else
      {
        // maybe the pad name has already been changed
        if (component.hasPad(padName))
        {
          pad = component.getPad(padName);
        }
        else
        {
          String throughHoleLandPatternNdfFileName = FileName.getLegacyThroughHoleLandPatternNdfFullPath(_mainReader.getPanelName());
          String surfaceMountLandPatternNdfFileName = FileName.getLegacySurfaceMountLandPatternNdfFullPath(_mainReader.getPanelName());
          String landPatternName = component.getLandPattern().getName();
          throw new InvalidPadNumberDatastoreException(_currentFile,
                                                       _is.getLineNumber(),
                                                       legacyPadNumber,
                                                       throughHoleLandPatternNdfFileName,
                                                       surfaceMountLandPatternNdfFileName,
                                                       landPatternName);
        }
      }

      LandPatternPad landPatternPad = pad.getLandPatternPad();
      boolean padAddedToSet = _landPatternPadSet.add(landPatternPad);
      if (padAddedToSet)
      {
        if ((pad.getName().equals(padName) == false) && (landPattern.isLandPatternPadNameDuplicateDuringPinsNdfParsing(padName)))
        {
          String origName = padName;
          // find a unique tempName
          int i = 1;
          String tempName = origName + "_duplicate" + Integer.toString(i);
          while (landPattern.isLandPatternPadNameDuplicateDuringPinsNdfParsing(tempName))
          {
            ++i;
            tempName = origName + "_duplicate" + Integer.toString(i);
          }

          origNameToTempNameMap = _compToOrigPadNameToTempPadNameMap.get(component);
          if (origNameToTempNameMap == null)
          {
            origNameToTempNameMap = new HashMap<String, String>();
            _compToOrigPadNameToTempPadNameMap.put(component, origNameToTempNameMap);
          }
          origNameToTempNameMap.put(origName, tempName);

          // now replace the original name with the temporary one
          LandPatternPad origLandPatternPad = landPattern.getLandPatternPad(origName);
          origLandPatternPad.setName(tempName);
        }

        // set the pad to the new name
        if ((padName.equals("") == false) && (pad.getName().equals(padName) == false))
          landPatternPad.setName(padName);
      }
    }
  }

  /**
   * @author Keith Lee
   */
  private String parseDataLineReferenceDesignator(String token) throws UnexpectedEolDatastoreException
  {
    if (token == null)
    {
      String expected = _EXPECTED_REFERENCE_DESIGNATOR;
      throw new UnexpectedEolDatastoreException(_currentFile, _is.getLineNumber(), expected);
    }

    String retVal = null;

    // reference designator
    retVal = token;

    return retVal;
  }

  /**
   * @author Keith Lee
   */
  private int parseDataLinePinNumber(String token) throws UnexpectedEolDatastoreException, FileCorruptDatastoreException
  {
    if (token == null)
    {
      String expected = EXPECTED_POSITIVE_NON_ZERO_NUMBER;
      throw new UnexpectedEolDatastoreException(_currentFile, _is.getLineNumber(), expected);
    }

    int retVal = 0;

    // component pin number
    Exception ex = null;
    try
    {
      retVal = StringUtil.convertStringToPositiveInt(token);
      if (retVal == 0)
      {
        String expected = EXPECTED_POSITIVE_NON_ZERO_NUMBER;
        String actual = token;
        throw new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), actual, expected);
      }
    }
    catch(BadFormatException e)
    {
      ex = e;
    }
    catch (ValueOutOfRangeException vex)
    {
      ex = vex;
    }
    if (ex != null)
    {
      String expected = EXPECTED_POSITIVE_NON_ZERO_NUMBER;
      String actual = token;

      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), expected, actual);
      dex.initCause(ex);
      throw dex;
    }

    return retVal;
  }

  /**
   * @author Keith Lee
   */
  private String parseDataLinePinName(StringTokenizer st) throws UnexpectedEolDatastoreException, ExpectedEolDatastoreException, FileCorruptDatastoreException
  {
    Assert.expect(st != null);

    String retVal = null;

    String temp = ParseUtil.getNextToken(st, _NAME_DELIMITERS);
    if (temp == null)
    {
      String expected = _EXPECTED_PIN_NAME;
      throw new UnexpectedEolDatastoreException(_currentFile, _is.getLineNumber(), expected);
    }

    temp = temp.trim();
    if (temp.length() == 0)
    {
      String expected = _EXPECTED_PIN_NAME;
      throw new UnexpectedEolDatastoreException(_currentFile, _is.getLineNumber(), expected);
    }
    else if (temp.startsWith(String.valueOf('\"')) == false)
    {
      String expected = _EXPECTED_PIN_NAME;
      String actual = temp;
      throw new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), expected, actual);
    }
    else if (temp.endsWith(String.valueOf('\"')) == false)
    {
      String expected = _EXPECTED_PIN_NAME;
      String actual = temp;
      throw new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), expected, actual);
    }

    QuoteTokenizer qt = new QuoteTokenizer(temp);
    retVal = qt.nextToken();
    if (retVal == null)
    {
      String expected = _EXPECTED_PIN_NAME;
      String actual = temp;
      throw new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), expected, actual);
    }

    String token = qt.nextToken();
    if (token != null)
    {
      String actual = token;
      throw new ExpectedEolDatastoreException(_currentFile, _is.getLineNumber(), actual);
    }

    return retVal;
  }

  /**
   * parses an attribute line in a pins.ndf file.
   * @author Keith Lee
   */
  protected void parseAttributeLine(String line) throws DatastoreException
  {
    Assert.expect(_currentFile != null);
    Assert.expect(_is != null);
    Assert.expect(line != null);

    String tempLine = null;
    tempLine = line.toUpperCase();
    if (tempLine.startsWith(CHECKSUM))
    {
      if (_checkSumFound == false)
      {
        line = line.substring(CHECKSUM.length());
        parseAttributeLineCheckSum(line);
      }
      else
      {
        throw new DuplicateTokenDatastoreException(_currentFile, _is.getLineNumber(), CHECKSUM);
      }
    }
    else
    {
      // invalid or unknown attribute line
      String expected = _EXPECTED_ATTRIBUTES;
      String actual = line;
      throw new FileCorruptDatastoreException(_currentFile, _is.getLineNumber(), expected, actual);
    }
  }

  /**
   * @author Keith Lee
   */
  protected void parseLinesRequiredByOtherLines()
  {
    // do nothing. only data lines should be in this file.
  }

  /**
   * @author Bill Darbie
   */
  void setSameSideBoardInstances(List<SideBoard> sameSideBoardInstances)
  {
    Assert.expect(sameSideBoardInstances != null);

    _sameSideBoardInstances = sameSideBoardInstances;
    Assert.expect(_sameSideBoardInstances.isEmpty() == false);
  }
}
