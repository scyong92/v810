package com.axi.v810.datastore.ndfReaders;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * @author Andy Mechtenberg
 */
class InvalidTestStatusDatastoreException extends DatastoreException
{
  /**
   * @param fileName is the name of the file that is corrupt.
   * @param lineNumber is the line number in the file where the eol occurred.
   * @param expectedSyntax is the String representing what was expected instead of the eol.
   * @param invalidFieldName is the invalid field
   * @param invalidFieldData is the invalid field data
   * @author Bill Darbie
   */
  InvalidTestStatusDatastoreException(String fileName,
                                      int lineNumber,
                                      String expectedSyntax,
                                      String invalidFieldName,
                                      String invalidFieldData)
  {
    super(new LocalizedString("DS_NDF_ERROR_INVALID_TEST_STATUS_KEY", new Object[]{fileName,
                                                                      new Integer(lineNumber),
                                                                      expectedSyntax,
                                                                      invalidFieldName,
                                                                      invalidFieldData}));
    Assert.expect(fileName != null);
    Assert.expect(lineNumber > 0);
    Assert.expect(expectedSyntax != null);
    Assert.expect(invalidFieldName != null);
    Assert.expect(invalidFieldData != null);
  }
}
