package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when a file is not found.
 * @author Bill Darbie
 */
public class CannotCopyFileDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param fromFile is the name of the file being copied
   * @param toFile is the name of the file to being copied to
   * @author Bill Darbie
   */
  public CannotCopyFileDatastoreException(String fromFile, String toFile)
  {
    super(new LocalizedString("DS_ERROR_CANNOT_COPY_KEY", new Object[]{fromFile, toFile}));
    Assert.expect(fromFile != null);
    Assert.expect(toFile != null);
  }
}
