package com.axi.v810.datastore;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

public class Test_DatastoreException extends UnitTest
{
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_DatastoreException());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    DatastoreException ex = new DatastoreException("fileName");
    String message = ex.getLocalizedMessage();
    os.println(message);
  }
}
