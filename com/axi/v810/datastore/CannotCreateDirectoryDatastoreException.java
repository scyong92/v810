package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class CannotCreateDirectoryDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public CannotCreateDirectoryDatastoreException(String dirName)
  {
    super(new LocalizedString("DS_ERROR_CANNOT_CREATE_DIRECTORY_KEY", new Object[]{dirName}));
    Assert.expect(dirName != null);
  }
}
