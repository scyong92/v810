package com.axi.v810.datastore;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.util.*;

public class Test_FileNotFoundDatastoreException extends UnitTest
{
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_FileNotFoundDatastoreException());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    FileNotFoundDatastoreException ex = new FileNotFoundDatastoreException("fileName");
    String message = ex.getLocalizedMessage();
    os.println(message);
  }
}
