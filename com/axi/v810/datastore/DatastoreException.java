package com.axi.v810.datastore;

import java.io.*;

import com.axi.v810.util.*;
import com.axi.util.*;

/**
 * This is the base class for all datastore layer IO Exceptions
 * @author Bill Darbie
 */
public class DatastoreException extends XrayTesterException
{
  /**
   * Create this exception with a localized string.
   * @author Bill Darbie
   */
  public DatastoreException(LocalizedString localizedString)
  {
    super(localizedString);
  }

  /**
   * Create this exception with a fileName of where the general IO exception happened.
   * Note that you should usually use a more specific exception than this if you can
   * @param fileName is the file that was being accessed when an IOException happened.
   * @author Bill Darbie
   */
  public DatastoreException(String fileName)
  {
    super(new LocalizedString("DS_ERROR_IOEXCEPTION_KEY", new Object[]{fileName}));
    Assert.expect(fileName != null);
  }
}
