package com.axi.v810.datastore;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.util.*;

/**
 * Tests the ImageSetInfoWriter class

 * @author George A. David
 */
public class Test_ImageSetInfoWriter extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] arguments)
  {
    UnitTest.execute(new Test_ImageSetInfoWriter());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      ImageSetInfoWriter writer = new ImageSetInfoWriter();
      try
      {
        writer.write(null);
        Expect.expect(false);
      }
      catch (AssertException exc)
      {
        //do nothing
      }

      ImageSetData imageSetData = new ImageSetData();
      String imageSetDirectory = getTestDataDir();
      String imageSetName = "Test_ImageSetInfoReader";
      String projectName = "projectName";
      int versionNumber = 4;
      imageSetData.setProjectName(projectName);
      imageSetData.setTestProgramVersionNumber(versionNumber);
      imageSetData.setImageSetName(imageSetName);
      imageSetData.setBoardPopulated(true);
      imageSetData.setUserDescription("imageSetDescription");
      imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.PANEL);
      imageSetData.setSystemDescription("imageSetDescription");
      imageSetData.setNumberOfImagesSaved(100);
      imageSetData.setDateInMils(1);
      imageSetData.setMachineSerialNumber("1");

      writer.write(imageSetData);
      ImageSetInfoReader reader = new ImageSetInfoReader();
      String imageSetInfoPath = FileName.getImageSetInfoFullPath(projectName, imageSetName);
      reader.parseFile(imageSetInfoPath);
      imageSetData = reader.getImageSetData();
      Expect.expect(imageSetData.getProjectName().equals(projectName));
      Expect.expect(imageSetData.getTestProgramVersionNumber() == versionNumber);

      FileUtilAxi.delete(imageSetInfoPath);
    }
    catch (DatastoreException ex)
    {
      ex.printStackTrace();
    }
  }
}
