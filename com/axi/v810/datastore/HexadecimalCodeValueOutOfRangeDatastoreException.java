package com.axi.v810.datastore;

import java.lang.*;

import com.axi.util.*;

/**
 * If a hexadecimal code is outside the allowed range (for a character), then throw this exception.
 *
 * @author Bob Balliew
 */
public class HexadecimalCodeValueOutOfRangeDatastoreException extends DatastoreException
{
  /**
   * This is for hexadecimal parsing errors.
   *
   * @param token is what had a invalid range.
   * @author Bob Balliew
   */
  public HexadecimalCodeValueOutOfRangeDatastoreException(String token)
  {
    super(new LocalizedString("DS_HEXADECIMAL_CODE_VALUE_OUT_OF_RANGE_EXCEPTION_KEY",
                              new Object[]
                              {
                                token,
                                Integer.toHexString(Character.MIN_VALUE),
                                Integer.toHexString(Character.MAX_VALUE)
                              }));

    Assert.expect(token != null);
  }
}
