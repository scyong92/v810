package com.axi.v810.datastore;

import java.io.*;
import java.text.*;
import java.util.*;

import javax.imageio.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.images.*;
import com.axi.v810.util.*;


/**
 * This class is where all the file names will be stored. This is to avoid
 * having constant file names listed in mutliple places in the code.
 * Note that a naming convention is followed to make it clear what is being
 * returned.  Any method that ends in FullPath() returns the full path including
 * the file name.  Any method that ends in File() returns the file name only.
 *
 * @author Bill Darbie
 * @author George A. David
 */
public class FileName
{
  // cad files
  private static final String _CAD_EXTENSION = ".cad";
  private static final String _PROJECT_PANEL_FILE = "panel";
  private static final String _PROJECT_BOARD_TYPE_FILE_FOR_VERSION_1 = "boardType_boardType";
  private static final String _PROJECT_BOARD_TYPE_FILE = "boardType_";
  private static final String _PROJECT_LAND_PATTERN_FILE = "landPattern";
  private static final String _PROJECT_COMP_PACKAGE_FILE = "package";
  //Khaw Chek Hau - XCR3554: Package on package (PoP) development
  private static final String _PROJECT_PACKAGE_ON_PACKAGE_FILE = "packageOnPackage";

  // settings files
  private static final String _SETTINGS_EXTENSION = ".settings";
  private static final String _PROJECT_PROJECT_SETTINGS_FILE = "project";
  private static final String _CAD_CREATOR_PROJECT_SETTINGS_FILE = "cadcreator";
  private static final String _PROJECT_PANEL_SETTINGS_FILE = "panel";
  private static final String _PROJECT_PSP_SETTINGS_FILE = "psp";
  private static final String _PROJECT_PANEL_SURFACE_MAP_SETTINGS_FILE = "panel_surface_map";
  private static final String _PROJECT_PANEL_MESH_SETTINGS_FILE = "panel_mesh";
  private static final String _PROJECT_PANEL_ALIGNMENT_SURFACE_MAP_SETTINGS_FILE = "panel_alignment_surface_map";
  private static final String _PROJECT_BOARD_SETTINGS_FILE = "board_";
  private static final String _PROJECT_BOARD_SURFACE_MAP_SETTINGS_FILE = "board_surface_map_";
  private static final String _PROJECT_BOARD_ALIGNMENT_SURFACE_MAP_SETTINGS_FILE = "board_alignment_surface_map_";
  private static final String _PROJECT_BOARD_MESH_SETTINGS_FILE = "board_mesh_";
  private static final String _PROJECT_BOARD_TYPE_SETTINGS_FILE_FOR_VERSION_1 = "boardType_boardType";
  private static final String _PROJECT_BOARD_TYPE_SETTINGS_FILE = "boardType_";
  private static final String _PROJECT_SUBTYPE_SETTINGS_FILE = "subtype";
  private static final String _PROJECT_COMPONENT_NOLOAD_SETTINGS_FILE = "componentNoLoad";
  private static final String _PROJECT_DATABASE_VERSION_SETTINGS_FILE = "projectDatabaseVersion";
  private static final String _PROJECT_ALIGNMENT_TRANSFORM_SETTINGS_FILE = "alignmentTransform";
  private static final String _PROJECT_PACKAGE_SETTINGS_FILE = "package";
  private static final String _PROJECT_SURFACE_MAP_SETTINGS_FILE = "surfaceMap";
  private static final String _PROJECT_PANEL_FOCUS_REGION_SETTINGS_FILE = "panel_focus_region";
  private static final String _PROJECT_PSH_SURFACE_MODEL_SETTINGS_FILE = "surfaceModel"; //Siew Yeng - XCR-3781
  private static final String _PROJECT_DEFECTIVE_JOINT_SETTING_FILE = "measurement_joint_highlight";
  
  //sham
  private static final String _PROJECT_SUBTYPE_ADVANCE_SETTINGS_FILE = "subtypeAdvance";
  private static final String _PROJECT_AUTO_EXPOSURE_SETTINGS_FILE = "autoExposure";
  // summary file
  private static final String _SUMMARY_EXTENSION = ".summary";
  private static final String _PROJECT_SUMMARY_FILE = "project";

  //XCR1725 - Serial number mapping
  private static final String _PROJECT_SERIAL_NUMBER_MAPPING_SETTINGS_FILE = "serialNumberMapping";
  // panel photo and panel CAD graphics
  private static final String _PANEL_PHOTO_IMAGE_FILE = "photo.png";
  private static final String _PANEL_IMAGE_FILE = "panel.png";
  private static final String _PANEL_FLIPPED_IMAGE_FILE = "panelFlipped.png";
  private static final String _BOARD_IMAGE_FILE_EXT = ".png";
  private static final String _BOARD_IMAGE_FILE = "panel_";
  private static final String _BOARD_FLIPPED_IMAGE_FILE = "panelFlipped_";
  
  // program file
  private static final String _TEST_PROGRAM_FILE_EXTENSION = ".program";
  private static final String _INSPECTION_IMAGE_INFO_PROGRAM_FILE = "inspectionImageInfo.1.program";
  private static final String _VERIFICATION_IMAGE_NAMES_PROGRAM_FILE = "verificationImageNames.1.program";
  private static final String _ADJUST_FOCUS_IMAGE_NAMES_PROGRAM_FILE = "adjustFocusImageNames.1.program";

  private static final String _TEMP_EXTENSION = ".temp";
  private static final String _NDF_EXTENSION = ".ndf";
  private static final String _XML_EXTENSION = ".xml";
  private static final String _NDF_WARNING_LOG_FILE = "importWarnings.log";
  private static final String _DATA_FILE_EXTENSION = ".dat";
  private static final String _BACKUP_EXTENSION = ".backup";
  private static final String _LOGFILE_EXTENSION = ".log";
  private static final String _ZIP_FILE_EXTENSION = ".zip";
  private static final String _NDF_ZIP_FILE_EXTENSION = ".ndf.zip";
  private static final String _PANEL_ZIP_FILE_EXTENSION = ".panel.zip";
  private static final String _PANEL_NAME_FILE = "dir_id.dat";
  private static final String _5dx_NAME_FILE = "project.testlink";
  private static final String _RTF_ZIP_FILE_EXTENSION = ".rtf.zip";
  private static final String _RESULTS_EXTENSION = ".results";
  private static final String _TEXT_EXTENSION = ".txt";
  private static final String _COMMA_SEPARATED_VALUE_EXTENSION = ".csv";
  private static final String _CONFIG_EXT = ".config";
  private static final String _INDICTMENTS_EXTENSION = ".indictments";
  private static final String _MEASUREMENTS_EXTENSION = ".measurements";
  private static final String _BOARD_INDICTMENTS_EXTENSION = ".boardIndictments";
  private static final String _BOARD_MEASUREMENTS_EXTENSION = ".boardMeasurements";
  private static final String _CALIB_EXTENSION = ".calib";
  private static final String _PROPERTIES_EXTENSION = ".properties";
  private static final String _BINARY_EXTENSION = ".binary";
  private static final String _JSON_EXTENSION = ".json";
  private static final String _CDNA_BENCHMARK_LONG_SCANPATH_FILE = "v810BenchmarkLongScans.scan";
  private static final String _INTERMEDIATE = "intermediate";

  private static final String _ADJUST_AND_CONFIRM_LOG_FILE = "adjustAndConfirm.log";
  private static final String _GREYSCALE_LOG_BASENAME = "grayscale";
  private static final String _XRAY_SPOT_ADJUSTMENT_LOG_BASENAME = "xrayspot";
  private static final String _HIGH_MAG_XRAY_SPOT_ADJUSTMENT_LOG_BASENAME = "highMagXrayspot";
  private static final String _SYSTEM_OFFSETS_LOG_BASENAME = "systemOffsets";
  private static final String _HIGH_MAG_SYSTEM_OFFSETS_LOG_BASENAME = "highMagSystemOffsets";
  private static final String _STAGE_HYSTERESIS_LOG_BASENAME = "stageHysteresis";
  private static final String _HIGH_MAG_STAGE_HYSTERESIS_LOG_BASENAME = "highMagStageHysteresis";
  private static final String _MAGNIFICATION_LOG_BASENAME = "magnification";
  private static final String _HIGH_MAGNIFICATION_LOG_BASENAME = "highMagnification";
  private static final String _OPTICAL_MAGNIFICATION_LOG_BASENAME = "opticalMagnification";
  private static final String _XRAY_SPOT_DEFLECTION_LOG_BASENAME = "xraySpotDeflection";
  private static final String _CAMERA_IMAGE_CONFIRMATION_BASENAME = "cameraImage";
  private static final String _OPTICAL_CAMERA_IMAGE_CONFIRMATION_BASENAME = "opticalCameraImage";
  private static final String _CAMERA_ADJUSTMENT_CONFIRMATION_BASENAME = "cameraAdjustment";
  private static final String _CONFIRM_XRAY_SOURCE_FILE = "xraySource";
  private static final String _CONFIRM_MICROCONTROLLER_FILE = "microController";
  private static final String _BASE_IMAGE_QUALITY_CONFIRMATION_LOG_FILENAME = "baseImageQuality";
  private static final String _FIXED_RAIL_IMAGE_QUALITY_CONFIRMATION_LOG_FILENAME = "fixedRailImageQuality";
  private static final String _SYSTEM_IMAGE_QUALITY_CONFIRMATION_LOG_FILENAME = "systemImageQuality";
  private static final String _ADJUSTABLE_RAIL_IMAGE_QUALITY_CONFIRMATION_LOG_FILENAME = "adjustableRailImageQuality";
  private static final String _SYSTEM_DIAGNOSTIC_LOG_FILENAME = "systemDiagnostic";
  private static final String _CONFIRM_SYSTEM_COMMUNICATION_NETWORK_LOG_FILENAME = "systemCommunicationNetwork";
  private static final String _CONFIRM_PANEL_HANDLING_LOG_FILENAME = "panelHandling";
  private static final String _CONFIRM_BENCHMARK_LOG_FILENAME = "benchmarking";
  private static final String _CONFIRM_MOTION_REPEATABILITY_LOG_FILENAME = "motionRepeatability";
  private static final String _CONFIRM_FRONT_OPTICAL_MEASUREMENT_REPEATABILITY_LOG_FILENAME = "frontOpticalMeasurementRepeatability";
  private static final String _CONFIRM_REAR_OPTICAL_MEASUREMENT_REPEATABILITY_LOG_FILENAME = "rearOpticalMeasurementRepeatability";
  private static final String _CONFIRM_CAMERA_QUALITY_LOG_FILENAME = "cameraQualityMonitoring";
  private static final String _CONFIRM_AREA_MODE_IMAGE_LOG_FILENAME = "areaModeImage";
  //Swee Yee Wong
  //private static final String _CONFIRM_CAMERA_CALIBRATION_IMAGES_LOG_FILENAME = "cameraCalibrationLog.tar";
  private static final String _CONFIRM_CAMERA_CALIBRATION_IMAGES_LOG_FILENAME = "calImage.tar";
  private static final String _OPTICAL_SYSTEM_FIDUCIAL_LOG_FILENAME = "opticalSystemFiducial";
  private static final String _OPTICAL_REFERENCE_PLANE_LOG_FILENAME = "opticalReferencePlane";
  private static final String _OPTICAL_SYSTEM_FIDUCIAL_ROTATION_LOG_FILENAME = "opticalSystemFiducialRotation";
  private static final String _OPTICAL_FRINGE_PATTERN_UNIFORMITY_LOG_FILENAME = "opticalFringePatternUniformity";
  private static final String _OPTICAL_SYSTEM_OFFSET_LOG_FILENAME = "opticalSystemFiducialOffset";

  // calibration files (all should end in .calib)
  private static final String _HARDWARE_CALIB_FILE = "hardware.calib";
  private static final String _HARDWARE_CALIB_CARRY_FORWARD_FILE = "hardware.calib.carryForward";
  private static final String _SOLDER_THICKNESS_TABLE1 = "eutectic6337_copper_shading";
  private static final String _SOLDER_THICKNESS_TABLE_HIGH_MAG = "copper_shading_highMag";
  private static final String _19UM_GAIN1_SOLDER_THICKNESS_TABLE = "19umGain1";
  private static final String _11UM_GAIN1_SOLDER_THICKNESS_TABLE = "11umGain1";
  
  // psp calibration files
  public static final String _PSP_CALIB_FILE = "psp.calib";

  // config files (all should end in .config)
  private static final String _AXI_TDI_XRAY_CAMERA_CONFIG_DOWNLOAD_FILES_ZIP_FILE = "axiXrayCameraRuntimeFiles.zip";
  private static final String _AXI_TDI_XRAY_CAMERA_RUNTIME_FILE_INFORMATION_CONFIG_FILE = "runtimeFiles.config";
  //Variable Mag Anthony August 2011
  private static final String _AXI_TDI_XRAY_CAMERA_LOW_MAGNIFICATION_CONFIG_FILE = "axiXrayCameraLowMag";
  private static final String _AXI_TDI_XRAY_CAMERA_HIGH_MAGNIFICATION_CONFIG_FILE = "axiXrayCameraHighMag";

  private static final String _HARDWARE_CONFIG_FILE = "hardware.config";
  private static final String _HARDWARE_CONFIG_OVERRIDE_FILE = "hardware.override.config";
  private static final String _HARDWARE_CONFIG_CARRY_FORWARD_FILE = "hardware.carryForward.config";
  private static final String _SOFTWARE_CONFIG_FILE = "software.config";
  private static final String _SOFTWARE_CONFIG_CARRY_FORWARD_FILE = "software.carryForward.config";
  private static final String _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG_FILE = "kollmorgen_cdXaxis.config";
  private static final String _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG_FILE = "kollmorgen_cdYaxis.config";
  private static final String _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG_FILE = "kollmorgen_cdRailWidthAxis.config";
  private static final String _PANEL_HANDLER_LOADED_PANEL_CONFIG_FILE = "loadedPanel.config";
  private static final String _XRAY_CAMERA_FIRMWARE_FILE = "xrayCamera.firmware";
  private static final String _PRODUCTION_TUNE_CONFIG_FILE = "productionTune.config";
  private static final String _SERIAL_NUMBER_TO_PROJECT_CONFIG_FILE = "serialNumberToProject.config";
  private static final String _SERIAL_NUMBERS_TO_SKIP_CONFIG_FILE = "serialNumbersToSkip.config";
  private static final String _VALID_SERIAL_NUMBERS_CONFIG_FILE = "validSerialNumbers.config";
  private static final String _DEFAULT_JOINT_TYPE_ASSIGNMENT_CONFIG_FILE = "defaultJointTypeAssignment";
  private static final String _DEFAULT_INITIAL_THRESHOLDS_CONFIG_FILE = "SystemDefaults";
  private static final String _DEFAULT_INITIAL_RECIPE_SETTING_CONFIG_FILE = "SystemDefaults";
  private static final String _RESULTS_PROCESSOR_CONFIG_FILE = "resultsProcessing.config";
  private static final String _DEVELOPER_DEBUG_CONFIG_FILE = "developer.config";
  private static final String _SCAN_ROUTE_CONFIG_FILE = "scanroute.csvconfig"; // XCR1529, New Scan Route, khang-wah.chnee
  
  private static final String _THUMBS_DB_FILE = "Thumbs.db";

  //Swee-Yee.Wong - XCR-3157
  //IRP error message is prompted if run alignment/fine tuning without run full cdna and without unload the board 1st
  private static final String _SYSTEM_STATUS_CONFIG_FILE = "systemStatus.config";
  
  // properties files (should all end in .properties)
  private static final String _CUSTOMER_LIMITS_PROPERTIES_FILE = "customerLimits.properties";

  // files found in the ndf directory under the panel name
  private static final String _PACKAGE_NDF_FILE = "package.ndf";
  private static final String _PROJECT_NDF_FILE = "project.ndf";
  private static final String _PANEL_NDF_FILE = "panel.ndf";
  private static final String _SURFACE_MOUNT_NDF_FILE = "landpat.ndf"; // may not exist if panel has no surface mount
  private static final String _THROUGH_HOLE_NDF_FILE = "padgeom.ndf"; // may not exist if panel has no through hole
  private static final String _LOAD_WARNINGS_LOG_FILE = "loadWarnings.log";
  private static final String _VERSION_NDF_FILE = "version.ndf"; //v810 project ->ndf, Siew Yeng
  private static final String _FOV_DATA_NDF_FILE = "fov_data.ndf"; //v810 project ->ndf, Siew Yeng
  private static final String _ALG_SLICE_NDF_FILE = "algslice.ndf"; //v810 project ->ndf, Siew Yeng

  // files found in the ndf directory under the panel/board directory
  private static final String _BOARD_NDF_FILE = "board.ndf";
  private static final String _COMPONENT_NDF_FILE = "componen.ndf";
  private static final String _SURFACE_MAP_NDF_FILE = "surfmap.ndf"; // only exists after surface map setup - only in top
  private static final String _PINS_NDF_FILE = "pins.ndf"; // may not exist

  private static final String _AXI_LOGO_FILE = "axi.gif";

  private static final String _PROJECT_EXTENSION = ".project";
  private static final String _PROJECT_VERSION_FILE_EXTENSION = ".version";
  private static final String _EXCEPTION_LOG_FILE = "exceptions.log";
  private static final String _EXCEPTION_LOGGER_FILE = "exceptions%g.log";  // the %g is for the rotating sequence of log files (0, 1, ...)
  private static final String _LICENSE_FILE_EXTENSION = ".lic";

  private static final String _ILLEGAL_FILENAME_CHARS = "/:*?\"<>|\\";
  private static final String _ILLEGAL_FULLPATH_CHARS = "*?\"<>|";
  private static final String _CHECKSUM = ".CHECKSUM:";

  private static final String _DISPLAY_CFG = "display.cfg";

  private static final String _DOT = ".";
  private static final String _CAD_FILE_NAME = "cad";
  private static final String _SETTINGS_FILE_NAME = "settings";

  // image files
  private static final String _INSPECTION_REGION = "region";
  private static final int _INSPECTION_REGION_DIGITS = 6;
  private static final String _INSPECTION_SLICE = "slice";
  private static final int _INSPECTION_SLICE_DIGITS = 3;
  private static final String _INSPECTION_EXTENSION = ".png";
  private static final String _INSPECTION_TIFF_EXTENSION = ".tiff";  
  private static final String _FULL_PRECISION_INSPECTION_EXTENSION = ".fpi";
  private static final String _PANEL_TILE_TOP_PREFIX = "top";
  private static final String _PANEL_TILE_BOTTOM_PREFIX = "bottom";
  private static final String _VERIFICATION_IMAGE_EXTENSION = ".jpg";
  private static final String _REPAIR_IMAGE_EXTENSION = ".jpg";
  private static final String _IMAGE_SET_INFO_FILE = "imageSet.info";
  private static NumberFormat _numberFormat = null;

  // useful regular expressions
  private static final String _DIGITS_REGEX = "(\\d+)";

  // persistance files for GUI they are found under config/gui
  private static String _TEST_DEV_PERSIST_FILE = "testDevOptions.persist";
  private static String _TEST_EXEC_PERSIST_FILE = "testExecOptions.persist";
  private static String _MAIN_MENU_PERSIST_FILE = "mainMenuOptions.persist";
  private static String _GUISTATE_PERSIST_FILE = "guiState.persist";
  private static String _SERVICE_PERSIST_FILE = "serviceUIState.persist";
  private static String _CONFIG_PERSIST_FILE = "configUIState.persist";
  private static String _PACKAGE_LIBRARY_SEARCH_PERSIST_FILE = "packageLibSearch.persist";
  private static String _PACKAGE_LIBRARY_IMPORT_OPTION_PERSIST_FILE = "packageLibImportOption.persist";
  private static String _PACKAGE_LIBRARY_DIRECTORY_PERSIST_FILE = "packageLibDir.persist";
  private static String _PACKAGE_LIBRARY_PANEL_SIZE_PERSIST_FILE = "packageLibPanelSize.persist";
  private static String _PACKAGE_LIBRARY_GENERAL_PERSIST_FILE = "packageLibGeneral.persist";
  private static String _VIRTUAL_LIVE_PERSIST = "virtualLive.persist";
  private static String _CAD_CREATOR_PERSIST = "cadCreator.persist";

  private static final String _DIAGNOSTIC_SETTINGS_FILE = "algoSettings.persist";

  private static String _ALGORITHM_SHORTS_LEARNING_FILE = "algorithmShorts.learned";
  private static String _ALGORITHM_SUBTYPES_LEARNING_FILE = "algorithmSubtypes.learned";
  private static String _ALGORITHM_EXPECTED_IMAGE_LEARNING_FILE = "algorithmExpectedImage.learned";
  private static String _ALGORITHM_BROKEN_PIN_LEARNING_FILE = "algorithmBrokenPin.learned";//Broken Pin
  private static String _ALGORITHM_EXPECTED_IMAGE_TEMPLATE_LEARNING_FILE = "algorithmExpectedImageTemplate.learned";//Clear Tombstone

  private static String _ALGORITHM_SHORTS_LEARNING_TEMP_PROJ_OPEN_FILE = "algorithmShorts.learned.temp.projOpen";
  private static String _ALGORITHM_SUBTYPES_LEARNING_TEMP_PROJ_OPEN_FILE = "algorithmSubtypes.learned.temp.projOpen";
  private static String _ALGORITHM_EXPECTED_IMAGE_LEARNING_TEMP_PROJ_OPEN_FILE = "algorithmExpectedImage.learned.temp.projOpen";
  private static String _ALGORITHM_BROKEN_PIN_LEARNING_TEMP_PROJ_OPEN_FILE = "algorithmBrokenPin.learned.temp.projOpen";//Broken Pin
  private static String _ALGORITHM_EXPECTED_IMAGE_TEMPLATE_LEARNING_TEMP_PROJ_OPEN_FILE = "algorithmExpectedImageTemplate.learned.temp.projOpen";//Clear Tombstone

  private static String _ALGORITHM_SHORTS_LEARNING_TEMP_OPEN_FILE = "algorithmShorts.learned.temp.open";
  private static String _ALGORITHM_SUBTYPES_LEARNING_TEMP_OPEN_FILE = "algorithmSubtypes.learned.temp.open";
  private static String _ALGORITHM_EXPECTED_IMAGE_LEARNING_TEMP_OPEN_FILE = "algorithmExpectedImage.learned.temp.open";
  private static String _ALGORITHM_BROKEN_PIN_LEARNING_TEMP_OPEN_FILE = "algorithmBrokenPin.learned.temp.open";//Broken Pin
  private static String _ALGORITHM_EXPECTED_IMAGE_TEMPLATE_LEARNING_TEMP_OPEN_FILE = "algorithmExpectedImageTemplate.learned.temp.open";//Clear Tombstone
  // user login accounts file
  private static String _USER_ACCOUNTS_MANAGER_FILE = "user.accounts";

  // help index file
  private static String _HELP_INDEX_FILE = "index.html";
  private static String _SERVICE_HELP_INDEX_FILE = "index_service.html";

  // service gui configuration files
  private static final String _SCAN_PATH_CONFIGURATION_FILE_EXTENSION = ".scan";
  private static final String _POINT_TO_POINT_CONFIGURATION_FILE_EXTENSION = ".pointToPoint";

  private static final String _PRE_INSPECTION_SCRIPT_INPUT_DATA_FILE = "preInspectionInput.data";
  private static final String _PRE_INSPECTION_SCRIPT_OUTPUT_DATA_FILE = "preInspectionOutput.data";
  private static final String _POST_INSPECTION_SCRIPT_INPUT_DATA_FILE = "postInspectionInput.data";

  // verification images
  private static final String _VERIFICATION_IMAGES_ZIP_FILE = "verificationImages.zip";

  // focus image set
  private static final String _FOCUS_IMAGE_SET_ZIP_FILE = "focusImageSet.zip";

  // XML schema files
  private static final String _CAD_XML_SCHEMA_FILE_NAME = "CadXmlSchema_1.0.xsd";
  private static final String _INDICTMENTS_XML_SCHEMA_FILE_NAME = "InspectionIndictmentsXmlSchema_1.1.xsd";
  private static final String _INDICTMENTS_XML_FOR_MRT_ONLY_SCHEMA_FILE_NAME = "MRTOnlyInspectionIndictmentsXmlSchema_1.1.xsd";
  private static final String _SETTINGS_XML_SCHEMA_FILE_NAME = "SettingsXmlSchema_1.0.xsd";

  // Dump plot info, for example measured BGA midball diameter vs Z height
  private static final String _DUMP_PLOT_INFO_LOG_FILE = "DumpPlotInfo";

  // IRP files
  private static final String _NEARLY_EMPTY_IRP_UPLOAD_FILE = "nearlyEmptyIrpUploadFile";
  private static final String _IMAGE_RECONSTRUCTION_PROCESSOR_APPLICATION_NAME = "IRP.exe";

  private static final String _FIRST_RUN_AFTER_INSTALL_FILE = "firstRunSetupAfterInstallComplete";

  // Library(database) List file for Package Libarry
  private static final String _LIBRARY_LIST_FILE = "libraryList";

  // Performance Log
  private static final String _PERFORMANCE_LOG_FILE = "performanceLog";
  
  // Smema Log
  private static final String _SMEMA_LOG_FILE = "smemaLog";
  private static final String _SMEMA_CONVEYOR_SIGNAL_LOG_FILE = "smemaConveyorSignalLog";

  // RMS files
  private static final String _NEARLY_EMPTY_UPLOAD_FILE = "nearlyEmptyUploadFile";
  private static final String _REMOTE_MOTION_SERVER_APPLICATION_NAME = "v810.jar";

  //virtual live jar file
  private static final String _VIRTUALLIVE_JAR_FILE_NAME = "virtualLive.jar";

  //defect packager exe file name
  private static final String _DEFECT_PACKAGER_EXE_FILE_NAME = "defectPackager.exe";
  
  //delete folder and file exe file name
  private static final String _DELETE_FOLDER_AND_FILE_EXE_FILE_NAME = "deleteFoldersAndFiles.exe";
  
  //runXrayTester.pl
  private static final String _RUN_XRAY_TESTER_FILE_NAME = "runXrayTester.pl";

  //Siew Yeng - XCR-2969
  private static final String _MS820_P2P = "_ms820_p2p";
  private static final String _MSQUADRUSMINI_P2P = "_msQuadrusMini_p2p";
  private static ArrayList<String> _dontCopyList = new ArrayList<String>();
  
  //False call triggering config file
   private static final String _FALSE_CALL_TRIGGERING_CONFIG_FILE = "falseCallTriggering.config";
   
   //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
   private static final String _FALSE_CALL_TRIGGERING_CONFIG_CARRY_FORWARD_FILE = "falseCallTriggering.carryforward.config";
   
  // Dump False call triggering log
   private static final String _DUMP_FALSE_CALL_TRIGGERING_LOG_FILE = "falseCallTriggeringLog";

  private static final String _VIRTUALLIVE_IMAGE_NAME_PREFIX = "VirtualLiveImage";
  private static final String _VIRTUALLIVE_IMAGE_SET_DATA_XML_FILENAME = "ImageSetData.xml";
  private static final String _VIRTUALLIVE_IMAGE_EXTENSION = ".png";
  private static final String _VIRTUALLIVE_TIFF_IMAGE_EXTENSION = ".tiff";
  
  private static String _PRODUCTION_TUNE_SETTINGS = "productionTune";
  private static int _PRODUCTION_TUNE_SETTINGS_VERSION = 1;
  
  private static String _MERGED_BOARD_SUFFIX = "_MERGED";
  
  //Create history file name ended with .history
  private static final String _HISTORY_EXTENSION = ".history";
  // Bee Hoon, XCR1650 - Open recipe and immediately save recipe, it will crash
  private static String _RECIPE_ERROR_LOG = "recipeError";
  private static final String _FRINGE_PATTERN_LINE_DATA_1 = "F1.txt";
  private static final String _FRINGE_PATTERN_LINE_DATA_2 = "F2.txt";
  private static final String _FRINGE_PATTERN_LINE_DATA_3 = "F3.txt";
  private static final String _FRINGE_PATTERN_LINE_DATA_4 = "F4.txt";
  private static final String _FRINGE_PATTERN_LINE_DATA_5 = "F5.txt";
  private static final String _FRINGE_PATTERN_LINE_DATA_6 = "F6.txt";
  private static final String _FRINGE_PATTERN_LINE_DATA_7 = "F7.txt";
  private static final String _FRINGE_PATTERN_LINE_DATA_8 = "F8.txt";
  
  private static final String _PSP_COLOURMAP_BAR = "pspColourBar.png";
  
  //Siew Yeng - XCR1745 - Semi-automated mode
  private static String _SEMI_AUTO_RECIPE_SUFFIX = "_SEMIAUTO";
  
  // Khang Wah, 2014-06-24, Real Time PSH Error handling
  private static final String _PROJECT_ISOLATED_REGION_CSV_FILE = "isolatedPSHRegion";
  
  // swee-yee.wong
  private static final String _CAMERA_COMMAND_REPLIES_LOG_FILE = "cameraCommandAndReplies.log";
  
  // swee-yee.wong
  // Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
  private static final String _IAE_CAMERA_COMMAND_LOG_FILE = "iaeCameraCommands.log";
  private static final String _IAE_STAGE_COMMAND_LOG_FILE = "iaeStageCommands.log";
  
  // bee-hoon.goh
  private static final String _ERROR_HANDLER_LOG_FILE = "errorHandler.log";
  
  //Siew Yeng
  private static final String _MASK_IMAGE_PREFIX = "mask#";
  private static final String _MASK_IMAGE_EXTENSION = ".png";
  private static final String _MASK_IMAGE_ROTATION_SEPARATOR = "@";
  
  private static List<String> _currentDirRecipesList = new ArrayList<String>();
  
  //Khaw Chek Hau - XCR2923 : Machine Utilization Report Tools Implementation
  private static final String _MACHINE_UTILIZATION_REPORT_FILE = "v810MachineUtilizationDailyEventReport";
  private static final String _MACHINE_UTILIZATION_SUMMARY_FILE = "v810MachineUtilizationSummaryReport";
  private static final String _ERROR_TIME_LOG_FILE = "errorTime";

  // swee-yee.wong
  private static final String _HARDWARE_STATUS_LOG_FILE = "hardwareStatus.log";
  
  private static final String _SMT_COMM_IMAGE_FILE = "SMTComm.gif";

  //Khaw Chek Hau - XCR2285: 2D Alignment on v810
  private static final String _ALIGNMENT_2D_IMAGE_FILE_NAME_PATTERN = "^(DraggedImage)\\_(M\\d+)\\_(G\\d+)\\_([^?]+)(\\_\\.png)$";
  
  //weng-jian.eoh
  private static final String _REVERT_RECIPE_FILE_NAME = "RevertRecipe.jar";
  private static final String _NDF_CONVERTER_FILE_NAME = "ndfConverter.jar";

  //Khaw Chek Hau - XCR3534: v810 SECS/GEM protocol implementation
  private static final String _SECS_GEM_LOG_FILE_NAME = "SECSGEMLogFile.csv";
  
  //Khaw Chek Hau - XCR3670: V-ONE AXI latest status monitoring log
  private static final String _MACHINE_LATEST_STATUS_LOG_FILE = "machineLatestStatus";
  
  private static final String _COMPONENT_MAGNIFICATION_INFO_FILE_NAME = "componentMagnification.info";
  
  //Janan Wong - XCR3551: customize measurement XML output file
  private static final String _CUSTOM_MEASUREMENT_XML_CONFIG_FILE = "customMeasurementXML.config";
  
  //Khaw Chek Hau - XCR-3870: False Call Triggering Feature Enhancement
  private static final String _FALSE_CALL_REMARK_TRIGGERED_FILE = "falseCallRemarkTriggered.txt";

  /**
   * @author Wei Chin
   */
  public static int getProductionTuneSettingsLatestFileVersion()
  {
    return _PRODUCTION_TUNE_SETTINGS_VERSION;
  }
  
  /**
   * @author Bill Darbie
   */
  public static int getProjectPanelLatestFileVersion()
  {
    return 2;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectPanelFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_PANEL_FILE + "." + version + _CAD_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectPanelFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);

    return Directory.getProjectDir(projectName) + File.separator + getProjectPanelFile(version);
  }

  /**
   * @author Bill Darbie
   * @author Lim, Seng Yew
   * 
   * Version 5  - 3.1 or below
   * Version 6  - Variable _intersectionMethod added to indicate old scan path generation.
   *              _intersectionMethod = 0, for recipes created using 3.1 or older
   *              _intersectionMethod = 1, for new recipes which will behave according to IL settings properly
   * Version 7  - _isGenerateByNewScanRoute added to indicate new scan route generation for recipe
   * Version 8  - _isVarDivNEnable added to indicate variable divn during old scan path generation
   * Version 9  - _isUseLargeTomoAngle add to indicate tomographic angle to cover during new scan route generation
   * Version 10 - _isEnableLargeImageView, key transferred from software.config enableLargeImageView
   *            - _isGenerateMultiAngleImage, key transferred from software.config generateMultiAngleImage
   *            - _isGenerateComponentImage, added to indicate component image generation for recipe
   * Version 11 - _panelExtraClearDelayInMiliSeconds, added to add extra delay on panel clear sensor during unload board
   * Version 12 - _isEnlargeReconstructionRegion, added for enlarge reconstruction region.
   * Version 13 - _projectLowMagnification and _projectHighMagnification, added for systems supporting more than one Low or High magnification
   * Version 14 - __isBGAJointBasedThresholdInspectionEnabled, added for joint based threshold inspection for BGA
   *            - _isBGAInspectionRegionSetTo1X1, added to set BGA joint inspection region to 1x1
   * Version 15 - _preSelectVariation
   * Version 16 - _thicknessTableVersion, set thickness table version for respective project
   * Version 17 - enable recipe based by pass mode for production run
   * Version 18 - _isEnlargeAlignmentRegion, added for enlarge alignment region.

   * Version 19 - _dynamicRangeOptimizationVersion, DRO version 
   * Version 20 - added initial recipe setting name 
   */
  public static int getProjectProjectSettingsLatestFileVersion()
  {
    return 20;
  }

/*
   * @author Kee Chin Seong
   */
  public static int getCadCreatorProjectSettingsLatestFileVersion()
  {
    return 1;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectProjectSettingsFile(int version)
  {
    Assert.expect(version > 0);

    return _PROJECT_PROJECT_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }
 /**
   * @author chin seong kee
   */
  public static String getProjectCadCreatorSettingsFile(int version)
  {
    Assert.expect(version > 0);

    return _CAD_CREATOR_PROJECT_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public static String getCadCreatorProjectSettingsFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);

    return Directory.getProjectDir(projectName) + File.separator + getProjectCadCreatorSettingsFile(version);
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectProjectSettingsFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);

    return Directory.getProjectDir(projectName) + File.separator + getProjectProjectSettingsFile(version);
  }

  /**
   * @author Bill Darbie
   */
  public static int getProjectPanelSettingsLatestFileVersion()
  {
    return 8;
  }

  /**
  * @author Cheah Lee Herng
  */
  public static int getProjectPspSettingsLatestFileVersion()
  {
    return 3;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static int getProjectPanelSurfaceMapSettingsLatestFileVersion()
  {
    return 4;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public static int getProjectPanelMeshSettingsLatestFileVersion()
  {
    return 1;
  }

  /*
   * @author Kee Chin Seong
   */
  public static int getProjectPanelFocusRegionSettingsLatestFileVersion()
  {
    return 1;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static int getProjectPanelAlignmentSurfaceMapSettingsLatestFileVersion()
  {
    return 2;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectPanelSettingsFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_PANEL_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }
  
   /**
   * @author Cheah Lee Herng 
   */
  public static String getProjectPspSettingsFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_PSP_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getProjectPanelSurfaceMapSettingsFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_PANEL_SURFACE_MAP_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public static String getProjectPanelMeshSettingsFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_PANEL_MESH_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }

  /*
   * @author Kee Chin Seong
   */
  public static String getProjectPanelFocusRegionSettingsFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_PANEL_FOCUS_REGION_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getProjectPanelAlignmentSurfaceMapSettingsFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_PANEL_ALIGNMENT_SURFACE_MAP_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectPanelSettingsFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectPanelSettingsFile(version);
  }
  
   /**
   * @author Cheah Lee Herng
   */
  public static String getProjectPspSettingsFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectPspSettingsFile(version);
  }
  
  /**
   * @author Bill Darbie
   */
  public static String getProjectPanelSurfaceMapSettingsFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectPanelSurfaceMapSettingsFile(version);
  }

  /**
   * @author Cheah Lee Herng 
   */
  public static String getProjectPanelMeshSettingsFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectPanelMeshSettingsFile(version);
  }

  /**
   * @author Kee Chin Seong
   */
  public static String getProjectPanelFocusRegionSettingsFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectPanelFocusRegionSettingsFile(version);
  }
  
  /**
   * @author Bill Darbie
   */
  public static String getProjectPanelAlignmentSurfaceMapSettingsFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectPanelAlignmentSurfaceMapSettingsFile(version);
  }

  /**
   * @author Bill Darbie
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public static int getProjectAlignmentTransfromSettingsLatestFileVersion()
  {
    return 5;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectAlignmentTransfromSettingsFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_ALIGNMENT_TRANSFORM_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectAlignmentTransformSettingsFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectAlignmentTransfromSettingsFile(version);
  }

  /**
   * @author Erica Wheatcroft
   */
  public static String getVerificationImagesZipFile()
  {
    return _VERIFICATION_IMAGES_ZIP_FILE;
  }

  /**
   * @author Erica Wheatcroft
   */
  public static String getFocusImageSetZipFile()
  {
    return _FOCUS_IMAGE_SET_ZIP_FILE;
  }

  /**
   * @author Bill Darbie
   * @author Khaw Chek Hau
   * XCR2514 : Threshold value changed by itself after using software 5.6
   * @author ShengChuan : Increase one version for Clear Tombstone
   */
  public static int getProjectSubtypeSettingsLatestFileVersion()
  {
    return 6;
  }

  /**
   * @author Cheah Lee Herng
   */
  public static int getProjectBoardSurfaceMapSettingsLatestFileVersion()
  {
    return 4;  
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static int getProjectAlignmentSurfaceMapSettingsLatestFileVersion()
  {
    return 2;  
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static int getProjectMeshSettingsLatestFileVersion()
  {
    return 1;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectSubtypeSettingsFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_SUBTYPE_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getProjectSurfaceMapSettingsFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_SURFACE_MAP_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectSubtypeSettingsFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectSubtypeSettingsFile(version);
  }
  
  /**
   * @author Bill Darbie
   */
  public static String getProjectSurfaceSettingsFile(int boardInstanceNumber, int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_BOARD_SURFACE_MAP_SETTINGS_FILE + boardInstanceNumber + "." + version + _SETTINGS_EXTENSION;
  }
  
  /**
   * @author Bill Darbie
   */
  public static String getProjectBoardAlignmentSurfaceMapSettingsFile(int boardInstanceNumber, int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_BOARD_ALIGNMENT_SURFACE_MAP_SETTINGS_FILE + boardInstanceNumber + "." + version + _SETTINGS_EXTENSION;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getProjectMeshSettingsFile(int boardInstanceNumber, int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_BOARD_MESH_SETTINGS_FILE + boardInstanceNumber + "." + version + _SETTINGS_EXTENSION;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getProjectBoardSurfaceMapSettingsFullPath(String projectName, int boardInstanceNumber, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectSurfaceSettingsFile(boardInstanceNumber, version);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getProjectBoardAlignmentSurfaceMapSettingsFullPath(String projectName, int boardInstanceNumber, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectBoardAlignmentSurfaceMapSettingsFile(boardInstanceNumber, version);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getProjectMeshSettingsFullPath(String projectName, int boardInstanceNumber, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectMeshSettingsFile(boardInstanceNumber, version);
  }

  /**
   * @author Bill Darbie
   * 
   * History & description
   * Difference from 4 to 5 - Added Focus Method option, keyword used is "globalSurfaceModel"
   * Difference from 5 to 6 - Added Multi User Gain option
   * Difference from 6 to 7 - Added IL8 option
   * Difference from 7 to 8 - Remove multigain, IL, IC
   */
  public static int getProjectBoardTypeSettingsLatestFileVersion()
  {
    return 8;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectBoardTypeSettingsFile(int boardTypeNumber, int version)
  {
    Assert.expect(version > 0);
    if (version == 1)
    {
      return _PROJECT_BOARD_TYPE_SETTINGS_FILE_FOR_VERSION_1 + boardTypeNumber + "." + version + _SETTINGS_EXTENSION;
    }

    return _PROJECT_BOARD_TYPE_SETTINGS_FILE + boardTypeNumber + "." + version + _SETTINGS_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectBoardTypeSettingsFullPath(String projectName, int boardTypeNumber, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectBoardTypeSettingsFile(boardTypeNumber, version);
  }

  /**
   * @author Bill Darbie
   */
  public static int getProjectBoardSettingsLatestFileVersion()
  {
    return 5;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectBoardSettingsFile(int boardInstanceNumber, int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_BOARD_SETTINGS_FILE + boardInstanceNumber + "." + version + _SETTINGS_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectBoardSettingsFullPath(String projectName, int boardInstanceNumber, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectBoardSettingsFile(boardInstanceNumber, version);
  }

  /**
   * @author Bill Darbie
   */
  public static int getProjectComponentNoLoadSettingsLatestFileVersion()
  {
    return 3;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectComponentNoLoadSettingsFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_COMPONENT_NOLOAD_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static String getProjectComponentNoLoadSettingsFile(int version, int variationNumber)
  {
    Assert.expect(version > 0);
    Assert.expect(variationNumber > 0);
    return _PROJECT_COMPONENT_NOLOAD_SETTINGS_FILE + "." + version + "." + variationNumber + _SETTINGS_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectComponentNoLoadSettingsFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectComponentNoLoadSettingsFile(version);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static String getProjectComponentNoLoadSettingsFullPath(String projectName, int version, int variationNumber)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    Assert.expect(variationNumber > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectComponentNoLoadSettingsFile(version, variationNumber);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static String getProjectComponentNoLoadSettingsFullPath(String projectName, String fileName)
  {
    Assert.expect(projectName != null);
    Assert.expect(fileName != null);
    return Directory.getProjectDir(projectName) + File.separator + fileName;
  }

  /**
   * @author Bill Darbie
   */
  public static int getProjectDatabaseVersionSettingsLatestFileVersion()
  {
    return 1;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectDatabaseVersionSettingsFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_DATABASE_VERSION_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectDatabaseVersionSettingsFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectDatabaseVersionSettingsFile(version);
  }

  /**
   * @author Bill Darbie
   * @author Chnee Khang Wah, XCR1374
   */
  public static int getProjectSummaryLatestFileVersion()
  {
    return 3;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectSummaryFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_SUMMARY_FILE + "." + version + _SUMMARY_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectSummaryFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectSummaryFile(version);
  }

  /**
   * @author Bill Darbie
   */
  public static String getTestProgramFile(String projectName)
  {
    Assert.expect(projectName != null);
    return projectName + _TEST_PROGRAM_FILE_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getTestProgramFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getProjectDir(projectName) + File.separator + getTestProgramFile(projectName);
  }

  /**
   * @author Bill Darbie
   */
  public static int getProjectBoardTypeLatestFileVersion()
  {
    return 2;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectBoardTypeFile(int boardTypeNumber, int version)
  {
    Assert.expect(version > 0);
    if (version == 1)
    {
      return _PROJECT_BOARD_TYPE_FILE_FOR_VERSION_1 + boardTypeNumber + "." + version + _CAD_EXTENSION;
    }

    return _PROJECT_BOARD_TYPE_FILE + boardTypeNumber + "." + version + _CAD_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectBoardTypeFullPath(String projectName, int boardTypeNumber, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectBoardTypeFile(boardTypeNumber, version);
  }

  /**
   * version 2 - added throughHole holeXDimension and holeYDimension - Siew Yeng - XCR-3318
   * @author Bill Darbie
   */
  public static int getProjectLandPatternLatestFileVersion()
  {
    return 2;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectLandPatternFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_LAND_PATTERN_FILE + "." + version + _CAD_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectLandPatternFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectLandPatternFile(version);
  }

  /**
   * @author Bill Darbie
   */
  public static int getProjectCompPackageLatestFileVersion()
  {
    return 2;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectCompPackageFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_COMP_PACKAGE_FILE + "." + version + _CAD_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectCompPackageFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectCompPackageFile(version);
  }

  /**
   * Returns true if the photo.jpg or the panel.jpg files exist properly
   * @author Andy Mechtenberg
   */
  public static boolean isPanelImageAvailable(String projectName)
  {
    Assert.expect(projectName != null);
    String photoImageName = FileName.getPanelPhotoImageFullPath(projectName);
    File photoFile = new File(photoImageName);
    if (photoFile.exists())
      return true;

    String panelImageName = FileName.getPanelImageFullPath(projectName);
    File panelFile = new File(panelImageName);
    if (panelFile.exists())
      return true;

    return false;
  }

  /**
   * @author George Booth
   */
  public static String getPanelImageFile()
  {
    return _PANEL_IMAGE_FILE;
  }

  /**
   * @author George Booth
   */
  public static String getPanelImageFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getProjectDir(projectName) + File.separator + _PANEL_IMAGE_FILE;
  }

  /**
   * @author George Booth
   */
  public static String getPanelFlippedImageFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getProjectDir(projectName) + File.separator + _PANEL_FLIPPED_IMAGE_FILE;
  }

  /**
   * @author George Booth
   */
  public static String getPanelPhotoImageFile()
  {
    return _PANEL_PHOTO_IMAGE_FILE;
  }

  /**
   * @author George Booth
   */
  public static String getPanelPhotoImageFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getProjectDir(projectName) + File.separator + _PANEL_PHOTO_IMAGE_FILE;
  }

  /**
   * @return board name full file path
   * @author Lim Boon Hong
   */
  public static String getPanelBoardImageFullPath(String projectName, String boardName)
  {
    Assert.expect(projectName != null);
    return Directory.getProjectDir(projectName) + File.separator + _BOARD_IMAGE_FILE + boardName + _BOARD_IMAGE_FILE_EXT;
  }
  
  /**
   * @return flipped board name full file path
   * @author Lim Boon Hong
   */
  public static String getPanelBoardFlippedImageFullPath(String projectName, String boardName)
  {
    Assert.expect(projectName != null);
    return Directory.getProjectDir(projectName) + File.separator + _BOARD_FLIPPED_IMAGE_FILE + boardName + _BOARD_IMAGE_FILE_EXT;
  }
  
  /**
    * @return true if "photo.jpg" or "panel.jpg" exists in projects dir
    * @author George Booth
   */
  public static boolean hasPanelImage(String projectName, boolean flipped)
  {
    Assert.expect(projectName != null);
    String photoImageName = getPanelPhotoImageFullPath(projectName);
    File photoFile = new File(photoImageName);
    if (photoFile.exists())
    {
      return true;
    }
    String panelImageName;
    if (flipped)
      panelImageName = FileName.getPanelFlippedImageFullPath(projectName);
    else
      panelImageName = FileName.getPanelImageFullPath(projectName);
    File panelFile = new File(panelImageName);
    if (panelFile.exists())
    {
      return true;
    }
    return false;
  }

  /**
    * @return the contents of "photo.jpg" if it exists, otherwise return
    * the contents of "panel.jpg", otherwise an image of "No Panel Image"
    * Photo.jpg -- a user supplied digital photo image
    * Panel.jpg -- a system generated line drawing image of the panel
    * All image files are assumed to reside in the hashed NDF panel level directory
    * @author George Booth
   */
  public static java.awt.Image getPanelImage(String projectName, boolean flipped)
  {
    Assert.expect(projectName != null);
    String photoImageName = getPanelPhotoImageFullPath(projectName);
    File photoFile = new File(photoImageName);
    if (photoFile.exists())
    {
      try
      {
        return ImageIO.read(photoFile);
      }
      catch (Exception ex)
      {
        // do nothing, let it try the panelImage
      }
    }
    String panelImageName;
    if (flipped)
      panelImageName = FileName.getPanelFlippedImageFullPath(projectName);
    else
      panelImageName = FileName.getPanelImageFullPath(projectName);

//    String panelImageName = FileName.getPanelImageFullPath(projectName);
    File panelFile = new File(panelImageName);
    if (panelFile.exists())
    {
      try
      {
        return ImageIO.read(panelFile);
      }
      catch (IOException iox)
      {
        return Image5DX.getImage(Image5DX.MM_NO_PANEL);
      }
    }
    return Image5DX.getImage(Image5DX.MM_NO_PANEL);
  }

   /**
    * @return the contents of "photo.jpg" if it exists, otherwise return
    * the contents of "panel_boardName.jpg", otherwise an image of "No Panel Image"
    * Photo.jpg -- a user supplied digital photo image
    * Panel_boardName.jpg -- a system generated line drawing image of the panel 
    * All image files are assumed to reside in the hashed NDF panel level directory
    * @author Lim Boon Hong
   */
  public static java.awt.Image getPanelImage(String projectName, String boardName, boolean flipped)
  {
    Assert.expect(projectName != null);
    String photoImageName = getPanelPhotoImageFullPath(projectName);
    File photoFile = new File(photoImageName);
    if (photoFile.exists())
    {
      try
      {
        return ImageIO.read(photoFile);
      }
      catch (Exception ex)
      {
        // do nothing, let it try the panelImage
      }
    }
    String panelImageName;
    if (flipped)
      panelImageName = FileName.getPanelBoardFlippedImageFullPath(projectName, boardName);
    else
      panelImageName = FileName.getPanelBoardImageFullPath(projectName, boardName);

    File panelFile = new File(panelImageName);
    if (panelFile.exists())
    {
      try
      {
        return ImageIO.read(panelFile);
      }
      catch (IOException iox)
      {
        return Image5DX.getImage(Image5DX.MM_NO_PANEL);
      }
    }
    return Image5DX.getImage(Image5DX.MM_NO_PANEL);
  }
  
  /**
   * @author George Booth
   */
  public static String getNoInspectionImageFile()
  {
    return Image5DX.REVIEW_DEFECTS_NO_INSPECTION_IMAGE;
  }

  /**
   * @author George Booth
   */
  public static String getNoInspectionImageFullPath()
  {
    return Directory.getGuiImagesDir() + File.separator + getNoInspectionImageFile();
  }

  /**
   * @author George David
   */
  public static String getSystemConfigFullPath(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);

    return Directory.getConfigDir() + File.separator + systemType.getName() + _CONFIG_EXT;
  }
    
  public static String getSystemConfigBinaryFullPath(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);

    return getSystemConfigFullPath(systemType)+ _BINARY_EXTENSION;
  }
  
  public static String getSystemConfigBinaryFile(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);

    return  systemType.getName() + _CONFIG_EXT+_BINARY_EXTENSION;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @param systemType
   * @return 
   *
   */
  public static String getSystemConfigFullPathForOLP(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);

    return Directory.getConfigDirWithSystemType(systemType)+ File.separator + systemType.getName() + _CONFIG_EXT;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @param systemType
   * @return 
   *
   */
  public static String getSystemConfigFile(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);

    return  systemType.getName() + _CONFIG_EXT;
  }
  
  
  /**
   * @author Wei Chin
   */
  public static String getCurrentSystemConfigFullPath()
  {
    return Directory.getConfigDir() + File.separator + XrayTester.getSystemType().getName() + _CONFIG_EXT;
  }

  /**
   * @author Bill Darbie
   */
  public static String getHardwareConfigFile()
  {
    return _HARDWARE_CONFIG_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getHardwareConfigFullPath()
  {
    return Directory.getConfigDir() + File.separator + _HARDWARE_CONFIG_FILE;
  }

  /**
   * @author George A. David
   */
  public static String getHardwareConfigFullPath(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);

    return Directory.getConfigDir() + File.separator + "hardware." + systemType.getName() + _CONFIG_EXT;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPrevHardwareConfigFullPath()
  {
    return Directory.getPrevConfigDir() + File.separator + _HARDWARE_CONFIG_FILE;
  }
  
 /**
   * @author Jack Hwee
   */
  public static String getPrevHardwareCalibFullPath()
  {
    return Directory.getPrevConfigDir() + File.separator + _HARDWARE_CALIB_FILE + _BINARY_EXTENSION;
  }
  
  /**
   * @author Ngie Xing
   */
  public static String getConfigBinaryFileFullPath()
  {
    return Directory.getConfigDir() + File.separator
      + XrayTester.getSystemType().getName() + getConfigFileExtension() + getBinaryExtension();
  }

  /**
   * @author Bill Darbie
   */
  public static String getHardwareConfigOverrideFile()
  {
    return _HARDWARE_CONFIG_OVERRIDE_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getHardwareConfigOverrideFullPath()
  {
    return Directory.getConfigDir() + File.separator + _HARDWARE_CONFIG_OVERRIDE_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPrevHardwareConfigOverrideFullPath()
  {
    return Directory.getPrevConfigDir() + File.separator + _HARDWARE_CONFIG_OVERRIDE_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getHardwareConfigCarryForwardFullPath()
  {
    return Directory.getConfigDir() + File.separator + _HARDWARE_CONFIG_CARRY_FORWARD_FILE;
  }
  
 /**
   * @author Jack Hwee
   */
  public static String getHardwareCalibCarryForwardFullPath()
  {
    return Directory.getConfigDir() + File.separator + _HARDWARE_CALIB_CARRY_FORWARD_FILE + _CONFIG_EXT;
  }

  /**
   * @author Bill Darbie
   */
  public static String getSoftwareConfigCarryForwareFile()
  {
    return _SOFTWARE_CONFIG_CARRY_FORWARD_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getSoftwareConfigCarryForwardFullPath()
  {
    return Directory.getConfigDir() + File.separator + _SOFTWARE_CONFIG_CARRY_FORWARD_FILE;
  }

  /**
   * @author Reid Hayhow
   */
  public static String getCustomerLimitsPropertiesFullPath()
  {
    return Directory.getPropertiesDir() + File.separator + _CUSTOMER_LIMITS_PROPERTIES_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPropertiesExtension()
  {
    return _PROPERTIES_EXTENSION;
  }

  /**
   * @author Rex Shang
   */
  public static String getKollmorgenCdMotionDriveXaxisConfigFile()
  {
    return _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG_FILE;
  }

  /**
   * @author Greg Esparza
   */
  public static String getKollmorgenCdMotionDriveXaxisConfigFullPath()
  {
    return Directory.getKollmorgenCdMotionDriveDir() + File.separator + _KOLLMORGEN_CD_MOTION_DRIVE_X_AXIS_CONFIG_FILE;
  }

  /**
   * @author Rex Shang
   */
  public static String getKollmorgenCdMotionDriveYaxisConfigFile()
  {
    return _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG_FILE;
  }

  /**
   * @author Greg Esparza
   */
  public static String getKollmorgenCdMotionDriveYaxisConfigFullPath()
  {
    return Directory.getKollmorgenCdMotionDriveDir() + File.separator + _KOLLMORGEN_CD_MOTION_DRIVE_Y_AXIS_CONFIG_FILE;
  }

  /**
   * @author Greg Esparza
   */
  public static String getKollmorgenCdMotionDriveYaxisConfigFullPath(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);

    return Directory.getKollmorgenCdMotionDriveDir() + File.separator + "kollmorgen_cdYaxis." + systemType.getName() + _CONFIG_EXT;
  }

  /**
   * @author Rex Shang
   */
  public static String getKollmorgenCdMotionDriveRailWidthAxisConfigFile()
  {
    return _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG_FILE;
  }

  /**
   * @author Greg Esparza
   */
  public static String getKollmorgenCdMotionDriveRailWidthAxisConfigFullPath()
  {
    return Directory.getKollmorgenCdMotionDriveDir() + File.separator + _KOLLMORGEN_CD_MOTION_DRIVE_RAIL_WIDTH_AXIS_CONFIG_FILE;
  }

  /**
   * @author Rex Shang
   */
  public static String getPanelHandlerLoadedPanelConfigFile()
  {
    return _PANEL_HANDLER_LOADED_PANEL_CONFIG_FILE;
  }
  
  /**
   * XCR-3157
   * IRP error message is prompted if run alignment/fine tuning without run full cdna and without unload the board 1st
   * @author Swee yee wong
   */
  public static String getSystemStatusConfigFile()
  {
    return _SYSTEM_STATUS_CONFIG_FILE;
  }

  /**
   * @author Rex Shang
   */
  public static String getPanelHandlerLoadedPanelConfigFullPath()
  {
	  // FIXME RJG we should not be putting anything into the OS directory
	  return Directory.getOperatingSystemDir() + File.separator + _PANEL_HANDLER_LOADED_PANEL_CONFIG_FILE;
	  // return Directory.getVitroxProgramDataDir() + File.separator + _PANEL_HANDLER_LOADED_PANEL_CONFIG_FILE;
  }
  
  /**
   * To retire method above
   * @author Chnee Khang Wah
   */
  public static String getPanelHandlerLoadedPanelConfigFullPath2()
  {
    return Directory.getSystemTempDir() + File.separator + _PANEL_HANDLER_LOADED_PANEL_CONFIG_FILE;
  }
  
  /**
   * XCR-3157
   * IRP error message is prompted if run alignment/fine tuning without run full cdna and without unload the board 1st
   * @author Swee Yee Wong
   */
  public static String getSystemStatusConfigFullPath()
  {
	  return Directory.getSystemTempDir() + File.separator + _SYSTEM_STATUS_CONFIG_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getHardwareCalibFile()
  {
    return _HARDWARE_CALIB_FILE;
  }

  /**
   * @author Chong Wei Chin
   */
  public static String getHardwareCalibFullPath()
  {
    // The XRayTest may not have been initialized yet when unit testing.
    String system_type;
    if (UnitTest.unitTesting())
    {
      system_type = SystemTypeEnum.THROUGHPUT.getName();
    }
    else
    {
      if(LicenseManager.isXXLEnabled())
        system_type = SystemTypeEnum.XXL.getName();
      else if(LicenseManager.isS2EXEnabled())
        system_type = SystemTypeEnum.S2EX.getName();
      else
        system_type = SystemTypeEnum.THROUGHPUT.getName();          
    }
    return Directory.getCalibDir() + File.separator + system_type + _DOT + _HARDWARE_CALIB_FILE;
  }

    /**
   * @author Chong Wei Chin
   */
  public static String getHardwareCalibFullPath(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);

    return Directory.getCalibDir() + File.separator + systemType.getName() + _DOT + _HARDWARE_CALIB_FILE;
  }
    
  public static String getSystemTypeHardwareCalibBinaryFile()
  {
    String system_type;
    
    if(LicenseManager.isXXLEnabled())
        system_type = SystemTypeEnum.XXL.getName();
      else if(LicenseManager.isS2EXEnabled())
        system_type = SystemTypeEnum.S2EX.getName();
      else
        system_type = SystemTypeEnum.THROUGHPUT.getName();    
    
    return system_type + _DOT + _HARDWARE_CALIB_FILE + _BINARY_EXTENSION;
  }
  
  public static String getHardwareCalibBinaryFullPath(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);
    
    return getHardwareCalibFullPath(systemType) + _BINARY_EXTENSION;
  }
  
  
  /**
   * @author Kok Chun, Tan
   */
  public static String getPspCalibFullPath()
  {
    // The XRayTest may not have been initialized yet when unit testing.
    String system_type;
    if (UnitTest.unitTesting())
    {
      system_type = SystemTypeEnum.THROUGHPUT.getName();
    }
    else
    {
      if(LicenseManager.isXXLEnabled())
        system_type = SystemTypeEnum.XXL.getName();
      else if(LicenseManager.isS2EXEnabled())
        system_type = SystemTypeEnum.S2EX.getName();
      else
        system_type = SystemTypeEnum.THROUGHPUT.getName();          
    }
    return Directory.getCalibDir() + File.separator + system_type + _DOT + _PSP_CALIB_FILE;
  }

    /**
   * @author Chong Wei Chin
   */
  public static String getPspCalibFullPath(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);

    return Directory.getCalibDir() + File.separator + systemType.getName() + _DOT + _PSP_CALIB_FILE;
  }
  
  /**
   * @author Eddie Williamson
   */
  public static String getSolderThicknessTable1File()
  {
    return _SOLDER_THICKNESS_TABLE1 + _CALIB_EXTENSION;
  }

  /**
   * @author Eddie Williamson
   */
  public static String getSolderThicknessTable1FileName()
  {
    return _SOLDER_THICKNESS_TABLE1;
  }
  
  /**
   * @author Eddie Williamson
   */
  public static String getSolderThicknessTable1FullPath()
  {
    return Directory.getSolderThicknessDir() + File.separator + _SOLDER_THICKNESS_TABLE1 + _CALIB_EXTENSION;
  }

  /**
   * @author Wei Chin
   */
  public static String getSolderThicknessTableInHighMagFileName()
  {
    return _SOLDER_THICKNESS_TABLE_HIGH_MAG;
  }

  /**
   * @author Wei Chin
   */
  public static String getSolderThicknessTableLowMagGain1FileName()
  {
    return _19UM_GAIN1_SOLDER_THICKNESS_TABLE;
  }

  /**
   * @author Wei Chin
   */
  public static String getSolderThicknessTableHighMagGain1FileName()
  {
    return _11UM_GAIN1_SOLDER_THICKNESS_TABLE;
  }
  
  /**
   * @author Swee Yee Wong - new thickness table implementation
   */
  public static String getThicknessTableFullPath(String fileName, String layer1SubDir, String layer2SubDir, int version)
  {
    Assert.expect(fileName != null);
    Assert.expect(version >= 0);
    
    //Swee Yee Wong - XCR-3115 Slow inspection and learning in S2
    //don't use LicenseManager to check system type, it will slow down the
    //process if using dongle, so we should use xrayTester instead.
    
    // XCR-3604 Unit Test Phase 2
    if (UnitTest.unitTesting() == false)
    {
      if(XrayTester.isXXLBased())
        fileName = fileName + "_" + "xxl" + "_" + layer1SubDir + "_" + layer2SubDir;
      else if(XrayTester.isS2EXEnabled())
        fileName = fileName + "_" + "s2ex" + "_" + layer1SubDir + "_" + layer2SubDir;
      else
        fileName = fileName + "_" + "standard" + "_" + layer1SubDir + "_" + layer2SubDir;
    }
    else
      fileName = fileName + "_" + "standard" + "_" + layer1SubDir + "_" + layer2SubDir;
    
    if(version == 0)
      return fileName + _CALIB_EXTENSION;
    else
      return fileName + '.' + String.valueOf(version) + _CALIB_EXTENSION;
  }

  /**
   * @author Swee Yee Wong - new thickness table implementation
   */
  public static String getProjectThicknessTableFullPath(String projectName, String layer1SubDir, String layer2SubDir, String fileName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(fileName != null);
    Assert.expect(layer1SubDir != null && Directory.verifySolderThicknessLayer1Dir(layer1SubDir));
    Assert.expect(layer2SubDir != null && Directory.verifySolderThicknessLayer2Dir(layer2SubDir));
    Assert.expect(version >= 0);

    return Directory.getProjectSolderThicknessDir(projectName) + File.separator + layer1SubDir + File.separator + layer2SubDir + File.separator + getThicknessTableFullPath(fileName, layer1SubDir, layer2SubDir, version);
  }
  
  /**
   * @author Swee Yee Wong - new thickness table implementation
   */
  public static String getCalibThicknessTableFullPath(String layer1SubDir, String layer2SubDir, String fileName, int version)
  {
    Assert.expect(fileName != null);
    Assert.expect(layer1SubDir != null && Directory.verifySolderThicknessLayer1Dir(layer1SubDir));
    Assert.expect(layer2SubDir != null && Directory.verifySolderThicknessLayer2Dir(layer2SubDir));
    Assert.expect(version >= 0);

    return Directory.getConfigSolderThicknessDir() + File.separator + layer1SubDir + File.separator + layer2SubDir + File.separator + getThicknessTableFullPath(fileName, layer1SubDir, layer2SubDir, version);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getNonCorrectedFringePatternLine1FullPath()
  {
    return Directory.getNonCorrectedFringePatternDir() + File.separator + _FRINGE_PATTERN_LINE_DATA_1;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getNonCorrectedFringePatternLine2FullPath()
  {
    return Directory.getNonCorrectedFringePatternDir() + File.separator + _FRINGE_PATTERN_LINE_DATA_2;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getNonCorrectedFringePatternLine3FullPath()
  {
    return Directory.getNonCorrectedFringePatternDir() + File.separator + _FRINGE_PATTERN_LINE_DATA_3;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getNonCorrectedFringePatternLine4FullPath()
  {
    return Directory.getNonCorrectedFringePatternDir() + File.separator + _FRINGE_PATTERN_LINE_DATA_4;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getNonCorrectedFringePatternLine5FullPath()
  {
    return Directory.getNonCorrectedFringePatternDir() + File.separator + _FRINGE_PATTERN_LINE_DATA_5;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getNonCorrectedFringePatternLine6FullPath()
  {
    return Directory.getNonCorrectedFringePatternDir() + File.separator + _FRINGE_PATTERN_LINE_DATA_6;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getNonCorrectedFringePatternLine7FullPath()
  {
    return Directory.getNonCorrectedFringePatternDir() + File.separator + _FRINGE_PATTERN_LINE_DATA_7;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getNonCorrectedFringePatternLine8FullPath()
  {
    return Directory.getNonCorrectedFringePatternDir() + File.separator + _FRINGE_PATTERN_LINE_DATA_8;
  }

  /**
   * @author Eddie Williamson
   */
  public static String getAdjustmentsLogFile()
  {
    return _ADJUST_AND_CONFIRM_LOG_FILE;
  }

  /**
   * @author Eddie Williamson
   */
  public static String getAdjustmentsAndConfirmationsLogFullPath()
  {
    return Directory.getLogDir() + File.separator + _ADJUST_AND_CONFIRM_LOG_FILE;
  }

  /**
   * @author Eddie Williamson
   */
  public static String getGreyscaleLogFileBasename()
  {
    return _GREYSCALE_LOG_BASENAME;
  }

  /**
   * @author Eddie Williamson
   */
  public static String getXraySpotLogFileBasename()
  {
    return _XRAY_SPOT_ADJUSTMENT_LOG_BASENAME;
  }

   /**
   * @author Wei Chin
   */
  public static String getHighMagXraySpotLogFileBasename()
  {
    return _HIGH_MAG_XRAY_SPOT_ADJUSTMENT_LOG_BASENAME;
  }
 
  
  /**
   * @author Eddie Williamson
   */
  public static String getStageHysteresisLogFileBasename()
  {
    return _STAGE_HYSTERESIS_LOG_BASENAME;
  }
  
  /**
   * @author Wei Chin
   */
  public static String getHighMagStageHysteresisLogFileBasename()
  {
    return _HIGH_MAG_STAGE_HYSTERESIS_LOG_BASENAME;
  }

  /**
   * @author Eddie Williamson
   */
  public static String getSystemOffsetsLogFileBasename()
  {
    return _SYSTEM_OFFSETS_LOG_BASENAME;
  }
  
  /**
   * @author Wei Chin
   */
  public static String getHighMagSystemOffsetsLogFileBasename()
  {
    return _HIGH_MAG_SYSTEM_OFFSETS_LOG_BASENAME;
  }

  /**
   * @author Eddie Williamson
   */
  public static String getMagnificationLogFileBasename()
  {
    return _MAGNIFICATION_LOG_BASENAME;
  }
  
  /**   
   * @author Cheah Lee Herng 
   */
  public static String getOpticalSystemOffsetLogFileBasename()
  {
    return _OPTICAL_SYSTEM_OFFSET_LOG_FILENAME; 
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getFrontOpticalMeasurementRepeatabilityLogFileBasename()
  {
    return _CONFIRM_FRONT_OPTICAL_MEASUREMENT_REPEATABILITY_LOG_FILENAME;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getRearOpticalMeasurementRepeatabilityLogFileBasename()
  {
    return _CONFIRM_REAR_OPTICAL_MEASUREMENT_REPEATABILITY_LOG_FILENAME;
  }

  /**
   * @author Cheah Lee Herng
   */
  public static String getOpticalMagnificationLogFileBasename()
  {
    return _OPTICAL_MAGNIFICATION_LOG_BASENAME;
  }
 
  /**
   * @author Wei Chin
   */
  public static String getHighMagnificationLogFileBasename()
  {
    return _HIGH_MAGNIFICATION_LOG_BASENAME;
  }

  /**
   * @author Reid Hayhow
   */
  public static String getXraySpotDeflectionLogFileBasename()
  {
    return _XRAY_SPOT_DEFLECTION_LOG_BASENAME;
  }

  /**
   * @author Reid Hayhow
   */
  public static String getCameraAdjustmentConfirmationlLogFileBasename()
  {
    return _CAMERA_ADJUSTMENT_CONFIRMATION_BASENAME;
  }

  /**
   * @author Reid Hayhow
   */
  public static String getCameraImageLogFileBasename()
  {
    return _CAMERA_IMAGE_CONFIRMATION_BASENAME;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getOpticalCameraImageLogFileBasename()
  {
    return _OPTICAL_CAMERA_IMAGE_CONFIRMATION_BASENAME;
  }

  /**
   * @author Reid Hayhow
   */
  public static String getConfirmXraySourceFullPath()
  {
    return Directory.getXraySourceConfirmationLogDir() + File.separator +
        _CONFIRM_XRAY_SOURCE_FILE;
  }

  /**
   * @author Cheah Lee Herng
   */
  public static String getConfirmMicroControllerFullPath()
  {
    return Directory.getXraySourceConfirmationLogDir() + File.separator
      + _CONFIRM_MICROCONTROLLER_FILE;
  }

  /**
   * @author John Dutton
   */
  public static String getBaseImageQualityLogFileName()
  {
    return _BASE_IMAGE_QUALITY_CONFIRMATION_LOG_FILENAME;
  }

  /**
   * @author John Dutton
   */
  public static String getFixedRailImageQualityConfirmationlLogFileName()
  {
    return _FIXED_RAIL_IMAGE_QUALITY_CONFIRMATION_LOG_FILENAME;
  }

  /**
   * @author John Dutton
   */
  public static String getSystemImageQualityConfirmationLogFileName()
  {
    return _SYSTEM_IMAGE_QUALITY_CONFIRMATION_LOG_FILENAME;
  }

  /**
   * @author John Dutton
   */
  public static String getAdjustableRailImageQualityConfirmationLogFileName()
  {
    return _ADJUSTABLE_RAIL_IMAGE_QUALITY_CONFIRMATION_LOG_FILENAME;
  }

  /**
   * @author John Dutton
   */
  public static String getSystemDiagnosticLogFileName()
  {
    return _SYSTEM_DIAGNOSTIC_LOG_FILENAME;
  }

  /**
   * @author Reid Hayhow
   */
  public static String getConfirmSystemCommunicationNetworkFullPath()
  {
    return Directory.getSystemCommunicationNetworkConfirmationLogDir() + File.separator +
        _CONFIRM_SYSTEM_COMMUNICATION_NETWORK_LOG_FILENAME;
  }

  /**
   * @author Reid Hayhow
   */
  public static String getConfirmPanelHandlingFullPath()
  {
    return Directory.getPanelHandlingConfirmationLogDir() + File.separator +
        _CONFIRM_PANEL_HANDLING_LOG_FILENAME;
  }
  
  /**
   * @author Anthony Fong
   */
  public static String getConfirmBenchmarkFullPath()
  {
    return Directory.getBenchmarkConfirmationLogDir() + File.separator +
        _CONFIRM_BENCHMARK_LOG_FILENAME;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalSystemFiducialFullPath()
  {
      return Directory.getOpticalSystemFiducialCalLogDir() + File.separator +
        _OPTICAL_SYSTEM_FIDUCIAL_LOG_FILENAME; 
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalReferencePlaneFullPath()
  {
      return Directory.getOpticalReferencePlaneCalLogDir() + File.separator +
        _OPTICAL_REFERENCE_PLANE_LOG_FILENAME; 
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getOpticalFringePatternUniformityFullPath()
  {
      return Directory.getOpticalFringePatternUniformityCalLogDir() + File.separator +
       _OPTICAL_FRINGE_PATTERN_UNIFORMITY_LOG_FILENAME;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalSystemFiducialRotationFullPath()
  {
      return Directory.getOpticalSystemFiducialCalLogDir() + File.separator +
        _OPTICAL_SYSTEM_FIDUCIAL_ROTATION_LOG_FILENAME; 
  }

  /**
   * @author Reid Hayhow
   */
  public static String getConfirmMotionRepeatabilityFullPath()
  {
    return Directory.getMotionRepeatabilityConfirmationLogDir() + File.separator +
        _CONFIRM_MOTION_REPEATABILITY_LOG_FILENAME;
  }

  /**
   * @author Anthony Fong - CameraQuality
   * XCR1471 X-Ray Camera Quality Monitoring by Anthony on July 2012
   */
  public static String getConfirmCameraQualityLogFileName()
  {
    return _CONFIRM_CAMERA_QUALITY_LOG_FILENAME;
  }

  /**
   * @author Reid Hayhow
   */
  public static String getConfirmAreaModeImageLogFileName()
  {
    return _CONFIRM_AREA_MODE_IMAGE_LOG_FILENAME;
  }
  
  public static String getConfirmCameraCalibrationImagesLogFileName()
  {
    return _CONFIRM_CAMERA_CALIBRATION_IMAGES_LOG_FILENAME;
  }

  /**
   * @author Bill Darbie
   */
  public static String getSoftwareConfigFile()
  {
    return _SOFTWARE_CONFIG_FILE;
  }
  
  /**
   * @author Chin-Seong, Kee
   */
  public static String getDeveloperDebugConfigFile()
  {
    return _DEVELOPER_DEBUG_CONFIG_FILE;
  }
  
  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public static String getScanRouteConfigFile()
  {
    return _SCAN_ROUTE_CONFIG_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getSoftwareConfigFullPath()
  {
    return Directory.getConfigDir() + File.separator + _SOFTWARE_CONFIG_FILE;
  }
  
  /**
   * @author Chin-Seong, Kee
   */
  public static String getDeveloperDebugConfigFullPath()
  {
    return Directory.getConfigDir() + File.separator + _DEVELOPER_DEBUG_CONFIG_FILE;
  }
  
  public static String getScanRouteConfigFullPath(SystemTypeEnum systemType)
  {
    return Directory.getConfigDirWithSystemType(systemType)+ File.separator + _SCAN_ROUTE_CONFIG_FILE;
  }
  /**
   * @author XCR1529, New Scan Route, khang-wah.chnee
   */
  public static String getScanRouteConfigFullPath()
  {
    return Directory.getConfigDir() + File.separator + _SCAN_ROUTE_CONFIG_FILE;
  }
  
  /**
   * @author George A. David
   */
  public static String getSoftwareConfigFullPath(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);

    return Directory.getConfigDir() + File.separator + "software." + systemType.getName() + _CONFIG_EXT;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPrevSoftwareConfigFullPath()
  {
    return Directory.getPrevConfigDir() + File.separator + _SOFTWARE_CONFIG_FILE;
  }

  /**
   * @return the extension used for backing up files.
   * @author Bill Darbie
   */
  public static String getBackupExtension()
  {
    return _BACKUP_EXTENSION;
  }

  /**
   * @return the extension used for log files.
   * @author John Dutton
   */
  public static String getLogFileExtension()
  {
    return _LOGFILE_EXTENSION;
  }

  /**
   * @return the extension used for backup/restore zip files.
   * @author Vincent Wong
   */
  public static String getZipFileExtension()
  {
    return _ZIP_FILE_EXTENSION;
  }

  /**
   * @return the extension used for Ndf panel directory zip file.
   * @author Vincent Wong
   */
  public static String getNdfZipFileExtension()
  {
    return _NDF_ZIP_FILE_EXTENSION;
  }

  /**
   * @return the extension used for RSI's panel directory zip file.
   * @author Vincent Wong
   */
  public static String getPanelZipFileExtension()
  {
    return _PANEL_ZIP_FILE_EXTENSION;
  }

  /**
   * @return the extension used for legacy NDF zip files.
   * @author Vincent Wong
   */
  public static String getLegacyNdfZipFileExtension()
  {
    return _NDF_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getXmlFileExtension()
  {
    return _XML_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getDataFileExtension()
  {
    return _DATA_FILE_EXTENSION;
  }

  /**
   * @author Vincent Wong
   */
  public static String getProjectFileExtension()
  {
    return _PROJECT_EXTENSION;
  }

  /**
   * @author Vincent Wong
   */
  public static String getProjectVersionFileExtension()
  {
    return _PROJECT_VERSION_FILE_EXTENSION;
  }

  /**
   * @return the extension used for backing up files.
   * @author Bill Darbie
   */
  public static String getTempFileExtension()
  {
    return _TEMP_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getCadFileExtension()
  {
    return _CAD_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getSettingsFileExtension()
  {
    return _SETTINGS_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getBinaryExtension()
  {
    return _BINARY_EXTENSION;
  }

  /**
   * @return the full path of the board file (board.ndf)
   * @author Keith Lee
   */
  public static String getLegacyBoardNdfFullPath(String projectName, String sideBoardName)
  {
    Assert.expect(projectName != null);
    Assert.expect(sideBoardName != null);
    return Directory.getLegacyNdfBoardDir(projectName, sideBoardName) + File.separator + _BOARD_NDF_FILE;
  }

  /**
   * @return the full path of the component file (componen.ndf)
   * @author Keith Lee
   */
  public static String getLegacyComponentNdfFullPath(String projectName, String sideBoardName)
  {
    Assert.expect(projectName != null);
    Assert.expect(sideBoardName != null);
    return Directory.getLegacyNdfBoardDir(projectName, sideBoardName) + File.separator + _COMPONENT_NDF_FILE;
  }

  /**
   * @return the full path of the package file (package.ndf)
   * @author Keith Lee
   */
  public static String getLegacyPackageNdfFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getLegacyNdfPanelDir(projectName) + File.separator + _PACKAGE_NDF_FILE;
  }

  /**
   * @return the full path of the project file (project.ndf)
   * @author Bill Darbie
   */
  public static String getLegacyProjectNdfFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getLegacyNdfPanelDir(projectName) + File.separator + _PROJECT_NDF_FILE;
  }

  /**
   * @return the full path of the panel file (panel.ndf)
   * @author Keith Lee
   */
  public static String getLegacyPanelNdfFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getLegacyNdfPanelDir(projectName) + File.separator + _PANEL_NDF_FILE;
  }

  /**
   * @author Roy Williams
   */
  public static int getProjectLatestFileVersion()
  {
    return 3;
  }

  /**
   * @author Roy Williams
   */
  public static String getProjectSerializedFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getProjectDir(projectName) +
           File.separator                       +
           getProjectSerializedNameWithExtension(projectName);
  }

  /**
   * @author Roy Williams
   */
  public static String getProjectSerializedNameWithExtension(String projectName)
  {
    Assert.expect(projectName != null);
    return projectName                          +
           _DOT                                 +
           getProjectLatestFileVersion()        +
           _PROJECT_EXTENSION;
  }

  /**
   * @return the full path of the surface map file (surfmap.ndf)
   * @author Keith Lee
   */
  public static String getLegacySurfaceMapNdfFullPath(String projectName, String sideBoardName)
  {
    Assert.expect(projectName != null);
    Assert.expect(sideBoardName != null);
    return Directory.getLegacyNdfBoardDir(projectName, sideBoardName) + File.separator + _SURFACE_MAP_NDF_FILE;
  }

  /**
   * @return the full path of the surface mount land pattern file (landpat.ndf)
   * @author Keith Lee
   */
  public static String getLegacySurfaceMountLandPatternNdfFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getLegacyNdfPanelDir(projectName) + File.separator + _SURFACE_MOUNT_NDF_FILE;
  }

  /**
   * @return the full path of the through hole land pattern file (padgeom.ndf)
   * @author Keith Lee
   */
  public static String getLegacyThroughHoleLandPatternNdfFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getLegacyNdfPanelDir(projectName) + File.separator + _THROUGH_HOLE_NDF_FILE;
  }

  /**
   * @return the full path of the pins file (pins.ndf)
   * @author Keith Lee
   */
  public static String getLegacyPinsNdfFullPath(String projectName, String sideBoardName)
  {
    Assert.expect(projectName != null);
    Assert.expect(sideBoardName != null);
    return Directory.getLegacyNdfBoardDir(projectName, sideBoardName) + File.separator + _PINS_NDF_FILE;
  }
  
  /*
   * @author Kee Chin Seong
   * @return a list of string of the names of projects that are available for Cad Creator
   */
  public static List<String> getSavedProgressProjectNames()
  {
    String directory = Directory.getProjectsDir();

    Set<String> projectFileNameSet = new HashSet<String>();
    int version = FileName.getCadCreatorProjectSettingsLatestFileVersion();
    for (int i = version; i > 0; --i)
      projectFileNameSet.add(FileName.getProjectCadCreatorSettingsFile(i).toLowerCase());

    List<String> projectNames = new ArrayList<String>();
    File dirNameFile = new File(directory);
    File[] files = dirNameFile.listFiles();
    if (files != null)
    {
      for(int i = 0; i < files.length; ++i)
      {
        if (files[i].isDirectory())
        {
          String dirName = files[i].getName();
          File[] projectFiles = files[i].listFiles();
          if (projectFiles != null)
          {
            for (int j = 0; j < projectFiles.length; ++j)
            {
              if (projectFiles[j].isFile())
              {
                // make sure a file called project.d.settings exists where d is a number.  if it does not
                // exist assume that this directory is not a project directoy
                String projFileName = projectFiles[j].getName().toLowerCase();
                if (projectFileNameSet.contains(projFileName))
                {
                  projectNames.add(dirName);
                  break;
                }
              }
            }
          }
        }
      }
    }

    Collections.sort(projectNames, new CaseInsensitiveStringComparator());
    return projectNames;
  }

  /**
   * @return a List of Strings of the names of projects that are available on this system.
   * @author Bill Darbie
   */
  public static List<String> getProjectNames()
  {
    String directory = Directory.getProjectsDir();

    Set<String> projectFileNameSet = new HashSet<String>();
    int version = FileName.getProjectProjectSettingsLatestFileVersion();
    for (int i = version; i > 0; --i)
      projectFileNameSet.add(FileName.getProjectProjectSettingsFile(i).toLowerCase());

    List<String> projectNames = new ArrayList<String>();
    File dirNameFile = new File(directory);
    File[] files = dirNameFile.listFiles();
    if (files != null)
    {
      for(int i = 0; i < files.length; ++i)
      {
        if (files[i].isDirectory())
        {
          String dirName = files[i].getName();
          File[] projectFiles = files[i].listFiles();
          if (projectFiles != null)
          {
            for (int j = 0; j < projectFiles.length; ++j)
            {
              if (projectFiles[j].isFile())
              {
                // make sure a file called project.d.settings exists where d is a number.  if it does not
                // exist assume that this directory is not a project directoy
                String projFileName = projectFiles[j].getName().toLowerCase();
                if (projectFileNameSet.contains(projFileName))
                {
                  projectNames.add(dirName);
                  break;
                }
              }
            }
          }
        }
      }
    }

    Collections.sort(projectNames, new CaseInsensitiveStringComparator());
    //store the current dir recipe in a list that will used in production to check the loaded panel match 
    //to any recipe in the particular dir or not
    _currentDirRecipesList.clear();
    _currentDirRecipesList.addAll(projectNames);
    return projectNames;
  }

  /**
   * sheng chuan
   */
  public static List<String> getProjectNameList()
  {
    return _currentDirRecipesList;
  }
  
  /**
   * @return a List of Strings of project names in the ndf directory.
   * @author Bill Darbie
   */
  public static List<String> getNdfProjectNames()
  {
    // look under all the directories in the ndf directories at the dir_id.dat file
    // and read out its contents - it has the actual name in it
    return getNdfProjectNames(Directory.getLegacyNdfDir());
  }

   /**
   * @return a List of Strings of 5dx project names in the ndf directory.
   * @author Jack Hwee
   */
  public static List<String> get5dxNdfProjectNames()
  {
    // look under all the directories in the ndf directories at the dir_id.dat file
    // and read out its contents - it has the actual name in it
    Map<String, String> projectNames5dxMap = get5dxNdfProjectNamesToProjectHashNameMap(Directory.getLegacyNdfDir());   
    List<String> projectNames5dxList = new ArrayList<String>(projectNames5dxMap.keySet());
    Collections.sort(projectNames5dxList, new CaseInsensitiveStringComparator());
    return projectNames5dxList;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static Map<String, String> get5dxNdfProjectNamesToProjectHashNameMap()
  {
      return get5dxNdfProjectNamesToProjectHashNameMap(Directory.getLegacyNdfDir());
  }

  /**
   * @return a List of Strings of the ndf or rtf panel names that exist in the directory passed in.
   * @author Bill Darbie
   */
  
  public static Map<String,String> getCadenceAllegroFileNames(String directory)
  {
    Assert.expect(directory != null);

    Map<String,String> projectNames = new HashMap<String,String>();

    File dirNameFile = new File(directory);
    File[] files = dirNameFile.listFiles();
    if (files != null)
    {
      for (int i = 0; i < files.length; ++i)
      {
        if (files[i].isFile())
        {
          String filePath = files[i].getAbsolutePath();
          String fileExtension = FileUtilAxi.getExtension(filePath);
          String projectName = FileUtilAxi.getNameWithoutExtension(filePath);

          if (fileExtension.equals(".cad") || fileExtension.equals(".val"))
          {
            try
            {
              FileInputStream fis = new FileInputStream(filePath);
              InputStreamReader isr = new InputStreamReader(fis);
              BufferedReader is = new BufferedReader(isr);
              String firstLine = is.readLine();

              if (firstLine.startsWith("R\\!") || firstLine.startsWith("A!") || firstLine.startsWith("J!"))
              {
                projectNames.put(projectName, filePath);
              }

              is.close();
            }
            catch (FileNotFoundException fnfe)
            {
              // do nothing
              //throw new FileNotFoundDatastoreException(projectNameFile);
            }
            catch (IOException ioe)
            {
              // do nothing
              //throw new DatastoreException(projectNameFile);
            }
          }

        }
      }
    }

    //Collections.sort(projectNames, new CaseInsensitiveStringComparator());

    return projectNames;
  }
  
  public static Map<String,String> getGencadFileNames(String directory)
  {
    Assert.expect(directory != null);

    Map<String,String> projectNames = new HashMap<String,String>();
    
    File dirNameFile = new File(directory);
    File[] files = dirNameFile.listFiles();
    if (files != null)
    {
      for (int i = 0; i < files.length; ++i)
      {
        if (files[i].isFile())
        {
          String filePath = files[i].getAbsolutePath();
          String fileExtension = FileUtilAxi.getExtension(filePath);
          String projectName = FileUtilAxi.getNameWithoutExtension(filePath);

          if (fileExtension.equals(".cad"))
          {
            try
            {
              FileInputStream fis = new FileInputStream(filePath);
              InputStreamReader isr = new InputStreamReader(fis);
              BufferedReader is = new BufferedReader(isr);
              String firstLine = is.readLine();

              if (firstLine.startsWith("$HEADER") )
              {
                projectNames.put(projectName,filePath);
              }

              is.close();
            }
            catch (FileNotFoundException fnfe)
            {
              // do nothing
              //throw new FileNotFoundDatastoreException(projectNameFile);
            }
            catch (IOException ioe)
            {
              // do nothing
              //throw new DatastoreException(projectNameFile);
            }
          }

        }
      }
    }

    //Collections.sort(projectNames, new CaseInsensitiveStringComparator());

    return projectNames;
  }
  
  public static Map<String,String> getODBFileNames(String directory)
  {
    Assert.expect(directory != null);

    Map<String,String> projectNames = new HashMap<String,String>();

    File dirNameFile = new File(directory);
    File[] files = dirNameFile.listFiles();
    if (files != null)
    {
      for (int i = 0; i < files.length; ++i)
      {
        if (files[i].isFile())
        {
          String filePath = files[i].getAbsolutePath();      
          String projectName = FileUtilAxi.getNameWithoutExtension(filePath);
          if (FileUtilAxi.getExtension(filePath).contains(".tgz"))
          {
            projectNames.put(projectName, filePath);
          }
        }
      }
    }

    //Collections.sort(projectNames, new CaseInsensitiveStringComparator());

    return projectNames;
  }
  
  public static Map<String,String> getFabmasterFileNames(String directory)
  {
    Assert.expect(directory != null);

    Map<String,String> projectNames = new HashMap<String,String>();

    File dirNameFile = new File(directory);
    File[] files = dirNameFile.listFiles();
    if (files != null)
    {
      for (int i = 0; i < files.length; ++i)
      {
        if (files[i].isFile())
        {
          String filePath = files[i].getAbsolutePath();
          String fileExtension = FileUtilAxi.getExtension(filePath);
          String projectName = FileUtilAxi.getNameWithoutExtension(filePath);

          if (fileExtension.equals(".fatf"))
          {
            try
            {
              FileInputStream fis = new FileInputStream(filePath);
              InputStreamReader isr = new InputStreamReader(fis);
              BufferedReader is = new BufferedReader(isr);
              String firstLine = is.readLine();

              if (firstLine.startsWith(";") )
              {
                projectNames.put(projectName,filePath);
              }

              is.close();
            }
            catch (FileNotFoundException fnfe)
            {
              // do nothing
              //throw new FileNotFoundDatastoreException(projectNameFile);
            }
            catch (IOException ioe)
            {
              // do nothing
              //throw new DatastoreException(projectNameFile);
            }
          }

        }
      }
    }

    //Collections.sort(projectNames, new CaseInsensitiveStringComparator());

    return projectNames;
  }
  
  private static List<String> getNdfProjectNames(String directory)
  {
    Assert.expect(directory != null);

    List<String> projectNames = new ArrayList<String>();

    File dirNameFile = new File(directory);
    File[] files = dirNameFile.listFiles();
    if (files != null)
    {
      for (int i = 0; i < files.length; ++i)
      {
        if (files[i].isDirectory())
        {
          String projectNameFile = files[i].getAbsoluteFile() + File.separator + _PANEL_NAME_FILE;
          String project5dxNameFile = files[i].getAbsoluteFile() + File.separator + _5dx_NAME_FILE;
          
          try
          {
            FileInputStream fis = new FileInputStream(projectNameFile);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader is = new BufferedReader(isr);
            String projectName = is.readLine();

            if (FileUtil.exists(project5dxNameFile) == false)
            {
              projectNames.add(projectName);
            }


            is.close();
          }
          catch (FileNotFoundException fnfe)
          {
            // do nothing
            //throw new FileNotFoundDatastoreException(projectNameFile);
          }
          catch (IOException ioe)
          {
            // do nothing
            //throw new DatastoreException(projectNameFile);
          }
        }
      }
    }

    Collections.sort(projectNames, new CaseInsensitiveStringComparator());

    return projectNames;

  }

   /**
   * @return a List of Strings of the 5dx ndf or rtf panel names that exist in the directory passed in.
   * @author Jack Hwee
   */
  private static Map<String, String> get5dxNdfProjectNamesToProjectHashNameMap(String directory)
  {
    Assert.expect(directory != null);

    Map<String, String> projectNames5dx = new HashMap<String, String>();
    File dirNameFile = new File(directory);
    File[] files = dirNameFile.listFiles();
    if (files != null)
    {
      for (int i = 0; i < files.length; ++i)
      {
        if (files[i].isDirectory())
        {
          String projectNameFile = files[i].getAbsoluteFile() + File.separator + _PANEL_NAME_FILE;
          String project5dxNameFile = files[i].getAbsoluteFile() + File.separator + _5dx_NAME_FILE;

          try
          {
            FileInputStream fis = new FileInputStream(projectNameFile);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader is = new BufferedReader(isr);
            String projectName = is.readLine();


            if (FileUtil.exists(project5dxNameFile) == true)
            {
              projectNames5dx.put(projectName, files[i].getName());
            }
            is.close();

          }
          catch (FileNotFoundException fnfe)
          {
            // do nothing
            //throw new FileNotFoundDatastoreException(projectNameFile);
          }
          catch (IOException ioe)
          {
            // do nothing
            //throw new DatastoreException(projectNameFile);
          }
        }
      }
    }

    return projectNames5dx;
  }

  /**
   * @return a List of Strings that contain the full path names of all the
   *         ndf files (in both the panel and board directories) for the panel passed in
   *
   * @author Bill Darbie
   */
  public static List<String> getAllLegacyNdfFullPaths(String projectName)
  {
    Assert.expect(projectName != null);

    List<String> files = new ArrayList<String>();
    String panelDir = Directory.getLegacyNdfPanelDir(projectName);
    FileUtil.findAllFullPaths(panelDir, _NDF_EXTENSION, files, true);

    Collections.sort(files);
    return files;
  }

  /**
   * @author Bill Darbie
   */
  public static String getExceptionLogFullName()
  {
    return Directory.getLogDir() + File.separator + _EXCEPTION_LOG_FILE;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static String getExceptionLoggerFullName()
  {
    return Directory.getLogDir() + File.separator + _EXCEPTION_LOGGER_FILE;
  }

  /**
   * @param projectName the name of the panel program
   * @return true if the string argument projectName contains illegal characters
   *
   * @author Rick Gaudette
   */
  public static boolean hasIllegalChars(String projectName)
  {
    Assert.expect(projectName != null);

    for(int i = 0; i < _ILLEGAL_FILENAME_CHARS.length(); ++i)
    {
      if (projectName.indexOf(_ILLEGAL_FILENAME_CHARS.charAt(i)) != -1)
        return true;
    }

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public static String getIllegalChars()
  {
    return _ILLEGAL_FILENAME_CHARS;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static String getIllegalFullPathChars()
  {
    return _ILLEGAL_FULLPATH_CHARS;
  }


  /**
   * @author Andy Mechtenberg
   */
  public static boolean isLegalFullPath(String fullPath)
  {
    Assert.expect(fullPath != null);

    int index = fullPath.indexOf(":");
    // if there is a colon, it should be 2nd character (index of 1)
    if ((index == 0) || (index > 1))
    {
      return false;
    }
    if (index == 1)
      fullPath = fullPath.replaceFirst(":", "");

    // other colons are not allowed
    index = fullPath.indexOf(":");
    if ((index == 0) || (index > 1))
    {
      return false;
    }

    for(int i = 0; i < _ILLEGAL_FULLPATH_CHARS.length(); ++i)
    {
      if (fullPath.indexOf(_ILLEGAL_FULLPATH_CHARS.charAt(i)) != -1)
        return false;
    }
    return true;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProjectVersionFullName(String projectName)
  {
    Assert.expect(projectName != null);

    return Directory.getProjectsDir() + File.separator + projectName + _PROJECT_VERSION_FILE_EXTENSION;
  }

  /**
   * @author Erica Wheatcroft
   * @return the license file extension
   */
  public static String getLicenseFileExtension()
  {
    return _LICENSE_FILE_EXTENSION;
  }

  /**
   * @author George A. David
   */
  public static String getLoadWarningsLogFullPath(String projectName)
  {
    Assert.expect(projectName != null);

    return Directory.getLegacyNdfPanelDir(projectName) + File.separator + _NDF_WARNING_LOG_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProductionTuneConfigFile()
  {
    return _PRODUCTION_TUNE_CONFIG_FILE;
  }

  /**
   * @author Tan Hock Zoon
   */
  public static String getPrevProductionTuneConfigFullPath()
  {
    return Directory.getPrevConfigDir() + File.separator + _PRODUCTION_TUNE_CONFIG_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getProductionTuneConfigFullPath()
  {
    return Directory.getConfigDir() + File.separator + _PRODUCTION_TUNE_CONFIG_FILE;
  }


  /**
   * @author Bill Darbie
   */
  public static String getSerialNumberToProjectConfigFile()
  {
    return _SERIAL_NUMBER_TO_PROJECT_CONFIG_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getSerialNumberToProjectConfigFullPath()
  {
    return Directory.getSerialNumberDir() + File.separator + _SERIAL_NUMBER_TO_PROJECT_CONFIG_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getSerialNumbersToSkipConfigFile()
  {
    return _SERIAL_NUMBERS_TO_SKIP_CONFIG_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getSerialNumbersToSkipConfigFullPath()
  {
    return Directory.getSerialNumberDir() + File.separator + _SERIAL_NUMBERS_TO_SKIP_CONFIG_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getValidSerialNumbersConfigFile()
  {
    return _VALID_SERIAL_NUMBERS_CONFIG_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getValidSerialNumbersConfigFullPath()
  {
    return Directory.getSerialNumberDir() + File.separator + _VALID_SERIAL_NUMBERS_CONFIG_FILE;
  }

  /**
   * @author Patrick Lacz
   */
  public static String getResultsProcessingConfigFile()
  {
    return _RESULTS_PROCESSOR_CONFIG_FILE;
  }

  /**
   * @author Patrick Lacz
   */
  public static String getResultsProcessingConfigFullPath()
  {
    return Directory.getConfigDir() + File.separator + _RESULTS_PROCESSOR_CONFIG_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPreviousConfigDirectoryResultsProcessingConfigFullPath()
  {
    return Directory.getPrevConfigDir() + File.separator + _RESULTS_PROCESSOR_CONFIG_FILE;
  }

  /**
   * @author George A. David
   */
  public static String getProjectDatabaseFileFullPath(String projectName, int databaseVersion)
  {
    Assert.expect(projectName != null);
    Assert.expect(databaseVersion > 0);

    return Directory.getProjectDatabaseDir() + File.separator + projectName + "." + databaseVersion + getZipFileExtension();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getProjectDatabaseSummaryFileFullPath(String projectName, int databaseVersion, int projectSummaryVersion)
  {
    Assert.expect(projectName != null);
    Assert.expect(databaseVersion > 0);
    Assert.expect(projectSummaryVersion > 0);

    return Directory.getProjectDatabaseDir() + File.separator + projectName + "." + databaseVersion + "." + projectSummaryVersion + ".summary";
  }

  /**
   * @author Chnee Khang Wah, XCR1374
   */
  public static String getProjectDatabaseSummaryFileFullPath2(String projectName, int databaseVersion)
  {
    Assert.expect(projectName != null);
    Assert.expect(databaseVersion > 0);

    return Directory.getProjectDatabaseDir() + File.separator + projectName + "." + databaseVersion + ".2.summary";
  }
  
  /**
   * @author George A. David
   */
  public static String getProjectDatabaseSummaryFileFullPath(String projectName, int databaseVersion)
  {
    Assert.expect(projectName != null);
    Assert.expect(databaseVersion > 0);

    return Directory.getProjectDatabaseDir() + File.separator + projectName + "." + databaseVersion + ".1.summary";
  }

  /**
   * @author George A. David
   */
  public static String getPreviousProjectDatabaseSummaryFileFullPath(String projectName, int databaseVersion)
  {
    Assert.expect(projectName != null);
    Assert.expect(databaseVersion > 0);

    return Directory.getProjectDatabaseDir() + File.separator + projectName + "." + databaseVersion + ".0.summary";
  }

  /**
   * @author George A. David
   */
  public static String getProjectDatabaseCommentFileFullPath(String projectName, int databaseVersion)
  {
    Assert.expect(projectName != null);
    Assert.expect(databaseVersion > 0);

    return Directory.getProjectDatabaseDir() + File.separator + projectName + "." + databaseVersion + ".comment";
  }

  /**
   * @author Kay Lannen
   */
  public static String getImageName(String reconstructionRegionName, int sliceId, int regionId)
  {
    Assert.expect(reconstructionRegionName != null);
    Assert.expect(sliceId >= 0);
    Assert.expect(regionId >= 0);
    if (Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType())
      return reconstructionRegionName + "_" + sliceId + "_" + regionId + _INSPECTION_TIFF_EXTENSION;
    else
      return reconstructionRegionName + "_" + sliceId + "_" + regionId + _INSPECTION_EXTENSION;
  }

  /**
   * @author Kay Lannen
   */
  public static String getInspectionImageName(String reconstructionRegionName, int sliceId)
  {
    Assert.expect(reconstructionRegionName != null);
    Assert.expect(sliceId >= 0);

    if (Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType() )
      return reconstructionRegionName + "_" + sliceId + _INSPECTION_TIFF_EXTENSION;
    else
      return reconstructionRegionName + "_" + sliceId + _INSPECTION_EXTENSION;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getInspectionImageName(String directory, String projectName, String imageSetName, String reconstructionRegionName, int sliceId)
  {
    Assert.expect(directory != null);
    Assert.expect(projectName != null);
    Assert.expect(reconstructionRegionName != null);
    Assert.expect(sliceId >= 0);
    
    return directory + File.separator + projectName + File.separator + imageSetName + File.separator + getInspectionImageName(reconstructionRegionName, sliceId);
  }

  /**
   * @author Wei Chin
   */
  public static String getTiffInspectionImageName(String reconstructionRegionName, int sliceId)
  {
    Assert.expect(reconstructionRegionName != null);
    Assert.expect(sliceId >= 0);
    return reconstructionRegionName + "_" + sliceId + _INSPECTION_TIFF_EXTENSION;
  }

  /**
   * @author Wei Chin
   */
  public static String getPngInspectionImageName(String reconstructionRegionName, int sliceId)
  {
    Assert.expect(reconstructionRegionName != null);
    Assert.expect(sliceId >= 0);

    return reconstructionRegionName + "_" + sliceId + _INSPECTION_EXTENSION;
  }

  /**
   * @author John Heumann
   */
  public static String getFullPrecisionInspectionImageName(String reconstructionRegionName, int sliceId)
  {
    Assert.expect(reconstructionRegionName != null);
    Assert.expect(sliceId >= 0);

    return reconstructionRegionName + "_" + sliceId + _FULL_PRECISION_INSPECTION_EXTENSION;
  }

  /**
   * @author George A. David
   */
  public static String getLightestInspectionImageName(String reconstructionRegionName, int sliceId)
  {
    Assert.expect(reconstructionRegionName != null);
    Assert.expect(sliceId >= 0);

    return reconstructionRegionName + "_" + sliceId + "_lightest" + _INSPECTION_EXTENSION;
  }

  /**
   * @author Matt Wharton
   */
  public static String getFocusConfirmationInspectionImageName(String reconstructionRegionName, int sliceId, int sliceOffset)
  {
    Assert.expect(reconstructionRegionName != null);
    Assert.expect(sliceId >= 0);

    return reconstructionRegionName + "_" + sliceId + "_" + sliceOffset + _INSPECTION_EXTENSION;
  }

  /**
   * adjust focus images are special images we capture when a user marks an inspection image as unfocused
   * the system will then generate about 100 images at differing z heights to attempt to find a focused image
   * @param reconstructionRegionName the name of the reconstruction region
   * @param zHeightInMils the zheight of the image in mils
   * @author George A. David
   */
  public static String getAdjustFocusImageName(String reconstructionRegionName, int zHeightInMils)
  {
    Assert.expect(reconstructionRegionName != null);

    if (Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType() )
      return reconstructionRegionName + "_offset_" + zHeightInMils + _INSPECTION_TIFF_EXTENSION;
    else
      return reconstructionRegionName + "_offset_" + zHeightInMils + _INSPECTION_EXTENSION;
  }

  /**
   * @author Matt Wharton
   */
  public static String getCachedInspectionImageName(String reconstructionRegionName, int sliceId, int imageTypeId)
  {
    Assert.expect(reconstructionRegionName != null);
    Assert.expect(sliceId >= 0);
    Assert.expect(imageTypeId >= 0);

    if (Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType() )
      return reconstructionRegionName + "_" + sliceId + "_" + imageTypeId + _INSPECTION_TIFF_EXTENSION;
    else
      return reconstructionRegionName + "_" + sliceId + "_" + imageTypeId + _INSPECTION_EXTENSION;
  }

  /**
   * @author Kay Lannen
   */
  public static String getVerificationImageName(String reconstructionRegionName)
  {
    Assert.expect(reconstructionRegionName != null);

    return reconstructionRegionName + _VERIFICATION_IMAGE_EXTENSION;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static String getAlignmentVerificationImageFullPath(String projectName, String alignmentGroupName, boolean isSecondOfLongPanel)
  {
    Assert.expect(projectName != null);
    Assert.expect(alignmentGroupName != null);

    String fullPath = Directory.getAlignmentImagesDir() + File.separator;

    if (isSecondOfLongPanel)
      fullPath = fullPath + "Alignment_Long_" + projectName + "_" + alignmentGroupName + _INSPECTION_EXTENSION;
    else
      fullPath = fullPath + "Alignment_" + projectName + "_" + alignmentGroupName + _INSPECTION_EXTENSION;
    return fullPath;
  }

  /**
   * @author Matt Wharton
   */
  public static String getSavedAlignmentImageFullPath(String projectName,
                                                      String alignmentGroupName,
                                                      boolean isSecondPortionOfLongPanel)
  {
    Assert.expect(projectName != null);
    Assert.expect(alignmentGroupName != null);

    String fullPath;
    String savedAlignmentImagesDir = Directory.getSavedAlignmentImagesDir();
    if (isSecondPortionOfLongPanel)
    {
      fullPath = savedAlignmentImagesDir +
          File.separator +
          "Raw_Alignment_Long_" +
          projectName +
          "_" +
          alignmentGroupName +
          _INSPECTION_EXTENSION;
    }
    else
    {
      fullPath = savedAlignmentImagesDir +
          File.separator +
          "Raw_Alignment_" +
          projectName +
          "_" +
          alignmentGroupName +
          _INSPECTION_EXTENSION;
    }

    return fullPath;
  }

  /**
   * @author Matt Wharton
   */
  public static String getRawAlignmentImageFullPath(String projectName,
                                                    String alignmentGroupName,
                                                    boolean isSecondPortionOfLongPanel)
  {
    Assert.expect(projectName != null);
    Assert.expect(alignmentGroupName != null);

    String fullPath;
    String alignmentImagesDir = Directory.getAlignmentImagesDir();
    if (isSecondPortionOfLongPanel)
    {
      fullPath = alignmentImagesDir +
          File.separator +
          "Raw_Alignment_Long_" +
          projectName +
          "_" +
          alignmentGroupName +
          _INSPECTION_EXTENSION;
    }
    else
    {
      fullPath = alignmentImagesDir +
          File.separator +
          "Raw_Alignment_" +
          projectName +
          "_" +
          alignmentGroupName +
          _INSPECTION_EXTENSION;
    }

    return fullPath;
  }

  /**
   * @author Seng-Yew Lim
   */
  public static String getRawAlignmentImageFullPath(String projectName,
                                                    String alignmentGroupName,
                                                    boolean isSecondPortionOfLongPanel,
                                                    long timeStampInMillis)
  {
    Assert.expect(projectName != null);
    Assert.expect(alignmentGroupName != null);

    SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd_kk-mm-ss-SSS");

    String fullPath;
    String alignmentImagesDir = Directory.getAlignmentImagesDir();
    if (isSecondPortionOfLongPanel)
    {
      fullPath = alignmentImagesDir +
          File.separator +
          "Raw_Alignment_Long_" +
          projectName +
          "_" +
          alignmentGroupName +
          "_" +
          dateTimeFormatter.format(new Date(timeStampInMillis)) +
          _INSPECTION_EXTENSION;
    }
    else
    {
      fullPath = alignmentImagesDir +
          File.separator +
          "Raw_Alignment_" +
          projectName +
          "_" +
          alignmentGroupName +
          "_" +
          dateTimeFormatter.format(new Date(timeStampInMillis)) +
          _INSPECTION_EXTENSION;
    }

    return fullPath;
  }

  /**
   * @author Seng-Yew Lim
   */
  public static String getAlignmentImageFullPath(String projectName,
                                                    String alignmentGroupName,
                                                    boolean isSecondPortionOfLongPanel,
                                                    long timeStampInMillis)
  {
    Assert.expect(projectName != null);
    Assert.expect(alignmentGroupName != null);

    SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd_kk-mm-ss-SSS");

    String fullPath;
    String alignmentImagesDir = Directory.getAlignmentImagesDir();
    if (isSecondPortionOfLongPanel)
    {
      fullPath = alignmentImagesDir +
          File.separator +
          "Alignment_Long_" +
          projectName +
          "_" +
          alignmentGroupName +
          "_" +
          dateTimeFormatter.format(new Date(timeStampInMillis)) +
          _INSPECTION_EXTENSION;
    }
    else
    {
      fullPath = alignmentImagesDir +
          File.separator +
          "Alignment_" +
          projectName +
          "_" +
          alignmentGroupName +
          "_" +
          dateTimeFormatter.format(new Date(timeStampInMillis)) +
          _INSPECTION_EXTENSION;
    }

    return fullPath;
  }

  /**
   * @author Seng-Yew Lim
   */
  public static String getTemplateAlignmentImageFullPath(String projectName,
                                                    String alignmentGroupName,
                                                    boolean isSecondPortionOfLongPanel,
                                                    long timeStampInMillis)
  {
    Assert.expect(projectName != null);
    Assert.expect(alignmentGroupName != null);

    SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd_kk-mm-ss-SSS");

    String fullPath;
    String alignmentImagesDir = Directory.getAlignmentImagesDir();
    if (isSecondPortionOfLongPanel)
    {
      fullPath = alignmentImagesDir +
          File.separator +
          "Template_Alignment_Long_" +
          projectName +
          "_" +
          alignmentGroupName +
          "_" +
          dateTimeFormatter.format(new Date(timeStampInMillis)) +
          _INSPECTION_EXTENSION;
    }
    else
    {
      fullPath = alignmentImagesDir +
          File.separator +
          "Template_Alignment_" +
          projectName +
          "_" +
          alignmentGroupName +
          "_" +
          dateTimeFormatter.format(new Date(timeStampInMillis)) +
          _INSPECTION_EXTENSION;
    }

    return fullPath;
  }

  /**
   * @author John Heumann
   */
  public static String getRawAlignmentFullPrecisionImageFullPath(String projectName,
                                                                 String alignmentGroupName,
                                                                 boolean isSecondPortionOfLongPanel)
  {
    Assert.expect(projectName != null);
    Assert.expect(alignmentGroupName != null);

    String fullPath;
    String alignmentImagesDir = Directory.getAlignmentImagesDir();
    if (isSecondPortionOfLongPanel)
    {
      fullPath = alignmentImagesDir +
          File.separator +
          "Raw_Alignment_Long_" +
          projectName +
          "_" +
          alignmentGroupName +
          _FULL_PRECISION_INSPECTION_EXTENSION;
    }
    else
    {
      fullPath = alignmentImagesDir +
          File.separator +
          "Raw_Alignment_" +
          projectName +
          "_" +
          alignmentGroupName +
          _FULL_PRECISION_INSPECTION_EXTENSION;
    }

    return fullPath;
  }

  /**
   * @author John Heumann
   */
  public static String getAlignmentTemplateFullPrecisionImageFullPath(String projectName,
                                                                      String alignmentGroupName,
                                                                      boolean isSecondPortionOfLongPanel)
  {
    Assert.expect(projectName != null);
    Assert.expect(alignmentGroupName != null);

    String fullPath;
    String alignmentImagesDir = Directory.getAlignmentImagesDir();
    if (isSecondPortionOfLongPanel)
    {
      fullPath = alignmentImagesDir +
          File.separator +
          "Alignment_Template_Long_" +
          projectName +
          "_" +
          alignmentGroupName +
          _FULL_PRECISION_INSPECTION_EXTENSION;
    }
    else
    {
      fullPath = alignmentImagesDir +
          File.separator +
          "Alignment_Template" +
          projectName +
          "_" +
          alignmentGroupName +
          _FULL_PRECISION_INSPECTION_EXTENSION;
    }

    return fullPath;
  }
  /**
   * @author Matt Wharton
   */
  public static String getRawManualAlignmentImageFullPath(String projectName,
                                                          String alignmentGroupName,
                                                          boolean isSecondPortionOfLongPanel)
  {
    Assert.expect(projectName != null);
    Assert.expect(alignmentGroupName != null);

    String fullPath;
    String alignmentImagesDir = Directory.getAlignmentImagesDir();
    if (isSecondPortionOfLongPanel)
    {
      fullPath = alignmentImagesDir +
          File.separator +
          "Raw_Manual_Alignment_Long_" +
          projectName +
          "_" +
          alignmentGroupName +
          _INSPECTION_EXTENSION;
    }
    else
    {
      fullPath = alignmentImagesDir +
          File.separator +
          "Raw_Manual_Alignment_" +
          projectName +
          "_" +
          alignmentGroupName +
          _INSPECTION_EXTENSION;
    }

    return fullPath;
  }

  /**
   * @author Matt Wharton
   */
  public static String getRawFailingAlignmentImageFullPath(String projectName,
                                                           String alignmentGroupName,
                                                           boolean isSecondPortionOfLongPanel,
                                                           long timeStampInMillis)
  {
    Assert.expect(projectName != null);
    Assert.expect(alignmentGroupName != null);

    String fullPath;
    String failingAlignmentImagesDir = Directory.getFailingAlignmentImagesDir();
    if (isSecondPortionOfLongPanel)
    {
      fullPath = failingAlignmentImagesDir +
          File.separator +
          "Raw_Alignment_Long_" +
          projectName +
          "_" +
          alignmentGroupName +
          "_" +
          timeStampInMillis +
          _INSPECTION_EXTENSION;
    }
    else
    {
      fullPath = failingAlignmentImagesDir +
          File.separator +
          "Raw_Alignment_" +
          projectName +
          "_" +
          alignmentGroupName +
          "_" +
          timeStampInMillis +
          _INSPECTION_EXTENSION;
    }

    return fullPath;
  }



  /**
   * @author Matt Wharton
   */
  public static String getFailingAlignmentImageFullPath(String projectName,
                                                        String alignmentGroupName,
                                                        boolean isSecondPortionOfLongPanel,
                                                        long timeStampInMillis)
  {
    Assert.expect(projectName != null);
    Assert.expect(alignmentGroupName != null);

    String fullPath;
    String failingAlignmentImagesDir = Directory.getFailingAlignmentImagesDir();
    if (isSecondPortionOfLongPanel)
    {
      fullPath = failingAlignmentImagesDir +
          File.separator +
          "Alignment_Long_" +
          projectName +
          "_" +
          alignmentGroupName +
          "_" +
          timeStampInMillis +
          _INSPECTION_EXTENSION;
    }
    else
    {
      fullPath = failingAlignmentImagesDir +
          File.separator +
          "Alignment_" +
          projectName +
          "_" +
          alignmentGroupName +
          "_" +
          timeStampInMillis +
          _INSPECTION_EXTENSION;
    }

    return fullPath;
  }

  /**
   * @author Matt Wharton
   */
  public static String getFailingAlignmentImageCSVFullPath(String projectName,
                                                           String alignmentGroupName,
                                                           boolean isSecondPortionOfLongPanel,
                                                           long timeStampInMillis)
  {
    Assert.expect(projectName != null);
    Assert.expect(alignmentGroupName != null);

    String fullPath;
    String failingAlignmentImagesDir = Directory.getFailingAlignmentImagesDir();
    if (isSecondPortionOfLongPanel)
    {
      fullPath = failingAlignmentImagesDir +
          File.separator +
          "Alignment_Long_" +
          projectName +
          "_" +
          alignmentGroupName +
          "_" +
          timeStampInMillis +
          ".csv";
    }
    else
    {
      fullPath = failingAlignmentImagesDir +
          File.separator +
          "Alignment_" +
          projectName +
          "_" +
          alignmentGroupName +
          "_" +
          timeStampInMillis +
          ".csv";
    }

    return fullPath;
  }

  /**
   * @todo MDW remove later!
   *
   * @author Matt Wharton
   */
  public static String getCallRateSpikeAlignmentImageFullPath(String projectName,
                                                              String alignmentGroupName,
                                                              boolean isSecondPortionOfLongPanel,
                                                              long timeStampInMillis)
  {
    Assert.expect(projectName != null);
    Assert.expect(alignmentGroupName != null);

    String fullPath;
    String callRateSpikeAlignmentImagesDir = Directory.getCallRateSpikeAlignmentImagesDir();
    if (isSecondPortionOfLongPanel)
    {
      fullPath = callRateSpikeAlignmentImagesDir +
          File.separator +
          "Alignment_Long_" +
          projectName +
          "_" +
          alignmentGroupName +
          "_" +
          timeStampInMillis +
          _INSPECTION_EXTENSION;
    }
    else
    {
      fullPath = callRateSpikeAlignmentImagesDir +
          File.separator +
          "Alignment_" +
          projectName +
          "_" +
          alignmentGroupName +
          "_" +
          timeStampInMillis +
          _INSPECTION_EXTENSION;
    }

    return fullPath;
  }

  /**
   * @author Patrick Lacz
   */
  public static RegExFileNameFilter getInspectionImageFilenameFilter()
  {
    if (Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType() )
      return new RegExFileNameFilter(
        java.util.regex.Pattern.quote(_INSPECTION_REGION) +
        _DIGITS_REGEX +
        java.util.regex.Pattern.quote(_INSPECTION_SLICE) +
        _DIGITS_REGEX +
        java.util.regex.Pattern.quote(_INSPECTION_TIFF_EXTENSION));
    else
      return new RegExFileNameFilter(
        java.util.regex.Pattern.quote(_INSPECTION_REGION) +
        _DIGITS_REGEX +
        java.util.regex.Pattern.quote(_INSPECTION_SLICE) +
        _DIGITS_REGEX +
        java.util.regex.Pattern.quote(_INSPECTION_EXTENSION));
  }

  /**
   * @author Laura Cormos
   * @author Kee Chin Seong - Adding the Width Height to make the image name unique
   * @author Kee Chin Seong - Adding the Width Height to make the image name unique
   */
  public static String getResultImageName(boolean topSide, int xCoordinate, int yCoordinate, int width, int height, int sliceId)
  {
    return getResultImageName(topSide, xCoordinate, yCoordinate, width, height, sliceId, 0);
  }

  /**
   * @author George A. David
   * @author Kee Chin Seong - Adding the Width Height to make the image name unique
   */
  public static String getResultImageName(boolean topSide, int xCoordinate, int yCoordinate, int width, int height, int sliceId, int sliceOffset)
  {
    String prefix;
    if (topSide)
      prefix = _PANEL_TILE_TOP_PREFIX;
    else
      prefix = _PANEL_TILE_BOTTOM_PREFIX;
    
    if (Config.getInstance().getIntValue(SoftwareConfigEnum.REPAIR_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getPngImageType())
      return prefix + xCoordinate + "x" + yCoordinate + "_" + width + "x" + height + "_" + sliceId + "_" + sliceOffset + _INSPECTION_EXTENSION;
    else
      return prefix + xCoordinate + "x" + yCoordinate + "_" + width + "x" + height + "_" + sliceId + "_" + sliceOffset + _REPAIR_IMAGE_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getRepairImageExtension()
  {
    return _REPAIR_IMAGE_EXTENSION;
  }

  /**
   * @author George A. David
   */
  public static String getInspectionImageInfoProgramFileName()
  {
    Assert.expect(_INSPECTION_IMAGE_INFO_PROGRAM_FILE != null);

    return _INSPECTION_IMAGE_INFO_PROGRAM_FILE;
  }
  
  public static String getComponentMagnificationInfoFileName()
  {
    Assert.expect(_COMPONENT_MAGNIFICATION_INFO_FILE_NAME != null);

    return _COMPONENT_MAGNIFICATION_INFO_FILE_NAME;
  }

  /**
   * @author George A. David
   */
  public static String getAdjustFocusImageNamesProgramFileName()
  {
    Assert.expect(_ADJUST_FOCUS_IMAGE_NAMES_PROGRAM_FILE != null);

    return _ADJUST_FOCUS_IMAGE_NAMES_PROGRAM_FILE;
  }

  /**
   * @author George A. David
   */
  public static String getVerificationImageNamesProgramFileName()
  {
    Assert.expect(_VERIFICATION_IMAGE_NAMES_PROGRAM_FILE != null);

    return _VERIFICATION_IMAGE_NAMES_PROGRAM_FILE;
  }

  /**
   * @author George A. David
   */
  public static String getImageSetInfoFileName()
  {
    Assert.expect(_IMAGE_SET_INFO_FILE != null);

    return _IMAGE_SET_INFO_FILE;
  }

  /**
   * @author George A. David
   */
  public static String getInspectionImageInfoProgramFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getProjectDir(projectName) + File.separator + getInspectionImageInfoProgramFileName();
  }

  /**
   * @author George A. David
   */
  public static String getVerificationImageNamesProgramFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getXrayVerificationImagesDir(projectName) + File.separator + getVerificationImageNamesProgramFileName();
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public static String get2DVerificationImageNamesProgramFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getXray2DVerificationImagesDir(projectName) + File.separator + getVerificationImageNamesProgramFileName();
  }

  /**
   * @author George A. David
   */
  public static String getVerificationImageNamesProgramFullPathInProjectDir(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getProjectDir(projectName) + File.separator + getVerificationImageNamesProgramFileName();
  }

  /**
   * @author Scott Richardson
   */
  public static String getVirtualLiveImageNamesProgramFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getXrayVirtualLiveImagesDir(projectName) + File.separator + getVerificationImageNamesProgramFileName();
  }

  /**
   * @author George A. David
   */
  public static String getInspectionImageInfoProgramFullPath(String projectName, String imageSetName)
  {
    Assert.expect(projectName != null);
    Assert.expect(imageSetName != null);
    return Directory.getXrayInspectionImagesDir(projectName, imageSetName) + File.separator + getInspectionImageInfoProgramFileName();
  }

  /**
   * @author George A. David
   */
  public static String getInspectionImageInfoProgramFullPathForOnlineTestDevelopment(String imageSetName)
  {
    Assert.expect(imageSetName != null);
    return Directory.getOnlineXrayImagesDir(imageSetName) + File.separator + getInspectionImageInfoProgramFileName();
  }
  
  public static String getInspectedComponentMagnificationInfoForOnlineTstDevelopMent(String imageSetName)
  {
    Assert.expect(imageSetName != null);
    return Directory.getOnlineXrayImagesDir(imageSetName) + File.separator + getComponentMagnificationInfoFileName();
  }
  
  public static String getInspectedComponentMagnificationInfoFullPath(String projectName,String imageSetName)
  {
    Assert.expect(imageSetName != null);
    return Directory.getXrayInspectionImagesDir(projectName, imageSetName) + File.separator + getComponentMagnificationInfoFileName();
  }

  /**
   * @author George A. David
   */
  public static String getAdjustImageNamesProgramFullPath(String projectName)
  {
    Assert.expect(projectName != null);

    return Directory.getAdjustFocusImagesDir(projectName) + File.separator + getAdjustFocusImageNamesProgramFileName();
  }

  /**
   * @author George A. David
   */
  public static String getImageSetInfoFullPath(String projectName,
                                               String imageSetName)
  {
    Assert.expect(projectName != null);
    Assert.expect(imageSetName != null);
    return Directory.getXrayInspectionImagesDir(projectName, imageSetName) + File.separator + getImageSetInfoFileName();
  }

  /**
   * @author George A. David
   */
  public static String getImageSetInfoFullPathForOnlineTestDevelopment(String imageSetName)
  {
    Assert.expect(imageSetName != null);
    return Directory.getOnlineXrayImagesDir(imageSetName) + File.separator + getImageSetInfoFileName();
  }

  /**
   * @author Vincent Wong
   */
  public static String getDisplayConfigFullPath()
  {
    return Directory.getCalibDir() + File.separator + _DISPLAY_CFG;
  }

  /**
   * @author Bill Darbie
   */
  public static String getResultsExtension()
  {
    return _RESULTS_EXTENSION;
  }

  /**
   * @author Laura Cormos
   */
  public static String getResultsFileName(String projName)
  {
    Assert.expect(projName != null);
    return projName + _RESULTS_EXTENSION;
  }

  /**
   * @author Laura Cormos
   */
  public static String getResultsFileFullPath(String projName, long inspectionStartTimeInMillis)
  {
    Assert.expect(projName != null);
    return Directory.getInspectionResultsDir(projName, inspectionStartTimeInMillis) + File.separator +
        getResultsFileName(projName);
  }

  /**
   * @author Laura Cormos
   */
  public static String getXMLResultsFileName(String projName)
  {
    Assert.expect(projName != null);
    return projName + _INDICTMENTS_EXTENSION + _XML_EXTENSION;
  }

  /**
   * @author Laura Cormos
   */
  public static String getXMLMeasurementsFileName(String projName)
  {
    Assert.expect(projName != null);
    return projName + _MEASUREMENTS_EXTENSION + _XML_EXTENSION;
  }

  /**
   * @author Laura Cormos
   */
  public static String getXMLResultsFileFullPath(String projName, long inspectionStartTimeInMillis)
  {
    Assert.expect(projName != null);
    return Directory.getInspectionResultsDir(projName, inspectionStartTimeInMillis) + File.separator +
        getXMLResultsFileName(projName);
  }

  /**
   * @author Laura Cormos
   */
  public static String getXMLMeasurementsFileFullPath(String projName, long inspectionStartTimeInMillis)
  {
    Assert.expect(projName != null);
    return Directory.getInspectionResultsDir(projName, inspectionStartTimeInMillis) + File.separator +
        getXMLMeasurementsFileName(projName);
  }

  /**
   * @author Laura Cormos
   */
  public static String getXMLTempResultsFileFullPath(String projName, long inspectionStartTimeInMillis)
  {
    Assert.expect(projName != null);
    return Directory.getInspectionResultsDir(projName, inspectionStartTimeInMillis) + File.separator +
        FileUtil.getTempFileName(getXMLResultsFileName(projName));
  }

  /**
   * @author Laura Cormos
   */
  public static String getXMLTempMeasurementsFileFullPath(String projName, long inspectionStartTimeInMillis)
  {
    Assert.expect(projName != null);
    return Directory.getInspectionResultsDir(projName, inspectionStartTimeInMillis) + File.separator +
        FileUtil.getTempFileName(getXMLMeasurementsFileName(projName));
  }

  /**
    * @author Laura Cormos
    */
   public static String getResultsXmlSchemaFileNameFullPath()
   {
     return (Directory.getXmlSchemaDir() + File.separator + _INDICTMENTS_XML_SCHEMA_FILE_NAME);
   }

   /**
    * @author Laura Cormos
    */
   public static String getResultsXmlSchemaForMRTOnlyFileNameFullPath()
   {
     return (Directory.getXmlSchemaDir() + File.separator + _INDICTMENTS_XML_FOR_MRT_ONLY_SCHEMA_FILE_NAME);
   }

   /**
    * @author Laura Cormos
    */
   public static String getResultsXmlSchemaInResultsDirFullPath(String projName, long inspectionStartTimeInMillis)
   {
     Assert.expect(projName != null);
     return (Directory.getInspectionResultsDir(projName, inspectionStartTimeInMillis) +
             File.separator +
             _INDICTMENTS_XML_SCHEMA_FILE_NAME);
  }

  /**
   * @author Laura Cormos
   */
  public static String getResultsXmlSchemaForMRTOnlyInResultsDirFullPath(String projName, long inspectionStartTimeInMillis)
  {
    Assert.expect(projName != null);
    return (Directory.getInspectionResultsDir(projName, inspectionStartTimeInMillis) +
            File.separator +
            _INDICTMENTS_XML_FOR_MRT_ONLY_SCHEMA_FILE_NAME);
  }
  /**
   * @author Laura Cormos
   */
  public static String getXMLBoardResultsFileName(String projName)
  {
    Assert.expect(projName != null);
    return projName + _BOARD_INDICTMENTS_EXTENSION + _XML_EXTENSION;
  }

  /**
   * @author Laura Cormos
   */
  public static String getXMLBoardMeasurementsFileName(String projName)
  {
    Assert.expect(projName != null);
    return projName + _BOARD_MEASUREMENTS_EXTENSION + _XML_EXTENSION;
  }


  /**
   * @author Laura Cormos
   */
  public static String getXMLBoardResultsFileFullPath(String boardName, String projName, long inspectionStartTimeInMillis)
  {
    Assert.expect(boardName != null);
    Assert.expect(projName != null);

    return Directory.getInspectionResultsDir(projName, inspectionStartTimeInMillis) + File.separator +
        projName + _BOARD_INDICTMENTS_EXTENSION + "_" + boardName + _XML_EXTENSION;
  }

  /**
   * @author Laura Cormos
   */
  public static String getXMLBoardMeasurementsFileFullPath(Board board, String projName, long inspectionStartTimeInMillis)
  {
    Assert.expect(board != null);
    Assert.expect(projName != null);

    return Directory.getInspectionResultsDir(projName, inspectionStartTimeInMillis) + File.separator +
        projName + _BOARD_MEASUREMENTS_EXTENSION + "_" + board.getName() + _XML_EXTENSION;
  }

  /**
   * @author Laura Cormos
   */
  public static String getXMLBoardResultsFileFullPath(String projName, long inspectionStartTimeInMillis)
  {
    Assert.expect(projName != null);
    return Directory.getInspectionResultsDir(projName, inspectionStartTimeInMillis) + File.separator +
        getXMLBoardResultsFileName(projName);
  }

  /**
   * @author Laura Cormos
   */
  public static String getXMLBoardMeasurementsFileFullPath(String projName, long inspectionStartTimeInMillis)
  {
    Assert.expect(projName != null);
    return Directory.getInspectionResultsDir(projName, inspectionStartTimeInMillis) + File.separator +
        getXMLBoardMeasurementsFileName(projName);
  }

  /**
   * @author Laura Cormos
   */
  public static String getInspectionIndictmentsXmlSchemaFileName()
  {
    return _INDICTMENTS_XML_SCHEMA_FILE_NAME;
  }

  /**
   * @author Laura Cormos
   */
  public static String getInspectionIndictmentsXmlSchemaForMRTOnlyFileName()
  {
    return _INDICTMENTS_XML_FOR_MRT_ONLY_SCHEMA_FILE_NAME;
  }

  /**
   * @author Seng-Yew Lim
   */
  public static String getDumpPlotInfoLogFullPath()//long inspectionStartTimeInMillis)
  {
    return Directory.getLogDir() + File.separator + _DUMP_PLOT_INFO_LOG_FILE + _COMMA_SEPARATED_VALUE_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getAlgorithmShortsLearningFileName()
  {
    return _ALGORITHM_SHORTS_LEARNING_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getAlgorithmShortsLearningFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmLearningDir(projName) + File.separator + getAlgorithmShortsLearningFileName();
  }

  /**
   * @author Bill Darbie
   */
  public static String getAlgorithmShortsLearningTempProjOpenFileName()
  {
    return _ALGORITHM_SHORTS_LEARNING_TEMP_PROJ_OPEN_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getAlgorithmShortsLearningTempProjOpenFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmLearningDir(projName) + File.separator + getAlgorithmShortsLearningTempProjOpenFileName();
  }

  /**
   * @author Bill Darbie
   */
  public static String getAlgorithmShortsLearningTempOpenFileName()
  {
    return _ALGORITHM_SHORTS_LEARNING_TEMP_OPEN_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getAlgorithmShortsLearningTempOpenFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmLearningDir(projName) + File.separator + getAlgorithmShortsLearningTempOpenFileName();
  }

  /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor - Broken Pin
   */
  public static String getAlgorithmBrokenPinLearningFileName()
  {
    return _ALGORITHM_BROKEN_PIN_LEARNING_FILE;
  }

  /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor - Broken Pin
   */
  public static String getAlgorithmBrokenPinLearningFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmLearningDir(projName) + File.separator + getAlgorithmBrokenPinLearningFileName();
  }
  
  /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor - Broken Pin
   */
  public static String getAlgorithmBrokenPinLearningTempProjOpenFileName()
  {
    return _ALGORITHM_BROKEN_PIN_LEARNING_TEMP_PROJ_OPEN_FILE;
  }
  
  /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor - Broken Pin
   */
  public static String getAlgorithmBrokenPinLearningTempProjOpenFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmLearningDir(projName) + File.separator + getAlgorithmBrokenPinLearningTempProjOpenFileName();
  }  
  
  /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor - Broken Pin
   */
  public static String getAlgorithmBrokenPinLearningTempOpenFileName()
  {
    return _ALGORITHM_BROKEN_PIN_LEARNING_TEMP_OPEN_FILE;
  }

  /**
   * @author Bill Darbie
   * @author Lim, Lay Ngor - Broken Pin
   */
  public static String getAlgorithmBrokenPinLearningTempOpenFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmLearningDir(projName) + File.separator + getAlgorithmBrokenPinLearningTempOpenFileName();
  }
  
  /**
   * @author Bill Darbie
   */
  public static String getAlgorithmSubtypesLearningFileName()
  {
    return _ALGORITHM_SUBTYPES_LEARNING_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getAlgorithmSubtypesLearningFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmLearningDir(projName) + File.separator + getAlgorithmSubtypesLearningFileName();
  }

  /**
   * @author Bill Darbie
   */
  public static String getAlgorithmSubtypesLearningTempProjOpenFileName()
  {
    return _ALGORITHM_SUBTYPES_LEARNING_TEMP_PROJ_OPEN_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getAlgorithmSubtypesLearningTempProjOpenFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmLearningDir(projName) + File.separator + getAlgorithmSubtypesLearningTempProjOpenFileName();
  }

  /**
   * @author Bill Darbie
   */
  public static String getAlgorithmSubtypesLearningTempOpenFileName()
  {
    return _ALGORITHM_SUBTYPES_LEARNING_TEMP_OPEN_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getAlgorithmSubtypesLearningTempOpenFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmLearningDir(projName) + File.separator + getAlgorithmSubtypesLearningTempOpenFileName();
  }

  /**
   * @author George Booth
   */
  public static String getAlgorithmExpectedImageLearningFileName()
  {
    return _ALGORITHM_EXPECTED_IMAGE_LEARNING_FILE;
  }

  /**
   * @author George Booth
   */
  public static String getAlgorithmExpectedImageLearningFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmLearningDir(projName) + File.separator + getAlgorithmExpectedImageLearningFileName();
  }

  /**
   * Clear Tobmstone
   * @author sheng chuan
   */
  public static String getAlgorithmExpectedImageTemplateLearningFileName()
  {
    return _ALGORITHM_EXPECTED_IMAGE_TEMPLATE_LEARNING_FILE;
  }

  /**
   * Clear Tobmstone
   * @author sheng chuan
   */
  public static String getAlgorithmExpectedImageTemplateLearningFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmTemplateLearningDir(projName) + File.separator + getAlgorithmExpectedImageTemplateLearningFileName();
  }

  /**
   * @author George Booth
   */
  public static String getAlgorithmExpectedImageLearningTempProjOpenFileName()
  {
    return _ALGORITHM_EXPECTED_IMAGE_LEARNING_TEMP_PROJ_OPEN_FILE;
  }

  /**
   * @author George Booth
   */
  public static String getAlgorithmExpectedImageLearningTempProjOpenFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmLearningDir(projName) + File.separator + getAlgorithmExpectedImageLearningTempProjOpenFileName();
  }

  /**
   * Clear Tobmstone
   * @author sheng-chuan
   */
  public static String getAlgorithmExpectedImageTemplateLearningTempProjOpenFileName()
  {
    return _ALGORITHM_EXPECTED_IMAGE_TEMPLATE_LEARNING_TEMP_PROJ_OPEN_FILE;
  }

  /**
   * Clear Tobmstone
   * @author  sheng-chuan
   */
  public static String getAlgorithmExpectedImageTemplateLearningTempProjOpenFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmTemplateLearningDir(projName) + File.separator + getAlgorithmExpectedImageTemplateLearningTempProjOpenFileName();
  }

  /**
   * @author George Booth
   */
  public static String getAlgorithmExpectedImageLearningTempOpenFileName()
  {
    return _ALGORITHM_EXPECTED_IMAGE_LEARNING_TEMP_OPEN_FILE;
  }

  /**
   * @author George Booth
   */
  public static String getAlgorithmExpectedImageLearningTempOpenFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmLearningDir(projName) + File.separator + getAlgorithmExpectedImageLearningTempOpenFileName();
  }

  /**
   * Clear Tobmstone
   * @author sheng chuan
   */
  public static String getAlgorithmExpectedImageTemplateLearningTempOpenFileName()
  {
    return _ALGORITHM_EXPECTED_IMAGE_TEMPLATE_LEARNING_TEMP_OPEN_FILE;
  }
  
  /**
   * Clear Tobmstone
   * @author sheng chuan
   */
  public static String getAlgorithmExpectedImageTemplateLearningTempOpenFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmTemplateLearningDir(projName) + File.separator + getAlgorithmExpectedImageTemplateLearningTempOpenFileName();
  }
  
  /**
   * Clear Tobmstone
   * @author sheng chuan
   */
  public static String getAlgorithmExpectedImageTemplateLearningAbsoultePath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmTemplateLearningDir(projName);
  }

  /**
   * @author George Booth
   */
  public static String getTextFileExtension()
  {
    Assert.expect(_TEXT_EXTENSION != null);
    return _TEXT_EXTENSION;
  }

  /**
   * @author George Booth
   */
  public static String getCommaSeparatedValueFileExtension()
  {
    Assert.expect(_COMMA_SEPARATED_VALUE_EXTENSION != null);
    return _COMMA_SEPARATED_VALUE_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getConfigFileExtension()
  {
    return _CONFIG_EXT;
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public static String getInitialRecipeDefaultSettingFileWithoutExtension()
  {
    return _DEFAULT_INITIAL_RECIPE_SETTING_CONFIG_FILE;
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public static List<String> getAllInitialRecipeSettingNames()
  {
    List<String> files = new ArrayList<String>(FileUtilAxi.listAllFilesInDirectory(Directory.getInitialRecipeSettingDir()));
    List<String> initialRecipeSettingFileNames = new ArrayList<String>();
    initialRecipeSettingFileNames.add(getInitialRecipeDefaultSettingFileWithoutExtension());
    for (String file : files)
    {
      if (file.endsWith(_CONFIG_EXT))
      {
        initialRecipeSettingFileNames.add(file.substring(0, file.length() - _CONFIG_EXT.length()));
      }
    }

    return initialRecipeSettingFileNames;
  }
  
  /**
   * XCR-2895: Initial recipes setting to speed up the programming time
   * 
   * @author weng-jian.eoh
   * @return 
   */
  public static String getInitialRecipeSettingFullPath(String fileName)
  {
    Assert.expect(fileName != null);
    return Directory.getInitialRecipeSettingDir() + File.separator + fileName + _CONFIG_EXT;
  }
  
  /**
   * @author Andy Mechtenberg
   */
   public static String getInitialThresholdsDefaultFileWithoutExtension()
   {
     return _DEFAULT_INITIAL_THRESHOLDS_CONFIG_FILE;
   }

   /**
   * @author Andy Mechtenberg
   */
   public static List<String> getAllInitialThresholdSetNames()
   {
     List<String> files = new ArrayList<String>(FileUtilAxi.listAllFilesInDirectory(Directory.getInitialThresholdsDir()));
     List<String> initialThresholdFileNames = new ArrayList<String>();
     initialThresholdFileNames.add(getInitialThresholdsDefaultFileWithoutExtension());
     for (String file : files)
     {
       if (file.endsWith(_CONFIG_EXT))
         initialThresholdFileNames.add(file.substring(0, file.length() - _CONFIG_EXT.length()));
     }

     return initialThresholdFileNames;
   }

  /**
   * @author Andy Mechtenberg
   */
  public static String getInitialThresholdsFullPath(String fileName)
  {
    Assert.expect(fileName != null);
    return Directory.getInitialThresholdsDir() + File.separator + fileName + _CONFIG_EXT;
  }

  /**
   * @author Bill Darbie
   */
  public static String getJointTypeAssignmentFullPath(String fileName)
  {
    Assert.expect(fileName != null);
    return Directory.getJointTypeAssignmentDir() + File.separator + fileName + _CONFIG_EXT;
  }

  /**
   * @author Bill Darbie
   */
  public static String getJointTypeAssignmentDefaultFile()
  {
    return _DEFAULT_JOINT_TYPE_ASSIGNMENT_CONFIG_FILE + _CONFIG_EXT;
  }

  /**
   * @author Bill Darbie
   */
  public static String getJointTypeAssignmentDefaultFileWithoutExtension()
  {
    return _DEFAULT_JOINT_TYPE_ASSIGNMENT_CONFIG_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static List<String> getJointTypeAssignmentFiles()
  {
    List<String> files = new ArrayList<String>(FileUtilAxi.listAllFilesInDirectory(Directory.getJointTypeAssignmentDir()));
    List<String> jointTypeFileNames = new ArrayList<String>();
    for (String file : files)
    {
      if (file.endsWith(_CONFIG_EXT))
        jointTypeFileNames.add(file.substring(0, file.length() - _CONFIG_EXT.length()));
    }

    return jointTypeFileNames;
  }

  /**
   * @author Laura Cormos
   */
  public static String getJointTypeAssignmentFileFullPath(String jointTypeAssignmentFileName)
  {
    Assert.expect(jointTypeAssignmentFileName != null);

    return Directory.getJointTypeAssignmentDir() + File.separator + jointTypeAssignmentFileName;
  }

  /**
   * Get a list of all the barcode reader configurations that currently exist.
   * A barcode reader configuration name is the same as the file name without
   * the configuration file extension.
   * @return a list of barcode reader configuration names.
   * @author Bob Balliew
   */
  public static List<String> getBarcodeReaderConfigurationNames()
  {
    List<String> files = new ArrayList<String>(FileUtilAxi.listAllFilesInDirectory(Directory.getBarcodeReaderDir()));
    List<String> barcodeReaderConfigFileNames = new ArrayList<String>();
    for (String file : files)
    {
      if (file.endsWith(_CONFIG_EXT))
        barcodeReaderConfigFileNames.add(file.substring(0, file.length() - _CONFIG_EXT.length()));
    }

    return barcodeReaderConfigFileNames;
  }

  /**
   * @author Bob Balliew
   */
  public static String getBarcodeReaderConfigurationFileName(String configName)
  {
    return configName + getConfigFileExtension();
  }
  
  public static String getBarcodeReaderConfigurationFullPath(String configName, SystemTypeEnum systemType)
  {
    return Directory.getBarcodeReaderDir(systemType) + File.separator + getBarcodeReaderConfigurationFileName(configName);
  }
  /**
   * @author Bob Balliew
   */
  public static String getBarcodeReaderConfigurationFullPath(String configName)
  {
    return Directory.getBarcodeReaderDir() + File.separator + getBarcodeReaderConfigurationFileName(configName);
  }
  
  /**
   * @author Siew Yeng
   */
  public static String getDefaultBarcodeReaderConfigurationFullPath()
  {
    return getBarcodeReaderConfigurationFullPath(_MS820_P2P);
  }

  /**
   * @author Laura Cormos
   */
  public static String getCadFileName(long checksum)
  {
    return (_CAD_FILE_NAME + _DOT + checksum + _XML_EXTENSION);
  }

  /**
   * @author Laura Cormos
   */
  public static String getCadFileNameFullPath(String projName, long checksum)
  {
    Assert.expect(projName != null);
    return (Directory.getProjectDir(projName) + File.separator + _CAD_FILE_NAME + _DOT + checksum + _XML_EXTENSION);
  }

  /**
   * @author Laura Cormos
   */
  public static String getCadFileNameInResultsDirFullPath(String projName, long checksum)
  {
    Assert.expect(projName != null);
    return (Directory.getResultsDir(projName) + File.separator + getCadFileName(checksum));
  }

  /**
   * @author Laura Cormos
   */
  public static String getCadXmlSchemaFileName()
  {
    return _CAD_XML_SCHEMA_FILE_NAME;
  }

  /**
   * @author Laura Cormos
   */
  public static String getCadXmlSchemaFileNameFullPath()
  {
    return (Directory.getXmlSchemaDir() + File.separator + _CAD_XML_SCHEMA_FILE_NAME);
  }

  /**
   * @author Laura Cormos
   */
  public static String getCadXmlSchemaInResultsDirFullPath(String projName, long inspectionStartTimeInMillis)
  {
    Assert.expect(projName != null);
    return (Directory.getInspectionResultsDir(projName, inspectionStartTimeInMillis) +
            File.separator +
            _CAD_XML_SCHEMA_FILE_NAME);
  }

  /**
   * @author Laura Cormos
   */
  public static String getSettingsFileNameInResultsDirFullPath(String projName, long checksum)
  {
    Assert.expect(projName != null);
    return (Directory.getResultsDir(projName) + File.separator + getSettingsFileName(checksum));
  }

  /**
   * @author Laura Cormos
   */
  public static String getSettingsFileNameFullPath(String projName, long checksum)
  {
    Assert.expect(projName != null);
    return (Directory.getProjectDir(projName) + File.separator + _SETTINGS_FILE_NAME + _DOT + checksum + _XML_EXTENSION);
  }

  /**
   * @author Laura Cormos
   */
  public static String getSettingsFileName(long checksum)
  {
    return (_SETTINGS_FILE_NAME + _DOT + checksum + _XML_EXTENSION);
  }

  /**
   * @author Laura Cormos
   */
  public static String getSettingsXmlSchemaFileName()
  {
    return _SETTINGS_XML_SCHEMA_FILE_NAME;
  }

  /**
   * @author Laura Cormos
   */
  public static String getSettingsXmlSchemaFileNameFullPath()
  {
    return (Directory.getXmlSchemaDir() + File.separator + _SETTINGS_XML_SCHEMA_FILE_NAME);
  }

  /**
   * @author Laura Cormos
   */
  public static String getSettingsXmlSchemaInResultsDirFullPath(String projName, long inspectionStartTimeInMillis)
  {
    Assert.expect(projName != null);
    return (Directory.getInspectionResultsDir(projName, inspectionStartTimeInMillis) +
            File.separator +
            _SETTINGS_XML_SCHEMA_FILE_NAME);
  }

  /**
   * @author Erica Wheatcroft
   */
  public static String getTestDevPersistFullPath()
  {
    return Directory.getGuiConfigDir() + File.separator + _TEST_DEV_PERSIST_FILE;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static String getTestExecPersistFullPath()
  {
    return Directory.getGuiConfigDir() + File.separator + _TEST_EXEC_PERSIST_FILE;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public static String getVirtualLivePersistFullPath()
  {
    return Directory.getGuiConfigDir() + File.separator + _VIRTUAL_LIVE_PERSIST;
  }
  /*
   * @author Kee Chin Seong
   */
  public static String getCadCreatorPersistFullPath()
  {
    return Directory.getGuiConfigDir() + File.separator + _CAD_CREATOR_PERSIST;
  }

  /**
   * @author Erica Wheatcroft
   */
  public static String getMainMenuPersistFullPath()
  {
    return Directory.getGuiConfigDir() + File.separator + _MAIN_MENU_PERSIST_FILE;
  }

  /**
   * @author Erica Wheatcroft
   */
  public static String getGuiStatePersistFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getProjectDir(projectName) + File.separator + _GUISTATE_PERSIST_FILE;
  }

  /**
   * @author Erica Wheatcroft
   */
  public static String getServiceUIStatePersistFullPath()
  {
    return Directory.getGuiConfigDir() + File.separator + _SERVICE_PERSIST_FILE;
  }

  /**
   * @author Laura Cormos
   */
  public static String getConfigUIStatePersistFullPath()
  {
    return Directory.getGuiConfigDir() + File.separator + _CONFIG_PERSIST_FILE;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static String getAlgorithmDiagnosticSettingPersistFullPath()
  {
    return Directory.getGuiConfigDir() + File.separator + _DIAGNOSTIC_SETTINGS_FILE;
  }

  /**
   * @author George Booth
   */
  public static String getUserAccountsFullPath()
  {
    return Directory.getUserAccountsDir() + File.separator + _USER_ACCOUNTS_MANAGER_FILE;
  }

  /**
   * @author George Booth
   */
  public static String getPerformanceLogFileNameFullPath()
  {
    return Directory.getPerformanceLogDir() + File.separator + _PERFORMANCE_LOG_FILE + _COMMA_SEPARATED_VALUE_EXTENSION;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getReviewMeasurementFileNameFullPath(String reportName, String recipeName, String subtypeName, String jointTypeName)
  {
    return reportName + File.separator + recipeName + "_" + subtypeName + "_" + jointTypeName + _COMMA_SEPARATED_VALUE_EXTENSION;
  }

  /**
   * @author George Booth
   */
  public static String getHelpFileFullPath()
  {
    return Directory.getHelpDir() + File.separator + _HELP_INDEX_FILE;
  }

  /**
   * @author George Booth
   */
  public static String getServiceHelpFileFullPath()
  {
    return Directory.getHelpDir() + File.separator + _SERVICE_HELP_INDEX_FILE;
  }

  /**
   * @author George Booth
   */
  public static String getOperatorHelpFileFullPath()
  {
    return Directory.getOperatorHelpDir() + File.separator + _HELP_INDEX_FILE;
  }

  /**
   * @author Laura Cormos
   */
  public static String getConcatenationIntermediateFileFullPath(String directoryName)
  {
    Assert.expect(directoryName != null);
    return directoryName + File.separator + FileUtil.getTempFileName(_INTERMEDIATE);
  }

  /**
   * @author Erica Wheatcroft
   */
  public static String getPointToPointMoveFileExtenstion()
  {
    return _POINT_TO_POINT_CONFIGURATION_FILE_EXTENSION;
  }

  /**
   * @author Erica Wheatcroft
   */
  public static String getScanMoveFileExtenstion()
  {
    return _SCAN_PATH_CONFIGURATION_FILE_EXTENSION;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPreInspectionScriptOutputDataFile()
  {
    return _PRE_INSPECTION_SCRIPT_OUTPUT_DATA_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPreInspectionScriptOutputDataFullPath()
  {
    return Directory.getInspectionDataDir() + File.separator + getPreInspectionScriptOutputDataFile();
  }

  /**
   * @author Bill Darbie
   */
  public static String getPreInspectionScriptInputDataFile()
  {
    return _PRE_INSPECTION_SCRIPT_INPUT_DATA_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPreInspectionScriptInputDataFullPath()
  {
    return Directory.getInspectionDataDir() + File.separator + getPreInspectionScriptInputDataFile();
  }

  /**
   * @author Bill Darbie
   */
  public static String getPostInspectionScriptInputDataFile()
  {
    return _POST_INSPECTION_SCRIPT_INPUT_DATA_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPostInspectionScriptDataFullPath()
  {
    return Directory.getInspectionDataDir() + File.separator + getPostInspectionScriptInputDataFile();
  }

  /**
   * @author Peter Esbensen
   */
  public static String getImageAnalysisCrashArchiveFullPath()
  {
    Calendar calendar = Calendar.getInstance();
    NumberFormat numberFormat = NumberFormat.getIntegerInstance(Locale.US);
    numberFormat.setMinimumIntegerDigits(2);
    return Directory.getLogDir() + File.separator + "inspectionException_" + calendar.get(Calendar.YEAR) + "_" +
        numberFormat.format(calendar.get(Calendar.MONTH) + 1) + "_" +
        numberFormat.format(calendar.get(Calendar.DATE)) + "_" +
        numberFormat.format(calendar.get(Calendar.HOUR_OF_DAY)) + "_" +
        numberFormat.format(calendar.get(Calendar.MINUTE)) + ".zip";
  }

  /**
   * @author Greg Esparza
   */
  public static String getCalibExtension()
  {
    return _CALIB_EXTENSION;
  }

  /**
   * @author Rex Shang
   */
  public static String getImageReconstructionProcessorApplicationFile()
  {
    return _IMAGE_RECONSTRUCTION_PROCESSOR_APPLICATION_NAME;
  }

  /**
   * This file is a tiny file used to clear out the destination files.
   * The real files are then uploaded.
   * @author Rex Shang
   */
  public static String getNearlyEmptyIrpUploadFile()
  {
    return _NEARLY_EMPTY_IRP_UPLOAD_FILE;
  }

  /**
   * @author Chong, Wei Chin
   */
  public static String getRemoteMotionServerApplicationFile()
  {
    return _REMOTE_MOTION_SERVER_APPLICATION_NAME;
  }

  /**
   * This file is a tiny file used to clear out the destination files.
   * The real files are then uploaded.
   * @author Chong, Wei Chin
   */
  public static String getNearlyEmptyUploadFile()
  {
    return _NEARLY_EMPTY_UPLOAD_FILE;
  }

  /**
   * @author Bill Darbie
   */
  public static String getFirstRunAfterInstallFullPath()
  {
    return Directory.getConfigDir() + File.separator + _FIRST_RUN_AFTER_INSTALL_FILE;
  }

  /**
   * @author Greg Esparza
   */
  public static String getAgilentTdiXrayCameraRuntimeZipFileNameFullPath()
  {
    return Directory.getXrayCameraConfigDir() +  _AXI_TDI_XRAY_CAMERA_CONFIG_DOWNLOAD_FILES_ZIP_FILE;
  }

  /**
   * @author Greg Esparza
   */
  public static String getAgilentXrayCameraRuntimeFileConfigFileName()
  {
    return _AXI_TDI_XRAY_CAMERA_RUNTIME_FILE_INFORMATION_CONFIG_FILE;
  }

  //Variable Mag Anthony August 2011
  /**
   * @author Anthony Fong
   */
  public static String getAgilentXrayCameraLowMagnificationFileConfigFileName()
  {
    return _AXI_TDI_XRAY_CAMERA_LOW_MAGNIFICATION_CONFIG_FILE ;
  }

  /**
   * @author Anthony Fong
   */
  public static String getAgilentXrayCameraHighMagnificationFileConfigFileName()
  {
    return _AXI_TDI_XRAY_CAMERA_HIGH_MAGNIFICATION_CONFIG_FILE;
  }

  /**
   * @author Greg Esparza
   */
  public static String getXrayCameraLogFileNameFullPath(int cameraId)
  {
    return Directory.getXrayCameraLogDir() + File.separator + "xRayCamera_" + String.valueOf(cameraId) + getLogFileExtension();
  }

  /**
   * @author Cheah Lee Herng
   */
  public static String getOpticalCameraLogFileNameFullPath(int cameraId)
  {
    return Directory.getOpticalCameraLogDir() + File.separator + "opticalCamera_" + String.valueOf(cameraId) + getLogFileExtension();
  }
  /**
   * @return String
   * @author Wei Chin
   */
  public static String getLibraryListFullPath()
  {
    return Directory.getConfigDir() + File.separator + _LIBRARY_LIST_FILE;
  }

  /**
   * @author Poh Kheng
   */
  public static int getProjectCompPackageSettingLatestFileVersion()
  {
    return 3;
  }

  /**
   * @author Poh Kheng
   */
  public static String getProjectCompPackageSettingFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_PACKAGE_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }

  /**
   * @author Poh Kheng
   */
  public static String getProjectCompPackageSettingFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectCompPackageSettingFile(version);
  }

  /**
   *
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public static String getPackageLibrarySearchPersistFullPath()
  {
    return Directory.getGuiConfigDir() + File.separator + _PACKAGE_LIBRARY_SEARCH_PERSIST_FILE;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getPackageLibraryImportOptionPersistFullPath()
  {
    return Directory.getGuiConfigDir() + File.separator + _PACKAGE_LIBRARY_IMPORT_OPTION_PERSIST_FILE;
  }

  /**
   *
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public static String getPackageLibraryDirectoryFullPath()
  {
    return Directory.getGuiConfigDir() + File.separator + _PACKAGE_LIBRARY_DIRECTORY_PERSIST_FILE;
  }

  /**
   *
   * @return String
   *
   * @author Tan Hock Zoon
   */
  public static String getPackageLibraryPanelSizePersistFullPath()
  {
    return Directory.getGuiConfigDir() + File.separator + _PACKAGE_LIBRARY_PANEL_SIZE_PERSIST_FILE;
  }
  
  /**   
   * @return 
   * 
   * @author Cheah Lee Herng
   */
  public static String getPackageLibraryGeneralPersistFullPath()
  {
    return Directory.getGuiConfigDir() + File.separator + _PACKAGE_LIBRARY_GENERAL_PERSIST_FILE;
  }

  /**
   * @author khang-shian.sham
   */
  public static String getGoodImagePathAndName(boolean topSide, int xCoordinate, int yCoordinate, int sliceId, String projectName)
  {

    String prefix;
    if (topSide)
    {
      prefix = _PANEL_TILE_TOP_PREFIX;
    }
    else
    {
      prefix = _PANEL_TILE_BOTTOM_PREFIX;
    }
    return Directory.getGoodImagesDir() + File.separator + projectName + File.separator + prefix + xCoordinate + "x" + yCoordinate + "_" + sliceId + _REPAIR_IMAGE_EXTENSION;
  }

  /**
   * @return String
   * @author Siew Yeng
   */
  public static String getPanelNdfFile()
  {
    return _PANEL_NDF_FILE;
  }
  
  /**
   * @return String
   * @author Siew Yeng
   */
  public static String getAlgSliceNdfFile()
  {
    return _ALG_SLICE_NDF_FILE;
  }
  
  /**
   * @return String
   * @author Siew Yeng
   */
  public static String getVersionNdfFile()
  {
    return _VERSION_NDF_FILE;
  }
  
  /**
   * @return String
   * @author Siew Yeng
   */
  public static String getPackageNdfFile()
  {
    return _PACKAGE_NDF_FILE;
  }
  
  /**
   * @return String
   * @author Siew Yeng
   */
  public static String getBoardNdfFile()
  {
    return _BOARD_NDF_FILE;
  }
  
  /**
   * @return String
   * @author Siew Yeng
   */
  public static String getComponentNdfFile()
  {
    return _COMPONENT_NDF_FILE;
  }
  
  /**
   * @return String
   * @author Siew Yeng
   */
  public static String getFovDataNdfFile()
  {
    return _FOV_DATA_NDF_FILE;
  }
  
  /**
   * @return String
   * @author Siew Yeng
   */
  public static String getSurfaceMountLandPatternNdfFile()
  {
    return _SURFACE_MOUNT_NDF_FILE;
  }
  
  /**
   * @return String
   * @author Siew Yeng
   */
  public static String getThroughHoleLandPatternNdfFile()
  {
    return _THROUGH_HOLE_NDF_FILE;
  }
  
  /**
   *
   * @return String
   *
   * @author Jack Hwee
   */
  public static String getPanelNameFile()
  {
    return _PANEL_NAME_FILE;
  }

   /**
   *
   * @return String
   *
   * @author Jack Hwee
   */
  public static String getRtfZipFileExtension()
  {
    return _RTF_ZIP_FILE_EXTENSION;
  }

    /**
   *
   * @return String
   *
   * @author sham khang shian
   */
  public static String getVirtualLiveJarFileName()
  {
    return _VIRTUALLIVE_JAR_FILE_NAME;
  }

  /**
   *  @return String 
   *  @author kee, chin seong
   */
  public static String getDefectPackagerExeFileName()
  {
    return _DEFECT_PACKAGER_EXE_FILE_NAME;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getDeleteFolderAndFileExeFullPath()
  {
    return Directory.getBinDir() + File.separator + _DELETE_FOLDER_AND_FILE_EXE_FILE_NAME;
  }

  /**
   *
   * @return String
   *
   * @author sham khang shian
   */
  public static String getVirtualLiveImageSetDataXMLFilePath(String projectName)
  {
    Assert.expect(projectName != null);

    return Directory.getXrayVirtualLiveImagesDir(projectName) + File.separator + VirtualLiveImageGeneration.getInstance().getImageSetName() + File.separator;
  }

  /**
   * @author Sham
   */
  public static ArrayList<String> getDontCopyList()
  {
    Assert.expect(_dontCopyList != null);

    return _dontCopyList;
  }

  /**
   * @author Sham
   */
  public static void setDontCopyList()
  {
    _dontCopyList.add(Directory.getPrevBarcodeReaderDir() + File.separator + _MS820_P2P + getConfigFileExtension());
    _dontCopyList.add(Directory.getPrevBarcodeReaderDir() + File.separator + _MSQUADRUSMINI_P2P + getConfigFileExtension());
    _dontCopyList.add(Directory.getPrevGuiImagesDir() + File.separator + _SMT_COMM_IMAGE_FILE);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalCameraLightImageFullPath(int cameraId, int cameraDeviceId, int sequenceNumber)
  {
    return Directory.getOpticalCameraLightConfirmationLogImageDir(cameraId) + File.separator + 
            "optical_camera_light_image_" + cameraId + "_" + cameraDeviceId + "_" + sequenceNumber +  _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalCalibrationReferencePlaneImageFullPath(int cameraId, int cameraDeviceId, int profileId, int sequenceNumber, int runIndex)
  {
    return Directory.getOpticalCalibrationCalLogDir() + File.separator + 
            "optical_camera_calibration_reference_plane_image_" + cameraId + "_" + cameraDeviceId + "_" + profileId + "_" + sequenceNumber + "_" + runIndex + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static String getLargerFovOpticalCalibrationReferencePlaneImageFullPath(int cameraId, int cameraDeviceId, int profileId, int sequenceNumber, int runIndex)
  {
    return Directory.getOpticalCalibrationCalLogDir() + File.separator + 
            "larger_fov_optical_camera_calibration_reference_plane_image_" + cameraId + "_" + cameraDeviceId + "_" + profileId + "_" + sequenceNumber + "_" + runIndex + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalCalibrationInitialPhaseValueTuningImageFullPath(int cameraId, int cameraDeviceId, int profileId, int sequenceNumber)
  {
    return Directory.getOpticalCalibrationCalLogDir() + File.separator + 
            "optical_camera_calibration_initial_phase_value_tuning_image_" + cameraId + "_" + cameraDeviceId + "_" + profileId + "_" + sequenceNumber + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalCalibrationObjectFringeImageFullPath(int cameraId, int cameraDeviceId, int profileId, int sequenceNumber, int runIndex)
  {
    return Directory.getOpticalCalibrationCalLogDir() + File.separator + 
            "optical_camera_calibration_object_fringe_image_" + cameraId + "_" + cameraDeviceId + "_" + profileId + "_" + sequenceNumber + "_" + runIndex + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalCalibrationObjectFringeMinusTwoImageFullPath(int cameraId, int cameraDeviceId, int profileId, int sequenceNumber, int runIndex)
  {
    return Directory.getOpticalCalibrationCalLogDir() + File.separator + 
            "optical_camera_calibration_object_fringe_minus_two_image_" + cameraId + "_" + cameraDeviceId + "_" + profileId + "_" + sequenceNumber + "_" + runIndex + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalCalibrationObjectFringePlusThreeImageFullPath(int cameraId, int cameraDeviceId, int profileId, int sequenceNumber, int runIndex)
  {
    return Directory.getOpticalCalibrationCalLogDir() + File.separator + 
            "optical_camera_calibration_object_fringe_plus_three_image_" + cameraId + "_" + cameraDeviceId + "_" + profileId + "_" + sequenceNumber + "_" + runIndex + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getLargerFovOpticalCalibrationObjectFringePlusThreeImageFullPath(int cameraId, int cameraDeviceId, int profileId, int sequenceNumber, int runIndex)
  {
    return Directory.getOpticalCalibrationCalLogDir() + File.separator + 
            "larger_fov_optical_camera_calibration_object_fringe_plus_three_image_" + cameraId + "_" + cameraDeviceId + "_" + profileId + "_" + sequenceNumber + "_" + runIndex + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalCalibrationObjectFringeMinusOneImageFullPath(int cameraId, int cameraDeviceId, int profileId, int sequenceNumber, int runIndex)
  {
    return Directory.getOpticalCalibrationCalLogDir() + File.separator + 
            "optical_camera_calibration_object_fringe_minus_one_image_" + cameraId + "_" + cameraDeviceId + "_" + profileId + "_" + sequenceNumber + "_" + runIndex + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalCalibrationObjectFringePlusFourImageFullPath(int cameraId, int cameraDeviceId, int profileId, int sequenceNumber, int runIndex)
  {
    return Directory.getOpticalCalibrationCalLogDir() + File.separator + 
            "optical_camera_calibration_object_fringe_plus_four_image_" + cameraId + "_" + cameraDeviceId + "_" + profileId + "_" + sequenceNumber + "_" + runIndex + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalReferencePlaneImageFullPath(int cameraId, int cameraDeviceId, int sequenceNumber)
  {
    return Directory.getOpticalReferencePlaneCalLogImageDir(cameraId) + File.separator + 
            "optical_reference_plane_image_" + cameraId + "_" + cameraDeviceId + "_" + sequenceNumber + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalMagnificationReferencePlaneImageFullPath(int cameraId, int cameraDeviceId, int planeId, int sequenceNumber)
  {
    return Directory.getOpticalMagnificationCalLogPlaneImageDir(cameraId, planeId) + File.separator + 
            "optical_camera_magnification_reference_plane_image_" + cameraId + "_" + cameraDeviceId + "_" + sequenceNumber + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalMagnificationSobelTempImageFullPath(int cameraId, int planeId)
  {
    return Directory.getOpticalMagnificationCalLogPlaneImageDir(cameraId, planeId) + File.separator + 
            "sobel_temp_image" + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalMagnificationObjectPlaneMinusTwoImageFullPath(int cameraId, int cameraDeviceId, int planeId, int sequenceNumber)
  {
    return Directory.getOpticalMagnificationCalLogPlaneImageDir(cameraId, planeId) + File.separator + 
            "optical_camera_magnification_object_plane_minus_two_image_" + cameraId + "_" + cameraDeviceId + "_" + sequenceNumber + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalMagnificationObjectPlanePlusThreeImageFullPath(int cameraId, int cameraDeviceId, int planeId, int sequenceNumber)
  {
    return Directory.getOpticalMagnificationCalLogPlaneImageDir(cameraId, planeId) + File.separator + 
            "optical_camera_magnification_object_plane_plus_three_image_" + cameraId + "_" + cameraDeviceId + "_" + sequenceNumber + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalMagnificationObjectPlaneMinusOneImageFullPath(int cameraId, int cameraDeviceId, int planeId, int sequenceNumber)
  {
    return Directory.getOpticalMagnificationCalLogPlaneImageDir(cameraId, planeId) + File.separator + 
            "optical_camera_magnification_object_plane_minus_one_image_" + cameraId + "_" + cameraDeviceId + "_" + sequenceNumber + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalMagnificationObjectPlanePlusFourImageFullPath(int cameraId, int cameraDeviceId, int planeId, int sequenceNumber)
  {
    return Directory.getOpticalMagnificationCalLogPlaneImageDir(cameraId, planeId) + File.separator + 
            "optical_camera_magnification_object_plane_plus_four_image_" + cameraId + "_" + cameraDeviceId + "_" + sequenceNumber + _INSPECTION_EXTENSION;    
  }
  
   /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalHeightReferencePlaneImageFullPath(int cameraId, int cameraDeviceId)
  {
    return Directory.getOpticalHeightCalLogDir() + File.separator + 
            "optical_camera_height_reference_plane_image_" + cameraId + "_" + cameraDeviceId + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalHeightObjectPlaneMinusTwoImageFullPath(int cameraId, int cameraDeviceId)
  {
    return Directory.getOpticalMagnificationCalLogDir() + File.separator + 
            "optical_camera_height_object_plane_minus_two_image_" + cameraId + "_" + cameraDeviceId + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static String getFrontOpticalCalibrationDataFullPath()
  {
    return Directory.getOpticalCalibrationProfileFrontModuleDir() + File.separator + "Calibration0" + _TEXT_EXTENSION;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static String getFrontOpticalCalibrationReferenceRawDataFullPath()
  {
    return Directory.getOpticalCalibrationProfileFrontModuleDir() + File.separator + "ReferenceRaw0" + _TEXT_EXTENSION;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static String getLargerFovFrontOpticalCalibrationDataFullPath()
  {
    return Directory.getLargerFovOpticalCalibrationProfileFrontModuleDir() + File.separator + "Calibration0" + _TEXT_EXTENSION;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static String getLargerFovFrontOpticalCalibrationReferenceRawDataFullPath()
  {
    return Directory.getLargerFovOpticalCalibrationProfileFrontModuleDir() + File.separator + "ReferenceRaw0" + _TEXT_EXTENSION;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static String getRearOpticalCalibrationDataFullPath()
  {
    return Directory.getOpticalCalibrationProfileRearModuleDir() + File.separator + "Calibration0" + _TEXT_EXTENSION;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static String getRearOpticalCalibrationReferenceRawDataFullPath()
  {
    return Directory.getOpticalCalibrationProfileRearModuleDir() + File.separator + "ReferenceRaw0" + _TEXT_EXTENSION;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static String getLargerFovRearOpticalCalibrationDataFullPath()
  {
    return Directory.getLargerFovOpticalCalibrationProfileRearModuleDir() + File.separator + "Calibration0" + _TEXT_EXTENSION;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static String getLargerFovRearOpticalCalibrationReferenceRawDataFullPath()
  {
    return Directory.getLargerFovOpticalCalibrationProfileRearModuleDir() + File.separator + "ReferenceRaw0" + _TEXT_EXTENSION;
  }
  
  /**
   * @author sham
   */
  public static String getProjectSubtypeAdvanceSettingsFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectSubtypeAdvanceSettingsFile(version);
  }

  /**
   * @author sham
   */
  public static String getProjectSubtypeAdvanceSettingsFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_SUBTYPE_ADVANCE_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }

  /**
   * @author Yong Sheng Chuan
   */
  public static String getProjectAutoExposureSettingsFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectAutoExposureSettingsFile(version);
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public static String getProjectAutoExposureSettingsFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_AUTO_EXPOSURE_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }
  
    /**
   * @author sham
   */
  public static int getProjectSubtypeAdvanceSettingsLatestFileVersion()
  {
    //change to version 3 by Siew Yeng, Dynamic Range Optimization
    // changed to 4 by sheng chuan for velocity configure feature
    return 4; //Change from 1 to 2 by Khang Wah, for new scan route, include 1.5, 5, 6, 7 into IL selection
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public static int getProjectAutoExposureSettingsLatestFileVersion()
  {
    return 1;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalHeightObjectPlanePlusThreeImageFullPath(int cameraId, int cameraDeviceId)
  {
    return Directory.getOpticalMagnificationCalLogDir() + File.separator + 
            "optical_camera_height_object_plane_plus_three_image_" + cameraId + "_" + cameraDeviceId + _INSPECTION_EXTENSION;    
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getOpticalImageFullPath(String projectName, String opticalImageSetName, String opticalImageName, int sequence)
  {
      Assert.expect(projectName != null);
      Assert.expect(opticalImageSetName != null);
      Assert.expect(opticalImageName != null);
      
      return Directory.getOpticalImagesDir(projectName, opticalImageSetName) + File.separator + opticalImageName + "_" + sequence + _INSPECTION_EXTENSION;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getOfflineOpticalImageFullPath(String projectName, String opticalImageName, int sequence)
  {
    Assert.expect(projectName != null);
    Assert.expect(opticalImageName != null);
    
    return Directory.getOfflineOpticalImagesDir(projectName) + File.separator + opticalImageName + "_" + sequence + _INSPECTION_EXTENSION;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getAlignmentOpticalImageFullPath(String opticalImageName, int sequence)
  {
      Assert.expect(opticalImageName != null);
      
      return Directory.getAlignmentOpticalImagesDir() + File.separator + opticalImageName + "_" + sequence + _INSPECTION_EXTENSION;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static String getPSPColourMapBarImageFullPath()
  {
    return Directory.getAlignmentOpticalImagesDir() + File.separator + _PSP_COLOURMAP_BAR;
  }

  
  /**
   * @author sham
   */
  public static String getVirtualLiveImageNamePrefix()
  {
    return _VIRTUALLIVE_IMAGE_NAME_PREFIX;
  }

  /**
   * @author sham khang shian
   */
  public static String getVirtualLiveOfflineImageSetDataXMLFile(String projectName)
  {
    Assert.expect(projectName != null);

    return VirtualLiveReconstructedImagesData.getInstance().getImageSetDir() + File.separator + _VIRTUALLIVE_IMAGE_SET_DATA_XML_FILENAME;
  }

  /**
   * @author sham
   */
  public static String getVirtualLiveImageSetDataXMLFileName()
  {
    return _VIRTUALLIVE_IMAGE_SET_DATA_XML_FILENAME;
  }
  
  /**
   * @author sham
   */
  public static String getVirtualLiveImageName(String reconstructionRegionName)
  {
    Assert.expect(reconstructionRegionName != null);

    return reconstructionRegionName + _VIRTUALLIVE_IMAGE_EXTENSION;
  }
  
  /**
   * @author sham
   */
  public static String getVirtualLiveTiffImageName(String reconstructionRegionName)
  {
    Assert.expect(reconstructionRegionName != null);

    return reconstructionRegionName + _VIRTUALLIVE_TIFF_IMAGE_EXTENSION;
  }
  
  /**
   * @author sham
   */
  public static String getVirtualLiveImageExtension()
  {
    return  _VIRTUALLIVE_IMAGE_EXTENSION;
  }
  
  /**
   * @author sham
   */
  public static String getVirtualLiveTiffImageExtension()
  {
    return _VIRTUALLIVE_TIFF_IMAGE_EXTENSION;
  }
  
  /**
   *
   * @return String
   *
   * @author sham khang shian
   */
  public static String getVirtualLiveImageSetDataXMLFile(String projectName, String imageSetName)
  {
    Assert.expect(projectName != null);

    return Directory.getXrayVirtualLiveImagesDir(projectName) + File.separator + imageSetName + File.separator + "ImageSetData.xml";
  }

  
  /**
   * @author Jack Hwee
   */
  public static String getFalseCallTriggeringConfigFullPath()
  {
    return Directory.getConfigDir() + File.separator + _FALSE_CALL_TRIGGERING_CONFIG_FILE;
  }
  
  public static String getFalseCallTriggeringConfigFullPath(SystemTypeEnum systemtype)
  {
    return Directory.getConfigDirWithSystemType(systemtype) + File.separator + _FALSE_CALL_TRIGGERING_CONFIG_FILE;
  }
  
  /**
   * @author weng-jian.eoh
   * @return 
   * 
   * XCR-3589 Combo STD and XXL software GUI
   */
  public static String getFalseCallTriggeringConfigFile()
  {
    return _FALSE_CALL_TRIGGERING_CONFIG_FILE;
  }

  /**
   * @author Wei Chin
   */
  public static String getProductionTuneSettingsFullPath(int version)
  {
    return Directory.getConfigDir() + File.separator + _PRODUCTION_TUNE_SETTINGS + _DOT + version + _SETTINGS_FILE_NAME;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public static String getMergedBoardProjectName(String projectName)
  {
    return projectName + _MERGED_BOARD_SUFFIX;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public static String getSemiAutomatedProjectName(String projectName)
  {
    return projectName + _SEMI_AUTO_RECIPE_SUFFIX;
  }
  
  /**
   * Project's history log CR
   * @author hsia-fen.tan
   */
  
  public static String getHistoryExtension()
  {
    return _HISTORY_EXTENSION;
  }

  public static String getHistoryFileName(String projName)
  {
    SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd_kk-mm-ss-SSS");
    Assert.expect(projName != null);
    return projName+"-"+ dateTimeFormatter.format(new Date())+ _HISTORY_EXTENSION;
  }
  
  public static String getProjectHistoryLogFileFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getHistoryFileDir(projName) + File.separator + getHistoryFileName(projName);
  }
  
  
  /**
   * @return a List of Strings of the history files that exist in the directory passed in.
   * @author hsia-fen.tan
   */
  private static List<String> getHistoryFileNames(String directory)
  {
    Assert.expect(directory != null);

    List<String> projectNames = new ArrayList<String>();

    File dirNameFile = new File(directory);
    File[] files = dirNameFile.listFiles();
    if (files != null)
    {
      for (int i = 0; i < files.length; ++i)
      {
        if (files[i].isFile())
        {
          String projectNameFile = files[i].getAbsolutePath();
          String projectHistoryName= files[i].getName();
          
          try
          {
            FileInputStream fis = new FileInputStream(projectNameFile);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader is = new BufferedReader(isr);

            if (FileUtil.exists(projectNameFile) == true)
            {
              projectNames.add(projectHistoryName);
            }
           is.close();
          }
          catch (FileNotFoundException fnfe)
          {
            // do nothing
            //throw new FileNotFoundDatastoreException(projectNameFile);
          }
           catch (IOException ioe)
          {
            // do nothing
            //throw new DatastoreException(projectNameFile);
          }
        }
      }
    }

    Collections.sort(projectNames, new CaseInsensitiveStringComparator());

    return projectNames;
  }
    
    /**
   * @return a List of Strings of project names in the History directory.
   * @author hsia-fen.tan
   */
  public static List<String> getHistoryFiles(String projName)
  {
    // look under all the directories in the History directories 
    // and read out its contents - it has the actual name in it
    return getHistoryFileNames(Directory.getHistoryFileDir(projName));
  }
  
  /**
   * @author Goh Bee Hoon
   */
  public static String getRecipeErrorLogFileNameFullPath(String projectName)
  {
    return Directory.getProjectDir(projectName) + File.separator + _RECIPE_ERROR_LOG + getLogFileExtension();
  }
   
  /**
   * @author Phang Siew Yeng
   */
  public static String getSerialNumberMappingSettingsFullPath(String projectName)
  {
    return Directory.getProjectDir(projectName) + File.separator + _PROJECT_SERIAL_NUMBER_MAPPING_SETTINGS_FILE +_SETTINGS_EXTENSION;
  }
  
   /**
   * @author Phang Siew Yeng
   */
  public static String getTempPanelImageFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getProjectTempDir(projectName) + File.separator + _PANEL_IMAGE_FILE;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public static String getTempPanelBoardImageFullPath(String projectName, String boardName)
  {
    Assert.expect(projectName != null);
    return Directory.getProjectTempDir(projectName) + File.separator + _BOARD_IMAGE_FILE + boardName + _BOARD_IMAGE_FILE_EXT;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public static String getTempPanelFlippedImageFullPath(String projectName)
  {
    Assert.expect(projectName != null);
    return Directory.getProjectTempDir(projectName) + File.separator + _PANEL_FLIPPED_IMAGE_FILE;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public static String getTempPanelBoardFlippedImageFullPath(String projectName, String boardName)
  {
    Assert.expect(projectName != null);
    return Directory.getProjectTempDir(projectName) + File.separator + _BOARD_FLIPPED_IMAGE_FILE + boardName + _BOARD_IMAGE_FILE_EXT;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public static java.awt.Image getTempPanelImage(String projectName, boolean flipped)
  {
    Assert.expect(projectName != null);
    String photoImageName = getPanelPhotoImageFullPath(projectName);
    File photoFile = new File(photoImageName);
    if (photoFile.exists())
    {
      try
      {
        return ImageIO.read(photoFile);
      }
      catch (Exception ex)
      {
        // do nothing, let it try the panelImage
      }
    }
    String panelImageName;
    if (flipped)
      panelImageName = FileName.getTempPanelFlippedImageFullPath(projectName);
    else
      panelImageName = FileName.getTempPanelImageFullPath(projectName);

    File panelFile = new File(panelImageName);
    if (panelFile.exists())
    {
      try
      {
        return ImageIO.read(panelFile);
      }
      catch (IOException iox)
      {
        return Image5DX.getImage(Image5DX.MM_NO_PANEL);
      }
    }
    return Image5DX.getImage(Image5DX.MM_NO_PANEL);
  }

   /**
    * @return the contents of "photo.jpg" if it exists, otherwise return
    * the contents of "panel_boardName.jpg", otherwise an image of "No Panel Image"
    * Photo.jpg -- a user supplied digital photo image
    * Panel_boardName.jpg -- a system generated line drawing image of the panel 
    * All image files are assumed to reside in the hashed NDF panel level directory
    * @author Phang Siew Yeng
   */
  public static java.awt.Image getTempPanelImage(String projectName, String boardName, boolean flipped)
  {
    Assert.expect(projectName != null);
    String photoImageName = getPanelPhotoImageFullPath(projectName);
    File photoFile = new File(photoImageName);
    if (photoFile.exists())
    {
      try
      {
        return ImageIO.read(photoFile);
      }
      catch (Exception ex)
      {
        // do nothing, let it try the panelImage
      }
    }
    String panelImageName;
    if (flipped)
      panelImageName = FileName.getTempPanelBoardFlippedImageFullPath(projectName, boardName);
    else
      panelImageName = FileName.getTempPanelBoardImageFullPath(projectName, boardName);

    File panelFile = new File(panelImageName);
    if (panelFile.exists())
    {
      try
      {
        return ImageIO.read(panelFile);
      }
      catch (IOException iox)
      {
        return Image5DX.getImage(Image5DX.MM_NO_PANEL);
      }
    }
    return Image5DX.getImage(Image5DX.MM_NO_PANEL);
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public static boolean hasTempPanelImage(String projectName, boolean flipped)
  {
    Assert.expect(projectName != null);
    String photoImageName = getPanelPhotoImageFullPath(projectName);
    File photoFile = new File(photoImageName);
    if (photoFile.exists())
    {
      return true;
    }
    String panelImageName;
    if (flipped)
      panelImageName = FileName.getTempPanelFlippedImageFullPath(projectName);
    else
      panelImageName = FileName.getTempPanelImageFullPath(projectName);
    File panelFile = new File(panelImageName);
    if (panelFile.exists())
    {
      return true;
    }
    return false;
  }
  
  /**
   * @author Chnee Khang Wah, Real Time PSH Error handling
   */
  public static String getProjectIsolatedPSHRegionCSVFile()
  {
    return _PROJECT_ISOLATED_REGION_CSV_FILE + _COMMA_SEPARATED_VALUE_EXTENSION;
  }

  /**
   * @author Chnee Khang Wah, Real Time PSH Error handling
   */
  public static String getProjectIsolatedPSHRegionCSVFullPath(String projectName)
  {
    Assert.expect(projectName != null);

    return Directory.getProjectDir(projectName) + File.separator + getProjectIsolatedPSHRegionCSVFile();
  }
  
    /*
   * @author swee-yee.wong
   */
  public static String getCameraCommandRepliesLogFullPath()
  {
    return Directory.getLogDir() + File.separator + _CAMERA_COMMAND_REPLIES_LOG_FILE;
  }
  
  /*
   * @author bee-hoon.goh
   */
  public static String getErrorHandlerLogFullPath()
  {
    return Directory.getLogDir() + File.separator + _ERROR_HANDLER_LOG_FILE;
  }
  
  /*
   * Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
   * @author swee-yee.wong
   */
  public static String getIaeCameraCommandLogFullPath()
  {
    return Directory.getLogDir() + File.separator + _IAE_CAMERA_COMMAND_LOG_FILE;
  }
  
  /*
   * Swee Yee Wong - XCR-3273 Insufficient trigger error when run motion repeatability confirmation for M23
   * @author swee-yee.wong
   */
  public static String getIaeStageCommandLogFullPath()
  {
    return Directory.getLogDir() + File.separator + _IAE_STAGE_COMMAND_LOG_FILE;
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public static String getCAMXOutputFilePath(String CAMXEventEnum, String timeStamp)
  {
    Assert.expect(CAMXEventEnum != null);
    Assert.expect(timeStamp != null);
    
    return Directory.getCAMXDir() + File.separator + "log" + File.separator + "camx"+ File.separator + timeStamp + "_" + CAMXEventEnum + ".xml";
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public static String getCAMXProgramPath()
  {    
    return Directory.getCAMXDir() + File.separator + "CAMX.exe";
  }
  
  /**
   * @author Anthony Fong
   */
  public static String getCDNABenchmarkLongScanPathFileName()
  {    
    return _CDNA_BENCHMARK_LONG_SCANPATH_FILE;
  }
  
  /**
   * @author Anthony Fong
   */
  public static String getJsonFileExtension()
  {  
    Assert.expect(_JSON_EXTENSION != null);  
    return _JSON_EXTENSION;
  } 
  
  /**
   * @author Khaw Chek Hau 
   * @XCR2923 : Machine Utilization Report Tools Implementation
   */
  public static String getMachineUtilizationReportFullPath()
  {
    return Directory.getMachineUtilizationReportDir() + File.separator + getMachineUtilizationReportFile();
  }
  
  /**
   * @author Khaw Chek Hau 
   * @XCR2923 : Machine Utilization Report Tools Implementation
   */
  public static String getMachineUtilizationReportFile()
  {
    return _MACHINE_UTILIZATION_REPORT_FILE + _COMMA_SEPARATED_VALUE_EXTENSION;
  }
    
  /**
   * @author Khaw Chek Hau 
   * @XCR2923 : Machine Utilization Report Tools Implementation
   */
  public static String getMachineUtilizationSummaryFullPath()
  {
    return Directory.getMachineUtilizationReportDir() + File.separator + getMachineUtilizationSummaryFile();
  }
  
  /**
   * @author Khaw Chek Hau 
   * @XCR2923 : Machine Utilization Report Tools Implementation
   */
  public static String getMachineUtilizationSummaryFile()
  {
    return _MACHINE_UTILIZATION_SUMMARY_FILE + _COMMA_SEPARATED_VALUE_EXTENSION;
  }   
 
  /*
   * @author swee-yee.wong
   */
  public static String getHardwareStatusLogFullPath()
  {
    return Directory.getLogDir() + File.separator + _HARDWARE_STATUS_LOG_FILE;
  }
  
  /**
   * Old mask image
   * @author Siew Yeng
   */
  public static String getMaskImageFullPath(String projName, String subtypeShortName)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmLearningDir(projName) + File.separator + _MASK_IMAGE_PREFIX + subtypeShortName + _MASK_IMAGE_EXTENSION;
  }
  
  /**
   * New mask image with rotation
   * @author Siew Yeng 
   */
  public static String getMaskImageWithRotationFullPath(String projName, String subtypeShortName, double rotation)
  {
    Assert.expect(projName != null);
    return Directory.getAlgorithmLearningDir(projName) + File.separator + _MASK_IMAGE_PREFIX + subtypeShortName + _MASK_IMAGE_ROTATION_SEPARATOR + rotation + _MASK_IMAGE_EXTENSION;
  }
  
  /**
   * @author Siew Yeng 
   */
  public static String getMaskImageExtension()
  {
    return _MASK_IMAGE_EXTENSION;
  }
  
  /**
   * @author Siew Yeng 
   */
  public static String getMaskImageRotationSeparator()
  {
    return _MASK_IMAGE_ROTATION_SEPARATOR;
  }
  
  /**
   * @author Siew Yeng
   */
  public static String getMaskImagePatternString(String subtypeShortName)
  {
    //Siew Yeng - XCR-3344 - handle subtype name with escape character
    return _MASK_IMAGE_PREFIX + java.util.regex.Pattern.quote(subtypeShortName) + _MASK_IMAGE_ROTATION_SEPARATOR + "(\\d+(.\\d+))" + _MASK_IMAGE_EXTENSION;
  }
  
  /**
   * @author Swee yee - new thickness table implementation
   */
  public static int getLatestThicknessTableVersionFound()
  {
    boolean isThicknessTableExist;
    List<String> solderThicknessLayer1DirFullList = Directory.getSolderThicknessLayer1DirFullList();
    List<String> solderThicknessLayer2DirFullList = Directory.getSolderThicknessLayer2DirFullList();
    for (int version = Config.getInstance().getIntValue(SoftwareConfigEnum.THICKNESS_TABLE_LATEST_VERSION); version >= 0; version--)
    {
      isThicknessTableExist = true;
      for (SolderThicknessEnum solderThicknessEnum : SolderThicknessEnum.getFullEnumList())
      {
        for (String layer1Dir : solderThicknessLayer1DirFullList)
        {
          for (String layer2Dir : solderThicknessLayer2DirFullList)
          {
            String solderThicknessFilePathInConfig = FileName.getCalibThicknessTableFullPath(layer1Dir, layer2Dir, solderThicknessEnum.getFileName(), version);

            if (FileUtil.exists(solderThicknessFilePathInConfig) == false)
            {
              isThicknessTableExist = false;
              break;
            }
          }
          if(isThicknessTableExist == false)
            break;
        }
        if(isThicknessTableExist == false)
            break;
      }
      if(isThicknessTableExist)
        return version;
    }
    return 0;
  }
  
  /**
   * @author Khaw Chek Hau 
   * @XCR2923 : Machine Utilization Report Tools Implementation
   */
  public static String getErrorTimeLogFullPath()
  {
    return Directory.getLogDir() + File.separator + getErrorTimeLogFile();
  }
  
  /**
   * @author Khaw Chek Hau 
   * @XCR2923 : Machine Utilization Report Tools Implementation
   */
  public static String getErrorTimeLogFile()
  {
    return _ERROR_TIME_LOG_FILE + _LOGFILE_EXTENSION;
  }
  
  /**
   * @author Khaw Chek Hau 
   * @XCR2923 : Machine Utilization Report Tools Implementation
   */
  public static String getMachineUtilizationBackupReportFile(String timeStamp)
  {
    Assert.expect(timeStamp != null);
    
    return _MACHINE_UTILIZATION_REPORT_FILE + timeStamp + _COMMA_SEPARATED_VALUE_EXTENSION;
  }
  
  /**
   * @author Khaw Chek Hau 
   * @XCR2923 : Machine Utilization Report Tools Implementation
   */
  public static String getMachineUtilizationBackupReportFullPath(String timeStamp)
  {
    return Directory.getMachineUtilizationBackupReportDir() + File.separator + getMachineUtilizationBackupReportFile(timeStamp);
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public static String get2DAlignmentImageFileName(String magnification, 
                                                   String groupName, 
                                                   int expectedMinX,
                                                   int expectedMinY,
                                                   int width,
                                                   int height,
                                                   int actualMinX,
                                                   int actualMinY)
  {    
    Assert.expect(magnification != null);
    Assert.expect(groupName != null);
    
    String alignment2DFileName = "DraggedImage_" + magnification + "_G" + groupName + "_" + 
                                 Integer.toString(expectedMinX) + "_" + Integer.toString(expectedMinY) + "_" +
                                 Integer.toString(width) + "_" + Integer.toString(height) + "_" + Integer.toString(actualMinX) + "_" + 
                                 Integer.toString(actualMinY) + "_.png";
    
    return alignment2DFileName;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public static String getAlignment2DImageFileNamePattern()
  {
    return _ALIGNMENT_2D_IMAGE_FILE_NAME_PATTERN;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3348: VOne Machine Utilization Feature
   */
  public static int getVOneMachineStatusMonitoringLogLatestFileVersion()
  {
    return 1;
  }
  
  /**
   * XCR-3116 Intermittent board chop during production looping using SMEMA
   * @param timeStampInMillis Smema Log timestamp
   * @return String Smema Log full path
   * @author Cheah Lee Herng
   */
  public static String getSmemaLogFileNameFullPath(long timeStampInMillis)
  {
    SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd_kk-mm-ss-SSS");
    return Directory.getLogDir() + File.separator + _SMEMA_LOG_FILE + "_" + dateTimeFormatter.format(new Date(timeStampInMillis)) + _LOGFILE_EXTENSION;
  }
  
  /**
   * XCR-3535 Log SMEMA Conveyor Signal
   * @author Kok Chun, Tan
   */
  public static String getSmemaConveyorSignalLogFileNameFullPath(long timeStampInMillis)
  {
    SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd_kk-mm-ss-SSS");
    return Directory.getLogDir() + File.separator + _SMEMA_CONVEYOR_SIGNAL_LOG_FILE + "_" + dateTimeFormatter.format(new Date(timeStampInMillis)) + _LOGFILE_EXTENSION;
  }
  
  /**
   * XCR-3345 Intermittent failed to open v810 application after patch installation
   * @author Cheah Lee Herng
   */
  public static String getPrevGuiImagesThumbsDBFullPath()
  {
    return Directory.getPrevGuiImagesDir() + File.separator + _THUMBS_DB_FILE;
  }
  
  /**
   * XCR-3735 Failed to open v810 application after patch installation due to thumbs.db lock it
   * @author Cheah Lee Herng
   */
  public static String getPrevJointThumbnailsThumbsDBFullPath()
  {
    return Directory.getPrevJointThumbnailsDir() + File.separator + _THUMBS_DB_FILE;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static String getRunXrayTesterFullPath()
  {
    return Directory.getBinDir() + File.separator + _RUN_XRAY_TESTER_FILE_NAME;
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public static String getFineTuningFileName(String projName)
  {
    SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd_kk-mm-ss-SSS");
    Assert.expect(projName != null);
    
    return projName + "-" + dateTimeFormatter.format(new Date()) + _COMMA_SEPARATED_VALUE_EXTENSION;
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public static String getProjectFineTuningFileFullPath(String projName)
  {
    Assert.expect(projName != null);
    return Directory.getFineTuningLogDir(projName) + File.separator + getFineTuningFileName(projName);
  }
  
  /**
   * @author weng-jian.eoh
   */
  public static String getRevertRecipeFileFullPath()
  {
    return Directory.getJreLibExt()+ File.separator + _REVERT_RECIPE_FILE_NAME;
  }
  
  
  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   * @param configName
   * @return
   */
  public static String getCustomizeConfigurationFileName(String configName)
  {
    return configName + getConfigFileExtension();
  }
  
   public static String getCustomizeConfigurationFullPath(String configName, SystemTypeEnum systemtype)
  {
    return Directory.getCustomizeSettingDir(systemtype) + File.separator + getCustomizeConfigurationFileName(configName);
  }

  /**
   * XCR-3436
   *
   * @author weng-jian.eoh
   * @param configName
   * @return
   */
  public static String getCustomizeConfigurationFullPath(String configName)
  {
    return Directory.getCustomizeSettingDir() + File.separator + getCustomizeConfigurationFileName(configName);
  }
  
  /**
   * XCR -3551
   * 
   * @author Janan Wong
   * @return custom measurement config file path.
   */
  public static String getCustomMeasurementConfigurationFullPath()
  {
    return Directory.getCustomMeasurementXMLSettingDir() + File.separator + _CUSTOM_MEASUREMENT_XML_CONFIG_FILE;
  }
  
  /**
   * For import/export landpattern
   * version 2 - added pad hole width, hole length(remove hole diameter)
   * @author Siew Yeng
   */
  public static int getLandPatternLatestCsvVersion()
  {
    return 2;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public static int getSECSGEMLogLatestFileVersion()
  {
    return 1;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public static String getSECSGEMLogFilePath()
  {    
    return Directory.getSECSGEMDir()+ File.separator + _SECS_GEM_LOG_FILE_NAME;
  }
  
  public static String getPreInspectionScriptInputConfigFullPath()
  {
    return Directory.getInspectionDataDir()+File.separator+"inspectionDataInputCustomizeList.config";
  }
  
  public static String getPreInspectionScriptOutputConfigFullPath()
  {
    return Directory.getInspectionDataDir()+File.separator+"inspectionDataOutputCustomizeList.config";
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public static int getProjectPackageOnPackageLatestFileVersion()
  {
    return 1;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public static String getProjectPackageOnPackageFile(int version)
  {
    Assert.expect(version > 0);

    return _PROJECT_PACKAGE_ON_PACKAGE_FILE + "." + version + _SETTINGS_EXTENSION;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3554: Package on package (PoP) development
   */
  public static String getProjectPackageOnPackageFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getProjectPackageOnPackageFile(version);
  }
  
  public static String getComponentMagnificationInfoFullPath(String projectName,long inspectionStartTime)
  {
    Assert.expect(projectName != null);
    Assert.expect(inspectionStartTime > 0);
    
    return Directory.getInspectionResultsDir(projectName, inspectionStartTime) + File.separator + getComponentMagnificationInfoFileName();
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3670: V-ONE AXI latest status monitoring log
   */
  public static int getMachineLatestStatusLogLatestFileVersion()
  {
    return 1;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3670: V-ONE AXI latest status monitoring log
   */
  public static String getMachineLatestStatusLogFullPath(int version)
  {
    return Directory.getVOneMachineStatusMonitoringReportDir() + File.separator + _MACHINE_LATEST_STATUS_LOG_FILE + "." + version + _LOGFILE_EXTENSION;
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public static String getProjectMeasurementJointHighlightSettingFullPath(String projectName)
  {
    return Directory.getProjectDir(projectName) + File.separator + getProjectMeasurementJoingHighlightSettingFile();
  }

  /**
   * XCR-3771 Highlight Defective Pin in Fine Tuning Graph
   *
   * @author weng-jian.eoh
   */
  public static String getProjectMeasurementJoingHighlightSettingFile()
  {
    return _PROJECT_DEFECTIVE_JOINT_SETTING_FILE + ".1" + _SETTINGS_EXTENSION;
  }
  
  /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public static String getNdfConverterFilePath()
  {
    return Directory.getJreLibExt()+ File.separator + _NDF_CONVERTER_FILE_NAME;
  }
  
  /**
   * XCR-3780 Additional Surface Map Info log
   * @author Cheah Lee Herng
   */
  public static String getSurfaceMapInfoLogFullPath(String projectName, String logName)
  {
    return Directory.getSurfaceMapInfoDir(projectName) + File.separator + logName + "." + _LOGFILE_EXTENSION;
  }
  
  /**
   * @author Siew Yeng
   */
  public static String getProjectPshSettingsFullPath(String projectName, int version)
  {
    Assert.expect(projectName != null);
    Assert.expect(version > 0);
    return Directory.getProjectDir(projectName) + File.separator + getPshSettingsFile(version);
  }
  
  /**
   * @author Siew Yeng
   */
  public static String getPshSettingsFile(int version)
  {
    Assert.expect(version > 0);
    return _PROJECT_PSH_SURFACE_MODEL_SETTINGS_FILE + "." + version + _SETTINGS_EXTENSION;
  }
  
  /**
   * @author Siew Yeng
   */
  public static int getProjectPshSettingsLatestFileVersion()
  {
    return 1;
  }
  
  /**
   * Surface Model Info log (PSH)
   * @author Siew Yeng
   */
  public static String getSurfaceModelInfoLogFullPath(String projectName, String logName)
  {
    return Directory.getSurfaceModelInfoDir(projectName) + File.separator + logName + _LOGFILE_EXTENSION;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static String getVelocityMappingFullPath(String magnification)
  {
    Assert.expect(magnification != null);
    
    return Directory.getVelocityMappingDir() + File.separator + magnification + File.separator + XrayTester.getSystemType().getName() + _CALIB_EXTENSION;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public static String getFalseCallRemarkTriggeredFullPath()
  {
    return Directory.getLogDir() + File.separator + _FALSE_CALL_REMARK_TRIGGERED_FILE;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public static int getFalseCallTriggeringLogLatestFileVersion()
  {
    return 1;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public static String getFalseCallTriggeringLogFullPath(int version)
  {
    return Directory.getLogDir() + File.separator + _DUMP_FALSE_CALL_TRIGGERING_LOG_FILE + _DOT + version + _COMMA_SEPARATED_VALUE_EXTENSION;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public static String getFalseCallTriggeringBackUpLogFullPath(String timeStamp) 
  {
    return Directory.getLogDir() + File.separator + _DUMP_FALSE_CALL_TRIGGERING_LOG_FILE + timeStamp + _DOT + 
            getFalseCallTriggeringLogLatestFileVersion() + _COMMA_SEPARATED_VALUE_EXTENSION;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public static String getFalseCallTriggeringConfigCarryForwardFullPath()
  {
    return Directory.getConfigDir() + File.separator + _FALSE_CALL_TRIGGERING_CONFIG_CARRY_FORWARD_FILE;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR-3870: False Call Triggering Feature Enhancement
   */
  public static String getPrevFalseCallTriggeringConfigFullPath()
  {
    return Directory.getPrevConfigDir() + File.separator + _FALSE_CALL_TRIGGERING_CONFIG_FILE;
  }
}
