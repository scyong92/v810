package com.axi.v810.datastore;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.testExec.*;

/**
 * @author George A. David
 */
public class Test_ImageSetInfoReader extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] arguments)
  {
    UnitTest.execute(new Test_ImageSetInfoReader());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    ImageSetInfoReader reader = new ImageSetInfoReader();
    try
    {
      try
      {
        reader.parseFile(null);
        Expect.expect(false);
      }
      catch (AssertException exc)
      {
        //do nothing
      }

      Expect.expect(ImageSetInfoReader.PROJECT_NAME_KEY.equals("projectName"));
      Expect.expect(ImageSetInfoReader.TEST_PROGRAM_VERSION_NUMBER_KEY.equals("testProgramVersionNumber"));

      // try reading a good file
      String testDir = getTestDataDir() + File.separator + "Test_ImageSetInfoReader";
      String filePath = testDir + File.separator + "good.info";
      reader.parseFile(filePath);
      ImageSetData imageSetData = reader.getImageSetData();
      Expect.expect(imageSetData.getProjectName().equals("projectName"));
      Expect.expect(imageSetData.getTestProgramVersionNumber() == 3);
      Expect.expect(imageSetData.isBoardPopulated() == true);
      Expect.expect(imageSetData.getImageSetName().equals("Test_ImageSetInfoReader"));
      Expect.expect(imageSetData.getUserDescription().equals("imageSetDescription"));
      Expect.expect(imageSetData.getImageSetTypeEnum().equals(ImageSetTypeEnum.JOINT_TYPE));

      // try reading a file with a bad comment
      filePath = testDir + File.separator + "badComment.info";
      try
      {
        reader.parseFile(filePath);
        Expect.expect(false);
      }
      catch (FileCorruptDatastoreException exception)
      {
        // do nothign
      }

      // read a file with a duplicate key
      filePath = testDir + File.separator + "duplicateKey.info";
      try
      {
        reader.parseFile(filePath);
        Expect.expect(false);
      }
      catch (FileCorruptDatastoreException exception)
      {
        // do nothign
      }

      // read a file with an invalid key
      filePath = testDir + File.separator + "invalidKey.info";
      try
      {
        reader.parseFile(filePath);
        Expect.expect(false);
      }
      catch (FileCorruptDatastoreException exception)
      {
        // do nothign
      }

      // read a file with an missing key
      filePath = testDir + File.separator + "missingKey.info";
      try
      {
        reader.parseFile(filePath);
        Expect.expect(false);
      }
      catch (FileCorruptDatastoreException exception)
      {
        // do nothign
      }

      // read a file with an missing key
      filePath = testDir + File.separator + "invalidVersionNumber.info";
      try
      {
        reader.parseFile(filePath);
        Expect.expect(false);
      }
      catch (FileCorruptDatastoreException exception)
      {
        // do nothign
      }
    }
    catch(Exception exception)
    {
      exception.printStackTrace();
    }
  }
}
