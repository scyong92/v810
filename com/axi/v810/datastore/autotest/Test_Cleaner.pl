###############################################################################
# File: Test_Cleaner.pl
# Purpose: Harness to call the completeCleanerTest.pl script.
#
# Author: Matt Wharton
###############################################################################

use strict vars;

sub main
{
  # Create temporary directories for NDFROOT and RTFROOT
  my $cmd = "mkdir c:/tempNDF c:/tempRTF";
  `$cmd`;
  
  # Copy over the checked in NDFs for the completeCleanerTest run
  $cmd = "cp -rf $ENV{'VIEW_DIR'}/5dx/rel/unitTest/ndf/cleanerTestNdfs/* c:/tempNdf";
  `$cmd`;

  # Temporarily set the env. variables to point to the unitTest directory
  $ENV{'NDFROOT'} = "c:/tempNdf";
  $ENV{'RTFROOT'} = "c:/tempRtf";
  $ENV{'RELROOT'} = "$ENV{'VIEW_DIR'}/5dx/rel/unitTest/rxx";
  $ENV{'IASROOT'} = "$ENV{'RELROOT'}/ias";
  $ENV{'AGT5DX_HOME'} = "$ENV{'VIEW_DIR'}/5dx/rel/unitTest/rxx";
  $ENV{'AGT5DX_PANELS'} = "$ENV{'VIEW_DIR'}/5dx/rel/unitTest/panels";
  $ENV{'RESROOT'} = "$ENV{'VIEW_DIR'}/5dx/rel/unitTest/res";

  # Make sure ServerConfig.properties has 'hardwareSimulation=true'
  open( INFILE, "$ENV{'AGT5DX_HOME'}/config/properties/ServerConfig.properties" );
  open( OUTFILE, ">$ENV{'AGT5DX_HOME'}/config/properties/ServerConfig.properties.temp" );
  while( <INFILE> )
  {
    my $line = $_;
    
    $line =~ s/^hardwareSimulation=false/hardwareSimulation=true/i;
    
    print OUTFILE $line;
  }
  close( INFILE );
  close( OUTFILE );
  rename( "$ENV{'AGT5DX_HOME'}/config/properties/ServerConfig.properties.temp", 
          "$ENV{'AGT5DX_HOME'}/config/properties/ServerConfig.properties" );
  
  # Start cxobjmgr
  $cmd = "start $ENV{'RELROOT'}/bin/cxobjmgr noservice";
  `$cmd`;
  
  # Start the java server
  $cmd = "start $ENV{'RELROOT'}/bin/run5dxserver.bat";
  `$cmd`;
  
  sleep( 10 );
 
  # Call the NDF Dirtier
  $cmd = "ndfDirtier -recurse $ENV{'NDFROOT'}";
  `$cmd`;
  
  # Call completeCleanerTest
  $cmd = "completeCleanerTest -recurse $ENV{'NDFROOT'}";
  my $out = `$cmd`;
  print $out;
  
  # Stop the java server
  $cmd = "$ENV{'RELROOT'}/bin/sendmessagetoserver shutdown";
  `$cmd`;
  
  # Stop cxobjmgr
  $cmd = "killprocess cxobjmgr.exe";
  `$cmd`;
  
  # Clean up the temporary NDF and RTF directories created for the test
  $cmd = "rm -rf c:/tempNdf c:/tempRtf";
  `$cmd`;
}

main();
