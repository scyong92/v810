package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when a file rename fails.
 * @author Bill Darbie
 */
public class FileCannotBeRenamedDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param originalFileName is the name of the file that is being renamed
   * @param newFileName is the new name of the file
   * @author Bill Darbie
   */
  public FileCannotBeRenamedDatastoreException(String originalFileName, String newFileName)
  {
    super(new LocalizedString("DS_ERROR_FILE_CANNOT_BE_RENAMED_KEY", new Object[]{originalFileName, newFileName}));
    Assert.expect(originalFileName != null);
    Assert.expect(newFileName != null);
  }
}
