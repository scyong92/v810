package com.axi.v810.datastore;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelSettings.Project;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.datastore.ndfReaders.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import java.util.logging.*;

/**
 * This class provides directory information.  This is the ONLY place that paths should be hard-coded.
 * Even here everything is based on AXI_XRAY_HOME.
 *
 * @author Bill Darbie
 */
public class Directory
{

  private static int _numInitCalls = 0;
  private static final String _CONFIG_DIR = "config";
  private static final String _XXL_CONFIG_DIR = "xxlConfig";
  private static final String _S2EX_CONFIG_DIR = "s2exConfig";
  private static final String _CONFIG_PREV_DIR = "config.prev";
  private static final String _GUI_PERSIST_DIR = "gui";
  private static final String _PROPERTIES_DIR = "properties";
  private static final String _PROPERTIES_NON_ASCII_DIR = "properties" + File.separator + "nonAscii";
  private static final String _PROPERTIES_PACKAGE = "com.axi.v810.properties";
  private static final String _ALGORITHMS_DIR = "algorithms";
  private static final String _SERIAL_NUM_DIR = "serialNumbers";
  private static final String _USER_ACCOUNTS_DIR = "accounts";
  private static final String _XML_SCHEMA_DIR = "xmlSchema";
  private static final String _BARCODE_READER_DIR = "barcodeReader";
  private static final String _JOINT_TYPE_ASSIGNMENT_DIR = "jointTypeAssignment";
  private static final String _INITIAL_THRESHOLDS_DIR = "initialThresholds";
  private static final String _INITIAL_RECIPE_SETTING_DIR = "initialRecipeSetting";
  private static final String _LOG_DIR = "log";
  private static final String _CAL_DIR = "calib";
  private static final String _XRAY_CAMERA_DIR = "xRayCamera";
  private static final String _OPTICAL_CAMERA_DIR = "opticalCamera";
  private static final String _OPTICAL_CALIBRATION_DIR = "opticalCalibration";
  private static final String _SMALL_FOV_DIR = "small";
  private static final String _LARGE_FOV_DIR = "large";
  private static final String _FRONT_MODULE_DIR = "front";
  private static final String _REAR_MODULE_DIR = "rear";
  private static final String _OPTICAL_PLANE_DIR = "plane";
  private static final String _OPTICAL_CAMERA_PROFILE_DIR = "profile";
  private static final String _XRAY_CAMERA_ARRAY_DIR = "xRayCameraArray";
  private static final String _AXI_TDI_XRAY_CAMERA_BOARD_VERSION_PREFIX_CONFIG_DIR = "boardVersion_";
  private static final String _AXI_DIR = "axi";
  private static final String _STAGE_HYSTERESIS_DIR = "StageHysteresis";
  private static final String _SYSTEM_FIDUCIAL_DIR = "SystemFiducial";
  private static final String _XRAY_SPOT_DIR = "XraySpot";
  private static final String _DARK_TEST_ONE_DIR = "DarkTestOne";
  private static final String _DARK_TEST_TWO_DIR = "DarkTestTwo";
  private static final String _LIGHT_TEST_ONE_DIR = "LightTestOne";
  private static final String _LIGHT_TEST_TWO_DIR = "LightTestTwo";
  private static final String _PANEL_POSITIONING_DIR = "panelPositioning";
  private static final String _CALIB_LOG_DIR = _LOG_DIR + File.separator + "calib";
  private static final String _SYSTEM_OFFSETS_CAL_LOG_DIR = _CALIB_LOG_DIR + File.separator + "SystemOffsets";
  private static final String _XRAY_SPOT_CAL_LOG_DIR = _CALIB_LOG_DIR + File.separator + _XRAY_SPOT_DIR;
  private static final String _GRAYSCALE_CAL_LOG_DIR = _CALIB_LOG_DIR + File.separator + "CameraGrayscale";
  private static final String _GRAYSCALE_CAL_LOG_DIR_HIGH_MAG = _CALIB_LOG_DIR + File.separator + "CameraGrayscaleHighMag";
  private static final String _GRAYSCALE_CAL_LOG_DIR_LOW_MAG = _CALIB_LOG_DIR + File.separator + "CameraGrayscaleLowMag";
  private static final String _STAGE_HYSTERESIS_CAL_LOG_DIR = _CALIB_LOG_DIR + File.separator + _STAGE_HYSTERESIS_DIR;
  private static final String _MAGNIFICATION_CAL_LOG_DIR = _CALIB_LOG_DIR + File.separator + "Magnification";  
  private static final String _OPTICAL_MAGNIFICATION_CAL_LOG_DIR = _CALIB_LOG_DIR + File.separator + "OpticalMagnification";
  private static final String _OPTICAL_CALIBRATION_CAL_LOG_DIR = _CALIB_LOG_DIR + File.separator + _OPTICAL_CALIBRATION_DIR;
  private static final String _OPTICAL_HEIGHT_CAL_LOG_DIR = _CALIB_LOG_DIR + File.separator + "OpticalCameraHeight";
  private static final String _OPTICAL_SYSTEM_FIDUCIAL_CAL_LOG_DIR = _CALIB_LOG_DIR + File.separator + "OpticalSystemFiducial";
  private static final String _OPTICAL_REFERENCE_PLANE_CAL_LOG_DIR = _CALIB_LOG_DIR + File.separator + "OpticalReferencePlane";
  private static final String _OPTICAL_SYSTEM_FIDUCIAL_ROTATION_CAL_LOG_DIR = _CALIB_LOG_DIR + File.separator + "OpticalSystemFiducialRotation";
  private static final String _OPTICAL_FRINGE_PATTERN_UNIFORMITY_CAL_LOG_DIR = _CALIB_LOG_DIR + File.separator + "OpticalFringePatternUniformity";
  private static final String _OPTICAL_SYSTEM_OFFSET_ADJUSTMENT_CAL_LOG_DIR = _CALIB_LOG_DIR + File.separator + "OpticalSystemOffsetAdjustment";  
  private static final String _XRAY_SPOT_DEFLECTION_CAL_LOG_DIR = _CALIB_LOG_DIR + File.separator + "XraySpotDeflection";
  private static final String _SOLDER_THICKNESS_DIR = "calib";
  private static final String _SOLDER_THICKNESS_SUB_DIR = "solderThicknessTable";
  private static final String _VELOCITY_MAPPING_SUB_DIR = "velocityMappingTable";
  private static final String _CONFIRMATION_LOG_DIR = _LOG_DIR + File.separator + "confirm";
  private static final String _CAMERA_LIGHT_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "CameraLight";
  private static final String _CAMERA_DARK_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "CameraDark";
  private static final String _OPTICAL_CAMERA_DARK_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "opticalCameraDark";
  private static final String _OPTICAL_CAMERA_LIGHT_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "opticalCameraLight";
  private static final String _CAMERA_HIGH_MAG_LIGHT_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "HighMagCameraLight";
  private static final String _CAMERA_HIGH_MAG_DARK_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "HighMagCameraDark";
  private static final String _CAMERA_ADJUSTMENT_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "CameraAdjustment";
  private static final String _CAMERA_HIGH_MAG_ADJUSTMENT_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "HighMagCameraAdjustment";
  private static final String _XRAY_SOURCE_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "XraySource";
  private static final String _MICROCONTROLLER_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "MicroController";
  private static final String _BASE_IMAGE_QUALITY_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "BaseImageQuality";
  private static final String _FIXED_RAIL_IMAGE_QUALITY_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "FixedRailImageQuality";
  private static final String _SYSTEM_IMAGE_QUALITY_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "SystemImageQuality";
  private static final String _ADJUSTABLE_RAIL_IMAGE_QUALITY_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "AdjustableRailImageQuality";
  private static final String _SYSTEM_DIAGNOSTIC_DIR = _CONFIRMATION_LOG_DIR + File.separator + "SystemDiagnostic";
  private static final String _SYSTEM_COMMUNICATION_NETWORK_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "SystemCommunicationNetwork";
  private static final String _PANEL_HANDLING_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "PanelHandling";
  private static final String _BENCHMARK_DIR = "benchmark";  
  private static final String _BENCHMARK_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + _BENCHMARK_DIR;
  private static final String _MOTION_REPEATABILITY_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "MotionRepeatability";
  private static final String _FRONT_OPTICAL_MEASUREMENT_REPEATABILITY_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "FrontOpticalMeasurementRepeatability";
  private static final String _REAR_OPTICAL_MEASUREMENT_REPEATABILITY_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "RearOpticalMeasurementRepeatability";
  private static final String _CAMERA_QUALITY_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "CameraQuality";
  private static final String _AREA_MODE_IMAGE_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "AreaModeImage";
  //Swee-yee.wong
  private static final String _CAMERA_CALIBRATION_IMAGES_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "CameraCalibrationImages";
  private static final String _CAMERA_HIGH_MAG_CALIBRATION_IMAGES_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "HighMagCameraCalibrationImages";
  private static final String _DOCS_DIR = "docs";
  private static final String _OPERATOR_HELP_DIR = "Operator Manual";
  private static final String _XRAY_SAFETY_TEST_DIR = "XraySafetyTest";
  private static final String _SERVICE_DIR = "service";
  private static final String _HWSTATUS_DOCS_DIR = "hw_status";
  private static final String _ONLINE_XRAY_IMAGES_DIR = "onlineXrayImages";
  private static final String _ONLINE_OPTICAL_IMAGES_DIR = "onlineOpticalImages";
  private static final String _GUI_IMAGES_DIR = "images";
  private static final String _GUI_IMAGES_RELATIVE_DIR = _CONFIG_DIR + File.separator + "images";
  private static final String _JOINT_THUMBNAILS_DIR = "jointThumbnails";
  private static final String _LEGACY_NDF_DIR = "ndf";
  private static final String _LEGACY_NDF_ORIG_DIR = "orig";
  private static final String _PROJECTS_DIR = "recipes";
  private static final String _BIN_DIR = "bin";
  private static final String _PSTOOLS_DIR = "psTools";
  private static final String _UNIT_TEST_DIR = "testRel" + File.separator + "unitTest";
  private static final String _UNIT_TEST_REL_DIR = "x.xx";
  private static final String _NDF_BACKUP_DIR = "backup";
  private static final String _NDF_TEMP_BACKUP_DIR = "temp";
  private static final String _TEMP = "temp";
  private static final String _LICENSE_DIR = "license";
  private static final String _MOTION_CONTROL_DIR = "motionControl";
  private static final String _MOTION_CONTROLLER_DIR = "motionController";
  private static final String _MOTION_DRIVE_DIR = "motionDrive";
  private static final String _SYNQNET_MOTION_DRIVE_DIR = "synqNet";
  private static final String _KOLLMORGEN_CD_MOTION_DRIVE_DIR = "kollmorgen" + File.separator + "cd";
  private static final String _BACKUP = "backup";
  private static final String _IMAGES_DIR = "images";
  private static final String _OPTICAL_IMAGES_DIR = "opticalImages";
  private static final String _INSPECTION_IMAGES_DIR = "inspection";
  private static final String _PSP_OFFLINE_IMAGES_DIR = "offline";
  private static final String _VERIFICATION_IMAGES_DIR = "verification";
  private static final String _VIRTUAL_LIVE_IMAGES_DIR = "virtualLive";
  private static final String _ADJUST_FOCUS_IMAGES_DIR = "adjustFocus";
  private static final String _IMAGE_SET_PREFIX = "imageset";
  private static final String _RESULTS_DIR = "results";
  private static final String _ALGORITHM_LEARNING_DIR = "algorithmLearning";
  private static final String _INSPECTION_RUN_BASE_DIR = "inspectionRun_";
  private static final String _MOTION_PATTERN_DIR = "motionPattern";
  private static final String _POINT_TO_POINT_DIR = "pointToPoint";
  private static final String _SCAN_DIR = "scan";
  private static final String _INSPECTION_DATA_DIR = "inspectionData";
  private static final String _TEMP_IMAGE_CACHE_DIR = "tempImageCache";
  private static final String _IRP_BIN_DIR = "irpBin";
  private static final String _RMS_BIN_DIR = "rmsBin";
  private static final String _FAILING_ALIGNMENT_IMAGES_DIR = "failingAlignmentImages";
  private static final String _SAVED_ALIGNMENT_IMAGES_DIR = "savedAlignmentImages";
  private static final String _FOCUS_CONFIRMATION_IMAGES_DIR = "focusConfirmation";
  private static final String _GOOD_IMAGES_DIR = "C:\\goodImages";
  private static final String _DEFECT_PACKAGER_DIR = "C:\\Program Files\\DefectPackager";  
  private static final String _NON_CORRECTED_FRINGE_PATTERN_DIR = "fringePattern\\nonCorrected";
  private static final String _SURFACE_MAP_INFO_DIR = "surfaceMap";
  
  private static final String _HIGH_MAG_BASE_IMAGE_QUALITY_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "baseImageQualityForHighMag";
  private static final String _HIGH_MAG_SYSTEM_IMAGE_QUALITY_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "SystemImageQualityForHighMag";
  private static final String _HIGH_MAG_FIXED_RAIL_IMAGE_QUALITY_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "FixedRailImageQualityForHighMag";
  private static final String _HIGH_MAG_ADJUSTABLE_RAIL_IMAGE_QUALITY_CONFIRMATION_DIR = _CONFIRMATION_LOG_DIR + File.separator + "AdjustableRailImageQualityForHighMag";
 
  /** @todo MDW - remove later! */
  private static final String _CALL_RATE_SPIKE_ALIGNMENT_IMAGES_DIR = "callRateSpikeAlignmentImages";
  private static final String _PACKAGE_LIBRARIES_DIR = "libraries";
  private static String _rootDir; // c:/program files/v810
  private static String _releaseDir; // c:/program files/v810/1.0
  private static String _jreDir; // c:/program files/v810/1.0/jre
  private static String _jre32Dir; // c:/program files/v810/1.0/jre
  private static boolean _initialized = false;
  private static Config _config = null;
  private static String _inspectionRunFolderPattern = _INSPECTION_RUN_BASE_DIR + "(\\d+)-(\\d+)-(\\d+)_(\\d+)-(\\d+)-(\\d+)-(\\d+)";  //XCR-2022 - Kok Chun, Tan
  private static Pattern _inspectionRunDirPattern = Pattern.compile(_inspectionRunFolderPattern);
  private static final String _FALSE_CALL_TRIGGERING_DIR = "falseCallTriggering";
  //project's history log-hsiafen.tan  
  private static final String _HISTORY_FILE_DIR = "history";
  // bee-hoon.goh - Virtual Live Verification Images
  private static final String _VIRTUAL_LIVE_VERIFICATION_IMAGES_DIR = "virtualLiveVerification";
  
  private static List<String> _solderThicknessLayer1DirList = new ArrayList<String>();
  private static List<String> _solderThicknessLayer2DirList = new ArrayList<String>();
  private static String _M11_SOLDER_THICKNESS_DIR = "M11";
  //Swee Yee Wong - include M13 for XXL
  private static String _M13_SOLDER_THICKNESS_DIR = "M13";
  private static String _M19_SOLDER_THICKNESS_DIR = "M19";
  private static String _M23_SOLDER_THICKNESS_DIR = "M23";
  private static String _STANDARD_PROJECT_SOLDER_THICKNESS_DIR = "standard";
  private static String _XXL_PROJECT_SOLDER_THICKNESS_DIR = "xxl";
  private static String _S2EX_PROJECT_SOLDER_THICKNESS_DIR = "s2ex";
  private static String _STANDARD_PROJECT_SOLDER_THICKNESS_BACKUP_DIR = "standard_backup";
  private static String _XXL_PROJECT_SOLDER_THICKNESS_BACKUP_DIR = "xxl_backup";
  private static String _S2EX_PROJECT_SOLDER_THICKNESS_BACKUP_DIR = "s2ex_backup";

  //ShengChuan - Clear Tombstone
  private static final String _IMAGE_TEMPLATE_LEARNING_DIR = "ImageTemplate";

  //Khaw Chek Hau - XCR2285: 2D Alignment on v810
  private static final String _VERIFICATION_2D_IMAGES_DIR = "2DVerification";
  private static final String _ALIGNMENT_2D_IMAGES_DIR = "alignment2DImages";
  
  //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
  private static final String _SPECIFIC_HISTORY_LOG_DIR = "historyLog";
  private static final String _FINE_TUNING_LOG_DIR = "fineTuningLog";
  
  private static final String _CUSTOMIZE_SETTING_DIR = "customizeSetting";
  
  private static final String _SYSTEM_TYPE_CONFIG_DIR_IN_RECIPE = "SystemTypeConfig";
  
  //Janan XCR-3551
  private static final String _CUSTOM_MEASUREMENT_XML_SETTING_DIR = "measurementXMLSetting";

  //Siew Yeng - XCR-3781
  private static final String _SURFACE_MODEL_INFO_DIR = "surfaceModel";

  /**
   * @author Bill Darbie
   */
  static
  {
    initVariablesUsingEnvironmentVariables();
    _config = Config.getInstance();
    
    addSolderThicknessLayer1DirList(_M11_SOLDER_THICKNESS_DIR);
    //Amos Chee Tze Wei - XCR-3227 - UnitTest Update for V810 (Phase 1)
    if (UnitTest.unitTesting() == false && XrayTester.isXXLBased())
    {
      //Swee Yee Wong - include M13 for XXL
      addSolderThicknessLayer1DirList(_M13_SOLDER_THICKNESS_DIR);
    }
    addSolderThicknessLayer1DirList(_M19_SOLDER_THICKNESS_DIR);
    addSolderThicknessLayer1DirList(_M23_SOLDER_THICKNESS_DIR);
    
    // XCR-3604 Unit Test Phase 2
    if (UnitTest.unitTesting())
    {
      addSolderThicknessLayer2DirList("Gain1");
      addSolderThicknessLayer2DirList("Gain2");
      addSolderThicknessLayer2DirList("Gain3");
      addSolderThicknessLayer2DirList("Gain4");
      addSolderThicknessLayer2DirList("Gain5");
      addSolderThicknessLayer2DirList("Gain6");
      addSolderThicknessLayer2DirList("Gain7");
      addSolderThicknessLayer2DirList("Gain8");
      addSolderThicknessLayer2DirList("Gain9");
    }
  }

  /**
   * @author Bill Darbie
   */
  private static void initVariablesUsingEnvironmentVariables()
  {
    try
    {
      _releaseDir = System.getenv("AXI_XRAY_HOME");
      if ((_releaseDir == null) || (UnitTest.unitTesting()))
      {
        String userDir = System.getProperty("user.dir");
        int index = userDir.indexOf("java");
        if (index > 0)
        {
          _releaseDir = userDir.substring(0, index) + "custRel" + File.separator + "v810" +
                        File.separator + _UNIT_TEST_REL_DIR;
        }
        else
        {
          _releaseDir = userDir;
        }
        if (UnitTest.unitTesting() == false)
        {
          System.out.println("WARNING: AXI_XRAY_HOME was not set - assuming it should be " + _releaseDir);
        }
      }
      // no matter if the variable is using / or \ set it to use File.separator so we are always consistent
      _releaseDir = _releaseDir.replace('\\', File.separatorChar);
      _releaseDir = _releaseDir.replace('/', File.separatorChar);

      int index = _releaseDir.lastIndexOf(File.separatorChar);
      _rootDir = _releaseDir.substring(0, index);

      if (UnitTest.unitTesting())
      {
        // find the last /
        index = _rootDir.lastIndexOf(File.separatorChar);
        // find the 2nd to last /
        index = _rootDir.lastIndexOf(File.separatorChar, index - 1);
        if (index != -1)
        {
          // replace custRel/x6000 with testRel/unitTest
          _rootDir = _rootDir.substring(0, index) + File.separator + _UNIT_TEST_DIR;
          _releaseDir = _rootDir + File.separator + _UNIT_TEST_REL_DIR;
        }
      }

      // no matter if the variable is using / or \ set it to use File.separator so we are always consistent
      _rootDir = _rootDir.replace('\\', File.separatorChar);
      _rootDir = _rootDir.replace('/', File.separatorChar);

      // get the XRAYTEST_JRE_HOME setting
      _jreDir = System.getProperty("java.home");
      _jre32Dir = System.getenv("AXI_XRAY_JRE_32_HOME");
      if(_jre32Dir == null)
      {
        _jre32Dir = "C:\\apps\\Java\\jre7_x86";
      }
    }
    catch (SecurityException se)
    {
      System.out.println("WARNING - AXI_XRAY_HOME is not set");
      // we are running as an applet
      _releaseDir = ".";
    }

  }

  /**
   * @return the jre directory, for example c:/program files/v810/1.0/jre/1.4.1_01/bin
   * @author Bill Darbie
   */
  public static String getJreBin()
  {
    Assert.expect(_jreDir != null);

    return _jreDir + File.separator + "bin";
  }
  
  public static String getJreLib()
  {
    return System.getenv("AXI_XRAY_JRE_HOME")+File.separator+"lib";
  }
  
  public static String getJreLibExt()
  {
    return getJreLib()+File.separator+"ext";
  }
  /**
   * @return the jre directory, for example c:/program files/v810/1.0/jre/1.4.1_01/bin
   * @author Chong Wei Chin
   * RJG FIXME this should not be hardcoded!!!
   */
  public static String getRMSJreBin()
  {
    return "C:\\Program Files\\AXI System\\v810\\5.0\\jre\\1.7.0_x86\\bin";
  }

  /**
   * @return the jre directory, for example c:/program files/v810/1.0/jre/1.4.1_01/bin
   * @author Bill Darbie
   */
  public static String getJre32Bin()
  {
    Assert.expect(_jre32Dir != null);

    return _jre32Dir + File.separator + "bin";
  }

  /**
   * @return the jre directory, for example c:/program files/v810/x1.0/jre/1.4.1_01
   * @author Erica Wheatcroft
   */
  public static String getJreDir()
  {
    Assert.expect(_jreDir != null);

    return _jreDir;
  }

  /**
   * Return the directory where GUI related configuration info is to be stored/retrieved.
   * XRAY_TEST_HOME must be set for this to work properly.
   * The directory returned is AXI_XRAY_HOME\config\gui
   * An Assert will be thrown if AXI_XRAY_HOME is not set.
   *
   * @return String containing the path name without the trailing slash
   * @author Andy Mechtenberg
   */
  public static String getGuiConfigDir()
  {
    return getConfigDir() + File.separator + _GUI_PERSIST_DIR;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPrevGuiConfigDir()
  {
    return getPrevConfigDir() + File.separator + _GUI_PERSIST_DIR;
  }

  /**
   * Return the directory where the User Accounts info is to be stored/retrieved.
   * XRAY_TEST_HOME must be set for this to work properly.
   * The directory returned is AXI_XRAY_HOME\config\accounts
   * An Assert will be thrown if AXI_XRAY_HOME is not set.
   *
   * @return String containing the path name without the trailing slash
   * @author Andy Mechtenberg
   */
  public static String getUserAccountsDir()
  {
    return getConfigDir() + File.separator + _USER_ACCOUNTS_DIR;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPrevUserAccountsDir()
  {
    return getPrevConfigDir() + File.separator + _USER_ACCOUNTS_DIR;
  }

  /**
   * Return the directory where the XML schema files are installed
   * @author Laura Cormos
   */
  public static String getXmlSchemaDir()
  {
    return getConfigDir() + File.separator + _XML_SCHEMA_DIR;
  }

  /**
   * Return the directory where the config files exist.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @return String containing the path name without the trailing slash
   * @author Bill Darbie
   */
  public static String getConfigDir()
  {
    Assert.expect(_releaseDir != null); 
    
    if (UnitTest.unitTesting() == false)
    {
      try
      {
        if (LicenseManager.isOfflineProgramming())
        {
          if (LicenseManager.isXXLEnabled() &&  FileUtilAxi.exists( _releaseDir + File.separator + _XXL_CONFIG_DIR))
          {
            return  _releaseDir + File.separator + _XXL_CONFIG_DIR;
          }
          else if (LicenseManager.isS2EXEnabled()&&  FileUtilAxi.exists( _releaseDir + File.separator + _S2EX_CONFIG_DIR))
          {
            return _releaseDir + File.separator + _S2EX_CONFIG_DIR;
          }
          else if (LicenseManager.isThroughputEnabled() &&  FileUtilAxi.exists( _releaseDir + File.separator + _CONFIG_DIR))
          {
            return _releaseDir + File.separator + _CONFIG_DIR;
          }
          else
          {
            if (LicenseManager.isXXLEnabled())
            {
              return _releaseDir + File.separator + _XXL_CONFIG_DIR;
            }
            else if (LicenseManager.isS2EXEnabled())
            {
              return _releaseDir + File.separator + _S2EX_CONFIG_DIR;
            }
            else
            {
              return _releaseDir + File.separator + _CONFIG_DIR;
            }
          }
        }
        else
        {
          if(LicenseManager.isXXLEnabled())
            return _releaseDir + File.separator + _XXL_CONFIG_DIR;
          else if(LicenseManager.isS2EXEnabled())
            return _releaseDir + File.separator + _S2EX_CONFIG_DIR;
          else
            return _releaseDir + File.separator + _CONFIG_DIR;
        }
      }
      catch (BusinessException e)
      {
        return _releaseDir + File.separator + _CONFIG_DIR;
      }
    }
    else
      return _releaseDir + File.separator + _CONFIG_DIR;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPrevConfigDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _CONFIG_PREV_DIR;
  }

  /**
   * @author Bill Darbie
   */
  public static String getInspectionDataDir()
  {
    return getConfigDir() + File.separator + _INSPECTION_DATA_DIR;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPrevInspectionDataDir()
  {
    return getPrevConfigDir() + File.separator + _INSPECTION_DATA_DIR;
  }

  /**
   * Return the directory where the calibration files exist.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @return String containing the path name without the trailing slash
   * @author Bill Darbie
   */
  public static String getCalibDir()
  {
    Assert.expect(_releaseDir != null);
    return _releaseDir + File.separator + _CAL_DIR;
  }

  /**
   * Return the directory where the log files for this cal will go.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @return String containing the path name without the trailing slash
   * @author Eddie Williamson
   */
  public static String getXraySpotCalLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _XRAY_SPOT_CAL_LOG_DIR;
  }

  /**
   * Return the directory where the log files for this cal will go.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @return String containing the path name without the trailing slash
   * @author Eddie Williamson
   */
  public static String getSystemOffsetsCalLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _SYSTEM_OFFSETS_CAL_LOG_DIR;
  }

  /**
   * Return the directory where the log files for this cal will go.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @return String containing the path name without the trailing slash
   * @author Eddie Williamson
   */
  public static String getGreyscaleCalLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _GRAYSCALE_CAL_LOG_DIR;
  }
  
  /**
   * Return the directory where the log files for this high mag cal will go.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @return String containing the path name without the trailing slash
   * @author Anthony Fong
   */
  public static String getGreyscaleCalHighMagLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _GRAYSCALE_CAL_LOG_DIR_HIGH_MAG;
  }

  /**
   * Return the directory where the log files for this high mag cal will go.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @return String containing the path name without the trailing slash
   * @author Anthony Fong
   */
  public static String getGreyscaleCalLowMagLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _GRAYSCALE_CAL_LOG_DIR_LOW_MAG;
  }

  /**
   * Return the directory where the log files for this cal will go.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @return String containing the path name without the trailing slash
   * @author Eddie Williamson
   */
  public static String getStageHysteresisCalLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _STAGE_HYSTERESIS_CAL_LOG_DIR;
  }

  /**
   * Return the directory where all the images are loaded from file for simulation.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @return String containing the path name without the trailing slash
   * @author Lim, Seng Yew
   */
  public static String getStageHysteresisCalLogDataDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _STAGE_HYSTERESIS_CAL_LOG_DIR + "\\data";
  }

  /**
   * Return the directory where the log files for this cal will go.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @return String containing the path name without the trailing slash
   * @author Eddie Williamson
   */
  public static String getMagnificationCalLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _MAGNIFICATION_CAL_LOG_DIR;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static String getOpticalSystemOffsetAdjustmentCalLogDir()
  {
    Assert.expect(_releaseDir != null);
    
    return _releaseDir + File.separator + _OPTICAL_SYSTEM_OFFSET_ADJUSTMENT_CAL_LOG_DIR;
  }

  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalMagnificationCalLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _OPTICAL_MAGNIFICATION_CAL_LOG_DIR;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalMagnificationCalLogImageDir(int cameraId)
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _OPTICAL_MAGNIFICATION_CAL_LOG_DIR + File.separator + "images" + getOpticalCameraIdDir(cameraId);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getOpticalReferencePlaneCalLogImageDir(int cameraId)
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _OPTICAL_REFERENCE_PLANE_CAL_LOG_DIR + File.separator + "images" + getOpticalCameraIdDir(cameraId);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalMagnificationCalLogPlaneImageDir(int cameraId, int planeId)
  {
    Assert.expect(_releaseDir != null);

    return getOpticalMagnificationCalLogImageDir(cameraId) + getOpticalPlaneDir(planeId);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalCalibrationCalLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _OPTICAL_CALIBRATION_CAL_LOG_DIR;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getNonCorrectedFringePatternDir()
  {
    Assert.expect(_releaseDir != null);

    if(LicenseManager.isXXLEnabled())      
      return _releaseDir + File.separator + _XXL_CONFIG_DIR + File.separator + _NON_CORRECTED_FRINGE_PATTERN_DIR;
    else if(LicenseManager.isS2EXEnabled())
      return _releaseDir + File.separator + _S2EX_CONFIG_DIR + File.separator + _NON_CORRECTED_FRINGE_PATTERN_DIR;
    else
      return _releaseDir + File.separator + _CONFIG_DIR + File.separator + _NON_CORRECTED_FRINGE_PATTERN_DIR;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getOpticalFringePatternUniformityCalLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _OPTICAL_FRINGE_PATTERN_UNIFORMITY_CAL_LOG_DIR; 
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalHeightCalLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _OPTICAL_HEIGHT_CAL_LOG_DIR;
  }
  
  /**
   * Return the directory where the log files for this cal will go.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @return String containing the path name without the trailing slash
   * @author Cheah Lee Herng
   */
  public static String getOpticalSystemFiducialCalLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _OPTICAL_SYSTEM_FIDUCIAL_CAL_LOG_DIR;
  }
  
  /**
   * Return the directory where the log files for this cal will go.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @return String containing the path name without the trailing slash
   * @author Cheah Lee Herng
   */
  public static String getOpticalReferencePlaneCalLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _OPTICAL_REFERENCE_PLANE_CAL_LOG_DIR;
  }
  
  /**
   * Return the directory where the log files for this cal will go.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @return String containing the path name without the trailing slash
   * @author Cheah Lee Herng
   */
  public static String getOpticalSystemFiducialRotationCalLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _OPTICAL_SYSTEM_FIDUCIAL_ROTATION_CAL_LOG_DIR;
  }


  /**
   * Return the directory where the log files for this cal will go.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @author Reid Hayhow
   */
  public static String getXraySpotDeflectionCalLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _XRAY_SPOT_DEFLECTION_CAL_LOG_DIR;
  }

  /**
   * Return the directory where the log files for this confirmation will go.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @author Anthony Fong - CameraQuality
   */
  public static String getConfirmCameraQualityLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _CAMERA_QUALITY_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where the log files for this confirmation will go.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @author Reid Hayhow
   */
  public static String getConfirmAreaModeImageLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _AREA_MODE_IMAGE_CONFIRMATION_DIR;
  }
  
  public static String getConfirmCameraCalibrationImagesLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _CAMERA_CALIBRATION_IMAGES_CONFIRMATION_DIR;
  }
  
  public static String getHighMagConfirmCameraCalibrationImagesLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _CAMERA_HIGH_MAG_CALIBRATION_IMAGES_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where the solder thickness tables exist.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @return String containing the path name without the trailing slash
   * @author Eddie Williamson
   */
  public static String getSolderThicknessDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _SOLDER_THICKNESS_DIR;
  }
  
  /**
   * @author Swee Yee Wong
   */
  public static String getConfigSolderThicknessDir()
  {
    //Swee Yee Wong - XCR-3115 Slow inspection and learning in S2
    //don't use LicenseManager to check system type, it will slow down the
    //process if using dongle, so we should use xrayTester instead.
    //DO NOT USE configDir
    Assert.expect(_releaseDir != null); 
    
    // XCR-3604 Unit Test Phase 2
    if (UnitTest.unitTesting() == false)
    {
      if(XrayTester.isXXLBased())
        return _releaseDir + File.separator + _XXL_CONFIG_DIR + File.separator + _SOLDER_THICKNESS_SUB_DIR;
      else if(XrayTester.isS2EXEnabled())
        return _releaseDir + File.separator + _S2EX_CONFIG_DIR + File.separator + _SOLDER_THICKNESS_SUB_DIR;
      else
        return _releaseDir + File.separator + _CONFIG_DIR + File.separator + _SOLDER_THICKNESS_SUB_DIR;
    }
    else
      return _releaseDir + File.separator + _CONFIG_DIR + File.separator + _SOLDER_THICKNESS_SUB_DIR;
  }
  
  /**
   * @author Swee Yee Wong
   */
  public static String getProjectSolderThicknessDir(String projectName)
  {
    //Swee Yee Wong - XCR-3115 Slow inspection and learning in S2
    //don't use LicenseManager to check system type, it will slow down the
    //process if using dongle, so we should use xrayTester instead.
    if(XrayTester.isXXLBased())
      return getProjectDir(projectName)+ File.separator + _SOLDER_THICKNESS_SUB_DIR + File.separator + _XXL_PROJECT_SOLDER_THICKNESS_DIR;
    else if(XrayTester.isS2EXEnabled())
      return getProjectDir(projectName)+ File.separator + _SOLDER_THICKNESS_SUB_DIR + File.separator + _S2EX_PROJECT_SOLDER_THICKNESS_DIR;
    else
      return getProjectDir(projectName)+ File.separator + _SOLDER_THICKNESS_SUB_DIR + File.separator + _STANDARD_PROJECT_SOLDER_THICKNESS_DIR;
  }
  
    /**
   * @author Swee Yee Wong
   */
  public static String getProjectSolderThicknessBackupDir(String projectName)
  {
    //Swee Yee Wong - XCR-3115 Slow inspection and learning in S2
    //don't use LicenseManager to check system type, it will slow down the
    //process if using dongle, so we should use xrayTester instead.
    if(XrayTester.isXXLBased())
      return getProjectDir(projectName)+ File.separator + _SOLDER_THICKNESS_SUB_DIR + File.separator + _XXL_PROJECT_SOLDER_THICKNESS_BACKUP_DIR;
    else if(XrayTester.isS2EXEnabled())
      return getProjectDir(projectName)+ File.separator + _SOLDER_THICKNESS_SUB_DIR + File.separator + _S2EX_PROJECT_SOLDER_THICKNESS_BACKUP_DIR;
    else
      return getProjectDir(projectName)+ File.separator + _SOLDER_THICKNESS_SUB_DIR + File.separator + _STANDARD_PROJECT_SOLDER_THICKNESS_BACKUP_DIR;
  }

  /**
   * Return the root directory where confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author Reid Hayhow
   */
  public static String getConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _CONFIRMATION_LOG_DIR;
  }

  /**
   * Return the directory where camera light confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author Reid Hayhow
   */
  public static String getCameraLightConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _CAMERA_LIGHT_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where camera dark confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author Reid Hayhow
   */
  public static String getCameraDarkConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _CAMERA_DARK_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where optical camera dark confirmation results will be logged
   * 
   * @return String containing the path name without trailing slash
   * @author Cheah Lee Herng
   */
  public static String getOpticalCameraDarkConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _OPTICAL_CAMERA_DARK_CONFIRMATION_DIR;
  }
  
  /**
   * Return the directory where optical camera light confirmation results will be logged
   * 
   * @return String containing the path name without trailing slash
   * @author Cheah Lee Herng
   */
  public static String getOpticalCameraLightConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _OPTICAL_CAMERA_LIGHT_CONFIRMATION_DIR;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalCameraLightConfirmationLogImageDir(int cameraId)
  {
      return getOpticalCameraLightConfirmationLogDir() + File.separator + "images" + getOpticalCameraIdDir(cameraId);
  }

  /**
   * Return the directory where camera light confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author Anthony Fong
   */
  public static String getCameraHighMagLightConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _CAMERA_HIGH_MAG_LIGHT_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where camera dark confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author Anthony Fong
   */
  public static String getCameraHighMagDarkConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _CAMERA_HIGH_MAG_DARK_CONFIRMATION_DIR;
  }  

  /**
   * Return the directory where camera calibration confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author Reid Hayhow
   */
  public static String getCameraAdjustmentConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _CAMERA_ADJUSTMENT_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where camera calibration confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author Anthony Fong
   */
  public static String getCameraHighMagAdjustmentConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _CAMERA_HIGH_MAG_ADJUSTMENT_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where xray source confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author Reid Hayhow
   */
  public static String getXraySourceConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _XRAY_SOURCE_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where micro-controller confirmation results will be logged
   *
   * @author Cheah Lee Herng
   */
  public static String getMicroControllerConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _MICROCONTROLLER_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where System MTF confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author John Dutton
   */
  public static String getBaseImageQualityConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _BASE_IMAGE_QUALITY_CONFIRMATION_DIR;
  }
  
  /**
   * Return the directory where System MTF confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author John Dutton
   */
  public static String getHighMagBaseImageQualityConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _HIGH_MAG_BASE_IMAGE_QUALITY_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where System Fiducial confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author John Dutton
   */
  public static String getFixedRailImageQualityConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _FIXED_RAIL_IMAGE_QUALITY_CONFIRMATION_DIR;
  }
  
  /**
   * Return the directory where System Fiducial confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author Wei Chin
   */
  public static String getHighMagFixedRailImageQualityConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _HIGH_MAG_FIXED_RAIL_IMAGE_QUALITY_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where Magnification Coupon confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author John Dutton
   */
  public static String getSystemImageQualityConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _SYSTEM_IMAGE_QUALITY_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where Magnification Coupon confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author Wei Chin
   */
  public static String getHighMagSystemImageQualityConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _HIGH_MAG_SYSTEM_IMAGE_QUALITY_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where System Fiducial B confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author John Dutton
   */
  public static String getAdjustableRailImageQualityConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _ADJUSTABLE_RAIL_IMAGE_QUALITY_CONFIRMATION_DIR;
  }
  
  /**
   * Return the directory where System Fiducial B confirmation results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author Wei Chin
   */
  public static String getHighMagAdjustableRailImageQualityConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _HIGH_MAG_ADJUSTABLE_RAIL_IMAGE_QUALITY_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where DiagnoseSharpness results will be logged
   *
   * @return String containing the path name without trailing slash
   * @author John Dutton
   */
  public static String getSystemDiagnosticLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _SYSTEM_DIAGNOSTIC_DIR;
  }

  /**
   * Return the directory where communication confirmation results will be logged
   *
   * @author Reid Hayhow
   */
  public static String getSystemCommunicationNetworkConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _SYSTEM_COMMUNICATION_NETWORK_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where panel handling confirmation results will be logged
   *
   * @author Reid Hayhow
   */
  public static String getPanelHandlingConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _PANEL_HANDLING_CONFIRMATION_DIR;
  }
  
  /**
   * Return the directory where Benchmark handling confirmation results will be logged
   *
   * @author Anthony Fong
   */
  public static String getBenchmarkConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _BENCHMARK_CONFIRMATION_DIR;
  }
    
  /**
   * Get the directory where we store the scan move configuration files for CDNA Benchmarking
   * @author Anthony Fong
   */
  public static String getScantMoveDirectoryForCDNABenchmarking()
  {
    return getConfigDir() + File.separator + _BENCHMARK_DIR;
  }
  
  /**
   * Return the directory where optical system fiducial confirmation results will be logged
   *
   * @author Reid Hayhow
   */
  public static String getOpticalSystemFiducialConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _PANEL_HANDLING_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where panel positioning confirmation results will be logged
   *
   * @author Reid Hayhow
   */
  public static String getMotionRepeatabilityConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _MOTION_REPEATABILITY_CONFIRMATION_DIR;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getFrontOpticalMeasurementRepeatabilityConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);
    
    return _releaseDir + File.separator + _FRONT_OPTICAL_MEASUREMENT_REPEATABILITY_CONFIRMATION_DIR;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getRearOpticalMeasurementRepeatabilityConfirmationLogDir()
  {
    Assert.expect(_releaseDir != null);
    
    return _releaseDir + File.separator + _REAR_OPTICAL_MEASUREMENT_REPEATABILITY_CONFIRMATION_DIR;
  }

  /**
   * Return the directory where the algorithm files exist.
   * AXI_XRAY_HOME must be set for this to work properly.
   * The directory returned is AXI_XRAY_HOME\config\algorithms
   * An Assert will be thrown if AXI_XRAY_HOME is not set.
   *
   * @return String containing the path name without the trailing slash
   * @author Keith Lee
   */
  public static String getAlgorithmsDir()
  {
    return getConfigDir() + File.separator + _ALGORITHMS_DIR;
  }

  /**
   * Return the directory where the config files exist.
   * AXI_XRAY_HOME must be set for this to work properly.
   * The directory returned is AXI_XRAY_HOME\calib
   * An Assert will be thrown if AXI_XRAY_HOME is not set.
   *
   * @return String containing the path name without the trailing slash
   * @author Bill Darbie
   */
  public static String getPropertiesDir()
  {
    return getConfigDir() + File.separator + _PROPERTIES_DIR;
  }

  /**
   * @return the relative properties directory (relative to AXI_XRAY_HOME)
   * @author Bill Darbie
   */
  public static String getRelativePropertiesDir()
  {
    if (UnitTest.unitTesting() == false)
    {
      if(LicenseManager.isXXLEnabled())
        return _XXL_CONFIG_DIR + File.separator + _PROPERTIES_DIR;
      else if(LicenseManager.isS2EXEnabled())
        return _S2EX_CONFIG_DIR + File.separator + _PROPERTIES_DIR;
      else
        return _CONFIG_DIR + File.separator + _PROPERTIES_DIR;
    }
    else
      return _CONFIG_DIR + File.separator + _PROPERTIES_DIR;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPropertiesNonAsciiDir()
  {
    return getConfigDir() + File.separator + _PROPERTIES_NON_ASCII_DIR;
  }

  /**
   * Return the directory where the log files exist.
   * AXI_XRAY_HOME must be set for this to work properly.
   * The directory returned is AXI_XRAY_HOME\log
   * An Assert will be thrown if AXI_XRAY_HOME is not set.
   *
   * @return String containing the path name without the trailing slash
   * @author Bill Darbie
   */
  public static String getLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _LOG_DIR;
  }

  /**
   * Return the directory where the performance log files exist.
   * AXI_XRAY_HOME must be set for this to work properly.
   * The directory returned is AXI_XRAY_HOME\log
   * An Assert will be thrown if AXI_XRAY_HOME is not set.
   *
   * @return String containing the path name without the trailing slash
   * @author Bill Darbie
   */
  public static String getPerformanceLogDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _LOG_DIR;
  }

  /**
   * Return the directory where the documentation directory is.
   * AXI_XRAY_HOME must be set for this to work properly.
   * The directory returned is AXI_XRAY_HOME\docs
   * An Assert will be thrown if AXI_XRAY_HOME is not set.
   *
   * @return String containing the path name without the trailing slash
   * @author Bill Darbie
   */
  public static String getDocsDir()
  {
    Assert.expect(_releaseDir.startsWith("..") == false, "ERROR: AXI_XRAY_HOME cannot be a relative directory");
    boolean http = false; // for now do not use http - eventually we will ship with http server
    String docsDir = "";
    if (http)
    {
      // do not add _DOCS_DIR for this case since the apache web server
      // should be pointing to rXX/docs for html files
      docsDir = "http://localhost";
    }
    else
    {
      Assert.expect(_releaseDir != null);
      docsDir = "file:/" + _releaseDir + File.separator + _DOCS_DIR;
    }


    Assert.expect(docsDir != null);
    return docsDir;
  }

  /**
   * @return the location of the x6000 directory for Help files.
   *
   * @author George Booth
   */
  public static String getHelpDir()
  {
    return getDocsDir();
  }

  /**
   * @return the location of the x6000 directory for Operator Help files.
   *
   * @author George Booth
   */
  public static String getOperatorHelpDir()
  {
    return getDocsDir() + File.separator + _OPERATOR_HELP_DIR;
  }

  /**
   * @return the location of the v810 bin directory.
   *
   * @author Bill Darbie
   */
  public static String getBinDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _BIN_DIR;
  }

  /**
   * @return the location of the psExec Tools directory.
   *
   * @author Chong, Wei Chin
   */
  public static String getPsToolsDir()
  {
    Assert.expect(_releaseDir != null);

    return getBinDir() + File.separator + _PSTOOLS_DIR;
  }

  /**
   * @return String
   *
   * @author Wei Chin, Chong
   */
  public static String getPackageLibraryDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir + File.separator + _PACKAGE_LIBRARIES_DIR;
  }

  /**
   * Return the directory where the service documentation exists.
   *
   * @author Bill Darbie
   */
  public static String getServiceDocsDir()
  {
    return getDocsDir() + File.separator + _SERVICE_DIR;
  }

  /**
   * Return the directory where the Hardware Status and Control documentation exists.
   *
   * @author Andy Mechtenberg
   */
  public static String getHwStatusDocsDir()
  {
    return getDocsDir() + File.separator + _HWSTATUS_DOCS_DIR;
  }

  /**
   * @return the directory where the ndf files are stored for panel information.
   * @author Bill Darbie
   * @author Jack Hwee
   */
  public static String getLegacyNdfPanelDir(String projectName)
  {
    Assert.expect(projectName != null);

    if (UnitTest.unitTesting())
    {
      return getLegacyNdfDir() + File.separator + projectName;
    }

    if ((NdfReader.is5dxNdfFlag()) || (NdfReader.is5dxProjectFlag()))
    {
      return getLegacyNdfDir() + File.separator + projectName;
    }
    else
    {
      return getLegacyNdfDir() + File.separator + HashUtil.hashName(projectName);
    }
  }

  /**
   * @return the backup directory for the project name passed in.
   * @author Bill Darbie
   */
  public static String getLegacyNdfBackupDir(String projectName)
  {
    String panelDir = getLegacyNdfPanelDir(projectName);

    return panelDir + File.separator + _NDF_BACKUP_DIR;
  }

  /**
   * @return the directory where the ndf files are stored for board information.
   * @author Bill Darbie
   * @author Jack Hwee
   */
  public static String getLegacyNdfBoardDir(String projectName, String sideBoardName)
  {
    Assert.expect(projectName != null);
    Assert.expect(sideBoardName != null);

    if (UnitTest.unitTesting())
    {
      return getLegacyNdfDir() + File.separator + projectName + File.separator + sideBoardName;
    }

    if ((NdfReader.is5dxNdfFlag()) || (NdfReader.is5dxProjectFlag()))
    {
      return getLegacyNdfDir() + File.separator + projectName + File.separator + HashUtil.hashName(sideBoardName);
    }
    else
    {
      return getLegacyNdfDir() + File.separator + HashUtil.hashName(projectName) + File.separator + HashUtil.hashName(sideBoardName);
    }

  }

  /**
   * @return the directory where all the ndf files are.
   * @author Horst Mueller
   */
  public static String getLegacyNdfDir()
  {
    String legacyNdfDir = null;
    if (UnitTest.unitTesting() == false)
    {
      legacyNdfDir = _config.getStringValue(SoftwareConfigEnum.NDF_LEGACY_DIR);
      legacyNdfDir = shortenPath(legacyNdfDir);
    }
    else
    {
      legacyNdfDir = _rootDir + File.separator + _LEGACY_NDF_DIR;
    }

    Assert.expect(legacyNdfDir != null);
    return legacyNdfDir;
  }

  /**
   * Set the legacy ndf directory
   * @param legacyNdfDir the path to the legacy ndf directory
   * @author George A. David
   */
  public static void setLegacyNdfDir(String legacyNdfDir) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.NDF_LEGACY_DIR, legacyNdfDir);
  }

  /**
   * @return a List of names (Stings) for each board directory of the given panel.
   * @author Bill Darbie
   */
  public static List<String> getLegacyNdfBoardDirs(String projectName)
  {
    List<String> dirs = new ArrayList<String>();
    String panelDir = getLegacyNdfPanelDir(projectName);
    File file = new File(panelDir);
    File[] files = file.listFiles();
    if (files != null)
    {
      for (int i = 0; i < files.length; ++i)
      {
        file = files[i];
        if (file.isDirectory())
        {
          dirs.add(file.getAbsolutePath());
        }
      }
    }

    return dirs;
  }

  /**
   * @return the directory where the res files are stored.
   * @author Steve Anonson
   */
  public static String getResultsDir()
  {
    String resultsDir = null;
    if (UnitTest.unitTesting() == false)
    {
      resultsDir = _config.getStringValue(SoftwareConfigEnum.RESULTS_DIR);
      resultsDir = shortenPath(resultsDir);
    }
    else
    {
      resultsDir = _rootDir + File.separator + _RESULTS_DIR;
    }

    return resultsDir;
  }

  /**
   * @author George A. David
   */
  public static void setResultsDir(String resultsDir) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.RESULTS_DIR, resultsDir);
  }

  /**
   * @return the v810 root directory
   * @author Bill Darbie
   */
  public static String getXrayTesterRootDir()
  {
    Assert.expect(_rootDir != null);

    return _rootDir;
  }

  /**
   * @return the directory that the projects are stored in.
   * @author Bill Darbie
   */
  public static String getProjectsDir()
  {
    String projectsDir = null;
    if (UnitTest.unitTesting() == false)
    {
      projectsDir = _config.getStringValue(SoftwareConfigEnum.PROJECT_DIR);
      projectsDir = shortenPath(projectsDir);
    }
    else
    {
      projectsDir = _rootDir + File.separator + _PROJECTS_DIR;
    }

    return projectsDir;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static String getProductionTuneProjectsDir(String defaultDestination)
  {
    return defaultDestination + File.separator + _PROJECTS_DIR;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static String getProductionTuneResultsDir(String defaultDestination)
  {
    return defaultDestination + File.separator + _RESULTS_DIR;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getProductionTuneUserDefinedDir(String defaultDestination, String projectName)
  {
    long currentTime = System.currentTimeMillis();
    return defaultDestination + File.separator + projectName + Directory.getDirName(currentTime);
  }
  
  /**
   * XCR-3139 Allow user to change verification image path
   * @author Kee Chin Seong - Verification Images Setup
   */
  public static void setVerificationImagesDir(String verificationImagesDir) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.VERIFICATION_DIR, verificationImagesDir);
  }

  /**
   * @author George A. David
   */
  public static void setProjectsDir(String projectsDir) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.PROJECT_DIR, projectsDir);
  }

  /**
   * @return the x6000 release directory
   */
  public static String getXrayTesterReleaseDir()
  {
    Assert.expect(_releaseDir != null);

    return _releaseDir;
  }

  /**
   * @return the drive letter where the x6000 code is installed
   * @author Bill Darbie
   */
  public static String getXrayTesterDriveLetter()
  {
    Assert.expect(_releaseDir != null);
    int index = _releaseDir.indexOf(':');
    Assert.expect(index == 1);

    return new String(new char[] {_releaseDir.charAt(0)} );
  }

  /**
   * @return the directory where the orignal ndf and rtf files are stored during a clean
   * operation.
   * @author Bill Darbie
   */
  public static String getLegacyOrigDir(String projectName)
  {
    String panelDir = getLegacyNdfPanelDir(projectName);

    return panelDir + File.separator + _LEGACY_NDF_ORIG_DIR;
  }

  /**
   * @return the name of the backup directory used
   * @author Erica Wheatcroft
   */
  private static String createBackupOfUserNdfDirectory(String projectName) throws DatastoreException
  {
    File ndfDirectory = new File(Directory.getLegacyNdfPanelDir(projectName));
    File backupDir = new File(Directory.getLegacyNdfPanelDir(projectName) + File.separator + _NDF_BACKUP_DIR + File.separator);

    if (backupDir.mkdir() == false)
    {
      throw new CannotCreateFileDatastoreException(backupDir.getPath());
    }

    List<String> dontMoveFiles = new ArrayList<String>();
    dontMoveFiles.add(getLegacyOrigDir(projectName));
    FileUtilAxi.moveDirectoryContents(ndfDirectory.getPath(), backupDir.getPath(), dontMoveFiles);

    return backupDir.getPath();
  }

  /**
   * @return the name of the backup directory
   * @param the panel name we need to temporary back up of files for
   * @author Erica Wheatcroft
   */
  private static String createTempBackupOfNdfDirectory(String projectName) throws DatastoreException
  {
    File ndfDirectory = new File(Directory.getLegacyNdfPanelDir(projectName));
    File tempBackupDir = new File(Directory.getLegacyNdfPanelDir(projectName) + File.separator + _NDF_TEMP_BACKUP_DIR + File.separator);

    // if one is still sitting around, delete it
    if (tempBackupDir.exists())
    {
      FileUtilAxi.delete(tempBackupDir.getPath());
    }

    if (tempBackupDir.mkdir() == false)
    {
      throw new CannotCreateFileDatastoreException(tempBackupDir.getPath());
    }

    List<String> dontMoveFiles = new ArrayList<String>();
    dontMoveFiles.add(Directory.getLegacyNdfPanelDir(projectName) + File.separator + _NDF_BACKUP_DIR + File.separator);
    dontMoveFiles.add(Directory.getLegacyOrigDir(projectName));
    FileUtilAxi.moveDirectoryContents(ndfDirectory.getPath(), tempBackupDir.getPath(), dontMoveFiles);

    return tempBackupDir.getPath();
  }

  /**
   * @return the name of the backup directory
   * @param projectName - the panel name we need to restore Ndfs for
   * @return the name of the backup directory that was used
   * @author Erica Wheatcroft
   */
  public static String moveNdfDirectoryToBackupDirectory(String projectName) throws DatastoreException
  {
    String backupDir;
    File backUpDir = new File(Directory.getLegacyNdfPanelDir(projectName) + File.separator + _NDF_BACKUP_DIR + File.separator);

    if (backUpDir.exists())
    {
      backupDir = createTempBackupOfNdfDirectory(projectName);
    }
    else
    {
      backupDir = createBackupOfUserNdfDirectory(projectName);
    }

    return backupDir;
  }

  /**
   * @author Bill Darbie
   */
  public static void removeTempBackupNdfDirectory(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);
    File tempBackupDir = new File(Directory.getLegacyNdfPanelDir(projectName) + File.separator + _NDF_TEMP_BACKUP_DIR + File.separator);
    if (tempBackupDir.exists())
    {
      FileUtilAxi.delete(tempBackupDir.getPath());
    }
  }

  /**
   * @param projectName the panel name we need to restore Ndfs for
   * @author Erica Wheatcroft
   */
  public static void restoreNdfDirectory(String projectName) throws DatastoreException
  {
    File ndfDirectory = new File(Directory.getLegacyNdfPanelDir(projectName));
    File backupDir = new File(Directory.getLegacyNdfPanelDir(projectName) + File.separator + _NDF_BACKUP_DIR + File.separator);
    File tempBackupDir = new File(Directory.getLegacyNdfPanelDir(projectName) + File.separator + _NDF_TEMP_BACKUP_DIR + File.separator);

    if (backupDir.exists())
    {
      if (tempBackupDir.exists())
      {
        //we will restore up from the temp Dir
        File[] tempFileList = tempBackupDir.listFiles();
        for (int i = 0; i < tempFileList.length; ++i)
        {
          File currentFile = tempFileList[i];
          String name = currentFile.getName();
          File restoredFile = new File(ndfDirectory.getPath() + File.separator + name);
          FileUtilAxi.rename(currentFile.getPath(), restoredFile.getPath());
        }
        FileUtilAxi.delete(tempBackupDir.getPath());
      }
      else
      {
        //since the temp dir does not exist, restore from the backup dir
        File[] backupFileList = backupDir.listFiles();
        for (int i = 0; i < backupFileList.length; i++)
        {
          File currentFile = backupFileList[i];
          String name = currentFile.getName();
          File restoredFile = new File(ndfDirectory.getPath() + File.separator + name);
          FileUtilAxi.rename(currentFile.getPath(), restoredFile.getPath());
        }
        //blow away the backup directory
        FileUtilAxi.delete(backupDir.getPath());
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  public static String getTempDir()
  {
    String tempDir = _config.getStringValue(SoftwareConfigEnum.TEMP_DIR);
    tempDir = shortenPath(tempDir);

    if (UnitTest.unitTesting())
    {
      // for unittesting we never want to write to the same directory from different
      // views, so this prevents that
      String viewName = System.getenv("VIEW_NAME");
      if (viewName != null)
      {
        tempDir += File.separator + viewName;
      }
    }

    Assert.expect(tempDir != null);
    return tempDir;
  }

  /**
   * @author Matt Wharton
   */
  public static String getTempImageCacheDir()
  {
    return getTempDir() + File.separator + _TEMP_IMAGE_CACHE_DIR;
  }

  /**
   * @author Rex Shang
   */
  public static String getOperatingSystemDir()
  {
    String sysDir = System.getenv("SystemRoot");
    Assert.expect(sysDir != null, "SystemRoot system env variable is null.");

    return sysDir;
  }

  /**
   * @author Rick Gaudette
   */
  public static String getProgramDataDir()
  {
    String program_data_dir = System.getenv("ProgramData");
    Assert.expect(program_data_dir != null, "ProgramData system env variable is null.");

    return program_data_dir;
  }


  /**
   * @author Rick Gaudette
   */
  public static String getVitroxProgramDataDir()
  {
    String vitrox_program_data_dir = getProgramDataDir() + File.separator + "ViTrox"  + File.separator + "AXI";
    return vitrox_program_data_dir;
  }

  /**
   * @return the test credit license directory
   * @author Eugene Kim-Leighton
   * @author Erica Wheatcroft
   */
  public static String getLicenseDir()
  {
    return Directory.getConfigDir() + File.separator + _LICENSE_DIR;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPrevLicenseDir()
  {
    return Directory.getPrevConfigDir() + File.separator + _LICENSE_DIR;
  }

  /**
   * @return the location of the images directory
   * @author Bill Darbie
   */
  public static String getGuiImagesDir()
  {
    return getConfigDir() + File.separator + _GUI_IMAGES_DIR;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPrevGuiImagesDir()
  {
    return getPrevConfigDir() + File.separator + _GUI_IMAGES_DIR;
  }

  /**
   * @return the location of the joint thumbnails directory for fake image run generation
   * @author Matt Wharton
   */
  public static String getJointThumbnailsDir()
  {
    return getGuiImagesDir() + File.separator + _JOINT_THUMBNAILS_DIR;
  }
  
  /**
   * XCR-3735 Failed to open v810 application after patch installation due to thumbs.db lock it
   * @author Cheah Lee Herng
   */
  public static String getPrevJointThumbnailsDir()
  {
    return getPrevGuiImagesDir() + File.separator + _JOINT_THUMBNAILS_DIR;
  }

  /**
   * @return the relative path (relative to AXI_XRAY_HOME) of the images directory
   * @author Bill Darbie
   */
  public static String getRelativeGuiImagesDir()
  {
    return getConfigDir() + File.separator + _GUI_IMAGES_RELATIVE_DIR;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static String getAlignmentImagesDir()
  {
    String imagesDir = getTempDir() + File.separator + _ONLINE_XRAY_IMAGES_DIR;
    return imagesDir;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getAlignmentOpticalImagesDir()
  {
    String imagesDir = getTempDir() + File.separator + _ONLINE_OPTICAL_IMAGES_DIR;
    return imagesDir; 
  }

  /**
   * @author Matt Wharton
   */
  public static String getFailingAlignmentImagesDir()
  {
    String failingAlignmentImagesDir = getTempDir() + File.separator + _FAILING_ALIGNMENT_IMAGES_DIR;
    return failingAlignmentImagesDir;
  }

  /**
   * @author John Heumann
   */
  public static String getFocusConfirmationImagesDir(long timeStampInMilis)
  {
    String focusConfirmationImagesDir = getTempDir() + File.separator + _FOCUS_CONFIRMATION_IMAGES_DIR + "_" + getDirName(timeStampInMilis);
    return focusConfirmationImagesDir;
  }

  /**
   * @author Matt Wharton
   */
  public static String getSavedAlignmentImagesDir()
  {
    String savedAlignmentImagesDir = getTempDir() + File.separator + _SAVED_ALIGNMENT_IMAGES_DIR;
    return savedAlignmentImagesDir;
  }

  /**
   * @todo MDW remove later!
   *
   * @author Matt Wharton
   */
  public static String getCallRateSpikeAlignmentImagesDir()
  {
    String failingAlignmentImagesDir = getTempDir() + File.separator + _CALL_RATE_SPIKE_ALIGNMENT_IMAGES_DIR;
    return failingAlignmentImagesDir;
  }

  /**
   * Get the location where we store a single set of images when online.
   * @return String directory path where images are stored online
   * @author Kay Lannen
   */
  public static String getOnlineXrayImagesDir()
  {
    String imagesDir = getTempDir() + File.separator + _ONLINE_XRAY_IMAGES_DIR;
    return imagesDir;
  }

  /**
   * @author George A. David
   */
  public static String getOnlineXrayImagesDir(String imageSetName)
  {
    String imagesDir = getTempDir() + File.separator + _ONLINE_XRAY_IMAGES_DIR + File.separator + imageSetName;
    return imagesDir;
  }

  /**
   * @author George A. David
   */
  public static String getXrayInspectionImagesDir(String projectName)
  {
    return getXrayImagesDir(projectName) + File.separator + _INSPECTION_IMAGES_DIR;
  }

  /**
   * @author George A. David
   */
  public static String getAdjustFocusImagesDir(String projectName)
  {
    return getXrayImagesDir(projectName) + File.separator + _ADJUST_FOCUS_IMAGES_DIR;
  }

  /**
   * @author George A. David
   */
  public static String getXrayInspectionImagesDir(String projectName, String imageSetName)
  {
    Assert.expect(imageSetName != null);

    return getXrayImagesDir(projectName) + File.separator + _INSPECTION_IMAGES_DIR + File.separator + imageSetName;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalImagesDir(String projectName, String imageSetName)
  {
    Assert.expect(imageSetName != null);
    
    return getOpticalImagesDir(projectName) + File.separator + _INSPECTION_IMAGES_DIR + File.separator + imageSetName; 
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getOfflineOpticalImagesDir(String projectName)
  {
    Assert.expect(projectName != null);
    
    return getOpticalImagesDir(projectName) + File.separator + _INSPECTION_IMAGES_DIR + File.separator + _PSP_OFFLINE_IMAGES_DIR;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static String getTargetXrayInspectionImagesDir(String baseLocation, String projectName, String imageSetName)
  {
    Assert.expect(baseLocation != null);
    Assert.expect(projectName != null);
    Assert.expect(imageSetName != null);

    return baseLocation + File.separator + projectName + File.separator + _IMAGES_DIR + File.separator + _INSPECTION_IMAGES_DIR + File.separator + imageSetName;
  }


  /**
   * @author Andy Mechtenberg
   */
  public static String getOnlineXrayVerificationImagesDir()
  {
    String verificationImagesDir = "";
    verificationImagesDir = _rootDir + File.separator + _IMAGES_DIR +  File.separator + _VERIFICATION_IMAGES_DIR;
    return verificationImagesDir;
  }
  
  /**
   * XCR-3139 Allow user to change verification image path
   * @author Kee Chin Seong
   */
  public static String getXrayVerificationImagesDirectory()
  {
    String verificationImagesDir = null;
    if (UnitTest.unitTesting() == false)
    {
      verificationImagesDir = _config.getStringValue(SoftwareConfigEnum.VERIFICATION_DIR);
      verificationImagesDir = shortenPath(verificationImagesDir);
    }
    else
    {
      verificationImagesDir = _rootDir + File.separator + _PROJECTS_DIR  ; 
    }
    
    return verificationImagesDir;
  }
  
  /**
   * Get online verification images directory from project directory
   * @author Ying-Huan.Chu
   * @Edited by Kee Chin Seong XCR-3139 Allow user to change verification image path
   */
  public static String getOnlineXrayVerificationImagesDirFromProject(String projectName)
  {
    String verificationImagesDir = "";
    verificationImagesDir = getXrayImagesDir(projectName) + File.separator + _VERIFICATION_IMAGES_DIR;
    return verificationImagesDir;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static String getOfflineXrayVerificationImagesDir(String projectName)
  {
    String verificationImagesDir = "";
    // on the TDW we want to store verification images under x6000/projects/projectName/verification
    // on the x6000 system we do not want to take the disk space to save under each project, so
    // we we use x6000/verification
    verificationImagesDir = getXrayImagesDir(projectName) + File.separator + _VERIFICATION_IMAGES_DIR;

    return verificationImagesDir;
  }
  
  /**
   * @author George A. David
   * @edited by Kee Chin Seong XCR-3139 Allow user to change verification image path
   */
  public static String getXrayVerificationImagesDir(String projectName)
  {
    String verificationImagesDir = "";
    // on the TDW we want to store verification images under x6000/projects/projectName/verification
    // on the x6000 system we do not want to take the disk space to save under each project, so
    // we we use x6000/verification
   // if (_config.getBooleanValue(SoftwareConfigEnum.ONLINE_WORKSTATION))
      // Ying-Huan.Chu [XCR1704]
      //verificationImagesDir = getOnlineXrayVerificationImagesDir();
    //  verificationImagesDir = getOnlineXrayVerificationImagesDirFromProject(projectName);
    //else
     // verificationImagesDir = getOfflineXrayVerificationImagesDir(projectName);
//    if (_config.getBooleanValue(SoftwareConfigEnum.ONLINE_WORKSTATION))
//      verificationImagesDir = _rootDir + File.separator + _IMAGES_DIR +  File.separator + _VERIFICATION_IMAGES_DIR;
//    else
//      verificationImagesDir = getXrayImagesDir(projectName) + File.separator + _VERIFICATION_IMAGES_DIR;

    verificationImagesDir = getXrayVerificationImagesDirectory() + File.separator + projectName +  File.separator + _IMAGES_DIR +  File.separator + _VERIFICATION_IMAGES_DIR;
    
    return verificationImagesDir;
  }
  
  /**
   * @author George A. David
   */
  public static String getXrayVirtualLiveImagesDir(String projectName)
  {
    Assert.expect(projectName != null);

    return getXrayImagesDir(projectName) + File.separator + _VIRTUAL_LIVE_IMAGES_DIR;
  }

  /**
   * @author Bill Darbie
   */
  public static List<String> getInspectionResultsDirs(String projectName)
  {
    List<String> dirs = new ArrayList<String>();
    String dir = getResultsDir(projectName);
    for (String fileName : FileUtil.listAllSubDirectoriesInDirectory(dir))
    {
      Matcher matcher = _inspectionRunDirPattern.matcher(fileName);
      if (matcher.matches())
      {
        dirs.add(dir + File.separator + fileName);
      }
    }

    return dirs;
  }
  
   /**
   * @author hsia-fen.tan
   */
    public static List<String> getProjectHistoryFileDirs(String projectName)
  {
    List<String> dirs = new ArrayList<String>();
    if(Project.isCurrentProjectLoaded()==true)
    projectName= Project.getCurrentlyLoadedProject().getName();
    String dir = getHistoryFileDir(projectName);
    Pattern _HistoryDirPattern = Pattern.compile(projectName + "-(\\d+)-(\\d+)-(\\d+)_(\\d+)-(\\d+)-(\\d+)-(\\d+).history");
    for (String fileName : FileUtil.listAllFilesInDirectory(dir))
    {
      Matcher matcher = _HistoryDirPattern.matcher(fileName);
      if (matcher.matches())
      {
        dirs.add(dir + File.separator + fileName);
      }
    }
    return dirs;
  }
  //Kok Chun, Tan - XCR-2022
  //This method is slow when searching the .results files. (Using new method)
   /**
   * @author Bill Darbie
   */
//  public static List<Long> getInspectionResultsStartTimesInMillis(String projectName)
//  {
//    Assert.expect(projectName != null);
//
//    Set<Long> times = new TreeSet<Long>();
//    String dir = getResultsDir(projectName);
//    for (String dirName : FileUtil.listAllSubDirectoriesFullPathInDirectory(dir))
//    {
//      String fileName = dirName;
//      int index = fileName.lastIndexOf(File.separator);
//      if (index != -1)
//        fileName = fileName.substring(index + 1, fileName.length());
//      Matcher matcher = _inspectionRunDirPattern.matcher(fileName);
//      if (matcher.matches())
//      {
//        // we have found a directory named inspectionRun_, now make sure that it has at least one
//        // file ending in .results
//        Collection<String> files = FileUtilAxi.listAllFilesInDirectory(dirName);
//        boolean resultsFileFound = false;
//        for (String file : files)
//        {
//          if (file.endsWith(FileName.getResultsExtension()))
//          {
//            resultsFileFound = true;
//            break;
//          }
//        }
//        if (resultsFileFound)
//          times.add(Directory.getTimeStamp(dirName));
//      }
//    }
//
//    // reverse the list so that the first one is the most recent.
//    List<Long> timesList = new ArrayList<Long>(times);
//    Collections.reverse(timesList);
//    return timesList;
//  }
  
  /**
   * XCR-2022
   * @author - Kok Chun, Tan
   * This method to search the .results files from directory is faster
   */
  public static List<Long> getInspectionResultsStartTimesInMillis(String projectName)
  {
    Assert.expect(projectName != null);

    Set<Long> times = new TreeSet<Long>();
    File dir = new File(getResultsDir(projectName));

    File[] subdirs = dir.listFiles(new FilenameFilter()
    {
      public boolean accept(File dir, String name)
      {
        return name.matches(_inspectionRunFolderPattern);
      }
    });

    if (subdirs != null)
    {
      for (File subdir : subdirs)
      {
        File[] files = subdir.listFiles(new FilenameFilter()
        {
          public boolean accept(File dir, String name)
          {
            return name.endsWith(FileName.getResultsExtension());
          }
        });
        // XCR-3179 Assert When Viewing Result
        if (files != null)
        {
          for (File file : files)
          {
            times.add(Directory.getTimeStamp(subdir.getName()));
          }
        }
      }
    }
    
    List<Long> timesList = new ArrayList<Long>(times);
    Collections.reverse(timesList);
    return timesList;
  }
    
  /**
   * @author Bill Darbie
   */
  public static String getInspectionResultsDir(String projectName, long timeStampInMillis)
  {
    return getResultsDir(projectName) + File.separator + _INSPECTION_RUN_BASE_DIR + getDirName(timeStampInMillis);
  }

  /**
   * @author Andy Mechtenberg
   */
  public static String getInspectionResultsBaseDir(String projectName, long timeStampInMillis)
  {
    return projectName + File.separator + _INSPECTION_RUN_BASE_DIR + getDirName(timeStampInMillis);
  }

  //get History Directory- hsia-fen.tan
   public static String getHistoryFileDir(String projectName)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    String historyDir = null;
    
    if (_config.getBooleanValue(SoftwareConfigEnum.USE_SPECIFIC_HISTORY_LOG_DIR))
    {
      if (UnitTest.unitTesting() == false)
      {
        historyDir = _config.getStringValue(SoftwareConfigEnum.SPECIFIC_HISTORY_LOG_DIR) + File.separator + projectName;
        historyDir = shortenPath(historyDir);
      }
      else
      {
        historyDir = _rootDir + File.separator + _SPECIFIC_HISTORY_LOG_DIR;
      } 
    }
    else
    {
      historyDir = getProjectDir(projectName) + File.separator + _HISTORY_FILE_DIR;  
    }
    
    return historyDir;
  }

  /**
   * Given a full path to an image set, return the image set root with out the trailing File.Seperator
   * @author Erica Wheatcroft
   */
  public static String getXrayImageSetRootDir(String imageSetFullPath, String imageSetName)
  {
    Assert.expect(imageSetFullPath != null);
    Assert.expect(imageSetName != null);
    Assert.expect(imageSetFullPath.contains(imageSetName), "Invalid path specified");
    int index = imageSetFullPath.lastIndexOf(imageSetName);

    String imageSetRoot = imageSetFullPath.substring(0, index - 1);

    return imageSetRoot;
  }

  /**
   * @author George A. David
   */
  public static List<String> getOfflineXrayImageSetRootDirs()
  {
    return shortenPaths(_config.getStringListValue(SoftwareConfigEnum.OFFLINE_IMAGE_SETS_DIRS));
  }

  /**
   * @author Erica Wheatcroft
   */
  public static void setOfflineXrayImageSetRootDirs(List<String> offlineXrayImageSetRootDirectories) throws
      DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.OFFLINE_IMAGE_SETS_DIRS, offlineXrayImageSetRootDirectories);
  }

  /**
   * @author George A. David
   */
  public static String getXrayImagesDir(String projectName)
  {
    Assert.expect(projectName != null);

    return getProjectDir(projectName) + File.separator + _IMAGES_DIR;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getOpticalImagesDir(String projectName)
  {
    return getProjectDir(projectName) + File.separator + _OPTICAL_IMAGES_DIR;
  }

  /**
   * Get the motion control home directory
   *
   * @return String containing the path name without the trailing slash
   * @author Greg Esparza
   */
  public static String getMotionControlDir()
  {
    return Directory.getConfigDir() + File.separator + _MOTION_CONTROL_DIR;
  }

  /**
   * @author Erica Wheatcroft
   */
  public static String getMotionPatternDir()
  {
    return getMotionControlDir() + File.separator + _SERVICE_DIR + File.separator + _MOTION_PATTERN_DIR;
  }

  /**
   * Get the directory where we store the point to point move configuration files.
   * @author Erica Wheatcroft
   */
  public static String getPointToPointMoveDir()
  {
    return getMotionPatternDir() + File.separator + _POINT_TO_POINT_DIR;
  }

  /**
   * Get the directory where we store the scan move configuration files
   * @author Erica Wheatcroft
   */
  public static String getScantMoveDir()
  {
    return getMotionPatternDir() + File.separator + _SCAN_DIR;
  }
  
  /**
   * Returns the name of the a directory with the specified timestamp.
   * @param timestampMillis is the timestamp in milliseconds since the epoch (Jan. 1 1970)
   * @return String containing the local directory name
   * @author Bill Darbie
   */
  public static String getDirName(long timestampMillis)
  {
    Assert.expect(timestampMillis >= 0);
    return StringUtil.convertMilliSecondsToTimeStamp(timestampMillis);
  }

  /**
   * @author Bill Darbie
   */
  public static long getTimeStamp(String dirName)
  {
    Assert.expect(dirName != null);

    long timeInMillis = 0;
    // YYYY-MM-DD_HH-MM-SS-ss
    // YYYY = year
    // MM = month (Jan = 0)
    // DD = day (1 = first day of the month)
    // HH = hour
    // MM = minutes
    // SS = seconds
    // ss = milliseconds
    String fileName = dirName;
    int index = fileName.lastIndexOf(File.separator);
    if (index != -1)
      fileName = fileName.substring(index + 1, fileName.length());

    // remove "inspectionRun_"
    fileName = fileName.replaceFirst(_INSPECTION_RUN_BASE_DIR, "");
    try
    {
      timeInMillis = StringUtil.convertStringToTimeInMilliSeconds(fileName);
    }
    catch(BadFormatException bfe)
    {
      Assert.expect(false);
    }
    return timeInMillis;
  }

  public static long getTimeStampforHistoryLog(String project,String projectName)
  {
    if(Project.isCurrentProjectLoaded()==true)
    project=Project.getCurrentlyLoadedProject().getName();
    
    Assert.expect(project != null);

    long timeInMillis = 0;
    // YYYY-MM-DD_HH-MM-SS-ss
    // YYYY = year
    // MM = month (Jan = 0)
    // DD = day (1 = first day of the month)
    // HH = hour
    // MM = minutes
    // SS = seconds
    // ss = milliseconds
    String fileName = projectName;
    int index = fileName.lastIndexOf(File.separator);
    if (index != -1)
      fileName = fileName.substring(index + 1, fileName.length());

    // remove "ProjectName-.history"
    fileName = fileName.replaceFirst(project, ""); 
    fileName = fileName.replaceFirst("-", ""); 
    fileName = fileName.replaceFirst(".history", ""); 
    try
    {
      timeInMillis = StringUtil.convertStringToTimeInMilliSeconds(fileName);
    }
    catch (BadFormatException bfe)
    {
      Assert.expect(false);
    }
    return timeInMillis;
  }
  
  /**
   * Get the motion controller home directory
   *
   * @return String containing the path name without the trailing slash
   * @author Greg Esparza
   */
  public static String getMotionControllerDir()
  {
    return getMotionControlDir() + File.separator + _MOTION_CONTROLLER_DIR;
  }

  /**
   * Get the motion drive home directory
   *
   * @return String containing the path name without the trailing slash
   * @author Greg Esparza
   */
  public static String getMotionDriveDir()
  {
    return getMotionControlDir() + File.separator + _MOTION_DRIVE_DIR;
  }

  /**
   * @author Greg Esparza
   */
  public static String getSynqNetMotionDriveDir()
  {
    return getMotionDriveDir() + File.separator + _SYNQNET_MOTION_DRIVE_DIR;
  }

  /**
   * Get the kollmorge_cd motion drive directory
   *
   * @return String containing the path name without the trailing slash
   * @author Greg Esparza
   */
  public static String getKollmorgenCdMotionDriveDir()
  {
    return getSynqNetMotionDriveDir() + File.separator + _KOLLMORGEN_CD_MOTION_DRIVE_DIR;
  }

  /**
   * @author George A. David
   */
  public static String getProjectDatabaseDir()
  {
    return _config.getStringValue(SoftwareConfigEnum.PROJECT_DATABASE_PATH);
  }

  /**
   * Set the project repository directory. The project repository is a central
   * location where all finished products exists. Users can add projects to
   * the repository as they wish. The TDT will check the project repository
   * to see if a new version of a project being loaded is available.
   *
   * @author George A. David
   */
  public static void setProjectDatabaseDir(String projectDatabaseDir) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.PROJECT_DATABASE_PATH, projectDatabaseDir);
  }

  /**
   * get the path containing the project files
   *
   * @param projectName the name of the project
   * @author George A. David
   */
  public static String getProjectDir(String projectName)
  {
    Assert.expect(projectName != null);

    String projectsDir = getProjectsDir();

    //Ngie Xing, PCR-44, Software Crash When Zipping Recipes from Drive root location
    //we'll get D:\\ if the recipes directory is at D:\ drive.
    char lastChar = projectsDir.charAt(projectsDir.length() - 1);
    if (lastChar == '\\')
    {
      return projectsDir + projectName;
    }

    return projectsDir + File.separator + projectName;
  }
   
   /**
   * get the path containing the history files
   *
   * @param projectName the name of the project
   * @author hsia-fen.tan
   */
  public static String getProjectHistoryDir(String projectName)
  {
    Assert.expect(projectName != null);
    return getProjectsDir() + File.separator + projectName + Directory.getHistoryFileDir(projectName);
  }

   /**
   * get the path containing the 5dx project files
   *
   * @param projectName the name of the project
   * @author Jack Hwee
   */
  public static String get5dxProjectDir(String projectName)
  {
    Assert.expect(projectName != null);
    return getLegacyNdfDir() + File.separator + projectName;
  }

  /**
   * @author Laura Cormos
   */
  public static String getResultsDir(String projName)
  {
    Assert.expect(projName != null);

    return getResultsDir() + File.separator + projName;
  }

  /**
   * Set the path to the offline workstation. In order to free up the system
   * for actual testing, the user can chooses to work offline. This will
   * transfer the project files and images over to a place of their choosing.
   * This is called the offline workstation. Here the user can then continue
   * developing a project without taking up time on the system itself.
   *
   * @param offlineWorkstationPath the path to the offline workstation
   * @author George A. David
   */
  public static void setOfflineWorkstationPath(String offlineWorkstationPath) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.OFFLINE_WORKSTATION_PATH, offlineWorkstationPath);
  }

  /**
   * get the path to the offline workstation.
   * @author George A. David
   */
  public static String getOfflineWorkstationPath()
  {
    return _config.getStringValue(SoftwareConfigEnum.OFFLINE_WORKSTATION_PATH);
  }

  /**
   * get the offline project directory
   * @param projectName the name of the project
   * @author George A. David
   */
  public static String getOfflineProjectDir(String projectName)
  {
    return getOfflineWorkstationPath() + File.separator + projectName;
  }

  /**
   * get the backup project directory
   * @author George A. David
   */
  public static String getBackupProjectDir(String projectName)
  {
    return getProjectDir(projectName) + File.separator + _BACKUP;
  }

  /**
   * get the offline backup project directory
   * @author George A. David
   */
  public static String getOfflineBackupProjectDir(String projectName)
  {
    return getOfflineProjectDir(projectName) + File.separator + _BACKUP;
  }

  /**
   * get the offline temporary project directory
   * @author George A. David
   */
  public static String getOfflineTempProjectDir(String projectName)
  {
    return getOfflineProjectDir(projectName) + File.separator + _TEMP;
  }

  /**
   * Gets the algortihm learning database path for the specified project name.
   *
   * @author Matt Wharton
   */
  public static String getAlgorithmLearningDir(String projectName)
  {
    return getProjectDir(projectName) + File.separator + _ALGORITHM_LEARNING_DIR;
  }

  /**
   * @author Bill Darbie
   */
  public static String getSerialNumberDir()
  {
    return getConfigDir() + File.separator + _SERIAL_NUM_DIR;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPrevSerialNumberDir()
  {
    return getPrevConfigDir() + File.separator + _SERIAL_NUM_DIR;
  }

  /**
   * Return the directory where the barcode reader config files exist.
   * AXI_XRAY_HOME must be set for this to work properly.
   *
   * @return String containing the path name without the trailing slash
   * @author Bob Balliew
   */
  public static String getBarcodeReaderDir()
  {
    return Directory.getConfigDir() + File.separator + _BARCODE_READER_DIR;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @param SystemType
   * @return 
   * 
   */
  public static String getBarcodeReaderDir(SystemTypeEnum SystemType)
  {
    return Directory.getConfigDirWithSystemType(SystemType)+ File.separator + _BARCODE_READER_DIR;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPrevBarcodeReaderDir()
  {
    return Directory.getPrevConfigDir() + File.separator + _BARCODE_READER_DIR;
  }

  /**
   * @return the relative barcode reader config directory (relative to the configuration directory)
   */
  public static String getRelativeBarcodeReaderDir()
  {
    return _BARCODE_READER_DIR;
  }

  /**
   * @author Bill Darbie
   */
  public static String getJointTypeAssignmentDir()
  {
    return getConfigDir() + File.separator + _JOINT_TYPE_ASSIGNMENT_DIR;
  }

  /**
   * @author Bill Darbie
   */
  public static String getPrevJointTypeAssignmentDir()
  {
    return getPrevConfigDir() + File.separator + _JOINT_TYPE_ASSIGNMENT_DIR;
  }
  
  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public static String getDefaultInitialRecipeSettingDir()
  {
    return Directory.getConfigDir() + File.separator + _INITIAL_RECIPE_SETTING_DIR;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public static String getInitialRecipeSettingDir()
  {
    String initialRecipeSettingDir = (String)_config.getValue(SoftwareConfigEnum.INITIAL_RECIPE_SETTING_FILEDIR);

    if (UnitTest.unitTesting() == false)
    {
      initialRecipeSettingDir = shortenPath(initialRecipeSettingDir);
    }

    return initialRecipeSettingDir;
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public static void setInitialRecipeSettingDir(String initialRecipsesettingDir) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.INITIAL_RECIPE_SETTING_FILEDIR, initialRecipsesettingDir);
  }

  /**
   * XCR-2895 : Initial recipes setting to speed up the programming time
   * @author weng-jian.eoh
   * @return 
   */
  public static String getPrevInitialRecipeSettingDir()
  {
    return getPrevConfigDir() + File.separator + _INITIAL_RECIPE_SETTING_DIR;
  }
  
  /**
   * @author Andy Mechtenberg
   */
  public static String getDefaultInitialThresholdsDir()
  {
    return Directory.getConfigDir() + File.separator + _INITIAL_THRESHOLDS_DIR;
  }

  /**
   * @author Andy Mechtenberg
   * @author Wei Chin, Chong
   */
  public static String getInitialThresholdsDir()
  {
    String initialThresholdDir = (String)_config.getValue(SoftwareConfigEnum.INITIAL_THRESHOLDS_FILEDIR);

    if (UnitTest.unitTesting() == false)
    {
      initialThresholdDir = shortenPath(initialThresholdDir);
    }

    return initialThresholdDir;
  }

  /**
   * @author Wei Chin, Chong
   */
  public static void setInitialThresholdsDir(String initialThresholdDir) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.INITIAL_THRESHOLDS_FILEDIR, initialThresholdDir);
  }

  /**
   * @author Andy Mechtenberg
   */
  public static String getPrevInitialThresholdsDir()
  {
    return getPrevConfigDir() + File.separator + _INITIAL_THRESHOLDS_DIR;
  }

  /**
   * @author Greg Esparza
   */
  public static String getXrayCameraCalibDir()
  {
    return Directory.getCalibDir() + getXrayCameraDir() + File.separator;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getOpticalCameraCalibDir(int cameraId)
  {
    return Directory.getCalibDir() + getOpticalCameraIdDir(cameraId) + File.separator;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getOpticalCameraCalibProfileDir(int cameraId, int profileId)
  {
    return Directory.getCalibDir() + getOpticalCameraIdDir(cameraId) + getOpticalCameraProfileDir(profileId) + File.separator;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalCalibrationProfileFrontModuleDir()
  {
    //Ying-Huan.Chu - temporary commented Larger FOV feature for 5.7 release.
    //return Directory.getCalibDir() + File.separator + _OPTICAL_CALIBRATION_DIR + File.separator + _SMALL_FOV_DIR + File.separator + _FRONT_MODULE_DIR;
    return Directory.getCalibDir() + File.separator + _OPTICAL_CALIBRATION_DIR + File.separator + _FRONT_MODULE_DIR;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static String getOpticalCalibrationProfileRearModuleDir()
  {
    //Ying-Huan.Chu - temporary commented Larger FOV feature for 5.7 release.
    //return Directory.getCalibDir() + File.separator + _OPTICAL_CALIBRATION_DIR + File.separator + _SMALL_FOV_DIR + File.separator + _REAR_MODULE_DIR;
    return Directory.getCalibDir() + File.separator + _OPTICAL_CALIBRATION_DIR + File.separator + _REAR_MODULE_DIR;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static String getLargerFovOpticalCalibrationProfileFrontModuleDir()
  {
    return Directory.getCalibDir() + File.separator + _OPTICAL_CALIBRATION_DIR + File.separator + _LARGE_FOV_DIR + File.separator + _FRONT_MODULE_DIR;
  }
  
  /**
   * @author Ying-Huan.Chu
   */
  public static String getLargerFovOpticalCalibrationProfileRearModuleDir()
  {
    return Directory.getCalibDir() + File.separator + _OPTICAL_CALIBRATION_DIR + File.separator + _LARGE_FOV_DIR + File.separator + _REAR_MODULE_DIR;
  }

  /**
   * @author Greg Esparza
   */
  public static String getXrayCameraConfigDir()
  {
    return Directory.getConfigDir() + getXrayCameraDir() + File.separator;
  }

  /**
   * @author Greg Esparza
   */
  public static String getAgilentXrayCameraConfigDir()
  {
    return getXrayCameraConfigDir() + _AXI_DIR + File.separator;
  }

  /**
   * @author Greg Esparza
   */
  public static String getAgilentTdiXrayCameraBoardVersionConfigDir(String version)
  {
    Assert.expect(version != null);
    Assert.expect(version.length() > 0);

    return getAgilentXrayCameraConfigDir() + _AXI_TDI_XRAY_CAMERA_BOARD_VERSION_PREFIX_CONFIG_DIR + version + File.separator;
  }

  /**
   * @author Greg Esparza
   */
  public static String getStageHysteresisDir()
  {
    return File.separator + _STAGE_HYSTERESIS_DIR;
  }

  /**
   * @author Greg Esparza
   */
  public static String getSystemFiducialDir()
  {
    return File.separator + _SYSTEM_FIDUCIAL_DIR;
  }

  /**
   * @author Greg Esparza
   */
  public static String getXraySpotDir()
  {
    return File.separator + _XRAY_SPOT_DIR;
  }

  /**
   * @author Greg Esparza
   */
  public static String getXrayCameraArrayDir()
  {
    return File.separator + _XRAY_CAMERA_ARRAY_DIR;
  }

  /**
   * @author Greg Esparza
   */
  public static String getXrayCameraDir()
  {
    return File.separator + _XRAY_CAMERA_DIR;
  }

  /**
   * @author Cheah Lee Herng
   */
  public static String getOpticalCameraDir()
  {
    return File.separator + _OPTICAL_CAMERA_DIR;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getOpticalCameraIdDir(int cameraId)
  {
    return File.separator + _OPTICAL_CAMERA_DIR + cameraId;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getOpticalPlaneDir(int planeId)
  {
    return File.separator + _OPTICAL_PLANE_DIR + planeId;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static String getOpticalCameraProfileDir(int profileId)
  {
    return File.separator + _OPTICAL_CAMERA_PROFILE_DIR + profileId;
  }

  /**
   * @author Greg Esparza
   */
  public static String getXrayCameraLogDir()
  {
    return getLogDir() + getXrayCameraDir();
  }

  /**
   * @author Cheah Lee Herng
   */
  public static String getOpticalCameraLogDir()
  {
    return getLogDir() + getXrayCameraDir();
  }

  /**
   * @author Greg Esparza
   */
  public static String getDarkTestOneDir()
  {
    return getXrayCameraDir() + File.separator + _DARK_TEST_ONE_DIR;
  }

  /**
   * @author Greg Esparza
   */
  public static String getDarkTestTwoDir()
  {
    return getXrayCameraDir() + File.separator + _DARK_TEST_TWO_DIR;
  }

  /**
   * @author Greg Esparza
   */
  public static String getLightTestOneDir()
  {
    return getXrayCameraDir() + File.separator + _LIGHT_TEST_ONE_DIR;
  }

  /**
   * @author Greg Esparza
   */
  public static String getLightTestTwoDir()
  {
    return getXrayCameraDir() + File.separator + _LIGHT_TEST_TWO_DIR;
  }

  /**
   * @author Greg Esparza
   */
  public static String getPanelPositioningDir()
  {
    return File.separator + _PANEL_POSITIONING_DIR;
  }

  /**
   * Get the IRP bin dir on the main controller.
   * @author Rex Shang
   */
  public static String getLocalImageReconstructionProcessorBinDir()
  {
    Assert.expect(_releaseDir != null);

    return FileUtil.getAbsolutePath(_releaseDir + File.separator + _IRP_BIN_DIR);
  }

  /**
   * Get the RMS bin dir on the main controller.
   * @author Chong, Wei Chin
   */
  public static String getLocalRemoteMotionServerBinDir()
  {
    Assert.expect(_releaseDir != null);

    return FileUtil.getAbsolutePath(_releaseDir + File.separator + _RMS_BIN_DIR);
  }

  /**
   * Ge the irp bin dir on the remote IRPs.
   * @author Rex Shang
   * @todo RMS need to return environment variable backed path so it can get
   * expanded on the IRP.
   */
  public static String getRemoteImageReconstructionProcessorBinDir()
  {
//    return "%AXI_XRAY_HOME%\\bin";
    return "C:\\Program Files\\AXI System\\v810\\bin";
  }

  /**
   * Ge the irp bin dir on the remote IRPs.
   * @author Rex Shang
   * @todo RMS need to return environment variable backed path so it can get
   * expanded on the IRP.
   */
  public static String getRemoteMotionServerBinDir()
  {
//    return "%AXI_XRAY_HOME%\\bin";
    return "C:\\Program Files\\AXI System\\v810\\bin";
  }

  /**
   * IRP file receiver's root dir is %AXI_XRAY_HOME%, to put a file on it,
   * we have to use a relative path.
   * @author Rex Shang
   */
  public static String getRemoteImageReconstructionProcessorRelativeBinDir()
  {
    return "bin";
  }

  /**
   * RMS file receiver's root dir is %AXI_XRAY_HOME%, to put a file on it,
   * we have to use a relative path.
   * @author Chong, Wei Chin
   */
  public static String getRemoteMotionServerRelativeBinDir()
  {
    return "5.0\\bin";
  }

  /**
   * XCR-3734 RMS folder auto copy config file when click "Upgrade RMS"
   * 
   * RMS file receiver's root dir is %AXI_XRAY_HOME%, to put a file on it,
   * we have to use a relative path.
   * @author Chong, Wei Chin
   * @edit Cheah Lee Herng
   */
  public static String getRemoteMotionServerRelativeConfigDir()
  {
    if(LicenseManager.isXXLEnabled())
      return "5.0\\" + _XXL_CONFIG_DIR;
    else if(LicenseManager.isS2EXEnabled())
      return "5.0\\" + _S2EX_CONFIG_DIR;
    else
      return "5.0\\" + _CONFIG_DIR;
  }

  /**
   * @return the location of the x6000 directory for X-ray Safety Test HTML files.
   *
   * @author George Booth
   */
  public static String getXraySafetyTestFileDir()
  {
    return getDocsDir() + File.separator + _XRAY_SAFETY_TEST_DIR;
  }

  /**
   * @author Bob Balliew
   */
  public static List<String> shortenPaths(List<String> paths)
  {
    Assert.expect(paths != null);
    List<String> shortenedPaths = new ArrayList<String>(paths.size());

    for (String path : paths)
      shortenedPaths.add(shortenPath(path));

    return shortenedPaths;
  }

  /**
   * Shorten the Path passed in by removing any /../ that exist.  For example
   * c:/program files/x6000/1.0/../ndf would becoome C:\\program files\\v810\\ndf
   *
   * @author Bill Darbie
   */
  public static String shortenPath(String path)
  {
    Assert.expect(path != null);
    File file = new File(path);

    try
    {
      path = file.getCanonicalPath();
    }
    catch (IOException ex)
    {
      // Java failed to do it, lets try it ourselves.
      path = compactPath(absolutePath(path));
    }

    return path;
  }

  /**
   * Determine if the specified path starts with a UNC pathname.
   * @param path is a path name to be checked.
   * @return true if the specified path starts with a drive letter, false otherwise..
   * @author Bob Balliew
   */
  private static boolean pathStartsWithUNCPathName(String path)
  {
    Assert.expect(path != null);
    return ((path.length() > 2) && (path.charAt(0) == File.separatorChar) && (path.charAt(1) == File.separatorChar));
  }

  /**
   * Determine if the specified path starts with a drive letter.
   * @param path is a path name to be checked.
   * @return true if the specified path starts with a drive letter, false otherwise..
   * @author Bob Balliew
   */
  private static boolean pathStartsWithDriveLetter(String path)
  {
    Assert.expect(path != null);
    return ((path.length() >= 2) && Character.isLetter(path.charAt(0)) && (path.charAt(1) == ':'));
  }

  /**
   * Extract prefix (drive letter or UNC pathname) from the specified path.
   * @param path is the path name
   * @return the prefix.
   * @author Bob Balliew
   */
  private static String extractPathPrefix(String path)
  {
    Assert.expect(path != null);
    String prefix;

    if (pathStartsWithDriveLetter(path))
    {
      prefix = path.substring(0, 2);
    }
    else if (pathStartsWithUNCPathName(path))
    {
      final int hostnameDelimiter = path.indexOf(2, File.separatorChar);
      final int sharenameDelimiter = (hostnameDelimiter < 0) ? -1 : path.indexOf(hostnameDelimiter + 1, File.separatorChar);

      if (sharenameDelimiter < 0)
        prefix = path.substring(0);
      else
        prefix = path.substring(0, sharenameDelimiter);
    }
    else
    {
      prefix = "";
    }

    return prefix;
  }

  /**
   * Remove prefix (drive letter or UNC pathname), if any from the specified path.
   * @param path is the path name
   * @return the path without the prefix.
   * @author Bob Balliew
   */
  private static String removePathPrefix(String path)
  {
    Assert.expect(path != null);
    int startOfRegularPath = extractPathPrefix(path).length();
    return (startOfRegularPath < 0) ? "" : path.substring(startOfRegularPath);
  }

  /**
   * Determine if the specified path is an absolute path.
   * @param path is the path name
   * @return true if the specified paths is absolute, false otherwise.
   * @author Bob Balliew
   */
  private static boolean isAnAbsolutePath(String path)
  {
    Assert.expect(path != null);

    if (pathStartsWithUNCPathName(path))
      return true;

    final int loc = pathStartsWithDriveLetter(path) ? 2 : 0;
    return (path.length() >= (loc + 1)) && (path.charAt(loc) == File.separatorChar);
  }

  /**
   * Determine if the specified paths have the same prefix.
   * @param path1 is the first path name.
   * @param path2 is the second path name
   * @return true if the both specified paths use the same prefix (drive letter
   * or UNC pathname), false otherwise.
   * @author Bob Balliew
   */
  private static boolean pathsHaveSamePrefix(String path1, String path2)
  {
    Assert.expect(path1 != null);
    Assert.expect(path2 != null);
    return extractPathPrefix(path1).equalsIgnoreCase(extractPathPrefix(path2));
  }

  /**
   * Convert the specified path into an absolute path.  This routine processes
   * a path with and without a drive letter and an UNC path.    This routinue
   * tolerates most badly formed paths.
   * @param path is a path name to be changed to absolute.
   * @return the absolute path name.
   * @author Bob Balliew
   */
  private static String absolutePath(String path)
  {
    Assert.expect(path != null);
    path = path.replace('/', File.separatorChar);
    path = path.replace('\\', File.separatorChar);
    String prefix = extractPathPrefix(path);

    if (isAnAbsolutePath(path) && (prefix.length() > 0))
      return path;

    String currentDir = (new File(".")).getAbsolutePath();

    if (currentDir.length() < 3)
      return path;

    currentDir = currentDir.replace('/', File.separatorChar);
    currentDir = currentDir.replace('\\', File.separatorChar);

    StringBuffer buf = new StringBuffer(currentDir.length() + path.length() + 1);

    if (isAnAbsolutePath(path))
      buf.append(extractPathPrefix(currentDir));
    else
      buf.append(((prefix.length() == 0) || pathsHaveSamePrefix(path, currentDir)) ? currentDir : prefix);

    buf.append(File.separatorChar);
    buf.append(removePathPrefix(path));
    return buf.toString();

  }

  /**
   * Remove "parent" '/../' and self '/./' from the specified path.  This routinue
   * tolerates a badly formed path.
   * @param path is a path name to be compacted.
   * @return the compacted path name.
   * @author Bob Balliew
   */
  private static String compactPath(String path)
  {
    Assert.expect(path != null);

    if (path.length() == 0)
      return path;

    path = path.replace('/', File.separatorChar);
    path = path.replace('\\', File.separatorChar);
    StringTokenizer tokenizer = new StringTokenizer(removePathPrefix(path), File.separator);
    Stack<String> nodeNames = new Stack<String>();

    while (tokenizer.hasMoreTokens())
    {
      String name = tokenizer.nextToken();

      if (!name.contentEquals("."))
      {
        if (name.contentEquals("..") && (!nodeNames.empty()))
          nodeNames.pop();
        else
          nodeNames.push(name);
      }
    }
    if (nodeNames.size() == 0)
      return extractPathPrefix(path) + File.separatorChar;

    StringBuffer buf = new StringBuffer(path.length());

    while (nodeNames.empty() == false)
    {
      buf.insert(0, nodeNames.pop());
      buf.insert(0, File.separatorChar);
    }

    buf.insert(0, extractPathPrefix(path));
    return buf.toString();
  }

  /**
   * @author Bill Darbie
   */
  public static String getPropertiesPackage()
  {
    return _PROPERTIES_PACKAGE;
  }

  /**
   * @author khang-shian.sham
   */
  public static String getGoodImagesDir()
  {
    return _GOOD_IMAGES_DIR;
  }
  
  /*
   * @author Kee, chin Seong
   */
  public static String getDefectPackagerDir()
  {
    return _DEFECT_PACKAGER_DIR;
  }

    /**
   *
   * @return String
   *
   * @author sham khang shian
   */
  public static String getVirtualLiveImageSetPath(String projectName)
  {
    Assert.expect(projectName != null);

    return Directory.getXrayVirtualLiveImagesDir(projectName) + File.separator + VirtualLiveImageGeneration.getInstance().getImageSetName();
  }
  
   /**
   *
   * @return String containing the path name without the trailing slash
   * @author Jack Hwee
   */
  public static String getFalseCallTriggeringDir()
  {
    return Directory.getConfigDir() + File.separator + _FALSE_CALL_TRIGGERING_DIR;
  }

  /**
   * @author sham khang shian
   */
  public static String getVirtualLiveImageSetPath(String projectName, String imageSetName)
  {
    Assert.expect(projectName != null);

    return Directory.getXrayVirtualLiveImagesDir(projectName) + File.separator + imageSetName;
  }
  
  /**
   * @author sham
   */
  public static String getAlignmentLogDir()
  {
    String alignmentLogDirectory;
    File alignmentLogDir = new File(getLogDir() + File.separator + "alignment" + File.separator);

    if(alignmentLogDir.exists() == false)
    {
      if(alignmentLogDir.mkdir() == false)
      {
        try
        {
          throw new CannotCreateFileDatastoreException(alignmentLogDir.getPath());
}
        catch (CannotCreateFileDatastoreException ex)
        {
        }
      }
    }
    alignmentLogDirectory = alignmentLogDir.getPath();

    return alignmentLogDirectory;
  }

/**
   * @author Jack Hwee
   */
  public static String getPrevFalseCallTriggeringDir()
  {
    return Directory.getPrevConfigDir() + File.separator + _FALSE_CALL_TRIGGERING_DIR;
  }
  
  /**
   * get multiboard backup directory
   * @author Phang Siew Yeng
   */
  public static String getMultiBoardBackupDir(String projectName)
  {
    return getProjectDir(projectName) + File.separator + "multiboard" + FileName.getBackupExtension();
  }
  
  /**
   * get the temporary project directory
   * @author Phang Siew Yeng
   */
  public static String getProjectTempDir(String projectName)
  {
    return getProjectDir(projectName) + File.separator + _TEMP;
  }
  
  /**
   * XCR-3284 Software hang when select TIFF image set in Virtual Live
   * 
   * @author bee-hoon.goh
   * @author Cheah Lee Herng
   */
  public static String getVirtualLiveVerificationImagesDir(String projectName)
  {
    return getProjectDir(projectName) + File.separator + _IMAGES_DIR + File.separator + _VIRTUAL_LIVE_VERIFICATION_IMAGES_DIR;    
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public static String getSystemTempDir()
  {
    String tempDir = System.getenv("Temp");
    Assert.expect(tempDir != null, "Temp env variable is null.");

    return tempDir;
  }
  
  /**
   * @author Khaw Chek Hau
   */
  public static String getCAMXDir()
  {
    return "C:\\cpi";
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR2737: Assert when there is invalid license in XXL machine
   */
  public static String getSystemTypeMotionDriveDir(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);
    String configDir;
    
    if (systemType == SystemTypeEnum.XXL)
      configDir = _XXL_CONFIG_DIR;
    else if (systemType == SystemTypeEnum.S2EX)
      configDir = _S2EX_CONFIG_DIR;  
    else
      configDir = _CONFIG_DIR; 
    
    return _releaseDir + File.separator + configDir + File.separator + _MOTION_CONTROL_DIR + File.separator + _MOTION_DRIVE_DIR; 
  }

  /**
   * @author Khaw Chek Hau 
   * @XCR2923 : Machine Utilization Report Tools Implementation
   */
  public static String getMachineUtilizationReportDir()
  {
    String machineUtilizationReportDir = _config.getStringValue(SoftwareConfigEnum.MACHINE_UTILIZATION_REPORT_DIR);
    
    Assert.expect(machineUtilizationReportDir != null);
    return machineUtilizationReportDir;
  }


  /**
   * @author Swee Yee Wong
   */
  public static void addSolderThicknessLayer1DirList(String solderThicknessLayer1Dir)
  {
    Assert.expect(_solderThicknessLayer1DirList != null, "Solder Thickness Layer 1 Directory List is null.");
    _solderThicknessLayer1DirList.add(solderThicknessLayer1Dir);
  }
  
  /**
   * @author Swee Yee Wong
   */
  public static void addSolderThicknessLayer2DirList(String solderThicknessLayer2Dir)
  {
    Assert.expect(_solderThicknessLayer2DirList != null, "Solder Thickness Layer 2 Directory List is null.");
    _solderThicknessLayer2DirList.add(solderThicknessLayer2Dir);
  }
  
  /**
   * @author Swee Yee Wong
   */
  public static boolean verifySolderThicknessLayer1Dir(String solderThicknessLayer1Dir)
  {
    Assert.expect(_solderThicknessLayer1DirList != null, "Solder Thickness Layer 1 Directory List is null.");
    return _solderThicknessLayer1DirList.contains(solderThicknessLayer1Dir);
  }
  
  /**
   * @author Swee Yee Wong
   */
  public static boolean verifySolderThicknessLayer2Dir(String solderThicknessLayer2Dir)
  {
    Assert.expect(_solderThicknessLayer2DirList != null, "Solder Thickness Layer 2 Directory List is null.");
    return _solderThicknessLayer2DirList.contains(solderThicknessLayer2Dir);
  }
  
  /**
   * @author Swee Yee Wong
   */
  public static List<String> getSolderThicknessLayer1DirFullList()
  {
    Assert.expect(_solderThicknessLayer1DirList != null, "Solder Thickness Layer 1 Directory List is null.");
    return _solderThicknessLayer1DirList;
  }
  
  /**
   * @author Swee Yee Wong
   */
  public static List<String> getSolderThicknessLayer2DirFullList()
  {
    Assert.expect(_solderThicknessLayer2DirList != null, "Solder Thickness Layer 2 Directory List is null.");
    return _solderThicknessLayer2DirList;
  }

  /**
   * Clear Tombstone
   * @author sheng-chuan.yong
   */
  public static String getAlgorithmTemplateLearningDir(String projectName)
  {
    String learnImagesTemplateDir = "";
    learnImagesTemplateDir = getAlgorithmLearningDir(projectName) + File.separator + _IMAGE_TEMPLATE_LEARNING_DIR;
    
    return learnImagesTemplateDir;
  }
  
  /**
   * @author Khaw Chek Hau 
   * @XCR2923 : Machine Utilization Report Tools Implementation
   */
  public static String getMachineUtilizationBackupReportDir()
  {
    return getMachineUtilizationReportDir() + File.separator + _BACKUP;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public static String getAlignment2DImagesDir(String projectName)
  {
    Assert.expect(projectName != null);
    
    String alignment2DImagesDir = "";
    alignment2DImagesDir = getXrayVerificationImagesDirectory() + File.separator + projectName +  File.separator + _IMAGES_DIR +  File.separator + _ALIGNMENT_2D_IMAGES_DIR;
    
    return alignment2DImagesDir;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public static String getAlignment2DImageFileFullPath(String projectName, String alignmentImageFileName)
  {
    Assert.expect(projectName != null);
    Assert.expect(alignmentImageFileName != null);
    
    String alignment2DImageFileFullPath = "";
    alignment2DImageFileFullPath = getAlignment2DImagesDir(projectName) + File.separator + alignmentImageFileName;

    return alignment2DImageFileFullPath;
  }

  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public static String getOnlineXrayVerification2DImagesDirFromProject(String projectName)
  {
    Assert.expect(projectName != null);
    
    String verification2DImagesDir = "";
    verification2DImagesDir = _rootDir + File.separator + _PROJECTS_DIR + File.separator + projectName + 
                            File.separator + _IMAGES_DIR + File.separator + _VERIFICATION_2D_IMAGES_DIR;
    return verification2DImagesDir;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public static String getOfflineXrayVerification2DImagesDir(String projectName)
  {
    Assert.expect(projectName != null);
    
    String verification2DImagesDir = "";
    // on the TDW we want to store verification images under x6000/projects/projectName/verification
    // on the x6000 system we do not want to take the disk space to save under each project, so
    // we we use x6000/verification
    verification2DImagesDir = getXrayImagesDir(projectName) + File.separator + _VERIFICATION_2D_IMAGES_DIR;

    return verification2DImagesDir;
  }

  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public static String getXray2DVerificationImagesDir(String projectName)
  {
    Assert.expect(projectName != null);
    
    String verificationImagesDir = "";
    verificationImagesDir = getXrayVerificationImagesDirectory() + File.separator + projectName +  File.separator + _IMAGES_DIR +  File.separator + _VERIFICATION_2D_IMAGES_DIR;  
    
    return verificationImagesDir;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3348: VOne Machine Utilization Feature
   */
  public static String getVOneMachineStatusMonitoringReportDir()
  {
    String vOneReportDir = _config.getStringValue(SoftwareConfigEnum.VONE_STATUS_MONITORING_REPORT_DIR);
    
    Assert.expect(vOneReportDir != null);
    return vOneReportDir;
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public static void setSpecificHistoryLogDir(String historyLogDir) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.SPECIFIC_HISTORY_LOG_DIR, historyLogDir);
  }

  /**
   * @author Kok Chun, Tan
   */
  public static String getVelocityMappingDir()
  {
    Assert.expect(_releaseDir != null); 
    
    return getConfigDir() + File.separator + _VELOCITY_MAPPING_SUB_DIR;
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public static String getFineTuningLogDir(String projectName)
  {
    String fineTuningLogDir = null;

    if (UnitTest.unitTesting() == false)
    {
      fineTuningLogDir = getHistoryFileDir(projectName) + File.separator + _FINE_TUNING_LOG_DIR;
      fineTuningLogDir = shortenPath(fineTuningLogDir);
    }
    else
    {
      fineTuningLogDir = _rootDir + File.separator + _SPECIFIC_HISTORY_LOG_DIR;
    }

    return fineTuningLogDir;
  }
  
  /**
   * XCR-3436
   * @author weng-jian.eoh
   * @return 
   */
  public static String getCustomizeSettingDir()
  {
    return Directory.getConfigDir() + File.separator + _CUSTOMIZE_SETTING_DIR;
  }
    
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @param SystemType
   * @return 
   * 
   */
  public static String getCustomizeSettingDir(SystemTypeEnum SystemType)
  {
    return Directory.getConfigDirWithSystemType(SystemType) + File.separator+ _CUSTOMIZE_SETTING_DIR;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3534: v810 SECS/GEM protocol implementation
   */
  public static String getSECSGEMDir()
  {
    String secsgemDir = _config.getStringValue(SoftwareConfigEnum.SECS_GEM_LOG_DIR);
    
    Assert.expect(secsgemDir != null);
    return secsgemDir;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @param systemType
   * @return 
   * 
   * XCR-3589 Combo STD and XXL software GUI
   */
  public static String getConfigDirWithSystemType(SystemTypeEnum systemType)
  {
    if (systemType.equals(SystemTypeEnum.S2EX))
      return _releaseDir + File.separator +_S2EX_CONFIG_DIR;
    else if (systemType.equals(SystemTypeEnum.XXL))
      return _releaseDir + File.separator +_XXL_CONFIG_DIR;
    else
      return _releaseDir + File.separator +_CONFIG_DIR;
  }
  
   /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public static String getCadDirectory()
  {
    String cadDir = null;
    if (UnitTest.unitTesting() == false)
    {
      cadDir = _config.getStringValue(SoftwareConfigEnum.CAD_DIR);
      cadDir = shortenPath(cadDir);
    }
    else
    {
      cadDir = _rootDir + File.separator + _LEGACY_NDF_DIR;
    }

    Assert.expect(cadDir != null);
    return cadDir;
  }
  
   /**
   * XCR-3661 Able to convert CAD to NDF or V810 recipes
   *
   * @author weng-jian.eoh
   */
  public static void setCadDir(String cadDir) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.CAD_DIR, cadDir);
  }
  
  /**
   * Returns Surface Map info folder
   * 
   * @param projectName Current loaded recipe name
   * @return Directory of Surface Map Info
   * @author Cheah Lee Herng
   */
  public static String getSurfaceMapInfoDir(String projectName)
  {
    return getProjectDir(projectName) + File.separator + _SURFACE_MAP_INFO_DIR;
  }
  
  /**
   * XCR-3551 Able to customize measurements xml output file.
   * 
   * @return Directory of Custom Measurement XML Setting Config File
   * @author Janan Wong
   */
  public static String getCustomMeasurementXMLSettingDir()
  {
    return Directory.getConfigDir() + File.separator + _CUSTOM_MEASUREMENT_XML_SETTING_DIR;
  } 
  
  /**
   * Returns Surface Model info folder
   * 
   * @param projectName Current loaded recipe name
   * @return Directory of Surface Model Info
   * @author Siew Yeng
   */
  public static String getSurfaceModelInfoDir(String projectName)
  {
    return getProjectDir(projectName) + File.separator + _SURFACE_MODEL_INFO_DIR;
  }
}
  
