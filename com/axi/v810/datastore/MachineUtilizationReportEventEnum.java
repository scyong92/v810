package com.axi.v810.datastore;

import java.io.*;

/**
 * @author Khaw Chek Hau
 * @XCR2923 : Machine Utilization Report Tools Implementation
 */
public class MachineUtilizationReportEventEnum extends com.axi.util.Enum implements Serializable
{  
  private static int _index = -1;
  private String _machineUtilizationReportEvent;
  
  public static final MachineUtilizationReportEventEnum PRODUCTION_RUN = new MachineUtilizationReportEventEnum(++_index, "ProductionRun"); 
  public static final MachineUtilizationReportEventEnum LOGIN = new MachineUtilizationReportEventEnum(++_index, "Login"); 
  public static final MachineUtilizationReportEventEnum LOG_OUT = new MachineUtilizationReportEventEnum(++_index, "LogOut"); 
  public static final MachineUtilizationReportEventEnum MACHINE_ERROR_HAPPEN = new MachineUtilizationReportEventEnum(++_index, "MachineErrorHappen"); 
  public static final MachineUtilizationReportEventEnum MACHINE_ERROR_CLEARED = new MachineUtilizationReportEventEnum(++_index, "MachineErrorCleared"); 
  public static final MachineUtilizationReportEventEnum LOAD_PANEL = new MachineUtilizationReportEventEnum(++_index, "LoadPanel"); 
  public static final MachineUtilizationReportEventEnum UNLOAD_PANEL = new MachineUtilizationReportEventEnum(++_index, "UnloadPanel"); 
  public static final MachineUtilizationReportEventEnum START_PROGRAMMING = new MachineUtilizationReportEventEnum(++_index, "StartProgramming"); 
  public static final MachineUtilizationReportEventEnum STOP_PROGRAMMING = new MachineUtilizationReportEventEnum(++_index, "StopProgramming"); 
  public static final MachineUtilizationReportEventEnum MACHINE_SETUP_RUNNING = new MachineUtilizationReportEventEnum(++_index, "MachineSetupRunning"); 
  public static final MachineUtilizationReportEventEnum MACHINE_SETUP_DONE = new MachineUtilizationReportEventEnum(++_index, "MachineSetupDone"); 
  public static final MachineUtilizationReportEventEnum SOFTWARE_POWER_ON = new MachineUtilizationReportEventEnum(++_index, "SoftwarePowerOn"); 
  public static final MachineUtilizationReportEventEnum SOFTWARE_POWER_OFF = new MachineUtilizationReportEventEnum(++_index, "SoftwarePowerOff"); 
  public static final MachineUtilizationReportEventEnum START_SERVICE = new MachineUtilizationReportEventEnum(++_index, "StartService"); 
  public static final MachineUtilizationReportEventEnum STOP_SERVICE = new MachineUtilizationReportEventEnum(++_index, "StopService"); 
  public static final MachineUtilizationReportEventEnum CHECK_SOFTWARE_PREVIOUS_STATUS = new MachineUtilizationReportEventEnum(++_index, "CheckSoftwarePreviousStatus"); 
  public static final MachineUtilizationReportEventEnum SOFTWARE_ERROR = new MachineUtilizationReportEventEnum(++_index, "SoftwareError"); 
  public static final MachineUtilizationReportEventEnum START_LOADING_RECIPE = new MachineUtilizationReportEventEnum(++_index, "StartLoadingRecipe"); 
  public static final MachineUtilizationReportEventEnum STOP_LOADING_RECIPE = new MachineUtilizationReportEventEnum(++_index, "StopLoadingRecipe"); 
  public static final MachineUtilizationReportEventEnum START_CHANGE_OVER = new MachineUtilizationReportEventEnum(++_index, "StartChangeOver"); 
  public static final MachineUtilizationReportEventEnum STOP_CHANGE_OVER = new MachineUtilizationReportEventEnum(++_index, "StopChangeOver"); 
  
  /**
   * @author Khaw Chek Hau
   */
  private MachineUtilizationReportEventEnum(int id, String machineUtilizationReportEvent)
  {
    super(id);
    _machineUtilizationReportEvent = machineUtilizationReportEvent;
  }

  /**
   * @author Khaw Chek Hau
   */
  public String toString()
  {
    return String.valueOf(_machineUtilizationReportEvent);
  }
}
