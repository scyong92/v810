package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when a key is missing from a config file.
 * @author Bill Darbie
 */
public class KeyMissingDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param key is the key that is missing
   * @param configFileName is the config file that the key belongs in
   * @author Bill Darbie
   */
  public KeyMissingDatastoreException(String key, String configFileName)
  {
    super(new LocalizedString("DS_ERROR_CONFIG_MISSING_KEY", new Object[]{key, configFileName}));
    Assert.expect(key != null);
    Assert.expect(configFileName != null);
  }
}
