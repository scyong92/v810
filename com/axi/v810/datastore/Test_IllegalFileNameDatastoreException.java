package com.axi.v810.datastore;

import java.io.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
* This is a unit test for IllegalFileNameDatastoreException.
* @author Bob Balliew
*/
public class Test_IllegalFileNameDatastoreException extends UnitTest
{

  /**
  * @author Bob Balliew
  */
  public static void main(String[] args)
  {
    UnitTest.execute (new Test_IllegalFileNameDatastoreException());
  }

  /**
  * @author Bob Balliew
  */
  public void test(BufferedReader is, PrintWriter os)
  {
    DatastoreException ex = new IllegalFileNameDatastoreException("bad:file;name");
    String message = ex.getLocalizedMessage();
    os.println(message);
  }
}
