package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when we fail to add the project to the repository.
 *
 * @author George A. David
 */
public class FailedAddingProjectToProjectDatabaseDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param projectName the name of the project that encountered problems while adding it to the repository
   * @author George A. David
   */
  public FailedAddingProjectToProjectDatabaseDatastoreException(String projectName, String repositoryPath)
  {
    super(new LocalizedString("DS_ERROR_FAILED_ADDING_PROJECT_TO_PROJECT_DATABASE_KEY", new Object[]{projectName, repositoryPath}));
    Assert.expect(projectName != null);
    Assert.expect(repositoryPath != null);
  }
}
