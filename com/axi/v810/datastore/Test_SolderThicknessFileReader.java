package com.axi.v810.datastore;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;


/**
 * @author Eddie Williamson
 */
public class Test_SolderThicknessFileReader extends UnitTest
{
  private static final int NUMGREYLEVELS = 256;
  private static final int MAXGREYLEVEL = NUMGREYLEVELS - 1;


  /**
   * @author Eddie Williamson
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_SolderThicknessFileReader());
  }

  /**
   * @author Eddie Williamson
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    float maxThickness;
    float datatable[][];

    try
    {
      SolderThicknessFileReader solderThicknessFileReader = SolderThicknessFileReader.getInstance();
      String solderThicknessFilePath = FileName.getCalibThicknessTableFullPath("M19", "Gain 1", SolderThicknessEnum.LEAD_SOLDER_SHADED_BY_COPPER.getFileName(), 0);
      solderThicknessFileReader.readDataFile(solderThicknessFilePath, MAXGREYLEVEL,
                                             NUMGREYLEVELS);
      datatable = solderThicknessFileReader.getDataTable();
      maxThickness = solderThicknessFileReader.getMaxThickness();

      // sanity check on some values
      Expect.expect(MathUtil.fuzzyEquals(maxThickness, 100f));
      Expect.expect(MathUtil.fuzzyEquals(datatable[0][0], 0.0));
      Expect.expect(MathUtil.fuzzyEquals(datatable[255][255], maxThickness));
      // deltagrey can't exceed background. Expect exception if try to access too far
      try
      {
        // This access should generate an exception
        float tmpValue = datatable[100][200];
        Expect.expect(false, "Did not get the expected ArrayIndexOutOfBoundsException");
      }
      catch (ArrayIndexOutOfBoundsException aex)
      {
        // Do nothing.
      }
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace(os);
      Expect.expect(false, "Unexpected DatastoreException during solderthickness table access.");
    }
  }

}
