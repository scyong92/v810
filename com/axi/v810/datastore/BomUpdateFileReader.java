package com.axi.v810.datastore;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Cheah Lee Herng
 */
public class BomUpdateFileReader 
{
    private static BomUpdateFileReader _instance = null;
    
    private LineNumberReader _is = null;
    private String _line = null;
    private String _fileName = null;
    
    private boolean _hasNextLineWasCalled = false;
    private boolean _prevHasNextLineReturnValue = false;
    
    private List<String> _missingReferenceDesignators = new ArrayList<String>();
    
    /**
     * @author Cheah Lee Herng
     */
    private BomUpdateFileReader()
    {
        // Do nothing
    }
    
    /**
     * @author Bill Darbie
    */
    private void init()
    {
        // do nothing
    }
    
    /**
     * @author Cheah Lee Herng
    */
    public static synchronized BomUpdateFileReader getInstance()
    {
        if (_instance == null)
        {
          _instance = new BomUpdateFileReader();
        }
        return _instance;
    }
    
    /**
     * @author Cheah Lee Herng
     * Edite by Kee Chin Seong - For those NOT UNDER component list, should considered under it.
     */
    public void readDataFile(Project project, final String bomUpdateFileName, boolean setComponentToTest) throws DatastoreException
    {
      Assert.expect(project != null);
      Assert.expect(bomUpdateFileName != null);

      try
      {
        _is = ParseUtil.openFile(bomUpdateFileName);
        _fileName = bomUpdateFileName;

        Panel panel = project.getPanel();
        BoardType boardType = panel.getBoardTypes().get(0);
        String boardTypeName = boardType.getName();
        
        _missingReferenceDesignators.clear();
        
        //Kee Chin Seong - List of RefDes which need to recorded.
        java.util.Collection<String> listOfRefDes = new java.util.ArrayList<String>();

        while(hasNextLine())
        {
          // get rid of leading and trailing spaces
          String refDes = getNextLine();

          // Check if we have comma-separated line
          String[] referenceDesignatorArray = refDes.split(",");

          if (referenceDesignatorArray.length > 0)
          {
            // Multi ref des in single line, separated by comma
            for(int i = 0; i < referenceDesignatorArray.length; i++)
            {
              String componentRefDes = referenceDesignatorArray[i].trim().toUpperCase();
              updateBom(panel, boardTypeName, componentRefDes, setComponentToTest);
              listOfRefDes.add(componentRefDes);
            }
          }
          else
          {
            String componentRefDes = refDes.trim().toUpperCase();
            updateBom(panel, boardTypeName, componentRefDes, setComponentToTest);
            listOfRefDes.add(componentRefDes);
          }          
        }
        
        //Kee Chin Seong - For those NOT under BOM list, need to inverse the component test !no load / no test.
        for(Board board : project.getPanel().getBoards())
        {
          for(Component comp : board.getComponents())
          {
            if(listOfRefDes.contains(comp.getReferenceDesignator()) == false)
            {
              updateBom(panel, boardTypeName, comp.getReferenceDesignator(), !setComponentToTest);
            }
          }
        }
      }
      finally
      {
        if (_is != null)
        {
          try
          {
          _is.close();
          }
          catch (IOException ioe)
          {
            DatastoreException dex = new DatastoreException(bomUpdateFileName);
            dex.initCause(ioe);
            throw dex;
          }
        }
      }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private String getNextLine() throws DatastoreException
    {
        // check that hasNextLine() was already called.  If it was not then
        // call it automatically.
        if (_hasNextLineWasCalled == false)
        {
          if (hasNextLine() == false)
          {
            // there is no next line so throw an exception
            int lineNumber = getLineNumber();
            closeFileDuringExceptionHandling();
            throw new FileCorruptDatastoreException(_fileName, lineNumber + 1);
          }
        }
        _hasNextLineWasCalled = false;

        Assert.expect(_line != null);
        return _line;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    private boolean hasNextLine() throws DatastoreException
    {
        // if this method was called already, do not parse again until getNextLine() is called
        if (_hasNextLineWasCalled)
          return _prevHasNextLineReturnValue;

        try
        {
          do
          {
            _line = _is.readLine();
            if (_line == null)
            {
              _prevHasNextLineReturnValue = false;
              _hasNextLineWasCalled = false;
              return false;
            }

            // get rid of leading and trailing spaces
            _line = _line.trim();
          }
          while (_line.equals(""));
        }
        catch (IOException ioe)
        {
          closeFileDuringExceptionHandling();

          DatastoreException de = new CannotReadDatastoreException(_fileName);
          de.initCause(ioe);
          throw de;
        }

        _hasNextLineWasCalled = true;
        Assert.expect(_line != null);
        _prevHasNextLineReturnValue = true;
        return true;
    }
    
    /**
     * @author Cheah Lee Herng
    */
    void closeFileDuringExceptionHandling()
    {
        if (_is != null)
        {
          try
          {
            _is.close();
            _is = null;
          }
          catch (IOException ioe)
          {
            // do nothing
          }
        }
    }
    
    /**
     * @author Bill Darbie
    */
    int getLineNumber()
    {
        Assert.expect(_is != null);

        int lineNumber = _is.getLineNumber();
        if (_hasNextLineWasCalled)
          --lineNumber;

        return lineNumber;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    private void setupComponentTypeStatus(ComponentType componentType, boolean setComponentToTest)
    {
      Assert.expect(componentType != null);
      
      if (setComponentToTest)
      {
        componentType.getComponentTypeSettings().setLoaded(true);
        componentType.getComponentTypeSettings().setInspected(true);
      }
      else
      {
        componentType.getComponentTypeSettings().setLoaded(false);
        componentType.getComponentTypeSettings().setInspected(false);
      }
    }

    /**
     * @author Cheah Lee Herng
     */
    private void updateBom(Panel panel, String boardTypeName, String refDes, boolean setComponentToTest) throws DatastoreException
    {
      Assert.expect(panel != null);
      Assert.expect(boardTypeName != null);
      Assert.expect(refDes != null);
      
      if (panel.getPanelSettings().isPanelBasedAlignment())
        panelBasedBomUpdate(panel, boardTypeName, refDes, setComponentToTest);
      else
        boardBasedBomUpdate(panel, refDes, setComponentToTest);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void panelBasedBomUpdate(Panel panel, String boardTypeName, String refDes, boolean setComponentToTest) throws DatastoreException
    {
      if (panel.hasComponentType(boardTypeName, refDes) == false)
      {
        _missingReferenceDesignators.add(refDes);
        System.out.println("BomUpdater: Missing Component: " + refDes);
        return;    
      }

      setupComponentTypeStatus(panel.getComponentType(boardTypeName, refDes), setComponentToTest);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void boardBasedBomUpdate(Panel panel, String refDes, boolean setComponentToTest) throws DatastoreException
    {
      Assert.expect(panel != null);
      Assert.expect(refDes != null);

      for(Board board : panel.getBoards())
      {
        if (board.hasComponent(refDes) == false)
        {
          _missingReferenceDesignators.add(board.getName() + " - " + refDes);
          System.out.println("BomUpdater: Missing Component: Board " + board.getName() + " - " + refDes);
          return;
        }

        setupComponentTypeStatus(board.getComponentType(refDes), setComponentToTest);
      }
    }
   
    /**
     * @author Cheah Lee Herng 
     */
    public List<String> getMissingReferenceDesignators()
    {
      Assert.expect(_missingReferenceDesignators != null);
      return _missingReferenceDesignators;
    }
}
