package com.axi.v810.datastore;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;

/**
 * @author Bill Darbie
 */
public class Test_FileName extends UnitTest
{
  private String _homeDir = File.separator + "testRel" + File.separator + "unitTest" + File.separator;
  private String _relDir = _homeDir + "x.xx" + File.separator;

  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_FileName());
  }

  /**
   * @author Bill Darbie
   * @author Bob Balliew
   * @author Kee Chin Seong - Adding the Width Height to make the image name unique
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String file;
    String path;
    file = FileName.getProjectPanelFile(FileName.getProjectPanelLatestFileVersion());
    /** @todo wpd - bob - I see no value in call a getXYZFile() method and then checking it is what you expected -
     *                    all this is accomplishing is making maintenance unecessarily hard - can you remove all these?
     */
    Expect.expect(file.equalsIgnoreCase("panel.2.cad"));

    /** @todo wpd - bob - I am not even sure if we should be checking these calls either - a lot of hard coded names
     *                    that have to match exactly what is hard coded in the class being tested.  I think we should
     *                    get rid of these too.
     */

    path = stripOffView(FileName.getProjectPanelFullPath("projectName", FileName.getProjectPanelLatestFileVersion()));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "panel.2.cad"));
    try
    {
      FileName.getProjectPanelFullPath(null, 1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getProjectProjectSettingsFile(FileName.getProjectProjectSettingsLatestFileVersion());
    Expect.expect(file.equalsIgnoreCase("project.4.settings"));
    path = stripOffView(FileName.getProjectProjectSettingsFullPath("projectName", FileName.getProjectProjectSettingsLatestFileVersion()));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "project.4.settings"));
    try
    {
      FileName.getProjectProjectSettingsFullPath(null, 1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getProjectPanelSettingsFile(FileName.getProjectPanelSettingsLatestFileVersion());
    Expect.expect(file.equalsIgnoreCase("panel.2.settings"));
    path = stripOffView(FileName.getProjectPanelSettingsFullPath("projectName", FileName.getProjectPanelSettingsLatestFileVersion()));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "panel.2.settings"));
    try
    {
      FileName.getProjectPanelSettingsFullPath(null, 1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    file = FileName.getProjectSubtypeSettingsFile(FileName.getProjectSubtypeSettingsLatestFileVersion());
    Expect.expect(file.equalsIgnoreCase("subtype.3.settings"));
    path = stripOffView(FileName.getProjectSubtypeSettingsFullPath("projectName", FileName.getProjectSubtypeSettingsLatestFileVersion()));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "subtype.3.settings"));
    try
    {
      FileName.getProjectSubtypeSettingsFullPath(null, 1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getProjectBoardTypeSettingsFile(1, FileName.getProjectBoardTypeSettingsLatestFileVersion());
    Expect.expect(file.equalsIgnoreCase("boardType_" + "1" + ".4.settings"));
    path = stripOffView(FileName.getProjectBoardTypeSettingsFullPath("projectName", 1, FileName.getProjectBoardTypeSettingsLatestFileVersion()));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "boardType_" + "1" + ".4.settings"));
    try
    {
      FileName.getProjectBoardTypeSettingsFullPath(null, 1, 1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getProjectBoardSettingsFile(1, FileName.getProjectBoardSettingsLatestFileVersion());
    Expect.expect(file.equalsIgnoreCase("board_" + "1" + ".1.settings"));

    path = stripOffView(FileName.getProjectBoardSettingsFullPath("projectName", 1, FileName.getProjectBoardSettingsLatestFileVersion()));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "board_" + "1" + ".1.settings"));
    try
    {
      FileName.getProjectBoardSettingsFullPath(null, 1, 1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getProjectComponentNoLoadSettingsFile(FileName.getProjectComponentNoLoadSettingsLatestFileVersion(), 1);
    Expect.expect(file.equalsIgnoreCase("componentNoLoad.2.1.settings"));
    path = stripOffView(FileName.getProjectComponentNoLoadSettingsFullPath("projectName", FileName.getProjectComponentNoLoadSettingsLatestFileVersion(), 1));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "componentNoLoad.2.1.settings"));
    try
    {
      FileName.getProjectComponentNoLoadSettingsFullPath(null, FileName.getProjectComponentNoLoadSettingsLatestFileVersion(), 1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getProjectDatabaseVersionSettingsFile(FileName.getProjectDatabaseVersionSettingsLatestFileVersion());
    Expect.expect(file.equalsIgnoreCase("projectDatabaseVersion.1.settings"));
    path = stripOffView(FileName.getProjectDatabaseVersionSettingsFullPath("projectName", FileName.getProjectDatabaseVersionSettingsLatestFileVersion()));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "projectDatabaseVersion.1.settings"));
    try
    {
      FileName.getProjectDatabaseVersionSettingsFullPath(null, 1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getProjectSummaryFile(FileName.getProjectSummaryLatestFileVersion());
    Expect.expect(file.equalsIgnoreCase("project.1.summary"));
    path = stripOffView(FileName.getProjectSummaryFullPath("projectName", FileName.getProjectSummaryLatestFileVersion()));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "project.1.summary"));
    try
    {
      FileName.getProjectSummaryFullPath(null, 1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getTestProgramFile("projectName");
    Expect.expect(file.equalsIgnoreCase("projectName" + ".program"));
    path = stripOffView(FileName.getTestProgramFullPath("projectName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "projectName" + ".program"));
    try
    {
      FileName.getTestProgramFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getProjectBoardTypeFile(1, FileName.getProjectBoardTypeLatestFileVersion());
    Expect.expect(file.equalsIgnoreCase("boardType_" + "1" + ".2.cad"));
    path = stripOffView(FileName.getProjectBoardTypeFullPath("projectName", 1, FileName.getProjectBoardTypeLatestFileVersion()));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "boardType_" + "1" + ".2.cad"));
    try
    {
      FileName.getProjectBoardTypeFullPath(null, 1, 1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getProjectLandPatternFile(FileName.getProjectLandPatternLatestFileVersion());
    Expect.expect(file.equalsIgnoreCase("landPattern.1.cad"));
    path = stripOffView(FileName.getProjectLandPatternFullPath("projectName", FileName.getProjectLandPatternLatestFileVersion()));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "landPattern.1.cad"));
    try
    {
      FileName.getProjectLandPatternFullPath(null, 1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getProjectCompPackageFile(FileName.getProjectCompPackageLatestFileVersion());
    Expect.expect(file.equalsIgnoreCase("package.2.cad"));
    path = stripOffView(FileName.getProjectCompPackageFullPath("projectName", FileName.getProjectCompPackageLatestFileVersion()));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "package.2.cad"));
    try
    {
      FileName.getProjectCompPackageFullPath(null, 1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    /*
     * @todo REB there should be a test for 'isPanelImageAvailable'.
     */
    file = FileName.getPanelImageFile();
    Expect.expect(file.equalsIgnoreCase("panel.png"));
    path = stripOffView(FileName.getPanelImageFullPath("projectName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "panel.png"));
    try
    {
      FileName.getPanelImageFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getPanelPhotoImageFile();
    Expect.expect(file.equalsIgnoreCase("photo.jpg"));
    path = stripOffView(FileName.getPanelPhotoImageFullPath("projectName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "photo.jpg"));
    try
    {
      FileName.getPanelPhotoImageFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    /*
     * @todo REB there should be a test for 'getPanelImage'.
     */
    file = FileName.getHardwareConfigFile();
    Expect.expect(file.equalsIgnoreCase("hardware.config"));
    path = stripOffView(FileName.getHardwareConfigFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "hardware.config"));
    for(SystemTypeEnum systemType : SystemTypeEnum.getAllSystemTypes())
    {
      path = stripOffView(FileName.getHardwareConfigFullPath(systemType));
      Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "hardware." + systemType.getName() + ".config"));
    }
    file = FileName.getKollmorgenCdMotionDriveXaxisConfigFile();
    Expect.expect(file.equalsIgnoreCase("kollmorgen_cdXaxis.config"));
    path = stripOffView(FileName.getKollmorgenCdMotionDriveXaxisConfigFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "motionControl" + File.separator +
                                        "motionDrive" + File.separator + "synqNet" + File.separator + "kollmorgen" +
                                        File.separator + "cd" + File.separator + "kollmorgen_cdXaxis.config"));
    file = FileName.getKollmorgenCdMotionDriveYaxisConfigFile();
    Expect.expect(file.equalsIgnoreCase("kollmorgen_cdYaxis.config"));
    path = stripOffView(FileName.getKollmorgenCdMotionDriveYaxisConfigFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "motionControl" + File.separator +
                                        "motionDrive" + File.separator + "synqNet" + File.separator + "kollmorgen" +
                                        File.separator + "cd" + File.separator + "kollmorgen_cdYaxis.config"));
    for(SystemTypeEnum systemType : SystemTypeEnum.getAllSystemTypes())
    {
      path = stripOffView(FileName.getKollmorgenCdMotionDriveYaxisConfigFullPath(systemType));
      Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "motionControl" + File.separator +
                                          "motionDrive" + File.separator + "synqNet" + File.separator + "kollmorgen" +
                                          File.separator + "cd" + File.separator + "kollmorgen_cdYaxis."+ systemType.getName() + ".config"));
    }
    file = FileName.getKollmorgenCdMotionDriveRailWidthAxisConfigFile();
    Expect.expect(file.equalsIgnoreCase("kollmorgen_cdRailWidthAxis.config"));
    path = stripOffView(FileName.getKollmorgenCdMotionDriveRailWidthAxisConfigFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "motionControl" + File.separator +
                                        "motionDrive" + File.separator + "synqNet" + File.separator + "kollmorgen" +
                                        File.separator + "cd" + File.separator + "kollmorgen_cdRailWidthAxis.config"));
    file = FileName.getPanelHandlerLoadedPanelConfigFile();
    Expect.expect(file.equalsIgnoreCase("loadedPanel.config"));
    path = stripOffView(FileName.getPanelHandlerLoadedPanelConfigFullPath());
    Expect.expect(path.equalsIgnoreCase(Directory.getOperatingSystemDir() + File.separator + "loadedPanel.config"));
    
	//Swee-Yee.Wong - XCR-3157
    //IRP error message is prompted if run alignment/fine tuning without run full cdna and without unload the board 1st
    file = FileName.getSystemStatusConfigFile();
    Expect.expect(file.equalsIgnoreCase("systemStatus.config"));
    path = stripOffView(FileName.getSystemStatusConfigFullPath());
    Expect.expect(path.equalsIgnoreCase(Directory.getSystemTempDir() + File.separator + "systemStatus.config"));
    
    file = FileName.getHardwareCalibFile();
    Expect.expect(file.equalsIgnoreCase("hardware.calib"));
    path = stripOffView(FileName.getHardwareCalibFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "calib" + File.separator + "hardware.calib"));
    file = FileName.getSolderThicknessTable1File();
    Expect.expect(file.equalsIgnoreCase("eutectic6337_copper_shading.calib"));
    path = stripOffView(FileName.getCalibThicknessTableFullPath("M19", "Gain 1", SolderThicknessEnum.LEAD_SOLDER_SHADED_BY_COPPER.getFileName(), 0));
    Expect.expect(path.equalsIgnoreCase(_relDir + "calib" + File.separator + "eutectic6337_copper_shading.calib"));
    file = FileName.getGreyscaleLogFileBasename();
    Expect.expect(file.equalsIgnoreCase("grayscale"));
    file = FileName.getXraySpotLogFileBasename();
    Expect.expect(file.equalsIgnoreCase("xrayspot"));
    file = FileName.getStageHysteresisLogFileBasename();
    Expect.expect(file.equalsIgnoreCase("stageHysteresis"));
    file = FileName.getSystemOffsetsLogFileBasename();
    Expect.expect(file.equalsIgnoreCase("systemOffsets"));
    file = FileName.getMagnificationLogFileBasename();
    Expect.expect(file.equalsIgnoreCase("magnification"));
    file = FileName.getCameraAdjustmentConfirmationlLogFileBasename();
    Expect.expect(file.equalsIgnoreCase("cameraAdjustment"));
    file = FileName.getCameraImageLogFileBasename();
    Expect.expect(file.equalsIgnoreCase("cameraImage"));
    path = stripOffView(FileName.getConfirmXraySourceFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "log" + File.separator + "confirm" + File.separator + "XraySource" + File.separator + "xraysource"));
    file = FileName.getBaseImageQualityLogFileName();
    Expect.expect(file.equalsIgnoreCase("baseImageQuality"));
    file = FileName.getFixedRailImageQualityConfirmationlLogFileName();
    Expect.expect(file.equalsIgnoreCase("fixedRailImageQuality"));
    file = FileName.getSystemImageQualityConfirmationLogFileName();
    Expect.expect(file.equalsIgnoreCase("systemImageQuality"));
    file = FileName.getAdjustableRailImageQualityConfirmationLogFileName();
    Expect.expect(file.equalsIgnoreCase("adjustableRailImageQuality"));
    path = stripOffView(FileName.getConfirmSystemCommunicationNetworkFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "log" + File.separator + "confirm" + File.separator + "systemCommunicationNetwork" + File.separator + "systemCommunicationNetwork"));
    path = stripOffView(FileName.getConfirmPanelHandlingFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "log" + File.separator + "confirm" + File.separator + "panelhandling" + File.separator + "panelhandling"));
    path = stripOffView(FileName.getConfirmMotionRepeatabilityFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "log" + File.separator + "confirm" + File.separator + "motionRepeatability" + File.separator + "motionRepeatability"));
    file = FileName.getSoftwareConfigFile();
    Expect.expect(file.equalsIgnoreCase("software.config"));
    path = stripOffView(FileName.getSoftwareConfigFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "software.config"), _relDir + "config" + File.separator + "software.config");
    for(SystemTypeEnum systemType : SystemTypeEnum.getAllSystemTypes())
    {
      path = stripOffView(FileName.getSoftwareConfigFullPath(systemType));
      Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "software." + systemType.getName() + ".config"));
    }
    file = FileName.getBackupExtension();
    Expect.expect(file.equalsIgnoreCase(".backup"));
    file = FileName.getLogFileExtension();
    Expect.expect(file.equalsIgnoreCase(".log"));
    file = FileName.getZipFileExtension();
    Expect.expect(file.equalsIgnoreCase(".zip"));
    file = FileName.getNdfZipFileExtension();
    Expect.expect(file.equalsIgnoreCase(".ndf.zip"));
    file = FileName.getPanelZipFileExtension();
    Expect.expect(file.equalsIgnoreCase(".panel.zip"));
    file = FileName.getLegacyNdfZipFileExtension();
    Expect.expect(file.equalsIgnoreCase(".ndf"));
    file = FileName.getXmlFileExtension();
    Expect.expect(file.equalsIgnoreCase(".xml"));
    file = FileName.getDataFileExtension();
    Expect.expect(file.equalsIgnoreCase(".dat"));
    file = FileName.getProjectFileExtension();
    Expect.expect(file.equalsIgnoreCase(".project"));
    file = FileName.getProjectVersionFileExtension();
    Expect.expect(file.equalsIgnoreCase(".version"));
    file = FileName.getTempFileExtension();
    Expect.expect(file.equalsIgnoreCase(".temp"));
    file = FileName.getCadFileExtension();
    Expect.expect(file.equalsIgnoreCase(".cad"));
    file = FileName.getSettingsFileExtension();
    Expect.expect(file.equalsIgnoreCase(".settings"));
    path = stripOffView(FileName.getLegacyBoardNdfFullPath("panelName", "boardName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "ndf" + File.separator + "panelName" + File.separator + "boardName" + File.separator + "board.ndf"));
    try
    {
      FileName.getLegacyBoardNdfFullPath(null, "boardName");
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      FileName.getLegacyBoardNdfFullPath("panelName", null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    path = stripOffView(FileName.getLegacyComponentNdfFullPath("panelName", "boardName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "ndf" + File.separator + "panelName" + File.separator + "boardName" + File.separator + "componen.ndf"));
    try
    {
      FileName.getLegacyComponentNdfFullPath(null, "boardName");
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      FileName.getLegacyComponentNdfFullPath("panelName", null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    path = stripOffView(FileName.getLegacyPackageNdfFullPath("panelName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "ndf" + File.separator + "panelName" + File.separator + "package.ndf"));
    try
    {
      FileName.getLegacyPackageNdfFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    path = stripOffView(FileName.getLegacyProjectNdfFullPath("projectName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "ndf" + File.separator + "projectName" + File.separator + "project.ndf"));
    try
    {
      FileName.getLegacyProjectNdfFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    path = stripOffView(FileName.getLegacyPanelNdfFullPath("panelName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "ndf" + File.separator + "panelName" + File.separator + "panel.ndf"));
    try
    {
      FileName.getProjectSerializedFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    path = stripOffView(FileName.getLegacySurfaceMapNdfFullPath("panelName", "boardName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "ndf" + File.separator + "panelName" + File.separator + "boardName" + File.separator + "surfmap.ndf"));
    try
    {
      FileName.getLegacySurfaceMapNdfFullPath(null, "boardName");
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      FileName.getLegacySurfaceMapNdfFullPath("panelName", null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    path = stripOffView(FileName.getLegacySurfaceMountLandPatternNdfFullPath("panelName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "ndf" + File.separator + "panelName" + File.separator + "landpat.ndf"));
    try
    {
      FileName.getLegacySurfaceMountLandPatternNdfFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    path = stripOffView(FileName.getLegacyThroughHoleLandPatternNdfFullPath("panelName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "ndf" + File.separator + "panelName" + File.separator + "padgeom.ndf"));
    try
    {
      FileName.getLegacyThroughHoleLandPatternNdfFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    path = stripOffView(FileName.getLegacyPinsNdfFullPath("panelName", "boardName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "ndf" + File.separator + "panelName" + File.separator + "boardName" + File.separator + "pins.ndf"));
    try
    {
      FileName.getLegacyPinsNdfFullPath(null, "boardName");
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      FileName.getLegacyPinsNdfFullPath("panelName", "boardName");
      // @todo REB should this assert? Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    /*
     * @todo REB there should be a test for 'getProjectNames'.
     * @todo REB there should be a test for 'getNdfProjectNames'.
     * @todo REB there should be a test for 'getNdfProjectNames(directory)'.
     */
    List<String> files = FileName.getAllLegacyNdfFullPaths("Test_FileName1");
    for (String name : files)
    {
      name = stripOffView(name);
      System.out.println("panel NDF: " + name);
    }
    path = stripOffView(FileName.getExceptionLogFullName());
    Expect.expect(path.equalsIgnoreCase(_relDir + "log" + File.separator + "exceptions.log"));
    path = stripOffView(FileName.getExceptionLoggerFullName());
    Expect.expect(path.equalsIgnoreCase(_relDir + "log" + File.separator + "exceptions%g.log"));
    {
      final boolean[] ILLEGAL =
      { // x0     x1     x2     x3     x4     x5     x6     x7     x8     x9     xa     xb     xc     xd     xe     xf
        false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,  // 0x
        false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,  // 1x
        false, false,  true, false, false, false, false, false, false, false,  true, false, false, false, false,  true,  // 2x
        false, false, false, false, false, false, false, false, false, false,  true, false,  true, false,  true,  true,  // 3x
        false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,  // 4x
        false, false, false, false, false, false, false, false, false, false, false, false,  true, false, false, false,  // 5x
        false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,  // 6x
        false, false, false, false, false, false, false, false, false, false, false, false,  true, false, false, false,  // 6x
      };
      final int MAX_CHARACTER_CODES = 65536;
      for (int i = 0; i < MAX_CHARACTER_CODES; ++i)
      {
        char[] c = {(char)i};
        String s = new String(c);
        Expect.expect(FileName.hasIllegalChars(s) == ((i < ILLEGAL.length) ? ILLEGAL[i] : false), " for code: " + Integer.toHexString(i) + " (hex)");
      }
      String illegalCharacters = FileName.getIllegalChars();
      for (int i = 0; i < illegalCharacters.length(); ++i)
      {
        int code = (int)illegalCharacters.charAt(i);
        Expect.expect((code >=0) && (code < ILLEGAL.length) && ILLEGAL[code], " for index: " + Integer.toString(i));
      }
    }
    path = stripOffView(FileName.getProjectVersionFullName("projectName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + ".version"));
    try
    {
      FileName.getProjectVersionFullName(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getLicenseFileExtension();
    Expect.expect(file.equalsIgnoreCase(".lic"));
    path = stripOffView(FileName.getLoadWarningsLogFullPath("projectName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "ndf" + File.separator + "projectName" + File.separator + "importWarnings.log"));
    try
    {
      FileName.getLoadWarningsLogFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }

    file = FileName.getSerialNumberToProjectConfigFile();
    Expect.expect(file.equalsIgnoreCase("serialNumberToProject.config"));
    path = stripOffView(FileName.getSerialNumberToProjectConfigFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "serialNumbers" + File.separator + "serialNumberToProject.config"));
    file = FileName.getResultsProcessingConfigFile();
    Expect.expect(file.equalsIgnoreCase("resultsProcessing.config"));
    path = stripOffView(FileName.getResultsProcessingConfigFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "resultsProcessing.config"));
    file = FileName.getSerialNumbersToSkipConfigFile();
    Expect.expect(file.equalsIgnoreCase("serialNumbersToSkip.config"));
    path = stripOffView(FileName.getSerialNumbersToSkipConfigFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "serialNumbers" + File.separator + "serialNumbersToSkip.config"));
    file = FileName.getValidSerialNumbersConfigFile();
    Expect.expect(file.equalsIgnoreCase("validSerialNumbers.config"));
    path = stripOffView(FileName.getValidSerialNumbersConfigFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "serialNumbers" + File.separator + "validSerialNumbers.config"));
    path = FileName.getProjectDatabaseFileFullPath("projectName", 1);
    Expect.expect(path.equalsIgnoreCase(Config.getInstance().getStringValue(SoftwareConfigEnum.PROJECT_DATABASE_PATH) + File.separator + "projectName" + "." + "1" + ".zip"));
    try
    {
      FileName.getProjectDatabaseFileFullPath(null, 1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      FileName.getProjectDatabaseFileFullPath("projectName", 0);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      FileName.getProjectDatabaseFileFullPath("projectName", -1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getImageName("reconstructionRegionName", 1, 1);
    Expect.expect(file.equalsIgnoreCase("reconstructionRegionName" + "_" + "1" + "_" + "1" + ".png"));
    try
    {
      FileName.getImageName(null, 1, 1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      FileName.getImageName("reconstructionRegionName", -1, 2);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      FileName.getImageName("reconstructionRegionName", 0, -1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getInspectionImageName("top_13_14_15_16", 0 );
    Expect.expect( file.equals( "top_13_14_15_16_0.png" ) );
    try
    {
      FileName.getInspectionImageName(null, 1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      FileName.getInspectionImageName("top_13_14_15_16", -1);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getVerificationImageName("test");
    Expect.expect( file.equals( "test.jpg" ));
    try
    {
      FileName.getVerificationImageName(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    /*
     * @todo REB there should be a test for 'RegExFileNameFilter'.
     */
    file = FileName.getResultImageName(true, 12345, 67890, 12345, 67890, 1);
    Expect.expect(file.equals("top" + "12345" + "x" + "67890" + "_" + "1" + "_" + "0" + ".jpg"));
    file = FileName.getResultImageName(false, 54321, 98760, 12345, 67890, 12);
    Expect.expect(file.equals("bottom" + "54321" + "x" + "98760" + "_" + "12" + "_" + "0" + ".jpg"));
    file = FileName.getResultImageName(false, 54321, 98760, 12345, 67890, 12, 44);
    Expect.expect(file.equals("bottom" + "54321" + "x" + "98760" +"_" + "12" + "_" + "44" + ".jpg"));
    file = FileName.getInspectionImageInfoProgramFileName();
    Expect.expect(file.equals("inspectionImageInfo.1.program"));
    file = FileName.getVerificationImageNamesProgramFileName();
    Expect.expect(file.equals("verificationImageNames.1.program"));
    file = FileName.getImageSetInfoFileName();
    Expect.expect(file.equals("imageSet.info"));
    path = stripOffView(FileName.getInspectionImageInfoProgramFullPath("projectName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "inspectionImageInfo.1.program"));
    path = stripOffView(FileName.getVerificationImageNamesProgramFullPath("projectName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "images" + File.separator + "verification" + File.separator + "verificationImageNames.1.program"));
    try
    {
      FileName.getInspectionImageInfoProgramFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    path = stripOffView(FileName.getVerificationImageNamesProgramFullPath("projectName"));
    String wpd = _homeDir + "images" + File.separator + "verification"+ File.separator + "imageNames.1.program";
    Expect.expect(path.equalsIgnoreCase(_homeDir + "images" + File.separator + "verification"+ File.separator + "verificationImageNames.1.program"));
    try
    {
      FileName.getVerificationImageNamesProgramFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    path = stripOffView(FileName.getInspectionImageInfoProgramFullPath("projectName", "imageSetName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "images" + File.separator + "inspection" + File.separator + "imageSetName" + File.separator + "inspectionImageInfo.1.program"));
    try
    {
      FileName.getInspectionImageInfoProgramFullPath(null, "imageSetName");
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      FileName.getInspectionImageInfoProgramFullPath("projectName", null);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }
    path = stripOffView(FileName.getImageSetInfoFullPath("projectName", "imageSetName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "images" + File.separator + "inspection" + File.separator + "imageSetName" + File.separator + "imageSet.info"));
    try
    {
      FileName.getImageSetInfoFullPath(null, "imageSetName");
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    try
    {
      FileName.getImageSetInfoFullPath("projectName", null);
      Expect.expect(false);
    }
    catch (AssertException e)
    {
      // do nothing
    }
    path = stripOffView(FileName.getDisplayConfigFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "calib" + File.separator + "display.cfg"));
    file = FileName.getResultsFileName("projectName");
    Expect.expect(file.equalsIgnoreCase("projectName" + ".results"));
    try
    {
      FileName.getResultsFileName(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    /**
     * @todo REB there should be a test for 'getResultsFileFullPath'.
     */
    file = FileName.getXMLResultsFileName("projectName");
    Expect.expect(file.equalsIgnoreCase("projectName" + ".indictments" + ".xml"));
    try
    {
      FileName.getXMLResultsFileName(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    /**
     * @todo REB there should be a test for 'getXMLTempResultsFileFullPath'.
     */
    file = FileName.getXMLBoardResultsFileName("projectName");
    Expect.expect(file.equalsIgnoreCase("projectName" + ".boardIndictments" + ".xml"));
    try
    {
      FileName.getXMLBoardResultsFileName(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    /**
     * @todo REB there should be a test for 'getXMLBoardResultsFileFullPath'.
     * @todo REB there should be a test for 'getXMLBoardResultsFileFullPath'.
     */
    file = FileName.getAlgorithmShortsLearningFileName();
    Expect.expect(file.equalsIgnoreCase("algorithmShorts.learned"));
    path = stripOffView(FileName.getAlgorithmShortsLearningFullPath("projectName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "algorithmLearning" + File.separator + "algorithmShorts.learned"));
    try
    {
      FileName.getAlgorithmShortsLearningFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getAlgorithmSubtypesLearningFileName();
    Expect.expect(file.equalsIgnoreCase("algorithmSubtypes.learned"));
    path = stripOffView(FileName.getAlgorithmSubtypesLearningFullPath("projectName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "algorithmLearning" + File.separator + "algorithmSubtypes.learned"));
    try
    {
      FileName.getAlgorithmSubtypesLearningFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getTextFileExtension();
    Expect.expect(file.equalsIgnoreCase(".txt"));
    file = FileName.getCommaSeparatedValueFileExtension();
    Expect.expect(file.equalsIgnoreCase(".csv"));
    file = FileName.getConfigFileExtension();
    Expect.expect(file.equalsIgnoreCase(".config"));
    path = stripOffView(FileName.getJointTypeAssignmentFullPath("fileName"));
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "jointTypeAssignment" + File.separator + "fileName" + ".config"));
    try
    {
      FileName.getJointTypeAssignmentFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getJointTypeAssignmentDefaultFile();
    Expect.expect(file.equalsIgnoreCase("defaultJointTypeAssignment.config"));
    /**
     * @todo REB there should be a test for 'getJointTypeAssignmentFiles'.
     */
    {
      List<String> configNames = FileName.getBarcodeReaderConfigurationNames();
      Expect.expect(configNames.contains("_ms820_p2p"));
      Expect.expect(!configNames.contains("GARBAGE"));
      Expect.expect(!configNames.contains("UNIT_TEST"));
      try
      {
        FileUtil.copy(Directory.getBarcodeReaderDir() + File.separator + "_ms820_p2p" + FileName.getConfigFileExtension(),
                      Directory.getBarcodeReaderDir() + File.separator + "UNIT_TEST" + FileName.getConfigFileExtension());
        List<String> newConfigNames = FileName.getBarcodeReaderConfigurationNames();
        Expect.expect(newConfigNames.size() == (configNames.size() + 1));
        Expect.expect(newConfigNames.contains("UNIT_TEST"));
        FileUtil.delete(Directory.getBarcodeReaderDir() + File.separator + "UNIT_TEST" + FileName.getConfigFileExtension());
        List<String> oldConfigNames = FileName.getBarcodeReaderConfigurationNames();
        Expect.expect(oldConfigNames.size() == (newConfigNames.size() - 1));
        Expect.expect(oldConfigNames.size() == configNames.size());
        Expect.expect(!oldConfigNames.contains("UNIT_TEST"));
        Expect.expect(oldConfigNames.containsAll(configNames));
      }
      catch (Exception e)
      {
        Expect.expect(false, e.getMessage());
      }
    }
    file = FileName.getBarcodeReaderConfigurationFileName("configName");
    Expect.expect(file.equalsIgnoreCase("configName" + ".config"));
    path = stripOffView(FileName.getBarcodeReaderConfigurationFullPath("configName"));
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "barcodeReader" + File.separator + "configName" + ".config"));
    file = FileName.getCadFileName(1234567890);
    Expect.expect(file.equalsIgnoreCase("cad" + "." + "1234567890" + ".xml"));
    path = stripOffView(FileName.getCadFileNameFullPath("projectName", 987654321));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "cad" + "." + "987654321" + ".xml"));
    try
    {
      FileName.getCadFileNameFullPath(null, 0);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    path = stripOffView(FileName.getCadFileNameInResultsDirFullPath("projectName", -123456789));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "results" + File.separator + "projectName" + File.separator + "cad" + "." + "-123456789" + ".xml"));
    try
    {
      FileName.getCadFileNameInResultsDirFullPath(null, 0);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    path = stripOffView(FileName.getSettingsFileNameInResultsDirFullPath("projectName", 543219876));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "results" + File.separator + "projectName" + File.separator + "settings" + "." + "543219876" + ".xml"));
    try
    {
      FileName.getSettingsFileNameInResultsDirFullPath(null, 0);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    path = stripOffView(FileName.getSettingsFileNameFullPath("projectName", 543219876));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "settings" + "." + "543219876" + ".xml"));
    try
    {
      FileName.getSettingsFileNameFullPath(null, 0);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    file = FileName.getSettingsFileName(12);
    Expect.expect(file.equalsIgnoreCase("settings" + "." + "12" + ".xml"));
    path = stripOffView(FileName.getTestDevPersistFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "gui" + File.separator + "TestDevOptions.persist"));
    path = stripOffView(FileName.getTestExecPersistFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "gui" + File.separator + "TestExecOptions.persist"));
    path = stripOffView(FileName.getMainMenuPersistFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "gui" + File.separator + "MainMenuOptions.persist"));
    path = stripOffView(FileName.getGuiStatePersistFullPath("projectName"));
    Expect.expect(path.equalsIgnoreCase(_homeDir + "recipes" + File.separator + "projectName" + File.separator + "GuiState.persist"));
    try
    {
      FileName.getGuiStatePersistFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
    path = stripOffView(FileName.getServiceUIStatePersistFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "gui" + File.separator + "ServiceUIState.persist"), path);
    path = stripOffView(FileName.getAlgorithmDiagnosticSettingPersistFullPath());
    Expect.expect(path.equalsIgnoreCase(_relDir + "config" + File.separator + "gui" + File.separator + "AlgoSettings.persist"), path);
    /**
     * @todo REB there should be a test for 'getConcatenationIntermediateFileFullPath'.
     */
    try
    {
      FileName.getConcatenationIntermediateFileFullPath(null);
      Expect.expect(false);
    }
    catch(AssertException e)
    {
      // do nothing
    }
  }


  /**
   * With ClearCase Remote Client views, the vob base path
   * may start with lio_axi instead of axi, so allow clients
   * to specify which base bath to use.  This is a modification
   * of Bill's original method with just the vob path parameterized.
   *
   * @author Peter Esbensen
   */
  private String stripOffView(String fullPath, String vobBasePath)
  {
    Assert.expect(fullPath != null);
    Assert.expect(vobBasePath != null);

    // view names might begin with axi, so we'll look for the
    // vob name all by itself with the slashes at both ends
    int index = fullPath.indexOf("\\" + vobBasePath + "\\");
    if (index != -1)
      index = index + ("\\" + vobBasePath).length();
    if (index == -1)
    {
      index = fullPath.indexOf("/" + vobBasePath + "/");
      if (index != -1)
        index = index + ("/" + vobBasePath).length();
    }

    String strippedPath = fullPath;
    if (index != -1)
      strippedPath = fullPath.substring(index, fullPath.length());

    return strippedPath;
  }

  /**
   * @author Bill Darbie
   * @author Peter Esbensen
   */
  private String stripOffView(String fullPath)
  {
    Assert.expect(fullPath != null);
    String pathStrippedOfAxi = stripOffView(fullPath, "axi");
    String pathStrippedOfAxiAndLio_Axi = stripOffView(pathStrippedOfAxi, "lio_axi");
    return pathStrippedOfAxiAndLio_Axi;
  }

}
