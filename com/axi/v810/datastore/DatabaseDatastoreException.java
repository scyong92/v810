package com.axi.v810.datastore;

import java.sql.*;

import com.axi.util.*;

/**
 * This exception is thrown whenever there's a problem working with a database.
 *
 * @author Matt Wharton
 */
public class DatabaseDatastoreException extends DatastoreException
{
  /**
   * @author Matt Wharton
   */
  public DatabaseDatastoreException(DatabaseTypeEnum databaseTypeEnum, Throwable initCause)
  {
    super(new LocalizedString(
        "DS_ERROR_OCCURED_WHILE_ACCESSING_DATABASE_KEY",
        new Object[] {databaseTypeEnum.getDescription(), initCause.toString()}));
  }

  /**
   * @author Matt Wharton
   */
  public DatabaseDatastoreException(DatabaseTypeEnum databaseTypeEnum, String errorMessage)
  {
    super(new LocalizedString(
      "DS_ERROR_OCCURED_WHILE_ACCESSING_DATABASE_KEY",
      new Object[] {databaseTypeEnum.getDescription(), errorMessage}));
  }
}
