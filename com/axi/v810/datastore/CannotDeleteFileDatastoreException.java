package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when a file is cannot be written for some reason.
 * @author Bill Darbie
 */
public class CannotDeleteFileDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public CannotDeleteFileDatastoreException(String fileName)
  {
    super(new LocalizedString("DS_ERROR_CANNOT_DELETE_KEY", new Object[]{fileName}));
    Assert.expect(fileName != null);
  }
}
