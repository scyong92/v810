package com.axi.v810.datastore;

import java.io.*;

/**
 * @author Khaw Chek Hau
 * @VOne Machine Utilization Feature
 */
public class VOneMachineStatusMonitoringEventEnum extends com.axi.util.Enum implements Serializable
{  
  private static int _index = -1;
  private String _vOneMachineStatusMonitoringEvent;
  
  public static final VOneMachineStatusMonitoringEventEnum PRODUCTION_RUNNING = new VOneMachineStatusMonitoringEventEnum(++_index, "ProductionRunning"); 
  public static final VOneMachineStatusMonitoringEventEnum PRODUCTION_STOP = new VOneMachineStatusMonitoringEventEnum(++_index, "ProductionStop"); 
  public static final VOneMachineStatusMonitoringEventEnum UPSTREAM_IDLING = new VOneMachineStatusMonitoringEventEnum(++_index, "UpstreamIdling"); 
  public static final VOneMachineStatusMonitoringEventEnum MACHINE_ERROR_START = new VOneMachineStatusMonitoringEventEnum(++_index, "MachineError"); 
  public static final VOneMachineStatusMonitoringEventEnum MACHINE_ERROR_CLEARED = new VOneMachineStatusMonitoringEventEnum(++_index, "MachineErrorClear"); 
  public static final VOneMachineStatusMonitoringEventEnum PROGRAMMING_START = new VOneMachineStatusMonitoringEventEnum(++_index, "Programming"); 
  public static final VOneMachineStatusMonitoringEventEnum PROGRAMMING_STOP = new VOneMachineStatusMonitoringEventEnum(++_index, "StopProgramming");
  public static final VOneMachineStatusMonitoringEventEnum SERVICE_START = new VOneMachineStatusMonitoringEventEnum(++_index, "Service"); 
  public static final VOneMachineStatusMonitoringEventEnum SERVICE_STOP = new VOneMachineStatusMonitoringEventEnum(++_index, "StopService");
  public static final VOneMachineStatusMonitoringEventEnum SOFTWARE_OFF = new VOneMachineStatusMonitoringEventEnum(++_index, "SoftwareOff");
  public static final VOneMachineStatusMonitoringEventEnum PANEL_LOADING_START = new VOneMachineStatusMonitoringEventEnum(++_index, "PanelLoading");
  public static final VOneMachineStatusMonitoringEventEnum PANEL_LOADING_END = new VOneMachineStatusMonitoringEventEnum(++_index, "PanelLoadingEnd");
  public static final VOneMachineStatusMonitoringEventEnum PANEL_UNLOADING_START = new VOneMachineStatusMonitoringEventEnum(++_index, "PanelUnloading");
  public static final VOneMachineStatusMonitoringEventEnum PANEL_UNLOADING_END = new VOneMachineStatusMonitoringEventEnum(++_index, "PanelUnloadingEnd");
  public static final VOneMachineStatusMonitoringEventEnum DOWNSTREAM_BLOCK_START = new VOneMachineStatusMonitoringEventEnum(++_index, "DownstreamBlock");
  public static final VOneMachineStatusMonitoringEventEnum MACHINE_SETUP_START = new VOneMachineStatusMonitoringEventEnum(++_index, "MachineSetup");
  public static final VOneMachineStatusMonitoringEventEnum MACHINE_SETUP_END = new VOneMachineStatusMonitoringEventEnum(++_index, "MachineSetupEnd");
  public static final VOneMachineStatusMonitoringEventEnum RECIPE_LOADING_START = new VOneMachineStatusMonitoringEventEnum(++_index, "RecipeLoadingStart");
  public static final VOneMachineStatusMonitoringEventEnum RECIPE_LOADING_END = new VOneMachineStatusMonitoringEventEnum(++_index, "RecipeLoadingEnd");
  
  /**
   * @author Khaw Chek Hau
   */
  private VOneMachineStatusMonitoringEventEnum(int id, String vOneMachineStatusMonitoringEvent)
  {
    super(id);
    _vOneMachineStatusMonitoringEvent = vOneMachineStatusMonitoringEvent;
  }

  /**
   * @author Khaw Chek Hau
   */
  public String toString()
  {
    return String.valueOf(_vOneMachineStatusMonitoringEvent);
  }
}
