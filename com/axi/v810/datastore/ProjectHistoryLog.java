/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import java.io.*;
import java.text.*;
import java.util.*;
import java.util.logging.*;

/**
 *
 * @author hsia-fen.tan
 */
public class ProjectHistoryLog
{
  private static final String _CSV_DELIMITER = ",";
  private static final String _NOT_AVAILABLE = "N/A";
  
  private static String _projectName;
  private FileWriterUtilAxi _historyLogFileWriter;
  private static ProjectHistoryLog _ProjectHistoryLog = null;
  private static String _ProjectHistoryLogFileFullPath;
  private MathUtilEnum mathUtilEnum;
  private String _jointType;
  private String _padType;
  private MeasurementUnitsEnum _measurementUnitsEnum;
  SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss");
  private String _currentUserName;
  private FileWriterUtilAxi _fineTuningLogFileWriter;
  private static String _fineTuningLogFileFullPath;

  /**
   * @author Tan Hsia Fen
   */
  private ProjectHistoryLog()
  {
    // make this private so getInstance() must be used
  }

  /**
   * @author Tan Hsia Fen
   */
  public synchronized static ProjectHistoryLog getInstance()
  {
    if (_ProjectHistoryLog == null)
    {
      _ProjectHistoryLog = new ProjectHistoryLog();
    }

    return _ProjectHistoryLog;
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public void openHistoryLog() throws DatastoreException
  {    
    closeHistoryLog();
    
    _projectName = Project.getCurrentlyLoadedProject().getName();
    
    String projectHistoryLogDir = Directory.getHistoryFileDir(_projectName);
    String fineTuningLogDir = Directory.getFineTuningLogDir(_projectName); 

    if (FileUtilAxi.exists(projectHistoryLogDir) == false)
      FileUtilAxi.createDirectory(projectHistoryLogDir);

    if (FileUtilAxi.exists(fineTuningLogDir) == false)
      FileUtilAxi.createDirectory(fineTuningLogDir);
    
    _ProjectHistoryLogFileFullPath = FileName.getProjectHistoryLogFileFullPath(_projectName);
    _fineTuningLogFileFullPath = FileName.getProjectFineTuningFileFullPath(_projectName);

    _historyLogFileWriter = new FileWriterUtilAxi(_ProjectHistoryLogFileFullPath, true);
    _historyLogFileWriter.open();
    _historyLogFileWriter.writeln(dateTimeFormatter.format(new Date()) + "," + " " + "V810 opened by User Account: " + _currentUserName);
    _historyLogFileWriter.writeln(dateTimeFormatter.format(new Date()) + "," + " " + _projectName + ", Version:" + Project.getCurrentlyLoadedProject().getVersion() + " is opened");
    _historyLogFileWriter.close();
    
    _fineTuningLogFileWriter = new FileWriterUtilAxi(_fineTuningLogFileFullPath, true);
    _fineTuningLogFileWriter.open();
    _fineTuningLogFileWriter.writeln("MachineSerialNumber:," + Config.getInstance().getStringValue(HardwareConfigEnum.XRAYTESTER_SERIAL_NUMBER));
    _fineTuningLogFileWriter.writeln("UserAccount:," + _currentUserName);
    _fineTuningLogFileWriter.writeln("RecipeName:," + _projectName);
    _fineTuningLogFileWriter.writeln("StartTime:," + dateTimeFormatter.format(new Date()));
    _fineTuningLogFileWriter.writeln();
    _fineTuningLogFileWriter.writeln("Modified Time,Joint Type,Subtype,Algorithm/Setting,Old Value,New Value,Unit");
    _fineTuningLogFileWriter.close();
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void append(String message)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + "," + " " + message);
      }
      catch (DatastoreException d)
      {
        // do nothing
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendCloseV810(String currentUserName)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", V810 closed by User Account:" + currentUserName);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendUndoThreshold(Subtype subtype, AlgorithmSettingEnum algorithmSettingEnum, Serializable _newValue, Serializable _oldValue)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    _jointType = subtype.getJointTypeEnum().getName();
    _measurementUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(algorithmSettingEnum);
    mathUtilEnum = Project.getCurrentlyLoadedProject().getDisplayUnits();

    if (_historyLogFileWriter != null && _fineTuningLogFileWriter != null)
    {
      try
      {
        if(_newValue instanceof Float && _oldValue instanceof Float)
        {
          Float newValue =(float)_newValue;
          Float oldValue =(float)_oldValue;
          
          if (_measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS) && Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == true) // get unit milimeter
          {
            _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Undo Threshold Value, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSettingEnum.getName() + " is changed back from " + newValue + " to " + oldValue + " " + mathUtilEnum);
            _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSettingEnum.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + mathUtilEnum);
          }
          else if (_measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS) && Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
          {
             oldValue = MathUtil.convertMillimetersToMils((float) _oldValue);
             newValue = MathUtil.convertMillimetersToMils((float) _newValue);
            _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Undo Threshold Value, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSettingEnum.getName() + " is changed back from " + newValue + " to " + oldValue + " mils");
            _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSettingEnum.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + "mils");
          }
          else
          {
            _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Undo Threshold Value, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSettingEnum.getName() + " is changed back from " + newValue + " to " + oldValue + " " + _measurementUnitsEnum);       
            _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSettingEnum.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + _measurementUnitsEnum);
          }      
        }
        else if (_newValue instanceof String && _oldValue instanceof Float)
        {
          String newValue =(String)_newValue;
          Float oldValue =(float)_oldValue;
          _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Undo Threshold Value, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSettingEnum.getName() + " is changed back from " + newValue + " to " + oldValue + " " + _measurementUnitsEnum);
          _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSettingEnum.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + _measurementUnitsEnum);
        }
         else if (_newValue instanceof Integer && _oldValue instanceof Integer)
        {
          Integer newValue =(int)_newValue;
          Integer oldValue =(int)_oldValue;
          _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Undo Threshold Value, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSettingEnum.getName() + " is changed back from " + newValue + " to " + oldValue + " " + _measurementUnitsEnum);
          _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSettingEnum.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + _measurementUnitsEnum);
        }
         else if (_newValue instanceof String && _oldValue instanceof String)
        {
          String newValue =(String)_newValue;
          String oldValue =(String)_oldValue;
          _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Undo Threshold Value, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSettingEnum.getName() + " is changed back from " + newValue + " to " + oldValue + " " + _measurementUnitsEnum);
          _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSettingEnum.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + _measurementUnitsEnum);
        }
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
   
  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendThresholdSliceSetup(Subtype subtype, AlgorithmSetting algorithmSetting, Serializable _oldValue, Serializable _newValue)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    _jointType = subtype.getJointTypeEnum().getName();
    _measurementUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(algorithmSetting.getAlgorithmSettingEnum());
    mathUtilEnum = Project.getCurrentlyLoadedProject().getDisplayUnits();
    if (_historyLogFileWriter != null && _fineTuningLogFileWriter != null)
    {
      try
      {
        if(_newValue instanceof String && _oldValue instanceof String)
        {
          String newValue =(String)_newValue;
          String oldValue =(String)_oldValue;
          _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Set Threshold Value, value is modified on Slice Setup tab, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSetting.getName() + " is set from " + oldValue + " to " + newValue + " " + _measurementUnitsEnum);
          _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSetting.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + _measurementUnitsEnum);
        }
        else if (_newValue instanceof Float && _oldValue instanceof Float)
        {
          Float newValue = (float) _newValue;
          Float oldValue = (float) _oldValue;
          if (_measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS) && Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == true) // get unit milimeter
          {
            _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Set Threshold Value, value is modified on Slice Setup tab, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSetting.getName() + " is set from " + oldValue + " to " + newValue + " " + mathUtilEnum);
            _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSetting.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + mathUtilEnum);
          }
          else if (_measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS) && Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false) //get unit mils
          {
            newValue = MathUtil.convertMillimetersToMils((float) _newValue);
            _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Set Threshold Value, value is modified on Slice Setup tab, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSetting.getName() + " is set from " + oldValue + " to " + newValue + " mils");
            _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSetting.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + "mils");
          }
          else
          {
            _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Set Threshold Value, value is modified on Slice Setup tab, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSetting.getName() + " is set from " + oldValue + " to " + newValue + " " + _measurementUnitsEnum);
            _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSetting.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + _measurementUnitsEnum);
          }
        }
        else if (_newValue instanceof String && _oldValue instanceof Float)
        {
          String newValue = (String) _newValue;
          Float oldValue = (float) _oldValue;
          if (_measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS) && Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
          {
            _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Set Threshold Value, value is modified on Slice Setup tab, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSetting.getName() + " is set from " + oldValue + " to " + newValue + " " + mathUtilEnum);
            _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSetting.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + mathUtilEnum);
          }
          else
          {
            _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Set Threshold Value, value is modified on Slice Setup tab, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSetting.getName() + " is set from " + oldValue + " to " + newValue + " " + _measurementUnitsEnum);        
            _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSetting.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + _measurementUnitsEnum);
          }
        }
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
   
  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendThreshold(Subtype subtype, AlgorithmSetting algorithmSetting, AlgorithmEnum algorithmEnum, int Value, MeasurementUnitsEnum unit)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    _jointType = subtype.getJointTypeEnum().getName();
    if (_historyLogFileWriter != null && _fineTuningLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Set Threshold Value, value is modified on " + algorithmEnum.getName() + " tab, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSetting.getName() + " is set to " + Value + " " + unit);
        _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSetting.getName() + _CSV_DELIMITER + _NOT_AVAILABLE + _CSV_DELIMITER + Value + _CSV_DELIMITER + unit);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
  
  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendThreshold(Subtype subtype, AlgorithmSetting algorithmSetting, AlgorithmEnum algorithmEnum, Serializable _oldValue, Serializable _newValue)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    _jointType = subtype.getJointTypeEnum().getName();
    _measurementUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(algorithmSetting.getAlgorithmSettingEnum());
    mathUtilEnum = Project.getCurrentlyLoadedProject().getDisplayUnits();
    if (_historyLogFileWriter != null && _fineTuningLogFileWriter != null)
    {
      try
      {
        if (_newValue instanceof String && _oldValue instanceof Float)
        {
          String newValue = (String) _newValue;
          Float oldValue = (float) _oldValue;
          if (_measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS) && Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
          {
            _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Set Threshold Value, value is modified on " + algorithmEnum.getName() + " tab, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSetting.getName() + " is set from " + oldValue + " to " + newValue + " " + mathUtilEnum);
            _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSetting.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + mathUtilEnum);
          }
          else
          {
            _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Set Threshold Value, value is modified on " + algorithmEnum.getName() + " tab, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSetting.getName() + " is set from " + oldValue + " to " + newValue + " " + _measurementUnitsEnum);
            _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSetting.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + _measurementUnitsEnum);
          }
        }
        else if (_newValue instanceof Float && _oldValue instanceof Float)
        {
          Float newValue = (float) _newValue;
          Float oldValue = (float) _oldValue;
          if (_measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS) && Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == true) // get unit milimeter
          {
            _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Set Threshold Value, value is modified on " + algorithmEnum.getName() + " tab, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSetting.getName() + " is set from " + oldValue + " to " + newValue + " " + mathUtilEnum);
            _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSetting.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + mathUtilEnum);
          }
          else if (_measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS) && Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false) //get unit mils
          {
            newValue = MathUtil.convertMillimetersToMils((float) _newValue);            
            _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Set Threshold Value, value is modified on " + algorithmEnum.getName() + " tab, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSetting.getName() + " is set from " + oldValue + " to " + newValue + " mils");
            _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSetting.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + "mils");
          }
          else
          {
            _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Set Threshold Value, value is modified on " + algorithmEnum.getName() + " tab, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSetting.getName() + " is set from " + oldValue + " to " + newValue + " " + _measurementUnitsEnum);
            _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSetting.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + _measurementUnitsEnum);
          }
        }
        else if(_newValue instanceof String && _oldValue instanceof String)
        {
          String newValue =(String)_newValue;
          String oldValue =(String)_oldValue; 
          _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Set Threshold Value, value is modified on " + algorithmEnum.getName() + " tab, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSetting.getName() + " is set from " + oldValue + " to " + newValue + " " + _measurementUnitsEnum);
          _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSetting.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + newValue + _CSV_DELIMITER + _measurementUnitsEnum);
        }
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
    
  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendThreshold(Subtype subtype, AlgorithmSetting algorithmSetting, AlgorithmEnum algorithmEnum, boolean Value)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    _jointType = subtype.getJointTypeEnum().getName();
    mathUtilEnum = Project.getCurrentlyLoadedProject().getDisplayUnits();
    boolean oldValue = true;
    
    if (Value)
      oldValue = false;  
    
    if (_historyLogFileWriter != null && _fineTuningLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Set Threshold Value, value is modified on " + algorithmEnum.getName() + " tab, Joint Type:" + _jointType + ", Subtype:" + subtype + ", " + algorithmSetting.getName() + " is set to " + Value + " " + mathUtilEnum);
        _fineTuningLogFileWriter.append(dateTimeFormatter.format(new Date()) + _CSV_DELIMITER + _jointType + _CSV_DELIMITER + subtype + _CSV_DELIMITER + algorithmSetting.getName() + _CSV_DELIMITER + oldValue + _CSV_DELIMITER + Value + _CSV_DELIMITER + mathUtilEnum);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendSubtype(Subtype subtype, SignalCompensationEnum oldValue, SignalCompensationEnum Value)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    _jointType = subtype.getJointTypeEnum().getName();
    if (_historyLogFileWriter != null && _fineTuningLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Joint Type:" + _jointType + ", Subtype:" + subtype + ", IL is changed from " + oldValue + " to " + Value);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendSubtype(Subtype subtype, ArtifactCompensationStateEnum oldValue, ArtifactCompensationStateEnum Value)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    _jointType = subtype.getJointTypeEnum().getName();
    if (_historyLogFileWriter != null && _fineTuningLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Joint Type:" + _jointType + ", Subtype:" + subtype + ", IC is changed from " + oldValue + " to " + Value);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendSubtype(Subtype subtype, UserGainEnum oldValue, UserGainEnum Value)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    _jointType = subtype.getJointTypeEnum().getName();
    if (_historyLogFileWriter != null && _fineTuningLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Joint Type:" + _jointType + ", Subtype:" + subtype + ", Gain is changed from " + oldValue + " to " + Value);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
  
  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendSubtype(Subtype subtype, StageSpeedEnum oldValue, StageSpeedEnum Value)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    _jointType = subtype.getJointTypeEnum().getName();
    if (_historyLogFileWriter != null && _fineTuningLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Joint Type:" + _jointType + ", Subtype:" + subtype + ", Speed is changed from " + oldValue + " to " + Value);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendSubtype(Subtype subtype, MagnificationTypeEnum oldValue, MagnificationTypeEnum Value)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    _jointType = subtype.getJointTypeEnum().getName();
    if (_historyLogFileWriter != null && _fineTuningLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Joint Type:" + _jointType + ", Subtype:" + subtype + ", Magnification is changed from " + oldValue + " to " + Value);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendSubtype(ComponentType componentType, Subtype OldValue, Subtype subtype)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Component:" + componentType + ", Subtype is changed from " + OldValue + " to " + subtype);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendSubtype(String _referenceDesignator, JointTypeEnum OldValue, JointTypeEnum subtype)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Component:" + _referenceDesignator + ", Joint type is changed from " + OldValue + " to " + subtype);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendSubtype(String _referenceDesignator, PadType padType, GlobalSurfaceModelEnum OldValue, GlobalSurfaceModelEnum newvalue)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Component:" + _referenceDesignator + ", Pad Type:" + padType + ", AutoFocus Method is changed from " + OldValue + " to " + newvalue);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendSubtypeRenameByComponent(ComponentType component, String oldValue, String value)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Component:" + component + ", Subtype is renamed from " + oldValue + " to " + value);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendSubtypeRenameOnly(String oldValue, String value)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", In Advance Subtype: Subtype is renamed from " + oldValue + " to " + value);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendSubtypeNoLoadNoTest(ComponentType component, String value)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Component:" + component + ", Subtype set to " + value);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
  
  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendComponentDetail(ComponentType component, JointTypeEnum jointType, PadType padType, Serializable value)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        if(value instanceof Subtype)
        {
          _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Component: "+ component+", Joint Type: "+jointType+ ", Joint:" + padType + ", Subtype set to " + value);
        }
        else
        {
          _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Component: "+ component+", Joint Type: "+jointType+ ", Joint:" + padType + ", Subtype set to " + value); 
        }
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendRenameComponentDetail(ComponentType component, JointTypeEnum jointType, String subtype,Serializable value)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        if(value instanceof Subtype)
        {
          _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Component: "+ component+", Joint Type: "+jointType+ ", Subtype:"+subtype+" is renamed to " + value);
        }
        else
        {
          _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Component: "+ component+", Joint Type: "+jointType+ ", Subtype:"+subtype+" is renamed to " + value); 
        }
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
    
  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendUndoSubtypeToNoLoad(ComponentType component)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Undo, Component:" + component + ", Subtype changed back to No Load ");
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendUndoSubtypeToNoTest(ComponentType component)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Undo, Component:" + component + ", Subtype changed back to No Test ");
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendUndoNoTestSubtype(ComponentType component, String oldValue)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Undo, Component:" + component + ", Subtype changed back from No Test to " + oldValue);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendUndoNoLoadSubtype(ComponentType component, String oldValue)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Undo, Component:" + component + ", Subtype changed back from No Load to " + oldValue);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendUndoNoLoadToNoTestSubtype(ComponentType component)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Undo, Component:" + component + ", Subtype changed back from No Load to No Test ");
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
      
  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendCreateSubtype(Collection<ComponentType> component, String value)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Component:" + component + ", New subtype: " + value + " is created");
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendWorkingUnit(MathUtilEnum oldValue, MathUtilEnum newValue)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Working Unit is changed from " + oldValue + " to " + newValue);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendUndoSubtype(Subtype subtype, String newValue, String oldValue)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    _jointType = subtype.getJointTypeEnum().getName();
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Undo, Joint Type:" + _jointType + ", Subtype is changed back from " + newValue + " to " + oldValue);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
  
  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendUndoPadType(PadType padType,JointTypeEnum jointType, Subtype newValue, Subtype oldValue)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    _padType = padType.getComponentAndPadName();
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Undo,  Component - joint: " +_padType+ ", Joint Type:" + jointType + ", Subtype is changed back from " + newValue + " to " + oldValue);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
   
  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendUndoNoTestPadType(PadType padType,JointTypeEnum jointType, Subtype oldValue)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();
    
    _padType = padType.getComponentAndPadName();
    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Undo,  Component - joint: " +_padType+ ", Joint Type:" + jointType + ", Subtype is changed back from No Test to " + oldValue);  
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
   
/**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendIsEnableLargeViewImage(boolean oldEnableLargeViewImage, boolean newEnableLargeViewImage)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();

    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Enable Large View Image is changed from " + oldEnableLargeViewImage + " to " + newEnableLargeViewImage);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
     
  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendIsGenerateMultiAngleImage(boolean oldGenerateMultiAngleImage, boolean newGenerateMultiAngleImage)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();

    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Enable Generate Multi Angle Image is changed from " + oldGenerateMultiAngleImage + " to " + newGenerateMultiAngleImage);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
    
  /**
   * @author Tan Hsia Fen
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public synchronized void appendIsGenerateComponentImage(boolean oldGenerateComponentImage, boolean newGenerateComponentImage)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();

    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Enable Generate Multi Angle Image is changed from " + oldGenerateComponentImage + " to " + newGenerateComponentImage);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
    
  /**
   * @author Siew Yeng
   */
  public synchronized void appendIsEnlargeReconstructionRegion(boolean oldEnlargeReconstructionRegion, boolean newEnlargeReconstructionRegion)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();

    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Enlarge Reconstruction Region is changed from " + oldEnlargeReconstructionRegion + " to " + newEnlargeReconstructionRegion);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
    
  /**
   * @author Swee Yee
   */
  public synchronized void appendIsBGAJointBasedThresholdInspectionEnabled(boolean oldBGAJointBasedThresholdInspectionEnabled, boolean newBGAJointBasedThresholdInspectionEnabled)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();

    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", BGA joint based threshold inspection setting is changed from " + oldBGAJointBasedThresholdInspectionEnabled + " to " + newBGAJointBasedThresholdInspectionEnabled);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
    
  /**
   * @author Swee Yee
   */
  public synchronized void appendIsBGAInspectionRegionSetTo1X1(boolean oldBGAInspectionRegionSetTo1X1, boolean newBGAInspectionRegionSetTo1X1)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();

    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", BGA inspection region setting is changed from " + oldBGAInspectionRegionSetTo1X1 + " to " + newBGAInspectionRegionSetTo1X1);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
    
  /**
   * @author Kok Chun, Tan
   */
  public synchronized void appendIsBypassMode(boolean oldIsBypassMode, boolean newIsBypassMode)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();

    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Bypass mode setting is changed from " + oldIsBypassMode + " to " + newIsBypassMode);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public synchronized void appendSetUseNewScanRoute(boolean oldScanRouteGeneration, boolean newScanRouteGeneration)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();

    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", New Scan Route Generation is changed from " + oldScanRouteGeneration + " to " + newScanRouteGeneration);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public synchronized void appendSetUseVariableDivN(boolean oldScanPassSetting, boolean newScanPassSetting)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();

    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Enable Variable DivN is changed from " + oldScanPassSetting + " to " + newScanPassSetting);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

  /**
   * @author Swee Yee
   */
  public synchronized void appendSetThicknessTableVersion(int oldThicknessTableVersion, int newThicknessTableVersion)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();

    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Thickness table version is changed from " + oldThicknessTableVersion + " to " + newThicknessTableVersion);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    }
    
  /**
   * @author Khaw Chek Hau
   * XCR2943: Alignment Fail Due to Too Close To Edge
   */
  public synchronized void appendIsEnlargeAlignmentRegion(boolean oldEnlargeAlignmentRegion, boolean newEnlargeAlignmentRegion)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();

    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Enlarge Alignment Region is changed from " + oldEnlargeAlignmentRegion + " to " + newEnlargeAlignmentRegion);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
   
  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public void closeHistoryLog() throws DatastoreException
  {
    if (_historyLogFileWriter != null)
      _historyLogFileWriter = null;  
    
    if (_fineTuningLogFileWriter != null)
      _fineTuningLogFileWriter = null;
  }
  
  public void openHistoryLogIfNecessary()
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    if (_historyLogFileWriter == null && _fineTuningLogFileWriter == null && _currentUserName != null && Project.isCurrentProjectLoaded() && 
          Config.getInstance().getBooleanValue(SoftwareConfigEnum.CREATE_HISTORY_LOG) != false)
    {
      try
      {
        openHistoryLog();
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR3178: Detail and organize finetuning history log
   */
  public void setCurrentUserName(String currentUserName)
  {
    _currentUserName = currentUserName;   
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public synchronized void appendSetDynamicRangeOptimizationVersion(int oldDynamicRangeOptimizationVersion, int newDynamicRangeOptimizationVersion)
  {
    //Khaw Chek Hau - XCR3178: Detail and organize finetuning history log
    openHistoryLogIfNecessary();

    if (_historyLogFileWriter != null)
    {
      try
      {
        _historyLogFileWriter.append(dateTimeFormatter.format(new Date()) + ", Dynamic Range Optimization is changed from " + oldDynamicRangeOptimizationVersion + " to " + newDynamicRangeOptimizationVersion);
      }
      catch (DatastoreException ex)
      {
        Logger.getLogger(ProjectHistoryLog.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
}
