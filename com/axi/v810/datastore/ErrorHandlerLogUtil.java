package com.axi.v810.datastore;

import com.axi.v810.hardware.XrayTester;
import com.axi.v810.util.*;

/**
 * This class is used to create log for recipe error 
 * @author bee-hoon.goh
 */
public class ErrorHandlerLogUtil 
{
  private static ErrorHandlerLogUtil _instance;
  private FileLoggerAxi _logUtility;
  private String _logFileNameFullPath;
  
  /**
   * @author bee-hoon.goh
   */
  private ErrorHandlerLogUtil()
  {
    //do nothing
  }

   /**
   * @author Goh Bee Hoon
   */
  public static synchronized ErrorHandlerLogUtil getInstance()
  {
    if (_instance == null)
      _instance = new ErrorHandlerLogUtil();

    return _instance;
  }
  
  /**
   * @author Goh Bee Hoon
   */
  public void log(String data) throws XrayTesterException
  {
    _logFileNameFullPath = FileName.getErrorHandlerLogFullPath();
    if (_logFileNameFullPath.isEmpty() == false)
    {
      _logUtility = new FileLoggerAxi(_logFileNameFullPath);
      _logUtility.append("Version: " + Version.getVersion() + " " + XrayTester.getInstance().getMachineDescription() + "  Error Code:  : " + data);
    }
  }

}
