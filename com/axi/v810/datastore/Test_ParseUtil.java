package com.axi.v810.datastore;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * This class performs a Unit Test on the ParseUtil class.
 *
 * @author Keith Lee
 */
public class Test_ParseUtil extends UnitTest
{
  private static final String _NON_EXISTANT_FILE = "nonExistantFile";
  private static final String _TEST_STRING = "t e s t - s t r i n g";

  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ParseUtil());
  }

  /**
   * @author Keith Lee
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

    LineNumberReader reader = null;

    String filename = "";
    try
    {
      filename = is.readLine();
    }
    catch(IOException e)
    {
      e.printStackTrace();
    }
    os.println("read in filename: " + filename);

    // test of corner cases
    boolean exception = false;
    try
    {
      reader = ParseUtil.openFile(null);
    }
    catch(AssertException ae)
    {
      exception = true;
    }
    catch(FileNotFoundDatastoreException e)
    {
      e.printStackTrace();
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      reader = ParseUtil.openFile(_NON_EXISTANT_FILE);
    }
    catch(FileNotFoundDatastoreException dfnfe)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      reader = ParseUtil.openFile(filename);
    }
    catch(FileNotFoundDatastoreException e)
    {
      // this exception should not happen
      e.printStackTrace();
    }

    try
    {
      ParseUtil.readNextLine(null, null);
    }
    catch(AssertException ae)
    {
      exception = true;
    }
    catch(DatastoreException e)
    {
      // this exception should not happen
      e.printStackTrace();
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      ParseUtil.readNextLine(null, _TEST_STRING);
    }
    catch(AssertException ae)
    {
      exception = true;
    }
    catch(DatastoreException e)
    {
      // this exception should not happen
      e.printStackTrace();
    }
    Expect.expect(exception);
    exception = false;

    try
    {
      ParseUtil.readNextLine(reader, null);
    }
    catch(AssertException ae)
    {
      exception = true;
    }
    catch(DatastoreException e)
    {
      // this exception should not happen
      e.printStackTrace();
    }
    Expect.expect(exception);
    exception = false;

    StringTokenizer st;
    String delim = " \n";
    boolean returnDelim = true;
    try
    {
      ParseUtil.getNextToken(null);
    }
    catch(AssertException ae)
    {
      exception = true;
    }
    Expect.expect(exception);
    exception = false;

    st = new StringTokenizer(_TEST_STRING, delim, returnDelim);
    String token = ParseUtil.getNextToken(st);
    Expect.expect(token.equals("t"));

    try
    {
      // full test of valid file
      String line = ParseUtil.readNextLine(reader, filename);
      while(line != null)
      {
        os.print("read in line: ");
        st = new StringTokenizer(line, delim, returnDelim);
        while(st.hasMoreTokens())
        {
          String atoken = ParseUtil.getNextToken(st);
          os.print(atoken);
          os.flush();
        }
        os.println();
        line = ParseUtil.readNextLine(reader, filename);
      }
    }
    catch(DatastoreException e)
    {
      e.printStackTrace();
    }

    try
    {
      reader.close();
    }
    catch(IOException e)
    {
      e.printStackTrace();
    }

    String testLine = "a123a456ab789ab012bab";
    String aDelimiter = "a";
    String bDelimiter = "b";
    st = new StringTokenizer(testLine,aDelimiter);
    Expect.expect((ParseUtil.getNextToken(st)).equals("123"));
    Expect.expect((ParseUtil.getNextToken(st)).equals("456"));
    Expect.expect((ParseUtil.getNextToken(st,bDelimiter)).equals("a"));
    Expect.expect((ParseUtil.getNextToken(st,bDelimiter)).equals("789a"));
    Expect.expect((ParseUtil.getNextToken(st)).equalsIgnoreCase("012"));
    Expect.expect((ParseUtil.getNextToken(st)).equalsIgnoreCase("a"));
    Expect.expect(ParseUtil.getNextToken(st) == null);

    testLine = "abc\"def\" \"ghi\"\"jkl\"";
    QuoteTokenizer qt = new QuoteTokenizer(testLine);
    Expect.expect(((ParseUtil.getNextQuoteDelimitedToken(qt)).equals("def")));
    Expect.expect(((ParseUtil.getNextQuoteDelimitedToken(qt)).equals("ghi")));
    Expect.expect(((ParseUtil.getNextQuoteDelimitedToken(qt)).equals("jkl")));
  }

}