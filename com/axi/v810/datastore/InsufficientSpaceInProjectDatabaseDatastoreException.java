package com.axi.v810.datastore;

import com.axi.util.*;

/**
 * This class gets thrown when there is not enough space in the repository to
 * add a project.
 *
 * @author George A. David
 */
public class InsufficientSpaceInProjectDatabaseDatastoreException extends DatastoreException
{
  /**
   * Construct the class.
   * @param repositoryPath the full path of the repository
   * @param projectName the name of the project that cannot be added to the repository
   * @author George A. David
   */
  public InsufficientSpaceInProjectDatabaseDatastoreException(String repositoryPath, String projectName)
  {
    super(new LocalizedString("DS_ERROR_INSUFFICIENT_SPACE_IN_PROJECT_DATABASE_KEY", new Object[]{repositoryPath, projectName}));
    Assert.expect(repositoryPath != null);
    Assert.expect(projectName != null);
  }
}
