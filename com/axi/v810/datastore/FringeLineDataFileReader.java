package com.axi.v810.datastore;

import java.io.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.hardware.ethernetBasedLightEngine.*;

/**
 * @author Cheah Lee Herng
 */
public class FringeLineDataFileReader 
{
  private static FringeLineDataFileReader _instance = null;
  
  private Pattern _commentPattern = Pattern.compile("^(.*)$");
  private LineNumberReader _is = null;
  
  /**
   * @author Cheah Lee Herng
   */
  public static synchronized FringeLineDataFileReader getInstance()
  {
    if (_instance == null)
    {
      _instance = new FringeLineDataFileReader();
    }
    return _instance;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public byte[] readDataFile(EthernetBasedLightEngineLineDataEnum lineDataEnum, int totalRecord) throws DatastoreException
  {
    Assert.expect(lineDataEnum != null);
    
    int counter = 0;
    byte[] lineDataByteArray = new byte[totalRecord];
    try
    {
      _is = ParseUtil.openFile(lineDataEnum.getFullPath());
      
      String line = getNextLine(lineDataEnum.getFullPath());
      while (line != null)
      {
        int integerValue = Integer.parseInt(line);
        lineDataByteArray[counter++] = (byte)(integerValue & 0xFF);
        line = getNextLine(lineDataEnum.getFullPath());
      }
    }
    finally
    {
      if (_is != null)
      {
        try
        {
          _is.close();
        }
        catch (IOException ioe)
        {
          DatastoreException dex = new DatastoreException(lineDataEnum.getFullPath());
          dex.initCause(ioe);
          throw dex;
        }
      }
      
      // Added by Lee Herng 2 Sept 2014
      // Sanity check to make sure we are reading FringePattern files that we are expected.
      if (counter != totalRecord)
      {
        DatastoreException dex = new DatastoreException(lineDataEnum.getFullPath());        
        throw dex;
      }
    }
    return lineDataByteArray;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private String getNextLine(String pathToFile) throws DatastoreException
  {
    String line = null;
    do
    {
      line = ParseUtil.readNextLine(_is, pathToFile);
      if (line != null)
      {
        Matcher commentMatcher = _commentPattern.matcher(line);
        if (commentMatcher.matches() == false)
        {
          throw new FileCorruptDatastoreException(pathToFile, _is.getLineNumber());
        }
        else
        {
          // get rid of any comments
          line = commentMatcher.group(1);
          // get rid of leading and trailing spaces
          line = line.trim();
        }
      }
      else
        break;
    }
    while (line.equals(""));

    return line;
  }
}
