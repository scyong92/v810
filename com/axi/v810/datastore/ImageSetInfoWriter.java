package com.axi.v810.datastore;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * Creates the file imageSet.info.
 * Every image set (collection of images for
 * offline testing) has this file that
 * currently only describes the project
 * and version number associated with the
 * image set. The file can have comments
 * (which are prepended with a hash mark #)
 * or key value pairs (key=value).
 *
 * @author George A. David
 */
public class ImageSetInfoWriter
{
  private FileWriterUtilAxi _writer;

  /**
   * @author George A. David
   */
  public ImageSetInfoWriter()
  {
    // do nothing
  }

  /**
   * @author George A. David
   */
  public void write(ImageSetData imageSetData) throws DatastoreException
  {
    Assert.expect(imageSetData != null);

    if(imageSetData.isOnlineTestDevelopment())
    {
      XrayImageIoUtil.createImageDirectoryIfNeeded(Directory.getOnlineXrayImagesDir(imageSetData.getImageSetName()));
      _writer = new FileWriterUtilAxi(FileName.getImageSetInfoFullPathForOnlineTestDevelopment(imageSetData.getImageSetName()), false);
    }
    else
    {
      XrayImageIoUtil.createImageDirectoryIfNeeded(Directory.getXrayInspectionImagesDir(imageSetData.getProjectName(),
        imageSetData.getImageSetName()));
      _writer = new FileWriterUtilAxi(FileName.getImageSetInfoFullPath(imageSetData.getProjectName(),
        imageSetData.getImageSetName()), false);
    }
    try
    {
      _writer.open();

      String line = ImageSetInfoReader.PROJECT_NAME_KEY + "=" + imageSetData.getProjectName();
      _writer.writeln(line);

      line = ImageSetInfoReader.TEST_PROGRAM_VERSION_NUMBER_KEY + "=" + imageSetData.getTestProgramVersionNumber();
      _writer.writeln(line);

      line = ImageSetInfoReader.IS_POPULATED + "=" + imageSetData.isBoardPopulated();
      _writer.writeln(line);

      line = ImageSetInfoReader.IMAGE_SET_NAME + "=" + imageSetData.getImageSetName();
      _writer.writeln(line);

      line = ImageSetInfoReader.IMAGE_SET_USER_DESCRIPTION + "=" + imageSetData.getUserDescription();
      _writer.writeln(line);

      line = ImageSetInfoReader.IMAGE_SET_TYPE + "=" + imageSetData.getImageSetTypeEnum().getName();
      _writer.writeln(line);

      if(Localization.getLocale().equals(Locale.ENGLISH))
        line = ImageSetInfoReader.IMAGE_SET_SYSTEM_DESCRIPTION + "=" + imageSetData.getSystemDescription();
      else
        line = ImageSetInfoReader.IMAGE_SET_SYSTEM_DESCRIPTION + "=" +
                StringUtil.convertUnicodeStringToAsciiString(imageSetData.getSystemDescription());
      _writer.writeln(line);

      line = ImageSetInfoReader.NUMBER_OF_IMAGES_SAVED + "=" + imageSetData.getNumberOfImagesSaved();
      _writer.writeln(line);

      line = ImageSetInfoReader.DATE_SAVED + "=" + imageSetData.getDateInMils();
      _writer.writeln(line);

      line = ImageSetInfoReader.MACHINE_SERIAL_NUMBER + "=" + imageSetData.getMachineSerialNumber();
      _writer.writeln(line);

      // XCR1594 - Added new key to keep track of 2.5D images feature
      line = ImageSetInfoReader.IS_GENERATE_MULTI_ANGLE_IMAGES + "=" + imageSetData.isGenerateMultiAngleImagesEnabled();
      _writer.writeln(line);
      
      //XCR 3322 - Add new keys to record the high and low magnification info
      if (UnitTest.unitTesting())
      {
        line = ImageSetInfoReader.HIGH_MAGNIFICATION + "=" + "M11";
      }
      else
      {
        line = ImageSetInfoReader.HIGH_MAGNIFICATION + "=" + Config.getSystemCurrentHighMagnification();
      }
      _writer.writeln(line);

      if (UnitTest.unitTesting())
      {
        line = ImageSetInfoReader.LOW_MAGNIFICATION + "=" + "M19";
      }
      else
      {
        line = ImageSetInfoReader.LOW_MAGNIFICATION + "=" + Config.getSystemCurrentLowMagnification();
      } 
       _writer.writeln(line);
      
   }
    finally
    {
      if(_writer != null)
        _writer.close();
    }
  }
}
