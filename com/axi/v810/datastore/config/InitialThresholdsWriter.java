package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projWriters.*;
import com.axi.v810.util.*;

/**
 * @author Andy Mechtenberg
 */
public class InitialThresholdsWriter
{
  private static InitialThresholdsWriter _instance;
  private EnumStringLookup _enumStringLookup;

  /**
   * @author Bill Darbie
   */
  public static synchronized InitialThresholdsWriter getInstance()
  {
    if (_instance == null)
      _instance = new InitialThresholdsWriter();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private InitialThresholdsWriter()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * @author Bill Darbie
   */
  public void write(String fullPathAndName, MathUtilEnum unitsEnum, Collection<SingleInitialThreshold> initialThresholds) throws DatastoreException
  {
    Assert.expect(fullPathAndName != null);
    Assert.expect(unitsEnum != null);
    Assert.expect(initialThresholds != null);

//    String fullPathAndName = null;
//    if (UnitTest.unitTesting())
//      fullPathAndName = fileName;
//    else
//      fullPathAndName = FileName.getInitialThresholdsFullPath(fileName);

    // make sure the initialThresholds directory exists here
    if (FileUtilAxi.existsDirectory(Directory.getInitialThresholdsDir()) == false)
    {
      FileUtilAxi.createDirectory(Directory.getInitialThresholdsDir());
    }

    FileWriter fw = null;
    try
    {
      fw = new FileWriter(fullPathAndName);
    }
    catch(IOException ioe)
    {
      CannotOpenFileDatastoreException ex = new CannotOpenFileDatastoreException(fullPathAndName);
      ex.initCause(ioe);
      throw ex;
    }
    PrintWriter os = new PrintWriter(fw);

    // ok - now write out the file
    os.println("#######################################################################");
    os.println("# FILE: " + fullPathAndName);
    os.println("# PURPOSE: To store the initial threshold value used when creating new subtypes.");
    os.println("#");
    os.println("#");
    os.println("# Each line specifies a tuned threshold value");
    os.println("#");
    os.println("# SYNTAX:");
    os.println("# units: [mils][millimeters]");
    os.println("# jointTypeEnum algorithmEnum algorithmSettingName tunedValueType tunedValue");
    os.println("#");
    os.println("########################################################################");
    os.println("# mils|millimeters");
    os.println("# jointTypeEnum algorithmEnum algorithmSettingName tunedValueType tunedValue");

    os.println(unitsEnum);
    for (SingleInitialThreshold sit : initialThresholds)
    {

      JointTypeEnum jointTypeEnum = sit.getJointTypeEnum();
      Algorithm algorithm = sit.getAlgorithm();
      AlgorithmSetting algorithmSetting = sit.getAlgorithmSetting();
      Serializable value = sit.getValue();

      String jointTypeEnumStr = _enumStringLookup.getJointTypeString(jointTypeEnum);
      String algorithmStr = _enumStringLookup.getAlgorithmString(algorithm.getAlgorithmEnum());
      String algorithmSettingStr = _enumStringLookup.getAlgorithmSettingString(algorithmSetting.getAlgorithmSettingEnum());
      String typeStr = getTypeString(value);

      os.println(jointTypeEnumStr + " " + algorithmStr + " " + algorithmSettingStr + " " + typeStr + " \"" + value + "\"");
    }

    os.close();
  }

  /**
   * @author Andy Mechtenberg
   */
  private String getTypeString(Serializable value)
  {
    Assert.expect(value != null);

    String typeStr = null;
    if (value instanceof Integer)
      typeStr = "int";
    else if (value instanceof String)
      typeStr = "string";
    else if (value instanceof Float)
      typeStr = "float";
    else
      Assert.expect(false);

    return typeStr;
  }
}
