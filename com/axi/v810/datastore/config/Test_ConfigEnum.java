package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * This enumeration contains all the hardware configuration settings for the test system.
 *
 * To add a configuration value, create a new static final instance and pass this
 * information to the constructor:
 *    name of key found in the configuration file
 *    name of the file that contains the key/value pair
 *    type of the configuration value
 *    array which holds the valid range of values (can be null)
 *    default value if no value is found (can be null)
 *    description key to use for localization
 *    type of machine the key would be found on
 *    default value for key if runned on a TDW machine
 *
 * @author Bill Darbie
 * @author Reid Hayhow
 */
public class Test_ConfigEnum extends ConfigEnum implements Serializable
{
  // the config file names
  private static final String _TEST_CFG = "usedByTestConfigWriter.config.temp";

  private static List<ConfigEnum> _hardwareConfigEnums = new ArrayList<ConfigEnum>();

  private static int _index = -1;
  private transient static List _enumList = new ArrayList();

  private static String _fileDir;

  public static final Test_ConfigEnum NAME =
      new Test_ConfigEnum(++_index, "name", _TEST_CFG, TypeEnum.STRING, null);
  public static final Test_ConfigEnum TEST_ENABLED =
          new Test_ConfigEnum(++_index, "enabled", _TEST_CFG, TypeEnum.BOOLEAN, null);
  //Added by Reid to test the new long type
  public static final Test_ConfigEnum TEST_LONG =
          new Test_ConfigEnum(++_index, "longTest", _TEST_CFG, TypeEnum.LONG, null);

  /**
   * @param id int value of the enumeration.
   * @param key String "key" name for the configuration in the file.
   * @param filename String which holds the configuration file that has this key.
   * @param type TypeEnum which determines the type of value associated with this key.
   * @param valid array of Objects which holds what values are valid. (can be null)
   * @param descriptionKey String key to use to lookup the description in the localization file.
   * @author Bill Darbie
   */
  private Test_ConfigEnum(int id,
                          String key,
                          String filename,
                          TypeEnum type,
                          Object[] valid)

  {
    super(id,
          key,
          filename,
          type,
          valid);
    _hardwareConfigEnums.add(this);
  }

  /**
   * @author Bill Darbie
   */
  static void setFileDir(String fileDir)
  {
    Assert.expect(fileDir != null);
    _fileDir = fileDir;
  }

  /**
   * @author Bill Darbie
   */
  public String getFileName()
  {
    return _fileDir + File.separator + super.getFileName();
  }

  /**
   * @author Bill Darbie
   */
  public List<ConfigEnum> getConfigEnums()
  {
    Assert.expect(_hardwareConfigEnums != null);
    return _hardwareConfigEnums;
  }
}
