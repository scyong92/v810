package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.AlgorithmEnum;
import com.axi.v810.business.imageAnalysis.AlgorithmSettingEnum;
import com.axi.v810.business.panelDesc.JointTypeEnum;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;

/**
 * @author Andy Mechtenberg
 */
public class Test_InitialThresholdsReader extends UnitTest
{
  private InitialThresholdsReader _reader;

  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_InitialThresholdsReader());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      _reader = InitialThresholdsReader.getInstance();

      try
      {
        // try opening a non-existant file
        String fileName = getTestDataDir() + File.separator + "nonExistantInitialThresholds.config";

        Collection<SingleInitialThreshold> data = _reader.read(fileName);
        Expect.expect(false);
      }
      catch(CannotOpenFileDatastoreException de)
      {
        // do nothing
      }

      for(int i = 1; i < 3; ++i)
      {
        try
        {
          // try opening an invalid file
          String fileName = getTestDataDir() + File.separator + "invalidInitialThresholds" + i + ".config";
          Collection<SingleInitialThreshold> data = _reader.read(fileName);
          Assert.expect(false);
        }
        catch(FileCorruptDatastoreException de)
        {
          // do nothing
        }
      }

      // try a good file
      String fileName = getTestDataDir() + File.separator + "initialThresholds1.config";
      Collection<SingleInitialThreshold> data = _reader.read(fileName);
      Iterator it = data.iterator();
      while(it.hasNext())
      {
        SingleInitialThreshold singleInitialThreshold = (SingleInitialThreshold)it.next();
        Expect.expect(singleInitialThreshold.getJointTypeEnum().equals(JointTypeEnum.CAPACITOR));
        Expect.expect(singleInitialThreshold.getAlgorithm().getAlgorithmEnum().equals(AlgorithmEnum.MEASUREMENT));
        Expect.expect(singleInitialThreshold.getAlgorithmSetting().getAlgorithmSettingEnum().equals(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ACROSS));
        Serializable value = singleInitialThreshold.getValue();
        Expect.expect((Float)value == 121.0);
      }

      // try opening a empty file
//      fileName = getTestDataDir() + File.separator + "emptyInitialThresholds.config";
//
//      data = _reader.read(fileName);
//      Expect.expect(data.size() == 0);
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
