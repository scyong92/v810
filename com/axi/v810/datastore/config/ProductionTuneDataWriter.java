package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Andy Mechtenberg
 */
public class ProductionTuneDataWriter
{
  private static ProductionTuneDataWriter _instance;
  private String _fileName = FileName.getProductionTuneConfigFullPath();

  /**
   * @author Andy Mechtenberg
   */
  public static synchronized ProductionTuneDataWriter getInstance()
  {
    if (_instance == null)
      _instance = new ProductionTuneDataWriter();

    return _instance;
  }

  /**
   * @author Andy Mechtenberg
   */
  private ProductionTuneDataWriter()
  {
    // do nothing
  }

  /**
   * @author Andy Mechtenberg
   */
  public void write(String defaultLocation, Collection<ProductionTuneData> productionTuneDataCollection) throws DatastoreException
  {
    Assert.expect(defaultLocation != null);
    Assert.expect(productionTuneDataCollection != null);

    // dont forget to call isExpressionValid on each item before writing anything
    // if they are not all valid do not write anything
    Iterator<ProductionTuneData> it = productionTuneDataCollection.iterator();
    while (it.hasNext())
    {
      ProductionTuneData data = (ProductionTuneData)it.next();
    }

    FileWriter fw = null;
    try
    {
      fw = new FileWriter(_fileName);
    }
    catch(IOException ioe)
    {
      CannotOpenFileDatastoreException ex = new CannotOpenFileDatastoreException(_fileName);
      ex.initCause(ioe);
      throw ex;
    }
    PrintWriter os = new PrintWriter(fw);

    // ok - now write out the file
    os.println("#######################################################################");
    os.println("# FILE: productionTune.config");
    os.println("# PURPOSE: To store all production tune settings.");
    os.println("#          This information is used to look");
    os.println("#          up which project should be used to collect");
    os.println("#          production tune data.");
    os.println("#");
    os.println("#");
    os.println("# The first line specifies the default directory location");
    os.println("#");
    os.println("# Each entry consists of seven lines.");
    os.println("# line1: project name");
    os.println("# line2: image destination directory");
    os.println("# line3: results destination directory");
    os.println("# line4: current count of number to collect");
    os.println("# line5: defect threshold to re enable the collection");
    os.println("# line6: the number to collect when re-enabled");
    os.println("# line7: the email address to use to notify of a re enable");
    os.println("#");
    os.println("# Here is an example:");
    os.println("# T:/default");
    os.println("#");
    os.println("# projectName1");
    os.println("# t:/home/projects");
    os.println("# t:/home/results");
    os.println("# 50");
    os.println("# 400");
    os.println("# 10");
    os.println("# notify@domain.com");
    os.println("#");
    os.println("########################################################################");

    if (defaultLocation.length() == 0)
      defaultLocation = "<blank>";
    os.println(defaultLocation);
    os.println("");
    it = productionTuneDataCollection.iterator();
    while (it.hasNext())
    {
      ProductionTuneData data = (ProductionTuneData)it.next();
      String projName = data.getProjectName();
      projName = projName.replaceAll("#", "\\\\#");
      if (projName.length() == 0)
        projName = "<blank>";
      os.println(projName);
      os.println(data.getImageDestination());
      os.println(data.getResultsDestination());
      os.println(data.getCurrentCount());
      os.println(data.getReEnableThreshold());
      os.println(data.getReEnableCount());
      String email = data.getNotificationEmailAddress();
      if (email.length() == 0)
        email = "<blank>";
      os.println(email);
      os.println("");
    }

    os.close();
  }

  /**
   * USE ONLY FOR UNIT TESTING!!
   * @author Andy Mechtenberg
   */
  public void setConfigFileNameForUnitTesting(String fileName)
  {
    Assert.expect(fileName != null);

    _fileName = fileName;
  }
}
