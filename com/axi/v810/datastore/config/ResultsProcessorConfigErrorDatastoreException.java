package com.axi.v810.datastore.config;

import com.axi.v810.datastore.DatastoreException;
import com.axi.util.Assert;
import com.axi.util.LocalizedString;

/**
 * @author Patrick Lacz
 */
public class ResultsProcessorConfigErrorDatastoreException extends DatastoreException
{
  /**
   * @author Patrick Lacz
   */
  public ResultsProcessorConfigErrorDatastoreException(String ruleName,
                                                       String fileName,
                                                       int lineNumber,
                                                       String actualLine)
  {
    super(new LocalizedString("DS_RESULTS_PROCESSOR_CONFIG_ERROR_KEY", new Object[]{ruleName, fileName, new Integer(lineNumber), actualLine}));
    Assert.expect(fileName != null);
    Assert.expect(actualLine != null);
    Assert.expect(ruleName != null);
  }
}
