package com.axi.v810.datastore.config;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kee chin Seong
 */
public class PreOrPostInspectionScriptOutputFileReader 
{
    private List<String> _data = new ArrayList<String>();
    private String _readerFilePath = "";
    /**
     * Author Kee chin Seong 
     * constructor
     */
    public PreOrPostInspectionScriptOutputFileReader()
    {
        
    }
    
    /**
     * @author Kee Chin Seong
     * @return param
     */
    public List<String> getPreviousSequenceSettings()
    {
       Assert.expect(_data != null);
       
       return _data;
    }
    
    /**
     * @author Kee Chin Seong
     * @param inputFileName 
     */
    public void setFilePath(String inputFileName)
    {
        _readerFilePath = inputFileName;
    }
    
    /**
     * @author Kee Chin Seong
     * @param inputFileName
     * @throws DatastoreException 
     */
    public void readPreviousSequenceSettings() throws DatastoreException
    {
        _data.clear();
        
        if (FileUtilAxi.exists(_readerFilePath))
        {
          String inputFileText = FileReaderUtilAxi.readFileContents(_readerFilePath);
          String[] lines = inputFileText.split("\\n");
          int lineNumber = 0;
          
          for (String line : lines)
          {
            ++lineNumber;
            
            // # is a comment
            if (line.startsWith("#"))
              continue;

            String[] splitLines = line.split(":");
            _data.add(line);
          }
        }
    }
}
