package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.util.*;

/**
 * @author Erica E. Wheatcroft
 */
public class Test_Config extends UnitTest
{
  private Config _config = Config.getInstance();

  /**
   * @author Erica E. Wheatcroft
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Config());
  }

  /**
   * @author Erica E. Wheatcroft
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      _config.loadIfNecessary();
    }
    catch(DatastoreException ioe)
    {
      ioe.printStackTrace();
    }
  }
}
