package com.axi.v810.datastore.config;

import java.util.regex.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projReaders.*;
import com.axi.v810.util.*;

/**
 * This class contains the serial number regular expression, the project name
 * that the regular expression maps to, and if the regulare expression is
 * a simple wild card match of * and ? or a full blown regular expression.
 *
 * @author Bill Darbie
 */
public class SerialNumberRegularExpressionData
{
  private String _originalExpression;
  private String _regularExpression;
  private String _projectName;
  private String _variationName;
  private boolean _interpretAsRegEx;
  private String _interpretAsRegExString;

  private ConfigObservable _configObservable = ConfigObservable.getInstance();

  /**
    * @param originalExpression is the regular expression as supplied by the user
    * @param interpretAsRegEx if true the regular expression should be used as a normal regular expression
    *                         if false then use ? and * wildcard characters instead of a normal regular expression.
    *
    * @author Michael Tutkowski
    */
   public SerialNumberRegularExpressionData(String originalExpression, boolean interpretAsRegEx) throws DatastoreException
   {
     setOriginalExpressionAndInterpretion(originalExpression, interpretAsRegEx);
   }

  /**
   * @author Bill Darbie
   * @param originalExpression is the regular expression as supplied by the user
   * @param projectName is the project to use if the regulare expression matches
   * @param interpretAsRegEx if true the regular expression should be used as a normal regular expression
   *              if false then use ? and * wildcard characters instead of a normal regular expression.
   *
   * @author Bill Darbie
   */
  public SerialNumberRegularExpressionData(String originalExpression, String projectName, boolean interpretAsRegEx, String variationName) throws DatastoreException
  {
    Assert.expect(projectName != null);
    Assert.expect(variationName != null);
    _projectName = projectName.intern();
    _variationName = variationName.intern();

    setOriginalExpressionAndInterpretion(originalExpression, interpretAsRegEx);
  }

  /**
   * @author Bill Darbie
   */
  public void setOriginalExpressionAndInterpretion(String originalExpression, boolean interpretAsRegEx) throws InvalidRegularExpressionDatastoreException
  {
    Assert.expect(originalExpression != null);

    _originalExpression = originalExpression.intern();
    _interpretAsRegEx = interpretAsRegEx;

    if (interpretAsRegEx)
    {
      // use real regular expressions
      _regularExpression = "^" + originalExpression + "$";
      _interpretAsRegExString = generateInterpretedAsRegularExpressStringValue(true);
    }
    else
    {
      // just use * and ? as wild cards - keep it simple

      // put a \ in front of all special reg ex characters except for * and ?
      Pattern pattern = Pattern.compile("([\\W&&[^\\*\\?]])");
      _regularExpression = originalExpression;
      Matcher matcher = pattern.matcher(_regularExpression);
      _regularExpression = matcher.replaceAll("\\\\$1");

      // now change * and ? to the correct reg expressions
      _regularExpression = _regularExpression.replaceAll("\\*", "[^\\\\n]*");
      _regularExpression = _regularExpression.replaceAll("\\?", "[^\\\\n]");
      _regularExpression = "^" + _regularExpression + "$";
      
      _interpretAsRegExString = generateInterpretedAsRegularExpressStringValue(false);
    }

    // now make sure we have a valid regular expression
    try
    {
      Pattern.compile(_regularExpression, Pattern.CASE_INSENSITIVE);
    }
    catch(PatternSyntaxException pse)
    {
      InvalidRegularExpressionDatastoreException de = new InvalidRegularExpressionDatastoreException(pse);
      de.initCause(pse);
      throw de;
    }

    _regularExpression = _regularExpression.intern();
    _configObservable.stateChanged(this);
  }

  /**
   * @author Bill Darbie
   */
  public String getOriginalExpression()
  {
    Assert.expect(_originalExpression != null);

    return _originalExpression;
  }

  /**
   * @author Bill Darbie
   */
  public String getRegularExpression()
  {
    Assert.expect(_regularExpression != null);

    return _regularExpression;
  }

  /**
   * Set the project name that should be used if the regular expression matches.
   * @author Bill Darbie
   */
  public void setProjectName(String projectName)
  {
    Assert.expect(projectName != null);

    _projectName = projectName.intern();
    _configObservable.stateChanged(this);
  }

  /**
   * @return the project name that should be used if the regular expression in the object is matched.
   *
   * @author Bill Darbie
   */
  public String getProjectName()
  {
    Assert.expect(_projectName != null);

    return _projectName;
  }

  /**
   * @return true if the regular expression in the object should be interpreted as a normal
   * regular expression, false if only * and ? are to be treated as wildcards.
   *
   * @author Bill Darbie
   */
  public boolean isInterpretedAsRegularExpression()
  {
    return _interpretAsRegEx;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public String getInterpretedAsRegularExpressionStringValue()
  {
    return _interpretAsRegExString;  
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private String generateInterpretedAsRegularExpressStringValue(boolean interpretAsRegEx)
  {
    String interpretAsRegExString = "";
    if (_interpretAsRegEx)
        interpretAsRegExString = StringLocalizer.keyToString("CFGUI_PATTERN_TYPE_REGEX_KEY");
    else
        interpretAsRegExString = StringLocalizer.keyToString("CFGUI_PATTERN_TYPE_SIMPLE_KEY");
    
    return interpretAsRegExString;
  }

  /**
   * @author Laura Cormos
   */
  public boolean isProjectNameSet()
  {
    return (_projectName != null);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void setVariationName(String variationName)
  {
    Assert.expect(variationName != null);

    _variationName = variationName.intern();
    _configObservable.stateChanged(this);
  }

  /**
   * @author Kok Chun, Tan
   */
  public String getVariationName()
  {
    Assert.expect(_variationName != null);

    return _variationName;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void clearVariationName(String projectName) throws DatastoreException
  {
    Assert.expect(projectName != null);
    boolean isPreSelectVariation = ProjectReader.getInstance().readProjectPreSelectVariation(projectName);
    
    if (isPreSelectVariation)
    {
      _variationName = VariationSettingManager.NOT_AVAILABLE;
      return;
    }
    
    List <String> variationNames = VariationSettingManager.getInstance().getProjectNameToVariationNamesMap(projectName);
    if (variationNames.isEmpty())
      _variationName = VariationSettingManager.NOT_AVAILABLE;
    else
      _variationName = variationNames.get(0);
  }
}
