package com.axi.v810.datastore.config;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import java.util.*;

/**
 *
 * @author kok-chun.tan
 */
public class PspCalibEnum extends ConfigEnum
{
  // the config file names
  private static final String _PSP_CALIB = FileName.getPspCalibFullPath();
  private static int _index = -1;

  private static List<ConfigEnum> _pspCalibEnums = new ArrayList<ConfigEnum>();
  private Map<String, Object> _pspCalibFileToDefaultValueMap = new HashMap<String, Object>();

  // no longer use this
  private Object _defaultValue;
  
  /**
   * @param id int value of the enumeration.
   * @param key the key part of the key value pair in the config file
   * @param filename String which holds the configuration file that has this key.
   * @param type TypeEnum which determines the type of value associated with this key.
   * @param valid array of Objects which holds what values are valid. (can be null)
   * @author Kok Chun, Tan
   */
  protected PspCalibEnum(int id,
                              String key,
                              String filename,
                              TypeEnum type,
                              Object[] valid,
                              Object defaultValue)
  {
    super(id,
          key,
          filename,
          type,
          valid);

    Assert.expect(defaultValue != null);
    // no longer use the value here
    _defaultValue = defaultValue;
    _pspCalibEnums.add(this);
  }
  
  //
  // Optical Calibration Profile
  //
  public static final PspCalibEnum OPTICAL_CALIBRATION_PROFILE_1_OFFSET =
      new PspCalibEnum(++_index,
                             "calOpticalCalibrationProfile1Offset",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             0);
  
  public static final PspCalibEnum OPTICAL_CALIBRATION_PROFILE_2_OFFSET =
      new PspCalibEnum(++_index,
                             "calOpticalCalibrationProfile2Offset",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             0);
  
  public static final PspCalibEnum OPTICAL_CALIBRATION_PROFILE_3_OFFSET =
      new PspCalibEnum(++_index,
                             "calOpticalCalibrationProfile3Offset",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             0);
  
  //
  // Optical Calibration values
  //
  public static final PspCalibEnum OPTICAL_CALIBRATION_LAST_TIME_RUN =
      new PspCalibEnum(++_index,
                             "calOpticalCalibrationAdjustmentLastTimeRun",
                             _PSP_CALIB,
                             TypeEnum.LONG,
                             null,
                             0);
  
  public static final PspCalibEnum OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "opticalCalibrationReferencePlaneCamera1StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             115000000);
  
  public static final PspCalibEnum OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "opticalCalibrationReferencePlaneCamera1StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             17000000);
  
  public static final PspCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "opticalCalibrationObjectPlaneMinusTwoCamera1StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             85000000);
  
  public static final PspCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "opticalCalibrationObjectPlaneMinusTwoCamera1StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             17000000);
  
  public static final PspCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "opticalCalibrationObjectPlanePlusThreeCamera1StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             55000000);
  
  public static final PspCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "opticalCalibrationObjectPlanePlusThreeCamera1StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             17000000);
  
  public static final PspCalibEnum OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "opticalCalibrationReferencePlaneCamera2StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             115000000);
  
  public static final PspCalibEnum OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "opticalCalibrationReferencePlaneCamera2StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             575000000);
  
  public static final PspCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "opticalCalibrationObjectPlaneMinusTwoCamera2StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             85000000);
  
  public static final PspCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "opticalCalibrationObjectPlaneMinusTwoCamera2StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             575000000);
  
  public static final PspCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "opticalCalibrationObjectPlanePlusThreeCamera2StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             55000000);
  
  public static final PspCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "opticalCalibrationObjectPlanePlusThreeCamera2StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             575000000);
  
  //
  // Optical Reference Plane values
  //
  public static final PspCalibEnum OPTICAL_REFERENCE_PLANE_DEFAULT_PSP_Z_HEIGHT_IN_NANOMETERS =
      new PspCalibEnum(++_index,
                             "opticalReferencePlaneDefaultPspZHeightInNanometers",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             334022);
  
  public static final PspCalibEnum OPTICAL_REFERENCE_PLANE_PSP_Z_HEIGHT_IN_NANOMETERS =
      new PspCalibEnum(++_index,
                             "opticalReferencePlanePspZHeightInNanometers",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             334022);
  
  //
  // Optical Fringe Pattern Uniformity Calibration values
  //
  public static final PspCalibEnum CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_1_INTENSITY_RATIO =
      new PspCalibEnum(++_index,
                             "calFringePatternUniformityCamera1IntensityRatio",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             1.0);
  
  public static final PspCalibEnum CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_2_INTENSITY_RATIO =
      new PspCalibEnum(++_index,
                             "calFringePatternUniformityCamera2IntensityRatio",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             1.0);
  
  //
  // Optical Fringe Pattern Global Intensity values
  //
  public static final PspCalibEnum CAL_FRINGE_PATTERN_CAMERA_1_GLOBAL_INTENSITY =
      new PspCalibEnum(++_index,
                             "calFringePatternCamera1GlobalIntensity",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             0.9);
  
  public static final PspCalibEnum CAL_FRINGE_PATTERN_CAMERA_2_GLOBAL_INTENSITY =
      new PspCalibEnum(++_index,
                             "calFringePatternCamera2GlobalIntensity",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             0.9);
  
  //
  // Optical System Fiducial Calibration values - Standard System (On-Rail Jig)
  //
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducialCamera1StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             264466000);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducialCamera1StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             2675500);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducialCamera2StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             270987500);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducialCamera2StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             589876500);
  
  // Optical System Fiducial Calibrtion value - XXL system (On-Rail jig)
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducialXXLCamera1StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             446698000);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducialXXLCamera1StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             25400000);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducialXXLCamera2StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             446698000);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducialXXLCamera2StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             607885504);

  // Optical System Fiducial Calibrtion value - S2EX system (On-Rail Jig)
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducialS2EXCamera1StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             400000000);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducialS2EXCamera1StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             3000000);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducialS2EXCamera2StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             400000000);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducialS2EXCamera2StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             592000000);
  
  // Optical System Fiducial Calibrtion value - Standard system New Manual Loaded Jig (3 plane)
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducial3PlaneManualLoadJigCamera1StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             395227500);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducial3PlaneManualLoadJigCamera1StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             438000);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducial3PlaneManualLoadJigCamera2StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             401791000);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducial3PlaneManualLoadJigCamera2StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             587787500);
  
  // Optical System Fiducial Calibrtion value - Standard system Legacy Manual Loaded Jig (5 plane)
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducial5PlaneManualLoadJigCamera1StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             395227500);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducial5PlaneManualLoadJigCamera1StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             438000);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducial5PlaneManualLoadJigCamera2StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             401791000);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calSystemFiducial5PlaneManualLoadJigCamera2StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             587787500);
  
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_LAST_TIME_RUN =
      new PspCalibEnum(++_index,
                            "calSystemFiducialConfirmationLastTimeRun",
                            _PSP_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);
  
  //
  // Optical Reference Plane Calibration values
  //
  public static final PspCalibEnum CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calOpticalReferencePlaneCamera1StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             53009800);
  
  public static final PspCalibEnum CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calOpticalReferencePLaneCamera1StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             217525600);
  
  public static final PspCalibEnum CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calOpticalReferencePlaneCamera2StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             450000000);
  
  public static final PspCalibEnum CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calOpticalReferencePlaneCamera2StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             550000000);
  
  public static final PspCalibEnum CAL_OPTICAL_REFERENCE_PLANE_LAST_TIME_RUN =
      new PspCalibEnum(++_index,
                            "calOpticalReferencePlaneConfirmationLastTimeRun",
                            _PSP_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);
  
  //
  // Optical Fiducial Rotation Confirmation values
  //
  public static final PspCalibEnum CAL_SYSTEM_FIDUCIAL_ROTATION_LAST_TIME_RUN =
      new PspCalibEnum(++_index,
                            "calSystemFiducialRotationConfirmationLastTimeRun",
                            _PSP_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);
  
  //
  // Optical Camera Light Image Confirmation values
  //
  public static final PspCalibEnum OPTICAL_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY =
      new PspCalibEnum(++_index,
                            "opticalCameraLightImageConfirmFrequency",
                            _PSP_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1); // This task should run every time it is requested to
  
  public static final PspCalibEnum OPTICAL_CAMERA_LIGHT_IMAGE_LAST_TIME_RUN =
      new PspCalibEnum(++_index,
                            "opticalCameraLightImageConfirmationLastTimeRun",
                            _PSP_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);
  
  public static final PspCalibEnum CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "confirmOpticalCameraGrayCardCamera1StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             30000000);
    
  public static final PspCalibEnum CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "confirmOpticalCameraGrayCardCamera1StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             10000000);

  public static final PspCalibEnum CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "confirmOpticalCameraGrayCardCamera2StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             30000000);

  public static final PspCalibEnum CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "confirmOpticalCameraGrayCardCamera2StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             593000000);

  public static final PspCalibEnum OPTICAL_CAMERA_1_GAIN_FACTOR =
      new PspCalibEnum(++_index,
                             "opticalCamera1GainFactor",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             11.0);

  public static final PspCalibEnum OPTICAL_CAMERA_2_GAIN_FACTOR =
      new PspCalibEnum(++_index,
                             "opticalCamera2GainFactor",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             11.0);
  
  //
  // Optical Magnification Confirmation values
  //
  public static final PspCalibEnum OPTICAL_REFERENCE_PLANE_MAGNIFICATION =
          new PspCalibEnum(++_index,
                             "opticalReferencePlaneMagnification",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             5.6);
  
  public static final PspCalibEnum OPTICAL_CAMERA_PIXEL_SIZE =
          new PspCalibEnum(++_index,
                             "opticalCameraPixelSize",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             6.0);
  
  public static final PspCalibEnum OPTICAL_NOMINAL_CALIBRATION_JIG_HOLE_DIAMETER =
          new PspCalibEnum(++_index,
                             "opticalNominalCalibrationJigHoleDiameter",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             7.0);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calMagnificationReferencePlaneCamera1StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             113000000);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calMagnificationReferencePlaneCamera1StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             2000000);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calMagnificationObjectPlaneMinusTwoCamera1StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             88000000);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calMagnificationObjectPlaneMinusTwoCamera1StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             2000000);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calMagnificationObjectPlanePlusThreeCamera1StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             63000000);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calMagnificationObjectPlanePlusThreeCamera1StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             2000000);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calMagnificationReferencePlaneCamera2StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             113000000);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calMagnificationReferencePlaneCamera2StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             592000000);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calMagnificationObjectPlaneMinusTwoCamera2StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             88000000);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calMagnificationObjectPlaneMinusTwoCamera2StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             592000000);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calMagnificationObjectPlanePlusThreeCamera2StagePositionXInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             63000000);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
      new PspCalibEnum(++_index,
                             "calMagnificationObjectPlanePlusThreeCamera2StagePositionYInNanoMeters",
                             _PSP_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             592000000);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_JIG_HOLE_DIAMETER =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera1ReferencePlaneJigHoleDiameter",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             7.5);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera1ObjectPlaneMinusTwoJigHoleDiameter",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             7.5);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera1ObjectPlanePlusThreeJigHoleDiameter",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             7.5);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera1ObjectPlaneMinusOneJigHoleDiameter",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             7.5);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera1ObjectPlanePlusFourJigHoleDiameter",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             7.5);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_JIG_HOLE_DIAMETER =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera2ReferencePlaneJigHoleDiameter",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             7.5);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera2ObjectPlaneMinusTwoJigHoleDiameter",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             7.5);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera2ObjectPlanePlusThreeJigHoleDiameter",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             7.5);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera2ObjectPlaneMinusOneJigHoleDiameter",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             7.5);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera2ObjectPlanePlusFourJigHoleDiameter",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             7.5);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_MAGNIFICATION =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera1ReferencePlaneMagnification",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             0.219428571428571);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera1ObjectPlaneMinusTwoMagnification",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             0.214285714285714);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera1ObjectPlanePlusThreeMagnification",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             0.227142857142857);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera1ObjectPlaneMinusOneMagnification",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             0.2117142857142855);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera1ObjectPlanePlusFourMagnification",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             0.22971428571428566666666666666667);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_MAGNIFICATION =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera2ReferencePlaneMagnification",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             0.219428571428571);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera2ObjectPlaneMinusTwoMagnification",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             0.214285714285714);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera2ObjectPlanePlusThreeMagnification",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             0.227142857142857);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera2ObjectPlaneMinusOneMagnification",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             0.2117142857142855);
  
  public static final PspCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION =
      new PspCalibEnum(++_index,
                             "calMagnificationCamera2ObjectPlanePlusFourMagnification",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             0.22971428571428566666666666666667);
  
  //
  // Optical System Fiducial Offset values
  //
  public static final PspCalibEnum CAL_OPTICAL_SYSTEM_FIDUCIAL_ZHEIGHT_IN_MILS =
      new PspCalibEnum(++_index,
                             "calOpticalSystemFiducialZHeightInMils",
                             _PSP_CALIB,
                             TypeEnum.DOUBLE,
                             null,
                             -81.0);
  
  public static final PspCalibEnum CAL_OPTICAL_SYSTEM_FIDUCIAL_OFFSET_LAST_TIME_RUN =
      new PspCalibEnum(++_index,
                            "calOpticalSystemFiducialOffsetAdjustmentLastTimeRun",
                            _PSP_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);
  
  public static final PspCalibEnum CONFIRM_OPTICAL_MEASUREMENT_REPEATABILITY_LAST_TIME_RUN =
      new PspCalibEnum(++_index,
                            "confirmOpticalMeasurementRepeatabilityLastTimeRun",
                            _PSP_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  /**
   * @author Kok Chun, Tan
   */
  static
  {
    // setup the default values
    setDefaultValueForCDNALastTimeRun();
    setDefaultValueForCDNAConfirmFrequency();
    
    // setup the PSP Optical Camera default values
    setPSPOpticalDefaultValues();
    
    // end hardware.calib
    validateCalibDefaultValue();
  }
  
  /**
   * @author Kok Chun, Tan
   */
  static private void setDefaultValueForCDNAConfirmFrequency()
  {
    PspCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    PspCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    PspCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    PspCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);
  }
  
  /**
   * @author Kok Chun, Tan
   */
  static private void setDefaultValueForCDNALastTimeRun()
  {
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_ROTATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_ROTATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_ROTATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_ROTATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);;

    PspCalibEnum.OPTICAL_CALIBRATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    PspCalibEnum.OPTICAL_CALIBRATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    PspCalibEnum.OPTICAL_CALIBRATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    PspCalibEnum.OPTICAL_CALIBRATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    PspCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    PspCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    PspCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    PspCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);
  }
  
  /**
   * Move from 
   * @author Wei Chin
   */
  static private void setPSPOpticalDefaultValues()
  {
    PspCalibEnum.CAL_FRINGE_PATTERN_CAMERA_1_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.STANDARD, 0.9);
    PspCalibEnum.CAL_FRINGE_PATTERN_CAMERA_1_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.9);
    PspCalibEnum.CAL_FRINGE_PATTERN_CAMERA_1_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.XXL, 0.9);
    PspCalibEnum.CAL_FRINGE_PATTERN_CAMERA_1_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.S2EX, 0.9);

    PspCalibEnum.CAL_FRINGE_PATTERN_CAMERA_2_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.STANDARD, 0.9);
    PspCalibEnum.CAL_FRINGE_PATTERN_CAMERA_2_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.9);
    PspCalibEnum.CAL_FRINGE_PATTERN_CAMERA_2_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.XXL, 0.9);
    PspCalibEnum.CAL_FRINGE_PATTERN_CAMERA_2_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.S2EX, 0.9);

    PspCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_1_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.STANDARD, 1.0);
    PspCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_1_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1.0);
    PspCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_1_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.XXL, 1.0);
    PspCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_1_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.S2EX, 1.0);

    PspCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_2_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.STANDARD, 1.0);
    PspCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_2_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1.0);
    PspCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_2_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.XXL, 1.0);
    PspCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_2_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.S2EX, 1.0);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.2117142857142855);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.2117142857142855);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1065);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.2117142857142855);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.214285714285714);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.214285714285714);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1053);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.214285714285714);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.227142857142857);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.227142857142857);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1113);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.227142857142857);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.22971428571428566666666666666667);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.22971428571428566666666666666667);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1125);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.22971428571428566666666666666667);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.2117142857142855);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.2117142857142855);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1064);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.2117142857142855);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.214285714285714);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.214285714285714);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1052);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.214285714285714);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.227142857142857);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.227142857142857);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1112);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.227142857142857);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.22971428571428566666666666666667);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.22971428571428566666666666666667);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1124);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.22971428571428566666666666666667);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.219428571428571);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.219428571428571);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1077);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.219428571428571);

    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.219428571428571);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.219428571428571);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1076);
    PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.219428571428571);

    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 88000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 88000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 396209500);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 88000000);

    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 2000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 2000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 206678020);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 2000000);

    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 88000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 88000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 396209500);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 88000000);

    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 592000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 592000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 810394020);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 592000000);

    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 63000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 63000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 370209500);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 63000000);

    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 2000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 2000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 206678020);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 2000000);

    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 63000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 63000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 370209500);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 63000000);

    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 592000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 592000000);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 810394020);
    PspCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 592000000);

    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 113000000);
    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 113000000);
    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 421009500);
    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 113000000);

    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 2000000);
    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 2000000);
    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 206678020);
    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 2000000);

    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 113000000);
    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 113000000);
    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 421009500);
    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 113000000);

    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 592000000);
    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 592000000);
    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 810394020);
    PspCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 592000000);

    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 53009800);
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 53009800);
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 53009800);
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 53009800);

    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 217525600);
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 217525600);
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 24325600);
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 217525600);

    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 450000000);
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 450000000);
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 450000000);
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 450000000);

    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 550000000);
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 550000000);
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 668800000);
    PspCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 550000000);

    // Default value for Optical System Fiducial (for S2 Standard System On-Rail Jig)
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 264466000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 264466000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 264466000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 264466000);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 2675500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 2675500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 2675500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 2675500);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 270987500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 270987500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 270987500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 270987500);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 589876500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 589876500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 589876500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 589876500);
    
    // Default value for Optical System Fiducial (for XXL System On-Rail Jig)
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 446035500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 446035500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 446035500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 446035500);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 58609000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 58609000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 58609000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 58609000);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 447600000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 447600000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 447600000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 447600000);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 645118500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 645118500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 645118500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_XXL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 645118500);

    // Default value for Optical System Fiducial (for S2EX Standard System On-Rail Jig)
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 251274000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 251274000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 251274000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 251274000);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 39274000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 39274000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 39274000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 39274000);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 253388500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 253388500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 253388500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 253388500);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 622810000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 622810000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 622810000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_S2EX_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 622810000);
    
    // Default value for Optical System Fiducial (for S2 Standard System 3-Plane New Manual Loaded Jig)
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 395325500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 395325500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 395325500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 395325500);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 2222000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 2222000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 2222000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 2222000);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 401784500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 401784500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 401784500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 401784500);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 589682000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 589682000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 589682000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_3_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 589682000);
    
    // Default value for Optical System Fiducial (for S2 Standard System 5-Plane Legacy Manual Loaded Jig)
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 395227500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 395227500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 395227500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 395227500);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 438000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 438000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 438000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 438000);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 401791000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 401791000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 401791000);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 401791000);

    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 587787500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 587787500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 587787500);
    PspCalibEnum.CAL_SYSTEM_FIDUCIAL_5_PLANE_MANUAL_LOAD_JIG_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 587787500);

    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 30000000);
    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 30000000);
    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 325034500);
    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 30000000);

    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 10000000);
    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 10000000);
    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 197780020);
    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 10000000);

    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 30000000);
    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 30000000);
    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 325034500);
    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 30000000);

    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 593000000);
    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 593000000);
    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 801496020);
    PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 593000000);

    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 85000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 85000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 85000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 85000000);

    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 17000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 17000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 17000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 17000000);

    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 85000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 85000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 85000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 85000000);

    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 575000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 575000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 575000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 575000000);

    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 55000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 55000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 55000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 55000000);

    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 17000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 17000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 17000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 17000000);

    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 55000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 55000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 55000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 55000000);

    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 575000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 575000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 575000000);
    PspCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 575000000);

    PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_1_OFFSET.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_1_OFFSET.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_1_OFFSET.setDefaultValue(SystemTypeEnum.XXL, 0);
    PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_1_OFFSET.setDefaultValue(SystemTypeEnum.S2EX, 0);

    PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_2_OFFSET.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_2_OFFSET.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_2_OFFSET.setDefaultValue(SystemTypeEnum.XXL, 0);
    PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_2_OFFSET.setDefaultValue(SystemTypeEnum.S2EX, 0);

    PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_3_OFFSET.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_3_OFFSET.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_3_OFFSET.setDefaultValue(SystemTypeEnum.XXL, 0);
    PspCalibEnum.OPTICAL_CALIBRATION_PROFILE_3_OFFSET.setDefaultValue(SystemTypeEnum.S2EX, 0);

    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 115000000);
    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 115000000);
    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 115000000);
    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 115000000);

    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 17000000);
    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 17000000);
    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 17000000);
    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 17000000);

    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 115000000);
    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 115000000);
    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 115000000);
    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 115000000);

    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 575000000);
    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 575000000);
    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 575000000);
    PspCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 575000000);

    PspCalibEnum.OPTICAL_CAMERA_1_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.STANDARD, 11.0);
    PspCalibEnum.OPTICAL_CAMERA_1_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.THROUGHPUT, 11.0);
    PspCalibEnum.OPTICAL_CAMERA_1_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.XXL, 11.0);
    PspCalibEnum.OPTICAL_CAMERA_1_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.S2EX, 11.0);

    PspCalibEnum.OPTICAL_CAMERA_2_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.STANDARD, 11.0);
    PspCalibEnum.OPTICAL_CAMERA_2_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.THROUGHPUT, 11.0);
    PspCalibEnum.OPTICAL_CAMERA_2_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.XXL, 11.0);
    PspCalibEnum.OPTICAL_CAMERA_2_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.S2EX, 11.0);

    PspCalibEnum.OPTICAL_CAMERA_PIXEL_SIZE.setDefaultValue(SystemTypeEnum.STANDARD, 6.0);
    PspCalibEnum.OPTICAL_CAMERA_PIXEL_SIZE.setDefaultValue(SystemTypeEnum.THROUGHPUT, 6.0);
    PspCalibEnum.OPTICAL_CAMERA_PIXEL_SIZE.setDefaultValue(SystemTypeEnum.XXL, 6.0);
    PspCalibEnum.OPTICAL_CAMERA_PIXEL_SIZE.setDefaultValue(SystemTypeEnum.S2EX, 6.0);

    PspCalibEnum.OPTICAL_NOMINAL_CALIBRATION_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.0);
    PspCalibEnum.OPTICAL_NOMINAL_CALIBRATION_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.0);
    PspCalibEnum.OPTICAL_NOMINAL_CALIBRATION_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.0);
    PspCalibEnum.OPTICAL_NOMINAL_CALIBRATION_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.0);

    PspCalibEnum.OPTICAL_REFERENCE_PLANE_DEFAULT_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 334022);
    PspCalibEnum.OPTICAL_REFERENCE_PLANE_DEFAULT_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 334022);
    PspCalibEnum.OPTICAL_REFERENCE_PLANE_DEFAULT_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 334022);
    PspCalibEnum.OPTICAL_REFERENCE_PLANE_DEFAULT_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 334022);

    PspCalibEnum.OPTICAL_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 5.6);
    PspCalibEnum.OPTICAL_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 5.6);
    PspCalibEnum.OPTICAL_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 5.6);
    PspCalibEnum.OPTICAL_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 5.6);

    PspCalibEnum.OPTICAL_REFERENCE_PLANE_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 334022);
    PspCalibEnum.OPTICAL_REFERENCE_PLANE_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 334022);
    PspCalibEnum.OPTICAL_REFERENCE_PLANE_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 334022);
    PspCalibEnum.OPTICAL_REFERENCE_PLANE_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 334022);

    PspCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_ZHEIGHT_IN_MILS.setDefaultValue(SystemTypeEnum.STANDARD, -86);
    PspCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_ZHEIGHT_IN_MILS.setDefaultValue(SystemTypeEnum.THROUGHPUT, -86);
    PspCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_ZHEIGHT_IN_MILS.setDefaultValue(SystemTypeEnum.XXL, 105);
    PspCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_ZHEIGHT_IN_MILS.setDefaultValue(SystemTypeEnum.S2EX, -86);

    PspCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_OFFSET_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    PspCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_OFFSET_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    PspCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_OFFSET_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    PspCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_OFFSET_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    PspCalibEnum.CONFIRM_OPTICAL_MEASUREMENT_REPEATABILITY_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    PspCalibEnum.CONFIRM_OPTICAL_MEASUREMENT_REPEATABILITY_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    PspCalibEnum.CONFIRM_OPTICAL_MEASUREMENT_REPEATABILITY_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    PspCalibEnum.CONFIRM_OPTICAL_MEASUREMENT_REPEATABILITY_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);
  }
  
  /**
   * @author Wei Chin
   */
  private void setDefaultValue(SystemTypeEnum systemType, Object defaultValue)
  {
    Assert.expect(systemType != null);
    Assert.expect(defaultValue != null);

    Object previous = _pspCalibFileToDefaultValueMap.put(FileName.getPspCalibFullPath(systemType), defaultValue);
    Assert.expect(previous == null);
  }
  
  /**
   * @author Bill Darbie
   */
  protected Object getDefaultValue()
  {
    String fileName = getFileName();
    Object value = _pspCalibFileToDefaultValueMap.get(fileName);
    Assert.expect(value != null, "No default value for " + fileName);
    return value;
//    Assert.expect(_defaultValue != null);
//    return _defaultValue;
  }
  
    /**
   * @author Wei Chin
   */
  protected Object getDefaultValue(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);
    String pspCalibFullPath = FileName.getPspCalibFullPath(systemType);
    Object defaultValue = _pspCalibFileToDefaultValueMap.get(pspCalibFullPath);
    Assert.expect(defaultValue != null);
    
    return defaultValue;
  }
  
  /**
   * @author Kok Chun, Tan
   * @return 
   */
  public List<ConfigEnum> getConfigEnums()
  {
    Assert.expect(_pspCalibEnums != null);

    return _pspCalibEnums;
  }
  
  /**
   * @return a List of all ConfigEnums available.
   * @author Bill Darbie
   */
  public static List<ConfigEnum> getPspCalibEnums()
  {
    Assert.expect(_pspCalibEnums != null);

    return _pspCalibEnums;
  }
  
  /**
   * a sanity check to enforce that all enums have a default value for all
   * system types  and initialize the correct file name. 
   * we couldn't do this beforehand because of a circular dependency.
   * @author Wei Chin
   */
  static private void validateCalibDefaultValue()
  {
    for (ConfigEnum calibEnum : _pspCalibEnums)
    {
      PspCalibEnum pspCalib = (PspCalibEnum) calibEnum;
      for (SystemTypeEnum systemType : SystemTypeEnum.getAllSystemTypes())
      {
        pspCalib.getDefaultValue(systemType);
      }
    }
  }
  
  /**
   * creates an object array of valid values for system type names
   * @author George A. David
   */
  public static Object[] getSystemTypeValidValues()
  {
    List<SystemTypeEnum> systemTypes =  SystemTypeEnum.getAllSystemTypes();
    Object[] systemTypeNames = new Object[systemTypes.size()];
    for(int i = 0; i < systemTypes.size(); ++i)
    {
      systemTypeNames[i] = systemTypes.get(i).getName();
    }

    return systemTypeNames;
  }
}
