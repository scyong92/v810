package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;

/**
 * The file will read in the serialNumToProject.config file to determine how
 * to map serial numbers to projects.
 *
 *
 * An example of the file syntax is:
 *
 * #######################################################################
 * # FILE: serialNumToProject.config
 * # PURPOSE: To store all serial number regular expression to
 * #          project name data.  This information is used to look
 * #          up which project should be used based on the serial number
 * #          entered by hand or with a bar code reader.
 * #
 * #
 * #
 * # If the line is simple then it does case insensitive
 * # matching and accepts the following wild cards:
 * #
 * #   ? - matches one of any character
 * #   * - matches one or more of any character
 * #
 * # simple
 * # 1234
 * #  projectName1
 * #
 * # simple
 * # 1234?
 * # projectName2
 * #
 * # simple
 * # 1234*
 * # projectName3
 * #
 * # If a line begines with "regEx" then it does full regular expression matching
 * #
 * # regEx
 * # 12\d\w
 * # projectName4
 * #
 * # regEx
 * # 12[a|b][a-z]*
 * # projectName5
 * #
 * ########################################################################
 *
 *
 *
 * @author Bill Darbie
 */
public class SerialNumberToProjectReader
{
  private static SerialNumberToProjectReader _instance;
  private List<SerialNumberRegularExpressionData> _serialNumberRegularExpressionData = new ArrayList<SerialNumberRegularExpressionData>();
  private String _fileName = FileName.getSerialNumberToProjectConfigFullPath();
  Pattern _commentPattern = Pattern.compile("^\\s*(#.*)?$");


  /**
   * @author Bill Darbie
   */
  public static synchronized SerialNumberToProjectReader getInstance()
  {
    if (_instance == null)
      _instance = new SerialNumberToProjectReader();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private SerialNumberToProjectReader()
  {
    // do nothing
  }

  /**
   * USE ONLY FOR UNIT TESTING!!
   * @author Bill Darbie
   */
  public void setConfigFileNameForUnitTesting(String fileName)
  {
    Assert.expect(fileName != null);

    _fileName = fileName;
  }

  /**
   * Parse in the files contents for later use.
   * @author Bill Darbie
   * @author Kok Chun, Tan
   */
  private void readSerialNumFile() throws DatastoreException
  {
    _serialNumberRegularExpressionData.clear();

    FileReader fr = null;
    try
    {
      fr = new FileReader(_fileName);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException ex = new CannotOpenFileDatastoreException(_fileName);
      ex.initCause(fnfe);
      throw ex;
    }

    LineNumberReader is = new LineNumberReader(fr);
    try
    {
      String line = null;
      int i = 0;
      String simpleOrRegEx = "";
      String serialNum = "";
      String projectName = "";
      String variationName = "";
      
      try
      {
        line = is.readLine();
      }
      catch (IOException ioe)
      {
        DatastoreException ex = new CannotWriteDatastoreException(_fileName);
        ex.initCause(ioe);
        throw ex;
      }
      
      do
      {
        if (line == null)
        {
          try
          {
            line = is.readLine();
          }
          catch (IOException ioe)
          {
            DatastoreException ex = new CannotWriteDatastoreException(_fileName);
            ex.initCause(ioe);
            throw ex;
          }
          continue;
        }
          

        // if the line is empty or a comment continue to the next line
        Matcher matcher = _commentPattern.matcher(line);
        if (matcher.matches())
        {
          try
          {
            line = is.readLine();
          }
          catch (IOException ioe)
          {
            DatastoreException ex = new CannotWriteDatastoreException(_fileName);
            ex.initCause(ioe);
            throw ex;
          }
          continue;
        }

        line = line.replaceAll("\\\\#", "#");

        if (i == 0)
        {
          simpleOrRegEx = "";
          serialNum = "";
          projectName = "";
          variationName = "";
        }

        ++i;
        if (i == 1)
        {
          simpleOrRegEx = line;
        }
        else if (i == 2)
        {
          serialNum = line;
        }
        else if (i == 3)
        {
          projectName = line;
        }
        else if (i == 4)
        {
          variationName = line;
        }
        else
          Assert.expect(false);
        
        try
        {
          line = is.readLine();
        }
        catch (IOException ioe)
        {
          DatastoreException ex = new CannotWriteDatastoreException(_fileName);
          ex.initCause(ioe);
          throw ex;
        }
        
        if (line == null)
        {
          try
          {
            line = is.readLine();
          }
          catch (IOException ioe)
          {
            DatastoreException ex = new CannotWriteDatastoreException(_fileName);
            ex.initCause(ioe);
            throw ex;
          }
          i = 0;
        }
          

        // if the line is empty or a comment continue to the next line
        matcher = _commentPattern.matcher(line);
        if (matcher.matches())
        {
          try
          {
            line = is.readLine();
          }
          catch (IOException ioe)
          {
            DatastoreException ex = new CannotWriteDatastoreException(_fileName);
            ex.initCause(ioe);
            throw ex;
          }
          i = 0;
        }
        
        if (i > 0)
          continue;

        boolean interpretAsRegEx = false;
        simpleOrRegEx = simpleOrRegEx.trim();
        if (simpleOrRegEx.equalsIgnoreCase("regEx"))
        {
          // make sure the regular expression is valid
          try
          {
            Pattern.compile(serialNum);
          }
          catch (PatternSyntaxException pse)
          {
            FileCorruptDatastoreException de = new FileCorruptDatastoreException(_fileName, is.getLineNumber());
            de.initCause(pse);
            throw de;
          }
          interpretAsRegEx = true;
        }
        else if (simpleOrRegEx.equalsIgnoreCase("simple"))
        {
          interpretAsRegEx = false;
        }
        else
        {
          FileCorruptDatastoreException de = new FileCorruptDatastoreException(_fileName, is.getLineNumber() - 2);
          throw de;
        }
        
        if (variationName.isEmpty())
        {
          variationName = VariationSettingManager.NOT_AVAILABLE;
        }

        SerialNumberRegularExpressionData data = new SerialNumberRegularExpressionData(serialNum,
                                                                                       projectName,
                                                                                       interpretAsRegEx,
                                                                                       variationName);
        _serialNumberRegularExpressionData.add(data);
      }
      while (line != null);
    }
    finally
    {
      // close the file
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch(IOException ioe)
        {
          ioe.printStackTrace();
        }
      }
    }
  }

  /**
   * @return a Collection of all the serial number regular expressions found in the config file
   * along with how the expression should be interpreted and which project name it maps to
   *
   * @author Bill Darbie
   */
  public Collection<SerialNumberRegularExpressionData> getSerialNumberRegularExpressionData() throws DatastoreException
  {
    readSerialNumFile();

    return _serialNumberRegularExpressionData;
  }
}
