package com.axi.v810.datastore.config;

import com.axi.util.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import java.util.*;

/**
 * Singleton class to store the custom settings for measurement XML output customization
 * @author huai-en.janan-ezekie
 */
public class CustomMeasurementSetting
{
  private static CustomMeasurementSetting _customMeasurementSetting = null;
  private Set<String> _customSettingMeasurement = new HashSet<String>();
  private Set<String> _customSettingJointType = new HashSet<String>();
  private Map<String,ConfigEnum> _keyToConfigEnumMap = new HashMap<String,ConfigEnum>();
  private ArrayList _customSettingMeasurementList = new ArrayList();
  private ArrayList _customSettingJointTypeList = new ArrayList();

  private CustomMeasurementSetting()
  {
     this.populateKeyToConfigEnumMap();
     this.validateCustomMeasurementFileConfigEnumUpToDate();
  }

  public synchronized static CustomMeasurementSetting getInstance()
  {
    if (_customMeasurementSetting == null)
      _customMeasurementSetting = new CustomMeasurementSetting();
   
    return _customMeasurementSetting;
  }

  /**
   * @author Janan Wong
   * Remove measurement type setting in set
   */
  public void removeCustomSettingMeasurement(String s)
  {
    _customSettingMeasurement.remove(s);
  }

  /**
   * @author Janan Wong
   * Return all measurement type setting that are being selected
   */
  public ArrayList<String> getCustomSettingMeasurement()
  {
    _customSettingMeasurementList.clear();
    _customSettingMeasurementList.addAll(_customSettingMeasurement);
    //add customer non visible measurements
    for (MeasurementEnum measurementEnum : MeasurementEnum.getNonCustomerVisibleMeasurements())
    {
      _customSettingMeasurement.add(measurementEnum.toString());
    }
    return _customSettingMeasurementList;
  }

  /**
   * @author Janan Wong
   * Add measurement type to the measurement setting list
   */
  public void addCustomSettingMeasurement(String s)
  {
    _customSettingMeasurement.add(s);
  }

  /**
   * @author Janan Wong
   * Remove joint type setting in list
   */
  public void removeCustomSettingJointType(String s)
  {
    _customSettingJointType.remove(s);
  }

  /**
   * @author Janan Wong
   * Return all joint type setting that are being selected
   */
  public ArrayList<String> getCustomSettingJointType()
  {
    _customSettingJointTypeList.clear();
    _customSettingJointTypeList.addAll(_customSettingJointType);
    return _customSettingJointTypeList;
  }

  /**
   * @author Janan Wong
   * Add joint type to the measurement setting list
   */
  public void addCustomSettingJointType(String s)
  {
    _customSettingJointType.add(s);
  }

  /**
   * @author Janan Wong
   * return custom measurements config enum based on string
   */
  public Map<String, ConfigEnum> getKeyToConfigEnumMap()
  {
    return _keyToConfigEnumMap;
  }

  /**
   * @author Janan Wong
   * populate string map to custom config enum
   */
  public void populateKeyToConfigEnumMap()
  {
    for (ConfigEnum configEnum : CustomMeasurementFileConfigEnum.getCustomMeasurementConfigEnums())
    {
      _keyToConfigEnumMap.put(configEnum.getKey().toLowerCase(), configEnum);
    }
  }
  
  /**
   * @author Janan Wong
   * remove all custom measurements setting
   */
  public void clearAllMeasurement()
  {
    _customSettingMeasurement.clear();
  }
  
  /**
   * XCR-3551 Custom Measurement XML
   * @author Janan Wong
   * 
   * To check whether the CustomMeasurementFileConfigEnum class is updated to list with MeasurementEnum
   * during initialization.
   */
  private void validateCustomMeasurementFileConfigEnumUpToDate()
  {
    int numberOfCustomerVisibleMeasurementEnum = 0;
    int numberOfCustomMeasurementConfigEnum = 0;
    Set<String> measurementEnumStringList = new HashSet<String>();
    
    int sizeOfCustomMeasurementFileConfigEnum = CustomMeasurementFileConfigEnum.getCustomMeasurementConfigEnums().size();
    Map<MeasurementEnum, Integer> measurementEnumToIntegerMap = EnumToUniqueIDLookup.getInstance().getMeasurementEnumToIntegerMap();
    List<MeasurementEnum> measurementEnums = new ArrayList<MeasurementEnum>(measurementEnumToIntegerMap.keySet());
  
    for(MeasurementEnum measurementEnum : measurementEnums)
    {
      String measurementEnumString = StringUtil.replace(measurementEnum.toString(), " ", "");
      measurementEnumStringList.add(measurementEnumString);
    }
   
    numberOfCustomerVisibleMeasurementEnum  = measurementEnumStringList.size() - MeasurementEnum.getNonCustomerVisibleMeasurements().size();
    numberOfCustomMeasurementConfigEnum  = sizeOfCustomMeasurementFileConfigEnum - CustomMeasurementFileConfigEnum.getNonMeasurementConfigEnums().size();
    Assert.expect(numberOfCustomerVisibleMeasurementEnum == numberOfCustomMeasurementConfigEnum, "New measurement enum key missing in CustomMeasurementFileConfigEnum class");
  }
}
