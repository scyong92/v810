package com.axi.v810.datastore.config;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * XCR-3551 
 * @author huai-en.janan-ezekie
 */
public class CustomMeasurementXMLConfigReader
{
  private static CustomMeasurementXMLConfigReader _customMeasurementXMLConfigReader = null;
  private Config _config;
   
  private CustomMeasurementXMLConfigReader() 
  {
    //do nothing
  }
  
  public static CustomMeasurementXMLConfigReader getInstance()
  {
    if (_customMeasurementXMLConfigReader == null)
    {
      _customMeasurementXMLConfigReader = new CustomMeasurementXMLConfigReader();
    }
    return _customMeasurementXMLConfigReader;
  }
  
  private void initializeConfig()
  {
    if (_config == null)
    {
      _config = Config.getInstance();
    }

    Assert.expect(_config != null);
  }
  
  public synchronized void load() throws DatastoreException
  {
    initializeConfig();
    Assert.expect(_config != null);

    if(FileUtilAxi.exists(FileName.getCustomMeasurementConfigurationFullPath()) == false)
    {
      String customMeasurementXMLConfigPath = FileName.getCustomMeasurementConfigurationFullPath();
      if(FileUtilAxi.exists(customMeasurementXMLConfigPath) == false)
      {
        if (FileUtilAxi.exists(Directory.getCustomMeasurementXMLSettingDir()) == false)
        {
          FileUtilAxi.createDirectory(Directory.getCustomMeasurementXMLSettingDir());
        }
          
        FileUtilAxi.createEmptyFile(customMeasurementXMLConfigPath);
      }
    }
    
    _config.load(FileName.getCustomMeasurementConfigurationFullPath());
  }
  
}
