package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.projWriters.*;

/**
 * The class reads in a List of JointTypeEnumRules for the JointTypeEnumAssignment file.
 *
 * An example of the file syntax is:
 *
 * # SYNTAX:
 * # "reference designator" "total # pads" "number of rows" "number of cols" "land pattern name" "pad in center of row or column" "jointType"
 * #
 * # All entires that are not numbers use regular expressions.
 *
 * @author Bill Darbie
 */
public class JointTypeAssignmentReader
{
  private static JointTypeAssignmentReader _instance;
  private List<JointTypeEnumRule> _jointTypeEnumRules;
  private EnumStringLookup _enumStringLookup;
  private Pattern _commentPattern = Pattern.compile("^\\s*(#.*)?$");
  private Pattern _pattern = Pattern.compile("^\\s*(enabled|disabled)\\s*\"(.*)\"\\s+\"(.*)\"\\s+\"(.*)\"\\s+\"(.*)\"\\s+\"(.*)\"\\s+\"((true|false)?)\"\\s+\"(.*)\"\\s*(#.*)?$");

  /**
   * @author Bill Darbie
   */
  public static synchronized JointTypeAssignmentReader getInstance()
  {
    if (_instance == null)
      _instance = new JointTypeAssignmentReader();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  private JointTypeAssignmentReader()
  {
    _enumStringLookup = EnumStringLookup.getInstance();
  }

  /**
   * Parse in the files contents for later use.
   * @author Bill Darbie
   */
  public List<JointTypeEnumRule> read(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);

    _jointTypeEnumRules = new ArrayList<JointTypeEnumRule>();

    String fileNameFullPath = FileName.getJointTypeAssignmentFullPath(fileName);


    FileReader fr = null;
    try
    {
      fr = new FileReader(fileNameFullPath);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException ex = new CannotOpenFileDatastoreException(fileNameFullPath);
      ex.initCause(fnfe);
      throw ex;
    }

    LineNumberReader is = new LineNumberReader(fr);

    try
    {
      String line = null;
      do
      {
        try
        {
          line = is.readLine();
        }
        catch(IOException ioe)
        {
          DatastoreException ex = new CannotWriteDatastoreException(fileNameFullPath);
          ex.initCause(ioe);
          throw ex;
        }

        if (line == null)
          continue;

        Matcher matcher = _commentPattern.matcher(line);
        if (matcher.matches())
          continue;

        //"reference designator" "total # pads" "number of rows" "number of cols" "land pattern name" "pad in center of row or column" "jointType"
        matcher = _pattern.matcher(line);
        if (matcher.matches() == false)
          throw new FileCorruptDatastoreException(fileNameFullPath, is.getLineNumber());

        String enabledOrDisabledStr = matcher.group(1);
        String refDes = matcher.group(2);
        String numPadsStr = matcher.group(3);
        String numRowsStr = matcher.group(4);
        String numColsStr = matcher.group(5);
        String landPattern = matcher.group(6);
        String padInCenterOfRowOrColStr = matcher.group(7);
        String jointTypeStr = matcher.group(9);

        // check that all patterns are valid
        try
        {
          Pattern.compile(refDes, Pattern.CASE_INSENSITIVE);
          Pattern.compile(landPattern, Pattern.CASE_INSENSITIVE);
        }
        catch (PatternSyntaxException pse)
        {
          throw new FileCorruptDatastoreException(fileNameFullPath, is.getLineNumber());
        }

        JointTypeEnumRule rule = new JointTypeEnumRule(_enumStringLookup.getJointTypeEnum(jointTypeStr));
        if (enabledOrDisabledStr.equals("enabled"))
          rule.setEnabled(true);
        else
          rule.setEnabled(false);
        if (refDes.equals("") == false)
          rule.setRefDesPattern(refDes);
        if (numPadsStr.equals("") == false)
          rule.setNumPadsPattern(numPadsStr);
        if (numRowsStr.equals("") == false)
          rule.setNumRowsPattern(numRowsStr);
        if (numColsStr.equals("") == false)
          rule.setNumColsPattern(numColsStr);
        if (landPattern.equals("") == false)
          rule.setLandPatternNamePattern(landPattern);
        if (padInCenterOfRowOrColStr.equals("") == false)
        {
          try
          {
            boolean inCenter = StringUtil.convertStringToBoolean(padInCenterOfRowOrColStr);
            rule.setPadOneInCenterOfRow(inCenter);
          }
          catch (BadFormatException bfe)
          {
            throw new FileCorruptDatastoreException(fileNameFullPath, is.getLineNumber());
          }
        }

        _jointTypeEnumRules.add(rule);
      }
      while (line != null);
    }
    finally
    {
      // close the file
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch(IOException ioe)
        {
          ioe.printStackTrace();
        }
      }
    }
    Assert.expect(_jointTypeEnumRules != null);
    return _jointTypeEnumRules;
  }
}
