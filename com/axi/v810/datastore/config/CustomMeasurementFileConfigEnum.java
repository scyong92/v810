/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.config;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import java.io.*;
import java.util.*;

/**
 *
 * @author Janan Wong
 */
public class CustomMeasurementFileConfigEnum extends ConfigEnum implements Serializable
{
  private static int _index = -1;
  private static final String _DEFAULT_CUSTOM_MEASUREMENT_CONFIG = FileName.getCustomMeasurementConfigurationFullPath();
  private static List<ConfigEnum> _customMeasurementConfigEnum = new ArrayList<ConfigEnum>();

  private Object _defaultValue;

  /**
   * @author Janan Wong
   */
  protected CustomMeasurementFileConfigEnum(int id,
                                            String key,
                                            String filename,
                                            TypeEnum type,
                                            Object[] valid,
                                            Object defaultValue)
  {
    super(id,
          key,
          filename,
          type,
          valid);
    _defaultValue = defaultValue;
    _customMeasurementConfigEnum.add(this);
  }
  public static final CustomMeasurementFileConfigEnum CUSTOMIZE_MEASUREMENT_OUTPUT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "custom",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_CAPACITOR
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "capacitor",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_CGA
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "cga",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_COLLAPSIBLE_BGA
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "collapsibleBGA",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_CSP
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "csp",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_EXPOSED_PAD
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "exposedPad",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_GULLWING
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "gullwing",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_JLEAD
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "jlead",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_LEADLESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "leadless",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_LGA
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "lga",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_NON_COLLAPSIBLE_BGA
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "nonCollapsibleBGA",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_OVAL_PTH
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "ovalPTH",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_POLARIZED_CAP
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "polarizedCapacitor",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_PRESSFIT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "pressfit",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_PTH
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "pth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_QFN
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "qfn",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_RESISTOR
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "resistor",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_RF_SHIELD
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "rfShield",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_SINGLE_PAD
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "singlePad",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_SMT_CONNECTOR
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "smtConnector",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_SOT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "sot",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum JOINT_TYPE_VARIABLE_HEIGHT_BGA_CONNECTOR
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "variableHeightBGAConnector",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_CENTER_ACROSS_LEADING_TRAILING_SLOPE_SUM
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "acrossCenterLeading/TrailingSlopeSum",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_CENTER_ACROSS_SLOPE_SUM
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "acrossCenterSumOfSlopes",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

//  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_ACROSS_PROFILE_CENTER_THICKNESS
//    = new CustomMeasurementFileConfigEnum(++_index,
//                                          "acrossCenterThickness",
//                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
//                                          TypeEnum.BOOLEAN,
//                                          new Object[]{new Boolean(true), new Boolean(false)},
//                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_ACROSS_PROFILE_CENTER_TO_HEEL_2_THICKNESS_PERCENT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "acrossCenterToHeel2ThicknessPercent",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_ACROSS_PROFILE_CENTER_TO_HEEL_THICKNESS_PERCENT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "acrossCenterToHeelThicknessPercent",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_ACROSS_PROFILE_CONCAVITY_RATIO
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "acrossConcavityRatio",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_ACROSS_PROFILE_HEEL_2_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "acrossHeel2Thickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_HEEL_ACROSS_LEADING_TRAILING_SLOPE_SUM
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "acrossHeelLeading/TrailingSlopeSum",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},  
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_HEEL_ACROSS_SLOPE_SUM
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "acrossHeelSumOfSlopes",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

//  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_ACROSS_PROFILE_HEEL_THICKNESS
//    = new CustomMeasurementFileConfigEnum(++_index,
//                                          "acrossHeelThickness",
//                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
//                                          TypeEnum.BOOLEAN,
//                                          new Object[]{new Boolean(true), new Boolean(false)},
//                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_PROFILE_OFFSET_ALONG_IN_PERCENTAGE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "acrossProfileOffsetFromUpperFillet",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

//  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_AVERAGE_BACKGROUND_VALUE
//    = new CustomMeasurementFileConfigEnum(++_index,
//                                          "averageBackgroundValue",
//                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
//                                          TypeEnum.BOOLEAN,
//                                          new Object[]{new Boolean(true), new Boolean(false)},
//                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_BODY_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "bodyThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_BROKEN_PIN_AREA_SIZE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "brokenPinAreaSize",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
    
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_CENTER_ACROSS_WIDTH
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "centerAcrossWidth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
    
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_CENTER_FILLET_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "centerFilletThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
    
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_CENTER_SLOPE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "centerSlope",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
    
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_SOLDER_BALL_SHORT_DELTA_THICKNESS 
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "centerSolderBallShortThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);    
    
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_QFN_CENTER_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
										  "centerThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
     
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_CENTER_THICKNESS_PERCENT_OF_NOMINAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "centerThicknessPercentOfNominal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
     
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_CENTER_THICKNESS_REGION_OUTLIER
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "centerThicknessRegionOutlier",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "centerToUpperFilletThicknessPercentage",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_CENTER_TO_HEEL_THICKNESS_PERCENT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "centerToHeelThicknessPercent",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_CLEAR_OPEN_SIGNAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "clearOpenSignal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_COMPONENT_ACROSS_PROFILE_SIZE_IN_PERCENTAGE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "componentAcrossProfileSize",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_COMPONENT_BODY_LENGTH
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "componentBodyLength",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_COMPONENT_LENGTH_PERCENT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "componentBodyLength%OfNominal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_COMPONENT_MIDBALL_VOIDING
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "componentMiballVoiding",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_COMPONENT_COMPONENT_ROTATION
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "componentRotation",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_COMPONENT_SHIFT_ACROSS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "componentShiftAcross",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_COMPONENT_SHIFT_ALONG_IN_PERCENTAGE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "componentShiftAlong",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_COMPONENT_TILT_RATIO
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "componentTiltRatio",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_COMPONENT_VOIDING_PERCENT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "componentVoidingArea",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_BODY_WIDTH
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "componentWidthAcross",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_DEGREE_COVERAGE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "degreeCoverage",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_DIAMETER 
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "diameter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
//  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_DIAMETER_DIFFERENCE_FROM_NEIGHBORS
//    = new CustomMeasurementFileConfigEnum(++_index,
//                                          "diameterDifferenceFromNeighbors",
//                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
//                                          TypeEnum.BOOLEAN,
//                                          new Object[]{new Boolean(true), new Boolean(false)},
//                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_DIFFERENCE_FROM_NOMINAL_GRAYLEVEL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "differenceFromNominalGraylevel",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_DIFFERENCE_FROM_NOMINAL_SOLDER_SIGNAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "differenceFromNominalSolderSignal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_DIFFERENCE_FROM_REGION_AVERAGE_GRAYLEVEL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "differenceFromRegionAverageGraylevel",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_DIFFERENCE_FROM_REGION_SOLDER_SIGNAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "differenceFromRegionAverageSolderSignal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);   
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OPEN_ECCENTRICITY
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "eccentricity",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_ENABLE_MAXIMUM_CENTER_TO_TOE_PERCENT_TEST
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "enableToeTestIfCenterToHeelThicknessTestFail",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_NOMINAL_SHIFT_ROTATION_IN_DEGREES
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "estimatedMaximumRotation",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_EXONERATED_VOIDING_GREY_LEVEL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "exoneratedVoidingAreaValue",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_EXCESS_SOLDER_SIGNAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "excessSolderSignal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ACROSS_CENTER_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap1ThicknessAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ACROSS_EAST_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap1ThicknessAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ACROSS_WEST_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap1ThicknessAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
    
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ALONG_MIDDLE_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap1ThicknessAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ALONG_NORTH_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap1ThicknessAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);  
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ALONG_SOUTH_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap1ThicknessAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);  
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ACROSS_CENTER_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap2ThicknessAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);  
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ACROSS_EAST_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap2ThicknessAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);  
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ACROSS_WEST_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap2ThicknessAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);  
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ALONG_MIDDLE_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap2ThicknessAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);  
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ALONG_NORTH_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap2ThicknessAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);  
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ALONG_SOUTH_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap2ThicknessAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ACROSS_CENTER_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap3ThicknessAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ACROSS_EAST_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap3ThicknessAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ACROSS_WEST_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap3ThicknessAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ALONG_MIDDLE_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap3ThicknessAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ALONG_NORTH_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap3ThicknessAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ALONG_SOUTH_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap3ThicknessAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ACROSS_CENTER_4 
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap4ThicknessAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ACROSS_EAST_4 
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap4ThicknessAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ACROSS_WEST_4 
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap4ThicknessAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ALONG_MIDDLE_4 
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap4ThicknessAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ALONG_NORTH_4 
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap4ThicknessAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLED_GAP_THICKNESS_ALONG_SOUTH_4 
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filledGap4ThicknessAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLET_GAP
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filletGap",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLET_LENGTH
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filletLength",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
    
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLET_LENGTH_PERCENT_OF_NOMINAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filletLengthPercentOfNominal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);     
    
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLET_LENGTH_REGION_OUTLIER
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filletLengthRegionOutlier",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
      
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLET_ONE_THICKNESS_PERCENT_OF_NOMINAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filletOneThicknessPercentOfNominal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);   
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLET_PEAK_OFFSET_FROM_REGION_NEIGHBORS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filletPeakOffsetFromRegionNeighbors",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
//  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLET_PEAK_POSITION
//    = new CustomMeasurementFileConfigEnum(++_index,
//                                          "filletPeakPosition",
//                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
//                                          TypeEnum.BOOLEAN,
//                                          new Object[]{new Boolean(true), new Boolean(false)},
//                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLET_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filletThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLET_THICKNESS_PERCENT_OF_NOMINAL 
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filletThicknessPercentOfNominal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLET_TWO_THICKNESS_PERCENT_OF_NOMINAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filletTwoThicknessPercentOfNominal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLET_FILLET_WIDTH_JOINT_RATIO
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "filletWidthAcrossRatioBetweenTwoJoint",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GRAYLEVEL 
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "graylevel",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_HIP_OUTLIER_SCORE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "hipOutlier",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_HIP_VALUE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "hipValue",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
    
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_HEEL_ACROSS_WIDTH
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "heelAcrossWidth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
 
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_HEEL_POSITION_DISTANCE_FROM_NOMINAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "heelPositionDistanceFromNominal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_HEEL_SHARPNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "heelSharpness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_HEEL_SLOPE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "heelSlope",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_HEEL_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "heelThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_HEEL_THICKNESS_PERCENT_OF_NOMINAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "heelThicknessPercentOfNominal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 
    
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_HEEL_TO_PAD_CENTER
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "heelToPadCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INDIVIDUAL_VOID_PERCENT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "individualVoidArea",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INDIVIDUAL_VOIDING_DIAMETER_PERCENT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "individualVoidDiameter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INDIVIDUAL_VOIDING_PERCENT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "individualVoidingArea",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ACROSS_CENTER_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap1AcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ACROSS_EAST_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap1AcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ACROSS_WEST_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap1AcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ALONG_MIDDLE_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap1AlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ALONG_NORTH_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap1AlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ALONG_SOUTH_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap1AlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ACROSS_CENTER_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap2AcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ACROSS_EAST_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap2AcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ACROSS_WEST_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap2AcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ALONG_MIDDLE_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap2AlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ALONG_NORTH_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap2AlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ALONG_SOUTH_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap2AlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ACROSS_CENTER_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap3AcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ACROSS_EAST_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap3AcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ACROSS_WEST_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap3AcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ALONG_MIDDLE_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap3AlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ALONG_NORTH_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap3AlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ALONG_SOUTH_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap3AlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ACROSS_CENTER_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap4AcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ACROSS_EAST_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap4AcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ACROSS_WEST_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap4AcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ALONG_MIDDLE_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap4AlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ALONG_NORTH_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap4AlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_RATIO_ALONG_SOUTH_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "inner%OfOuterGap4AlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ACROSS_CENTER_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap1LengthAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ACROSS_EAST_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap1LengthAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ACROSS_WEST_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap1LengthAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ALONG_MIDDLE_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap1LengthAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ALONG_NORTH_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap1LengthAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ALONG_SOUTH_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap1LengthAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ACROSS_CENTER_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap2LengthAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ACROSS_EAST_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap2LengthAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ACROSS_WEST_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap2LengthAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ALONG_MIDDLE_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap2LengthAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ALONG_NORTH_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap2LengthAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ALONG_SOUTH_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap2LengthAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ACROSS_CENTER_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap3LengthAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ACROSS_EAST_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap3LengthAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ACROSS_WEST_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap3LengthAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ALONG_MIDDLE_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap3LengthAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ALONG_NORTH_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap3LengthAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ALONG_SOUTH_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap3LengthAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ACROSS_CENTER_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap4LengthAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ACROSS_EAST_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap4LengthAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ACROSS_WEST_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap4LengthAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ALONG_MIDDLE_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap4LengthAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ALONG_NORTH_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap4LengthAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INNER_GAP_ALONG_SOUTH_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "innerGap4LengthAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_INSUFFICIENT_PAD_AREA_PERCENT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "insufficientAreaPercentage",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLET_WIDTH
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "jointFilletWidthAcross",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

//  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_JOINT_X_OFFSET_FROM_CAD
//    = new CustomMeasurementFileConfigEnum(++_index,
//                                          "jointOffsetX",
//                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
//                                          TypeEnum.BOOLEAN,
//                                         new Object[]{new Boolean(true), new Boolean(false)},
//                                          false);

//  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_JOINT_Y_OFFSET_FROM_CAD
//    = new CustomMeasurementFileConfigEnum(++_index,
//                                          "jointOffsetY",
//                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
//                                          TypeEnum.BOOLEAN,
//                                         new Object[]{new Boolean(true), new Boolean(false)},
//                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_JOINT_OFFSET_FROM_CAD
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "jointOffsetFromCAD",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_PARTIAL_FILLET_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "jointPartialFilletThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_LEADING_SLOPE_ACROSS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "leadingSlopeAcross",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_LEADING_SLOPE_ALONG
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "leadingSlopeAlong",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

//  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_LOWER_FILLET_THICKNESS
//    = new CustomMeasurementFileConfigEnum(++_index,
//                                          "lowerFilletThickness",
//                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
//                                          TypeEnum.BOOLEAN,
//                                          new Object[]{new Boolean(true), new Boolean(false)},
//                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_MASS_SHIFT_ACROSS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "massShiftAcross",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_TEMPLATE_MATCHING
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "matchingPercent",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_MAXIMUM_CENTER_TO_HEEL_PERCENT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "maximumCenterToHeelThicknessPercent",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_MAXIMUM_CENTER_TO_TOE_PERCENT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "maximumCenterToToeThicknessPercent",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_NEIGHBOR_LENGTH_DIFFERENCE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "neighborLengthDifference",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_NEIGHBOR_OUTLIER_SCORE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "neighborOutlier",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);  

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_NUMBER_FAILING_GAPS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "numberOfFailingGaps",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OFFSET_FROM_CAD
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "offsetFromCAD",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);    

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OFFSET_FROM_EXPECTED_LOCATION
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "offsetFromRegionJoints",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OPAQUE_OPEN_SIGNAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "opaqueOpenSignal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);    

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OPEN_SIGNAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "openSignal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);    

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ACROSS_CENTER_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap1LengthAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ACROSS_EAST_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap1LengthAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ACROSS_WEST_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap1LengthAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ALONG_MIDDLE_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap1LengthAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ALONG_NORTH_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap1LengthAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ALONG_SOUTH_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap1LengthAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ACROSS_CENTER_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap1ThicknessAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ACROSS_EAST_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap1ThicknessAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ACROSS_WEST_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap1ThicknessAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ALONG_MIDDLE_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap1ThicknessAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ALONG_NORTH_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap1ThicknessAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ALONG_SOUTH_1
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap1ThicknessAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ACROSS_CENTER_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap2LengthAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ACROSS_EAST_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap2LengthAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ACROSS_WEST_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap2LengthAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ALONG_MIDDLE_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap2LengthAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ALONG_NORTH_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap2LengthAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ALONG_SOUTH_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap2LengthAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ACROSS_CENTER_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap2ThicknessAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ACROSS_EAST_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap2ThicknessAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ACROSS_WEST_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap2ThicknessAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ALONG_MIDDLE_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap2ThicknessAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ALONG_NORTH_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap2ThicknessAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ALONG_SOUTH_2
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap2ThicknessAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ACROSS_CENTER_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap3LengthAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ACROSS_EAST_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap3LengthAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ACROSS_WEST_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap3LengthAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ALONG_MIDDLE_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap3LengthAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ALONG_NORTH_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap3LengthAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ALONG_SOUTH_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap3LengthAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ACROSS_CENTER_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap3ThicknessAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ACROSS_EAST_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap3ThicknessAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ACROSS_WEST_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap3ThicknessAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ALONG_MIDDLE_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap3ThicknessAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ALONG_NORTH_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap3ThicknessAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ALONG_SOUTH_3
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap3ThicknessAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ACROSS_CENTER_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap4LengthAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ACROSS_EAST_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap4LengthAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ACROSS_WEST_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap4LengthAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ALONG_MIDDLE_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap4LengthAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ALONG_NORTH_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap4LengthAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OUTER_GAP_ALONG_SOUTH_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap4LengthAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ACROSS_CENTER_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap4ThicknessAcrossCenter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ACROSS_EAST_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap4ThicknessAcrossEast",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ACROSS_WEST_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap4ThicknessAcrossWest",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ALONG_MIDDLE_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap4ThicknessAlongMiddle",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ALONG_NORTH_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap4ThicknessAlongNorth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_GAP_THICKNESS_ALONG_SOUTH_4
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "outerGap4ThicknessAlongSouth",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_PAD_SOLDER_AREA_PERCENTAGE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "padSolderAreaPercentage",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_PAD_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "padThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_COMPONENT_LEVEL_VOIDING_PERCENT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "percentOfJointsFailingComponent-levelVoiding",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_PERCENT_OF_JOINTS_WITH_INSUFFICIENT_DIAMETER
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "percentOfJointsWithMarginalDiameter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_DIAMETER_PERCENT_OF_NOMINAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "percentOfNominalDiameter",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_THICKNESS_PERCENT_OF_NOMINAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "percentOfNominalThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_PIN1_ALONG_OFFSET_IN_PERCENTAGE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "pin1AlongOffset",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_PIN1_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "pin1MisalignmentAlongDelta",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_PIN2_ALONG_OFFSET_IN_PERCENTAGE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "pin2AlongOffset",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_PIN2_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "pin2MisalignmentAlongDelta",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_PIN_LENGTH
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "pinLength",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLET_ONE_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "pinOneFilletThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

//  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_PAD_ONE_LOWER_FILLET_THICKNESS
//    = new CustomMeasurementFileConfigEnum(++_index,
//                                          "pinOneLowerFilletThickness",
//                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
//                                          TypeEnum.BOOLEAN,
//                                          new Object[]{new Boolean(true), new Boolean(false)},
//                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OPEN_PAD_ONE_OPEN_SIGNAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "pinOneOpenSignal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_FILLET_TWO_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "pinTwoFilletThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

//  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_PAD_TWO_LOWER_FILLET_THICKNESS
//    = new CustomMeasurementFileConfigEnum(++_index,
//                                          "pinTwoLowerFilletThickness",
//                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
//                                          TypeEnum.BOOLEAN,
//                                          new Object[]{new Boolean(true), new Boolean(false)},
//                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OPEN_PAD_TWO_OPEN_SIGNAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "pinTwoOpenSignal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

//  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_LOCATOR_X_LOCATION
//    = new CustomMeasurementFileConfigEnum(++_index,
//                                          "pixelXLocationInRegion",
//                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
//                                          TypeEnum.BOOLEAN,
//                                          new Object[]{new Boolean(true), new Boolean(false)},
//                                          false);

//  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_LOCATOR_Y_LOCATION
//    = new CustomMeasurementFileConfigEnum(++_index,
//                                          "pixelYLocationInRegion",
//                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
//                                          TypeEnum.BOOLEAN,
//                                          new Object[]{new Boolean(true), new Boolean(false)},
//                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_POLARITY_SIGNAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "polaritySignal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_REGION_OUTLIER
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "regionOutlier",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_ROUNDNESS_PERCENT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "roundnessPercent",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_SHORT_LENGTH
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "shortLength",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_SHORT_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "shortThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_SLOPE_SUM_ACROSS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "slopeSumAcross",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_SLOPE_SUM_ALONG
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "slopeSumAlong",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_SLUG_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "slugThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_SOLDER_SIGNAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "solderSignal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_SOLDER_THICKNESS_AT_LOCATION
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "solderThicknessAtLocation",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_MAX_HEEL_AND_TOE_SLOPES_SUM
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "sumOfHeelAndToeSlopes",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false); 

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_SUM_OF_SLOPE_CHANGES
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "sumOfSlopeChanges",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "thickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_MAX_TOE_SLOPE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "toeSlope",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_MAX_TOE_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "toeThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_TOE_THICKNESS_PERCENT_OF_NOMINAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "toeThicknessPercentOfNominal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_TRAILING_SLOPE_ACROSS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "trailingSlopeAcross",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_TRAILING_SLOPE_ALONG
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "trailingSlopeAlong",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_UPWARD_CURVATURE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "upwardCurvature",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_VOID_AREA
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "voidArea",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_DIFFERENCE_FROM_NOMINAL_PERCENTAGE 
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "voidGreyLevelDifferenceFromNominal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_VOID_VOLUME_PERCENTAGE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "voidVolumePercentage",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_VOIDING_PERCENT
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "voidingArea",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_DEFINITE_VOIDING_GREY_LEVEL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "voidingAreaValue",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_VOIDING_PERCENTAGE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "voidingPercentage",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_WETTING_COVERAGE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "wettingCoverage",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
										  
//  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_IMAGE_RESIZE_SCALE
//    = new CustomMeasurementFileConfigEnum(++_index,
//                                          "imageResizeScale",
//                                         _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
//                                          TypeEnum.BOOLEAN,
//                                          new Object[]{new Boolean(true), new Boolean(false)},
//                                         false);
										  
 public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_CENTER_OFFSET_FOR_LEARNING
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "centerOffsetForLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
										  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "centerOfSolderVolume",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
										  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_CENTER_OF_CENTER_OF_SOLDER_VOLUME
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "centerOfSolderVolumeLocationPercentOfNominal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_LEARNING_CENTER_OF_SOLDER_VOLUME
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "centerOfSolderVolumeForLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_EDGE_TO_CENTER_DISTANCE_PERCENT_OF_FILLET_LENGTH_ALONG
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "edgetoCenterDistanceforLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);										  
										  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_EDGE_TO_HEEL_PEAK_DISTANCE_PERCENT_OF_FILLET_LENGTH_ALONG
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "edgetoHeelPeakDistanceforLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_LEARNING_OPEN_SIGNAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "openSignalForLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
										  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_LEARNING_HEEL_EDGE_PERCENT_OF_MAX_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "heelEdgeThicknessPercentForLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_HEEL_EDGE_TO_PEAK_DISTANCE_PERCENT_OF_PAD_LENGTH_ALONG
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "heelEdgeToPeakDistanceForLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_LEARNING_HEEL_EDGE_MAX_SLOPE_INDEX
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "heelMaxSlopeIndexforLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_HEEL_POSITION
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "heelPosition",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_EDGE_TO_TOE_PEAK_DISTANCE_PERCENT_OF_FILLET_LENGTH_ALONG
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "heelToToeOffsetDistanceForLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_HEEL_TO_PAD_CENTER_DIFFERENCE_FROM_NOMINAL
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "heelToPadCenterDifferenceFromNominal",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_OVERALL_SHARPNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "overallSharpness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
										
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_AVERAGE_PAD_ONE_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "padOneAverageThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
										  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_AVERAGE_PAD_TWO_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "padTwoAverageThickness",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
										  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_LEARNING_PIN_LENGTH
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "pinLengthForLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
										  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_PROFILE_SHARPNESS_ALONG
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "sharpnessAlongProfile",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
										  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_LEARNING_CENTER_OFFSET
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "sideFilletCenterOffsetForLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_SIDE_FILLET_EDGE_PERCENT_OF_MAX_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "sideFilletEdgeThicknessPercentForLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_EDGE_TO_PEAK_DISTANCE
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "sideFilletToPeakDistanceForLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_SLUG_EDGE_REGION_LENGTH_ACROSS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "slugEdgeRegionLengthAcross",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
										  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_SLUG_EDGE_REGION_LENGTH_ALONG
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "slugEdgeRegionLengthAlong",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_SLUG_EDGE_REGION_POSITION
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "slugEdgeRegionPosition",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_SLUG_EDGE_THRESHOLD
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "slugEdgeThreshold",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
										  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_TOE_EDGE_THICKNESS_PERCENT_OF_NOMINAL_TOE_THICKNESS
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "toeEdgeThicknessPercentForLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
										  
  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_HEEL_EDGE_MAX_SLOPE_INDEX
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "toeMaxSlopeIndexForLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_UPWARD_CURVATURE_END_PERCENT_OF_FILLET_LENGTH_ALONG
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "upwardCurvatureEndPercentForLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_UPWARD_CURVATURE_START_PERCENT_OF_FILLET_LENGTH_ALONG
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "upwardCurvatureStartPercentForLearning",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);		

  public static final CustomMeasurementFileConfigEnum MEASUREMENT_TYPE_WETTING_COVERAGE_SENSITIVITY
    = new CustomMeasurementFileConfigEnum(++_index,
                                          "wettingCoverageSensitivity",
                                          _DEFAULT_CUSTOM_MEASUREMENT_CONFIG,
                                          TypeEnum.BOOLEAN,
                                          new Object[]{new Boolean(true), new Boolean(false)},
                                          false);
										  
  private static List<ConfigEnum> _nonMeasurementConfigEnum = new ArrayList<ConfigEnum>();
  static
  {
	 _nonMeasurementConfigEnum.add(CUSTOMIZE_MEASUREMENT_OUTPUT);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_CAPACITOR);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_CGA);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_COLLAPSIBLE_BGA);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_CSP);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_EXPOSED_PAD);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_GULLWING);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_JLEAD);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_LEADLESS);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_LGA);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_NON_COLLAPSIBLE_BGA);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_OVAL_PTH);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_POLARIZED_CAP);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_PRESSFIT);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_PTH);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_QFN);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_RESISTOR);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_RF_SHIELD);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_SINGLE_PAD);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_SMT_CONNECTOR);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_SOT);
	 _nonMeasurementConfigEnum.add(JOINT_TYPE_VARIABLE_HEIGHT_BGA_CONNECTOR);	 
  }
  /**
   * @return a List of all ConfigEnums available.
   * @author Janan Wong
   */
  public static List<ConfigEnum> getCustomMeasurementConfigEnums()
  {
    Assert.expect(_customMeasurementConfigEnum != null);

    return _customMeasurementConfigEnum;
  }

  /**
   * @author Janan Wong
   */
  public List<ConfigEnum> getConfigEnums()
  {
    Assert.expect(_customMeasurementConfigEnum != null);

    return _customMeasurementConfigEnum;
  }

  /**
   * @author Janan Wong
   */
  protected Object getDefaultValue()
  {
    return _defaultValue;
  }

   /**
    * @author Janan Wong
    * @return specific CustomMeasurementFileConfigEnum 
    */
  public static ConfigEnum getCustomMeasurementFileConfigEnum(String measurementEnumName)
  {
	Assert.expect(measurementEnumName != null);
    for (ConfigEnum current : _customMeasurementConfigEnum)
    {
      if (current.getKey().toLowerCase().equals(measurementEnumName.replace(" ", "").toLowerCase()))
        return current;
    }
    return null;
  }
  
  /**
   * @author Janan Wong
   */
  public static List<ConfigEnum> getNonMeasurementConfigEnums()
  {
	  return _nonMeasurementConfigEnum;
  }
  
}
