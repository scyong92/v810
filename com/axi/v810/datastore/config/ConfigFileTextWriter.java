package com.axi.v810.datastore.config;

import java.io.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 * This class writes key value pairs to disk without losing any
 * formatting or comments of the original configuration file.
 * The supported formats are:
 *
 * <pre>
 * key = value
 *
 * @author Bill Darbie
 */
class ConfigFileTextWriter
{
  // look for key=value
  //                                                                        =
  private Pattern _keyValuePattern = Pattern.compile("^\\s*(([^#=\\s]+)(\\s*=\\s*)((\\\\#|[^#=])*))\\s*([#?].*)?$");
  private Comparator<ConfigEnum> _configEnumCaseInsensitiveKeyComparator = new Comparator<ConfigEnum>()
  {
    public int compare(ConfigEnum lhs, ConfigEnum rhs)
    {
      return lhs.getKey().compareToIgnoreCase(rhs.getKey());
    }
  };
  
  private static final String _equalsStr = " = ";

  /**
   * @author Bill Darbie
   */
  void saveValuesToDisk(Map<ConfigEnum, Object> keyToValueMap) throws DatastoreException
  {
    Assert.expect(keyToValueMap != null);

    // create a map from file name to key value pairs
    Map<String, Map<String, Object>> fileNameToKeyToValueMap = new HashMap<String, Map<String, Object>>();
    for (Map.Entry<ConfigEnum, Object> entry : keyToValueMap.entrySet())
    {
      ConfigEnum configEnum = entry.getKey();
      Object value = entry.getValue();
      String fileName = configEnum.getFileName();

      Map<String, Object> aFilesKeyToValueMap = fileNameToKeyToValueMap.get(fileName);
      if (aFilesKeyToValueMap == null)
      {
        aFilesKeyToValueMap = new HashMap<String, Object>();
        aFilesKeyToValueMap.put(configEnum.getKey(), value);
        fileNameToKeyToValueMap.put(fileName, aFilesKeyToValueMap);
      }
      else
      {
        aFilesKeyToValueMap.put(configEnum.getKey(), value);
      }
    }

    // now write out all new values for a single file at once
    for (Map.Entry<String, Map<String, Object>> entry : fileNameToKeyToValueMap.entrySet())
    {
      String fileName = entry.getKey();
      Map<String, Object> theKeyToValueMap = entry.getValue();

      saveValuesToDisk(fileName, theKeyToValueMap);
    }
  }

  /**
   * @author George A. David
   */
  public void saveValuesToDiskAsNewFile(String fileName, List<ConfigEnum> configEnums) throws DatastoreException
  {
    Assert.expect(fileName != null);
    Assert.expect(configEnums != null);

    Set<ConfigEnum> sortedConfigEnums = new TreeSet<ConfigEnum>(_configEnumCaseInsensitiveKeyComparator);
    sortedConfigEnums.addAll(configEnums);

    FileWriterUtilAxi writerUtil = new FileWriterUtilAxi(fileName, false);
    writerUtil.open();
    for(ConfigEnum configEnum : sortedConfigEnums)
    {
      writerUtil.writeln(configEnum.getKey() + "=" + createStringValue(configEnum.getValue()));
    }
    writerUtil.close();
  }

  /**
   * @author Bill Darbie
   */
  private void saveValuesToDisk(String fileName, Map<String, Object> keyToValueMap) throws DatastoreException
  {
    Assert.expect(fileName != null);
    Assert.expect(keyToValueMap != null);

    Set<String> keySet = keyToValueMap.keySet();

    int numKeys = keySet.size();
    int numModifiedLines = 0;
    // open the file for reading
    String inputFileName = fileName;
    FileReader fr = null;
    try
    {
      fr = new FileReader(inputFileName);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException ex = new CannotOpenFileDatastoreException(inputFileName);
      ex.initCause(fnfe);
      throw ex;
    }
    BufferedReader is = new BufferedReader(fr);

    // open a temporary file for writing
    String outputFileName = inputFileName + FileName.getTempFileExtension();
    FileWriter fw = null;
    try
    {
      fw = new FileWriter(outputFileName);
    }
    catch(IOException ioe)
    {
      DatastoreException ex = new CannotCreateFileDatastoreException(outputFileName);
      ex.initCause(ioe);
      throw ex;
    }
    BufferedWriter bw = new BufferedWriter(fw);
    PrintWriter os = new PrintWriter(bw);

    try
    {
      // OK, now read in the input file and write out the modified file to
      // the output file

      String line = null;
      do
      {
        try
        {
          line = is.readLine();
        }
        catch(IOException ioe)
        {
          DatastoreException ex = new CannotReadDatastoreException(inputFileName);
          ex.initCause(ioe);
          throw ex;
        }
        if (line == null)
          continue;

        if (numModifiedLines < numKeys)
        {
          Matcher keyValueMatcher = _keyValuePattern.matcher(line);

          if (keyValueMatcher.matches())
          {
            String keyValueStr = keyValueMatcher.group(1);
            String keyStr = keyValueMatcher.group(2);
            String equalsStr = keyValueMatcher.group(3);
            String valueStr = keyValueMatcher.group(6);

            Object value = keyToValueMap.get(keyStr);

            if (value != null)
            {
              ++numModifiedLines;
              String stringValue = createStringValue(value);

              // change keyValueStr so it has \\ in front of all special characters so it will be interpreted
              // literally
              keyValueStr = keyValueStr.replaceAll("(\\W)","\\\\$1");
              // now replace the non comment part of the line with the new value
              line = line.replaceFirst(keyValueStr, keyStr + equalsStr + stringValue);
            }
          }
        }
        os.println(line);
      } while(line != null);
    }
    finally
    {
      // close the files
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch (Exception ex)
        {
          // do nothing
        }
      }

      if (os != null)
        os.close();

      // a line should have been modified for each key or there was a bug
      Assert.expect(numModifiedLines == numKeys);
    }

    // now move the temporary file over to the original file
    FileUtilAxi.rename(outputFileName, inputFileName);
  }

  /**
   * @author George A. David
   */
  private String createStringValue(Object value)
  {
    String stringValue = value.toString();
    // change \ to \\
    stringValue = stringValue.replaceAll("\\\\", "\\\\\\\\");
    // change any # to \#
    stringValue = stringValue.replaceAll("#", "\\\\\\\\#");
    // change any $ to \$
    stringValue = stringValue.replaceAll("(\\$)", "\\\\$1");

    return stringValue;
  }

  /**
   * Save the current key value pair on disk in the appropriate file.
   * @author Bill Darbie
   */
  void saveValueToDisk(ConfigEnum key, Object value) throws DatastoreException
  {
    Assert.expect(key != null);
    Assert.expect(value != null);

    Map<String, Object> keyValueMap = new HashMap<String, Object>();
    keyValueMap.put(key.getKey(), value);
    saveValuesToDisk(key.getFileName(), keyValueMap);
  }

  /**
   * @author George A. David
   */
  public void saveValuesToDisk(Collection<ConfigEnum> configEnums) throws DatastoreException
  {
    saveValuesToDisk(getConfigEnumToValueMap(configEnums));
  }

  /**
   * @author George A. David
   */
  private Map<ConfigEnum, Object> getConfigEnumToValueMap(Collection<ConfigEnum> configEnums)
  {
    Assert.expect(configEnums != null);

    Map<ConfigEnum, Object> configEnumToValueMap = new HashMap<ConfigEnum, Object>();

    // fill in the keyToIdValueMap with all values for this configEnum
    for (ConfigEnum aConfigEnum : configEnums)
    {
      configEnumToValueMap.put(aConfigEnum, aConfigEnum.getValue());
    }

    return configEnumToValueMap;
  }
  
  /**
   * @param configEnums ConfigEnums with missing keys in sequence.
   * @author Siew Yeng
   */
  public void updateValueToDiskWithMissingKeys(String fileName, List<ConfigEnum> configEnums) throws DatastoreException
  {
    Assert.expect(fileName != null);
    Assert.expect(configEnums != null);

    //Siew Yeng - XCR-2969 - create map for the config file read
    Map<String, String> keyToConfigMap = new LinkedHashMap();
    int numKeys = configEnums.size();
    int numModifiedLines = 0;
    // open the file for reading
    String inputFileName = fileName;
    FileReader fr = null;
    try
    {
      fr = new FileReader(inputFileName);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException ex = new CannotOpenFileDatastoreException(inputFileName);
      ex.initCause(fnfe);
      throw ex;
    }
    BufferedReader is = new BufferedReader(fr);

    // open a temporary file for writing
    String outputFileName = inputFileName + FileName.getTempFileExtension();
    FileWriter fw = null;
    try
    {
      fw = new FileWriter(outputFileName);
    }
    catch(IOException ioe)
    {
      DatastoreException ex = new CannotCreateFileDatastoreException(outputFileName);
      ex.initCause(ioe);
      throw ex;
    }
    BufferedWriter bw = new BufferedWriter(fw);
    PrintWriter os = new PrintWriter(bw);

    try
    {
      // OK, now read in the input file and write out the modified file to
      // the output file
      String line = null;
      do
      {
        try
        {
          line = is.readLine();
        }
        catch(IOException ioe)
        {
          DatastoreException ex = new CannotReadDatastoreException(inputFileName);
          ex.initCause(ioe);
          throw ex;
        }
        if (line == null)
          continue;

        Matcher keyValueMatcher = _keyValuePattern.matcher(line);
        if (keyValueMatcher.matches())
        {
          //Siew Yeng - XCR-2969
          String keyStr = keyValueMatcher.group(2);
          keyToConfigMap.put(keyStr, line);
        }
        else
          os.println(line);
      } while(line != null);
      
      for(ConfigEnum configEnum : configEnums)
      {
        if(keyToConfigMap.containsKey(configEnum.getKey()))
        {
          os.println(keyToConfigMap.get(configEnum.getKey()));
          numModifiedLines++;
        }
        else
        {
          //Siew Yeng - XCR-2969 - handle missing key
          Object value;
          if(configEnum.getType().equals(TypeEnum.INTEGER))
          {
            value = (Integer)configEnum.getDefaultValue();
          }
          else if(configEnum.getType().equals(TypeEnum.BOOLEAN))
          {
            value = (Boolean)configEnum.getDefaultValue();
          }
          else if(configEnum.getType().equals(TypeEnum.DOUBLE))
          {
            value = (Double)configEnum.getDefaultValue();
          }
          else if(configEnum.getType().equals(TypeEnum.LONG))
          {
            value = (Long)configEnum.getDefaultValue();
          }
          else if(configEnum.getType().equals(TypeEnum.BINARY))
          {
            value = StringUtil.convertUnicodeStringIntoHexCodedString((String)configEnum.getDefaultValue());
          }
          else
          {
            value = (String)configEnum.getDefaultValue();              
          }
          String keyValueString = configEnum.getKey() + _equalsStr + createStringValue(value);
          os.println(keyValueString);
          numModifiedLines++;
        }
      }
      keyToConfigMap.clear();
    }
    finally
    {
      // close the files
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch (Exception ex)
        {
          // do nothing
        }
      }

      if (os != null)
        os.close();

      // a line should have been modified for each key or there was a bug
      Assert.expect(numModifiedLines == numKeys);
    }

    // now move the temporary file over to the original file
    FileUtilAxi.rename(outputFileName, inputFileName);
  }
}
