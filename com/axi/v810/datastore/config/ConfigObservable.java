package com.axi.v810.datastore.config;

import com.axi.util.*;
import java.util.*;

/**
 * This class allows Observers to be updated each time something in the config changes.
 * @author Andy Mechtenberg
 */
public class ConfigObservable extends Observable
{
  private static ConfigObservable _instance = null;
  private int _numDisables = 0;

  /**
   * Do not allow the constructor to be called.
   * @author Andy Mechtenberg
   */
  private ConfigObservable()
  {
    // do nothing
  }

  /**
  * @return an instance of this object.
  * @author Andy Mechtenberg
  */
  public static synchronized ConfigObservable getInstance()
  {
    if (_instance == null)
      _instance = new ConfigObservable();

    return _instance;
  }

  /**
   * Each datastore object should call this method when ever it does something that changes the
   * state of the data.  Any Observers of this class will be notified that the datastore state
   * has changed.
   *
   * @author Bill Darbie
   */
  public void stateChanged(Object configChange)
  {
    Assert.expect(configChange != null);

    if (isEnabled())
    {
      setChanged();
      notifyObservers(configChange);
    }
  }

  /**
   * Set enable to true to cause config events to trigger update() calls.
   * When enable is set to false no updates will be made.  Note that you must
   * pair each setEnabled(false) with a setEnabled(true) because this method
   * keeps track of nested calls in order to work properly.
   *
   * @author Bill Darbie
   * @author Laura Cormos
   */
  public void setEnabled(boolean enable)
  {
    if (enable)
      --_numDisables;
    else
      ++_numDisables;
  }

  /**
   * @author Bill Darbie
   * @author Laura Cormos
   */
  public boolean isEnabled()
  {
    if (_numDisables == 0)
      return true;

    return false;
  }
}


