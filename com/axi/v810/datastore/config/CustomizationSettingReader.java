/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.datastore.config;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 *
 * @author weng-jian.eoh
 */
public class CustomizationSettingReader
{
  private static CustomizationSettingReader _customizeSettingReader = null;
  private Config _config;
  private String _currentConfig;
  
  /**
   * @author weng-jian.eoh
   */
  private CustomizationSettingReader() 
  {
    //do nothing
  }
  
  /**
   * @author weng-jian.eoh
   */
  public static CustomizationSettingReader getInstance()
  {
    if (_customizeSettingReader == null)
    {
      _customizeSettingReader = new CustomizationSettingReader();
    }
    return _customizeSettingReader;
  }
  
  /**
   * XCR-3436
   * @author weng-jian.eoh
   * @param customizeConfigName
   * @throws DatastoreException 
   */
  public synchronized void load(String customizeConfigName) throws DatastoreException
  {
    initializeConfig();
    Assert.expect(_config != null);
    Assert.expect(customizeConfigName != null);

    if(_currentConfig == null)
    {        
      _currentConfig = customizeConfigName;
    }
        
    if(FileUtilAxi.exists(getCustomizeSettingConfigFullPath(customizeConfigName)) == false)
    {
      String customizeSettingConfigPath = getCustomizeSettingConfigFullPath(_config.getStringValue(SoftwareConfigEnum.CUSTOMIZE_SETTING_CONFIGURATION_NAME_TO_USE));
      if(FileUtilAxi.exists(customizeSettingConfigPath) == false)
      {
        if (FileUtilAxi.exists(Directory.getCustomizeSettingDir()) == false)
        {
          FileUtilAxi.createDirectory(Directory.getCustomizeSettingDir());
        }
          
        FileUtilAxi.createEmptyFile(customizeSettingConfigPath);
      }
    }
    
    _config.load(getCustomizeSettingConfigFullPath(customizeConfigName));
  }
  
  /**
   * XCR-3436
   * @author weng-jian.eoh
   */
  private void initializeConfig()
  {
    if (_config == null)
    {
      _config = Config.getInstance();
    }

    Assert.expect(_config != null);
  }
  
  /**
   * XCR-3436
   * @author weng-jian.eoh
   * @return 
   */
  public String getCustomizationSettingConfigurationName()
  {
    initializeConfig();
    Assert.expect(_config != null);
    String configurationFileName = _config.getStringValue(SoftwareConfigEnum.CUSTOMIZE_SETTING_CONFIGURATION_NAME_TO_USE);
    Assert.expect(configurationFileName != null);
    return configurationFileName;
  }
  
  static private String getCustomizeSettingConfigFullPath(String configName)
  {
    Assert.expect(configName != null);
    return configName.endsWith(FileName.getConfigFileExtension()) ? configName : FileName.getCustomizeConfigurationFullPath(configName);
  }
}
