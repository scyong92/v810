package com.axi.v810.datastore.config;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * This exception is thrown if two keys are set in an invalid way.
 * @author Bill Darbie
 */
public class InvalidConfigurationDatastoreException extends DatastoreException
{
  /**
   * @author Bill Darbie
   */
  public InvalidConfigurationDatastoreException(String key1,
                                                String value1,
                                                String key2,
                                                String value2,
                                                String configFile)
  {
    super(new LocalizedString("DS_ERROR_INVALID_CONFIG_KEY", new Object[]{key1, value1, key2, value2, configFile}));
    Assert.expect(key1 != null);
    Assert.expect(value1 != null);
    Assert.expect(key2 != null);
    Assert.expect(value2 != null);
    Assert.expect(configFile != null);
  }
}
