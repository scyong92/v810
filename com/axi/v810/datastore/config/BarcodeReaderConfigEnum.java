package com.axi.v810.datastore.config;

import java.io.*;

import com.axi.v810.datastore.*;
import java.util.*;
import com.axi.util.*;

/**
 * This class contains all the barcode reader settings.
 * @author Bob Balliew
 */
public class BarcodeReaderConfigEnum extends ConfigEnum implements Serializable
{
  private static int _index = -1;
  private static final String _DEFAULT_BARCODE_CONFIG = FileName.getBarcodeReaderConfigurationFullPath(Config.getInstance().getStringValue(SoftwareConfigEnum.BARCODE_READER_CONFIGURATION_NAME_TO_USE));

  private static List<ConfigEnum> _barcodeReaderConfigEnums = new ArrayList<ConfigEnum>();
  
  private Object _defaultValue;

  /**
   * @param id int value of the enumeration.
   * @param key the key part of the key value pair in the barcode reader config file
   * @param filename String which holds the barcode reader configuration filename.
   * @param type TypeEnum which determines the type of value associated with this key.
   * @param valid array of Objects which holds what values are valid. (can be null)
   * @author Bob Balliew
   */
  protected BarcodeReaderConfigEnum(int id,
                                    String key,
                                    String filename,
                                    TypeEnum type,
                                    Object[] valid,
                                    Object defaultValue)
  {
    super(id,
          key,
          filename,
          type,
          valid);
    _defaultValue = defaultValue;
    _barcodeReaderConfigEnums.add(this);
  }

  public static final BarcodeReaderConfigEnum BARCODE_READER_NUMBER_OF_SCANNERS =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderNumberOfScanners",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.INTEGER,
                                  range(0, 16),
                                  1);

  public static final BarcodeReaderConfigEnum BARCODE_READER_OUTPUT_BUFFER_SIZE =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderOutputBufferSize",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.INTEGER,
                                  lowerBound(16),
                                  256);

  public static final BarcodeReaderConfigEnum BARCODE_READER_INPUT_BUFFER_SIZE =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderInputBufferSize",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.INTEGER,
                                  lowerBound(16),
                                  1024);

  public static final BarcodeReaderConfigEnum BARCODE_READER_CHARSET_NAME =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderCharsetName",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.STRING,
                                  new Object[]
                                  {"US-ASCII", "ISO-8859-1", "UTF-8"},
                                  "US-ASCII");

  public static final BarcodeReaderConfigEnum BARCODE_READER_SERIAL_PORT_NAME =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderSerialPortName",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.STRING,
                                  new Object[]
                                  {"COM1", "COM2", "COM3", "COM4", "COM5", "COM6"},
                                  "COM1");

  public static final BarcodeReaderConfigEnum BARCODE_READER_RS232_DATA_BITS =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderRs232DataBits",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.STRING,
                                  new Object[]
                                  {"5", "6", "7", "8"},
                                  "7");

  public static final BarcodeReaderConfigEnum BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_IN =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderRs232RtsCtsFlowControlIn",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BOOLEAN,
                                  booleanRange(),
                                  false);

  public static final BarcodeReaderConfigEnum BARCODE_READER_RS232_RTS_CTS_FLOW_CONTROL_OUT =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderRs232RtsCtsFlowControlOut",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BOOLEAN,
                                  booleanRange(),
                                  false);

  public static final BarcodeReaderConfigEnum BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_IN =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderRs232XonXoffFlowControlIn",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BOOLEAN,
                                  booleanRange(),
                                  false);

  public static final BarcodeReaderConfigEnum BARCODE_READER_RS232_XON_XOFF_FLOW_CONTROL_OUT =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderRs232XonXoffFlowControlOut",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BOOLEAN,
                                  booleanRange(),
                                  false);

  public static final BarcodeReaderConfigEnum BARCODE_READER_RS232_PARITY =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderRs232Parity",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.STRING,
                                  new Object[]
                                  {"EVEN", "NONE", "ODD"},
                                  "EVEN");

  public static final BarcodeReaderConfigEnum BARCODE_READER_RS232_STOP_BITS =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderRs232StopBits",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.STRING,
                                  new Object[]
                                  {"1", "2"},
                                  "1");

  public static final BarcodeReaderConfigEnum BARCODE_READER_RS232_BITS_PER_SECOND =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderRs232BitsPerSecond",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.STRING,
                                  new Object[]
                                  {
                                    "75",
                                    "110",
                                    "134",
                                    "150",
                                    "300",
                                    "600",
                                    "1200",
                                    "2400",
                                    "4800",
                                    "9600",
                                    "19200",
                                    "38400",
                                    "57600",
                                    "115200"
                                  },
                                  "9600");

  public static final BarcodeReaderConfigEnum BARCODE_READER_RS232_APPLICATION_NAME =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderRs232ApplicationName",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.STRING,
                                  null,
                                  "v810_barcode_reader");

  public static final BarcodeReaderConfigEnum BARCODE_READER_RS232_OPEN_TIMEOUT_IN_MILLISECONDS =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderRs232OpenTimeoutInMilliseconds",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.INTEGER,
                                  range(0, 65000),
                                  0);

  public static final BarcodeReaderConfigEnum BARCODE_READER_ENABLE_RECEIVE_FRAMING =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderEnableReceiveFraming",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BOOLEAN,
                                  booleanRange(),
                                  false);

  public static final BarcodeReaderConfigEnum BARCODE_READER_RECEIVE_FRAMING_BYTE =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderReceiveFramingByte",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.INTEGER,
                                  range(0x00, 0xFF),
                                  13);

  public static final BarcodeReaderConfigEnum BARCODE_READER_ENABLE_RECEIVE_THRESHOLD =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderEnableReceiveThreshold",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BOOLEAN,
                                  booleanRange(),
                                  false);

  public static final BarcodeReaderConfigEnum BARCODE_READER_RECEIVE_THRESHOLD_COUNT =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderReceiveThresholdCount",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.INTEGER,
                                  range(0, Integer.MAX_VALUE - 1),
                                  16);

  public static final BarcodeReaderConfigEnum BARCODE_READER_ENABLE_RECEIVE_TIMEOUT =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderEnableReceiveTimeout",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BOOLEAN,
                                  booleanRange(),
                                  true);

  public static final BarcodeReaderConfigEnum BARCODE_READER_RECEIVE_TIMEOUT_IN_MILLISECONDS =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderReceiveTimeoutInMilliseconds",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.INTEGER,
                                  range(0, Integer.MAX_VALUE - 1),
                                  3000);

  public static final BarcodeReaderConfigEnum BARCODE_READER_ENABLE_NO_READ_MESSAGE =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderEnableNoReadMessage",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BOOLEAN,
                                  booleanRange(),
                                  true);

  public static final BarcodeReaderConfigEnum BARCODE_READER_NO_READ_MESSAGE =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderReceiveNoReadMessage",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "NOREAD");

  public static final BarcodeReaderConfigEnum BARCODE_READER_ENABLE_BAD_SYMBOL_MESSAGE =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderEnableBadSymbolMessage",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BOOLEAN,
                                  booleanRange(),
                                  false);

  public static final BarcodeReaderConfigEnum BARCODE_READER_BAD_SYMBOL_MESSAGE =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderReceiveBadSymbolMessage",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "BADCODE");

  public static final BarcodeReaderConfigEnum BARCODE_READER_ENABLE_NO_LABEL_MESSAGE =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderEnableNoLabelMessage",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BOOLEAN,
                                  booleanRange(),
                                  false);

  public static final BarcodeReaderConfigEnum BARCODE_READER_NO_LABEL_MESSAGE =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderReceiveNoLabelMessage",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "NOLABEL");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_1 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_1",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "<\u001d>");
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_1 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_1",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_1 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_1",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_1 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_1",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "\r\n");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_2 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_2",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_2 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_2",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_2 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_2",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_2 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_2",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_3 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_3",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_3 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_3",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_3 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_3",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_3 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_3",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_4 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_4",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_4 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_4",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_4 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_4",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_4 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_4",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_5 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_5",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_5 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_5",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_5 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_5",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_5 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_5",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_6 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_6",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_6 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_6",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_6 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_6",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_6 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_6",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_7 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_7",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_7 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_7",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_7 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_7",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_7 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_7",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_8 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_8",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_8 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_8",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_8 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_8",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_8 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_8",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_9 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_9",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_9 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_9",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_9 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_9",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_9 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_9",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_10 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_10",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_10 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_10",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_10 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_10",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_10 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_10",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_11 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_11",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_11 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_11",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_11 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_11",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_11 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_11",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_12 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_12",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_12 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_12",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_12 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_12",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_12 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_12",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_13 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_13",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_13 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_13",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_13 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_13",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_13 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_13",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_14 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_14",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_14 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_14",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_14 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_14",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_14 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_14",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_15 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_15",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_15 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_15",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");
 
  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_15 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_15",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_15 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_15",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_TRIGGER_SCANNER_16 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderTriggerScanner_16",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_STOP_SCANNER_16 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderStopScanner_16",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_PREAMBLE_SCANNER_16 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPreambleScanner_16",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  public static final BarcodeReaderConfigEnum BARCODE_READER_POSTAMBLE_SCANNER_16 =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderPostambleScanner_16",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BINARY,
                                  null,
                                  "");

  // Wei Chin
  public static final BarcodeReaderConfigEnum BARCODE_READER_ENABLE_RESPONSE_DELAY =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderEnableResponseDelay",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.BOOLEAN,
                                  booleanRange(),
                                  false);

  public static final BarcodeReaderConfigEnum BARCODE_READER_RESPONSE_DELAY_IN_MILLISECONDS =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderResponseDelayInMilliseconds",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.INTEGER,
                                  range(0, Integer.MAX_VALUE - 1),
                                  3000);
  
  public static final BarcodeReaderConfigEnum BARCODE_READER_SCAN_METHOD =
      new BarcodeReaderConfigEnum(++_index,
                                  "barcodeReaderScanMethod",
                                  _DEFAULT_BARCODE_CONFIG,
                                  TypeEnum.STRING,
                                  new Object[]
                                  {BarcodeReaderScanMethodEnum.SCAN_BEFORE_BOARD_LOAD.toString(), BarcodeReaderScanMethodEnum.SCAN_DURING_BOARD_LOADING.toString()},
                                  BarcodeReaderScanMethodEnum.SCAN_BEFORE_BOARD_LOAD.toString());

  /**
   * @return a List of all ConfigEnums available.
   * @author Bill Darbie
   */
  public static List<ConfigEnum> getBarCodeReaderConfigEnums()
  {
    Assert.expect(_barcodeReaderConfigEnums != null);

    return _barcodeReaderConfigEnums;
  }

  /**
   * @author Bill Darbie
   */
  public List<ConfigEnum> getConfigEnums()
  {
    Assert.expect(_barcodeReaderConfigEnums != null);

    return _barcodeReaderConfigEnums;
  }
  
  /*
   * @author Siew Yeng
   */
  protected Object getDefaultValue()
  {    
    return _defaultValue;
  }
}

