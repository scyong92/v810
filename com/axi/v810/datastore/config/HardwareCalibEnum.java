package com.axi.v810.datastore.config;

import java.lang.reflect.Field;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.autoCal.EdgeMTFBusinessException;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;

/**
 * This class contains all enumerations for all the calibration settings of the test system.
 * ONLY calibration information should be put in this file.  NO configuration settings belong here!
 *
 * The only possible instances of the class are found in the static final instances
 * defined within the class.  Adding, deleting or modifying a configuration is
 * done in this file only.
 * To add a configuration value, create a new static final instance and pass this
 * information to the constructor:
 *    name of key found in the configuration file
 *    name of the file that contains the key/value pair
 *    type of the configuration value
 *    array which holds the valid range of values (can be null)
 *    default value if no value is found (can be null)
 *    description key to use for localization
 * AND
 * call the setDefault method on the new instance for each system type in the static class initializer
 *
 * @author Bill Darbie
 * @author Rick Gaudette
 */
public class HardwareCalibEnum extends ConfigEnum
{
  // the config file names
  private static final String _HARDWARE_CALIB = FileName.getHardwareCalibFullPath();
  private static int _index = -1;

  private static List<ConfigEnum> _hardwareCalibEnums = new ArrayList<ConfigEnum>();
  private Map<String, Object> _hardwareCalibFileToDefaultValueMap = new HashMap<String, Object>();

  // no longer use this
  private Object _defaultValue;

  /**
   * @param id int value of the enumeration.
   * @param key the key part of the key value pair in the config file
   * @param filename String which holds the configuration file that has this key.
   * @param type TypeEnum which determines the type of value associated with this key.
   * @param valid array of Objects which holds what values are valid. (can be null)
   * @author Bill Darbie
   */
  protected HardwareCalibEnum(int id,
                              String key,
                              String filename,
                              TypeEnum type,
                              Object[] valid,
                              Object defaultValue)
  {
    super(id,
          key,
          filename,
          type,
          valid);

    Assert.expect(defaultValue != null);
    // no longer use the value here
    _defaultValue = defaultValue;
    _hardwareCalibEnums.add(this);
  }

  public static final HardwareCalibEnum RUN_ALL_CALS_ON_SOFTWARE_STARTUP =
      new HardwareCalibEnum(++_index,
                            "runAllCalsOnSoftwareStartup",
                            _HARDWARE_CALIB,
                            TypeEnum.BOOLEAN,
                            new Object[]
                            {new Boolean(true), new Boolean(false)},
                            false);

  //
  // the location of the xray spot in the imaging plane
  //
  /** @todo wpd - have Eddie use this only from XraySource */
  //                       Xray spot position
  // The location of the xray spot in the reference plane.
  // This value is in machine coordinates.  (i.e Relative to the homed position
  // of the system fiducial)
  public static final HardwareCalibEnum XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "xRaySpotXPositionInMachineCoordinatesNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            53252586);

  public static final HardwareCalibEnum XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "xRaySpotYPositionInMachineCoordinatesNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            531352028);
  
  // High Magnification
  public static final HardwareCalibEnum HIGH_MAG_XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagXRaySpotXPositionInMachineCoordinatesNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            53252586);

  public static final HardwareCalibEnum HIGH_MAG_XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagXRaySpotYPositionInMachineCoordinatesNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            531352028);

  /* The Default Xray Spot values can be in hardware.calib now that it is binary */
  public static final HardwareCalibEnum DEFAULT_XRAY_SPOT_LOCATION_X_NANOMETERS =
      new HardwareCalibEnum(++_index,
                             "defaultXraySpotLocationXNanometers",
                             _HARDWARE_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             53252586);

  public static final HardwareCalibEnum DEFAULT_XRAY_SPOT_LOCATION_Y_NANOMETERS =
      new HardwareCalibEnum(++_index,
                             "defaultXraySpotLocationYNanometers",
                             _HARDWARE_CALIB,
                             TypeEnum.INTEGER,
                             null,
                             531352028);

  public static final HardwareCalibEnum REFERENCE_PLANE_MAGNIFICATION_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "referencePlaneMagnificationAdjustmentLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  //-------------------------------------------------------------------------------------
  // For system fiducial cal there are four values stored for each of the 14 cameras
  //-------------------------------------------------------------------------------------
  // Camera 14
  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera0StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            30403603);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera0StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            530384100);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_0_ROW =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera0Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_0_COLUMN =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera0Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);
  // Camera 1
  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera1StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            53418035);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera1StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            539003844);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_1_ROW =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera1Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_1_COLUMN =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera1Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 2
  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera2StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            45818355);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera2StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            553520452);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_2_ROW =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera2Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_2_COLUMN =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera2Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 3
  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera3StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            33366259);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera3StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            546928644);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_3_ROW =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera3Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_3_COLUMN =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera3Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 4
  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera4StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            12192819);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera4StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            556235204);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_4_ROW =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera4Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_4_COLUMN =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera4Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 5
  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera5StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            18004339);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera5StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            542714276);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_5_ROW =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera5Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_5_COLUMN =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera5Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 6
  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera6StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            15151411);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera6StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            536520740);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_6_ROW =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera6Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_6_COLUMN =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera6Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 7
  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera7StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            7974387);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera7StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            520593924);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_7_ROW =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera7Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_7_COLUMN =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera7Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 8
  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera8StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            10099859);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera8StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            509117188);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_8_ROW =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera8Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_8_COLUMN =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera8Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 9
  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera9StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            26542803);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera9StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            503899012);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_9_ROW =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera9Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_9_COLUMN =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera9Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 10
  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera10StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            33435347);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera10StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            514351620);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_10_ROW =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera10Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_10_COLUMN =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera10Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 11
  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera11StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            50101811);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera11StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            508105252);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_11_ROW =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera11Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_11_COLUMN =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera11Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 12
  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera12StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            52706835);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera12StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            518155524);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_12_ROW =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera12Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_12_COLUMN =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera12Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 13
  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera13StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            43725395);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera13StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            522378020);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_13_ROW =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera13Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_CAMERA_13_COLUMN =
      new HardwareCalibEnum(++_index,
                            "systemOffsetsCamera13Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);
  

public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera0StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            30403603);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSHIGH_MAG_ystemOffsetsCamera0StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            530384100);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_ROW =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera0Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_COLUMN =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera0Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);
  // Camera 1
  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera1StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            53418035);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera1StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            539003844);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_ROW =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera1Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_COLUMN =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera1Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 2
  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera2StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            45818355);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera2StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            553520452);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_ROW =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera2Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_COLUMN =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera2Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 3
  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera3StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            33366259);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera3StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            546928644);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_ROW =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera3Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_COLUMN =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera3Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 4
  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera4StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            12192819);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera4StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            556235204);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_ROW =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera4Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_COLUMN =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera4Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 5
  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera5StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            18004339);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera5StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            542714276);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_ROW =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera5Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_COLUMN =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera5Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 6
  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera6StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            15151411);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera6StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            536520740);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_ROW =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera6Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_COLUMN =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera6Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 7
  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera7StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            7974387);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera7StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            520593924);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_ROW =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera7Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_COLUMN =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera7Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 8
  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera8StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            10099859);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera8StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            509117188);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_ROW =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera8Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_COLUMN =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera8Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 9
  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera9StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            26542803);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera9StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            503899012);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_ROW =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera9Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_COLUMN =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera9Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 10
  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera10StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            33435347);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera10StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            514351620);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_ROW =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera10Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_COLUMN =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera10Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 11
  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera11StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            50101811);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera11StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            508105252);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_ROW =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera11Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_COLUMN =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera11Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 12
  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera12StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            52706835);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera12StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            518155524);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_ROW =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera12Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_COLUMN =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera12Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  // Camera 13
  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera13StageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            43725395);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera13StageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            522378020);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_ROW =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera13Row",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_COLUMN =
      new HardwareCalibEnum(++_index,
                            "highMagSystemOffsetsCamera13Column",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            544);

  /** @todo wpd - remove this */
  // Early proto systems have shadows on the coupon during adjustments. This
  // additional projection width is added to the right side and effectively
  // moves the fiducial to the left and out of the shade. Not needed on later systems.
  public static final HardwareCalibEnum ADDITIONAL_PROJECTION_WIDTH_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "additionalProjectionWidthNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum SYSTEM_OFFSET_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "systemOffsetLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  // Motion Control Axes Hysteresis Adjustment
  public static final HardwareCalibEnum MOTION_CONTROL_X_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "xAxisHysteresisOffsetInNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "yAxisHysteresisOffsetInNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            160000);
  
  public static final HardwareCalibEnum MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS_FOR_HIGH_MAG =
      new HardwareCalibEnum(++_index,
                            "yAxisHysteresisOffsetInNanometersForHighMag",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            160000);

  public static final HardwareCalibEnum MOTION_CONTROL_RAIL_WIDTH_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "railWidthAxisHysteresisOffsetInNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum HYSTERESIS_ADJUSTMENT_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "hysteresisAdjustmentLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  // Camera Grayscale
  public static final HardwareCalibEnum CAMERA_GRAYSCALE_GROUP_ONE_LIST_OF_CAMERAS =
      new HardwareCalibEnum(++_index,
                            "cameraGrayscaleGroupOneListOfCameras",
                            _HARDWARE_CALIB,
                            TypeEnum.STRING,
                            null,
                            "0,1,2,3,4,10,11,12,13");

  public static final HardwareCalibEnum CAMERA_GRAYSCALE_GROUP_TWO_LIST_OF_CAMERAS =
      new HardwareCalibEnum(++_index,
                            "cameraGrayscaleGroupTwoListOfCameras",
                            _HARDWARE_CALIB,
                            TypeEnum.STRING,
                            null,
                            "none");


  public static final HardwareCalibEnum CAMERA_GRAYSCALE_GROUP_THREE_LIST_OF_CAMERAS =
      new HardwareCalibEnum(++_index,
                            "cameraGrayscaleGroupThreeListOfCameras",
                            _HARDWARE_CALIB,
                            TypeEnum.STRING,
                            null,
                            "5,6,7,8,9");

  public static final HardwareCalibEnum CAMERA_GRAYSCALE_STAGE_POSITION_1_X_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "cameraGrayscaleStagePosition1Xnanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            19685000);

  public static final HardwareCalibEnum CAMERA_GRAYSCALE_STAGE_POSITION_1_Y_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "cameraGrayscaleStagePosition1Ynanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            454025000);

  public static final HardwareCalibEnum CAMERA_GRAYSCALE_STAGE_POSITION_2_X_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "cameraGrayscaleStagePosition2Xnanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum CAMERA_GRAYSCALE_STAGE_POSITION_2_Y_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "cameraGrayscaleStagePosition2Ynanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum CAMERA_GRAYSCALE_STAGE_POSITION_3_X_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "cameraGrayscaleStagePosition3Xnanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            482600000);

  public static final HardwareCalibEnum CAMERA_GRAYSCALE_STAGE_POSITION_3_Y_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "cameraGrayscaleStagePosition3Ynanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            454025000);
  
  public static final HardwareCalibEnum CAMERA_CLEAR_STAGE_SKIP_AWAY_POSITION_X_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "cameraClearStageSkipAwayXnanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            254000000);
  
  public static final HardwareCalibEnum CAMERA_CLEAR_STAGE_SKIP_AWAY_POSITION_Y_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "cameraClearStageSkipAwayYnanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            96520000);
  
  public static final HardwareCalibEnum CAMERA_CLEAR_WITHOUT_STAGE_SKIP_AWAY_RAILWIDTH_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "cameraClearWithoutStageSkipAwayRailWidthnanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            101600000);

  public static final HardwareCalibEnum CAMERA_GRAYSCALE_COMPLETE_ADJUSTMENT_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "cameraGrayscaleCompleteAdjustmentLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  //Sharpness confirmation
  //frequency to run when triggered
  public static final HardwareCalibEnum SHARPNESS_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "sharpnessConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1);

  public static final HardwareCalibEnum XRAY_SPOT_DEFLECTION_ADJUSTMENT_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "xraySpotDeflectionAdjustmentLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  //Camera Calibration Confirmation values
  public static final HardwareCalibEnum CAMERA_ADJUSTMENT_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "cameraAdjustmentConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1); // frequency to run when triggered

  //Camera Calibration Confirmation values
  public static final HardwareCalibEnum HIGH_MAG_CAMERA_ADJUSTMENT_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "highMagCameraAdjustmentConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1); // frequency to run when triggered

  //Legacy Xray Source Confirmation values
  public static final HardwareCalibEnum LEGACY_XRAY_SOURCE_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "legacyXraySourceConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1); // frequency to run when triggered


  //Camera Dark Image Confirmation values
  public static final HardwareCalibEnum CAMERA_DARK_IMAGE_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "cameraDarkImageConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1); // This task should run every time it is requested to

  //Camera Dark Image Confirmation values
  public static final HardwareCalibEnum HIGH_MAG_CAMERA_DARK_IMAGE_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "highMagCameraDarkImageConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1); // This task should run every time it is requested to

  //Camera Light Image All Groups Confirmation values
  public static final HardwareCalibEnum CAMERA_LIGHT_IMAGE_ALL_GROUPS_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "cameraLightAllGroupsImageConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1); // This task should run every time it is requested to

  //Camera Light Image Group One Confirmation values
  public static final HardwareCalibEnum CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "cameraLightGroupConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1);

  public static final HardwareCalibEnum CAMERA_LIGHT_IMAGE_GROUP_ONE_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "cameraLightGroupOneImageConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1);

  //Camera Light Image Group Two Confirmation values
  public static final HardwareCalibEnum CAMERA_LIGHT_IMAGE_GROUP_TWO_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "cameraLightImageGroupTwoConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1); // This task should run every time it is requested to

  //Camera Light Image Group Three Confirmation values
  public static final HardwareCalibEnum CAMERA_LIGHT_IMAGE_GROUP_THREE_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "cameraLightImageGroupThreeConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1); // This task should run every time it is requested to

  //Camera Light Image All Groups Confirmation values
  public static final HardwareCalibEnum HIGH_MAG_CAMERA_LIGHT_IMAGE_ALL_GROUPS_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "highMagCameraLightAllGroupsImageConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1); // This task should run every time it is requested to

  //Camera Light Image Group One Confirmation values
  public static final HardwareCalibEnum HIGH_MAG_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "highMagCameraLightGroupConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1);

  public static final HardwareCalibEnum HIGH_MAG_CAMERA_LIGHT_IMAGE_GROUP_ONE_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "highMagCameraLightGroupOneImageConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1);

  //Camera Light Image Group Two Confirmation values
  public static final HardwareCalibEnum HIGH_MAG_CAMERA_LIGHT_IMAGE_GROUP_TWO_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "highMagCameraLightImageGroupTwoConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1); // This task should run every time it is requested to

  //Camera Light Image Group Three Confirmation values
  public static final HardwareCalibEnum HIGH_MAG_CAMERA_LIGHT_IMAGE_GROUP_THREE_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "highMagCameraLightImageGroupThreeConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1); // This task should run every time it is requested to

  // Area Mode Image Confirmation values
  public static final HardwareCalibEnum AREA_MODE_IMAGE_CONFIRM_FREQUENCY =
      new HardwareCalibEnum(++_index,
                            "areaModeConfirmFrequency",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            1); // This task should run every time it is requested to


  ///////////////// Enum values for saving time stamps for Adjustments and Confirmations
  public static final HardwareCalibEnum LIGHT_IMAGE_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "lightImageConfirmationLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum LIGHT_IMAGE_GROUP_ONE_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "lightImageConfirmationGroupOneLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum LIGHT_IMAGE_GROUP_TWO_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "lightImageConfirmationGroupTwoLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum LIGHT_IMAGE_GROUP_THREE_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "lightImageConfirmationGroupThreeLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum LIGHT_IMAGE_ALL_GROUPS_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "lightImageConfirmationAllGroupsLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_LIGHT_IMAGE_GROUP_ONE_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "highMagLightImageConfirmationGroupOneLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_LIGHT_IMAGE_GROUP_TWO_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "highMagLightImageConfirmationGroupTwoLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_LIGHT_IMAGE_GROUP_THREE_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "highMagLightImageConfirmationGroupThreeLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_LIGHT_IMAGE_ALL_GROUPS_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "highMagLightImageConfirmationAllGroupsLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum DARK_IMAGE_GROUP_ONE_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "darkImageConfirmationLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_DARK_IMAGE_GROUP_ONE_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "highMagDarkImageConfirmationLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum CAMERA_CALIBRATION_CONFIRMATION_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "cameraAdjustmentFileConfirmationLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum HIGH_MAG_CAMERA_CALIBRATION_CONFIRMATION_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "highMagCameraAdjustmentFileConfirmationLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum CONFIRM_LEGACY_XRAY_SOURCE_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "confirmLegacyXraySourceLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum CONFIRM_COMMUNICATION_NETWORK_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "confirmCommunicationNetworkLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum CONFIRM_COMMUNICATION_WITH_CAMERAS_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "confirmCommunicationWithCamerasLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum CONFIRM_COMMUNICATION_WITH_IRPS_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "confirmCommunicationsWithIRPsLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

    public static final HardwareCalibEnum CONFIRM_COMMUNICATION_WITH_RMS_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "confirmCommunicationsWithRMSLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum CONFIRM_COMMUNICATION_WITH_SWITCH_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "confirmCommunicationsWithSwitchLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum CONFIRM_PANEL_HANDLING_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "confirmPanelHandlingLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum CONFIRM_MOTION_REPEATABILITY_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "confirmMotionRepeatabilityLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);
  
//  public static final HardwareCalibEnum CONFIRM_OPTICAL_MEASUREMENT_REPEATABILITY_LAST_TIME_RUN =
//      new HardwareCalibEnum(++_index,
//                            "confirmOpticalMeasurementRepeatabilityLastTimeRun",
//                            _HARDWARE_CALIB,
//                            TypeEnum.LONG,
//                            null,
//                            0);

  public static final HardwareCalibEnum XRAY_SPOT_CALIBRATION_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "xraySpotCalibrationLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum CONFIRM_SYSTEM_MTF_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "confirmSystemMTFLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum CONFIRM_MAG_COUPON_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "confirmMagCouponLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum CONFIRM_SYSTEM_FIDUCIAL_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "confirmSystemFiducialLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum CONFIRM_SYSTEM_FIDUCIAL_B_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "confirmSystemFiducialBLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  public static final HardwareCalibEnum CONFIRM_AREA_MODE_IMAGE_LAST_TIME_RUN =
      new HardwareCalibEnum(++_index,
                            "confirmAreaModeImageLastTimeRun",
                            _HARDWARE_CALIB,
                            TypeEnum.LONG,
                            null,
                            0);

  ///////////////////// End of time stamp enum values

  // Xray Safety Test values - not really calib values but this is the best place for them

  public static final HardwareCalibEnum XRAY_SAFETY_TEST_STAGE_X_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "xraySafetyTestStageXPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            228600000);    // 9000 mils

  public static final HardwareCalibEnum XRAY_SAFETY_TEST_STAGE_Y_POSITION_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "xraySafetyTestStageYPositionNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            279400000);    // 11000 mils

  public static final HardwareCalibEnum XRAY_SAFETY_TEST_RAIL_WIDTH_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "xraySafetyTestRailWidthNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            457200000);    // 18000 mils

  /* ---------- Values moved from hardware.config to protect IP ----------- */

  //
  //The physical dimension of the system fiducial
  //
  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_COUPON_X_SIZE_IN_NANOMETERS =
    new HardwareCalibEnum(++_index,
                           "systemFiducialCouponXSizeInNanometers",
                           _HARDWARE_CALIB,
                           TypeEnum.INTEGER,
                           null,
                           1270000);

  public static final HardwareCalibEnum SYSTEM_FIDUCIAL_COUPON_Y_SIZE_IN_NANOMETERS =
    new HardwareCalibEnum(++_index,
                           "systemFiducialCouponYSizeInNanometers",
                           _HARDWARE_CALIB,
                           TypeEnum.INTEGER,
                           null,
                           2540000);

  public static final HardwareCalibEnum X_BORDER_FOR_SYSTEM_FIDUCIAL_LOCATION_IN_NANOMETERS =
    new HardwareCalibEnum(++_index,
                           "xBorderForSystemFiducialLocationInNanometers",
                           _HARDWARE_CALIB,
                           TypeEnum.INTEGER,
                           null,
                           685984);

  public static final HardwareCalibEnum Y_BORDER_FOR_SYSTEM_FIDUCIAL_LOCATION_IN_NANOMETERS =
    new HardwareCalibEnum(++_index,
                           "yBorderForSystemFiducialLocationInNanometers",
                           _HARDWARE_CALIB,
                           TypeEnum.INTEGER,
                           null,
                           685984);

  public static final HardwareCalibEnum COUPON_INDENTATION_IN_Y_FROM_RAILS_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "couponIndentationInYFromRailsNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            18250000);


  public static final HardwareCalibEnum DISTANCE_IN_X_BETWEEN_FIDUCIAL_POINTS_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "distanceInXBetweenFiducialPointsNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            395136780);

  public static final HardwareCalibEnum DISTANCE_IN_Y_BETWEEN_FIDUCIAL_POINTS_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "distanceInYBetweenFiducialPointsNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            0);

  public static final HardwareCalibEnum MAG_TRIANGLE_WIDTH_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "magTriangleWidthNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            2000000);

  public static final HardwareCalibEnum MAG_TRIANGLE_HEIGHT_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "magTriangleHeightNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            2000000);

  public static final HardwareCalibEnum CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED =
      new HardwareCalibEnum(++_index,
                            "cameraSegmentWidthWithSynthesized",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            271);

  public static final HardwareCalibEnum SYSCOUP_FIDUCIAL_POINT_X_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "sysCouponFiducialPointXNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            12660000);

  public static final HardwareCalibEnum SYSCOUP_FIDUCIAL_POINT_Y_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "sysCouponFiducialPointYNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            11050000);

  public static final HardwareCalibEnum SYSCOUP_QUADRILATERAL_UPPER_LEFT_X_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "sysCouponQuadrilateralUpperLeftXNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            21930000);

  public static final HardwareCalibEnum SYSCOUP_QUADRILATERAL_UPPER_LEFT_Y_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "sysCouponQuadrilateralUpperLeftYNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            14050000);

  public static final HardwareCalibEnum SYSCOUP_QUADRILATERAL_UPPER_RIGHT_X_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "sysCouponQuadrilateralUpperRightXNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            5420000);

  public static final HardwareCalibEnum SYSCOUP_QUADRILATERAL_UPPER_RIGHT_Y_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "sysCouponQuadrilateralUpperRightYNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            12890000);

  public static final HardwareCalibEnum SYSCOUP_QUADRILATERAL_LOWER_LEFT_X_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "sysCouponQuadrilateralLowerLeftXNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            21710000);

  public static final HardwareCalibEnum SYSCOUP_QUADRILATERAL_LOWER_LEFT_Y_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "sysCouponQuadrilateralLowerLeftYNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            17250000);

  public static final HardwareCalibEnum SYSCOUP_QUADRILATERAL_LOWER_RIGHT_X_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "sysCouponQuadrilateralLowerRightXNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            5710000);

  public static final HardwareCalibEnum SYSCOUP_QUADRILATERAL_LOWER_RIGHT_Y_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "sysCouponQuadrilateralLowerRightYNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            18360000);
  
//  //
//  // Optical Calibration Profile
//  //
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_PROFILE_1_OFFSET =
//      new HardwareCalibEnum(++_index,
//                             "calOpticalCalibrationProfile1Offset",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             0);
//  
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_PROFILE_2_OFFSET =
//      new HardwareCalibEnum(++_index,
//                             "calOpticalCalibrationProfile2Offset",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             0);
//  
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_PROFILE_3_OFFSET =
//      new HardwareCalibEnum(++_index,
//                             "calOpticalCalibrationProfile3Offset",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             0);
//  
//  //
//  // Optical Calibration values
//  //
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_LAST_TIME_RUN =
//      new HardwareCalibEnum(++_index,
//                             "calOpticalCalibrationAdjustmentLastTimeRun",
//                             _HARDWARE_CALIB,
//                             TypeEnum.LONG,
//                             null,
//                             0);
//  
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "opticalCalibrationReferencePlaneCamera1StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             115000000);
//  
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "opticalCalibrationReferencePlaneCamera1StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             17000000);
//  
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "opticalCalibrationObjectPlaneMinusTwoCamera1StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             85000000);
//  
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "opticalCalibrationObjectPlaneMinusTwoCamera1StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             17000000);
//  
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "opticalCalibrationObjectPlanePlusThreeCamera1StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             55000000);
//  
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "opticalCalibrationObjectPlanePlusThreeCamera1StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             17000000);
//  
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "opticalCalibrationReferencePlaneCamera2StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             115000000);
//  
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "opticalCalibrationReferencePlaneCamera2StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             575000000);
//  
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "opticalCalibrationObjectPlaneMinusTwoCamera2StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             85000000);
//  
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "opticalCalibrationObjectPlaneMinusTwoCamera2StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             575000000);
//  
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "opticalCalibrationObjectPlanePlusThreeCamera2StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             55000000);
//  
//  public static final HardwareCalibEnum OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "opticalCalibrationObjectPlanePlusThreeCamera2StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             575000000);
//  
//  //
//  // Optical Reference Plane values
//  //
//  public static final HardwareCalibEnum OPTICAL_REFERENCE_PLANE_DEFAULT_PSP_Z_HEIGHT_IN_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "opticalReferencePlaneDefaultPspZHeightInNanometers",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             334022);
//  
//  public static final HardwareCalibEnum OPTICAL_REFERENCE_PLANE_PSP_Z_HEIGHT_IN_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "opticalReferencePlanePspZHeightInNanometers",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             334022);
//  
//  //
//  // Optical Fringe Pattern Uniformity Calibration values
//  //
//  public static final HardwareCalibEnum CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_1_INTENSITY_RATIO =
//      new HardwareCalibEnum(++_index,
//                             "calFringePatternUniformityCamera1IntensityRatio",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             1.0);
//  
//  public static final HardwareCalibEnum CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_2_INTENSITY_RATIO =
//      new HardwareCalibEnum(++_index,
//                             "calFringePatternUniformityCamera2IntensityRatio",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             1.0);
//  
//  //
//  // Optical Fringe Pattern Global Intensity values
//  //
//  public static final HardwareCalibEnum CAL_FRINGE_PATTERN_CAMERA_1_GLOBAL_INTENSITY =
//      new HardwareCalibEnum(++_index,
//                             "calFringePatternCamera1GlobalIntensity",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             0.9);
//  
//  public static final HardwareCalibEnum CAL_FRINGE_PATTERN_CAMERA_2_GLOBAL_INTENSITY =
//      new HardwareCalibEnum(++_index,
//                             "calFringePatternCamera2GlobalIntensity",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             0.9);
//  
//  //
//  // Optical System Fiducial Calibration values
//  //
//  public static final HardwareCalibEnum CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calSystemFiducialCamera1StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             400000000);
//  
//  public static final HardwareCalibEnum CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calSystemFiducialCamera1StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             3000000);
//  
//  public static final HardwareCalibEnum CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calSystemFiducialCamera2StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             400000000);
//  
//  public static final HardwareCalibEnum CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calSystemFiducialCamera2StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             592000000);
//  
//  public static final HardwareCalibEnum CAL_SYSTEM_FIDUCIAL_LAST_TIME_RUN =
//      new HardwareCalibEnum(++_index,
//                            "calSystemFiducialConfirmationLastTimeRun",
//                            _HARDWARE_CALIB,
//                            TypeEnum.LONG,
//                            null,
//                            0);
//  
//  //
//  // Optical Reference Plane Calibration values
//  //
//  public static final HardwareCalibEnum CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calOpticalReferencePlaneCamera1StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             53009800);
//  
//  public static final HardwareCalibEnum CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calOpticalReferencePLaneCamera1StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             217525600);
//  
//  public static final HardwareCalibEnum CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calOpticalReferencePlaneCamera2StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             450000000);
//  
//  public static final HardwareCalibEnum CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calOpticalReferencePlaneCamera2StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             550000000);
//  
//  public static final HardwareCalibEnum CAL_OPTICAL_REFERENCE_PLANE_LAST_TIME_RUN =
//      new HardwareCalibEnum(++_index,
//                            "calOpticalReferencePlaneConfirmationLastTimeRun",
//                            _HARDWARE_CALIB,
//                            TypeEnum.LONG,
//                            null,
//                            0);
//  
//  //
//  // Optical Fiducial Rotation Confirmation values
//  //
//  public static final HardwareCalibEnum CAL_SYSTEM_FIDUCIAL_ROTATION_LAST_TIME_RUN =
//      new HardwareCalibEnum(++_index,
//                            "calSystemFiducialRotationConfirmationLastTimeRun",
//                            _HARDWARE_CALIB,
//                            TypeEnum.LONG,
//                            null,
//                            0);
//  
//  //
//  // Optical Camera Light Image Confirmation values
//  //
//  public static final HardwareCalibEnum OPTICAL_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY =
//      new HardwareCalibEnum(++_index,
//                            "opticalCameraLightImageConfirmFrequency",
//                            _HARDWARE_CALIB,
//                            TypeEnum.INTEGER,
//                            null,
//                            1); // This task should run every time it is requested to
//  
//  public static final HardwareCalibEnum OPTICAL_CAMERA_LIGHT_IMAGE_LAST_TIME_RUN =
//      new HardwareCalibEnum(++_index,
//                            "opticalCameraLightImageConfirmationLastTimeRun",
//                            _HARDWARE_CALIB,
//                            TypeEnum.LONG,
//                            null,
//                            0);
//  
//  public static final HardwareCalibEnum CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "confirmOpticalCameraGrayCardCamera1StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             30000000);
//    
//  public static final HardwareCalibEnum CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "confirmOpticalCameraGrayCardCamera1StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             10000000);
//
//  public static final HardwareCalibEnum CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "confirmOpticalCameraGrayCardCamera2StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             30000000);
//
//  public static final HardwareCalibEnum CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "confirmOpticalCameraGrayCardCamera2StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             593000000);
//
//  public static final HardwareCalibEnum OPTICAL_CAMERA_1_GAIN_FACTOR =
//      new HardwareCalibEnum(++_index,
//                             "opticalCamera1GainFactor",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             11.0);
//
//  public static final HardwareCalibEnum OPTICAL_CAMERA_2_GAIN_FACTOR =
//      new HardwareCalibEnum(++_index,
//                             "opticalCamera2GainFactor",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             11.0);
//  
//  //
//  // Optical Magnification Confirmation values
//  //
//  public static final HardwareCalibEnum OPTICAL_REFERENCE_PLANE_MAGNIFICATION =
//          new HardwareCalibEnum(++_index,
//                             "opticalReferencePlaneMagnification",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             5.6);
//  
//  public static final HardwareCalibEnum OPTICAL_CAMERA_PIXEL_SIZE =
//          new HardwareCalibEnum(++_index,
//                             "opticalCameraPixelSize",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             6.0);
//  
//  public static final HardwareCalibEnum OPTICAL_NOMINAL_CALIBRATION_JIG_HOLE_DIAMETER =
//          new HardwareCalibEnum(++_index,
//                             "opticalNominalCalibrationJigHoleDiameter",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             7.0);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationReferencePlaneCamera1StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             113000000);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationReferencePlaneCamera1StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             2000000);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationObjectPlaneMinusTwoCamera1StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             88000000);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationObjectPlaneMinusTwoCamera1StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             2000000);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationObjectPlanePlusThreeCamera1StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             63000000);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationObjectPlanePlusThreeCamera1StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             2000000);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationReferencePlaneCamera2StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             113000000);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationReferencePlaneCamera2StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             592000000);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationObjectPlaneMinusTwoCamera2StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             88000000);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationObjectPlaneMinusTwoCamera2StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             592000000);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationObjectPlanePlusThreeCamera2StagePositionXInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             63000000);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationObjectPlanePlusThreeCamera2StagePositionYInNanoMeters",
//                             _HARDWARE_CALIB,
//                             TypeEnum.INTEGER,
//                             null,
//                             592000000);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_JIG_HOLE_DIAMETER =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera1ReferencePlaneJigHoleDiameter",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             7.5);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera1ObjectPlaneMinusTwoJigHoleDiameter",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             7.5);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera1ObjectPlanePlusThreeJigHoleDiameter",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             7.5);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera1ObjectPlaneMinusOneJigHoleDiameter",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             7.5);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera1ObjectPlanePlusFourJigHoleDiameter",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             7.5);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_JIG_HOLE_DIAMETER =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera2ReferencePlaneJigHoleDiameter",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             7.5);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera2ObjectPlaneMinusTwoJigHoleDiameter",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             7.5);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera2ObjectPlanePlusThreeJigHoleDiameter",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             7.5);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera2ObjectPlaneMinusOneJigHoleDiameter",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             7.5);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera2ObjectPlanePlusFourJigHoleDiameter",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             7.5);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_MAGNIFICATION =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera1ReferencePlaneMagnification",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             0.219428571428571);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera1ObjectPlaneMinusTwoMagnification",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             0.214285714285714);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera1ObjectPlanePlusThreeMagnification",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             0.227142857142857);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera1ObjectPlaneMinusOneMagnification",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             0.2117142857142855);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera1ObjectPlanePlusFourMagnification",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             0.22971428571428566666666666666667);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_MAGNIFICATION =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera2ReferencePlaneMagnification",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             0.219428571428571);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera2ObjectPlaneMinusTwoMagnification",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             0.214285714285714);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera2ObjectPlanePlusThreeMagnification",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             0.227142857142857);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera2ObjectPlaneMinusOneMagnification",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             0.2117142857142855);
//  
//  public static final HardwareCalibEnum CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION =
//      new HardwareCalibEnum(++_index,
//                             "calMagnificationCamera2ObjectPlanePlusFourMagnification",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             0.22971428571428566666666666666667);
//  
//  //
//  // Optical System Fiducial Offset values
//  //
//  public static final HardwareCalibEnum CAL_OPTICAL_SYSTEM_FIDUCIAL_ZHEIGHT_IN_MILS =
//      new HardwareCalibEnum(++_index,
//                             "calOpticalSystemFiducialZHeightInMils",
//                             _HARDWARE_CALIB,
//                             TypeEnum.DOUBLE,
//                             null,
//                             -81.0);
//  
//  public static final HardwareCalibEnum CAL_OPTICAL_SYSTEM_FIDUCIAL_OFFSET_LAST_TIME_RUN =
//      new HardwareCalibEnum(++_index,
//                            "calOpticalSystemFiducialOffsetAdjustmentLastTimeRun",
//                            _HARDWARE_CALIB,
//                            TypeEnum.LONG,
//                            null,
//                            0);

  // Note from George A. David
  // The following are locations of the dots one the system coupons. There are
  // 56 dots that surround the triangle and the quadrilateral. Looking at the
  // cad drawing of the system coupon, the dot furthest down and furthest to the
  // left is dot #1. From there, we will number the dots in a clockwise fashion.
  // This is an abitrary naming convention chosen by me. If there is a better
  // way feel free to change it.
  //
  // The cad drawing is such that the right angle is facing to the right and the
  // smallest side of the quadrilateral is on the left. The triangle is also
  // below the quadrilateral. Below is simple asci art to illustrate the
  // orientation of the cad drawing.
  //
  //              ---------- _________
  //            /                      -------- _______
  //           /                                        \
  //          /                                          \
  //         /                                            \
  //        /                                          ____\
  //       /                            _______ ------
  //      /          _________ --------
  //      ----------
  //
  //                            ____
  //                           |   /
  //                           |  /
  //                           | /
  //                           |/
  //
  //
  // all coordinates are from the lower left of the coupon when oriented in the
  // above fashion.
  // see SystemFiducialCoupon for more information


  public static final HardwareCalibEnum SYSCOUP_DOT_WIDTH_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "sysCouponDotWidthNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            130000);

  // this is dot 19, apparently it was the lowest vissible dot that can be
  // seen or should be seen when imaged with a 16 micron system.
  public static final HardwareCalibEnum SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "sysCouponLowerRightVisibleDotYNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            19850000);

  // this is the closest dot to the upper horizontal edge of the quadrilateral
  // that is within the x bounds of the quadrilateral.
  public static final HardwareCalibEnum SYSCOUP_DOT_10_Y_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "sysCouponDot10yNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            12250000);

  // this is the closest dot to the lower horizontal edge of the quadrilateral
  // that is within the x bounds of the quadrilateral.
  public static final HardwareCalibEnum SYSCOUP_DOT_31_Y_NANOMETERS =
      new HardwareCalibEnum(++_index,
                            "sysCouponDot31yNanometers",
                            _HARDWARE_CALIB,
                            TypeEnum.INTEGER,
                            null,
                            19040000);

  public static final HardwareCalibEnum XRAY_TESTER_SYSTEM_TYPE =
      new HardwareCalibEnum(++_index,
                             "xrayTesterSystemType2",
                             _HARDWARE_CALIB,
                             TypeEnum.STRING,
                             getSystemTypeValidValues(),
                             SystemTypeEnum.STANDARD.getName());

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_0_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_0_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_0_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_0_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_1_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_1_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_1_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_1_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);


  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_2_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_2_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_2_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_2_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_3_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_3_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_3_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_3_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_4_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_4_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_4_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_4_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_5_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_5_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_5_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_5_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);


  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_6_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_6_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_6_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_6_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_7_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_7_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_7_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_7_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_8_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_8_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_8_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_8_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_9_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_9_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_9_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_9_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);


  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_10_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_10_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_10_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_10_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_11_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_11_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_11_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_11_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_12_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_12_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_12_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_12_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_13_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_13_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_HIGH_MAG_CAMERA_13_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateHighMagCamera_13_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_0_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_0_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_0_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_0_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_1_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_1_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_1_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_1_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);


  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_2_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_2_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_2_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_2_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_3_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_3_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_3_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_3_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_4_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_4_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_4_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_4_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_5_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_5_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_5_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_5_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);


  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_6_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_6_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_6_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_6_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_7_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_7_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_7_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_7_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_8_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_8_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_8_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_8_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_9_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_9_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_9_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_9_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);


  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_10_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_10_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_10_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_10_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_11_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_11_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_11_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_11_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_12_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_12_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_12_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_12_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_13_X =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_13_X",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  public static final HardwareCalibEnum EDGE_SPREAD_FUNCTION_RATE_LOW_MAG_CAMERA_13_Y =
      new HardwareCalibEnum(++_index,
                            "edgeSpreadFunctionRateLowMagCamera_13_Y",
                            _HARDWARE_CALIB,
                            TypeEnum.DOUBLE,
                            null,
                            1.0);

  /**
   * @author Wei Chin
   */
  static
  {
    // setup the default values
    setDefaultValueForCDNALastTimeRun();
    setDefaultValueForCDNAConfirmFrequency();
    
    // For system fiducial cal there are four values stored for each of the 14 cameras
    setDefaultValueForCameraSettings();
    setDefaultValuesForHighMagCameraSettings();
    
    // setup the PSP Optical Camera default values
//    setPSPOpticalDefaultValues();
    
    setDefaultValuesForEdgeSpreadFunctions();

    // other CD&A default values ...
    HardwareCalibEnum.ADDITIONAL_PROJECTION_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.ADDITIONAL_PROJECTION_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.ADDITIONAL_PROJECTION_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.ADDITIONAL_PROJECTION_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_ONE_LIST_OF_CAMERAS.setDefaultValue(SystemTypeEnum.STANDARD, "0,1,2,3,4,10,11,12,13");
    HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_ONE_LIST_OF_CAMERAS.setDefaultValue(SystemTypeEnum.THROUGHPUT, "0,1,2,3,4,10,11,12,13");
    HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_ONE_LIST_OF_CAMERAS.setDefaultValue(SystemTypeEnum.XXL, "0,1,2,3,4,10,11,12,13");
    HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_ONE_LIST_OF_CAMERAS.setDefaultValue(SystemTypeEnum.S2EX, "0,1,2,3,4,10,11,12,13");

    HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_TWO_LIST_OF_CAMERAS.setDefaultValue(SystemTypeEnum.STANDARD, "none");
    HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_TWO_LIST_OF_CAMERAS.setDefaultValue(SystemTypeEnum.THROUGHPUT, "none");
    HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_TWO_LIST_OF_CAMERAS.setDefaultValue(SystemTypeEnum.XXL, "none");
    HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_TWO_LIST_OF_CAMERAS.setDefaultValue(SystemTypeEnum.S2EX, "none");

    HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_THREE_LIST_OF_CAMERAS.setDefaultValue(SystemTypeEnum.STANDARD, "5,6,7,8,9");
    HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_THREE_LIST_OF_CAMERAS.setDefaultValue(SystemTypeEnum.THROUGHPUT, "5,6,7,8,9");
    HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_THREE_LIST_OF_CAMERAS.setDefaultValue(SystemTypeEnum.XXL, "5,6,7,8,9");
    HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_THREE_LIST_OF_CAMERAS.setDefaultValue(SystemTypeEnum.S2EX, "5,6,7,8,9");

    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 19685000);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 19685000);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 150000000);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 120000000);//99685000);

    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 454025000);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 454025000);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 690000000);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 504025000); //this value is only applicable when AWA is>4.5 inches, should use 504025000 if AWA is set <4.5 inches

    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_2_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_2_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_2_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_2_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_2_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_2_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_2_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_2_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_3_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 482600000);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_3_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 482600000);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_3_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 482600000);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_3_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 482600000);

    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_3_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 454025000);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_3_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 454025000);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_3_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 454025000);
    HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_3_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 454025000);
    
    //Swee Yee Wong - This value should be revised again if the footprint of system is changed significantly
    HardwareCalibEnum.CAMERA_CLEAR_STAGE_SKIP_AWAY_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 254000000);
    HardwareCalibEnum.CAMERA_CLEAR_STAGE_SKIP_AWAY_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 254000000);
    HardwareCalibEnum.CAMERA_CLEAR_STAGE_SKIP_AWAY_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 254000000);
    HardwareCalibEnum.CAMERA_CLEAR_STAGE_SKIP_AWAY_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 254000000);

    HardwareCalibEnum.CAMERA_CLEAR_STAGE_SKIP_AWAY_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 96520000);
    HardwareCalibEnum.CAMERA_CLEAR_STAGE_SKIP_AWAY_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 96520000);
    HardwareCalibEnum.CAMERA_CLEAR_STAGE_SKIP_AWAY_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 96520000);
    HardwareCalibEnum.CAMERA_CLEAR_STAGE_SKIP_AWAY_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 96520000);
    
    //Swee Yee Wong - This value should be revised again if the distance between camera array and xraytube of system is changed significantly
    HardwareCalibEnum.CAMERA_CLEAR_WITHOUT_STAGE_SKIP_AWAY_RAILWIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 101600000); //4inches
    HardwareCalibEnum.CAMERA_CLEAR_WITHOUT_STAGE_SKIP_AWAY_RAILWIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 101600000);
    HardwareCalibEnum.CAMERA_CLEAR_WITHOUT_STAGE_SKIP_AWAY_RAILWIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 101600000);
    HardwareCalibEnum.CAMERA_CLEAR_WITHOUT_STAGE_SKIP_AWAY_RAILWIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 152400000);//6inches

    HardwareCalibEnum.CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED.setDefaultValue(SystemTypeEnum.STANDARD, 271);
    HardwareCalibEnum.CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED.setDefaultValue(SystemTypeEnum.THROUGHPUT, 271);
    HardwareCalibEnum.CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED.setDefaultValue(SystemTypeEnum.XXL, 271);
    HardwareCalibEnum.CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED.setDefaultValue(SystemTypeEnum.S2EX, 271);

    HardwareCalibEnum.COUPON_INDENTATION_IN_Y_FROM_RAILS_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 18250000);
    HardwareCalibEnum.COUPON_INDENTATION_IN_Y_FROM_RAILS_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 18250000);
    HardwareCalibEnum.COUPON_INDENTATION_IN_Y_FROM_RAILS_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 18250000);
    HardwareCalibEnum.COUPON_INDENTATION_IN_Y_FROM_RAILS_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 18250000);

    HardwareCalibEnum.DISTANCE_IN_X_BETWEEN_FIDUCIAL_POINTS_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 395136780);
    HardwareCalibEnum.DISTANCE_IN_X_BETWEEN_FIDUCIAL_POINTS_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 395136780); //398.184mm - From Standard AutoCAD
    HardwareCalibEnum.DISTANCE_IN_X_BETWEEN_FIDUCIAL_POINTS_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 70233000);
    HardwareCalibEnum.DISTANCE_IN_X_BETWEEN_FIDUCIAL_POINTS_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 190273000); //190273000 - from AutoCAD

    HardwareCalibEnum.DISTANCE_IN_Y_BETWEEN_FIDUCIAL_POINTS_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.DISTANCE_IN_Y_BETWEEN_FIDUCIAL_POINTS_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.DISTANCE_IN_Y_BETWEEN_FIDUCIAL_POINTS_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.DISTANCE_IN_Y_BETWEEN_FIDUCIAL_POINTS_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 53252586);
    HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 53252586);
    HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 393823499);
    HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 173715500);

    HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 531352028);
    HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 531352028);
    HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 762104219);
    HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 583954000);

    HardwareCalibEnum.XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 53252586);
    HardwareCalibEnum.XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 53252586);
    HardwareCalibEnum.XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 393823499);
    HardwareCalibEnum.XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 173715500);

    HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 531352028);
    HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 531352028);
    HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 762104219);
    HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 583954000);  //584954000

    HardwareCalibEnum.DEFAULT_XRAY_SPOT_LOCATION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 53252586);
    HardwareCalibEnum.DEFAULT_XRAY_SPOT_LOCATION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 53252586);
    HardwareCalibEnum.DEFAULT_XRAY_SPOT_LOCATION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 393823499);
    HardwareCalibEnum.DEFAULT_XRAY_SPOT_LOCATION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 174215500);  //173715500

    HardwareCalibEnum.DEFAULT_XRAY_SPOT_LOCATION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 531352028);
    HardwareCalibEnum.DEFAULT_XRAY_SPOT_LOCATION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 531352028);
    HardwareCalibEnum.DEFAULT_XRAY_SPOT_LOCATION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 762104219);
    HardwareCalibEnum.DEFAULT_XRAY_SPOT_LOCATION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 583954000);

    HardwareCalibEnum.MAG_TRIANGLE_HEIGHT_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 2000000);
    HardwareCalibEnum.MAG_TRIANGLE_HEIGHT_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 2000000);
    HardwareCalibEnum.MAG_TRIANGLE_HEIGHT_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 2000000);
    HardwareCalibEnum.MAG_TRIANGLE_HEIGHT_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 2000000);

    HardwareCalibEnum.MAG_TRIANGLE_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 2000000);
    HardwareCalibEnum.MAG_TRIANGLE_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 2000000);
    HardwareCalibEnum.MAG_TRIANGLE_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 2000000);
    HardwareCalibEnum.MAG_TRIANGLE_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 2000000);

    HardwareCalibEnum.MOTION_CONTROL_RAIL_WIDTH_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.MOTION_CONTROL_RAIL_WIDTH_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.MOTION_CONTROL_RAIL_WIDTH_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.MOTION_CONTROL_RAIL_WIDTH_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.MOTION_CONTROL_X_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.MOTION_CONTROL_X_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.MOTION_CONTROL_X_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.MOTION_CONTROL_X_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 160000);
    HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 160000);
    HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 160000);
    HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 160000);

    HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS_FOR_HIGH_MAG.setDefaultValue(SystemTypeEnum.STANDARD, 160000);
    HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS_FOR_HIGH_MAG.setDefaultValue(SystemTypeEnum.THROUGHPUT, 160000);
    HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS_FOR_HIGH_MAG.setDefaultValue(SystemTypeEnum.XXL, 160000);
    HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS_FOR_HIGH_MAG.setDefaultValue(SystemTypeEnum.S2EX, 160000);

    HardwareCalibEnum.RUN_ALL_CALS_ON_SOFTWARE_STARTUP.setDefaultValue(SystemTypeEnum.STANDARD, false);
    HardwareCalibEnum.RUN_ALL_CALS_ON_SOFTWARE_STARTUP.setDefaultValue(SystemTypeEnum.THROUGHPUT, false);
    HardwareCalibEnum.RUN_ALL_CALS_ON_SOFTWARE_STARTUP.setDefaultValue(SystemTypeEnum.XXL, false);
    HardwareCalibEnum.RUN_ALL_CALS_ON_SOFTWARE_STARTUP.setDefaultValue(SystemTypeEnum.S2EX, false);

    HardwareCalibEnum.SYSCOUP_DOT_10_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 12250000);
    HardwareCalibEnum.SYSCOUP_DOT_10_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 12250000);
    HardwareCalibEnum.SYSCOUP_DOT_10_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 12250000);
    HardwareCalibEnum.SYSCOUP_DOT_10_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 12250000);

    HardwareCalibEnum.SYSCOUP_DOT_31_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 19040000);
    HardwareCalibEnum.SYSCOUP_DOT_31_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 19040000);
    HardwareCalibEnum.SYSCOUP_DOT_31_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 19040000);
    HardwareCalibEnum.SYSCOUP_DOT_31_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 19040000);

    HardwareCalibEnum.SYSCOUP_DOT_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 130000);
    HardwareCalibEnum.SYSCOUP_DOT_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 130000);
    HardwareCalibEnum.SYSCOUP_DOT_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 130000);
    HardwareCalibEnum.SYSCOUP_DOT_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 130000);

    HardwareCalibEnum.SYSCOUP_FIDUCIAL_POINT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 12660000);
    HardwareCalibEnum.SYSCOUP_FIDUCIAL_POINT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 12660000);
    HardwareCalibEnum.SYSCOUP_FIDUCIAL_POINT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 12660000);
    HardwareCalibEnum.SYSCOUP_FIDUCIAL_POINT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 12660000);

    HardwareCalibEnum.SYSCOUP_FIDUCIAL_POINT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 11050000);
    HardwareCalibEnum.SYSCOUP_FIDUCIAL_POINT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 11050000);
    HardwareCalibEnum.SYSCOUP_FIDUCIAL_POINT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 11050000);
    HardwareCalibEnum.SYSCOUP_FIDUCIAL_POINT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 11050000);

    HardwareCalibEnum.SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 19850000);
    HardwareCalibEnum.SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 19850000);
    HardwareCalibEnum.SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 19850000);
    HardwareCalibEnum.SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 19850000);

    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_LEFT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 21710000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_LEFT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 21710000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_LEFT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 21710000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_LEFT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 21710000);

    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_LEFT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 17250000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_LEFT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 17250000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_LEFT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 17250000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_LEFT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 17250000);

    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_RIGHT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 5710000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_RIGHT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 5710000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_RIGHT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 5710000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_RIGHT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 5710000);

    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_RIGHT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 18360000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_RIGHT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 18360000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_RIGHT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 18360000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_RIGHT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 18360000);

    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_LEFT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 21930000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_LEFT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 21930000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_LEFT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 21930000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_LEFT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 21930000);

    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_LEFT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 14050000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_LEFT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 14050000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_LEFT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 14050000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_LEFT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 14050000);

    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_RIGHT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 5420000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_RIGHT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 5420000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_RIGHT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 5420000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_RIGHT_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 5420000);

    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_RIGHT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 12890000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_RIGHT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 12890000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_RIGHT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 12890000);
    HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_RIGHT_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 12890000);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_COUPON_X_SIZE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 1270000);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_COUPON_X_SIZE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1270000);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_COUPON_X_SIZE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 1270000);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_COUPON_X_SIZE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 1270000);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_COUPON_Y_SIZE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 2540000);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_COUPON_Y_SIZE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 2540000);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_COUPON_Y_SIZE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 2540000);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_COUPON_Y_SIZE_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 2540000);

    HardwareCalibEnum.XRAY_SAFETY_TEST_RAIL_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 457200000);
    HardwareCalibEnum.XRAY_SAFETY_TEST_RAIL_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 457200000);
    HardwareCalibEnum.XRAY_SAFETY_TEST_RAIL_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 660400000);
    HardwareCalibEnum.XRAY_SAFETY_TEST_RAIL_WIDTH_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 457200000);

    HardwareCalibEnum.XRAY_SAFETY_TEST_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 228600000);
    HardwareCalibEnum.XRAY_SAFETY_TEST_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 228600000);
    HardwareCalibEnum.XRAY_SAFETY_TEST_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 330200000);
    HardwareCalibEnum.XRAY_SAFETY_TEST_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 228600000);

    HardwareCalibEnum.XRAY_SAFETY_TEST_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 279400000);
    HardwareCalibEnum.XRAY_SAFETY_TEST_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 279400000);
    HardwareCalibEnum.XRAY_SAFETY_TEST_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 482600000);
    HardwareCalibEnum.XRAY_SAFETY_TEST_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 279400000);

    HardwareCalibEnum.XRAY_TESTER_SYSTEM_TYPE.setDefaultValue(SystemTypeEnum.STANDARD, SystemTypeEnum.STANDARD.getName());
    HardwareCalibEnum.XRAY_TESTER_SYSTEM_TYPE.setDefaultValue(SystemTypeEnum.THROUGHPUT, SystemTypeEnum.THROUGHPUT.getName());
    HardwareCalibEnum.XRAY_TESTER_SYSTEM_TYPE.setDefaultValue(SystemTypeEnum.XXL, SystemTypeEnum.XXL.getName());
    HardwareCalibEnum.XRAY_TESTER_SYSTEM_TYPE.setDefaultValue(SystemTypeEnum.S2EX, SystemTypeEnum.S2EX.getName());

    HardwareCalibEnum.X_BORDER_FOR_SYSTEM_FIDUCIAL_LOCATION_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 685984);
    HardwareCalibEnum.X_BORDER_FOR_SYSTEM_FIDUCIAL_LOCATION_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 685984);
    HardwareCalibEnum.X_BORDER_FOR_SYSTEM_FIDUCIAL_LOCATION_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 685984);
    HardwareCalibEnum.X_BORDER_FOR_SYSTEM_FIDUCIAL_LOCATION_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 685984);

    HardwareCalibEnum.Y_BORDER_FOR_SYSTEM_FIDUCIAL_LOCATION_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 685984);
    HardwareCalibEnum.Y_BORDER_FOR_SYSTEM_FIDUCIAL_LOCATION_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 685984);
    HardwareCalibEnum.Y_BORDER_FOR_SYSTEM_FIDUCIAL_LOCATION_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 685984);
    HardwareCalibEnum.Y_BORDER_FOR_SYSTEM_FIDUCIAL_LOCATION_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 685984);

    // end hardware.calib
    ////////////////////////////////////////
    validateCalibDefaultValue();
  }
  
  /**
   * Move from 
   * @author Wei Chin
   */
//  static private void setPSPOpticalDefaultValues()
//  {
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_CAMERA_1_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.STANDARD, 0.9);
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_CAMERA_1_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.9);
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_CAMERA_1_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.XXL, 0.9);
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_CAMERA_1_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.S2EX, 0.9);
//
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_CAMERA_2_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.STANDARD, 0.9);
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_CAMERA_2_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.9);
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_CAMERA_2_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.XXL, 0.9);
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_CAMERA_2_GLOBAL_INTENSITY.setDefaultValue(SystemTypeEnum.S2EX, 0.9);
//
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_1_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.STANDARD, 1.0);
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_1_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1.0);
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_1_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.XXL, 1.0);
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_1_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.S2EX, 1.0);
//
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_2_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.STANDARD, 1.0);
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_2_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1.0);
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_2_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.XXL, 1.0);
//    HardwareCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_2_INTENSITY_RATIO.setDefaultValue(SystemTypeEnum.S2EX, 1.0);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.5);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.5);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.2117142857142855);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.2117142857142855);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1065);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.2117142857142855);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.214285714285714);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.214285714285714);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1053);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.214285714285714);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.227142857142857);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.227142857142857);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1113);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.227142857142857);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.22971428571428566666666666666667);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.22971428571428566666666666666667);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1125);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.22971428571428566666666666666667);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.2117142857142855);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.2117142857142855);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1064);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.2117142857142855);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.214285714285714);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.214285714285714);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1052);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.214285714285714);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.227142857142857);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.227142857142857);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1112);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.227142857142857);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.22971428571428566666666666666667);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.22971428571428566666666666666667);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1124);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.22971428571428566666666666666667);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.219428571428571);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.219428571428571);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1077);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.219428571428571);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 0.219428571428571);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0.219428571428571);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 0.1076);
//    HardwareCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 0.219428571428571);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 88000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 88000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 396209500);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 88000000);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 2000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 2000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 206678020);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 2000000);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 88000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 88000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 396209500);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 88000000);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 592000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 592000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 810394020);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 592000000);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 63000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 63000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 370209500);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 63000000);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 2000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 2000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 206678020);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 2000000);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 63000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 63000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 370209500);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 63000000);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 592000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 592000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 810394020);
//    HardwareCalibEnum.CAL_MAGNIFICATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 592000000);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 113000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 113000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 421009500);
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 113000000);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 2000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 2000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 206678020);
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 2000000);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 113000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 113000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 421009500);
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 113000000);
//
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 592000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 592000000);
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 810394020);
//    HardwareCalibEnum.CAL_MAGNIFICATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 592000000);
//
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 53009800);
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 53009800);
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 53009800);
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 53009800);
//
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 217525600);
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 217525600);
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 24325600);
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 217525600);
//
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 450000000);
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 450000000);
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 450000000);
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 450000000);
//
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 550000000);
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 550000000);
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 668800000);
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 550000000);
//
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 400000000);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 400000000);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 446698000);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 253184000);
//
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 3000000);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 3000000);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 25400000);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 46154000);
//
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 400000000);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 400000000);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 446698000);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 253672000);
//
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 592000000);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 592000000);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 607885504);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 617901000);
//
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 30000000);
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 30000000);
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 325034500);
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 30000000);
//
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 10000000);
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 10000000);
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 197780020);
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 10000000);
//
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 30000000);
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 30000000);
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 325034500);
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 30000000);
//
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 593000000);
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 593000000);
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 801496020);
//    HardwareCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 593000000);
//
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 85000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 85000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 85000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 85000000);
//
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 17000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 17000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 17000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 17000000);
//
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 85000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 85000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 85000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 85000000);
//
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 575000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 575000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 575000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 575000000);
//
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 55000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 55000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 55000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 55000000);
//
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 17000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 17000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 17000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 17000000);
//
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 55000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 55000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 55000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 55000000);
//
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 575000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 575000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 575000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 575000000);
//
//    HardwareCalibEnum.OPTICAL_CALIBRATION_PROFILE_1_OFFSET.setDefaultValue(SystemTypeEnum.STANDARD, 0);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_PROFILE_1_OFFSET.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_PROFILE_1_OFFSET.setDefaultValue(SystemTypeEnum.XXL, 0);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_PROFILE_1_OFFSET.setDefaultValue(SystemTypeEnum.S2EX, 0);
//
//    HardwareCalibEnum.OPTICAL_CALIBRATION_PROFILE_2_OFFSET.setDefaultValue(SystemTypeEnum.STANDARD, 0);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_PROFILE_2_OFFSET.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_PROFILE_2_OFFSET.setDefaultValue(SystemTypeEnum.XXL, 0);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_PROFILE_2_OFFSET.setDefaultValue(SystemTypeEnum.S2EX, 0);
//
//    HardwareCalibEnum.OPTICAL_CALIBRATION_PROFILE_3_OFFSET.setDefaultValue(SystemTypeEnum.STANDARD, 0);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_PROFILE_3_OFFSET.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_PROFILE_3_OFFSET.setDefaultValue(SystemTypeEnum.XXL, 0);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_PROFILE_3_OFFSET.setDefaultValue(SystemTypeEnum.S2EX, 0);
//
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 115000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 115000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 115000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 115000000);
//
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 17000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 17000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 17000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_1_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 17000000);
//
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 115000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 115000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 115000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_X_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 115000000);
//
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 575000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 575000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 575000000);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_REFERENCE_PLANE_MAGNIFICATION_CAMERA_2_STAGE_POSITION_Y_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 575000000);
//
//    HardwareCalibEnum.OPTICAL_CAMERA_1_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.STANDARD, 11.0);
//    HardwareCalibEnum.OPTICAL_CAMERA_1_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.THROUGHPUT, 11.0);
//    HardwareCalibEnum.OPTICAL_CAMERA_1_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.XXL, 11.0);
//    HardwareCalibEnum.OPTICAL_CAMERA_1_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.S2EX, 11.0);
//
//    HardwareCalibEnum.OPTICAL_CAMERA_2_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.STANDARD, 11.0);
//    HardwareCalibEnum.OPTICAL_CAMERA_2_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.THROUGHPUT, 11.0);
//    HardwareCalibEnum.OPTICAL_CAMERA_2_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.XXL, 11.0);
//    HardwareCalibEnum.OPTICAL_CAMERA_2_GAIN_FACTOR.setDefaultValue(SystemTypeEnum.S2EX, 11.0);
//
//    HardwareCalibEnum.OPTICAL_CAMERA_PIXEL_SIZE.setDefaultValue(SystemTypeEnum.STANDARD, 6.0);
//    HardwareCalibEnum.OPTICAL_CAMERA_PIXEL_SIZE.setDefaultValue(SystemTypeEnum.THROUGHPUT, 6.0);
//    HardwareCalibEnum.OPTICAL_CAMERA_PIXEL_SIZE.setDefaultValue(SystemTypeEnum.XXL, 6.0);
//    HardwareCalibEnum.OPTICAL_CAMERA_PIXEL_SIZE.setDefaultValue(SystemTypeEnum.S2EX, 6.0);
//
//    HardwareCalibEnum.OPTICAL_NOMINAL_CALIBRATION_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.STANDARD, 7.0);
//    HardwareCalibEnum.OPTICAL_NOMINAL_CALIBRATION_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7.0);
//    HardwareCalibEnum.OPTICAL_NOMINAL_CALIBRATION_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.XXL, 7.0);
//    HardwareCalibEnum.OPTICAL_NOMINAL_CALIBRATION_JIG_HOLE_DIAMETER.setDefaultValue(SystemTypeEnum.S2EX, 7.0);
//
//    HardwareCalibEnum.OPTICAL_REFERENCE_PLANE_DEFAULT_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 334022);
//    HardwareCalibEnum.OPTICAL_REFERENCE_PLANE_DEFAULT_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 334022);
//    HardwareCalibEnum.OPTICAL_REFERENCE_PLANE_DEFAULT_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 334022);
//    HardwareCalibEnum.OPTICAL_REFERENCE_PLANE_DEFAULT_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 334022);
//
//    HardwareCalibEnum.OPTICAL_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.STANDARD, 5.6);
//    HardwareCalibEnum.OPTICAL_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.THROUGHPUT, 5.6);
//    HardwareCalibEnum.OPTICAL_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.XXL, 5.6);
//    HardwareCalibEnum.OPTICAL_REFERENCE_PLANE_MAGNIFICATION.setDefaultValue(SystemTypeEnum.S2EX, 5.6);
//
//    HardwareCalibEnum.OPTICAL_REFERENCE_PLANE_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 334022);
//    HardwareCalibEnum.OPTICAL_REFERENCE_PLANE_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 334022);
//    HardwareCalibEnum.OPTICAL_REFERENCE_PLANE_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 334022);
//    HardwareCalibEnum.OPTICAL_REFERENCE_PLANE_PSP_Z_HEIGHT_IN_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 334022);
//
//    HardwareCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_ZHEIGHT_IN_MILS.setDefaultValue(SystemTypeEnum.STANDARD, -86);
//    HardwareCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_ZHEIGHT_IN_MILS.setDefaultValue(SystemTypeEnum.THROUGHPUT, -86);
//    HardwareCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_ZHEIGHT_IN_MILS.setDefaultValue(SystemTypeEnum.XXL, 105);
//    HardwareCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_ZHEIGHT_IN_MILS.setDefaultValue(SystemTypeEnum.S2EX, -86);
//
//    HardwareCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_OFFSET_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
//    HardwareCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_OFFSET_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
//    HardwareCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_OFFSET_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
//    HardwareCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_OFFSET_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);
//
//    HardwareCalibEnum.CONFIRM_OPTICAL_MEASUREMENT_REPEATABILITY_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
//    HardwareCalibEnum.CONFIRM_OPTICAL_MEASUREMENT_REPEATABILITY_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
//    HardwareCalibEnum.CONFIRM_OPTICAL_MEASUREMENT_REPEATABILITY_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
//    HardwareCalibEnum.CONFIRM_OPTICAL_MEASUREMENT_REPEATABILITY_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);
//  }
  
  /**
   * a sanity check to enforce that all enums have a default value for all
   * system types  and initialize the correct file name. 
   * we couldn't do this beforehand because of a circular dependency.
   * @author Wei Chin
   */
  static private void validateCalibDefaultValue()
  {
    for (ConfigEnum calibEnum : _hardwareCalibEnums)
    {
      HardwareCalibEnum hardwareCalib = (HardwareCalibEnum) calibEnum;
      for (SystemTypeEnum systemType : SystemTypeEnum.getAllSystemTypes())
      {
        hardwareCalib.getDefaultValue(systemType);
      }
    }
  }
  
  /**
   * @author Wei Chin
   */
  static private void setDefaultValueForCDNALastTimeRun()
  {
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
//    HardwareCalibEnum.CAL_OPTICAL_REFERENCE_PLANE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);
//
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);
//
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_ROTATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_ROTATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_ROTATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
//    HardwareCalibEnum.CAL_SYSTEM_FIDUCIAL_ROTATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CAMERA_CALIBRATION_CONFIRMATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CAMERA_CALIBRATION_CONFIRMATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CAMERA_CALIBRATION_CONFIRMATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CAMERA_CALIBRATION_CONFIRMATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CAMERA_GRAYSCALE_COMPLETE_ADJUSTMENT_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CAMERA_GRAYSCALE_COMPLETE_ADJUSTMENT_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CAMERA_GRAYSCALE_COMPLETE_ADJUSTMENT_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CAMERA_GRAYSCALE_COMPLETE_ADJUSTMENT_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CONFIRM_AREA_MODE_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CONFIRM_AREA_MODE_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CONFIRM_AREA_MODE_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CONFIRM_AREA_MODE_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CONFIRM_COMMUNICATION_NETWORK_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CONFIRM_COMMUNICATION_NETWORK_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CONFIRM_COMMUNICATION_NETWORK_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CONFIRM_COMMUNICATION_NETWORK_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_CAMERAS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_CAMERAS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_CAMERAS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_CAMERAS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_IRPS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_IRPS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_IRPS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_IRPS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_RMS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_RMS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_RMS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_RMS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_SWITCH_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_SWITCH_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_SWITCH_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_SWITCH_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CONFIRM_LEGACY_XRAY_SOURCE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CONFIRM_LEGACY_XRAY_SOURCE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CONFIRM_LEGACY_XRAY_SOURCE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CONFIRM_LEGACY_XRAY_SOURCE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CONFIRM_MAG_COUPON_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CONFIRM_MAG_COUPON_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CONFIRM_MAG_COUPON_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CONFIRM_MAG_COUPON_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CONFIRM_MOTION_REPEATABILITY_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CONFIRM_MOTION_REPEATABILITY_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CONFIRM_MOTION_REPEATABILITY_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CONFIRM_MOTION_REPEATABILITY_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CONFIRM_PANEL_HANDLING_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CONFIRM_PANEL_HANDLING_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CONFIRM_PANEL_HANDLING_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CONFIRM_PANEL_HANDLING_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CONFIRM_SYSTEM_FIDUCIAL_B_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CONFIRM_SYSTEM_FIDUCIAL_B_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CONFIRM_SYSTEM_FIDUCIAL_B_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CONFIRM_SYSTEM_FIDUCIAL_B_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CONFIRM_SYSTEM_FIDUCIAL_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CONFIRM_SYSTEM_FIDUCIAL_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CONFIRM_SYSTEM_FIDUCIAL_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CONFIRM_SYSTEM_FIDUCIAL_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.CONFIRM_SYSTEM_MTF_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.CONFIRM_SYSTEM_MTF_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.CONFIRM_SYSTEM_MTF_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.CONFIRM_SYSTEM_MTF_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.DARK_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.DARK_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.DARK_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.DARK_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_CAMERA_CALIBRATION_CONFIRMATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_CAMERA_CALIBRATION_CONFIRMATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_CAMERA_CALIBRATION_CONFIRMATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_CAMERA_CALIBRATION_CONFIRMATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_DARK_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_DARK_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_DARK_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_DARK_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_ALL_GROUPS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_ALL_GROUPS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_ALL_GROUPS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_ALL_GROUPS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_GROUP_TWO_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_GROUP_TWO_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_GROUP_TWO_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_GROUP_TWO_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_GROUP_THREE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_GROUP_THREE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_GROUP_THREE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_LIGHT_IMAGE_GROUP_THREE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HYSTERESIS_ADJUSTMENT_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HYSTERESIS_ADJUSTMENT_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HYSTERESIS_ADJUSTMENT_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HYSTERESIS_ADJUSTMENT_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.LIGHT_IMAGE_ALL_GROUPS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.LIGHT_IMAGE_ALL_GROUPS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.LIGHT_IMAGE_ALL_GROUPS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.LIGHT_IMAGE_ALL_GROUPS_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.LIGHT_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.LIGHT_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.LIGHT_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.LIGHT_IMAGE_GROUP_ONE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.LIGHT_IMAGE_GROUP_TWO_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.LIGHT_IMAGE_GROUP_TWO_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.LIGHT_IMAGE_GROUP_TWO_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.LIGHT_IMAGE_GROUP_TWO_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.LIGHT_IMAGE_GROUP_THREE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.LIGHT_IMAGE_GROUP_THREE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.LIGHT_IMAGE_GROUP_THREE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.LIGHT_IMAGE_GROUP_THREE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.LIGHT_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.LIGHT_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.LIGHT_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.LIGHT_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

//    HardwareCalibEnum.OPTICAL_CALIBRATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
//    HardwareCalibEnum.OPTICAL_CALIBRATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);
//
//    HardwareCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
//    HardwareCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
//    HardwareCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
//    HardwareCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.REFERENCE_PLANE_MAGNIFICATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.REFERENCE_PLANE_MAGNIFICATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.REFERENCE_PLANE_MAGNIFICATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.REFERENCE_PLANE_MAGNIFICATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.SYSTEM_OFFSET_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.SYSTEM_OFFSET_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.SYSTEM_OFFSET_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.SYSTEM_OFFSET_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.XRAY_SPOT_CALIBRATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.XRAY_SPOT_CALIBRATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.XRAY_SPOT_CALIBRATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.XRAY_SPOT_CALIBRATION_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.XRAY_SPOT_DEFLECTION_ADJUSTMENT_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.XRAY_SPOT_DEFLECTION_ADJUSTMENT_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.XRAY_SPOT_DEFLECTION_ADJUSTMENT_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.XRAY_SPOT_DEFLECTION_ADJUSTMENT_LAST_TIME_RUN.setDefaultValue(SystemTypeEnum.S2EX, 0);
  }
  
  /**
   * @author Wei Chin
   */
  static private void setDefaultValueForCDNAConfirmFrequency()
  {
    HardwareCalibEnum.AREA_MODE_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.AREA_MODE_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.AREA_MODE_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.AREA_MODE_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.CAMERA_ADJUSTMENT_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.CAMERA_ADJUSTMENT_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.CAMERA_ADJUSTMENT_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.CAMERA_ADJUSTMENT_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.CAMERA_DARK_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.CAMERA_DARK_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.CAMERA_DARK_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.CAMERA_DARK_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_ALL_GROUPS_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_ALL_GROUPS_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_ALL_GROUPS_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_ALL_GROUPS_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_GROUP_ONE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_GROUP_ONE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_GROUP_ONE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_GROUP_ONE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_GROUP_TWO_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_GROUP_TWO_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_GROUP_TWO_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_GROUP_TWO_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_GROUP_THREE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_GROUP_THREE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_GROUP_THREE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.CAMERA_LIGHT_IMAGE_GROUP_THREE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.HIGH_MAG_CAMERA_ADJUSTMENT_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_ADJUSTMENT_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_ADJUSTMENT_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_ADJUSTMENT_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.HIGH_MAG_CAMERA_DARK_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_DARK_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_DARK_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_DARK_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_ALL_GROUPS_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_ALL_GROUPS_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_ALL_GROUPS_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_ALL_GROUPS_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_GROUP_ONE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_GROUP_ONE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_GROUP_ONE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_GROUP_ONE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_GROUP_TWO_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_GROUP_TWO_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_GROUP_TWO_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_GROUP_TWO_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_GROUP_THREE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_GROUP_THREE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_GROUP_THREE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.HIGH_MAG_CAMERA_LIGHT_IMAGE_GROUP_THREE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.LEGACY_XRAY_SOURCE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.LEGACY_XRAY_SOURCE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.LEGACY_XRAY_SOURCE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.LEGACY_XRAY_SOURCE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

    HardwareCalibEnum.SHARPNESS_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
    HardwareCalibEnum.SHARPNESS_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
    HardwareCalibEnum.SHARPNESS_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
    HardwareCalibEnum.SHARPNESS_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

//    HardwareCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.STANDARD, 1);
//    HardwareCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1);
//    HardwareCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.XXL, 1);
//    HardwareCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY.setDefaultValue(SystemTypeEnum.S2EX, 1);

  }
  
  /**
   * @author Wei Chin
   */
  static private void setDefaultValueForCameraSettings()
  {
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    //author Ngie Xing
    //position of camera 0 is not directly under camera spot,
    //for now assuming that camera 0 is under x-ray spot for S2EX
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 30403603);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 30403603);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 393850499);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 171715500);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 530384100);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 530384100);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 762131219);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 585654000);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 53418035);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 53418035);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 367224935);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 194729932);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 539003844);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 539003844);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 750509370);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 594273744);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 45818355);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 45818355);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 369748068);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 187130252);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 553520452);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 553520452);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 736885418);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 608790352);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 33366259);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 33366259);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 389267370);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 174678156);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 546928644);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 546928644);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 730690959);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 602198544);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 12192819);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 12192819);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 397449461);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 153504716);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 556235204);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 556235204);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 743099176);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 611505104);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 18004339);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 18004339);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 417234102);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 159316236);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 542714276);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 542714276);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 735684156);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 597984176);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 15151411);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 15151411);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 420326507);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 156463308);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 536520740);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 536520740);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 747614763);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 591790640);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 7974387);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7974387);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 409664703);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 149286284);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 520593924);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 520593924);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 752627258);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 575863824);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 10099859);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 10099859);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 421170768);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 151411756);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 509117188);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 509117188);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 772363656);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 564387088);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 26542803);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 26542803);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 412149242);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 167854700);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 503899012);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 503899012);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 789596218);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 559168912);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 33435347);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 33435347);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 397367447);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 174747244);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 514351620);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 514351620);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 781771130);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 569621520);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 50101811);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 50101811);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 372232606);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 191413708);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 508105252);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 508105252);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 792818881);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 563375152);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 52706835);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 52706835);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 379131421);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 194018732);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 518155524);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 518155524);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 776768283);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 573425424);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 43725395);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 43725395);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 375744730);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 185037292);

    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 522378020);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 522378020);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 769415981);
    HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 577647920);
  }
  
  /**
   * @author Wei Chin
   */
  static private void setDefaultValuesForHighMagCameraSettings()
  {
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 30403603);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 30403603);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 393850449);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 30403603);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 530384100);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 530384100);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 762131219);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 530384100);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 53418035);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 53418035);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 364591133);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 53418035);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 539003844);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 539003844);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 749359737);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 539003844);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 45818355);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 45818355);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 367363855);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 45818355);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 553520452);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 553520452);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 734388103);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 553520452);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 33366259);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 33366259);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 388814007);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 33366259);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 546928644);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 546928644);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 727580887);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 546928644);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 12192819);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 12192819);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 397805470);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 12192819);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 556235204);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 556235204);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 741216525);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 556235204);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 18004339);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 18004339);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 419547209);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 18004339);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 542714276);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 542714276);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 733068012);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 542714276);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 15151411);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 15151411);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 422945571);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 15151411);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 536520740);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 536520740);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 746178764);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 536520740);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 7974387);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 7974387);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 411229045);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 7974387);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 520593924);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 520593924);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 751687126);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 520593924);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 10099859);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 10099859);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 423873290);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 10099859);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 509117188);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 509117188);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 773375849);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 509117188);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 26542803);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 26542803);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 413959354);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 26542803);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 503899012);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 503899012);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 792313057);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 503899012);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 33435347);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 33435347);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 397715343);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 33435347);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 514351620);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 514351620);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 783713911);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 514351620);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 50101811);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 50101811);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 370094164);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 50101811);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 508105252);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 508105252);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 795854506);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 508105252);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 52706835);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 52706835);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 377675409);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 52706835);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 518155524);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 518155524);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 778216182);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 518155524);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_COLUMN.setDefaultValue(SystemTypeEnum.STANDARD, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_COLUMN.setDefaultValue(SystemTypeEnum.THROUGHPUT, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_COLUMN.setDefaultValue(SystemTypeEnum.XXL, 544);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_COLUMN.setDefaultValue(SystemTypeEnum.S2EX, 544);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_ROW.setDefaultValue(SystemTypeEnum.STANDARD, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_ROW.setDefaultValue(SystemTypeEnum.THROUGHPUT, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_ROW.setDefaultValue(SystemTypeEnum.XXL, 0);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_ROW.setDefaultValue(SystemTypeEnum.S2EX, 0);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 43725395);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 43725395);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 373953707);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 43725395);

    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.STANDARD, 522378020);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.THROUGHPUT, 522378020);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.XXL, 770136590);
    HardwareCalibEnum.HIGH_MAG_SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS.setDefaultValue(SystemTypeEnum.S2EX, 522378020);
  }


  static private void setDefaultValuesForEdgeSpreadFunctions()
  {
    String[] magnificationIDs = {"LOW_MAG", "HIGH_MAG"};
    String[] dimensionIDs = {"X", "Y"};

    for (String magnificationID : magnificationIDs)
    {
      for (int cameraID = 0; cameraID < 14; cameraID++)
      {
        for (String dimensionID : dimensionIDs)
        {
          HardwareCalibEnum hardwareCalibEnum = getEdgeSpreadFunctionRateEnum(magnificationID,
                                                                              cameraID,
                                                                              dimensionID);
          hardwareCalibEnum.setDefaultValue(SystemTypeEnum.STANDARD, 1.0);
          hardwareCalibEnum.setDefaultValue(SystemTypeEnum.THROUGHPUT, 1.0);
          hardwareCalibEnum.setDefaultValue(SystemTypeEnum.XXL, 1.0);
          hardwareCalibEnum.setDefaultValue(SystemTypeEnum.S2EX, 1.0);
        }
      }
    }
  }


  static public HardwareCalibEnum getEdgeSpreadFunctionRateEnum(String magnificationID,
                                                                int cameraID,
                                                                String dimensionID)
  {
    try
    {
      String className = "com.axi.v810.datastore.config.HardwareCalibEnum";
      Class<?> cls = Class.forName(className);
      String field_name = String.format("EDGE_SPREAD_FUNCTION_RATE_%s_CAMERA_%d_%s",
                                        magnificationID,
                                        cameraID,
                                        dimensionID);
      Field fld = cls.getField(field_name);
      return (HardwareCalibEnum) fld.get(cls);
    }
    catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e)
    {
      // Code/strings must be incorrect if we got here, just assert
      assert false;
    }
    // Can't get here, this function either returns within the try or asserts during the catch
    return null;
  }


  /**
   * @return a List of all ConfigEnums available.
   * @author Bill Darbie
   */
  public static List<ConfigEnum> getHardwareCalibEnums()
  {
    Assert.expect(_hardwareCalibEnums != null);

    return _hardwareCalibEnums;
  }

  /**
   * @author Bill Darbie
   */
  public List<ConfigEnum> getConfigEnums()
  {
    Assert.expect(_hardwareCalibEnums != null);

    return _hardwareCalibEnums;
  }

  /**
   * @author Bill Darbie
   */
  protected Object getDefaultValue()
  {
    String fileName = getFileName();
    Object value = _hardwareCalibFileToDefaultValueMap.get(fileName);
    Assert.expect(value != null, "No default value for " + fileName);
    return value;
//    Assert.expect(_defaultValue != null);
//    return _defaultValue;
  }
  
    /**
   * @author Wei Chin
   */
  protected Object getDefaultValue(SystemTypeEnum systemType)
  {
    Assert.expect(systemType != null);
    String hardwareCalibFullPath = FileName.getHardwareCalibFullPath(systemType);
    Object defaultValue = _hardwareCalibFileToDefaultValueMap.get(hardwareCalibFullPath);
    Assert.expect(defaultValue != null);
    
    return defaultValue;
  }
  
  /**
   * @author Wei Chin
   */
  private void setDefaultValue(SystemTypeEnum systemType, Object defaultValue)
  {
    Assert.expect(systemType != null);
    Assert.expect(defaultValue != null);

    Object previous = _hardwareCalibFileToDefaultValueMap.put(FileName.getHardwareCalibFullPath(systemType), defaultValue);
    Assert.expect(previous == null);
  }

  /**
   * creates an object array of valid values for system type names
   * @author George A. David
   */
  public static Object[] getSystemTypeValidValues()
  {
    List<SystemTypeEnum> systemTypes =  SystemTypeEnum.getAllSystemTypes();
    Object[] systemTypeNames = new Object[systemTypes.size()];
    for(int i = 0; i < systemTypes.size(); ++i)
    {
      systemTypeNames[i] = systemTypes.get(i).getName();
    }

    return systemTypeNames;
  }
}
