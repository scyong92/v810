package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.v810.datastore.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class Test_ConfigFileTextReader extends UnitTest
{
  private Map<String, String> _mapping = null;
  private List<LocalizedString> _warnings = new ArrayList<LocalizedString>();

  /**
   * @author Bill Darbie
   */
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_ConfigFileTextReader());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      ConfigFileTextReader parser = new ConfigFileTextReader();

      String dataDir = getTestDataDir();

      System.out.println("\nreading in empty.config");
      String configFile = dataDir + File.separator + "empty.config";
      _mapping = parser.getKeyValueMapping(configFile);
      displayMappingData();

      System.out.println("\nreading in good.config");
      configFile = dataDir + File.separator + "good.config";
      _mapping = parser.getKeyValueMapping(configFile);
      displayMappingData();

      // now try reading a file that does not exist
      try
      {
        _mapping = parser.getKeyValueMapping("nonexistentfile");
        Expect.expect(false);
      }
      catch(Exception e)
      {
        // do nothing
      }

      // now read a corrupt file
      System.out.println("\nreading in corrupt1.config");
      configFile = dataDir + File.separator + "corrupt1.config";
      try
      {
        _mapping = parser.getKeyValueMapping(configFile);
      }
      catch(ConfigFileCorruptDatastoreException de)
      {
        System.out.println(UnitTest.stripOffViewName(de.getLocalizedMessage()));
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * @author Bill Darbie
   */
  private void displayMappingData()
  {
    Map<String, String> sortedMap = new TreeMap<String, String>(_mapping);
    for (String key : sortedMap.keySet())
    {
      String value = (String)sortedMap.get(key);
      System.out.println("key: " + key + " value: " + value);
    }
    for (LocalizedString localizedString : _warnings)
    {
      System.out.println("warning message key is: " + localizedString.getMessageKey());
    }
  }
}
