package com.axi.v810.datastore.config;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Erica Wheatcroft
 */
public class MachineTypeEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;

  public static final MachineTypeEnum ONLINE = new MachineTypeEnum(++_index);
  public static final MachineTypeEnum OFFLINE = new MachineTypeEnum(++_index);
  public static final MachineTypeEnum BOTH = new MachineTypeEnum(++_index);

  /**
   * @author Erica Wheatcroft
   */
  private MachineTypeEnum(int id)
  {
    super(id);
  }
}
