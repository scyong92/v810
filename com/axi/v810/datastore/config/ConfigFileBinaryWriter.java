package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class writes key value pairs to disk without losing any
 * formatting or comments of the original configuration file.
 * The supported formats are:
 *
 * <pre>
 * key = value
 *
 * @author Bill Darbie
 */
class ConfigFileBinaryWriter implements Serializable
{
  /**
   * @author Bill Darbie
   */
  void saveValuesToDisk(List<ConfigEnum> configEnums) throws DatastoreException
  {
    Assert.expect(configEnums != null);

    Set<String> fileNameSet = new HashSet<String>();
    for (ConfigEnum configEnum : configEnums)
    {
      boolean added = fileNameSet.add(configEnum.getFileName());
      if (added)
        saveValueToDisk(configEnum);
    }
  }

  /**
   * Save the current key value pair on disk in the appropriate file.
   * @author Bill Darbie
   */
  void saveValueToDisk(ConfigEnum configEnum) throws DatastoreException
  {
    Assert.expect(configEnum != null);

    Map<Integer, Object> keyIdToValueMap = getKeyIdToValueMap(configEnum);

    // now write it to disk
    String fileName = getFileName(configEnum.getFileName());
    saveValuesToDisk(fileName, keyIdToValueMap);
  }

  /**
   * @author Bill Darbie
   */
  private void saveValuesToDisk(String fileName, Map<Integer, Object> keyIdToValueMap) throws DatastoreException
  {
    Assert.expect(fileName != null);
    Assert.expect(keyIdToValueMap != null);

    FileUtilAxi.saveObjectToSerializedFile(keyIdToValueMap, fileName);
  }

  /**
   * @author Bill Darbie
   */
  private String getFileName(String origFileName)
  {
    Assert.expect(origFileName != null);

    String fileName = origFileName + FileName.getBinaryExtension();
    return fileName;
  }

  /**
   * @author Bill Darbie
   */
  private Map<Integer, Object> getKeyIdToValueMap(ConfigEnum configEnum)
  {
    Assert.expect(configEnum != null);

    Map<Integer, Object> keyIdToValueMap = new HashMap<Integer, Object>();

    // fill in the keyToIdValueMap with all values for this configEnum
    List<ConfigEnum> configEnums = configEnum.getConfigEnums();
    for (ConfigEnum aConfigEnum : configEnums)
    {
      keyIdToValueMap.put(aConfigEnum.getId(), aConfigEnum.getValue());
    }

    return keyIdToValueMap;
  }
}
