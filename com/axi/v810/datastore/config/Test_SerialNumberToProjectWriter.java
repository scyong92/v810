package com.axi.v810.datastore.config;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Bill Darbie
 */
public class Test_SerialNumberToProjectWriter extends UnitTest
{
  private SerialNumberToProjectWriter _writer;
  private SerialNumberToProjectReader _reader;

  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_SerialNumberToProjectWriter());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      _writer = SerialNumberToProjectWriter.getInstance();

      // try a good file
      String fileName = getTestDataDir() + File.separator + "serialNumToProjectWriter.config";
      _writer.setConfigFileNameForUnitTesting(fileName);

      // create some data for the writer to write
      Collection<SerialNumberRegularExpressionData> dataCollection = new ArrayList<SerialNumberRegularExpressionData>();
      SerialNumberRegularExpressionData serialNumData = new SerialNumberRegularExpressionData("1234", "project1", false, "");
      dataCollection.add(serialNumData);
      serialNumData = new SerialNumberRegularExpressionData("12?4", "project2", false, "");
      dataCollection.add(serialNumData);
      serialNumData = new SerialNumberRegularExpressionData("12*4", "project3", false, "");
      dataCollection.add(serialNumData);
      serialNumData = new SerialNumberRegularExpressionData("12.*4", "project4", true, "");
      dataCollection.add(serialNumData);
      serialNumData = new SerialNumberRegularExpressionData("1234[a-z]", "project5", true, "");
      dataCollection.add(serialNumData);

      _writer.write(dataCollection);

      // now check that it was written out correctly by reading it back in
      _reader = SerialNumberToProjectReader.getInstance();
      _reader.setConfigFileNameForUnitTesting(fileName);
      Collection readInDataCollection = _reader.getSerialNumberRegularExpressionData();

      Assert.expect(readInDataCollection.size() == dataCollection.size());

      Iterator origIt = dataCollection.iterator();
      Iterator readIt = readInDataCollection.iterator();
      while (readIt.hasNext())
      {
        SerialNumberRegularExpressionData origData = (SerialNumberRegularExpressionData)origIt.next();
        SerialNumberRegularExpressionData readInData = (SerialNumberRegularExpressionData)readIt.next();

        //System.out.println(origData.getOriginalExpression());
        //System.out.println(readInData.getOriginalExpression());
        Assert.expect(origData.getOriginalExpression().equals(readInData.getOriginalExpression()));

        //System.out.println(origData.getRegularExpression());
        //System.out.println(readInData.getRegularExpression());
        Assert.expect(origData.getRegularExpression().equals(readInData.getRegularExpression()));

        //System.out.println(origData.getProjectName());
        //System.out.println(readInData.getProjectName());
        Assert.expect(origData.getProjectName().equals(readInData.getProjectName()));

        //System.out.println(origData.isInterpretedAsRegularExpression());
        //System.out.println(readInData.isInterpretedAsRegularExpression());
        Assert.expect(origData.isInterpretedAsRegularExpression() == readInData.isInterpretedAsRegularExpression());
      }
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
